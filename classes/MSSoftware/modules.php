<?php
/**
 * MSAdminControlPanel
 * Date: 28/06/18
 */

namespace MSSoftware;

class modules {
    /**
     * I moduli indicati in questo array saranno SEMPRE disponibili per gli utenti SaaS
     * @var array
     */
    private $avail_modules_on_saas = array("dashboard", "abbonamento_saas", "assistenza_ticket", "settings", "payway");

    /**
     * I moduli indicati in questa variabile saranno rimossi da quelli sempre disponibili agli utenti che hanno un abbonamento SaaS scaduto
     * @var array
     */
    private $remove_modules_on_saas_expired = array();

    /**
     * Solo i moduli indicati in questa variabile NON saranno disponibili agli utenti dei SaaS
     * @var array
     */
    private $saas_never_avail_modules = array("sitemanager", "extramodules", "i18n", "manage_email_templates", "redirect");

    public function __construct() {
        global $MSFrameworkDatabase, $MSFrameworkSaaSBase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkSaaSBase = $MSFrameworkSaaSBase;
        $this->MSFrameworkSaaSEnvironments = new \MSFramework\SaaS\environments();

        if($this->MSFrameworkSaaSBase->isSaaSDomain() && $this->MSFrameworkSaaSEnvironments->isExpired()) {
            $this->avail_modules_on_saas = array_diff($this->avail_modules_on_saas, $this->remove_modules_on_saas_expired);
        }
    }

    /**
     * Struttura dell'array che definisce la sezione/modulo
     *
     * name: Il nome del modulo
     * id: Un ID univoco per il modulo (viene utilizzato per identificarlo all'interno dei permessi speciali in "Configurazione produttore"
     * path: Il path del modulo (se lasciato vuoto punta alla dashboard)
     * fa_icon: L'icona di font-awesome da associare
     * superadmin_limit: Se impostato su true, il modulo sarà disponibile solo per i superadmin. In caso contrario, varranno le impostazioni selezionate all'interno dei ruoli dell'utente.
     * limit_extra_function: Se impostato, il modulo viene visualizzato solo se determinate funzionalità extra (ad esempio l'ecommerce) sono state abilitate
     * limit_extra_module: Se impostato, il modulo viene visualizzato solo se determinati plugin extra (moduli extra, ad esempio il sistema dei ticket) sono stati abilitati
     * childrens: Array di moduli figli (possono avere la stessa struttura chiave/valore qui documentata)
     * granular_settings: Un array di sotto-permessi per definire regole specifiche per le singole funzionalità del modulo. Vedere documentazione di seguito per la struttura dell'array.
     * "granular_settings" => array(
     *      "test_setting_1" => array("name" => "Settaggio di test 1", "type" => "input:checkbox", "default" => "0"),
     *      "test_setting_2" => array("name" => "Settaggio di test 2", "type" => "input:text", "default" => ""),
     *      "test_setting_3" => array("name" => "Settaggio di test 3", "type" => "input:number", "default" => "4"),
     *      "test_setting_4" => array("name" => "Settaggio di test 4", "type" => "select", "options" => array("opt1" => "option 1", "opt2" => "option 2"), "default" => "opt2"),
     * )
     *
     * @var array
     */
    private $modules = array(
        array(
            "name" => "Riepilogo",
            "id" => "dashboard",
            "path" => "",
            "fa_icon" => "dashboard",
            "include_packs" => array('datepicker', 'daterangepicker'),
            "additional_js" => array(
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/fullcalendar/moment.min.js",
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/sparkline/jquery.sparkline.min.js"
            ),
            "additional_css" => array()
        ),
        array(
            "name" => "Framework",
            "id" => "producer_config",
            "path" => "framework/producer_config/",
            "superadmin_limit" => true,
            "fa_icon" => "puzzle-piece",
            "include_packs" => array('orakuploader', 'colorpicker', 'rangeslider', 'nestable'),
            "additional_js" => array(
                ABSOLUTE_SW_PATH_HTML . "assets/js/extraFunctionsUtils.js",
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/ace.js",
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/mode-css.js",
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/mode-javascript.js",
                ABSOLUTE_SW_PATH_HTML . "assets/js/menuSetterUtils.js",
            ),
            "additional_css" => array(
                ABSOLUTE_SW_PATH_HTML . "assets/css/menuOrderNestable.css",
            )
        ),
        array(
            "name" => "Sito e Utenti",
            "id" => "site_and_user",
            "fa_icon" => "briefcase",
            "childrens" => array(
                array(
                    "name" => "Moduli Extra",
                    "id" => "extramodules",
                    "path" => "manager/modules/",
                    "include_packs" => array('orakuploader', 'query-builder'),
                ),
                array(
                    "name" => "Traduzioni",
                    "id" => "i18n",
                    "path" => "manager/i18n/",
                    "include_packs" => array('dataTables'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/bootstrap3-editable/js/bootstrap-editable.min.js",
                    ),
                    "additional_css" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/bootstrap3-editable/css/bootstrap-editable.css",
                    )
                ),
                array(
                    "name" => "Utenti & Permessi",
                    "id" => "users",
                    "fa_icon" => "male",
                    "childrens" => array(
                        array(
                            "name" => "Utenti",
                            "id" => "usermanager",
                            "path" => "manager/utenti/list/",
                            "include_packs" => array('dataTables', 'orakuploader', 'datepicker', 'worldDataUtils', 'clockpicker')
                        ),
                        array(
                            "name" => "Ruoli & Permessi",
                            "id" => "rolesmanager",
                            "path" => "manager/utenti/roles/",
                            "include_packs" => array('dataTables', 'nestable'),
                            "additional_js" => array(
                                ABSOLUTE_SW_PATH_HTML . "assets/js/menuSetterUtils.js",
                            ),
                            "additional_css" => array(
                                ABSOLUTE_SW_PATH_HTML . "assets/css/menuOrderNestable.css",
                            )
                        )
                    )
                ),
                array(
                    "name" => "Redirect",
                    "id" => "redirect",
                    "fa_icon" => "sitemap",
                    "path" => "manager/redirect/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "File Editor",
                    "id" => "file_editor",
                    "fa_icon" => "file",
                    "path" => "manager/file-editor/",
                    "include_packs" => array('codemirror'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/jsTree/jstree.min.js"
                    ),
                    "additional_css" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/jsTree/style.min.css"
                    )
                ),
            )
        ),
        array(
            "name" => "Clienti",
            "id" => "clienti",
            "fa_icon" => "male",
            "childrens" => array(
                array(
                    "name" => "Gestione Clienti",
                    "id" => "manage_customers",
                    "path" => "customers/list/",
                    "include_packs" => array('dataTables', 'datepicker', 'orakuploader'),
                ),
                array(
                    "name" => "Cronologia Clienti",
                    "id" => "customers_histories",
                    "path" => "customers/histories/",
                    "include_packs" => array('dataTables', 'datetimepicker', 'daterangepicker'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/fullcalendar/moment.min.js",
                    ),
                    "additional_css" => array()
                ),
                array(
                    "name" => "Conto Clienti",
                    "id" => "customers_balance",
                    "path" => "customers/balance/",
                    "include_packs" => array('dataTables', 'datepicker'),
                ),
                array(
                    "name" => "Punti Clienti",
                    "id" => "customers_points",
                    "path" => "customers/points/",
                    "limit_extra_function" => array("points"),
                    "include_packs" => array('dataTables', 'datepicker'),
                ),
                array(
                    "name" => "Regali Clienti",
                    "id" => "customers_gift",
                    "path" => "customers/gift/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Import/Export CSV",
                    "id" => "importazione_csv_clienti",
                    "hidden" => true,
                    "path" => "customers/import_csv/",
                    "include_packs" => array('orakuploader', 'dataTables'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/papacsv/papaparse.min.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dropzone/dist/dropzone.js",
                    ),
                    "additional_css" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dropzone/dist/dropzone.css",
                    )
                )
            )
        ),
        array(
            "name" => "Appuntamenti",
            "id" => "appointments",
            "fa_icon" => "calendar",
            "limit_extra_function" => array("appointments"),
            "childrens" => array(
                array(
                    "name" => "Gestione Appuntamenti",
                    "id" => "manage_appontments",
                    "path" => "appointments/list/",
                    "include_packs" => array('dataTables', 'datepicker'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/dhtmlxscheduler.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/ext/dhtmlxscheduler_multiselect.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/ext/dhtmlxscheduler_limit.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/ext/dhtmlxscheduler_editors.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/ext/dhtmlxscheduler_tooltip.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/ext/dhtmlxscheduler_minical.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/ext/pro/dhtmlxscheduler_timeline.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/ext/pro/dhtmlxscheduler_units.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/locale/locale_it.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxCombo/codebase/dhtmlxcombo.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxMenu/codebase/dhtmlxmenu.js",
                    ),
                    "additional_css" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxScheduler/dhtmlxscheduler_material.css",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxCombo/codebase/dhtmlxcombo.css",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dhtmlxMenu/codebase/dhtmlxmenu.css",
                    )
                )
            )
        ),
        array(
            "name" => "Abbonamenti",
            "id" => "subscriptions",
            "fa_icon" => "diamond",
            "limit_extra_function" => array("subscriptions"),
            "childrens" => array(
                array(
                    "name" => "Piani d'Abbonamento",
                    "id" => "subscriptions_plans",
                    "path" => "subscriptions/plans/",
                    "include_packs" => array('orakuploader', 'dataTables', 'tinymce'),
                ),
                array(
                    "name" => "Clienti Abbonati",
                    "id" => "subscriptions_subscribers",
                    "path" => "subscriptions/subscribers/",
                    "include_packs" => array('dataTables', 'select2', 'datetimepicker'),
                ),
                array(
                    "name" => "Ordini",
                    "id" => "subscriptions_orders",
                    "fa_icon" => "shopping-cart",
                    "path" => "subscriptions/orders/",
                    "include_packs" => array('dataTables', 'toastr'),
                ),
            )
        ),
        array(
            "name" => "Affiliazioni",
            "id" => "affiliations",
            "fa_icon" => "users",
            "limit_extra_function" => array("affiliations"),
            "childrens" => array(
                array(
                    "name" => "Clienti Affiliati",
                    "id" => "affiliations_affiliates",
                    "path" => "affiliations/affiliates/",
                    "include_packs" => array('dataTables', 'select2', 'datetimepicker'),
                ),
                array(
                    "name" => "Richieste di Prelievo",
                    "id" => "affiliations_withdrawals",
                    "path" => "affiliations/withdrawals/",
                    "include_packs" => array('dataTables', 'datepicker'),
                )
            )
        ),
        array(
            "name" => "Contenuti",
            "id" => "contenuti",
            "fa_icon" => "file",
            "childrens" => array(
                array(
                    "name" => "Pagine",
                    "id" => "contenuti_pagine",
                    "path" => "pages/list/",
                    "include_packs" => array('dataTables', 'serpPreviewUtils', 'orakuploader', 'easyRowDuplication', 'tinymce', 'colorpicker', 'visualBuilder', 'codemirror'),
                ),
                array(
                    "name" => "Blocchi globali",
                    "id" => "contenuti_blocchi",
                    "fa_icon" => 'cubes',
                    "path" => "pages/blocks/",
                    "include_packs" => array('dataTables', 'orakuploader', 'visualBuilder', 'codemirror'),
                ),
                array(
                    "name" => "Menu",
                    "id" => "contenuti_menu",
                    "path" => "pages/menu/",
                    "include_packs" => array('dataTables', 'nestable')
                ),
                array(
                    "name" => "Widget",
                    "id" => "contenuti_widget",
                    "path" => "pages/widget/",
                    "include_packs" => array('dataTables', 'orakuploader', 'easyRowDuplication', 'tinymce'),
                ),
                array(
                    "name" => "FAQ",
                    "id" => "contenuti_faq",
                    "path" => "pages/faq/",
                    "include_packs" => array('orakuploader', 'dataTables', 'easyRowDuplication'),
                ),
                array(
                    "name" => "Form",
                    "id" => "contenuti_form",
                    "path" => "pages/form/",
                    "include_packs" => array('orakuploader','dataTables', 'easyRowDuplication', 'email-editor', 'tinymce', 'tagsinput'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/ace.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/mode-javascript.js"
                    )
                ),
                array(
                    "name" => "Popup",
                    "id" => "contenuti_popup",
                    "fa_icon" => 'window-maximize',
                    "path" => "pages/popup/",
                    "include_packs" => array('dataTables', 'orakuploader', 'visualBuilder', 'codemirror', 'query-builder', 'tagsinput'),
                )
            )
        ),
        array(
            "name" => "Blog",
            "id" => "blog",
            "fa_icon" => "file-text-o",
            "limit_extra_function" => array("blog"),
            "childrens" => array(
                array(
                    "name" => "Articoli",
                    "id" => "blog_articoli",
                    "path" => "blog/articoli/",
                    "include_packs" => array('dataTables', 'orakuploader', 'tinymce', 'select2', 'social-share-sdk', 'tagsinput')
                ),
                array(
                    "name" => "Categorie",
                    "id" => "blog_categorie",
                    "path" => "blog/categorie/",
                    "include_packs" => array('orakuploader', 'colorpicker', 'nestable')
                )
            )
        ),
        array(
            "name" => "Esercizio/Negozio",
            "id" => "esercizio",
            "fa_icon" => "building",
            "childrens" => array(
                array(
                    "name" => "Team",
                    "id" => "attivita_team",
                    "fa_icon" => "users",
                    "path" => "attivita/team/",
                    "include_packs" => array('orakuploader', 'dataTables', 'tinymce'),
                ),
                array(
                    "name" => "Sedi",
                    "id" => "attivita_sedi",
                    "fa_icon" => "building",
                    "path" => "attivita/sedi/",
                    "include_packs" => array('orakuploader', 'dataTables', 'worldDataUtils', 'tinymce'),
                ),
                array(
                    "name" => "Reparti",
                    "id" => "attivita_reparti",
                    "fa_icon" => "black-tie",
                    "path" => "attivita/reparti/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Orari di Lavoro",
                    "id" => "attivita_orari",
                    "fa_icon" => "clock-o",
                    "path" => "attivita/orari_lavoro/",
                    "include_packs" => array('dataTables', 'clockpicker')
                ),
            )
        ),
        array(
            "name" => "Vendite",
            "id" => "fatturazione_vendite",
            "path" => "fatturazione/vendite/",
            "fa_icon" => "calculator",
            "include_packs" => array('dataTables', 'datepicker', 'select2')
        ),
        array(
            "name" => "Report",
            "id" => "report",
            "path" => "report/",
            "fa_icon" => "pie-chart",
            "include_packs" => array('datepicker'),
            "additional_js" => array(
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/sparkline/jquery.sparkline.min.js",
                ABSOLUTE_SW_PATH_HTML . "assets/js/jquery.fileDownload.js",
            ),
        ),
        array(
            "name" => "Tracking",
            "id" => "tracking",
            "path" => "tracking/",
            "fa_icon" => "line-chart",
            "include_packs" => array('dataTables', 'easyRowDuplication', 'tagsinput', 'codemirror', 'select2'),
            "additional_js" => array(
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/ace.js",
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/mode-css.js",
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/mode-javascript.js",

                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/morris/raphael-2.1.0.min.js",
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/morris/morris.js"

            ),
            "additional_css" => array()
        ),
        array(
            "name" => "Agenzia Immobiliare",
            "id" => "immobili_main",
            "fa_icon" => "building-o",
            "limit_extra_function" => array("realestate"),
            "childrens" => array(
                array(
                    "name" => "Elenco Immobili",
                    "id" => "realestate_immobili",
                    "path" => "immobili/list/",
                    "include_packs" => array('dataTables', 'orakuploader', 'worldDataUtils', 'social-share-sdk'),
                    "mandatory_geonames" => true,
                ),
                array(
                    "name" => "Categorie",
                    "id" => "realestate_categorie",
                    "fa_icon" => "th",
                    "path" => "immobili/categorie/",
                    "include_packs" => array('orakuploader', 'colorpicker', 'nestable'),
                ),
                array(
                    "name" => "Rapporti",
                    "id" => "realestate_rapporti",
                    "fa_icon" => "bar-chart-o",
                    "limit_extra_function" => array("realestate"),
                    "childrens" => array(
                        array(
                            "name" => "Ricerche",
                            "id" => "realestate_ricerche",
                            "path" => "immobili/rapporti/ricerche/",
                            "include_packs" => array('dataTables'),
                        ),
                        array(
                            "name" => "Immobili più visti",
                            "id" => "realestate_topviewed",
                            "path" => "immobili/rapporti/topviewed/",
                            "include_packs" => array('dataTables'),
                        ),
                        array(
                            "name" => "Zone di interesse",
                            "id" => "realestate_zone",
                            "path" => "immobili/rapporti/zone/",
                            "include_packs" => array('dataTables', 'googleMaps'),
                        ),
                        array(
                            "name" => "Prezzi",
                            "id" => "realestate_prezzi",
                            "path" => "immobili/rapporti/prezzi/",
                            "include_packs" => array('dataTables'),
                        ),
                    )
                ),
            )
        ),
        array(
            "name" => "Recensioni",
            "id" => "recensioni",
            "path" => "recensioni/",
            "fa_icon" => "comments",
            "multilang_content" => true,
            "include_packs" => array('dataTables', 'orakuploader'),
        ),
        array(
            "name" => "Servizi e Settori",
            "id" => "servizi_main",
            "fa_icon" => "suitcase",
            "childrens" => array(
                array(
                    "name" => "Categorie Servizi",
                    "id" => "servizi_categorie",
                    "path" => "servizi/categorie/",
                    "include_packs" => array('dataTables', 'orakuploader', 'tinymce'),
                    "page_relation" => 'service_category'
                ),
                array(
                    "name" => "Elenco Servizi",
                    "id" => "servizi",
                    "path" => "servizi/list/",
                    "include_packs" => array('dataTables', 'orakuploader', 'tinymce'),
                    "page_relation" => 'services'
                ),
                array(
                    "name" => "Elenco Settori",
                    "id" => "servizi_settori",
                    "path" => "servizi/settori/",
                    "include_packs" => array('dataTables', 'orakuploader', 'tinymce'),
                    "page_relation" => 'service_sector'
                ),
                array(
                    "name" => "Portfolio",
                    "id" => "servizi_portfolio",
                    "path" => "servizi/portfolio/",
                    "include_packs" => array('dataTables', 'orakuploader', 'tinymce'),
                )
            )
        ),
        array(
            "name" => "Notifiche",
            "id" => "notifications_list",
            "path" => "notifications/list/",
            "include_packs" => array('orakuploader', 'email-editor', 'tinymce', 'dataTables'),
            "fa_icon" => "bell"
        ),
        array(
            "name" => "Gallerie e Sliders",
            "id" => "gallery_slider",
            "fa_icon" => "photo",
            "childrens" => array(
                array(
                    "name" => "Gallery",
                    "id" => "gallery_gallery",
                    "fa_icon" => "photo",
                    "path" => "media/gallery/",
                    "include_packs" => array('orakuploader', 'dataTables'),
                ),
                array(
                    "name" => "Sliders",
                    "id" => "gallery_sliders",
                    "path" => "media/sliders/",
                    "include_packs" => array('orakuploader', 'dataTables', 'colorpicker'),
                )
            )
        ),
        array(
            "name" => "Marketing",
            "id" => "marketing",
            "fa_icon" => "bullhorn",
            "childrens" => array(
                array(
                    "name" => "Campagne",
                    "id" => "marketing_campagne",
                    "fa_icon" => "table",
                    "include_packs" => array('orakuploader', 'dataTables', 'easyRowDuplication', 'email-editor', 'tinymce', 'googleMaps', 'query-builder', 'bootstrap-select'),
                    "path" => "marketing/campagne/",
                    "additional_js" => array(
                        "https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js",
                        ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'marketing/campagne/scripts/connectionLine.js'
                    ),
                    "additional_css" => array(),
                    "subscription_settings" => array(
                        'automations_enabled' => array(
                            'title' => 'Abilita automazioni?',
                            'def_value' => false
                        )
                    )
                ),
                array(
                    "name" => "Destinatari",
                    "id" => "marketing_indirizzi",
                    "fa_icon" => "edit",
                    "childrens" => array(
                        array(
                            "name" => "Destinatari",
                            "id" => "marketing_destinatari",
                            "path" => "marketing/destinatari/lista/",
                            "include_packs" => array('dataTables'),
                        ),
                        array(
                            "name" => "Liste",
                            "id" => "marketing_liste",
                            "path" => "marketing/destinatari/tag/",
                            "include_packs" => array('dataTables', 'query-builder', 'bootstrap-select')
                        )
                    )
                ),
                array(
                    "name" => "Mittenti",
                    "id" => "newsletter__emails_mittenti",
                    "path" => "marketing/settings/mittenti/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Templates",
                    "id" => "newsletter_templates",
                    "path" => "marketing/templates/",
                    "include_packs" => array('orakuploader', 'dataTables', 'email-editor', 'tinymce'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/html2canvas/html2canvas.min.js",
                    )
                )
            )
        ),
        array(
            "name" => "Hotel",
            "id" => "hotel",
            "fa_icon" => "hotel",
            "limit_extra_function" => array("hotel"),
            "childrens" => array(
                array(
                    "name" => "Categorie Camere",
                    "id" => "hotel_camere_cats",
                    "path" => "hotel/camere_cats/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Camere",
                    "id" => "hotel_camere",
                    "path" => "hotel/camere/",
                    "include_packs" => array('orakuploader', 'dataTables', 'tinymce'),
                ),
                array(
                    "name" => "Offerte",
                    "id" => "hotel_offerte",
                    "path" => "hotel/offerte/",
                    "include_packs" => array('orakuploader', 'dataTables', 'tinymce', 'datepicker'),
                )
            )
        ),
        array(
            "name" => "Campeggio",
            "id" => "campeggio",
            "fa_icon" => "hotel",
            "limit_extra_function" => array("camping"),
            "childrens" => array(
                array(
                    "name" => "Piazzole",
                    "id" => "camping_piazzole",
                    "fa_icon" => "hotel",
                    "path" => "camping/piazzole/",
                    "include_packs" => array('orakuploader', 'dataTables', 'tinymce', 'easyRowDuplication', 'datepicker'),
                ),
                array(
                    "name" => "Offerte",
                    "id" => "camping_offerte",
                    "fa_icon" => "clock-o",
                    "path" => "camping/offerte/",
                    "include_packs" => array('orakuploader', 'dataTables', 'tinymce', 'datepicker'),
                ),
                array(
                    "name" => "Stagioni",
                    "id" => "camping_stagioni",
                    "fa_icon" => "calendar-o",
                    "path" => "camping/stagioni/",
                    "include_packs" => array('easyRowDuplication', 'datepicker')
                )
            )
        ),
        array(
            "name" => "Ristorante",
            "id" => "ristorante",
            "fa_icon" => "cutlery",
            "limit_extra_function" => array("restaurant"),
            "childrens" => array(
                array(
                    "name" => "Menù",
                    "id" => "ristorante_menu",
                    "path" => "restaurant/menu/",
                    "include_packs" => array('orakuploader', 'dataTables', 'easyRowDuplication'),
                )
            )
        ),
        array(
            "name" => "Centro Estetico",
            "id" => "centro_estetico",
            "fa_icon" => "heart",
            "limit_extra_function" => array("beautycenter"),
            "childrens" => array(
                array(
                    "name" => "Offerte & Pacchetti",
                    "id" => "beautycenter_offerte",
                    "path" => "beautycenter/offerte/",
                    "include_packs" => array('orakuploader', 'dataTables', 'tinymce', 'datepicker'),
                ),
                array(
                    "name" => "Cabine",
                    "id" => "beautycenter_cabine",
                    "path" => "beautycenter/cabine/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Sedute Clienti",
                    "id" => "beautycenter_sedute",
                    "path" => "beautycenter/sedute/",
                    "include_packs" => array('dataTables'),
                )
            )
        ),
        array(
            "name" => "Agenzia Viaggi",
            "id" => "travelagency",
            "fa_icon" => "plane",
            "limit_extra_function" => array("travelagency"),
            "childrens" => array(
                array(
                    "name" => "Pacchetti",
                    "id" => "travelagency_packages",
                    "path" => "travelagency/packages/",
                    "include_packs" => array('orakuploader', 'dataTables', 'tinymce', 'datetimepicker'),
                ),
                array(
                    "name" => "Categorie Pacchetti",
                    "id" => "travelagency_packages_categories",
                    "path" => "travelagency/categories/",
                    "include_packs" => array('orakuploader', 'dataTables', 'nestable')
                ),
                array(
                    "name" => "Ordini",
                    "id" => "travelagency_orders",
                    "fa_icon" => "shopping-cart",
                    "path" => "travelagency/ordini/",
                    "include_packs" => array('dataTables', 'toastr'),
                ),
            )
        ),
        array(
            "name" => "Ecommerce",
            "id" => "ecommerce",
            "fa_icon" => "money",
            "limit_extra_function" => array("ecommerce"),
            "childrens" => array(
                array(
                    "name" => "Prodotti",
                    "id" => "prodotti_ecommerce",
                    "fa_icon" => "cubes",
                    "limit_extra_function" => array("ecommerce"),
                    "childrens" => array(
                        array(
                            "name" => "Prodotti",
                            "id" => "ecommerce_prodottiprodotti",
                            "fa_icon" => "cubes",
                            "path" => "ecommerce/prodotti/list/",
                            "include_packs" => array('dataTables', 'serpPreviewUtils', 'easyRowDuplication', 'orakuploader', 'select2', 'datepicker', 'tinymce', 'social-share-sdk', 'ecommerceUtils'),
                        ),
                        array(
                            "name" => "Recensioni",
                            "id" => "ecommerce_prodottirecensioni",
                            "fa_icon" => "comments",
                            "path" => "ecommerce/prodotti/recensioni/",
                            "include_packs" => array('dataTables', 'select2', 'datetimepicker', 'ecommerceUtils'),
                        ),
                        array(
                            "name" => "Domande Clienti",
                            "id" => "ecommerce_prodottidomande",
                            "fa_icon" => "question",
                            "path" => "ecommerce/prodotti/domande/",
                            "include_packs" => array('dataTables', 'select2', 'datetimepicker', 'ecommerceUtils'),
                        ),
                        array(
                            "name" => "Import/Export CSV",
                            "id" => "importazione_csv_prodotti",
                            "hidden" => true,
                            "path" => "ecommerce/prodotti/import_csv/",
                            "include_packs" => array('orakuploader', 'dataTables'),
                            "additional_js" => array(
                                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/papacsv/papaparse.min.js",
                                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dropzone/dist/dropzone.js",
                            ),
                            "additional_css" => array(
                                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dropzone/dist/dropzone.css",
                            )
                        )
                    )
                ),
                array(
                    "name" => "Categorie",
                    "id" => "ecommerce_categorie",
                    "fa_icon" => "th",
                    "path" => "ecommerce/categorie/",
                    "include_packs" => array('orakuploader', 'colorpicker', 'nestable', 'tinymce')
                ),
                array(
                    "name" => "Spedizione",
                    "id" => "ecommerce_spedizione",
                    "fa_icon" => "truck",
                    "path" => "ecommerce/spedizione/",
                    "include_packs" => array('dataTables', 'select2'),
                ),
                array(
                    "name" => "Coupon",
                    "id" => "ecommerce_coupon",
                    "fa_icon" => "ticket",
                    "path" => "ecommerce/coupon/",
                    "include_packs" => array('dataTables', 'select2', 'easyRowDuplication', 'datepicker', 'ecommerceUtils'),
                ),
                array(
                    "name" => "Attributi",
                    "id" => "ecommerce_attributi",
                    "fa_icon" => "cubes",
                    "path" => "ecommerce/attributi/",
                    "include_packs" => array('dataTables', 'easyRowDuplication', 'orakuploader', 'colorpicker'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/fuzzyset/fuzzyset.js"
                    ),
                ),
                array(
                    "name" => "Ordini",
                    "id" => "ecommerce_ordini",
                    "fa_icon" => "shopping-cart",
                    "path" => "ecommerce/ordini/",
                    "include_packs" => array('dataTables', 'toastr', 'select2'),
                )
            )
        ),
        array(
            "name" => "Ticket",
            "id" => "ticket",
            "fa_icon" => "ticket",
            "limit_extra_function" => array("ticket"),
            "childrens" => array(
                array(
                    "name" => "Prenotazioni",
                    "id" => "richieste_ticket",
                    "path" => "ticket/prenotazioni/",
                    "include_packs" => array('dataTables')
                ),
                array(
                    "name" => "Date",
                    "id" => "date_ticket",
                    "path" => "ticket/date/",
                    "include_packs" => array('dataTables', 'datetimepicker', 'tinymce', 'easyRowDuplication'),
                )
            )
        ),
        array(
            "name" => "Richieste",
            "id" => "richieste",
            "fa_icon" => "envelope",
            "childrens" => array(
                array(
                    "name" => "Prenotazioni",
                    "id" => "richieste_prenotazioni",
                    "path" => "richieste/prenotazioni/",
                    "include_packs" => array('dataTables')
                ),
                array(
                    "name" => "Messaggi",
                    "id" => "richieste_generiche",
                    "path" => "richieste/generiche/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Immobili",
                    "id" => "richieste_immobili",
                    "path" => "richieste/immobili/",
                    "include_packs" => array('dataTables'),
                    "limit_extra_function" => array("realestate"),
                )
            )
        ),
        array(
            "name" => "Integrazione",
            "id" => "integrazione",
            "fa_icon" => "cube",
            "path" => "integrazione/",
            "include_packs" => array('tagsinput', 'codemirror')
        ),
        array(
            "name" => "MailBox",
            "id" => "mailbox",
            "fa_icon" => "envelope",
            "childrens" => array(
                array(
                    "name" => "Posta",
                    "id" => "mailbox_list",
                    "path" => "mailbox/list/",
                    "include_packs" => array('dataTables', 'tinymce')
                ),
                array(
                    "name" => "Template",
                    "id" => "mailbox_templates",
                    "path" => "mailbox/templates/",
                    "include_packs" => array('orakuploader', 'dataTables', 'email-editor', 'tinymce'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/html2canvas/html2canvas.min.js",
                    )
                )
            )
        ),
        array(
            "name" => "Personalizzazione",
            "id" => "appearance",
            "fa_icon" => "paint-brush",
            "childrens" => array(
                array(
                    "name" => "Preferenze",
                    "id" => "appearance_preferences",
                    "path" => "appearance/preferences/",
                    "include_packs" => array("tinymce", "colorpicker", "rangeslider", "worldDataUtils", "orakuploader")
                ),
                array(
                    "name" => "Temi",
                    "id" => "appearance_themes",
                    "path" => "appearance/themes/",
                    "include_packs" => array('dataTables'),
                    "additional_css" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/slick/slick.css",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/slick/slick-theme.css"
                    ),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/slick/slick.min.js"
                    ),
                ),
                array(
                    "name" => "Funzionalità",
                    "id" => "appearance_plugins",
                    "path" => "appearance/plugins/",
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "assets/js/extraFunctionsUtils.js",
                    )
                )
            )
        ),
        array(
            "name" => "Agenzia",
            "id" => "ms_agency",
            "fa_icon" => "building",
            "childrens" => array(
                array(
                    "name" => "Acquisto Servizi",
                    "id" => "ms_agency_servizi",
                    "path" => "ms_agency/servizi/",
                    "include_packs" => array('tinymce'),
                ),
                array(
                    "name" => "Ordini",
                    "id" => "ms_agency_ordini",
                    "path" => "ms_agency/ordini/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Guadagni",
                    "id" => "ms_agency_guadagni",
                    "path" => "ms_agency/guadagni/",
                    "include_packs" => array('dataTables', 'daterangepicker'),
                ),
                array(
                    "name" => "Dati di pagamento",
                    "id" => "ms_agency_pagamenti",
                    "path" => "ms_agency/pagamenti/",
                    "include_packs" => array(),
                )
            )
        ),
        array(
            "name" => "Impostazioni",
            "id" => "impostazioni",
            "fa_icon" => "gear",
            "childrens" => array(
                array(
                    "name" => "Configurazione",
                    "id" => "settings",
                    "path" => "settings/",
                    "fa_icon" => "gear",
                    "include_packs" => array('worldDataUtils', 'tagsinput'),
                    "mandatory_geonames" => true
                ),
                array(
                    "name" => "Sito Web",
                    "id" => "sitemanager",
                    "path" => "manager/site/",
                    "include_packs" => array('orakuploader', 'easyRowDuplication', 'tinymce', 'worldDataUtils'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/ace.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/mode-css.js",
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ace/mode-javascript.js",
                    )
                ),
                array(
                    "name" => "Ecommerce",
                    "id" => "immpostazioni_ecommerce",
                    "fa_icon" => "cogs",
                    "limit_extra_function" => array("ecommerce"),
                    "childrens" => array(
                        array(
                            "name" => "Prodotti",
                            "id" => "ecommerce_settingsprodotti",
                            "path" => "ecommerce/settings/prodotti/",
                        ),
                        array(
                            "name" => "Cassa",
                            "id" => "ecommerce_settingscassa",
                            "path" => "ecommerce/settings/cassa/",
                            "include_packs" => array('orakuploader', 'select2'),
                        ),
                        array(
                            "name" => "Punti",
                            "id" => "ecommerce_settingspoints",
                            "path" => "ecommerce/settings/points/",
                            "limit_extra_function" => array("points"),
                            "include_packs" => array('orakuploader', 'select2', 'tinymce'),
                        )
                    )
                ),
                array(
                    "name" => "Fatturazione",
                    "id" => "impostazioni_fatturazione",
                    "fa_icon" => "cogs",
                    "childrens" => array(
                        array(
                            "name" => "Casse",
                            "id" => "fatturazione_settings_cassa",
                            "path" => "fatturazione/settings/casse/",
                            "include_packs" => array('orakuploader', 'dataTables')
                        ),
                        array(
                            "name" => "Fatture e Pagamenti",
                            "id" => "fatturazione_settings_fatture",
                            "path" => "fatturazione/settings/fatture/",
                            "include_packs" => array('orakuploader', 'touchspin', 'easyRowDuplication')
                        ),
                        array(
                            "name" => "Imposte",
                            "id" => "fatturazione_settings_imposte",
                            "path" => "fatturazione/settings/imposte/",
                            "include_packs" => array('easyRowDuplication'),
                        ),
                        array(
                            "name" => "Valute",
                            "id" => "fatturazione_settings_currencies",
                            "path" => "fatturazione/settings/currencies/",
                            "include_packs" => array('easyRowDuplication'),
                        )
                    )
                ),
                array(
                    "name" => "Newsletter",
                    "id" => "newsletter__config_settings",
                    "path" => "marketing/settings/general/",
                    "fa_icon" => "cogs"
                ),
                array(
                    "name" => "Appuntamenti",
                    "id" => "customers_settings_appointments",
                    "path" => "appointments/settings/",
                    "limit_extra_function" => array("appointments")
                ),
                array(
                    "name" => "Blog",
                    "id" => "blog_preferenze",
                    "path" => "blog/preferenze/",
                    "limit_extra_function" => array("blog")
                ),
                array(
                    "name" => "Notifiche",
                    "id" => "notifications_settings",
                    "path" => "notifications/settings/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Server Email",
                    "id" => "manage_email_smtp",
                    "fa_icon" => "envelope",
                    "path" => "manager/email/smtp/",
                    "include_packs" => array('dataTables')
                ),
                array(
                    "name" => "Template Email",
                    "id" => "manage_email_templates",
                    "fa_icon" => "envelope",
                    "path" => "manager/email/templates/",
                    "include_packs" => array('orakuploader', 'dataTables', 'email-editor', 'tinymce'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/html2canvas/html2canvas.min.js",
                    )
                )
            )
        )
    );

    private $fw360_modules = array(
        array(
            "name" => "Agenzia",
            "id" => "fw360_ms",
            "fa_icon" => "building",
            "superadmin_limit" => true,
            "childrens" => array(
                array(
                    "name" => "Ordini",
                    "id" => "fw360_ms_orders",
                    "superadmin_limit" => true,
                    "path" => "framework360/ms/orders/",
                    "include_packs" => array('dataTables', 'tinymce', 'orakuploader'),
                ),
                array(
                    "name" => "Staff",
                    "id" => "fw360_utenti",
                    "superadmin_limit" => true,
                    "path" => "framework360/ms/utenti/",
                    "include_packs" => array('dataTables', 'orakuploader', 'datepicker', 'worldDataUtils', 'clockpicker')
                ),
                array(
                    "name" => "Richieste di prelievo",
                    "id" => "fw360_ms_withdrawals",
                    "superadmin_limit" => true,
                    "path" => "framework360/ms/withdrawals/",
                    "include_packs" => array('dataTables', 'tinymce', 'orakuploader'),
                ),
                array(
                    "name" => "Servizi",
                    "id" => "fw360_ms_services",
                    "fa_icon" => "suitcase",
                    "superadmin_limit" => true,
                    "childrens" => array(
                        array(
                            "name" => "Lista",
                            "id" => "fw360_ms_services_list",
                            "superadmin_limit" => true,
                            "path" => "framework360/ms/servizi/lista/",
                            "include_packs" => array('dataTables', 'tinymce', 'orakuploader'),
                        ),
                        array(
                            "name" => "Pacchetti",
                            "id" => "fw360_ms_services_packages",
                            "superadmin_limit" => true,
                            "path" => "framework360/ms/servizi/pacchetti/",
                            "include_packs" => array('dataTables', 'tinymce', 'orakuploader'),
                        ),
                        array(
                            "name" => "Categorie",
                            "id" => "fw360_ms_service_categories",
                            "superadmin_limit" => true,
                            "path" => "framework360/ms/servizi/categorie/",
                            "include_packs" => array('dataTables', 'tinymce', 'orakuploader'),
                        )
                    )
                )
            )
        ),
        array(
            "name" => "Siti Web",
            "id" => "fw360_sites",
            "fa_icon" => "desktop",
           // "superadmin_limit" => true,
            "childrens" => array(
                array(
                    "name" => "Configurazione",
                    "id" => "site_configuration",
                    //"superadmin_limit" => true,
                    "path" => "framework360/sites/configuration/",
                    "include_packs" => array('dataTables'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "assets/js/extraFunctionsUtils.js",
                    )
                )
            ),
        ),
        array(
            "name" => "SaaS",
            "id" => "fw360_saas",
            "fa_icon" => "cloud",
            "sidebar_id" => array("fw360_saas"),
            "childrens" => array(
                array(
                    "name" => "Configurazione",
                    "id" => "saas_configuration",
                    "superadmin_limit" => true,
                    "path" => "framework360/saas/configuration/",
                    "include_packs" => array('dataTables', 'easyRowDuplication', 'tinymce', 'nestable'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "assets/js/extraFunctionsUtils.js",
                        ABSOLUTE_SW_PATH_HTML . "assets/js/menuSetterUtils.js",
                    )
                ),
                array(
                    "name" => "Ambienti",
                    "id" => "saas_environments",
                    "path" => "framework360/saas/environments/",
                    "include_packs" => array('dataTables', 'datepicker'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "assets/js/extraFunctionsUtils.js",
                    )
                ),
                array(
                    "name" => "Ordini",
                    "id" => "saas_orders",
                    "superadmin_limit" => true,
                    "path" => "framework360/saas/orders/",
                    "include_packs" => array('dataTables')
                )
            ),
        ),
        array(
            "name" => "Aggiornamento",
            "superadmin_limit" => true,
            "id" => "fw360_updates",
            "fa_icon" => "refresh",
            "childrens" => array(
                array(
                    "name" => "Database",
                    "id" => "db_updater",
                    "path" => "framework360/updater/database/",
                ),
                array(
                    "name" => "Framework360 / Siti",
                    "id" => "sw_updater",
                    "path" => "framework360/updater/software/",
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dualListbox/jquery.bootstrap-duallistbox.js",
                    ),
                    "additional_css" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dualListbox/bootstrap-duallistbox.min.css",
                    )
                ),
            ),
        ),
        array(
            "name" => "Backup & Ripristino",
            "superadmin_limit" => true,
            "id" => "db_backup",
            "path" => "framework360/db_backup/",
            "fa_icon" => "download",
            "additional_js" => array(
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dropzone/dist/dropzone.js",
            ),
            "additional_css" => array(
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dropzone/dist/dropzone.css",
            )
        ),
        array(
            "name" => "Log Errori",
            "id" => "errors_log",
            "fa_icon" => "exclamation",
            "superadmin_limit" => true,
            "childrens" => array(
                array(
                    "name" => "Errori Javascript",
                    "id" => "errors_log_js",
                    "superadmin_limit" => true,
                    "path" => "framework360/errors_log/javascript/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Errori PHP",
                    "id" => "errors_log_php",
                    "superadmin_limit" => true,
                    "path" => "framework360/errors_log/php/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Errori SQL",
                    "id" => "errors_log_sql",
                    "superadmin_limit" => true,
                    "path" => "framework360/errors_log/sql/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Errori Email",
                    "id" => "errors_log_email",
                    "superadmin_limit" => true,
                    "path" => "framework360/errors_log/email/",
                    "include_packs" => array('dataTables'),
                ),
            )
        ),
        array(
            "name" => "Assistenza Clienti",
            "id" => "fw360_assistenza",
            "fa_icon" => "question",
            "superadmin_limit" => true,
            "childrens" => array(
                array(
                    "name" => "Ticket",
                    "id" => "fw360_assistenza_ticket",
                    "superadmin_limit" => true,
                    "fa_icon" => "question",
                    "path" => "framework360/assistenza/ticket/",
                    "include_packs" => array('dataTables', 'tinymce', 'orakuploader', 'datepicker'),
                ),
                array(
                    "name" => "Ordini",
                    "id" => "fw360_assistenza_ticket_ordini",
                    "superadmin_limit" => true,
                    "path" => "framework360/assistenza/orders/",
                    "include_packs" => array('dataTables')
                )
            )
        ),
        array(
            "name" => "Preferenze",
            "id" => "fw360_preferenze",
            "fa_icon" => "cog",
            "superadmin_limit" => true,
            "childrens" => array(
                array(
                    "name" => "Impostazioni",
                    "id" => "fw_settings",
                    "superadmin_limit" => true,
                    "path" => "framework360/settings/",
                    "include_packs" => array('dataTables'),
                ),
                array(
                    "name" => "Traduzioni",
                    "id" => "framework360_i18n",
                    "path" => "framework360/i18n/",
                    "include_packs" => array('dataTables'),
                    "additional_js" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/bootstrap3-editable/js/bootstrap-editable.min.js",
                    ),
                    "additional_css" => array(
                        ABSOLUTE_SW_PATH_HTML . "vendor/plugins/bootstrap3-editable/css/bootstrap-editable.css",
                    )
                ),
                array(
                    "name" => "Template Email",
                    "id" => "email_templates",
                    "superadmin_limit" => true,
                    "path" => "framework360/templates/",
                    "include_packs" => array('orakuploader', 'dataTables', 'email-editor', 'tinymce'),
                ),
                array(
                    "name" => "Risorse GDPR",
                    "id" => "gdpr_scripts",
                    "superadmin_limit" => true,
                    "path" => "framework360/gdpr_scripts/",
                    "include_packs" => array('dataTables', 'easyRowDuplication', 'tagsinput'),
                )
            ),
        )
    );

    private $hidden_modules = array(
        array(
            "name" => "Profilo Utente",
            "id" => "userprofile",
            "path" => "manager/utenti/profile/",
            "include_packs" => array('dataTables', 'orakuploader', 'datepicker', 'worldDataUtils', 'clockpicker')
        ),
        array(
            "name" => "Esporta Tema",
            "id" => "appearance_themes_export",
            "superadmin_limit" => true,
            "path" => "appearance/themes/export/",
            "include_packs" => array('tinymce', 'orakuploader'),
            "additional_js" => array(
                ABSOLUTE_SW_PATH_HTML . "vendor/plugins/html2canvas/html2canvas.min.js",
            )
        ),
        array(
            "name" => "Assistenza Clienti",
            "id" => "assistenza_ticket",
            "fa_icon" => "question",
            "path" => "assistenza/ticket/",
            "hidden" => true,
            "include_packs" => array('dataTables', 'tinymce', 'orakuploader'),
        ),
        array(
            "name" => "Abbonamento",
            "id" => "abbonamento_saas",
            "fa_icon" => "cubes",
            "path" => "saas/abbonamento/",
            "hidden" => true,
            "include_packs" => array(),
        ),
        array(
            "name" => "Pagamento Abbonamenti e Servizi",
            "id" => "payway",
            "fa_icon" => "money",
            "path" => "payway/checkout/",
            "hidden" => true,
            "include_packs" => array(),
        )
        /*
        array(
            "name" => "Acquisto Servizi",
            "id" => "MS_servizi",
            "fa_icon" => "question",
            "path" => "agenzia/servizi/",
            "hidden" => true,
            "include_packs" => array('dataTables', 'tinymce', 'orakuploader'),
        ) */
    );

    /**
     * Ottiene l'array completo dei moduli
     *
     * @param $type string Indica quali voci ottenere (fw360|hidden|standard|all)
     *
     * @return array
     */
    public function getModules($type = 'standard') {
        global $MSFrameworkCMS;

        if($type == 'fw360') {
            $return_modules = $this->fw360_modules;
        } else if($type == 'hidden') {
            $return_modules = $this->hidden_modules;
        } else if($type == 'all') {
            $return_modules = array_merge($this->hidden_modules, $this->fw360_modules, $this->modules);
        } else {
            $return_modules = $this->modules;
        }

        return $return_modules;
    }

    /**
     * Ottiene i presets dei menù
     *
     * @param $include_custom boolean Include anche il menù custom
     * @param $include_user_perms boolean Include anche il menù personalizzati dei ruoli utente
     *
     * @return array
     */
    public function getMenuPresets($include_custom = true, $include_user_perms = false) {
        global $MSFrameworkCMS, $MSFrameworkDatabase;

        $presets = array();
        foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`menu_presets` ORDER BY is_standard DESC, `name` ASC") as $preset) {
            $preset['preset'] = json_decode($preset['preset'], true);
            if($preset['preset']) {
                $presets[$preset['id']] = $preset;
            }
        }

        if($include_user_perms) {
            foreach ($MSFrameworkDatabase->getAssoc("SELECT id, modules_perms FROM `users_roles`") as $preset) {
                $preset['preset'] = json_decode($preset['modules_perms'], true);
                if ($preset['preset']) {
                    $presets['custom-' . $preset['id']] = $preset;
                }
            }
        }

        // Carica il preset personalizzato se presente
        if($include_custom) {
            $custom_preset = $MSFrameworkCMS->getCMSData('menu_preset');
            if ($custom_preset) {
                $presets['custom'] = array(
                    'id' => 'custom',
                    'name' => 'Custom',
                    'preset' => $custom_preset
                );
            }
        }

        return $presets;
    }

    /**
     * Costruisce l'HTML da visualizzare all'interno della sidebar dell'ACP, occupandosi di verificare anche ulteriori aspetti come i permessi
     *
     * @param null $ary L'array in analisi. Se null, preleva l'array da $this->getModules
     * @param null $ereditary_perms I permessi ereditati dal modulo padre (vengono sovrascritti se il modulo in analisi ne ha di personalizzati)
     * @param null $cur_user_level Selezionata il menù da mostrare in base al livello utente
     *
     * @return string
     */
    public function printSidebarStructure($ary = null, $ereditary_perms = null, $cur_user_level = null) {
        global $module_config, $firephp, $MSFrameworkCMS, $MSFrameworkUsers; //è la variabile proveniente dal file moduleConfig del singolo modulo

        $active_module = $module_config['id'];

        $r_producer_config = $MSFrameworkCMS->getCMSData('producer_config');

        if(is_null($cur_user_level)) {
            $cur_user_level = $MSFrameworkUsers->getUserDataFromSession('userlevel');
        }

        $is_owner = $MSFrameworkUsers->getUserDataFromSession('is_owner');

        if($ary == null) {
            $ary = $this->getModules();

            if($cur_user_level !== "0") {

                $isSaaSDomain = $this->MSFrameworkSaaSBase->isSaaSDomain();

                if ($isSaaSDomain) {
                    $MSFrameworkSubscriptions = new \MSFramework\SaaS\subscriptions();
                    $cur_subs = $MSFrameworkSubscriptions->getAvailSubscriptions();
                    $menu_order = json_decode($cur_subs[$this->MSFrameworkSaaSEnvironments->getCurrentSubscription()]['menu'], true);

                    $ary = $this->orderSidebarModules($ary, $menu_order, $_SESSION['userData']['userlevel']);


                    $saasExpiredChecks = ($isSaaSDomain && $this->MSFrameworkSaaSEnvironments->isExpired());

                    foreach ($ary as $k => $m) {
                        if ($saasExpiredChecks) {
                            if ($_SESSION['userData']['userlevel'] == "1") {
                                if (!in_array($ary[$k]['id'], $this->avail_modules_on_saas)) {
                                    unset($ary[$k]);
                                }
                            } else if ($_SESSION['userData']['userlevel'] != "1" && $_SESSION['userData']['userlevel'] != "0") {
                                return array();
                            }
                        }

                        $children = $this->getAllChildrenIDS($m, 'childrens');
                        if ($_SESSION['userData']['userlevel'] != "0" && $isSaaSDomain && $m['id'] != "abbonamento_saas") {
                            //se siamo in un saas, escludo SEMPRE i moduli non inclusi nell'abbonamento dell'utente (tranne quelli definiti in avail_modules_on_saas)
                            $flat_ary = $this->getFlatSidebarArray($menu_order, "children");

                            if (!array_key_exists($m['id'], $flat_ary) && !in_array($m['id'], $this->avail_modules_on_saas)) {
                                unset($ary[$k]);
                                continue;
                            }

                            //escludo SEMPRE i moduli presenti nell'array $saas_never_avail_modules
                            if (in_array($ary[$k]['id'], $this->saas_never_avail_modules)) {
                                unset($ary[$k]);
                                continue;
                            }

                            $children = array_diff($this->getAllChildrenIDS($m, 'childrens'), $this->saas_never_avail_modules);
                        }

                        $ary[$k]['sidebar_id'] = $children;
                    }
                } else {
                    $cust_menu_order = (strstr($cur_user_level, 'custom-') ? $cur_user_level : null);
                    $ary = $this->orderSidebarModules($ary, $cust_menu_order, $cur_user_level);
                }
            }

        }

        $html = "";
        foreach($ary as $module) {

            if($_SESSION['userData']['userlevel'] != "0" && $this->MSFrameworkSaaSBase->isSaaSDomain()) {
                //escludo SEMPRE i moduli presenti nell'array $saas_never_avail_modules
                if(in_array($module['id'], $this->saas_never_avail_modules)) {
                    continue;
                }
            }

            if ($module['id'] == 'divider')
            {
                $html .= '<li class="divider"><h4>' . $module['value'] . '</h4></li>';
            }
            else
            {
                if ($ereditary_perms != null) {
                    $module['superadmin_limit'] = $ereditary_perms;
                }

                if ((!$module['superadmin_limit'] || $module['superadmin_limit'] && $is_owner) && (!is_array($module['limit_extra_function']) || count(array_intersect($module['limit_extra_function'], json_decode($r_producer_config['extra_functions'], true))) > 0) && ($module['limit_extra_module'] == "" || (new \MSFramework\modules())->checkIfIsActive($module['limit_extra_module']))) {
                    $set_active = (in_array($active_module, $module["sidebar_id"]) ? "active" : "");

                    $module['superadmin_limit'] = array();

                    if (is_array($module['childrens']) && $module['childrens'])
                    {

                        $fa_icon = 'fa fa-' . $module['fa_icon'];

                        if(stristr($fa_icon, '.')) $fa_icon = '<img src="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . $module['fa_icon'] . '" class="menu_icon">';
                        else $fa_icon = '<i class="' . $fa_icon . '"></i>';

                        //è un modulo padre
                        $html .= '<li class="' . $module['id'] . ' ' . $set_active . '">';
                        $html .= '<a href="#">' . $fa_icon . ' <span class="nav-label">' . $module['name'] . '</span> <span class="fa arrow"></span></a>';
                        $html .= '<ul class="nav nav-second-level">';

                        $html .= $this->printSidebarStructure($module['childrens'], $module['superadmin_limit']);

                        $html .= '</ul>';
                        $html .= '</li>';

                    } else {

                        $fa_icon = ($module['fa_icon'] != "" && is_null($ereditary_perms)) ? '<i class="fa fa-' . $module['fa_icon'] . '"></i>' : '';
                        if(stristr($fa_icon, '.')) $fa_icon = '<img src="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . $module['fa_icon'] . '" class="menu_icon">';

                        $path = ($module['path'] == "") ? ABSOLUTE_SW_PATH_HTML : ABSOLUTE_ADMIN_MODULES_PATH_HTML;

                        $html .= '<li class="' . $module['id'] . ' ' . $set_active . '">';
                        $html .= '<a href="' . $path . $module['path'] . '">' . $fa_icon . ' <span class="nav-label">' . $module['name'] . '</span></a>';
                        $html .= '</li>';
                    }

                }
            }
        }

        return $html;
    }

    /**
     * Costruisce l'HTML da visualizzare all'interno del modulo "Configurazioni produttore" dell'ACP, occupandosi di permettere la modifica dell'ordine di visualizzazione delle voci
     *
     * @param null $ary L'array in analisi. Se null, preleva l'array da $this->getModules
     * @param array $order_preset L'array con i preset ordinato. (Se NULL otterrà quello selezionato sul sito attuale, se false lascerà l'ordine di default)
     * @param integer $user_level Il livello utente per la quale si desidera mostrare la composizione
     * @param bool $rollback_on_producer_config Se impostato su true, nel caso in cui non ci sia nessuna configurazione precaricata, restituisce quella impostata nel modulo configurazione produttore
     * @param bool $get_max_available Se impostato su true, ottiene la lista completa dei moduli selezionabili
     *
     * @return string
     */
    public function printSidebarSetterOrder($ary = null, $order_preset = null, $user_level = 1, $rollback_on_producer_config = false, $get_max_available = false) {
        global $module_config; //è la variabile proveniente dal file moduleConfig del singolo modulo

        if($user_level == "0") return "";

        if($ary == null) {
            $ary = $this->orderSidebarModules($this->getModules(), $order_preset, $user_level, $rollback_on_producer_config, $get_max_available);
        }

        $html = "";
        foreach($ary as $module) {
            if($this->MSFrameworkSaaSBase->isSaaSDomain()) {
                //escludo SEMPRE i moduli presenti nell'array $saas_never_avail_modules
                if(in_array($module['id'], $this->saas_never_avail_modules)) {
                    continue;
                }
            }

            if ($module['id'] == 'divider')
            {
                $html .= '<li class="dd-item divider" data-id="divider" data-value="' . $module['value'] .  '"><div class="dd-handle"><span class="label label-success"><i class="fa fa-bars"></i></span> <span class="text">[DIVISORE] ' . $module['value'] . '</span> <a class="delete" onclick="$(this).closest(\'li\').remove()">Elimina</a></div></li>';
            }
            else
            {

                if ($module['path'] == "" && is_array($module['childrens'])) { //è un modulo padre

                    $jump_module = true;
                    foreach($module['childrens'] as $ck => $ch) {

                        // Imposto le variabili mancanti nel children (Utile quando il menù ($ary) passato non è passato da getFlatSidebarArray());
                        if(!isset($ch['superadmin_limit'])) $ch['superadmin_limit'] = $module['superadmin_limit'];
                        if(!isset($ch['limit_extra_function'])) $ch['limit_extra_function'] = $module['limit_extra_function'];
                        $module['childrens'][$ck] = $ch;

                        // Nel caso in cui nessuno dei children è visibile non mostro neanche il parent
                        if($ch['superadmin_limit'] !== true) {
                            $jump_module = false;
                        }
                    }

                    if($jump_module) {
                        continue;
                    }

                    $extra_required = true;
                    if( is_array($module['limit_extra_function']) || $module['limit_extra_module'] != "" ) {
                        $extra_required = false;
                    }

                    $fa_icon = 'fa fa-' . $module['fa_icon'];

                    if(stristr($fa_icon, '.')) $fa_icon = '<img src="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . $module['fa_icon'] . '" class="menu_icon">';
                    else $fa_icon = '<i class="' . $fa_icon . '"></i>';


                    $html .= '<li class="dd-item dd-collapsed ' . (!$extra_required ? ' disabled_in_current' : '') . '" data-id="' . $module['id'] . '">';

                    $html .= '<div class="dd-handle"><span class="label label-inverse">' . $fa_icon . '</span> ' . $module['name']  . (is_array($module['granular_settings']) ? '<span class="granular_settings_menuicon"><i class="fas fa-sliders-h"></i></span>' : '') . '</div>';

                    $html .= '<ol class="dd-list">';

                    $html .= $this->printSidebarSetterOrder($module['childrens'], null, $user_level);

                    $html .= '</ol>';
                    $html .= '</li>';
                } else {

                    if($module['superadmin_limit'] === true) {
                        continue;
                    }

                    $extra_required = true;
                    if( is_array($module['limit_extra_function']) || $module['limit_extra_module'] != "" ) {
                        $extra_required = false;
                    }

                    $fa_icon = ($module['fa_icon'] != "") ? '<i class="fa fa-' . $module['fa_icon'] . '"></i> ' : '';

                    if(stristr($fa_icon, '.')) $fa_icon = '<span class="label label-inverse"><img src="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . $module['fa_icon'] . '" class="menu_icon"></span>';

                    $html .= '<li class="dd-item dd-collapsed' . (!$extra_required ? ' disabled_in_current' : '') . '" data-id="' . $module['id'] . '">';
                    $html .= '<div class="dd-handle">' . $fa_icon . $module['name'] . (is_array($module['granular_settings']) ? '<span class="granular_settings_menuicon"><i class="fas fa-sliders-h"></i></span>' : '') . '</div>';
                    $html .= '</li>';
                }
            }
        }

        return $html;
    }

    /**
     * Ordina l'array con le voci del menù in modo personalizzato
     *
     * @param null $ary L'array in analisi
     * @param mixed $menu_order L'ID del preset da utilizzare (Se NULL otterrà quello selezionato sul sito attuale, se false lascerà l'ordine di default)
     * @param integer $user_level Il livello utente per la quale si desidera mostrare la composizione
     * @param bool $rollback_on_producer_config Se impostato su true, nel caso in cui non ci sia nessuna configurazione precaricata, restituisce quella impostata nel modulo configurazione produttore
     * @param bool $get_max_available Se impostato su true, ottiene la lista completa dei moduli selezionabili
     *
     * @return array
     */
    public function orderSidebarModules($ary, $menu_order = null, $user_level = 1, $rollback_on_producer_config = false, $get_max_available = false) {
        global $firephp, $MSFrameworkCMS, $MSFrameworkUsers;

        //alcuni ruoli statici (non admin) possono essere personalizzati per quanto riguarda il menu. in quel caso, la personalizzazione viene salvata nella tabella users_roles come custom in modo tale da poter recuperare i dati relativi al menu
        if(!strstr($user_level, "static-") && $this->MSFrameworkSaaSBase->isSaaSDomain()) {
            $linked_id = $this->MSFrameworkDatabase->getAssoc("SELECT id FROM users_roles WHERE static_id = :link", array(":link" => $user_level), true);

            if($linked_id['id'] != "") {
                $new_id = "custom-" . $linked_id['id'];
                $menu_order = $new_id;
                $user_level = $new_id;
            }
        }

        $include_user_roles = strstr($user_level, 'custom-');
        $presets = $this->getMenuPresets(true, $include_user_roles);

        if($get_max_available) {
            return $ary;
        }

        if(!is_array($menu_order)) {

            if (is_null($menu_order)) {
                if($this->MSFrameworkSaaSBase->isSaaSDomain()) {
                    $MSFrameworkSubscriptions = new \MSFramework\SaaS\subscriptions();
                    $cur_subs = $MSFrameworkSubscriptions->getAvailSubscriptions();
                    $menu_order = json_decode($cur_subs[$this->MSFrameworkSaaSEnvironments->getCurrentSubscription()]['menu'], true);
                } else {
                    $r_producer_config = $MSFrameworkCMS->getCMSData('producer_config');
                    $menu_order = $r_producer_config['modules_order'];
                }
            }

            if (isset($presets[$menu_order])) {
                $menu_order = $presets[$menu_order]['preset'];

                if (isset($menu_order[$user_level]) && !isset($menu_order[$user_level]['id'])) {
                    $menu_order = $menu_order[$user_level];
                }

            } else if (!isset($presets[$menu_order]) && $include_user_roles) { //non ci sono preferenze impostate per questo ruolo personalizzato, imposto di default la massima scelta possibile (quella di default dell'admin presente nel modulo "Impostazioni framework"
                $r_producer_config = $MSFrameworkCMS->getCMSData('producer_config');
                $menu_order = $presets[$r_producer_config['modules_order']]['preset'][1];
            } else {
                if ($rollback_on_producer_config) {
                    $r_producer_config = $MSFrameworkCMS->getCMSData('producer_config');
                    $menu_order = $presets[$r_producer_config['modules_order']]['preset'][$user_level];
                } else {
                    return $ary;
                }
            }
        }

        $flat_ary = $this->getFlatSidebarArray($ary);

        $return_ary = array();
        foreach($menu_order as $m) {

            if($m['id'] == 'divider') {
                $return_ary[] = array('id' => 'divider', 'value' => $m['value']);
            }
            else {

                $menu_key = array_search($m['id'], array_map(function ($product) {
                    return $product["id"];
                }, $flat_ary));

                if ($menu_key !== false) {
                    $menu_voice = $flat_ary[$menu_key];

                    $menu_voice['sidebar_id'] = $this->getAllChildrenIDS($m);

                    if ($m['children']) {


                        $menu_voice['childrens'] = $this->orderSidebarModules($ary, $m['children'], $user_level);
                    }

                    $return_ary[] = $menu_voice;
                }
            }
        }

        return $return_ary;

    }

    /**
     * Ottiene tutti i dettagli relativi ad un modulo, integrandole con alcune impostazioni comuni del modulo genitore (se presente).
     *
     * @param $id L'ID del modulo del quale si vogliono ottenere i dettagli
     * @param null $ary L'array in analisi. Se null, preleva l'array da $this->getModules
     * @param null $parent_details Le informazioni del modulo genitore
     * @param array $keeping_infos L'array utilizzato per ricordarsi le informazioni dei moduli genitori
     *
     * @return bool|mixed
     */
    public function getModuleDetailsFromID($id, $ary = null, $parent_details = null, $keeping_infos = array()) {
        global $firephp, $MSFrameworkCMS;

        if($ary == null) {
            $ary = $this->getModules('all');
        }

        if($id == "") {
            return false;
        }

        if($parent_details != null) {
            $keeping_infos['breadcrumbs'][] = $parent_details['name'];

            if($parent_details['superadmin_limit']) {
                $keeping_infos['superadmin_limit'] = $parent_details['superadmin_limit'];
            }
        }

        $specialJSeCSSPacks = array(
            'orakuploader' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/orakuploader/orakuploader.js",
                    ABSOLUTE_SW_PATH_HTML . "assets/js/uploadUtils.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/orakuploader/orakuploader.css",
                ),
                "dependencies" => array("rangeslider")
            ),
            'rangeslider' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ionRangeSlider/ion.rangeSlider.min.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ionRangeSlider/ion.rangeSlider.css",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css"
                )
            ),
            'toastr' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/toastr/toastr.min.js"
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/toastr/toastr.min.css",
                )
            ),
            'easyRowDuplication' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/easyRowDuplication/easyRowDuplication.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/easyRowDuplication/lang/it.js",
                ),
            ),
            'tinymce' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/tinymce/tinymce.min.js",
                    ABSOLUTE_SW_PATH_HTML . "assets/js/tinyMCEUtils.js",
                ),
            ),
            'dataTables' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/datatables.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/dataTables.responsive.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/responsive.bootstrap.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/dataTables.checkboxes.min.js",
                    ABSOLUTE_SW_PATH_HTML . "assets/js/dataTablesUtils.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/datatables.min.css",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/responsive.bootstrap.min.css",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/dataTables.checkboxes.css",
                ),
                "dependencies" => array("contextMenu")
            ),
            'contextMenu' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/contextMenu/jquery.contextMenu.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/contextMenu/jquery.ui.position.js"
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/contextMenu/jquery.contextMenu.min.css"
                )
            ),
            'ecommerceUtils' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "assets/js/ecommerceUtils.js",
                ),
                "dependencies" => array("select2")
            ),
            'dataTablesExport' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/jquery.dataTables.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/dataTables.buttons.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/jszip.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/dataTables/buttons.html5.min.js"
                )
            ),
            'datepicker' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/datapicker/bootstrap-datepicker.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/datapicker/bootstrap-datepicker-it.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/datapicker/datepicker3.css",
                )
            ),
            'datetimepicker' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/bootstrap-datetime-picker/js/locales/bootstrap-datetimepicker.it.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css",
                )
            ),
            'select2' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/select2/js/select2.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/select2/js/i18n/it.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/select2/css/select2.min.css",
                )
            ),
            'colorpicker' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/colorpicker/bootstrap-colorpicker.min.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/colorpicker/bootstrap-colorpicker.min.css",
                )
            ),
            'fontawesome-iconpicker' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/fontawesome-iconpicker/js/fontawesome-iconpicker.min.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/fontawesome-iconpicker/css/fontawesome-iconpicker.min.css",
                )
            ),
            'touchspin' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/touchspin/jquery.bootstrap-touchspin.min.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/touchspin/jquery.bootstrap-touchspin.min.css",
                )
            ),
            'googleMaps' => array(
                "options" => array("after_main.js"),
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/js/google/maps/markerclusterer/markerclusterer.js",
                    "https://maps.googleapis.com/maps/api/js?key=" . GMAPS_API_KEY . "&callback=initMap",
                ),
            ),
            'email-editor' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/email-editor/js/colorpicker.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/email-editor/js/bal-email-editor-plugin.js",
                    "https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.01/ace.js",
                    "https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.01/theme-monokai.js",
                    ABSOLUTE_SW_PATH_HTML . "assets/js/grapesjsUtils.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/textareaHighlight/jquery.highlighttextarea.min.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/email-editor/css/email-editor.bundle.min.css",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/email-editor/css/colorpicker.css",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/email-editor/css/editor-color.css",
                    ABSOLUTE_SW_PATH_HTML .  "vendor/plugins/textareaHighlight/jquery.highlighttextarea.min.css",
                ),
                'dependencies' => array('orakuploader', 'tinymce', 'rangeslider')
            ),
            'codemirror' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/lib/codemirror.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/addon/edit/matchbrackets.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/addon/format/formatting.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/mode/javascript/javascript.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/mode/htmlembedded/htmlembedded.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/mode/markdown/markdown.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/mode/htmlmixed/htmlmixed.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/mode/xml/xml.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/mode/clike/clike.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/mode/css/css.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/mode/php/php.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/lib/codemirror.css",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/theme/ambiance.css",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/codemirror/theme/icecoder.css",
                )
            ),
            'social-share-sdk' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "assets/js/social-share-sdk.js",
                )
            ),
            'worldDataUtils' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "assets/js/worldDataUtils.js",
                )
            ),
            'serpPreviewUtils' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "assets/js/serpPreviewUtils.js",
                )
            ),
            'clockpicker' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/clockpicker/clockpicker.js"
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/clockpicker/clockpicker.css",
                )
            ),
            'tagsinput' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/jquery.tagsinput/src/jquery.tagsinput.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/jquery.tagsinput/src/jquery.tagsinput.css",
                )
            ),
            'daterangepicker' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/daterangepicker/daterangepicker.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/daterangepicker/daterangepicker-bs3.css",
                )
            ),
            'query-builder' => array(
                "dependencies" => array('clockpicker', 'datetimepicker', 'daterangepicker', 'bootstrap-select'),
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/fullcalendar/moment.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/query-builder/js/query-builder.standalone.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/query-builder/i18n/query-builder.it.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/query-builder/css/query-builder.default.min.css"
                )
            ),
            'bootstrap-select' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/bootstrap-select/js/bootstrap-select.min.js"
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/bootstrap-select/css/bootstrap-select.min.css"
                )
            ),
            'nestable' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/nestable2/jquery.nestable.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/nestable2/jquery.nestable.css",
                )
            ),
            'visualBuilder' => array(
                "additional_js" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/visualBuilder/js/visualBuilder.js",
                ),
                "additional_css" => array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/visualBuilder/css/visualBuilder.css",
                    FRAMEWORK_COMMON_CDN . "modules/frontendParser/css/frontendHelper.css"
                ),
                "dependencies" => array("tinymce", "colorpicker", "rangeslider", "worldDataUtils", "orakuploader", "datetimepicker", "fontawesome-iconpicker")
            ),
        );

        $ary_to_return = false;

        foreach($ary as $module) {
            if($module['id'] == $id) {
                if($parent_details != null) {
                    unset($parent_details['childrens']);
                }

                $keeping_infos['breadcrumbs'][] = $module['name'];
                $module['breadcrumbs'] = $keeping_infos['breadcrumbs'];

                if($module['superadmin_limit']) {
                    $keeping_infos['superadmin_limit'] = $module['superadmin_limit'];
                }
                $module['superadmin_limit'] = $keeping_infos['superadmin_limit'];

                if(!is_array($parent_details)) {
                    $parent_details = array();
                }

                $m = array_merge($parent_details, $module);

                $module_relation = array();
                foreach((new \MSFramework\Frontend\Tags\tags())->getMSTags() as $parserTagID => $parserTagValues) {
                    if(isset($parserTagValues['module_relation']) && $parserTagValues['module_relation'] === $id) {
                        $module_relation[$parserTagID] = $parserTagValues;
                    }
                }

                if($module_relation) {
                    $m['frontend_shortcode'] = $module_relation;
                    $m['include_packs'][] = 'codemirror';
                }

                if(!in_array('toastr', $m['include_packs']) && json_decode($MSFrameworkCMS->getCMSData('site')['acp'], true)['exit_after_save'] != "1") {
                    $m['include_packs'][] = "toastr";
                }

                //verifico le dipendenze e le includo nei pacchetti da caricare
                foreach($m['include_packs'] as $included_pack) {
                    if(is_array($specialJSeCSSPacks[$included_pack]['dependencies'])) {
                        $m['include_packs'] = array_merge($m['include_packs'], $specialJSeCSSPacks[$included_pack]['dependencies']);
                    }
                }

                $m['include_packs'] = array_unique($m['include_packs']);

                $m['additional_js'] = (!is_array($m['additional_js'])) ? array() : $m['additional_js'];
                $m['additional_css'] = (!is_array($m['additional_css'])) ? array() : $m['additional_css'];

                //verifico la presenza di eventuali pacchetti da includere ed effettuo il merge con quelli peculiari del modulo

                $after_main = array('additional_js' => array(), 'additional_css' => array());
                foreach($specialJSeCSSPacks as $packK => $packV) {
                    $insert_after_main = false;
                    if(in_array($packK, $m['include_packs'])) {
                        if(in_array('after_main.js', $packV['options'])) {
                            $insert_after_main = true;
                        }

                        foreach($packV as $packType => $packValue) {
                            if(is_array($packValue) && $packType != "options") {
                                if($insert_after_main) {
                                    $after_main[$packType] = array_merge($packValue, $after_main[$packType]);
                                } else {
                                    $m[$packType] = array_merge($packValue, $m[$packType]);
                                }
                            }
                        }
                    }
                }

                unset($m['include_packs']);

                //aggiungo le inclusioni comuni a tutti i moduli
                $m['additional_js'] = array_merge($m['additional_js'], array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/staps/jquery.steps.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/validate/jquery.validate.min.js",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/chosen/chosen.jquery.js",
                    ABSOLUTE_SW_PATH_HTML . "assets/js/editFormUtils.js",
                    ABSOLUTE_SW_PATH_HTML . "assets/js/formStepsUtils.js",
                ));

                $m['additional_css'] = array_merge($m['additional_css'], array(
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/steps/jquery.steps.css",
                    ABSOLUTE_SW_PATH_HTML . "vendor/plugins/chosen/chosen.css",
                ), $after_main['additional_css']);

                if($m['path'] != "") $m['additional_js'][] = ABSOLUTE_SW_PATH_HTML . "modules/" . $m['path'] . "scripts/main.js";

                $m['additional_js'] = array_merge($m['additional_js'], $after_main['additional_js']);

                $ary_to_return = $m;
            }

            if($module['path'] == "" && is_array($module['childrens'])) { //è un modulo padre
                $t = $this->getModuleDetailsFromID($id, $module['childrens'], $module, $keeping_infos);
                if(is_array($t)) {
                    $ary_to_return = $t;
                }
            }
        }

        if(is_array($ary_to_return)) {
            $ary_to_return['module_fields_status'] = array();
            $current_modude_fields_status = $MSFrameworkCMS->getCMSData("modules_fields_status");
            if($current_modude_fields_status  && isset($current_modude_fields_status[$ary_to_return['id']])) {
                $ary_to_return['module_fields_status'] = $current_modude_fields_status[$ary_to_return['id']];
            }

            $ary_to_return["additional_js"] = array_unique($ary_to_return["additional_js"]);
            $ary_to_return["additional_css"] = array_unique($ary_to_return["additional_css"]);
        }

        return $ary_to_return;
    }

    /**
     * Ottiene tutti i dettagli relativi ad un modulo, integrandole con alcune impostazioni comuni del modulo genitore (se presente).
     *
     * @param $id L'ID del modulo del quale si vogliono ottenere i dettagli
     *
     * @return string L'URL del modulo in questione
     *
     */
    public function getModuleUrlByID($id) {
        return ABSOLUTE_ADMIN_MODULES_PATH_HTML . $this->getModuleDetailsFromID($id)['path'];
    }

    /**
     * Verifica se l'utente ha i permessi per accedere ad un determinato modulo
     *
     * @param $module_id L'ID del modulo
     * @param bool $force_logout Se impostato su true e l'utente non ha i permessi per visualizzare il modulo, viene effettuato il logout
     *
     * @return bool
     */
    public function accessGranted($module_id, $force_logout = false) {
        global $MSFrameworkCMS, $MSFrameworkDatabase, $MSFrameworkUsers;

        $cur_user_level = $MSFrameworkUsers->getUserDataFromSession('userlevel');
        $is_owner = $MSFrameworkUsers->getUserDataFromSession('is_owner');
        $r_producer_config = $MSFrameworkCMS->getCMSData('producer_config');

        $cust_menu_order = (strstr($_SESSION['userData']['userlevel'], 'custom-') ? $_SESSION['userData']['userlevel'] : null);

        if($cur_user_level == 0) {
            $ary =  $this->getFlatSidebarArray($this->getModules('all'));
        } else if($cust_menu_order || $cur_user_level == 1) {
            $ary = $this->getModules();

            if ($cur_user_level != "0") {
                $ary = $this->orderSidebarModules($ary, $cust_menu_order, $cur_user_level, false);
            } else {
                $ary = array_merge($ary, $this->getModules('fw360'));
            }

            $ary = array_merge($ary, $this->getModules('hidden'));
            $ary = $this->getFlatSidebarArray($ary);

            if ($this->MSFrameworkSaaSBase->isSaaSDomain()) {
                foreach ($this->avail_modules_on_saas as $force_avail) {
                    if (!array_key_exists($force_avail, $ary)) {
                        $ary[$force_avail] = array("id" => $force_avail);
                    }
                }
            }
        } else {
            $ary = array();
        }

        $access_granted = false;
        foreach($ary as $module) {
            if($module_id == $module['id']) {
                if ((!$module['superadmin_limit'] || ($module['superadmin_limit'] && $is_owner)) && (!is_array($module['limit_extra_function']) || count(array_intersect($module['limit_extra_function'], json_decode($r_producer_config['extra_functions'], true))) > 0) && ($module['limit_extra_module'] == "" || (new \MSFramework\modules())->checkIfIsActive($module['limit_extra_module']))) {
                    $access_granted = true;
                    break;
                }
            }
        }

        if(!$access_granted) {
            if ($force_logout) {
                $MSFrameworkUsers->endUserSession();
                $_SESSION['redirect_after_login'] = $_SERVER["REQUEST_URI"];
                header("location: " . ABSOLUTE_SW_PATH_HTML . "login.php?not-logged");
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Porta tutte le voci children del menù al primo livello
     *
     * @param $array L'array con la struttura originaria del menù
     * @param $children_key La chiave dei menu figli
     *
     * @return array
     */
    protected function getFlatSidebarArray($array, $children_key = "childrens")
    {

        $result = array();
        foreach ($array as $menu) {

            if(isset($menu[$children_key])) {
                foreach($this->getFlatSidebarArray($menu[$children_key]) as $sm) {

                    $var_to_inherit = array('fa_icon', 'userlevel', 'limit_extra_function');
                    foreach($var_to_inherit as $var) {
                        if(!isset($sm[$var])) {
                            $sm[$var] = $menu[$var];
                        }
                    }
                    $result[$sm['id']] = $sm;
                };
                unset($menu[$children_key]);
            }
            $result[$menu['id']] = $menu;
        }

        return $result;
    }

    /**
     * Ottiene gli IDS di tutte le voci dei menù figli
     *
     * @param array $array L'array con la struttura originaria del menù (array('id' => '', 'children' => array(...))
     * @param string $childrens_key La chiave dei menu figli
     *
     * @return array
     */
    protected function getAllChildrenIDS($array, $childrens_key = 'children')
    {

        $ids_to_return = array($array['id']);

        if(isset($array[$childrens_key])) {
            foreach($array[$childrens_key] as $c) {
                $ids_to_return = array_merge($ids_to_return, $this->getAllChildrenIDS($c, $childrens_key));
            }
        }

        return $ids_to_return;
    }

    /**
     * Ottiene un array con tutte le voci del menù nascoste (chiave "hidden" impostata)
     *
     * @param $array L'array con la struttura originaria del menù
     * @param $show_hidden bool Se impostato su false nasconderà i moduli con il parametro 'hidden' => true
     *
     * @return array
     */
    private function getHiddenMenuVoices($array, $show_hidden = true)
    {

        $result = array();
        foreach ($array as $menu) {
            if(!$show_hidden && isset($menu['hidden']) && $menu['hidden']) continue;
            if(isset($menu['childrens'])) {
                foreach($this->getFlatSidebarArray($menu['childrens']) as $sm) {
                    $var_to_inherit = array('fa_icon', 'userlevel', 'limit_extra_function');
                    foreach($var_to_inherit as $var) {
                        if(!isset($sm[$var])) {
                            $sm[$var] = $menu[$var];
                        }
                    }
                    $result[] = $sm;
                };
                unset($menu['childrens']);
            }
            $result[] = $menu;
        }

        return $result;
    }

    /**
     * Restituisce un array contenente tutti i moduli disponibili per l'array passato
     *
     * @param $array L'array con la struttura originaria del menù
     *
     * @return array
     */
    public function getEnabledModuleIDs($array = null)
    {
        if($this->MSFrameworkSaaSBase->isSaaSDomain() && $this->MSFrameworkSaaSEnvironments->isExpired()) {
            if($_SESSION['userData']['userlevel'] == "1") {
                return $this->avail_modules_on_saas;
            } else if($_SESSION['userData']['userlevel'] != "1" && $_SESSION['userData']['userlevel'] != "0") {
                return array();
            }
        }

        if($_SESSION['userData']['userlevel'] == "0") {
            $array = $this->getModules();
        } else {
            if(is_null($array)) {
                $cust_menu_order = (strstr($_SESSION['userData']['userlevel'], 'custom-') ? $_SESSION['userData']['userlevel'] : null);
                $array = $this->orderSidebarModules($this->getModules(), $cust_menu_order, $_SESSION['userData']['userlevel'], false);
                $array = array_merge($array, $this->getModules('hidden'));
            }
        }

        $result = array();
        foreach ($array as $menu) {
            if(isset($menu['childrens'])) {
                foreach($this->getFlatSidebarArray($menu['childrens']) as $sm) {
                    $var_to_inherit = array('fa_icon', 'userlevel', 'limit_extra_function');
                    foreach($var_to_inherit as $var) {
                        if(!isset($sm[$var])) {
                            $sm[$var] = $menu[$var];
                        }
                    }

                    $result[] = $sm['id'];
                };

                unset($menu['childrens']);
            }
            $result[] = $menu['id'];
        }

        if($this->MSFrameworkSaaSBase->isSaaSDomain() && is_array($this->avail_modules_on_saas)) {
            $result = array_merge($result, $this->avail_modules_on_saas);
        }

        return $result;
    }

    /**
     * Ottiene i campi formattati in HTML per l'assegnazione dei permessi granulari del modulo
     *
     * @param $module_id L'ID del modulo
     * @param $use_data Se valorizzato, i campi verranno precompilati a partire dai dati contenuti in questo array (utile in fase di modifica del record)
     * @param $context Il contesto in cui ci si trova (id del modulo che sta gestendo i permessi)
     * @param string $subscription_id Se si opera su un SaaS, è possibile specificare il tipo di abbonamento al quale fare riferimento. Lasciare vuoto per ottenere l'abbonamento corrente.
     * @param string $role_id Se si opera su un non-SaaS, è possibile specificare il ruolo utente a cui fare riferimento. Lasciare vuoto per ottenere il ruolo corrente.
     *
     * @return string
     */
    public function getHTMLGranularSettingsFields($module_id, $use_data = array(), $context, $subscription_id = "", $role_id = "") {
        if($context == "saas_configuration" && $subscription_id == "") {
            $subscription_id = $this->MSFrameworkSaaSEnvironments->getCurrentSubscription();
            $role_id = "";
        }

        if($context == "producer_config" && $role_id == "") {
            $role_id = $_SESSION['userData']['userlevel'];
            $subscription_id = "";
        }

        if($context == "rolesmanager") {
            $role_id = "";
            $subscription_id = "";
        }

        $module_details = $this->getModuleDetailsFromID($module_id);

        $html = '<input type="hidden" id="module_id" value="' . $module_id . '">';

        $html .= '<div class="row">';
        foreach($module_details['granular_settings'] as $settingID => $setting) {
            $cur_granular_value = $this->getGranularSettingsForID($module_id, $settingID, $context, $use_data, $subscription_id, $role_id);

            $type = $setting['type'];

            $html .= '<div class="col-md-6">';
            $html .= '<label>' . ($type == "input:checkbox" ? "&nbsp;" : $setting['name']) . '</label>';

            if($type == "input:checkbox") {
                $html .= '<div class="styled-checkbox form-control">
                                <label style="width: 100%;">
                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                                        <input type="checkbox" class="granular_field" id="' . $settingID . '" ' . ($cur_granular_value == "1" ? "checked" : "") . '>
                                        <i></i>
                                    </div>
                                    <span style="font-weight: normal;">' . $setting['name'] . '</span>
                                </label>
                            </div>';
            } else if($type == "input:text") {
                $html .= '<input type="text" id="' . $settingID . '" class="form-control granular_field" value="' . $cur_granular_value . '">';
            } else if($type == "input:number") {
                $html .= '<input type="number" id="' . $settingID . '" class="form-control granular_field" value="' . $cur_granular_value . '">';
            } else if($type == "select") {
                $html .= '<select class="form-control granular_field" id="' . $settingID . '">';
                foreach($setting['options'] as $optionK => $optionV) {
                    $html .= '<option value="' . $optionK . '" ' . ($cur_granular_value == $optionK ? "selected" : "") . '>' . $optionV . '</option>';
                }
                $html .= '</select>';
            }

            $html .= '</div>';
        }

        $html .= '</div>';

        return $html;
    }

    /**
     * Verifica se una funzionalità granulare è disponibile a fronte dei parametri indicati
     *
     * @param $module_id L'ID del modulo
     * @param $setting_id L'ID del settaggio granulare
     * @param $allowed_values Se il settaggio granulare rientra in uno o più parametri indicati in questo valore (array) allora la funzionalità risulta disponibile
     *
     * @return bool
     */
    public function checkGranularPermissions($module_id, $setting_id, $allowed_values) {
        $granular_value = $this->getGranularSettingsForID($module_id, $setting_id);

        if(!is_array($allowed_values)) {
            $allowed_values = array($allowed_values);
        }

        return (in_array($granular_value, $allowed_values) || $_SESSION['userData']['userlevel'] == "0");
    }

    /**
     * Ottiene il valore assegnato (o quello di default, in mancanza) per un particolare settaggio
     *
     * @param $module_id L'ID del modulo
     * @param $setting_id L'ID del settaggio granulare
     * @param $use_data Se valorizzato, i campi verranno precompilati a partire dai dati contenuti in questo array (utile in fase di modifica del record)
     * @param $context Il contesto in cui ci si trova (id del modulo che sta gestendo i permessi). Se vuoto, viene dato per scontato che il controllo debba essere fatto sul tipo di menu in uso dall'utente (quello che vede sulla sidebar) ed i restanti parametri vengono determinati in base alle informazioni relative all'utente.
     * @param string $subscription_id Se si opera su un SaaS, è possibile specificare il tipo di abbonamento al quale fare riferimento. Lasciare vuoto per ottenere l'abbonamento corrente.
     * @param string $role_id Se si opera su un non-SaaS, è possibile specificare il ruolo utente a cui fare riferimento. Lasciare vuoto per ottenere il ruolo corrente (solo se non ci si trova in un SaaS).
     *
     * @return mixed
     */
    private function getGranularSettingsForID($module_id, $setting_id, $context = "", $use_data = array(), $subscription_id = "", $role_id = "") {
        global $MSFrameworkCMS;

        if($context == "") { //devo determinare il contesto in autonomia
            $r_producer_config = $MSFrameworkCMS->getCMSData('producer_config');

            $use_data = array();
            $role_id = $_SESSION['userData']['userlevel'];

            if($this->MSFrameworkSaaSBase->isSaaSDomain()) {
                $subscription_id = $this->MSFrameworkSaaSEnvironments->getCurrentSubscription();
                $context = "saas_configuration";
            } else {
                $subscription_id = $r_producer_config['modules_order'];
                $context = "producer_config";

                //controllo se per questo ruolo utente ci sono impostazioni particolari
                if($this->MSFrameworkDatabase->getCount("SELECT id FROM `users_roles` WHERE id = :id", array(":id" => str_replace("custom-", "", $role_id))) > 0) {
                    $context = "rolesmanager";
                }
            }
        }

        if($context == "producer_config" && $role_id == "") {
            $role_id = $_SESSION['userData']['userlevel'];
            $subscription_id = "";
        }

        if(count($use_data) != 0) {
            $menu_granular = $use_data;
        } else {
            $menu_granular = $this->getCurrentGranularSettings($subscription_id, $context, $role_id);
        }

        if($context == "producer_config") {
            $cur_module_granular = $menu_granular[$role_id][$module_id];
        } else {
            $cur_module_granular = $menu_granular[$module_id];
        }

        if(!array_key_exists($setting_id, $cur_module_granular)) {
            $module_details = $this->getModuleDetailsFromID($module_id);
            $cur_module_granular[$setting_id] = $module_details['granular_settings'][$setting_id]['default'];
        }

        return $cur_module_granular[$setting_id];
    }

    /**
     * Ottiene l'array contenente le impostazioni granulari
     *
     * @param string $id Se ci si trova in un SaaS, questo parametro corrisponde all'ID dell'abbonamento. Se ci si trova in un ambiente normale, questo parametro corrisponde all'id della configurazione del menu (o a custom, per quelli personalizzati)
     * @param $context Il contesto in cui ci si trova (id del modulo che sta gestendo i permessi)
     * @param string $role_id Se si opera su un non-SaaS, è possibile specificare il ruolo utente a cui fare riferimento.
     *
     * @return array|mixed
     */
    public function getCurrentGranularSettings($id = "", $context = "", $role_id = "") {
        global $MSFrameworkDatabase;

        if($context == "saas_configuration") {
            $MSFrameworkSubscriptions = new \MSFramework\SaaS\subscriptions();
            $cur_subs = $MSFrameworkSubscriptions->getAvailSubscriptions();
            $menu_granular = json_decode($cur_subs[($id != "" ? $id : $this->MSFrameworkSaaSEnvironments->getCurrentSubscription())]['menu_granular'], true);
        } else if($context == "rolesmanager") {
            $r = $MSFrameworkDatabase->getAssoc("SELECT menu_granular FROM `users_roles` WHERE id = :id", array(":id" => str_replace("custom-", "", $role_id)), true);
            $menu_granular = json_decode($r['menu_granular'], true);
        } else {
            if($id == "custom") {
                $r = $MSFrameworkDatabase->getAssoc("SELECT value FROM `cms` WHERE type = 'menu_granular'", array(), true);
                $menu_granular = json_decode($r['value'], true);
            } else {
                $r = $MSFrameworkDatabase->getAssoc("SELECT menu_granular FROM `" . FRAMEWORK_DB_NAME . "`.`menu_presets` WHERE id = :id", array(":id" => $id), true);
                $menu_granular = json_decode($r['menu_granular'], true);
            }

            if($role_id != "") {
                $menu_granular = $menu_granular[$role_id];
            }
        }

        return $menu_granular;
    }
}
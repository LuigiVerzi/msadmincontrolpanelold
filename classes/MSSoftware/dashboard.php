<?php
/**
 * MSAdminControlPanel
 * Date: 28/06/18
 */

namespace MSSoftware;

class dashboard {

    /**
     * Ottiene l'array completo dei moduli
     *
     * @return array
     */
    public function getWidgets() {

        /**
         * Struttura dell'array che definisce i widget per la dashboard
         *
         * name: Il nome del modulo
         * id: Un ID univoco per il widget (Viene utilizzato per ottenere i file all'interno della cartella ajax/dashboard/[id_widget]
         * userlevel: Gli ID dei livelli utente che possono visualizzare il widget
         * limit_extra_function: Se impostato, il widget viene visualizzato solo se determinate funzionalità extra (ad esempio l'ecommerce) sono state abilitate.
         * limit_extra_module: Se impostato, il widget viene visualizzato solo se determinati plugin extra (moduli extra, ad esempio il sistema dei ticket) sono stati abilitati.
         * visibility_check: Se presente, la funzione indicata deve avere un risultato positivo per rendere disponibile il widget
         *
         */
        $widgets = array(
            array(
                "name" => "Informazioni Principali",
                "id" => "main",
                "path" => "ajax/dashboard/main",
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "full" => true
            ),
            array(
                "name" => "I tuoi guadagni",
                "id" => "ms_agency_guadagni",
                "path" => "ajax/dashboard/ms_agency/guadagni",
                "userlevel" => array(0, 'custom-2', 'custom-1'),
                "visibility_check" => function() {
                    return $_SESSION['db'] == (new \MSFramework\MSAgency\orders())->sellerOriginDB;
                },
                'full' => true
            ),
            array(
                "name" => "Informazioni Sito",
                "id" => "website",
                "path" => "ajax/dashboard/website",
                "userlevel" => array(0, 1, 2, 'realestate-3')
            ),
            array(
                "name" => "Informazioni Clienti",
                "id" => "customers",
                "path" => "ajax/dashboard/customers/riepilogo",
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM customers LIMIT 1") ? true : false);
                }
            ),
            /* DISATTIVATO ATTUALMENTE PERCHé PRESENTE NELLA DASH PRINCIPALE
            array(
                "name" => "Cronologia Clienti",
                "id" => "customers_histories",
                "path" => "ajax/dashboard/customers/cronologia",
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM customers LIMIT 1") ? true : false);
                }
            ), */
            array(
                "name" => "Bilancio Clienti",
                "id" => "customers_debiti",
                "path" => "ajax/dashboard/customers/bilancio",
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM customers_balance LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Sedute Clienti",
                "id" => "beautycenter_sedute",
                "path" => "ajax/dashboard/beautycenter/sedute",
                "limit_extra_function" => array("beautycenter"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM beautycenter_sedute LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Grafico Appuntamenti",
                "id" => "appuntamenti",
                "path" => "ajax/dashboard/appuntamenti",
                "limit_extra_function" => array("appointments"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                   Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM appointments_list LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Grafico Operazioni",
                "id" => "fatturazione_vendite",
                "path" => "ajax/dashboard/fatturazione/grafico",
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM fatturazione_vendite LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Grafico Ecommerce",
                "id" => "ecommerce_grafico",
                "path" => "ajax/dashboard/ecommerce/grafico",
                "limit_extra_function" => array("ecommerce"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_orders LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Domande Prodotti Ecommerce",
                "id" => "ecommerce_domande",
                "path" => "ajax/dashboard/ecommerce/domande",
                "limit_extra_function" => array("ecommerce"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_product_questions LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Carrelli Abbandonati",
                "id" => "ecommerce_carrelli",
                "path" => "ajax/dashboard/ecommerce/carrelli",
                "limit_extra_function" => array("ecommerce"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_users_extra WHERE carrello LIKE '[{\"product_id\"%' LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Articoli Più Letti",
                "id" => "blog_articoli",
                "path" => "ajax/dashboard/blog/articoli",
                "limit_extra_function" => array("blog"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM blog_posts LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Tracking Eventi",
                "id" => "tracking_eventi",
                "path" => "ajax/dashboard/tracking/eventi",
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM tracking__stats LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Grafico Prenotazioni",
                "id" => "hotel_grafico",
                "path" => "ajax/dashboard/hotel/grafico",
                "limit_extra_function" => array("hotel"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM prenotazioni LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Grafico Prenotazioni",
                "id" => "camping_grafico",
                "path" => "ajax/dashboard/camping/grafico",
                "limit_extra_function" => array("camping"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM prenotazioni LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Richieste Generiche",
                "id" => "richieste_generiche",
                "path" => "ajax/dashboard/richieste/generiche",
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM richieste WHERE type LIKE 'generic' LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Immobili Totali",
                "id" => "realestate_total_properties",
                "path" => "ajax/dashboard/realestate/properties/total",
                "limit_extra_function" => array("realestate"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili LIMIT 1") ? true : false);
                }
            ),
            array(
                "name" => "Grafico Immobili",
                "id" => "realestate_properties_graph",
                "path" => "ajax/dashboard/realestate/properties/graph",
                "limit_extra_function" => array("realestate"),
                "userlevel" => array(0, 1, 2, 'realestate-3'),
                "visibility_check" => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili LIMIT 1") ? true : false);
                }
            ),
        );

        return $widgets;
    }

    /**
     * Ottiene solamente i widget da mostrare
     *
     * @return array
     */
    public function getWidgetsToShow()
    {

        $cur_user_level = (new \MSFramework\users())->getUserDataFromSession('userlevel');
        $r_producer_config = (new \MSFramework\cms())->getCMSData('producer_config');

        $widget_to_show = array();
        foreach($this->getWidgets() as $widget) {

            $show_widget = false;
            if (in_array($cur_user_level, $widget['userlevel']) && (!is_array($widget['limit_extra_function']) || count(array_intersect($widget['limit_extra_function'], json_decode($r_producer_config['extra_functions'], true))) > 0) && ($widget['limit_extra_module'] == "" || (new \MSFramework\modules())->checkIfIsActive($widget['limit_extra_module']))) {
                $show_widget = true;
            }

            if($show_widget && isset($widget['visibility_check'])) {
                if(is_callable($widget['visibility_check'])) {
                    $show_widget = $widget['visibility_check']();
                }
            }

            if($show_widget) {
                $widget_to_show[] = $widget;
            }

        }

        return $widget_to_show;

    }
    /**
     * Ottiene le informazioni del widget
     *
     * @param $id L'id del widget
     *
     * @return array
     */
    public function getWidgetByID($id) {
        $ary_to_return = array();
        foreach($this->getWidgets() as $widget) {
            if($widget['id'] == $id) {
                $ary_to_return = $widget;
            }
        }

        return $ary_to_return;
    }


}
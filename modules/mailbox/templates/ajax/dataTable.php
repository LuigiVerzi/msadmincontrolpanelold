<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM mailbox__templates") as $r) {
    $array['data'][] = array(
        $r['id'],
        '<img src="' . UPLOAD_MAILBOX_TEMPLATES_FOR_DOMAIN_HTML . 'thumb_' . $r['id'] . '.png" style="width: 100px;">',
        ucfirst($r['tipologia']),
        $r['titolo'],
        $r['data_creazione'],
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

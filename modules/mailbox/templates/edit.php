<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */ ?>

<?php
require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM mailbox__templates WHERE id = :id", array(':id' => $_GET['id']), true);
}
$shortcodes_globali = (new \MSFramework\emails())->getCommonShortcodes();
$emailClass = new \MSFramework\emails();
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php
                require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php");
                ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nome Template*</label>
                                    <input id="titolo" name="titolo" class="form-control required" value="<?php echo $r['titolo'] ?>" type="text" required>

                                    <div style="display: none;"> <!-- Nascondo e non elimino perchè voglio comunque che il SW mi salvi "newsletter" nella colonna "tipologia" del DB. Lo conservo per eventuali usi futuri -->
                                        <br>
                                        <label>Tipologia Template</label> <br />
                                        <select id="tipologia" class="form-control required" required>
                                            <option value="newsletter" <?= ($r['tipologia'] == 'newsletter' ? ' selected' : ''); ?>>Per le Newsletter</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <h2 class="title-with-button">
                                Contenuto
                                <a class="btn btn-default btn-sm btn_shortcodes" data-toggle="modal" data-target="#modal_shortcodes"><i class="fa fa-code"></i> Shortcodes</a>
                            </h2>

                            <div class="alert alert-info" id="shortcodes_alert" style="padding: 10px; display: none;">
                                <h3>Lista shortcodes</h3>
                                <p>
                                    <?php foreach($shortcodes_globali[0] as $k => $shortcode) { ?>
                                        <b><?= $shortcode; ?></b> - <?= $shortcodes_globali[1][$k]; ?><br>
                                    <?php } ?>
                                </p>
                            </div>

                            <div id="versione_html" name="versione_html"><?php echo $emailClass->formatTemplateForEditor($r['versione_html']); ?></div>

                            <input type="hidden" id="canvas_preview">
                            <div id="preview_template_html" style="width: 600px; height: 600px; position: fixed; top: -100vh; left: -100vw; display: none;"></div>
                        </fieldset>

                        <h1>Allegati</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php (new \MSFramework\uploads('MAILBOX'))->initUploaderHTML("mainImages", $r['attachments']); ?>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>


<script>
    window.template_shortcodes = {global: <?= json_encode($shortcodes_globali[0]); ?>};
    
    globalInitForm();
</script>
</body>
</html>
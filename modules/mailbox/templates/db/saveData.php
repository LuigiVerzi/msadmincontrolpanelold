<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pTitolo'] == "" || $_POST['pVersioneHTML'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$uploader = new \MSFramework\uploads('MAILBOX');
$ary_files = $uploader->prepareForSave($_POST['pMainImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_template = (new \MSFramework\mailBox())->getTemplateDetails($_POST['pID'], "attachments")[$_POST['pID']];
}

// Creo la versione testuale estrapolando il testo dall'HTML
$html = new \Html2Text\Html2Text($_POST['pVersioneHTML']);
$versione_testuale =  $html->getText();

$array_to_save = array(
    "titolo" => $_POST['pTitolo'],
    "versione_testuale" => $versione_testuale,
    "versione_html" => $_POST['pVersioneHTML'],
    "attachments" => json_encode($ary_files),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO mailbox__templates ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE mailbox__templates SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $unlink_files = array();
        foreach ($ary_files as $file) {
            if(!(new \MSFramework\mailBox())->isSharedAttachment($file, 'mailbox__templates', $_POST['pID'])) {
                $unlink_files[] = $file;
            }
        }

        $uploader->unlink($unlink_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $unlink_files = array();
    foreach(json_decode($r_old_template['attachments'], true) as $old_file) {
        if(!in_array($old_file, $ary_files) && !(new \MSFramework\mailBox())->isSharedAttachment($old_file, 'mailbox__templates', $_POST['pID'])) {
            $unlink_files[] = $old_file;
        }
    }

    $uploader->unlink($unlink_files);
}

// Aggiorna l'anteprima del template
$template_id = ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : $_POST['pID']);
$preview_image = $_POST['pPreviewImage'];

if(!file_exists(UPLOAD_MAILBOX_TEMPLATES_FOR_DOMAIN)) {
    mkdir(UPLOAD_MAILBOX_TEMPLATES_FOR_DOMAIN);
}

$preview_image = str_replace('data:image/png;base64,', '', $preview_image);
$preview_image = str_replace(' ', '+', $preview_image);
$preview_image = base64_decode($preview_image);

file_put_contents(UPLOAD_MAILBOX_TEMPLATES_FOR_DOMAIN . 'thumb_' . $template_id . '.png', $preview_image);

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    initOrak('mainImages', 10, 0, 0, 100, getOrakImagesToPreattach('mainImages'), false, ['image/jpeg', 'image/png']);

    html_editor = initEmailEditor('#versione_html', 'MAILBOX', window.template_shortcodes);

    $('.btn_shortcodes').on('click', function() {
        $('#shortcodes_alert').toggle();
    });

    $('#dataTableSendTestTemplate').show();

}

function moduleSaveFunction(callbackSuccess) {

    startLoadingMode('Generazione anteprima');
    var $previewBlock = $('#preview_template_html');
    var width = ($('.bal-content table.main').length ? $('.bal-content table.main').first().css('width').replace('px', '') : 600);
    $previewBlock.html(html_editor.getContentHtml()).css('width', width).css('height', width).show();

    html2canvas($previewBlock[0], {
        x: $previewBlock.offset().left,
        y: $previewBlock.offset().top
    }).then(function(canvas) {
        var templatePreview = canvas.toDataURL("image/png");
        $previewBlock.hide();

        $.ajax({
            url: "db/saveData.php",
            type: "POST",
            data: {
                "pID": $('#record_id').val(),
                "pTitolo": $('#titolo').val(),
                "pVersioneHTML": html_editor.getContentHtml(),
                "pMainImagesAry": composeOrakImagesToSave('mainImages'),
                "pPreviewImage": templatePreview
            },
            async: false,
            dataType: "json",
            success: function(data) {
                if(data.status == "mandatory_data_missing") {
                    bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
                } else if(data.status == "mv_error") {
                    bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
                } else if(data.status == "query_error") {
                    bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
                } else if(data.status == "ok") {
                    succesModuleSaveFunctionCallback(data);
                    if(typeof(callbackSuccess) === 'function') {
                        callbackSuccess(data.id);
                    }
                }
            }
        });
    });
}

function getTemplatePreview(options) {

    startLoadingMode('Generazione anteprima');

    var $previewBlock = $('#preview_template_html');

    var width = ($('.bal-content table.main').length ? $('.bal-content table.main').first().css('width').replace('px', '') : 600);

    $previewBlock.html(html_editor.getContentHtml()).css('width', width).css('height', width).show();

    options = {
        x: $previewBlock.offset().left,
        y: $previewBlock.offset().top
    };

    html2canvas($previewBlock[0], options).then(canvas => {
        var imgageData = canvas.toDataURL("image/png");
        $previewBlock.hide();
        $('#canvas_preview').val('src', imgageData);

        endLoadingMode();
    });

}
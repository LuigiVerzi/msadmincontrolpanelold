webClient = {
    navigation: {
        checkHash: function () {
            if (location.hash.indexOf('view') >= 0) {
                webClient.load.mailContent(location.hash.replace('#view', ''));
            } else if (location.hash.indexOf('folder') >= 0) {
                webClient.load.mailFolder(location.hash.replace('#folder', ''));
            } else if (location.hash.indexOf('compose') >= 0) {
                webClient.load.mailCompose();
            } else {
                webClient.load.mailFolder();
            }
        }
    },
    load: {
        mailFolder: function (id) {
            if(typeof(id) === 'undefined')  {
                if($('.changeFolderBtn.active').length) {
                    id = $('.changeFolderBtn.active').data('id');
                } else {
                    id = $('.changeFolderBtn').first().data('id');
                }
            }

            if(!$('.changeFolderBtn[data-id="' + id + '"]').length) {
                return false;
            }

            $('.changeFolderBtn').removeClass('active');
            $('.changeFolderBtn[data-id="' + id + '"]').addClass('active');

            window.history.pushState('', '', '#folder' + id);

            var dirInfo = $('.changeFolderBtn[data-id="' + id + '"]').closest('.serverDirectories').data();
            $('#emailClient .dirName').html($('.changeFolderBtn[data-id="' + id + '"]').find('span').html());
            $('#emailClient .mailName').html(dirInfo.mail);

            webClient.load.mailList(id);
        },
        mailList: function (id) {
            if(typeof(id) === 'undefined') id = '';
            window.emailDataTable.ajax.url( 'ajax/dataTable.php?folder=' + id ).load();
            window.currentClientFolder = id;
            $('#emailClient .emailClientContent').hide();
            $('#emailClient .emailList').show();
        },
        mailContent: function (id) {
            $('#emailClient .emailView').html('<div class="circlePreloader"><div></div></div>').show();
            $.ajax({
                url: "ajax/getEmail.php",
                type: "GET",
                data: {
                    "id": id
                },
                async: true,
                dataType: "text",
                success: function(data) {
                    $('#emailClient').removeClass('writingMode');
                    $('#emailClient .emailClientContent').hide();
                    if(data.length) {
                        window.history.pushState('', '', '#view' + id);
                        $('#emailClient .emailView').html(data).show();

                        webClient.editor.initTemplateChooser($('.emailView .choseTemplateList'), $('#emailReplyTextarea'), function ($target) {
                            $('#emailClient .emailClientContent.emailView .emailChoseTemplate').hide();
                            $target.closest('.emailWriteBox').show();
                        });

                        $('#emailClient .emailClientContent.emailView').off('click').on('click', '.undoBtn', function () {
                            $('#emailClient .emailClientContent.emailView .emailWriteBox').hide();
                            $('#emailClient .emailClientContent.emailView .emailChoseTemplate').show();
                            $('#emailClient').removeClass('writingMode');
                        }).on('click', '.sendBtn', function () {
                            webClient.email.send(webClient.editor.getContent($('#emailReplyTextarea')), id);
                        });

                    } else {
                        toastr['error']('Errore durante il caricamento dell\'email');
                        $('#emailClient .emailList').show();
                    }
                }
            });
        },
        mailCompose: function () {

            $('#emailClient .emailClientContent').hide();
            $('#emailClient .emailClientContent.emailCompose').show();

            $('#emailClient .emailClientContent.emailCompose .emailWriteBox').hide();
            $('#emailClient .emailClientContent.emailCompose .emailChoseTemplate').show();

            webClient.editor.initTemplateChooser($('.emailCompose .choseTemplateList'), $('#emailComposeTextarea'), function ($target) {
                $('#emailClient .emailClientContent.emailCompose .emailChoseTemplate').hide();
                $target.closest('.emailWriteBox').show();
            });

            $('#emailClient .emailClientContent.emailCompose').off('click').on('click', '.undoBtn', function () {
                $('#emailClient .emailClientContent.emailCompose .emailWriteBox').hide();
                $('#emailClient .emailClientContent.emailCompose .emailChoseTemplate').show();
            }).on('click', '.sendBtn', function () {

                var recipients = $('#emailComposeTo').val();
                var subject = $('#emailComposeSubject').val();
                var server_id = $('#emailComposeSender').val();

                webClient.email.send(webClient.editor.getContent($('#emailComposeTextarea')), {
                    recipients: recipients,
                    subject: subject,
                    server_id: server_id
                });

            });

            window.history.pushState('', '', '#compose');
        }
    },
    email: {
        refresh: function() {
            var emailDownloadDialog = bootbox.dialog({
                message: '<div class="text-center mb-0"><i class="fa fa-spin fa-cog fa-2x block"></i> Download email in corso...</div>',
                closeButton: false
            });

            $.ajax({
                url: "ajax/downloadEmails.php",
                type: "POST",
                async: true,
                dataType: "text",
                success: function(data) {
                    webClient.load.mailFolder();
                    emailDownloadDialog.modal('hide');
                }
            });
        },
        loadTemplate: function(tpl_id) {
            $('#emailClient').addClass('writingMode');
            $.ajax({
                url: 'ajax/loadTemplate.php',
                type: "POST",
                data: {
                    "pLayout": tpl_id
                },
                async: true,
                dataType: "json",
                success: function (data) {
                    $('.emailChoseTemplate').hide();
                    $('.emailWriteBox').show();

                    $('.sendEmailBtn').show();

                    for (var i = tinymce.editors.length - 1 ; i > -1 ; i--) {
                        var ed_id = tinymce.editors[i].id;
                        tinyMCE.execCommand("mceRemoveEditor", true, ed_id);
                    }

                    if(data[0].length) {
                        $('#emailWriteTextarea').html(data[0]);

                        $('#emailWriteTextarea .element-content:not(.image)').each(function () {
                            $(this).html('<div>' + $(this).html() + '</div>');
                            initTinyMCE($(this).find('> div'), {small: true}, {inline: true});
                        });
                    } else {
                        $('#emailWriteTextarea').html('');
                        initTinyMCE($('#emailWriteTextarea'));
                    }
                }
            })
        },
        print: function () {
            $('#emailClient iframe')[0].contentWindow.print();
        },
        delete: function (id) {
            if(typeof(id) == 'undefined') {
                sel_row = getSelectedRows('main_table_list');
            } else {
                sel_row = [{id: id}];
            }
            if(sel_row.length > 0) {
                bootbox.confirm('Sei sicuro di voler eliminare definitivamente l\'email?' + (sel_row.length > 1 ? "<br><small>(" + sel_row.length  + " email selezionate)</small>" : ""), function(result) {
                    if(result) {
                        var ids = [];
                        sel_row.forEach(function(row, key) {
                            ids.push(row.id);
                        });
                        unselectAllRow('main_table_list');

                        $.ajax({
                            url: "db/deleteData.php",
                            type: "POST",
                            data: {
                                "pID": ids
                            },
                            async: true,
                            dataType: "json",
                            success: function(data) {
                                webClient.load.mailFolder();
                                toastr['success']('Email eliminate definitivamente');
                            }
                        });
                    }
                });
            } else {
                bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
                return false;
            }
        },
        trash: function (id) {
            if(typeof(id) == 'undefined') {
                sel_row = getSelectedRows('main_table_list');
            } else {
                sel_row = [{id: id}];
            }
            if(sel_row.length > 0) {
                bootbox.confirm('Sei sicuro di voler spostare l\'email nel cestino?' + (sel_row.length > 1 ? "<br><small>(" + sel_row.length  + " email selezionate)</small>" : ""), function(result) {
                    if(result) {
                        var ids = [];
                        sel_row.forEach(function(row, key) {
                            ids.push(row.id);
                        });
                        unselectAllRow('main_table_list');

                        $.ajax({
                            url: "db/deleteData.php",
                            type: "POST",
                            data: {
                                "pID": ids
                            },
                            async: true,
                            dataType: "json",
                            success: function(data) {
                                webClient.load.mailFolder();
                                toastr['success']('Email spostate nel cestino');
                            }
                        });
                    }
                });
            } else {
                bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
                return false;
            }
        },
        send: function (content, recipient_info) {

            var params = {
                content: content
            };

            if(typeof(recipient_info) === 'object') {
                params = Object.assign(recipient_info, params);
            } else {
                params.in_reply = recipient_info;
            }

            $.ajax({
                url: 'ajax/sendEmail.php',
                type: "POST",
                data: params,
                async: true,
                dataType: "json",
                success: function (data) {
                    if(data.status === 'ok') {
                        toastr['success']("Email inviata correttamente");
                    } else {
                        toastr['error']("Errore durante l'invio della mail");
                    }
                }
            });

        }
    },
    editor: {
        initTemplateChooser: function ($elem, $target, onChooseFn) {
            $elem.html($('#tpl_templateChooser').val());
            $elem.off('click').on('click', '.template_choose', function (e) {
                e.preventDefault();
                onChooseFn($target);
                webClient.editor.initTextArea($target, $(this).data('id'));
            }).closest('.emailWriteContent').off('click').on('click', '.importEmailtemplate', function(e) {
                e.preventDefault();
                var tpl_id = $(this).data('id');
                bootbox.confirm("Importando un nuovo template perderai le modifiche effettuate fino ad ora, vuoi procedere?", function(result) {
                    if (result) {
                        webClient.editor.initTextArea($target, tpl_id);
                    }
                });
            });
        },
        initTextArea: function ($elem, tpl_id) {
            $elem.before('<div class="circlePreloader"><div></div></div>');
            $.ajax({
                url: 'ajax/loadTemplate.php',
                type: "POST",
                data: {
                    "pLayout": tpl_id
                },
                async: true,
                dataType: "json",
                success: function (data) {

                    $elem.parent().find('.circlePreloader').remove();

                    for (var i = tinymce.editors.length - 1; i > -1; i--) {
                        var ed_id = tinymce.editors[i].id;
                        tinyMCE.execCommand("mceRemoveEditor", true, ed_id);
                    }

                    if (data[0].length) {
                        $elem.html(data[0]).show().css('visibility', 'visible');
                        $elem.find('.element-content:not(.image)').each(function () {
                            $(this).html('<div>' + $(this).html() + '</div>');
                                initTinyMCE($(this).find('> div'), {small: true}, {inline: true});
                        });
                    } else {
                        $elem.html('').show().css('visibility', 'visible');
                        initTinyMCE($elem);
                    }

                }
            });
        },
        getContent: function($elem) {
            var value = '';
            var editorToReattach = [];
            tinymce.EditorManager.editors.forEach(function(editor) {
                tinymce.EditorManager.execCommand('mceRemoveEditor', false, editor.id);
                editorToReattach.push(editor.id);
            });

            value = $elem.html();

            editorToReattach.forEach(function (id) {
                tinymce.EditorManager.execCommand('mceAddEditor', false, id);
            });

            return value;
        }
    }
};

$(document).ready(function() {
    window.emailDataTable = loadStandardDataTable('main_table_list', true);
    window.currentClientFolder = '';
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');

    $('#emailClient').css('min-height', $('#page-wrapper').height() - $('#emailClient').offset().top);

    webClient.navigation.checkHash();
});

window.onpopstate = function(event) {
    webClient.navigation.checkHash();
};

function initForm() {}

function onDataTableEdit(rowInfo) {
    var id = rowInfo[0].id;
    webClient.load.mailContent(id);
    return false;
}

window['webClient.email.trash'] = webClient.email.trash;
window['webClient.email.delete'] = webClient.email.delete;
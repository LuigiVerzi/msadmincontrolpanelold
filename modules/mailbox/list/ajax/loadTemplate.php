<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($_POST['pLayout']) {
    $r = (new \MSFramework\mailBox())->getTemplateDetails($_POST['pLayout'], 'versione_html, attachments, versione_testuale')[$_POST['pLayout']];
    $r['versione_html'] = (new \MSFramework\emails())->formatTemplateForEditor($r['versione_html']);
} else {
    $r['versione_html'] = '';
    $r['versione_testuale'] = '';
}


ob_start();
(new \MSFramework\uploads('MAILBOX'))->initUploaderHTML("emailImages", ($r ? $r['attachments'] : array()));
$attachments = ob_get_contents();
ob_clean();


echo json_encode(array($r['versione_html'], $attachments, $r['versione_testuale']));
?>

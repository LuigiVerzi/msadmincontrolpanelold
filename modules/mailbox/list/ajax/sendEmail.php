<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$status = array('status' => 'error');

$recipients = array();
$subject = '';
$headers = array();

if(isset($_POST['in_reply'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM mailbox__messages WHERE id = :id", array(':id' => $_POST['in_reply']), true);
    if($r) {
        $recipients = array($r['sender_email']);
        $subject = 'RE: ' . $r['subject'];
        $server = $r['server_id'];
        $headers = array(
            'In-Reply-To' => '<' . $r['message_id'] . '>'
        );
    }
} else {
    $recipients = explode(',', $_POST['recipients']);
    $subject = $_POST['subject'];
    $server = $_POST['server_id'];
}

$content = $_POST['content'];

$html = new \Html2Text\Html2Text($content);
$txt_version = $html->getText();

if($server === '') {
    $server = 'default';
}

$smtp_server = (new \MSFramework\emails())->getSMTPDetails($server, true);

if(!$recipients || empty($subject) || !$smtp_server) {
    $status['status'] = 'error';
}

$smtp_server = $smtp_server['data'];

/* IMPOSTO I DATI SMTP DEL SERVER */
$emailClass = new \MSFramework\emails();
if (!empty($smtp_server['smtpsecure'])) {
    $emailClass->mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
}

$emailClass->mail->Host = $smtp_server['host'];
$emailClass->mail->Port = $smtp_server['port'];
$emailClass->mail->SMTPAuth = true;
$emailClass->mail->AuthType = 'LOGIN';
$emailClass->mail->Username = $smtp_server['login'];
$emailClass->mail->Password = $smtp_server['password'];

$emailClass->mail->SMTPSecure = $smtp_server['smtpsecure'];
$emailClass->mail->Sender = $smtp_server['username'];  // bounce to
$emailClass->mail->setFrom($smtp_server['username'], $smtp_server['name']);

$reply_to = array('email' => $smtp_server['username'], 'name' => $smtp_server['name']);

foreach($headers as $hN => $hV) {
    $emailClass->mail->addCustomHeader($hN, $hV);
}

if($emailClass->sendCustomMail($subject, $content, $recipients, $reply_to, $txt_version)) {
    $status['status'] = 'ok';

    // Salvo la mail inviata nella cartella IMAP del server
    $mailBox = new \MSFramework\mailBox();
    $imapStream = $mailBox->getImapConnection($smtp_server, '');

    $mailBoxPath = $mailBox->lastImapPath .= 'INBOX.[FW360] Inviate';
    imap_createmailbox($imapStream, imap_utf7_encode($mailBoxPath));
    imap_subscribe($imapStream, $mailBoxPath );
    $res = imap_append($imapStream, $mailBoxPath, $emailClass->mail->getSentMIMEMessage());
    imap_close($imapStream);
} else {
    $status['status'] = 'error';
}

die(json_encode($status));
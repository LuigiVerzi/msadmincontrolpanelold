<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$id = $_POST['id'];
$folder = $_POST['folder'];

$result = 0;
if(in_array($folder, array_keys((new \MSFramework\mailBox())->getFolders()))) {
    $result = $MSFrameworkDatabase->query("UPDATE mailbox__messages SET folder = :folder WHERE id = :id", array(':folder' => $folder, ':id' => $id));
}

die($result);
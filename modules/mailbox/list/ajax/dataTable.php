<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');
$MSFrameworkUtils = new \MSFramework\utils();
$defaultAvatar = $MSFrameworkCMS->getURLToSite(1) . 'assets/img/user.png';

$folder = $_GET['folder'];
if(count(explode('_', $folder)) === 2) {
    $explodedFolder = explode('_', $folder);
}

$siteLogo = json_decode($MSFrameworkCMS->getCMSData('site')["logos"], true)['logo'];
$siteLogo = UPLOAD_LOGOS_FOR_DOMAIN_HTML . $siteLogo;

$extraBtn = array();
$extraBtn[] = array(
    'label' => "Leggi email",
    'btn' => 'info',
    'icon' => 'eye',
    'class' => 'loadEmailContent',
    'function' => 'loadEmailContent',
    'context' => false
);

if(!$explodedFolder || $explodedFolder[1] !== 'Trash') {
    $extraBtn[] = array(
        'label' => "Sposta nel cestino",
        'btn' => 'danger',
        'icon' => 'trash',
        'class' => 'moveMailToTrash',
        'function' => 'webClient.email.trash',
        'context' => true
    );
} else {
    $extraBtn[] = array(
        'label' => "Elimina definitivamente",
        'btn' => 'danger',
        'icon' => 'trash',
        'class' => 'deleteMail',
        'function' => 'webClient.email.delete',
        'context' => true
    );
}

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array(
        'db' => 'id',
        'dt' => 'DT_ExtraBtn',
        'formatter' => function( $d, $row ) {
            Global $extraBtn;
            return $extraBtn;
        }
    ),
    array(
        'db' => 'id',
        'dt' => 'DT_buttonsLimit',
        'formatter' => function( $d, $row ) {
            return "read";
        }
    ),
    array(
        'db' => 'folder',
        'dt' => 'DT_mailFolder'
    ),
    array(
        'db' => 'sender_email',
        'dt' => 'DT_SenderEmail'
    ),
    array(
        'db' => 'CONCAT(sender_info, ",", sender_email)',
        'dt' => 0,
        'formatter' => function($d, $row) {
            Global $defaultAvatar, $siteLogo;

            $info = explode(',', $d);
            if($row['folder'] === 'Sent') {
                return '<img src="' . $siteLogo . '" width="35" height="35" class="avatarLogo">';
            } else {
                $gravemail = md5(strtolower(trim($info[1])));
                return '<img src="http://www.gravatar.com/avatar/' . $gravemail . '?d=' . $defaultAvatar . '" width="35" height="35" class="avatarLogo">';
            }
        }
    ),
    array(
        'db' => 'sender_info',
        'dt' => 1,
        'formatter' => function($d, $row) {

            $html = '<div style="line-height: 1;">';
            if($row['folder'] === 'Sent') {
                $html .= 'Tu';
            } else {
                $html .= $d;
            }
            $html .= '<br>';
            $html .= '<small class="text-info">' . $row['sender_email'] . '</small>';
            $html .= '</div>';

            return $html;
        }
    ),
    array(
        'db' => 'subject',
        'dt' => 2
    ),
    array(
        'db' => '(SELECT COUNT(*) FROM mailbox__attachments WHERE mailbox__attachments.email_id = mailbox__messages.id)',
        'dt' => 3,
        'formatter' => function($d, $row) {
            if($d > 1) {
                return '<i class="text-muted fa fa-paperclip"></i>';
            }
        }
    ),
    array(
        'db' => 'mail_date',
        'dt' => 4,
        'formatter' => function($d, $row) {
            Global $MSFrameworkUtils;
            return array("display" => '<div class="text-right small">' . $MSFrameworkUtils->smartdate(strtotime($d)) . '</div>', 'sort' => strtotime($d));
        }
    )
);

if($explodedFolder) {
    $results = $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'mailbox__messages', 'id', $columns, null, "folder = '" . $explodedFolder[1] . "' AND server_id = '" . (int)$explodedFolder[0] . "'");
} else {
    $results = $datatableHelper::simple( $_GET, $MSFrameworkDatabase, 'mailbox__messages', 'id', $columns );
}

echo json_encode($results);
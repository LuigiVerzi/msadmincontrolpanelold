<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkMailBox = new \MSFramework\mailBox();

$r = array();

if(isset($_GET['id']) && !empty($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM mailbox__messages WHERE id = :id", array(':id' => $_GET['id']), true);
}

if(!$r) die();

$deleteFunction = ($r['folder'] === 'Trash' ? 'webClient.email.delete' : 'webClient.email.trash');

if(isset($_GET['body'])) {
    echo '<style>body {padding: 0; margin: 0;}</style>';
    die($r['body']);
}

$mailAttachments = $MSFrameworkDatabase->getAssoc("SELECT * FROM mailbox__attachments WHERE email_id = :email_id", array(':email_id' => $r['id']));
?>

<div class="mail-box-header">

    <div class="pull-right emailActions">
        <button class="btn btn-white btn-sm" title="Rispondi" onclick="$('#emailClient').addClass('writingMode');"><i class="fa fa-reply"></i> Rispondi</button>
        <button class="btn btn-white btn-sm" title="Stampa" onclick="webClient.email.print();"><i class="fa fa-print"></i></button>
        <button class="btn btn-danger btn-sm" onclick="<?= $deleteFunction; ?>(<?= $r['id']; ?>);"><i class="fa fa-trash-o"></i></button>
    </div>

    <button class="btn btn-white btn-sm pull-left" style="margin-right: 10px;" title="Torna indietro" onclick="webClient.load.mailFolder();"><i class="fa fa-arrow-left"></i></button>
    <h2 style="margin-top: 2px;">
        <?= htmlentities($r['subject']); ?>
    </h2>
    <div class="mail-tools tooltip-demo">
        <h5>
            <span class="pull-right font-normal" title="<?= date('d/m/Y H:i', strtotime($r['mail_date'])); ?>"><?= (new \MSFramework\utils())->smartdate(strtotime($r['mail_date'])); ?></span>
            <span class="font-normal"><b>Da:</b> </span><?= $r['sender_email']; ?>
        </h5>
    </div>
</div>
<div class="mail-box">

    <div class="replyBox emailWriteContent">

        <div class="emailChoseTemplate">
            <h2>Seleziona un template</h2>
            <div class="choseTemplateList">
                <?php include('../includes/templateList.php'); ?>
            </div>
        </div>

        <div class="emailWriteBox" style="display: none;">
            <h2>
                <div class="btn-group pull-right">
                    <button data-toggle="dropdown" class="btn btn-sm btn-info dropdown-toggle">Cambia Template <i class="fa fa-angle-down"></i></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" data-id="0" class="importEmailtemplate">Nessun template</a></li>
                        <?php foreach((new \MSFramework\Newsletter\emails())->getTemplateDetails('', 'id, titolo') as $template) { ?>
                            <li><a href="#" data-id="<?= $template['id'] ?>" class="importEmailtemplate"><?= $template['titolo'] ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                Stai rispondendo a <small class="text-info"><?= $r['sender_email']; ?></small>
            </h2>

            <div id="emailReplyTextarea"></div>

            <div class="emailWritingActions">
                <button class="btn btn-primary btn-sm sendBtn" title="Invia risposta" onclick=""><i class="fa fa-send"></i> Invia</button>
                <button class="btn btn-danger btn-outline btn-sm undoBtn" title="Annulla risposta">Annulla</button>
            </div>
        </div>

    </div>

    <div class="mail-body">
        <iframe src="ajax/getEmail.php?id=<?= $r['id']; ?>&body"></iframe>
    </div>

    <?php if($mailAttachments) { ?>
        <div class="mail-attachment">
            <p><span><i class="fa fa-paperclip"></i> <?= count($mailAttachments); ?> allegat<?= (count($mailAttachments) === 1 ? 'o' : 'i'); ?> - </span> <a href="ajax/downloadAttachments.php?email=<?= $r['id']; ?>">Scarica tutti</a></p>
            <div class="attachment">
                <?php foreach($mailAttachments as $file) { ?>

                    <div class="file-box">
                        <div class="file">
                            <a href="ajax/downloadAttachments.php?id=<?= $file['id']; ?>">
                                <span class="corner"></span>

                                <?php if(!stristr($file['mime'], 'image/')) { ?>
                                    <div class="icon">
                                        <i class="fa fa-file"></i>
                                    </div>
                                <?php } else { ?>
                                    <div class="image">
                                        <img alt="image" class="img-fluid" src="data:<?= $file['mime']; ?>;base64,<?= $file['data']; ?>">
                                    </div>
                                <?php } ?>
                                <div class="file-name">
                                    <?= $file['file_name']; ?>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
        </div>
    <?php } ?>

    <div class="clearfix"></div>

</div>

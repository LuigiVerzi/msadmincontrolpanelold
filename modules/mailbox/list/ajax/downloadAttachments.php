<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$attachment_id = false;
$email_id = false;

if(isset($_GET['id'])) {
    $attachment_id = (int)$_GET['id'];
} else if(isset($_GET['email'])) {
    $email_id =  (int)$_GET['email'];
}

if($attachment_id) {
    $attachmentInfo = $MSFrameworkDatabase->getAssoc("SELECT * FROM mailbox__attachments WHERE id = :id", array(':id' => $attachment_id));
} else if($email_id) {
    $attachmentInfo = $MSFrameworkDatabase->getAssoc("SELECT * FROM mailbox__attachments WHERE email_id = :email_id", array(':email_id' => $email_id));
}

$attachment = array();
if(count($attachmentInfo) === 1) {
    $attachment = $attachmentInfo[0];
    $attachment['data'] = base64_decode($attachment['data']);
} else if(count($attachmentInfo) > 1) {
    $attachment = array();

    $zip = new \ZipArchive();
    $attachment['file_name'] = "allegati.zip";

    if ($zip->open(UPLOAD_TMP_FOR_DOMAIN . $attachment['file_name'], ZIPARCHIVE::CREATE)) {
        foreach($attachmentInfo as $s)  $zip->addFromString($s['file_name'], base64_decode($s['data']));
        $zip->close();
    }

    $attachment['data'] = file_get_contents(UPLOAD_TMP_FOR_DOMAIN . $attachment['file_name']);
    unlink(UPLOAD_TMP_FOR_DOMAIN . $attachment['file_name']);
}

if(!$attachment) {
    die('File non è stato trovato.');
}

header('Content-Description: File Transfer');
header('Content-Type: ' . $attachment['mime']);
header('Content-Disposition: attachment; filename='. $attachment['file_name']);
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . strlen($attachment['data']));
ob_clean();
flush();
echo $attachment['data'];
exit;
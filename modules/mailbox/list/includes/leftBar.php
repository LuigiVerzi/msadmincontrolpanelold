<div class="ibox ">
    <div class="ibox-content mailbox-content" id="mailSidebar">
        <div class="file-manager">
            <h2 class="title-divider" style="margin: -15px -15px 0; border-top: 0; background: white;">
                <button class="btn btn-block btn-primary compose-mail" onclick="webClient.load.mailCompose();">Scrivi email</button>
            </h2>
            <?php foreach($MSFrameworkMailBox->getMailBoxServers() as $SMTPDetail) { ?>
                <div class="serverDirectories" data-mail="<?= htmlentities($SMTPDetail['data']["username"]); ?>" data-serverName="<?= htmlentities($SMTPDetail['data']["name"]); ?>">
                    <div class="space-25"></div>
                    <h5><?= $SMTPDetail['data']['name']; ?> <small class="pull-right"><?= $SMTPDetail['data']["username"]; ?></small></h5>
                    <ul class="folder-list" style="padding: 0; margin: 0;">

                        <?php
                        $folders = $MSFrameworkMailBox->getFolders();
                        $customFolders = $MSFrameworkDatabase->getAssoc("SELECT folder FROM `mailbox__messages` WHERE server_id = :id GROUP BY folder", array(':id' => $SMTPDetail['id']));

                        foreach($customFolders as $cf) {
                            if(!in_array(strtoupper($cf['folder']), array_map('strtoupper', array_keys($folders)))) {
                                $folders[$cf['folder']] = array(
                                    'name' => $cf['folder'],
                                    'icon' => 'fa fa-cube'
                                );
                            }
                        }
                        ?>

                        <?php foreach($folders as $folderID => $folderInfo) { ?>
                            <li>
                                <a onclick="webClient.load.mailFolder('<?= $SMTPDetail['id']; ?>_<?= $folderID; ?>');" data-id="<?= $SMTPDetail['id']; ?>_<?= $folderID; ?>" class="changeFolderBtn">
                                    <i class="<?= $folderInfo['icon']; ?>"></i><span><?= $folderInfo['name']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
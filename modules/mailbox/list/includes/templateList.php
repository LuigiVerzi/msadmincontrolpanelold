<?php foreach((new \MSFramework\mailBox())->getTemplateDetails('', 'id, titolo,data_creazione') as $template) { ?>
    <div class="file-box">
        <div class="file">
            <a href="#" class="template_choose" data-id="<?= $template['id']; ?>">
                <span class="corner"></span>
                <div class="image">
                    <img alt="image" class="img-responsive" src="<?= UPLOAD_MAILBOX_TEMPLATES_FOR_DOMAIN_HTML; ?>thumb_<?= $template['id']; ?>.png">
                </div>
                <div class="file-name">
                    <?= $template['titolo'] ?>
                    <br>
                    <small>Creato: <?= (new \MSFramework\utils())->smartdate(strtotime($template['data_creazione'])); ?></small>
                </div>
            </a>
        </div>
    </div>
<?php } ?>

<div class="file-box">
    <div class="file">
        <a href="#" class="template_choose" data-id="0">
            <span class="corner"></span>
            <div class="icon">
                <i class="fa fa-plus"></i>
            </div>
            <div class="file-name">
                Crea nuova email
                <br>
                <small>Parti da un template vuoto</small>
            </div>
        </a>
    </div>
</div>

<div style="clear: both;"></div>
<?php
/**
 * MSAdminControlPanel
 * Date: 08/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$folderToDelete = array();
$folderToMove = array();

$emailInfo = $MSFrameworkDatabase->getAssoc("SELECT folder FROM mailbox__messages WHERE id = :id", array(':id' => $_POST['pID'][0]), true);

if(!is_numeric($_POST['pID'][0])) die();

$status = false;
if($emailInfo) {
    if ($emailInfo['folder'] === 'Trash') {
        $status = $MSFrameworkDatabase->deleteRow("mailbox__messages", "id", $_POST['pID']);
        $status = $MSFrameworkDatabase->deleteRow("mailbox__attachments", "email_id", $_POST['pID']);
    } else {
        $status = $MSFrameworkDatabase->pushToDB("UPDATE mailbox__messages SET folder = 'Trash' WHERE id IN(" . implode(',', $_POST['pID']) . ")");
    }
}

if($status) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


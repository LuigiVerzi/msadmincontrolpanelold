<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkMailBox = new \MSFramework\mailBox();
?>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="wrapper wrapper-content full-space">
            <div class="row">
                <div class="col-lg-3">
                    <? include('includes/leftBar.php'); ?>
                </div>
                <div class="col-lg-9 animated fadeInRight">

                    <div id="emailClient">

                        <div class="emailClientContent emailList">
                            <div class="mail-box-header">
                                <button class="btn btn-white btn-sm pull-right" onclick="webClient.email.refresh();" id="downloadFromServer" style="margin: 10px 0;"><i class="fa fa-refresh"></i> Aggiorna</button>
                                <h2 id="mail-folder-title">
                                    <span class="dirName">...</span><br>
                                    <small class="mailName">...</small>
                                </h2>
                            </div>
                            <div class="mail-box">
                                <table id="main_table_list" class="display table table-striped table-hover" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th width="35" style="width: 35px;" class="no-sort"></th>
                                        <th></th>
                                        <th></th>
                                        <th class="no-sort text-right"></th>
                                        <th class="is_data default-sort" data-sort="DESC"></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <div class="emailClientContent emailView" style="display: none;"></div>

                        <div class="emailClientContent emailCompose" style="display: none;">

                            <div class="mail-box-header">
                                <h2 style="margin-top: 2px;">
                                    Nuova email
                                </h2>
                            </div>

                            <form class="mail-box-details">
                                <div class="form-group">
                                    <label>Destinatari</label>
                                    <input type="text" class="form-control" id="emailComposeTo" placeholder="Email destinatario" value="">
                                </div>
                                <div class="form-group">
                                    <label>Oggetto</label>
                                    <input type="text" class="form-control" id="emailComposeSubject" placeholder="Oggetto" value="">
                                </div>
                                <div class="form-group">
                                    <label>Invia come</label>
                                    <select class="form-control" id="emailComposeSender">
                                        <option value="">Usa server di sistema</option>
                                        <?php foreach($MSFrameworkMailBox->getMailBoxServers() as $SMTPDetail) { ?>
                                            <option value="<?= $SMTPDetail['id']; ?>"><?= $SMTPDetail['data']['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </form>

                            <div class="mail-box">

                                <div class="emailWriteContent">

                                    <div class="emailChoseTemplate">
                                        <h2>Seleziona un template</h2>
                                        <div class="choseTemplateList"></div>
                                    </div>
                                    <div class="emailWriteBox" style="display: none;">
                                        <h2>
                                            <div class="btn-group pull-right">
                                                <button data-toggle="dropdown" class="btn btn-sm btn-info dropdown-toggle">Cambia Template <i class="fa fa-angle-down"></i></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" data-id="0" class="importEmailtemplate">Nessun template</a></li>
                                                    <?php foreach((new \MSFramework\Newsletter\emails())->getTemplateDetails('', 'id, titolo') as $template) { ?>
                                                        <li><a href="#" data-id="<?= $template['id'] ?>" class="importEmailtemplate"><?= $template['titolo'] ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                            Contenuto email
                                        </h2>

                                        <div id="emailComposeTextarea"></div>

                                        <div class="emailWritingActions">
                                            <button class="btn btn-primary btn-sm sendBtn" title="Invia email" onclick=""><i class="fa fa-send"></i> Invia</button>
                                            <button class="btn btn-danger btn-outline btn-sm undoBtn" title="Annulla risposta">Annulla</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    </div>
</div>

<textarea id="tpl_templateChooser" style="display: none;">
    <?php include('includes/templateList.php'); ?>
</textarea>

<style>
    #main_table_list_wrapper {
        margin: 0 -1px 5px;
        width: auto;
        max-width: none;
    }
    #main_table_list_wrapper .avatarLogo{
        object-fit: contain;
        background: #fafafa;
    }
    #main_table_list_wrapper > .row:last-child {
        background: #fafafa;
        margin: -10px 1px -5px;
        padding: 0 0 5px;
    }
</style>

<script>
    globalInitForm();
</script>
</body>
</html>
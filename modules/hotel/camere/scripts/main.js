$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('nome', 'slug', 'hotel_rooms', $('#record_id'));
    initOrak('images', 25, 500, 300, 200, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
    initTinyMCE( '#pageContentText');
}

function moduleSaveFunction() {
    services_ary = new Array();
    $('.service_checkbox:checked').each(function(k, v) {
        services_ary.push($(this).attr('id'));
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pPrezzo": $('#prezzo').val(),
            "pCategory": $('#category').val(),
            "pageContentText": tinymce.get('pageContentText').getContent(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pServicesAry": services_ary,
            "pSlug": $('#slug').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
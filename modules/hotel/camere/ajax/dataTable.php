<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM hotel_rooms") as $r) {

    $image = json_decode($r['gallery']);

    if(!empty($r['category'])) {
        $category = $MSFrameworkDatabase->getAssoc("SELECT * FROM hotel_rooms_cats WHERE id=" . (int)$r['category'])[0];
    } else {
        $category = array();
    }

    $array['data'][] = array(
        '<img src="' . UPLOAD_ROOMGALLERY_FOR_DOMAIN_HTML . "tn/" . $image[0] . '" alt="" height="auto" width="80">',
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        $MSFrameworki18n->getFieldValue($r['slug'], true),
        CURRENCY_SYMBOL . " " . $r['prezzo'],
        ($category ? $MSFrameworki18n->getFieldValue($category['nome'], true) : '<span class="text-muted">N/A</span>'),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

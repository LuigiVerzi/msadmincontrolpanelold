<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\Hotels\rooms())->getRoomDetails($_POST['pID']) as $post) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($post['gallery'], true));
}

if($MSFrameworkDatabase->deleteRow("hotel_rooms", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('ROOMGALLERY'))->unlink($images_to_delete);
    $MSFrameworkUrl->deleteRedirectsByReferences('hotel-camere', $_POST['pID']);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pPrezzo'] == "" || $_POST['pSlug'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "hotel_rooms", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

$_POST['pPrezzo'] = str_replace(",", ".", $_POST['pPrezzo']);
if(!strstr($_POST['pPrezzo'], ".")) {
    $_POST['pPrezzo'] .= ".00";
}

if(!is_numeric($_POST['pPrezzo'])) {
    echo json_encode(array("status" => "price_numeric"));
    die();
}

$uploader = new \MSFramework\uploads('ROOMGALLERY');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT gallery, nome, slug, content FROM hotel_rooms WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pageContentText'], 'oldValue' => $r_old_data['content']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "prezzo" => $_POST['pPrezzo'],
    "content" => $MSFrameworki18n->setFieldValue($r_old_data['content'], $_POST['pageContentText']),
    "category" => $_POST['pCategory'],
    "gallery" => json_encode($ary_files),
    "services" => json_encode($_POST['pServicesAry']),
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO hotel_rooms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE hotel_rooms SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['gallery'], true));
}

$ref_id = ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : $_POST['pID']);
$MSFrameworkUrl->makeRedirectIfNeeded('hotel-camere-' . $ref_id, (new \MSFramework\Hotels\rooms())->getURL($ref_id), ($db_action === 'update' ? (new \MSFramework\Hotels\rooms())->getURL($r_old_data) : ''));

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
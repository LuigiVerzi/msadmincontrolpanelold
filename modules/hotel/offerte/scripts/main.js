$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('nome', 'slug', 'hotel_offerte', $('#record_id'));
    initOrak('images', 25, 500, 300, 200, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);

    $('.input-group.date.scadenza').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
        startDate: new Date(),
    });
    initTinyMCE( '#pageContentText, #shortPageContentText');
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pTipologia": $('#offer_type').val(),
            "pNotti": $('#notti').val(),
            "pPrezzo": $('#prezzo').val(),
            "pScadenza": $('#scadenza').val(),
            "pageContentText": tinymce.get('pageContentText').getContent(),
            "shortPageContentText": tinymce.get('shortPageContentText').getContent(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pSlug": $('#slug').val(),
            "pSimpleBookingPkgId":  $('#simplebooking_pkgId').val(),
            "pAutoRenewPromo":  $('#auto_renew_promo').val(),
            "pAvaialableRooms": $('#offer_rooms').val(),
            "pActive": $('#is_active:checked').length
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data di scadenza non è valido. Impossibile continuare.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
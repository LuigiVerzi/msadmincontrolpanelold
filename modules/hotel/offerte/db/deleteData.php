<?php
/**
 * MSAdminControlPanel
 * Date: 08/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\Hotels\offers())->getOfferDetails($_POST['pID']) as $post) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($post['gallery'], true));
}

if($MSFrameworkDatabase->deleteRow("hotel_offerte", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('OFFERSGALLERY'))->unlink($images_to_delete);
    $MSFrameworkUrl->deleteRedirectsByReferences('hotel-offerte', $_POST['pID']);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


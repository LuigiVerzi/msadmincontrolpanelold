<?php
/**
 * MSAdminControlPanel
 * Date: 08/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pTipologia'] == "" || $_POST['pNotti'] == "" || $_POST['pPrezzo'] == "" || $_POST['pSlug'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "hotel_offerte", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

$ts_scadenza = "";
if($_POST['pScadenza'] != "") {
    $ary_scadenza = explode("/", $_POST['pScadenza']);
    $ts_scadenza = mktime(23, 59, 59, $ary_scadenza[1], $ary_scadenza[0], $ary_scadenza[2]);
    if(!$ts_scadenza) {
        echo json_encode(array("status" => "expire_not_valid"));
        die();
    }
}

$uploader = new \MSFramework\uploads('OFFERSGALLERY');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT gallery, nome, slug, content, short_content FROM hotel_offerte WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pageContentText'], 'oldValue' => $r_old_data['content']),
        array('currentValue' => $_POST['shortPageContentText'], 'oldValue' => $r_old_data['short_content']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "type" => $_POST['pTipologia'],
    "notti" => $_POST['pNotti'],
    "prezzo" => $_POST['pPrezzo'],
    "scadenza" => $ts_scadenza,
    "content" => $MSFrameworki18n->setFieldValue($r_old_data['content'], $_POST['pageContentText']),
    "short_content" => $MSFrameworki18n->setFieldValue($r_old_data['short_content'], $_POST['shortPageContentText']),
    "gallery" => json_encode($ary_files),
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
    "simplebooking_pkgId" => $_POST['pSimpleBookingPkgId'],
    "auto_renew_promo" => $_POST['pAutoRenewPromo'],
    "avaialable_rooms" => json_encode($_POST['pAvaialableRooms']),
    "is_active" => $_POST['pActive']
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO hotel_offerte ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE hotel_offerte SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['gallery'], true));
}

$ref_id = ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : $_POST['pID']);
$MSFrameworkUrl->makeRedirectIfNeeded('hotel-offerte-' . $ref_id, (new \MSFramework\Hotels\offers())->getURL($ref_id), ($db_action === 'update' ? (new \MSFramework\Hotels\offers())->getURL($r_old_data) : ''));

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 08/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM hotel_offerte WHERE id = :id", array(":id" => $_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Offerta</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome Offerta*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Slug*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                            <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-3">
                                    <label>Tipologia*</label>
                                    <select id="offer_type" name="offer_type" class="form-control required">
                                        <option value=""></option>
                                        <?php
                                        foreach((new \MSFramework\Hotels\offers())->getTypes() as $typeK => $typeV) {
                                            ?>
                                            <option value="<?php echo $typeK ?>" <?php if($r['type'] == $typeK) { echo "selected"; } ?>><?php echo $typeV ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php if($r['is_active'] == "1" || $_GET['id'] == "") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Mostra sul sito</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">

                                <div class="col-sm-3">
                                    <label>Notti*</label>
                                    <input id="notti" name="notti" type="text" class="form-control required" value="<?php echo htmlentities($r['notti']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Prezzo o Sconto*</label>
                                    <input id="prezzo" name="prezzo" type="text" class="form-control required" value="<?php echo htmlentities($r['prezzo']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Scadenza</label>
                                    <div class="input-group date scadenza">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="scadenza" value="<?php echo ($r['scadenza'] != '') ? date("d/m/Y", $r['scadenza']) : '' ?>">
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Ripeti promozione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imposta questo valore se desideri che la promozione si rinnovi automaticamente alla scadenza. Il valore impostato rappresenta il numero di giorni oltre la scadenza per il quale la promozione resterà attiva. La promozione si rinnoverà all'infinito fin quanto questo campo sarà valorizzato. Lasciare vuoto o su '0' per nessuna ripetizione."></i></span>
                                    <input id="auto_renew_promo" name="auto_renew_promo" type="number" class="form-control" value="<?php echo htmlentities($r['auto_renew_promo']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">

                                <div class="col-sm-3">
                                    <label>Limita Camere</label>
                                    <select id="offer_rooms" name="offer_rooms" class="form-control" multiple>
                                        <option value=""></option>
                                        <?php foreach((new \MSFramework\Hotels\rooms())->getRoomDetails() as $roomID => $roomValue) { ?>
                                            <option value="<?php echo $roomID ?>" <?php if(in_array($roomID, json_decode($r['avaialable_rooms']))) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($roomValue['nome']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-5">
                                    <label>Descrizione breve</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['short_content']) ?>"></i></span>
                                    <textarea id="shortPageContentText" name="shortPageContentText"><?php echo $MSFrameworki18n->getFieldValue($r['short_content']) ?></textarea>
                                </div>

                                <div class="col-sm-7">
                                    <label>Descrizione estesa</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['content']) ?>"></i></span>
                                    <textarea id="pageContentText" name="pageContentText"><?php echo $MSFrameworki18n->getFieldValue($r['content']) ?></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Gallery</h1>
                        <fieldset>
                            <h2 class="title-divider">Immagini</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    (new \MSFramework\uploads('OFFERSGALLERY'))->initUploaderHTML("images", $r['gallery']);
                                    ?>
                                </div>
                            </div>

                        </fieldset>

                        <h1>Integrazione</h1>
                        <fieldset>
                            <h2 class="title-divider">SimpleBooking</h2>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>ID Offerta (pkgId)</label>
                                    <input id="simplebooking_pkgId" name="simplebooking_pkgId" type="number" class="form-control" value="<?= $r['simplebooking_pkgId']; ?>">
                                </div>
                            </div>
                            <hr>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 24/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM hotel_rooms_cats") as $r) {
    $array['data'][] = array(
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

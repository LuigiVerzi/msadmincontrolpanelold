<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if($_GET['id'] != "") {
    $notification_info = (new \MSFramework\notifications())->getAvailNotifications($_GET['id']);
}

if(!is_array($notification_info) || $_GET['id'] == "") {
    header("location: index.php");
    die();
}

$shortcodes_globali = (new \MSFramework\emails())->getCommonShortcodes();
$r = (new \MSFramework\notifications())->getNotificationDetails($_GET['id'])[$_GET['id']];
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Impostazioni Email</h1>
                        <?php
                        $email_settings = json_decode($r['email_settings'], true);
                        ?>
                        <fieldset>
                            <h2 class="title-divider">Preferenze <?= $notification_info['name']; ?></h2>
                            <div class="row m-t-md">
                                <div class="col-sm-4">
                                    <label>Oggetto</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($email_settings['subject']) ?>"></i></span>
                                    <input id="subject" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($email_settings['subject'])) ?>">
                                </div>

                                <div class="col-sm-4">
                                    <label>Indirizzo Reply To</label>
                                    <input id="reply_to_addr" type="text" class="form-control" value="<?php echo $email_settings['reply_to_addr'] ?>">
                                </div>

                                <div class="col-sm-4">
                                    <label>Nome Reply To</label>
                                    <input id="reply_to_name" type="text" class="form-control" value="<?php echo $email_settings['reply_to_name'] ?>">
                                </div>
                            </div>

                            <h2 class="title-with-button">
                                Contenuto
                            </h2>

                            <?= (new \MSFramework\utils())->generateShortcodesTable(array_merge($notification_info['shortcodes'][0], (new \MSFramework\notifications())->getShortcodeList()[0]), true); ?>

                            <div class="row m-t-md custom_layout_container">
                                <div class="col-sm-12 custom_layout_container_editor">
                                    <label>Versione HTML</label>
                                    <div id="versione_html_personalizzata" name="versione_html_personalizzata"><?php echo $email_settings['custom_html'] ?></div>
                                </div>
                            </div>

                            <div class="row m-t-md">
                                <div class="col-sm-12">
                                    <label>Allegati Email</label>
                                    <div id="attachment_container">
                                        <?php
                                        (new \MSFramework\uploads('NOTIFICATIONS'))->initUploaderHTML("mainImages", $email_settings['attachments']);
                                        ?>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <h1>Impostazioni SMS</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12">
                                    Presto disponibile
                                </div>
                            </div>
                        </fieldset>

                        <h1>Preferenze</h1>
                        <fieldset>
                            <h2 class="title-divider">Generali</h2>
                            <div class="row">
                                <?php
                                $is_active_check = "checked";
                                if($r['is_active'] === "0" && $_GET['id'] != "") {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>Stato della notifica</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deselezionando questa voce la notifica non sarà più inviata agli utenti"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attiva</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <?php if(file_exists('ajax/tabs/' . $_GET['id'] . ".php")) { ?>
                            <h2 class="title-divider">Specifiche</h2>
                            <?php
                            $specific_settings = json_decode($r['specific_settings'], true);
                            include('ajax/tabs/' . $_GET['id'] . ".php");
                            ?>
                            <?php } ?>
                        </fieldset>

                        <h1 class="tab-right"> <i class="fa fa-history"></i> Cronologia degli Invii</h1>
                        <fieldset class="histories">
                            <h2 class="title-divider">Cronologia degli Invii</h2>
                            <table id="notification_histories" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th class="default-sort" data-sort="DESC" style="width: 150px;" width="150">Data Invio</th>
                                    <th>Utente</th>
                                    <th class="no-sort" width="60" style="width: 60px !important;"></th>
                                </tr>
                                </thead>
                            </table>
                        </fieldset>


                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    window.template_shortcodes = {custom: <?= json_encode(array_merge(array_keys($notification_info['shortcodes'][0]), array_keys((new \MSFramework\notifications())->getShortcodeList()[0]))); ?>, global: <?= json_encode($shortcodes_globali[0]); ?>};

    globalInitForm();
</script>
</body>
</html>
function initTileClick() {
    $('.notification_box').on('click', function() {
        new_tab = window.maiuscIsPressed;

        sel_id = $(this).data('notification-id');
        if(typeof(new_tab) === 'undefined') new_tab = false;

        if(new_tab) {
            var win = window.open('edit.php?id=' + sel_id, '_blank');
            win.focus();
        } else {
            window.location.href = 'edit.php?id=' + sel_id;
        }
    })
}

function initForm() {
    initClassicTabsEditForm();

    $('#deleteNotification').on('click', function() {
        original_save_value = $('#deleteNotification').html();
        $('#deleteNotification').html($('#deleteNotification').attr('checking_txt')).addClass('disabled').attr('disabled', 'disabled');
        deleteRecord($('#record_id').val(), true);
        $('#deleteNotification').html(original_save_value).removeClass('disabled').attr('disabled', false);
    });

    html_editor = initEmailEditor('#versione_html_personalizzata', 'NOTIFICATIONS', window.template_shortcodes);
    attachShortcodesToTextarea($('#versione_txt_personalizzata'), window.template_shortcodes);

    $('.btn_shortcodes').on('click', function() {
        $('#shortcodes_alert').toggle();
    });

    initOrak('mainImages', 10, 0, 0, 100, getOrakImagesToPreattach('mainImages'), false, ['image/jpeg', 'image/png', 'application/pdf']);

    /* CRONOLOGIA DEGLI INVII */
    if($('.histories').length) {

        var datatableFormat = function (d) {
            return '<table cellpadding="5" cellspacing="0" border="0" style="width: 100%; border: 1px solid #ececec;">' +
                '<tr style="background: white;">' +
                '<td style="padding: 15px;">' + d[3] + '</td>' +
                '</tr>' +
                '</table>'
        };

        var form_submission_table = loadStandardDataTable('notification_histories', true, {ajax: 'ajax/histories/dataTable.php?notification_id=' + $('#record_id').val()}, false);
        $('#notification_histories tbody').on('click', 'td', function () {
            var tr = $(this).closest('tr');
            var row = form_submission_table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(datatableFormat(row.data())).show();
                tr.addClass('shown');
            }
        });
    }
}

function moduleSaveFunction() {
    specific = new Object();

    $('.specific_field').each(function () {
        specific[$(this).attr('name')] = $(this).val();
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pIsActive": $('#is_active:checked').length,
            "emailSettings": {
                "subject" : $('#subject').val(),
                "custom_html" :  html_editor.getContentHtml(),
                "custom_txt" : $('#versione_txt_personalizzata').val(),
                "reply_to_addr" : $('#reply_to_addr').val(),
                "reply_to_name" : $('#reply_to_name').val(),
                "pImagesAry" : composeOrakImagesToSave('mainImages'),
            },
            "pSpecific" : specific,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
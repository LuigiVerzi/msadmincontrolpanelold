<?php
/**
 * MSAdminControlPanel
 * Date: 02/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\notifications())->getNotificationDetails($_POST['pID']) as $r) {
    $email_settings = json_decode($r['email_settings'], true);
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($email_settings['attachments'], true));
}

if($MSFrameworkDatabase->deleteRow("notifications", "type", $_POST['pID'])) {
    (new \MSFramework\uploads('NOTIFICATIONS'))->unlink($images_to_delete);
    $MSFrameworkDatabase->deleteRow("notifications__histories", "notification", $_POST['pID']);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


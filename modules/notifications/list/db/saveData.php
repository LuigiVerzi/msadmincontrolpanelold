<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;

if(trim($_POST['pID']) == "" || $_POST['emailSettings']['subject'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(!(new \MSFramework\notifications())->checkExists($_POST['pID'])) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

$uploader = new \MSFramework\uploads('NOTIFICATIONS');
$ary_files = $uploader->prepareForSave($_POST['emailSettings']['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT email_settings FROM notifications WHERE type = :type", array(":type" => $_POST['pID']), true);

    $old_mail_settings = json_decode($r_old_data['email_settings'], true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['emailSettings']['subject'], 'oldValue' => $old_mail_settings['subject']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}


// Creo la versione testuale estrapolando il testo dall'HTML
$html = new \Html2Text\Html2Text($_POST['emailSettings']['custom_html']);
$versione_testuale =  $html->getText();

$array_to_save = array(
    "email_settings" => json_encode(array(
        "subject" => $MSFrameworki18n->setFieldValue($old_mail_settings['subject'], $_POST['emailSettings']['subject']),
        "custom_html" => $_POST['emailSettings']['custom_html'],
        "custom_txt" => $versione_testuale,
        "reply_to_addr" => $_POST['emailSettings']['reply_to_addr'],
        "reply_to_name" => $_POST['emailSettings']['reply_to_name'],
        "attachments" => json_encode($ary_files),
    )),
    "is_active" => $_POST['pIsActive'],
    "specific_settings" => json_encode($_POST['pSpecific']),
    "type" => $_POST['pID'],
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO notifications ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE notifications SET $stringForDB[1] WHERE type = :mytype", array_merge(array(":mytype" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($old_mail_settings['attachments'], true));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
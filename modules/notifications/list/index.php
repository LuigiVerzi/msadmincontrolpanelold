<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkNotifications = new \MSFramework\notifications();

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">

            </div>
        </div>

        <div class="wrapper wrapper-content">

            <?php
            $notifications_settings = $MSFrameworkCMS->getCMSData('notifications_settings');
            if($notifications_settings['is_active'] === "0") {
            ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-danger">
                            Le notifiche sono state disabilitate globalmente all'interno delle preferenze. Nessuna delle notifiche elencate di seguito verrà inviata agli utenti, indipendentemente dal loro stato individuale.
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>

            <div class="row">
                <?php
                foreach($MSFrameworkNotifications->getAvailNotifications() as $notificationK => $notification) {

                    $notification_sent_count = '0';
                    if($MSFrameworkNotifications->checkIsActive($notificationK)) {
                        $notification_sent_count = $MSFrameworkNotifications->countSentNotifications($notificationK);
                        $tile_bg = "active";
                    } else {
                        if($MSFrameworkNotifications->checkExists($notificationK)) {
                            $tile_bg = "paused";
                        } else {
                            $tile_bg = "";
                        }
                    }
                ?>
                    <div class="col-lg-3 notification_box" style="cursor: pointer;" data-notification-id="<?= $notificationK ?>">
                        <div class="widget <?= $tile_bg; ?> p-md text-center" style="height: 225px !important; margin-top: 5%;">
                            <?php if($tile_bg == 'active') { ?>
                                <span class="count_label">Inviata <?= $notification_sent_count; ?> volt<?= ($notification_sent_count == 1 ? 'a' : 'e'); ?></span>
                            <?php } ?>
                            <div>
                                <i class="fa <?= $notification['fa-icon'] ?> fa-3x"></i>
                                <h1 class="m-b-md m-t-md"> <?= $notification['name'] ?></h1>
                                <h4 style="font-weight: normal;">
                                    <?= $notification['descr'] ?>
                                </h4>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    initTileClick();
</script>
</body>
</html>
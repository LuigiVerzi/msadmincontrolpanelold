<?php
/**
 * MSAdminControlPanel
 * Date: 14/12/2018
 */ ?>

<div class="row m-t-md">
    <div class="col-sm-4">
        <label>Giorni da aspettare</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imposta il numero di giorni da aspettare (dopo l'appuntamento) prima di inviare la mail"></i></span>
        <input id="wait_days" name="wait_days" type="number" placeholder="1" class="specific_field form-control" value="<?= $specific_settings['wait_days'] ?>">
    </div>
</div>
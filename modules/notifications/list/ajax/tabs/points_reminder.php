<?php
/**
 * MSAdminControlPanel
 * Date: 14/12/2018
 */ ?>

<div class="row m-t-md">
    <div class="col-sm-4">
        <label>Invia al raggiungimento di <small>(punti)</small> </label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Invia l'email di promemoria al raggiungimento dei punti"></i></span>
        <input id="points" name="points" type="number" placeholder="1" class="specific_field form-control" value="<?= $specific_settings['points'] ?>">
    </div>
</div>
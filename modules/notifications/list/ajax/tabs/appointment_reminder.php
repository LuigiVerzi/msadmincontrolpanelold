<?php
/**
 * MSAdminControlPanel
 * Date: 14/12/2018
 */ ?>

<div class="row m-t-md">
    <div class="col-sm-4">
        <label>Giorni di anticipo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imposta il quanti giorni prima rispetto all'appuntamento va inviato il promemoria"></i></span>
        <input id="days_before" name="days_before" type="number" placeholder="1" class="specific_field form-control" value="<?= $specific_settings['days_before'] ?>">
    </div>
</div>
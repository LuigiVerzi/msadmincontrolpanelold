<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();

$notification_id = $_GET['notification_id'];

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array(
        'db' => 'sent_date',
        'dt' => 0,
        'formatter' => function( $d, $row ) {
            return date('d/m/Y H:i', strtotime($d)) . '<br><small style="display: block; line-height: 1;">' . (new \MSFramework\utils())->smartdate(strtotime($d)) . '</small>';
        }
    ),
    array(
        'db' => 'user_id',
        'dt' => 1,
        'formatter' => function($d, $row) {
            Global $MSFrameworkCustomers;

            $r = $MSFrameworkCustomers->getCustomerDataFromDB($d);

            if($r) {
                $info = array();
                if (!empty($r['email'])) {
                    $info[] = $r['email'];
                }
                if (!empty($r['telefono_casa'])) {
                    $info[] = $r['telefono_casa'];
                }
                if (!empty($r['telefono_cellulare'])) {
                    $info[] = $r['telefono_cellulare'];
                }

                $return_html = '';

                $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
                if (json_decode($r['avatar'])) {
                    $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
                }

                $return_html .= '<div style="position: relative; padding-left: 65px;">';
                $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
                $return_html .= '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
                $return_html .= '</div>';
            } else {
                $return_html = '<span class="text-muted">Cliente non trovato</span>';
            }

            return $return_html;
        }
    ),
    array(
        'db' => 'id',
        'dt' => 2,
        'formatter' => function( $d, $row ) {
            return '<a href="#" class="btn btn-default btn-block btn-sm">Dettagli</a>';
        }
    ),
    array(
        'db' => 'email_data',
        'dt' => 3,
        'formatter' => function( $d, $row ) {
            $email_data = json_decode($d, true);

            $email_name_replace = array(
                'recipient' => "Destinatario",
                'reply_to' => "Reply To",
                'subject' => 'Oggetto',
                'body' => 'Contenuto',
                'txt_body' => 'Version Testuale'
            );

            $form_html_values = array();
            $first = true;
            foreach($email_data as $field_name => $field_value) {
                $value = (is_array($field_value) ? explode(', ', $field_value) : $field_value);
                if(!empty($value))
                {
                    $form_html_values[] = '<h2 style="' . ($first ? 'margin-top: -16px;' : '') . '">' . (isset($email_name_replace[$field_name]) ? $email_name_replace[$field_name] : ucfirst($field_name)) . '</h2><p>' . $value . '</p>';
                    $first = false;
                }
            }

            return implode('', $form_html_values);
        }
    ),
);


echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'notifications__histories', 'id', $columns, null, 'notification = ' . $MSFrameworkDatabase->quote($notification_id))
);

<?php
/**
 * MSAdminControlPanel
 * Date: 14/12/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'notifications_settings'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkCMS->getCMSData('notifications_settings');
}


$array_to_save = array(
    "value" => json_encode(array(
        "is_active" => $_POST['pIsActive'],
    )),
    "type" => "notifications_settings"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'notifications_settings'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
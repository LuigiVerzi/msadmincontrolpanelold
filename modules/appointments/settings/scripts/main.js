function initForm() {
    initClassicTabsEditForm();

    $('#dynamic_end_time').on('ifChanged', function(event){
        if($('#dynamic_end_time:checked').length == 1) {
            $('#dynamic_end_default_value_block').show();
        } else {
            $('#dynamic_end_default_value_block').hide();
        }
    });
}


function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pReverseAxis": $('#reverse_timeline_axis:checked').length,
            "pLimitWorkingHours": $('#limit_working_hours:checked').length,
            "pCompactView": $('#compact_view:checked').length,
            "pCalendarStartDay": $('#calendar_start_day').val(),
            "pDynEnd" : {
                'enable' : $('#dynamic_end_time:checked').length,
                'empty_default' : $('#dynamic_end_default_value').val(),
            }
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
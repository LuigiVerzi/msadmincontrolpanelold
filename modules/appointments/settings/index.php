<?php
/**
 * MSAdminControlPanel
 * Date: 03/06/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('appointments_settings');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Impostazioni Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La posizione degli orari e degli operatori sarà invertita"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="reverse_timeline_axis" <?php if($r['reverse_timeline_axis'] == "1") { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Inverti assi timeline</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Non sarà possibile inserire appuntamenti al di fuori delle fasce orarie definite nel modulo 'Esercizio/Negozio -> Orari di Lavoro'"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="limit_working_hours" <?php if($r['limit_working_hours'] == "1") { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Limita orari</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Gli orari mostrati sul calendario saranno ridotti al minimo. Gli orari di chiusura o di riposo non saranno visualizzati, quando possibile."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="compact_view" <?php if($r['compact_view'] == "1") { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Vista compatta</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>I calendari iniziano da</label>
                                    <select id="calendar_start_day" class="form-control">
                                        <option value="monday" <?php if($r['calendar_start_day'] == "monday") { echo "selected"; } ?>>Lunedì</option>
                                        <option value="sunday" <?php if($r['calendar_start_day'] == "sunday") { echo "selected"; } ?>>Domenica</option>
                                        <option value="today" <?php if($r['calendar_start_day'] == "today") { echo "selected"; } ?>>Giorno Corrente</option>
                                    </select>
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="L'orario di fine dell'appuntamento sarà calcolato dinamicamente in base ai servizi selezionati. L'orario è sempre e comunque modificabile manualmente."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="dynamic_end_time" <?php if($r['dynamic_end']['enable'] == "1") { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Orario di fine dinamico</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3" id="dynamic_end_default_value_block" style="display: <?php echo ($r['dynamic_end']['enable'] == "1") ? 'block' : 'none' ?>">
                                    <label>Valore di default (minuti)</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Per i servizi senza una durata specifica, sarà applicato di default il valore impostato in questa casella"></i></span>
                                    <input type="number" class="form-control" id="dynamic_end_default_value" value="<?php echo $r['dynamic_end']['empty_default'] ?>" placeholder="0"/>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
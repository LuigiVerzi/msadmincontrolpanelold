<?php
/**
 * MSAdminControlPanel
 * Date: 01/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'appointments_settings'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

$array_to_save = array(
    "value" => json_encode(array(
        "reverse_timeline_axis" => $_POST['pReverseAxis'],
        "limit_working_hours" => $_POST['pLimitWorkingHours'],
        "compact_view" => $_POST['pCompactView'],
        "calendar_start_day" => $_POST['pCalendarStartDay'],
        "dynamic_end" => $_POST['pDynEnd'],
    )),
    "type" => "appointments_settings"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'appointments_settings'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 22/11/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r_appointments_settings = $MSFrameworkCMS->getCMSData('appointments_settings');
?>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg" style="display: flex;flex-flow: column;">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
        </div>

        <input type="hidden" id="force_view" value="<?= $_GET['force_view'] ?>" />
        <input type="hidden" id="reverse_timeline_axis" value="<?= $r_appointments_settings['reverse_timeline_axis'] ?>" />
        <input type="hidden" id="limit_working_hours" value="<?= $r_appointments_settings['limit_working_hours'] ?>" />
        <input type="hidden" id="compact_view" value="<?= $r_appointments_settings['compact_view'] ?>" />
        <input type="hidden" id="calendar_start_day" value="<?= $r_appointments_settings['calendar_start_day'] ?>" />
        <input type="hidden" id="dynamic_end_enable" value="<?= $r_appointments_settings['dynamic_end']['enable'] ?>" />

        <div id="customer_details_modal" style="display: none;">
            <div class="ibox-content" style="background: #fafafa; padding: 30px;">

                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="title-divider">Dati Cliente</h2>
                        <div class="info_cliente">
                            <input name="id" type="hidden" value="">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome*</label>
                                    <input id="nome" name="nome" type="text" class="form-control required">
                                </div>
                                <div class="col-sm-6">
                                    <label>Cognome*</label>
                                    <input id="cognome" name="cognome" type="text" class="form-control required">
                                </div>
                                <div class="col-sm-6">
                                    <label>Sesso</label>
                                    <select id="sesso" name="sesso" class="form-control">
                                        <option value="">Non specificato</option>
                                        <option value="m">Maschio</option>
                                        <option value="f">Femmina</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label>Data di Nascita</label>
                                    <div class="input-group date scadenza">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="data_nascita" id="data_nascita">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label>Indirizzo</label>
                                    <input id="indirizzo" name="indirizzo" type="text" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <label>Città</label>
                                    <input id="citta" name="citta" type="text" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <label>Provincia</label>
                                    <input id="provincia" name="provincia" type="text" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <label>CAP</label>
                                    <input id="cap" name="cap" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Email</label>
                                    <input id="email" name="email" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Telefono Casa</label>
                                    <input id="telefono_casa" name="telefono_casa" type="text" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <label>Telefono Cellulare</label>
                                    <input id="telefono_cellulare" name="telefono_cellulare" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <h2 class="title-divider">Dati Fatturazione</h2>
                        <div class="info_fatturazione">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Ragione Sociale</label>
                                    <input id="ragione_sociale" placeholder="Richiesto per Fatturazione" name="ragione_sociale" type="text" class="form-control fatturazione">
                                </div>

                                <div class="col-sm-6">
                                    <label>Partita IVA</label>
                                    <input id="piva" name="piva" placeholder="Richiesto per Fatturazione" type="text" class="form-control fatturazione" "="">
                                </div>

                                <div class="col-sm-6">
                                    <label>Codice Fiscale</label>
                                    <input id="cf" name="cf" type="text" placeholder="Richiesto se P.IVA mancante" class="form-control fatturazione">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Indirizzo</label>
                                    <input id="indirizzo_fatturazione" placeholder="Richiesto per Fatturazione" name="indirizzo" type="text" class="form-control fatturazione">
                                </div>

                                <div class="col-sm-12">
                                    <label>Città</label>
                                    <input id="citta_fatturazione" placeholder="Richiesto per Fatturazione" name="citta" type="text" class="form-control fatturazione">
                                </div>

                                <div class="col-sm-6">
                                    <label>Provincia</label>
                                    <input id="provincia_fatturazione" placeholder="Richiesto per Fatturazione" name="provincia" type="text" class="form-control fatturazione">
                                </div>

                                <div class="col-sm-6">
                                    <label>CAP</label>
                                    <input id="cap_fatturazione" placeholder="Richiesto per Fatturazione" name="cap" type="text" class="form-control fatturazione">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="clear: both;"></div>
            </div>
        </div>

        <div class="wrapper wrapper-content" style="flex-grow: 1; display: flex; padding: 0; margin: 0 -15px; padding-bottom: 40px;">
            <div id="scheduler" class="dhx_cal_container" style="width: 100%; flex-grow: 1; ">
                <div class="dhx_cal_navline">
                    <div class="dhx_cal_prev_button">&nbsp;</div>
                    <div class="dhx_cal_next_button">&nbsp;</div>
                    <div class="dhx_cal_today_button"></div>
                    <div class="dhx_cal_date"></div>

                    <div class="dhx_minical_icon" id="dhx_minical_icon" onclick="showMiniCal()" style="left: 390px !important;">&nbsp;</div>

                    <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
                    <div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
                    <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>

                    <?php
                    if($r_appointments_settings['reverse_timeline_axis'] == "1") {
                    ?>
                        <div class="dhx_cal_tab dhx_cal_tab_standalone" name="timeline_tab" style="right:294px;"></div>
                    <?php
                    } else {
                    ?>
                        <div class="dhx_cal_tab dhx_cal_tab_standalone" name="unit_tab" style="right:294px;"></div>
                    <?php
                    }
                    ?>


                </div>
                <div class="dhx_cal_header"></div>
                <div class="dhx_cal_data"></div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 05/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$services = $_POST['pServices'];
$customer = $_POST['pCustomerID'];

if(count(array_filter($services)) == 0) {
    echo json_encode(
        array("timeData" => array("sum" => 0)),
        array("residuoSedute" => array("output" => ""))
    );

    die();
}

$r_appointments_settings = $MSFrameworkCMS->getCMSData('appointments_settings');
$seduteObj = new \MSFramework\BeautyCenter\sedute();

$to_return = array();
$to_return['residuoSedute']['output'] = "";

$servicesObj = new \MSFramework\services();
foreach($services as $service) {
    $service_details = $servicesObj->getServiceDetails($service, 'extra_fields, nome')[$service];
    $extra_fields = $service_details['extra_fields'];

    $durata = ($extra_fields['durata'] == "" ? ($r_appointments_settings['dynamic_end']['empty_default'] == "" ? 0 : $r_appointments_settings['dynamic_end']['empty_default']) : $extra_fields['durata']);
    $to_return['timeData']['services'][$service] = $durata;
    $to_return['timeData']['sum'] += $durata;

    if($customer != "") {
        $sedute = $seduteObj->getCustomerSessionsBalance($customer, 'service_'.$service);
        if($sedute != 0) {
            $to_return['residuoSedute']['services'][$service] = $sedute;
            $to_return['residuoSedute']['sum'] += $sedute;
            $to_return['residuoSedute']['output'] .= $sedute . " " . $MSFrameworki18n->getFieldValue($service_details['nome']) . "<br />";
        }
    }
}

echo json_encode($to_return);
die();
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 07/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$customers = new \MSFramework\customers();

$bilancio_attuale = (float)$customers->getCustomerBalance($_GET['id']);
$cust_data = $customers->getCustomerDataFromDB($_GET['id'], "nome, cognome, data_nascita, email, telefono_casa, telefono_cellulare, indirizzo, citta, provincia, cap, sesso, dati_fatturazione");

$cust_data['data_nascita'] = ($cust_data['data_nascita'] != "" ? $cust_data['data_nascita'] = date("d/m/Y", $cust_data['data_nascita']) : "");

$fatturazione = json_decode($cust_data['dati_fatturazione'], true);
$cust_data['dati_fatturazione'] = (is_array($fatturazione) ? $fatturazione : array());

$telefono_str = "";
$telefono_str .= ($cust_data['telefono_casa'] != "" ? "Tel. casa: " . $cust_data['telefono_casa'] . ", " : "");
$telefono_str .= ($cust_data['telefono_cellulare'] != "" ? "Tel. cellulare: " . $cust_data['telefono_cellulare'] . ", " : "");
if($telefono_str != "") {
    $telefono_str = " (" . substr($telefono_str, 0, -2) . ")";
}

echo json_encode(array_merge(array("balance" => $bilancio_attuale, "tel_str" => $telefono_str), $cust_data));
?>

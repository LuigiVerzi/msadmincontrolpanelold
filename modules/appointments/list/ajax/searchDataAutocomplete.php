<?php
/**
 * MSAdminControlPanel
 * Date: 10/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$limit = "";
if(strlen($_GET['term']) <= 5) {
    $limit = " LIMIT 10";
}

$ary = array();
if($_GET['t'] == "customer") {
    $where_res = $MSFrameworkDatabase->composeSearch($_GET['term'], array("nome", "cognome"));

    foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, telefono_casa, telefono_cellulare FROM customers WHERE id != '' $where_res[0] ORDER BY cognome, nome $limit", $where_res[1]) as $r) {
        $telefono_str = "";
        $telefono_str .= ($r['telefono_casa'] != "" ? "Tel. casa: " . $r['telefono_casa'] . ", " : "");
        $telefono_str .= ($r['telefono_cellulare'] != "" ? "Tel. cellulare: " . $r['telefono_cellulare'] . ", " : "");
        if($telefono_str != "") {
            $telefono_str = " (" . substr($telefono_str, 0, -2) . ")";
        }

        $ary[] = array("value" => $r['cognome'] . " " . $r['nome'] . $telefono_str, "label" => $r['cognome'] . " " . $r['nome'] . $telefono_str, "id" => $r['id']);
    }
}

echo json_encode($ary);
?>

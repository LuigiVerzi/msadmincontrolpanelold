<?php
/**
 * MSAdminControlPanel
 * Date: 29/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$same_db_string_ary = array("", array());
$sel_services = explode(",", $_POST['servizi']);

if($_POST['id'] != "-1") {
    $user = (new \MSFramework\users())->getUserDataFromDB($_POST['id'], "limit_services");
    $limit_services = json_decode($user['limit_services'], true);
}

if($_POST['id'] != "-1" && ($limit_services['type'] != "0" && $limit_services['type'] != "")) {
    $limit_values = $limit_services['values'];

    if($limit_services['type'] == "1") {
        $same_db_string_ary = $MSFrameworkDatabase->composeSameDBFieldString($limit_values, "OR", "id");
    } else if($limit_services['type'] == "2") {
        $same_db_string_ary = $MSFrameworkDatabase->composeSameDBFieldString($limit_values, "AND", "id", "!=");
    }

    $same_db_string_ary[0] = "AND (" . $same_db_string_ary[0] . ")";
}

$html = "";
foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome FROM servizi WHERE id != '' $same_db_string_ary[0]", $same_db_string_ary[1]) as $r) {
    $html .= "<option value='" . $r['id'] . "' " . (in_array($r['id'], $sel_services) ? "selected" : "") . ">" . $MSFrameworki18n->getFieldValue($r['nome']) . "</option>";
}

echo $html;
?>

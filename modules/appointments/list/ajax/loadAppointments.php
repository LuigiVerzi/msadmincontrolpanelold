<?php
/**
 * MSAdminControlPanel
 * Date: 22/11/2018
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

require_once("../../../../vendor/plugins/dhtmlxScheduler/connector/scheduler_connector.php");
require_once("../../../../vendor/plugins/dhtmlxScheduler/connector/crosslink_connector.php");
require_once("../../../../vendor/plugins/dhtmlxScheduler/connector/db_pdo.php");

$customers = new \MSFramework\customers();
$seduteObj = new \MSFramework\BeautyCenter\sedute();

function formatText($row){
    global $ary_clienti, $ary_servizi, $ary_cabine, $firephp, $customers, $seduteObj, $MSFrameworkDatabase;

    $customer_id = $row->get_value("customer");
    $appointment_date = $row->get_value("start_date");
    $appointment_services = explode(",", $row->get_value("services"));

    $bilancio_attuale = (float)$customers->getCustomerBalance($customer_id);
    $customer_details = $customers->getCustomerDataFromDB($customer_id);

    $name_color = "";
    $name_append = "";
    $debt_status = "";

    if($bilancio_attuale != "0") {
        $name_color = "#25d801";
        $name_append = " (C)";
        $debt_status = "credito";

        if(strstr($bilancio_attuale, "-")) {
            $name_color = "#ff0b00";
            $name_append = " (D)";
            $debt_status = "debito";
        }

        $name_popup_append = 'Il cliente ha un ' . $debt_status . ' di ' . CURRENCY_SYMBOL . str_replace("-", "", $bilancio_attuale);
    } else {
        $name_popup_append = 'Il cliente non ha crediti o debiti';
    }


    $services_str = "";
    foreach(explode(",", $row->get_value("services")) as $service) {
        if($ary_servizi[$service] == "") {
            continue;
        }

        $sedute = $seduteObj->getCustomerSessionsBalance($customer_id, 'service_'.$service);

        $append_sedute_str = "";
        if($sedute != 0) {
            $append_sedute_str = " (" . ($sedute == 1 ? $sedute . " seduta rimanente" : $sedute . " sedute rimanenti") . ")";
        }

        $services_str .= "<li>" . $ary_servizi[$service] . $append_sedute_str . "</li>";
    }

    $services_str = ($services_str != "" ? '<div style=\'margin-top: 10px;\'><b>Servizi</b><ul>' . substr($services_str, 0, -2) . '</ul></div>' : '');
    $cabin_str = '<div style=\'margin-top: 10px;\'>' . ($row->get_value("cabin") === "-1" || $row->get_value("cabin") === "" ? $ary_cabine[$row->get_value("cabin")] : "<b>Cabina</b>: " . $ary_cabine[$row->get_value("cabin")]) . '</div>';
    $note_str = ($row->get_value("note") != "" ? '<div style=\'margin-top: 10px;\'><b>Note</b>: ' . $row->get_value("note") . '</div>' : '');

    $telefono_str = "";
    $telefono_str .= ($customer_details['telefono_casa'] != "" ? "<li>Telefono casa: " . $customer_details['telefono_casa'] . "</li>" : "");
    $telefono_str .= ($customer_details['telefono_cellulare'] != "" ? "<li>Telefono cellulare: " . $customer_details['telefono_cellulare'] . "</li>" : "");
    if($telefono_str != "") {
        $telefono_str = '<div style=\'margin-top: 10px;\'><b>Contatti</b><ul>' . $telefono_str . '</ul></div>';
    }

    $services_match = 0;
    foreach($MSFrameworkDatabase->getAssoc("SELECT id, carrello, sedute_utilizzate FROM fatturazione_vendite WHERE cliente = :cliente AND DATE(`data`) = :sel_data", array(":cliente" => $customer_id, ":sel_data" => date("Y-m-d", strtotime($appointment_date)))) as $pay_r) {
        $carrello = json_decode($pay_r['carrello'], true);
        $sedute_utilizzate = json_decode($pay_r['sedute_utilizzate'], true);

        foreach($carrello as $car_item) {
            if(in_array($car_item['id'], $appointment_services)) {
                $services_match++;
            }
        }

        foreach($sedute_utilizzate as $sed_item) {
            if(in_array($sed_item['id'], $appointment_services)) {
                $services_match++;
            }
        }
    }

    $pay_status = "";
    $pay_status_append = "";
    if(time() >= strtotime($appointment_date)) {
        if ($services_match == 0) {
            $pay_status = "no_pay";
            $pay_status_color = "#ff0b00";
            $pay_status_append = "Questo appuntamento risulta NON pagato";
        } else if ($services_match == count($appointment_services)) {
            $pay_status = "full_pay";
            $pay_status_color = "#25d801";
            $pay_status_append = "Questo appuntamento risulta pagato";
        } else {
            $pay_status = "partial_pay";
            $pay_status_color = "#E5CA2E";
            $pay_status_append = "Questo appuntamento risulta parzialmente pagato";
        }
    }

    $row->set_value("text", $ary_clienti[$row->get_value("customer")] . $name_append);
    $row->set_userdata("popup_data", "<b>" . $ary_clienti[$row->get_value("customer")] . "</b><div style='color: " . $name_color . ";'>" . $name_popup_append . "</div><div style='color: " . $pay_status_color . ";'>" . $pay_status_append . "</div><div style='font-weight: normal;'>" . $telefono_str . $services_str . $cabin_str . $note_str . "</div>");
    $row->set_userdata("debt_status", $debt_status);
    $row->set_userdata("pay_status", $pay_status);
}

function checkCustomerValidity($action, $customer) {
    global $firephp;

    if($customer == "" || $customer == "null") {
        $action->set_response_text('E\' necessario selezionare un cliente per poter salvare l\'appuntamento.');
        $action->set_response_attribute('highlight_section', 'Cliente');
        $action->invalid();
    }
}

function checkDateViolation($action, $start_date, $end_date, $user) {
    global $firephp, $MSFrameworkDatabase;

    if($start_date == "" || strtotime($start_date) <= time()) {
        $action->set_response_text('Non puoi modificare/creare eventi nel passato.');
        $action->set_response_attribute('highlight_section', 'time');
        $action->invalid();
    }

    if(strtotime($end_date) < strtotime($start_date)) {
        $action->set_response_text('La data di fine non può essere antecedente a quella di inizio.');
        $action->set_response_attribute('highlight_section', 'time');
        $action->invalid();
    }
}

function checkServices($action, $services) {
    global $firephp;

    if($services == "null") {
        $action->set_response_text('E\' necessario selezionare un servizio per poter salvare l\'appuntamento.');
        $action->set_response_attribute('highlight_section', 'Servizi');
        $action->invalid();
    }
}

function checkCabinAvailability($action, $ins_upd, $action_data) {
    global $MSFrameworkDatabase;

    if($action_data['cabin'] != "-1") {
        $str_id = "";
        $ary_id = array();
        if($ins_upd == "updated") {
            $str_id = " AND id != :id";
            $ary_id[':id'] = $action_data['id'];
        }
        if($MSFrameworkDatabase->getCount("SELECT id FROM appointments_list WHERE cabin = :cabin AND ((start_date BETWEEN :start1 AND :end1) OR (end_date BETWEEN :start2 AND :end2)) $str_id", array_merge($ary_id, array(":cabin" => $action_data['cabin'], ":start1" => $action_data['start_date'], ":end1" => $action_data['end_date'], ":start2" => $action_data['start_date'], ":end2" => $action_data['end_date']))) != 0) {
            $action->set_response_text('La cabina selezionata non è disponibile in questa fascia oraria!');
            $action->set_response_attribute('highlight_section', 'Cabina');
            $action->invalid();
        }
    }
}

function beforeProcessing($action) {
    global $firephp;

    $action_data = $action->get_data();
    $action_status = $action->get_status();

    if($action_status != "deleted") {
        checkCustomerValidity($action, $action_data['customer']);
        checkDateViolation($action, $action_data['start_date'], $action_data['end_date'], $action_data['user']);
        checkServices($action, $action_data['services']);
        checkCabinAvailability($action, $action_status, $action_data);
    }

}

$scheduler = new JSONSchedulerConnector($MSFrameworkDatabase->getPDOInstance(),"PDO");

/*
 * Ottengo gli operatori (tutti se superadmin o admin, solo l'operatore corrente in caso contrario)
 */
$users_where_str = "";
$users_where_ary = array();
if((new \MSFramework\users())->getUserDataFromSession('userlevel') != "0" && (new \MSFramework\users())->getUserDataFromSession('userlevel') != "1") {
    $users_where_str = " AND id = :id ";
    $users_where_ary = array(":id" => (new \MSFramework\users())->getUserDataFromSession('user_id'));

    //filtro i risultati mostrati nel calendario solo per l'utente corrente.
    //NB: se un domani volessimo far vedere a tutti gli utenti gli appuntamenti dei colleghi, dovremo fare attenzione alla select OPERATORE visualizzata nel modale che, a causa della mancanza dei nomi di tutti i colleghi, resterebbe vuota in caso di visualizzazione di un appuntamento non proprio.
    $scheduler->filter("(user='" . (new \MSFramework\users())->getUserDataFromSession('user_id') . "' OR user = '-1')");
}

$ary_operatori = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome FROM users WHERE id != '' AND enable_appointments != 0 $users_where_str", $users_where_ary) as $r) {
    $ary_operatori[$r['id']] = $r['cognome'] . " " . $r['nome'];
}
asort($ary_operatori);
if(count($ary_operatori) == 0) {
    $ary_operatori = array("-1" => "Operatore Generico") + $ary_operatori;
}

$scheduler->set_options("users", $ary_operatori);
$scheduler->set_options("users_for_timeline", $ary_operatori);

/*
 * Ottengo i servizi
 */
$ary_servizi = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome FROM servizi") as $r) {
    $ary_servizi[$r['id']] = $MSFrameworki18n->getFieldValue($r['nome'], true);
}
asort($ary_servizi);
$scheduler->set_options("servizi", $ary_servizi);

/*
 * Ottengo le cabine
 */
$ary_cabine = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome FROM beautycenter_cabine") as $r) {
    $ary_cabine[$r['id']] = $MSFrameworki18n->getFieldValue($r['nome'], true);
}
asort($ary_cabine);
$ary_cabine = array("-1" => "Cabina Generica") + $ary_cabine;
$scheduler->set_options("cabine", $ary_cabine);

/*
 * Ottengo i clienti
 */
$ary_clienti = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome FROM customers") as $r) {
    $ary_clienti[$r['id']] = $r['cognome'] . " " . $r['nome'];
}
asort($ary_clienti);
$scheduler->set_options("customers", $ary_clienti);

/*
 * Gestisco gli eventi
 */
$scheduler->event->attach("beforeProcessing", "beforeProcessing");
$scheduler->event->attach("beforeRender","formatText");

/*
 * Ottengo i dati degli appuntamenti
 */
$scheduler->render_table("appointments_list","id","start_date,end_date,text,user,customer,services,note,cabin");
?>
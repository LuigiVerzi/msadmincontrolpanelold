<?php
/**
 * MSAdminControlPanel
 * Date: 29/12/2018
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$r = $MSFrameworkCMS->getCMSData('working_hours');
$r_appointments_settings = $MSFrameworkCMS->getCMSData('appointments_settings');

$admin_mode = true;
$users_where_str = "";
$users_where_ary = array();
if((new \MSFramework\users())->getUserDataFromSession('userlevel') != "0" && (new \MSFramework\users())->getUserDataFromSession('userlevel') != "1") {
    $admin_mode = false;
    $users_where_str = " AND id = :id ";
    $users_where_ary = array(":id" => (new \MSFramework\users())->getUserDataFromSession('user_id'));
}

$r_dipendenti = $MSFrameworkDatabase->getAssoc("SELECT id, work_hours, nome, cognome FROM users WHERE id != '' AND enable_appointments != 0 $users_where_str", $users_where_ary);

if($_POST['mode'] != "" && $_POST['date'] != "") {
    if ($_POST['mode'] == "day" || $_POST['mode'] == "unit" || $_POST['mode'] == "timeline") {
        $filter_date_from = $_POST['date'];
        $filter_date_to = $_POST['date'];

        $day = date("N", strtotime($_POST['date']))-1;
    } else if ($_POST['mode'] == "week") {
        if($r_appointments_settings['calendar_start_day'] == "monday") {
            $day = date("N", strtotime($_POST['date']));
            $filter_date_from = date('d-m-Y', strtotime('-' . $day+1 . ' days'));
            $filter_date_to = date('d-m-Y', strtotime('+' . (6 - $day+1) . ' days'));
        } else if($r_appointments_settings['calendar_start_day'] == "sunday") {
            $day = date("w", strtotime($_POST['date']));
            $filter_date_from = date('d-m-Y', strtotime('-' . $day . ' days'));
            $filter_date_to = date('d-m-Y', strtotime('+' . (6 - $day) . ' days'));
        } else if($r_appointments_settings['calendar_start_day'] == "today") {
            $filter_date_from = $_POST['date'];
            $filter_date_to = date('d-m-Y', strtotime($_POST['date'] . " + 6 days"));
        }
    } else if ($_POST['mode'] == "month") {
        $expl_date = explode("-", $_POST['date']);

        $filter_date_from = '01-' . $expl_date[1] . '-' . $expl_date[2];
        $filter_date_to = date('t-m-Y', strtotime($filter_date_from));
    }

    $filter_date_from = strtotime($filter_date_from);
    $filter_date_to = strtotime($filter_date_to);
}

foreach($r_dipendenti as $r_dipendente) {
    $assocDipendenti[$r_dipendente['id']] = $r_dipendente['cognome'] . " " . $r_dipendente['nome'];

    $hoursGlobal = json_decode($r_dipendente['work_hours'], true);
    $got_hours_dipendente = false;

    if($_POST['mode'] != "" && $_POST['date'] != "") {
        foreach ($hoursGlobal['custom'] as $hoursGroup) {
            $custom_from = strtotime(str_replace("/", "-", $hoursGroup['range']['from']));
            $custom_to = strtotime(str_replace("/", "-", $hoursGroup['range']['to']));

            if ($filter_date_from >= $custom_from && $filter_date_to <= $custom_to) {
                $hours_dipendente = $hoursGroup['hours'];

                $got_hours_dipendente = true;
                break;
            }
        }
    }

    if(!$got_hours_dipendente) {
        if(is_array($hoursGlobal['default']['hours'])) {
            $hours_dipendente = $hoursGlobal['default']['hours'];
            $got_hours_dipendente = true;
        }
    }

    if(!$got_hours_dipendente) {
        $work_hours_dipendenti[$r_dipendente['id']] = $r['hours'];
        continue;
    }

    foreach($hours_dipendente as $todayK => $todayV) {
        if($todayV['row_type'] == "custom" || $todayV['row_type'] == "dayoff") {
            $work_hours_dipendenti[$r_dipendente['id']][$todayK] = $todayV;
        } else {
            $work_hours_dipendenti[$r_dipendente['id']][$todayK] = $r['hours'][$todayK];
        }
    }
}

$all_values = array();
$values_to_search = array("am_in", "am_out", "pm_in", "pm_out");
for($x=0; $x<=6; $x++) {
    $all_values_day = array();
    $tmp_min_max = array();
    $found_dayoff = false;

    foreach($values_to_search as $value) {
        if(!$admin_mode) {
            foreach ($work_hours_dipendenti as $dipendente_hours) {
                if ($dipendente_hours[$x]['row_type'] == "dayoff") {
                    $found_dayoff = true;
                    $work_hours_calendar[$x][$value] = "";

                    continue 2;
                } else {
                    if ($dipendente_hours[$x][$value] != "") {
                        $tmp_min_max[$value][] = $dipendente_hours[$x][$value];
                        $all_values[] = $dipendente_hours[$x][$value];
                        $all_values_day[] = $dipendente_hours[$x][$value];
                    }
                }
            }
        }

        if($r['hours'][$x][$value] != "") {
            $tmp_min_max[$value][] = $r['hours'][$x][$value];
            $all_values[] = $r['hours'][$x][$value];
            $all_values_day[] = $r['hours'][$x][$value];
        }
    }

    if(!$found_dayoff) {
        $work_hours_calendar[$x]['am_in'] = min($tmp_min_max['am_in']);
        $work_hours_calendar[$x]['am_out'] = max($tmp_min_max['am_out']);
        $work_hours_calendar[$x]['pm_in'] = min($tmp_min_max['pm_in']);
        $work_hours_calendar[$x]['pm_out'] = max($tmp_min_max['pm_out']);
    }


    $min_max_per_day[$x] = array("min" => min($all_values_day), "max" => max($all_values_day));
}

$users_avail_today = null;
if($_POST['mode'] == "day" || $_POST['mode'] == "unit" || $_POST['mode'] == "timeline") {
    foreach($work_hours_dipendenti as $userID => $userHours) {
        if($userHours[$day]['rowtype'] == "dayoff" || ($userHours[$day]['am_in'] == "" && $userHours[$day]['am_out'] == "" && $userHours[$day]['pm_in'] == "" && $userHours[$day]['pm_out'] == "")) {
            continue;
        }

        $users_avail_today[$assocDipendenti[$userID]] = array("key" => $userID, "label" => $assocDipendenti[$userID]);
    }
}

if(!is_array($users_avail_today)) {
    $users_avail_today['Operatore Generico'] = array("key" => "-1", "label" => "Operatore Generico");
}

ksort($users_avail_today);
$tmp_users = $users_avail_today;
unset($users_avail_today);
foreach($tmp_users as $userV) {
    $users_avail_today[] = $userV;
}

$min_max_overall = array("min" => min($all_values), "max" => max($all_values));

echo json_encode(
    array(
        "orari_negozio" => $r['hours'], //gli orari impostati nel modulo esercizio/negozio
        "orari_dipendenti" => $work_hours_dipendenti, //un array di orari di lavoro per ogni utente
        "orari_calendario" => $work_hours_calendar, //gli orari che verranno effettivamente mostrati sul calendario, mixando i valori impostati nel modulo esercizio/negozio e in quello utente
        "min_max_per_day" => $min_max_per_day, //il valore minimo e massimo degli orari disponibili per ogni giorno (tiene conto sia degli orari del negozio che di quelli dell'utente)
        "min_max_overall" => $min_max_overall, //il valore minimo e massimo degli orari per l'intera settimana (tiene conto sia degli orari del negozio che di quelli dell'utente)
        "users_avail_today" => $users_avail_today, //l'elenco degli utenti lavoranti per la giornata odierna (disponibile solo per day, unit e timeline)
    )
);
?>
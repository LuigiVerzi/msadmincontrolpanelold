$(document).ready(function() {
    if($('#force_view').val() == "today_list") {
        scheduler.setCurrentView(new Date(), "day");
    }
});

function initForm() {
    scheduler.config.xml_date = "%Y-%m-%d %H:%i";
    scheduler.config.details_on_dblclick = true;
    scheduler.config.icons_select = ["icon_details", "icon_delete"];
    scheduler.config.default_date = "%l %j %M %Y";
    scheduler.config.details_on_create = true;
    scheduler.config.start_on_monday = ($('#calendar_start_day').val() == "sunday" ? false : true);
    scheduler.config.show_loading = true;
    
    scheduler.keys.edit_save = false;

    scheduler.config.buttons_left=["save_not_close_button","custom_close_button"];
    scheduler.locale.labels["save_not_close_button"] = "Salva";
    scheduler.locale.labels["custom_close_button"] = "Annulla";

    window.doNotReloadWorkingHours = false;
    window.savingStatus = false;
    window.completedWorkingHourProcess = false;

    var menu = new dhtmlXMenuObject();
    menu.setIconsPath("ajax/dhtmlx/data/imgs/");
    menu.renderAsContextMenu();
    menu.loadStruct("ajax/dhtmlx/data/dhxmenu.xml");
    menu.attachEvent("onClick", function(id, zoneId, cas){
        if(id == "fatturazione") {
            var win = window.open($("#baseElementPathAdmin").val() + 'modules/fatturazione/vendite/edit.php?event_id=' + window.contextMenuEventId, '_blank');
            win.focus();
        } else if(id == "elimina") {
            scheduler._dhtmlx_confirm(scheduler.locale.labels.confirm_deleting, scheduler.locale.labels.title_confirm_deleting, function(){
                scheduler.deleteEvent(window.contextMenuEventId);
            });
        } else if(id == "modifica") {
            scheduler.showLightbox(window.contextMenuEventId);
        } else if(id == "duplica") {
            eventObj = scheduler.getEvent(window.contextMenuEventId);

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);

            scheduler.addEventNow({
                start_date: tomorrow,
                end_date: tomorrow,
                text: eventObj.text,
                cabin: eventObj.cabin,
                customer: eventObj.customer,
                note: eventObj.note,
                services: eventObj.services,
                user: eventObj.user,
            });
        }
    });

    //a causa di un bug che fa "dimenticare" allo script lo stato dell'appuntamento (nuovo/già esistente) devo rinfrescargli la memoria manualmente ricordandomelo al posto suo, così se l'utente annulla la creazione di un nuovo appuntamento, questo non compare nel calendario
    //https://forum.dhtmlx.com/t/cancelling-a-reopened-lightbox-leaves-new-event-on-scheduler/36270/2
    window.newEvent = null;
    window.keepNewEventStatus = false;

    var startDate = new Date();
    var endDate = new Date();
    startDate.setDate(startDate.getDate()-1);
    endDate.setDate(endDate.getDate()+(365*5));

    scheduler.config.limit_start = new Date(startDate);
    scheduler.config.limit_end = new Date(endDate);

    //Tab visualizzazione settimana/mese a partire da oggi (non da lunedì/domenica)
    if($('#calendar_start_day').val() == "today") {
        scheduler.attachEvent("onTemplatesReady", function () {
            scheduler.date.week_start = function (date) {
                date.setHours(0,0,0,0)
                return date;
            }

            scheduler.date.get_week_end = function (start_date) {
                return scheduler.date.add(start_date, 7, "day");
            }

            scheduler.date.add_week = function (date, inc) {
                return scheduler.date.add(date, inc * 7, "day");
            }
        });
    }

    scheduler.config.full_day = true;

    scheduler.form_blocks["helpButtons"] = {
        render:function(sns){
            return "<div id=\"customerDetailsBtn\" role=\"button\" aria-label=\"Dettagli Cliente\" tabindex=\"0\" class=\"dhx_btn_set dhx_left_btn_set view_customer_btn_set\"><div dhx_button=\"1\" class=\"view_customer_btn\"></div><div style='margin-top: -10px; padding: 0px;'>Dettagli Cliente</div></div>" +
                "<div id=\"customerAddBtn\" role=\"button\" aria-label=\"Aggiungi Cliente\" tabindex=\"0\" class=\"dhx_btn_set dhx_left_btn_set add_customer_btn_set\"><div dhx_button=\"1\" class=\"add_customer_btn\"></div><div style='margin-top: -10px; padding: 0px;'>Aggiungi Cliente</div></div>" +
                "<div dhx_button=\"1\" class=\"customer_debt_btn\"></div><div style='float: right; font-weight: bold;' id='customerDebtStatus'></div>";
        },
        set_value:function(node,value,ev,config){
            //non rimuovere questa funzione, anche se vuota. l'editor ne necessita
        },
        get_value:function(node,ev,config){
            //non rimuovere questa funzione, anche se vuota. l'editor ne necessita
        },
    }

    scheduler.form_blocks["custom_services"] = {
        render: function (sns) {
            var height = (sns.height || "35") + "px";
            var html = "<div class='scheduler-multiselect dhx_cal_ltext' style='height:" + height + ";'><select data-placeholder='Seleziona un servizio' class='chosenSelect customerChosenSelect' multiple style='width: 100%'>";

            if (sns.options) {
                for (var i = 0; i < sns.options.length; i++)
                    html += "<option value='" + sns.options[i].key + "'>" + sns.options[i].label + "</option>";
            }

            html += "</select></div><div class='dhx_cal_ltext' id='servicesSeduteResidueContainer' style='height:120px !important; overflow-y: auto; display: none;'><div class='dhx_left_btn_set' id='servicesSeduteResidue'></div></div>";
            return html;
        },
        set_value: function (node, value, ev, sns) {
            node.style.overflow = "visible";
            node.parentNode.style.overflow = "visible";
            var select = $(node.firstChild);
            if (value) {
                value = value.split(",");
                select.val(value);
            }
            else {
                select.val([]);
            }
            select.chosen({
                search_contains: true,
                no_results_text: "Nessun risultato trovato per",
            });
            select.trigger("chosen:updated");
            $('.customerChosenSelect').unbind('change');
            $('.customerChosenSelect').on('change', calcolaTempoNecessario);
        },
        get_value: function (node, ev) {
            var value = $(node.firstChild).val();
            value = value ? value.join(",") : null;
            return value;
        },
        focus: function (node) {
            $(node.firstChild).focus();
        }
    }
    scheduler.form_blocks["custom_customers"] = {
        render: function (sns) {
            var height = (sns.height || "36") + "px";

            return "<div class='dhx_cal_ltext'><input id='customers_search' style='height:" + height + "; width: 100%;' /><input id='customers_search_hidden' type='hidden'/></div>";
        },
        set_value: function (node, value, ev) {
            if(typeof(value) == "undefined") {
                value = "";
            }

            customerChangedCallback(value, node);
        },
        get_value: function (node, ev) {
            return node.childNodes[1].value;
        },
        focus: function (node) {
            var a = node.childNodes[0];
            a.select();
            a.focus();
        }
    }

    scheduler.config.lightbox.sections = [
        {name:"Operatore", map_to:"user", type:"select", options: scheduler.serverList("users"), onchange: operatorChangeCallback},
        {name:"Cliente", map_to:"customer", type:"custom_customers"},
        {name:" ", type:"helpButtons"},
        {name:"Cabina", map_to:"cabin", type:"select", options: scheduler.serverList("cabine")},
        {name:"Servizi", map_to:"services", type:"custom_services", options: scheduler.serverList("servizi")},
        {name:"time", height:72, type:"time", map_to:"auto"},
        {name:"Note", height:130, map_to:"note", type:"textarea"},
    ];

    if($('#reverse_timeline_axis').val() == "1") {
        admin_default_view_name = "timeline";

        scheduler.createTimelineView({
            name:"timeline",
            x_unit:"minute",
            x_date:"%H:%i",
            x_step:30,
            x_size:48,
            x_start:0,
            x_length:48,
            y_unit: scheduler.serverList("users_for_timeline"),
            y_property:"user",
            render:"bar",
            scrollable: true,
        });

        scheduler.templates.timeline_scale_label = function(key, label, unit) {
            return "<span onclick='goToOperatorProfile(" + key + ");' class='unit_scale_header'>" + label + "</span>"
        };
    } else {
        admin_default_view_name = "unit";

        scheduler.locale.labels.unit_tab = "Timeline";
        scheduler.createUnitsView({
            name:"unit",
            property:"user",
            list: scheduler.serverList("users_for_timeline")
        });

        scheduler.templates.unit_scale_text = function(key, label, unit) {
            return "<span onclick='goToOperatorProfile(" + key + ");' class='unit_scale_header'>" + label + "</span>"
        };
    }

    scheduler.init('scheduler', new Date(), ($('#userLevel').val() == '0' || $('#userLevel').val() == '1' ? admin_default_view_name : "week"));
    scheduler.load("ajax/loadAppointments.php", "json");

    $('.dhx_minical_icon').attr('style', function(i,s) { return s + 'left: 395px !important;' });

    scheduler.templates.tooltip_text = function(start,end,ev){
        return ev.popup_data;
    };

    scheduler.templates.event_class = function (start, end, event) {
        //Evidenzia di rosso/verse l'appuntamento se il cliente ha un debito/credito
        /*if (event.debt_status == 'debito') {
            return "event_red_bg";
        } else if(event.debt_status == 'credito') {
            return "event_green_bg";
        }*/

        if (event.pay_status == 'no_pay') {
            return "event_red_bg";
        } else if(event.pay_status == 'full_pay') {
            return "event_green_bg";
        } else if(event.pay_status == 'partial_pay') {
            return "event_orange_bg";
        }
    };

    scheduler.config.hour_size_px = 88;
    scheduler.templates.hour_scale = function(date){
        format = scheduler.date.date_to_str("%H:%i");
        step = 30;
        step_height = scheduler.config.hour_size_px/3;

        html = "";

        for (var i=0; i<60/step; i++){
            html+="<div style='height:" + step_height + "px;line-height:" + step_height + "px;'>"+format(date)+"</div>";
            date = scheduler.date.add(date,step,"minute");

            if(i == 1) {
                html+="<div style='height:" + step_height + "px;line-height:" + step_height + "px;'>"+format(date)+"</div>";
                date = scheduler.date.add(date,step,"minute");
            }
        }

        return html;
    }

    var dp = new dataProcessor("ajax/loadAppointments.php");

    dp.defineAction("invalid",function(response){
        window.doNotReloadWorkingHours = true;
        scheduler.showLightbox(window.eventId);

        if(typeof(response.attributes.highlight_section.nodeValue) != "undefined") {
            found_section = scheduler.formSection(response.attributes.highlight_section.nodeValue);

            $(found_section.header).find('label').css('color', 'red');
        }

        enableSaveButton();

        bootbox.error((response.textContent != "" ? response.textContent : "Errore generico. Impossibile continuare."));

        return true;
    })

    dp.attachEvent("onAfterUpdate", function(id, action, tid, response){
        scheduler.load("ajax/loadAppointments.php", "json");

        if(action == "inserted" || action == "updated") {
            scheduler.hide_lightbox(); //chiudo manualmente perchè uso un pulsante "salva" customizzato che, di default, non chiude la lightbox
            window.newEvent = null;
            window.keepNewEventStatus = false;
        }

        return true;
    })

    dp.attachEvent("onBeforeUpdate", function (id, state, data) {
        scheduler._new_event = window.newEvent;

        canContinueSaving = true;
        if(typeof(data.start_date) != "undefined") {
            canContinueSaving = checkLimitViolationForEvent(new Date(data.start_date), new Date(data.end_date), data.user, data.text);
        }

        return canContinueSaving;
    });

    dp.attachEvent("onBeforeDataSending", function (id, state, data) {
        scheduler._new_event = window.newEvent;

        canContinueSaving = true;
        if(typeof(data.start_date) != "undefined") {
            canContinueSaving = checkLimitViolationForEvent(new Date(data.start_date), new Date(data.end_date), data.user, data.text);
        }

        return canContinueSaving;
    });

    scheduler.attachEvent("onContextMenu", function(event_id, native_event_object) {
        window.contextMenuEventId = event_id;

        if (event_id) {
            var posx = 0;
            var posy = 0;
            if (native_event_object.pageX || native_event_object.pageY) {
                posx = native_event_object.pageX;
                posy = native_event_object.pageY;
            } else if (native_event_object.clientX || native_event_object.clientY) {
                posx = native_event_object.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                posy = native_event_object.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }
            menu.showContextMenu(posx, posy);

            return false; // prevent default action and propagation
        }

        return true;
    });

    scheduler.attachEvent("onBeforeLightbox",function(id){
        enableSaveButton();
        window.eventId = null;

        return true;
    })

    scheduler.attachEvent("onLightbox", function(id){
        $('.dhx_wrap_section').find('label').css('color', 'black');

        $("#customers_search").autocomplete({
            source: "ajax/searchDataAutocomplete.php?t=customer",
            select: function( event, ui ) {
                $("#customers_search").val(ui.item.value);
                $("#customers_search_hidden").val(ui.item.id);

                customerChangedCallback($("#customers_search_hidden").val(), $("#customers_search").closest('.dhx_cal_ltext')[0]);
            },
            change: function( event, ui ) {
                if($("#customers_search_hidden").val() == "") {
                    $("#customers_search").val('');

                    $('#customerDetailsBtn').hide();
                    $('#customerAddBtn').show();
                    $('#customerDebtStatus').hide();
                }
            }
        });

        $("#customers_search").on('keyup', function() {
            $("#customers_search_hidden").val('');
        })

        operatorChangeCallback();

        $('.dhx_lightbox_time_select:first').unbind('change');
        $('.dhx_lightbox_time_select:first').on('change', function() {
            calcolaTempoNecessario();
        })

        if(window.keepNewEventStatus !== false) {
            scheduler._new_event = window.keepNewEventStatus;
            window.keepNewEventStatus = false;
        }

        window.newEvent = scheduler._new_event;

        //Ricreo al volo le options per la selezione degli orari in base ai limiti impostati all'interno dei vari giorni
        $('.dhx_lightbox_year_select, .dhx_lightbox_month_select, .dhx_lightbox_day_select').unbind('change');
        $('.dhx_lightbox_year_select, .dhx_lightbox_month_select, .dhx_lightbox_day_select').on('change', function () {
            start_date = new Date(Number($('.dhx_lightbox_year_select:first').val()), Number($('.dhx_lightbox_month_select:first').val()), Number($('.dhx_lightbox_day_select:first').val()), 0, 0, 0, 0);
            end_date = new Date(Number($('.dhx_lightbox_year_select:last').val()), Number($('.dhx_lightbox_month_select:last').val()), Number($('.dhx_lightbox_day_select:last').val()), 0, 0, 0, 0);

            start_date_limits = zones_values_obj[start_date.getDay()];
            end_date_limits = zones_values_obj[end_date.getDay()];

            start_options = "";
            end_options = "";

            if (start_date_limits != "fullday") {
                cur_element = 0;
                for (i = 0; i <= 1440; i += 5) {
                    if (i >= start_date_limits[cur_element] && i <= start_date_limits[cur_element + 1]) {
                        start_options += "<option value='" + i + "'>" + String(Math.floor(i / 60)).padStart(2, '0') + ':' + String(i % 60).padStart(2, '0') + "</option>";
                    }

                    if (i == start_date_limits[cur_element + 1]) {
                        cur_element += 2;
                    }
                }
            }

            if (end_date_limits != "fullday") {
                cur_element = 0;
                for (i = 0; i <= 1440; i += 5) {
                    if (i >= end_date_limits[cur_element] && i <= end_date_limits[cur_element + 1]) {
                        end_options += "<option value='" + i + "'>" + String(Math.floor(i / 60)).padStart(2, '0') + ':' + String(i % 60).padStart(2, '0') + "</option>";
                    }

                    if (i == end_date_limits[cur_element + 1]) {
                        cur_element += 2;
                    }
                }
            }

            hour_first_val = $('.dhx_lightbox_time_select:first').val();
            hour_last_val = $('.dhx_lightbox_time_select:last').val();
            $('.dhx_lightbox_time_select:first').html(start_options).val(hour_first_val);
            $('.dhx_lightbox_time_select:last').html(end_options).val(hour_last_val);
        });

        $('.dhx_lightbox_year_select:first').trigger('change');

        return true;
    })

    scheduler.attachEvent("onLightboxButton", function(button_id, node, e){
        if(button_id == "view_customer_btn"){
            sel_customer = scheduler.formSection('Cliente').getValue();

            if(sel_customer == "" || sel_customer == null) {
                bootbox.error("Selezionare un cliente per poterne visualizzare i dettagli");
            } else {
                edit_user_dialog = bootbox.dialog({
                    message: $('#customer_details_modal').html(),
                    hideHeader: true,
                    size: 'large',
                    buttons: {
                        cancel: {
                            label: "Annulla",
                            className: 'btn-danger'
                        },
                        ok: {
                            label: "Aggiorna Dati Cliente",
                            className: 'btn-info',
                            callback: function(){
                                fastCustomerSave(sel_customer);
                            }
                        }
                    }
                });

                edit_user_dialog.init(function(){
                    $('.modal-content .input-group.date').datepicker({
                        keyboardNavigation: false,
                        autoclose: true,
                        language: "it",
                        endDate: new Date(),
                    });
                });
            }
        } else if(button_id == "add_customer_btn"){
            new_user_dialog = bootbox.dialog({
                message: $('#customer_details_modal').html(),
                hideHeader: true,
                size: 'large',
                buttons: {
                    cancel: {
                        label: "Annulla",
                        className: 'btn-danger'
                    },
                    ok: {
                        label: "Crea Nuovo Cliente",
                        className: 'btn-info',
                        callback: function(){
                            fastCustomerSave('');
                        }
                    }
                }
            });

            new_user_dialog.init(function(){
                $('.modal-content  input, .modal-content select').val('');

                $('.modal-content .input-group.date').datepicker({
                    keyboardNavigation: false,
                    autoclose: true,
                    language: "it",
                    endDate: new Date(),
                });
            });
        } else if(button_id == "save_not_close_button"){
            //pulsante salva personalizzato. evita di chiudere la lightbox automaticamente
            window.savingStatus = true;

            disableSaveButton();

            window.keepNewEventStatus = window.newEvent;
            time = scheduler.formSection("time").getValue();

            $('.dhx_wrap_section').find('label').css('color', 'black');
            canContinueSaving = checkLimitViolationForEvent(time.start_date, time.end_date, scheduler.formSection("Operatore").getValue(), '');

            if(canContinueSaving) {
                var eventId = scheduler.getState().lightbox_id;
                window.eventId = eventId;
                scheduler.save_lightbox();
            }

            window.savingStatus = false;
        } else if(button_id == "custom_close_button") {
            scheduler._new_event = window.newEvent;
            scheduler.hide_lightbox();
        }
    });

    scheduler.attachEvent("onLimitViolation",function(id, obj){
        if(scheduler.getState().mode == "month") { //issue #10
            test_date = obj.start_date;
            test_date.setHours(0,0,0,0);

            today = new Date();
            today.setHours(0,0,0,0);

            if(test_date >= today) {
                return true;
            }
        }

        enableSaveButton();
        bootbox.error("Non puoi gestire un appuntamento in questa fascia oraria! Il negozio è chiuso, il dipendente selezionato non è disponibile o stai provando a modificare/creare eventi nel passato.");

        return false;
    })

    scheduler.attachEvent("onBeforeViewChange", function(old_mode, old_date, new_mode, new_date){
        if(window.doNotReloadWorkingHours) {
            window.doNotReloadWorkingHours = false;
        } else {
            reinitTimeLocks(new_mode, new_date);
        }

        return true;
    })

    dp.init(scheduler);
}

function enableSaveButton() {
    $('.save_not_close_button_set').removeClass('save_not_close_button_set_disabled');
    $('.save_not_close_button_set div:not(.save_not_close_button)').html('Salva');

    $('.custom_close_button_set, .dhx_delete_btn_set').show();
}

function disableSaveButton() {
    $('.save_not_close_button_set').addClass('save_not_close_button_set_disabled');
    $('.save_not_close_button_set div:not(.save_not_close_button)').html('Salvataggio in corso...');

    $('.custom_close_button_set, .dhx_delete_btn_set').hide();
}

function checkLimitViolationForEvent(start_date, end_date, user, txt) {
    reinitTimeLocks('day', start_date);

    canContinueSaving = scheduler.checkLimitViolation({
        text : txt,
        start_date : start_date,
        end_date : end_date,
        user : user,
    });

    return canContinueSaving;
}

function reinitTimeLocks(new_mode, new_date) {
    //devo ricontrollare gli orari di lavoro dei dipendenti, perchè potrebbero cambiare in base al periodo dell'anno
    hours_data = getWorkingHours(new_mode, new_date);

    if($('#compact_view').val() == "1") {
        if (timeStringToFloat(hours_data.min_max_overall.min) != "") {
            scheduler.config.first_hour = timeStringToFloat(hours_data.min_max_overall.min);
        }

        if (timeStringToFloat(hours_data.min_max_overall.max) != "") {
            scheduler.config.last_hour = timeStringToFloat(hours_data.min_max_overall.max);
        }
    }

    if($('#limit_working_hours').val() == "1") {
        scheduler.deleteMarkedTimespan();

        scheduler.config.check_limits = true;
        scheduler.config.limit_time_select = true;
        scheduler.config.full_day = false;

        zones_values_obj = new Object();
        start_day = 1;

        $(hours_data.orari_calendario).each(function (dataK, dataV) {
            dataV['am_in'] = timeStringToFloat(dataV['am_in']);
            dataV['am_out'] = timeStringToFloat(dataV['am_out']);
            dataV['pm_in'] = timeStringToFloat(dataV['pm_in']);
            dataV['pm_out'] = timeStringToFloat(dataV['pm_out']);

            if (dataV['am_in'] == "" && dataV['am_out'] == "" && dataV['pm_in'] == "" && dataV['pm_out'] == "") {
                zones_value = "fullday";
            } else {
                zones_value = new Array();
                skip_pm = false;

                if (dataV['am_in'] != "") {
                    zones_value.push(dataV['am_in'] * 60);

                    if (dataV['am_out'] != "") { //è stato impostato un orario di apertura mattutino ed è presente anche chiusura mattutina
                        zones_value.push(dataV['am_out'] * 60);
                    } else if (dataV['pm_out'] != "") { //è stato impostato un orario di apertura mattutino ma NON è presente una chiusura mattutina. E' presente, però, quella pomeridiana. Prendo in riferimento quella
                        skip_pm = true;
                        zones_value.push(dataV['pm_out'] * 60);
                    } else { //è stato impostato un orario di apertura mattutino ma non è presente alcun orario di chiusura. Chiude alle 24
                        zones_value.push(24 * 60);
                    }
                }

                if (dataV['pm_in'] != "" && !skip_pm) {
                    zones_value.push(dataV['pm_in'] * 60);

                    if (dataV['pm_out'] != "") { //è stato impostato un orario di apertura pomeridiano ed è presente anche chiusura pomeridiana
                        zones_value.push(dataV['pm_out'] * 60);
                    } else { //è stato impostato un orario di apertura pomeridiano ma NON è presente una chiusura pomeridiana. Chiude alle 24
                        zones_value.push(24 * 60);
                    }
                }
            }

            conv_day = (start_day == 7 ? 0 : start_day);
            zones_values_obj[conv_day] = new Array();
            zones_values_obj[conv_day] = zones_value;

            scheduler.addMarkedTimespan({
                days: conv_day,
                zones: zones_value,
                invert_zones: (zones_value == "fullday" ? false : true),
                css: "gray_section",
                type: "dhx_time_block",
                html: "<div style='text-align: center;'>" + ($('#userLevel').val() == '0' || $('#userLevel').val() == '1' ? "Chiusura" : "Chiusura/Riposo") + "</div>",
            });

            start_day++;
        })

        users_avail_today = [];
        if(hours_data.users_avail_today != null) {
            $.each(hours_data.users_avail_today, function(k, v) {
                users_avail_today.push(v['key'].toString());
            })
        }

        $(hours_data.orari_dipendenti).each(function (dataTMPK, dataVTMP) {
            for (IDDipendente in dataVTMP) {
                start_day = 1;

                dataVAry = dataVTMP[IDDipendente];

                for (dataK in dataVAry) {
                    dataV = dataVAry[dataK];

                    dataV['am_in'] = timeStringToFloat(dataV['am_in']);
                    dataV['am_out'] = timeStringToFloat(dataV['am_out']);
                    dataV['pm_in'] = timeStringToFloat(dataV['pm_in']);
                    dataV['pm_out'] = timeStringToFloat(dataV['pm_out']);

                    if (dataV['am_in'] == "" && dataV['am_out'] == "" && dataV['pm_in'] == "" && dataV['pm_out'] == "") {
                        zones_value = "fullday";
                    } else {
                        zones_value = new Array();
                        skip_pm = false;

                        if (dataV['am_in'] != "") {
                            zones_value.push(dataV['am_in'] * 60);

                            if (dataV['am_out'] != "") { //è stato impostato un orario di apertura mattutino ed è presente anche chiusura mattutina
                                zones_value.push(dataV['am_out'] * 60);
                            } else if (dataV['pm_out'] != "") { //è stato impostato un orario di apertura mattutino ma NON è presente una chiusura mattutina. E' presente, però, quella pomeridiana. Prendo in riferimento quella
                                skip_pm = true;
                                zones_value.push(dataV['pm_out'] * 60);
                            } else { //è stato impostato un orario di apertura mattutino ma non è presente alcun orario di chiusura. Chiude alle 24
                                zones_value.push(24 * 60);
                            }
                        }

                        if (dataV['pm_in'] != "" && !skip_pm) {
                            zones_value.push(dataV['pm_in'] * 60);

                            if (dataV['pm_out'] != "") { //è stato impostato un orario di apertura pomeridiano ed è presente anche chiusura pomeridiana
                                zones_value.push(dataV['pm_out'] * 60);
                            } else { //è stato impostato un orario di apertura pomeridiano ma NON è presente una chiusura pomeridiana. Chiude alle 24
                                zones_value.push(24 * 60);
                            }
                        }
                    }

                    conv_day = (start_day == 7 ? 0 : start_day);

                    scheduler.addMarkedTimespan({
                        days: conv_day,
                        zones: zones_value,
                        sections: {unit: IDDipendente, timeline: IDDipendente},
                        invert_zones: (zones_value == "fullday" ? false : true),
                        css: "closed_section",
                        type: "dhx_time_block",
                        html: "<div style='text-align: center;'>Riposo</div>"
                    });

                    start_day++;
                }

            }
        })
    }

    scheduler.updateView();
}

function getWorkingHours(mode, date) {
    var hours_data = {};
    if(date != "") {
        date = date.getDate() + "-" + String(date.getMonth()+1).padStart(2, '0') + "-" + date.getFullYear();
    }

    //Devo necessariamente ricaricare gli appuntamenti e riverificare gli orari di lavoro perchè, in alcuni casi, le interfacce potrebbero non combaciare e lo scheduler si confonde nel piazzare gli appuntamenti.
    if(!window.completedWorkingHourProcess) {
        scheduler.load("ajax/loadAppointments.php", "json");
        window.completedWorkingHourProcess = true;
    } else {
        window.completedWorkingHourProcess = false;
    }

    $.ajax({
        url: "ajax/getWorkingHours.php",
        async: false,
        dataType: "json",
        type: "POST",
        data: {
            "mode" : mode,
            "date" : date,
        },
        success: function (data) {
            hours_data = data;

            if(data.users_avail_today != null && !window.savingStatus) {
                window.doNotReloadWorkingHours = true;

                scheduler.updateCollection("users_for_timeline", data.users_avail_today);
                scheduler.updateView();
            }

        }
    })

    return hours_data;
}

function calcolaTempoNecessario() {
    if($('#dynamic_end_enable').val() != "1") {
        return true;
    }

    getServicesData();
}

function customerChangedCallback(value, node) {
    $('#customers_search_hidden').val(value);
    node.childNodes[0].value = '';

    $('#customerDetailsBtn').hide();
    $('#customerAddBtn').hide();
    $('#customerDebtStatus').hide();

    if(value != "") {

        node.childNodes[0].value = 'Ottengo dati cliente...';

        $.ajax({
            url: "ajax/getCustomerData.php?id=" + value,
            async: false,
            dataType: "json",
            success: function (data) {
                balance = String(data.balance);
                node.childNodes[0].value = data.cognome + " " + data.nome + data.tel_str;

                $('#customerDetailsBtn').show();

                $('#customerDebtStatus').css('color', 'grey');

                if(balance == "0") {
                    $('#customerDebtStatus').html('Il cliente non ha crediti o debiti');
                } else {
                    debt_status = "credito";
                    debt_color = "green";

                    if(balance.indexOf('-') != "-1") {
                        debt_status = "debito";
                        debt_color = "red";
                    }

                    $('#customerDebtStatus').html('Il cliente ha un ' + debt_status + ' di €' + balance.replace('-', '')).css('color', debt_color);
                }

                $('#customerDebtStatus').show();

                //Dati modal "Dettagli cliente"
                $('#nome').attr('value', data.nome);
                $('#cognome').attr('value', data.cognome);
                $('#sesso').attr('value', data.sesso);
                $('#data_nascita').attr('value', data.data_nascita);
                $('#indirizzo').attr('value', data.indirizzo);
                $('#citta').attr('value', data.citta);
                $('#cap').attr('value', data.cap);
                $('#provincia').attr('value', data.provincia);
                $('#email').attr('value', data.email);
                $('#telefono_casa').attr('value', data.telefono_casa);
                $('#telefono_cellulare').attr('value', data.telefono_cellulare);
                $('#ragione_sociale').attr('value', data.dati_fatturazione.ragione_sociale);
                $('#piva').attr('value', data.dati_fatturazione.piva);
                $('#cf').attr('value', data.dati_fatturazione.cf);
                $('#indirizzo_fatturazione').attr('value', data.dati_fatturazione.indirizzo);
                $('#citta_fatturazione').attr('value', data.dati_fatturazione.citta);
                $('#provincia_fatturazione_fatturazione').attr('value', data.dati_fatturazione.provincia);
                $('#cap_fatturazione').attr('value', data.dati_fatturazione.cap);

                getServicesData();
            }
        })
    } else {
        $('#customerAddBtn').show();
    }
}

function getServicesData() {
    $('#servicesSeduteResidue').html('');
    $('#servicesSeduteResidueContainer').hide();

    $.ajax({
        url: "ajax/getServicesData.php",
        type: "POST",
        data: {
            "pServices": $('.customerChosenSelect').val(),
            "pCustomerID": $('#customers_search_hidden').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            dest_value = data.timeData.sum+$('.dhx_lightbox_time_select:first').val()*1;

            $('.dhx_lightbox_time_select:last').val(Math.ceil(dest_value/5)*5);

            if(data.residuoSedute.output != "") {
                $('#servicesSeduteResidue').html('<b>Sedute Residue</b> <br />' + data.residuoSedute.output);
                $('#servicesSeduteResidueContainer').show();
            }
        }
    })
}


function fastCustomerSave(id) {
    $.ajax({
        url: "db/saveCustomer.php",
        type: "POST",
        data: {
            "pID": id,
            "pNome": $('.modal-content #nome').val(),
            "pCognome": $('.modal-content #cognome').val(),
            "pDataNascita": $('.modal-content #data_nascita').val(),
            "pTelefonoCasa": $('.modal-content #telefono_casa').val(),
            "pTelefonoCellulare": $('.modal-content #telefono_cellulare').val(),
            "pIndirizzo": $('.modal-content #indirizzo').val(),
            "pCitta": $('.modal-content #citta').val(),
            "pProvincia": $('.modal-content #provincia').val(),
            "pCAP": $('.modal-content #cap').val(),
            "pProvenienza": $('.modal-content #provenienza').val(),
            "pSesso": $('.modal-content #sesso').val(),
            "pEmail": $('.modal-content #email').val(),
            "pFatturazione": {
                "ragione_sociale": $('.modal-content #ragione_sociale').val(),
                "piva": $('.modal-content #piva').val(),
                "cf": $('.modal-content #cf').val(),
                "indirizzo": $('.modal-content #indirizzo_fatturazione').val(),
                "citta": $('.modal-content #citta_fatturazione').val(),
                "provincia": $('.modal-content #provincia_fatturazione').val(),
                "cap": $('.modal-content #cap_fatturazione').val()
            }
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido o è già stata utilizzata da un altro utente!");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data di nascita non è valido. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                customer_id = data.customer_id;

                $("#customers_search").val(data.customer_name);
                $("#customers_search_hidden").val(customer_id);

                customerChangedCallback($("#customers_search_hidden").val(), $("#customers_search").closest('.dhx_cal_ltext')[0]);
            }
        }
    })
}

function timeStringToFloat(time) {
    if(time == "" || typeof(time) == "undefined" || time == false || time == null) {
        return "";
    }

    var hoursMinutes = time.split(/[.:]/);
    var hours = parseInt(hoursMinutes[0], 10);
    var minutes = hoursMinutes[1] ? parseInt(hoursMinutes[1], 10) : 0;

    return hours + minutes / 60;
}

function operatorChangeCallback() {
    $.ajax({
        url: "ajax/getOperatorServices.php",
        async: false,
        dataType: "html",
        type: "POST",
        data: {
            "id" : scheduler.formSection('Operatore').getValue(),
            "servizi" : scheduler.formSection('Servizi').getValue(),
        },
        success: function (data) {
            $('.customerChosenSelect').html(data);
            $('.customerChosenSelect').trigger("chosen:updated");
        }
    })
}

function showMiniCal(){
    if (scheduler.isCalendarVisible()){
        scheduler.destroyCalendar();
    } else {
        scheduler.renderCalendar({
            position:"dhx_minical_icon",
            date:scheduler._date,
            navigation:true,
            handler:function(date,calendar){
                scheduler.setCurrentView(date);
                scheduler.destroyCalendar()
            }
        });
    }
}

function goToOperatorProfile(op_id) {
    var win = window.open($("#baseElementPathAdmin").val() + 'modules/manager/utenti/list/edit.php?id=' + op_id, '_blank');
    win.focus();
}
<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM travelagency__packages ORDER BY id DESC") as $r) {

    $image = json_decode($r['gallery']);
    $location = json_decode($r['destinazione'], true);

    $array['data'][] = array(
        '<img src="' . UPLOAD_TRAVELAGENCY_PACKAGE_FOR_DOMAIN_HTML . "tn/" . $image[0] . '" alt="" height="auto" width="80">',
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        $MSFrameworki18n->getFieldValue($r['slug'], true),
        CURRENCY_SYMBOL . " " . $r['prezzo'],
        $r['destinazione'],
        date('d/m/Y', strtotime($r['data_partenza'])),
        date('d/m/Y', strtotime($r['scadenza'])),
        ($r['featured'] ? '<span class="label label-warning">In Evidenza</span>' : '<span class="label label-inverse">No</span>'),
        ($r['prezzo_scontato'] > 0 && (strtotime($r['scadenza_sconto']) < 0 || strtotime($r['scadenza_sconto']) > time()) ? '<span class="label label-primary">In Offerta</span>' : '<span class="label label-inverse">No</span>'),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

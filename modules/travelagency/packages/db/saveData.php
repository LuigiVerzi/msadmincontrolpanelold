<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pPrezzo'] == "" || $_POST['pCategoria'] == "" || $_POST['pSlug'] == "" || $_POST['pPersone'] == "" || $_POST['pNotti'] == "" || $_POST['pDestinazione'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "travelagency__packages", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

$price_error = false;

$_POST['pPrezzo'] = str_replace(",", ".", $_POST['pPrezzo']);
if(!strstr($_POST['pPrezzo'], ".")) {
    $_POST['pPrezzo'] .= ".00";
}

if(!is_numeric($_POST['pPrezzo'])) {
    $price_error = true;
}

if(isset($_POST['pExtraFields']['prezzo_affiliati'])) {
    $_POST['pExtraFields']['prezzo_affiliati'] = str_replace(",", ".", $_POST['pExtraFields']['prezzo_affiliati']);
    if(!is_numeric($_POST['pExtraFields']['prezzo_affiliati'])) {
        $price_error = true;
    }
}

if($_POST['pScontato']) {
    $_POST['pPrezzoScontato'] = str_replace(",", ".", $_POST['pPrezzoScontato']);
    if(!is_numeric($_POST['pPrezzoScontato'])) {
        $price_error = true;
    }
}
else {
    $_POST['pPrezzoScontato'] = 0;
}

if($price_error) {
    echo json_encode(array("status" => "price_numeric"));
    die();
}


$date_error = false;

$_POST['pPartenza']['data'] = formattaDataItaliana($_POST['pPartenza']['data']);
$_POST['pRitorno']['data'] = formattaDataItaliana($_POST['pRitorno']['data']);

if(!$_POST['pPartenza']['data'] || !$_POST['pRitorno']['data']) {
    $date_error = true;
}

if($_POST['pScadenza'])
{
    $_POST['pScadenza'] = formattaDataItaliana($_POST['pScadenza']);
    if(!$_POST['pScadenza']) {
        $date_error = true;
    }
}

if($_POST['pScontato']) {
    $_POST['pScadenzaSconto'] = formattaDataItaliana($_POST['pScadenzaSconto']);
    if(!$_POST['pScadenzaSconto']) {
        $date_error = true;
    }
} else {
    $_POST['pScadenzaSconto'] = '';
}

if($date_error) {
    echo json_encode(array("status" => "expire_not_valid"));
    die();
}

if(!empty($_POST['pExtraFields']['youtube_video'])) {
    $formatted_url = (new \MSFramework\utils())->getEmbedVideoUrl($_POST['pExtraFields']['youtube_video']);
    if(!$formatted_url) {
        echo json_encode(array("status" => "video_error"));
        die();
    }
    $_POST['pExtraFields']['youtube_video'] = $formatted_url;
}

$uploader = new \MSFramework\uploads('TRAVELAGENCY_PACKAGE');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$uploader = new \MSFramework\uploads('TRAVELAGENCY_PACKAGE_HOTEL');
$ary_hotel_files = $uploader->prepareForSave($_POST['pHotel']['foto']);
if($ary_hotel_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}
else {
    $_POST['pHotel']['foto'] = json_encode($ary_hotel_files);
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT gallery, hotel, nome, slug, descrizione_breve, descrizione_lunga FROM travelagency__packages WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pDescrizioneBreve'], 'oldValue' => $r_old_data['descrizione_breve']),
        array('currentValue' => $_POST['pDescrizioneLunga'], 'oldValue' => $r_old_data['descrizione_lunga']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

// Sistemo l'eventuale URL Tripadvisor per estrapolare esclusivamente l'ID
$trip_id = $_POST['pHotel']['tripadvisor_id'];
if(!empty($trip_id)) {

    preg_match_all('/-d([0-9]+)/m', $trip_id, $matches, PREG_SET_ORDER, 0);

    if($matches) {
        $trip_id = $matches[0][1];
    }
    else if($trip_id[0] == 'd') {
        $trip_id = ltrim($trip_id, 'd');
    }
    else if(!is_numeric($trip_id)) {
        $trip_id = '';
    }

}
$_POST['pHotel']['tripadvisor_id'] = $trip_id;

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
    "prezzo" => $_POST['pPrezzo'],
    "descrizione_breve" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione_breve'], $_POST['pDescrizioneBreve']),
    "descrizione_lunga" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione_lunga'], $_POST['pDescrizioneLunga']),
    "categoria" => $_POST['pCategoria'],
    "persone" => $_POST['pPersone'],
    "notti" => $_POST['pNotti'],
    "prezzo_scontato" => $_POST['pPrezzoScontato'],
    "scadenza_sconto" => $_POST['pScadenzaSconto'],
    "hotel" => json_encode($_POST['pHotel']),
    "destinazione" => $_POST['pDestinazione'],
    "partenza" => json_encode($_POST['pPartenza']),
    "ritorno" => json_encode($_POST['pRitorno']),
    "gallery" => json_encode($ary_files),
    "services" => implode(',', $_POST['pServicesAry']),
    "scadenza" => $_POST['pScadenza'],
    "data_partenza" => $_POST['pPartenza']['data'],
    "featured" => $_POST['pFeatured'],
    "extra_fields" => json_encode($_POST['pExtraFields'])
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO travelagency__packages ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE travelagency__packages SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
        $uploader->unlink($ary_hotel_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['gallery'], true));
    $uploader->unlink($ary_hotel_files, json_decode(json_decode($r_old_data['hotel'], true)['foto'], true));
}

$ref_id = ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : $_POST['pID']);
$MSFrameworkUrl->makeRedirectIfNeeded('travelagency-package-' . $ref_id, (new \MSFramework\TravelAgency\packages())->getURL($ref_id), ($db_action === 'update' ? (new \MSFramework\TravelAgency\packages())->getURL($r_old_data) : ''));

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();

function formattaDataItaliana($data) {
    $date_tmp = DateTime::createFromFormat('d/m/Y H:i', $data);
    if(!$date_tmp) {
        return false;
    }
    return $date_tmp->format('Y-m-d H:i');
}
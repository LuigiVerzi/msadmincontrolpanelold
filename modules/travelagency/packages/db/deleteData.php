<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
$hotel_images_to_delete = array();
foreach((new \MSFramework\TravelAgency\packages())->getPackageDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['gallery'], true));
    $hotel_images_to_delete = array_merge_recursive ($images_to_delete, json_decode(json_decode($r['hotel'], true)['foto'], true));
}

if($MSFrameworkDatabase->deleteRow("travelagency__packages", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('TRAVELAGENCY_PACKAGE'))->unlink($images_to_delete);
    (new \MSFramework\uploads('TRAVELAGENCY_PACKAGE_HOTEL'))->unlink($hotel_images_to_delete);
    $MSFrameworkUrl->deleteRedirectsByReferences('travelagency-package', $_POST['pID']);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


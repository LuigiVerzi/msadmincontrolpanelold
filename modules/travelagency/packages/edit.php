<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = array(
    'hotel' => array(),
    'partenza' => array(),
    'ritorno' => array(),
    'extra_fields' => array(),
);

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM travelagency__packages WHERE id = :id", array(":id" => $_GET['id']), true);
}

if($r) {
    if(json_decode($r['hotel'])) {
        $r['hotel'] = json_decode($r['hotel'], true);
    }
    if(json_decode($r['partenza'])) {
        $r['partenza'] = json_decode($r['partenza'], true);
    }
    if(json_decode($r['ritorno'])) {
        $r['ritorno'] = json_decode($r['ritorno'], true);
    }
    if(json_decode($r['extra_fields'])) {
        $r['extra_fields'] = json_decode($r['extra_fields'], true);
    }
}

$lista_servizi = (new \MSFramework\services())->getServiceDetails();

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Pacchetto</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome Pacchetto*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Slug*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                            <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Categoria*</label>
                                    <select id="categoria" name="categoria" type="text" class="form-control required">
                                        <option value="">Seleziona categoria...</option>
                                        <?php foreach((new \MSFramework\TravelAgency\categories())->getCategoryDetails() as $category) { ?>
                                            <option value="<?= $category['id']; ?>" <?= ($r['categoria'] == $category['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($category['nome']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="featured" class="" <?php if($r['featured']) { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Pacchetto in Evidenza</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Notti*</label>
                                    <input id="notti" name="notti" type="number" min="1" max="100" class="form-control required" value="<?php echo htmlentities($r['notti']) ?>">
                                </div>
                                <div class="col-sm-2">
                                    <label>Numero di Persone*</label>
                                    <input id="persone" name="persone" type="number" min="1" max="100" class="form-control required" value="<?php echo htmlentities($r['persone']) ?>">
                                </div>
                                <div class="col-sm-2">
                                    <label>Prezzo per Persona*</label>
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-eur"></i>
                                            </span>
                                        <input id="prezzo" name="prezzo" type="number" min="0" class="form-control required" value="<?php echo htmlentities($r['prezzo']) ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label>Scadenza Pacchetto</label>
                                    <div class="input-group date scadenza">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control" id="scadenza" value="<?php echo ($r['scadenza'] != '') ? date("d/m/Y H:i", strtotime($r['scadenza'])) : '' ?>">
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="sconto" class="" <?php if($r['prezzo_scontato'] > 0 && (strtotime($r['scadenza_sconto']) < 0 || strtotime($r['scadenza_sconto']) > time()) ) { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">In Offerta</span>
                                        </label>
                                    </div>
                                </div>

                            </div>

                            <div class="preferenze_sconto" style="display: <?= ($r['prezzo_scontato'] > 0 && (strtotime($r['scadenza_sconto']) < 0 || strtotime($r['scadenza_sconto']) > time()) ? 'block' : 'none' )?>;">
                                <div class="hr-line-dashed"></div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label>Prezzo Scontato *</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-eur"></i>
                                            </span>
                                            <input id="prezzo_scontato" name="prezzo_scontato" type="number" min="0" class="form-control required" value="<?php echo ($r['prezzo_scontato'] ? htmlentities($r['prezzo_scontato']) : '') ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Scadenza Offerta</label>
                                        <div class="input-group date scadenza">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                            <input type="text" class="form-control" id="scadenza_sconto" value="<?php echo ($r['scadenza_sconto'] != '' && strtotime($r['scadenza_sconto']) > 0) ? date("d/m/Y H:i", strtotime($r['scadenza_sconto'])) : '' ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Imposta applicata</label>
                                    <select id="imposta" name="imposta" class="form-control extra_fields">
                                        <option value="default">Usa impostazioni Predefinite</option>
                                        <option value="">- Nessuna imposta -</option>
                                        <?php foreach((new \MSFramework\Fatturazione\imposte())->getImposte() as $impK => $impV) { ?>
                                            <option value="<?php echo $impK ?>" <?php if($r['extra_fields']['imposta'] == $impK) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($impV[0], true) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                            <?php /* if($MSFrameworkCMS->checkExtraFunctionsStatus('affiliations')) { ?>
                                <h2 class="title-divider">Affiliati</h2>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Provvigione per gli Sponsor*</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-percent"></i>
                                            </span>
                                            <input id="provvigione_affiliati" name="provvigione_affiliati" type="number" min="0" class="form-control extra_fields required" value="<?php echo htmlentities($r['extra_fields']['provvigione_affiliati']) ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Sconto Clienti Sponsorizzati*</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-percent"></i>
                                            </span>
                                            <input id="sconto_sponsorizzati" name="sconto_sponsorizzati" type="number" min="0" class="form-control extra_fields required" value="<?php echo htmlentities($r['extra_fields']['sconto_sponsorizzati']) ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            <?php } */ ?>

                            <h2 class="title-divider">Descrizioni</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione_breve']) ?>"></i></span>
                                    <textarea id="descrizione_breve" name="descrizione_breve" class="form-control" rows="6"><?php echo $MSFrameworki18n->getFieldValue($r['descrizione_breve']) ?></textarea>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 30px">
                                <div class="col-sm-12">
                                    <label>Dettagli</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione_lunga']) ?>"></i></span>
                                    <textarea id="descrizione_lunga" name="descrizione_lunga"><?php echo $MSFrameworki18n->getFieldValue($r['descrizione_lunga']) ?></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Itinerario</h1>
                        <fieldset>
                            <h2 class="title-divider">Destinazione</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nome Destinazione *</label>
                                    <input id="nome_destinazione" name="nome_destinazione" type="text" class="form-control required" value="<?php echo htmlentities($r['destinazione']) ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <h2 class="title-divider">Partenza</h2>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Luogo*</label>
                                            <input id="partenza_luogo" name="luogo" type="text" class="form-control required" value="<?php echo htmlentities($r['partenza']['luogo']) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>Data*</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <input type="text" class="form-control required" id="partenza_data" value="<?php echo ($r['partenza']['data']) ? date("d/m/Y H:i", strtotime($r['partenza']['data'])) : '' ?>">
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <label>Luogo d'Arrivo*</label>
                                            <input id="partenza_arrivo" name="arrivo" type="text" class="form-control required" value="<?php echo htmlentities($r['partenza']['arrivo']) ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <h2 class="title-divider">Ritorno</h2>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Luogo*</label>
                                            <input id="ritorno_luogo" name="luogo" type="text" class="form-control required" value="<?php echo htmlentities($r['ritorno']['luogo']) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>Data</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <input id="ritorno_data" name="data" type="text" class="form-control" value="<?php echo ($r['ritorno']['data']) ? date("d/m/Y H:i", strtotime($r['ritorno']['data'])) : '' ?>">
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <label>Luogo d'Arrivo</label>
                                            <input id="ritorno_arrivo" name="arrivo" type="text" class="form-control required" value="<?php echo htmlentities($r['ritorno']['arrivo']) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="title-divider">Informazioni Hotel</h2>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nome Hotel*</label>
                                    <input id="nome_hotel" name="nome_hotel" type="text" class="form-control required" value="<?php echo htmlentities($r['hotel']['nome']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Location Hotel*</label>
                                    <input id="location_hotel" name="location_hotel" type="text" class="form-control required" value="<?php echo htmlentities($r['hotel']['location']) ?>">
                                </div>

                                <div class="col-sm-2">
                                    <label>Sistemazione</label>
                                    <input id="sistemazione_hotel" name="sistemazione_hotel" type="text" class="form-control required" value="<?php echo htmlentities($r['hotel']['sistemazione']) ?>">
                                </div>

                                <div class="col-sm-2">
                                    <label>Trattamento</label>
                                    <input id="trattamento_hotel" name="trattamento_hotel" type="text" class="form-control required" value="<?php echo htmlentities($r['hotel']['trattamento']) ?>">
                                </div>

                                <div class="col-sm-2">
                                    <label>URL TripAdvisor</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci l'URL di TripAdvisor (o l'ID, es. dXXXXXXX-) se desideri mostrare le recensioni. (Opzionale)"></i></span>
                                    <input id="tripadvisor_hotel" name="tripadvisor_hotel" type="text" placeholder="URL o ID (ES: dXXXXXXX-)" class="form-control" value="<?php echo htmlentities($r['hotel']['tripadvisor_id']) ?>">
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="hotel_image_label">Immagine Hotel</label>
                                    <?php (new \MSFramework\uploads('TRAVELAGENCY_PACKAGE_HOTEL'))->initUploaderHTML("hotel_foto", $r['hotel']['foto']); ?>
                                </div>
                                <style>
                                    .hotel_image_label,
                                    .hotel_image_label + .row {
                                        display: none !important;
                                    }
                                </style>
                            </div>

                        </fieldset>

                        <?php if($lista_servizi) { ?>
                            <h1>Servizi</h1>
                            <fieldset>
                                <h2 class="title-divider">Servizi disponibili per questa camera</h2>
                                <?php $sel_services = explode(',', $r['services']); ?>
                                <div class="row text-center">
                                    <?php foreach((new \MSFramework\services())->getServiceDetails() as $service) { ?>
                                        <div class="col-sm-3">
                                            <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($service['nome']) ?>"></i></span>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="<?php echo $service['id'] ?>" class="service_checkbox" <?php if(in_array($service['id'], $sel_services)) { echo "checked"; } ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;"><?php echo $MSFrameworki18n->getFieldValue($service['nome']) ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </fieldset>
                        <?php } ?>

                        <h1>Gallery</h1>
                        <fieldset>
                            <h2 class="title-divider">Immagini</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php (new \MSFramework\uploads('TRAVELAGENCY_PACKAGE'))->initUploaderHTML("images", $r['gallery']); ?>
                                </div>
                            </div>

                            <h2 class="title-divider">Video Youtube</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Link Youtube</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci l'URL del video di YouTube. (Opzionale)"></i></span>
                                    <input id="youtube_video" name="youtube_video" type="text" class="form-control extra_fields" value="<?php echo htmlentities($r['extra_fields']['youtube_video']) ?>">
                                </div>
                            </div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
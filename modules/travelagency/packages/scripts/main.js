$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('nome', 'slug', 'travelagency__packages', $('#record_id'));
    initOrak('images', 25, 500, 300, 200, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
    initOrak('hotel_foto', 1, 150, 150, 200, getOrakImagesToPreattach('hotel_foto'), false, ['image/jpeg', 'image/png']);
    initTinyMCE('#descrizione_breve, #descrizione_lunga');

    $('.input-group.date input').datetimepicker({
        keyboardNavigation: false,
        autoclose: true,
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('dp.change', function(e) {
    });

    $('#notti, #partenza_data').on('change', function () {
        if($('#notti').val().length && $('#partenza_data').val().length) {
            var data_partenza = $('#partenza_data').val();
            data_partenza = data_partenza.split("/");
            var time = data_partenza[2].split(' ');
            data_partenza[2] = time[0];
            time = time[1];
            if(data_partenza.length == 3) {
                var real_date = new Date(data_partenza[2] + '-' + data_partenza[1] + '-' + data_partenza[0] + ' ' + time);
                var ms = real_date.getTime() + (86400000 * $('#notti').val());
                var data_ritorno = new Date(ms);
                $('#ritorno_data').val(data_ritorno.getDate() + '/' + (data_ritorno.getMonth() + 1) + '/' +  data_ritorno.getFullYear() + ' ' + time);
            }
        }
    });

    $('#sconto').on('ifChanged', function () {
        if($(this).prop('checked')) {
            $('.preferenze_sconto').show();
        }
        else {
            $('.preferenze_sconto').hide();
        }
    });

}

function moduleSaveFunction() {
    var services_ary = new Array();
    $('.service_checkbox:checked').each(function(k, v) {
        services_ary.push($(this).attr('id'));
    });

    var extra_fields = {};
    $('.extra_fields').each(function () {
        var name = $(this).attr('name');
        extra_fields[name] = $(this).val();
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pSlug": $('#slug').val(),
            "pCategoria": $('#categoria').val(),
            "pNotti": $('#notti').val(),
            "pPersone": $('#persone').val(),
            "pPrezzo": $('#prezzo').val(),
            "pScadenza": $('#scadenza').val(),
            "pScontato": ($('#sconto:checked').length ? 1 : 0),
            "pPrezzoScontato": $('#prezzo_scontato').val(),
            "pScadenzaSconto": $('#scadenza_sconto').val(),
            "pDescrizioneBreve": tinymce.get('descrizione_breve').getContent(),
            "pDescrizioneLunga": tinymce.get('descrizione_lunga').getContent(),
            "pDestinazione": $('#nome_destinazione').val(),
            "pPartenza": {
                luogo: $('#partenza_luogo').val(),
                data: $('#partenza_data').val(),
                arrivo: $('#partenza_arrivo').val(),
            },
            "pRitorno": {
                luogo: $('#ritorno_luogo').val(),
                data: $('#ritorno_data').val(),
                arrivo: $('#ritorno_arrivo').val(),
            },
            "pHotel": {
                nome: $('#nome_hotel').val(),
                location: $('#location_hotel').val(),
                sistemazione: $('#sistemazione_hotel').val(),
                trattamento: $('#trattamento_hotel').val(),
                tripadvisor_id: $('#tripadvisor_hotel').val(),
                foto: composeOrakImagesToSave('hotel_foto')
            },
            "pImagesAry": composeOrakImagesToSave('images'),
            "pServicesAry": services_ary,
            "pFeatured": ($('#featured:checked').length ? 1 : 0),
            "pExtraFields": extra_fields
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "video_error") {
                bootbox.error("L'indirizzo del video inserito non è valido, inserisci un URL YouTube corretto.");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data non è valido. Impossibile continuare.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
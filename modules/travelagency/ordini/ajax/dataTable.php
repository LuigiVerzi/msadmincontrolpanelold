<?php
/**
 * MSAdminControlPanel
ecommerce_orders
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM travelagency__orders") as $r) {
    $info_fatturazione = json_decode($r['info_fatturazione'], true);
    $cart = json_decode($r['package_info'], true);

    if(json_decode($r["coupons_used"])) $r["coupons_used"] = json_decode($r["coupons_used"], true);
    else $r["coupons_used"] = array();

    $prezzo = ($cart['prezzo_scontato'] > 0 ? $cart['prezzo_scontato'] : $cart['prezzo']);
    $sconto = ($r["coupons_used"] ? $r['coupons_used']['discount'] : 0);

    $orde_status_color = array(
        "0" => "rgba(244,67,54,1)",
        "1" => "rgba(255,152,0,1)",
        "2" => "rgba(33,150,243,1)",
        "3" => "rgba(139,195,74,1)",
        "4" => "rgba(78,70,69,1)",
    );

    $array['data'][] = array(
        $r['id'],
        $info_fatturazione['nome'] . " " . $info_fatturazione['cognome'],
        ($sconto > 0 ? "<del>" . CURRENCY_SYMBOL . " " . number_format($prezzo, 2, ',', '.') . "</del> " . CURRENCY_SYMBOL . " " . number_format($prezzo-$sconto, 2, ',', '.') : CURRENCY_SYMBOL . " " . number_format($prezzo, 2, ',', '.')) . CURRENCY_SYMBOL,
        (new DateTime($r['order_date']))->format("d/m/Y H:i"),
        (new DateTime($r['order_edit_date']))->format("d/m/Y H:i"),
        '<span class="label" style="color: white; background: ' . $orde_status_color[$r['order_status']] . ';">' . (new \MSFramework\TravelAgency\orders)->getStatus($r['order_status']) . '</span>',
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

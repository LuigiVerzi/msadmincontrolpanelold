<?php
/**
 * MSAdminControlPanel
 * Date: 07/05/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$orderClass = new \MSFramework\TravelAgency\orders();

if(isset($_GET['id'])) {
    $r = $orderClass->getOrderDetails($_GET['id'])[$_GET['id']];
    $info_cliente = (new \MSFramework\customers())->getCustomerDataFromDB($r['user_id']);
    $info_cliente = (new \MSFramework\customers())->getCustomerDataFromDB($r['user_id']);

    $prezzo_pacchetto = ($r['package_info']['prezzo_scontato'] > 0 ? $r['package_info']['prezzo_scontato'] : $r['package_info']['prezzo']);

} else {
    header('Location: index.php');
    die();
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Data Ordine</label>
                                    <div><?php echo (new DateTime($r['order_date']))->format("d/m/Y H:i") ?></div>
                                </div>

                                <div class="col-sm-4">
                                    <label>Data ultimo aggiornamento</label>
                                    <div><?php echo (new DateTime($r['order_edit_date']))->format("d/m/Y H:i") ?></div>
                                </div>

                                <div class="col-sm-4">
                                    <label>Stato Ordine</label>
                                    <input type="hidden" id="original_order_status" value="<?php echo $r['order_status'] ?>" />
                                    <select id="order_status" name="order_status" class="form-control">
                                        <?php
                                        foreach((new \MSFramework\TravelAgency\orders)->getStatus() as $statK => $statV) {
                                            ?>
                                            <option value="<?php echo $statK ?>" <?php if($r['order_status'] == (string)$statK) { echo "selected"; } ?>><?php echo $statV ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <h2 class="title-divider">Cliente</h2>
                            <?php if($info_cliente) { ?>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Nome</label>
                                        <div><a id="customer_name" href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>customers/list/edit.php?id=<?php echo $info_cliente['id']; ?>"><?php echo $info_cliente['nome'] . ' ' . $info_cliente['cognome']; ?></a></div>
                                    </div>

                                    <div class="col-sm-4">
                                        <label>Email</label>
                                        <div id="customer_email"><?php echo $info_cliente['email'] ?></div>
                                    </div>

                                    <div class="col-sm-4">
                                        <label>Cellulare</label>
                                        <div><?php echo (!empty($info_cliente['telefono_cellulare']) ? $info_cliente['telefono_cellulare'] : (!empty($info_cliente['telefono_casa']) ? $info_cliente['telefono_casa'] : 'Non Specificato')); ?></div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <?php if($r['guest_email']) { ?>
                                    <div>L'utente ha effettuato l'acquisto in modalità <b>Ospite</b>.</div>
                                    <br>
                                    <label>Email</label>
                                    <div id="customer_email"><?php echo $r['guest_email']; ?></div>
                                <?php } else { ?>
                                <div>L'utente non risulta registrato, probabilmente il suo account è stato eliminato.</div>
                                <?php } ?>
                            <?php } ?>

                            <div class="row">
                                <div class="col-sm-12">
                                    <h2 class="title-divider">Fatturazione <a class="label label-info copy_order_info" href="#" style="font-size: 11px; top: -3px; position: relative;">Copia Indirizzo</a></h2>
                                    <div class="address_to_copy">
                                        <div><?php echo $r['info_fatturazione']['nome'] ?> <?php echo $r['info_fatturazione']['cognome'] ?></div>
                                        <div><?php echo $r['info_fatturazione']['indirizzo'] ?> <?php echo $r['info_fatturazione']['indirizzo_2'] ?></div>
                                        <div><?php echo $r['info_fatturazione']['cap'] ?> <?php echo $r['info_fatturazione']['comune'] ?></div>
                                        <div><?php echo $r['info_fatturazione']['citta'] ?></div>
                                        <div><?php echo $r['info_fatturazione']['telefono'] ?></div>
                                        <div><?php echo (new \MSFramework\geonames())->getCountryDetails($r['info_fatturazione']['stato'])[$r['info_fatturazione']['stato']]['name'] ?></div>
                                    </div>
                                    <p>
                                        <br>
                                        <b>Pagamento tramite:</b> <?= ucfirst($r["payment_type"]); ?>
                                    </p>
                                </div>
                            </div>

                            <?php if($r['info_viaggiatori']) { ?>
                                <h2 class="title-divider">Viaggiatori</h2>
                                <?php foreach($r['info_viaggiatori'] as $k => $viaggiatore) { ?>
                                    <h2>Viaggiatore <?= $k; ?></h2>
                                    <?php foreach($viaggiatore as $field => $v) { ?>
                                        <div><b><?= $orderClass->getTravelerFieldName($field); ?>:</b> <?= $v; ?></div>
                                    <?php } ?>
                                    <?php if($k < count($r['info_viaggiatori'])) { ?>
                                        <div class="hr-line-dashed"></div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>

                            <h2 class="title-divider">Dettagli Pacchetto</h2>

                            <div class="row">
                                <div class="col-md-1 col-sm-2">
                                    <?php if($r['package_info']['gallery_friendly']) { ?>
                                        <img src="<?= $r['package_info']['gallery_friendly'][0]['html']['thumb'] ?>" style="max-width: 120px; width: 100%;" />
                                    <?php } else if($r['package_info']['hotel']['foto'][0]) { ?>
                                        <img src="<?= $r['package_info']['hotel']['foto'][0] ?>" style="max-width: 120px; width: 100%;" />
                                    <?php } ?>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <label>Pacchetto</label>
                                    <div><?php echo $MSFrameworki18n->getFieldValue($r['package_info']['nome']); ?></div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <label>Prezzo</label>
                                    <div><?= CURRENCY_SYMBOL; ?> <?= number_format((new \MSFramework\Fatturazione\imposte())->getPriceToShow($prezzo_pacchetto, (isset($r['package_info']['extra_fields']['imposta']) && $r['package_info']['extra_fields']['imposta'] > 0 ? $r['package_info']['extra_fields']['imposta'] : ''))['no_tax'],2,',','.'); ?></div>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <label>Coupon</label>
                                    <div><?= ($r["coupons_used"] ? $r['coupons_used']['code'] . ' (- ' . CURRENCY_SYMBOL . ' ' . number_format($r['coupons_used']['discount'],2,',','.') . ')' : 'Nessuno'); ?></div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <label>Totale Pagato</label>
                                    <div><?= CURRENCY_SYMBOL; ?> <?= number_format( (new \MSFramework\Fatturazione\imposte())->getPriceToShow($prezzo_pacchetto, (isset($r['package_info']['extra_fields']['imposta']) && $r['package_info']['extra_fields']['imposta'] > 0 ? $r['package_info']['extra_fields']['imposta'] : ''))['tax'] - ($r['coupons_used'] ? $r['coupons_used']['discount'] : 0),2,',','.'); ?> <?= (isset($r['package_info']['extra_fields']['imposta'])  && $r['package_info']['extra_fields']['imposta'] > 0 ? '(imposta ' . $r['package_info']['extra_fields']['imposta'] . '% inclusa)' : '(Nessuna Imposta Applicata)'); ?></div>
                                </div>
                            </div>

                            <?php if($r['note']) { ?>
                                <h2 class="title-divider">Note dell'ordine</h2>
                                <?= $r['note']; ?>
                            <?php } ?>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 07/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);
if($db_action == "insert") {
    die();
}

$r_old = $MSFrameworkDatabase->getAssoc("SELECT order_status FROM travelagency__orders WHERE id = :id", array($_POST['pID']), true);
$array_to_save = array(
    "order_status" => $_POST['pOrderStatus'],
    "order_edit_date" => date("Y-m-d H:i:s"),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
$result = $MSFrameworkDatabase->pushToDB("UPDATE travelagency__orders SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

if($r_old['order_status'] != $_POST['pOrderStatus']) {
    (new MSFramework\TravelAgency\orders())->updateOrderStatus($_POST['pID'], $_POST['pOrderStatus']);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
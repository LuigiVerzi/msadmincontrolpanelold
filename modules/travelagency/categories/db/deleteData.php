<?php
/**
 * MSAdminControlPanel
 * Date: 17/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$r = $MSFrameworkDatabase->getAssoc("SELECT images, banner FROM travelagency__packages_categories WHERE id = :id", array(":id" => $_POST['pID']), true);

if($MSFrameworkDatabase->deleteRow("travelagency__packages_categories", "id", $_POST['pID'])) {

    foreach($MSFrameworkDatabase->getAssoc("SELECT id, category FROM travelagency__packages WHERE find_in_set(" . $_POST['pID'] . ", category)") as $p) {
        $cat_array = array_diff(explode(',', $p['category']), array($_POST['pID']));
        $MSFrameworkDatabase->pushToDB("UPDATE travelagency__packages SET category = :new_cats WHERE id = :id", array(":new_cats" => implode(',', $cat_array), ':id' => $p['id']));
    }

    (new \MSFramework\uploads('TRAVELAGENCY_PACKAGE_CATEGORY'))->unlink(json_decode($r['images'], true));
    (new \MSFramework\uploads('TRAVELAGENCY_PACKAGE_CATEGORY'))->unlink(json_decode($r['banner'], true));

    $MSFrameworkUrl->deleteRedirectsByReferences('travelagency-category', $_POST['pID']);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 01/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$r_old_data = $MSFrameworkDatabase->getAssoc("SELECT payment_data FROM " . FRAMEWORK_DB_NAME . ".ms_agency__payments_info WHERE user_id = :user_id", array(':user_id' => $MSFrameworkUsers->getUserDataFromSession('id')), true);

if(!$r_old_data) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

$can_save = true;
if($_POST['data']['mode'] === 'paypal') {

    if(!filter_var($_POST['data']['email'], FILTER_VALIDATE_EMAIL)) {
        die(json_encode(array('status' => 'mail_not_valid')));
    }

} else if($_POST['data']['mode'] === 'bonifico') {

} else if($_POST['data']['mode'] === 'cash') {

} else {
    $can_save = false;
}

foreach($_POST['data'] as $field) {
    if(empty($field)) $can_save = false;
}

if(!$can_save) {
    die(json_encode(array('status' => 'mandatory_data_missing')));
}

$array_to_save = array(
    "payment_data" => json_encode($_POST['data']),
    "user_id" => $MSFrameworkUsers->getUserDataFromSession('id')
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO " . FRAMEWORK_DB_NAME . ".ms_agency__payments_info ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE " . FRAMEWORK_DB_NAME . ".ms_agency__payments_info SET $stringForDB[1] WHERE user_id = '" . $MSFrameworkUsers->getUserDataFromSession('id') . "'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ''));
die();
?>
function initForm() {
    initClassicTabsEditForm();

    // PAGAMENTI WEB
    $('[name="paymentRadio"]').on('ifChanged', function (event) {

        $('.webPaymentBox').addClass('collapsed');

        console.log($(this));
        console.log($(this).closest('.webPaymentBox'));

        if($(this).is(':checked')) {
            $(this).closest('.webPaymentBox').removeClass('collapsed');
        }

    });
}

function moduleSaveFunction() {

    var web_payments_ary = {};

    if($('#enable_bonifico:checked').length) {
        web_payments_ary = {
            "mode": 'bonifico',
            "intestatario": $('#intestatario_bonifico').val(),
            "conto": $('#conto_bonifico').val(),
            "banca": $('#banca_bonifico').val(),
            "filiale": $('#filiale_bonifico').val(),
            "iban": $('#iban_bonifico').val(),
            "bic_swift": $('#bic_swift_bonifico').val(),
        };
    } else if($('#enable_paypal:checked').length) {
        web_payments_ary = {
            "mode": 'paypal',
            "email": $('#email_paypal').val()
        };
    } else if($('#enable_cash:checked').length) {
        web_payments_ary = {
            "mode": 'cash'
        };
    }
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            'data': web_payments_ary
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            }
            else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
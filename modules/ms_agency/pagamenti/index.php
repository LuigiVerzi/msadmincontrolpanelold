<?php
/**
 * MSAdminControlPanel
 * Date: 03/06/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$payment_data = (new \MSFramework\MSAgency\earnings())->getUserPaymentsData($MSFrameworkUsers->getUserDataFromSession('id'));
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Dati di pagamento</h1>
                        <fieldset>

                            <div class="ibox <?php if($payment_data['mode'] != "bonifico") { echo "collapsed"; } ?> webPaymentBox">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -5px 10px 0 -3px;">
                                        <input type="radio" name="paymentRadio" id="enable_bonifico" class="webPayments_settings" <?php if($payment_data['mode'] == "bonifico") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Utilizza Bonifico
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Intestatario Conto</label>
                                            <input id="intestatario_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payment_data['intestatario']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Numero Conto</label>
                                            <input id="conto_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payment_data['conto']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Nome Banca</label>
                                            <input id="banca_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payment_data['banca']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Filiale</label>
                                            <input id="filiale_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payment_data['filiale']) ?>">
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>IBAN</label>
                                            <input id="iban_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payment_data['iban']) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>BIC / Swift</label>
                                            <input id="bic_swift_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payment_data['bic_swift']) ?>">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="ibox <?php if($payment_data['mode'] != "paypal") { echo "collapsed"; } ?> webPaymentBox">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -5px 10px 0 -3px;">
                                        <input type="radio" name="paymentRadio" id="enable_paypal" class="webPayments_settings" <?php if($payment_data['mode'] == "paypal") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Utilizza PayPal
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>Email PayPal</label>
                                            <input id="email_paypal" type="text" class="form-control" value="<?php echo htmlentities($payment_data['email']) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="ibox <?php if($payment_data['mode'] != "cash") { echo "collapsed"; } ?> webPaymentBox">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -5px 10px 0 -3px;">
                                        <input type="radio" name="paymentRadio" id="enable_cash" class="webPayments_settings" <?php if($payment_data['mode'] == "cash") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Utilizza Contanti
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <!-- <div class="alert alert-warning">

                                    </div> -->
                                </div>
                            </div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
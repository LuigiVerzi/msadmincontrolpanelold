<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSAgencyServices = new \MSFramework\MSAgency\services();

$serviceCategories = $MSAgencyServices->getCategoryDetails();

$time_conversion = array(
    'days' => array('giorn', 'o', 'i'),
    'months' => array('mes', 'e', 'i'),
    'years' => array('ann', 'o', 'i')
);

$stringToTranslate = array(
    'descr', 'descr_title', 'long_descr', 'nome'
);

$commercial_mode_enabled = ($MSFrameworkUsers->getUserDataFromSession('userlevel') == 0 || $MSAgencyServices->userViewMode !== 'customer' || CUSTOMER_DOMAIN_INFO['id'] === 41);

$info_fatturazione = array();
if(!$commercial_mode_enabled) {

    $userData = $MSFrameworkUsers->getUserDataFromDB($MSFrameworkUsers->getUserDataFromSession('id'));
    $siteData = $MSFrameworkCMS->getCMSData('site');

    $geo_data = json_decode($siteData['geo_data'], true);

    $info_fatturazione = array(
        'id' => $userData['id'],

        'nome' => $userData['nome'],
        'cognome' => $userData['cognome'],
        'email' => $userData['email'],
        'cellulare' => $userData['cellulare'],

        // Fatturazione
        'ragione_sociale' => $siteData['rag_soc'],
        'piva' => $siteData['piva'],

        // Indirizzi
        'stato' => (new \MSFramework\geonames())->getCountryDetails($geo_data['stato'])['name'],
        'regione' => (new \MSFramework\geonames())->getDettagliRegione($geo_data['regione'])['name'],
        'citta' => (new \MSFramework\geonames())->getDettagliProvincia($geo_data['provincia'])['name'],
        'comune' => (new \MSFramework\geonames())->getDettagliComune($geo_data['comune'])['name'],
        'indirizzo' => $geo_data['indirizzo'],
        'cap' => $geo_data['cap'],
    );
}
?>

<style>
    .checkoutStep  .ibox-content > .row {
        display: flex;
        flex-wrap: wrap;
        align-items: stretch;
    }

    .checkoutStep  .ibox-content > .row > [class*="col-"] {
        /* height: 100%; */
        align-items: stretch;
        margin-bottom: 15px;
    }

    .checkoutStep  .ibox-content > .row > [class*="col-"] > .ibox {
        height: 100%;
        position: relative;
        padding-bottom: 85px !important;
    }

    .checkoutStep  .ibox-content > .row > [class*="col-"] > .ibox > .ibox-content {
        height: 100%;
    }

    .checkoutStep  .ibox-content > .row > [class*="col-"] > .ibox .ibox-price {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
    }
</style>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
            <div class="col-sm-7">
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12">

                    <div id="services_list" class="checkoutStep active" data-step="1">
                        <div class="ibox">
                            <div class="ibox-content filterBtns" style="padding-top: 0;">
                                <?php
                                $k = 0;
                                foreach((new \MSFramework\MSAgency\services())->getCategoryDetails() as $category_id => $categoryDetail) {
                                    $k++;
                                    echo '<span class="btn ' . ($k > 1 ? 'btn-outline' : '') . ' btn-success categoryFilterBtn" data-id="' . $category_id . '" onclick="filterService(' . $category_id . ');" style="margin-top: 15px;">' . $MSFrameworki18n->getFieldValue($categoryDetail['nome']) . '</span> ';
                                }
                                ?>
                            </div>
                            <div class="ibox-content" style="padding-bottom: 0; padding-top: 0;">
                                <h1 class="title-divider" style="margin-top: 0px;">Servizi</h1>
                                <div class="row">
                                    <?php foreach((new \MSFramework\MSAgency\services())->getServiceDetails() as $service_id => $serviceDetail) {
                                        foreach($stringToTranslate as $k) {
                                            $serviceDetail[$k] = $MSFrameworki18n->getFieldValue($serviceDetail[$k]);
                                        }
                                        $serviceDetail['type'] = 'service';
                                        $serviceDetail['id'] = $service_id;
                                        $serviceDetail['pay_type'] = '';
                                        $serviceDetail['pay_price'] = ($serviceDetail['quote_needed'] ? 'Richiedi preventivo': $serviceDetail['prezzo']);
                                        if(!$serviceDetail['quote_needed']) $serviceDetail['pay_type'] = '/ ' . ($serviceDetail['una_tantum'] ? 'Una tantum' : $serviceDetail['duration']['value'] . ' ' . $time_conversion[$serviceDetail['duration']['type']][0] . $time_conversion[$serviceDetail['duration']['type']][($serviceDetail['duration']['value'] == 1 ? 1 : 2)]);
                                        ?>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="ibox" style="padding: 0; margin-bottom: 15px;">
                                                <div class="ibox-content product-box" style="padding: 0;">
                                                    <div class="serviceBox serviceElement" style="text-decoration: none; color: inherit;" data-category="<?= htmlentities($serviceDetail['category']); ?>" data-id="<?= $service_id; ?>">
                                                        <a class="product-name" onclick="loadServiceDetails('service', <?= htmlentities($serviceDetail['id']); ?>);">
                                                            <img src="<?= $serviceDetail['gallery_friendly'][0]['html']['main']; ?>" class="img-responsive center-block m-t">
                                                        </a>
                                                        <div class="product-desc">
                                                            <a class="product-name" onclick="loadServiceDetails('service', <?= htmlentities($serviceDetail['id']); ?>);"><?= htmlentities($MSFrameworki18n->getFieldValue($serviceDetail['descr_title'])); ?></a>
                                                            <?= $MSFrameworki18n->getFieldValue($serviceDetail['descr']); ?>
                                                        </div>
                                                        <div class="bg-muted ibox-price" style="padding: 15px;">
                                                            <div class="text-center">
                                                                <?php if($serviceDetail['quote_needed']) { ?>
                                                                    <span class="m-n h3 block">Richiesto preventivo</span>
                                                                <?php } else { ?>
                                                                    <span class="m-n h3 block">
                                                                        <?= number_format($serviceDetail['prezzo'], 2, ',', '.'); ?> <small><?= CURRENCY_SYMBOL; ?></small>
                                                                        <small class="text-info"><?= ($serviceDetail['una_tantum'] ? 'una tantum' : '/' . $serviceDetail['duration']['value'] . ' ' . $time_conversion[$serviceDetail['duration']['type']][0] . $time_conversion[$serviceDetail['duration']['type']][($serviceDetail['duration']['value'] == 1 ? 1 : 2)]); ?></small>
                                                                    </span>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="m-t">
                                                                <a class="btn btn-block btn-success addToCartBtn" onclick="addServiceToCart(<?= htmlentities($serviceDetail['id']); ?>);">Aggiungi all'ordine</a>
                                                            </div>
                                                            <div style="clear: both;"></div>
                                                        </div>
                                                        <textarea class="elementJSON" style="display: none;"><?= json_encode($serviceDetail); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                            <div class="ibox-content" style="padding-bottom: 0; padding-top: 0;">
                                <h1 class="title-divider" style="margin-top: 0px;">Pacchetti</h1>
                                <div class="row">
                                    <?php foreach((new \MSFramework\MSAgency\services())->getPackageDetails() as $package_id => $packageDetail) {
                                        foreach($stringToTranslate as $k) {
                                            $packageDetail[$k] = $MSFrameworki18n->getFieldValue($packageDetail[$k]);
                                        }
                                        $packageDetail['type'] = 'package';
                                        $packageDetail['id'] = $package_id;
                                        $packageDetail['pay_price'] = $packageDetail['prezzo'] . ' <small>' . CURRENCY_SYMBOL . '</small>';
                                        $packageDetail['pay_type'] = '/' . ($packageDetail['una_tantum'] ? 'Una tantum' : $packageDetail['duration']['value'] . ' ' . $time_conversion[$packageDetail['duration']['type']][0] . $time_conversion[$packageDetail['duration']['type']][($packageDetail['duration']['value'] == 1 ? 1 : 2)]);
                                        ?>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="ibox" style="padding: 0; margin-bottom: 15px;">
                                                <div class="ibox-content product-box" style="padding: 0;">
                                                    <div class="serviceBox packageElement" style="text-decoration: none; color: inherit;" data-category="<?= htmlentities($packageDetail['category']); ?>" data-id="<?= $package_id; ?>">
                                                        <a class="product-name" onclick="loadServiceDetails('package', <?= htmlentities($packageDetail['id']); ?>);">
                                                            <img src="<?= $packageDetail['gallery_friendly'][0]['html']['main']; ?>" class="img-responsive center-block m-t">
                                                        </a>
                                                        <div class="product-desc">
                                                            <a class="product-name" onclick="loadServiceDetails('package', <?= htmlentities($packageDetail['id']); ?>);"><?= htmlentities($MSFrameworki18n->getFieldValue($packageDetail['descr_title'])); ?></a>
                                                            <?= $MSFrameworki18n->getFieldValue($packageDetail['descr']); ?>
                                                        </div>
                                                        <div class="bg-muted ibox-price" style="padding: 15px;">
                                                            <div class="text-center">
                                                                <span class="m-n h3 block">
                                                                    <?= number_format($packageDetail['prezzo'], 2, ',', '.'); ?> <small><?= CURRENCY_SYMBOL; ?></small>
                                                                    <small class="text-info"><?= ($packageDetail['una_tantum'] ? 'una tantum' : '/' . $packageDetail['duration']['value'] . ' ' . $time_conversion[$packageDetail['duration']['type']][0] . $time_conversion[$packageDetail['duration']['type']][($packageDetail['duration']['value'] == 1 ? 1 : 2)]); ?></small>
                                                                </span>
                                                            </div>
                                                            <div class="m-t">
                                                                <a class="btn btn-block btn-success addToCartBtn" onclick="addPackageToCart(<?= htmlentities($packageDetail['id']); ?>);">Aggiungi all'ordine</a>
                                                            </div>
                                                            <div style="clear: both;"></div>
                                                        </div>
                                                        <textarea class="elementJSON" style="display: none;"><?= json_encode($packageDetail); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="customer_data" class="checkoutStep" data-step="2" style="display: none;">
                        <div class="ibox">
                            <div class="ibox-content">
                                <a href="#" onclick="undoCheckout();" class="btn btn-default"><i class="fa fa-arrow-left"></i> Torna indietro</a>
                            </div>
                            <div class="ibox-content" style="padding-top: 0;">
                                <h1 class="title-divider" style="margin-top: 0px;">Dati del cliente</h1>

                                <div id="customerInfo">

                                    <input type="hidden" id="customer_id" value="<?= $info_fatturazione['id']; ?>">

                                    <?php if($commercial_mode_enabled) { ?>
                                        <div class="data" style="display: none; margin-bottom: 15px; float: left;"></div>
                                        <a href="#" class="changeCustomer btn btn-sm btn-default createCustomer">
                                            Crea nuovo cliente
                                        </a>
                                        <a href="#" class="changeCustomer btn btn-sm btn-default selectCustomer" style="margin-right: 5px;">
                                            Seleziona cliente
                                        </a>
                                        <a href="#" class="editCustomer btn btn-warning btn-sm btn-outline" style="margin-right: 5px; display: none;">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    <?php } ?>
                                </div>
                                <div style="clear: both;"></div>

                                <form id="info_fatturazione" style="background: #fafafa; margin-top: 15px; padding: 15px 15px 0; border: 1px solid #ececec; display: <?= ($commercial_mode_enabled ? 'none' : 'block'); ?>;">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Nome</label>
                                            <input name="nome" type="text" class="form-control required" value="<?php echo $info_fatturazione['nome'] ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Cognome</label>
                                            <input name="cognome" type="text" class="form-control required" value="<?php echo $info_fatturazione['cognome'] ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Email</label>
                                            <input name="email" type="email" class="form-control required" value="<?php echo $info_fatturazione['email'] ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Cellulare</label>
                                            <input name="cellulare" type="text" class="form-control required" value="<?php echo $info_fatturazione['cellulare'] ?>">
                                        </div>
                                    </div>

                                    <h2 class="title-divider">Dati di fatturazione</h2>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Ragione Sociale</label>
                                            <input name="ragione_sociale" type="text" class="form-control" value="<?php echo $info_fatturazione['ragione_sociale'] ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Partita IVA</label>
                                            <input name="piva" type="text" class="form-control" value="<?php echo $info_fatturazione['piva'] ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Codice Fiscale</label>
                                            <input name="cf" type="text" class="form-control" value="<?php echo $info_fatturazione['cf'] ?>">
                                        </div>
                                    </div>

                                    <h2 class="title-divider">Indirizzo</h2>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Stato</label>
                                            <select class="form-control" name="stato">
                                                <option value=""><?= $MSFrameworki18n->gettext('Scegli'); ?>...</option>
                                                <?php foreach((new \MSFramework\geonames)->getCountryDetails($stati_disponibili, 'geonameId, name, fips_code') as $countryK => $countryV) { ?>
                                                    <option value="<?php echo $countryV['name'] ?>" <?php if($countryV['name'] == $info_fatturazione['stato'] || (!$info_fatturazione['stato'] && $countryV['fips_code'] === 'IT')) { echo "selected"; } ?>><?php echo $countryV['name'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Regione</label>
                                            <input name="regione" type="text" class="form-control" value="<?php echo $info_fatturazione['regione'] ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Provincia</label>
                                            <input name="citta" type="text" class="form-control" value="<?php echo $info_fatturazione['citta'] ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Comune</label>
                                            <input name="comune" type="text" class="form-control" value="<?php echo $info_fatturazione['comune'] ?>">
                                        </div>

                                        <div class="col-sm-9">
                                            <label>Indirizzo</label>
                                            <input name="indirizzo" type="text" class="form-control" value="<?php echo $info_fatturazione['indirizzo'] ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>CAP</label>
                                            <input name="cap" type="text" class="form-control" value="<?php echo $info_fatturazione['cap'] ?>">
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div id="customer_data" class="checkoutStep" data-step="3" style="display: none;">
                        <div class="ibox">
                            <div class="ibox-content">
                                <a href="#" onclick="undoCheckout();" class="btn btn-default"><i class="fa fa-arrow-left"></i> Torna indietro</a>
                            </div>
                            <div class="ibox-content" style="padding-top: 0;">
                                <h1 class="title-divider" style="margin-top: 0px;">Note dell'ordine</h1>

                                <div class="alert alert-warning" id="quote_needed_alertbox">
                                    <h3 style="margin-bottom: 5px;">Preventivo necessario</h3>
                                    <p>Il seguente ordine ha bisogno di essere preventivato prima dell'acquisto. Inserisci eventuali note.</p>
                                </div>

                                <textarea id="order_note" class="form-control"></textarea>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="ibox" id="carrello">
                        <div class="ibox-title">
                            <h5>Ordine</h5>
                        </div>
                        <div class="ibox-content" id="cart_content">

                        </div>

                        <div class="ibox-content customerNotification" style="display: none;">
                            <div class="styled-checkbox form-control">
                                <label style="width: 100%;">
                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                        <input type="checkbox" id="customer_notification" checked>
                                        <i></i>
                                    </div>
                                    <span style="font-weight: normal;">Invia notifiche al cliente</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<textarea id="cart_empty_tpl" style="display: none;">
    <span class="text-muted">Il carrello non contiene ancora nessun servizio.</span>
</textarea>
<textarea id="cart_btn_continue_tpl" style="display: none;">
    <i class="fa fa-shopping-cart"></i> <span>Prosegui</span>
</textarea>
<textarea id="cart_btn_purchase_tpl" style="display: none;">
    <i class="fa fa-credit-card"></i> <span>Completa ordine</span>
</textarea>
<textarea id="cart_base_tpl" style="display: none;">
    <div class="cartContent">{content}</div>
    <span>Totale</span>
    <h2 class="font-bold cartTotal">{total}</h2>
    <div class="m-t-sm">
        <a href="#" class="btn btn-primary btn-block btn-lg" id="continue_checkout_btn" onclick="continueShopping()"></a>
    </div>
</textarea>
<textarea id="cart_single_tpl" style="display: none;">
<div class="singleCartProduct">
    <a href="#" class="pull-right btn btn-success btn-outline btn-sm" onclick="loadServiceDetails('{type}', {id});"><i class="fa fa-info-circle" aria-hidden="true"></i> Info</a>
    <a href="#" class="product-name"> {descr_title}</a>
    <div class="small m-t-xs">
        <b>{pay_price}</b> {pay_type}
    </div>
    <div style="clear: both;"></div>
    <div class="hr-line-dashed"></div>
</div>
</textarea>

<script>
    globalInitForm();
</script>
</body>
</html>
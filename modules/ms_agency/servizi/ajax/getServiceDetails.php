<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$service_data = $_POST['data'];
$intoCart = $_POST['intoCart'];

$uploads = new \MSFramework\uploads(($service_data['type'] === 'service' ? 'SERVICES' : 'PACKAGES'));
?>

<div class="product-detail">
    <div class="row">
        <div class="col-md-8">
            <div class="margin: 0 0 15px">
                <?php if(json_decode($service_data['banner'], true)) { ?>
                    <img class="img-responsive" src="<?= $uploads->path_html; ?><?= json_decode($service_data['banner'], true)[0]; ?>">
                <?php } ?>
                <?= $service_data['long_descr']; ?>
            </div>
        </div>
        <div class="col-md-4">

            <h2 class="font-bold m-b-xs">
                <?= $service_data['descr_title']; ?>
            </h2>

            <hr>

            <div id="serviceDetails">
                <h4>Descrizione</h4>
                <div class="small text-muted">
                    <?= $service_data['descr']; ?>
                </div>

                <hr>

                <div>

                    <?php if($intoCart) { ?>
                        <a class="btn btn-block btn-primary addToCartBtn" onclick="add<?= ($service_data['type'] === 'service' ? 'Service' : 'Package'); ?>ToCart(<?= htmlentities($service_data['id']); ?>); window.serviceDetailsModal.modal('hide');"><i class="fa fa-check"></i> Rimuovi dall'ordine</a>
                    <?php } else { ?>
                        <a class="btn btn-block btn-success addToCartBtn" onclick="add<?= ($service_data['type'] === 'service' ? 'Service' : 'Package'); ?>ToCart(<?= htmlentities($service_data['id']); ?>); window.serviceDetailsModal.modal('hide');">Aggiungi all'ordine</a>
                    <?php } ?>

                    <button class="btn btn-default btn-block" onclick="window.serviceDetailsModal.modal('hide');">Chiudi</button>
                </div>
            </div>

        </div>
    </div>
</div>
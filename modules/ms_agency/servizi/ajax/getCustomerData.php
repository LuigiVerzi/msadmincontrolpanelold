<?php
/**
 * MSAdminControlPanel
 * Date: 31/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$id = $_POST['id'];

$customer_data = (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))->getCustomerDataFromDB($id);

if($customer_data) {
    $dati_fatturazione = json_decode($customer_data['dati_fatturazione'], true);

    $dati_fatturazione['nome'] = $customer_data['nome'];
    $dati_fatturazione['cognome'] = $customer_data['cognome'];
    $dati_fatturazione['email'] = $customer_data['email'];

    $dati_fatturazione['comune'] = $dati_fatturazione['citta'];
    $dati_fatturazione['citta'] = $dati_fatturazione['provincia'];

    unset($dati_fatturazione['provincia']);

    if(empty($dati_fatturazione['cellulare'])) {
        $dati_fatturazione['cellulare'] = $customer_data['telefono_cellulare'];
    }

    die(json_encode(array(
        'id' => $customer_data['id'],
        'addresses' => $dati_fatturazione,
        'preview' => $customer_data['html_preview']
    )));

}
<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

// Salvo (o aggiorno) l'utente su Marketing Studio
$updateUser = (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))
    ->registerUser($_POST['customer_data']['nome'], $_POST['customer_data']['cognome'], $_POST['customer_data']['email'], '', $_POST['customer_data']['cellulare'], '', array('dati_fatturazione' => $_POST['customer_data']));

if(!$updateUser) {
    echo json_encode(array("status" => "query_error"));
    die();
}

// Ricalcolo il totale del carrello per sicurezza

$services_ids = array();
$packages_ids = array();

foreach($_POST['cart']['products'] as $p) {
    if($p['type'] == 'service') $services_ids[] = $p['id'];
    else $packages_ids[] = $p['id'];
}

$cart = array();

if($services_ids) {
    foreach((new \MSFramework\MSAgency\services())->getServiceDetails($services_ids) as $p_id => $p) $cart['service_' . $p_id] = $p;
}

if($packages_ids) {
    foreach((new \MSFramework\MSAgency\services())->getPackageDetails($packages_ids) as $p_id => $p) $cart['package_' . $p_id] = $p;
}

$cart_total = 0.0;
foreach($cart as $product) {
    if($product['quote_needed'] == 0 && is_numeric($cart_total)) {
        $cart_total += $product['prezzo'];
    } else {
        $cart_total = false;
    }
}
if(!$cart_total) $cart_total = 0;

$array_to_save = array(
    "customer_data" => json_encode($_POST['customer_data']),
    "customer_reference" => $updateUser,
    "cart" => json_encode($cart),
    "seller" => $MSFrameworkUsers->getUserDataFromSession('id'),
    "seller_commission" => (new \MSFramework\MSAgency\earnings())->sellerCommission,
    "note" => $_POST['note'],
    "origin_reference" => $_SESSION['db']
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
$result = $MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`ms_agency__orders` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

$order_id = $MSFrameworkDatabase->lastInsertId();

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$ary_to_return = array(
    'status' => 'ok',
    'id' => $MSFrameworkDatabase->lastInsertId()
);

// Notifico gli admin dell'acquisto
(new \MSFramework\MSAgency\emails())->sendMail(
    '[msagency][admin]new-order',
    array(
        'order' => (new \MSFramework\MSAgency\orders())->getOrderDetails($ary_to_return['id'])[$ary_to_return['id']]
    )
);

// Invio il resoconto al cliente
(new \MSFramework\MSAgency\emails())->sendMail(
    '[msagency][customer]order-summary',
    array(
        'order' => (new \MSFramework\MSAgency\orders())->getOrderDetails($ary_to_return['id'])[$ary_to_return['id']]
    )
);

// Invio il resoconto al venditore
(new \MSFramework\MSAgency\emails())->sendMail(
    '[msagency][seller]order-summary',
    array(
        'order' => (new \MSFramework\MSAgency\orders())->getOrderDetails($ary_to_return['id'])[$ary_to_return['id']]
    )
);

if(!(float)$_POST['cart']['total']) {
    $ary_to_return['action'] = 'quote_needed';
}

$ary_to_return['redirect_url'] = $MSSoftwareModules->getModuleUrlByID('ms_agency_ordini') . 'edit.php?id=' . $order_id;

echo json_encode($ary_to_return);
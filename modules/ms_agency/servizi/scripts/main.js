window.currentServiceCart = {
    products: {},
    total: {}
};

function initForm() {
    initClassicTabsEditForm();
    initTinyMCE($('#order_note'));
    filterService($('.categoryFilterBtn').first().data('id'));

    /* INIZIALIZZO IL FORM */
    $('#info_fatturazione').validate({
        rules: {
            field: {
                required: true,
                email: true
            }
        }
    });

    /* CARRELLO */
    $('#cart_content').html($('#cart_empty_tpl').val());

    /* INDIRIZZI */
    $('.selectCustomer').on('click', function (e) {
        e.preventDefault();

        showFastPicker('manage_customers', function (row) {

            $('.changeCustomer').css('float', 'right');
            $('.editCustomer').show().html('Modifica cliente').css('float', 'right');

            updateCustomerAddresses(row.DT_RowId);

            toastr['success']('Cliente selezionato con successo');
            $('#fastModulePicker').modal('hide');
        }, 'cliente', 41);

    });

    $('.createCustomer').on('click', function (e) {
        e.preventDefault();
        showFastEditor('manage_customers', '', function (results) {
            updateCustomerAddresses(results.id);
            toastr['success']('Cliente creato con successo');
            $('#fastModuleEditor').modal('hide');
        }, 41);
    });

    $('.editCustomer').on('click', function (e) {
        e.preventDefault();

        showFastEditor('manage_customers', $('#customer_id').val(), function (results) {
            updateCustomerAddresses($('#customer_id').val());
            toastr['success']('Cliente aggiornato con successo');
            $('#fastModuleEditor').modal('hide');
        }, 41);
    });

    refreshCart();

    if(window.location.hash.length && parseInt(window.location.hash.replace('#', '')) > 0) {
        filterService(parseInt(window.location.hash.replace('#', '')));
    }
}

/* SERVIZI */
function loadServiceDetails(type, id) {
    if(window.serviceDetailsModal) {
        window.serviceDetailsModal.modal('hide');
    }

    var data = JSON.parse($('.' + type + 'Element[data-id="' + id + '"] .elementJSON').val());

    $.ajax({
        url: "ajax/getServiceDetails.php",
        type: "POST",
        data: {
            data: data,
            intoCart: (window.currentServiceCart.products[type + '_' + id] ? 1 : 0)
        },
        async: true,
        dataType: "text",
        success: function (html) {

            window.serviceDetailsModal = bootbox.dialog({
                title: 'Dettagli',
                size: 'large',
                message: html
            });
        }
    });
}
function filterService(category) {
    $('.categoryFilterBtn').addClass('btn-outline');
    $('.categoryFilterBtn[data-id="' + category + '"]').removeClass('btn-outline');

    $('#services_list .serviceBox').closest('.ibox').parent().hide();
    $('#services_list .serviceBox[data-category="' + category + '"]').closest('.ibox').parent().show();

    $('#services_list .title-divider').closest('.ibox-content').show();
    $('#services_list .title-divider').closest('.ibox-content').each(function () {
        if($(this).find('.product-box:visible').length) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

/* CARRELLO */
function addServiceToCart(id) {
    if(typeof(window.currentServiceCart.products['service_' + id]) !== 'undefined' && window.currentServiceCart.products['service_' + id]) {
        delete window.currentServiceCart.products['service_' + id];
    } else {
        var serviceData = JSON.parse($('.serviceElement[data-id="' + id + '"] .elementJSON').val());
        window.currentServiceCart.products['service_' + id] = serviceData;
    }

    refreshCart();
}
function addPackageToCart(id) {

    if(typeof(window.currentServiceCart.products['package_' + id]) !== 'undefined' && window.currentServiceCart.products['package_' + id]) {
        delete window.currentServiceCart.products['package_' + id];
    } else {
        var serviceData = JSON.parse($('.packageElement[data-id="' + id + '"] .elementJSON').val());
        window.currentServiceCart.products['package_' + id] = serviceData;
    }

    refreshCart();
}
function refreshCart() {
    var contentHtml = '';
    var price = 0.0;

    $('.addToCartBtn')
        .attr('class', 'btn btn-block btn-success addToCartBtn')
        .html('Aggiungi all\'ordine');

    Object.keys(window.currentServiceCart.products).forEach(function (element_id) {
        var productTpl = $('#cart_single_tpl').val();

        if(!isNaN(parseFloat(window.currentServiceCart.products[element_id]['prezzo']))) {
            if(window.currentServiceCart.products[element_id]['quote_needed'] !== 1 && !isNaN(price)) {
                price = parseFloat(price) + parseFloat(window.currentServiceCart.products[element_id]['prezzo']);
            } else {
                price = 'Richiedi preventivo';
            }
        }

        if(isNaN(price)) {
            window.currentServiceCart.total = 0;
            $('#quote_needed_alertbox').show();
        } else {
            window.currentServiceCart.total = price;
            $('#quote_needed_alertbox').hide();
        }

        Object.keys(window.currentServiceCart.products[element_id]).forEach(function (key) {
            productTpl = productTpl.replace('{' + key + '}', window.currentServiceCart.products[element_id][key]);
        });

        contentHtml += productTpl;

        // Aggiorno il pulsante
        $('.' + window.currentServiceCart.products[element_id].type + 'Element[data-id="' + window.currentServiceCart.products[element_id]['id'] + '"] .addToCartBtn')
            .attr('class', 'btn btn-block btn-primary addToCartBtn')
            .html('<i class="fa fa-check"></i> Rimuovi dall\'ordine');
    });

    $('#cart_content').html($('#cart_base_tpl').val()
        .replace('{content}', contentHtml)
        .replace('{total}', ((!isNaN(parseFloat(price)) ? price.toFixed(2).replace('.', ',') + ' <small><?= CURRENCY_SYMBOL; ?></small>' : price)))
    ).find('#continue_checkout_btn')
        .html($(($('.checkoutStep.active').data('step') === $('.checkoutStep').last().data('step') ? '#cart_btn_purchase_tpl' : '#cart_btn_continue_tpl')).val());

    if($('.checkoutStep.active').data('step') === $('.checkoutStep').last().data('step')) {
        $('.customerNotification').show();
    } else {
        $('.customerNotification').hide();
    }

}

/* CHECKOUT */
function continueShopping() {

    var currentStep = $('.checkoutStep.active').data('step');
    var canProceed = true;

    if(currentStep === 1) {
        if(!Object.keys(window.currentServiceCart.products).length) {
            toastr['error']("Per proseguire con l'acquisto hai bisogno di inserire almeno un prodotto nel carrello.");
            canProceed = false;
        }
    } else if(currentStep === 2) {
        if(!$('#info_fatturazione').valid() || !$('#customer_id').val().length) {
            toastr['error']("Per proseguire con l'acquisto compila tutti i campi richiesti.");
            canProceed = false;
        }
    } else if(currentStep === 3) {

    }

    if(canProceed) {
        if (currentStep < $('.checkoutStep').last().data('step')) $('.checkoutStep.active').removeClass('active').hide().next().show().addClass('active');
        else moduleSaveFunction();

        refreshCart();
    }

}

function undoCheckout() {
    $('.checkoutStep.active').removeClass('active').hide().prev().show().addClass('active');

    refreshCart();
}

/* DATI CLIENTE */
function updateCustomerAddresses(id) {

    $('#info_fatturazione').show().find('[name]').val('');

    $.ajax({
        url: "ajax/getCustomerData.php",
        type: "POST",
        data: {
            "id": id
        },
        async: true,
        dataType: "json",
        success: function (data) {

            $('#customerInfo .data').html(data.preview).show();
            $('#customerInfo #customer_id').val(data.id);

            Object.keys(data.addresses).forEach(function (key) {
                if($('#info_fatturazione [name="' + key + '"]').length && !$('#info_fatturazione [name="' + key + '"]').val().length) {
                    $('#info_fatturazione [name="' + key + '"]').val(data.addresses[key]);
                }
                if($('#info_spedizione [name="' + key + '"]').length && !$('#info_spedizione [name="' + key + '"]').val().length) {
                    $('#info_spedizione [name="' + key + '"]').val(data.addresses[key]);
                }
            });
        }
    });
}

function moduleSaveFunction(confirm) {
    if(typeof(confirm) === 'undefined') confirm = false;

    if(Object.keys(window.currentServiceCart.products).length) {

        var dati_fatturazione = {};
        $.map($('#info_fatturazione').serializeArray(), function(n, i){
            dati_fatturazione[n['name']] = n['value'];
        });

        if($('#customer_notification:checked').length) {
            dati_fatturazione['send_notification'] = 1;
        } else {
            dati_fatturazione['send_notification'] = 0;
        }

        var data_to_sent = {
            cart: window.currentServiceCart,
            customer_data: dati_fatturazione,
            customer_id: $('#customerInfo #customer_id').val(),
            note: tinymce.get('order_note').getContent()
        };

        $.ajax({
            url: "db/saveData.php",
            type: "POST",
            data: data_to_sent,
            async: false,
            dataType: "json",
            success: function(data) {
                if(data.status == "query_error") {
                    bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
                } else if(data.status == "ok") {

                    if(data.action === 'quote_needed') {
                        location.href = data.redirect_url;
                    } else {
                        bootbox.alert("L'ordine è stato creato correttamente, stai per essere reindirizzato alla pagina di pagamento.", function () {
                            location.href = data.redirect_url;
                        });
                    }

                }
            }
        })

    }
}
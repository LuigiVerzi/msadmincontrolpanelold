<?php
require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$payment_data = (new \MSFramework\MSAgency\earnings())->getUserPaymentsData($MSFrameworkUsers->getUserDataFromSession('id'));

if($MSFrameworkDatabase->getCount("SELECT * FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = :user_id AND withdrawal_status = 1 AND withdrawal_date = 0", array(':user_id' => $MSFrameworkUsers->getUserDataFromSession('id')))) {
    die(json_encode(array(
        'status' => 'already_open'
    )));
}

if(!$payment_data) {
    die(json_encode(array(
        'status' => 'no_payment_data'
    )));
}

$request_value = $MSFrameworkDatabase->getAssoc("SELECT SUM(earning) as totale FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = :user_id AND withdrawal_status = 0", array(':user_id' => $MSFrameworkUsers->getUserDataFromSession('id')), true)['totale'];

if($request_value < (new \MSFramework\MSAgency\earnings())->minWithdrawalCredit) {
    die(json_encode(array(
        'status' => 'minimum_credit',
        'minimum_credit' => (new \MSFramework\MSAgency\earnings())->minWithdrawalCredit
    )));
}

$MSFrameworkDatabase->pushToDB("UPDATE " . FRAMEWORK_DB_NAME . ".ms_agency__earnings SET withdrawal_status = 1, payment_data = :payment_data", array(':payment_data' => json_encode($payment_data)));

(new \MSFramework\MSAgency\emails())->sendMail(
    '[msagency][admin]withdrawal-request',
    array(
        'user_data' => $MSFrameworkUsers->getUserDataFromSession(),
        'value' => number_format($request_value, 2, ',', '.')
    )
);

(new \MSFramework\MSAgency\emails())->sendMail(
    '[msagency][user]withdrawal-requested',
    array(
        'user_data' => $MSFrameworkUsers->getUserDataFromSession(),
        'value' => number_format($request_value, 2, ',', '.')
    )
);

die(json_encode(array(
    'status' => 'ok'
)));
<?php
require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSAgencyServices = new \MSFramework\MSAgency\services();
$statusLabel = (new \MSFramework\MSAgency\orders())->getStatus();

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    )
);

$columns[] = array(
    'db' => 'id',
    'dt' => 'DT_buttonsLimit',
    'formatter' => function( $d, $row ) {
        return "read";
    }
);

$columns[] = array(
    'db' => 'withdrawal_status',
    'dt' => 'DT_Status'
);

$columns[] = array(
    'db' => 'order_id',
    'dt' => 'DT_OrderID'
);

$k = 0;

$columns[] = array('db' => 'order_id', 'dt' => $k, 'formatter' => function ($d, $row) {
    $orderDetails = (new \MSFramework\MSAgency\orders())->getOrderDetails($d)[$d];
    return (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))->getCustomerPreviewHTML($orderDetails['customer_reference']);
});
$k++;

$columns[] = array('db' => 'product', 'dt' => $k, 'formatter' => function ($d, $row) {
    Global $MSFrameworki18n;
    $orderDetails = (new \MSFramework\MSAgency\orders())->getOrderDetails($row['order_id'], true)[$row['order_id']];
    return $MSFrameworki18n->getFieldValue($orderDetails['cart'][$d]['nome']);
});
$k++;

$columns[] = array('db' => 'earning', 'dt' => $k, 'formatter' => function ($d, $row) {
    return number_format($d, 2, ',', '.') . ' ' . CURRENCY_SYMBOL;
});
$k++;


$columns[] = array( 'db' => 'date', 'dt' => $k, 'formatter' => function($d, $row) {
    return '<div style="line-height: 1.5">' . (new \MSFramework\utils())->smartdate(strtotime($d)) . '<small class="text-muted"><br>' . date('d/m/Y H:i', strtotime($d)) . '</small></div>';
});
$k++;

$columns[] = array( 'db' => 'withdrawal_date', 'dt' => $k, 'formatter' => function($d, $row) {
    if((int)$row['withdrawal_status']) {
        if($d > 0) return '<div class="label label-primary" style="display: inline-block; text-align: left; color: white; line-height: 1.5;">Prelevato il<small><br>' . date('d/m/Y H:i', $d) . '</small></div>';
        else return '<div class="label label-success" style="line-height: 1.5">Prelievo in attesa</div>';
    } else {
        return '<span class="label label-success">Disponibili</span>';
    }
});
$k++;

echo json_encode(
    $datatableHelper::complex($_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.ms_agency__earnings', 'id', $columns, '', "user_id = '" . $MSFrameworkUsers->getUserDataFromSession('id') . "'")
);
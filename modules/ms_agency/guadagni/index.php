<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$stati_cliente = array();
$totale = $MSFrameworkDatabase->getAssoc("SELECT SUM(earning) as totale FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = :user_id AND withdrawal_status = 0", array(':user_id' => $MSFrameworkUsers->getUserDataFromSession('id')), true)['totale'];
$totale_prelevato = $MSFrameworkDatabase->getAssoc("SELECT SUM(earning) as totale FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = :user_id AND withdrawal_status = 1 AND withdrawal_date > 0", array(':user_id' => $MSFrameworkUsers->getUserDataFromSession('id')), true)['totale'];
$totale_attesa = $MSFrameworkDatabase->getAssoc("SELECT SUM(earning) as totale FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = :user_id AND withdrawal_status = 1 AND withdrawal_date = 0", array(':user_id' => $MSFrameworkUsers->getUserDataFromSession('id')), true)['totale'];
?>

<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewDelGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h1 class="no-margins text-navy"><?= number_format($totale, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                                    <small>Credito disponibile</small>
                                </div>
                                <div class="col-lg-6">
                                    <?php if($MSFrameworkDatabase->getCount("SELECT * FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = :user_id AND withdrawal_status = 1 AND withdrawal_date = 0", array(':user_id' => $MSFrameworkUsers->getUserDataFromSession('id')))) { ?>
                                        <a style="margin-top: 3px;" href="#" class="btn btn-block btn-lg btn-success disabled"><i class="fas fa-credit-card"></i> Richiesta in corso</a>
                                    <?php } else { ?>
                                        <a style="margin-top: 3px;" href="#" onclick="withdrawalRequest();" class="btn btn-block btn-lg btn-primary"><i class="fas fa-credit-card"></i> Richiedi prelievo</a>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 pull-right">
                    <div class="ibox">
                        <div class="ibox-content">
                            <h1 class="no-margins text-success"><?= number_format($totale_prelevato, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                            <small>Credito prelevato</small>
                        </div>
                    </div>
                </div>

                <?php if($totale_attesa) { ?>
                    <div class="col-lg-3 pull-right">
                        <div class="ibox">
                            <div class="ibox-content">
                                <h1 class="no-margins text-warning"><?= number_format($totale_attesa, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                                <small>Credito in attesa</small>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>

            <div class="row">
                <div class="col-lg-12">

                    <table id="customers_with_balances_table" data-readonly="1" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Cliente</th>
                            <th>Ordine</th>
                            <th>Entrate</th>
                            <th>Data</th>
                            <th>Stato</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
<script>
    globalInitForm();
</script>
</html>
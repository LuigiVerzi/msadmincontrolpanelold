$(document).ready(function() {
        loadStandardDataTable('customers_with_balances_table', true, {"ajax": 'ajax/dataTable.php?source=with_balances'});
        allowRowHighlights('customers_with_balances_table');
        manageGridButtons('customers_with_balances_table');
});

function initForm() {
    initClassicTabsEditForm();
}

function moduleSaveFunction() {

}

function withdrawalRequest() {
    bootbox.confirm("Proseguendo verrà aperta una richiesta di prelievo del credito attuale. Vuoi continuare?", function (confirm) {

        if(confirm) {
            $.ajax({
                url: "ajax/withdrawalRequest.php",
                type: "POST",
                async: false,
                dataType: "json",
                success: function (data) {
                    if (data.status == "already_open") {
                        bootbox.error("Hai già una richiesta di prelievo in attesa. Potrai aprirne un'altra non appena sarà processata la richiesta attuale.");
                    } else if (data.status == "no_payment_data") {
                        bootbox.error("Non hai ancora impostato i tuoi dati di pagamento, stai per essere reindirizzato alla pagina di preferenze.", function () {
                            location.href = '../pagamenti';
                        });
                    }  else if (data.status == "minimum_credit") {
                        bootbox.error("Il credito minimo necessario per richiedere un prelievo è di <b>" + data.minimum_credit + " <?= CURRENCY_SYMBOL; ?></b>");
                    } else if (data.status == "ok") {
                        bootbox.alert("La richiesta di prelievo è stata aperta correttamente, riceverai una risposta nel minor tempo possibile.", function () {
                            location.reload();
                        });
                    }
                }
            });
        }

    });
}
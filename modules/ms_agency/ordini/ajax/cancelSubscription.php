<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if((new \MSFramework\MSAgency\orders())->cancelSubscription($_POST['order_id'], $_POST['product_id'])) {
    echo json_encode(array("status" => "ok"));
    die();
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

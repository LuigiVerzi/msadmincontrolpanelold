<?php
require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSAgencyServices = new \MSFramework\MSAgency\services();
$statusLabel = (new \MSFramework\MSAgency\orders())->getStatus();

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    )
);

$columns[] = array(
    'db' => 'id',
    'dt' => 'DT_buttonsLimit',
    'formatter' => function( $d, $row ) {
        return "read";
    }
);

$k = 0;

// Mostro il cliente ai consulenti
if($MSAgencyServices->userViewMode !== 'customer') {
    $columns[] = array('db' => 'customer_reference', 'dt' => $k, 'formatter' => function ($d, $row) {
        return (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))->getCustomerPreviewHTML($d);
    });
    $k++;
}



$columns[] = array('db' => 'cart', 'dt' => $k, 'formatter' => function ($d, $row) {
    Global $MSFrameworki18n;
    $return = array();
    foreach ((new \MSFramework\MSAgency\orders())->getOrderDetails($row['id'], true)[$row['id']]['cart'] as $p) {
        $return[] = $MSFrameworki18n->getFieldValue($p['nome']);
    }
    return '<ul style="padding: 0 20px; margin: 0;"><li>' . implode('</li><li>', $return) . '</li></ul>';
});
$k++;

$columns[] = array('db' => 'id', 'dt' => $k, 'formatter' => function ($d, $row) use ($statusLabel) {
    $return = '';
    foreach ((new \MSFramework\MSAgency\orders())->getFullOrderStatus($row['id'], (new \MSFramework\MSAgency\services())->userViewMode) as $singleStatus) {
        $return .= '<span class="label" style="margin-bottom: 5px; display: block; color: white; background: ' . $singleStatus['color'] . '; text-align: left;">' . $singleStatus['label'] . '</span>';
    }
    return $return;
});
$k++;

$columns[] = array( 'db' => 'last_update', 'dt' => $k, 'formatter' => function($d, $row) {
    return '<div style="line-height: 1.5">' . (new \MSFramework\utils())->smartdate(strtotime($d)) . '<small class="text-muted"><br>' . date('d/m/Y H:i', strtotime($d)) . '</small></div>';
});

$search_sql = "seller = '" . $MSFrameworkUsers->getUserDataFromSession('id') . "' AND origin_reference = '" . $_SESSION['db'] . "'";

if((new \MSFramework\MSAgency\services())->userViewMode === 'consultant') {
    echo json_encode(
        $datatableHelper::complex($_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.ms_agency__orders', 'id', $columns, '', "FIND_IN_SET('" . $MSFrameworkUsers->getUserDataFromSession('id') . "', consultant) OR ($search_sql)")
    );
} else if((new \MSFramework\MSAgency\services())->userViewMode === 'seller') {
    echo json_encode(
        $datatableHelper::complex($_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.ms_agency__orders', 'id', $columns, '', "seller = '" . $MSFrameworkUsers->getUserDataFromSession('id') . "' OR ($search_sql)")
    );
} else {
    echo json_encode(
        $datatableHelper::complex($_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.ms_agency__orders', 'id', $columns, '', $search_sql)
    );
}
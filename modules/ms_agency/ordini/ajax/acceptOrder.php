<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$orderDetails = (new \MSFramework\MSAgency\orders())->getOrderDetails($_POST['order_id'], true)[$_POST['order_id']];
$product_details = $orderDetails['cart'][$_POST['product_id']];

$result = false;
if($orderDetails && $product_details) {
    $result = (new \MSFramework\MSAgency\orders())->updateOrderProducts($orderDetails['id'], $_POST['product_id'], array(
        'consultant_accepted' => "1"
    ));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ''));
die();
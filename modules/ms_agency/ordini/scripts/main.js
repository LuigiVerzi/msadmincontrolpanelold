$(document).ready(function() {
    loadStandardDataTable('main_table_list', true);
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
}

function acceptOrder(product_id, confirm) {

    if(typeof confirm === 'undefined') confirm = false;

    if(!confirm) {
        bootbox.confirm("Confermi di voler accettare l'ordine?", function (send) {
            if(send) acceptOrder(product_id, true);
        });
        return false;
    }

    var data_to_sent = {
        order_id: $('#record_id').val(),
        product_id: product_id,
    };

    $.ajax({
        url: "ajax/acceptOrder.php",
        type: "POST",
        data: data_to_sent,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                bootbox.alert("Hai preso in carico l'ordine. Ricordati di rispettare i tempi di consegna.", function () {
                    location.reload();
                });
            }
        }
    });

}

function completeOrder(product_id, confirm) {

    if(typeof confirm === 'undefined') confirm = false;

    if(!confirm) {
        bootbox.confirm("Confermi di voler contrassegnare l'ordine come Completato?", function (send) {
            if(send) completeOrder(product_id, true);
        });
        return false;
    }

    var data_to_sent = {
        order_id: $('#record_id').val(),
        product_id: product_id,
    };

    $.ajax({
        url: "ajax/completeOrder.php",
        type: "POST",
        data: data_to_sent,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                bootbox.alert("Hai contrassegnato l'ordine come completato.", function () {
                    location.reload();
                });
            }
        }
    });

}

function cancelSubscription(product_id, confirm) {

    if(typeof confirm === 'undefined') confirm = false;

    if(!confirm) {
        bootbox.confirm("Confermi di voler cancellare l'abbonamento? Il servizio cesserà automaticamente", function (send) {
            if(send) cancelSubscription(product_id, true);
        });
        return false;
    }

    var data_to_sent = {
        order_id: $('#record_id').val(),
        product_id: product_id,
    };

    $.ajax({
        url: "ajax/cancelSubscription.php",
        type: "POST",
        data: data_to_sent,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "query_error") {
                bootbox.error("Si è verificato un problema inaspettato durante la disattivazione dell'abbonamento. Se il problema persiste la preghiamo di contattare l'assistenza.");
            } else if(data.status == "ok") {
                bootbox.alert("Hai annullato il tuo abbonamento.", function () {
                    location.reload();
                });
            }
        }
    });

}
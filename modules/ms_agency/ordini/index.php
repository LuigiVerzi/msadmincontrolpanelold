<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">

                <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <?php if((new \MSFramework\MSAgency\services())->userViewMode !== 'customer') { ?>
                        <th>Cliente</th>
                        <?php } ?>
                        <th>Prodotti</th>
                        <th class="no-sort">Stato</th>
                        <th>Ultimo aggiornamento</th>
                    </tr>
                    </thead>
                </table>

            </div>
        </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
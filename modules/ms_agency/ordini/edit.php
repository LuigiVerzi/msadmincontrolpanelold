<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');

// Se sta visualizzando l'ordine tramite token segreto allora imposto la modalità visitatore
if(!isset($_GET['id']) && isset($_GET['token'])) {
    $module_config['guest_mode'] = true;
}

require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSAgencyOrders = new \MSFramework\MSAgency\orders();

$r = array();
if(isset($_GET['id'])) {
    $r = $MSAgencyOrders->getOrderDetails($_GET['id'], true)[$_GET['id']];
} else if(isset($_GET['token'])) {
    $r = $MSAgencyOrders->getOrderByToken($_GET['token']);
}

if(!$r) {
    header('Location: index.php');
}

$settings_data = $MSFrameworkFW->getCMSData('settings');
$framework_data = json_decode($r['framework_data'], true);

$statusLabel = (new \MSFramework\MSAgency\orders())->getStatus();

$serviceUploads = new \MSFramework\uploads('SERVICES', true);
$packageUploads = new \MSFramework\uploads('PACKAGES', true);

uasort($r['cart'], function($a, $b) {
   return $a['payment_status'] - $b['payment_status'];
})
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php if(!$module_config['guest_mode']) { ?>
        <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>
    <?php } ?>

    <div id="page-wrapper" class="gray-bg" style="<?= ($module_config['guest_mode'] ? 'margin: 0;' : ''); ?>">

        <?php if(!$module_config['guest_mode']) { ?>
            <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
                </div>
                <div class="col-sm-8">
                    <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
                </div>
            </div>
        <?php } ?>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Resoconto</h1>

                        <fieldset>
                            <div class="row">

                                <div class="col-md-3 col-sm-6" style="margin-bottom: 30px;">
                                    <label>Data</label>
                                    <p>
                                        <u>Creazione:</u> <?= date('d/m/Y H:i', strtotime($r['order_date'])); ?>
                                        <br>
                                        <u>Ultimo aggiornamento:</u> <?= date('d/m/Y H:i', strtotime($r['last_update'])); ?>
                                    </p>
                                </div>

                                <div class="col-md-3 col-sm-6" style="margin-bottom: 30px;">
                                    <label>Cliente</label>
                                    <?= (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))->getCustomerPreviewHTML($r['customer_reference']); ?>
                                </div>

                                <div class="col-md-6 col-sm-12 text-right" style="margin-bottom: 30px;">
                                    <label>Stato ordine</label>
                                    <?php
                                    foreach((new \MSFramework\MSAgency\orders())->getFullOrderStatus($r, (new \MSFramework\MSAgency\services())->userViewMode) as $singleStatus) {
                                        echo '<span class="label" style="margin-bottom: 5px; margin-right: 5px; display: inline-block; color: white; background: ' . $singleStatus['color'] . ';">' . $singleStatus['label'] . '</span>';
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <h2 class="title-divider" style="margin-top: -15px;">Servizi Acquistati</h2>

                                    <table class="table table-bordered" id="products_table" style="margin: 0;">
                                        <tbody>
                                        <?php foreach($r['cart'] as $full_id => $product) { ?>
                                            <tr class="product-row" data-id="<?= $full_id; ?>">
                                                <td width="90" class="text-center">
                                                    <?php if($product['images']) { ?>
                                                        <?php
                                                        if(stristr($full_id, 'service')) $image_base = $serviceUploads->path_html;
                                                        else $image_base = $packageUploads->path_html;
                                                        ?>
                                                        <img src="<?php echo $image_base . json_decode($product['images'], true)[0] ?>" width="80" />
                                                    <?php } ?>
                                                </td>
                                                <td class="desc">
                                                    <h3><?php echo $MSFrameworki18n->getFieldValue($product['nome']); ?></h3>
                                                    <p><?php echo $MSFrameworki18n->getFieldValue($product['descr']); ?></p>
                                                </td>

                                                <?php
                                                if(!$product['quote_needed'] && (new \MSFramework\MSAgency\services())->userViewMode !== 'consultant') {
                                                    $singleProductStatus = (new \MSFramework\MSAgency\orders())->getStatus((int)$product['payment_status']);
                                                } else {
                                                    $singleProductStatus = (new \MSFramework\MSAgency\orders())->getSingleProductStatus($product);
                                                }

                                                if($singleProductStatus['id'] === 'complete' && (int)$product['una_tantum'] === 0) {
                                                    $singleProductStatus = (new \MSFramework\MSAgency\orders())->getSingleProductStatus($product);
                                                }

                                                $payment_label = '<span class="badge" style="margin-bottom: 10px; display: inline-block; color: white; background: ' . $singleProductStatus['color'] . ';">' . $singleProductStatus['label'] . '</span>';
                                                ?>

                                                <td style="vertical-align: middle; min-width: 100px; background: <?= (new \MSFramework\utils())->hex2rgba($singleProductStatus['color'], '0.15'); ?>" class="text-center">

                                                    <?= $payment_label; ?>
                                                    <div style="clear: both;"></div>

                                                    <?php if(!$product['quote_needed']) { ?>
                                                        <b><?php echo number_format($product['prezzo'], 2, ',', '.') ?> <?= CURRENCY_SYMBOL; ?></b>
                                                        <br>
                                                    <?php } ?>

                                                    <small><?= $product['payment_recurrence_string']; ?></small>

                                                    <?php if($singleProductStatus['id'] === "awaiting_payment") { ?>
                                                        <div style="clear: both;"></div>
                                                        <a href="<?= $MSSoftwareModules->getModuleUrlByID('payway') . '?type=ms_service&id=' . $full_id . '.' . $r['id']; ?>" class="btn btn-block btn-primary" style="margin: 5px 0 0;"><i class="fa fa-credit-card"></i> Paga adesso</a>
                                                    <?php } else if($singleProductStatus['id'] == 'active_subscription') { ?>
                                                        <div style="clear: both;"></div>
                                                        <a href="#" class="btn btn-block btn-default" onclick="cancelSubscription('<?= $full_id; ?>');" style="margin: 5px 0 0;"><i class="fa fa-close"></i> Disdici abbonamento</a>
                                                    <?php } else { ?>
                                                        <?php if((new \MSFramework\MSAgency\services())->userViewMode === 'consultant') { ?>
                                                            <?php if($singleProductStatus['id'] === "awaiting_seller") { ?>
                                                                <div style="clear: both;"></div>
                                                                <a href="#" class="btn btn-block btn-primary" onclick="acceptOrder('<?= $full_id; ?>');" style="margin: 5px 0 0;"><i class="fa fa-credit-card"></i> Accetta ordine</a>
                                                            <?php } else if($singleProductStatus['id'] === "wip" && (int)$product['una_tantum'] === 1) { ?>
                                                                <div style="clear: both;"></div>
                                                                <a href="#" class="btn btn-block btn-primary" onclick="completeOrder('<?= $full_id; ?>');" style="margin: 5px 0 0;"><i class="fa fa-check"></i> Ordine completato</a>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>

                                                </td>
                                            </tr>
                                        <?php } ?>

                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-lg-3">
                                    <h2 class="title-divider">Dati di Fatturazione</h2>

                                    <h3><?=  $r['customer_data']['nome']; ?> <?=  $r['customer_data']['cognome']; ?></h3>

                                    <address>

                                        <div><strong><?= $r['customer_data']['ragione_sociale']; ?></strong></div>

                                        <?php if( $r['customer_data']['piva']) { ?>
                                            <abbr title="Partita Iva">P.IVA:</abbr> <?=  $r['customer_data']['piva']; ?><br>
                                        <?php } else if( $r['customer_data']['cf']) { ?>
                                            <abbr title="Partita Iva">C.F:</abbr> <?=  $r['customer_data']['cf']; ?><br>
                                        <?php } ?>

                                        <div class="hr-line-dashed"></div>

                                        <?= $r['customer_data']['indirizzo']; ?><br>
                                        <?= $r['customer_data']['comune']; ?>, <?= $r['customer_data']['citta']; ?> <?= $r['customer_data']['cap']; ?><br>

                                        <div class="hr-line-dashed"></div>

                                        <abbr title="Telefono">T:</abbr> <?=  $r['customer_data']['cellulare']; ?><br>
                                        <abbr title="Email">E:</abbr> <?=  $r['customer_data']['email']; ?>
                                    </address>

                                </div>

                                <div class="col-lg-9">
                                    <h2 class="title-divider">Note</h2>

                                    <div class="order-note">
                                        <?= $r['note']; ?>
                                    </div>
                                </div>

                            </div>
                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<style>
    @media (max-width: 1200px) {

        #products_table {
            border: none;
        }

        #products_table tr td {
            border-bottom: 0;
            border: none;
        }

        #products_table,
        #products_table tbody,
        #products_table tr,
        #products_table tr td {
            display: block;
            width: 100%;
        }

        #products_table tr {
            margin-bottom: 30px;
            border: 1px solid #e7eaec;
        }
    }
</style>

<script>
    globalInitForm();
</script>
</body>
</html>
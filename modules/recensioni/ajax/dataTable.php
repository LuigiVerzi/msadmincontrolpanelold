<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM recensioni WHERE lang = :lang", array(":lang" => USING_LANGUAGE_CODE)) as $r) {
    $images = json_decode($r['propic'], true);

    $ary_data = array();

    $ary_data = array(
        $r['titolo'],
        $r['autore'],
        $r['stelle']
    );

    if(!isset($module_config['module_fields_status']['provenienza']) || $module_config['module_fields_status']['provenienza'] == "1") {
        $ary_data[] =  (new \MSFramework\recensioni())->getProvenienze($r['provenienza'])['name'];
    }

    $ary_data["DT_RowId"] = $r['id'];

    $array['data'][] = $ary_data;

}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

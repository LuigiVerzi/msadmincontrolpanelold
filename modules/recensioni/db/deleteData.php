<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\recensioni())->getRecensioniDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['gallery'], true));
}

if($MSFrameworkDatabase->deleteRow("recensioni", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('REVIEWS'))->unlink($images_to_delete);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM recensioni WHERE id = :id", array($_GET['id']), true);
}

if(USING_LANGUAGE_CODE != $r['lang'] && $_GET['id'] != "") {
    header("location: " . ABSOLUTE_ADMIN_MODULES_PATH_HTML . "recensioni/index.php?different_lang_prop");
    die();
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Titolo*</label>
                                    <input id="titolo" name="titolo" type="text" class="form-control msShort required" value="<?php echo htmlentities($r['titolo']) ?>">
                                </div>

                                <div class="col-sm-6" data-optional>
                                    <label>Provenienza</label>
                                    <select id="provenienza" name="provenienza" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach((new \MSFramework\recensioni())->getProvenienze() as $typeK => $typeV) {
                                            ?>
                                            <option value="<?php echo $typeK ?>" <?php if($r['provenienza'] == $typeK) { echo "selected"; } ?>><?php echo $typeV['name'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Testo recensione*</label>
                                    <textarea id="testo" name="testo" class="form-control msShort required"><?php echo $r['testo'] ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-7">
                                    <label>Foto</label>
                                    <?php
                                    (new \MSFramework\uploads('REVIEWS'))->initUploaderHTML("images", $r['gallery']);
                                    ?>
                                </div>

                                <div class="col-sm-2">
                                    <label>Stelle*</label>
                                    <select id="stelle" name="stelle" class="form-control required">
                                        <option value=""></option>
                                        <?php
                                        for($xstelle=0; $xstelle<=5; $xstelle++) {
                                            ?>
                                            <option value="<?php echo $xstelle ?>" <?php if($r['stelle'] === (string)$xstelle) { echo "selected"; } ?>><?php echo $xstelle ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Autore*</label>
                                    <input id="autore" name="autore" type="text" class="form-control required" value="<?php echo htmlentities($r['autore']) ?>">
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
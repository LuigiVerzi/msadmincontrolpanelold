<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
        <?php
        if(isset($_GET['different_lang_prop'])) {
            ?>
            <input type="hidden" id="different_lang_prop" value="different_lang_prop" />
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                Hai provato a modificare un record che non appartiene alla lingua impostata.
            </div>
            <?php
        }
        ?>

        <div class="row">
            <div class="col-lg-12">

                <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Titolo</th>
                        <th>Autore</th>
                        <th>Stelle</th>
                        <?php if(!isset($module_config['module_fields_status']['provenienza']) || $module_config['module_fields_status']['provenienza'] == "1") { ?>
                            <th>Provenienza</th>
                        <?php } ?>
                    </tr>
                    </thead>
                </table>

            </div>
        </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
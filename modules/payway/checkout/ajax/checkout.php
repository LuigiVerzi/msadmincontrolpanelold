<?php
/**
 * MSAdminControlPanel
 * Date: 2019-09-07
 */
require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkPayWay = new \MSFramework\Framework\payway();

$isPayable = true;
$paywayPlan = array(); // se settate le info verrà impostato un pagamento ricorrente invece che uno standard

if(!$MSFrameworkPayWay->checkTypeExists($_GET['type'])) $isPayable = false;
if(!$MSFrameworkPayWay->checkProviderEnabled($_GET['provider'], $_GET['type'], array("id" => $_GET['id']))) $isPayable = false;

if($_GET['type'] == "ticket_quotation") {
    $MSFrameworkTicketsQuotation = new \MSFramework\Framework\Ticket\quotations();

    if(!$MSFrameworkTicketsQuotation->checkQuotationPayable($_GET['id'])) $isPayable = false;

    $r_response_details = $MSFrameworkTicketsQuotation->getTicketResponseDetails($_GET['id'], 'quotation')[$_GET['id']];
    $quotation = json_decode($r_response_details['quotation'], true);
    $priceData = $MSFrameworkPayWay->formatPrice($quotation['data']['price']);

    $itemAry = array("title" => "Ticket", "descr" => "Pagamento ticket assistenza");

    $user_info = $MSFrameworkUsers->getUserDetails($_SESSION['userData']['user_id'], "email")[$_SESSION['userData']['user_id']];
    $customer_email = $user_info['email'];
    $transaction_id = array("colname" => "quote_id", "value" => $_GET['id']);
} else if($_GET['type'] == "ms_service") {
    $MSAgencyOrders = new \MSFramework\MSAgency\orders();

    $explode_ids = explode('.', $_GET['id']);
    if(!$MSAgencyOrders->checkOrderPayable($explode_ids[1], $explode_ids[0])) $isPayable = false;

    $r_response_details = $MSAgencyOrders->getOrderDetails($explode_ids[1])[$explode_ids[1]];
    $r_product_details = $r_response_details['cart'][$explode_ids[0]];

    $priceData = $MSFrameworkPayWay->formatPrice($r_product_details['prezzo']);

    if((int)$r_product_details['una_tantum'] === 0) {
        $frequencyConversion = array(
            'days' => 'DAY',
            'months' => 'MONTH',
            'years' => 'YEAR'
        );

        $paywayPlan = array(
            'name' => "Abbonamento " . $MSFrameworki18n->getFieldValue($r_product_details['nome']),
            'description' => "Abbonamento " . $MSFrameworki18n->getFieldValue($r_product_details['nome']),
            'payments' => array(
                'interval' => array(
                    'value' => $r_product_details['duration']['value'],
                    'type' => $frequencyConversion[$r_product_details['duration']['type']]
                ),
                'value' => $MSFrameworkPayWay->formatPrice($r_product_details['prezzo'])['iva_inclusa'],
                'currency' => 'EUR'
            )

        );
    }

    $itemAry = array("title" => $MSFrameworki18n->getFieldValue($r_product_details['nome']), "descr" => "Pagamento " . $MSFrameworki18n->getFieldValue($r_product_details['nome']), "id" => $explode_ids[0]);

    $customer_email = $r_response_details['customer_data']['email'];
    $transaction_id = array("colname" => "order_id", "value" => $explode_ids[1]);
} else if($_GET['type'] == "saas") {

    if($MSFrameworkSaaSEnvironments->getOwnerID() != $_SESSION['userData']['user_id'] && $_SESSION['userData']['userlevel'] != "0") {
        $MSFrameworkUsers->endUserSession();
        header("location: " . ABSOLUTE_SW_PATH_HTML . "login.php?not-logged");
        die();
    }

    $MSFrameworkSaaSSubscriptions = new \MSFramework\SaaS\subscriptions();

    $activeSubscriptions = $MSFrameworkSaaSSubscriptions->getAvailSubscriptions(true);
    if(array_key_exists($_GET['id'], $activeSubscriptions)) {
        $details_in_context = $MSFrameworkSaaSSubscriptions->getDetailsInContext($_GET['id']);
        $priceData = $details_in_context[0];
        $subscription = $activeSubscriptions[$_GET['id']];

        $itemAry = array("title" => "Abbonamento", "descr" => 'Rinnovo Abbonamento: ' . $subscription['name']);

        $customer_email = $MSFrameworkSaaSEnvironments->getOwnerEmail();
        $transaction_id = array("colname" => "env_id", "value" => $MSFrameworkSaaSEnvironments->getCurrentEnvironmentID());
    } else {
        $isPayable = false;
    }

}

if(!$isPayable) {
    header("location: ../index.php");
    die();
}

$providerData = $MSFrameworkPayWay->getProviderData($_GET['provider']);

$payway_ipn = $MSFrameworkCMS->getURLToSite(true) . "modules/payway/checkout/ipn/paypal.php?type=" . $_GET['type'];
$payway_index = $MSFrameworkCMS->getURLToSite(true) . "modules/payway/checkout/index.php?redirected=1";
$payway_checkout = $MSFrameworkCMS->getURLToSite(true) . "modules/payway/checkout/ajax/checkout.php?redirected=1";

foreach($_GET as $getK => $getV) {
    if($getK == "paymentId" || $getK == "PayerID" || $getK == "token" || $getK == "redirected") {

        if(!strstr($payway_index, "successPayRedirect")) {
            $payway_index .= "&successPayRedirect=1";
        }

        if(!strstr($payway_checkout, "successPayRedirect")) {
            $payway_checkout .= "&successPayRedirect=1";
        }

        continue;
    }

    $payway_index .= "&" . $getK . "=" . $getV;
    $payway_checkout .= "&" . $getK . "=" . $getV;
}

$r = $MSFrameworkCMS->getCMSData('settings');
$fatturazione = json_decode($r['datiFatturazione'], true);

if($_GET['type'] == "ms_service") {
    $fatturazione = $r_response_details['customer_data'];
    $fatturazione['rag_soc'] = $fatturazione['ragione_sociale'];
    $fatturazione['provincia'] = $fatturazione['citta'];
}

$framework_data_ary = array(
    "user" => array("dati_fatturazione" => $fatturazione, "id" => $_SESSION['userData']['id'], "email" => $_SESSION['userData']['email']),
    "fw" => array("database" => $_SESSION['db']),
    "price" => $priceData,
    "item" => $itemAry,
    "plan" => $paywayPlan
);

if($_GET['type'] == "saas") {
    $framework_data_ary['environment']['id'] = $MSFrameworkSaaSEnvironments->getCurrentEnvironmentID();
    $framework_data_ary['subscription']['exp_date'] = (\DateTime::createFromFormat("d/m/Y", $details_in_context[1]))->format("Y-m-d");
    $framework_data_ary['subscription']['id'] = $_GET['id'];
    $framework_data_ary['subscription']['details'] = $MSFrameworkSaaSSubscriptions->getAvailSubscriptions(true)[$_GET['id']];
}


if(isset($_GET['provider']) && !isset($_GET['status'])) {
    $preorder_reference = $MSFrameworkPayWay->createPreOrder($_GET['type'], $_GET['provider'], $framework_data_ary, $transaction_id);
    $preorder_unique_id = implode('__', $preorder_reference);
    $_SESSION['preorder_' . $preorder_unique_id] = $preorder_reference;
    $payway_checkout .= '&preorder=' . $preorder_unique_id;
}

if($_GET['provider'] == "paypal") {
    $settings_data = $providerData['pay_data'][PAYMETHODS_USE_KEY];
    $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential($settings_data['client_id'], $settings_data['secret']));
    $apiContext->setConfig(
        array(
            'mode' => (PAYMETHODS_USE_KEY == "sandbox" ? PAYMETHODS_USE_KEY : "live")
        )
    );

    if($_GET['status'] == "success") {

        if($_GET['mode'] === 'subscription') {
            $token = $_GET['token'];
            $agreement = new \PayPal\Api\Agreement();

            try {
                // Execute agreement
                if($_SESSION['preorder_' . $_GET['preorder']]) {
                    $result = $agreement->execute($token, $apiContext);
                    $MSFrameworkPayWay->createSubscriptionAgreement($_SESSION['preorder_' . $_GET['preorder']], $agreement->getId());
                }

            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                echo $ex->getCode();
                echo $ex->getData();
                die($ex);
            } catch (Exception $ex) {
                header('location: ' . $payway_index . "&status=" . $_GET['status']);
                die();
            }
        } else {
            $paymentId = $_GET['paymentId'];
            $payment = \PayPal\Api\Payment::get($paymentId, $apiContext);

            $execution = new \PayPal\Api\PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);
            try {
                $result = $payment->execute($execution, $apiContext);
                try {
                    $payment = \PayPal\Api\Payment::get($paymentId, $apiContext);
                } catch (Exception $ex) {
                    header('location: ' . $payway_index . "&status=" . $_GET['status']);
                    die();
                }
            } catch (Exception $ex) {
                header('location: ' . $payway_index . "&status=" . $_GET['status']);
                die();
            }
        }

        header('location: ' . $payway_index . "&status=" . $_GET['status']);

    } else {

        if ($paywayPlan) include('paypal/createSubscription.php');
        else include('paypal/createSinglePayment.php');

    }
} else if($_GET['provider'] == "stripe") {
    $settings_data = $providerData['pay_data'][PAYMETHODS_USE_KEY];

    \Stripe\Stripe::setVerifySslCerts(false);
    \Stripe\Stripe::setApiKey($settings_data['secret_key']);

    $session = \Stripe\Checkout\Session::create([
        'payment_method_types' => ['card'],
        'line_items' => [[
            'name' => $itemAry['title'],
            'description' => $itemAry['descr'],
            'amount' => (int)$priceData['iva_inclusa']*100,
            'currency' => 'eur',
            'quantity' => 1,
        ]],
        'success_url' => $payway_index . "&status=success&successPayRedirect=1",
        'cancel_url' => $payway_index . "&status=cancel",
        "client_reference_id" => json_encode($preorder_reference),
        "customer_email" => $customer_email
    ]);

    $session['ms_stripe_public'] = $settings_data['public_key'];
    echo json_encode($session);
    die();
}

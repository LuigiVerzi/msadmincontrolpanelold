<?php
use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

// Create a new billing plan
$plan = new Plan();
$plan->setName($paywayPlan['name'])
    ->setDescription($paywayPlan["description"])
    ->setType('FIXED');

// Set billing plan definitions
$paymentDefinition = new PaymentDefinition();
$paymentDefinition->setName('Pagamento ricorrente')
    ->setType('REGULAR')
    ->setFrequency($paywayPlan['payments']['interval']['type'])
    ->setFrequencyInterval($paywayPlan['payments']['interval']['value'])
    ->setCycles('12')
    ->setAmount(new Currency(array(
        'value' => (float)$paywayPlan['payments']['value'],
        'currency' => $paywayPlan['payments']['currency']
    )));

// Set merchant preferences
$merchantPreferences = new MerchantPreferences();
$merchantPreferences->setReturnUrl($payway_checkout . "&mode=subscription&status=success")
    ->setCancelUrl($payway_index . "&mode=subscription&status=cancel")
    ->setAutoBillAmount('yes')
    ->setInitialFailAmountAction('CONTINUE')
    ->setMaxFailAttempts('3')
    ->setSetupFee(new Currency(array(
        'value' => (float)$paywayPlan['payments']['value'],
        'currency' => $paywayPlan['payments']['currency']
    )));

$plan->setPaymentDefinitions(array(
    $paymentDefinition
));
$plan->setMerchantPreferences($merchantPreferences);

try {
    $createdPlan = $plan->create($apiContext);

    try {
        $patch = new Patch();
        $value = new PayPalModel('{"state":"ACTIVE"}');
        $patch->setOp('replace')
            ->setPath('/')
            ->setValue($value);
        $patchRequest = new PatchRequest();
        $patchRequest->addPatch($patch);
        $createdPlan->update($patchRequest, $apiContext);
        $patchedPlan = Plan::get($createdPlan->getId(), $apiContext);

        require_once "createSubscriptionAgreement.php";
    } catch (PayPal\Exception\PayPalConnectionException $ex) {
        echo $ex->getCode();
        echo $ex->getData();
        die($ex);
    } catch (Exception $ex) {
        die($ex);
    }
} catch (PayPal\Exception\PayPalConnectionException $ex) {
    echo $ex->getCode();
    echo $ex->getData();
    die($ex);
} catch (Exception $ex) {
    die($ex);
}
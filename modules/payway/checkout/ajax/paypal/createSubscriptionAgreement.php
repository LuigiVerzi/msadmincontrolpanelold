<?php
use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\Plan;

// Create new agreement
$startDate = gmdate("Y-m-d\TH:i:s\Z", strtotime("+" . $paywayPlan['payments']['interval']['value'] . " " . strtolower($paywayPlan['payments']['interval']['type']), time()));

$agreement = new Agreement();
$agreement->setName('PHP Tutorial Plan Subscription Agreement')
    ->setDescription('PHP Tutorial Plan Subscription Billing Agreement')
    ->setStartDate($startDate);

// Set plan id
$plan = new Plan();
$plan->setId($patchedPlan->getId());
$agreement->setPlan($plan);

// Add payer type
$payer = new Payer();
$payer->setPaymentMethod('paypal');
$agreement->setPayer($payer);


try {
    $agreement = $agreement->create($apiContext);
    $approvalUrl = $agreement->getApprovalLink();
    header("Location: " . $approvalUrl);
    exit();
} catch (PayPal\Exception\PayPalConnectionException $ex) {
    echo $ex->getCode();
    echo $ex->getData();
    die($ex);
} catch (Exception $ex) {
    die($ex);
}
?>
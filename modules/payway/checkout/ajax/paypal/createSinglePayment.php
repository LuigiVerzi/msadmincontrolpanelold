<?php
$payer = new \PayPal\Api\Payer();
$payer->setPaymentMethod('paypal');

$inputFields = new \PayPal\Api\InputFields();
$inputFields->setAllowNote(false)
    ->setNoShipping(1)
    ->setAddressOverride(0);

$presentation = new \PayPal\Api\Presentation();
$presentation->setBrandName(SW_NAME . " by Marketing Studio")
    ->setLocaleCode("IT");

$webProfile = new \PayPal\Api\WebProfile();
$webProfile->setName("Marketing Studio " . uniqid())
    ->setPresentation($presentation)
    ->setInputFields($inputFields)
    ->setTemporary(true);

try {
    $createProfileResponse = $webProfile->create($apiContext);
} catch (\PayPal\Exception\PayPalConnectionException $ex) {
    header('location: ' . $payway_index . "&status=internal_error");
    die();
}

$item = new \PayPal\Api\Item();
$item->setName($itemAry['title'])
    ->setCurrency('EUR')
    ->setQuantity(1)
    ->setPrice($priceData['iva_inclusa'])
    ->setTax($priceData['iva']);

$itemList = new \PayPal\Api\ItemList();
$itemList->setItems(array($item));

$amount = new \PayPal\Api\Amount();
$amount->setTotal($priceData['iva_inclusa']);
$amount->setCurrency('EUR');

$details = new \PayPal\Api\Details();
$details->setTax($priceData['iva']);
$details->setSubtotal($priceData['iva_esclusa']);

$transaction = new \PayPal\Api\Transaction();
$transaction->setAmount($amount)
    ->setCustom(json_encode($preorder_reference))
    ->setItemList($itemList)
    ->setDescription($itemAry['descr']);

$redirectUrls = new \PayPal\Api\RedirectUrls();
$redirectUrls
    ->setReturnUrl($payway_checkout . "&status=success")
    ->setCancelUrl($payway_index . "&status=cancel");

$payment = new \PayPal\Api\Payment();
$payment->setIntent('sale')
    ->setPayer($payer)
    ->setTransactions(array($transaction))
    ->setRedirectUrls($redirectUrls)
    ->setExperienceProfileId($createProfileResponse->getId());

try {
    $payment->create($apiContext);
    header('location: ' . $payment->getApprovalLink());
} catch (\PayPal\Exception\PayPalConnectionException $ex) {
    header('location: ' . $payway_index . "&status=internal_error");
    die();
}
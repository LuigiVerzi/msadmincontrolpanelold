<?php

require_once('../../../sw-config.php');
require('config/moduleConfig.php');

// Se l'utente non è loggato e il servizio lo permette abilito la modalità guest
if(!$MSFrameworkUsers->getUserDataFromSession() && $_GET['type'] == "ms_service") {
    $module_config['guest_mode'] = true;
}

require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkPayWay = new \MSFramework\Framework\payway();
$MSFrameworkPayWay->checkTypeExists($_GET['type'], true);

$MSFrameworkGeonames = new \MSFramework\geonames();

//controlli sicurezza e uniformazione dati
$is_payable = true;

/* DATI DI FATTURAZIONE */
$r = $MSFrameworkCMS->getCMSData('settings');
$fatturazione = json_decode($r['datiFatturazione'], true);

$got_dati_fatturazione = true;

if($fatturazione['rag_soc'] == "" || ($fatturazione['piva'] == "" && $fatturazione['cf'] == "") || $fatturazione['geo_data']['indirizzo'] == "" || $fatturazione['geo_data']['cap'] == "" || $fatturazione['geo_data']['comune'] == "" || $fatturazione['geo_data']['provincia'] == "") {
    $got_dati_fatturazione = false;
}

if($_GET['type'] == "ms_service") {
    $MSAgencyOrders = new \MSFramework\MSAgency\orders();

    $explode_ids = explode('.', $_GET['id']);
    $is_payable = $MSAgencyOrders->checkOrderPayable($explode_ids[1], $explode_ids[0]);

    $r_response_details = $MSAgencyOrders->getOrderDetails($explode_ids[1])[$explode_ids[1]];
    $r_product_details = $r_response_details['cart'][$explode_ids[0]];

    $order_details['name'] = $MSFrameworki18n->getFieldValue($r_product_details['nome']);
    $priceData = $MSFrameworkPayWay->formatPrice($r_product_details['prezzo']);
    $payMethods = $MSAgencyOrders->getAvailPayMethods();

    /* Sovrascrivo i dati di fatturazione con quelli del cliente */
    $fatturazione = $r_response_details['customer_data'];
    $fatturazione['rag_soc'] = $fatturazione['ragione_sociale'];
    $fatturazione['provincia'] = $fatturazione['citta'];

    $got_dati_fatturazione = true;
} else if($_GET['type'] == "ticket_quotation") {
    $MSFrameworkTicketsQuotation = new \MSFramework\Framework\Ticket\quotations();

    $is_payable = $MSFrameworkTicketsQuotation->checkQuotationPayable($_GET['id'], (!isset($_GET['successPayRedirect'])));

    $r_response_details = $MSFrameworkTicketsQuotation->getTicketResponseDetails($_GET['id'], 'quotation')[$_GET['id']];
    $quotation = json_decode($r_response_details['quotation'], true);

    $order_details['name'] = "Pagamento ticket assistenza";
    $priceData = $MSFrameworkPayWay->formatPrice($quotation['data']['price']);
    $payMethods = $MSFrameworkTicketsQuotation->getAvailPayMethods($_GET['id']);
} else if($_GET['type'] == "saas") {

    if($MSFrameworkSaaSEnvironments->getOwnerID() != $_SESSION['userData']['user_id'] && $_SESSION['userData']['userlevel'] != "0") {
        $MSFrameworkUsers->endUserSession();
        header("location: " . ABSOLUTE_SW_PATH_HTML . "login.php?not-logged");
        die();
    }

    $MSFrameworkSaaSSubscriptions = new \MSFramework\SaaS\subscriptions();

    $envData = $MSFrameworkSaaSEnvironments->getEnvironmentDataFromDB($_SESSION['SaaS']['saas__environments']['id'], "user_email, expiration_date")[$_SESSION['SaaS']['saas__environments']['id']];
    $data_scadenza_abbonamento_saas = (new \DateTime($envData['expiration_date']))->format("d/m/Y");
    $saas_expired = $MSFrameworkSaaSEnvironments->isExpired();

    $activeSubscriptions = $MSFrameworkSaaSSubscriptions->getAvailSubscriptions(true);
    if(!array_key_exists($_GET['id'], $activeSubscriptions)) {
        header("location: index.php");
        die();
    }

    $payMethods = $MSFrameworkSaaSSubscriptions->getAvailPayMethods();

    $order_details = $activeSubscriptions[$_GET['id']];
    $details_in_context = $MSFrameworkSaaSSubscriptions->getDetailsInContext($_GET['id']);
    $description_in_context = $details_in_context[2];

    $priceData = $details_in_context[0];

    if($description_in_context == "") {
        header('Location: index.php?cannot_downgrade');
        die();
    }
}
?>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php if(!$module_config['guest_mode']) { ?>
        <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>
    <?php } ?>

    <div id="page-wrapper" class="gray-bg">
        <?php if(!$module_config['guest_mode']) { ?>
            <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
                </div>
            </div>
        <?php } ?>

        <div class="wrapper wrapper-content animated fadeInRight">
            <input type="hidden" id="type" value="<?= $_GET['type'] ?>" />
            <input type="hidden" id="id" value="<?= $_GET['id'] ?>" />

            <div class="row">
                <?php
                if(isset($_GET['provider']) && isset($_GET['status'])) {
                    if($_GET['status'] == "cancel") {
                        $status_color = "yellow";
                        $status_icon = "fas fa-ban";
                        $status_message = "Hai annullato il pagamento.";
                    } else if($_GET['status'] == "internal_error") {
                        $status_color = "red";
                        $status_icon = "fas fa-times";
                        $status_message = "Si è verificato un errore interno durante la procedura di pagamento. Se il problema persiste, ti preghiamo di segnalarcelo utilizzando il pulsante qui sotto.";
                    } else {
                        $status_message = "Il pagamento è andato a buon fine. Grazie!";
                        $status_color = "navy";
                        $status_icon = "fas fa-check";
                    }
                    ?>
                    <div class="col-md-12" id="after_ipn_message" style="cursor: pointer;">
                        <div class="widget <?= $status_color ?>-bg p-lg text-center">
                            <div>
                                <i class="<?= $status_icon ?> fa-3x"></i>
                                <h3 style="margin-top: 20px;">
                                    <?= $status_message ?>
                                    <?php if($status_color != "navy") { ?>
                                        <br /> Non è stata apportata alcuna modifica al tuo account e non ti è stato addebitato alcun costo.
                                        <br /><br />Problemi con il pagamento? <br />
                                        <div style="text-align: center; margin-top: 20px;"><a class="text-info" href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'assistenza/ticket/' ?>"><button class="btn btn-white btn-lg" type="button"><i class="fa fa-life-ring"></i> Richiedi Assistenza</button></a></div>
                                    <?php } else { ?>
                                        <div style="text-align: center; margin-top: 20px;"><a class="text-info" href="<?= ABSOLUTE_SW_PATH_HTML ?>"><button class="btn btn-white btn-lg" type="button"><i class="fa fa-life-ring"></i> Torna alla Dashboard</button></a></div>
                                    <?php } ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if($_GET['status'] !== "success" && $is_payable) { ?>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Dettagli Ordine
                                    </div>
                                    <div class="panel-body">
                                        <p><b><?= $order_details['name'] ?></b></p>
                                        <table width="100%">
                                            <tr>
                                                <td><b>Prezzo</b></td>
                                                <td><?= CURRENCY_SYMBOL; ?> <?= $priceData['iva_esclusa'] ?> (IVA esclusa)</td>
                                            </tr>
                                            <tr>
                                                <td><b>IVA 22%</b></td>
                                                <td><?= CURRENCY_SYMBOL; ?> <?= $priceData['iva'] ?></td>
                                            </tr>
                                            <tr>
                                                <td> &nbsp; </td>
                                                <td> &nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td><b>Totale Ordine</b></td>
                                                <td><?= CURRENCY_SYMBOL; ?> <?= $priceData['iva_inclusa'] ?> <?= ($_GET['type'] == "saas" ? "fino al " . $details_in_context[1] : "") ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        Dati Fatturazione
                                    </div>
                                    <div class="panel-body">
                                        <?php if($got_dati_fatturazione) { ?>
                                            <address>
                                                <strong><?= $fatturazione['rag_soc'] ?></strong><br>

                                                <?php if($fatturazione['geo_data']) { ?>
                                                <?= $fatturazione['geo_data']['indirizzo'] ?> <?php ($fatturazione['geo_data']['street_number'] != "" ? ", " . $fatturazione['geo_data']['street_number'] : "") ?><br>
                                                <?= $fatturazione['geo_data']['cap'] ?> - <?= $MSFrameworkGeonames->getDettagliComune($fatturazione['geo_data']['comune'], "name")['name'] ?> (<?= $MSFrameworkGeonames->getDettagliProvincia($fatturazione['geo_data']['provincia'], "name")['name'] ?>)<br><br>
                                                <?php } else { ?>
                                                    <?= $fatturazione['indirizzo'] ?>
                                                    <?= $fatturazione['cap'] ?> - <?= $fatturazione['comune']; ?> (<?= $fatturazione['provincia']; ?>)<br><br>
                                                <?php } ?>

                                                <?php if($fatturazione['piva'] != "") { ?>
                                                    P.IVA <?= $fatturazione['piva'] ?><br>
                                                <?php }  ?>

                                                <?php if($fatturazione['cf'] != "") { ?>
                                                    CF <?= $fatturazione['cf'] ?><br>
                                                <?php } ?>
                                            </address>
                                        <?php } else { ?>
                                            <p><div class="alert alert-danger">I dati di fatturazione sono mancanti o incompleti, utilizza il pulsante "<?= (!$got_dati_fatturazione ? "Aggiorna" : "Modifica") ?>" per aggiornare i tuoi dati.</div></p>
                                        <?php } ?>
                                    </div>
                                    <?php if($fatturazione['geo_data']) { ?>
                                        <div class="panel-footer" style="text-align: right;">
                                            <a style="color: unset" href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'settings/' ?>"><button type="button" class="btn btn-default btn-sm"><?= (!$got_dati_fatturazione ? "Aggiorna" : "Modifica") ?></button></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        Assistenza
                                    </div>
                                    <div class="panel-body">
                                        <div>Problemi o dubbi?</div>
                                        <div style="text-align: center; margin-top: 20px;"><a style="color: unset" href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'assistenza/ticket/' ?>"><button class="btn btn-white btn-lg" type="button"><i class="fa fa-life-ring"></i> Richiedi Assistenza</button></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9">
                        <div class="panel">
                            <div class="panel-body">
                                <?php if(!$got_dati_fatturazione) { ?>
                                <div class="alert alert-danger">
                                    <b>Attenzione!</b> E' necessario aggiornare i dati di fatturazione prima di poter procedere all'acquisto.
                                    <div style="text-align: center; margin-top: 20px;">
                                        <a style="color: unset" href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML . "settings/" ?>"><button class="btn btn-white btn-lg" type="button"><i class="fa fa-paste"></i> Aggiorna</button></a>
                                    </div>
                                </div>
                            </div>
                            <?php } else { ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row paysaas_container">
                                            <?php
                                            foreach($payMethods as $payMethodK => $payMethod) {
                                                ?>
                                                <div class="col-lg-12">
                                                    <div class="widget style1" data-provider="<?= $payMethodK ?>">
                                                        <div class="row vertical-align">
                                                            <div class="col-xs-4">
                                                                <i class="<?= $payMethod['fa_class'] ?> fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-8 text-right">
                                                                <span>Paga con</span>
                                                                <h2 class="font-bold servicename"><?= $MSFrameworki18n->getFieldValue($payMethod['display_name']) ?></h2>
                                                                <div class="sottotitolo-widget"><i>Elaborazione: <?= ($payMethodK == "bonifico" ? "5/7gg lavorativi" : "immediata") ?></i></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="alert alert-success" style="text-align: center;">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <span class="fas fa-shield-alt fa-3x text-navy" style="vertical-align: middle;"></span>
                                                </div>
                                                <div class="col-md-11">
                                                    <span>Utilizziamo i migliori standard di sicurezza per consentirti di pagare in tranquillità. I tuoi dati sono <b>sempre</b> al sicuro.</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="right_box_renew" style="display: none;">

                                            <div class="well well-lg">
                                                <h3>
                                                    Riepilogo Ordine
                                                </h3>
                                                <?php if($_GET['type'] == "saas") { ?>
                                                    <div>Il tuo abbonamento sarà rinnovato <span id="renewing_end_date">fino al <?= $details_in_context[1] ?></span> con il pacchetto <?= $order_details['name'] ?>.</div>
                                                    <?php if($description_in_context != "") { ?>
                                                        <div class="m-t-md"><i><?= $description_in_context ?></i></div>
                                                    <?php } ?>
                                                <?php } else if($_GET['type'] == "ticket_quotation") { ?>
                                                    <div>Il tuo ticket sarà posto in lavorazione non appena riceveremo il pagamento. La data di consegna stimata potrebbe variare in base ai tempi di ricezione del pagamento.</div>
                                                <?php } else if($_GET['type'] == "ms_service") { ?>
                                                    <div>Il tuo ordine sarà posto in lavorazione non appena riceveremo il pagamento. La data di consegna stimata potrebbe variare in base ai tempi di ricezione del pagamento.</div>
                                                <?php } ?>
                                            </div>


                                            <div style="1px solid #dedede">
                                                <a class="btn btn-primary btn-rounded btn-block btn-lg btn_renew" href="javascript:void(0)">Conferma e paga con <span id="servicename"></span></a>
                                            </div>

                                            <div id="infobonifico" style="display: none;">
                                                <div class="alert alert-warning">
                                                    <div>I tempi di elaborazione di questo metodo di pagamento richiedono 5/7 giorni lavorativi.</div>
                                                    <div>
                                                        <?php
                                                        $causale = "Contattaci per conoscere la causale";

                                                        if($_GET['type'] == "saas") {
                                                            echo "La data di scadenza del tuo abbonamento sarà determinata a partire dalla data di elaborazione del pagamento.";
                                                            $causale = "Rinnovo abbonamento ENV ID: " . $MSFrameworkSaaSEnvironments->getCurrentEnvironmentID();
                                                        } else if($_GET['type'] == "ticket_quotation") {
                                                            echo "La data di consegna stimata potrebbe variare in base ai tempi di ricezione del pagamento.";
                                                            $causale = "Pagamento ticket assistenza ID: " . $_GET['id'];
                                                        } else if($_GET['type'] == "ms_service") {
                                                            echo "La data di consegna stimata potrebbe variare in base ai tempi di ricezione del pagamento.";
                                                            $causale = "Pagamento servizi Marketing Studio: " . $_GET['id'];
                                                        }
                                                        ?>
                                                    </div>

                                                    <div style="background-color: #fff; padding: 30px; margin-top: 15px;">
                                                        Inviare il pagamento a <br /><br />
                                                        <table width="100%">
                                                            <tr>
                                                                <td><b>Intestatario</b></td>
                                                                <td><?= $payMethods['bonifico']['pay_data']['intestatario'] ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Banca</b></td>
                                                                <td><?= $payMethods['bonifico']['pay_data']['banca'] ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>IBAN</b></td>
                                                                <td><?= $payMethods['bonifico']['pay_data']['iban'] ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Importo</b></td>
                                                                <td><?= CURRENCY_SYMBOL; ?><?= $priceData['iva_inclusa'] ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Causale</b></td>
                                                                <td><?= $causale ?></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } else if(!$_GET['status']) { ?>
                    <div class="col-lg-12">
                        <div class="alert alert-danger" style="text-align: center;">
                            <div class="row">
                                <div class="col-md-1">
                                    <span class="fas fa-exclamation-triangle fa-3x text-danger" style="vertical-align: middle;"></span>
                                </div>
                                <div class="col-md-11">
                                    <span>
                                        <b>La pagina cercata non esiste.</b><br>
                                        Probabilmente l'ordine è già stato pagato o non è più disponibile.
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    <script src="https://js.stripe.com/v3/"></script>

</div>
</div>

</body>
</html>
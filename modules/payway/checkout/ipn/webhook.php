<?php
require_once('../../../../sw-config.php');

$providerData = (new \MSFramework\Framework\payway())->getProviderData('paypal');

$settings_data = $providerData['pay_data'][PAYMETHODS_USE_KEY];
$apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential($settings_data['client_id'], $settings_data['secret']));
$apiContext->setConfig(
    array(
        'mode' => (PAYMETHODS_USE_KEY == "sandbox" ? PAYMETHODS_USE_KEY : "live")
    )
);

use \PayPal\Api\VerifyWebhookSignature;
use \PayPal\Api\WebhookEvent;

$bodyReceived = file_get_contents('php://input');
$headers = getallheaders();
$headers = array_change_key_case($headers, CASE_UPPER);

$signatureVerification = new VerifyWebhookSignature();
$signatureVerification->setWebhookId("1A0527301T3362401");
$signatureVerification->setAuthAlgo($headers['PAYPAL-AUTH-ALGO']);
$signatureVerification->setTransmissionId($headers['PAYPAL-TRANSMISSION-ID']);
$signatureVerification->setCertUrl($headers['PAYPAL-CERT-URL']);
$signatureVerification->setTransmissionSig($headers['PAYPAL-TRANSMISSION-SIG']);
$signatureVerification->setTransmissionTime($headers['PAYPAL-TRANSMISSION-TIME']);

$webhookEvent = new WebhookEvent();
$webhookEvent->fromJson($bodyReceived);
$signatureVerification->setRequestBody($webhookEvent);
$request = clone $signatureVerification;

try {
    $output = $signatureVerification->post($apiContext);
} catch (Exception $ex) {
    print_r($ex->getMessage());
    exit(1);
}

$return_data = json_decode($bodyReceived, true);

if ($return_data['event_type'] == 'PAYMENT.SALE.COMPLETED') {

    // L'ordine è un pagamento una tantum
    if (isset($return_data['resource']['custom'])) {
        $preorder_reference = json_decode($return_data['resource']['custom'], true);
    } else { // L'ordine è un abbonamento
        $preorder_reference = (new \MSFramework\Framework\payway())->getSubscriptionAgreement($return_data['resource']['billing_agreement_id']);
        $MSFrameworkDatabase->getAssoc("UPDATE `" . FRAMEWORK_DB_NAME . "`.`subscription__agreements` SET state = 1 WHERE agreement_id = :agreement_id", array(':agreement_id' => $return_data['resource']['billing_agreement_id']));
    }

    if ($preorder_reference) {
        if ($preorder_reference[0] == "saas__transactions") {
            $MSFrameworkSaaSEnvironments->renew($preorder_reference, $return_data);
        } else if ($preorder_reference[0] == "ticket__transactions") {
            (new \MSFramework\Framework\Ticket\quotations())->changeStatus($preorder_reference, 'paid', $return_data);
        } else if ($preorder_reference[0] == "ms_agency__transactions") {
            (new \MSFramework\MSAgency\orders())->changeStatus($preorder_reference, 1, $return_data);
        }
    }
} else if ($return_data['event_type'] == 'BILLING.SUBSCRIPTION.CANCELLED') {

    // L'ordine è un pagamento una tantum
    $preorder_reference = (new \MSFramework\Framework\payway())->getSubscriptionAgreement($return_data['resource']['id']);
    $MSFrameworkDatabase->getAssoc("UPDATE `" . FRAMEWORK_DB_NAME . "`.`subscription__agreements` SET state = 0 WHERE agreement_id = :agreement_id", array(':agreement_id' => $return_data['resource']['id']));

    if ($preorder_reference) {
        if ($preorder_reference[0] == "saas__transactions") {
            $MSFrameworkSaaSEnvironments->disable($preorder_reference);
        } else if ($preorder_reference[0] == "ms_agency__transactions") {
            (new \MSFramework\MSAgency\orders())->changeStatus($preorder_reference, 3, $return_data);
        }
    }
}
$(document).ready(function() {
    if($('.abb-highlight').length != 0) {
        $('.abb-container:not(.abb-highlight)').css('transform', 'scale(0.93)');
    }

    $('.paysaas_container > div').on('click', function() {
        if($(this).find('.widget').hasClass('disabled')) {
            return false;
        }

        $('.paysaas_container .widget').removeClass('active');

        $(this).find('.widget').addClass('active');

        if($('.paysaas_container .widget.active').length == 0) {
            $('#right_box_renew').hide();
        } else {
            provider = $(this).find('.widget').data('provider');
            servicename = $(this).find('.widget').find('.servicename').html();

            $('#right_box_renew').show();
            $('#right_box_renew .btn_renew').show();

            $('#right_box_renew #infobonifico').hide();
            $('#right_box_renew #renewing_end_date').show();

            $('#right_box_renew .btn_renew #servicename').html(servicename);

            if(provider == "bonifico") {
                $('#right_box_renew .btn_renew').hide();
                $('#right_box_renew #infobonifico').show();
                $('#right_box_renew #renewing_end_date').hide();
            }

            $('#right_box_renew .btn_renew').unbind('click');
            $('#right_box_renew .btn_renew').on('click', function() {
                $(this).addClass('disabled');
                if(provider == "stripe") {
                    $.ajax({
                        url: 'ajax/checkout.php?id=' + $('#id').val() + "&type=" + $('#type').val() + "&provider=" + provider,
                        dataType: "json",
                        async: false,
                        success: function(data) {
                            var stripe = Stripe(data.ms_stripe_public);

                            stripe.redirectToCheckout({
                                sessionId: data.id
                            }).then(function (result) {
                                bootbox.error(result.error.message);
                            });

                            $(this).removeClass('disabled');
                        }
                    })
                } else {
                    window.location.href = 'ajax/checkout.php?id=' + $('#id').val() + "&type=" + $('#type').val() + "&provider=" + provider;
                }

            });
        }
    })

    $('#silly_downgrade_box, #after_ipn_message').on('click', function() {
        $(this).fadeOut();
    });

    $('.priceDetails').on('click', function() {
        cur_tooltip_index = $('.priceDetails').index(this);
        console.log(cur_tooltip_index);

        var dialog = bootbox.dialog({
            message: $('.priceDetailsTextContainer:eq(' + cur_tooltip_index + ')').html(),
            buttons: {
                cancel: {
                    label: "OK",
                    className: 'btn-info'
                }
            }
        });
    })
})
<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = ($MSFrameworkDatabase->getCount("SELECT * from subscriptions__subscribers WHERE cliente = :id", array(':id' => $_POST['pID'])) ? 'update' : 'insert');

$can_save = true;

if($_POST['pID'] == "" || $_POST['pAbbonamento'] == "" || $_POST['pDataInizio'] == "" || $_POST['pDataFine'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_var_date = array('pDataInizio', 'pDataFine');
foreach($array_var_date as $curPostDate) {
    $date_tmp = DateTime::createFromFormat('d/m/Y H:i', $_POST[$curPostDate]);

    if(!$date_tmp) {
        echo json_encode(array("status" => "expire_not_valid"));
        die();
    }
    $_POST[$curPostDate] = $date_tmp->format('Y-m-d h:i');
}


if($_POST['pDataInizio'] >= $_POST['pDataFine']) {
    echo json_encode(array("status" => "past_expiration_date"));
    die();
}

$array_to_save = array(
    "piano" => $_POST['pAbbonamento'],
    "data_inizio" => $_POST['pDataInizio'],
    "data_fine" => $_POST['pDataFine']
);

if($db_action == "insert") {
    $array_to_save['cliente'] = $_POST['pID'];
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO subscriptions__subscribers ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE subscriptions__subscribers SET $stringForDB[1] WHERE cliente = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

// Aggiorna le info riguardo i moduli esterni
if(isset($_POST['pExtraFields']['affiliations_code'])) {

    /* AGGIORNAMENTO CODICE SPONSOR */
    $clean_code = $MSFrameworkUrl->cleanString($_POST['pExtraFields']['affiliations_code']);

    // Controllo se i campi sono compilati correttamente
    if(empty($clean_code) || strlen($clean_code) < 3) {
        echo json_encode(array("status" => "input_error", "input" => "affiliations_code", "message" => $MSFrameworki18n->gettext('Il codice deve contenere almeno 3 caratteri.')));
        die();
    }

    if($clean_code !== strtolower($_POST['pExtraFields']['affiliations_code'])) {
        echo json_encode(array("status" => "input_error", "input" => "affiliations_code", "message" => $MSFrameworki18n->gettext('Il codice cliente non ammette caratteri speciali e spazi.')));
        die();
    }

    $customer_id = (new \MSFramework\Affiliations\customers())->getCustomerIDByCode($clean_code);

    if($customer_id > 0 && $customer_id != $_POST['pID']) {
        echo json_encode(array("status" => "input_error", "input" => "affiliations_code", "message" => $MSFrameworki18n->gettext('Questo codice utente è già utilizzato.')));
        die();
    }

    (new \MSFramework\Affiliations\customers())->setCustomerCode($_POST['pID'], $_POST['pExtraFields']['affiliations_code']);

}

echo json_encode(array("status" => "ok", "id" => $_POST['pID']));
die();
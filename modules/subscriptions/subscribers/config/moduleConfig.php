<?php
/**
 * MSAdminControlPanel
 * Date: 1/02/2019
 */

$module_id = 'subscriptions_subscribers';
$module_config = $MSSoftwareModules->getModuleDetailsFromID($module_id);
$MSSoftwareModules->accessGranted($module_id, true);

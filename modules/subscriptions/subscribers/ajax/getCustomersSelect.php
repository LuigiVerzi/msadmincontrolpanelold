<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$term = $_GET['q'];

$return = array(
    'items' => array()
);

if(strlen($term) < 3) {
    die(json_encode($return));
}

$lista_clienti = $MSFrameworkDatabase->getAssoc("SELECT * FROM customers WHERE CONCAT(nome, ' ', cognome , ' ', telefono_casa, ' ', telefono_cellulare, ' ', email) LIKE :term", array(':term' => '%' . $term . '%'));

foreach($lista_clienti as $c) {
    $c['dati_fatturazione'] = json_decode($c['dati_fatturazione'], true);
    $return['items'][] = array(
        'id' => $c['id'],
        'text' => '<b>' . $c['nome'] . ' ' . $c['cognome'] . '</b>' . (!empty($c['dati_fatturazione']['ragione_sociale']) ? ' - ' . $c['dati_fatturazione']['ragione_sociale'] : '') . ($c['email'] ? ' (' . $c['email'] . ')' : '') . ($c['telefono_cellulare'] ? ' (' . $c['telefono_cellulare'] . ')' : '') . ($c['telefono_casa'] ? ' (' . $c['telefono_casa'] . ')' : '')
    );
}

die(json_encode($return));

?>
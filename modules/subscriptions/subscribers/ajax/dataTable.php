<?php
/**
 * MSAdminControlPanel
 * Date: 01/02/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$lista_piani = (new \MSFramework\Subscriptions\plans())->getPlanDetails();
$MSFrameworkCustomers = new \MSFramework\customers();

if($_GET['source'] == 'subscriber') {
    $columns = array(
        array(
            'db' => 'id',
            'dt' => 'DT_buttonsLimit',
            'formatter' => function ($d, $row) {
                return 'edit';
            }
        ),
        array(
            'db' => 'id',
            'dt' => 'DT_RowId',
            'formatter' => function ($d, $row) {
                return $d;
            }
        ),
        array('db' => 'CONCAT(nome,cognome,email)', 'dt' => 0,
            'formatter' => function ($d, $row) {
                Global $MSFrameworkCustomers;

                $cliente = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

                $info_cliente = array();
                if (!empty($cliente['email'])) {
                    $info_cliente[] = $cliente['email'];
                }
                if (!empty($cliente['telefono_casa'])) {
                    $info_cliente[] = $cliente['telefono_casa'];
                }
                if (!empty($cliente['telefono_cellulare'])) {
                    $info_cliente[] = $cliente['telefono_cellulare'];
                }

                return '<h2 class="no-margins">' . $cliente['nome'] . ' ' . $cliente['cognome'] . '</h2><small>' . implode(' - ', $info_cliente) . '</small>';

            }
        ),
        array('db' => '(SELECT piano FROM subscriptions__subscribers WHERE subscriptions__subscribers.cliente = customers.id)', 'dt' => 1,
            'formatter' => function ($d, $row) {
                Global $MSFrameworki18n, $lista_piani;
                if ($d) {
                    return $MSFrameworki18n->getFieldValue($lista_piani[$d]['titolo']);
                } else {
                    return '<small class="text-muted">N/A</small>';
                }

            }
        ),
        array('db' => '(SELECT data_inizio FROM subscriptions__subscribers WHERE subscriptions__subscribers.cliente = customers.id)', 'dt' => 2,
            'formatter' => function ($d, $row) {

                if ($d) {
                    return array("display" =>  date('d/m/Y H:i', strtotime($d)), 'sort' => strtotime($d));
                } else {
                    return array("display" => '<small class="text-muted">N/A</small>', 'sort' => strtotime($d));
                }
            }
        ),
        array('db' => '(SELECT data_fine FROM subscriptions__subscribers WHERE subscriptions__subscribers.cliente = customers.id)', 'dt' => 3,
            'formatter' => function ($d, $row) {
                if ($d) {
                    return array("display" =>  '<span class="label label-' . (strtotime($d) < time() ? 'danger' : (strtotime($d) < strtotime('+5 day') ? 'warning' : 'primary')) . '">' . date('d/m/Y H:i', strtotime($d)) . '</span>', 'sort' => strtotime($d));
                } else {
                    return array("display" => '<small class="text-muted">Nessuno</small>', 'sort' => strtotime($d));
                }
            }
        )
    );

    echo json_encode(
        $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'customers', 'id', $columns, null, 'customers.id IN (SELECT subscriptions__subscribers.cliente FROM subscriptions__subscribers WHERE subscriptions__subscribers.data_fine > NOW())')
    );
} else {

    $columns = array(
        array(
            'db' => 'id',
            'dt' => 'DT_buttonsLimit',
            'formatter' => function ($d, $row) {
                return 'edit';
            }
        ),
        array(
            'db' => 'id',
            'dt' => 'DT_RowId',
            'formatter' => function ($d, $row) {
                return $d;
            }
        ),
        array('db' => 'CONCAT(nome,cognome,email)', 'dt' => 0,
            'formatter' => function ($d, $row) {
                Global $MSFrameworkCustomers;

                $cliente = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

                $info_cliente = array();
                if (!empty($cliente['email'])) {
                    $info_cliente[] = $cliente['email'];
                }
                if (!empty($cliente['telefono_casa'])) {
                    $info_cliente[] = $cliente['telefono_casa'];
                }
                if (!empty($cliente['telefono_cellulare'])) {
                    $info_cliente[] = $cliente['telefono_cellulare'];
                }

                return '<h2 class="no-margins">' . $cliente['nome'] . ' ' . $cliente['cognome'] . '</h2><small>' . implode(' - ', $info_cliente) . '</small>';

            }
        ),
        array('db' => '(SELECT data_fine FROM subscriptions__subscribers WHERE subscriptions__subscribers.cliente = customers.id)', 'dt' => 1,
            'formatter' => function ($d, $row) {
                if ($d) {
                    return array("display" => '<span class="label label-danger">Scaduto il ' . date('d/m/Y H:i', strtotime($d)) . '</span>', 'sort' => strtotime($d));
                } else {
                    return array("display" => '<small class="text-muted">Nessuno</small>', 'sort' => strtotime($d));
                }
            }
        )
    );

    echo json_encode(
        $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'customers', 'id', $columns, null, 'customers.id NOT IN (SELECT subscriptions__subscribers.cliente FROM subscriptions__subscribers WHERE subscriptions__subscribers.data_fine > NOW())')
    );
}
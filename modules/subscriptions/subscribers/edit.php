<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = array();
if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM customers LEFT JOIN subscriptions__subscribers ON subscriptions__subscribers.cliente = customers.id WHERE customers.id = :id", array(':id' => $_GET['id']), true);
}

if(!$r) {
    header('Location: index.php');
}

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Modifica abbonamento</h2>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Cliente</label>
                                    <?php echo '<p class="form-control"><a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'customers/list/edit.php?id=' . $r['cliente'] . '" target="_blank">' . $r['nome'] . ' ' . $r['cognome'] . ' (' . $r['email']  . ')</a></p>'; ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Piano d'abbonamento*</label>
                                    <select id="abbonamento" name="abbonamento" class="form-control required">
                                        <?php foreach((new \MSFramework\Subscriptions\plans())->getPlanDetails() as $impK => $impV) { ?>
                                            <option value="<?php echo $impK ?>" <?php if($r['piano'] == $impK) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($impV['titolo'], true) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Inizio Abbonamento*</label>
                                    <div class="input-group date scadenza">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control required" id="data_inizio" name="data_inizio" value="<?php echo ($r['data_inizio'] != '') ? date("d/m/Y H:i", strtotime($r['data_inizio'])) : '' ?>">
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Scadenza Abbonamento*</label>
                                    <div class="input-group date scadenza">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control required" id="data_fine" name="data_fine" value="<?php echo ($r['data_fine'] != '') ? date("d/m/Y H:i", strtotime($r['data_fine'])) : '' ?>">
                                    </div>
                                </div>
                            </div>

                            <?php if($MSFrameworkCMS->checkExtraFunctionsStatus('affiliations')) { ?>
                                <h2 class="title-divider">Stato Affiliazione</h2>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Codice Sponsor</label>
                                        <input  type="text" id="affiliations_code" name="affiliations_code" value="<?= (new \MSFramework\Affiliations\customers())->getCustomerCode($r['cliente']); ?>" class="form-control extra_fields">
                                    </div>
                                </div>
                            <?php } ?>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
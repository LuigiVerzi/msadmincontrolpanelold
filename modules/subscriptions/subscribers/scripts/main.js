$(document).ready(function() {
    loadStandardDataTable('subscriber_table_list', true, {"ajax": 'ajax/dataTable.php?source=subscriber'});
    allowRowHighlights('subscriber_table_list');
    manageGridButtons('subscriber_table_list');

    loadStandardDataTable('not_subscriber_table_list', true, {"ajax": 'ajax/dataTable.php?source=not_subscriber'});
    allowRowHighlights('not_subscriber_table_list');
    manageGridButtons('not_subscriber_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    if($('#cliente').length) {
        $('#cliente').select2({
            ajax: {
                url: "ajax/getCustomersSelect.php",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.items
                    }
                },
                cache: true
            },
            placeholder: 'Cerca un Cliente',
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 0
        });
    }

    $('.input-group.date').datetimepicker({
        keyboardNavigation: false,
        autoclose: true,
        format: 'dd/mm/yyyy hh:ii'
    });

}

function moduleSaveFunction() {

    var extra_fields = {};
    $('.extra_fields').each(function () {
        var name = $(this).attr('name');
        extra_fields[name] = $(this).val();
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pCliente": $('#cliente').val(),
            "pAbbonamento": $('#abbonamento').val(),
            "pDataInizio": $('#data_inizio').val(),
            "pDataFine": $('#data_fine').val(),
            "pExtraFields": extra_fields
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data non è valido. Impossibile continuare.");
            } else if(data.status == "past_expiration_date") {
                bootbox.error("La data di scadenza deve essere successiva a quella di inizio.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
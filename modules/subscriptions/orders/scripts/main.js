$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('.copy_order_info').on('click', function (e) {
        e.preventDefault();

        var mainDiv = $('.address_to_copy')[0];
        var startNode = mainDiv;
        var endNode = mainDiv.lastChild;

        var range = document.createRange();
        range.setStart(startNode, 0);
        range.setEnd(endNode, 1);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);

        document.execCommand("copy");
        if (window.getSelection) {
            if (window.getSelection().empty) {
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) {
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) {
            document.selection.empty();
        }
        document.activeElement.blur();

        toastr["info"]('L\'indirizzo è stato copiato correttamente negli appunti', 'Copiato negli appunti');
    });
}
function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pOrderStatus": $('#order_status').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "courier_not_set") {
                bootbox.error("Specifica il nome del corriere o selezionane uno dalla lista.");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
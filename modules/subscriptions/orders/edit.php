<?php
/**
 * MSAdminControlPanel
 * Date: 07/05/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM subscriptions__orders WHERE id = :id", array($_GET['id']), true);
    $info_cliente = (new \MSFramework\customers())->getCustomerDataFromDB($r['user_id']);
    $info_fatturazione = json_decode($r['info_fatturazione'], true);
    $info_spedizione = json_decode($r['info_spedizione'], true);
    $coupons_used = $r['coupons_used'];
    $info_abbonamento = json_decode($r['plan_info'], true);

}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php
                $operation_id = (new \MSFramework\Fatturazione\vendite())->getOperationIDByReference('subscriptions', $r['id']);
                if($operation_id) {
                    $customToolbarButton = '<a class="btn btn-info" href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . (new \MSSoftware\modules())->getModuleDetailsFromID("fatturazione_vendite")['path'] . 'edit.php?id=' . $operation_id . '">Vedi Documento</a>';
                }
                require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php");
                ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Data Ordine</label>
                                    <div><?php echo (new DateTime($r['order_date']))->format("d/m/Y H:i") ?></div>
                                </div>

                                <div class="col-sm-4">
                                    <label>Data ultimo aggiornamento</label>
                                    <div><?php echo (new DateTime($r['order_edit_date']))->format("d/m/Y H:i") ?></div>
                                </div>

                                <div class="col-sm-4">
                                    <label>Stato Ordine</label>
                                    <input type="hidden" id="original_order_status" value="<?php echo $r['order_status'] ?>" />
                                    <select id="order_status" name="order_status" class="form-control">
                                        <?php
                                        foreach((new \MSFramework\Subscriptions\orders)->getStatus() as $statK => $statV) {
                                            ?>
                                            <option value="<?php echo $statK ?>" <?php if($r['order_status'] == (string)$statK) { echo "selected"; } ?>><?php echo $statV ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <h2 class="title-divider">Cliente</h2>
                            <?php if($info_cliente) { ?>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Nome</label>
                                        <div><a id="customer_name" href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>customers/list/edit.php?id=<?php echo $info_cliente['id']; ?>"><?php echo $info_cliente['nome'] . ' ' . $info_cliente['cognome']; ?></a></div>
                                    </div>

                                    <div class="col-sm-4">
                                        <label>Email</label>
                                        <div id="customer_email"><?php echo $info_cliente['email'] ?></div>
                                    </div>

                                    <div class="col-sm-4">
                                        <label>Cellulare</label>
                                        <div><?php echo (!empty($info_cliente['cellulare']) ? $info_cliente['cellulare'] : 'Non Specificato'); ?></div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <?php if($r['guest_email']) { ?>
                                    <div>L'utente ha effettuato l'acquisto in modalità <b>Ospite</b>.</div>
                                    <br>
                                    <label>Email</label>
                                    <div id="customer_email"><?php echo $r['guest_email']; ?></div>
                                <?php } else { ?>
                                <div>L'utente non risulta registrato, probabilmente il suo account è stato eliminato.</div>
                                <?php } ?>
                            <?php } ?>

                            <div class="row">
                                <div class="col-sm-6">
                                    <h2 class="title-divider">Fatturazione <a class="label label-info copy_order_info" href="#" style="font-size: 11px; top: -3px; position: relative;">Copia Indirizzo</a></h2>
                                    <div class="address_to_copy">
                                        <div><?php echo $info_fatturazione['nome'] ?> <?php echo $info_fatturazione['cognome'] ?></div>
                                        <div><?php echo $info_fatturazione['indirizzo'] ?> <?php echo $info_fatturazione['indirizzo_2'] ?></div>
                                        <div><?php echo $info_fatturazione['cap'] ?> <?php echo $info_fatturazione['comune'] ?></div>
                                        <div><?php echo $info_fatturazione['citta'] ?></div>
                                        <div><?php echo (new \MSFramework\geonames())->getCountryDetails($info_fatturazione['stato'])[$info_fatturazione['stato']]['name'] ?></div>
                                    </div>
                                    <p>
                                       <br><b>Pagamento tramite:</b> <?= ucfirst($r["payment_type"]); ?>
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <h2 class="title-divider">Spedizione <a class="label label-info copy_order_info" href="#" style="font-size: 11px; top: -3px; position: relative;">Copia Indirizzo</a></h2>
                                    <?php
                                    if($info_spedizione['spedizione_diversa_fatturazione'] != "1") {
                                        $info_spedizione = $info_fatturazione;
                                    }
                                    ?>
                                    <div class="address_to_copy">
                                        <div><?php echo $info_spedizione['nome'] ?> <?php echo $info_spedizione['cognome'] ?></div>
                                        <div><?php echo $info_spedizione['indirizzo'] ?> <?php echo $info_spedizione['indirizzo_2'] ?></div>
                                        <div><?php echo $info_spedizione['cap'] ?> <?php echo $info_spedizione['comune'] ?></div>
                                        <div><?php echo $info_spedizione['citta'] ?></div>
                                        <div><?php echo (new \MSFramework\geonames())->getCountryDetails($info_spedizione['stato'])[$info_spedizione['stato']]['name'] ?></div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="title-divider">Dettagli Abbonamento</h2>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Pacchetto</label>
                                    <div><?php echo $MSFrameworki18n->getFieldValue($info_abbonamento['titolo']); ?></div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Prezzo</label>
                                    <?php if($r['is_trial']) { ?>
                                        <div>Prova gratuita</div>
                                    <?php } else { ?>
                                    <div><?= CURRENCY_SYMBOL; ?> <?= number_format((new \MSFramework\Fatturazione\imposte())->getPriceToShow($info_abbonamento['prezzo_annuale'])['tax'],2,',','.'); ?> (imposta <?php echo $info_abbonamento['imposta'] ?>% inclusa)</div>
                                    <?php } ?>
                                </div>
                            </div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pTitolo'] == "" || $_POST['pPrezzoMensile'] == "" || $_POST['pPrezzoAnnuale'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_var_prezzi = array('pPrezzoMensile', 'pPrezzoAnnuale');
foreach($array_var_prezzi as $curPostPrice) {
    $_POST[$curPostPrice] = str_replace(",", ".", $_POST[$curPostPrice]);
    if(!strstr($_POST[$curPostPrice], ".") && $_POST[$curPostPrice] != "") {
        $_POST[$curPostPrice] .= ".00";
    }

    if(!is_numeric($_POST[$curPostPrice]) && $_POST[$curPostPrice] != "") {
        echo json_encode(array("status" => "price_numeric"));
        die();
    }
}

if(isset($_POST['pExtraFields']['provvigione_affiliato'])) {
    $_POST['pExtraFields']['provvigione_affiliato'] = str_replace(",", ".", $_POST['pExtraFields']['provvigione_affiliato']);
    if(!is_numeric($_POST['pExtraFields']['provvigione_affiliato'])) {
        echo json_encode(array("status" => "price_numeric"));
        die();
    }
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT titolo, descrizione, gallery FROM subscriptions__plans WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pTitolo'], 'oldValue' => $r_old_data['titolo']),
        array('currentValue' => $_POST['pDescrizione'], 'oldValue' => $r_old_data['descrizione']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$uploader = new \MSFramework\uploads('SUBSCRIPTION_PLANS');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$array_to_save = array(
    "titolo" => $MSFrameworki18n->setFieldValue($r_old_data['titolo'], $_POST['pTitolo']),
    "descrizione" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione'], $_POST['pDescrizione']),
    "prezzo_mensile" => $_POST['pPrezzoMensile'],
    "prezzo_annuale" => $_POST['pPrezzoAnnuale'],
    "trial_days" => $_POST['pTrialDays'],
    "imposta" => $_POST['pImposta'],
    "gallery" => json_encode($ary_files),
    "extra_fields" => json_encode($_POST['pExtraFields'])
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO subscriptions__plans ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE subscriptions__plans SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }
    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['gallery'], true));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
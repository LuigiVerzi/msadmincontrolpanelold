$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initOrak('images', 25, 500, 300, 200, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
    initTinyMCE( '#descrizione');

    $('#enable_trial').on('ifToggled', function () {
        if($(this).prop('checked')) {
            $('#trial_days_container').show();
        } else {
            $('#trial_days_container').hide();
        }
    });
}

function moduleSaveFunction() {

    var extra_fields = {};
    $('.extra_fields').each(function () {
        var name = $(this).attr('name');
        extra_fields[name] = $(this).val();
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pTitolo": $('#titolo').val(),
            "pPrezzoMensile": $('#prezzo_mensile').val(),
            "pPrezzoAnnuale": $('#prezzo_annuale').val(),
            "pDescrizione": tinymce.get('descrizione').getContent(),
            "pImposta": $('#imposta').val(),
            "pTrialDays": $('#trial_days').val(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pExtraFields": extra_fields
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
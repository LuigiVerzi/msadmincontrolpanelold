<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = (new \MSFramework\Subscriptions\plans())->getPlanDetails($_GET['id'])[$_GET['id']];
}

if($r) {
    if(json_decode($r['extra_fields'])) {
        $r['extra_fields'] = json_decode($r['extra_fields'], true);
    }
}

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Titolo*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['titolo']) ?>"></i></span>
                                    <input id="titolo" name="titolo" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['titolo'])) ?>">
                                </div>
                                <div class="col-sm-3">
                                    <label>Prezzo Mensile <?= CURRENCY_SYMBOL; ?> (<?php echo ((new \MSFramework\Fatturazione\imposte())->getPriceType() == "0") ? "IVA Inclusa" : "Senza IVA" ?>) *</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Impostare '0' per gratuito"></i></span>
                                    <input type="number" min="0" id="prezzo_mensile" name="prezzo_mensile" class="form-control required" value="<?php echo htmlentities($r['prezzo_mensile']) ?>">
                                </div>
                                <div class="col-sm-3">
                                    <label>Prezzo Annuale <?= CURRENCY_SYMBOL; ?> (<?php echo ((new \MSFramework\Fatturazione\imposte())->getPriceType() == "0") ? "IVA Inclusa" : "Senza IVA" ?>) *</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Impostare '0' per gratuito"></i></span>
                                    <input type="number" min="0" id="prezzo_annuale" name="prezzo_annuale" class="form-control required" value="<?php echo htmlentities($r['prezzo_annuale']) ?>">
                                </div>
                                <div class="col-sm-3">
                                    <label>Imposta applicata</label>
                                    <select id="imposta" name="imposta" class="form-control">
                                        <option value="default">Usa impostazioni Predefinite</option>
                                        <option value="0">- Nessuna imposta -</option>
                                        <?php foreach((new \MSFramework\Fatturazione\imposte())->getImposte() as $impK => $impV) { ?>
                                            <option value="<?php echo $impK ?>" <?php if($r['imposta'] == $impK) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($impV[0], true) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <?php
                                $trial_enabled = "";
                                if((int)$r['trial_days'] > 0) {
                                    $trial_enabled = "checked";
                                }
                                ?>

                                <div class="col-sm-2">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_trial" <?php echo $trial_enabled ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita giorni di prova</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-2" id="trial_days_container" style="display: <?= (!empty($trial_enabled) ? 'block' : 'none'); ?>;">
                                    <label>Giorni di prova gratuiti</label>
                                    <input type="number" min="0" step="1" id="trial_days" name="trial_days" class="form-control required" value="<?php echo intval($r['trial_days']) ?>">
                                </div>

                            </div>
                            <?php /* if($MSFrameworkCMS->checkExtraFunctionsStatus('affiliations')) { ?>
                                <h2 class="title-divider">Affiliati</h2>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Provvigione per l'affiliato*</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-eur"></i>
                                            </span>
                                            <input id="provvigione_affiliato" name="provvigione_affiliato" type="number" min="0" class="form-control extra_fields required" value="<?php echo htmlentities($r['extra_fields']['provvigione_affiliato']) ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            <?php } */ ?>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione']) ?>"></i></span>
                                    <textarea id="descrizione" name="descrizione" class="form-control" rows="4"><?php echo $MSFrameworki18n->getFieldValue($r['descrizione']) ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed" style="margin-top: 15px;"></div>

                            <h2 class="title-divider">Immagini</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php (new \MSFramework\uploads('SUBSCRIPTION_PLANS'))->initUploaderHTML("images", $r['gallery']); ?>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
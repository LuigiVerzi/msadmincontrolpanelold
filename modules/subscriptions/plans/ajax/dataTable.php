<?php
/**
 * MSAdminControlPanel
 * Date: 01/02/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach((new \MSFramework\Subscriptions\plans())->getPlanDetails() as $r) {

    $data = array(
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['titolo'], true),
        number_format($r['prezzo_mensile'], 2, '.', ',') . ' ' . CURRENCY_SYMBOL,
        number_format($r['prezzo_annuale'], 2, '.', ',') . ' ' . CURRENCY_SYMBOL,
        "DT_RowId" => $r['id']
    );

    $array['data'][] = $data;

}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

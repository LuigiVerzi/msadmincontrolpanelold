<?php
/**
 * MSAdminControlPanel
 * Date: 14/12/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\services())->getCategoryDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['images'], true));
}

if($MSFrameworkDatabase->deleteRow("servizi_categorie", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('SERVICES_CATEGORY'))->unlink($images_to_delete);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


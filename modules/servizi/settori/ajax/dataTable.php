<?php
/**
 * MSAdminControlPanel
 * Date: 14/12/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM servizi_settori") as $r) {
    $array['data'][] = array(
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

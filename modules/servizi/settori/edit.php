<?php
/**
 * MSAdminControlPanel
 * Date: 14/12/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM servizi_settori WHERE id = :id", array($_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Preferenze settore</h2>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nome Settore*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">
                                </div>
                            </div>

                            <h2 class="title-divider">Contenuto</h2>

                            <div class="row">
                                <div class="col-sm-4" data-optional>
                                    <label>Descrizione breve</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descr']) ?>"></i></span>
                                    <textarea id="descr" name="descr" class="form-control msShort" rows="4"><?php echo $MSFrameworki18n->getFieldValue($r['descr']) ?></textarea>
                                </div>
                                <div class="col-sm-8" data-optional data-default-view="0">
                                    <label>Descrizione lunga</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['long_descr']) ?>"></i></span>
                                    <textarea id="long_descr" name="long_descr" class="form-control msShort" rows="4"><?php echo $MSFrameworki18n->getFieldValue($r['long_descr']) ?></textarea>
                                </div>
                            </div>

                            <h2 class="title-divider">Media</h2>

                            <div class="row">
                                <div class="col-sm-6" data-optional>
                                    <label>Immagine</label>
                                    <?php
                                    (new \MSFramework\uploads('SERVICES_SECTOR'))->initUploaderHTML("images", $r['images']);
                                    ?>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
<input name="image" type="file" id="upload" class="hidden" onchange="">

<script>
    
    globalInitForm();
</script>
</body>
</html>
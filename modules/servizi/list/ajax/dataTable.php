<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$beautycenter = $MSFrameworkCMS->checkExtraFunctionsStatus('beautycenter');

foreach((new \MSFramework\services())->getServiceDetails() as $r) {
   $data = array(
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        "DT_RowId" => $r['id']
    );

   if(!isset($module_config['module_fields_status']['category']) || $module_config['module_fields_status']['category'] == "1") {
       $data[] = (!empty($r['category']) ? $MSFrameworki18n->getFieldValue((new \MSFramework\services())->getCategoryDetails($r['category'], "nome")[$r['category']]['nome']) : 'N/A');
   }

    if($beautycenter) {
        $data[] = ($r['extra_fields']['reparto'] ? $MSFrameworki18n->getFieldValue((new \MSFramework\Attivita\reparti())->getRepartoDetails($r['extra_fields']['reparto'])[$r['extra_fields']['reparto']]['nome']) : 'N/A');
        $data[] = (int)$r['extra_fields']['durata'] . ' minuti';
    }

    $array['data'][] = $data;

}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_var_prezzi = array('pPrezzo');
foreach($array_var_prezzi as $curPostPrice) {

    if($_POST[$curPostPrice] == "") $_POST[$curPostPrice] = 0;

    $_POST[$curPostPrice] = str_replace(",", ".", $_POST[$curPostPrice]);
    if(!strstr($_POST[$curPostPrice], ".") && $_POST[$curPostPrice] != "") {
        $_POST[$curPostPrice] .= ".00";
    }

    if(!is_numeric($_POST[$curPostPrice]) && $_POST[$curPostPrice] != "") {
        echo json_encode(array("status" => "price_numeric"));
        die();
    }
}

$uploader = new \MSFramework\uploads('SERVICES');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$uploaderBanner = new \MSFramework\uploads('SERVICES');
$ary_filesBanner = $uploader->prepareForSave($_POST['pBannerAry']);
if($ary_filesBanner === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT nome, descr, long_descr, images, banner, descr_title FROM servizi WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pDescrizione'], 'oldValue' => $r_old_data['descr']),
        array('currentValue' => $_POST['pDescrizioneEstesa'], 'oldValue' => $r_old_data['long_descr']),
        array('currentValue' => $_POST['pTitoloDescrizione'], 'oldValue' => $r_old_data['descr_title'])
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "descr" => $MSFrameworki18n->setFieldValue($r_old_data['descr'], $_POST['pDescrizione']),
    "long_descr" => $MSFrameworki18n->setFieldValue($r_old_data['long_descr'], $_POST['pDescrizioneEstesa']),
    "descr_title" => $MSFrameworki18n->setFieldValue($r_old_data['descr_title'], $_POST['pTitoloDescrizione']),
    "prezzo" => $_POST['pPrezzo'],
    "category" => $_POST['pCategory'],
    "sector" => json_encode($_POST['pSettore']),
    "images" => json_encode($ary_files),
    "banner" => json_encode($ary_filesBanner),
    "extra_fields" => json_encode($_POST['pExtraFields'])
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO servizi ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE servizi SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
        $uploaderBanner->unlink($ary_filesBanner);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['images'], true));
    $uploaderBanner->unlink($ary_filesBanner, json_decode($r_old_data['banner'], true));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
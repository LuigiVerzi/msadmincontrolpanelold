<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\services())->getServiceDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['images'], true), json_decode($r['banner'], true));
}

if($MSFrameworkDatabase->deleteRow("servizi", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('SERVICES'))->unlink($images_to_delete);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


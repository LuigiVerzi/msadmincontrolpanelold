<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = (new \MSFramework\services())->getServiceDetails($_GET['id'])[$_GET['id']];
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Preferenze servizio</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">
                                </div>

                                <div class="col-sm-2" data-optional>
                                    <label>Prezzo <?= CURRENCY_SYMBOL; ?> (<?php echo ((new \MSFramework\Fatturazione\imposte())->getPriceType() == "0") ? "IVA Inclusa" : "Senza IVA" ?>)</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Impostare '0' per gratuito"></i></span>
                                    <input type="text" id="prezzo" name="prezzo" class="form-control" value="<?php echo htmlentities($r['prezzo']) ?>">
                                </div>

                                <div class="col-sm-2" data-optional>
                                    <label>Categoria</label>
                                    <select id="category" name="category" class="form-control">
                                        <option value="">Nessuna categoria</option>
                                        <?php foreach((new \MSFramework\services())->getCategoryDetails() as $category) { ?>
                                            <option value="<?= $category['id']; ?>" <?= ($r['category'] == $category['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($category['nome']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-2" data-optional data-default-view="0">
                                    <label>Settori</label>
                                    <select id="settore" name="settore" class="form-control" multiple>
                                        <?php foreach((new \MSFramework\services())->getSectorDetails() as $sector) { ?>
                                            <option value="<?= $sector['id']; ?>" <?= (in_array($sector['id'], json_decode($r['sector'], true)) ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($sector['nome']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <h2 class="title-divider">Contenuti</h2>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-12" data-optional>
                                            <label>Titolo descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descr_title']) ?>"></i></span>
                                            <input type="text" id="descr_title" name="descr_title" class="form-control msShort" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['descr_title'])); ?>">
                                        </div>

                                        <div class="col-sm-12" data-optional>
                                            <label>Descrizione breve</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descr']) ?>"></i></span>
                                            <textarea id="descr" name="descr" class="form-control msShort" rows="4"><?php echo $MSFrameworki18n->getFieldValue($r['descr']) ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-8" data-optional data-default-view="0">
                                    <label>Descrizione estesa</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['long_descr']) ?>"></i></span>
                                    <textarea id="long_descr" name="long_descr" class="form-control" rows="4"><?php echo $MSFrameworki18n->getFieldValue($r['long_descr']) ?></textarea>
                                </div>
                            </div>

                            <h2 class="title-divider">Fatturazione</h2>
                            <div class="row">

                                <div class="col-sm-3">
                                    <label>Imposta applicata</label>
                                    <select id="imposta" name="imposta" class="form-control extra_fields">
                                        <option value="default">Usa impostazioni Predefinite</option>
                                        <option value="">- Nessuna imposta -</option>
                                        <?php foreach((new \MSFramework\Fatturazione\imposte())->getImposte() as $impK => $impV) { ?>
                                            <option value="<?php echo $impK ?>" <?php if($r['extra_fields']['imposta'] == $impK) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($impV[0], true) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Assegna Crediti (<?= CURRENCY_SYMBOL; ?>)</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La quantità di crediti da accreditare al cliente all'acquisto"></i></span>
                                    <input type="number" id="crediti" name="crediti" class="form-control extra_fields" value="<?php echo htmlentities($r['extra_fields']['crediti']) ?>" min="0">
                                </div>

                            </div>

                            <?php if($MSFrameworkCMS->checkExtraFunctionsStatus('beautycenter')) { ?>
                                <h2 class="title-divider">Centro Estetico</h2>
                                <div class="row">

                                    <div class="col-sm-3">
                                        <label>Reparto</label>
                                        <select id="reparto" name="reparto" class="form-control extra_fields">
                                            <option></option>
                                            <?php foreach((new \MSFramework\Attivita\reparti())->getRepartoDetails() as $reparto) { ?>
                                                <option value="<?= $reparto['id']; ?>" <?= ($r['extra_fields']['reparto'] == $reparto['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($reparto['nome']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label>Sedute (Quantità)</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La quantità di sessioni incluse nel servizio"></i></span>
                                        <input type="number" id="sessioni" name="sessioni" class="form-control extra_fields" value="<?php echo htmlentities($r['extra_fields']['sessioni']) ?>" min="1">
                                    </div>

                                    <div class="col-sm-3">
                                        <label>Durata Trattamento (Minuti)</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La durata del trattamento"></i></span>
                                        <input type="number" id="durata" name="durata" class="form-control extra_fields" value="<?php echo htmlentities($r['extra_fields']['durata']) ?>">
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            <?php } ?>

                            <h2 class="title-divider">Media</h2>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Icona</label>
                                    <?php
                                    (new \MSFramework\uploads('SERVICES'))->initUploaderHTML("images", $r['images']);
                                    ?>
                                </div>

                                <div class="col-sm-4">
                                    <label>Banner</label>
                                    <?php
                                    (new \MSFramework\uploads('SERVICES'))->initUploaderHTML("banner", $r['banner']);
                                    ?>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
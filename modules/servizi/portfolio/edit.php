<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = (new \MSFramework\services())->getPortfolioDetails($_GET['id'])[$_GET['id']];
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni</h2>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Nome*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">
                                    <div class="row">
                                        <div class="col-sm-12" data-optional>
                                            <label>Slug*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                            <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?= ($r['is_active'] == "1" || $_GET['id'] == "" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_highlighted" <?= ($r['is_highlighted'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">In Evidenza</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3" data-optional>
                                    <label>Cliente</label>
                                    <select id="customer" name="customer" class="form-control">
                                        <option value=""></option>
                                        <?php foreach((new \MSFramework\customers())->getCustomerDetails() as $customer) { ?>
                                            <option value="<?= $customer['id']; ?>" <?= ($r['customer'] == $customer['id'] ? 'selected' : ''); ?>><?= $customer['nome'] . " " . $customer['cognome']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3" data-optional>
                                    <label>URL</label>
                                    <input id="url" name="url" type="text" class="form-control" value="<?php echo htmlentities($r['url']) ?>">
                                </div>

                                <div class="col-sm-2" data-optional>
                                    <label>Servizio</label>
                                    <select id="service" name="service" class="form-control">
                                        <option value="">Nessun servizio</option>
                                        <?php foreach((new \MSFramework\services())->getServiceDetails() as $service) { ?>
                                            <option value="<?= $service['id']; ?>" <?= ($r['service'] == $service['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($service['nome']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-2" data-optional>
                                    <label>Settore</label>
                                    <select id="sector" name="sector" class="form-control">
                                        <option value="">Nessun settore</option>
                                        <?php foreach((new \MSFramework\services())->getSectorDetails() as $sector) { ?>
                                            <option value="<?= $sector['id']; ?>" <?= ($r['sector'] == $sector['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($sector['nome']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>

                            <h2 class="title-divider">Contenuto</h2>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descr']) ?>"></i></span>
                                    <textarea id="descr" name="descr" class="form-control" rows="4"><?php echo $MSFrameworki18n->getFieldValue($r['descr']) ?></textarea>
                                </div>

                                <div class="col-sm-6" data-optional>
                                    <label>Descrizione estesa</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['long_descr']) ?>"></i></span>
                                    <textarea id="long_descr" name="long_descr" class="form-control" rows="4"><?php echo $MSFrameworki18n->getFieldValue($r['long_descr']) ?></textarea>
                                </div>
                            </div>

                            <h2 class="title-divider">Media</h2>

                            <div class="row">
                                <div class="col-sm-4">

                                    <div class="row">
                                        <div class="col-sm-12" data-optional data-default-view="0">
                                            <input type="hidden" id="optional_main_image_generator">
                                            <label style="display: none;">Generatore Immagine Principale</label>
                                            <a class="btn btn-app web_template_button generate_web_preview preview" data-viewport="1280x1280">
                                                <i class="fa fa-desktop"></i> Ottieni immagine Principale
                                            </a>
                                        </div>
                                    </div>

                                    <label>Immagini Principale</label>
                                    <?php (new \MSFramework\uploads('SERVICES_PORTFOLIO'))->initUploaderHTML("mainImages", $r['images']); ?>
                                </div>

                                <div class="col-sm-4" data-optional data-default-view="0">
                                    <a class="btn btn-app web_template_button generate_web_preview desktop" data-viewport="1280x1280">
                                        <i class="fa fa-desktop"></i> Ottieni immagine Desktop
                                    </a>
                                    <label>Immagini desktop</label>
                                    <?php (new \MSFramework\uploads('SERVICES_PORTFOLIO'))->initUploaderHTML("webImages", $r['web_images']); ?>
                                </div>

                                <div class="col-sm-4" data-optional data-default-view="0">
                                    <a class="btn btn-app web_template_button generate_web_preview mobile" data-viewport="360x360">
                                        <i class="fa fa-mobile-phone"></i> Ottieni immagine Mobile
                                    </a>
                                    <label>Immagini mobile</label>
                                    <?php (new \MSFramework\uploads('SERVICES_PORTFOLIO'))->initUploaderHTML("mobImages", $r['mob_images']); ?>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
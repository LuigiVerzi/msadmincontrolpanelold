<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');


foreach((new \MSFramework\services())->getPortfolioDetails() as $r) {
   $data = array(
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        $MSFrameworki18n->getFieldValue($r['descr'], true),
        $MSFrameworki18n->getFieldValue((new \MSFramework\services())->getServiceDetails($r['service'], "nome")[$r['service']]['nome'], true),
       ($r['is_active'] == "1" ? "Si" : "No"),
       ($r['is_highlighted'] == "1" ? "Si" : "No"),
        "DT_RowId" => $r['id']
    );

    $array['data'][] = $data;
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

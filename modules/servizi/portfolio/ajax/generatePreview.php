<?php
/**
 * Marketing Studio
 * Date: 26/01/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

mkdir(UPLOAD_TMP_FOR_DOMAIN, 0777, true);
mkdir(UPLOAD_TMP_FOR_DOMAIN . "tn/", 0777, true);

$file = $_POST['picture_url'];
$name = time(). '_' . 'webpreview.png';

$sizes = $_POST['sizes'];

$data = file_get_contents($file);

file_put_contents(UPLOAD_TMP_FOR_DOMAIN . $name, $data);
createThumbnail(UPLOAD_TMP_FOR_DOMAIN, $name, UPLOAD_TMP_FOR_DOMAIN . "tn/", ($sizes['thumb_cropWidth'] ? $sizes['thumb_cropWidth'] : 200), 100);

die(json_encode(array('name' => $name)));

function createThumbnail($imageDirectory, $imageName, $thumbDirectory, $thumbWidth, $quality = 100)
{
    $image_extension = @end(explode(".", $imageName));

    if($image_extension == "svg" || $image_extension == "ico") {
        copy("$imageDirectory/$imageName", "$thumbDirectory/$imageName");
    } else {
        switch($image_extension)
        {
            case "jpg":
                @$srcImg = imagecreatefromjpeg("$imageDirectory/$imageName");
                break;
            case "jpeg":
                @$srcImg = imagecreatefromjpeg("$imageDirectory/$imageName");
                break;
            case "png":
                $srcImg = imagecreatefrompng("$imageDirectory/$imageName");
                break;
        }

        if(!$srcImg) exit;
        $origWidth = imagesx($srcImg);
        $origHeight = imagesy($srcImg);
        $ratio = $origHeight/ $origWidth;
        $thumbHeight = $thumbWidth * $ratio;

        $thumbImg = imagecreatetruecolor($thumbWidth, $thumbHeight);

        if($image_extension == 'png')
        {
            $background = imagecolorallocate($thumbImg, 0, 0, 0);
            imagecolortransparent($thumbImg, $background);
            imagealphablending($thumbImg, false);
            imagesavealpha($thumbImg, true);
        }

        imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origWidth, $origHeight);

        switch($image_extension)
        {
            case "jpg":
                imagejpeg($thumbImg, "$thumbDirectory/$imageName", $quality);
                break;
            case "jpeg":
                imagejpeg($thumbImg, "$thumbDirectory/$imageName", $quality);
                break;
            case "png":
                imagepng($thumbImg, "$thumbDirectory/$imageName");
                break;
        }
    }
}

?>
<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\services())->getPortfolioDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['images'], true), json_decode($r['web_images'], true), json_decode($r['mob_images'], true));
}

if($MSFrameworkDatabase->deleteRow("servizi_portfolio", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('SERVICES_PORTFOLIO'))->unlink($images_to_delete);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


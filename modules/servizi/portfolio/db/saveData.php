<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pSlug'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "servizi_portfolio", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

$uploader_mainImages = new \MSFramework\uploads('SERVICES_PORTFOLIO');
$ary_mainImages = $uploader_mainImages->prepareForSave($_POST['pmainImagesAry']);
if($ary_mainImages === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$uploader_webImages = new \MSFramework\uploads('SERVICES_PORTFOLIO');
$ary_webImages = $uploader_webImages->prepareForSave($_POST['pwebImagesAry']);
if($ary_webImages === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}


$uploader_mobImages = new \MSFramework\uploads('SERVICES_PORTFOLIO');
$ary_mobImages = $uploader_mobImages->prepareForSave($_POST['pmobImagesAry']);
if($ary_mobImages === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT nome, descr, long_descr, images, web_images, mob_images, slug FROM servizi_portfolio WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pDescrizione'], 'oldValue' => $r_old_data['descr']),
        array('currentValue' => $_POST['pDescrizioneEstesa'], 'oldValue' => $r_old_data['long_descr']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "descr" => $MSFrameworki18n->setFieldValue($r_old_data['descr'], $_POST['pDescrizione']),
    "long_descr" => $MSFrameworki18n->setFieldValue($r_old_data['long_descr'], $_POST['pDescrizioneEstesa']),
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
    "url" => $_POST['pURL'],
    "service" => $_POST['pService'],
    "sector" => $_POST['pSector'],
    "customer" => $_POST['pCustomer'],
    "is_active" => $_POST['pIsActive'],
    "is_highlighted" => $_POST['pIsHighlighted'],
    "images" => json_encode($ary_mainImages),
    "web_images" => json_encode($ary_webImages),
    "mob_images" => json_encode($ary_mobImages),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO servizi_portfolio ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE servizi_portfolio SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader_mainImages->unlink($ary_mainImages);
        $uploader_webImages->unlink($ary_webImages);
        $uploader_mobImages->unlink($ary_mobImages);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader_mainImages->unlink($ary_mainImages, json_decode($r_old_data['images'], true));
    $uploader_webImages->unlink($ary_webImages, json_decode($r_old_data['web_images'], true));
    $uploader_mobImages->unlink($ary_mobImages, json_decode($r_old_data['mob_images'], true));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
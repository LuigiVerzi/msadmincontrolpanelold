$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('nome', 'slug', 'servizi_portfolio', $('#record_id'));

    initOrak('mainImages', 30, 515, 515, 153, getOrakImagesToPreattach('mainImages'), false, ['image/jpeg', 'image/png', 'image/svg+xml']);
    initOrak('webImages', 30, 515, 515, 153, getOrakImagesToPreattach('webImages'), false, ['image/jpeg', 'image/png', 'image/svg+xml']);
    initOrak('mobImages', 30, 200, 200, 153, getOrakImagesToPreattach('mobImages'), false, ['image/jpeg', 'image/png', 'image/svg+xml']);

    $('.generate_web_preview').on('click', function (e) {
        e.preventDefault();

        var $button = $(this);
        var original_html = $button.html();

        var viewport = $button.data('viewport');
        var url = $('#url').val();
        var width = 1080;

        var pic_type = ($(this).hasClass('mobile') ? 'mobile' : ($(this).hasClass('preview') ? 'main' : 'desktop'));

        var $uploadModal = $(this).closest('.col-sm-4');

        var cropWidth = $uploadModal.find('[id*="crop_width"]').val();
        var cropHeight = $uploadModal.find('[id*="crop_height"]').val();

        var thumb_cropWidth = $uploadModal.find('[id*="crop_width_thumb"]').val();
        var thumb_cropHeight = $uploadModal.find('[id*="crop_height_thumb"]').val();

        if(cropWidth !== "" && cropWidth > 100) {
            width = cropWidth;
        }

        if($button.hasClass('loading')) return false;

        $button.addClass('loading');
        $button.html('<i class="fa fa-spinner" aria-hidden="true"></i> Generazione...');

        if(/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(url)){

            var picture_url = 'http://api.screenshotlayer.com/api/capture?access_key=ea696e5ec2e453ef45fe1489c865e855&url=' + encodeURIComponent(url) + '&viewport=' + viewport + '&width=' + width + (pic_type != 'main' ? '&fullpage=1' : '');

            $.ajax({
                url: "ajax/generatePreview.php",
                type: "POST",
                data: {
                    "picture_url": picture_url,
                    "sizes": {
                        'cropWidth': cropWidth,
                        'cropHeight': cropHeight,
                        'thumb_cropWidth': thumb_cropWidth,
                        'thumb_cropHeight': thumb_cropHeight,
                    }
                },
                async: true,
                dataType: "json",
                success: function(data) {
                    if(pic_type == 'desktop') {
                        //modifico al volo il path per le anteprime utilizzato da orakuploader in quanto (solo in questo caso) le anteprime vanno caricate dalla TMP in fase di init
                        import_saved_realpath = $('#webImages_realPath').val();
                        $('#webImages_realPath').val($('#baseElementPathTmpFolder').val());
                        $('#webImages').html('');

                        initOrak('webImages', 10, 515, 515, 153, [data.name], false, ['image/jpeg', 'image/png', 'image/svg+xml']);
                        $('#webImages_realPath').val(import_saved_realpath);
                    } else if(pic_type == 'mobile'){
                        //modifico al volo il path per le anteprime utilizzato da orakuploader in quanto (solo in questo caso) le anteprime vanno caricate dalla TMP in fase di init
                        import_saved_realpath = $('#mobImages_realPath').val();
                        $('#mobImages_realPath').val($('#baseElementPathTmpFolder').val());
                        $('#mobImages').html('');

                        initOrak('mobImages', 10, 515, 515, 153, [data.name], false, ['image/jpeg', 'image/png', 'image/svg+xml']);
                        $('#mobImages_realPath').val(import_saved_realpath);
                    } else {
                        //modifico al volo il path per le anteprime utilizzato da orakuploader in quanto (solo in questo caso) le anteprime vanno caricate dalla TMP in fase di init
                        import_saved_realpath = $('#mainImages_realPath').val();
                        $('#mainImages_realPath').val($('#baseElementPathTmpFolder').val());
                        $('#mainImages').html('');

                        initOrak('mainImages', 10, 515, 515, 153, [data.name], false, ['image/jpeg', 'image/png', 'image/svg+xml']);
                        $('#mainImages_realPath').val(import_saved_realpath);
                    }

                    $button.html('<i class="fa fa-check" aria-hidden="true"></i> Generata');
                    $button.removeClass('loading');
                }
            });

        } else {
            toastr["error"]("L'url inserito non risulta valido, per favore compila correttamente il campo URL prima.", "URL mancante o non valido!");
            $button.removeClass('loading');
            $button.html(original_html);
        }

    })
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pSlug": $('#slug').val(),
            "pService": $('#service').val(),
            "pSector": $('#sector').val(),
            "pCustomer": $('#customer').val(),
            "pDescrizione": $('#descr').val(),
            "pDescrizioneEstesa": $('#long_descr').val(),
            "pURL": $('#url').val(),
            "pIsActive": $('#is_active:checked').length,
            "pIsHighlighted": $('#is_highlighted:checked').length,
            "pmainImagesAry": composeOrakImagesToSave('mainImages'),
            "pwebImagesAry": composeOrakImagesToSave('webImages'),
            "pmobImagesAry": composeOrakImagesToSave('mobImages'),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
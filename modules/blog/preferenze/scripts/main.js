function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('titolo', 'slug');
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pTitolo": $('#titolo').val(),
            "pSlug": $('#slug').val(),
            "pDescrizione": $('#descrizione').val(),
            "pSeoTitolo": $('#seo_titolo').val(),
            "pSeoDescrizione": $('#seo_descrizione').val(),
            "pBlogArticoliMostrati": $('#blog_articoli_mostrati').val(),
            "pCategoriaArticoliMostrati": $('#categoria_articoli_mostrati').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
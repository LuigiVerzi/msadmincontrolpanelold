<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('blog');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Titolo Blog</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['titolo']) ?>"></i></span>
                                    <input class="form-control" id="titolo" value="<?php echo $MSFrameworki18n->getFieldValue($r['titolo']) ?>" type="text">
                                </div>
                                <div class="col-sm-12">
                                    <label>Slug Blog</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                    <input class="form-control" id="slug" value="<?php echo $MSFrameworki18n->getFieldValue($r['slug']) ?>" type="text">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <label>Descrizione Blog</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione']) ?>"></i></span>
                                    <textarea class="form-control" id="descrizione" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r['descrizione']) ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Articoli da Mostrare (Pagina Blog)</label>
                                    <input type="number" id="blog_articoli_mostrati" name="blog_articoli_mostrati" class="form-control" value="<?= ($r['blog_articoli_mostrati'] == "" ? "1" : $r['blog_articoli_mostrati']) ?>"  min="1" />
                                </div>
                                <div class="col-sm-6">
                                    <label>Articoli da Mostrare (Pagina Categoria)</label>
                                    <input type="number" id="categoria_articoli_mostrati" name="categoria_articoli_mostrati" class="form-control" value="<?= ($r['categoria_articoli_mostrati'] == "" ? "1" : $r['categoria_articoli_mostrati']) ?>"  min="1" />
                                </div>
                            </div>
                        </fieldset>

                        <h1>SEO</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label>TAG Titolo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['seo_titolo']) ?>"></i></span>
                                    <input class="form-control " id="seo_titolo" value="<?php echo $MSFrameworki18n->getFieldValue($r['seo_titolo']) ?>" type="text" >
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <label>TAG Description</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['seo_descrizione']) ?>"></i></span>
                                    <textarea class="form-control " id="seo_descrizione" rows="5" ><?php echo $MSFrameworki18n->getFieldValue($r['seo_descrizione']) ?></textarea>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
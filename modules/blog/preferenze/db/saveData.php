<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'blog'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkCMS->getCMSData('blog');
}


if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "blog_categories", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pTitolo'], 'oldValue' => $r_old_data['titolo']),
        array('currentValue' => $_POST['pDescrizione'], 'oldValue' => $r_old_data['descrizione']),
        array('currentValue' => $_POST['pSeoTitolo'], 'oldValue' => $r_old_data['seo_titolo']),
        array('currentValue' => $_POST['pSeoDescrizione'], 'oldValue' => $r_old_data['seo_descrizione']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "value" => json_encode(array(
        "titolo" => $MSFrameworki18n->setFieldValue($r_old_data['titolo'], $_POST['pTitolo']),
        "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
        "descrizione" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione'], $_POST['pDescrizione']),
        "seo_titolo" => $MSFrameworki18n->setFieldValue($r_old_data['seo_titolo'], $_POST['pSeoTitolo']),
        "seo_descrizione" => $MSFrameworki18n->setFieldValue($r_old_data['seo_descrizione'], $_POST['pSeoDescrizione']),
        "blog_articoli_mostrati" => ($_POST['pBlogArticoliMostrati'] == "" || $_POST['pBlogArticoliMostrati'] == "0") ? "1" : $_POST['pBlogArticoliMostrati'],
        "categoria_articoli_mostrati" => ($_POST['pCategoriaArticoliMostrati'] == "" || $_POST['pCategoriaArticoliMostrati'] == "0") ? "1" : $_POST['pCategoriaArticoliMostrati'],
        "social" => $_POST['pSocial'],
    )),
    "type" => "blog"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'blog'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
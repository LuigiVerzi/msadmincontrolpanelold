$(document).ready(function() {
    loadStandardDataTable('main_table_list', true);
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('articolo_titolo', 'slug', 'blog_posts', $('#record_id'));
    initTinyMCE( '#articolo_content');

    $('#articolo_tag').tagsInput({
        'height':'70px',
        'width':'100%',
        'interactive':true,
        'defaultText':'Aggiungi',
        'removeWithBackspace' : true
    });

    initOrak('mainImages', 1, 600, 300, 100, getOrakImagesToPreattach('mainImages'), false, ['image/jpeg', 'image/png']);
    initOrak('bannerImages', 1, 1280, 300, 100, getOrakImagesToPreattach('bannerImages'), false, ['image/jpeg', 'image/png']);
    initOrak('iconImages', 1, 250, 250, 100, getOrakImagesToPreattach('iconImages'), false, ['image/jpeg', 'image/png']);

    $('#articolo_categoria').chosen();
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pTitolo": $('#articolo_titolo').val(),
            "pSlug": $('#slug').val(),
            "pDescrizioneBreve": $('#articolo_descr_breve').val(),
            "pContent": tinymce.get('articolo_content').getContent(),
            "pCategoria": $('#articolo_categoria').val(),
            "pTag": $('#articolo_tag').val(),
            "pSeoTitolo": $('#articolo_seo_titolo').val(),
            "pSeoDescrizione": $('#articolo_seo_descrizione').val(),
            "pMainImagesAry": composeOrakImagesToSave('mainImages'),
            "pBannerImagesAry": composeOrakImagesToSave('bannerImages'),
            "pIconImagesAry": composeOrakImagesToSave('iconImages'),
            "pActive": $('#is_active:checked').length
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mv_error") {
                bootbox.error("C'Ã¨ stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}
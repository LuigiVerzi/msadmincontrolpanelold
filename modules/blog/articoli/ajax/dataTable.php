<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$lingue_attive = $MSFrameworki18n->getActiveLanguagesDetails();

$MSFrameworkCustomers = new \MSFramework\customers();
$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array(
        'db' => 'images',
        'dt' => 0,
        'formatter' => function( $d, $row ) {
            return '<img src="' . UPLOAD_BLOG_POSTS_FOR_DOMAIN_HTML . "tn/" . json_decode($d, true)['main'][0] . '" width="80">';
        }
    ),
    array(
        'db' => 'titolo',
        'dt' => 1,
        'formatter' => function($d, $row) {
            Global $MSFrameworki18n;
            return $MSFrameworki18n->getFieldValue($d, true);
        }
    ),
    array(
        'db' => 'slug',
        'dt' => 2,
        'formatter' => function($d, $row) {
            Global $MSFrameworki18n;
            return $MSFrameworki18n->getFieldValue($d, true);
        }
    ),
    array(
        'db' => 'category',
        'dt' => 3,
        'formatter' => function($d, $row) {
            Global $MSFrameworki18n;

            $str_categoria = "";
            foreach((new \MSFramework\Blog\categories())->getCategoryDetails(explode(",", $d)) as $categoria) {
                $str_categoria .= $MSFrameworki18n->getFieldValue($categoria['nome'], true) . ", ";
            }

            return ($str_categoria != '' ? substr($str_categoria, 0, -2) : 'Nessuna Categoria');
        }
    ),
    array(
        'db' => 'author',
        'dt' => 4,
        'formatter' => function($d, $row) {
            Global $MSFrameworki18n, $MSFrameworkUsers;
            $author = $MSFrameworkUsers->getUserDataFromDB($d['author']);
            return $author['nome'] . ' ' . $author['cognome'];
        }
    ),
    array(
        'db' => 'slug',
        'dt' => 5,
        'formatter' => function($d, $row) {
            Global $MSFrameworki18n, $MSFrameworkUsers, $lingue_attive;
            $json_slug = json_decode($d, true);
            $lingue_field = '';
            foreach($lingue_attive as $lingua) {
                $lingue_field .= '<img style="opacity: ' . (isset($json_slug[$lingua['long_code']]) ? '1' : '0.3') . ';" src="' . FRAMEWORK_COMMON_CDN . 'img/languages/' . $lingua['flag_code'] . '.png"> ';
            }
            return $lingue_field;
        }
    ),
    array(
        'db' => 'visite',
        'dt' => 6
    ),
    array(
        'db' => 'last_update',
        'dt' => 7,
        'formatter' => function($d, $row) {
            return array("display" => date('d/m/Y H:i', strtotime($d)), 'sort' => strtotime($d));
        }
    ),
    array(
        'db' => 'active',
        'dt' => 8,
        'formatter' => function($d, $row) {
            return ($d ? '<span class="label label-primary">Attivo</span>' : '<span class="label label-danger">Non Attivo</span>');
        }
    )
);

echo json_encode(
    $datatableHelper::simple( $_GET, $MSFrameworkDatabase, 'blog_posts', 'id', $columns )
);
<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id']) && !empty($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM blog_posts WHERE id = :id", array(":id" => $_GET['id']), true);
    if(!$r) header('Location: index.php');
}

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>

                            <div class="row form-with-sidebar">

                                <div class="col-xs-12 col-sm-4 col-lg-3 col-lg-push-9 col-sm-push-8 right-options">
                                    <div class="row">
                                        <div class="col-xs-12" style="margin-bottom: 15px;">
                                            <label>Categoria</label>
                                            <select id="articolo_categoria" name="articolo_categoria" class="form-control chosen-select" data-placeholder="Nessuna categoria..." multiple>
                                                <?php
                                                echo (new \MSFramework\Blog\categories)->composeHTMLMenu('select', explode(',', $r['category']));
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label>Tag Articolo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['tag']) ?>"></i></span>
                                            <input class="form-control" id="articolo_tag" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['tag'])) ?>" type="text">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="is_active" <?php if($r['active'] == "1" || $_GET['id'] == "") { echo "checked"; } ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Mostra sul sito</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-8 col-lg-9 col-lg-pull-3 col-sm-pull-4">

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label>Titolo Articolo*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['titolo']) ?>"></i></span>
                                            <input class="form-control required" id="articolo_titolo" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['titolo'])) ?>" type="text" required>
                                        </div>
                                        <div class="col-xs-12">
                                            <label>Slug Articolo*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                            <input id="old_slug" value="<?php echo htmlentities($r['slug']); ?>" type="hidden">
                                            <input class="form-control required" id="slug" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>" type="text" required>
                                        </div>
                                    </div>

                                    <div class="col-attached-left">
                                        <h2 class="title-divider" style="margin-top: 0;">Contenuto</h2>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label>Descrizione Breve*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione_breve']) ?>"></i></span>
                                                <textarea class="form-control msShort required" id="articolo_descr_breve" rows="5" required><?php echo $MSFrameworki18n->getFieldValue($r['descrizione_breve']) ?></textarea>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label>Contenuto*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['content']) ?>"></i></span>
                                                <textarea class="form-control required" id="articolo_content" rows="5" required><?php echo $MSFrameworki18n->getFieldValue($r['content']) ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </fieldset>

                        <h1>Immagini</h1>
                        <fieldset>
                            <div class="row">
                                <?php
                                $images = json_decode($r['images'], true);
                                ?>

                                <div class="col-sm-4">
                                    <label>Anteprima (minimo 250x250 pixel)</label>
                                    <?php
                                    (new \MSFramework\uploads('BLOG_POSTS'))->initUploaderHTML("iconImages", json_encode($images['icon']));
                                    ?>
                                </div>

                                <div class="col-sm-4" style="margin-bottom: 40px;">
                                    <label>Principale (minimo 750x300 pixel)</label>
                                    <?php
                                    (new \MSFramework\uploads('BLOG_POSTS'))->initUploaderHTML("mainImages", json_encode($images['main']));
                                    ?>
                                </div>

                                <div class="col-sm-4" style="margin-bottom: 40px;">
                                    <label>Banner (minimo 1280x300 pixel)</label>
                                    <?php
                                    (new \MSFramework\uploads('BLOG_POSTS'))->initUploaderHTML("bannerImages", json_encode($images['banner']));
                                    ?>
                                </div>

                            </div>
                        </fieldset>

                        <h1>SEO</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label>TAG Titolo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['seo_titolo']) ?>"></i></span>
                                    <input class="form-control" id="articolo_seo_titolo" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['seo_titolo'])) ?>" type="text">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <label>TAG Description</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['seo_descrizione']) ?>"></i></span>
                                    <textarea class="form-control" id="articolo_seo_descrizione" rows="5"><?php echo htmlentities($MSFrameworki18n->getFieldValue($r['seo_descrizione'])) ?></textarea>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
        <input name="image" type="file" id="upload" class="hidden" onchange="">
        
        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
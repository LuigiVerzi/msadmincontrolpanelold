<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pTitolo'] == "" || $_POST['pSlug'] == "" || $_POST['pDescrizioneBreve'] == "" || $_POST['pContent'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$uploader = new \MSFramework\uploads('BLOG_POSTS');

$ary_files['main'] = $uploader->prepareForSave($_POST['pMainImagesAry']);
if($ary_files['main'] === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$ary_files['banner'] = $uploader->prepareForSave($_POST['pBannerImagesAry']);
if($ary_files['banner'] === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$ary_files['icon'] = $uploader->prepareForSave($_POST['pIconImagesAry']);
if($ary_files['icon'] === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$_POST['pContent'] = $uploader->saveFromBlob($_POST['pContent']);

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "blog_posts", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

if($db_action == "update") {
    $r_old_data = (new \MSFramework\Blog\posts())->getBlogArticleByID($_POST['pID']);
    $old_img = json_decode($r_old_data['images'], true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pTitolo'], 'oldValue' => $r_old_data['titolo']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
        array('currentValue' => $_POST['pDescrizioneBreve'], 'oldValue' => $r_old_data['descrizione_breve']),
        array('currentValue' => $_POST['pContent'], 'oldValue' => $r_old_data['content']),
        array('currentValue' => $_POST['pSeoTitolo'], 'oldValue' => $r_old_data['seo_titolo']),
        array('currentValue' => $_POST['pSeoDescrizione'], 'oldValue' => $r_old_data['seo_descrizione']),
        array('currentValue' => $_POST['pTag'], 'oldValue' => $r_old_data['tag']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "titolo" => $MSFrameworki18n->setFieldValue($r_old_data['titolo'], $_POST['pTitolo']),
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
    "descrizione_breve" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione_breve'], $_POST['pDescrizioneBreve']),
    "content" => $MSFrameworki18n->setFieldValue($r_old_data['content'], $_POST['pContent']),
    "tag" => $MSFrameworki18n->setFieldValue($r_old_data['tag'], $_POST['pTag']),
    "category" => implode(',', $_POST['pCategoria']),
    "seo_titolo" => $MSFrameworki18n->setFieldValue($r_old_data['seo_titolo'], $_POST['pSeoTitolo']),
    "seo_descrizione" => $MSFrameworki18n->setFieldValue($r_old_data['seo_descrizione'], $_POST['pSeoDescrizione']),
    "active" => $_POST['pActive'],
    "images" => json_encode($ary_files),
    "last_update" => date("Y-m-d H:i:s"),
);

if($db_action == 'insert') $array_to_save['author'] = $_SESSION['userData']['user_id'];

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO blog_posts ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $record_id = $MSFrameworkDatabase->lastInsertId();
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE blog_posts SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $record_id = $_POST['pID'];
}

if(!$result) {
    if($db_action == "insert") {
        if($db_action == "insert") {
            $uploader->unlink($ary_files['main']);
            $uploader->unlink($ary_files['banner']);
            $uploader->unlink($ary_files['icon']);

            $uploader->unlinkFromHTML($_POST['pContent']);
        }
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files['main'], $old_img['main']);
    $uploader->unlink($ary_files['banner'], $old_img['banner']);
    $uploader->unlink($ary_files['icon'], $old_img['icon']);

    $uploader->unlinkFromHTML($_POST['pContent'], $r_old_data['content']);
}

$MSFrameworkUrl->makeRedirectIfNeeded('blog-post-' . $record_id, (new \MSFramework\Blog\posts())->getURL($record_id), ($db_action === 'update' ?  (new \MSFramework\Blog\posts())->getURL($r_old_data) : ''));

echo json_encode(array("status" => "ok", "id" => $record_id));
die();
?>
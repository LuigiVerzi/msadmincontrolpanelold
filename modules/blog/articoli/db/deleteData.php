<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
$contents_html = array();
foreach((new \MSFramework\Blog\posts())->getPostDetails($_POST['pID'], "images, content") as $post) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($post['images'], true));
    $contents_html = $post['content'];
}

if($MSFrameworkDatabase->deleteRow("blog_posts", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('BLOG_POSTS'))->unlink($images_to_delete['main']);
    (new \MSFramework\uploads('BLOG_POSTS'))->unlink($images_to_delete['banner']);
    (new \MSFramework\uploads('BLOG_POSTS'))->unlink($images_to_delete['icon']);
    foreach($contents_html as $html) {
        (new \MSFramework\uploads('BLOG_POSTS'))->unlinkFromHTML($html);
    }

    $MSFrameworkUrl->deleteRedirectsByReferences('blog-post', $_POST['pID']);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
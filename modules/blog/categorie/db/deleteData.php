<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$r = $MSFrameworkDatabase->getAssoc("SELECT images, banner FROM blog_categories WHERE id = :id", array(":id" => $_POST['pID']), true);

if($MSFrameworkDatabase->deleteRow("blog_categories", "id", $_POST['pID'])) {
    foreach($MSFrameworkDatabase->getAssoc("SELECT id, category FROM blog_posts WHERE find_in_set(" . $_POST['pID'] . ", category)") as $p) {
        $cat_array = array_diff(explode(',', $p['category']), array($_POST['pID']));
        $MSFrameworkDatabase->pushToDB("UPDATE blog_posts SET category = :new_cats WHERE id = :id", array(":new_cats" => implode(',', $cat_array), ':id' => $p['id']));
    }

    (new \MSFramework\uploads('BLOG_CATEGORIES'))->unlink(json_decode($r['images'], true));
    (new \MSFramework\uploads('BLOG_CATEGORIES'))->unlink(json_decode($r['banner'], true));

    $MSFrameworkUrl->deleteRedirectsByReferences('blog-category', $_POST['pID']);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
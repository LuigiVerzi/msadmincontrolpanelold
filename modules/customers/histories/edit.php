<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$customers_class = new \MSFramework\customers();

$customer = array();

if(isset($_GET['id'])) {
    $customer = $customers_class->getCustomerDataFromDB($_GET['id']);
}

if($customer) {
    $info_principali = array();
    if(!empty($customer['email'])) {
        $info_principali[] = $customer['email'];
    }
    if(!empty($customer['telefono_casa'])) {
        $info_principali[] = $customer['telefono_casa'];
    }
    if(!empty($customer['telefono_cellulare'])) {
        $info_principali[] = $customer['telefono_cellulare'];
    }

    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
    if(json_decode($customer['avatar'])) {
        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($customer['avatar'], true)[0];
    }

    $customerTrackingReferences = array();
    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM tracking__references WHERE user_ref = :user_id", array(':user_id' => $customer['id'])) as $trackingProjectReference) {
        $customerTrackingReferences[$trackingProjectReference['project_id']] = (json_decode($trackingProjectReference['track_references']) ? json_decode($trackingProjectReference['track_references']) : array());
    }

    $trackingReferenceNames = array(
        'ga_clientId' => array(
            'name' => 'Analytics ClientID',
            'icon' => 'fa fa-google',
            'color' => '#ED750A'
        ),
        'gclid' => array(
            'name' => 'Google Ads gclid',
            'icon' => 'fa fa-google',
            'color' => '#13a365'
        ),
        'fbclid' => array(
            'name' => 'Facebook fbclid',
            'icon' => 'fa fa-facebook',
            'color' => '#3C5A99'
        )
    );

} else {
    header('location: index.php');
    die();
}

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-3 col-md-4">

                    <div class="contact-box">
                        <div class="profile-image">
                            <img src="<?= $avatar_url; ?>" class="rounded-circle circle-border m-b-md" alt="profile">
                        </div>
                        <div class="profile-info">
                            <div class="">
                                <div>
                                    <h2 class="no-margins">
                                        <?= $customer['nome']; ?> <?= $customer['cognome']; ?>
                                    </h2>
                                    <h4>Registrato dal <?= date('d/m/Y H:i', strtotime($customer['registration_date'])); ?></h4>
                                    <address class="m-t-sm">
                                        <?= implode('<br>', $info_principali); ?>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php if($customerTrackingReferences) { ?>
                        <table class="table table-striped table-bordered">
                            <?php foreach($customerTrackingReferences as $projectID => $trackingReferences) { ?>
                                <tr>
                                    <th colspan="2">Progetto <?= (new \MSFramework\tracking())->getTrackingProjectDetails($projectID)['name']; ?></th>
                                </tr>
                                <?php foreach($trackingReferences as $referenceName => $trackingReference) { ?>
                                    <tr>
                                        <td><span class="label" style="color: white; background: <?= $trackingReferenceNames[$referenceName]['color']; ?>;"><i class="<?= $trackingReferenceNames[$referenceName]['icon']; ?>"></i> <?= $trackingReferenceNames[$referenceName]['name']; ?></span></td>
                                        <td><?= $trackingReference; ?></td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        </table>
                    <?php } ?>

                    <div class="alert alert-info">
                        <p>
                            <b>Tracking Personalizzato:</b><br>
                            Tutti gli obiettivi tracciati tramite il <a target="_blank" href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>tracking">modulo Tracking</a> verranno visualizzati anche in questa sezione.
                        </p>
                    </div>

                    <?php /*
                    <div class="contact-box">
                        <form id="addCustomerHistory">
                            <h2 class="m-t-none m-b-md">Aggiungi Azione</h2>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Data Evento</label>
                                    <div class="input-group date scadenza">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" id="data_evento">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label>Titolo Evento</label>
                                    <input type="text" class="form-control" placeholder="Il titolo dell'azione">
                                </div>
                                <div class="col-sm-12">
                                    <label>Testo Evento</label>
                                    <textarea class="form-control" placeholder="Il testo dell'azione"></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#" class="btn btn-primary pull-right">Aggiungi Evento</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    */ ?>
                </div>
                <div class="col-lg-9 col-md-8">

                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1 class="tab-right history"><i class="fa fa-history" aria-hidden="true"></i> Cronologia Eventi</h1>
                        <fieldset>

                            <div class="row" style="margin-bottom: 15px;">
                                <div class="col-xs-8 ">
                                    <h3 class="font-bold no-margins">
                                        Cronologia Eventi
                                    </h3>
                                    <small id="last_actions_time">Stai visualizzando <b>Tutto</b>.</small>
                                </div>
                                <div class="col-xs-4">
                                    <div class="text-right">
                                        <input type="text" name="daterange" id="last_actions_daterange" value="Tutto" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation" data-page="1">

                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<textarea id="no_results_template" style="display: none;">
    <div class="alert alert-danger" style="margin: 0; position: relative;"><b>Nessun risultato trovato</b><br>Non è stato trovato nessun risultato nel range di date inserite, modifica le date per visualizzare dei risultati.</div>
</textarea>

<textarea id="end_results_template" style="display: none;">
    <div class="alert alert-info" style="margin: 0; position: relative;"><b>Nessun altro risultato</b><br>Non ci sono altri risultati da mostrare.</div>
</textarea>

<script>
    globalInitForm();
</script>
</body>
</html>
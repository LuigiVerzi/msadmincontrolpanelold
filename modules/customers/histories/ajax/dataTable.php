<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array(
        'db' => 'id',
        'dt' => 'DT_buttonsLimit',
        'formatter' => function( $d, $row ) {
            return 'read';
        }
    ),
    array(
        'db' => 'avatar',
        'dt' => 'DT_Avatar'
    ),
    array(
        'db' => 'CONCAT(nome,cognome,email)',
        'dt' => 0,
        'formatter' => function ($d, $row) {
            Global $MSFrameworkCustomers;

            $r = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

            $info = array();
            if (!empty($r['email'])) {
                $info[] = $r['email'];
            }
            if (!empty($r['telefono_casa'])) {
                $info[] = $r['telefono_casa'];
            }
            if (!empty($r['telefono_cellulare'])) {
                $info[] = $r['telefono_cellulare'];
            }

            $return_html = '';

            $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
            if(json_decode($r['avatar'])) {
                $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
            }

            $return_html .= '<div style="position: relative; padding-left: 65px;">';
            $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
            $return_html .= '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
            $return_html .= '</div>';

            return $return_html;
        }
    ),
    array(
        'db' => 'id',
        'dt' => 1,
        'customOrder' => '(' . str_replace(array('*'), array('COUNT(*)'), $MSFrameworkCustomers->getCustomerHistorySQL(array('id' => 'customers.id', 'email' => 'customers.email'))) . ')',
        'formatter' => function( $d, $row ) {
            Global $MSFrameworkCustomers;
            $azioni_effettuate = $MSFrameworkCustomers->countCustomerHistories($d);
            return '<b>' . $azioni_effettuate . '</b> Attività';
        }
    ),
    array(
        'db' => 'id',
        'dt' => 2,
        'customOrder' => '(' . str_replace('*', 'MAX(date)', $MSFrameworkCustomers->getCustomerHistorySQL(array('id' => 'customers.id', 'email' => 'customers.email'))) . ')',
        'formatter' => function( $d, $row ) {
            Global $MSFrameworkCustomers;
            $last_action = array_values($MSFrameworkCustomers->getCustomerHistories($d, 0,1))[0];
            return array("display" => date("d/m/Y H:i", $last_action['date']), "sort" => $last_action['date']);
        }
    ),
    array(
        'db' => 'registration_date',
        'dt' => 3,
        'formatter' => function( $d, $row ) {
            return array("display" => date("d/m/Y H:i", strtotime($d)), "sort" => strtotime($d));
        }
    )
);

echo json_encode(
    $datatableHelper::simple( $_GET, $MSFrameworkDatabase, 'customers', 'id', $columns )
);
?>

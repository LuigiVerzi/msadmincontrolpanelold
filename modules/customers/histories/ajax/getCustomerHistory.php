<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$customers_class = new \MSFramework\customers();
$utils_class = new \MSFramework\utils();

$id = $_POST['id'];
$page = $_POST['page'];
$start = $_POST['start'];
$end = $_POST['end'];

$sql = $customers_class->getCustomerHistorySQL($customers_class->getCustomerDataFromDB($id));

if($start != $end) {
    $sql .= "AND date >= " . strtotime($_POST['start']) . " AND date <= " . strtotime($_POST['end'] . " 23:59") . "";
}
$sql .= ' ORDER by date DESC';

$start = 0;
if($page > 0) {
    $start = ($page*10)-10;
}
$sql .= ' LIMIT ' . $start . ', ' . 10;

$azioni = $MSFrameworkDatabase->getAssoc($sql);

if(!$azioni) {
    die('end');
}

?>
<?php foreach($azioni as $azione) { ?>

    <?php
    $history = $customers_class->formatCustomerHistories($azione);
    ?>

    <div class="vertical-timeline-block">
        <div class="vertical-timeline-icon <?= $history['timeline']['bg']; ?>-bg">
            <i class="fa fa-<?= $history['timeline']['icon']; ?>"></i>
        </div>

        <div class="vertical-timeline-content">
            <h2 style="margin: 0;"><?= $history['name']; ?></h2>
            <?= (isset($history['description']) && !empty($history['description']) ? '<p>' . (new \MSFramework\customers())->parseBalanceNotes($history['description']) . '</p>' : ''); ?>
            <?php if (isset($history['button']) && $history['button']) { ?>
                <a href="<?= $history['button']['href']; ?>" class="btn btn-sm btn-<?= (isset($history['button']['class']) ? $history['button']['class'] : 'primary'); ?>"><?= $history['button']['text']; ?></a>
            <?php } ?>
            <span class="vertical-date">
                <?= $utils_class->smartdate($history['date']); ?>
                <br/>
                <small><?= date('d/m/Y H:i', $history['date']); ?></small>
            </span>
        </div>
    </div>
<?php } ?>

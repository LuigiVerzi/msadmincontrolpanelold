$(document).ready(function() {
    loadStandardDataTable('main_table_list', true);
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('#data_evento').datetimepicker({
        keyboardNavigation: false,
        autoclose: true,
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('dp.change', function(e) {});

    /* INIZIALIZZAZIONE GRAFICO ORDINI */
    $('#last_actions_daterange').daterangepicker({
        startDate: moment(),
        locale: {
            format: 'DD/MM/YYYY',
            applyLabel: "Cerca",
            cancelLabel: "Annulla",
            fromLabel: "Da",
            toLabel: "A",
            customRangeLabel: "Personalizzato",
        },
        opens: 'left',
        ranges: {
            'Tutto': [moment(), moment()],
        },
        alwaysShowCalendars: true,
        showCustomRangeLabel: true,
        autoUpdateInput: false
    }, function(start, end, label) {
        label = (label != 'Personalizzato' ? label : 'Dal ' + start.format('DD/MM/YY') + ' a ' + end.format('DD/MM/YY'));
        $('#last_actions_time b').html(label);
        $('#last_actions_daterange').val(label);
        $('#vertical-timeline').data('page', 0).html('');
        loadHistory(1, start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
    });

    loadHistory(1, 0, 0);

    window.loading_history = 0;
    $(window).on('scroll', function () {
        if(window.loading_history == 0) {
            if (($('html').scrollTop() + $(window).height()) > ($('#vertical-timeline').offset().top + $('#vertical-timeline').height() - 100)) {
                window.loading_history = 1;
                loadHistory($('#vertical-timeline').data('page'), $('#vertical-timeline').data('start'), $('#vertical-timeline').data('end'));
            }
        }
    });
}

function loadHistory(page, start, end) {

    $('#vertical-timeline').data('start', start).data('end', end);

    $.ajax({
        url: "ajax/getCustomerHistory.php",
        type: "POST",
        data: {
            "id": $('#record_id').val(),
            "page": page,
            "start": start,
            "end": end
        },
        async: true,
        success: function(data) {
            if(data != 'end') {
                window.loading_history = 0;
                $('#vertical-timeline').data('page', page+1).append(data);
            } else if(!$('#vertical-timeline').find('.vertical-timeline-block').length) {
                $('#vertical-timeline').html($('#no_results_template').val());
            } else {
                $('#vertical-timeline').append($('#end_results_template').val());
            }
        }
    });
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pCognome": $('#cognome').val(),
            "pDataNascita": $('#data_nascita').val(),
            "pEmail": $('#email').val(),
            "pTelefonoCasa": $('#telefono_casa').val(),
            "pTelefonoCellulare": $('#telefono_cellulare').val(),
            "pIndirizzo": $('#indirizzo').val(),
            "pCitta": $('#citta').val(),
            "pProvincia": $('#provincia').val(),
            "pCAP": $('#cap').val(),
            "pProvenienza": $('#provenienza').val(),
            "pSesso": $('#sesso').val(),
            "pFatturazione" : {
                "ragione_sociale" : $('#ragione_sociale').val(),
                "piva" : $('#piva').val(),
                "cf" : $('#cf').val(),
                "indirizzo" : $('#indirizzo_fatturazione').val(),
                "citta" : $('#citta_fatturazione').val(),
                "provincia" : $('#provincia_fatturazione').val(),
                "cap" : $('#cap_fatturazione').val(),
            },
            "pPass": $('#password').val(),
            "pPassConfirm": $('#password_confirm').val(),
            "pIsActive": $('#is_active:checked').length,
            "pMailConfermata": $('#mail_active:checked').length,
            "pImagesAry": composeOrakImagesToSave('images'),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido o è già stata utilizzata da un altro utente!");
            } else if(data.status == "email_missing") {
                bootbox.error("Se abiliti il login utente l'indirizzo email è obbligatorio!");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data di scadenza non è valido. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "password_strength") {
                bootbox.error("La password deve essere lunga almeno 8 caratteri e deve contenere almeno una lettera ed un numero!");
            } else if(data.status == "password_does_not_match") {
                bootbox.error("Le password inserite devono corrispondere!");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');

require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$tags_newsletter = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__tag");

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <div id="form" class="form-horizontal wizard-big">

                        <h1>Importazione</h1>
                        <fieldset>
                            <div id="import_csv_form">

                                <div class="sk-spinner sk-spinner-double-bounce">
                                    <div class="sk-double-bounce1"></div>
                                    <div class="sk-double-bounce2"></div>
                                </div>

                                <div class="csv_import_step show" id="csv-1">
                                    <form action="#" class="dropzone" id="dropzoneForm">
                                        <div class="fallback">
                                            <input name="file" type="file"/>
                                        </div>
                                    </form>
                                    <br>
                                </div>

                                <div class="csv_import_step" id="csv-2" style="display: none;">

                                    <h2 class="title-divider" style="margin-top: 0;">Collega i campi con i valori corretti</h2>
                                    <div class="row">
                                        <div id="assegnazione_pannelli">

                                        </div>
                                    </div>

                                    <h2 class="title-divider">Iscrizione Newsletter</h2>
                                    <div class="alert alert-info">Selezionando una o più liste i clienti verranno automaticamente iscritti alla newsletter.</div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <select class="form-control m-b custom_list"  style="min-height: 300px;" multiple>
                                                <?php foreach($tags_newsletter as $tag) { ?>
                                                    <option value="<?= $tag['id']; ?>"><?= $tag['nome']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </fieldset>

                        <h1>Esportazione</h1>
                        <fieldset>
                            <form method="POST" action="ajax/exportCSV.php" target="_blank" id="export_csv_form" name="export_csv_form">

                                <p>Selezionare i campi da esportare:</p>

                                <p><input type="checkbox" checked="checked" class="flat" disabled> Nome</p>
                                <p><input type="checkbox" checked="checked" class="flat" disabled> Cognome</p>
                                <p><input type="checkbox" checked="checked" name="data_nascita" class="flat"> Data di Nascita</p>
                                <p><input type="checkbox" checked="checked" name="sesso" class="flat"> Sesso</p>
                                <p><input type="checkbox" checked="checked" name="email" class="flat"> Indirizzo e-mail</p>
                                <p><input type="checkbox" checked="checked" name="telefono_casa" class="flat"> Telefono Casa</p>
                                <p><input type="checkbox" checked="checked" name="telefono_cellulare" class="flat"> Telefono Cellulare</p>
                                <p><input type="checkbox" checked="checked" name="indirizzo" class="flat"> Indirizzo</p>
                                <p><input type="checkbox" checked="checked" name="regione" class="flat"> Regione</p>
                                <p><input type="checkbox" checked="checked" name="citta" class="flat"> Città</p>
                                <p><input type="checkbox" checked="checked" name="cap" class="flat"> CAP</p>
                                <p><input type="checkbox" checked="checked" name="ragione_sociale" class="flat"> Ragione Sociale</p>
                                <p><input type="checkbox" checked="checked" name="piva" class="flat"> Partita IVA</p>
                                <p><input type="checkbox" checked="checked" name="cf" class="flat"> Codice Fiscale</p>
                                <p><input type="checkbox" checked="checked" name="fatt_address" class="flat"> Indirizzo Fatturazione</p>
                                <p><input type="checkbox" checked="checked" name="fatt_reg" class="flat"> Regione Fatturazione</p>
                                <p><input type="checkbox" checked="checked" name="fatt_prov" class="flat"> Provincia Fatturazione</p>
                                <p><input type="checkbox" checked="checked" name="fatt_city" class="flat"> Città Fatturazione</p>
                                <p><input type="checkbox" checked="checked" name="fatt_zip" class="flat"> CAP Fatturazione</p>

                                <hr class="hr-line-dashed">
                            </form>
                        </fieldset>

                    </div>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>

    globalInitForm();
</script>

<textarea id="template_assegnazione" style="display: none;">
    <div class="col-lg-12">
        <div class="">
        <p class="col-form-label">{valori}</p>
            <select class="form-control m-b valori_colonna" name="valori_colonna" data-id="{id}">
                <option value=""></option>
                <optgroup label="Dati principali">
                    <option value="nome">Nome</option>
                    <option value="cognome">Cognome</option>
                    <option value="email" {is_email}>Email</option>
                </optgroup>
                <optgroup label="Dati secondari">
                    <option value="data_nascita">Data di Nascita</option>
                    <option value="sesso">Sesso</option>
                    <option value="telefono_casa">Telefono Casa</option>
                    <option value="telefono_cellulare">Telefono Cellulare</option>
                </optgroup>
                <optgroup label="Indirizzo">
                    <option value="indirizzo">Indirizzo</option>
                    <option value="stato">Stato</option>
                    <option value="regione">Regione</option>
                    <option value="provincia">Provincia</option>
                    <option value="citta">Città</option>
                    <option value="cap">CAP</option>
                </optgroup>
                <optgroup label="Fatturazione">
                    <option value="ragione_sociale">Ragione Sociale</option>
                    <option value="piva">Partita IVA</option>
                    <option value="cf">Codice Fiscale</option>
                    <option value="fatt_indirizzo">Indirizzo</option>
                    <option value="fatt_stato">Stato</option>
                    <option value="fatt_regione">Regione</option>
                    <option value="fatt_provincia">Provincia</option>
                    <option value="fatt_citta">Città</option>
                    <option value="fatt_cap">CAP</option>
                </optgroup>
                <optgroup label="Newsletter">
                    <option value="lista">Lista</option>
                </optgroup>
            </select>
        </div>
        {divider}
    </div>
</textarea>

</body>
</html>


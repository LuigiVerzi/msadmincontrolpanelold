<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */
require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$filename = date("Y-m-d_His").".csv";

$eol = "\r\n";
$recipients = '';

$result = $MSFrameworkDatabase->getAssoc("SELECT * FROM customers");

foreach ($result as $row) {
    $fatturazione = json_decode($row['dati_fatturazione'], true);

    $recipients .= $row['nome'] . ";";
    $recipients .= $row['cognome'] . ";";
    $recipients .= isset($_POST['data_nascita']) ? ';'.date("d/m/Y", $row['data_nascita']) : '';
    $recipients .= isset($_POST['sesso']) ? ';'.$row['sesso'] : '';
    $recipients .= isset($_POST['email']) ? ';'.$row['email'] : '';
    $recipients .= isset($_POST['telefono_cellulare']) ? ';'.$row['telefono_cellulare'] : '';
    $recipients .= isset($_POST['telefono_casa']) ? ';'.$row['telefono_casa'] : '';
    $recipients .= isset($_POST['indirizzo']) ? ';'.$row['indirizzo'] : '';
    $recipients .= isset($_POST['citta']) ? ';'.$row['citta'] : '';
    $recipients .= isset($_POST['provincia']) ? ';'.$row['provincia'] : '';
    $recipients .= isset($_POST['cap']) ? ';'.$row['cap'] : '';
    $recipients .= isset($_POST['ragione_sociale']) ? ';'.$fatturazione['ragione_sociale'] : '';
    $recipients .= isset($_POST['piva']) ? ';'.$fatturazione['piva'] : '';
    $recipients .= isset($_POST['cf']) ? ';'.$fatturazione['cf'] : '';
    $recipients .= isset($_POST['fatt_address']) ? ';'.$fatturazione['indirizzo'] : '';
    $recipients .= isset($_POST['fatt_reg']) ? ';'.$fatturazione['regione'] : '';
    $recipients .= isset($_POST['fatt_city']) ? ';'.$fatturazione['citta'] : '';
    $recipients .= isset($_POST['fatt_prov']) ? ';'.$fatturazione['provincia'] : '';
    $recipients .= isset($_POST['fatt_zip']) ? ';'.$fatturazione['cap'] : '';
    $recipients .= $eol;
}

header('Content-Description: File Transfer'.$eol);
header('Content-Type: application/octet-stream'.$eol);
header('Content-Disposition: attachment; filename='.$filename.$eol);
header('Expires: 0'.$eol);
header('Cache-Control: must-revalidate'.$eol);
header('Pragma: public'.$eol);
header('Content-Length: '.strlen($recipients).$eol);
echo $recipients;
<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */
require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');


$data = json_decode(file_get_contents("php://input"), true);

if(!is_array($data[0])) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$status = importaClienti($data[0]);
unset($data[0]); // Libero memoria

$list_status = array();
if($data[1]) {
    $list_status = importaLista($data[1]);
}
unset($data); // Libero memoria

$success = false;
foreach(array_merge(array($status[0], $status[1]), $list_status) as $s) {
    if((int)$s > 0) {
        $success = true;
    }
}

echo json_encode(
    array(
        "status" => ($success ? 'ok' : 'err'),
        "totalCustomers" => $status[2],
        "newCustomers" => $status[0],
        "updatedCustomers" => $status[1],
        "newLists" => $list_status[1],
        "newRecipients" =>  $list_status[2],
        "newTagAssociation" =>  $list_status[3],
    )
);
die();


/* INIZIO FUNZIONI */
function importaClienti($csv_array) {
    global $MSFrameworkDatabase, $firephp;

    $total_rows = count($csv_array);
    $initial_rows = $MSFrameworkDatabase->getCount('SELECT id FROM customers');

    $csv_array = array_chunk($csv_array, 1000);
    $correct_col = count($csv_array[0][0]);

    $customerColumns = array();
    foreach($MSFrameworkDatabase->getAssoc("SHOW COLUMNS FROM customers") as $col) {
        $customerColumns[] = $col['Field'];
    }

    $sql_keys = '';
    $row_values = array();


    // Se vengono passate delle date cerco di capire quale sia il formato utilizzato
    $current_date_format = "d/m/Y";
    if(array_key_exists('data_nascita', $csv_array[0][0])) {
        $date_array = array();
        foreach($csv_array[0] as $k => $csv_row) {
            if(!empty($csv_row['data_nascita'])) {
                $date_array[] = $csv_row['data_nascita'];
            }
        }
        $current_date_format = detectCorrectDateFormat($date_array);
    }

    foreach($csv_array as $csv_chuck) {
        foreach($csv_chuck as $k=>$csv_row) {
            if(count($csv_row) != $correct_col) {
                // Salta se le colonne non corrispondono (CSV formattato male)
                continue;
            }

            // Salta se il campo email non è una mail valida
            if($csv_row['email'] != "" && !filter_var($csv_row['email'], FILTER_VALIDATE_EMAIL)) {
                if($k == 0) $total_rows--; // Nel caso in cui si tratta della riga d'intestazione allora non viene segnato come errore
                continue;
            }

            if(array_key_exists('sesso', $csv_row)) {
                if($csv_row['sesso'][0] == "m" || $csv_row['sesso'][0] == "u") {
                    $csv_row['sesso'] = "m";
                } else if($csv_row['sesso'][0] == "f" || $csv_row['sesso'][0] == "d" || $csv_row['sesso'][0] == "g") {
                    $csv_row['sesso'] = "f";
                } else {
                    $csv_row['sesso'] = "";
                }
            }

            if(array_key_exists('data_nascita', $csv_row)) {
                if($csv_row['data_nascita'] !== "") {
                    $dateString = DateTime::createFromFormat($current_date_format, $csv_row['data_nascita']);
                    if($dateString) {
                        $csv_row['data_nascita'] = $dateString->getTimestamp();
                    } else {
                        $csv_row['data_nascita'] = '';
                    }
                }
            }

            if($csv_row['email'] == "") {
                continue;
            }

            $ary_fatturazione = array(
                "ragione_sociale" => $csv_row['ragione_sociale'],
                "piva" => $csv_row['piva'],
                "cf" => $csv_row['cf'],
                "indirizzo" => $csv_row['fatt_indirizzo'],
                "citta" => $csv_row['fatt_citta'],
                "provincia" => $csv_row['fatt_provincia'],
                "regione" => $csv_row['fatt_regione'],
                "stato" => $csv_row['fatt_stato'],
                "cap" => $csv_row['fatt_cap'],
            );

            foreach($ary_fatturazione as $k => $v) {
                if(is_null($v)) unset($ary_fatturazione[$k]);
            }

            $csv_row['dati_fatturazione'] = json_encode($ary_fatturazione);

            // Rimuove le chiavi non presenti nella tabella customers
            foreach($csv_row as $col_k => $col_v) {
                if(!in_array($col_k, $customerColumns)) {
                    unset($csv_row[$col_k]);
                }
            }

            $sql_keys = implode(', ', array_keys($csv_row));

            // Crea la stringa di inserimento dei valori
            $row_values[] = implode(', ', array_values(
                array_map(function($v){
                    global $MSFrameworkDatabase;

                    return $MSFrameworkDatabase->quote($v);
                }, $csv_row)
            ));

        }

    }

    $sql_values = implode('), (', $row_values);

    // Crea la stringa di aggiornamento dei valori
    $update_string = implode(', ', array_values(
        array_map(function($v){
            return $v . "=VALUES(" . $v . ")";
        }, explode(', ', $sql_keys))
    ));

    $MSFrameworkDatabase->pushToDB("INSERT IGNORE INTO customers ($sql_keys) VALUES ($sql_values) ON DUPLICATE KEY UPDATE $update_string;");
    $countUpdatedCustomers = $MSFrameworkDatabase->lastAffectedRows;

    $inserted_row = ($MSFrameworkDatabase->getCount('SELECT id FROM customers') - $initial_rows);

    $countUpdatedCustomers -= $inserted_row;

    return array($inserted_row, $countUpdatedCustomers, $total_rows);
}

function importaLista($list_data)
{
    global $MSFrameworkDatabase;

    $tag_to_create = array();
    $destinatari_sql = array();
    $tag_sqls = array();

    foreach ($list_data as $keys => $list) {

        foreach(explode(',', $keys) as $k) {

            if ($k == 'undefined' || empty($k)) continue;

            if (!is_numeric($k)) {
                $tag_to_create[] = $MSFrameworkDatabase->quote($k);
            }

            $destinatari = array_values(
                array_map(function ($v) {
                    Global $MSFrameworkDatabase;
                    return "(SELECT id FROM customers WHERE email LIKE " . $MSFrameworkDatabase->quote($v) . ")";
                }, $list)
            );

            $valori = array_values(
                array_map(function ($v) use ($k) {
                    Global $MSFrameworkDatabase;
                    return (is_numeric($k) ? $k : "(SELECT id FROM newsletter__tag WHERE nome LIKE " . $MSFrameworkDatabase->quote($k) . ")") . ", (SELECT id FROM customers WHERE email LIKE " . $MSFrameworkDatabase->quote($v) . ")";
                }, $list)
            );

            $destinatari_sql[] = implode('), (', $destinatari);
            $tag_sqls[] = implode('), (', $valori);
        }
    }

    // Creo i tag SQL
    if($tag_to_create) {
        $create_tag_sql = "INSERT IGNORE INTO newsletter__tag (nome) VALUES (" . implode("), (", $tag_to_create) . ")";
        $MSFrameworkDatabase->pushToDB($create_tag_sql);
        $countNewTags = $MSFrameworkDatabase->lastAffectedRows;
    } else {
        $countNewTags = 0;
    }

    // Aggiungo i destinatari alla newsletter
    if($destinatari_sql) {
        $add_recipients_sql = "INSERT IGNORE INTO newsletter__destinatari (id) VALUES (" . implode("), (", $destinatari_sql) . ");";
        $MSFrameworkDatabase->pushToDB($add_recipients_sql);
        $countNewRecipients = $MSFrameworkDatabase->lastAffectedRows;
    } else {
        $countNewRecipients = 0;
    }

    // Applico i tag ai destinatari
    if($tag_sqls) {
        $apply_tag_sql = "INSERT IGNORE INTO newsletter__tag_destinatari (tag, destinatario) VALUES (" . implode("), (", $tag_sqls) . ");";
        $MSFrameworkDatabase->pushToDB($apply_tag_sql);
        $countNewTagsAssociation = $MSFrameworkDatabase->lastAffectedRows;
    } else {
        $countNewTagsAssociation = 0;
    }

    return array($countNewTags, $countNewRecipients, $countNewTagsAssociation);
}


/**
 * Cerca di capire in che formato è stata scritta la data.
 *
 * @param array $date_array Un array con le date (un numero maggiore di date garantisce una probabilità più alta di successo)
 *
 * @return string Il formato corretto della data
 */
function detectCorrectDateFormat($date_array) {

    $date_formats_count = array(
        'd/m/Y' => 0,
        'm/d/Y' => 0,
        'Y-m-d' => 0,
        'd-m-Y' => 0,
        'm-Y-d' => 0,
        'd/m/y' => 0,
        'm/d/y' => 0,
        'y-m-d' => 0,
        'd-m-y' => 0,
        'm-y-d' => 0
    );

    $count_valid = 0;
    foreach ($date_formats_count as $date_format => $success_count) {

        if($count_valid > 15) continue;

        foreach($date_array as $single_date) {
            $validation = DateTime::createFromFormat($date_format, $single_date);

            if($validation) {
                if($validation->format($date_format) == $single_date || $validation->getTimestamp() > -2000000000) {
                    $date_formats_count[$date_format]++;
                    $count_valid++;
                }
            }
        }
    }

    arsort($date_formats_count);

    return array_keys($date_formats_count)[0];
}
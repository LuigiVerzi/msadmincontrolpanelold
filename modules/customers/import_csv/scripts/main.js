function initForm() {
    initClassicTabsEditForm();

    $('#cancel_import_csv').on('click', function (e) {
        e.preventDefault();

        window.csv_file = false;
        Dropzone.forElement("#dropzoneForm").removeAllFiles(true);
        $('#assegnazione_pannelli').html('');
        validateImportStep(0);
    });

    $('#start_import_csv').on('click', function (e) {
        e.preventDefault();

        window.dialog = bootbox.dialog({
            message: '<p class="text-center">Importazione del file CSV in corso. Attendere...</p>',
            closeButton: false
        });

        if($(this).hasClass('disabled')) return false;
        if(validateImportStep(1)) {
            $('#import_csv_form').addClass('sk-loading');

            window.csv_total = 0;
            window.csv_updated = 0;
            window.csv_inserted = 0;

            window.lists_inserted = 0;
            window.lists_recipients = 0;
            window.tags_updated = 0;

            sendCSVData(chunk(window.csv_file.data, 5000), 0);
        } else {
            window.dialog.modal('hide');
            bootbox.error("Per poter compiere questa operazione, devi prima collegare tutti i campi obbligatori (*)!");
        }
    });

    $('#start_export_csv').on('click', function() {
        $('#export_csv_form').submit();
    })

    window.csv_file = false;

    Dropzone.options.dropzoneForm = {
        paramName: "file",
        maxFilesize: 100,
        multiple: false,
        dictDefaultMessage: "<strong>Seleziona un file .CSV da caricare</strong></br> (Oppure trascinalo qui sopra)",
        acceptedFiles: '.csv',
        maxFiles:1,
        autoProcessQueue:false,
        autoQueue:false,
        init: function() {
            this.on('addedfile', function(file) {
                $('.dz-progress').hide();
                window.csv_file = false;

                Papa.parse(file, {
                    skipEmptyLines: true,
                    complete: function(results, file_data) {

                        if(!results.data.length) {
                            window.csv_file = false;
                            Dropzone.forElement("#dropzoneForm").removeAllFiles(true);
                            validateImportStep(0);
                        }
                        else {
                            window.csv_file = results;
                            validateImportStep(0);
                        }
                    }
                });

                if (this.files.length > 1) {
                    this.removeFile(this.files[0]);
                }

                return false;
            });

            $('.sk-spinner').hide();
        }
    };

}

function sendCSVData(chunk_data, step) {
    var assegnazione_colonne = {};
    $('.valori_colonna').each(function () {
        var col = $(this).data('id');
        var type = $(this).val();
        if(type.length > 0) {
            assegnazione_colonne[type] = col;
        }
    });

    var liste_statiche_newsletter = [];
    if($('.custom_list').val() && $('.custom_list').val().length) {
        $('.custom_list').val().forEach(function(l_id) {
            liste_statiche_newsletter.push(l_id);
        });
    }

    var lista_data = {};
    var db_data = [];
    chunk_data[step].forEach(function(riga) {

        /* AGGIUNGO L'EMAIL ALLA LISTA STATICA SE PRESENTE */
        if(liste_statiche_newsletter.length) {
            liste_statiche_newsletter.forEach(function(l_id) {
                if(typeof(lista_data[l_id]) === "undefined") {
                    lista_data[l_id] = [];
                }
                lista_data[l_id].push(riga[assegnazione_colonne['email']]);
            });
        }

        var valori_assegnazione = {};
        Object.keys(assegnazione_colonne).forEach(function(key) {

            if(key === "lista") {
                if(riga[assegnazione_colonne[key]].length) {
                    if (typeof (lista_data[riga[assegnazione_colonne[key]]]) === "undefined") {
                        lista_data[riga[assegnazione_colonne[key]]] = [];
                    }
                    lista_data[riga[assegnazione_colonne[key]]].push(riga[assegnazione_colonne["email"]]);
                }
            }  else {
                valori_assegnazione[key] = riga[assegnazione_colonne[key]];
            }

        });
        db_data.push(valori_assegnazione);
    });

    $.ajax({
        url: "db/saveData.php",
        type: "PUT",
        data: JSON.stringify([db_data, lista_data]),
        contentType: "application/json",
        dataType: "json",
        success: function(data) {

            /* Aggiorno i contatori relativi ai clienti */
            if(typeof data.newCustomers !== 'undefined') window.csv_inserted += data.newCustomers;
            if(typeof data.updatedCustomers !== 'undefined') window.csv_updated += data.updatedCustomers;
            if(typeof data.totalCustomers !== 'undefined') window.csv_total += data.totalCustomers;

            /* Aggiorno i contatori relativi  alla newsletter */
            if(typeof data.newLists !== 'undefined') window.lists_inserted += data.newLists;
            if(typeof data.newRecipients !== 'undefined') window.lists_recipients += data.newRecipients;
            if(typeof data.tagsUpdated !== 'undefined') window.tags_updated += data.tagsUpdated;

            if(step >= chunk_data.length - 1 ) {
                window.dialog.modal('hide');

                $('#import_csv_form').removeClass('sk-loading');

                if (data.status == "mandatory_data_missing") {
                    bootbox.error("Per poter compiere questa operazione, devi prima collegare tutti i campi obbligatori (*)!");
                } else if(data.status == "err") {
                    bootbox.error("L'importazione non ha apportato nessun cambiamento. Probabilmente gli indirizzi importati sono già presenti sul sito.");
                } else {
                    bootbox.alert("<h2 style='margin: 0;'>Importazione effettuata con successo!</h2><br><div style='text-align: left;'>" + [
                        "<b>Clienti inseriti </b>: " + window.csv_inserted,
                        "<b>Clienti totali</b>: " + window.csv_total,
                        "<br><h3>Newsletter</h3><b>Liste inserite</b>: " + window.lists_inserted,
                        "<b>Clienti iscritti</b>: " + window.lists_recipients,
                        "<b>Tag aggiornati</b>: " + window.tags_updated
                    ].join("<br>") + '</div>', function () {
                       // window.location.reload();
                    });
                }
            }
            else
            {
                sendCSVData(chunk_data, (step+1) );
            }

        }
    });
}

function validateImportStep(index) {
    if(index == 0) {
        if (window.csv_file) {
            createTableFromCSV(window.csv_file.data);

            $('#import_csv_form .csv_import_step').hide();
            $('#import_csv_form #csv-2').show();
        }
        else {
            $('#import_csv_form .csv_import_step').hide();
        }
    }
    if(index == 1) {

        var assegnazione = {nome: null, email: null};

        if($('#assegnazione_pannelli .valori_colonna').length) {
            $('#assegnazione_pannelli .valori_colonna').each(function () {
                if($(this).val() != '') assegnazione[$(this).val()] = $(this).data('id');
            });
        }

        if(assegnazione['email'] !== null) {
            return true;
        }
        else return false;

    }

    return true;

}

function createTableFromCSV(data) {
    var html = '';
    var template = $('#template_assegnazione').val();

    data[0].forEach(function(riga, colonna) {

        var valori = [];

        for (var i = 0; i < data.length; i++) {
            if(typeof(data[i][colonna]) !== 'undefined' && data[i][colonna] !== '') valori.push(data[i][colonna]);
            if(valori.length >= 5) break;
        }

        if(valori.length > 0) {
            html += template
                .replace('{valori}', '<span class="label label-inverse">' + valori.join('</span> <span class="label label-inverse">') + '</span>')
                .replace(/{id}/g, colonna)
                .replace(/{is_email}/g, (validateEmail(valori[1]) ? 'selected' : ''))
                .replace(/{divider}/g, (colonna < (data[0].length-1) ? '<hr class="hr-line-dashed">' : ''));
        }
    });

    $('#assegnazione_pannelli').html(html);

    $('.valori_colonna').on('change', function () {
        var type = $(this).val();

        if(type.length) {
            $('.valori_colonna').not($(this)).each(function () {
                if ($(this).val() === type) {
                    $(this).val('').change();
                    return true;
                }
            });
        }

    });

}

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function chunk (arr, len) {
    var chunks = [],
        i = 0,
        n = arr.length;

    while (i < n) {
        chunks.push(arr.slice(i, i += len));
    }

    return chunks;
}

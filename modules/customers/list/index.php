<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$dividi_clienti = ($MSFrameworkDatabase->getCount("SELECT id FROM customers WHERE active = 1") && $MSFrameworkDatabase->getCount("SELECT id FROM customers WHERE active = 0"));

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php
                $customToolbarButton = '<a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . (new \MSSoftware\modules())->getModuleDetailsFromID("importazione_csv_clienti")['path'] . '" class="btn btn-default"><i class="fa fa-download"></i> CSV</a>';
                require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardGrid.php");
                ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <?php if($dividi_clienti) { ?>
                <form method="get" id="form" action="#" class="form-horizontal wizard-big ">

                    <h1>Clienti Attivi</h1>
                    <fieldset>
                        <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th style="width: 50px;">Avatar</th>
                                <th>Nome</th>
                                <th>Cognome</th>
                                <th>Email</th>
                                <th>Telefono</th>
                                <th>Cellulare</th>
                                <th class="is_data default-sort" data-sort="desc">Data Registrazione</th>
                                <?php if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) { ?>
                                    <th>Carrello</th>
                                <?php } ?>
                            </tr>
                            </thead>
                        </table>
                    </fieldset>

                    <h1>Clienti Inattivi</h1>
                    <fieldset>
                        <table id="secondary_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th style="width: 50px;">Avatar</th>
                                <th>Nome</th>
                                <th>Cognome</th>
                                <th>Email</th>
                                <th>Telefono</th>
                                <th>Cellulare</th>
                                <th class="is_data default-sort" data-sort="desc">Data Registrazione</th>
                                <?php if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) { ?>
                                    <th>Carrello</th>
                                <?php } ?>
                            </tr>
                            </thead>
                        </table>
                    </fieldset>
                </form>
                <?php } else { ?>
                <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th style="width: 50px;">Avatar</th>
                        <th>Nome</th>
                        <th>Cognome</th>
                        <th>Email</th>
                        <th>Telefono</th>
                        <th>Cellulare</th>
                        <th class="is_data default-sort" data-sort="desc">Data Registrazione</th>
                        <?php if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) { ?>
                            <th>Carrello</th>
                        <?php } ?>
                    </tr>
                    </thead>
                </table>
            <?php } ?>

            </div>
        </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
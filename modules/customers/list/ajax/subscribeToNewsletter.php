<?php
/**
 * MSAdminControlPanel
 * Date: 06/12/2018
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if(trim($_GET['mail']) == "" || trim($_GET['nome']) == "") {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$customer_data = (new \MSFramework\customers())->getCustomerDataByEmail($_GET['mail']);

if($customer_data && (int)$_GET['id'] !== (int)$customer_data['id']) {
    echo json_encode(array("status" => "customer_already_exists"));
    die();
}

if(!filter_var(trim($_GET['mail']), FILTER_VALIDATE_EMAIL)) {
    echo json_encode(array("status" => "mail_validation"));
    die();
}


(new \MSFramework\Newsletter\subscribers())->subscribeNewEmail(trim($_GET['nome']) . " " . trim($_GET['cognome']), trim($_GET['mail']), 'Iscrizione Manuale da Operatore');

$user_id = (new \MSFramework\customers())->getCustomerDataByEmail($_GET['mail'])['id'];

if($_GET['lists']) {
    $customer_data['id'] = (new \MSFramework\Newsletter\subscribers())->subscribeToTag($_GET['lists'], $user_id);
}

$subscribed_tags = array();

if($db_action != "insert") {

    foreach($MSFrameworkDatabase->getAssoc("SELECT tag FROM newsletter__tag_destinatari WHERE destinatario = :dest", array(":dest" => $user_id)) as $dest_lists) {
        $subscribed_tags[] = $dest_lists['tag'];
    }

    $delete_from_tags = array_diff($subscribed_tags, (is_array($_GET['lists']) ? $_GET['lists'] : array()));
    if(is_array($delete_from_tags) && count($delete_from_tags) > 0) {
        $same_db_string_ary = $MSFrameworkDatabase->composeSameDBFieldString($delete_from_tags, "OR", "tag");
        $MSFrameworkDatabase->query("DELETE FROM newsletter__tag_destinatari WHERE destinatario = :dest_id AND (" . $same_db_string_ary[0] . ")", array_merge(array(":dest_id" => $user_id), $same_db_string_ary[1]));
    }
}

echo json_encode(array("status" => "ok", "id" => $user_id));

die();
?>

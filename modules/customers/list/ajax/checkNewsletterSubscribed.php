<?php
/**
 * MSAdminControlPanel
 * Date: 06/12/2018
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$mail = $_GET['mail'];
if(!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
    echo "Indirizzo email non valido per l'iscrizione.";
    die();
}

$details = (new \MSFramework\Newsletter\subscribers())->getSubscriberByMail($mail, "id, data_inserimento");

$origin_tags = array();


$avaialable_lists = (new \MSFramework\Newsletter\lists())->getTagDetails('', 'id, nome');
foreach ($avaialable_lists as $list_k => $list_v) {
    $avaialable_lists[$list_k]['count'] = (new \MSFramework\Newsletter\lists())->getSubscribersCountForTag($list_v['id']);

    if ($details['id']) {
        $avaialable_lists[$list_k]['is_sub'] = (new \MSFramework\Newsletter\subscribers())->checkIfAlreadySubscribedToTag($list_v['id'], $details['id']);
    } else {
        $avaialable_lists[$list_k]['is_sub'] = false;
    }
}

if($avaialable_lists) {
    ?>
    <div style="margin-bottom: 10px;">
        <select id="newsletter_list" class="form-control" style="margin-bottom: 10px;" multiple>
            <?php foreach ($avaialable_lists as $tag) { ?>
                <option
                    value="<?= $tag['id'] ?>" <?php echo ($tag['is_sub'] ? "selected" : "") ?>><?= $tag['nome'] ?>
                    (<?= $tag['count'] ?>)
                </option>
            <?php } ?>
        </select>
    </div>
    <?php
}

if($details['id'] != "") {
    echo "Iscritto. Data iscrizione: " . date("d/m/Y H:i", strtotime($details['data_inserimento']));
} else {
    echo "Utente non iscritto alla newsletter";
}

if($details['id'] == "" || $avaialable_lists) {
    echo "<a href='javascript:void(0)' class='btn btn-success pull-right' id='subscribe_to_newsletter'>" . ($details['id'] != "" ? 'Aggiorna' : 'Iscrivi') . "</a>";
}
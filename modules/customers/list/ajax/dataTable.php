<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array( 'db' => 'avatar',  'dt' => 0,
        'formatter' => function( $d, $row ) {
            $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
            if(json_decode($d)) {
                $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($d, true)[0];
            }

            return '<img src="' . $avatar_url . '" width="50">';
        }),
    array( 'db' => 'nome',  'dt' => 1 ),
    array( 'db' => 'cognome',  'dt' => 2 ),
    array( 'db' => 'email',   'dt' => 3 ),
    array( 'db' => 'telefono_casa', 'dt' => 4 ),
    array( 'db' => 'telefono_cellulare',     'dt' => 5 ),
    array( 'db' => 'registration_date', 'dt' => 6,
        'formatter' => function( $d, $row ) {
            return array("display" => date("d/m/Y H:i", strtotime($d)), "sort" => strtotime($d));
        }
    )
);

if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) {
    $columns[] = array(
        'db' => '(SELECT carrello FROM ecommerce_users_extra WHERE ecommerce_users_extra.id = customers.id)',
        'customOrder' => '(SELECT LENGTH(carrello) FROM ecommerce_users_extra WHERE ecommerce_users_extra.id = customers.id)',
        'formatter' => function($d, $row) {
            return (json_decode($d) ? '<span class="label label-warning">' . count(json_decode($d)) . ' Prodotti</span>': 'Nessuno');
        },
        'dt' => count($columns)-1
    );
}


$columns[] = array(
    'db' => 'CONCAT(nome,cognome,email)',
    'dt' => 'DT_FormattedData',
    'formatter' => function ($d, $row) {
        Global $MSFrameworkCustomers;
        return $MSFrameworkCustomers->getCustomerPreviewHTML($row[0]);
    }
);

$where_all = ' id != "" ';
if(isset($_GET['type'])) {
    if($_GET['type'] == 'active') {
        $where_all .= ' AND active = 1 ';
    } else {
        $where_all .= ' AND active = 0';
    }
}

if(!$_SESSION['userData']['is_owner']) {
    $where_all .= ' AND (owner_id = "' . (new \MSFramework\users())->getUserDataFromSession('user_id') . '" OR owner_id = "")';
}

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'customers', 'id', $columns, '', $where_all)
);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r_producer_config = $MSFrameworkCMS->getCMSData('producer_config');
$customers_class = new \MSFramework\customers();

$r = array();
$fatturazione = array();

$realEstateEnabled = $MSFrameworkCMS->checkExtraFunctionsStatus('realestate');
$MSFrameworkAgents = new \MSFramework\RealEstate\agents();

if(isset($_GET['id'])) {
    $r = $customers_class->getCustomerDataFromDB($_GET['id'], '*', true);
}

if($r) {
    $fatturazione = json_decode($r['dati_fatturazione'], true);

    if(in_array('ecommerce', json_decode($r_producer_config['extra_functions'], true))) {

        $imposte = new \MSFramework\Ecommerce\imposte();
        $tax_settings = $imposte->getSettings();
        $info_imposte = $imposte->getImposte();

        $carrello_cliente = array();
        $customer_info = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($r['id']);

        if($customer_info['extra']['carrello'] && json_decode($customer_info['extra']['carrello'])) {
            $carrello_cliente = json_decode($customer_info['extra']['carrello'], true);
        }

    }

}

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Dati del Cliente</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nome*</label>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($r['nome']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Cognome</label>
                                    <input id="cognome" name="cognome" type="text" class="form-control" value="<?php echo htmlentities($r['cognome']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Data di Nascita</label>
                                    <div class="input-group date scadenza">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="data_nascita" value="<?php echo ($r['data_nascita'] != '') ? date("d/m/Y", $r['data_nascita']) : '' ?>">
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Sesso</label>
                                    <select id="sesso" name="sesso" class="form-control">
                                        <option value="">Non specificato</option>
                                        <option value="m" <?php if($r['sesso'] == "m") { echo "selected"; } ?>>Maschio</option>
                                        <option value="f" <?php if($r['sesso'] == "f") { echo "selected"; } ?>>Femmina</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">

                                    <?php
                                    if($_GET['id'] != "" && $r['active'] == 1) {
                                        $email_asterisk = "*";
                                        $email_required = "required";
                                    } else {
                                        $email_asterisk = "";
                                        $email_required = "";
                                    }
                                    ?>

                                    <label>Email<?= $email_asterisk; ?></label>
                                    <input id="email" name="email" type="text" class="form-control email <?= $email_required; ?>" value="<?php echo $r['email'] ?>">
                                </div>
                                <div class="col-sm-3">
                                    <label>Telefono Casa</label>
                                    <input id="telefono_casa" name="telefono_casa" type="text" class="form-control" value="<?php echo htmlentities($r['telefono_casa']); ?>">
                                </div>
                                <div class="col-sm-3">
                                    <label>Telefono Cellulare</label>
                                    <input id="telefono_cellulare" name="telefono_cellulare" type="text" class="form-control" value="<?php echo htmlentities($r['telefono_cellulare']); ?>">
                                </div>
                                <div class="col-sm-3">
                                    <label>Sito web</label>
                                    <input id="sito_web" name="sito_web" type="text" class="form-control" value="<?php echo htmlentities($r['sito_web']); ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Indirizzo</label>
                                    <input id="indirizzo" name="indirizzo" type="text" class="form-control" value="<?php echo htmlentities($r['indirizzo']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Stato</label>
                                    <input id="stato" name="stato" type="text" class="form-control" value="<?php echo htmlentities($r['stato']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Regione</label>
                                    <input id="regione" name="regione" type="text" class="form-control" value="<?php echo htmlentities($r['regione']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Provincia</label>
                                    <input id="provincia" name="provincia" type="text" class="form-control" value="<?php echo htmlentities($r['provincia']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Città</label>
                                    <input id="citta" name="citta" type="text" class="form-control" value="<?php echo htmlentities($r['citta']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>CAP</label>
                                    <input id="cap" name="cap" type="text" class="form-control" value="<?php echo htmlentities($r['cap']); ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Provenienza</label>
                                    <select id="provenienza" name="provenienza" class="form-control">
                                        <option value=""></option>
                                        <?php foreach($customers_class->getCustomerSource() as $k => $v) { ?>
                                            <option value="<?php echo $k ?>" <?php if($r['provenienza'] == $k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-6">
                                    <label>Iscritto alla newsletter</label>
                                    <div id="newsletter__status_container">
                                        Verifico...
                                    </div>
                                </div>

                                <div class="col-sm-3" style="display: <?= ($realEstateEnabled ? 'block' : 'none'); ?>;">
                                    <label>Agente di riferimento</label>
                                    <select id="agente_riferimento" name="agente_riferimento" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        if($r['owner_id'] == "") {
                                            $r['owner_id'] = $MSFrameworkAgents->getUserDataFromSession('user_id');
                                        }

                                        foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, ruolo FROM users ORDER BY cognome, nome") as $user) {
                                            ?>
                                            <option value="<?php echo $user['id'] ?>" <?php if($user['id'] == $r['owner_id']) { echo "selected"; } ?>><?php echo $user['cognome'] ?> <?php echo $user['nome'] ?> (<?php echo $MSFrameworkAgents->getUserLevels($user['ruolo']) ?>)</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <?php if($r['id'] != "") { ?>
                                <div class="col-sm-3">
                                    <label>Data accettazione Privacy</label>
                                    <div>
                                        <?= date('d/m/Y H:i:s', strtotime($r['registration_date'])); ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>


                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12" style="margin-bottom: 40px;">
                                    <label>Foto utente</label>
                                    <?php
                                    (new \MSFramework\uploads('CUSTOMER_AVATAR'))->initUploaderHTML("images", $r['avatar']);
                                    ?>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Dati Fatturazione</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Ragione Sociale</label>
                                    <input id="ragione_sociale" name="ragione_sociale" type="text" class="form-control" value="<?php echo $fatturazione['ragione_sociale'] ?>">
                                </div>

                                <div class="col-sm-4">
                                    <label>Partita IVA</label>
                                    <input id="piva" name="piva" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['piva']); ?>">
                                </div>

                                <div class="col-sm-4">
                                    <label>Codice Fiscale</label>
                                    <input id="cf" name="cf" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['cf']); ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Indirizzo</label>
                                    <input id="indirizzo_fatturazione" name="indirizzo_fatturazione" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['indirizzo']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Stato</label>
                                    <input id="stato_fatturazione" name="stato_fatturazione" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['stato']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Regione</label>
                                    <input id="regione_fatturazione" name="regione_fatturazione" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['regione']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Provincia</label>
                                    <input id="provincia_fatturazione" name="provincia_fatturazione" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['provincia']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Città</label>
                                    <input id="citta_fatturazione" name="citta_fatturazione" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['citta']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>CAP</label>
                                    <input id="cap_fatturazione" name="cap_fatturazione" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['cap']); ?>">
                                </div>
                            </div>

                        </fieldset>

                        <h1>Informazioni accesso</h1>
                        <fieldset>
                            <div class="row">
                                <?php
                                $is_active_check = "checked";
                                if($r['active'] == 0 || !$r) {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Login</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $is_mail_check = "";
                                if($r['mail_auth'] == "" && $_GET['id'] != "") {
                                    $is_mail_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3" id="mail_confirmed_container" style="display: <?= ($is_active_check == "checked" ? 'block' : 'none') ?>">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="mail_active" <?php echo $is_mail_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Mail Confermata</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <?php
                            if($_GET['id'] != "" && !empty($r['password'])) {
                                $pass_asterisk = "";
                                $pass_helper = "Lasciare vuoto per non modificare";
                                $pass_required = "";
                            } else {
                                $pass_asterisk = "*";
                                $pass_helper = "";
                                $pass_required = "empty_pass required";
                            }
                            ?>

                            <div class="row" id="passwords_box" style="display: <?= ($is_active_check ? 'block' : 'none'); ?>;">
                                <div class="col-sm-6">
                                    <label>Password<?php echo $pass_asterisk ?></label>
                                    <input id="password" name="password" type="password" class="form-control <?php echo $pass_required ?>">
                                    <span class="help-block m-b-none"><?php echo $pass_helper ?></span>
                                </div>

                                <div class="col-sm-6">
                                    <label>Conferma Password<?php echo $pass_asterisk ?></label>
                                    <input id="password_confirm" name="password_confirm" type="password" class="form-control <?php echo $pass_required ?>">
                                </div>

                                <div class="col-lg-12">
                                    <div class="hr-line-dashed"></div>
                                </div>
                            </div>

                            <div class="row">
                                <?php
                                $txt_help_login_active = "";
                                $txt_help_login_active .= ($is_active_check ? "L'utente può effettuare il login" : "L'utente non può effettuare il login");
                                if($is_active_check) {
                                    $txt_help_login_active .= " " . ($is_mail_check ? "ed ha confermato la propria mail." : "ma per accedere dovrà prima confermare la propria email attraverso il messaggio che riceverà sulla casella di posta.");
                                }

                                ?>
                                <div class="col-sm-12">
                                    <label id="lbl_explain_access"><?= $txt_help_login_active ?></label>
                                </div>
                            </div>

                        </fieldset>

                        <?php if(isset($carrello_cliente)) { ?>
                            <h1>Articoli Nel Carrello</h1>
                            <fieldset>

                                <?php
                                $total_tax = 0;
                                foreach($carrello_cliente as $product) {
                                    $total_tax += (new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['prezzo']['tax']) * $product['quantity'];
                                }
                                ?>

                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col" colspan="2">Prodotto</th>
                                        <th scope="col">Prezzo</th>
                                        <th scope="col">Quantità</th>
                                        <th scope="col">Totale</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($carrello_cliente as $product) { ?>
                                        <tr>
                                            <td data-label="Prodotto">
                                                <img class="img-thumbnail img-md" src="<?= UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML .  $product['image']; ?>" onerror="this.style.display='none'">
                                            </td>
                                            <td>
                                                <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'ecommerce/prodotti/list/edit.php?id=' . $product['product_id']; ?>" class="product_title"><?= $product['nome']; ?></a>
                                                <?php
                                                if($product['formattedVariation']) {
                                                    echo '<br><small class="attribute">' . implode(', ', array_map(array($MSFrameworki18n, 'getFieldValue'), $product['formattedVariation'])) . '</small>';
                                                }
                                                if(!empty($product['custom'])) {
                                                    echo '<br><small class="attribute">' . $product['custom'] . '</small>';
                                                }
                                                ?>
                                            </td>
                                            <td data-label="Prezzo">
                                                <span class="resoconto_singolo"><i><?= $product['quantity']; ?></i> x <b><?= number_format($product['prezzo']['cart_price'][0] . '.' . $product['prezzo']['cart_price'][1],2,',','.'); ?><?= CURRENCY_SYMBOL; ?></b></span>
                                            </td>
                                            <td data-label="Quantità">
                                                <b><?=  $product['quantity']; ?></b>
                                            </td>
                                            <td data-label="Prezzo">
                                                    <span class="resoconto_singolo">
                                                        <b><?= number_format( (new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['prezzo']['cart_price']) * $product['quantity'],2,',','.'); ?><?= CURRENCY_SYMBOL; ?></b> <?= ($tax_settings['inclusa_carrello'] == 1 ? '<small class="suffix">' . $tax_settings['suffix'] . '</small>' : ''); ?>
                                                        <?php if($tax_settings['inclusa_carrello'] == 0 && $tax_settings['mostra_per_singolo']) { ?>
                                                            <span class="iva_singola">
                                                                <span class="piu">+</span>
                                                                <?= $MSFrameworki18n->getFieldValue($info_imposte[$product['imposta']][0]); ?>
                                                                <b class="value"><?=  number_format((new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['prezzo']['tax']) * $product['quantity'],2,',','.'); ?><?= CURRENCY_SYMBOL; ?></b>
                                                            </span>
                                                        <?php } ?>
                                                    </span>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            <?php if(!$carrello_cliente) { ?>
                                                <small style="line-height: 40px;">Il cliente non ha aggiunto nessun prodotto al carrello.</small>
                                            <?php } ?>
                                        </td>
                                        <td data-label="">
                                            <span class="total_title">Totale</span><br>
                                            <b class="total"><?= number_format($total_tax, 2); ?><?= CURRENCY_SYMBOL; ?></b>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </fieldset>
                        <?php } ?>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
$(document).ready(function() {

    if(!$('#secondary_table_list').length) {
        loadStandardDataTable('main_table_list', true);
        allowRowHighlights('main_table_list');
        manageGridButtons('main_table_list');
    } else {
        initClassicTabsEditForm();

        loadStandardDataTable('main_table_list', true, {ajax: 'ajax/dataTable.php?type=active'});
        allowRowHighlights('main_table_list');
        manageGridButtons('main_table_list');

        loadStandardDataTable('secondary_table_list', true, {ajax: 'ajax/dataTable.php?type=inactive'});
        allowRowHighlights('secondary_table_list');
        manageGridButtons('secondary_table_list');
    }
});

function composeAccessHelperTXT() {
    txt_help_login_active = "";
    txt_help_login_active += ($('#is_active:checked').length ? "L'utente può effettuare il login" : "L'utente non può effettuare il login");
    if($('#is_active:checked').length) {
        txt_help_login_active += " " + ($('#mail_active:checked').length ? "ed ha confermato la propria mail." : "ma per accedere dovrà prima confermare la propria email attraverso il messaggio che riceverà sulla casella di posta.");
    }

    $('#lbl_explain_access').html(txt_help_login_active);
}

function initForm() {
    initClassicTabsEditForm();
    initOrak('images', 1, 0, 0, 100, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);

    $('#data_nascita').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
        endDate: new Date(),
    });

    $('#email').on('keyup', function() {
        $('#newsletter__status_container').html('Verifico...');

        $.ajax({
            url: "ajax/checkNewsletterSubscribed.php?mail=" + $('#email').val(),
            success: function(data) {
                $('#newsletter__status_container').html(data);

                $("#newsletter_list").multiselect({
                    columns: 3,
                    texts: {
                        placeholder: 'Seleziona liste'
                    }
                });

                $('#subscribe_to_newsletter').unbind('click');
                $('#subscribe_to_newsletter').on('click', function() {
                    bootbox.confirm({
                        message: "Stai per iscrivere questo utente alla newsletter. Devi avere acquisito il consenso dell'utente per poter procedere all'iscrizione. Continuare?",
                        buttons: {
                            confirm: {
                                label: 'OK, iscrivi',
                                className: 'btn-success',
                            },
                            cancel: {
                                label: 'Annulla',
                                className: 'btn-danger',
                            }
                        },
                        callback: function (result) {
                            if (result === true) {

                                var lists = [];
                                if($('#newsletter_list').length) {
                                    lists = $('#newsletter_list').val();
                                }

                                $.ajax({
                                    url: "ajax/subscribeToNewsletter.php",
                                    dataType: "json",
                                    data: {
                                        id: $('#record_id').val(),
                                        mail: $('#email').val(),
                                        nome:  $('#nome').val(),
                                        cognome:  $('#cognome').val(),
                                        lists: lists
                                    },
                                    success: function (data) {
                                        if(data.status == "mandatory_data_missing") {
                                            bootbox.error("Per poter compiere questa operazione, devi prima compilare i campi nome, cognome ed email!");
                                        } else if(data.status == "mail_validation") {
                                            bootbox.error("L'indirizzo email fornito non è valido!");
                                        } else if(data.status == "customer_already_exists") {
                                            bootbox.error("Esiste già un cliente con il seguente indirizzo email");
                                        } else {
                                            succesModuleSaveFunctionCallback(data);
                                            $('#email').trigger('keyup');
                                        }
                                    }
                                });
                            }
                            return true;
                        }
                    });
                })
            }
        })
    });
    $('#email').trigger('keyup');

    $('#is_active').on('ifChanged', function () {
        var is_checked = $('#is_active:checked').length;
        if(is_checked) {
            $('#passwords_box').show();
            $('#mail_confirmed_container').show();
            if($('#passwords_box input').hasClass('empty_pass')) {
                $('#passwords_box input').addClass('required');
            }
            $('#email').addClass('required');
            addRequiredFieldLabel();
        }
        else {
            $('#passwords_box').hide();
            $('#mail_confirmed_container').hide();
            $('#passwords_box input').val('').removeClass('required');
            $('#email').removeClass('required');
            addRequiredFieldLabel();
        }

        composeAccessHelperTXT();
    });

    $('#mail_active').on('ifChanged', function () {
        composeAccessHelperTXT();
    });

}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pCognome": $('#cognome').val(),
            "pDataNascita": $('#data_nascita').val(),
            "pEmail": $('#email').val(),
            "pTelefonoCasa": $('#telefono_casa').val(),
            "pTelefonoCellulare": $('#telefono_cellulare').val(),
            "pSitoWeb": $('#sito_web').val(),
            "pIndirizzo": $('#indirizzo').val(),
            "pRegione": $('#regione').val(),
            "pStato": $('#stato').val(),
            "pCitta": $('#citta').val(),
            "pProvincia": $('#provincia').val(),
            "pCAP": $('#cap').val(),
            "pProvenienza": $('#provenienza').val(),
            "pSesso": $('#sesso').val(),
            "pFatturazione" : {
                "ragione_sociale" : $('#ragione_sociale').val(),
                "piva" : $('#piva').val(),
                "cf" : $('#cf').val(),
                "indirizzo" : $('#indirizzo_fatturazione').val(),
                "stato" : $('#stato_fatturazione').val(),
                "citta" : $('#citta_fatturazione').val(),
                "provincia" : $('#provincia_fatturazione').val(),
                "regione" : $('#regione_fatturazione').val(),
                "cap" : $('#cap_fatturazione').val(),
            },
            "pPass": $('#password').val(),
            "pPassConfirm": $('#password_confirm').val(),
            "pIsActive": $('#is_active:checked').length,
            "pMailConfermata": $('#mail_active:checked').length,
            "pImagesAry": composeOrakImagesToSave('images'),
            "pAgenteRiferimento": $('#agente_riferimento').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "site_not_valid") {
                bootbox.error("L'indirizzo web inserito è in un formato non valido! Assicurarsi di inserire http:// prima dell'indirizzo");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido o è già stata utilizzata da un altro utente!");
            } else if(data.status == "email_missing") {
                bootbox.error("Se abiliti il login utente l'indirizzo email è obbligatorio!");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data di scadenza non è valido. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "password_strength") {
                bootbox.error("La password deve essere lunga almeno 8 caratteri e deve contenere almeno una lettera ed un numero!");
            } else if(data.status == "password_does_not_match") {
                bootbox.error("Le password inserite devono corrispondere!");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
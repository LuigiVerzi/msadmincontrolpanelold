<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($_SESSION['userData']['userlevel'] != 0 && $MSFrameworkDatabase->getCount("SELECT id FROM customers WHERE id IN(" . implode(',', $_POST['pID']) . ") AND (owner_id != '' AND owner_id != :owner)", array(":owner" => (new \MSFramework\users())->getUserDataFromSession('user_id'))) > 0) {
    echo json_encode(array("status" => "not_allowed"));
    die();
}

if($MSFrameworkDatabase->deleteRow("customers", "id", $_POST['pID'])) {

    // Elimina le referenze del cliente
    $MSFrameworkDatabase->deleteRow("customers_balance", "cliente", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("customers_gift", "cliente", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("beautycenter_sedute", "cliente", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("ecommerce_users_extra", "id", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("affiliations__customers", "id", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("affiliations__withdrawals", "cliente", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("subscriptions__subscribers", "cliente", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("newsletter__destinatari", "id", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("newsletter__tag_destinatari", "destinatario", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("points_histories", "cliente", $_POST['pID']);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
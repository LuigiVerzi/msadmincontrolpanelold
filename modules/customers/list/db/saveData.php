<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "") {
    $can_save = false;
}

if($_POST['pIsActive'] == "1") {

    if($db_action == "insert") {
        if($_POST['pPass'] == "" || $_POST['pPassConfirm'] == "") {
            $can_save = false;
        }
    }

    if($_POST['pEmail'] == "") {
        echo json_encode(array("status" => "email_missing"));
        die();
    }

    if($_POST['pPass'] != $_POST['pPassConfirm']) {
        echo json_encode(array("status" => "password_does_not_match"));
        die();
    }

    if($_POST['pPass'] != "" && (new \MSFramework\customers())->checkPasswordStrength($_POST['pPass']) != "") {
        echo json_encode(array("status" => "password_strength"));
        die();
    }

}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$mail_conflict = false;
if($_POST['pEmail'] != "") {
    if($MSFrameworkDatabase->getCount("SELECT email FROM customers WHERE email = :mail AND id != :id", array(":mail" => $_POST['pEmail'], ":id" => $_POST['pID'])) != 0) {
        $mail_conflict = true;
    }

    if(!filter_var($_POST['pEmail'], FILTER_VALIDATE_EMAIL)) {
        $mail_conflict = true;
    }
}

if($_POST['pSitoWeb'] != "") {
    if(!filter_var($_POST['pSitoWeb'], FILTER_VALIDATE_URL)) {
        echo json_encode(array("status" => "site_not_valid"));
        die();
    }
}

if($mail_conflict) {
    echo json_encode(array("status" => "mail_not_valid"));
    die();
}

$ts_scadenza = "";
if($_POST['pDataNascita'] != "") {
    $ary_scadenza = explode("/", $_POST['pDataNascita']);
    $ts_scadenza = mktime(23, 59, 59, $ary_scadenza[1], $ary_scadenza[0], $ary_scadenza[2]);
    if(!$ts_scadenza) {
        echo json_encode(array("status" => "expire_not_valid"));
        die();
    }
}

if($db_action == "update") {
    $r_old_userinfo = (new \MSFramework\customers())->getCustomerDataFromDB($_POST['pID'], "avatar, mail_auth, active");
}

$send_activation_mail = false;
if($_POST['pIsActive'] == "1") {
    if ($db_action == "update") {
        if ($r_old_userinfo['mail_auth'] == "" && $_POST['pMailConfermata'] == "0") {
            $send_activation_mail = true;
        }
    } else {
        if ($_POST['pMailConfermata'] == "0") {
            $send_activation_mail = true;
        }
    }
}

$send_convalidation_confirm = false;
if($_POST['pIsActive'] == "1") {
    if ($db_action == "update") {
        if ($r_old_userinfo['active'] == "0") {
            $send_convalidation_confirm = true;
        }
    }
}

$uploader = new \MSFramework\uploads('CUSTOMER_AVATAR');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "cognome" => $_POST['pCognome'],
    "data_nascita" => $ts_scadenza,
    "email" => $_POST['pEmail'],
    "telefono_casa" => $_POST['pTelefonoCasa'],
    "telefono_cellulare" => $_POST['pTelefonoCellulare'],
    "sito_web" => $_POST['pSitoWeb'],
    "indirizzo" => $_POST['pIndirizzo'],
    "citta" => $_POST['pCitta'],
    "provincia" => $_POST['pProvincia'],
    "stato" => $_POST['pStato'],
    "regione" => $_POST['pRegione'],
    "cap" => $_POST['pCAP'],
    "provenienza" => $_POST['pProvenienza'],
    "sesso" => $_POST['pSesso'],
    "dati_fatturazione" => json_encode($_POST['pFatturazione']),
    "password" => password_hash($_POST['pPass'], PASSWORD_DEFAULT),
    "active" => $_POST['pIsActive'],
    "owner_id" => $_POST['pAgenteRiferimento'],
    "mail_auth" => sha1($_POST['pEmail'] . time()),
    "avatar" => json_encode($ary_files),
);

if($_POST['pMailConfermata'] == "1") {
    $array_to_save['mail_auth'] = "";
}

if($db_action == "update") {

    if(!$send_activation_mail && $_POST['pMailConfermata'] == "0") {
        unset($array_to_save['mail_auth']);
    }

    if($_POST['pIsActive'] != "1"  || $_POST['pPass'] == "" ) {
        unset($array_to_save['password']);
    }
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO customers ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $new_user_id = $MSFrameworkDatabase->lastInsertId();
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE customers SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $new_user_id = '';
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_userinfo['avatar'], true));
}

if($send_convalidation_confirm) {
    (new \MSFramework\emails())->sendMail('customer-account-confirmed', array(
        "email" => $_POST['pEmail'],
        'nome' => (!empty($array_to_save['nome']) ? $array_to_save['nome'] . ' ' . $array_to_save['cognome'] : $array_to_save['email']),
    ));
}

if($send_activation_mail) {
    (new \MSFramework\emails())->sendMail('mail-confirmation', array("email" => $_POST['pEmail'], "origin" => 'customer'));
    echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : ''), "email" => "must_confirm"));
    die();
}

/* CERCA DI OTTENERE IL GRAVATAR DEL CLIENTE SE NON HA UN AVATAR IMPOSTATO */
if($db_action == 'insert' && !count($ary_files)) {
    (new \MSFramework\customers())->setGravatarIfExist($MSFrameworkDatabase->lastInsertId());
}

echo json_encode(array("status" => "ok", "id" => $new_user_id));
die();
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    header('Location: index.php');
    die();
}

$r = array();
$customer = (New \MSFramework\customers())->getCustomerDataFromDB($_GET['customer']);

if(!$customer) {
    header('Location: index.php');
}

$bilancio = (new \MSFramework\customers())->getCustomerBalance($customer['id']);

$info_localita = array();

if(!empty($customer['indirizzo'])) {
    $info_localita[] = $customer['indirizzo'];
}
if(!empty($customer['citta'])) {
    $info_localita[] = $customer['citta'];
}
if(!empty($customer['provincia'])) {
    $info_localita[] = $customer['cap'];
}

$info_principali = array();
if(!empty($customer['email'])) {
    $info_principali[] = $customer['email'];
}
if(!empty($customer['telefono_casa'])) {
    $info_principali[] = $customer['telefono_casa'];
}
if(!empty($customer['telefono_cellulare'])) {
    $info_principali[] = $customer['telefono_cellulare'];
}

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-md-3">

                    <div class="contact-box">

                        <h3 class="m-b-xs"><strong><?= $customer['nome']; ?> <?= $customer['cognome']; ?></strong></h3>

                        <address class="m-t-md">
                            <?= implode('<br>', $info_principali); ?>
                        </address>

                        <address class="m-t-md">
                            <?= implode('<br>', $info_localita); ?>
                        </address>

                        <div class="contact-box-footer" style="padding: 10px 0 0;">
                            <div class="widget style1 bg-<?= ($bilancio < 0 ? 'danger' : ($bilancio == 0 ? 'warning' : 'primary')); ?>" style="margin: 0;">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="fa fa-ticket fa-5x"></i>
                                    </div>
                                    <div class="col-xs-8 text-right">
                                        <span>Bilancio Attuale</span>
                                        <h2 class="font-bold"><?= number_format($bilancio, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-lg-9">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <input type="hidden" id="customer_id" value="<?= $customer['id']; ?>">
                        <h1><?= ($r ? 'Modifica' : 'Nuovo'); ?> Debito/Credito</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Tipologia</label>
                                    <select id="tipologia" name="tipologia" class="form-control">
                                        <option value="credito" <?= ($r['tipo'] == 'credito' ? 'selected' : ''); ?>>Credito Cliente</option>
                                        <option value="debito" <?= ($r['tipo'] == 'debito' ? 'selected' : ''); ?>>Debito Cliente</option>
                                    </select>
                                    <div class="hr-line-dashed"></div>
                                    <label>Valore</label>
                                    <input type="number" id="valore" name="valore" class="form-control" value="<?= (float)$r['valore']; ?>" min="0">
                                    <div class="hr-line-dashed"></div>
                                    <label>Note Interne</label>
                                    <textarea id="note" name="note" class="form-control"><?= htmlentities($r['note']); ?></textarea>
                                </div>
                                <div class="col-sm-6">
                                    <label>Nuovo Bilancio <small>(successivo al salvataggio)</small></label>
                                    <div class="p-xs border-top-bottom border-left-right border-size-lg text-center" id="newBalance"><span class="text-info">...</span></div>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
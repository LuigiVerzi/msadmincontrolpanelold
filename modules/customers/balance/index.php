<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$stati_cliente = array();

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM customers_balance") as $operazione) {
    if(!isset($stati_cliente[$operazione['cliente']])) $stati_cliente[$operazione['cliente']] = 0.0;
    if($operazione['tipo'] == 'debito')  $stati_cliente[$operazione['cliente']] -= (float)$operazione['valore'];
    else $stati_cliente[$operazione['cliente']] += (float)$operazione['valore'];
}

$crediti = 0.0;
$debiti = 0.0;

foreach($stati_cliente as $stato_cliente) {
    if($stato_cliente > 0) {
        $crediti += abs($stato_cliente);
    }
    else {
        $debiti += abs($stato_cliente);
    }
}

$bilancio = $debiti-$crediti;

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewDelGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Resoconto</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <h1 class="no-margins"><?= number_format($debiti, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                                    <div class="font-bold text-danger">Debiti clienti</div>
                                </div>
                                <div class="col-md-6">
                                    <h1 class="no-margins"><?= number_format($crediti, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                                    <div class="font-bold text-navy">Crediti clienti</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-<?= ($bilancio >= 0 ? 'primary' : 'danger'); ?> pull-right">Sei in <?= ($bilancio >= 0 ?  ($bilancio == 0 ? 'Pari' : 'Credito') : 'Debito'); ?></span>
                            <h5>Rapporto</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins"><?= number_format($bilancio, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                            <div class="stat-percent font-bold text-success"></div>
                            <small>Bilancio Attuale</small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Utenti con bilancio</h1>
                        <fieldset>
                            <table id="customers_with_balances_table" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Bilancio</th>
                                    <th>Movimenti</th>
                                    <th class="default-sort is_data" data-sort="DESC">Ultimo Movimento</th>
                                </tr>
                                </thead>
                            </table>
                        </fieldset>

                        <h1>Utenti senza bilancio</h1>
                        <fieldset>
                            <table id="customers_without_balances_table" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th class="no-sort"></th>
                                </tr>
                                </thead>
                            </table>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
<script>
    globalInitForm();
</script>
</html>
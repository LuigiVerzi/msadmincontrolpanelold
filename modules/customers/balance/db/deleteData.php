<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$type = 'cliente';
if(strpos($_SERVER['HTTP_REFERER'], 'balance.php') !== false || strpos($_SERVER['HTTP_REFERER'], 'edit.php') !== false)
{
    $type = 'id';
}
if($MSFrameworkDatabase->deleteRow("customers_balance", $type, $_POST['pID'])) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
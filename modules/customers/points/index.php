<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$stati_cliente = array();

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM points_histories") as $operazione) {
    if(!isset($stati_cliente[$operazione['cliente']])) $stati_cliente[$operazione['cliente']] = 0.0;
    if($operazione['tipo'] == 'debito')  $stati_cliente[$operazione['cliente']] -= (float)$operazione['valore'];
    else $stati_cliente[$operazione['cliente']] += (float)$operazione['valore'];
}

$crediti = 0.0;
$debiti = 0.0;

foreach($stati_cliente as $stato_cliente) {
    if($stato_cliente > 0) {
        $crediti += abs($stato_cliente);
    }
    else {
        $debiti += abs($stato_cliente);
    }
}

$bilancio = $debiti-$crediti;

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewDelGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">

                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Utenti con punti</h1>
                        <fieldset>
                            <table id="customers_with_balances_table" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Punti</th>
                                    <th>Movimenti</th>
                                    <th class="default-sort is_data" data-sort="DESC">Ultimo Movimento</th>
                                </tr>
                                </thead>
                            </table>
                        </fieldset>

                        <h1>Utenti senza punti</h1>
                        <fieldset>
                            <table id="customers_without_balances_table" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th class="no-sort"></th>
                                </tr>
                                </thead>
                            </table>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
<script>
    globalInitForm();
</script>
</html>
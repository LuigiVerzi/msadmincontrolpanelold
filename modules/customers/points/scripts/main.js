$(document).ready(function() {
    if(!$('#main_table_list').hasClass('balance_table')) {
        loadStandardDataTable('customers_with_balances_table', true, {"ajax": 'ajax/dataTable.php?source=with_balances'});
        allowRowHighlights('customers_with_balances_table');
        manageGridButtons('customers_with_balances_table');

        loadStandardDataTable('customers_without_balances_table', true, {"ajax": 'ajax/dataTable.php?source=without_balances'});
        allowRowHighlights('customers_without_balances_table');
        manageGridButtons('customers_without_balances_table');
    }
});

function initForm() {
    initClassicTabsEditForm();

    if($('#record_id').length) {
        $('#cliente').chosen();
        $('#valore, #tipologia').on('change', function () {

            $.ajax({
                url: "ajax/getCustomerBalance.php",
                type: "POST",
                data: {
                    "id": $('#customer_id').val()
                },
                async: true,
                success: function (bilancio_attuale) {
                    bilancio_attuale = parseFloat(bilancio_attuale);

                    var newValue = $('#valore').val();
                    if(isNaN(newValue)) newValue = 0;

                    if(newValue > 0) {
                        if ($('#tipologia').val() == 'debito') {
                            bilancio_attuale += parseFloat(-newValue);
                        } else {
                            bilancio_attuale += parseFloat(newValue);
                        }
                    }

                    $('#newBalance').html('<span class="text-' + (parseFloat(bilancio_attuale) < 0 ? 'danger' : 'info') + '">' + parseFloat(bilancio_attuale) + '</span>');
                }
            });
        });
        $('#valore').change();
    }
    else {
        loadStandardDataTable('main_table_list', true, {
            "ajax": 'ajax/dataTable.php?balance=' + $('#customer_id').val(),
            "searching": false,
            "lengthChange" : false
        }, false);
    }

}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pCliente": $('#customer_id').val(),
            "pTipo": $('#tipologia').val(),
            "pValore": $('#valore').val(),
            "pNote": $('#note').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
                location.href = 'edit.php?id=' + $('#customer_id').val();
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = (New \MSFramework\customers())->getCustomerDataFromDB($_GET['id']);
    $bilancio = (new \MSFramework\Points\points())->getCustomerBalance($r['id']);

    $info_localita = array();

    if(!empty($r['indirizzo'])) {
        $info_localita[] = $r['indirizzo'];
    }
    if(!empty($r['citta'])) {
        $info_localita[] = $r['citta'];
    }
    if(!empty($r['provincia'])) {
        $info_localita[] = $r['cap'];
    }

    $info_principali = array();
    if(!empty($r['email'])) {
        $info_principali[] = $r['email'];
    }
    if(!empty($r['telefono_casa'])) {
        $info_principali[] = $r['telefono_casa'];
    }
    if(!empty($r['telefono_cellulare'])) {
        $info_principali[] = $r['telefono_cellulare'];
    }

    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
    if(json_decode($r['avatar'])) {
        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
    }

}

if(!isset($r) || !$r) {
    header('Location: index.php');
}

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">

                <div class="col-md-3">

                    <div class="contact-box">
                        <div class="profile-image">
                            <img src="<?= $avatar_url; ?>" class="rounded-circle circle-border m-b-md" alt="profile">
                        </div>
                        <div class="profile-info">
                            <div class="">
                                <div>
                                    <h2 class="no-margins">
                                        <?= $r['nome']; ?> <?= $r['cognome']; ?>
                                    </h2>
                                    <h4>Registrato dal <?= date('d/m/Y H:i', strtotime($r['registration_date'])); ?></h4>
                                    <address class="m-t-sm">
                                        <?= implode('<br>', $info_principali); ?>
                                    </address>
                                    <address class="m-t-md">
                                        <?= implode('<br>', $info_localita); ?>
                                    </address>
                                </div>
                            </div>
                        </div>

                        <div class="contact-box-footer" style="padding: 10px 0 0;">
                            <div class="widget style1 bg-<?= ($bilancio < 0 ? 'danger' : ($bilancio == 0 ? 'warning' : 'primary')); ?>" style="margin: 0;">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="fa fa-ticket fa-5x"></i>
                                    </div>
                                    <div class="col-xs-8 text-right">
                                        <span>Bilancio Attuale</span>
                                        <h2 class="font-bold"><?= (int)$bilancio; ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-lg-9">

                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5 style="line-height: 33px; font-size: 14px;">Lista Movimenti</h5>
                            <div class="ibox-tools" style="margin-bottom: 6px;">
                                <a class="btn btn-primary" id="dataTableNew" href="balance.php?customer=<?= $_GET['id']; ?>">Aggiungi</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <input type="hidden" id="customer_id" value="<?= $_GET['id']; ?>">
                            <table id="main_table_list" class="balance_table display table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="margin-bottom: -30px;">
                                <thead>
                                <tr>
                                    <th>Tipologia</th>
                                    <th>Valore</th>
                                    <th>Note</th>
                                    <th class="default-sort is_data" data-sort="DESC">Data</th>
                                    <th class="no-sort" style="width: 35px;" width="35px"></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
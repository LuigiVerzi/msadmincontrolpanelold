<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

if($db_action == 'update') {
    die();
}

$can_save = true;

if($_POST['pCliente'] == "" || $_POST['pValore'] == "" || $_POST['pTipo'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

(new \MSFramework\Points\points())->addNewOperation($_POST['pCliente'],  floatval(($_POST['pTipo'] === 'debito' ? '-' : '') . $_POST['pValore']), $_POST['pNote']);

echo json_encode(array("status" => "ok"));
die();

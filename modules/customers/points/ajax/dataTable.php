<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();
$MSFrameworkPoints = new \MSFramework\Points\points();

if(isset($_GET['balance'])) {

    $columns = array(
        array(
            'db' => 'id',
            'dt' => 'DT_RowId',
            'formatter' => function( $d, $row ) {
                return $d;
            }
        ),
        array(
            'db' => 'valore',
            'dt' => 0,
            'formatter' => function($d, $row) {
                return '<span class="label label-' . ($d < 0 ? 'danger' : 'primary') . '">' . ($d < 0 ? 'Scalati' : 'Aggiunti') . '</span>';
            }
        ),
        array(
            'db' => 'valore',
            'dt' => 1,
            'formatter' => function($d, $row) {
                return $d;
            }
        ),
        array(
            'db' => 'note',
            'dt' => 2,
            'formatter' => function($d, $row) {
                return (new \MSFramework\customers())->parseBalanceNotes($d);
            }
        ),
        array(
            'db' => 'data',
            'dt' => 3,
            'formatter' => function($d, $row) {
                return array("display" => date("d/m/Y H:i", strtotime($d)), "sort" => strtotime($d));
            }
        ),
        array(
            'db' => 'id',
            'dt' => 4,
            'formatter' => function($d, $row) {
                return '<div class="text-right"><a href="javascript:;" class="btn btn-danger btn-outline btn-sm" onclick="deleteRecord(' . $d . ');"><i class="fa fa-trash-o"></i></a></div>';
            }
        )
    );

    echo json_encode(
        $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'points_histories', 'cliente', $columns, null, 'cliente = ' . (int)$_GET['balance'])
    );
}
else
{

    if($_GET['source'] == 'with_balances') {

        $columns = array(
            array(
                'db' => 'id',
                'dt' => 'DT_RowId',
                'formatter' => function ($d, $row) {
                    return $d;
                }
            ),
            array(
                'db' => 'CONCAT(nome,cognome,email)',
                'dt' => 0,
                'formatter' => function ($d, $row) {
                    Global $MSFrameworkCustomers;

                    $r = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

                    $info = array();
                    if (!empty($r['email'])) {
                        $info[] = $r['email'];
                    }
                    if (!empty($r['telefono_casa'])) {
                        $info[] = $r['telefono_casa'];
                    }
                    if (!empty($r['telefono_cellulare'])) {
                        $info[] = $r['telefono_cellulare'];
                    }

                    $return_html = '';

                    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
                    if(json_decode($r['avatar'])) {
                        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
                    }

                    $return_html .= '<div style="position: relative; padding-left: 65px;">';
                    $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
                    $return_html .= '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
                    $return_html .= '</div>';

                    return $return_html;
                }
            ),
            array(
                'db' => '(SELECT (SELECT SUM(valore) FROM points_histories WHERE points_histories.cliente = customers.id))',
                'dt' => 1,
                'formatter' => function ($d, $row) {
                    Global $MSFrameworkPoints;
                    $balance = $MSFrameworkPoints->getCustomerBalance($row[0]);
                    return '<span class="label label-' . ($balance < 0 ? 'danger' : ($balance == 0 ? 'warning' : 'primary')) . '">' . (int)$balance . '</span>';
                }
            ),
            array(
                'db' => '(SELECT COUNT(*) FROM points_histories WHERE points_histories.cliente = customers.id)',
                'dt' => 2,
                'formatter' => function ($d, $row) {
                    return $d;
                }
            ),
                array(
                'db' => '(SELECT MAX(data) FROM points_histories WHERE points_histories.cliente = customers.id)',
                'dt' => 3,
                'formatter' => function ($d, $row) {
                    if ($d) {
                        return array("display" =>  date('d/m/Y H:i', strtotime($d)), 'sort' => strtotime($d));
                    } else {
                        return array("display" => '<small class="text-muted">Mai</small>', 'sort' => strtotime($d));
                    }
                }
            )
        );

        echo json_encode(
            $datatableHelper::complex($_GET, $MSFrameworkDatabase, 'customers', 'id', $columns, null, '(SELECT COUNT(*) FROM points_histories WHERE points_histories.cliente = customers.id) > 0')
        );
    } else {

        $columns = array(
            array(
                'db' => 'id',
                'dt' => 'DT_RowId',
                'formatter' => function ($d, $row) {
                    return $d;
                }
            ),
            array(
                'db' => 'CONCAT(nome,cognome,email)',
                'dt' => 0,
                'formatter' => function ($d, $row) {

                    Global $MSFrameworkCustomers;

                    $r = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

                    $info = array();
                    if (!empty($r['email'])) {
                        $info[] = $r['email'];
                    }
                    if (!empty($r['telefono_casa'])) {
                        $info[] = $r['telefono_casa'];
                    }
                    if (!empty($r['telefono_cellulare'])) {
                        $info[] = $r['telefono_cellulare'];
                    }

                    $return_html = '';

                    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
                    if(json_decode($r['avatar'])) {
                        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
                    }

                    $return_html .= '<div style="position: relative; padding-left: 65px;">';
                    $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
                    $return_html .= '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
                    $return_html .= '</div>';

                    return $return_html;
                }
            ),
            array(
                'db' => 'id',
                'dt' => 1,
                'formatter' => function ($d, $row) {


                    return '<span class="text-muted">Ancora nessuna transazione</span>';
                }
            ),
        );

        echo json_encode(
            $datatableHelper::complex($_GET, $MSFrameworkDatabase, 'customers', 'id', $columns, null, '(SELECT COUNT(*) FROM points_histories WHERE points_histories.cliente = customers.id) = 0')
        );

    }
}
?>

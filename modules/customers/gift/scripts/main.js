$(document).ready(function() {
    if(!$('#main_table_list').hasClass('balance_table')) {
        loadStandardDataTable('customers_with_gift', true, {"ajax": 'ajax/dataTable.php?source=with_gift'});
        allowRowHighlights('customers_with_gift');
        manageGridButtons('customers_with_gift');

        loadStandardDataTable('customers_without_gift', true, {"ajax": 'ajax/dataTable.php?source=without_gift'});
        allowRowHighlights('customers_without_gift');
        manageGridButtons('customers_without_gift');
    }
});

function initForm() {
    initClassicTabsEditForm();

    loadStandardDataTable('main_table_list', true, {
        "ajax": 'ajax/dataTable.php?balance=' + $('#customer_id').val(),
        "searching": false,
        "lengthChange" : false
    }, false);

}
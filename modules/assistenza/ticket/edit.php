<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($_GET['id'])[$_GET['id']];
}

if($r) {
    header('Location: view.php?id=' . $r['id']);
    die();
}

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="" />
            <input type="hidden" id="record_type" value="edit" />

            <div class="row" style="margin-bottom: 15px;">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>

                            <h2 class="title-divider">Apertura Ticket di Assistenza</h2>

                            <div class="row">
                                <div class="col-sm-8 col-lg-9">
                                    <label>Titolo</label>
                                    <input id="titolo" name="titolo" type="text" class="form-control required" placeholder="Di cosa si tratta?" value="">
                                </div>

                                <div class="col-sm-4 col-lg-3">
                                    <label>Tipologia di richiesta</label>
                                    <select id="categoria" name="categoria" class="form-control required" required>
                                        <option value=""></option>
                                        <?php foreach((new \MSFramework\Framework\ticket())->getTicketCategories() as $id => $category) { ?>
                                            <option value="<?= $id; ?>" data-disclaimer="<?= htmlentities($category['disclaimer']); ?>"><?= $category['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div id="disclaimer_box" style="display: none;">
                                <div class="alert alert-info"></div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Messaggio</label>
                                    <textarea id="descrizione" name="descrizione" class="form-control required" placeholder="Spiegaci nel dettaglio quello di cui hai bisogno, cercheremo di risponderti il prima possibile" rows="4"></textarea>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-3">
                                    <label>Notifica aggiornamenti via email</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="notifica_via_mail" class="notifica_via_mail" checked>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita notifiche via email</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 notification_email_container">
                                    <label>Email di notifica</label>
                                    <input id="email_notifica" name="email_notifica" type="email" class="form-control required" placeholder="Dove possiamo inviarti le notifiche?" value="<?= $MSFrameworkUsers->getUserDataFromSession('username'); ?>">
                                </div>

                                <div class="col-sm-5 text-right pull-right" style="margin-bottom: 15px;">
                                    <a class="btn btn-success" id="editSaveBtn" saving_txt="Salvo i dati...">Apri Ticket</a>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Allegati</label>
                                    <?php
                                    (new \MSFramework\uploads('ASSISTENZA_TICKET'))->initUploaderHTML("allegati", json_encode(array()), array(), array(), false);
                                    ?>
                                </div>

                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>

            <div style="margin-bottom: -20px;">
            <?php include('includes/contacts_box.php'); ?>
            </div>

        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
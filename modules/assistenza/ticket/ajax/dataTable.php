<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkUtils = new \MSFramework\utils();

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    ),
    array(
        'db' => 'question_date',
        'dt' => 'DT_QuestionDate'
    ),
    array(
        'db' => 'id',
        'dt' => 'DT_buttonsLimit',
        'formatter' => function( $d, $row ) {
            return "read";
        }
    ),
    array( 'db' => 'status', 'dt' => 0,
        'formatter' => function( $d, $row ) {

            $status_label = array(
                'label' => 'In Attesa',
                'color' => 'success'
            );

            if($d > 0) {
                if($d == 100) {
                    $status_label = array(
                        'label' => 'Completato',
                        'color' => 'danger'
                    );
                } else {
                    $status_label = array(
                        'label' => 'In Corso',
                        'color' => 'primary'
                    );
                }
            }

            return '<span class="label label-' . $status_label['color'] . '" style="padding: 0;width: 100%; line-height: 40px; margin: auto; display: block;">' . $status_label['label'] . '</span>';
        }
    ),
    array( 'db' => 'title', 'dt' => 1,
        'formatter' => function( $d, $row ) {
            Global $MSFrameworkUtils;
            return '<b>' . $d . '</b><br><small>' . $MSFrameworkUtils->smartdate(strtotime($row['question_date'])) . '</small>' ;
        }
    ),
    array( 'db' => '(SELECT GROUP_CONCAT(user_id SEPARATOR \',\') FROM marke833_framework.ticket__risposte WHERE marke833_framework.ticket__risposte.question = marke833_framework.ticket__domande.id AND marke833_framework.ticket__risposte.is_staff = 1)', 'dt' => 2,
        'formatter' => function( $d, $row ) {
            $staff_ids = array_unique(explode(',', $d));
            if($staff_ids) {
                $staff_avatar = array();

                foreach((new \MSFramework\users())->getUserDetails($staff_ids) as $staff) {
                    $avatar = ($staff['gallery_friendly']['propic'] ? $staff['gallery_friendly']['propic'][0]['html']['thumb'] : ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png');
                    $staff_avatar[] = '<img alt="image" title="' . $staff['nome'] . ' ' . $staff['cognome'] . '" class="rounded-circle" src="' . $avatar . '">';
                }

                if(in_array('0', $staff_ids)) {
                    $staff_avatar[] = '<img alt="image" title="Assistenza Marketing Studio" class="rounded-circle" src="' . ABSOLUTE_SW_PATH_HTML . 'assets/img/ms.svg">';
                }

                return '<div class="project-people">' . implode(' ', $staff_avatar) . '</div>';
            } else {
                return '';
            }
        }
    ),
    array( 'db' => 'status', 'dt' => 3,
        'formatter' => function( $d, $row ) {

            $status_label = array(
                'label' => 'In attesa di risposta ' . $d . '%',
            );

            if($d > 0) {
                if($d == 100) {
                    $status_label = array(
                        'label' => 'Completato al 100%',
                    );
                } else {
                    $status_label = array(
                        'label' => 'Completato al ' . $d . '%',
                    );
                }
            }

            return '<small>' . $status_label['label'] . '</small><div class="progress progress-mini"><div style="width: ' . $d . '%;" class="progress-bar"></div></div>';
        }
    ),
    array( 'db' => '(SELECT answer_date FROM marke833_framework.ticket__risposte WHERE marke833_framework.ticket__risposte.question = marke833_framework.ticket__domande.id ORDER by id DESC LIMIT 1)', 'dt' => 4,
        'formatter' => function( $d, $row ) {
        Global $MSFrameworkUtils;

            if(!$d) $d = $row['question_date'];
            return array("display" => $MSFrameworkUtils->smartdate(strtotime($d)), "sort" => strtotime($d));
        }
    )
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'marke833_framework.ticket__domande', 'id', $columns, null, "domain LIKE '" . CUSTOMER_DOMAIN_INFO['id'] . "'")
);
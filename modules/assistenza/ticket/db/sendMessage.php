<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;
if($_POST['pMessaggio'] == "" || !$_SESSION['userData']) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$message_id = (new \MSFramework\Framework\ticket())->addReplyToTicket($_POST['pID'], $_SESSION['userData']['user_id'], $_POST['pMessaggio'], $_POST['pAllegati']);

if(!$message_id) {
    json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => $message_id));
die();
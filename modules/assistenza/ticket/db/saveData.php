<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = 'insert';

$can_save = true;

if($_POST['pCategoria'] == "" || $_POST['pTitolo'] == "" || $_POST['pMessaggio'] == "" || !$_SESSION['userData']) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$ticket_id = (new \MSFramework\Framework\ticket())->createNewTicket(CUSTOMER_DOMAIN_INFO['id'], $_SESSION['userData'], $_POST['pTitolo'], $_POST['pMessaggio'], $_POST['pEmailNotifica'], $_POST['pCategoria'], $_POST['pAllegati']);

if(!$ticket_id) {
    json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => $ticket_id));
die();
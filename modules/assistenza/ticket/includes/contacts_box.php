<?php
$marketingStudioData = json_decode($MSFrameworkDatabase->getAssoc("SELECT value FROM marke833_marketingstudio.cms WHERE type = 'site'", array(), true)['value'], true);
$marketingStudioOrari = json_decode($MSFrameworkDatabase->getAssoc("SELECT value FROM marke833_marketingstudio.cms WHERE type = 'working_hours'", array(), true)['value'], true);

$orari_lavoro = new \MSFramework\Attivita\orari($marketingStudioOrari, array(), array(
    'open'           => '<div class="col-sm-4"><small><strong class="text-navy">Disponibile</strong></small></div><div class="col-sm-8 text-right"><small>{%hours%}.</small></div>',
    'closed'         => '<div class="col-sm-4"><small><strong class="text-warning">Attualmente chiusi</strong></small></div><div class="col-sm-8 text-right"><small>{%hours%}.</small></div>',
    'closed_all_day' => '<div class="col-sm-4"><small><strong class="text-danger">Giorno di chiusura</strong></small></div><div class="col-sm-8 text-right"></div>',
    'format' => 'G:i',
    'overview_format' => 'G:i',
    'overview_join' => '<br>'
));
?>

<div class="row">
    <div class="col-md-6 col-lg-4">
        <div class="payment-card">
            <i class="fa fa-phone payment-icon-big text-info"></i>
            <h2>
                <?= $marketingStudioData["telefono"]; ?>
            </h2>
            <div class="row">
                <?php $orari_lavoro->render(); ?>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-4">
        <div class="payment-card">
            <i class="fa fa-whatsapp payment-icon-big text-navy"></i>
            <h2>
                <?= $marketingStudioData["cellulare"]; ?>
            </h2>
            <div class="row">
                <?php $orari_lavoro->render(); ?>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-4">
        <div class="payment-card">
            <i class="fa fa-envelope payment-icon-big text-warning"></i>
            <h2>
                <a href="mailto:assistenza@marketingstudio.it">assistenza@marketingstudio.it</a>
            </h2>
            <div class="row">
                <div class="col-sm-4">
                    <small>
                        <strong class="text-navy">Attivo 24/7</strong>
                    </small>
                </div>
                <div class="col-sm-8 text-right">
                    <small>
                        Risposta entro 24h
                    </small>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkTickets = new \MSFramework\Framework\ticket();
$MSFrameworkTicketsQuotation = new \MSFramework\Framework\Ticket\quotations();

if(isset($_GET['id'])) {
    $r = $MSFrameworkTickets->getTicketQuestionDetails($_GET['id'])[$_GET['id']];
}

if(strstr($r['domain'], "saas_")) {
    $domain_info = $MSFrameworkFW->getWebsitePathsBy('id', str_replace("saas_", "", $r['domain']), true)['id'];
} else {
    $domain_info = $MSFrameworkFW->getWebsitePathsBy('customer_domain', '', strstr($r['domain'], "saas_"))['id'];
}

if(!$r || $r['domain'] != $domain_info) {
    header('Location: index.php');
    die();
}

$MSFrameworkUtils = new \MSFramework\utils();

$settings_data = $MSFrameworkFW->getCMSData('settings');
$fw_payMethods = $settings_data['payMethods'];

$author_info = array_values($r["messages"])[0]['author_info'];
$last_update = $MSFrameworkDatabase->getAssoc("SELECT answer_date FROM marke833_framework.ticket__risposte WHERE marke833_framework.ticket__risposte.question = :id ORDER BY id DESC", array(':id' => $r['id']), true);

// Se a visualizzare il ticket non è un superadmin allora resetto il contatore dei messaggi non letti
if($_SESSION['userData']['userlevel'] != 0) {
    $MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.ticket__domande SET have_unread = 0 WHERE id = :id", array(':id' => $_GET['id']));
}

$status_label = array(
    'label' => 'In attesa di risposta ' . $r['status'] . '%'
);

if($r['status'] > 0) {
    if($r['status'] == 100) {
        $status_label = array(
            'label' => 'Completato al 100%',
        );
    } else {
        $status_label = array(
            'label' => 'Completato al ' . $r['status'] . '%',
        );
    }
}

$progress_slider = '<small>' . $status_label['label'] . '</small><div class="progress progress-mini"><div style="width: ' . $r['status'] . '%;" class="progress-bar"></div></div>';

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $r['id'] ?>" />
            <input type="hidden" id="record_type" value="view" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>

                            <h2 class="title-divider">
                                <b>Ticket #<?= $r['id']; ?>:</b> <?= $r['title']; ?>
                                <span class="pull-right">
                                    <?= (new \MSFramework\Framework\ticket())->getTicketCategories()[$r['category']]['title']; ?>
                                </span>
                            </h2>

                            <div class="row">

                                <div class="col-sm-3">
                                    <label>Progresso</label>
                                    <div style="margin-bottom: 15px;"><?= $progress_slider; ?></div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Aperto il</label>
                                    <div class="form-control"><?= date("d/m/Y H:i", strtotime($r['question_date'])); ?></div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Da</label>
                                    <div class="form-control"><?= $author_info['name'] ; ?></div>
                                </div>
                                <div class="col-sm-3">
                                    <label>Ultimo aggiornamento</label>
                                    <div class="form-control"><?= date("d/m/Y H:i", strtotime(($last_update['answer_date'] ? $last_update['answer_date'] : $r['question_date']))); ?></div>
                                </div>

                            </div>

                            <h2 class="title-divider">Messaggi</h2>

                            <div style="margin: -15px -15px -30px; padding: 15px; background: #fafafa;">

                                <?php foreach($r["messages"] as $messaggio) { ?>
                                    <div class="social-feed-separated">
                                        <div class="social-avatar">

                                                <img alt="image" src="<?= $messaggio['author_info']['avatar']; ?>">

                                        </div>

                                        <div class="social-feed-box">
                                            <div class="social-avatar">
                                                <b><?= $messaggio['author_info']['name']; ?></b><br>
                                                    <small class="text-muted"><?= $MSFrameworkUtils->smartdate(strtotime($messaggio['answer_date'])); ?> - <?= date("d/m/Y H:i", strtotime($messaggio['answer_date'])); ?></small>
                                            </div>
                                            <div class="social-body">
                                                <?= $messaggio['message']; ?>

                                                <?php if(is_array($messaggio['quotation']) && count($messaggio['quotation']) > 0) { ?>
                                                    <div class="hr-line-dashed"></div>

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label>Prezzo (IVA inclusa)</label>
                                                                    <div><?= CURRENCY_SYMBOL; ?> <?= $messaggio['quotation']['data']['price'] ?></div>
                                                                </div>

                                                                <div class="col-lg-3">
                                                                    <label>Data di consegna stimata</label>
                                                                    <div>
                                                                        <?php
                                                                        if($messaggio['quotation']['state'] == "unpaid") {
                                                                            echo (new \DateTime())->modify("+ " . $messaggio['quotation']['data']['delivery_days'] . " days")->format("d/m/Y");
                                                                        } else if($messaggio['quotation']['state'] == "paid") {
                                                                            echo \DateTime::createFromFormat("d/m/Y H:i", $messaggio['quotation']['state_change_date'])->modify("+ " . $messaggio['quotation']['data']['delivery_days'] . " days")->format("d/m/Y");
                                                                        } else {
                                                                            echo "Mai";
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-4">
                                                                    <label>Metodi di pagamento accettati</label>
                                                                    <div>
                                                                        <?php
                                                                        $payMethodsString = array();
                                                                        foreach($messaggio['quotation']['data']['pay_methods'] as $paymethod) {
                                                                            $payMethodsString[] = $MSFrameworki18n->getFieldValue($fw_payMethods[$paymethod]['display_name'], true);
                                                                        }

                                                                        echo implode(", ", $payMethodsString);
                                                                        ?>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-3">
                                                                    <label>Stato Pagamento</label>
                                                                    <div><?= $MSFrameworkTicketsQuotation->formatPaymentState($messaggio['quotation']['state']) . ($messaggio['quotation']['state_change_date'] != "" ? " in data " . $messaggio['quotation']['state_change_date'] : "") ?></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12" style="text-align: center; margin-top: 20px; margin-bottom: 20px;">
                                                            <?php
                                                            if($messaggio['quotation']['state'] == "unpaid") {
                                                                ?>
                                                                <a href="<?php echo ABSOLUTE_ADMIN_MODULES_PATH_HTML ?>payway/checkout/index.php?type=ticket_quotation&id=<?= $messaggio['id']; ?>">
                                                                    <button type="button" class="btn btn-primary btn-lg">
                                                                        <div>Procedi al pagamento</div>
                                                                    </button>
                                                                </a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>

                                                    <div class="hr-line-dashed"></div>
                                                <?php } ?>

                                                <?php if($messaggio['files']) { ?>

                                                    <div class="hr-line-dashed"></div>

                                                    <div class="row" style="margin-bottom: -20px;">
                                                        <div class="col-lg-12">
                                                            <?php foreach($messaggio['files'] as $file) { ?>
                                                                <div class="file-box">
                                                                    <div class="file">
                                                                        <a download href="<?= $file['html']['main']; ?>" target="_blank">
                                                                            <span class="corner"></span>
                                                                            <?php if(@is_array(getimagesize($file['absolute']['main']))) { ?>
                                                                                <div class="image">
                                                                                    <img alt="image" class="img-responsive" src="<?= $file['html']['thumb']; ?>">
                                                                                </div>
                                                                            <?php } else { ?>
                                                                                <div class="icon">
                                                                                    <i class="fa fa-file"></i>
                                                                                </div>
                                                                            <?php } ?>
                                                                            <div class="file-name">
                                                                                <?= $file['filename']; ?>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>


                            <h2 class="title-divider">Nuovo messaggio</h2>
                            <?php if($MSFrameworkUsers->getUserDataFromSession('userlevel') != 0) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <textarea id="descrizione" name="descrizione" class="form-control required" placeholder="Spiegaci nel dettaglio quello di cui hai bisogno, cercheremo di risponderti il prima possibile" rows="4"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Allegati</label>
                                    <?php (new \MSFramework\uploads('ASSISTENZA_TICKET'))->initUploaderHTML("allegati", json_encode(array()), array(), array(), false); ?>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a id="send_ticket_message" class="btn btn-success btn-outline">Invia Messaggio</a>
                                </div>
                            </div>
                            <?php } else { ?>
                                <div class="alert alert-danger">Sei loggato come SuperAdmin!<br>Utilizza il pannello di Supporto Clienti per rispondere a questo ticket.<br><a style="margin-top: 15px;" class="btn btn-success" href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>framework360/assistenza/ticket/edit.php?id=<?= $r['id']; ?>">Clicca qui per Rispondere</a></div>
                            <?php } ?>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    </div>

    <script>

        globalInitForm();
    </script>
</body>
</html>
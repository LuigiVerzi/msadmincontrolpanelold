$(document).ready(function() {
    loadStandardDataTable('main_table_list', true);
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initOrak('allegati', 3, 50, 50, 250, [], false, [
        'image/jpeg',
        'image/png',
        'application/pdf',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'text/plain',
        'application/zip',
        'application/x-rar',
        'text/xml',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ]);

    initTinyMCE( '#descrizione');

    if($('#record_type').val() == 'edit') {
        $('#notifica_via_mail').on('ifToggled', function (event) {

            if ($(this).is(':checked')) {
                $('.notification_email_container').show();
            } else {
                $('.notification_email_container').hide();
            }
        });

        $('#categoria').on('change', function () {
            if ($(this).val() != '') {
                $('#disclaimer_box .alert').html($(this).find('option:selected').data('disclaimer'));
                $('#disclaimer_box').show();
            } else {
                $('#disclaimer_box').hide();
            }
        });
    } else {
        $('#send_ticket_message').on('click', function (e) {
            e.preventDefault();

            $.ajax({
                url: "db/sendMessage.php",
                type: "POST",
                data: {
                    "pID": $('#record_id').val(),
                    "pMessaggio": tinymce.get('descrizione').getContent(),
                    "pAllegati": composeOrakImagesToSave('allegati')
                },
                async: false,
                dataType: "json",
                success: function (data) {
                    if (data.status == "mandatory_data_missing") {
                        bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
                    } else if (data.status == "mv_error") {
                        bootbox.error("C'è stato un problema durante il salvataggio dei degli allegati. Impossibile continuare.");
                    } else if (data.status == "query_error") {
                        bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
                    } else if (data.status == "ok") {
                        location.reload();
                    }
                }
            });

        });
    }
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pCategoria": $('#categoria').val(),
            "pTitolo": $('#titolo').val(),
            "pMessaggio": tinymce.get('descrizione').getContent(),
            "pAllegati": composeOrakImagesToSave('allegati'),
            "pEmailNotifica": ($('#notifica_via_mail:checked').length ? $('#email_notifica').val() : '')
        },
        async: false,
        dataType: "json",
        success: function (data) {
            if (data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if (data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio degli allegati. Impossibile continuare.");
            } else if (data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if (data.status == "ok") {
                location.href = 'view.php?id=' + data.id;
            }
        }
    });
}
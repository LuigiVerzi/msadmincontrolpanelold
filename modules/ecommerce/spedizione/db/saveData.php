<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pPrezzo'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_var_prezzi = array('pPrezzo', 'pMinExpense', 'pMaxExpense');
foreach($array_var_prezzi as $curPostPrice) {
    $_POST[$curPostPrice] = str_replace(",", ".", $_POST[$curPostPrice]);
    if(!strstr($_POST[$curPostPrice], ".") && $_POST[$curPostPrice] != "") {
        $_POST[$curPostPrice] .= ".00";
    }

    if(!is_numeric($_POST[$curPostPrice]) && $_POST[$curPostPrice] != "") {
        echo json_encode(array("status" => "price_numeric"));
        die();
    }
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT nome, descr FROM ecommerce_shipping_methods WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pDescr'], 'oldValue' => $r_old_data['descr']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "prezzo" => $_POST['pPrezzo'],
    "descr" => $MSFrameworki18n->setFieldValue($r_old_data['descr'], $_POST['pDescr']),
    "ships_to" => $_POST['pShipAvail'],
    "sel_countries" => json_encode($_POST['pSelCountries']),
    "min_expense" => $_POST['pMinExpense'],
    "max_expense" => $_POST['pMaxExpense'],
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_shipping_methods ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $last_insert_id = $MSFrameworkDatabase->lastInsertId();
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_shipping_methods SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $last_insert_id = $_POST['pID'];
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
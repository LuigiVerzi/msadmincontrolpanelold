<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->deleteRow("ecommerce_shipping_methods", "id", $_POST['pID'])) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


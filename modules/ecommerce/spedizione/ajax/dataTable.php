<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_shipping_methods") as $r) {
    $array['data'][] = array(
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        (round($r['prezzo']) == 0) ? "Gratuito" : CURRENCY_SYMBOL . ' ' . $r['prezzo'],
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

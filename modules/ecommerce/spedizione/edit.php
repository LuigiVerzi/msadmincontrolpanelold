<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_shipping_methods WHERE id = :id", array($_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Prezzo <?= CURRENCY_SYMBOL; ?>*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Impostare '0' per gratuito"></i></span>
                                    <input id="prezzo" name="prezzo" type="text" class="form-control required" value="<?php echo htmlentities($r['prezzo']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descr']) ?>"></i></span>
                                    <input id="descrizione" name="descrizione" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['descr'])) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                        <h1>Disponibilità</h1>
                        <fieldset>
                            <h2 class="title-divider">Destinazioni disponibili</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Disponibilità metodo spedizione*</label>
                                    <select id="shipment_availability" class="form-control">
                                        <?php
                                        foreach((new \MSFramework\Ecommerce\shipping)->getCountryAvailType() as $k => $v) {
                                        ?>
                                            <option value="<?php echo $k ?>" <?php if($r['ships_to'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <?php
                                $country_availability_container_style = "";
                                if($r['ships_to'] == "" || $r['ships_to'] == "0") {
                                    $country_availability_container_style = " display: none; ";
                                }
                                ?>
                                <div class="col-sm-6" style="<?php echo $country_availability_container_style ?>" id="country_availability_container">
                                    <label>Paesi selezionati</label>
                                    <select id="country_availability" multiple="multiple" class="form-control">
                                        <?php
                                        $sel_countries = json_decode($r['sel_countries'], true);
                                        foreach((new \MSFramework\geonames)->getCountryDetails('', 'geonameId, name') as $countryK => $countryV) {
                                            ?>
                                            <option value="<?php echo $countryK ?>" <?php if(in_array($countryK, $sel_countries)) { echo "selected"; } ?>><?php echo $countryV['name'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Spesa minima <?= CURRENCY_SYMBOL; ?></label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il valore minimo del carrello affinchè questo metodo di spedizione possa essere scelto dall'utente. Impostare '0' o vuoto per nessuna spesa minima"></i></span>
                                    <input id="min_expense" name="min_expense" type="text" class="form-control" value="<?php echo htmlentities($r['min_expense']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Spesa massima <?= CURRENCY_SYMBOL; ?></label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il valore massimo del carrello affinchè questo metodo di spedizione possa essere scelto dall'utente. Impostare '0' o vuoto per nessuna spesa massima"></i></span>
                                    <input id="max_expense" name="max_expense" type="text" class="form-control" value="<?php echo htmlentities($r['max_expense']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
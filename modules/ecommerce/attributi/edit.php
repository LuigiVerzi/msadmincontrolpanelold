<?php
/**
 * MSAdminControlPanel
 * Date: 16/04/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_attributes WHERE id = :id", array($_GET['id']), true);
    if($_GET['id'] != "") {
        $r_attr_values = (new \MSFramework\Ecommerce\attributes)->getValuesOfAttributeDetails($_GET['id']);
    }
}
?>

<style>
    #automatic_colors {
        display: none;
    }

    #tipologia.colori {
        width: calc(100% - 50px);
        float: left;
    }

    #tipologia.colori +#automatic_colors {
        display: inline-block;
        float: right;
    }


    #colors_modal .colors_list_check {
    }

    #colors_modal .colors_list_check button {
        margin: 0 5px 5px 0;
        border-width: 3px;
        color: black;
        box-shadow: none;
    }

    #colors_modal .colors_list_check button:not(.active) {
        background: white !important;
        opacity: 0.3;
    }

    #colors_modal .colors_list_check button.active {
        font-weight: 500;
    }
    #colors_modal .colors_list_check button.active.dark {
        color: black !important;
    }
    #colors_modal .colors_list_check button.active.light {
        color: white !important;
    }
</style>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome Attributo*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Tipologia Attributi</label>
                                    <select id="tipologia" name="tipologia" class="form-control <?= $r['show_mode']; ?>">
                                        <option value="">Standard</option>
                                        <option value="colori" <?= ($r['show_mode'] == 'colori' ? 'selected' : ''); ?>>Mostra Colori</option>
                                        <option value="foto" <?= ($r['show_mode'] == 'foto' ? 'selected' : ''); ?>>Mostra Foto</option>
                                        <option value="brand" <?= ($r['show_mode'] == 'brand' ? 'selected' : ''); ?>>Mostra Brand</option>
                                    </select>
                                    <a href="#" id="automatic_colors" class="btn btn-default" data-toggle="modal" data-target="#colors_modal">
                                        <i class="fa fa-magic" aria-hidden="true"></i>
                                    </a>
                                </div>


                                <?php
                                $main_attribute_check = "checked";
                                if($r['is_main'] == 0) {
                                    $main_attribute_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selezionando questo campo l'attributo sarà visualizzato nella scheda principale della modifica del prodotto come un campo principale."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="show_main_tab" <?php echo $main_attribute_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Mostra nella scheda Principale</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $is_variant_check = "";
                                if($r['is_variant'] == 1) {
                                    $is_variant_check = "checked";
                                }
                                ?>
                                <div class="col-sm-2">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questo campo sarà possibile combinare il seguente attributo con altri durante la creazione del prodotto per creare delle variazioni"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_variant" <?php echo $is_variant_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Usa nelle variazioni</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $is_multiple_check = "";
                                if($r['is_multiple'] == 1) {
                                    $is_multiple_check = "checked";
                                }
                                ?>
                                <div class="col-sm-2" id="is_multiple_box" style="display: <?= (empty($is_variant_check) ? 'block' : 'none'); ?>;">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questo campo sarà possibile inserire valori multipli su questo attributo"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_multiple" <?php echo $is_multiple_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Permetti valori multipli</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $is_filter_check = "";
                                if($r['is_filter'] == 1) {
                                    $is_filter_check = "checked";
                                }
                                ?>
                                <div class="col-sm-2">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questo campo i clienti potranno filtrare i prodotti in base a questo attributo"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_filter" <?php echo $is_filter_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Usa nei filtri</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $show_in_product_check = "checked";
                                if($r['show_in_product'] == 0 && $_GET['id'] != "") {
                                    $show_in_product_check = "";
                                }
                                ?>
                                <div class="col-sm-2">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imposta se questo attributo dovrà essere visibile nella scheda del prodotto"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="show_in_product" <?php echo $show_in_product_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Mostra nel prodotto</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="easyRowDuplicationContainer">
                                <?php
                                if(count($r_attr_values) == 0) {
                                    $r_attr_values[] = array();
                                }

                                foreach($r_attr_values as $dataK => $dataV) {
                                    ?>
                                    <div class="stepsDuplication rowDuplication attrDuplication">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <label>Valore Attributo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($dataV['nome']) ?>"></i></span>
                                                <input type="hidden" class="data_id" value="<?php echo $dataV['id'] ?>" />
                                                <input type="text" class="form-control data_val" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($dataV['nome'])) ?>">
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="type_container">
                                                    <div class="attr_type attr_type_colori" style="display: none;">
                                                        <label>Codice Colore</label>
                                                        <div class="input-group colorpicker-component">
                                                            <span class="input-group-addon"><i></i></span>
                                                            <input class="form-control data_color" value="<?php echo htmlentities($dataV['color_hex']) ?>">
                                                        </div>
                                                    </div>
                                                    <div class="attr_type attr_type_foto attr_type_brand" style="display: none;">
                                                        <label>Immagine Attributo</label>
                                                        <?php (new \MSFramework\uploads('ECOMMERCE_ATTRIBUTES'))->initUploaderHTML("image_" . $dataK, $dataV['images'], array("realPath" => "image_realPath", "prevUploaded" => "prev_upl_image", "element" => "image", "container" => "image_container"));  ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<div class="modal" id="colors_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Autocompilazione Colori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Seleziona tra i seguenti colori quelli che desideri aggiungere alla lista. Potrai successivamente modificarli.</p>
                <button class="btn btn-default btn-sm pull-right select_all_colors">Seleziona/Deseleziona Tutto</button>
                <div style="clear: both;"></div>
                <h2 class="title-divider">Colori Principali</h2>
                <div class="colors_list_check">
                    <?php
                    $used_colors = array();
                    foreach(json_decode(file_get_contents('ajax/colorsMain.json'), true) as $nome => $hex) {
                        echo '<button data-toggle="button" class="btn btn-primary btn-outline ' . ((new \MSFramework\utils())->getHexLightness($hex) > 50 ? 'dark' : 'light') . ' active" type="button" data-hex="' . $hex . '" style="background-color: ' . $hex . '; border-color: ' . $hex . ';">' . $nome . '</button>';
                        $used_colors[] = $hex;
                    }
                    ?>

                    <h2 class="title-divider">Colori Secondari</h2>
                    <?php
                    foreach(json_decode(file_get_contents('ajax/colorsList.json'), true) as $nome => $hex) {
                        if(in_array($hex, $used_colors)) continue;
                        echo '<button data-toggle="button" class="btn btn-primary btn-outline ' . ((new \MSFramework\utils())->getHexLightness($hex) > 50 ? 'dark' : 'light') . '" type="button" data-hex="' . $hex . '" style="background-color: ' . $hex . '; border-color: ' . $hex . ';">' . $nome . '</button>';
                    }
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="autocompile_colors">Inserisci Colori</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
            </div>
        </div>
    </div>
</div>

<script>
    window.colorsList = <? include('ajax/colorsList.json'); ?>;
    
    globalInitForm();
</script>
</body>
</html>
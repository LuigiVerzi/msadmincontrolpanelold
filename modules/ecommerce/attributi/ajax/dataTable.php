<?php
/**
 * MSAdminControlPanel
 * Date: 16/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_attributes") as $r) {

    $array['data'][] = array(
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        ($r['is_variant'] == "1") ? "Si" : "No",
        ($r['is_filter'] == "1") ? "Si" : "No",
        ($r['show_in_product'] == "1") ? "Si" : "No",
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initImages();
    initColorPicker();
    $('.rowDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi attributo",
        "deleteButtonText": "Elimina attributo",
        "afterAddCallback": function() {

            var $row = $('.rowDuplication:last');

            $row.find('.data_val').val('');
            $row.find('.data_id').val('');

            $row.find('.prev_upl_image').html('');
            $row.find('.image_container .clone_item').remove();
            $row.find('.image_container [id$="DDArea"]').remove();
            $row.find('.image_container').append('<div id="image_x" class="image" orakuploader="on"></div>');

            $row.find('.colorpicker-element').removeClass('colorpicker-element');
            $row.find('.data_color').val('');

            $row.find('.attr_type').hide();
            if($('#tipologia').val() != "") {
                $row.find('.attr_type_' + $('#tipologia').val()).show();
            }

            initImages();
            initColorPicker();
        },
        "afterDeleteCallback": function() {
        },
    });

    $('#tipologia').on('change', function () {
        var value = $(this).val();
        var $box = $('.stepsDuplication .attr_type_' + value);

        $(this).removeClass('foto colori');
        if(value != '') {
            $(this).addClass(value);
        }

        $('.stepsDuplication .attr_type').hide();

        if($box.length) {
            $box.show();
        }
    });
    if($('#tipologia').val() != '') {
        $('#tipologia').change();
    }

    colorMatch = FuzzySet();
    Object.keys(colorsList).forEach(function(name) {
        colorMatch.add(name);
    });

    $("form").on('keypress change', '.data_val', function () {
        if($('#tipologia').val() == 'colori')
        {
            var colore = $(this).val();
            var match = colorMatch.get(colore);

            if(match.length) {
                $(this).closest(".stepsDuplication").find('.colorpicker-component').colorpicker('setValue', colorsList[match[0][1]]);
                $(this).closest(".stepsDuplication").find('.data_color').val(colorsList[match[0][1]]);
            }
        }
    });

    $('#colors_modal .select_all_colors').on('click', function () {
        if($('#colors_modal .colors_list_check button.active').length == $('#colors_modal .colors_list_check button').length) {
            $('#colors_modal .colors_list_check button').removeClass('active');
        }
        else {
            $('#colors_modal .colors_list_check button').addClass('active');
        }
    });

    $('#autocompile_colors').on('click', function () {
       var active_colors = {};

       $('#colors_modal .colors_list_check button.active').each(function () {
           var name = $(this).text();
           var hex = $(this).data('hex');

           var already_exist = false;
           $('.rowDuplication').each(function () {
              if($(this).find('.data_color').val() == hex) {
                  already_exist = true;
              }
           });

           if(!already_exist) {
               if ($('.rowDuplication').last().find('.data_val').val() != '' && $('.rowDuplication').last().find('.data_color').val() != '') {
                   $('.easyRowDuplicationFooter .easyRowDuplicationAddBtn').click();
               }

               var $row = $('.rowDuplication').last();
               $row.find('.data_val').val(name);
               $row.find('.colorpicker-component').colorpicker('setValue', hex);
               $row.find('.data_color').val(hex);
           }
       });

        $('#colors_modal').modal('hide');

    });

    $("#is_variant").on('ifToggled', function () {
        if ($(this).is(':checked')) {
            $('#is_multiple_box').hide();
        } else {
            $('#is_multiple_box').show();
        }
    });

}

function initImages() {
    $('.rowDuplication').each(function(index) {
        if($('.image_container:eq(' + index + ') #image_' + index + 'DDArea').length != 0) {
            return true;
        }

        $('.image_container:eq(' + index + ') .image').attr('id', 'image_' + index);
        $('.image_container:eq(' + index + ') .image_realPath').attr('id', 'image_' + index + "_realPath");
        $('.image_container:eq(' + index + ') .image_absPath').attr('id', 'image_' + index + "_absPath");

        preattach_array_image = new Array();
        if ($('.prev_upl_image:eq(' + index + ')').val() != "") {
            img_to_cycle = $.parseJSON($('.prev_upl_image:eq(' + index + ')').val()) || new Array();
            $(img_to_cycle).each(function (k, v) {
                preattach_array_image.push(v);
            })
        }

        initOrak('image_' + index, 1, 100, 100, 150, preattach_array_image, false, ['image/jpeg', 'image/png', 'image/svg+xml']);
    })
}

function initColorPicker() {
    $('.rowDuplication').each(function(index) {
        if($(this).find('.colorpicker-element').length) {
            return true;
        }
        $(this).find('.colorpicker-component').colorpicker({align:'right'});
    })
}

function moduleSaveFunction() {
    data_ary = new Object();
    $('.stepsDuplication.attrDuplication').each(function(row_index) {
        data_ary_tmp = new Array();

        data_val = $(this).find('.data_val').val();
        if(data_val == "") {
            return true;
        }

        data_ary_tmp.push($(this).find('.data_id').val());
        data_ary_tmp.push(data_val);

        if($('#tipologia').val() == 'colori') {
            var data_color = $(this).find('.data_color').val();
            data_ary_tmp.push(data_color);
        }
        else if($('#tipologia').val() == 'foto' || $('#tipologia').val() == 'brand') {
            var images_ary = new Array();
            $(this).find('input[name^=image]').each(function(k, v) {
                images_ary.push($(this).val());
            });
            data_ary_tmp.push(images_ary);
        }

        data_ary[row_index] = data_ary_tmp;
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pShowMode": $('#tipologia').val(),
            "pIsVariant": $('#is_variant:checked').length,
            "pIsMultiple": $('#is_multiple:checked').length,
            "pIsFilter": $('#is_filter:checked').length,
            "pShowInProduct": $('#show_in_product:checked').length,
            "pIsMainAttr": $('#show_main_tab:checked').length,
            "pAttributesData": data_ary,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
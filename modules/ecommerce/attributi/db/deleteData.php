<?php
/**
 * MSAdminControlPanel
 * Date: 16/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if(!is_array($_POST['pID'])) {
    $_POST['pID'] = array($_POST['pID']);
}

$images_to_delete = array();
foreach($_POST['pID'] as $single_attr_id) {

    //rimuovo i riferimenti all'attributo eliminato anche all'interno dei prodotti (attributi e variazioni)
    foreach((new \MSFramework\Ecommerce\products())->getProductsByAttributes($single_attr_id, "id, attributes, variants") as $r_prod) {
        $attributes = json_decode($r_prod['attributes'], true);
        $variants = json_decode($r_prod['variants'], true);

        foreach($attributes as $attributeK => $attributeV) {
            if($single_attr_id == (new \MSFramework\Ecommerce\attributes())->jsonPrefix('attribute', "remove", $attributeV[0])) {
                unset($attributes[$attributeK]);
            }
        }
        $attributes = array_values($attributes);

        foreach($variants as $variantK => $variantV) {
            if(array_key_exists($single_attr_id, $variants[$variantK]['type'])) {
                unset($variants[$variantK]);
            }
        }
        $variants = array_values($variants);


        $array_to_save = array(
            "attributes" => json_encode($attributes),
            "variants" => json_encode($variants),
        );
        $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'update');
        $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_products SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $r_prod['id']), $stringForDB[0]));
    }

    foreach((new \MSFramework\Ecommerce\attributes)->getValuesOfAttributeDetails($single_attr_id) as $old_group) {
        $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($old_group['images'], true));
    }

}


$r_old_attr_data = (new \MSFramework\Ecommerce\attributes)->getValuesOfAttributeDetails($_POST['pID']);

if($MSFrameworkDatabase->deleteRow("ecommerce_attributes", "id", $_POST['pID'])) {
    $MSFrameworkDatabase->deleteRow("ecommerce_attributes_values", "child_of", $_POST['pID']);

    (new \MSFramework\uploads('ECOMMERCE_ATTRIBUTES'))->unlink($images_to_delete);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


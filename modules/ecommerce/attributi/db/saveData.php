<?php
/**
 * MSAdminControlPanel
 * Date: 16/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT nome FROM ecommerce_attributes WHERE id = :id", array(":id" => $_POST['pID']), true);
    $r_old_attr_data = (new \MSFramework\Ecommerce\attributes)->getValuesOfAttributeDetails($_POST['pID']);

    foreach($r_old_attr_data as $old_group) {
        foreach(json_decode($old_group['images'], true) as $old_img) {
            $old_img_variations[] = $old_img;
        }
    }
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
    array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
)) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "show_mode" => $_POST['pShowMode'],
    "is_variant" => $_POST['pIsVariant'],
    "is_multiple" => $_POST['pIsMultiple'],
    "is_filter" => $_POST['pIsFilter'],
    "show_in_product" => $_POST['pShowInProduct'],
    "is_main" => $_POST['pIsMainAttr']
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_attributes ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $last_insert_id = $MSFrameworkDatabase->lastInsertId();
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_attributes SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $last_insert_id = $_POST['pID'];
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$old_attr_ids = array_column($r_old_attr_data, 'id');
if(!is_array($old_attr_ids)) {
    $old_attr_ids = array();
}

$ary_files_variations = array();
$uploaderVariants = new \MSFramework\uploads('ECOMMERCE_ATTRIBUTES');

foreach($_POST['pAttributesData'] as $kAttr => $vAttr) {
    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $vAttr[1], 'oldValue' => $r_old_attr_data[$vAttr[0]]['nome']),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $cur_files = array();
    $color_hex = "";

    if($_POST['pShowMode'] == 'foto' || $_POST['pShowMode'] == 'brand') {

        $cur_files = $uploaderVariants->prepareForSave($vAttr[2], array("png", "jpeg", "jpg", "svg"));
        if ($cur_files === false) {
            echo json_encode(array("status" => "mv_error"));
            die();
        }

        $ary_files_variations = array_merge($ary_files_variations, $cur_files);
    }
    else if($_POST['pShowMode'] == 'colori') {
        $color_hex = $vAttr[2];
    }

    $array_to_save = array(
        "nome" => $MSFrameworki18n->setFieldValue($r_old_attr_data[$vAttr[0]]['nome'], $vAttr[1]),
        "color_hex" => $color_hex,
        "images" => json_encode($cur_files),
        "child_of" => $last_insert_id,
    );

    if(in_array($vAttr[0], array_column($r_old_attr_data, 'id'))) {
        $envolved_attr_ids[] = $vAttr[0];
        $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'update');
        $result = $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_attributes_values SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $vAttr[0]), $stringForDB[0]));
    } else {
        $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_attributes_values ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    }
}

$env_del_ids = array_diff($old_attr_ids, $envolved_attr_ids);
if(count($env_del_ids) != 0) {
    $r_to_del = $MSFrameworkDatabase->composeSameDBFieldString($env_del_ids, "OR", "id");
    $MSFrameworkDatabase->query("DELETE FROM ecommerce_attributes_values WHERE " . $r_to_del[0], $r_to_del[1]);
}

if($db_action == "update") {
    $uploaderVariants->unlink($ary_files_variations, $old_img_variations);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $last_insert_id : '')));
die();
?>
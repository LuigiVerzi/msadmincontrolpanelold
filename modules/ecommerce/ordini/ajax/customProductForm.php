<?php

include('../../../../sw-config.php');

$MSFrameworkImposte = new \MSFramework\Ecommerce\imposte();
?>

<form id="customProductForm">
    <div class="row">
        <div class="col-lg-12">
            <label>Nome prodotto</label>
            <input id="product_name" type="text" class="form-control required" placeholder="Il nome del prodotto" value="">
        </div>

        <div class="col-lg-8">
            <label>Prezzo</label>
            <input id="product_price" type="number" class="form-control required" placeholder="Prezzo singolo prodotto" value="">
        </div>

        <div class="col-lg-4">
            <label>Quantità</label>
            <input id="product_quantity" type="number" class="form-control required" min="1" value="1" placeholder="1">
        </div>

        <div class="col-lg-12">
            <label>Imposta</label>
            <select id="product_vat" class="form-control">
                <option value="default" selected>Usa impostazioni Predefinite</option>
                <option value="0">- Nessuna imposta -</option>
                <?php
                $getImposte = $MSFrameworkImposte->getImposte();
                $num_imp = count($getImposte);
                foreach($getImposte as $impK => $impV) { ?>
                    <option value="<?php echo $impK ?>"><?php echo $MSFrameworki18n->getFieldValue($impV[0], true) ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-lg-12">
            Inserisci i prezzi <b><?php echo ($MSFrameworkImposte->getPriceType() == "0") ? "comprensivi delle imposte" : "al netto delle imposte" ?></b>. Puoi modificare quest'opzione all'interno delle impostazioni
        </div>

    </div>
</form>

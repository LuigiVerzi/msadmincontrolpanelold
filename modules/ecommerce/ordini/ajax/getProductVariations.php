<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$products = new \MSFramework\Ecommerce\products();

$type = $_POST['type'];
$product_id = (int)$_POST['id'];
$product = $products->getProductDetails($product_id)[$product_id];

if($type == 'html_select') {
    $html = '';
    $i = 0;
    foreach($product["formattedAttributes"]['variations'] as $k => $v) {
        if($i > 0) $html .= '<br>';
        $html .= '<label>' . $MSFrameworki18n->getFieldValue($v['nome']) . '</label><select class="form-control variation" data-name="' . $MSFrameworki18n->getFieldValue($v['nome']) . '" data-id="' . $k . '">';
        foreach($v['value'] as $o_id => $o_v) {
            $html .= '<option value="' . $o_id . '">' . $MSFrameworki18n->getFieldValue($o_v) . '</option>';
        }
        $html .= '</select>';
        $i++;
    }
    $return = $html;
}
else if($type == 'variation_detail') {
    $variations = $_POST['variations'];
    $array_to_return = $products->getProductVariationInfo($product_id, $variations);
    $return = json_encode($array_to_return);
}

die($return);

?>

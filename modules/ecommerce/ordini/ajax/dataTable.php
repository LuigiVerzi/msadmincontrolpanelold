<?php
/**
 * MSAdminControlPanel
ecommerce_orders
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT *, (SELECT prezzo FROM ecommerce_shipping_methods WHERE ecommerce_shipping_methods.id = ecommerce_orders.tipo_spedizione) as prezzo_spedizione FROM ecommerce_orders") as $r) {
    $info_fatturazione = json_decode($r['info_fatturazione'], true);
    $cart = json_decode($r['cart'], true);

    $orde_status_color = array(
        "0" => "rgba(244,67,54,1)",
        "1" => "rgba(255,152,0,1)",
        "2" => "rgba(33,150,243,1)",
        "3" => "rgba(139,195,74,1)",
        "4" => "rgba(78,70,69,1)",
        "5" => "rgba(255, 195, 53,1)",
    );


    $array['data'][] = array(
        $r['id'],
        $info_fatturazione['nome'] . " " . $info_fatturazione['cognome'],
        number_format($cart['coupon_price']+(float)$r['prezzo_spedizione'], 2) . CURRENCY_SYMBOL,
        (new DateTime($r['order_date']))->format("d/m/Y H:i"),
        (new DateTime($r['order_edit_date']))->format("d/m/Y H:i"),
        '<span class="label" style="color: white; background: ' . $orde_status_color[(string)$r['order_status']] . ';">' . (new \MSFramework\Ecommerce\orders)->getStatus((string)$r['order_status']) . '</span>',
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

<?php
$returns = array();

if(!defined('SW_VERSION')) {
    include('../../../../sw-config.php');

    $cartClass = new \MSFramework\Ecommerce\cart();
    $cartClass->customerID = $_POST['customer_id'];
    $cartClass->isAdminManaged = true;

    $cart = $_POST['cart'];
    $coupons_used = $_POST['coupons'];
    $id_spedizione = $_POST['id_spedizione'];
    $metodo_pagamento = $_POST['metodo_pagamento'];

    $origin_cart = $cartClass->getCart(array(), $coupons_used);
    $cartClass->syncCart(array());

    foreach($cart as $product) {
        if($product['is_custom'] == "true" || $product['is_custom'] == "1") {
            if(!in_array($product['id'], array("rincaro_spedizione", "rincaro_pagamento"))) {
                if ($product['custom_product']) {
                    $cartClass->customAddToCart($product['custom_product']['cart_unique_id'], $product['custom_product']['nome'], $product['custom_product']['prezzo'], $product['custom_product']['quantity'], $product['custom_product']['imposta']);
                } else {
                    $return = $cartClass->customAddToCart($product['product_id'], $product['nome'], $product['prezzo'], $product['quantity'], $product['imposta']);
                }
            }
        } else {
            $returns[] = $cartClass->addToCart($product['id'], $product['quantity'], (is_array($product['variations']) ? $product['variations'] : array()));
        }
    }

    if($metodo_pagamento && (float)$metodo_pagamento['rincaro'] > 0 ) {
        $cartClass->customAddToCart('rincaro_pagamento', $MSFrameworki18n->getFieldValue($metodo_pagamento['descrizione']), (float)$metodo_pagamento['rincaro'],1, $metodo_pagamento['aliquota_rincaro']);
    }

    if($id_spedizione) {
        $tipo_spedizione = (new \MSFramework\Ecommerce\shipping())->getShippingDetails($id_spedizione)[$id_spedizione];
        if ((float)$tipo_spedizione['prezzo'] > 0) {
            $cartClass->customAddToCart('rincaro_spedizione', $MSFrameworki18n->getFieldValue($tipo_spedizione['nome']), (float)$tipo_spedizione['prezzo'], 1, 0);
        }
    }

    $cart = $cartClass->getCart(array(), $coupons_used);

    foreach($returns as $return) {
        if($return['status'] === 'no_avaialable') {
            $cart = $origin_cart;
            $cartClass->syncCart($cart['products']);
        }
    }

} else {

    $cartClass = new \MSFramework\Ecommerce\cart();
    $cartClass->customerID = $r['user_id'];
    $cartClass->isAdminManaged = true;

    $origin_cart = $cartClass->getCart(array(), $coupons_used);

    $cartClass->syncCart($cart['products']);

    if((float)$metodo_pagamento['rincaro'] > 0) {
        $cartClass->customAddToCart('rincaro_pagamento', $MSFrameworki18n->getFieldValue($metodo_pagamento['descrizione']), (float)$metodo_pagamento['rincaro'],1, $metodo_pagamento['aliquota_rincaro']);
    }

    if($r['tipo_spedizione']) {
        $tipo_spedizione = (new \MSFramework\Ecommerce\shipping())->getShippingDetails($r['tipo_spedizione'])[$r['tipo_spedizione']];

        if ((float)$tipo_spedizione['prezzo'] > 0) {
            $cartClass->customAddToCart('rincaro_spedizione', $MSFrameworki18n->getFieldValue($tipo_spedizione['nome']), (float)$tipo_spedizione['prezzo'], 1, 0);
        }
    }

    $cart = $cartClass->getCart(array(), $coupons_used);
}

$cartClass->syncCart($origin_cart['products']);

?>
<?php if($cart['products']) { ?>
    <div class="table-responsive">
        <table class="table table-bordered">
            <tbody>
            <?php foreach($cart['products'] as $product) { ?>

                <tr>
                    <td width="90">
                        <?php if(!in_array($product['product_id'], array("rincaro_pagamento"))) { ?>
                            <textarea class="product_json" style="display: none;"><?= json_encode(array('id' => $product['product_id'], 'variations' => $product['variations'], 'quantity' => $product['quantity'], 'is_custom' => (isset($product['is_custom']) ? $product['is_custom'] : false), 'custom_product' => ((isset($product['is_custom']) ? $product : array())))); ?></textarea>
                            <?php if($product['image']) { ?>
                                <img src="<?php echo UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML . $product['image'] ?>" width="80" />
                            <?php } ?>
                        <?php } ?>
                    </td>
                    <td class="desc">
                        <a href="#" class="btn btn-danger btn-outline btn-sm pull-right deleteFromCart only-edit-container"><i class="fa fa-trash"></i> Rimuovi</a>
                        <h3>
                            <?php echo $product['nome'] ?>
                        </h3>
                        <?php
                        if(!empty($product['product_code'])) {
                            echo '<p><b>SKU:</b> ' . $product['product_code'] . '</p>';
                        }

                        foreach($product['formattedVariation'] as $k=>$variation) {
                            $product['formattedVariation'][$k] = '<b>' . $MSFrameworki18n->getFieldValue($k) . '</b>: ' . $MSFrameworki18n->getFieldValue($variation);
                        }
                        if(!empty($product['custom'])) $product['formattedVariation'][] = '<b>Campo Personalizzato</b>: ' . $product['custom'];

                        echo implode(", ", $product['formattedVariation']);
                        ?>
                    </td>

                    <td style="vertical-align: middle;" class="text-center">
                        <?= CURRENCY_SYMBOL; ?> <?php echo implode(".", $product['prezzo']['tax']) ?>
                    </td>
                    <td width="65" style="vertical-align: middle;">
                        <input type="text" class="form-control quantity_input" placeholder="" value="<?= $product['quantity']; ?>">
                    </td>
                    <td style="vertical-align: middle;" class="text-center">
                        <?= CURRENCY_SYMBOL; ?> <?php echo implode(".", $product['prezzo']['tax'])*$product['quantity']; ?>
                    </td>
                </tr>
            <?php } ?>

            <?php if($cart["coupon"]) { ?>
                <?php foreach($cart["coupon"] as $coupon_name => $coupon_value) { ?>
                    <tr>
                        <td width="90">
                            <textarea class="coupon_name" style="display: none;"><?= $coupon_name; ?></textarea>
                        </td>
                        <td class="desc" style="vertical-align: middle;">
                            <a href="#" class="btn btn-danger btn-outline btn-sm pull-right deleteFromCart only-edit-container"><i class="fa fa-trash"></i> Rimuovi</a>
                            <b>Coupon:</b><br><?php echo $coupon_name ?>

                            <?php if((int)$coupon_value['sale_value'] === 0) { ?>
                                <div class="alert alert-warning" style="margin: 15px 0 0;">
                                    Il coupon non è stato applicato!
                                    <br>
                                    Probabilmente a causa di qualche limitazione del coupon stesso. (Limite di utilizzi per utente, limitazione sui prodotti o altro).
                                    <br>
                                    Se ti interessa applicare comunque un coupon puoi <a href="#" class="createCoupon">crearne uno nuovo</a>.
                                </div>
                            <?php } ?>

                        </td>

                        <td colspan="3" style="vertical-align: middle;" class="text-center">
                            - <?= CURRENCY_SYMBOL; ?> <?php echo number_format($coupon_value['sale_value'], 2) ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>

            </tbody>
        </table>
    </div>

    <?php
    $coupon_where = '';
    if($cart['coupon']) {
        $coupon_where = 'WHERE code NOT IN ("' . implode('","', array_keys($cart['coupon'])) . '")';
    }

    $avaialable_coupons = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_coupon $coupon_where");

    ?>
    <div class="row text-right">
        <div class="col-sm-3 text-left">
            <div class="only-edit-container">
                <label>Aggiungi</label><br>
                <a href="#" class="btn btn-default addCustomProduct" style="margin: 0 5px 5px 0;" ><i class="fa fa-cubes"></i> Prodotto personalizzato</a>
                <?php foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_coupon $coupon_where") as $coupon) { ?>
                    <a href="#" class="btn btn-default apply_coupon" style="margin: 0 5px 5px 0;" data-code="<?= $coupon['code']; ?>"><i class="fa fa-ticket"></i> <?= $coupon['code']; ?></a>
                <?php } ?>
                <a href="#" class="btn btn-default createCoupon"  style="margin: 0 5px 5px 0;" ><i class="fa fa-ticket"></i> Crea nuovo coupon</a>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 gray-bg">
                <span>Totale (imposte escluse)</span>
                <h2><?= CURRENCY_SYMBOL; ?> <?php echo number_format($cart['total_price']['no_tax'], 2) ?></h2>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 gray-bg">
                <span>Totale (imposte incluse)</span>
                <h2><?= CURRENCY_SYMBOL; ?> <?php echo number_format($cart['total_price']['tax'], 2) ?></h2>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 navy-bg">
                <span>Totale (al netto dei coupon)</span>
                <h2 class="font-bold"><?= CURRENCY_SYMBOL; ?> <?php echo number_format($cart['coupon_price'], 2) ?></h2>
            </div>
        </div>
    </div>

    <?php
    foreach($cart['products'] as $k => $single_cart) {
        if($single_cart['product_id'] === 'rincaro_spedizione') {
            unset($cart['products'][$k]);
        }
    }
    ?>

    <textarea id="full_cart_json" style="display: none;"><?= json_encode($cart); ?></textarea>

<?php } else { ?>
    <div class="alert alert-info">Il carrello è attualmente vuoto, aggiungi uno o più prodotti tramite la scheda in basso.</div>
<?php } ?>

<?php
foreach($returns as $return) {
    if($return['status'] === 'no_avaialable') {
        echo "<script>toastr['error']('La quantità del prodotto è esaurita.');</script>";
    }
}
?>
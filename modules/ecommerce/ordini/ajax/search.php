<?php
/**
 * MSAdminControlPanel
 * Date: 31/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$query = $_POST['query'];

$products_data = array();

foreach((new \MSFramework\Ecommerce\products())->getProductsList(array('query' => $query))['products'] as $prodotto) {
    $products_data[] = array(
        'data' => array(
            'id' => $prodotto['id'],
            'type' => 'prodotto' . ($prodotto["formattedAttributes"]['variations'] ? '_variabile' : ''),
            'price' => (float)(count($prodotto['prezzo_promo']['no_tax']) ? $prodotto['prezzo_promo']['no_tax'][0] . '.' . $prodotto['prezzo_promo']['no_tax'][1] : $prodotto['prezzo']['no_tax'][0] . '.' . $prodotto['prezzo']['no_tax'][1]),
            'imposta' => floatval($prodotto['imposta'])
        ),
        'image' => UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML . 'tn/' . json_decode($prodotto['gallery'], true)[0],
        'name' =>  $MSFrameworki18n->getFieldValue($prodotto['nome']),
        'category' =>  $MSFrameworki18n->getFieldValue($prodotto['category_name']),
        'price' => ($prodotto["formattedAttributes"]['variations'] ? 'Più variazioni disponibili' : (count($prodotto['prezzo_promo']['tax']) ? $prodotto['prezzo_promo']['tax'][0] . ',' . $prodotto['prezzo_promo']['tax'][1] : $prodotto['prezzo']['tax'][0] . ',' . $prodotto['prezzo']['tax'][1]) . CURRENCY_SYMBOL)
    );
}
?>

<?php foreach($products_data as $product) { ?>

    <?php
    $data_to_append = array();
    foreach($product['data'] as $data_k => $data_v) $data_to_append[] = 'data-' . $data_k . '="' . $data_v . '"';
    $data_to_append = implode(' ', $data_to_append);
    ?>

    <div class="col-lg-4 col-sm-6">
        <a class="btn btn-block btn-white add_to_cart" <?= $data_to_append; ?>>
            <img class="icon" onerror="iconError(this);" src="<?= $product['image']; ?>">
            <div class="details_container">
                <span class="name"><?= $product['name']; ?></span>
                <span class="category"><?= $product['category']; ?></span>
                <span class="price"><?= $product['price']; ?></span>
            </div>
        </a>
    </div>
<?php } ?>
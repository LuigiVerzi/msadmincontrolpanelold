$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $(document).ready(function () {

        $('#order_status').on('change', function() {
            if($(this).val() == "2") {
                $('#tracking_data_box').show();
                $('#courier').trigger('change');
            } else {
                $('#tracking_data_box').hide();
                $('#courier').val('0');
                $('#courier_custom_name').val('');
                $('#tracking_code').val('');
            }
        });
        $('#courier').on('change', function() {
            if($(this).val() == "0") {
                $('#courier_custom_name_block').show();
            } else {
                $('#courier_custom_name_block').hide();
                $('#courier_custom_name').val('');
            }
        });

        $('.copy_order_info').on('click', function (e) {
            e.preventDefault();

            var mainDiv = $(this).closest('[class*="col-"]').find('.address_to_copy')[0];
            var startNode = mainDiv;
            var endNode = mainDiv.lastChild;

            var range = document.createRange();
            range.setStart(startNode, 0);
            range.setEnd(endNode, 1);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);

            document.execCommand("copy");
            if (window.getSelection) {
                if (window.getSelection().empty) {
                    window.getSelection().empty();
                } else if (window.getSelection().removeAllRanges) {
                    window.getSelection().removeAllRanges();
                }
            } else if (document.selection) {
                document.selection.empty();
            }
            document.activeElement.blur();

            toastr["info"]('L\'indirizzo è stato copiato correttamente negli appunti', 'Copiato negli appunti');
        });
        $('#order_status').trigger('change');

        $('#editOrderBtn').on('click', function (e) {
            e.preventDefault();
            $('form.only-view').removeClass('only-view');
            $('#printOrderBtn').hide();
            $('#editOrderBtn').hide();
            toastr['warning']('È possibile modificare i dati dell\'ordine.', 'Sei entrato in modalità modifica')
        });

        $('.selectCustomer').on('click', function (e) {
            e.preventDefault();

            showFastPicker('manage_customers', function (row) {

                $('#customerInfo .data').html(row.DT_FormattedData).show();
                $('#customerInfo #customer_id').val(row.DT_RowId);

                $('.changeCustomer').html('Cambia').css('float', 'right');
                $('.editCustomer').show().css('float', 'right');

                updateCustomerAddresses(row.DT_RowId);

                toastr['success']('Cliente selezionato con successo');
                $('#fastModulePicker').modal('hide');
            }, 'cliente');

        });

        $('.editCustomer').on('click', function (e) {
            e.preventDefault();

            showFastEditor('manage_customers', $('#customer_id').val(), function (results) {

                $('#customerInfo .data h2').html(results.form_data.nome + ' ' + results.form_data.cognome);
                $('#customerInfo .data h2').html(results.form_data.nome + ' ' + results.form_data.cognome);


                updateCustomerAddresses($('#customer_id').val());

                toastr['success']('Cliente aggiornato con successo');
                $('#fastModuleEditor').modal('hide');
            }, 'cliente');

        });

        $('#spedizione, #pagamento').on('change', function (e) {
            e.preventDefault();

            $('#metodo_' + $(this).attr('id')).text($(this).find('option:selected').text());

            calculateTableSummary();
        });

        $('.filter_tables').on('input keyup', function () {
            var value = $(this).val();
            var $search_container = $(this).parent().find('.search-container');
            searchInTable($search_container, value);
        }).trigger('keyup');

        $('#articoli_e_servizi').on('click', '.add_to_cart', function (e) {
            e.preventDefault();

            var name = $(this).find('.name').text();
            var data = $(this).data();

            if (data.type == 'prodotto_variabile') {
                composeVariationModal(data.id, name);
            } else {
                addArticleToCart(data.id, {}, 1);
            }

        });

        $('#selectVariationModal').on('click', '.add_variation', function (e) {
            e.preventDefault();
            addProductVariationToCart();
        });

        $('#cart_container').on('click', '.deleteFromCart', function (e) {
            e.preventDefault();
            $(this).closest('tr').remove();
            calculateTableSummary();
        });

        $('#cart_container').on('change', '.quantity_input', function (e) {
            e.preventDefault();
            calculateTableSummary();
        });

        $('#cart_container').on('click', '.apply_coupon', function (e) {
            e.preventDefault();
            $('#cart_container').append('<textarea class="coupon_name" style="display: none;">' + $(this).data('code') + '</textarea>');
            calculateTableSummary();
        });

        $('#cart_container').on('click', '.createCoupon', function (e) {
            e.preventDefault();
            showFastEditor('ecommerce_coupon', '', function (results) {

                $('#cart_container').append('<textarea class="coupon_name" style="display: none;">' + results.form_data.code + '</textarea>');
                $('#fastModuleEditor').modal('toggle');
                calculateTableSummary();
            });
        });

        $('#cart_container').on('click', '.addCustomProduct', function (e) {
            e.preventDefault();

            $.ajax({
                url: "ajax/customProductForm.php",
                type: "POST",
                data: {

                },
                async: true,
                dataType: 'text',
                success: function (html) {
                    var dialog = bootbox.dialog({
                        size: "md",
                        title: 'Prodotto personalizzato',
                        message: html,
                        buttons: {
                            cancel: {
                                label: "Annulla",
                                className: 'btn-default',
                                callback: function(){
                                    console.log('Custom cancel clicked');
                                }
                            },
                            ok: {
                                label: "Inserisci",
                                className: 'btn-primary',
                                callback: function(){

                                    var have_error = false;
                                    $('#customProductForm input, #customProductForm select').each(function () {
                                        if($(this).hasClass('required') && !$(this).val().length) {
                                            have_error = true;
                                            $(this).addClass('error').one('focus input', function () {
                                                $(this).removeClass('error');
                                            });
                                        }
                                    });

                                    if(have_error) {
                                        return false;
                                    }

                                    var unique_id = 'custom_' + (new Date().getUTCMilliseconds());

                                    var product_array = {
                                        'is_custom': true,
                                        'product_id': unique_id,
                                        'cart_unique_id': unique_id,
                                        'nome': $('#product_name').val(),
                                        'prezzo': $('#product_price').val(),
                                        'imposta': $('#product_vat').val(),
                                        'quantity': $('#product_quantity').val()
                                    };

                                    $('#cart_container').append('<textarea class="product_json" style="display: none;">' + JSON.stringify(product_array) + '</textarea>');
                                    calculateTableSummary();
                                }
                            }
                        }
                    });

                }
            });
        });
    });
}

function composeVariationModal(id, nome) {
    $.ajax({
        url: "ajax/getProductVariations.php",
        type: "POST",
        data: {
            "id": id,
            "type": 'html_select'
        },
        async: true,
        success: function (data) {

            $('#selectVariationModal #id_prodotto').val(id);
            $('#selectVariationModal .product_name').text(nome);
            $('#selectVariationModal .variations_box').html(data);
            $('#selectVariationModal').modal('show');

        }
    });
}

function addProductVariationToCart() {

    var product_id = $('#selectVariationModal #id_prodotto').val();
    var product_name = $('#selectVariationModal .product_name').text();
    var unique_id = product_id;
    var variations = new Array;
    var empty_variations = false;

    $('#selectVariationModal .variations_box select').each(function () {
        variations.push({
            id: $(this).data('id'),
            value: $(this).val()
        });
        if($(this).val() === '') empty_variations = true;
        unique_id += '_' + $(this).data('id') + '_' + $(this).val();
        product_name += ' - ' + $(this).data('name') + ': ' + $(this).find('option:selected').text();
    });

    if(!empty_variations) {
        addArticleToCart(product_id, variations, 1);
        $('#selectVariationModal').modal('hide');
    }
}

function addArticleToCart(id, variations, quantity) {

    if(typeof(variations) === 'undefined') {
        variations = {};
    }

    if(typeof(quantity) === 'undefined') {
        quantity = 1;
    }

    var product_json = {
        id: id,
        variations: variations,
        quantity: quantity
    };

    $('#cart_container').append('<textarea class="product_json" style="display: none;">' + JSON.stringify(product_json) + '</textarea>');

    calculateTableSummary();
}

function calculateTableSummary() {

    var products = [];
    var coupons = [];

    $('#cart_container').find('.product_json').each(function () {
        var product_json = JSON.parse($(this).val());
        if($(this).closest('tr').find('.quantity_input').length) {
            product_json.quantity = $(this).closest('tr').find('.quantity_input').val();
        }
        products.push(product_json);
    });

    $('#cart_container').find('.coupon_name').each(function () {
        coupons.push($(this).val());
    });

    $('#cart_container').height($('#cart_container').height()).html('<div class="circlePreloader"><div></div></div>');

    $.ajax({
        url: "ajax/getCart.php",
        type: "POST",
        data: {
            "cart": products,
            "coupons": coupons,
            "customer_id": $('#customer_id').val(),
            "id_spedizione": $('#spedizione').val(),
            "metodo_pagamento": (typeof($('#pagamento option:selected').data('json')) === 'object' ? $('#pagamento option:selected').data('json') : {})
        },
        async: true,
        dataType: "html",
        success: function (html) {
            $('#cart_container').height('auto');
            $('#cart_container').html(html);
        }
    });
}

function updateCustomerAddresses(id) {

    $.ajax({
        url: "ajax/getCustomerAddresses.php",
        type: "POST",
        data: {
            "id": id
        },
        async: true,
        dataType: "json",
        success: function (data) {
            Object.keys(data).forEach(function (key) {

                if($('#info_fatturazione [name="' + key + '"]').length && !$('#info_fatturazione [name="' + key + '"]').val().length) {
                    $('#info_fatturazione [name="' + key + '"]').val(data[key]);
                }

                if($('#info_spedizione [name="' + key + '"]').length && !$('#info_spedizione [name="' + key + '"]').val().length) {
                    $('#info_spedizione [name="' + key + '"]').val(data[key]);
                }

            });
        }
    });
}

function searchInTable($container, query) {
    query = query.toLowerCase();

    if(typeof(window.search_call) !== 'undefined' && window.search_call !== false) {
        window.search_call.abort();
    }

    window.search_call = $.ajax({
        url: "ajax/search.php",
        type: "POST",
        data: {
            "query": query
        },
        async: true,
        dataType: "html",
        success: function (html) {
            $container.html(html);

            $container.find('img').on('error', function (e) {
                var type = $(this).closest('.add_to_cart').data('type');
                $(this).attr('src', $('#baseElementPathAdmin').val() + 'modules/fatturazione/vendite/images/' + type + '.png');
            });
        }
    });
}

function moduleSaveFunction() {
    continue_saving = true;

    if($('#original_order_status').val() != "1" && $('#original_order_status').val() != "2" && $('#original_order_status').val() != "3" && ($('#order_status').val() == "1" || $('#order_status').val() == "2" || $('#order_status').val() == "3")) {
        continue_saving = false;

        bootbox.confirm('Impostando questo ordine su "' + $('#order_status option[value=' + $('#order_status').val() + ']').html() + '" il magazzino sarà aggiornato riducendo la quantità di prodotti disponibili. Continuare?', function (result) {
            if (result) {
                continueModuleSave();
            }
        })
    }

    if($('#original_order_status').val() != "4" && $('#original_order_status').val() != "0" && ($('#order_status').val() == "4" || $('#order_status').val() == "0")) {
        continue_saving = false;

        bootbox.confirm('Impostando questo ordine su "' + $('#order_status option[value=' + $('#order_status').val() + ']').html() + '" il magazzino sarà aggiornato aumentando la quantità di prodotti disponibili. Continuare?', function (result) {
            if (result) {
                continueModuleSave();
            }
        })
    }

    if(continue_saving) {
        continueModuleSave();
    }
}

function continueModuleSave() {

    var main_data = {
        "pID": $('#record_id').val(),
        "pOrderStatus": $('#order_status').val(),
        "pCourier": $('#courier').val(),
        "pCourierCustomName": $('#courier_custom_name').val(),
        "pTrackingCode": $('#tracking_code').val(),
    };

    if(!$('form.only-view').length) {

        if(!$('#full_cart_json').length) {
            bootbox.error("Il carrello deve necessariamente contenere dei prodotti!");
            return false;
        }

        var info_fatturazione = {};
        var info_spedizione = {};
        $('#info_fatturazione [name]').each(function () {
            info_fatturazione[$(this).attr('name')] = $(this).val();
        });
        $('#info_spedizione [name]').each(function () {
            info_spedizione[$(this).attr('name')] = $(this).val();
        });

        main_data['pCart'] = JSON.parse($('#full_cart_json').val());
        main_data['pInfoFatturazione'] = info_fatturazione;
        main_data['pInfoSpedizione'] = info_spedizione;
        main_data['pCouponsUsed'] = main_data['pCart'].coupon;
        main_data['pCustomerID'] = $('#customer_id').val();
        main_data['pTipoSpedizione'] = $('#spedizione').val();
        main_data['pTipoPagamento'] = $('#pagamento').val();
    }
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: main_data,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "courier_not_set") {
                bootbox.error("Specifica il nome del corriere o selezionane uno dalla lista.");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "cart_empty") {
                bootbox.error("Il carrello deve necessariamente contenere dei prodotti!");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
                if(data.refresh) {
                    location.reload();
                } else {
                    $('form').addClass('only-view');
                    $('#printOrderBtn').show();
                    $('#editOrderBtn').show();
                }
            }
        }
    })
}
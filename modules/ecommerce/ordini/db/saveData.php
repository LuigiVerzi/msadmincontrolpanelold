<?php
/**
 * MSAdminControlPanel
 * Date: 07/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

if($_POST['pCourier'] == "0" && $_POST['pCourierCustomName'] == "" && $_POST['pOrderStatus'] == "2") {
    echo json_encode(array("status" => "courier_not_set"));
    die();
}

$r_old = $MSFrameworkDatabase->getAssoc("SELECT order_status FROM ecommerce_orders WHERE id = :id", array($_POST['pID']), true);
$array_to_save = array(
    "order_status" => $_POST['pOrderStatus'],
    "courier" => $_POST['pCourier'],
    "courier_custom_name" => $_POST['pCourierCustomName'],
    "tracking_code" => $_POST['pTrackingCode'],
    "order_edit_date" => date("Y-m-d H:i:s"),
);

// Controllo se vengono passati anche i dati avanzati
if(isset($_POST['pCart']) && (!$r_old || $r_old['order_status'] == "0")) {
    $array_to_save['cart'] = json_encode($_POST['pCart']);
    $array_to_save['info_fatturazione'] = json_encode($_POST['pInfoFatturazione']);
    $array_to_save['info_spedizione'] = json_encode($_POST['pInfoSpedizione']);
    $array_to_save['coupons_used'] = json_encode($_POST['pCouponsUsed']);
    $array_to_save['tipo_spedizione'] = $_POST['pTipoSpedizione'];
    $array_to_save['payment_type'] = $_POST['pTipoPagamento'];

    if(!count($_POST['pCart']['products'])) {
        die(json_encode(array('status' => 'cart_empty')));
    }

    if(!$r_old) {
        $array_to_save['user_id'] = $_POST['pCustomerID'];
        if (!(new \MSFramework\customers())->getCustomerDataFromDB($_POST['pCustomerID'])) {
            die(json_encode(array('status' => 'mandatory_data_missing')));
        }
    }

}

$needRefresh = ($r_old['order_status'] != $_POST['pOrderStatus'] && ($r_old['order_status'] > 0 && $_POST['pOrderStatus'] == 0 || $r_old['order_status'] == 0 && $_POST['pOrderStatus'] > 0));

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_orders ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $order_id = $MSFrameworkDatabase->lastInsertId();
    $needRefresh = true;
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_orders SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $order_id = $_POST['pID'];
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

if(!$r_old || $r_old['order_status'] != $_POST['pOrderStatus']) {
    (new MSFramework\Ecommerce\orders())->updateOrderStatus($order_id, $_POST['pOrderStatus']);
}

if($r_old['order_status'] != "1" && $r_old['order_status'] != "2" && $r_old['order_status'] != "3" && ($_POST['pOrderStatus'] == "1" || $_POST['pOrderStatus'] == "2" || $_POST['pOrderStatus'] == "3")) {
    (new MSFramework\Ecommerce\orders())->updateProductsQtyOnStatusVariation($order_id, "remove");
}

if($r_old['order_status'] != "4" && $r_old['order_status'] != "0" && ($_POST['pOrderStatus'] == "4" || $_POST['pOrderStatus'] == "0")) {
    (new MSFramework\Ecommerce\orders())->updateProductsQtyOnStatusVariation($order_id, "add");
}

echo json_encode(array("status" => "ok", "id" => $order_id, "refresh" => $needRefresh));
die();
?>
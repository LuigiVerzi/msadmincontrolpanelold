<?php
/**
 * MSAdminControlPanel
 * Date: 07/05/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = array();
$info_cliente = array();
$formatted_cart = array(array());
$anteprima_cliente = '';
$info_spedizione = array();
$info_fatturazione = array();
$tipo_spedizione = array();
$metodo_pagamento = array();

if(isset($_GET['id'])) {

    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_orders WHERE id = :id", array($_GET['id']), true);
    $info_cliente = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($r['user_id']);
    $info_fatturazione = json_decode($r['info_fatturazione'], true);
    $info_spedizione = json_decode($r['info_spedizione'], true);
    $tipo_spedizione = (new \MSFramework\Ecommerce\shipping())->getShippingDetails($r['tipo_spedizione'])[$r['tipo_spedizione']];

    if($info_spedizione['usa_fatturazione']) {
        $info_spedizione = $info_fatturazione;
    }

     foreach(json_decode($MSFrameworkCMS->getCMSData('fatturazione_fatture')['webPayments'], true) as $id_metodo => $dati_metodo) {
         if (is_array($dati_metodo) && !empty($MSFrameworki18n->getFieldValue($dati_metodo['descrizione']))) {
             if(explode('_', $id_metodo)[0] === $r['payment_type']) {
                 $metodo_pagamento = $dati_metodo;
             }
         }
     }

    $coupons_used = json_decode($r['coupons_used'], true);
    $cart = json_decode($r['cart'], true);
}


if($info_cliente) {
    $info = array();
    if (!empty($info_cliente['email'])) {
        $info[] = $info_cliente['email'];
    }
    if (!empty($info_cliente['telefono_casa'])) {
        $info[] = $info_cliente['telefono_casa'];
    }
    if (!empty($info_cliente['telefono_cellulare'])) {
        $info[] = $info_cliente['telefono_cellulare'];
    }

    $anteprima_cliente = '';

    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
    if(json_decode($info_cliente['avatar'])) {
        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($info_cliente['avatar'], true)[0];
    }

    $anteprima_cliente .= '<div style="position: relative; padding-left: 65px;">';
    $anteprima_cliente .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
    $anteprima_cliente .= '<h2 class="no-margins">' . htmlentities($info_cliente['nome'] . ' ' . $info_cliente['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
    $anteprima_cliente .= '</div>';
}

?>

<style>
    .add_to_cart {
        text-align: left;
        margin-bottom: 15px;
        height: 93px;
    }

    .add_to_cart .category {
        display: block;
        text-align: left;
        font-size: 13px;
        text-overflow: ellipsis;
        overflow: hidden;
    }

    .add_to_cart .name {
        font-weight: 500;
        text-overflow: ellipsis;
        overflow: auto;
        display: block;
        white-space: initial;
        width: 100%;
        max-height: 40px;
    }

    .add_to_cart .price {
        font-weight: 500;
        text-align: right;
        color: #299946;
    }

    .add_to_cart .icon {
        display: block;
        width: 58px;
        height: 58px;
        float: left;
        margin-left: -5px;
        margin-right: 10px;
        border-radius: 3px;
        object-fit: cover;
        margin-top: 11px;
    }

    .add_to_cart .details_container {
        margin-left: 65px;
        width: 78%;
    }

    .only-view .only-edit-container {
        display: none;
    }

    .only-view .quantity_input {
        background: none;
        border: none;
        pointer-events: none;
        text-align: center;;
    }

    .only-view-container {
        display: none;
    }
    
    .only-view .only-view-container {
        display: block;
    }
</style>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php

                $customToolbarButton = '<a class="btn btn-success btn-outline" id="printOrderBtn" style="display: ' . ($r ? 'inline-block' : 'none'). ';" onclick="window.print();"><i class="fa fa-print"></i> Stampa</a> ';

                if($r && $r['order_status'] === 0) {
                    $customToolbarButton .= '<a class="btn btn-warning" id="editOrderBtn">Modifica</a>';
                }

                require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php");
                ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big <?= ($r ? 'only-view' : ''); ?>">
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Ordine</h2>
                            <div class="row orderHeaderRow">
                                <div class="col-sm-3" id="customerInfo">
                                    <input type="hidden" id="customer_id" class="required" value="<?= $r['user_id']; ?>">
                                    <label>Cliente</label>
                                    <?php if($info_cliente) { ?>
                                        <div class="data" style="margin-bottom: 15px; float: left;">
                                            <?= $anteprima_cliente; ?>
                                        </div>
                                        <?php if(!$r) { ?>
                                            <a href="#" class="editCustomer btn btn-sm btn-default pull-right">
                                                Modifica
                                            </a>
                                        <?php } ?>
                                    <?php } elseif($r['guest_email']) { ?>
                                        <div>L'utente ha effettuato l'acquisto in modalità <b>Ospite</b>.</div>
                                        <b>Email:</b> <span id="customer_email"><?php echo $r['guest_email']; ?></span>
                                    <?php } else { ?>
                                        <div class="data" style="display: none; margin-bottom: 15px; float: left;"></div>
                                        <a href="#" class="changeCustomer btn btn-sm btn-default selectCustomer">
                                            Seleziona cliente
                                        </a>
                                        <a href="#" class="editCustomer btn btn-warning btn-sm btn-outline" style="margin-right: 5px; display: none;">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    <?php } ?>
                                </div>

                                <div class="col-sm-6">
                                    <?php if($r) { ?>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="widget style1 gray-bg">
                                                    <span>Data ordine</span>
                                                    <h3><?php echo (new DateTime($r['order_date']))->format("d/m/Y H:i") ?></h3>
                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="widget style1 gray-bg">
                                                    <span>Ultimo aggiornamento</span>
                                                    <h3><?php echo (new DateTime($r['order_edit_date']))->format("d/m/Y H:i") ?></h3>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>

                                <div class="col-sm-3 orderStatusColumn">
                                    <label>Stato Ordine</label>
                                    <input type="hidden" id="original_order_status" value="<?php echo $r['order_status'] ?>" />
                                    <select id="order_status" name="order_status" class="form-control">
                                        <?php foreach((new \MSFramework\Ecommerce\orders)->getStatus() as $statK => $statV) { ?>
                                            <option value="<?php echo $statK ?>" <?php if($r['order_status'] == (string)$statK) { echo "selected"; } ?>><?php echo $statV ?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>

                            <div id="tracking_data_box" style="display: none;">
                                <div class="hr-line-dashed"></div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Corriere</label>
                                        <select id="courier" class="form-control">
                                            <option value="0">Altro corriere (Specificare)</option>
                                            <?php foreach((new \MSFramework\Ecommerce\shipping())->getCouriers() as $courierK => $courierV) { ?>
                                                <option value="<?php echo $courierK ?>" <?php if($r['courier'] == (string)$courierK) { echo "selected"; } ?>><?php echo $courierV ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-4" id="courier_custom_name_block" style="display: none;">
                                        <label>Nome Corriere</label>
                                        <input id="courier_custom_name" type="text" class="form-control" value="<?php echo htmlentities($r['courier_custom_name']) ?>">
                                    </div>

                                    <div class="col-sm-4">
                                        <label># Tracking</label>
                                        <input id="tracking_code" type="text" class="form-control" value="<?php echo htmlentities($r['tracking_code']) ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="only-view-container">
                                        <h2 class="title-divider"><?= ($info_fatturazione['richiedi_fattura'] ? 'Fatturazione' : 'Dati cliente'); ?> <a class="label label-info copy_order_info" href="#" style="font-size: 11px; top: -3px; position: relative;">Copia dati</a></h2>
                                        <div class="address_to_copy">
                                            <?= (new \MSFramework\Ecommerce\orders())->formatCustomerDetails($info_fatturazione, 'billing'); ?>
                                        </div>
                                        <?php if(ucfirst($r["payment_type"])) { ?>
                                            <div>
                                                <br>
                                                <b>Pagamento tramite:</b> <span class="metodo_pagamento"><?= ucfirst($r["payment_type"]); ?></span>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="only-edit-container">
                                        <h2 class="title-divider">Fatturazione</h2>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Metodo di pagamento</label>
                                                <select id="pagamento" type="text" class="form-control">
                                                    <option value="">Non indicato</option>
                                                    <?php foreach(json_decode($MSFrameworkCMS->getCMSData('fatturazione_fatture')['webPayments'], true) as $id_metodo => $dati_metodo) { ?>
                                                        <?php if(is_array($dati_metodo) && !empty($MSFrameworki18n->getFieldValue($dati_metodo['descrizione']))) { ?>
                                                        <option value="<?= explode('_', $id_metodo)[0]; ?>" data-json="<?= htmlentities(json_encode($dati_metodo)); ?>" data-prezzo="<?= ($dati_metodo['rincaro'] ? (new \MSFramework\Fatturazione\imposte())->getPriceToShow($dati_metodo['rincaro'], $dati_metodo['imposta'])['tax'] : ''); ?>" <?= (explode('_', $id_metodo)[0] === $r['payment_type'] ? 'selected' : ''); ?>>
                                                            <?= $MSFrameworki18n->getFieldValue($dati_metodo['descrizione']); ?>
                                                        </option>
                                                            <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="info_fatturazione" style="background: #fafafa; padding: 15px 15px 0; border: 1px solid #ececec;">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Nome</label>
                                                    <input name="nome" type="text" class="form-control required" value="<?php echo $info_fatturazione['nome'] ?>">
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Cognome</label>
                                                    <input name="cognome" type="text" class="form-control required" value="<?php echo $info_fatturazione['cognome'] ?>">
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Cellulare</label>
                                                    <input name="cellulare" type="text" class="form-control required" value="<?php echo $info_fatturazione['cellulare'] ?>">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Ragione Sociale</label>
                                                    <input name="ragione_sociale" type="text" class="form-control" value="<?php echo $info_fatturazione['ragione_sociale'] ?>">
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Partita IVA</label>
                                                    <input name="piva" type="text" class="form-control" value="<?php echo $info_fatturazione['piva'] ?>">
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Codice Fiscale</label>
                                                    <input name="cf" type="text" class="form-control" value="<?php echo $info_fatturazione['cf'] ?>">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Indirizzo</label>
                                                    <input name="indirizzo" type="text" class="form-control" value="<?php echo $info_fatturazione['indirizzo'] ?>">
                                                </div>

                                                <div class="col-sm-3">
                                                    <label>Provincia</label>
                                                    <input name="citta" type="text" class="form-control" value="<?php echo $info_fatturazione['citta'] ?>">
                                                </div>

                                                <div class="col-sm-3">
                                                    <label>Comune</label>
                                                    <input name="comune" type="text" class="form-control" value="<?php echo $info_fatturazione['comune'] ?>">
                                                </div>

                                                <div class="col-sm-3">
                                                    <label>CAP</label>
                                                    <input name="cap" type="text" class="form-control" value="<?php echo $info_fatturazione['cap'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="only-view-container">
                                        <h2 class="title-divider">Spedizione <a class="label label-info copy_order_info" href="#" style="font-size: 11px; top: -3px; position: relative;">Copia Indirizzo</a></h2>
                                        <div class="address_to_copy">
                                            <?= (new \MSFramework\Ecommerce\orders())->formatCustomerDetails($info_spedizione, 'shipping'); ?>
                                        </div>
                                        <?php if($tipo_spedizione) { ?>
                                        <div>
                                            <br>
                                            <b>Metodo di Spedizione:</b> <span class="metodo_spedizione"><?php echo $MSFrameworki18n->getFieldValue($tipo_spedizione['nome']) ?></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="only-edit-container">
                                        <h2 class="title-divider">Spedizione</h2>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Metodo di spedizione</label>
                                                <select id="spedizione" type="text" class="form-control required">
                                                    <?php foreach((new \MSFramework\Ecommerce\shipping())->getShippingDetails() as $id_spedizione => $spedizione) { ?>
                                                        <option data-prezzo="<?= $spedizione['prezzo']; ?>" value="<?= $id_spedizione; ?>" <?= ($r['tipo_spedizione'] == $id_spedizione ? 'selected' : ''); ?>>
                                                            <?= $MSFrameworki18n->getFieldValue($spedizione['nome']); ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="info_spedizione" style="background: #fafafa; padding: 15px 15px 0; border: 1px solid #ececec;">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Nome</label>
                                                    <input name="nome" type="text" class="form-control required" value="<?php echo $info_spedizione['nome'] ?>">
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Cognome</label>
                                                    <input name="cognome" type="text" class="form-control required" value="<?php echo $info_spedizione['cognome'] ?>">
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Cellulare</label>
                                                    <input name="cellulare" type="text" class="form-control required" value="<?php echo $info_spedizione['cellulare'] ?>">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Ragione Sociale</label>
                                                    <input name="ragione_sociale" type="text" class="form-control" value="<?php echo $info_spedizione['ragione_sociale'] ?>">
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Partita IVA</label>
                                                    <input name="piva" type="text" class="form-control" value="<?php echo $info_spedizione['piva'] ?>">
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Codice Fiscale</label>
                                                    <input name="cf" type="text" class="form-control" value="<?php echo $info_spedizione['cf'] ?>">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Indirizzo</label>
                                                    <input name="indirizzo" type="text" class="form-control required" value="<?php echo $info_spedizione['indirizzo'] ?>">
                                                </div>

                                                <div class="col-sm-3">
                                                    <label>Provincia</label>
                                                    <input name="citta" type="text" class="form-control required" value="<?php echo $info_spedizione['citta'] ?>">
                                                </div>

                                                <div class="col-sm-3">
                                                    <label>Comune</label>
                                                    <input name="comune" type="text" class="form-control required" value="<?php echo $info_spedizione['comune'] ?>">
                                                </div>

                                                <div class="col-sm-3">
                                                    <label>CAP</label>
                                                    <input name="cap" type="text" class="form-control required" value="<?php echo $info_spedizione['cap'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="title-divider">
                                Carrello
                            </h2>

                            <div id="cart_container" style="position: relative;">
                                <?php include('ajax/getCart.php'); ?>
                            </div>

                            <div class="only-edit-container">
                                <h2 class="title-divider">Aggiungi prodotti</h2>
                                <div id="articoli_e_servizi">
                                    <input type="text" class="form-control filter_tables" placeholder="Cerca prodotto..." style="margin-bottom: 10px;">
                                    <div class="row search-container">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<div class="modal inmodal fade" id="selectVariationModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Chiudi</span></button>
                <h4 class="modal-title">Seleziona Variazione</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id_prodotto" value="">
                <p style="text-align: center;">
                    Seleziona la variazione desiderata per <strong class="product_name"></strong>.
                </p>

                <div class="hr-line-dashed"></div>

                <div class="variations_box"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-success add_variation">Aggiungi</button>
            </div>
        </div>
    </div>
</div>

<textarea id="coupons_used" style="display: none;"><?= json_encode($coupons_used); ?></textarea>

<script>
    globalInitForm();
</script>

<style>
    /* PRINT */
    @media print {
        .wrapper.wrapper-content {
            padding: 0 !important;
        }
        
        #form {
            left: 0;
            top: 0;
            z-index: 9999999;
            width: 100%;
            height: 100%;
            background: white;
            height: auto !important;
        }

        #toast-container {
            display: none;
        }

        .page-heading, .navbar {
            display: none !important;
        }

        #form a {
            display: none !important;
        }

        #form .orderHeaderRow > * {
            width: 50%;
        }

        #form .orderHeaderRow .orderStatusColumn {
            display: none;
        }

        #form [class*="col-"] {
            width: 50%;
            float: left;
        }

        #form.tabcontrol .req_input {
            display: none;
        }

        .footer {
            display: none !important;
        }
    }
</style>

</body>
</html>
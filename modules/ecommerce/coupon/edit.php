<?php
/**
 * MSAdminControlPanel
 * Date: 05/05/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkEcommerceProducts = new \MSFramework\Ecommerce\products();
if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_coupon WHERE id = :id", array($_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Codice coupon</label>
                                    <input id="code" type="text" class="form-control required" value="<?php echo htmlentities($r['code']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Tipo coupon*</label>
                                    <select id="type" class="form-control">
                                        <?php
                                        foreach((new \MSFramework\Ecommerce\coupon)->getTypes() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($r['type'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-2">
                                    <label>Valore (<?= CURRENCY_SYMBOL; ?> / %)</label>
                                    <input id="value" type="text" class="form-control required" value="<?php echo htmlentities($r['value']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Scadenza</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lasciare vuoto per nessuna scadenza"></i></span>
                                    <div class="input-group date expiration_date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="expiration_date" value="<?php echo ($r['expiration_date'] != '') ? date("d/m/Y", $r['expiration_date']) : '' ?>">
                                    </div>
                                </div>

                                <?php
                                $free_shipping_check = "";
                                if($r['free_shipping'] == 1) {
                                    $free_shipping_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Seleziona questa casella per azzerare i costi di spedizione verso qualunque destinazione"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="free_shipping" <?php echo $free_shipping_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Spedizione gratuita</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                        <h1>Restrizioni</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Limita per prodotto</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il/I prodotto/i al quale applicare lo sconto in percentuale/fisso sul prodotto o che deve essere presente nel carrello per attivare lo sconto percentuale/fisso sul carrello"></i></span>
                                    <select id="product_limit" class="form-control" multiple="multiple" width="100%">
                                        <?php
                                        $product_limit = json_decode($r['product_limit'], true);
                                        if(count($product_limit) != 0) {
                                            foreach($product_limit as $prod) {
                                                $info_prodotto = $MSFrameworkEcommerceProducts->getProductDetails($prod)[$prod];
                                                $firephp->log($info_prodotto);
                                        ?>
                                                <option value="<?= $info_prodotto['id']; ?>" selected><?= $MSFrameworki18n->getFieldValue($info_prodotto['nome']); ?> [ID: <?= $info_prodotto['id']; ?>]</option>
                                        <?php
                                            }
                                        } else { ?>
                                            <option></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-6">
                                    <label>Limita per categoria</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La/le categoria/e al quale applicare lo sconto in percentuale/fisso sul prodotto o che deve essere presente nel carrello per attivare lo sconto percentuale/fisso sul carrello"></i></span>
                                    <select id="category_limit" class="form-control" multiple="multiple" width="100%">
                                        <?php
                                        $category_limit = json_decode($r['category_limit'], true);
                                        foreach((new \MSFramework\Ecommerce\categories)->getCategoryDetails() as $categoryK => $categoryV) {
                                        ?>
                                            <option value="<?php echo $categoryK ?>" <?php if(in_array($categoryK, $category_limit)) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($categoryV['nome'], true) ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Escludi prodotti</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il/I prodotto/i al quale NON applicare lo sconto in percentuale/fisso sul prodotto o che NON deve essere presente nel carrello per attivare lo sconto percentuale/fisso sul carrello"></i></span>
                                    <select id="product_exclude" class="form-control" multiple="multiple" width="100%">
                                        <?php
                                        $product_exclude = json_decode($r['product_exclude'], true);
                                        if(count($product_exclude) != 0) {
                                            foreach($product_exclude as $prod) {
                                                $info_prodotto = $MSFrameworkEcommerceProducts->getProductDetails($prod)[$prod];
                                                ?>
                                                <option value="<?= $info_prodotto['id']; ?>" selected><?= $MSFrameworki18n->getFieldValue($info_prodotto['nome']); ?> [ID: <?= $info_prodotto['id']; ?>]</option>
                                                <?php
                                            }
                                        } else { ?>
                                            <option></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-6">
                                    <label>Escludi categoria</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La/le categoria/e al quale NON applicare lo sconto in percentuale/fisso sul prodotto o che NON deve essere presente nel carrello per attivare lo sconto percentuale/fisso sul carrello"></i></span>
                                    <select id="category_exclude" class="form-control" multiple="multiple" width="100%">
                                        <?php
                                        $category_exclude = json_decode($r['category_exclude'], true);
                                        foreach((new \MSFramework\Ecommerce\categories)->getCategoryDetails() as $categoryK => $categoryV) {
                                            ?>
                                            <option value="<?php echo $categoryK ?>" <?php if(in_array($categoryK, $category_exclude)) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($categoryV['nome'], true) ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Spesa minima <?= CURRENCY_SYMBOL; ?></label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il valore minimo del carrello affinchè questo coupon possa essere utilizzato dall'utente. Impostare '0' o vuoto per nessuna spesa minima"></i></span>
                                    <input id="min_expense" name="min_expense" type="text" class="form-control" value="<?php echo htmlentities($r['min_expense']) ?>">
                                </div>

                                <?php
                                $individual_use_check = "";
                                if($r['individual_use'] == 1) {
                                    $individual_use_check = "checked";
                                }
                                ?>
                                <div class="col-sm-4">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Seleziona questa casella per impedire l'uso di questo coupon in combinazione con altri"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="individual_use" <?php echo $individual_use_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Uso Individuale</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $exclude_promo_items_check = "";
                                if($r['exclude_promo_items'] == 1) {
                                    $exclude_promo_items_check = "checked";
                                }
                                ?>
                                <div class="col-sm-4">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Seleziona questa casella se il coupon non si applica agli oggetti in saldo. I coupon per prodotto funzionano solo se l'oggetto non è in saldo. I coupon per carrello funzionano solo se non ci sono articoli in saldo nel carrello."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="exclude_promo_items" <?php echo $exclude_promo_items_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Escludi articoli in saldo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="easyRowDuplicationContainer">
                                <?php
                                $r_mail = json_decode($r['mail_limit'], true);
                                if(count($r_mail) == 0) {
                                    $r_mail = array('');
                                }

                                foreach($r_mail as $dataK => $dataV) {
                                    ?>
                                    <div class="stepsDuplication rowDuplication mailLimitDuplication">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Email</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Elenco delle email degli utenti che possono usufruire del buono"></i></span>
                                                <input type="text" class="form-control email" value="<?php echo htmlentities($dataV) ?>">
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                        </fieldset>

                        <h1>Limiti</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Limite di utilizzo per coupon</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il numero di volte che questo coupon può essere utilizzato prima di scadere. Impostare '0' o vuoto per nessuna scadenza"></i></span>
                                    <input id="coupon_limit_usage" type="number" class="form-control" value="<?php echo htmlentities($r['coupon_limit_usage']) ?>" min="0">
                                </div>

                                <div class="col-sm-6">
                                    <label>Limite di utilizzo per utente</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il numero di volte che questo coupon può essere utilizzato dallo stesso utente prima di scadere. Impostare '0' o vuoto per nessuna scadenza"></i></span>
                                    <input id="user_limit_usage" type="number" class="form-control" value="<?php echo htmlentities($r['user_limit_usage']) ?>" min="0">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                        <?php
                        if($_GET['id'] != "") {
                        ?>
                        <h1>Storico utilizzi</h1>
                        <fieldset>
                            <div class="row m-b">
                                <div class="col-sm-1">
                                    <label>ID Ordine</label>
                                </div>

                                <div class="col-sm-2">
                                    <label>Data</label>
                                </div>

                                <div class="col-sm-3">
                                    <label>Utente</label>
                                </div>

                                <div class="col-sm-2">
                                    <label>Totale <br /> (senza coupon)</label>
                                </div>

                                <div class="col-sm-2">
                                    <label>Totale Coupon</label>
                                </div>

                                <div class="col-sm-2">
                                    <label>Totale <br /> (con coupon)</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Questo totale indica il valore del carrello al netto di tutti i coupon applicati"></i></span>
                                </div>
                            </div>

                            <?php
                            foreach((new \MSFramework\Ecommerce\coupon)->getOrdersWithCoupon($_GET['id']) as $orderDetail) {
                                $order_cart = json_decode($orderDetail['cart'], true);
                                $order_coupons_used = json_decode($orderDetail['coupons_used'], true);

                                $sale_value = 0;
                                foreach($order_coupons_used as $coupon_used) {
                                    if($coupon_used['coupon_id'] == $_GET['id']) {
                                        $sale_value = $coupon_used['sale_value'];
                                        break;
                                    }
                                }
                            ?>
                                <div class="row">
                                    <div class="col-sm-1">
                                        <?php echo $orderDetail['id'] ?>
                                    </div>

                                    <div class="col-sm-2">
                                        <?php echo (new DateTime($orderDetail['order_date']))->format("d/m/Y H:i") ?>
                                    </div>

                                    <div class="col-sm-3">
                                        <?php
                                            $userdata = (new \MSFramework\customers())->getCustomerDataFromDB($orderDetail['user_id'], "nome, cognome");
                                            echo $userdata['nome'] . " " . $userdata['cognome'];
                                        ?>
                                    </div>

                                    <div class="col-sm-2">
                                        <?= CURRENCY_SYMBOL; ?> <?php echo number_format($order_cart['total_price']['tax'], 2) ?>
                                    </div>

                                    <div class="col-sm-2">
                                        - <?= CURRENCY_SYMBOL; ?> <?php echo number_format($sale_value, 2) ?>
                                    </div>

                                    <div class="col-sm-2">
                                        <?= CURRENCY_SYMBOL; ?> <?php echo number_format($order_cart['coupon_price'], 2) ?>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>
                            <?php
                            }
                            ?>
                        </fieldset>

                        <?php } ?>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
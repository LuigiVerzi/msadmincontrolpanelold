<?php
/**
 * MSAdminControlPanel
 * Date: 05/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pType'] == "" || $_POST['pValue'] == "" || $_POST['pCode'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_var_prezzi = array('pValue');
foreach($array_var_prezzi as $curPostPrice) {
    $_POST[$curPostPrice] = str_replace(",", ".", $_POST[$curPostPrice]);
    if(!strstr($_POST[$curPostPrice], ".") && $_POST[$curPostPrice] != "") {
        $_POST[$curPostPrice] .= ".00";
    }

    if(!is_numeric($_POST[$curPostPrice]) && $_POST[$curPostPrice] != "") {
        echo json_encode(array("status" => "value_numeric"));
        die();
    }
}

$array_var_prezzi = array('pMinExpense');
foreach($array_var_prezzi as $curPostPrice) {
    $_POST[$curPostPrice] = str_replace(",", ".", $_POST[$curPostPrice]);
    if(!strstr($_POST[$curPostPrice], ".") && $_POST[$curPostPrice] != "") {
        $_POST[$curPostPrice] .= ".00";
    }

    if(!is_numeric($_POST[$curPostPrice]) && $_POST[$curPostPrice] != "") {
        echo json_encode(array("status" => "min_expense_numeric"));
        die();
    }
}

$array_var_prezzi = array('pCouponLimitUsage', 'pUserLimitUsage');
foreach($array_var_prezzi as $curPostPrice) {
    if(!is_numeric($_POST[$curPostPrice]) && !is_int($_POST[$curPostPrice]) && $_POST[$curPostPrice] != "") {
        echo json_encode(array("status" => "limit_usage_numeric"));
        die();
    }
}

$ts_scadenza = "";
if($_POST['pExpirationDate'] != "") {
    $ary_scadenza = explode("/", $_POST['pExpirationDate']);
    $ts_scadenza = mktime(23, 59, 59, $ary_scadenza[1], $ary_scadenza[0], $ary_scadenza[2]);
    if(!$ts_scadenza) {
        echo json_encode(array("status" => "expire_not_valid"));
        die();
    }
}

$array_to_save = array(
    "code" => $_POST['pCode'],
    "type" => $_POST['pType'],
    "value" => $_POST['pValue'],
    "expiration_date" => $ts_scadenza,
    "free_shipping" => $_POST['pFreeShipping'],
    "product_limit" => json_encode($_POST['pProductLimit']),
    "category_limit" => json_encode($_POST['pCategoryLimit']),
    "product_exclude" => json_encode($_POST['pProductExclude']),
    "category_exclude" => json_encode($_POST['pCategoryExclude']),
    "min_expense" => $_POST['pMinExpense'],
    "individual_use" => $_POST['pIndividualUse'],
    "exclude_promo_items" => $_POST['pExcludePromoItems'],
    "mail_limit" => json_encode($_POST['pLimitToMails']),
    "coupon_limit_usage" => $_POST['pCouponLimitUsage'],
    "user_limit_usage" => $_POST['pUserLimitUsage'],
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_coupon ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_coupon SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
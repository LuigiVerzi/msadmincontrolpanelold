<?php
/**
 * MSAdminControlPanel
 * Date: 05/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_coupon") as $r) {
    $array['data'][] = array(
        $r['code'],
        (new \MSFramework\Ecommerce\coupon)->getTypes($r['type']),
        $r['value'],
        ($r['expiration_date'] != '') ? date("d/m/Y", $r['expiration_date']) : 'Mai',
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

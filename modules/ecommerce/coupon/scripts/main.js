$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('.input-group.date.expiration_date').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
        startDate: new Date(),
    });

    $('.rowDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi email",
        "deleteButtonText": "Elimina email",
        "afterAddCallback": function() {
            $('.rowDuplication:last').find('.email').val('');
        },
        "afterDeleteCallback": function() {
        },
    });

    initProductsSelect2($('#product_limit, #product_exclude'));

    $('#category_limit, #category_exclude').select2();
}

function moduleSaveFunction() {
    data_ary = new Array();
    $('.stepsDuplication.mailLimitDuplication').each(function(row_index) {
        data_ary.push($(this).find('.email').val());
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pCode": $('#code').val(),
            "pType": $('#type').val(),
            "pValue": $('#value').val(),
            "pExpirationDate": $('#expiration_date').val(),
            "pFreeShipping": $('#free_shipping:checked').length,
            "pProductLimit": $('#product_limit').val(),
            "pCategoryLimit": $('#category_limit').val(),
            "pProductExclude": $('#product_exclude').val(),
            "pCategoryExclude": $('#category_exclude').val(),
            "pMinExpense": $('#min_expense').val(),
            "pIndividualUse": $('#individual_use:checked').length,
            "pExcludePromoItems": $('#exclude_promo_items:checked').length,
            "pLimitToMails": data_ary,
            "pCouponLimitUsage": $('#coupon_limit_usage').val(),
            "pUserLimitUsage": $('#user_limit_usage').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "value_numeric") {
                bootbox.error("Il campo 'Valore' deve contenere un valore numerico");
            } else if(data.status == "min_expense_numeric") {
                bootbox.error("Il campo 'Spesa minima' deve contenere un valore numerico");
            } else if(data.status == "limit_usage_numeric") {
                bootbox.error("I campi 'Limite di utilizzo coupon/utente' devono contenere un valore numerico");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data di scadenza non è valido. Impossibile continuare.");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
function initForm() {
    initClassicTabsEditForm();

    $('#cancel_import_csv').on('click', function (e) {
        e.preventDefault();

        window.csv_file = false;
        Dropzone.forElement("#dropzoneForm").removeAllFiles(true);
        $('#assegnazione_pannelli').html('');
        validateImportStep(0);
    });

    $('#start_import_csv').on('click', function (e) {
        e.preventDefault();

        window.dialog = bootbox.dialog({
            message: '<p class="text-center">Importazione del file CSV in corso. Attendere...</p>',
            closeButton: false
        });

        if($(this).hasClass('disabled')) return false;
        if(validateImportStep(1)) {
            $('#import_csv_form').addClass('sk-loading');
            window.csv_total = 0;
            window.csv_inserted = 0;
            window.csv_updated = 0;
            sendCSVData(chunk(window.csv_file.data, 5000), 0);
        } else {
            if(!window.lockedByDiscriminante) {
                window.dialog.modal('hide');
                bootbox.error("Per poter compiere questa operazione, devi prima collegare tutti i campi obbligatori (*)!");
            }
        }
    });

    $('#start_export_csv').on('click', function() {
        $('#export_csv_form').submit();
    })

    window.csv_file = false;

    Dropzone.options.dropzoneForm = {
        paramName: "file",
        maxFilesize: 100,
        multiple: false,
        dictDefaultMessage: "<strong>Seleziona un file .CSV da caricare</strong></br> (Oppure trascinalo qui sopra)",
        acceptedFiles: '.csv',
        maxFiles:1,
        autoProcessQueue:false,
        autoQueue:false,
        init: function() {
            this.on('addedfile', function(file) {
                $('.dz-progress').hide();
                window.csv_file = false;

                Papa.parse(file, {
                    complete: function(results, file_data) {

                        if(!results.data.length) {
                            window.csv_file = false;
                            Dropzone.forElement("#dropzoneForm").removeAllFiles(true);
                            validateImportStep(0);
                        }
                        else {
                            window.csv_file = results;
                            validateImportStep(0);
                        }
                    }
                });

                if (this.files.length > 1) {
                    this.removeFile(this.files[0]);
                }

                return false;
            });

            $('.sk-spinner').hide();
        }
    };

}

function sendCSVData(chunk_data, step) {
    var assegnazione_colonne = {};
    $('.valori_colonna').each(function () {
        var col = $(this).data('id');
        var type = $(this).val();
        if(type != '') {
            assegnazione_colonne[type] = col;
        }
    });

    var db_data = [];
    chunk_data[step].forEach(function(riga) {
        var valori_assegnazione = {};
        Object.keys(assegnazione_colonne).forEach(function(key) {
            valori_assegnazione[key] = riga[assegnazione_colonne[key]]
        });
        db_data.push(valori_assegnazione);
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            'data' : JSON.stringify([db_data]),
            'discriminante' : $('#discriminante').val(),
        },
        dataType: "json",
        success: function(data) {

            if(typeof data.inserted != 'undefined') window.csv_inserted += data.inserted;
            if(typeof data.updated != 'undefined') window.csv_updated += data.updated;
            if(typeof data.total != 'undefined') window.csv_total += data.total;

            if(step >= chunk_data.length - 1 ) {
                window.dialog.modal('hide');

                $('#import_csv_form').removeClass('sk-loading');

                if (data.status == "mandatory_data_missing") {
                    bootbox.error("Per poter compiere questa operazione, devi prima collegare tutti i campi obbligatori (*)!");
                } else {
                    if(data.status == "query_error" && (window.csv_inserted > 0 || window.csv_updated > 0)) {
                        bootbox.error("Non siamo riusciti a importare tutte le righe. Probabilmente alcuni dei record importati non rispettavano i campi obbligatori richiesti o il formato dei dati era errato. Prodotti importati: <b>" + window.csv_inserted + "</b>. Prodotti aggiornati: <b>" + window.csv_updated + "</b>");
                    } else if (data.status == "query_error" && window.csv_inserted == 0 && window.csv_updated == 0) {
                        bootbox.error("Non è stata inserita/aggiornata nessuna riga. Probabilmente alcuni dei record importati non rispettavano i campi obbligatori richiesti o il formato dei dati era errato.");
                    } else {
                        bootbox.alert("Successo! Prodotti importati: <b>" + window.csv_inserted + "</b>. Prodotti aggiornati: <b>" + window.csv_updated + "</b>", function () {
                            window.location.href = '../import_csv';
                        });
                    }
                }
            }
            else
            {
                sendCSVData(chunk_data, (step+1) );
            }

        }
    });
}

function validateImportStep(index) {
    window.lockedByDiscriminante = false;

    if(index == 0) {
        if (window.csv_file) {
            createTableFromCSV(window.csv_file.data);

            $('#import_csv_form .csv_import_step').hide();
            $('#import_csv_form #csv-2').show();
        }
        else {
            $('#import_csv_form .csv_import_step').hide();
        }
    }
    if(index == 1) {

        var assegnazione = {nome: null, email: null};

        if($('#assegnazione_pannelli .valori_colonna').length) {
            $('#assegnazione_pannelli .valori_colonna').each(function () {
                if($(this).val() != '') assegnazione[$(this).val()] = $(this).data('id');
            });
        }

        if(assegnazione[$('#discriminante').val()] == undefined) {
            window.lockedByDiscriminante = true;
            window.dialog.modal('hide');
            bootbox.error("Per poter compiere questa operazione, devi indicare in quale colonna si trova il campo discriminante!");
            return false;
        }

        /*if(assegnazione['email'] !== null) {
            return true;
        }
        else return false;*/

        return true;
    }

    return true;

}

function createTableFromCSV(data) {
    var html = '';
    var template = $('#template_assegnazione').val();

    data[0].forEach(function(riga, colonna) {

        var valori = [];

        for (var i = 0; i < data.length; i++) {
            if(data[i][colonna] != '') valori.push(data[i][colonna]);
            if(valori.length >= 5) break;
        }

        if(!valori.length) valori = '[VUOTO]';
        html += template
            .replace('{valori}', valori.join('<br>'))
            .replace(/{id}/g, colonna)
            .replace(/{is_email}/g, (validateEmail(valori[1]) ? 'selected' : '') );

    });

    $('#assegnazione_pannelli').html(html);

    setTimeout(function() {
    }, 50);

    $('.valori_colonna').on('change', function () {
        var type = $(this).val();

        if(type == 'lista' && $('.custom_list').val()) {
            $('.custom_list').val('').change();
        }

        $('.valori_colonna').not($(this)).each(function () {
            if($(this).val() == type) {
                $(this).val('').change();
                return true;
            }
        });

    });

}

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function chunk (arr, len) {
    var chunks = [],
        i = 0,
        n = arr.length;

    while (i < n) {
        chunks.push(arr.slice(i, i += len));
    }

    return chunks;
}

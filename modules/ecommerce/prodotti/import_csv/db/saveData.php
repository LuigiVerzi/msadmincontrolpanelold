<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');


$discriminante = $_POST['discriminante'];
$data = json_decode($_POST['data'], true);

if(!is_array($data[0]) || $discriminante == "") {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$status = importaProdotti($data[0], $discriminante);

unset($data);

if ($status[0]['new'] == 0 && $status[0]['updated'] == 0) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "inserted" => $status[0]['new'], "updated" => $status[0]['updated']));
die();


/* INIZIO FUNZIONI */
function importaProdotti($csv_array, $discriminante) {
    global $MSFrameworkDatabase, $firephp, $MSFrameworki18n;

    $ary_status = array("new" => 0, "updated" => 0);

    $cur_lang_code = $MSFrameworki18n->getCurrentLanguageDetails()['long_code'];
    $MSFrameworkUrl = new \MSFramework\url();

    $MSFrameworkImposte = new \MSFramework\Ecommerce\imposte();
    $getImposte = $MSFrameworkImposte->getImposte();

    $total_rows = count($csv_array);
    $initial_rows = $MSFrameworkDatabase->getCount('SELECT id FROM ecommerce_products');

    $csv_array = array_chunk($csv_array, 1000);
    $correct_col = count($csv_array[0][0]);

    $check_datalang = array("nome", "slug", "long_descr", "short_descr");
    $imploded_datalang = implode(", ", $check_datalang);

    foreach($csv_array as $csv_chuck) {
        $tmp_chunk = $csv_chuck[0];

        $sql_keys = implode(', ', array_keys($tmp_chunk));

        $row_values = array();
        foreach($csv_chuck as $k=>$csv_row) {
            if(count($csv_row) != $correct_col) continue; // Salta se le colonne non corrispondono (CSV formattato male)

            $discriminante_value = $csv_row[$discriminante];
            if($discriminante_value == "") {
                continue;
            }

            if($csv_row['warehouse_qty'] != "" && is_numeric($csv_row['warehouse_qty'])){
                $csv_row['manage_warehouse'] = "1";
            } else {
                continue;
            }

            if($csv_row['imposta'] != "" && !array_key_exists($csv_row['imposta'], $getImposte)){
                continue;
            }

            if($csv_row['prezzo'] != "") {
                $csv_row['prezzo'] = str_replace(",", ".", $csv_row['prezzo']);
                if (!strstr($csv_row['prezzo'], ".") && $csv_row['prezzo'] != "") {
                    $csv_row['prezzo'] .= ".00";
                }

                if (!is_numeric($csv_row['prezzo'])) {
                    continue;
                }
            }

            if($csv_row['is_active'] != "") {
                if($csv_row['is_active'] == "1" || $csv_row['is_active'] == "si" || $csv_row['is_active'] == "yes" || $csv_row['is_active'] == "on") {
                    $csv_row['is_active'] = "1";
                } else if($csv_row['is_active'] == "0" || $csv_row['is_active'] == "no" || $csv_row['is_active'] == "off") {
                    $csv_row['is_active'] = "0";
                } else {
                    $csv_row['is_active'] = "";
                }
            }

            if($MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_products WHERE `" . $discriminante . "` = :discriminante", array(":discriminante" => $discriminante_value)) == 0) {
                //è un nuovo prodotto, verifico i campi obbligatori e lo inserisco
                if($csv_row['nome'] == "" || $csv_row['prezzo'] == "") {
                    continue;
                }

                if($csv_row['product_code'] != "" && $MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_products WHERE product_code = :code", array(":code" => $csv_row['product_code'])) != 0) {
                    continue;
                }

                if($csv_row['ean'] != "" && $MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_products WHERE ean = :code", array(":code" => $csv_row['ean'])) != 0) {
                    continue;
                }

                $csv_row['slug'] = $MSFrameworkUrl->cleanString($csv_row['nome']);
                if(!$MSFrameworkUrl->checkSlugAvailability($csv_row['slug'], "ecommerce_products", '0')) {
                    $csv_row['slug'] = $MSFrameworkUrl->findNextAvailableSlug($csv_row['slug']);
                }

                foreach($check_datalang as $cur_datalang) {
                    if($csv_row[$cur_datalang] != "") {
                        $csv_row[$cur_datalang] = json_encode(array($cur_lang_code => $csv_row[$cur_datalang]));
                    }
                }

                if($csv_row['is_active'] == "") { //se non ci sono preferenze per il prodotto (attivo/non attivo) i nuovi prodotti inseriti sono sempre ATTIVI
                    $csv_row['is_active'] = "1";
                }

                $stringForDB = $MSFrameworkDatabase->createStringForDB($csv_row, 'insert');
                $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_products ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

                if($result) {
                    $ary_status['new'] = $ary_status['new']+1;
                }
            } else {
                //è un prodotto già esistente, aggiorno i campi
                $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT id, " . $imploded_datalang . " FROM ecommerce_products WHERE `" . $discriminante . "` = :discriminante", array(":discriminante" => $discriminante_value), true);

                if($csv_row['product_code'] != "" && $MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_products WHERE product_code = :code AND id != :id", array(":code" => $csv_row['product_code'], ":id" => $r_old_data['id'])) != 0) {
                    continue;
                }

                if($csv_row['ean'] != "" && $MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_products WHERE ean = :code AND id != :id", array(":code" => $csv_row['ean'], ":id" => $r_old_data['id'])) != 0) {
                    continue;
                }

                foreach($check_datalang as $cur_datalang) {
                    if ($csv_row[$cur_datalang] != "") {
                        if ($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                                array('currentValue' => $csv_row[$cur_datalang], 'oldValue' => $r_old_data[$cur_datalang]),
                            )) == false) {
                            continue 2;
                        }

                        $csv_row[$cur_datalang] = $MSFrameworki18n->setFieldValue($r_old_data[$cur_datalang], $csv_row[$cur_datalang]);
                    }
                }

                if($csv_row['is_active'] == "") { //se non ci sono preferenze per il prodotto (attivo/non attivo) i prodotti esistenti mantengono lo stato precedente
                    unset($csv_row['is_active']);
                }

                $stringForDB = $MSFrameworkDatabase->createStringForDB($csv_row, 'update');
                $result = $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_products SET $stringForDB[1] WHERE `" . $discriminante . "` = :discriminante", array_merge(array(":discriminante" => $discriminante_value), $stringForDB[0]));

                if($result) {
                    $ary_status['updated'] = $ary_status['updated'] + 1;
                }
            }
        }
    }

    return array($ary_status, $total_rows);
}
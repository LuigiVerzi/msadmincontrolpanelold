<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */ ?>

<?php
require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkImposte = new \MSFramework\Ecommerce\imposte();

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <div id="form" class="form-horizontal wizard-big">

                        <h1>Importazione</h1>
                        <fieldset>
                            <div id="import_csv_form">

                                <div class="sk-spinner sk-spinner-double-bounce">
                                    <div class="sk-double-bounce1"></div>
                                    <div class="sk-double-bounce2"></div>
                                </div>

                                <div class="csv_import_step show" id="csv-1">
                                    <form action="#" class="dropzone" id="dropzoneForm">
                                        <div class="fallback">
                                            <input name="file" type="file"/>
                                        </div>
                                    </form>
                                    <br>
                                </div>

                                <div class="csv_import_step" id="csv-2" style="display: none;">
                                    <div class="alert alert-info">
                                        Se all'interno del modulo "prodotti" è già presente un elemento a parità di discriminante (es: Codice Prodotto), allora tale prodotto sarà aggiornato con i valori impostati nel CSV. Se il prodotto non è presente, verrà creato (a patto che nel CSV siano presenti almeno tutti i campi obbligatori del modulo "prodotti") <br /><br />
                                        Inserisci i prezzi <b><?php echo ($MSFrameworkImposte->getPriceType() == "0") ? "comprensivi delle imposte" : "al netto delle imposte" ?></b>. Puoi modificare quest'opzione all'interno delle impostazioni
                                    </div>

                                    <h3 style="padding-bottom: 5px;">Discriminante</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select class="form-control" id="discriminante">
                                                <option value="product_code">Codice Prodotto</option>
                                                <option value="ean">EAN</option>
                                            </select>
                                        </div>
                                    </div>

                                    <h3 style="padding-bottom: 5px;">Collega i campi con i valori corretti</h3>
                                    <div class="row">
                                        <div id="assegnazione_pannelli">

                                        </div>
                                    </div>
                                    <hr class="hr-line-dashed">
                                </div>
                            </div>
                        </fieldset>

                        <h1>Esportazione</h1>
                        <fieldset>
                            <form method="POST" action="ajax/exportCSV.php" target="_blank" id="export_csv_form" name="export_csv_form">

                                <p>Selezionare i campi da esportare:</p>

                                <p><input type="checkbox" checked="checked" class="flat" disabled> Nome</p>
                                <p><input type="checkbox" checked="checked" class="flat" disabled> Prezzo</p>
                                <p><input type="checkbox" checked="checked" name="product_code" class="flat"> Codice Prodotto</p>
                                <p><input type="checkbox" checked="checked" name="ean" class="flat"> EAN</p>
                                <p><input type="checkbox" checked="checked" name="warehouse_qty" class="flat"> Quantità in magazzino</p>
                                <p><input type="checkbox" checked="checked" name="is_active" class="flat"> Attivo (si/no)</p>
                                <p><input type="checkbox" checked="checked" name="short_descr" class="flat"> Descrizione Breve</p>
                                <p><input type="checkbox" checked="checked" name="long_descr" class="flat"> Descrizione Estesa</p>
                                <p><input type="checkbox" checked="checked" name="imposta" class="flat"> Imposta applicata</p>

                                <hr class="hr-line-dashed">
                            </form>
                        </fieldset>

                        <h1 class="tab-right"><i class="fas fa-sync-alt"></i> Sincronizzazione</h1>
                        <fieldset>
                            <div class="alert alert-info">
                                È possibile sincronizzare il catalogo dei prodotti con i seguenti strumenti di marketing.
                            </div>

                            <div class="ibox">
                                <div class="ibox-title">
                                    <i class="ibox-icon fab fa-facebook fa-3x" style="color: #4f7abe;"></i>
                                    <h5>Facebook Ads</h5>
                                </div>
                                <div class="ibox-content">
                                    <p>
                                        È possibile sincronizzare il catalogo dei prodotti con la piattaforma Ads di Facebook. <a href="https://www.facebook.com/business/help/125074381480892?id=725943027795860">Scopri di più</a>
                                    </p>
                                    <code>
                                        <?= $MSFrameworkCMS->getURLToSite() . 'm/ecommerceFeeds/facebook.csv'; ?>
                                    </code>
                                </div>
                            </div>


                            <div class="ibox">
                                <div class="ibox-title">
                                    <i class="ibox-icon fab fa-google fa-3x" style="color: #13a365;"></i>
                                    <h5>Google Ads</h5>
                                </div>
                                <div class="ibox-content">
                                    <p>
                                        È possibile sincronizzare il catalogo dei prodotti con la piattaforma Ads di Google. <a href="https://support.google.com/merchants/answer/1219255">Scopri di più</a>
                                    </p>
                                    <code>
                                        <?= $MSFrameworkCMS->getURLToSite() . 'm/ecommerceFeeds/googleads.csv'; ?>
                                    </code>
                                </div>
                            </div>

                        </fieldset>
                    </div>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>

<textarea id="template_assegnazione" style="display: none;">
    <div class="col-lg-6">
        <label class="col-form-label">COLONNA {id}</label>
        <p class="col-form-label">{valori}</p>
        <div class="">
            <select class="form-control m-b valori_colonna" name="valori_colonna" data-id="{id}">
                <option value=""></option>
                <option value="nome">Nome Prodotto</option>
                <option value="prezzo">Prezzo</option>
                <option value="product_code">Codice Prodotto</option>
                <option value="ean">EAN</option>
                <option value="warehouse_qty">Quantità in magazzino</option>
                <option value="is_active">Attivo (si/no)</option>
                <option value="short_descr">Descrizione Breve</option>
                <option value="long_descr">Descrizione Estesa</option>
                <option value="imposta">Imposta applicata</option>
            </select>
        </div>
        <hr class="hr-line-dashed">
    </div>
</textarea>

</body>
</html>


<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$filename = date("Y-m-d_His").".csv";

$eol = "\r\n";
$recipients = '';

$str_db = implode(", ", array_keys($_POST));
if($str_db != "") {
    $str_db = ", " . $str_db;
}
$result = $MSFrameworkDatabase->getAssoc("SELECT nome, prezzo $str_db FROM ecommerce_products");

$translatable_fields = array("short_descr", "long_descr");

$out = fopen('php://output', 'w');

foreach ($result as $row) {
    $row['nome'] = $MSFrameworki18n->getFieldValue($row['nome']);
    foreach(array_keys($_POST) as $db_el) {
        $row[$db_el] = strip_tags((in_array($db_el, $translatable_fields) ? $MSFrameworki18n->getFieldValue($row[$db_el]) : $row[$db_el]));
    }


    /*$recipients .= $MSFrameworki18n->getFieldValue($row['nome']) . ";";
    $recipients .= $row['prezzo'] . ";";
    foreach(array_keys($_POST) as $db_el) {
        $recipients .= ';'. str_replace(";", "\;", strip_tags((in_array($db_el, $translatable_fields) ? $MSFrameworki18n->getFieldValue($row[$db_el]) : $row[$db_el])));
    }

    $recipients .= $eol;*/


    fputcsv($out, array_values($row), ";", '"', '"');
}

fclose($out);

header('Content-Description: File Transfer'.$eol);
header('Content-Type: application/octet-stream'.$eol);
header('Content-Disposition: attachment; filename='.$filename.$eol);
header('Expires: 0'.$eol);
header('Cache-Control: must-revalidate'.$eol);
header('Pragma: public'.$eol);
header('Content-Length: '.strlen($out).$eol);
echo $out;
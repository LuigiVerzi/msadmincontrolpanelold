<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th width="80px" class="no-sort">Anteprima</th>
                        <th class="default_sort" data-sort="DESC" width="50px">ID</th>
                        <th>Nome</th>
                        <th>Categoria</th>
                        <th>Prezzo</th>
                        <th>In vendita</th>
                        <th>In evidenza</th>
                        <?php if($MSFrameworkDatabase->getCount("SELECT id FROM `ecommerce_products` WHERE product_code != '' LIMIT 1")) { ?>
                        <th width="10%">Codice Univoco</th>
                        <?php } ?>
                        <th width="30px">Visite</th>
                    </tr>
                    </thead>
                </table>

            </div>
        </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$_POST['pVariantData'] = json_decode($_POST['pVariantData'], true);

$MSFrameworkEcommerceAttributes = new \MSFramework\Ecommerce\attributes();

$dettagliLinguaCorrente = $MSFrameworki18n->getCurrentLanguageDetails();
$dettagliLinguaPrincipale = $MSFrameworki18n->getLanguagesDetails($MSFrameworki18n->getPrimaryLangId())[$MSFrameworki18n->getPrimaryLangId()];

if($_POST['pSavingAsNew'] == "true" && $_POST['pID'] != "") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT gallery, variants, attachments FROM ecommerce_products WHERE id = :id", array(":id" => $_POST['pID']), true);
    $old_img = json_decode($r_old_data['gallery'], true);
    $old_variants = json_decode($r_old_data['variants'], true);
    $old_attachments = json_decode($r_old_data['attachments'], true);

    foreach($old_img as $single_file) {
        $expl_file = explode("_", $single_file);
        $expl_file[0] = time();
        $new_filename = implode("_", $expl_file);

        foreach($_POST['pImagesAry'] as $kfile => $file) {
            if($file == $single_file) {
                $_POST['pImagesAry'][$kfile] = $new_filename;
            }
        }

        copy (UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN . $single_file, UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN . $new_filename);
        copy (UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN . "tn/" . $single_file, UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN . "tn/" . $new_filename);
    }

    foreach($old_variants as $old_variants_v) {
        foreach($old_variants_v['data']['gallery'] as $single_file) {
            $expl_file = explode("_", $single_file);
            $expl_file[0] = time();
            $new_filename = implode("_", $expl_file);

            foreach ($_POST['pVariantData'] as $kField => $vField) {
                foreach ($vField['data']['gallery'] as $kfile => $file) {
                    if($file == $single_file) {
                        $_POST['pVariantData'][$kField]['data']['gallery'][$kfile] = $new_filename;
                    }
                }
            }

            copy (UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN . $single_file, UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN . $new_filename);
            copy (UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN . "tn/" . $single_file, UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN . "tn/" . $new_filename);
        }
    }

    foreach($old_attachments as $kField => $old_attachments_v) {

        $single_file = $old_attachments_v['file'][0];

        $expl_file = explode("_", $single_file);
        $expl_file[0] = time();
        $new_filename = implode("_", $expl_file);

        $_POST['pAttachmentsData'][$kField]['file'] = array($new_filename);

        copy (UPLOAD_ECOMMERCE_ATTACHMENTS_FOR_DOMAIN . $single_file, UPLOAD_ECOMMERCE_ATTACHMENTS_FOR_DOMAIN . $new_filename);
        copy (UPLOAD_ECOMMERCE_ATTACHMENTS_FOR_DOMAIN . "tn/" . $single_file, UPLOAD_ECOMMERCE_ATTACHMENTS_FOR_DOMAIN . "tn/" . $new_filename);
    }

    $_POST['pID'] = "";
}

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;
if($_POST['pNome'] == "" || $_POST['pSlug'] == "" || $_POST['pPrezzo'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "ecommerce_products", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

$ts_scadenza = "";
if($_POST['pPrezzoPromoExpire'] != "") {
    $ary_scadenza = explode("/", $_POST['pPrezzoPromoExpire']);
    $ts_scadenza = mktime(23, 59, 59, $ary_scadenza[1], $ary_scadenza[0], $ary_scadenza[2]);
    if(!$ts_scadenza) {
        echo json_encode(array("status" => "expire_not_valid"));
        die();
    }
    $_POST['pPrezzoPromoExpire'] = $ts_scadenza;
}

$array_var_prezzi = array('pPrezzo', 'pPrezzoPromo');
foreach($array_var_prezzi as $curPostPrice) {
    $_POST[$curPostPrice] = str_replace(",", ".", $_POST[$curPostPrice]);
    if(!strstr($_POST[$curPostPrice], ".") && $_POST[$curPostPrice] != "") {
        $_POST[$curPostPrice] .= ".00";
    }

    if(!is_numeric($_POST[$curPostPrice]) && $_POST[$curPostPrice] != "") {
        echo json_encode(array("status" => "price_numeric"));
        die();
    }
}

if(!is_numeric($_POST['pWeight']) && $_POST['pWeight'] != "") {
    echo json_encode(array("status" => "weight_numeric"));
    die();
}

$array_var_prezzi = array('prezzo', 'prezzo_promo');
foreach($_POST['pVariantData'] as $k => $v) {

    foreach($array_var_prezzi as $keyname) {
        $_POST['pVariantData'][$k]['data'][$keyname] = str_replace(",", ".", $_POST['pVariantData'][$k]['data'][$keyname]);

        if(!strstr($_POST['pVariantData'][$k]['data'][$keyname], ".") && $_POST['pVariantData'][$k]['data'][$keyname] != "") {
            $_POST['pVariantData'][$k]['data'][$keyname] .= ".00";
        }

        if(!is_numeric($_POST['pVariantData'][$k]['data'][$keyname]) && $_POST['pVariantData'][$k]['data'][$keyname] != "") {
            echo json_encode(array("status" => "price_numeric"));
            die();
        }
    }

    if($_POST['pVariantData'][$k]['data']['promo_price_expire'] != "") {
        $ary_scadenza = explode("/", $_POST['pVariantData'][$k]['data']['promo_price_expire']);
        $ts_scadenza = mktime(23, 59, 59, $ary_scadenza[1], $ary_scadenza[0], $ary_scadenza[2]);
        if(!$ts_scadenza) {
            echo json_encode(array("status" => "expire_not_valid"));
            die();
        }
        $_POST['pVariantData'][$k]['data']['promo_price_expire'] = $ts_scadenza;
    }

}

foreach($_POST['pAttachmentsData'] as $k => $v) {

    if($_POST['pVariantData'][$k]['data']['promo_price_expire'] != "") {
        $ary_scadenza = explode("/", $_POST['pVariantData'][$k]['data']['promo_price_expire']);
        $ts_scadenza = mktime(23, 59, 59, $ary_scadenza[1], $ary_scadenza[0], $ary_scadenza[2]);
        if(!$ts_scadenza) {
            echo json_encode(array("status" => "expire_not_valid"));
            die();
        }
        $_POST['pVariantData'][$k]['data']['promo_price_expire'] = $ts_scadenza;
    }

}

$uploader = new \MSFramework\uploads('ECOMMERCE_PRODUCTS');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$_POST['pShortDescr'] = $uploader->saveFromBlob($_POST['pShortDescr']);
$_POST['pLongDescr'] = $uploader->saveFromBlob($_POST['pLongDescr']);

$ary_files_variations = array();
foreach ($_POST['pVariantData'] as $kField => $vField) {
    $uploaderVariants = new \MSFramework\uploads('ECOMMERCE_PRODUCTS');

    $cur_files = $uploaderVariants->prepareForSave($vField['data']['gallery']);
    if($cur_files === false) {
        echo json_encode(array("status" => "mv_error"));
        die();
    }

    $ary_files_variations = array_merge($ary_files_variations, $cur_files);
}

if($_POST['pProductCode'] != "" && $MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_products WHERE product_code = :code AND id != :id", array(":code" => $_POST['pProductCode'], ":id" => $_POST['pID'])) != 0) {
    echo json_encode(array("status" => "product_code_already_exists"));
    die();
}

if($_POST['pEAN'] != "" && $MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_products WHERE ean = :code AND id != :id", array(":code" => $_POST['pEAN'], ":id" => $_POST['pID'])) != 0) {
    echo json_encode(array("status" => "ean_already_exists"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT gallery, nome, slug, long_descr, short_descr, variants, attachments, custom_message, seo FROM ecommerce_products WHERE id = :id", array(":id" => $_POST['pID']), true);
    $old_img = json_decode($r_old_data['gallery'], true);

    $old_custom_message_title = '';
    $old_custom_message_placeholder = '';
    if(empty($r_old_data['custom_message'])) {
        $old_custom_message_json = json_decode($r_old_data['custom_message'], true);
        $old_custom_message_title = $old_custom_message_json['title'];
        $old_custom_message_placeholder = $old_custom_message_json['placeholder'];
    }

    $old_variants = json_decode($r_old_data['variants'], true);
    $old_img_variations = array();
    foreach($old_variants as $old_variants_v) {
        foreach($old_variants_v['data']['gallery'] as $single_file) {
            $old_img_variations[] = $single_file;
        }
    }

    $old_attachments = json_decode($r_old_data['attachments'], true);
    foreach($old_attachments as $old_attachments_v) {
        foreach($old_attachments_v['file'] as $single_file) {
            $old_file_attachments[] = $single_file;
        }
    }
}

// Salvo i dati relativi al salvataggio sul DB degli attributi custom per usarli successivamente anche nelle varianti
$custom_attributes = array();

foreach($_POST['pAttributesData'] as $varK => $varV) {
    $clean_id = $MSFrameworkEcommerceAttributes->jsonPrefix('attribute', 'remove', $varV[0]);


    $attribute_values = $varV[1];
    if(!is_array($varV[1])) {
        $attribute_values = array($varV[1]);
    }

    foreach($attribute_values as $attribute_key => $attribute_value) {
        if ($MSFrameworkEcommerceAttributes->isCustomAttr($varV[0], $attribute_value)) {

            if ($MSFrameworki18n->getPrimaryLangId() == $dettagliLinguaCorrente['id']) {
                $ary_nome = array($dettagliLinguaCorrente['long_code'] => $attribute_value);
            } else {
                $ary_nome = array($dettagliLinguaCorrente['long_code'] => $attribute_value, $dettagliLinguaPrincipale['long_code'] => $attribute_value);
            }

            $array_to_save = array(
                "nome" => json_encode($ary_nome),
                "child_of" => $clean_id,
            );

            // Controllo se esiste già un attributo con lo stesso nome
            //(Perchè può capitare che il salvataggio del prodotto avvenga più volte senza aggiornare la pagina e quindi venga nuovamente passato il nome invece che l'ID)
            $check_attribute_with_same_name = $MSFrameworkDatabase->getAssoc("SELECT id, nome FROM ecommerce_attributes_values WHERE nome = :nome AND child_of = :child_of", array(':nome' => $array_to_save['nome'], ':child_of' => $array_to_save['child_of']), true);

            if(isset($check_attribute_with_same_name['id'])) {
                $last_insert_id = $check_attribute_with_same_name['id'];
            } else {
                $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
                $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_attributes_values ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
                $last_insert_id = $MSFrameworkDatabase->lastInsertId();
            }

            if(is_array($varV[1])) {
                $_POST['pAttributesData'][$varK][1][$attribute_key] = $MSFrameworkEcommerceAttributes->jsonPrefix('attribute_value', 'add', $last_insert_id);
            } else {
                $_POST['pAttributesData'][$varK][1] = $MSFrameworkEcommerceAttributes->jsonPrefix('attribute_value', 'add', $last_insert_id);
            }

            $custom_attributes[$attribute_value] = $last_insert_id;

        }
    }
}

// Sostituisco il nome dell'attributo CUSTOM con il suo ID (Ottenuto precedentemente)
if(count($custom_attributes) > 0) {
    foreach ($_POST['pVariantData'] as $varK => $varV) {
        foreach($varV['type'] as $variant_key => $variant_value) {
            if(in_array($variant_value, array_keys($custom_attributes))) {
                $_POST['pVariantData'][$varK]['type'][$variant_key] = $custom_attributes[$variant_value];
            }
        }
    }
}

$ary_files_attachments = array();
foreach ($_POST['pAttachmentsData'] as $kField => $vField) {

    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $vField['name'], 'oldValue' => $old_attachments[$kField]['name'])
        )) == false) {
        $old_attachments[$kField]['name'] = json_encode(array($dettagliLinguaPrincipale['long_code'] => $vField['name']));
    }

    $_POST['pAttachmentsData'][$kField]['name'] = $MSFrameworki18n->setFieldValue($old_attachments[$kField]['name'], $vField['name']);

    $uploaderAttachments = new \MSFramework\uploads('ECOMMERCE_ATTACHMENTS');

    $cur_files = $uploaderAttachments->prepareForSave($vField['file'],
        array(
            "png",
            "jpeg",
            "jpg",
            "pdf",
            "rar",
            "zip",
            "doc",
            "docx",
            "ppt",
            "svg",
            "txt",
            "xls",
            "xlsx",
            "xml"
        )
    );

    if($cur_files === false) {
        echo json_encode(array("status" => "mv_error"));
        die();
    }

    $ary_files_attachments = array_merge($ary_files_attachments, $cur_files);
}

// Imposto le traduzioni per i campi delle varianti
foreach ($_POST['pVariantData'] as $kField => $vField) {

    $old_translation = array('short_descr' => json_encode(array($dettagliLinguaPrincipale['long_code'] => '')));

    foreach($old_variants as $old_variant_v) {

        if($old_variant_v['type'] === $vField['type']) {
            //controlli di sicurezza per le traduzioni
            foreach($old_translation as $trans_k => $trans_v) {
                if ($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                        array('currentValue' => $vField['data'][$trans_k], 'oldValue' => $old_variant_v['data'][$trans_k])
                    )) == false) {
                    echo json_encode(array("status" => "no_datalang_for_primary"));
                    die();
                }
                $old_translation[$trans_k] = $old_variant_v['data'][$trans_k];
            }

        }
    }

    $_POST['pVariantData'][$kField]['data']['short_descr'] = $MSFrameworki18n->setFieldValue($old_translation['short_descr'], $vField['data']['short_descr']);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
        array('currentValue' => $_POST['pLongDescr'], 'oldValue' => $r_old_data['long_descr']),
        array('currentValue' => $_POST['pShortDescr'], 'oldValue' => $r_old_data['short_descr']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}


$old_seo_data = (json_decode($r_old_data['seo']) ? json_decode($r_old_data['seo'], true) : array());
foreach($_POST['pSeoData'] as $seo_k => $seo_v) {
    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $seo_v, 'oldValue' => $old_seo_data[$seo_k]),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $_POST['pSeoData'][$seo_k] = $MSFrameworki18n->setFieldValue($old_seo_data[$seo_k], $seo_v);
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
    "category" => implode(',', $_POST['pCategory']),
    "is_highlighted" => $_POST['pIsHighlighted'],
    "is_active" => $_POST['pIsActive'],
    "long_descr" => $MSFrameworki18n->setFieldValue($r_old_data['long_descr'], $_POST['pLongDescr']),
    "short_descr" => $MSFrameworki18n->setFieldValue($r_old_data['short_descr'], $_POST['pShortDescr']),
    "gallery" => json_encode($ary_files),
    "attributes" => json_encode($_POST['pAttributesData']),
    "variants" => json_encode($_POST['pVariantData']),
    "attachments" => json_encode($_POST['pAttachmentsData']),
    "prezzo" => $_POST['pPrezzo'],
    "prezzo_promo" => $_POST['pPrezzoPromo'],
    "prezzo_promo_expire" => $_POST['pPrezzoPromoExpire'],
    "auto_renew_promo" => $_POST['pAutoRenewPromo'],
    "show_lowest_price" => $_POST['pShowLowestPrice'],
    "imposta" => $_POST['pImposta'],
    "manage_warehouse" => $_POST['pManageWarehouse'],
    "manage_reviews" => $_POST['pManageReviews'],
    "moderate_reviews" => $_POST['pModerateReviews'],
    "enable_questions" => $_POST['pEnableQuestions'],
    "warehouse_qty" => $_POST['pWarehouseQty'],
    "enable_custom_message" => $_POST['pEnableCustomMessage'],
    "ean" => $_POST['pEAN'],
    "product_code" => $_POST['pProductCode'],
    "weight" => $_POST['pWeight'],
    "custom_message" => ($_POST['pEnableCustomMessage'] ? json_encode(array('required' => $_POST['pRequiredCustomMessage'], 'title' => $MSFrameworki18n->setFieldValue($old_custom_message_title, $_POST['pCustomMessageTitle']), 'placeholder' => $MSFrameworki18n->setFieldValue($old_custom_message_title, $_POST['pCustomMessagePlaceholder']))) : ''),
    "up_sell" => json_encode($_POST['pUpSellProd']),
    "cross_sell" => json_encode($_POST['pCrossSellProd']),
    "seo" => json_encode($_POST['pSeoData'])
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_products ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $record_id = $MSFrameworkDatabase->lastInsertId();
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_products SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $record_id = $_POST['pID'];
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
        $uploaderVariants->unlink($ary_files_variations);
        $uploaderVariants->unlink($ary_files_attachments);


        $uploader->unlinkFromHTML($_POST['pShortDescr']);
        $uploader->unlinkFromHTML($_POST['pLongDescr']);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, $old_img);
    $uploaderVariants->unlink($ary_files_variations, $old_img_variations);
    $uploaderVariants->unlink($ary_files_attachments, $old_file_attachments);

    $uploader->unlinkFromHTML($_POST['pShortDescr'], $r_old_data['short_descr']);
    $uploader->unlinkFromHTML($_POST['pLongDescr'], $r_old_data['long_descr']);

}

foreach($_POST['pChangedReviewStatus'] as $reviewID => $newStatus) {
    if($newStatus == "del") {
        $MSFrameworkDatabase->deleteRow("ecommerce_product_reviews", "id", $reviewID);
    } else {
        $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_product_reviews SET status = :status WHERE id = :id", array(":id" => $reviewID, ":status" => $newStatus));
    }
}

foreach($_POST['pChangedQuestionStatus'] as $questionID => $newDataAry) {
    if($newDataAry[0] == "del") {
        $MSFrameworkDatabase->deleteRow("ecommerce_product_questions", "id", $questionID);
    } else {

        $before_save_question = $MSFrameworkDatabase->getAssoc('SELECT * FROM ecommerce_product_questions WHERE id = :id', array(":id" => $questionID), true);

        $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_product_questions SET status = :status, answer = :answer, answer_date = :answer_date WHERE id = :id", array(":id" => $questionID, ":status" => $newDataAry[0], ":answer" => $newDataAry[1], ":answer_date" => date("Y-m-d H:i:s")));

        // Invia l'email all'utente
        if($before_save_question && empty($before_save_question['answer']) && !empty($newDataAry[1])) {
            (new \MSFramework\Ecommerce\questions())->sendNewAnswerEmail($questionID);
        }
    }
}

$MSFrameworkUrl->makeRedirectIfNeeded('ecommerce-product-' . $record_id, (new \MSFramework\Ecommerce\products())->getURL($record_id), ($db_action === 'update' ? (new \MSFramework\Ecommerce\products())->getURL($r_old_data) : ''));

echo json_encode(array("status" => "ok", "id" => $record_id));
die();
?>
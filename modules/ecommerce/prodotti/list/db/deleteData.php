<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
$contents_html = array();
$variants_images_to_delete = array();
foreach((new \MSFramework\Ecommerce\products())->getProductDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['gallery'], true));
    foreach(json_decode($r['variants'], true) as $old_variants_v) {
        $variants_images_to_delete = array_merge_recursive($variants_images_to_delete, $old_variants_v['data']['gallery']);
    }
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['gallery'], true));

    $contents_html[] = $r['short_descr'];
    $contents_html[] = $r['long_descr'];

}

if($MSFrameworkDatabase->deleteRow("ecommerce_products", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('ECOMMERCE_PRODUCTS'))->unlink($images_to_delete);
    (new \MSFramework\uploads('ECOMMERCE_PRODUCTS'))->unlink($variants_images_to_delete);


    foreach($contents_html as $html) {
        (new \MSFramework\uploads('ECOMMERCE_PRODUCTS'))->unlinkFromHTML($html);
    }

    $MSFrameworkUrl->deleteRedirectsByReferences('ecommerce-product', $_POST['pID']);

    $MSFrameworkDatabase->deleteRow("ecommerce_product_reviews", "product_id", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("ecommerce_product_questions", "product_id", $_POST['pID']);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


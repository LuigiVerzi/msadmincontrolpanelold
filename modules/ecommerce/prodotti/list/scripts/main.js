$(document).ready(function() {
    loadStandardDataTable('main_table_list', true);
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    if($('#record_id').val() == "") {
        $('#editSaveAsNewBtn').remove();
    }

    initClassicTabsEditForm();
    initSlugChecker('nome', 'slug', 'ecommerce_products', $('#record_id'));

    $('.input-group.date.prezzo_promo_expire, .input-group.date.variation_promo_price_expire_dt').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
        startDate: new Date(),
    });

    $('.attrDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi attributo",
        "deleteButtonText": "Elimina attributo",
        "afterAddCallback": function() {
            $('.attrDuplication:last .select2-container').remove();

            $('.attrDuplication:last').find('.product_attribute').val('');
            $('.attrDuplication:last').find('.product_attribute_values').val('');
            $('.attrDuplication:last').find('.product_attribute_values_container').hide();

            initAttrSelects('.product_attribute:last, .product_attribute_values:last');

            disableSelectOptions();
            checkIfCanAddMoreAttr();
        },
        "afterDeleteCallback": function() {
            disableSelectOptions();
            checkIfCanAddMoreAttr();
            composeVariationRows();
        },
    });

    if($('.attachmentsDuplication').length) {
        initAttachmentsDuplications();
    }

    initOrak('images', 25, 500, 300, 200, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);

    initAttrSelects();
    checkIfCanAddMoreAttr();
    disableSelectOptions();
    initProductsSelect2($('#up_sell_products, #cross_sell_products'));

    $('#createVarFromAllAttr').on('click', function() {
        composeVariationRows(true);
    });

    $('#deleteAllVar').on('click', function() {
        bootbox.confirm('Sei sicuro di voler eliminare tutte le variazioni? I dati non salvati andranno persi.', function(result) {
            if(result) {
                $('.variations_container .variationDuplication').not(':first').remove();

                $('.variations_container .variation_value_select').val('');

                $('.variations_container .variation_price').val('');
                $('.variations_container .variation_promo_price').val('');
                $('.variations_container .variation_auto_renew_promo').val('');

                $('.variations_container .variation_manage_warehouse').iCheck('uncheck');
                $('.variations_container .variation_warehouse_qty').val('');

                $('.variations_container .variation_short_descr').replaceWith('<textarea class="variation_short_descr"></textarea>');
                $('.variations_container [role="application"]').remove();

                $('.variations_container .variation_sku').val('');

                initEditorForVariations();
            }
        })
    });

    $('#manage_warehouse').on('ifToggled', function(event) {
        checkIfShowWarehouseQty();
    });

    $('#enable_custom_message').on('ifToggled', function(event) {
        checkIfShowCustomMessage();
    });

    checkIfShowWarehouseQty();
    checkIfShowCustomMessage();
    initGalleryForAttachments();

    $(document).ready(function () {
        composeVariationRows();
    });

    $('#category').chosen();

    initTinyMCE( '#short_descr, #long_descr');

    var inputs = [{
        preview_title:  [$('#seo_titolo')],
        preview_url: false,
        preview_description: [$('#seo_description')]
    },{
        preview_title: [$('#nome')],
        preview_url: [$('#baseElementPathSW'), $('#slug')],
        preview_description: false
    }];

    $("#serpPreview").serpPreview(inputs);
}

function initVariationDuplications() {
    $('.variationDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary m-t",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi variazione",
        "deleteButtonText": "Elimina variazione",
        "beforeAddCallback": function() {
            $('.variationDuplication').find('.variation_manage_warehouse').iCheck('destroy');
        },
        "afterAddCallback": function() {
            $('.variationDuplication:last').find('.variation_value_select').val('');
            $('.variationDuplication:last').find('.variation_manage_warehouse').attr('checked', false);

            $('.variationDuplication:last').find('.variation_price').val('');
            $('.variationDuplication:last').find('.variation_promo_price').val('');
            $('.variationDuplication:last').find('.variation_auto_renew_promo').val('');

            $('.variationDuplication:last').find('.variation_manage_warehouse_field').hide();
            $('.variationDuplication:last').find('.variation_warehouse_qty').val('');

            $('.variationDuplication:last').find('.variation_sku').val('');

            $('.variationDuplication:last').find('.variation_short_descr').replaceWith('<textarea class="variation_short_descr"></textarea>');
            $('.variationDuplication:last').find('[role="application"]').remove();

            $('.variationDuplication:last').find('.prev_upl_variationimage').html('');
            $('.variationDuplication:last').find('.variationimage_container .clone_item').remove();
            $('.variationDuplication:last').find('.variationimage_container [id$="DDArea"]').remove();
            $('.variationDuplication:last').find('.variationimage_container').append('<div id="variationimage_x" class="variationimage" orakuploader="on"></div>');

            initIChecks();
            initMSTooltip();
            initCheckWarehouseForVariations();
            initGalleryForVariations();
            initEditorForVariations();
        },
        "afterDeleteCallback": function() {
        },
    });
}

function initGalleryForVariations() {
    $('.variationDuplication').each(function(index) {
        if($('.variationimage_container:eq(' + index + ') #variationimage_' + index + 'DDArea').length != 0) {
            return true;
        }

        $('.variationimage_container:eq(' + index + ') .variationimage').attr('id', 'variationimage_' + index);
        $('.variationimage_container:eq(' + index + ') .variationimage_realPath').attr('id', 'variationimage_' + index + "_realPath");

        preattach_array_variationimage = new Array();
        if ($('.prev_upl_variationimage:eq(' + index + ')').val() != "") {
            img_to_cycle = $.parseJSON($('.prev_upl_variationimage:eq(' + index + ')').val()) || new Array();
            $(img_to_cycle).each(function (k, v) {
                preattach_array_variationimage.push(v);
            })
        }

        initOrak('variationimage_' + index, 25, 500, 300, 200, preattach_array_variationimage, false, ['image/jpeg', 'image/png']);
    })
}

function initEditorForVariations() {
    $('.variationDuplication').each(function(index) {
        if(!$(this).find('[role="application"]').length) {
            initTinyMCE($(this).find('.variation_short_descr'));
        }
    })
}


function initAttachmentsDuplications() {
    $('.attachmentsDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary m-t",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi Allegato",
        "deleteButtonText": "Elimina Allegato",
        "beforeAddCallback": function() {
            $('.attachmentsDuplication').find('.only_for_users').iCheck('destroy');
        },
        "afterAddCallback": function() {
            $('.attachmentsDuplication:last').find('.attachment_name').val('');
            $('.attachmentsDuplication:last').find('.only_for_users').attr('checked', false);

            $('.attachmentsDuplication:last').find('.prev_upl_attachmentfile').html('');
            $('.attachmentsDuplication:last').find('.attachmentfile_container .clone_item').remove();
            $('.attachmentsDuplication:last').find('.attachmentfile_container [id$="DDArea"]').remove();
            $('.attachmentsDuplication:last').find('.attachmentfile_container').append('<div id="attachmentfile_x" class="attachmentfile" orakuploader="on"></div>');

            initIChecks();
            initMSTooltip();
            initGalleryForAttachments();
        },
        "afterDeleteCallback": function() {
        },
    });
}

function initGalleryForAttachments() {
    $('.attachmentsDuplication').each(function(index) {
        if($('.attachmentfile_container:eq(' + index + ') #attachmentfile_' + index + 'DDArea').length != 0) {
            return true;
        }

        $('.attachmentfile_container:eq(' + index + ') .attachmentfile').attr('id', 'attachmentfile_' + index);
        $('.attachmentfile_container:eq(' + index + ') .attachmentfile_realPath').attr('id', 'attachmentfile_' + index + "_realPath");

        preattach_array_attachmentfile = new Array();
        if ($('.prev_upl_attachmentfile:eq(' + index + ')').val() != "") {
            img_to_cycle = $.parseJSON($('.prev_upl_attachmentfile:eq(' + index + ')').val()) || new Array();
            $(img_to_cycle).each(function (k, v) {
                preattach_array_attachmentfile.push(v);
            })
        }

        initOrak('attachmentfile_' + index, 1, 10, 10, 200, preattach_array_attachmentfile, false,
            [
                'image/jpeg',
                'image/png',
                'application/pdf',
                'application/msword',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'text/plain',
                'application/zip',
                'application/x-rar',
                'text/xml',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]
        );
    })
}

function checkIfShowWarehouseQty() {
    if($('#manage_warehouse:checked').length == 1) {
        $('#manage_warehouse_field').show();
    } else {
        $('#manage_warehouse_field').hide();
        $('#warehouse_qty').val('');
    }
}

function checkIfShowCustomMessage() {

    if($('#enable_custom_message:checked').length == 1) {
        $('#custom_message_title_field, #custom_message_placeholder_field, #required_custom_message_field').show();
    } else {
        $('#custom_message_title_field, #custom_message_placeholder_field, #required_custom_message_field').hide();
        $('#custom_message_placeholder, #custom_message_title').val('');
        $('#required_custom_message').iCheck('uncheck');
    }
}

function initAttrSelects(selector) {
    if(typeof(selector) == "undefined") {
        selector = ".product_attribute, .product_attribute_values";
    }

    $(selector).select2();

    $('.product_attribute').off('select2:select');
    $('.product_attribute').on('select2:select', function (e) {
        keep_this = this;

        $(keep_this).closest('div.row').find('.product_attribute_values_container').hide();
        $(keep_this).closest('div.row').find('.product_attribute_values').html('<option value=""></option>');

        if(e.params.data.id != "") {
            $.ajax({
                url: "ajax/getAttrOptions.php",
                type: "POST",
                data: {
                    "pID": e.params.data.id,
                },
                async: true,
                dataType: "json",
                success: function(data) {
                    if(data[0] == false) {
                        $(keep_this).closest('div.row').find('.product_attribute_values').attr('multiple', false);
                        $(keep_this).closest('div.row').find('.product_attribute_values').attr('data-tags', 'true');
                    } else {
                        $(keep_this).closest('div.row').find('.product_attribute_values').attr('multiple', 'multiple');
                        $(keep_this).closest('div.row').find('.product_attribute_values').attr('data-tags', 'true');
                    }

                    $(keep_this).closest('div.row').find('.product_attribute_values').html(data[1]);
                    $(keep_this).closest('div.row').find('.product_attribute_values').select2({
                        "language": {
                            "noResults": function(){
                                return "Cerca tra i valori esistenti o inseriscine uno nuovo";
                            }
                        },
                        tags: true,
                    }).trigger('change.select2');

                    $(keep_this).closest('div.row').find('.product_attribute_values_container').show();

                    composeVariationRows();
                    disableSelectOptions();
                }
            })
        } else {
            composeVariationRows();
            disableSelectOptions();
        }
    });

    $('.product_attribute_values').off('select2:select');
    $('.product_attribute_values').on('select2:select', function (e) {
        composeVariationRows();
    })

    $('.product_attribute_values').off('select2:unselect');
    $('.product_attribute_values').on('select2:unselect', function (e) {
        composeVariationRows();
    })
}

function composeVariationRows(auto_create) {
    if(typeof(auto_create) == "undefined") {
        auto_create = false;
    }

    if($('#pAttributesData').length) {
        var data = {
            "pAttributesData": JSON.parse($('#pAttributesData').val()),
            "pCurVariantData": JSON.parse($('#pCurVariantData').val()),
            "pAutoCreate": auto_create,
        }
    } else {
        var data = {
            "pAttributesData": getAttributesAry(),
            "pCurVariantData": getVariantsAry(),
            "pAutoCreate": auto_create,
        }
    }

    $.ajax({
        url: "ajax/composeVariationRows.php",
        type: "POST",
        data: data,
        async: true,
        dataType: "html",
        success: function(data) {
            $('.variations_container').html(data);

            initIChecks();
            initMSTooltip();
            initCheckWarehouseForVariations();
            initGalleryForVariations();
            initEditorForVariations();
            initVariationDuplications();
        }
    })
}

function initCheckWarehouseForVariations() {
    $('.variation_manage_warehouse').unbind('ifToggled');
    $('.variation_manage_warehouse').on('ifToggled', function(event) {
        index = $(".variation_manage_warehouse").index($(this));

        if($('.variation_manage_warehouse:eq(' + index + '):checked').length == 1) {
            $('.variation_manage_warehouse_field:eq(' + index + ')').show();
        } else {
            $('.variation_manage_warehouse_field:eq(' + index + ')').hide();
            $('.variation_warehouse_qty:eq(' + index + ')').val('');
        }
    })
}

function disableSelectOptions() {
    $('.product_attribute').find('option').prop('disabled', false);
    $('.product_attribute').select2().trigger('change.select2');

    $('.product_attribute').each(function() {
        if($(this).val() == "") {
            return true;
        }

        $('.product_attribute').not($(this)).find('option[value=' + $(this).val() + ']').attr('disabled', 'disabled');
        $('.product_attribute').select2().trigger('change.select2');
    })
}

function getAttributesAry() {
    attributes_ary = new Object();

    var row_index = 0;
    $('.main_product_attribute_values').each(function() {
        attributes_ary_tmp = new Array();

        data_val = $(this).data('id');
        if(data_val == "") {
            return true;
        }

        attributes_ary_tmp.push(data_val);
        attributes_ary_tmp.push($(this).val());

        attributes_ary[row_index] = attributes_ary_tmp;

        row_index++;
    });

    $('.stepsDuplication.attrDuplication').each(function() {
        attributes_ary_tmp = new Array();

        data_val = $(this).find('.product_attribute').val();
        if(data_val == "") {
            return true;
        }

        attributes_ary_tmp.push(data_val);
        attributes_ary_tmp.push($(this).find('.product_attribute_values').val());

        attributes_ary[row_index] = attributes_ary_tmp;

        row_index++;
    });

    return attributes_ary;
}

function getVariantsAry() {
    variants_ary = new Object();
    $('.stepsDuplication.variationDuplication').each(function(row_index) {
        variants_ary_tmp_type = new Object();
        variants_ary_tmp_data = new Object();

        $(this).find('.variation_value_select').each(function() {
            variants_ary_tmp_type[$(this).attr('attr-id')] = $(this).val();
        });

        variants_ary_tmp_data['prezzo'] = $(this).find('.variation_price').val();
        variants_ary_tmp_data['prezzo_promo'] = $(this).find('.variation_promo_price').val();
        variants_ary_tmp_data['auto_renew_promo'] = $(this).find('.variation_auto_renew_promo').val();
        variants_ary_tmp_data['promo_price_expire'] = $(this).find('.variation_promo_price_expire').val();
        variants_ary_tmp_data['manage_warehouse'] = $(this).find('.variation_manage_warehouse:checked').length;
        variants_ary_tmp_data['warehouse_qty'] = $(this).find('.variation_warehouse_qty').val();
        variants_ary_tmp_data['short_descr'] = tinymce.get($(this).find('.variation_short_descr').attr('id')).getContent();
        variants_ary_tmp_data['sku'] = $(this).find('.variation_sku').val();

        variationimages_ary = new Array();
        $(this).find('input[name^=variationimage]').each(function(k, v) {
            variationimages_ary.push($(this).val());
        })
        variants_ary_tmp_data['gallery'] = variationimages_ary;

        variants_ary[row_index] = {
            "type": variants_ary_tmp_type,
            "data": variants_ary_tmp_data,
        };
    });

    return variants_ary;
}

function getAttachmentsAry() {
    attachments_ary = new Object();
    if($('.attachmentsDuplication').length) {
        $('.stepsDuplication.attachmentsDuplication').each(function (row_index) {
            var attachments_ary_tmp_data = new Object();

            attachments_ary_tmp_data['name'] = $(this).find('.attachment_name').val();
            attachments_ary_tmp_data['only_for_users'] = $(this).find('.only_for_users:checked').length;

            var attachmentfile_ary = new Array();
            $(this).find('input[name^=attachmentfile]').each(function (k, v) {
                attachmentfile_ary.push($(this).val());
            })

            attachments_ary_tmp_data['file'] = attachmentfile_ary;
            attachments_ary[row_index] = attachments_ary_tmp_data;
        });
    }

    return attachments_ary;
}

function moduleSaveFunction(save_as_new) {
    changed_review_status = new Object;
    $('.review_row').each(function() {
        if($(this).attr('data-review-original-status') != $(this).find('.review_status').val()) {
            changed_review_status[$(this).attr('data-review-id')] = $(this).find('.review_status').val();
        }
    })

    changed_question_status = new Object;
    $('.question_row').each(function() {
        if($(this).attr('data-question-original-status') != $(this).find('.question_status').val() || $(this).attr('data-question-original-answer') != $(this).find('.question_answer').val()) {
            changed_question_status[$(this).attr('data-question-id')] = [$(this).find('.question_status').val(), $(this).find('.question_answer').val()];
        }
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pSavingAsNew": save_as_new,
            "pNome": $('#nome').val(),
            "pCategory": $('#category').val(),
            "pSlug": $('#slug').val(),
            "pLongDescr": tinymce.get('long_descr').getContent(),
            "pShortDescr": tinymce.get('short_descr').getContent(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pAttributesData": getAttributesAry(),
            "pVariantData": JSON.stringify(getVariantsAry()),
            "pAttachmentsData": getAttachmentsAry(),
            "pIsHighlighted": $('#is_highlighted:checked').length,
            "pIsActive": $('#is_active:checked').length,
            "pPrezzo": $('#prezzo').val(),
            "pPrezzoPromo": $('#prezzo_promo').val(),
            "pPrezzoPromoExpire": $('#prezzo_promo_expire').val(),
            "pAutoRenewPromo": $('#auto_renew_promo').val(),
            "pShowLowestPrice": $('#show_lowest_price').val(),
            "pImposta": $('#imposta').val(),
            "pManageWarehouse": $('#manage_warehouse:checked').length,
            "pManageReviews": $('#manage_reviews').val(),
            "pEnableQuestions": $('#enable_questions').val(),
            "pModerateReviews": $('#moderate_reviews').val(),
            "pWarehouseQty": $('#warehouse_qty').val(),
            "pEnableCustomMessage": $('#enable_custom_message:checked').length,
            "pRequiredCustomMessage": $('#required_custom_message:checked').length,
            "pCustomMessageTitle": $('#custom_message_title').val(),
            "pCustomMessagePlaceholder": $('#custom_message_placeholder').val(),
            "pUpSellProd": $('#up_sell_products').val(),
            "pCrossSellProd": $('#cross_sell_products').val(),
            "pEAN": $('#ean').val(),
            "pProductCode": $('#product_code').val(),
            "pWeight": $('#weight').val(),
            "pChangedReviewStatus": changed_review_status,
            "pChangedQuestionStatus": changed_question_status,
            "pSeoData": {
                title: $('#seo_titolo').val(),
                description: $('#seo_description').val(),
            }
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data di scadenza della promozione non è valido. Impossibile continuare.");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "product_code_already_exists") {
                bootbox.error("Questo codice prodotto esiste già. Impostare un codice prodotto univoco.");
            } else if(data.status == "ean_already_exists") {
                bootbox.error("Questo codice EAN esiste già. Impostare un codice EAN univoco.");
            } else if(data.status == "weight_numeric") {
                bootbox.error("Il campo 'peso' deve contenere un valore numerico");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}

function checkIfCanAddMoreAttr() {
    if($('.attrDuplication').length >= $('.product_attribute:first option[value!=""]').length) {
        $('.attrDuplication').closest('.easyRowDuplicationContainer').find('.easyRowDuplicationAddBtn').hide();
    } else {
        $('.attrDuplication').closest('.easyRowDuplicationContainer').find('.easyRowDuplicationAddBtn').show();
    }
}

/* FUNZIONI DATATABLE */
function activateProduct(sel_row) {

    if(typeof(sel_row) == 'undefined') {
        sel_row = getSelectedRows('main_table_list');
    } else {
        sel_row = [{id: sel_row}];
    }

    if(sel_row.length > 0) {
        bootbox.confirm('Sei sicuro di voler mettere in vendita ' + sel_row.length  + ' prodotti' + (sel_row.length === 1 ? 'o' : 'i') + '?', function(result) {
            if(result) {
                var ids = [];
                sel_row.forEach(function(row, key) {
                    ids.push(row.id);
                });
                updateProductStatus(ids, 1);
                unselectAllRow('main_table_list');
            }
        });
    } else {
        bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
        return false;
    }
}

function deactivateProduct(sel_row) {

    if(typeof(sel_row) == 'undefined') {
        sel_row = getSelectedRows('main_table_list');
    } else {
        sel_row = [{id: sel_row}];
    }

    if(sel_row.length > 0) {
        bootbox.confirm('Sei sicuro di voler nascondere dal catalogo ' + sel_row.length  + ' prodott' + (sel_row.length === 1 ? 'o' : 'i') + '?', function(result) {
            if(result) {
                var ids = [];
                sel_row.forEach(function(row, key) {
                    ids.push(row.id);
                });
                updateProductStatus(ids, 0);
                unselectAllRow('main_table_list');
            }
        });
    } else {
        bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
        return false;
    }
}

function updateProductStatus(sel_id, status) {
    $.ajax({
        url: "ajax/updateProductStatus.php",
        type: "POST",
        data: {
            "pID": sel_id,
            "pStatus": status
        },
        async: false,
        dataType: "json",
        success: function(data) {
            $('.dataTable').DataTable().ajax.reload(null, false);
        }
    });
}
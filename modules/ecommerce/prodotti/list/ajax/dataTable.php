<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array(
        'db' => 'product_code',
        'dt' => 'DT_Sku'
    ),
    array(
        'db' => 'is_active',
        'dt' => 'DT_ExtraBtn',
        'formatter' => function( $d, $row ) {
            if($d) {
                return array(
                    array(
                        'label' => 'Nascondi dal catalogo',
                        'btn' => 'danger',
                        'icon' => 'toggle-off',
                        'class' => 'deactivateProduct',
                        'function' => 'deactivateProduct',
                        'context' => true
                    )
                );
            } else {
                return array(
                    array(
                        'label' => 'Metti in vendita',
                        'btn' => 'primary',
                        'icon' => 'toggle-on',
                        'class' => 'activateProduct',
                        'function' => 'activateProduct',
                        'context' => true
                    )
                );
            }
        }
    ),
    array(
        'db' => 'gallery',
        'dt' => 0,
        'formatter' => function( $d, $row ) {
            $image = json_decode($d);
            return '<img src="' . UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML . "tn/" . $image[0] . '" alt="" height="auto" width="80">';
        }
    ),
    array(
        'db' => 'id',
        'dt' => 1
    ),
    array(
        'db' => 'nome',
        'dt' => 2,
        'formatter' => function( $d, $row ) {
            global $MSFrameworki18n;
           return $MSFrameworki18n->getFieldValue($d, true);
        }
    ),
    array(
        'db' => 'category',
        'dt' => 3,
        'formatter' => function( $d, $row ) {
            global $MSFrameworki18n;
            $categories_label = array();
            foreach( (new \MSFramework\Ecommerce\categories())->getCategoryDetails(explode(',', $d), "nome") as $category) {
                $categories_label[] = $MSFrameworki18n->getFieldValue($category['nome']);
            }
            return $categories_label;
        }
    ),
    array(
        'db' => 'id',
        'dt' => 4,
        'formatter' => function( $d, $row ) {
            global $MSFrameworkDatabase;
            $r = $MSFrameworkDatabase->getAssoc("SELECT prezzo, prezzo_promo FROM ecommerce_products WHERE id = :id", array(':id' => $d), true);
            return ($r['prezzo_promo'] != "") ? "<del><?= CURRENCY_SYMBOL; ?> " . $r['prezzo'] . "</del> " . CURRENCY_SYMBOL . " " . $r['prezzo_promo'] : CURRENCY_SYMBOL . " " . $r['prezzo'];
        }

    ),
    array(
        'db' => 'is_active',
        'dt' => 5,
        'formatter' => function( $d, $row ) {
            return ($d == "1") ? '<span class="label label-primary">Si</span>' : '<span class="label label-danger">No</span>';
        }
    ),
    array(
        'db' => 'is_highlighted',
        'dt' => 6,
        'formatter' => function( $d, $row ) {
            return ($d == "1") ? '<span class="label label-warning">In Evidenza</span>' : '<span class="label label-inverse">No</span>';
        }
    )
);


if($MSFrameworkDatabase->getCount("SELECT id FROM `ecommerce_products` WHERE product_code != '' LIMIT 1")) {
    $columns[] = array(
        'db' => 'product_code',
        'dt' => end($columns)['dt']+1,
        'formatter' => function( $d, $row ) {
            if($d) return $d;
            else return '<span class="text-muted">N/A</span>';
        }
    );
}

$columns[] = array(
    'db' => 'visite',
    'dt' => end($columns)['dt']+1,
    'formatter' => function( $d, $row ) {
        return '<span class="label label-success"><i class="fas fa-eye"></i> ' . $d . '</span>';
    }
);

echo json_encode(
    $datatableHelper::simple( $_GET, $MSFrameworkDatabase, 'ecommerce_products', 'id', $columns )
);
?>

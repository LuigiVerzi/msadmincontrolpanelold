<?php
/**
 * MSAdminControlPanel
 * Date: 21/04/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkEcommerceAttributes = new \MSFramework\Ecommerce\attributes();

if($_POST['pID'] == "") {
    echo json_encode(array(false, "<option value=''></option>"));
    die();
}

$html = "";
foreach($MSFrameworkEcommerceAttributes->getValuesOfAttributeDetails($_POST['pID']) as $v_data) {
    $html .= "<option value='" . $MSFrameworkEcommerceAttributes->jsonPrefix('attribute_value', 'add', $v_data['id']) . "'>" . $MSFrameworki18n->getFieldValue($v_data['nome'], true) . "</option>";
}

echo json_encode(array($MSFrameworkEcommerceAttributes->checkIfIsMultiple($MSFrameworkEcommerceAttributes->jsonPrefix('attribute', 'remove', $_POST['pID'])), $html));
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$ids = $_POST['pID'];
$status = $_POST['pStatus'];

$ids_in = implode(",", array_map(
    function($v) {
        return (int)$v;
    }, $ids)
);

$MSFrameworkDatabase->query("UPDATE ecommerce_products SET is_active = :status WHERE id IN ($ids_in)", array(':status' => ($status ? 1 : 0)));

die(json_encode(array('status' => 'ok')));
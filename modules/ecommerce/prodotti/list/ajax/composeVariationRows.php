<?php
/**
 * MSAdminControlPanel
 * Date: 22/04/18
 */

if(!isset($_POST['pIncludedFromEdit'])) {
    require_once('../../../../../sw-config.php');
    require('../config/moduleConfig.php');
}

$MSFrameworkEcommerceAttributes = new \MSFramework\Ecommerce\attributes();

$gotCurVariations = $_POST['pCurVariantData'];
$gotAvailVariations = $MSFrameworkEcommerceAttributes->getVariationsFromAttributes($_POST['pAttributesData']);


if($_POST['pAutoCreate'] == "false") {
    foreach($gotCurVariations as $k => $v) {
        $broken = false;
        foreach($v['type'] as $typeK => $typeV) {
            if((string)$typeV == "") {
                continue;
            }

            if (!array_key_exists($typeK, $gotAvailVariations) || !in_array($typeV, $gotAvailVariations[$typeK])) {
                //$broken = true;
                break;
            }
        }

        if(!$broken) {
            $keep_variations[] = $v;
        }
    }

    if(count($keep_variations) == 0) {
        $keep_variations[] = array();
    }

    $master_cycle = $keep_variations;
}
else {
    $master_cycle_original = $MSFrameworkEcommerceAttributes->getVariationsCombinationsFromAttributes($_POST['pAttributesData']);

    //modifico al volo la struttura dell'array restituito da getVariationsCombinationsFromAttributes in modo da farla combaciare con quella restituita da getVariationsFromAttributes
    foreach($master_cycle_original as $k => $v) {

        $variationOptionsHTMLData = "";
        foreach($gotCurVariations as $gotCurVariationsV) {
            if($v == $gotCurVariationsV['type']) {
                $variationOptionsHTMLData = $gotCurVariationsV['data'];
                break;
            }
        }

        foreach($v as $vk => $vv) {
            $master_cycle[$k]['type'][$vk] = $vv;
            $master_cycle[$k]['data'] = $variationOptionsHTMLData;
        }
    }
}

foreach($master_cycle as $varK => $varV) {
    $variationOptionsHTMLData = $varV['data'];
    ?>
        <div class="stepsDuplication rowDuplication variationDuplication" style="background-color: #e0f8f8; padding: 20px; border: 1px solid rgba(0, 0, 0, 0.1);">
            <div class="row">

                <?php  foreach ($gotAvailVariations as $combK => $combV) { ?>
                    <?php $attribute_detail = $MSFrameworkEcommerceAttributes->getAttributeDetails($combK)[$combK]; ?>
                    <div class="col-sm-3">
                        <label><?php echo $MSFrameworki18n->getFieldValue($attribute_detail['nome'], true) ?></label>
                        <select class="form-control variation_value_select" attr-id="<?php echo $combK ?>">
                            <option value="">
                                Qualsiasi <?php echo $MSFrameworki18n->getFieldValue($attribute_detail['nome'], true) ?></option>
                                <?php foreach ($combV as $k => $v) { ?>
                                <option value="<?php echo $v ?>" <?= ($varV['type'][$combK] == $v ? "selected" : ''); ?>>
                                    <?php
                                    $attribute_value_detail = $MSFrameworkEcommerceAttributes->getValuesOfAttributeDetails($combK, $v)[$v];
                                    echo ($attribute_value_detail ? $MSFrameworki18n->getFieldValue($attribute_value_detail['nome'], true) : $v);
                                    ?>
                                </option>
                                <?php } ?>
                        </select>
                    </div>
                <?php } ?>
            </div>

            <?php include('variationOptionsHTML.php'); ?>
        </div>
        <?php
}
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 27/04/18
 */
?>

<div class="row" style="margin-top: 10px;">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Codice</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label><span>Codice Prodotto</span></label>
                        <input type="text" class="form-control variation_sku" value="<?php echo htmlentities($variationOptionsHTMLData['sku']) ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Magazzino</div>
            <div class="panel-body">
                <div class="row">
                    <?php
                    $manage_warehouse_container_style = "display: none;";
                    $manage_warehouse_check = "";
                    if($variationOptionsHTMLData['manage_warehouse'] == 1) {
                        $manage_warehouse_check = "checked";
                        $manage_warehouse_container_style = "";
                    }
                    ?>
                    <div class="col-sm-12 col-md-6">
                        <label>&nbsp;</label>
                        <div class="styled-checkbox form-control">
                            <label style="width: 100%;">
                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                    <input type="checkbox" class="variation_manage_warehouse" <?php echo $manage_warehouse_check ?>>
                                    <i></i>
                                </div>
                                <span style="font-weight: normal;">Gestisci magazzino</span>
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 variation_manage_warehouse_field" style="<?php echo $manage_warehouse_container_style ?>">
                        <label><span>Quantità in magazzino</span> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quantità prodotto in magazzino. Se questo prodotto prevede delle variazioni, la quantità definita in questo campo sarà utilizzata per tutte le variazioni che non hanno un proprio magazzino attivo."></i></span></label>
                        <input type="number" class="form-control variation_warehouse_qty" value="<?php echo htmlentities($variationOptionsHTMLData['warehouse_qty']) ?>" min="0">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Prezzi</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <label><span>Prezzo <?= CURRENCY_SYMBOL; ?></span></label>
                        <input type="text" class="form-control variation_price" value="<?php echo htmlentities($variationOptionsHTMLData['prezzo']) ?>">
                    </div>

                    <div class="col-sm-6">
                        <label><span>Prezzo promozionale <?= CURRENCY_SYMBOL; ?></span> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Impostando questo valore il prodotto verrà venduto in promozione con il prezzo indicato"></i></span></label>
                        <input type="text" class="form-control variation_promo_price" value="<?php echo htmlentities($variationOptionsHTMLData['prezzo_promo']) ?>">
                    </div>

                    <div class="col-sm-6">
                        <label><span>Scadenza promozione</span> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Impostare la data di fine della promozione. Lasciare vuoto per nessuna scadenza."></i></span></label>
                        <div class="input-group date variation_promo_price_expire_dt">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control variation_promo_price_expire" value="<?php echo ($variationOptionsHTMLData['promo_price_expire'] != '') ? date("d/m/Y", $variationOptionsHTMLData['promo_price_expire']) : '' ?>">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label><span>Ripeti promozione</span> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imposta questo valore se desideri che la promozione si rinnovi automaticamente alla scadenza. Il valore impostato rappresenta il numero di giorni oltre la scadenza per il quale la promozione resterà attiva. La promozione si rinnoverà all'infinito fin quanto questo campo sarà valorizzato. Lasciare vuoto o su '0' per nessuna ripetizione."></i></span></label>
                        <input type="number" class="form-control variation_auto_renew_promo" value="<?php echo htmlentities($variationOptionsHTMLData['auto_renew_promo']) ?>">
                    </div>

                </div>

                <div>
                    Inserisci i prezzi <b><?php echo ((new \MSFramework\Ecommerce\imposte())->getPriceType() == "0") ? "comprensivi delle imposte" : "al netto delle imposte" ?></b>. Puoi modificare quest'opzione all'interno delle impostazioni. Verrà applicata l'imposta selezionata all'interno della scheda "Prodotto".
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Descrizione</div>
            <div class="panel-body">
                <label> </label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($variationOptionsHTMLData['short_descr']) ?>"></i></span>
                <textarea class="variation_short_descr"><?php echo $MSFrameworki18n->getFieldValue($variationOptionsHTMLData['short_descr']) ?></textarea>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <label>Gallery Variazione</label>
        <?php (new \MSFramework\uploads('ECOMMERCE_PRODUCTS'))->initUploaderHTML("variationimage_" . $varK, json_encode($variationOptionsHTMLData['gallery']), array("realPath" => "variationimage_realPath", "prevUploaded" => "prev_upl_variationimage", "element" => "variationimage", "container" => "variationimage_container")); ?>
    </div>
</div>
<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkEcommerceAttributes = new \MSFramework\Ecommerce\attributes();

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_products WHERE id = :id", array(":id" => $_GET['id']), true);
}

$r_global_settings = $MSFrameworkCMS->getCMSData('ecommerce_prodotti');

/* Attributi */
$r_attr_values = json_decode($r['attributes'], true);
$r_attr_details = $MSFrameworkEcommerceAttributes->getAttributeDetails();

$r_main_attr_details = array();
foreach($r_attr_details as $attr_key => $attr_detail) {
    if($attr_detail['is_main']) {
        $r_main_attr_details[$attr_key] = $attr_detail;
        unset($r_attr_details[$attr_key]);
    }
}

$r_main_attr_values = array();
foreach($r_attr_values as $attr_value_key => $attr_value) {
    if(in_array($MSFrameworkEcommerceAttributes->jsonPrefix('attribute', 'remove', $attr_value[0]), array_keys($r_main_attr_details))) {
    $r_main_attr_values[$MSFrameworkEcommerceAttributes->jsonPrefix('attribute', 'remove', $attr_value[0])] = $attr_value;
        unset($r_attr_values[$attr_value_key]);
    }
}

$r['seo'] = (json_decode($r['seo']) ? json_decode($r['seo'], true) : array());

$MSFrameworkProducts = new \MSFramework\Ecommerce\products();
$MSFrameworkImposte = new \MSFramework\Ecommerce\imposte();
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEditWithSaveAsNew.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Prodotto</h1>
                        <fieldset>

                            <div class="row">
                                <div class="col-lg-6 col-sm-6">
                                    <label>Nome Prodotto*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Slug*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                            <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-2 col-sm-6">
                                    <label>Categoria</label>
                                    <select id="category" name="category" class="form-control chosen-select" data-placeholder="Nessuna categoria..." multiple>
                                        <?php
                                            echo (new \MSFramework\Ecommerce\categories)->composeHTMLMenu('select', explode(',', $r['category']));
                                        ?>
                                    </select>
                                </div>

                                <div class="col-lg-2 col-sm-6">
                                    <label>EAN</label>
                                    <input id="ean" name="ean" type="text" class="form-control" value="<?php echo htmlentities($r['ean']) ?>">
                                </div>

                                <div class="col-lg-2 col-sm-6">
                                    <label>Codice Prodotto</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci qui il codice identificativo interno del prodotto."></i></span>
                                    <input id="product_code" name="product_code" type="text" class="form-control" value="<?php echo htmlentities($r['product_code']) ?>">
                                </div>
                            </div>

                            <div class="row">
                                <?php if($r_main_attr_details) { ?>

                                    <?php foreach($r_main_attr_details as $attr_detail_value) { ?>
                                        <div class="col-md-3  col-sm-6">
                                            <label><?= $MSFrameworki18n->getFieldValue($attr_detail_value['nome']); ?></label>


                                            <?php
                                            $product_attribute_values_multple_control = "";
                                            $product_attribute_values_tags_control = ' data-tags="true"';

                                            if((int)$attr_detail_value['is_variant'] || (int)$attr_detail_value['is_multiple']) {
                                                $product_attribute_values_multple_control = ' multiple="multiple" ';
                                            }
                                            ?>

                                            <select class="main_product_attribute_values product_attribute_values form-control" data-id="attr_<?= $attr_detail_value['id']; ?>" <?= $product_attribute_values_tags_control; ?> <?= $product_attribute_values_multple_control; ?> width="100%">
                                                <?php if(empty($product_attribute_values_multple_control)) { ?>
                                                    <option value="">&nbsp;</option>
                                                <?php } ?>
                                                <?php foreach($MSFrameworkEcommerceAttributes->getValuesOfAttributeDetails($attr_detail_value['id']) as $k => $v) { ?>
                                                    <option value="<?php echo $MSFrameworkEcommerceAttributes->jsonPrefix('attribute_value', 'add', $v['id']) ?>" <?php if(in_array($MSFrameworkEcommerceAttributes->jsonPrefix('attribute_value', 'add', $v['id']), (is_array($r_main_attr_values[$attr_detail_value['id']][1]) ? $r_main_attr_values[$attr_detail_value['id']][1] : array($r_main_attr_values[$attr_detail_value['id']][1])))) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($v['nome'], true) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <?php } ?>
                                <?php } ?>
                                
                            </div>

                            <div class="hr-line-dashed"></div>
                            
                            <div class="row">
                                <div class="col-sm-12 m-b-sm">
                                    Inserisci i prezzi <b><?php echo ($MSFrameworkImposte->getPriceType() == "0") ? "comprensivi delle imposte" : "al netto delle imposte" ?></b>. Puoi modificare quest'opzione all'interno delle impostazioni
                                </div>

                                <div class="col-lg-3 col-sm-6">
                                    <label>Prezzo <?= CURRENCY_SYMBOL; ?>*</label>
                                    <input id="prezzo" name="prezzo" type="text" class="form-control required" value="<?php echo htmlentities($r['prezzo']) ?>">
                                </div>

                                <div class="col-lg-3 col-sm-6">
                                    <label>Prezzo promozionale <?= CURRENCY_SYMBOL; ?></label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Impostando questo valore il prodotto verrà venduto in promozione con il prezzo indicato"></i></span>
                                    <input id="prezzo_promo" name="prezzo_promo" type="text" class="form-control" value="<?php echo htmlentities($r['prezzo_promo']) ?>">
                                </div>

                                <div class="col-lg-3 col-sm-6">
                                    <label>Scadenza promozione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Impostare la data di fine della promozione. Lasciare vuoto per nessuna scadenza."></i></span>
                                    <div class="input-group date prezzo_promo_expire">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="prezzo_promo_expire" value="<?php echo ($r['prezzo_promo_expire'] != '') ? date("d/m/Y", $r['prezzo_promo_expire']) : '' ?>">
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6">
                                    <label>Ripeti promozione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imposta questo valore se desideri che la promozione si rinnovi automaticamente alla scadenza. Il valore impostato rappresenta il numero di giorni oltre la scadenza per il quale la promozione resterà attiva. La promozione si rinnoverà all'infinito fin quanto questo campo sarà valorizzato. Lasciare vuoto o su '0' per nessuna ripetizione."></i></span>
                                    <input id="auto_renew_promo" name="auto_renew_promo" type="number" class="form-control" value="<?php echo htmlentities($r['auto_renew_promo']) ?>">
                                </div>

                                <div class="col-lg-3 col-sm-6 m-t">
                                    <label>Usa prezzo più basso</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questa opzione, se il prodotto ha diversi prezzi per le variazioni, verrà mostrato all'utente il prezzo più basso fin quando non verrà selezionata una variazione."></i></span>
                                    <select id="show_lowest_price" class="form-control">
                                        <option value="0" <?php if($r['show_lowest_price'] == "0") { echo "selected"; } ?>>Usa impostazioni globali (<?php echo ($r_global_settings['show_lowest_price'] == 1) ? 'Attivo' : 'Non Attivo' ?>)</option>
                                        <option value="1" <?php if($r['show_lowest_price'] == "1") { echo "selected"; } ?>>Sempre più basso</option>
                                        <option value="2" <?php if($r['show_lowest_price'] == "2") { echo "selected"; } ?>>Sempre prezzo reale</option>
                                    </select>
                                </div>

                                <div class="col-lg-3 col-sm-6 m-t">
                                    <label>Imposta applicata</label>
                                    <select id="imposta" class="form-control">
                                        <option value="default">Usa impostazioni Predefinite</option>
                                            <option value="">- Nessuna imposta -</option>
                                        <?php
                                        $getImposte = $MSFrameworkImposte->getImposte();
                                        $num_imp = count($getImposte);
                                        foreach($getImposte as $impK => $impV) {
                                            ?>
                                            <option value="<?php echo $impK ?>" <?php if($r['imposta'] == $impK || $num_imp == 1) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($impV[0], true) ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-lg-3 col-sm-6 m-t">
                                    <label>Peso (<?= $MSFrameworkProducts->getWeightUM() ?>)</label>
                                    <input id="weight" name="weight" type="text" class="form-control" value="<?php echo htmlentities($r['weight']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <?php
                                $is_active_check = "checked";
                                if($r['is_active'] == 0 && $_GET['id'] != "") {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deselezionando questa voce il prodotto sarà rimosso dal portale"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">In Vendita</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $is_highlighted_check = "";
                                if($r['is_highlighted'] == 1) {
                                    $is_highlighted_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_highlighted" <?php echo $is_highlighted_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">In evidenza</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $manage_warehouse_check = "";
                                if($r['manage_warehouse'] == 1) {
                                    $manage_warehouse_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="manage_warehouse" <?php echo $manage_warehouse_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Gestisci magazzino</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3" id="manage_warehouse_field" style="display: none;">
                                    <label>Quantità in magazzino</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quantità prodotto in magazzino. Se questo prodotto prevede delle variazioni, la quantità definita in questo campo sarà utilizzata per tutte le variazioni che non hanno un proprio magazzino attivo."></i></span>
                                    <input id="warehouse_qty" name="warehouse_qty" type="number" class="form-control" value="<?php echo htmlentities($r['warehouse_qty']) ?>" min="0">
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Descrizione Breve</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['short_descr']) ?>"></i></span>
                                    <textarea id="short_descr" name="short_descr"><?php echo $MSFrameworki18n->getFieldValue($r['short_descr']) ?></textarea>
                                </div>

                                <div class="col-sm-6">
                                    <label>Descrizione Estesa</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['long_descr']) ?>"></i></span>
                                    <textarea id="long_descr" name="long_descr"><?php echo $MSFrameworki18n->getFieldValue($r['long_descr']) ?></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Attributi / Variazioni</h1>
                        <fieldset>

                            <div class="row">
                                <?php
                                $enable_custom_message = "";
                                if($r['enable_custom_message'] == 1) {
                                    $enable_custom_message = "checked";
                                    $r['custom_message'] = json_decode($r['custom_message'], true);
                                    $required_custom_message = ($r['custom_message'] &&  $r['custom_message']['required'] ? 'checked' : '');
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label><span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitato questa voce il cliente potrà inserire un messaggio personalizzato prima di effettuare l'acquisto."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_custom_message" <?php echo $enable_custom_message ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Campo Personalizzato</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3"  id="required_custom_message_field" style="display: none;">
                                    <label>&nbsp;</label><span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitato questa voce il campo diventa obbligatorio per poter proseguire con l'acquisto."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="required_custom_message" <?php echo $required_custom_message; ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Rendi campo obbligatorio</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3" id="custom_message_title_field" style="display: none;">
                                    <label>Titolo 'Messaggio Personalizzato'</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Indica ai clienti cosa dovranno inserire all'interno del campo."></i></span>
                                    <input id="custom_message_title" name="custom_message_title" type="text" class="form-control" value="<?php echo ($r['enable_custom_message'] && !empty($r['custom_message']) ?  $MSFrameworki18n->getFieldValue($r['custom_message']['title']) : ''); ?>">
                                </div>

                                <div class="col-sm-3" id="custom_message_placeholder_field" style="display: none;">
                                    <label>Descrizione 'Messaggio Personalizzato'</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Descrivi meglio ciò che i clienti dovranno inserire all'interno del campo."></i></span>
                                    <input id="custom_message_placeholder" name="custom_message_placeholder" type="text" class="form-control" value="<?php echo ($r['enable_custom_message'] && !empty($r['custom_message']) ?  $MSFrameworki18n->getFieldValue($r['custom_message']['placeholder']) : ''); ?>">
                                </div>
                            </div>

                            <h2 class="title-divider">Selezione Attributi</h2>

                            <div class="easyRowDuplicationContainer">
                                <?php

                                if(count($r_attr_values) == 0) {
                                    $r_attr_values[] = array();
                                }

                                foreach($r_attr_values as $dataK => $dataV) {
                                ?>
                                <div class="stepsDuplication rowDuplication attrDuplication">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <select class="product_attribute form-control" width="100%">
                                                <option value="">Seleziona Attributo...</option>
                                                <?php foreach($r_attr_details as $k => $v) { ?>
                                                    <option value="<?php echo $MSFrameworkEcommerceAttributes->jsonPrefix('attribute', 'add', $v['id']) ?>" <?php if($dataV[0] == $MSFrameworkEcommerceAttributes->jsonPrefix('attribute', 'add', $v['id'])) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($v['nome'], true) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <?php
                                        $product_attribute_values_container_style = "display: none;";
                                        $product_attribute_values_multple_control = "";
                                        $product_attribute_values_tags_control = ' data-tags="true"';

                                        if($dataV[0] != "")  $product_attribute_values_container_style = "";

                                        if($MSFrameworkEcommerceAttributes->checkIfIsMultiple($MSFrameworkEcommerceAttributes->jsonPrefix('attribute', 'remove', $dataV[0]))) {
                                            $product_attribute_values_multple_control = ' multiple="multiple" ';
                                        }

                                        ?>

                                        <div class="col-sm-8 product_attribute_values_container" style="<?php echo $product_attribute_values_container_style ?>">
                                            <select class="product_attribute_values form-control" <?php echo $product_attribute_values_tags_control . $product_attribute_values_multple_control ?> width="100%">
                                                <?php

                                                if(!is_array($dataV[1])) {
                                                    $dataV[1] = array($dataV[1]); //uniformo la struttura del dato per gestire in un colpo solo la versione "multiselect" e quella "singleselect"
                                                }

                                                foreach($MSFrameworkEcommerceAttributes->getValuesOfAttributeDetails($dataV[0]) as $k => $v) {
                                                ?>
                                                    <option value="<?php echo $MSFrameworkEcommerceAttributes->jsonPrefix('attribute_value', 'add', $v['id']) ?>" <?php if(in_array($MSFrameworkEcommerceAttributes->jsonPrefix('attribute_value', 'add', $v['id']), $dataV[1])) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($v['nome'], true) ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                            <div class="hr-line-dashed" style="margin: 40px 0;"></div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <h2 style="margin-top: 0px;">Selezione Variazioni</h2>
                                </div>

                                <div class="col-sm-8 text-right">
                                    <a class="btn btn-primary m-b" id="createVarFromAllAttr">Crea variazione per ogni attributo</a>
                                    <a class="btn btn-danger m-b" id="deleteAllVar">Elimina tutte le variazioni</a>
                                </div>

                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        Le impostazioni di questa sezione sovrascriveranno quelle globali settate all'interno della scheda "Prodotto".
                                    </div>
                                </div>
                            </div>

                            <div class="easyRowDuplicationContainer variations_container">
                                <textarea style="display: none;" id="pAttributesData"><?= json_encode(array_merge($r_attr_values, $r_main_attr_values)); ?></textarea>
                                <textarea style="display: none;" id="pCurVariantData"><?= ($r['variants'] ? $r['variants'] : '[]'); ?></textarea>
                            </div>
                        </fieldset>

                        <h1>Articoli Collegati</h1>
                        <fieldset>
                            <h2 class="title-divider">Selezione Articoli Collegati</h2>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Up-Sell</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="I prodotti in Up-Sell sono prodotti che vengono proposti al cliente in alternativa a quello correntemente visualizzato (ad esempio di miglior qualità o più redditizi)"></i></span>
                                    <select id="up_sell_products" class="form-control" multiple="multiple" width="100%">
                                        <?php
                                        $up_sell = json_decode($r['up_sell'], true);
                                        $info_prodotti = array();
                                        if($up_sell) $info_prodotti = $MSFrameworkProducts->getProductDetails($up_sell);
                                        ?>
                                        <?php foreach($info_prodotti as $product) { ?>
                                        <option value="<?php echo $product['id'] ?>" selected><?= $MSFrameworki18n->getFieldValue($product['nome']); ?> [ID: <?= $product['id']; ?>]</option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-6">
                                    <label>Cross-Sell</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="I prodotti in Cross-Sell sono prodotti che vengono proposti al cliente in aggiunta a quello correntemente visualizzato (ad esempio prima di completare il checkout)"></i></span>
                                    <select id="cross_sell_products" class="form-control" multiple="multiple" width="100%">
                                        <?php
                                        $cross_sell = json_decode($r['cross_sell'], true);
                                        $info_prodotti = array();
                                        if($cross_sell) $info_prodotti = $MSFrameworkProducts->getProductDetails($cross_sell);
                                        ?>
                                        <?php foreach($info_prodotti as $product) { ?>
                                            <option value="<?php echo $product['id'] ?>" selected><?= $MSFrameworki18n->getFieldValue($product['nome']); ?> [ID: <?= $product['id']; ?>]</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                        </fieldset>

                        <h1>Gallery</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Gallery</label>
                                    <?php
                                    (new \MSFramework\uploads('ECOMMERCE_PRODUCTS'))->initUploaderHTML("images", $r['gallery']);
                                    ?>
                                </div>
                            </div>

                        </fieldset>

                        <?php if($r_global_settings['enable_attachments'] == 1) { ?>
                        <h1>Allegati</h1>
                        <fieldset>


                            <div class="easyRowDuplicationContainer">
                                <?php
                                $r_attachments_values = json_decode($r['attachments'], true);
                                if(count($r_attachments_values) == 0) {
                                    $r_attachments_values[] = array('file' => array(), 'name' => '', 'only_for_users' => 0);
                                }
                                ?>

                                <?php foreach($r_attachments_values as $dataK => $dataV) { ?>
                                    <div class="stepsDuplication rowDuplication attachmentsDuplication" style="background-color: #fafafa; padding: 20px; border: 1px solid rgba(0, 0, 0, 0.1);">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Nome Allegato*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($dataV['name']) ?>"></i></span>
                                                <input type="text" class="form-control required attachment_name" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($dataV['name'])) ?>">
                                            </div>

                                            <?php
                                            $only_for_users_check = "";
                                            if($dataV['only_for_users'] == 1) {
                                                $only_for_users_check = "checked";
                                            }
                                            ?>
                                            <div class="col-sm-6">
                                                <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questa opzione, il file allegato sarà disponibile solamente per gli utenti registrati."></i></span>
                                                <div class="styled-checkbox form-control">
                                                    <label style="width: 100%;">
                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                            <input type="checkbox" class="only_for_users" <?php echo $only_for_users_check ?>>
                                                            <i></i>
                                                        </div>
                                                        <span style="font-weight: normal;">Registrazione Richiesta</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Documento (.pdf, .doc, .xml, .zip, .rar)</label>
                                                <?php (new \MSFramework\uploads('ECOMMERCE_ATTACHMENTS'))->initUploaderHTML("attachment" . $dataK, json_encode($dataV['file']), array("realPath" => "attachmentfile_realPath", "prevUploaded" => "prev_upl_attachmentfile", "element" => "attachmentfile", "container" => "attachmentfile_container"), array(), false);  ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>

                            <div class="hr-line-dashed" style="margin: 40px 0;"></div>


                        </fieldset>
                        <?php } ?>

                        <?php
                        if($_GET['id'] != "") {
                        ?>
                            <h1>Recensioni</h1>
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Recensioni</label>
                                        <select id="manage_reviews" class="form-control">
                                            <option value="0" <?php if($r['manage_reviews'] == "0") { echo "selected"; } ?>>Usa impostazioni globali (<?php echo ($r_global_settings['manage_reviews'] == 1) ? 'Attive' : 'Non Attive' ?>)</option>
                                            <option value="1" <?php if($r['manage_reviews'] == "1") { echo "selected"; } ?>>Sempre attive</option>
                                            <option value="2" <?php if($r['manage_reviews'] == "2") { echo "selected"; } ?>>Sempre non attive</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label>Modera Recensioni</label>
                                        <select id="moderate_reviews" class="form-control">
                                            <option value="0" <?php if($r['moderate_reviews'] == "0") { echo "selected"; } ?>>Usa impostazioni globali (<?php echo ($r_global_settings['moderate_reviews'] == 1) ? 'Moderate' : 'Non Moderate' ?>)</option>
                                            <option value="1" <?php if($r['moderate_reviews'] == "1") { echo "selected"; } ?>>Sempre moderate</option>
                                            <option value="2" <?php if($r['moderate_reviews'] == "2") { echo "selected"; } ?>>Sempre non moderate</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="hr-line-dashed" style="margin: 40px 0;"></div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="title-divider">Lista Recensioni</h2>
                                    </div>
                                    <div class="col-lg-6 text-right">
                                        <a href="../recensioni/custom.php?product=<?= $_GET['id']; ?>" class="btn btn-outline btn-primary">Aggiungi nuova</a>
                                    </div>
                                </div>
                            <?php
                            $got_reviews = $MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_product_reviews` WHERE product_id = :id ORDER BY comment_date desc", array(":id" => $_GET['id']));
                            if(count($got_reviews) == 0) {
                            ?>
                                <div class="alert alert-info">
                                    Non ci sono ancora recensioni per questo prodotto.
                                </div>
                            <?php
                            } else {
                                foreach($got_reviews as $review) {
                                    if($review['user_id'] == 0) {
                                        $user_data = json_decode($review['fake_customer'], true);
                                    }
                                    else {
                                        $user_data = (new \MSFramework\customers())->getCustomerDataFromDB($review['user_id'], "nome, cognome");
                                    }
                                    ?>
                                    <div class="review_row" data-review-id="<?php echo $review['id'] ?>" data-review-original-status="<?php echo $review['status'] ?>" style="background-color: <?php echo ($review['status'] == '1') ? '#bcf6b5' : '#f6c7c2' ?>; padding: 20px; border: 1px solid rgba(0, 0, 0, 0.1);">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Utente</label>
                                                <div><?php echo $user_data['nome'] . " " . $user_data['cognome'] ?> <?= ($review['user_id'] == 0 ? '<a class="btn btn-xs btn-info" href="../recensioni/edit.php?id=' . $review['id'] . '">Modifica recensione</a>' : ''); ?></div>
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Data</label>
                                                <div><?php echo (new DateTime($review['comment_date']))->format("d/m/Y H:i") ?></div>
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Valutazione</label>
                                                <div><?php echo $review['rating'] . " stell"; echo ($review['rating'] == "1") ? "a" : "e" ?></div>
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Stato</label>
                                                <select class="form-control review_status">
                                                    <option value="0" <?php if($review['status'] == "0") { echo "selected"; } ?>>Non Approvata/Non visibile sul sito</option>
                                                    <option value="1" <?php if($review['status'] == "1") { echo "selected"; } ?>>Approvata/Visibile sul sito</option>
                                                    <option value="del">Elimina definitivamente</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-12 m-t">
                                                <label>Testo della recensione</label>
                                                <div><?php echo $review['comment_body'] ?></div>
                                            </div>

                                            <?php
                                            $review_variations = json_decode($review['product_variations'], true);
                                            if(count($review_variations) > 0) {
                                                $variation_review_html = "";
                                                foreach($review_variations as $variationK => $variationV) {
                                                    $variation_review_html .= $MSFrameworki18n->getFieldValue($variationK, true) . ": " . $MSFrameworki18n->getFieldValue($variationV, true) . ", ";
                                                }
                                                $variation_review_html = substr($variation_review_html, 0, -2);
                                            ?>
                                            <div class="col-sm-12 m-t">
                                                <label>Rilasciata per la variazione</label>
                                                <div><?php echo $variation_review_html ?></div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                            <?php
                                }
                            }
                            ?>
                            </fieldset>
                        <?php
                        }
                        ?>

                        <?php
                        if($_GET['id'] != "") {
                            ?>
                            <h1>Domande Clienti</h1>
                            <fieldset>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Domande Clienti</label>
                                        <select id="enable_questions" class="form-control">
                                            <option value="0" <?php if($r['enable_questions'] == "0") { echo "selected"; } ?>>Usa impostazioni globali (<?php echo ($r_global_settings['enable_questions'] == 1) ? 'Attive' : 'Non Attive' ?>)</option>
                                            <option value="1" <?php if($r['enable_questions'] == "1") { echo "selected"; } ?>>Sempre attive</option>
                                            <option value="2" <?php if($r['enable_questions'] == "2") { echo "selected"; } ?>>Sempre non attive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="hr-line-dashed" style="margin: 40px 0;"></div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="title-divider">Lista Domande</h2>
                                    </div>
                                    <div class="col-lg-6 text-right">
                                        <a href="../domande/custom.php?product=<?= $_GET['id']; ?>" class="btn btn-outline btn-primary">Aggiungi nuova</a>
                                    </div>
                                </div>

                                <?php
                                $got_questions = $MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_product_questions` WHERE product_id = :id ORDER BY question_date desc", array(":id" => $_GET['id']));
                                if(count($got_questions) == 0) {
                                    ?>
                                    <div class="alert alert-info">
                                        Non ci sono ancora domande per questo prodotto.
                                    </div>
                                    <?php
                                } else {
                                    foreach($got_questions as $question) {
                                        if($question['user_id'] == 0) {
                                            $user_data = json_decode($question['fake_customer'], true);
                                        }
                                        else {
                                            $user_data = (new \MSFramework\customers())->getCustomerDataFromDB($question['user_id'], "nome, cognome");
                                        }
                                        ?>
                                        <div class="question_row" data-question-id="<?php echo $question['id'] ?>" data-question-original-status="<?php echo $question['status'] ?>" data-question-original-answer="<?php echo htmlentities($question['answer']) ?>" style="background-color: <?php echo ($question['status'] == '1') ? (($question['answer'] != '') ? '#bcf6b5' : '#f6fbb2') : '#f6c7c2' ?>; padding: 20px;  border: 1px solid rgba(0, 0, 0, 0.1);">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Utente</label>
                                                    <div><?php echo $user_data['nome'] . " " . $user_data['cognome'] ?> <?= ($review['user_id'] == 0 ? '<a class="btn btn-info btn-xs" href="../domande/edit.php?id=' . $review['id'] . '">Modifica domanda</a>' : ''); ?></div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <label>Data</label>
                                                    <div><?php echo (new DateTime($question['question_date']))->format("d/m/Y H:i") ?></div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label>Stato</label>
                                                    <select class="form-control question_status">
                                                        <option value="0" <?php if($question['status'] == "0") { echo "selected"; } ?>>Non Approvata/Non visibile sul sito</option>
                                                        <option value="1" <?php if($question['status'] == "1") { echo "selected"; } ?>>Approvata/Visibile sul sito se ha ricevuto risposta</option>
                                                        <option value="del">Elimina definitivamente</option>
                                                    </select>
                                                </div>

                                                <div class="col-sm-12 m-t">
                                                    <label>Testo della domanda</label>
                                                    <div><?php echo $question['question'] ?></div>
                                                </div>

                                                <div class="col-sm-12 m-t">
                                                    <label>La tua risposta</label>
                                                    <div><textarea class="form-control question_answer" rows="4"><?php echo $question['answer'] ?></textarea></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>
                                        <?php
                                    }
                                }
                                ?>
                            </fieldset>

                            <h1>SEO</h1>
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>TAG Title</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['seo']['title']) ?>"></i></span>
                                        <input id="seo_titolo" name="seo_titolo" type="text" class="form-control serpUpdate" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['seo']['title'])) ?>" <?php echo ($r['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>TAG Description</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['seo']['description']) ?>"></i></span>
                                        <textarea id="seo_description" name="seo_description" type="text" class="form-control serpUpdate" <?php echo ($r['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>><?php echo htmlentities($MSFrameworki18n->getFieldValue($r['seo']['description'])) ?></textarea>
                                    </div>
                                </div>

                                <div class="ibox">
                                    <div class="ibox-title" style="background: #fefefe;">
                                        <i class="ibox-icon fa fa-google fa-3x"></i>
                                        <h5>
                                            <span class="title">Anteprima</span>
                                        </h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div id="serpPreview"></div>
                                    </div>
                                </div>
                            </fieldset>
                            <?php
                        }
                        ?>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
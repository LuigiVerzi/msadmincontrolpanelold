$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    if($('#id_prodotto').length) {
        initProductsSelect2($('#id_prodotto'));

        $('.input-group.date').datetimepicker({
            keyboardNavigation: false,
            autoclose: true,
            startDate: new Date(),
            format: 'dd/mm/yyyy hh:ii'
        });
    }

}

function moduleSaveFunction() {

    if($('#id_prodotto').length) {

        var variazioni = new Array();

        if($('.product_variation').length) {
            $('.product_variation').each(function () {
                if($(this).val() != "") {
                    variazioni.push($(this).val());
                }
            });
        }

        var data = {
            "pID": $('#record_id').val(),
            "pStatus": $('#review_status').val(),
            "pProductID": $('#id_prodotto').val(),
            "pVariations": variazioni,
            "pNomeUtente": $('#nome_utente').val(),
            "pCognomeUtente": $('#cognome_utente').val(),
            "pData": $('#data_recensione').val(),
            "pValutazione": $('#valutazione').val(),
            "pTesto": $('#testo_recensione').val()
        };
        var endpoint = 'saveCustom';
    }
    else {
        var data = {
            "pID": $('#record_id').val(),
            "pStatus": $('#review_status').val(),
        };
        var endpoint = 'saveData';
    }

    $.ajax({
        url: "db/" + endpoint + ".php",
        type: "POST",
        data: data,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "date_format") {
                bootbox.error("La data inserita non è valida! Inserisci una data valida.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}
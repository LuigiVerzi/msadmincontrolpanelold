<?php
/**
 * MSAdminControlPanel
 * Date: 26/05/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_reviews ORDER BY status, comment_date asc") as $r) {

    if($r['user_id'] == 0) {
        $user = json_decode($r['fake_customer'], true);
    }
    else {
        $user = (new \MSFramework\customers())->getCustomerDataFromDB($r['user_id']);
    }

    $array['data'][] = array(
        $user['nome'] . ' ' . $user['cognome'] . ($r['user_id'] == 0 ? ' <span class="badge badge-info">Manuale</span>' : ''),
        (strlen($r['comment_body']) < 130 ? $r['comment_body'] : substr($r['comment_body'], 0, 127) . "..."),
        $MSFrameworki18n->getFieldValue((new \MSFramework\Ecommerce\products())->getProductDetails($r['product_id'], "nome")[$r['product_id']]['nome'], true),
        $r['rating'] . " stell" . (($r['rating'] == "1") ? "a" : "e"),
        (new DateTime($r['comment_date']))->format("d/m/Y H:i"),
        ($r['status'] == "1" ? "Approvata" : "Non Approvata"),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 26/05/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_reviews WHERE id = :id", array($_GET['id']), true);
    if($r['user_id'] == '0') header('Location: custom.php?id=' . $_GET['id']);
}

if(!$r) {
    if(isset($_GET['id']) && !empty($_GET['id'])) header('Location: index.php');
    else header('Location: custom.php');
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>
                            <?php
                            $review = $r;
                            $user_data = (new \MSFramework\customers())->getCustomerDataFromDB($review['user_id'], "nome, cognome");
                            ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Rilasciata per il prodotto</label>
                                    <div><?php echo $MSFrameworki18n->getFieldValue((new \MSFramework\Ecommerce\products())->getProductDetails($review['product_id'], "nome")[$review['product_id']]['nome'], true) ?></div>
                                </div>

                                <div class="col-sm-3 m-t">
                                    <label>Utente</label>
                                    <div><?php echo $user_data['nome'] . " " . $user_data['cognome'] ?></div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Data</label>
                                    <div><?php echo (new DateTime($review['comment_date']))->format("d/m/Y H:i") ?></div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Valutazione</label>
                                    <div><?php echo $review['rating'] . " stell"; echo ($review['rating'] == "1") ? "a" : "e" ?></div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Stato</label>
                                    <select class="form-control" id="review_status">
                                        <option value="0" <?php if($review['status'] == "0") { echo "selected"; } ?>>Non Approvata/Non visibile sul sito</option>
                                        <option value="1" <?php if($review['status'] == "1") { echo "selected"; } ?>>Approvata/Visibile sul sito</option>
                                    </select>
                                </div>

                                <div class="col-sm-12 m-t">
                                    <label>Testo della recensione</label>
                                    <div><?php echo $review['comment_body'] ?></div>
                                </div>

                                <?php
                                $review_variations = json_decode($review['product_variations'], true);
                                if(count($review_variations) > 0) {
                                    $variation_review_html = "";
                                    foreach($review_variations as $variationK => $variationV) {
                                        $variation_review_html .= $MSFrameworki18n->getFieldValue($variationK, true) . ": " . $MSFrameworki18n->getFieldValue($variationV, true) . ", ";
                                    }
                                    $variation_review_html = substr($variation_review_html, 0, -2);
                                    ?>
                                    <div class="col-sm-12 m-t">
                                        <label>Rilasciata per la variazione</label>
                                        <div><?php echo $variation_review_html ?></div>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
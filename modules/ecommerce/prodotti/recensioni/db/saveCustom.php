<?php
/**
 * MSAdminControlPanel
 * Date: 26/05/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pProductID'] == "" || $_POST['pNomeUtente'] == "" || $_POST['pCognomeUtente'] == "" || $_POST['pTesto'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$date = DateTime::createFromFormat('d/m/Y H:i', $_POST['pData']);

if(!$date) {
    echo json_encode(array("status" => "date_format"));
    die();
}

$array_to_save = array(
    "status" => $_POST['pStatus'],
    "product_id" => $_POST['pProductID'],
    "product_variations" => json_encode($_POST['pVariations']),
    "user_id" => 0,
    "fake_customer" => json_encode(
        array(
            'nome' => $_POST['pNomeUtente'],
            'cognome' => $_POST['pCognomeUtente'],
        )
    ),
    "comment_date" => $date->format('Y-m-d H:i'),
    "comment_body" => $_POST['pTesto'],
    "rating" => $_POST['pValutazione'],
);


$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_product_reviews ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_product_reviews SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
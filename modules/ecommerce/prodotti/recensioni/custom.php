<?php
/**
 * MSAdminControlPanel
 * Date: 26/05/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_reviews WHERE id = :id", array($_GET['id']), true);
    if($r['user_id'] != 0) header('Location: edit.php?id=' . $_GET['id']);
}

$user_data = array('nome' => '', 'cognome' => '');

$info_prodotto = array();

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <?php if(isset($r) && $r) { ?>
                                <h2 class="title-divider">Modifica Recensione</h2>
                            <?php } else { ?>
                                <h2 class="title-divider">Crea nuova recensione</h2>
                            <?php } ?>

                            <?php
                            if(isset($r['user_id'])) {
                                $info_prodotto = (new \MSFramework\Ecommerce\products())->getProductDetails($r['product_id'])[$r['product_id']];
                                $user_json = json_decode($r['fake_customer'], true);
                                $user_data = array('nome' => $user_json['nome'], 'cognome' => $user_json['cognome']);
                            } else if(isset($_GET['product'])) {
                                $info_prodotto = (new \MSFramework\Ecommerce\products())->getProductDetails($_GET['product'])[$_GET['product']];
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-8">
                                    <label>Rilasciata per il prodotto</label>
                                    <select id="id_prodotto" class="form-control">
                                        <?php if($info_prodotto) { ?>
                                            <option value="<?= $info_prodotto['id']; ?>"><?= $MSFrameworki18n->getFieldValue($info_prodotto['nome']); ?> [ID: <?= $info_prodotto['id']; ?>]</option>
                                        <?php } else { ?>
                                        <option></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label>Nella seguente variazione</label>
                                    <div id="contenitore_variazioni">
                                        <?php if($info_prodotto && count($info_prodotto['formattedAttributes']['variations'])) { ?>
                                            <?php foreach($info_prodotto['formattedAttributes']['variations'] as $id=>$variation) { ?>
                                                <div class="attribute">
                                                    <div class="attribute_value">
                                                        <select class="product_variation form-control" data-id="<?= $id; ?>">
                                                            <option value="">Seleziona <?= $MSFrameworki18n->getFieldValue($variation['nome']); ?></option>
                                                            <?php foreach($variation['value'] as $value=>$nome) { ?>
                                                                <option <?= (in_array($MSFrameworki18n->getFieldValue($nome), json_decode($r['product_variations'], true)) ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($nome); ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            <?php } ?>
                                        <?php } else { ?>
                                        Nessuna variazione disponibile
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Utente</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="Nome" class="form-control" id="nome_utente" value="<?= htmlentities($user_data['nome']); ?>" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="Cognome" class="form-control" id="cognome_utente" value="<?= htmlentities($user_data['cognome']); ?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Data</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control required" id="data_recensione" value="<?php echo (new DateTime($r['comment_date']))->format("d/m/Y H:i") ?>">
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Valutazione</label>
                                    <select id="valutazione" class="form-control">
                                        <?php
                                        for($i = 1; $i <= 5; $i++) echo '<option value="' . $i . '" ' . ($r['rating'] == $i ? 'selected' : '') . '>' . $i . ' stell' . ($i == "1" ? "a" : "e") . '</option>';
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Stato</label>
                                    <select class="form-control" id="review_status">
                                        <option value="1" <?php if($r['status'] == "1") { echo "selected"; } ?>>Approvata/Visibile sul sito</option>
                                        <option value="0" <?php if($r['status'] == "0") { echo "selected"; } ?>>Non Approvata/Non visibile sul sito</option>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12 m-t">
                                    <label>Testo della recensione</label>
                                    <textarea id="testo_recensione" class="form-control"><?php echo htmlentities($r['comment_body']); ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
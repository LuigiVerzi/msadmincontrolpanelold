<?php
/**
 * MSAdminControlPanel
 * Date: 26/05/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_questions ORDER BY answer, status, question_date asc") as $r) {

    if($r['user_id'] == 0) {
        $user = json_decode($r['fake_customer'], true);
    }
    else {
        $user = (new \MSFramework\customers())->getCustomerDataFromDB($r['user_id']);
    }

    $array['data'][] = array(
        $user['nome'] . ' ' . $user['cognome'] . ($r['user_id'] == 0 ? ' <span class="badge badge-info">Manuale</span>' : ''),
        (strlen($r['question']) < 130 ? $r['question'] : substr($r['question'], 0, 127) . "..."),
        $MSFrameworki18n->getFieldValue((new \MSFramework\Ecommerce\products())->getProductDetails($r['product_id'], "nome")[$r['product_id']]['nome'], true),
        (new DateTime($r['question_date']))->format("d/m/Y H:i"),
        ($r['answer'] == "" ? "No" : "Si"),
        ($r['status'] == "1" ? "Approvata" : "Non Approvata"),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

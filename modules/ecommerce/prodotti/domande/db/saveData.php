<?php
/**
 * MSAdminControlPanel
 * Date: 26/05/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);
if($db_action == "insert") {
    die();
}

$can_save = true;
if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "status" => $_POST['pStatus'],
    "answer" => $_POST['pAnswer'],
    "answer_date" => date("Y-m-d H:i:s"),
);

$before_save_question = $MSFrameworkDatabase->getAssoc('SELECT * FROM ecommerce_product_questions WHERE id = :id', array(":id" => $_POST['pID']), true);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
$result = $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_product_questions SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

// Invia l'email all'utente
if($before_save_question && empty($before_save_question['answer']) && !empty($array_to_save['answer'])) {
    (new \MSFramework\Ecommerce\questions())->sendNewAnswerEmail($_POST['pID']);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 26/05/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_questions WHERE id = :id", array($_GET['id']), true);
    if($r['user_id'] != 0) header('Location: edit.php?id=' . $_GET['id']);
}

$user_data = array('nome' => '', 'cognome' => '');
$info_prodotto = array();

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <?php if(isset($r) && $r) { ?>
                                <h2 class="title-divider">Modifica Domanda</h2>
                            <?php } else { ?>
                                <h2 class="title-divider">Crea nuova Domanda</h2>
                            <?php } ?>

                            <?php
                            if(isset($r['user_id'])) {
                                $info_prodotto = (new \MSFramework\Ecommerce\products())->getProductDetails($r['product_id'])[$r['product_id']];
                                $user_json = json_decode($r['fake_customer'], true);
                                $user_data = array('nome' => $user_json['nome'], 'cognome' => $user_json['cognome']);
                            } else if(isset($_GET['product'])) {
                                $info_prodotto = (new \MSFramework\Ecommerce\products())->getProductDetails($_GET['product'])[$_GET['product']];
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Domanda per il prodotto</label>
                                    <select id="id_prodotto" class="form-control">
                                        <?php if($info_prodotto) { ?>
                                            <option value="<?= $info_prodotto['id']; ?>"><?= $MSFrameworki18n->getFieldValue($info_prodotto['nome']); ?> [ID: <?= $info_prodotto['id']; ?>]</option>
                                        <?php } else { ?>
                                        <option></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Utente</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="Nome" class="form-control" id="nome_utente" value="<?= htmlentities($user_data['nome']); ?>" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="Cognome" class="form-control" id="cognome_utente" value="<?= htmlentities($user_data['cognome']); ?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <label>Data Domanda</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control required" id="data_domanda" value="<?php echo (new DateTime($r['question_date']))->format("d/m/Y H:i") ?>">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <label>Stato</label>
                                    <select class="form-control" id="question_status">
                                        <option value="1" <?php if($r['status'] == "1") { echo "selected"; } ?>>Approvata/Visibile sul sito</option>
                                        <option value="0" <?php if($r['status'] == "0") { echo "selected"; } ?>>Non Approvata/Non visibile sul sito</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row m-t">
                                <div class="col-lg-12">
                                    <label>Testo della domanda</label>
                                    <textarea id="testo_domanda" class="form-control"><?php echo htmlentities($r['question']); ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-8">
                                    <label>Testo della risposta</label>
                                    <textarea id="testo_risposta" class="form-control"><?php echo htmlentities($r['answer']); ?></textarea>
                                </div>
                                <div class="col-sm-4">
                                    <label>Data Risposta</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control required" id="data_risposta" value="<?php echo (new DateTime($r['answer_date']))->format("d/m/Y H:i") ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
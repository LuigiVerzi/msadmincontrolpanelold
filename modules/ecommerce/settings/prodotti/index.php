<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('ecommerce_prodotti');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Impostazioni Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Recensioni</h2>
                            <div class="row">
                                <?php
                                $manage_reviews_check = "";
                                if($r['manage_reviews'] == 1) {
                                    $manage_reviews_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Disabilitando questa opzione saranno disattivate le recensioni per tutti i prodotti"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="manage_reviews" <?php echo $manage_reviews_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita recensioni</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $moderate_reviews_check = "";
                                if($r['moderate_reviews'] == 1) {
                                    $moderate_reviews_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questa opzione dovrai approvare manualmente le recensioni prima che queste vengano visualizzate sul sito"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="moderate_reviews" <?php echo $moderate_reviews_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Modera recensioni</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="title-divider">Categorie</h2>
                            <div class="row">
                                <?php
                                $show_category_multilevel_slug = "";
                                if($r['category_multilevel_slug'] == 1 || !isset($r['category_multilevel_slug'])) {
                                    $show_category_multilevel_slug = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questa opzione, lo slug del prodotto sarà preceduto dalla categoria al quale appartiene."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="category_multilevel_slug" <?php echo $show_category_multilevel_slug ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Utilizza slug multilivello</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="title-divider">Prezzi</h2>
                            <div class="row">
                                <?php
                                $show_lowest_price_check = "";
                                if($r['show_lowest_price'] == 1) {
                                    $show_lowest_price_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questa opzione, se il prodotto ha diversi prezzi per le variazioni, verrà mostrato all'utente il prezzo più basso fin quando non verrà selezionata una variazione."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="show_lowest_price" <?php echo $show_lowest_price_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Usa prezzo più basso</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="title-divider">Peso</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Unità di Misura</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci l'unità di misura del peso che vuoi utilizzare"></i></span>
                                    <select id="weight_um" class="form-control">
                                        <option value="kg" <?php if($r['weight_um'] == "kg") { echo "selected"; } ?>>Chili (KG)</option>
                                        <option value="gr" <?php if($r['weight_um'] == "gr") { echo "selected"; } ?>>Grammi (GR)</option>
                                    </select>
                                </div>
                            </div>

                            <h2 class="title-divider">Domande Clienti</h2>
                            <div class="row">
                                <?php
                                $enable_questions_check = "";
                                if($r['enable_questions'] == 1) {
                                    $enable_questions_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questa opzione, i clienti potranno fare domande all'interno della pagina del prodotto ricevendo le risposte direttamente dall'admin."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_questions" <?php echo $enable_questions_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Domande</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="title-divider">Allegati</h2>
                            <div class="row">
                                <?php
                                $enable_attachments_check = "";
                                if($r['enable_attachments'] == 1) {
                                    $enable_attachments_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questa opzione, sarà possibile allegare dei file scaricabili su ogni prodotto."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_attachments" <?php echo $enable_attachments_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Allegati</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
function initForm() {
    initClassicTabsEditForm();

    $('#manage_reviews').on('ifToggled', function() {
        if($('#manage_reviews:checked').length == 1) {
            $('#moderate_reviews').iCheck('enable');
        } else {
            $('#moderate_reviews').iCheck('disable');
            $('#moderate_reviews').iCheck('uncheck');
        }
    })
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pManageReviews": $('#manage_reviews:checked').length,
            "pModerateReviews": $('#moderate_reviews:checked').length,
            "pShowLowestPrice": $('#show_lowest_price:checked').length,
            "pEnableQuestions": $('#enable_questions:checked').length,
            "pEnableAttachments": $('#enable_attachments:checked').length,
            "pCategoryMultilevelSlug": $('#category_multilevel_slug:checked').length,
            "pWeightUM": $('#weight_um').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
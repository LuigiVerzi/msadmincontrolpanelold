<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'ecommerce_prodotti'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
    $r_settings = $MSFrameworkCMS->getCMSData('ecommerce_prodotti');
}

$array_to_save = array(
    "value" => json_encode(array(
        "manage_reviews" => $_POST['pManageReviews'],
        "moderate_reviews" => $_POST['pModerateReviews'],
        "show_lowest_price" => $_POST['pShowLowestPrice'],
        "enable_questions" => $_POST['pEnableQuestions'],
        "enable_attachments" => $_POST['pEnableAttachments'],
        "category_multilevel_slug" => $_POST['pCategoryMultilevelSlug'],
        "weight_um" => $_POST['pWeightUM'],
        "social" => $_POST['pSocial'],
    )),
    "type" => "ecommerce_prodotti"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'ecommerce_prodotti'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
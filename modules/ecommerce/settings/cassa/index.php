<?php
/**
 * MSAdminControlPanel
 * Date: 28/04/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('ecommerce_cassa');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Impostazioni Generali</h1>
                        <fieldset>
                            <div class="row">
                                <?php
                                $enable_coupons_check = "";
                                if($r['enable_coupons'] == 1) {
                                    $enable_coupons_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_coupons" <?php echo $enable_coupons_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Coupons</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $enable_guest_checkout_check = "";
                                if($r['enable_guest_checkout'] == 1) {
                                    $enable_guest_checkout_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_guest_checkout" <?php echo $enable_guest_checkout_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Acquisto Ospiti</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
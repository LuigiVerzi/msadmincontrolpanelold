function initForm() {

}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pEnableCoupons": $('#enable_coupons:checked').length,
            "pGuestCheckout": $('#enable_guest_checkout:checked').length
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mandatory_bonifico") {
                bootbox.error("Se abiliti il pagamento con Bonifico Bancario, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_contrassegno") {
                bootbox.error("Se abiliti il pagamento con Contrassegno, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_postepay") {
                bootbox.error("Se abiliti il pagamento con PostePay, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_paypal") {
                bootbox.error("Se abiliti il pagamento con PayPal, devi compilare tutti i dati della relativa scheda!");
            }  else if(data.status == "mandatory_nexi") {
                bootbox.error("Se abiliti il pagamento con Nexi, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_stripe") {
                bootbox.error("Se abiliti il pagamento con Stripe, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "paypal_mail_not_valid") {
                bootbox.error("La mail inserita all'interno della scheda PayPal è in un formato non valido!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 28/04/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'ecommerce_points'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
    $r_old_data = $MSFrameworkCMS->getCMSData('ecommerce_points');
}

foreach($_POST['pMessages'] as $k => $v) {
    $_POST['pMessages'][$k] = strip_tags($v, '<br><p><div><a><b><strong><small><span><i>');
}

$array_to_save = array(
    "value" => json_encode(array(
        "enabled" => $_POST['pEnablePoints'],
        "assignation" => $_POST['pAssignation'],
        "conversion" => $_POST['pConversion'],
        "messages" => $_POST['pMessages'],
    )),
    "type" => "ecommerce_points"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'ecommerce_points'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
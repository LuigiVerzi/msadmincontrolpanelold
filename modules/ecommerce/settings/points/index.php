<?php
/**
 * MSAdminControlPanel
 * Date: 28/04/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('ecommerce_points');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Impostazioni Generali</h1>
                        <fieldset>

                            <div class="row">
                                <?php
                                $enable_points_check = "";
                                if($r['enabled'] == 1) {
                                    $enable_points_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <div class="styled-checkbox form-control" style="margin: 0;">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_points" <?php echo $enable_points_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita assegnazione Punti</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="enable_points_container" style="margin-top: 15px;">

                                <div class="row">
                                    <div class="col-lg-5">

                                        <h2 class="title-divider" style="margin-top: 0;">Assegnazione</h2>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Assegna <small>(punti)</small></label>
                                                <input id="assign_points_number" name="checkout_points" type="number" min="0" class="pointsAssignation form-control" value="<?= htmlentities($r['assignation']['checkout_points']); ?>" placeholder="N. Punti">
                                            </div>

                                            <div class="col-sm-6">
                                                <label>Per ogni <small>(<?= CURRENCY_SYMBOL; ?> di spesa)</small></label>
                                                <input id="assign_points_value" name="checkout_value" type="number" min="0" class="pointsAssignation form-control" value="<?= htmlentities($r['assignation']['checkout_value']); ?>" placeholder="<?= CURRENCY_SYMBOL; ?> di spesa">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Assegna alla registrazione <small>(punti)</small></label>
                                                <input id="assign_registration_number" name="registration_points"  type="number" min="0" class="pointsAssignation form-control" value="<?= htmlentities($r['assignation']['registration_points']); ?>" placeholder="N. Punti">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Assegna alla registrazione Newsletter <small>(punti)</small></label>
                                                <input id="assign_newsletter_signup_number" name="newsletter_points" type="number" min="0" class="pointsAssignation form-control" value="<?= htmlentities($r['assignation']['newsletter_points']); ?>" placeholder="N. Punti">
                                            </div>
                                        </div>

                                        <h2 class="title-divider">Utilizzo</h2>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Tasso di conversione <small>(punti)</small></label>
                                                <input id="points_conversion_number" name="points" type="number" min="0" class="pointsConversion form-control" value="<?= htmlentities($r['conversion']['points']); ?>" placeholder="N. Punti">
                                            </div>

                                            <div class="col-sm-6">
                                                <label>Equivalgono a <small>(<?= CURRENCY_SYMBOL; ?> di spesa)</small></label>
                                                <input id="points_conversion_value" name="value" type="number" min="0" class="pointsConversion form-control" value="<?= htmlentities($r['conversion']['value']); ?>" placeholder="<?= CURRENCY_SYMBOL; ?> di spesa">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Punti minimi utilizzabili</label>
                                                <input id="points_min_value" name="min_points_value" type="number" min="0" class="pointsConversion form-control" value="<?= htmlentities($r['conversion']['min_points_value']); ?>" placeholder="N. Punti">
                                            </div>

                                            <div class="col-sm-6">
                                                <label>Spesa minima <small>(<?= CURRENCY_SYMBOL; ?>)</small></label>
                                                <input id="points_min_checkout" name="min_checkout_value" type="number" min="0" class="pointsConversion form-control" value="<?= htmlentities($r['conversion']['min_checkout_value']); ?>" placeholder="<?= CURRENCY_SYMBOL; ?> di spesa minimi">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-8">
                                                <label>Sconto checkout massimo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Indica lo sconto massimo applicabile al carrello durante il checkout"></i></span>
                                                <input id="max_checkout_discount" name="max_sale_value" type="number" min="0" class="pointsConversion form-control" value="<?= htmlentities($r['conversion']['max_sale_value']); ?>" placeholder="Valore sconto">
                                            </div>
                                            <div class="col-sm-4">
                                                <label> </label>
                                                <select class="pointsConversion form-control" name="max_sale_type" id="max_checkout_discount_type">
                                                    <option value="€"><?= CURRENCY_SYMBOL; ?></option>
                                                    <option value="%" <?= ($r['conversion']['max_sale_type'] === '%' ? 'selected' : ''); ?>>%</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-7 col-attached-right">

                                        <h2 class="title-divider" style="margin-top: 0;">Messaggi</h2>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Scheda del prodotto <small>(Prodotto standard)</small></label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il messaggio che verrà visualizzato nella scheda del prodotto. (Sopra il pulsante Aggiungi al carrello)"></i></span>
                                                <textarea id="messages_product_page" name="product" class="form-control pointsMessage" data-shortcodes="{points}" placeholder="Shortcode disponibili: {points}"><?= htmlentities($r['messages']['product']); ?></textarea>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Scheda del prodotto <small>(Prodotto variabile)</small></label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il messaggio che verrà visualizzato nella scheda del prodotto variabile. (Sopra il pulsante Aggiungi al carrello)"></i></span>
                                                <textarea id="messages_product_variable_page" name="product_variable" class="form-control pointsMessage" data-shortcodes="{points}" placeholder="Shortcode disponibili: {points}"><?= htmlentities($r['messages']['product_variable']); ?></textarea>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Pagina Checkout (Guadagno dei punti)</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Messaggio che invita il cliente ad effettuare l'ordine. Verrà visualizzato nella pagina del checkout."></i></span>
                                                <textarea id="messages_earn_checkout_page" name="checkout_earning" class="form-control pointsMessage" data-shortcodes="{points}" placeholder="Shortcode disponibili: {points}"><?= htmlentities($r['messages']['checkout_earning']); ?></textarea>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Pagina Checkout (Sconto disponibile)</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Indica al cliente lo sconto che verrà applicato all'ordine. Verrà visualizzato nella pagina del checkout."></i></span>
                                                <textarea id="messages_sale_checkout_page" name="checkout_sale" class="form-control pointsMessage" data-shortcodes="{points}|{points-value}" placeholder="Shortcode disponibili: {points} - {points-value}"><?= htmlentities($r['messages']['checkout_sale']); ?></textarea>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Testo Pulsante 'Applica Sconto'</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il testo del pulsante 'Applica Sconto'."></i></span>
                                                <input id="messages_apply_sale_btn_checkout_page" name="checkout_apply_sale_txt" class="form-control pointsMessage" value="<?= htmlentities($r['messages']['checkout_apply_sale_txt']); ?>" data-shortcodes="{points}|{points-value}" placeholder="">
                                            </div>

                                            <div class="col-sm-6">
                                                <label>Classe CSS Pulsante 'Applica Sconto'</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La classe CSS da applicare al pulsante applica sconto"></i></span>
                                                <input id="messages_apply_sale_btn_css_checkout_page" name="checkout_apply_sale_class" class="form-control pointsMessage" value="<?= htmlentities($r['messages']['checkout_apply_sale_class']); ?>"  placeholder="">
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="alert alert-info" style="margin: 0;">
                                            È possibile personalizzare il contenuto delle email relative ai punti tramite il modulo <a href="<?= (new \MSSoftware\modules())->getModuleUrlByID('manage_email_templates'); ?>">Template Email</a>.
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
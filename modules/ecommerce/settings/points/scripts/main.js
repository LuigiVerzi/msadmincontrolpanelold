function initForm() {
    initClassicTabsEditForm();

    $('#enable_points').on('ifToggled', function () {
        if($(this).is(':checked')) {
            $('.enable_points_container').show();
        } else {
            $('.enable_points_container').hide();
        }
    }).trigger('ifToggled');

    $('textarea.pointsMessage').each(function () {
        initTinyMCE($(this), {small: true, shortcodes: {custom: $(this).data('shortcodes').split('|')}}, {height: 100});
    });

}

function moduleSaveFunction() {

    var pointsMessage = {};
    $('.pointsMessage').each(function () {
       pointsMessage[$(this).attr('name')] = ($(this)[0].tagName === 'TEXTAREA' ? tinymce.get($(this).attr('id')).getContent() : $(this).val());
    });

    var pointsAssignation = {};
    $('.pointsAssignation').each(function () {
        pointsAssignation[$(this).attr('name')] = $(this).val();
    });

    var pointsConversion = {};
    $('.pointsConversion').each(function () {
        pointsConversion[$(this).attr('name')] = $(this).val();
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pEnablePoints": $('#enable_points:checked').length,
            "pAssignation": pointsAssignation,
            "pConversion": pointsConversion,
            "pMessages": pointsMessage
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
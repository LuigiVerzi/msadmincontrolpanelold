<?php
/**
 * MSAdminControlPanel
 * Date: 02/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM gallery") as $r) {
    $array['data'][] = array(
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['titolo'], true),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

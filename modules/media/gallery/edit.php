<?php
/**
 * MSAdminControlPanel
 * Date: 02/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM gallery WHERE id = :id", array(":id" => $_GET['id']), true);

    $only_photo = array();
    foreach(json_decode($r['gallery']) as $k=>$g) {
        if(is_array($g)) {
            $only_photo[] = $g[0];
        }
        else
        {
            $only_photo[] = $g;
        }
    }
    $only_photo = json_encode($only_photo);

}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Gallery</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Titolo Gallery*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['titolo']) ?>"></i></span>
                                    <input id="titolo" name="titolo" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['titolo'])) ?>">
                                </div>

                                <div class="col-sm-9">
                                    <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione']) ?>"></i></span>
                                    <textarea id="descr" name="descr" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r['descrizione']) ?></textarea>
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>
                            <h2 class="title-divider">Immagini</h2>

                            <?php
                            (new \MSFramework\uploads('GALLERY'))->initUploaderHTML("images", $only_photo);
                            ?>
                        </fieldset>
                        <h1>Proprietà</h1>
                        <fieldset>
                            <div id="images_info">
                                <div class="row">
                                    <?php foreach(json_decode($r['gallery'], true) as $img_arr) {

                                        $immagine = $img_arr;
                                        $titolo = '';
                                        $testo = '';
                                        if(is_array($img_arr)) {
                                            $immagine = $img_arr[0];
                                            $titolo = $img_arr[1];
                                            $testo = $img_arr[2];
                                        }
                                        ?>

                                        <div class="col-lg-2 col-md-3 col-sm-6">
                                            <div class="image_info_container" data-id="<?= htmlentities($immagine); ?>">
                                                <img src="<?= UPLOAD_GALLERY_FOR_DOMAIN_HTML; ?>tn/<?= $immagine; ?>" class="img-thumbnail" style="margin: 0 auto 15px;">

                                                <label>Titolo Immagine</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($titolo) ?>"></i></span>
                                                <input type="text" class="form-control image_title" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($titolo)) ?>">
                                                <br>
                                                <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($testo) ?>"></i></span>
                                                <textarea class="form-control image_descr" rows="3"><?php echo $MSFrameworki18n->getFieldValue($testo) ?></textarea>


                                            </div>
                                            <div class="hr-line-dashed"></div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div id="image_info_template" style="display: none;">
                                <div class="col-lg-2 col-md-3 col-sm-6">
                                    <div class="image_info_container" data-id="">
                                        <img src="<?= UPLOAD_TMP_FOR_DOMAIN_HTML; ?>tn/" class="img-thumbnail" style="margin: 0 auto 15px;">
                                        <label>Titolo Immagine</label>
                                        <input type="text" class="form-control image_title" value="">
                                        <label style="margin-top: 15px;">Descrizione</label>
                                        <textarea class="form-control image_descr" rows="3"></textarea>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
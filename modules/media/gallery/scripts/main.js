$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initOrak('images', 100, 50, 50, 200, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
}

function getImagesWithInfo() {
    var images = composeOrakImagesToSave('images');

    images.forEach(function (image, key) {
        if($('.image_info_container[data-id="' + image + '"]').length) {
            var $elem = $('.image_info_container[data-id="' + image + '"]');
            images[key] = [image, $elem.find('.image_title').val(), $elem.find('.image_descr').val()];
        }
    });

    return images;
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pTitolo": $('#titolo').val(),
            "pDescrizione": $('#descr').val(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pImagesWithInfoAry": getImagesWithInfo(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}

function  orakuploader_finished(data) {
    var $element = $($.parseHTML($('#image_info_template').html()));

    $element.find('.image_info_container').attr('data-id', data);
    $element.find('img').attr('src', $element.find('img').attr('src') + data);

    $element.appendTo($('#images_info .row'));
}

function orakuploader_picture_deleted(data) {
    $('#images_info').find('[data-id="' + data + '"]').parent().remove();

}
<?php
/**
 * MSAdminControlPanel
 * Date: 02/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pTitolo'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$uploader = new \MSFramework\uploads('GALLERY');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT gallery, titolo, descrizione FROM gallery WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pTitolo'], 'oldValue' => $r_old_data['titolo']),
        array('currentValue' => $_POST['pDescrizione'], 'oldValue' => $r_old_data['descrizione']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}


foreach($_POST['pImagesWithInfoAry'] as $k => $imageInfoAry) {

    $oldInfoAry = array('titolo' => json_decode(array($MSFrameworki18n->primary_lang_id => '')), 'descrizione' => json_decode(array($MSFrameworki18n->primary_lang_id => '')));
    if(isset($r_old_data['gallery']) && json_decode($r_old_data['gallery'])) {
        foreach(json_decode($r_old_data['gallery'], true) as $old_img) {
            if(is_array($old_img) && $old_img[0] == $imageInfoAry[0]) {
                $oldInfoAry['titolo'] = $old_img[1];
                $oldInfoAry['descrizione'] = $old_img[2];
            }
        }
    }

    $_POST['pImagesWithInfoAry'][$k][1] =  $MSFrameworki18n->setFieldValue($oldInfoAry['titolo'], $imageInfoAry[1]);
    $_POST['pImagesWithInfoAry'][$k][2] =  $MSFrameworki18n->setFieldValue($oldInfoAry['descrizione'], $imageInfoAry[2]);

}

$array_to_save = array(
    "titolo" => $MSFrameworki18n->setFieldValue($r_old_data['titolo'], $_POST['pTitolo']),
    "descrizione" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione'], $_POST['pDescrizione']),
    "gallery" => json_encode($_POST['pImagesWithInfoAry']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO gallery ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE gallery SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['gallery'], true));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
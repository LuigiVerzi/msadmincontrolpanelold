<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pSliderID'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$uploader = new \MSFramework\uploads('SLIDERS');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry'], array("png", "jpeg", "jpg", "gif", "mp4", "svg"));
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$ary_files_mobile = $uploader->prepareForSave($_POST['pImagesMobileAry'], array("png", "jpeg", "jpg", "gif", "mp4", "svg"));
if($ary_files_mobile === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}
$ary_files_overlay = $uploader->prepareForSave($_POST['pImagesOverlayAry'], array("png", "jpeg", "jpg", "gif", "mp4", "svg"));
if($ary_files_overlay === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT images, images_mobile, images_overlay, titolo, sottotitolo, intestazione, btn_text, background_video FROM sliders WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pTitolo'], 'oldValue' => $r_old_data['titolo']),
        array('currentValue' => $_POST['pSottotitolo'], 'oldValue' => $r_old_data['sottotitolo']),
        array('currentValue' => $_POST['pIntestazione'], 'oldValue' => $r_old_data['intestazione']),
        array('currentValue' => $_POST['pBTNText'], 'oldValue' => $r_old_data['btn_text']),
        array('currentValue' => $_POST['pBTN2Text'], 'oldValue' => $r_old_data['btn2_text']),
        array('currentValue' => $_POST['pVideoBackground'], 'oldValue' => $r_old_data['background_video']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "slider_id" => $_POST['pSliderID'],
    "titolo" => $MSFrameworki18n->setFieldValue($r_old_data['titolo'], $_POST['pTitolo']),
    "sottotitolo" => $MSFrameworki18n->setFieldValue($r_old_data['sottotitolo'], $_POST['pSottotitolo']),
    "intestazione" => $MSFrameworki18n->setFieldValue($r_old_data['intestazione'], $_POST['pIntestazione']),
    "btn_text" => $MSFrameworki18n->setFieldValue($r_old_data['btn_text'], $_POST['pBTNText']),
    "btn_href" => $_POST['pBTNHref'],
    "btn2_text" => $MSFrameworki18n->setFieldValue($r_old_data['btn2_text'], $_POST['pBTN2Text']),
    "btn2_href" => $_POST['pBTN2Href'],
    "use_in_home" => $_POST['pUseInHome'],
    "bg_type" => $_POST['pBGType'],
    "desktop_rollback" => $_POST['pDesktopRollback'],
    "images" => json_encode($ary_files),
    "images_mobile" => json_encode($ary_files_mobile),
    "images_overlay" => json_encode($ary_files_overlay),
    "background_video" => $MSFrameworki18n->setFieldValue($r_old_data['background_video'], $_POST['pVideoBackground']),
    "background_color" => $_POST['pColorBackground'],
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO sliders ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE sliders SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
        $uploader->unlink($ary_files_mobile);
        $uploader->unlink($ary_files_overlay);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['images'], true));
    $uploader->unlink($ary_files_mobile, json_decode($r_old_data['images_mobile'], true));
    $uploader->unlink($ary_files_overlay, json_decode($r_old_data['images_overlay'], true));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
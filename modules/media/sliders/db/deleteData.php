<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\sliders())->getSliderDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['images'], true), json_decode($r['images_mobile'], true));
}

if($MSFrameworkDatabase->deleteRow("sliders", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('SLIDERS'))->unlink($images_to_delete);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


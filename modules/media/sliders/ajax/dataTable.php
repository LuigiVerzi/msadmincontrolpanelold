<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM sliders") as $r) {
    $array['data'][] = array(
        $r['id'],
        $r['slider_id'],
        ($r['use_in_home'] == "1") ? "Si" : "No",
        ($r['desktop_rollback'] == "1" || count(json_decode($r['images_mobile'])) ? "Si" : "No"),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

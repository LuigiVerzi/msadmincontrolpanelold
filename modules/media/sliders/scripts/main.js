$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initOrak('images', 1, 200, 60, 100, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png', 'image/gif', 'video/mp4', 'image/svg+xml']);
    initOrak('mobile_images', 1, 200, 60, 100, getOrakImagesToPreattach('mobile_images'), false, ['image/jpeg', 'image/gif', 'image/png', 'video/mp4', 'image/svg+xml']);
    initOrak('overlay_images', 1, 200, 60, 100, getOrakImagesToPreattach('overlay_images'), false, ['image/jpeg', 'image/png', 'image/gif', 'video/mp4', 'image/svg+xml']);

    $('.colorpicker-component').colorpicker();
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pSliderID": $('#slider_id').val(),
            "pTitolo": $('#titolo').val(),
            "pSottotitolo": $('#sottotitolo').val(),
            "pIntestazione": $('#intestazione').val(),
            "pBTNText": $('#btn_text').val(),
            "pBTNHref": $('#btn_href').val(),
            "pBTN2Text": $('#btn2_text').val(),
            "pBTN2Href": $('#btn2_href').val(),
            "pBGType": $('#bg_type').val(),
            "pUseInHome": $('#use_in_home:checked').length,
            "pDesktopRollback": $('#desktop_rollback:checked').length,
            "pImagesAry": composeOrakImagesToSave('images'),
            "pImagesMobileAry": composeOrakImagesToSave('mobile_images'),
            "pImagesOverlayAry": composeOrakImagesToSave('overlay_images'),
            "pVideoBackground": $('#video_background_url').val(),
            "pColorBackground": $('#bg_color').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
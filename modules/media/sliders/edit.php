<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM sliders WHERE id = :id", array(":id" => $_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Slider</h1>
                        <fieldset>

                            <h2 class="title-divider">Informazioni Generali</h2>

                            <div class="row">
                                <div class="col-lg-10 col-md-9">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Nome Slider*</label>
                                            <input id="slider_id" name="slider_id" type="text" class="form-control required" value="<?php echo htmlentities($r['slider_id']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Titolo Slider</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['titolo']) ?>"></i></span>
                                            <input id="titolo" name="titolo" type="text" class="form-control msShort" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['titolo'])) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Sottotitolo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['sottotitolo']) ?>"></i></span>
                                            <input id="sottotitolo" name="sottotitolo" type="text" class="form-control msShort" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['sottotitolo'])) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Intestazione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['intestazione']) ?>"></i></span>
                                            <input id="intestazione" name="intestazione" type="text" class="form-control msShort" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['intestazione'])) ?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>Testo pulsante</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['btn_text']) ?>"></i></span>
                                            <input id="btn_text" name="btn_text" type="text" class="form-control msShort" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['btn_text'])) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>HREF pulsante</label>
                                            <input id="btn_href" name="btn_href" type="text" class="form-control msShort" value="<?php echo htmlentities($r['btn_href']) ?>">
                                        </div>

                                        <?php
                                        $is_active_check = "checked";
                                        if($r['use_in_home'] == 0 && $_GET['id'] != "") {
                                            $is_active_check = "";
                                        }
                                        ?>
                                        <div class="col-sm-4">
                                            <label>&nbsp;</label>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="use_in_home" <?php echo $is_active_check ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Mostra in home page</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4"  data-optional data-default-view="0">
                                            <label>Testo pulsante 2</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['btn2_text']) ?>"></i></span>
                                            <input id="btn2_text" name="btn2_text" type="text" class="form-control msShort" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['btn2_text'])) ?>">
                                        </div>

                                        <div class="col-sm-4"  data-optional data-default-view="0">
                                            <label>HREF pulsante 2</label>
                                            <input id="btn2_href" name="btn2_href" type="text" class="form-control msShort" value="<?php echo htmlentities($r['btn2_href']) ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-3">
                                    <label>Immagine</label>
                                    <?php (new \MSFramework\uploads('SLIDERS'))->initUploaderHTML("overlay_images", $r['images_overlay']); ?>
                                </div>
                            </div>

                            <h2 class="title-divider">Sfondo</h2>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Immagine Desktop</label>
                                    <?php
                                    (new \MSFramework\uploads('SLIDERS'))->initUploaderHTML("images", $r['images']);
                                    ?>
                                </div>

                                <div class="col-sm-4">
                                    <label>Immagine Mobile</label>
                                    <?php
                                    (new \MSFramework\uploads('SLIDERS'))->initUploaderHTML("mobile_images", $r['images_mobile']);
                                    ?>
                                </div>

                                <div class="col-sm-4">

                                    <div class="row">
                                        <div class="col-lg-4 col-md-12" data-optional data-default-view="0">
                                            <label>Colore</label>
                                            <div class="input-group colorpicker-component">
                                                <input id="bg_color" name="bg_color" type="text" class="form-control" value="<?php echo htmlentities($r['background_color']) ?>">
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-8 col-md-12">
                                            <label>Video (YouTube)</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['background_video']) ?>"></i></span>
                                            <input id="video_background_url" name="video_background_url" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['background_video'])); ?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4 col-md-12">
                                            <label>Overlay</label>
                                            <select id="bg_type" name="bg_type" class="form-control">
                                                <option value="">Nessuno</option>
                                                <option value="light" <?php if($r['bg_type'] == "light") { echo "selected"; } ?>>Chiaro</option>
                                                <option value="dark" <?php if($r['bg_type'] == "dark") { echo "selected"; } ?>>Scuro</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-8 col-md-12">
                                            <label>Rollback</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se spuntato, consente il caricamento dell'immagine 'Desktop' anche su mobile qualora la versione ottimizzata per mobile non dovesse essere presente. In caso contrario, lo slider verrà escluso dalle visite provenienti da mobile."></i></span>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="desktop_rollback" <?php echo ($r['desktop_rollback'] == 1 || $_GET['id'] == "" ? 'checked' : ''); ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Consenti rollback su versione desktop</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
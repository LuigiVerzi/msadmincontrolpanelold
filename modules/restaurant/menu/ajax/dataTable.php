<?php
/**
 * MSAdminControlPanel
 * Date: 26/06/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM restaurant_menu") as $r) {
    $array['data'][] = array(
        $MSFrameworki18n->getFieldValue($r['nome']),
        ($r['is_active'] == "1") ? "Si" : "No",
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

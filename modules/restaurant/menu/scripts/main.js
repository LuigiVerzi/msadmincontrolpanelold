$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('.groupDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-info",
        "deleteButtonClasses": "btn btn-danger",
        "addButtonText": "Aggiungi sezione",
        "deleteButtonText": "Elimina sezione",
        "afterAddCallback": function(dup_element) {
            $('.groupDuplication:last').find('.rowDuplication').not(':first').remove();
            $('.groupDuplication:last').find('.rowDuplication').find('.easyRowDuplicationDelBtn').parent().remove();

            $('.groupDuplication:last').find('.section_name').val('');

            $('.groupDuplication:last').find('.rowDuplication:last').find('.product_name').val('');
            $('.groupDuplication:last').find('.rowDuplication:last').find('.product_descr').val('');
            $('.groupDuplication:last').find('.rowDuplication:last').find('.product_price').val('');

            $('.groupDuplication:last').find('.rowDuplication:last').find('.prev_upl_image').html('');
            $('.groupDuplication:last').find('.rowDuplication:last').find('.image_container .clone_item').remove();
            $('.groupDuplication:last').find('.rowDuplication:last').find('.image_container [id$="DDArea"]').remove();
            $('.groupDuplication:last').find('.rowDuplication:last').find('.image_container').append('<div id="image_x" class="image" orakuploader="on"></div>');

            initUploaders();

            initFieldsDuplication(true);
        },
        "afterDeleteCallback": function() {
        },
    });

    initFieldsDuplication(false);
    initUploaders();
}

function initFieldsDuplication(last) {
    //questa funzione gestisce la duplicazione del singolo prodotto

    selector = "";
    if(last) {
        selector = ':last';
    }

    $('.rowDuplication' + selector).easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi campo",
        "deleteButtonText": "Elimina campo",
        "afterAddCallback": function(dup_element) {
            $(dup_element).closest('.groupDuplication').find('.rowDuplication:last').find('.product_name').val('');
            $(dup_element).closest('.groupDuplication').find('.rowDuplication:last').find('.product_descr').val('');
            $(dup_element).closest('.groupDuplication').find('.rowDuplication:last').find('.product_price').val('');

            $(dup_element).closest('.groupDuplication').find('.rowDuplication:last').find('.prev_upl_image').html('');
            $(dup_element).closest('.groupDuplication').find('.rowDuplication:last').find('.image_container .clone_item').remove();
            $(dup_element).closest('.groupDuplication').find('.rowDuplication:last').find('.image_container [id$="DDArea"]').remove();
            $(dup_element).closest('.groupDuplication').find('.rowDuplication:last').find('.image_container').append('<div id="image_x" class="image" orakuploader="on"></div>');

            initUploaders();
        },
        "afterDeleteCallback": function() {
        },
    });
}

function initUploaders() {
    $('.image_container').each(function(index) {
        if($('.image_container:eq(' + index + ')').find('div[id$="DDArea"]').length != 0) {
            return true;
        }

        $('.image_container:eq(' + index + ') .image').attr('id', 'image_' + index);
        $('.image_container:eq(' + index + ') .image_realPath').attr('id', 'image_' + index + "_realPath");

        preattach_array_image = new Array();
        if($('.prev_upl_image:eq(' + index + ')').val() != "") {
            img_to_cycle = $.parseJSON($('.prev_upl_image:eq(' + index + ')').val()) || new Array();
            $(img_to_cycle).each(function(k, v) {
                preattach_array_image.push(v);
            })
        }

        initOrak('image_' + index, 1, 400, 200, 200, preattach_array_image, false, ['image/jpeg', 'image/png']);
    })
}

function moduleSaveFunction() {
    fields_ary_general = new Array();
    $('.groupDuplication').each(function(row_index) {
        fields_ary = new Object();
        section_name = $(this).find('.section_name').val();
        if(section_name == "") {
            return true;
        }

        fields_ary['section'] = new Object();
        fields_ary['products'] = new Object();

        fields_ary['section']['name'] = section_name;

        $(this).find('.rowDuplication').each(function(row_index_product) {
            product_name = $(this).find('.product_name').val();
            if(product_name == "") {
                return true;
            }

            fields_ary['products'][row_index_product] = new Object();
            fields_ary['products'][row_index_product]['name'] = product_name;
            fields_ary['products'][row_index_product]['descr'] = $(this).find('.product_descr').val();
            fields_ary['products'][row_index_product]['price'] = $(this).find('.product_price').val();

            images_ary = new Array();
            $(this).find('input[name^=image]').each(function(k, v) {
                images_ary.push($(this).val());
            })

            fields_ary['products'][row_index_product]['images'] = images_ary;
        });

        if(Object.keys(fields_ary['products']).length != 0) {
            fields_ary_general.push(fields_ary);
        }
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pIsActive": $('#is_active:checked').length,
            "pFields": fields_ary_general,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 26/06/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;
if($_POST['pNome'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT fields, nome FROM restaurant_menu WHERE id = :id", array(":id" => $_POST['pID']), true);

    $old_fields = json_decode($r_old_data['fields'], true);
    foreach($old_fields as $old_field_v) {
        foreach($old_field_v['products'] as $product) {
            foreach($product['images'] as $single_file) {
                $old_img[] = $single_file;
            }
        }
    }
}

$images_from_fields = array();
foreach($_POST['pFields'] as $kFieldGlobal => $vFieldGlobal) {
    if ($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $vFieldGlobal['section']['name'], 'oldValue' => $old_fields[$kFieldGlobal]['section']['name']),
        )) == false
    ) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $_POST['pFields'][$kFieldGlobal]['section']['name'] = $MSFrameworki18n->setFieldValue($old_fields[$kFieldGlobal]['section']['name'], $vFieldGlobal['section']['name']);

    foreach($vFieldGlobal['products'] as $kField => $vField) {
        if ($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                array('currentValue' => $vField['name'], 'oldValue' => $old_fields[$kFieldGlobal]['products'][$kField]['name']),
                array('currentValue' => $vField['descr'], 'oldValue' => $old_fields[$kFieldGlobal]['products'][$kField]['descr']),
            )) == false
        ) {
            echo json_encode(array("status" => "no_datalang_for_primary"));
            die();
        }

        if(!strstr($vField['price'], ".") && $vField['price'] != "") {
            $_POST['pFields'][$kFieldGlobal]['products'][$kField]['price'] .= ".00";
        }

        if(!is_numeric($vField['price']) && $vField['price'] != "") {
            echo json_encode(array("status" => "price_numeric"));
            die();
        }

        $_POST['pFields'][$kFieldGlobal]['products'][$kField]['name'] = $MSFrameworki18n->setFieldValue($old_fields[$kFieldGlobal]['products'][$kField]['name'], $vField['name']);
        $_POST['pFields'][$kFieldGlobal]['products'][$kField]['descr'] = $MSFrameworki18n->setFieldValue($old_fields[$kFieldGlobal]['products'][$kField]['descr'], $vField['descr']);

        foreach ($vField['images'] as $single_file) {
            $images_from_fields[] = $single_file;
        }
    }
}

$uploader = new \MSFramework\uploads('RESTAURANT_MENU');
$ary_files = $uploader->prepareForSave($images_from_fields);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "is_active" => $_POST['pIsActive'],
    "fields" => json_encode($_POST['pFields']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO restaurant_menu ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE restaurant_menu SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, $old_img);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 26/06/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\Ristorante\menu())->getMenuDetails($_POST['pID']) as $r) {
    foreach(json_decode($r['fields'], true) as $old_fields) {
        foreach ($old_fields['products'] as $product) {
            $images_to_delete = array_merge_recursive ($images_to_delete, $product['images']);
        }
    }
}

if($MSFrameworkDatabase->deleteRow("restaurant_menu", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('RESTAURANT_MENU'))->unlink($images_to_delete);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


<?php
/**
 * MSAdminControlPanel
 * Date: 26/06/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM restaurant_menu WHERE id = :id", array($_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome Menù*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($addPhV[1]) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>" placeholder="Es. Menù, Carta dei Vini, Birre Artigianali, ecc.">
                                </div>

                                <?php
                                $is_active_check = "checked";
                                if($r['is_active'] == 0 && $_GET['id'] != "") {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="easyRowDuplicationContainer">
                                <?php
                                $fields = json_decode($r['fields'], true);

                                if(count($fields) == 0) {
                                    $fields[] = array('products' => array(''));
                                }

                                foreach($fields as $k => $v) {
                                ?>
                                <div class="groupDuplication" style="margin-bottom: 20px; background-color: #e0f8f8; padding: 20px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Nome Sezione*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($addPhV[1]) ?>"></i></span>
                                            <input type="text" class="form-control section_name" placeholder="Es. Birre Bionde, Birre Rosse, Doppio Malto, ecc." value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($v['section']['name'])) ?>">
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="easyRowDuplicationContainer m-t-md">
                                        <?php
                                        foreach($v['products'] as $kP => $vP) {
                                            ?>
                                            <div class="stepsDuplication rowDuplication fieldsDuplication m-b-lg">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>Nome prodotto*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($addPhV[1]) ?>"></i></span>
                                                        <input type="text" class="form-control product_name" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($vP['name'])) ?>">
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($addPhV[1]) ?>"></i></span>
                                                        <textarea class="form-control product_descr" rows="4"><?php echo $MSFrameworki18n->getFieldValue($vP['descr']) ?></textarea>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <label>Prezzo <?= CURRENCY_SYMBOL; ?></label>
                                                        <input type="text" class="form-control product_price" value="<?php echo htmlentities($vP['price']) ?>">
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <label>&nbsp;</label>
                                                        <?php
                                                        (new \MSFramework\uploads('RESTAURANT_MENU'))->initUploaderHTML("image_" . $kP, json_encode($vP['images']), array("realPath" => "image_realPath", "prevUploaded" => "prev_upl_image", "element" => "image", "container" => "image_container"));
                                                        ?>
                                                    </div>
                                                </div>

                                            </div>
                                        <?php } ?>
                                    </div>


                                </div>
                                <?php } ?>
                            </div>


                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSReport = new \MSFramework\Report\report();
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">

            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-md-4 tabs_col">
                    <div class="tabs-container">

                        <div class="tabs-left">
                            <ul class="nav nav-tabs" style="width: 40% !important;">
                                <?php
                                $first = true;
                                foreach($MSReport->getAvailCategories() as $catK => $catV) {
                                ?>
                                    <li class="<?= ($first ? 'active' : '') ?>"><a data-toggle="tab" href="#tab-<?= $catK ?>"> <i class="fa <?= $catV['fa-icon'] ?>"></i><?= $catV['name'] ?></a></li>
                                <?php
                                    $first = false;
                                }
                                ?>
                            </ul>

                            <div class="tab-content">
                                <?php
                                $first = true;
                                foreach($MSReport->getAvailCategories() as $catK => $catV) {
                                ?>
                                <div id="tab-<?= $catK ?>" class="tab-pane <?= ($first ? 'active' : '') ?>">
                                    <div class="panel-body" style="width: 60% !important; margin-left: 40%;">
                                        <?php
                                        foreach($MSReport->getChildrenForCat($catK) as $childK => $child) {
                                        ?>
                                            <div class="report_item" data-parent="<?= $catK ?>" data-item="<?= $childK ?>">
                                                <?= $child['name'] ?>
                                                <span style="float: right"> > </span>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                    $first = false;
                                }
                                ?>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-8 content_col">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content p-md">
                            <div id="topbtns_container" style="display: none;">
                                <div class="ms-label-tooltip"><span class="simple_tag btn_expand"> <i class="fa fa-arrows-alt"></i> </span></div>
                                <div class="ms-label-tooltip"><span class="simple_tag btn_export" data-export-type="pdf"> <i class="fa fa-file-pdf-o"></i> PDF </span></div>
                                <div class="ms-label-tooltip"><span class="simple_tag btn_export" data-export-type="xls"> <i class="fa fa-file-excel-o"></i> XLS </span></div>

                                <div style="clear: both;" class="m-b-md"></div>
                            </div>

                            <div id="report_container">
                                <div class="text-center">Selezionare un report da generare dal menu a sinistra</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
function initForm() {
    $('.btn_expand').on('click', function() {
        if($('.content_col').hasClass('col-md-8')) {
            $('.tabs_col').hide();
            $('.content_col').removeClass('col-md-8').addClass('col-md-12');
        } else {
            $('.tabs_col').show();
            $('.content_col').removeClass('col-md-12').addClass('col-md-8');
        }
    })

    $('.nav-tabs li a').on('click', function() {
        $('#report_container').html('<div class="text-center">Selezionare un report da generare dal menu a sinistra</div>');
        $('#topbtns_container').hide();
        $('.report_item').removeClass('font-bold');
    })

    $('.report_item').on('click', function() {
        $('#report_container').html('<div class="text-center">Carico i dati per il report selezionato...</div>');
        $('#topbtns_container').hide();
        $('.report_item').removeClass('font-bold');
        $(this).addClass('font-bold');

        $.ajax({
            url: "ajax/report/common.php?p=" + $(this).data('parent') + "&i=" + $(this).data('item'),
            async: true,
            dataType: "json",
            preloader: 'Caricamento report in corso',
            success: function(data) {
                if(data.status != "err") {
                    $('#topbtns_container').show();
                    $('#report_container').html(data.html);

                    initCommonInterface();
                } else {
                    $('#report_container').html('<div class="text-center">Errore durante il caricamento dei dati</div>');
                }
            },
            error: function() {
                $('#report_container').html('<div class="text-center">Errore durante il caricamento dei dati</div>');
            }
        })
    })
}

function initCommonInterface() {
    $('.collapse-link').on('click', function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.children('.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    $(document).keydown(function(event){
        if(event.which == "16") {
            window.maiuscIsPressed = true;
            $('.new_tab_str').remove();
            $('.btn_export[data-export-type=pdf]').append('<span class="new_tab_str"> (apri nuova scheda)</span>');
        }
    });

    $(document).keyup(function(event){
        if(event.which == "16") {
            window.maiuscIsPressed = false;
            $('.new_tab_str').remove();
        }
    });

    $('.btn_export').unbind('click');
    $('.btn_export').on('click', function() {
        export_type = $(this).data('export-type');
        src = "ajax/report/" + $('#hidden_parent').val() + "/" + $('#hidden_item').val() + "/output/" + export_type + "/index.php?p=" + $('#hidden_parent').val() + "&i=" + $('#hidden_item').val();

        if(window.maiuscIsPressed) {
            $('#fakeForm').remove();
            $('#report_container').append('<form target="_blank" action="' + src + '" id="fakeForm" method="POST" style="display: none;"><textarea name="filters">' + JSON.stringify(window.filters) + '</textarea><input type="hidden" name="decode_filters" value="1" /><input type="hidden" name="output_type" value="I" /></form>')
            $('#fakeForm').submit();
        } else {
            window.dialog = bootbox.dialog({
                message: '<p class="text-center no-margins">Generazione del file richiesto in corso. Attendere...</p>',
                closeButton: false
            });

            $.fileDownload(src, {
                successCallback: function (url) {
                    window.dialog.modal('hide');
                },
                failCallback: function (responseHtml, url) {
                    window.dialog.modal('hide');
                    bootbox.error('Non è stato possibile generare il file richiesto');
                },
                httpMethod: "POST",
                data: window.filters
            });
            return false;
        }
    })

    initIChecks();
    initFilters();

    if($('#order_by').length != 0) {
        $('#order_by').on('change', function() {
            if($(this).val() == "") {
                $('#sort_direction').hide();
            } else {
                $('#sort_direction').show();
            }
        });

        $('#order_by').trigger('change');
    } else {
        updateFilters();
    }

    if(typeof(previewUpdateCallback) == "function") {
        previewUpdateCallback(); //va definita nei singoli file "preview.js" dei report (se serve) e viene chiamata ogni volta che l'interfaccia con l'anteprima dei dati viene aggiornata
    }
}

function initFilters() {
    window.filters = {};
    window.filtersOriginalValues = {};

    changeDataLockStatus('1');

    if($('#filters_container').length != 0) {
        //Salvo i dati di partenza dei filtri
        $('.filters_monitor_changes').find('input[type=text], select, textarea, input[type=checkbox]').each(function() {
            if($(this).prop("tagName").toLowerCase() == "input" && $(this).attr('type') == "checkbox") {
                original_value = ($(this).is(':checked') ? "1" : "0");
            } else {
                original_value = $(this).val();
            }

            window.filtersOriginalValues[$(this).attr('id')] = {
                'value' : original_value,
            }
        });

        //Intercetto le modifiche ai filtri
        $('.filters_monitor_changes').find('input[type=text], select, textarea').on('change', function() {
            window.filters[$(this).attr('id')] = {
                'value' : $(this).val(),
            }

            commonAfterChangeFilter(this);
        })

        $('.filters_monitor_changes').find('input[type=checkbox]').on('ifToggled', function(event) {
            window.filters[$(this).attr('id')] = {
                'value' : ($(this).is(':checked') ? "1" : "0"),
            }

            commonAfterChangeFilter(this);
        });
    }
}

function commonAfterChangeFilter(changed_el) {
    window.filters[$(changed_el).attr('id')]['db-operator'] = ($(changed_el).data('db-operator') == "" || typeof($(changed_el).data('db-operator')) == "undefined" ? "=" : $(changed_el).data('db-operator'));
    window.filters[$(changed_el).attr('id')]['db-exclude-common'] = ($(changed_el).data('db-exclude-common') == "" || typeof($(changed_el).data('db-exclude-common')) == "undefined" ? "0" : $(changed_el).data('db-exclude-common'));

    lockStatus = "1";
    $.each(window.filters, function( filterK, filterV ) {
        if(typeof(window.filtersOriginalValues[filterK]) != "undefined" && filterK != "order_by" && filterK != "order_by_dir") {
            if (window.filtersOriginalValues[filterK]['value'] != filterV['value']) {
                lockStatus = "2";
                return true;
            }
        }
    });

    changeDataLockStatus(lockStatus);

    if(!$(changed_el).hasClass('do_not_update_preview')) {
        updateFilters();
    }
}

function updateFilters() {
    $('#lock_data_load').unbind('click');

    if(window.filters.lock_data_load.value == "1") { //non sono stati selezionati filtri, richiedo azione esplicita per caricare i dati
        $('#filter_preview').html('<div class="text-center m-b-sm">Nessun filtro selezionato.</div><div class="text-center"><button type="button" id="lock_data_load" class="btn btn-primary">Visualizza tutti i dati, senza filtri</button></div>');
    } else if(window.filters.lock_data_load.value == "2") { //c'è stata una modifica ai filtri, richiedo azione esplicita per caricare i dati
        $('#filter_preview').html('<div class="text-center m-b-sm">Hai modificato i filtri.</div><div class="text-center"><button type="button" id="lock_data_load" class="btn btn-primary">Aggiorna i dati utilizzando i nuovi filtri</button></div>');
    } else {
        $('#filter_preview').html('<div class="text-center">Aggiorno i dati in base ai filtri selezionati...</div>');

        $.ajax({
            url: "ajax/report/" + $('#hidden_parent').val() + "/" + $('#hidden_item').val() + "/output/html/index.php?standalone",
            type: "POST",
            data: {
                "filters" : window.filters
            },
            preloader: 'Aggiornamento',
            success: function(data) {
                $('#filter_preview').html(data);

                if(typeof(previewUpdateCallback) == "function") {
                    previewUpdateCallback(); //va definita nei singoli file "preview.js" dei report (se serve) e viene chiamata ogni volta che l'interfaccia con l'anteprima dei dati viene aggiornata
                }
            }
        })
    }

    $('#lock_data_load').on('click', function() {
        changeDataLockStatus('0');
    })
}

function changeDataLockStatus(status) {
    window.filters['lock_data_load'] = {
        'value' : status,
        'db-exclude-common' : "1",
    }

    updateFilters();
}
<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

//In alcuni casi (es: in fase di sviluppo) potrebbe tornare utile aggiornare la pagina (es: il PDF) e vedere i dati aggiornati. Mantengo i filtri che mi arrivano dal POST in una sessione in modo da riutilizzarli in futuro
if(isset($_POST) & count($_POST)) { $_SESSION['report_post_data'] = $_POST; }
if(isset($_SESSION['report_post_data']) && count($_SESSION['report_post_data'])) { $_POST = $_SESSION['report_post_data']; }

if($_POST['decode_filters'] == "1") {
    $_POST['filters'] = json_decode($_POST['filters'], true);
}

$filters = $_POST['filters'];
$where_str = "";
$where_ary = array();

/*
Filtri il cui id dell'elemento HTML corrisponde al nome della colonna del DB
Vengono esclusi da questo controllo i filtri che rientrano nei seguenti parametri
    1) Il filtro è già presente nell'array "$where_ary"
    2) Il filtro ha l'attributo db-exclude-common impostato a "1"
    3) I valore del filtro è vuoto
*/
foreach($filters as $filterK => $filterV) {
    $named_par = $MSFrameworkDatabase->formatNamedParameter($filterK);

    if($filterV['value'] == "" || array_key_exists(':' . $named_par, $where_ary) || $filterV['db-exclude-common'] == "1") {
        continue;
    }

    if($filterV['db-operator'] == "%LIKE%") {
        $where_str .= " AND $filterK LIKE :" . $named_par . " ";
        $where_ary[':' . $named_par] = '%' . $filterV['value'] . "%";
    } else if($filterV['db-operator'] == "%LIKE") {
        $where_str .= " AND $filterK LIKE :" . $named_par . " ";
        $where_ary[':' . $named_par] = '%' . $filterV['value'];
    } else if($filterV['db-operator'] == "LIKE%") {
        $where_str .= " AND $filterK LIKE :" . $named_par . " ";
        $where_ary[':' . $named_par] = $filterV['value'] . "%";
    } else {
        $where_str .= " AND $filterK " . $filterV['db-operator'] . " :" . $named_par . " ";
        $where_ary[':' . $named_par] = $filterV['value'];
    }
}

$order_by_str = ($filters['order_by']['value'] != "" ? "ORDER BY " . $filters['order_by']['value'] . " " . ($filters['order_by_dir']['value'] != '' ? $filters['order_by_dir']['value'] : '') : "");
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

if(isset($_GET['standalone'])) {
    require_once('../../../../../../../../sw-config.php');
    require_once("../../db/getData.php");
}
?>

<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Stato</th>
                    <th>Data e Ora</th>
                    <th>Cliente</th>
                    <th>Metodo di Pagamento</th>
                    <th>Totale Ordine</th>
                </tr>
            </thead>

            <tbody>
            <?php
            $totale_globale = 0.0;

            $orde_status_color = array(
                "0" => "rgba(244,67,54,1)",
                "1" => "rgba(255,152,0,1)",
                "2" => "rgba(33,150,243,1)",
                "3" => "rgba(139,195,74,1)",
                "4" => "rgba(78,70,69,1)",
            );

            foreach($reportData['queryResult'] as $r) {
                $carrello = json_decode($r['cart'], true);

                $totale = (float)$carrello['coupon_price'];
                $totale_globale += $totale;

                $cliente_data = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($r['user_id']);
                if($cliente_data) {
                    $cliente = $cliente_data['nome'] . ' ' . $cliente_data['cognome'];
                }
                else
                {
                    $cliente = 'Ospite (' . $r['guest_email'] . ')';
                }

                $metodo_pagamento = ucfirst($r['payment_type']);
                if(empty($metodo_pagamento)) $metodo_pagamento = 'N/A';

            ?>
            <tr>
                <td>
                    <?= '<span class="label" style="color: white; background: ' . $orde_status_color[$r['order_status']] . ';">' . (new \MSFramework\Ecommerce\orders)->getStatus($r['order_status']) . '</span>'; ?>
                </td>
                <td>
                    <?= date("d/m/Y H:i", strtotime($r['order_date'])) ?>
                </td>
                <td>
                    <?= $cliente ?>
                </td>
                <td>
                    <?= $metodo_pagamento ?>
                </td>
                <td>
                    <?= number_format($totale, 2, ",", ".") . CURRENCY_SYMBOL ?>
                </td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="5" style="text-align: right;">Totale: <?= number_format($totale_globale, 2, ",", ".") . CURRENCY_SYMBOL ?></th>
            </tr>
            <tr>
                <th colspan="5" style="text-align: right;">Totale risultati: <?= count($reportData['queryResult']) ?></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Risultati per Stato</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-4">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Tipo Documento</th>
                                <th>Totale</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            foreach($reportData['groupData']['stato_ordine'] as $typeK => $typeV) {
                                ?>
                                <tr>
                                    <td>
                                        <?= (new \MSFramework\Ecommerce\orders())->getStatus((string)$typeK) ?>
                                    </td>
                                    <td>
                                        <?= $typeV ?>
                                        <input type="hidden" class="type_value" id="type_<?= $typeK ?>" value="<?= $typeV ?>" />
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-8">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="text-center">
                                    <div id="torta_type"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

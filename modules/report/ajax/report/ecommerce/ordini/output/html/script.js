$(document).ready(function() {
    $('#data_to, #data_from').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
        endDate: new Date(),
    });
})

function previewUpdateCallback() {
    data_ary = new Array();
    $('.type_value').each(function() {
        data_ary.push($(this).val());
    });

    $("#torta_type").sparkline(data_ary, {
        type: 'pie',
        height: '140',
    });
}

<?php
/**
 * MSAdminControlPanel
 * Date: 21/12/2018
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

$pdf = new \MSFramework\Report\Output\PDF\pdf($_GET['p'], $_GET['i']);
$table = new \Interpid\PdfLib\Table($pdf);

$pdf->openReport();

$table->initialize( [ $pdf->widthFromPercentage("15"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("15"), $pdf->widthFromPercentage("30"), $pdf->widthFromPercentage("30") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Stato' ],
    [ 'TEXT' => 'Data e Ora' ],
    [ 'TEXT' => 'Cliente' ],
    [ 'TEXT' => 'Metodo di Pagamento' ],
    [ 'TEXT' => 'Totale Ordine' ]
));

$totale_globale = 0;

foreach($reportData['queryResult'] as $r) {
    $carrello = json_decode($r['cart'], true);

    $totale = (float)$carrello['coupon_price'];
    $totale_globale += $totale;

    $cliente_data = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($r['user_id']);
    if($cliente_data) {
        $cliente = $cliente_data['nome'] . ' ' . $cliente_data['cognome'] . ' - ID#' . $cliente_data['id'];
    }
    else
    {
        $cliente = 'Ospite (' . $r['guest_email'] . ')';
    }

    $metodo_pagamento = ucfirst($r['payment_type']);
    if(empty($metodo_pagamento)) $metodo_pagamento = 'N/A';

    $table->addRow(array(
        [ 'TEXT' => (new \MSFramework\Ecommerce\orders())->getStatus($r['order_status']) ],
        [ 'TEXT' => date("d/m/Y H:i", strtotime($r['order_date'])) ],
        [ 'TEXT' => $cliente ],
        [ 'TEXT' => $metodo_pagamento ],
        [ 'TEXT' => CURRENCY_SYMBOL . number_format($totale, 2, ",", ".") ],
    ));
}

$table->close();
$pdf->SetDefaultStyle();

$pdf->ln(8);

$table->initialize( [ $pdf->widthFromPercentage("30"), $pdf->widthFromPercentage("20") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Stato Ordine' ],
    [ 'TEXT' => 'Totale' ],
));

foreach($reportData['groupData']['stato_ordine'] as $typeK => $typeV) {
    $dati_torta['data'][] = $typeV;

    $table->addRow(array(
        [ 'TEXT' => (new \MSFramework\Ecommerce\orders())->getStatus((string)$typeK) ],
        [ 'TEXT' => $typeV ],
    ));
}

$table->close();
$pdf->SetDefaultStyle();


$pdf->addPieChart($dati_torta, $pdf->widthFromPercentage("85"));
unset($dati_torta);

$pdf->closeReport($_POST['output_type']);
?>

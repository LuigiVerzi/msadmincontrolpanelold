<?php
/**
 * MSAdminControlPanel
 * Date: 22/12/2018
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use \PhpOffice\PhpSpreadsheet\Chart\Chart;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use \PhpOffice\PhpSpreadsheet\Chart\Layout;
use \PhpOffice\PhpSpreadsheet\Chart\Legend;
use \PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use \PhpOffice\PhpSpreadsheet\Chart\Title;

$spreadsheet = new \MSFramework\Report\Output\XLS\xls($_GET['p'], $_GET['i']);

$spreadsheet->Header(array(
    array("Stato", 23),
    array("Data e Ora", 23),
    array("Cliente", 40),
    array("Metodo di Pagamento", 40),
    array("Totale Ordine", 25)
));

$dataAry = array();
foreach($reportData['queryResult'] as $r) {
    $carrello = json_decode($r['cart'], true);

    $totale = (float)$carrello['coupon_price'];
    $totale_globale += $totale;

    $cliente_data = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($r['user_id']);
    if($cliente_data) {
        $cliente = $cliente_data['nome'] . ' ' . $cliente_data['cognome'] . ' - ID#' . $cliente_data['id'];
    }
    else
    {
        $cliente = 'Ospite (' . $r['guest_email'] . ')';
    }

    $metodo_pagamento = ucfirst($r['payment_type']);
    if(empty($metodo_pagamento)) $metodo_pagamento = 'N/A';

    $dataAry[] = array(
        (new \MSFramework\Ecommerce\orders())->getStatus($r['order_status']),
        date("d/m/Y H:i", strtotime($r['order_date'])),
        $cliente,
        $metodo_pagamento,
        CURRENCY_SYMBOL . number_format($totale, 2, ",", ".")
    );
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row);



$spreadsheet->addNewSheet('Grafico Stato Ordine');
$spreadsheet->Header(array(
    array("Stato", 25),
    array("Totale", 10),
));

$dataAry = array();
$type_sum = 0;
foreach($reportData['groupData']['stato_ordine'] as $typeK => $typeV) {
    $dataAry[] = array(
        (new \MSFramework\Ecommerce\orders())->getStatus((string)$typeK),
        $typeV,
    );

    $type_sum += $typeV;
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->getStyle('B6')
    ->getNumberFormat()
    ->setFormatCode(
        '0'
    );

$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row, true);

$spreadsheet->addPieChart('Grafico Stato Ordine',
    array(
        'sheet_name' => 'Grafico Stato Ordine',
        'label' => array('$A$5', '$A$'. ($row+count($dataAry)-1)),
        'data' => array('$B$5', '$B$'. ($row+count($dataAry)-1))
    ),
    array("E5", "L20")
);

$spreadsheet->closeReport();
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

require(dirname(__FILE__, 4) . "/commonGetData.php");

if($filters['data_from']['value'] != "") {
    $named_par = $MSFrameworkDatabase->formatNamedParameter('data_from');

    $where_str .= " AND order_date >= :" . $named_par . " ";
    $where_ary[':' . $named_par] = $filters['data_from']['value'];

    unset($filters['data_from']);
}

if($filters['data_to']['value'] != "") {
    $named_par = $MSFrameworkDatabase->formatNamedParameter('data_to');

    $where_str .= " AND order_date <= :" . $named_par . " ";
    $where_ary[':' . $named_par] = $filters['data_to']['value'];

    unset($filters['data_to']);
}

$reportDataResult = $MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_orders WHERE id != '' $where_str $order_by_str", $where_ary);

foreach($reportDataResult as $r) {
    $stato_ordine[$r['order_status']]++;
}

$reportData = array(
    'queryResult' => $reportDataResult,
    'groupData' => array(
        "stato_ordine" => $stato_ordine,
    )
);

if(count($reportDataResult) == 0) {
    die('<div class="text-center">Nessun dato disponibile per i filtri impostati</div>');
}
?>
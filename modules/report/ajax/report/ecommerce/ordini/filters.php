<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

$sort_options = array(
    "order_date" => "Data",
    "order_status" => "Stato Ordine",
);

$metodi_di_pagamento = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT payment_type FROM `ecommerce_orders` GROUP by payment_type") as $payment) {
    if(!empty($payment['payment_type'])) {
        $metodi_di_pagamento[$payment['payment_type']] = ucfirst($payment['payment_type']);
    }
}
?>

<div class="row">

    <div class="col-sm-3">
        <label>Da</label>
        <div class="input-group date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="data_from" data-db-exclude-common="1">
        </div>
    </div>

    <div class="col-sm-3">
        <label>Fino a</label>
        <div class="input-group date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="data_to" data-db-exclude-common="1">
        </div>
    </div>

    <div class="col-sm-3">
        <label>Metodo di Pagamento</label>
        <select id="payment_type" class="form-control">
            <option value="">Tutti</option>
            <?php foreach($metodi_di_pagamento as $docK => $docName) { ?>
                <option value="<?= $docK ?>"><?= $docName ?></option>
            <?php } ?>
        </select>
    </div>


    <div class="col-sm-3">
        <label>Stato Ordine</label>
        <select id="order_status" class="form-control">
            <option value="">Tutti</option>
            <?php foreach((new \MSFramework\Ecommerce\orders())->getStatus() as $docK => $docName) { ?>
                <option value="<?= $docK ?>"><?= $docName ?></option>
            <?php } ?>
        </select>
    </div>
</div>
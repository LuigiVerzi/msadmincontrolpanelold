<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

require(dirname(__FILE__, 4) . "/commonGetData.php");

if($filters['data_from']['value'] != "") {
    $named_par = $MSFrameworkDatabase->formatNamedParameter('data_from');

    $where_str .= " AND DATE(data) >= :" . $named_par . " ";
    $where_ary[':' . $named_par] = date("Y-m-d", strtotime(str_replace("/", "-", $filters['data_from']['value'])));

    unset($filters['data_from']);
}

if($filters['data_to']['value'] != "") {
    $named_par = $MSFrameworkDatabase->formatNamedParameter('data_to');

    $where_str .= " AND DATE(data) <= :" . $named_par . " ";
    $where_ary[':' . $named_par] = date("Y-m-d", strtotime(str_replace("/", "-", $filters['data_to']['value'])));

    unset($filters['data_to']);
}

if($filters['metodo_pagamento']['value'] != "") {
    $named_par = $MSFrameworkDatabase->formatNamedParameter('metodo_pagamento');

    if($filters['metodo_pagamento']['value'] == "-1") {
        $where_str .= " AND info_documento NOT LIKE :" . $named_par . " ";
        $where_ary[':' . $named_par] = '%"pagamento"%';
    } else {
        $where_str .= " AND info_documento LIKE :" . $named_par . " ";
        $where_ary[':' . $named_par] = '%"pagamento":"' . $filters['metodo_pagamento']['value'] . '"%';
    }

    unset($filters['metodo_pagamento']);
}

$reportDataResult = $MSFrameworkDatabase->getAssoc("SELECT cliente, dati_cliente, operatore, `data`, tipo_documento, carrello FROM fatturazione_vendite WHERE id != '' $where_str $order_by_str", $where_ary);

foreach($reportDataResult as $r) {
    $carrello = json_decode($r['carrello'], true);
    $decoded_docinfo = json_decode($r['info_documento'], true);
    $tipo_documento[$r['tipo_documento']]++;
    if(!array_key_exists('pagamento', $decoded_docinfo)) {
        $payMethod = '-1';
    } else {
        $payMethod = $decoded_docinfo['pagamento'];
    }

    foreach($carrello as $c) {
        $totale = (float)$c['subtotal'];
        $imponibile = (float)$c['prezzo'];
        $imposta = (float)$c['prezzo_iva_inclusa']-(float)$c['prezzo'];

        $groupByDocType[$r['tipo_documento']] = array("imponibile" => $groupByDocType[$r['tipo_documento']]['imponibile']+$imponibile, "imposta" => $groupByDocType[$r['tipo_documento']]['imposta']+$imposta, "totale" => $groupByDocType[$r['tipo_documento']]['totale']+$totale, "totale_operazioni" => $groupByDocType[$r['tipo_documento']]['totale_operazioni']+1);
        $groupByOperator[$r['operatore']] = array("imponibile" => $groupByOperator[$r['operatore']]['imponibile']+$imponibile, "imposta" => $groupByOperator[$r['operatore']]['imposta']+$imposta, "totale" => $groupByOperator[$r['operatore']]['totale']+$totale, "totale_operazioni" => $groupByOperator[$r['operatore']]['totale_operazioni']+1);
        $groupByPayMethod[$payMethod] = array("imponibile" => $groupByPayMethod[$payMethod]['imponibile']+$imponibile, "imposta" => $groupByPayMethod[$payMethod]['imposta']+$imposta, "totale" => $groupByPayMethod[$payMethod]['totale']+$totale, "totale_operazioni" => $groupByPayMethod[$payMethod]['totale_operazioni']+1);
    }
}

$reportData = array(
    'queryResult' => $reportDataResult,
    'groupData' => array(
        "tipo_documento" => $tipo_documento,
        "groupedTotals" => array(
            "docType" => $groupByDocType,
            "operator" => $groupByOperator,
            "payMethod" => $groupByPayMethod,
        ),
    )
);

if(count($reportDataResult) == 0) {
    die('<div class="text-center">Nessun dato disponibile per i filtri impostati</div>');
}
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 21/12/2018
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

$pdf = new \MSFramework\Report\Output\PDF\pdf($_GET['p'], $_GET['i']);
$table = new \Interpid\PdfLib\Table($pdf);

$pdf->openReport();

$table->initialize( [ $pdf->widthFromPercentage("20"), $pdf->widthFromPercentage("16"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("17"), $pdf->widthFromPercentage("17") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Tipo Documento' ],
    [ 'TEXT' => 'Data e Ora' ],
    [ 'TEXT' => 'Imponibile' ],
    [ 'TEXT' => 'Imposta' ],
    [ 'TEXT' => 'Totale Documento' ],
    [ 'TEXT' => 'Cliente' ],
    [ 'TEXT' => 'Operatore' ],
));

$totale_globale = 0;

foreach($reportData['queryResult'] as $r) {
    $carrello = json_decode($r['carrello'], true);

    $totale = 0;
    $imponibile = 0;
    $imposta = 0;
    foreach($carrello as $c) {
        $totale += (float)$c['subtotal'];
        $imponibile += (float)$c['prezzo'];
        $imposta += (float)$c['prezzo_iva_inclusa']-(float)$c['prezzo'];

        $totale_globale += (float)$c['subtotal'];
        $imponibile_globale += (float)$c['prezzo'];
        $imposta_globale += (float)$c['prezzo_iva_inclusa']-(float)$c['prezzo'];
    }

    $cliente_data = (new \MSFramework\customers())->getCustomerDataFromDB($r['cliente']);
    $cliente = $cliente_data['nome'] . ' ' . $cliente_data['cognome'];

    $operatore_data = (new \MSFramework\users())->getUserDataFromDB($r['operatore']);
    $operatore = $operatore_data['nome'] . ' ' . $operatore_data['cognome'];

    $table->addRow(array(
        [ 'TEXT' => (new \MSFramework\Fatturazione\vendite())->ottieniTipiDocumento($r['tipo_documento'], false) ],
        [ 'TEXT' => date("d/m/Y H:i", strtotime($r['data'])) ],
        [ 'TEXT' => number_format($imponibile, 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => number_format($imposta, 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => number_format($totale, 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => $cliente ],
        [ 'TEXT' => $operatore ],
    ));
}

$table->close();
$pdf->SetDefaultStyle();

$pdf->AddPage();

$pdf->SetFont($pdf->defaultFont, 'B', 9);
$pdf->MultiCell('', '', 'Vendite per operatore', 0, 'L');
$pdf->SetDefaultStyle();

$table->initialize( [ $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Operatore' ],
    [ 'TEXT' => 'Imponibile' ],
    [ 'TEXT' => 'Imposta' ],
    [ 'TEXT' => 'Totale Vendite' ],
    [ 'TEXT' => 'Totale Operazioni' ],
));

foreach($reportData['groupData']['groupedTotals']['operator'] as $k => $r) {
    $user = (new \MSFramework\users())->getUserDataFromDB($k, "nome, cognome");
    $dati_torta['data'][] = number_format($r['totale'], 2, ".", "");

    $table->addRow(array(
        [ 'TEXT' => $user['cognome'] . " " . $user['nome'] ],
        [ 'TEXT' => number_format($r['imponibile'], 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => number_format($r['imposta'], 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => number_format($r['totale'], 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => $r['totale_operazioni'] ],
    ));
}

$table->close();
$pdf->SetDefaultStyle();

$pdf->addPieChart($dati_torta, $pdf->widthFromPercentage("85"));
unset($dati_torta);
$pdf->ln(25);


$pdf->SetFont($pdf->defaultFont, 'B', 9);
$pdf->MultiCell('', '', 'Vendite per metodo di pagamento', 0, 'L');
$pdf->SetDefaultStyle();

$table->initialize( [ $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Metodo' ],
    [ 'TEXT' => 'Imponibile' ],
    [ 'TEXT' => 'Imposta' ],
    [ 'TEXT' => 'Totale Vendite' ],
    [ 'TEXT' => 'Totale Operazioni' ],
));

foreach($reportData['groupData']['groupedTotals']['payMethod'] as $k => $r) {
    $dati_torta['data'][] = number_format($r['totale'], 2, ".", "");

    $table->addRow(array(
        [ 'TEXT' => ($k == "-1" ? "Altro/Non definito" : (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamento($k)['nome']) ],
        [ 'TEXT' => number_format($r['imponibile'], 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => number_format($r['imposta'], 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => number_format($r['totale'], 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => $r['totale_operazioni'] ],
    ));
}

$table->close();
$pdf->SetDefaultStyle();

$pdf->addPieChart($dati_torta, $pdf->widthFromPercentage("85"));
unset($dati_torta);
$pdf->ln(25);


$pdf->SetFont($pdf->defaultFont, 'B', 9);
$pdf->MultiCell('', '', 'Vendite per tipologia documento', 0, 'L');
$pdf->SetDefaultStyle();

$table->initialize( [ $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Tipologia' ],
    [ 'TEXT' => 'Imponibile' ],
    [ 'TEXT' => 'Imposta' ],
    [ 'TEXT' => 'Totale Vendite' ],
    [ 'TEXT' => 'Totale Operazioni' ],
));

foreach($reportData['groupData']['groupedTotals']['docType'] as $k => $r) {
    $dati_torta['data'][] = number_format($r['totale'], 2, ".", "");

    $table->addRow(array(
        [ 'TEXT' => (new \MSFramework\Fatturazione\vendite())->ottieniTipiDocumento($k, false) ],
        [ 'TEXT' => number_format($r['imponibile'], 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => number_format($r['imposta'], 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => number_format($r['totale'], 2, ",", ".") . CURRENCY_SYMBOL ],
        [ 'TEXT' => $r['totale_operazioni'] ],
    ));
}

$table->close();
$pdf->SetDefaultStyle();

$pdf->addPieChart($dati_torta, $pdf->widthFromPercentage("85"));
unset($dati_torta);
$pdf->ln(25);


$pdf->SetFont($pdf->defaultFont, 'B', 9);
$pdf->MultiCell('', '', 'Riepilogo imponibile/imposte', 0, 'L');
$pdf->SetDefaultStyle();

$table->initialize( [ $pdf->widthFromPercentage("25"), $pdf->widthFromPercentage("25") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Tipologia' ],
    [ 'TEXT' => 'Totale' ]
));

$dati_torta['data'] = array(number_format($imponibile_globale, 2, ".", ""), number_format($imposta_globale, 2, ".", ""));

$table->addRow(array(
    [ 'TEXT' => "Imponibile" ],
    [ 'TEXT' => number_format($imponibile_globale, 2, ",", ".") . CURRENCY_SYMBOL ]
));
$table->addRow(array(
    [ 'TEXT' => "Imposte" ],
    [ 'TEXT' => number_format($imposta_globale, 2, ",", ".") . CURRENCY_SYMBOL ]
));

$table->close();
$pdf->SetDefaultStyle();

$pdf->addPieChart($dati_torta, $pdf->widthFromPercentage("85"));
unset($dati_torta);

$pdf->closeReport($_POST['output_type']);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 22/12/2018
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use \PhpOffice\PhpSpreadsheet\Chart\Chart;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use \PhpOffice\PhpSpreadsheet\Chart\Layout;
use \PhpOffice\PhpSpreadsheet\Chart\Legend;
use \PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use \PhpOffice\PhpSpreadsheet\Chart\Title;

use \PhpOffice\PhpSpreadsheet\Style\NumberFormat;

$spreadsheet = new \MSFramework\Report\Output\XLS\xls($_GET['p'], $_GET['i']);

$spreadsheet->Header(array(
    array("Tipo Documento", 23),
    array("Data e Ora", 23),
    array("Imponibile", 25),
    array("Imposta", 25),
    array("Totale Documento", 25),
    array("Cliente", 40),
    array("Operatore", 40),
));

$dataAry = array();
$totale_globale = 0;
$imponibile_globale = 0;
$imposta_globale = 0;

foreach($reportData['queryResult'] as $r) {
    $carrello = json_decode($r['carrello'], true);

    $totale = 0;
    $imponibile = 0;
    $imposta = 0;
    foreach($carrello as $c) {
        $totale += (float)$c['subtotal'];
        $imponibile += (float)$c['prezzo'];
        $imposta += (float)$c['prezzo_iva_inclusa']-(float)$c['prezzo'];

        $totale_globale += (float)$c['subtotal'];
        $imponibile_globale += (float)$c['prezzo'];
        $imposta_globale += (float)$c['prezzo_iva_inclusa']-(float)$c['prezzo'];
    }

    $cliente_data = (new \MSFramework\customers())->getCustomerDataFromDB($r['cliente']);
    $cliente = $cliente_data['nome'] . ' ' . $cliente_data['cognome'];

    $operatore_data = (new \MSFramework\users())->getUserDataFromDB($r['operatore']);
    $operatore = $operatore_data['nome'] . ' ' . $operatore_data['cognome'];

    $dataAry[] = array(
        (new \MSFramework\Fatturazione\vendite())->ottieniTipiDocumento($r['tipo_documento'], false),
        date("d/m/Y H:i", strtotime($r['data'])),
        number_format($imponibile, 2, ".", ","),
        number_format($imposta, 2, ".", ","),
        number_format($totale, 2, ".", ","),
        $cliente,
        $operatore
    );
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row);
$spreadsheet->getActiveSheet()->getStyle('C5:C' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
$spreadsheet->getActiveSheet()->getStyle('D5:D' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
$spreadsheet->getActiveSheet()->getStyle('E5:E' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);




$spreadsheet->addNewSheet('Vendite per operatore');
$spreadsheet->Header(array(
    array("Operatore", 27),
    array("Imponibile", 17),
    array("Imposta", 17),
    array("Totale Vendite", 17),
    array("Totale Operazioni", 17),
));

$dataAry = array();
foreach($reportData['groupData']['groupedTotals']['operator'] as $k => $r) {
    $user = (new \MSFramework\users())->getUserDataFromDB($k, "nome, cognome");

    $dataAry[] = array(
        $user['cognome'] . " " . $user['nome'],
        number_format($r['imponibile'], 2, ".", ","),
        number_format($r['imposta'], 2, ".", ","),
        number_format($r['totale'],  2, ".", ","),
        $r['totale_operazioni'],
    );
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row, true);
$spreadsheet->getActiveSheet()->getStyle('B5:B' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
$spreadsheet->getActiveSheet()->getStyle('C5:C' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
$spreadsheet->getActiveSheet()->getStyle('D5:D' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);

$spreadsheet->addPieChart('Vendite per operatore',
    array(
        'sheet_name' => 'Vendite per operatore',
        'label' => array('$A$5', '$A$'. ($row+count($dataAry)-1)),
        'data' => array('$D$5', '$D$'. ($row+count($dataAry)-1))
    ),
    array("H5", "M20")
);




$spreadsheet->addNewSheet('Vendite per metodo di pagamento');
$spreadsheet->Header(array(
    array("Metodo", 27),
    array("Imponibile", 17),
    array("Imposta", 17),
    array("Totale Vendite", 17),
    array("Totale Operazioni", 17),
));

$dataAry = array();
foreach($reportData['groupData']['groupedTotals']['payMethod'] as $k => $r) {
    $dataAry[] = array(
        ($k == "-1" ? "Altro/Non definito" : (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamento($k)['nome']),
        number_format($r['imponibile'], 2, ".", ","),
        number_format($r['imposta'], 2, ".", ","),
        number_format($r['totale'],  2, ".", ","),
        $r['totale_operazioni'],
    );
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row, true);
$spreadsheet->getActiveSheet()->getStyle('B5:B' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
$spreadsheet->getActiveSheet()->getStyle('C5:C' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
$spreadsheet->getActiveSheet()->getStyle('D5:D' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);

$spreadsheet->addPieChart('Vendite per metodo di pagamento',
    array(
        'sheet_name' => 'Vendite per metodo di pagamento',
        'label' => array('$A$5', '$A$'. ($row+count($dataAry)-1)),
        'data' => array('$D$5', '$D$'. ($row+count($dataAry)-1))
    ),
    array("H5", "M20")
);





$spreadsheet->addNewSheet('Vendite per tipologia documento');
$spreadsheet->Header(array(
    array("Tipologia", 27),
    array("Imponibile", 17),
    array("Imposta", 17),
    array("Totale Vendite", 17),
    array("Totale Operazioni", 17),
));

$dataAry = array();
foreach($reportData['groupData']['groupedTotals']['docType'] as $k => $r) {
    $dataAry[] = array(
        (new \MSFramework\Fatturazione\vendite())->ottieniTipiDocumento($k, false),
        number_format($r['imponibile'], 2, ".", ","),
        number_format($r['imposta'], 2, ".", ","),
        number_format($r['totale'],  2, ".", ","),
        $r['totale_operazioni'],
    );
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row, true);
$spreadsheet->getActiveSheet()->getStyle('B5:B' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
$spreadsheet->getActiveSheet()->getStyle('C5:C' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
$spreadsheet->getActiveSheet()->getStyle('D5:D' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);

$spreadsheet->addPieChart('Vendite per tipologia documento',
    array(
        'sheet_name' => 'Vendite per tipologia documento',
        'label' => array('$A$5', '$A$'. ($row+count($dataAry)-1)),
        'data' => array('$D$5', '$D$'. ($row+count($dataAry)-1))
    ),
    array("H5", "M20")
);



$spreadsheet->addNewSheet('Riepilogo imponibile-imposte');
$spreadsheet->Header(array(
    array("Tipologia", 27),
    array("Totale", 17),
));

$dataAry = array(
    array(
        'Imponibile',
        number_format($imponibile_globale, 2, ".", ","),
    ),array(
        'Imposte',
        number_format($imposta_globale, 2, ".", ","),
    )
);

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row, true);
$spreadsheet->getActiveSheet()->getStyle('B5:B' . ($row+count($dataAry)-1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);

$spreadsheet->addPieChart('Riepilogo imponibile-imposte',
    array(
        'sheet_name' => 'Riepilogo imponibile-imposte',
        'label' => array('$A$5', '$A$'. ($row+count($dataAry)-1)),
        'data' => array('$B$5', '$B$'. ($row+count($dataAry)-1))
    ),
    array("H5", "M20")
);

$spreadsheet->closeReport();
?>

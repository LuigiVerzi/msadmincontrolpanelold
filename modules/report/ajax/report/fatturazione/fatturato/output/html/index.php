<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

if(isset($_GET['standalone'])) {
    require_once('../../../../../../../../sw-config.php');
    require_once("../../db/getData.php");
}
?>

<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Tipo Documento</th>
                    <th>Data e Ora</th>
                    <th>Imponibile</th>
                    <th>Imposta</th>
                    <th>Totale Documento</th>
                    <th>Cliente</th>
                    <th>Operatore</th>
                </tr>
            </thead>

            <tbody>
            <?php
            $totale_globale = 0;
            $imponibile_globale = 0;
            $imposta_globale = 0;

            foreach($reportData['queryResult'] as $r) {
                $carrello = json_decode($r['carrello'], true);

                $totale = 0;
                $imponibile = 0;
                $imposta = 0;
                foreach($carrello as $c) {
                    $totale += (float)$c['subtotal'];
                    $imponibile += (float)$c['prezzo'];
                    $imposta += (float)$c['prezzo_iva_inclusa']-(float)$c['prezzo'];

                    $totale_globale += (float)$c['subtotal'];
                    $imponibile_globale += (float)$c['prezzo'];
                    $imposta_globale += (float)$c['prezzo_iva_inclusa']-(float)$c['prezzo'];
                }

                $cliente_data = (new \MSFramework\customers())->getCustomerDataFromDB($r['cliente']);
                $cliente = $cliente_data['nome'] . ' ' . $cliente_data['cognome'];

                $operatore_data = (new \MSFramework\users())->getUserDataFromDB($r['operatore']);
                $operatore = $operatore_data['nome'] . ' ' . $operatore_data['cognome'];
            ?>
            <tr>
                <td>
                    <?= (new \MSFramework\Fatturazione\vendite())->ottieniTipiDocumento($r['tipo_documento'], false) ?>
                </td>
                <td>
                    <?= date("d/m/Y H:i", strtotime($r['data'])) ?>
                </td>
                <td>
                    <?= number_format($imponibile, 2, ",", ".") . CURRENCY_SYMBOL ?>
                </td>
                <td>
                    <?= number_format($imposta, 2, ",", ".") . CURRENCY_SYMBOL ?>
                </td>
                <td>
                    <?= number_format($totale, 2, ",", ".") . CURRENCY_SYMBOL ?>
                </td>
                <td>
                    <?= $cliente ?>
                </td>
                <td>
                    <?= $operatore ?>
                </td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="1" style="text-align: right;"> &nbsp; </th>
                <th colspan="2" style="text-align: right;">Tot. imponibile: <?= number_format($imponibile_globale, 2, ",", ".") . CURRENCY_SYMBOL ?></th>
                <th colspan="2" style="text-align: right;">Tot. imposte: <?= number_format($imposta_globale, 2, ",", ".") . CURRENCY_SYMBOL ?></th>
                <th colspan="2" style="text-align: right;">Tot. fatturato: <?= number_format($totale_globale, 2, ",", ".") . CURRENCY_SYMBOL ?></th>
            </tr>
            <tr>
                <th colspan="7" style="text-align: right;">Totale risultati: <?= count($reportData['queryResult']) ?></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Vendite per operatore</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-8">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Operatore</th>
                                <th>Imponibile</th>
                                <th>Imposta</th>
                                <th>Totale Vendite</th>
                                <th>Totale Operazioni</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            foreach($reportData['groupData']['groupedTotals']['operator'] as $k => $r) {
                                ?>
                                <tr>
                                    <td>
                                        <?php
                                            $user = (new \MSFramework\users())->getUserDataFromDB($k, "nome, cognome");
                                            echo $user['cognome'] . " " . $user['nome'];
                                        ?>
                                    </td>
                                    <td>
                                        <?= number_format($r['imponibile'], 2, ",", ".") . CURRENCY_SYMBOL ?>
                                    </td>
                                    <td>
                                        <?= number_format($r['imposta'], 2, ",", ".") . CURRENCY_SYMBOL ?>
                                    </td>
                                    <td>
                                        <?= number_format($r['totale'], 2, ",", ".") . CURRENCY_SYMBOL ?>
                                        <input type="hidden" class="torta_vendite_operatori_value" value="<?= number_format($r['totale'], 2, ".", "") ?>" />
                                    </td>
                                    <td>
                                        <?= $r['totale_operazioni'] ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-4">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="text-center">
                                    <div id="torta_vendite_operatori" class="torta"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Vendite per metodo di pagamento</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-8">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Metodo</th>
                                <th>Imponibile</th>
                                <th>Imposta</th>
                                <th>Totale Vendite</th>
                                <th>Totale Operazioni</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            foreach($reportData['groupData']['groupedTotals']['payMethod'] as $k => $r) {
                                ?>
                                <tr>
                                    <td>
                                        <?= ($k == "-1" ? "Altro/Non definito" : (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamento($k)['nome']) ?>
                                    </td>
                                    <td>
                                        <?= number_format($r['imponibile'], 2, ",", ".") . CURRENCY_SYMBOL ?>
                                    </td>
                                    <td>
                                        <?= number_format($r['imposta'], 2, ",", ".") . CURRENCY_SYMBOL ?>
                                    </td>
                                    <td>
                                        <?= number_format($r['totale'], 2, ",", ".") . CURRENCY_SYMBOL ?>
                                        <input type="hidden" class="torta_metodi_pagamento_value" value="<?= number_format($r['totale'], 2, ".", "") ?>" />
                                    </td>
                                    <td>
                                        <?= $r['totale_operazioni'] ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-4">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="text-center">
                                    <div id="torta_metodi_pagamento" class="torta"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Vendite per tipologia documento</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-8">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Tipologia</th>
                                <th>Imponibile</th>
                                <th>Imposta</th>
                                <th>Totale Vendite</th>
                                <th>Totale Operazioni</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            foreach($reportData['groupData']['groupedTotals']['docType'] as $k => $r) {
                                ?>
                                <tr>
                                    <td>
                                        <?= (new \MSFramework\Fatturazione\vendite())->ottieniTipiDocumento($k, false) ?>
                                    </td>
                                    <td>
                                        <?= number_format($r['imponibile'], 2, ",", ".") . CURRENCY_SYMBOL ?>
                                    </td>
                                    <td>
                                        <?= number_format($r['imposta'], 2, ",", ".") . CURRENCY_SYMBOL ?>
                                    </td>
                                    <td>
                                        <?= number_format($r['totale'], 2, ",", ".") . CURRENCY_SYMBOL ?>
                                        <input type="hidden" class="torta_tipologia_documento_value" value="<?= number_format($r['totale'], 2, ".", "") ?>" />
                                    </td>
                                    <td>
                                        <?= $r['totale_operazioni'] ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-4">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="text-center">
                                    <div id="torta_tipologia_documento" class="torta"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Riepilogo imponibile/imposte</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-4">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Tipologia</th>
                                <th>Totale</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td>Imponibile</td>
                                <td><?= number_format($imponibile_globale, 2, ",", ".") . CURRENCY_SYMBOL ?></td>
                                <input type="hidden" class="torta_tasse_value" value="<?= number_format($imponibile_globale, 2, ".", "") ?>" />
                            </tr>
                            <tr>
                                <td>Imposte</td>
                                <td><?= number_format($imposta_globale, 2, ",", ".") . CURRENCY_SYMBOL ?></td>
                                <input type="hidden" class="torta_tasse_value" value="<?= number_format($imposta_globale, 2, ".", "") ?>" />
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-8">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="text-center">
                                    <div id="torta_tasse" class="torta"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

$(document).ready(function() {
    $('#data_to, #data_from').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
        endDate: new Date(),
    });
})

function previewUpdateCallback() {
    $('.torta').each(function() {
        torta_id = $(this).attr('id');
        data_ary = new Array();
        $('.' + torta_id + '_value').each(function() {
            data_ary.push($(this).val());
        });

        $("#" + torta_id).sparkline(data_ary, {
            type: 'pie',
            height: '140',
        });
    })
}

<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

$sort_options = array(
    "data" => "Data",
    "tipo_documento" => "Tipo Documento",
);

$impostazioni_fatture = $MSFrameworkCMS->getCMSData('fatturazione_fatture');
?>

<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <label>Emissione</label>
            </div>

            <div class="col-sm-6">
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <label>Da</label>
        <div class="input-group date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="data_from" data-db-exclude-common="1">
        </div>
    </div>

    <div class="col-sm-3">
        <label>Fino a</label>
        <div class="input-group date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="data_to" data-db-exclude-common="1">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <label>Tipo Documento</label>
        <select id="tipo_documento" class="form-control">
            <option value="">Tutti</option>

            <?php
            foreach((new \MSFramework\Fatturazione\vendite())->ottieniTipiDocumento() as $group => $docType) {
                ?>
                <optgroup label="<?= $group ?>">
                    <?php
                    foreach($docType as $docK => $docName) {
                        ?>
                        <option value="<?= $docK ?>"><?= $docName ?></option>
                        <?php
                    }
                    ?>
                </optgroup>
                <?php
            }
            ?>
        </select>
    </div>

    <div class="col-sm-4">
        <label>Operatore</label>
        <select id="operatore" class="form-control">
            <option value="">Tutti</option>
            <?php foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome FROM users") as $operatore) { ?>
                <option value="<?= $operatore['id']; ?>"><?= $operatore['nome'] . ' ' . $operatore['cognome']; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-sm-4">
        <label>Metodo di pagamento</label>
        <select id="metodo_pagamento" class="form-control" data-db-exclude-common="1">
            <option value="">Tutti</option>
            <option value="-1">Altro/Non definito</option>
            <?php foreach((new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamento() as $id => $metodo) { ?>
                <option value="<?= $id; ?>"><?= $metodo['nome']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
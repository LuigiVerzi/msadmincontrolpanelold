<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

$sort_options = array(
    "customers.nome, customers.cognome" => "Nome e Cognome",
    "customers.cognome, customers.nome" => "Cognome e Nome",
    "customers.email" => "Email",
    "customers.data_nascita" => "Compleanno",
);
?>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-6">
                <label>Data registrazione</label>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <label>Da</label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="registration_date_from" data-db-exclude-common="1">
                </div>
            </div>

            <div class="col-sm-6">
                <label>Fino a</label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="registration_date_to" data-db-exclude-common="1">
                </div>
            </div>
        </div>
    </div>



    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-6">
                <label>Compleanno</label>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <label>Da</label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="birthday_from" data-db-exclude-common="1">
                </div>
            </div>

            <div class="col-sm-6">
                <label>Fino a</label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="birthday_to" data-db-exclude-common="1">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <label>&nbsp;</label>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                    <input type="checkbox" id="just_newsletter" data-db-exclude-common="1">
                    <i></i>
                </div>
                <span style="font-weight: normal;">Iscritti a Newsletter</span>
            </label>
        </div>
    </div>

    <div class="col-sm-2">
        <label>Sesso</label>
        <select id="sesso" class="form-control">
            <option value="">Tutti</option>
            <option value="m">Maschio</option>
            <option value="f">Femmina</option>
        </select>
    </div>

    <div class="col-sm-3">
        <label>Città</label>
        <input id="citta" type="text" class="form-control" data-db-operator="%LIKE%" />
    </div>

    <div class="col-sm-3">
        <label>CAP</label>
        <input id="cap" type="text" class="form-control" />
    </div>
</div>
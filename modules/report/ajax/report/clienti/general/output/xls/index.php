<?php
/**
 * MSAdminControlPanel
 * Date: 22/12/2018
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use \PhpOffice\PhpSpreadsheet\Chart\Chart;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use \PhpOffice\PhpSpreadsheet\Chart\Layout;
use \PhpOffice\PhpSpreadsheet\Chart\Legend;
use \PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use \PhpOffice\PhpSpreadsheet\Chart\Title;

$spreadsheet = new \MSFramework\Report\Output\XLS\xls($_GET['p'], $_GET['i']);

$spreadsheet->Header(array(
    array("Nome e Cognome", 45),
    array("Telefono Casa", 20),
    array("Telefono Cellulare", 20),
    array("Email", 55),
    array("Compleanno", 25),
));

$dataAry = array();
foreach($reportData['queryResult'] as $r) {
    $dataAry[] = array(
        $r['nome'] . ' ' . $r['cognome'],
        $r['telefono_casa'],
        $r['telefono_cellulare'],
        $r['email'],
        date("d/m/Y", $r['data_nascita'])
    );
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row);



$spreadsheet->addNewSheet('Grafico Sesso');
$spreadsheet->Header(array(
    array("Sesso", 25),
    array("Totale", 10),
));

$dataAry = array();
$sex_sum = 0;
foreach($reportData['groupData']['sesso'] as $sexK => $sexV) {
    if($sexK == "m") {
        $row_type = "Uomini";
    } else if($sexK == "f") {
        $row_type = "Donne";
    } else {
        $row_type = "Non Specificato";
    }

    $dataAry[] = array(
        $row_type,
        $sexV,
    );

    $sex_sum += $sexV;
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->getStyle('B6')
    ->getNumberFormat()
    ->setFormatCode(
        '0'
    );

$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row, true);

$spreadsheet->addPieChart('Grafico Sesso',
    array(
        'sheet_name' => 'Grafico Sesso',
        'label' => array('$A$5', '$A$'. ($row+count($dataAry)-1)),
        'data' => array('$B$5', '$B$'. ($row+count($dataAry)-1))
    ),
    array("E5", "L20")
);

$spreadsheet->closeReport();
?>

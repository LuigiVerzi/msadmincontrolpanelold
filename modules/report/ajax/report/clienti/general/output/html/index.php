<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

if(isset($_GET['standalone'])) {
    require_once('../../../../../../../../sw-config.php');
    require_once("../../db/getData.php");
}
?>

<div class="row" style="max-height: 350px; overflow: auto;">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Nome e Cognome</th>
                    <th>Telefono Casa</th>
                    <th>Telefono Cellulare</th>
                    <th>Email</th>
                    <th>Compleanno</th>
                </tr>
            </thead>

            <tbody>
            <?php
            foreach($reportData['queryResult'] as $r) {
            ?>
            <tr>
                <td>
                    <?= $r['nome'] ?> <?= $r['cognome'] ?>
                </td>
                <td>
                    <?= $r['telefono_casa'] ?>
                </td>
                <td>
                    <?= $r['telefono_cellulare'] ?>
                </td>
                <td>
                    <?= $r['email'] ?>
                </td>
                <td>
                    <?= date("d/m/Y", $r['data_nascita']) ?>
                </td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="3" style="text-align: right;">Totale risultati: <?= count($reportData['queryResult']) ?></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Risultati per sesso</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-4">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sesso</th>
                                <th>Totale</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            foreach($reportData['groupData']['sesso'] as $sexK => $sexV) {
                                ?>
                                <tr>
                                    <td>
                                        <?php
                                        if($sexK == "m") {
                                            echo "Uomini";
                                        } else if($sexK == "f") {
                                            echo "Donne";
                                        } else {
                                            echo "Non Specificato";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?= $sexV ?>
                                        <input type="hidden" id="sex_<?= $sexK ?>" value="<?= $sexV ?>" />
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-8">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="text-center">
                                    <div id="torta_sesso"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

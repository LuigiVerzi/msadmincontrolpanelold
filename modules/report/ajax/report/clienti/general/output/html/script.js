$(document).ready(function() {
    $('#birthday_to, #birthday_from, #registration_date_to, #registration_date_from').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
        endDate: new Date(),
    });
})

function previewUpdateCallback() {
    $("#torta_sesso").sparkline([$('#sex_m').val(), $('#sex_f').val(), $('#sex_nd').val()], {
        type: 'pie',
        height: '140',
        sliceColors: ['#28d1fa', '#f389f5', '#dadfdf']
    });
}

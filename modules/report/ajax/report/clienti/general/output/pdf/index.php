<?php
/**
 * MSAdminControlPanel
 * Date: 21/12/2018
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

$pdf = new \MSFramework\Report\Output\PDF\pdf($_GET['p'], $_GET['i']);
$table = new \Interpid\PdfLib\Table($pdf);

$pdf->openReport();

$table->initialize( [ $pdf->widthFromPercentage("25"), $pdf->widthFromPercentage("15"), $pdf->widthFromPercentage("15"), $pdf->widthFromPercentage("25"), $pdf->widthFromPercentage("20") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Nome e Cognome' ],
    [ 'TEXT' => 'Telefono Casa' ],
    [ 'TEXT' => 'Telefono Cellulare' ],
    [ 'TEXT' => 'Email' ],
    [ 'TEXT' => 'Compleanno' ],
));

foreach($reportData['queryResult'] as $r) {
    $table->addRow(array(
        [ 'TEXT' => $r['nome'] . ' ' . $r['cognome'] ],
        [ 'TEXT' => $r['telefono_casa'] ],
        [ 'TEXT' => $r['telefono_cellulare'] ],
        [ 'TEXT' => $r['email'] ],
        [ 'TEXT' => date("d/m/Y", $r['data_nascita']) ],
    ));
}

$table->close();
$pdf->SetDefaultStyle();

$pdf->ln(8);

$table->initialize( [ $pdf->widthFromPercentage("30"), $pdf->widthFromPercentage("20") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Sesso' ],
    [ 'TEXT' => 'Totale' ],
));

foreach($reportData['groupData']['sesso'] as $sexK => $sexV) {
    if($sexK == "m") {
        $row_type = "Uomini";
    } else if($sexK == "f") {
        $row_type = "Donne";
    } else {
        $row_type = "Non Specificato";
    }

    $table->addRow(array(
        [ 'TEXT' => $row_type ],
        [ 'TEXT' => $sexV ],
    ));
}

$dati_torta['data'] = array($reportData['groupData']['sesso']['m'], $reportData['groupData']['sesso']['f'], $reportData['groupData']['sesso']['-']);

$table->close();
$pdf->SetDefaultStyle();


$pdf->addPieChart($dati_torta, $pdf->widthFromPercentage("85"));
unset($dati_torta);

$pdf->closeReport($_POST['output_type']);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

require(dirname(__FILE__, 4) . "/commonGetData.php");

if($filters['birthday_from']['value'] != "") {
    $named_par = $MSFrameworkDatabase->formatNamedParameter('birthday_from');

    $where_str .= " AND data_nascita >= :" . $named_par . " ";
    $where_ary[':' . $named_par] = strtotime(str_replace('/', '-', $filters['birthday_from']['value']));

    unset($filters['birthday_from']);
}

if($filters['birthday_to']['value'] != "") {
    $named_par = $MSFrameworkDatabase->formatNamedParameter('birthday_to');

    $where_str .= " AND data_nascita <= :" . $named_par . " ";
    $where_ary[':' . $named_par] = strtotime(str_replace('/', '-', $filters['birthday_to']['value']));

    unset($filters['birthday_to']);
}

if($filters['registration_date_from']['value'] != "") {
    $filters['registration_date_from']['value'] = date("Y-m-d", strtotime(str_replace("/", "-", $filters['registration_date_from']['value'])));

    $named_par = $MSFrameworkDatabase->formatNamedParameter('registration_date_from');

    $where_str .= " AND DATE(registration_date) >= :" . $named_par . " ";
    $where_ary[':' . $named_par] = $filters['registration_date_from']['value'];

    unset($filters['registration_date_from']);
}

if($filters['registration_date_to']['value'] != "") {
    $filters['registration_date_to']['value'] = date("Y-m-d", strtotime(str_replace("/", "-", $filters['registration_date_to']['value'])));

    $named_par = $MSFrameworkDatabase->formatNamedParameter('registration_date_to');

    $where_str .= " AND DATE(registration_date) <= :" . $named_par . " ";
    $where_ary[':' . $named_par] = $filters['registration_date_to']['value'];

    unset($filters['registration_date_to']);
}

if($filters['just_newsletter']['value'] == "1") {
    $subscribed_lj = " LEFT JOIN newsletter__destinatari ON newsletter__destinatari.email = customers.email ";
    $subscribed_where = " AND newsletter__destinatari.active = 1 ";
}

$reportDataResult = $MSFrameworkDatabase->getAssoc("SELECT customers.nome, customers.cognome, sesso, data_nascita, customers.email, customers.telefono_casa, customers.telefono_cellulare FROM customers $subscribed_lj WHERE customers.id != '' $where_str $subscribed_where $order_by_str", $where_ary);

$sex_groups = array(
    "m" => 0,
    "f" => 0,
    "-" => 0,
);

foreach($reportDataResult as $r) {
    $sex_groups[($r['sesso'] != "" ? $r['sesso'] : "-")]++;
}

$reportData = array(
    'queryResult' => $reportDataResult,
    'groupData' => array(
        "sesso" => $sex_groups,
    )
);

if(count($reportDataResult) == 0) {
    die('<div class="text-center">Nessun dato disponibile per i filtri impostati</div>');
}
?>
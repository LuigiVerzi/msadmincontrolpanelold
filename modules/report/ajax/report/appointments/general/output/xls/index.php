<?php
/**
 * MSAdminControlPanel
 * Date: 06/02/2019
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use \PhpOffice\PhpSpreadsheet\Chart\Chart;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use \PhpOffice\PhpSpreadsheet\Chart\Layout;
use \PhpOffice\PhpSpreadsheet\Chart\Legend;
use \PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use \PhpOffice\PhpSpreadsheet\Chart\Title;

use \PhpOffice\PhpSpreadsheet\Style\NumberFormat;

$spreadsheet = new \MSFramework\Report\Output\XLS\xls($_GET['p'], $_GET['i']);

$spreadsheet->Header(array(
    array("Inizio appuntamento", 20),
    array("Fine appuntamento", 20),
    array("Cliente", 45),
    array("Operatore", 25),
    array("Servizi", 55),
    array("Cabina", 30)
));

$dataAry = array();
foreach($reportData['queryResult'] as $r) {
    $customer_data = (new \MSFramework\customers())->getCustomerDataFromDB($r['customer'], "cognome, nome, telefono_casa, telefono_cellulare");

    if($r['user'] != "-1") {
        $user_data = (new \MSFramework\users())->getUserDataFromDB($r['user'], "cognome, nome");
    } else {
        $user_data = array("cognome" => "Operatore", "nome" => "Generico");
    }

    $cabin_str = "Cabina Generica";
    if($r['cabin'] != "-1") {
        $r_cabin = $MSFrameworkDatabase->getAssoc("SELECT nome FROM beautycenter_cabine WHERE id = :id", array(":id" => $r['cabin']), true);
        $cabin_str = "Cabina " . $MSFrameworki18n->getFieldValue($r_cabin['nome'], true);
    }

    $services_str = "";
    foreach(explode(",", $r['services']) as $service) {
        if(!in_array($service, $service_cache)) {
            $services_data = (new \MSFramework\services())->getServiceDetails($service, "nome")[$service];
            foreach($services_data as $serviceK => $serviceV) {
                $service_cache[$service][$serviceK] = $serviceV;
            }
        }

        $services_str .= $MSFrameworki18n->getFieldValue($service_cache[$service]['nome'], true) . ", ";
    }
    $services_str = ($services_str != "" ? substr($services_str, 0, -2) : "");

    $telefono_str = "";
    $telefono_str .= ($customer_data['telefono_casa'] != "" ? "Telefono casa: " . $customer_data['telefono_casa'] . ", " : "");
    $telefono_str .= ($customer_data['telefono_cellulare'] != "" ? "Telefono cellulare: " . $customer_data['telefono_cellulare'] . ", " : "");
    if($telefono_str != "") {
        $telefono_str = " \nContatti: " . substr($telefono_str, 0, -2);
    }

    $dataAry[] = array(
        date("d/m/Y", strtotime($r['day'])) . " " . date("H:i", strtotime($r['start_date'])),
        date("d/m/Y", strtotime($r['end_day'])) . " " . date("H:i", strtotime($r['end_date'])),
        $customer_data['cognome'] . " " . $customer_data['nome'] . $telefono_str,
        $user_data['cognome'] . " " . $user_data['nome'],
        $services_str,
        $cabin_str
    );
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row);
$spreadsheet->getActiveSheet()->getStyle('C5:C' . ($row+count($dataAry)-1))->getAlignment()->setWrapText(true);

$spreadsheet->closeReport();
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 06/02/2019
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

$pdf = new \MSFramework\Report\Output\PDF\pdf($_GET['p'], $_GET['i']);
$table = new \Interpid\PdfLib\Table($pdf);
$table->setStyle( "p", $pdf->defaultFont, "", $pdf->defaultFontSize, "0,0,0" );
$table->setStyle( "b", $pdf->defaultFont, "B", $pdf->defaultFontSize, "0,0,0" );

$pdf->openReport();

$table->initialize( [ $pdf->widthFromPercentage("15"), $pdf->widthFromPercentage("15"), $pdf->widthFromPercentage("25"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("25"), $pdf->widthFromPercentage("10") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Inizio appuntamento' ],
    [ 'TEXT' => 'Fine appuntamento' ],
    [ 'TEXT' => 'Cliente' ],
    [ 'TEXT' => 'Operatore' ],
    [ 'TEXT' => 'Servizi' ],
    [ 'TEXT' => 'Cabina' ],
));

foreach($reportData['queryResult'] as $r) {
    $customer_data = (new \MSFramework\customers())->getCustomerDataFromDB($r['customer'], "cognome, nome, telefono_casa, telefono_cellulare");

    if($r['user'] != "-1") {
        $user_data = (new \MSFramework\users())->getUserDataFromDB($r['user'], "cognome, nome");
    } else {
        $user_data = array("cognome" => "Operatore", "nome" => "Generico");
    }

    $cabin_str = "Cabina Generica";
    if($r['cabin'] != "-1") {
        $r_cabin = $MSFrameworkDatabase->getAssoc("SELECT nome FROM beautycenter_cabine WHERE id = :id", array(":id" => $r['cabin']), true);
        $cabin_str = "Cabina " . $MSFrameworki18n->getFieldValue($r_cabin['nome'], true);
    }

    $services_str = "";
    foreach(explode(",", $r['services']) as $service) {
        if(!in_array($service, $service_cache)) {
            $services_data = (new \MSFramework\services())->getServiceDetails($service, "nome")[$service];
            foreach($services_data as $serviceK => $serviceV) {
                $service_cache[$service][$serviceK] = $serviceV;
            }
        }

        $services_str .= $MSFrameworki18n->getFieldValue($service_cache[$service]['nome'], true) . ", ";
    }
    $services_str = ($services_str != "" ? substr($services_str, 0, -2) : "");

    $telefono_str = "";
    $telefono_str .= ($customer_data['telefono_casa'] != "" ? "Telefono casa: " . $customer_data['telefono_casa'] . ", " : "");
    $telefono_str .= ($customer_data['telefono_cellulare'] != "" ? "Telefono cellulare: " . $customer_data['telefono_cellulare'] . ", " : "");
    if($telefono_str != "") {
        $telefono_str = " <b>\nContatti</b>: " . substr($telefono_str, 0, -2);
    }

    $table->addRow(array(
        [ 'TEXT' => date("d/m/Y", strtotime($r['day'])) . " " . date("H:i", strtotime($r['start_date'])) ],
        [ 'TEXT' => date("d/m/Y", strtotime($r['end_day'])) . " " . date("H:i", strtotime($r['end_date'])) ],
        [ 'TEXT' => $customer_data['cognome'] . " " . $customer_data['nome'] . $telefono_str ],
        [ 'TEXT' => $user_data['cognome'] . " " . $user_data['nome'] ],
        [ 'TEXT' => $services_str ],
        [ 'TEXT' => $cabin_str ],
    ));
}

$table->close();
$pdf->SetDefaultStyle();

$pdf->closeReport($_POST['output_type']);
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 06/02/2019
 */

if(isset($_GET['standalone'])) {
    require_once('../../../../../../../../sw-config.php');
    require_once("../../db/getData.php");
}
?>

<div class="row" style="max-height: 350px; overflow: auto;">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Inizio appuntamento</th>
                    <th>Fine appuntamento</th>
                    <th>Cliente</th>
                    <th>Operatore</th>
                    <th>Servizi</th>
                    <th>Cabina</th>
                </tr>
            </thead>

            <tbody>
            <?php
            foreach($reportData['queryResult'] as $r) {
                $customer_data = (new \MSFramework\customers())->getCustomerDataFromDB($r['customer'], "cognome, nome, telefono_casa, telefono_cellulare");

                if($r['user'] != "-1") {
                    $user_data = (new \MSFramework\users())->getUserDataFromDB($r['user'], "cognome, nome");
                } else {
                    $user_data = array("cognome" => "Operatore", "nome" => "Generico");
                }

                $cabin_str = "Cabina Generica";
                if($r['cabin'] != "-1") {
                    $r_cabin = $MSFrameworkDatabase->getAssoc("SELECT nome FROM beautycenter_cabine WHERE id = :id", array(":id" => $r['cabin']), true);
                    $cabin_str = "Cabina " . $MSFrameworki18n->getFieldValue($r_cabin['nome'], true);
                }

                $services_str = "";
                foreach(explode(",", $r['services']) as $service) {
                    if(!in_array($service, $service_cache)) {
                        $services_data = (new \MSFramework\services())->getServiceDetails($service, "nome")[$service];
                        foreach($services_data as $serviceK => $serviceV) {
                            $service_cache[$service][$serviceK] = $serviceV;
                        }
                    }

                    $services_str .= $MSFrameworki18n->getFieldValue($service_cache[$service]['nome'], true) . ", ";
                }
                $services_str = ($services_str != "" ? substr($services_str, 0, -2) : "");

                $telefono_str = "";
                $telefono_str .= ($customer_data['telefono_casa'] != "" ? "Telefono casa: " . $customer_data['telefono_casa'] . ", " : "");
                $telefono_str .= ($customer_data['telefono_cellulare'] != "" ? "Telefono cellulare: " . $customer_data['telefono_cellulare'] . ", " : "");
                if($telefono_str != "") {
                    $telefono_str = '<div style="margin-top: 5px;"><b>Contatti:</b> ' . substr($telefono_str, 0, -2) . "</div>";
                }
            ?>
            <tr>
                <td>
                    <?= date("d/m/Y", strtotime($r['day'])) . " " . date("H:i", strtotime($r['start_date'])) ?>
                </td>
                <td>
                    <?= date("d/m/Y", strtotime($r['end_day'])) . " " . date("H:i", strtotime($r['end_date'])) ?>
                </td>
                <td>
                    <?= $customer_data['cognome'] . " " . $customer_data['nome'] . $telefono_str ?>
                </td>
                <td>
                    <?= $user_data['cognome'] . " " . $user_data['nome'] ?>
                </td>
                <td>
                    <?= $services_str ?>
                </td>
                <td>
                    <?= $cabin_str ?>
                </td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="6" style="text-align: right;">Totale risultati: <?= count($reportData['queryResult']) ?></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
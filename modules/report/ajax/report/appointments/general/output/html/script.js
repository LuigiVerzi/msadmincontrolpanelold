$(document).ready(function() {
    $('#appointment_date_to, #appointment_date_from').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it"
    });
})

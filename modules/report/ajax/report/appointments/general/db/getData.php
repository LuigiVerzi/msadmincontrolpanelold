<?php
/**
 * MSAdminControlPanel
 * Date: 06/02/2019
 */

require(dirname(__FILE__, 4) . "/commonGetData.php");

if($filters['appointment_date_from']['value'] != "") {
    $filters['appointment_date_from']['value'] = date("Y-m-d", strtotime(str_replace("/", "-", $filters['appointment_date_from']['value'])));

    $named_par = $MSFrameworkDatabase->formatNamedParameter('appointment_date_from');

    $where_str .= " AND DATE(start_date) >= :" . $named_par . " ";
    $where_ary[':' . $named_par] = $filters['appointment_date_from']['value'];

    unset($filters['appointment_date_from']);
}

if($filters['appointment_date_to']['value'] != "") {
    $filters['appointment_date_to']['value'] = date("Y-m-d", strtotime(str_replace("/", "-", $filters['appointment_date_to']['value'])));

    $named_par = $MSFrameworkDatabase->formatNamedParameter('appointment_date_to');

    $where_str .= " AND DATE(start_date) <= :" . $named_par . " ";
    $where_ary[':' . $named_par] = $filters['appointment_date_to']['value'];

    unset($filters['appointment_date_to']);
}

$order_by_str = str_replace("ORDER BY", "ORDER BY start_date ASC, ");
$reportDataResult = $MSFrameworkDatabase->getAssoc("SELECT *, date(start_date) as day, date(end_date) as end_day FROM appointments_list WHERE id != '' $where_str $order_by_str", $where_ary);

$reportData = array(
    'queryResult' => $reportDataResult
);

if(count($reportDataResult) == 0) {
    die('<div class="text-center">Nessun dato disponibile per i filtri impostati</div>');
}
?>
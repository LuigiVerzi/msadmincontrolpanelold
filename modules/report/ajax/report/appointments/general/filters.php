<?php
/**
 * MSAdminControlPanel
 * Date: 06/02/2019
 */

/*
 * Ottengo gli operatori (tutti se superadmin o admin, solo l'operatore corrente in caso contrario)
 */
$users_where_str = "";
$users_where_ary = array();
if((new \MSFramework\users())->getUserDataFromSession('userlevel') != "0" && (new \MSFramework\users())->getUserDataFromSession('userlevel') != "1") {
    $users_where_str = " AND id = :id ";
    $users_where_ary = array(":id" => (new \MSFramework\users())->getUserDataFromSession('user_id'));
}
$ary_operatori = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome FROM users WHERE id != '' AND enable_appointments != 0 $users_where_str", $users_where_ary) as $r) {
    $ary_operatori[$r['id']] = $r['cognome'] . " " . $r['nome'];
}
asort($ary_operatori);
if(count($ary_operatori) == 0) {
    $ary_operatori = array("-1" => "Operatore Generico") + $ary_operatori;
}

$ary_cabine = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome FROM beautycenter_cabine") as $r) {
    $ary_cabine[$r['id']] = $MSFrameworki18n->getFieldValue($r['nome'], true);
}
asort($ary_cabine);
$ary_cabine = array("-1" => "Cabina Generica") + $ary_cabine;
?>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-6">
                <label>Data appuntamento</label>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <label>Da</label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="appointment_date_from" data-db-exclude-common="1">
                </div>
            </div>

            <div class="col-sm-6">
                <label>Fino a</label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="appointment_date_to" data-db-exclude-common="1">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <label>Operatore</label>
        <select id="user" class="form-control">
            <option value="">Tutti</option>
            <?php
            foreach($ary_operatori as $k => $v) {
            ?>
                <option value="<?= $k ?>"><?= $v ?></option>
            <?php
            }
            ?>
        </select>
    </div>

    <div class="col-sm-4">
        <label>Cabina</label>
        <select id="cabin" class="form-control">
            <option value="">Tutte</option>
            <?php
            foreach($ary_cabine as $k => $v) {
                ?>
                <option value="<?= $k ?>"><?= $v ?></option>
                <?php
            }
            ?>
        </select>
    </div>
</div>
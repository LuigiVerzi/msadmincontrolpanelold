<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

require_once('../../../../sw-config.php');

$parent = $_GET['p'];
$item = $_GET['i'];

$filepath_preview = $parent . "/" . $item . "/output/html/index.php";
$filepath_filters = $parent . "/" . $item . "/filters.php";

$filepath_preview_js = $parent . "/" . $item . "/output/html/script.js";

ob_start();
$html_cont = "";
$status = "err";

if(file_exists($filepath_preview)) {
    $status = "ok";
    $cat_details = (new \MSFramework\Report\report())->getAvailCategories($parent);
?>

    <input type="hidden" value="<?= $_GET['p'] ?>" id="hidden_parent" />
    <input type="hidden" value="<?= $_GET['i'] ?>" id="hidden_item" />

    <h2 class="m-b-lg" style="margin-top: -45px;"><?= $cat_details['name'] ?> > <?= $cat_details['childrens'][$item]['name'] ?></h2>

    <?php if(file_exists($filepath_filters)) { ?>
        <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtri & Ordinamento <small class="m-l-sm">Lasciare il filtro vuoto per non utilizzarlo durante la ricerca dei dati</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="filters_container" class="filters_monitor_changes"><?php include($filepath_filters); ?></div>

                    <?php
                    if(is_array($sort_options)) {
                    ?>
                    <div class="row filters_monitor_changes">
                        <div class="col-sm-6">
                            <label>Ordinamento</label>
                            <select id="order_by" class="form-control" data-db-exclude-common="1">
                                <option value="">Nessuno</option>
                                <?php
                                foreach($sort_options as $sortK => $sortV) {
                                ?>
                                    <option value="<?= $sortK ?>"><?= $sortV ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col-sm-3" id="sort_direction" style="display: none;">
                            <label> &nbsp; </label>
                            <select id="order_by_dir" class="form-control" data-db-exclude-common="1">
                                <option value="ASC">Crescente</option>
                                <option value="DESC">Decrescente</option>
                            </select>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
    <?php } ?>

    <div id="filter_preview">
        <?php //include($filepath_preview); (lo carico via ajax in modo da impostare automaticamente anche i filtri/ordinamento di default) ?>
    </div>

    <div class="row m-t-lg">
        <div class="col-md-12 small text-right bg-muted" style="padding: 10px;"><i class="fa fa-info-circle"></i> E' possibile esportare questo report in PDF o in XLS</div>
    </div>

    <?php if(file_exists($filepath_preview_js)) { ?>
    <script src="ajax/report/<?= $filepath_preview_js ?>"></script>
    <?php } ?>

<?php
}

$html_cont = ob_get_contents();
ob_clean();

echo json_encode(array("status" => $status, "html" => $html_cont));
?>

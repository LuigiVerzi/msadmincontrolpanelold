<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

$sort_options = array(
    "nome, cognome" => "Nome e Cognome",
    "cognome, nome" => "Cognome e Nome",
    "email" => "Email"
);
?>

<div class="row">
    <div class="col-sm-4">
        <label>Livello</label>
        <select id="ruolo" class="form-control">
            <option value=""></option>
            <optgroup label="Ruoli Standard">
                <?php
                foreach($MSFrameworkUsers->getUserLevels() as $levelK => $levelV) {
                    ?>
                    <option value="<?php echo $levelK ?>" <?php if($r['ruolo'] == $levelK) { echo "selected"; } ?>><?php echo $levelV ?></option>
                    <?php
                }
                ?>
            </optgroup>
            <optgroup label="Ruoli Personalizzati">
                <?php
                foreach($MSFrameworkUsers->getCustomUserLevels() as $levelK => $levelV) {
                    ?>
                    <option value="custom-<?php echo $levelV['id'] ?>" <?php if($r['ruolo'] == 'custom-' . $levelV['id']) { echo "selected"; } ?>><?php echo $levelV['name'] ?></option>
                    <?php
                }
                ?>
            </optgroup>
        </select>
    </div>

    <div class="col-sm-3">
        <label>&nbsp;</label>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                    <input type="checkbox" id="just_active" data-db-exclude-common="1">
                    <i></i>
                </div>
                <span style="font-weight: normal;">Solo Attivi</span>
            </label>
        </div>
    </div>
</div>
<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

require(dirname(__FILE__, 4) . "/commonGetData.php");

if($filters['just_active']['value'] == "1") {
    $subscribed_where = " AND active = 1 ";
}

$reportDataResult = $MSFrameworkDatabase->getAssoc("SELECT nome, cognome, active, email, ruolo FROM users WHERE id != '' $where_str $subscribed_where $order_by_str", $where_ary);

$reportData = array(
    'queryResult' => $reportDataResult
);

if(count($reportDataResult) == 0) {
    die('<div class="text-center">Nessun dato disponibile per i filtri impostati</div>');
}
?>
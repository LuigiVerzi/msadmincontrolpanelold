<?php
/**
 * MSAdminControlPanel
 * Date: 22/12/2018
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use \PhpOffice\PhpSpreadsheet\Chart\Chart;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use \PhpOffice\PhpSpreadsheet\Chart\Layout;
use \PhpOffice\PhpSpreadsheet\Chart\Legend;
use \PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use \PhpOffice\PhpSpreadsheet\Chart\Title;

$spreadsheet = new \MSFramework\Report\Output\XLS\xls($_GET['p'], $_GET['i']);
$usersClass = new \MSFramework\users();

$spreadsheet->Header(array(
    array("Nome e Cognome", 45),
    array("Email", 45),
    array("Livello", 20),
    array("Attivo", 20)
));

$dataAry = array();
foreach($reportData['queryResult'] as $r) {
    $dataAry[] = array(
        $r['nome'] . ' ' . $r['cognome'],
        $r['email'],
        $usersClass->getUserLevels($r['ruolo']),
        ($r['active'] == "1" ? "Attivo" : "Non Attivo")
    );
}

$row = $spreadsheet->getActiveSheet()->getHighestRow()+1;
$spreadsheet->getActiveSheet()->fromArray($dataAry, null, "A" . $row);

$spreadsheet->closeReport();
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 20/12/2018
 */

require(dirname(__FILE__, 4) . "/commonGetData.php");

$r_orari_negozio = $MSFrameworkCMS->getCMSData('working_hours')['hours'];

function getTodayWorkedHours($hours, $day_num) {
    global $r_orari_negozio;

    $hours = $hours[$day_num];

    if($hours['row_type'] == "dayoff") {
        return 0;
    }

    if($hours['row_type'] == "") {
        $hours = $r_orari_negozio[$day_num];
    }

    $to_return = 0;
    if($hours['am_in'] != "") {
        $in = $hours['am_in'];

        if ($hours['am_out'] != "") {
            $out = $hours['am_out'];
        } else {
            $out = $hours['pm_out'];
        }

        if($in != "" && $out != "") {
            $d1 = new DateTime(date("Y-m-d") . " " . $in . ":00");
            $d2 = new DateTime(date("Y-m-d") . " " . $out . ":00");

            $interval= $d1->diff($d2);
            $to_return += $interval->h;
        }
    }

    if($hours['pm_in'] != "") {
        $in = $hours['pm_in'];
        $out = $hours['pm_out'];

        if($in != "" && $out != "") {
            $d1 = new DateTime(date("Y-m-d") . " " . $in . ":00");
            $d2 = new DateTime(date("Y-m-d") . " " . $out . ":00");

            $interval= $d1->diff($d2);
            $to_return += $interval->h;
        }
    }

    return $to_return;
}

if($filters['just_active']['value'] == "1") {
    $subscribed_where = " AND active = 1 ";
}

if($filters['work_date_from']['value'] != "") {
    $startDate = new DateTime(date("Y-m-d", strtotime(str_replace("/", "-", $filters['work_date_from']['value']))));
    unset($filters['work_date_from']);
} else {
    $startDate = new DateTime();
    $startDate->modify('-7 days');
}

if($filters['work_date_to']['value'] != "") {
    $endDate = new DateTime(date("Y-m-d", strtotime(str_replace("/", "-", $filters['work_date_to']['value']))));
    unset($filters['work_date_to']);
} else {
    $endDate = new DateTime();
    $endDate->modify('+7 days');
}

$period = new DatePeriod($startDate, DateInterval::createFromDateString('1 day'), $endDate->modify('+1 day'));


$reportDataResult = $MSFrameworkDatabase->getAssoc("SELECT nome, cognome, active, email, ruolo, work_hours, id FROM users WHERE id != '' $where_str $subscribed_where $order_by_str", $where_ary);

$hoursResult = array();
foreach($reportDataResult as $r) {
    $work_hours = json_decode($r['work_hours'], true);

    foreach($period as $cur_date) {
        $found = false;
        if(array_key_exists('custom', $work_hours)) {
            foreach($work_hours['custom'] as $customData) {
                if($cur_date->format("d/m/Y") >= $customData['range']['from'] && $cur_date->format("d/m/Y") <= $customData['range']['to']) {
                    $hoursResult[$r['id']] += getTodayWorkedHours($customData['hours'], $cur_date->format( 'N')-1);
                    $found = true;
                    continue 2;
                }
            }
        }

        if(!$found) {
            $hoursResult[$r['id']] += getTodayWorkedHours($work_hours['default']['hours'], $cur_date->format( 'N')-1);
        }
    }
}

$reportData = array(
    'queryResult' => $reportDataResult,
    'groupData' => array(
        "work_hours" => $hoursResult,
    )
);

if(count($reportDataResult) == 0) {
    die('<div class="text-center">Nessun dato disponibile per i filtri impostati</div>');
}
?>
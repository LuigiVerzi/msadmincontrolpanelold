<?php
/**
 * MSAdminControlPanel
 * Date: 21/12/2018
 */

require_once('../../../../../../../../sw-config.php');
require_once("../../db/getData.php");

$pdf = new \MSFramework\Report\Output\PDF\pdf($_GET['p'], $_GET['i']);
$table = new \Interpid\PdfLib\Table($pdf);
$usersClass = new \MSFramework\users();

$pdf->openReport();

$table->initialize( [ $pdf->widthFromPercentage("35"), $pdf->widthFromPercentage("35"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10"), $pdf->widthFromPercentage("10") ] );
$table->addHeader(array(
    [ 'TEXT' => 'Nome e Cognome' ],
    [ 'TEXT' => 'Email' ],
    [ 'TEXT' => 'Livello' ],
    [ 'TEXT' => 'Attivo' ],
    [ 'TEXT' => 'Ore lavorate' ]
));

foreach($reportData['queryResult'] as $r) {
    $table->addRow(array(
        [ 'TEXT' => $r['nome'] . ' ' . $r['cognome'] ],
        [ 'TEXT' => $r['email'] ],
        [ 'TEXT' => $usersClass->getUserLevels($r['ruolo']) ],
        [ 'TEXT' => ($r['active'] == "1" ? "Attivo" : "Non Attivo")],
        [ 'TEXT' => $reportData['groupData']['work_hours'][$r['id']]],
    ));
}

$table->close();
$pdf->SetDefaultStyle();

$pdf->closeReport($_POST['output_type']);
?>

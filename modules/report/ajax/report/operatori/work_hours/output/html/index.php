<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

if(isset($_GET['standalone'])) {
    require_once('../../../../../../../../sw-config.php');
    require_once("../../db/getData.php");
}

$usersClass = new \MSFramework\users();
?>

<div class="row" style="max-height: 350px; overflow: auto;">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Nome e Cognome</th>
                    <th>Email</th>
                    <th>Livello</th>
                    <th>Attivo</th>
                    <th>Ore lavorate</th>
                </tr>
            </thead>

            <tbody>
            <?php
            foreach($reportData['queryResult'] as $r) {
            ?>
            <tr>
                <td>
                    <?= $r['nome'] ?> <?= $r['cognome'] ?>
                </td>
                <td>
                    <?= $r['email'] ?>
                </td>
                <td>
                    <?= $usersClass->getUserLevels($r['ruolo']) ?>
                </td>
                <td>
                    <?= ($r['active'] == "1" ? "Attivo" : "Non Attivo") ?>
                </td>
                <td>
                    <?= $reportData['groupData']['work_hours'][$r['id']] ?>
                </td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="5" style="text-align: right;">Totale risultati: <?= count($reportData['queryResult']) ?></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
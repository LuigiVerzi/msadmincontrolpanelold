function initForm() {
    initClassicTabsEditForm();
    manageWorldDataSelects();

    $('#destination_address').tagsInput({
        'height':'35px',
        'width':'100%',
        'interactive':true,
        'defaultText':'Aggiungi indirizzo email',
        'removeWithBackspace' : true,
        'onAddTag': function (value) {
            if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value)) {
                $('#destination_address').removeTag(value);
            }
        }
    });
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "datiFatturazione": {
                "rag_soc" : $('#rag_soc').val(),
                "piva" : $('#piva').val(),
                "cf" : $('#cf').val(),
                "geo_data": getGeoDataAryToSave(),
            },
            "pDestAddr": $('#destination_address').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            geoDataErrorsOnSave(data.status);

            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido!");
            } else if(data.status == "piva_and_cf_missing") {
                bootbox.error("E' necessario impostare almeno uno tra Partita IVA e Codice Fiscale");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                $('#exitAfterSave').val($('#exit_after_save:checked').length);

                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
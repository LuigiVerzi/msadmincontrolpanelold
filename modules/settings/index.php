<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('settings');

$fatturazione = json_decode($r['datiFatturazione'], true);
$r['geo_data'] = json_encode($fatturazione['geo_data']);
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Dati Fatturazione</h1>
                        <fieldset>
                            <h2 class="title-divider">Dati Fatturazione</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Ragione Sociale*</label>
                                    <input id="rag_soc" name="rag_soc" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['rag_soc']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Partita IVA*</label>
                                    <input id="piva" name="piva" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['piva']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Codice Fiscale*</label>
                                    <input id="cf" name="cf" type="text" class="form-control" value="<?php echo htmlentities($fatturazione['cf']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php require(ABSOLUTE_SW_PATH . "includes/template/geodata/geodata.php"); ?>

                        </fieldset>

                        <h1>Email</h1>
                        <fieldset>
                            <h2 class="title-divider">Dati Email Destinatari</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Indirizzi</label>
                                    <input id="destination_address" name="destination_address" type="text" class="form-control" value="<?php echo htmlentities($r['destination_address']) ?>">
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
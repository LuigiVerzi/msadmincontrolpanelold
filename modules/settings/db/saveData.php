<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;

if($_POST['datiFatturazione']['rag_soc'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if($_POST['datiFatturazione']['piva'] == "" && $_POST['datiFatturazione']['cf'] == "") {
    echo json_encode(array("status" => "piva_and_cf_missing"));
    die();
}

(new MSFramework\geonames())->checkDataBeforeSave(true, $_POST['datiFatturazione']['geo_data']);

$db_action = "update";
if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'settings'") == 0) {
    $db_action = "insert";
}

$array_to_save = array(
    "value" => json_encode(array(
        "datiFatturazione" => json_encode($_POST['datiFatturazione']),
        "destination_address" => $_POST['pDestAddr'],
    )),
    "type" => "settings"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'settings'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
<?php
/**
 * MSAdminControlPanel
 * Date: 03/06/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$settings_data = $MSFrameworkFW->getCMSData('settings');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Metodi di Pagamento</h1>
                        <?php
                        $payMethods = $settings_data['payMethods'];
                        ?>
                        <fieldset>
                            <div class="ibox <?php if($payMethods['bonifico']['enable'] != "1") { echo "collapsed"; } ?> webPaymentBox" data-group-name="bonifico">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -10px 10px 0 -3px;">
                                        <input type="checkbox" id="enable_bonifico" class="webPayments_settings" <?php if($payMethods['bonifico']['enable'] == "1") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Abilita Bonifico Bancario
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Nome Visualizzato</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($payMethods['bonifico']['display_name']) ?>"></i></span>
                                            <input id="nome_visualizzato_bonifico" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($payMethods['bonifico']['display_name'])) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Classe FontAwesome</label>
                                            <input id="fa_class_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payMethods['bonifico']['fa_class']) ?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Intestatario Conto</label>
                                            <input id="intestatario_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payMethods['bonifico']['pay_data']['intestatario']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Numero Conto</label>
                                            <input id="conto_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payMethods['bonifico']['pay_data']['conto']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Nome Banca</label>
                                            <input id="banca_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payMethods['bonifico']['pay_data']['banca']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Filiale</label>
                                            <input id="filiale_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payMethods['bonifico']['pay_data']['filiale']) ?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>IBAN</label>
                                            <input id="iban_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payMethods['bonifico']['pay_data']['iban']) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>BIC / Swift</label>
                                            <input id="bic_swift_bonifico" type="text" class="form-control" value="<?php echo htmlentities($payMethods['bonifico']['pay_data']['bic_swift']) ?>">
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($payMethods['bonifico']['descr_data']['descr']) ?>"></i></span>
                                            <textarea id="descr_bonifico" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($payMethods['bonifico']['descr_data']['descr']) ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Istruzioni</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($payMethods['bonifico']['descr_data']['istr']) ?>"></i></span>
                                            <textarea id="istr_bonifico" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($payMethods['bonifico']['descr_data']['istr']) ?></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="ibox <?php if($payMethods['paypal']['enable'] != "1") { echo "collapsed"; } ?> webPaymentBox" data-group-name="paypal">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -10px 10px 0 -3px;">
                                        <input type="checkbox" id="enable_paypal" class="webPayments_settings" <?php if($payMethods['paypal']['enable'] == "1") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Abilita PayPal
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Nome Visualizzato</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($payMethods['paypal']['display_name']) ?>"></i></span>
                                            <input id="nome_visualizzato_paypal" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($payMethods['paypal']['display_name'])) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Classe FontAwesome</label>
                                            <input id="fa_class_paypal" type="text" class="form-control" value="<?php echo htmlentities($payMethods['paypal']['fa_class']) ?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <?php
                                        foreach(array("sandbox", "produzione") as $env) {
                                        ?>
                                        <div class="col-lg-6">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Dati <b><?= ucfirst($env) ?></b></h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Client ID</label>
                                                            <input id="<?= $env ?>_paypal_client_id" type="text" class="form-control" value="<?php echo htmlentities($payMethods['paypal']['pay_data'][$env]['client_id']) ?>">
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <label>Secret</label>
                                                            <input id="<?= $env ?>_paypal_secret" type="text" class="form-control" value="<?php echo htmlentities($payMethods['paypal']['pay_data'][$env]['secret']) ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($payMethods['paypal']['descr_data']['descr']) ?>"></i></span>
                                            <textarea id="descr_paypal" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($payMethods['paypal']['descr_data']['descr']) ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Istruzioni</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($payMethods['paypal']['descr_data']['istr']) ?>"></i></span>
                                            <textarea id="istr_paypal" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($payMethods['paypal']['descr_data']['istr']) ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="ibox <?php if($payMethods['stripe']['enable'] != "1") { echo "collapsed"; } ?> webPaymentBox" data-group-name="stripe">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -10px 10px 0 -3px;">
                                        <input type="checkbox" id="enable_stripe" class="webPayments_settings" <?php if($payMethods['stripe']['enable'] == "1") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Abilita Stripe
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Nome Visualizzato</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($payMethods['stripe']['display_name']) ?>"></i></span>
                                            <input id="nome_visualizzato_stripe" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($payMethods['stripe']['display_name'])) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Classe FontAwesome</label>
                                            <input id="fa_class_stripe" type="text" class="form-control" value="<?php echo htmlentities($payMethods['stripe']['fa_class']) ?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <?php
                                        foreach(array("sandbox", "produzione") as $env) {
                                            ?>
                                            <div class="col-lg-6">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title">
                                                        <h5>Dati <b><?= ucfirst($env) ?></b></h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <label>Public Key</label>
                                                                <input id="<?= $env ?>_stripe_public" type="text" class="form-control" value="<?php echo htmlentities($payMethods['stripe']['pay_data'][$env]['public_key']) ?>">
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <label>Secret Key</label>
                                                                <input id="<?= $env ?>_stripe_secret" type="text" class="form-control" value="<?php echo htmlentities($payMethods['stripe']['pay_data'][$env]['secret_key']) ?>">
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <label>Webhook Getpayment Key</label>
                                                                <input id="<?= $env ?>_stripe_webhook_getpayment" type="text" class="form-control" value="<?php echo htmlentities($payMethods['stripe']['pay_data'][$env]['webhook_getpayment']) ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($payMethods['stripe']['descr_data']['descr']) ?>"></i></span>
                                            <textarea id="descr_stripe" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($payMethods['stripe']['descr_data']['descr']) ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Istruzioni</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($payMethods['stripe']['descr_data']['istr']) ?>"></i></span>
                                            <textarea id="istr_stripe" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($payMethods['stripe']['descr_data']['istr']) ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Fatturazione</h1>
                        <fieldset>
                            <div class="ibox-title">
                                Fatture In Cloud
                            </div>
                            <div class="ibox-content">
                                <?php
                                $invoices = $settings_data['invoices'];
                                ?>
                                <div class="row">
                                    <?php
                                    foreach(array("sandbox", "produzione") as $env) {
                                        ?>
                                        <div class="col-lg-6">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Dati <b><?= ucfirst($env) ?></b></h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>UID</label>
                                                            <input id="<?= $env ?>_fic_uid" type="text" class="form-control" value="<?php echo htmlentities($invoices['fatture_in_cloud'][$env]['uid']) ?>">
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <label>API Key</label>
                                                            <input id="<?= $env ?>_fic_api_key" type="text" class="form-control" value="<?php echo htmlentities($invoices['fatture_in_cloud'][$env]['api_key']) ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
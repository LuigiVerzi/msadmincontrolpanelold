<?php
/**
 * MSAdminControlPanel
 * Date: 01/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM `" . FRAMEWORK_DB_NAME . "`.`cms` WHERE type = 'settings'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
    $r_old_data = $MSFrameworkFW->getCMSData('settings');

    $old_metodi_pagamento = json_decode($r_old_data['payMethods'], true);
    $old_invoices = json_decode($r_old_data['invoices'], true);
}

if($_POST['payMethods']) {
    $payBonifico = $_POST['payMethods']['bonifico'];
    if($payBonifico['display_name'] == "") {
        echo json_encode(array("status" => "mandatory_bonifico"));
        die();
    }
    if($payBonifico['enable'] == "1") {
        foreach($payBonifico['pay_data'] as $value) {
            if($value == "") {
                echo json_encode(array("status" => "mandatory_bonifico"));
                die();
            }
        }
    }

    $payPayPal = $_POST['payMethods']['paypal'];
    if($payPayPal['display_name'] == "") {
        echo json_encode(array("status" => "mandatory_bonifico"));
        die();
    }
    if($payPayPal['enable'] == "1") {
        foreach($payPayPal['pay_data']['sandbox'] as $value) {
            if($value == "") {
                echo json_encode(array("status" => "mandatory_paypal"));
                die();
            }
        }

        foreach($payPayPal['pay_data']['produzione'] as $value) {
            if($value == "") {
                echo json_encode(array("status" => "mandatory_paypal"));
                die();
            }
        }
    }

    $payStripe = $_POST['payMethods']['stripe'];
    if($payStripe['display_name'] == "") {
        echo json_encode(array("status" => "mandatory_bonifico"));
        die();
    }
    if($payStripe['enable'] == "1") {
        foreach($payStripe['pay_data']['sandbox'] as $value) {
            if($value == "") {
                echo json_encode(array("status" => "mandatory_stripe"));
                die();
            }
        }

        foreach($payStripe['pay_data']['produzione'] as $value) {
            if($value == "") {
                echo json_encode(array("status" => "mandatory_stripe"));
                die();
            }
        }
    }

    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $_POST['payMethods']['bonifico']['display_name'], 'oldValue' => $old_metodi_pagamento['bonifico']['display_name']),
            array('currentValue' => $_POST['payMethods']['bonifico']['descr_data']['descr'], 'oldValue' => $old_metodi_pagamento['bonifico']['descr_data']['descr']),
            array('currentValue' => $_POST['payMethods']['bonifico']['descr_data']['istr'], 'oldValue' => $old_metodi_pagamento['bonifico']['descr_data']['istr']),
            array('currentValue' => $_POST['payMethods']['paypal']['display_name'], 'oldValue' => $old_metodi_pagamento['paypal']['display_name']),
            array('currentValue' => $_POST['payMethods']['paypal']['descr_data']['descr'], 'oldValue' => $old_metodi_pagamento['paypal']['descr_data']['descr']),
            array('currentValue' => $_POST['payMethods']['paypal']['descr_data']['istr'], 'oldValue' => $old_metodi_pagamento['paypal']['descr_data']['istr']),
            array('currentValue' => $_POST['payMethods']['stripe']['display_name'], 'oldValue' => $old_metodi_pagamento['stripe']['display_name']),
            array('currentValue' => $_POST['payMethods']['stripe']['descr_data']['descr'], 'oldValue' => $old_metodi_pagamento['stripe']['descr_data']['descr']),
            array('currentValue' => $_POST['payMethods']['stripe']['descr_data']['istr'], 'oldValue' => $old_metodi_pagamento['stripe']['descr_data']['istr']),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $_POST['payMethods']['bonifico']['display_name'] = $MSFrameworki18n->setFieldValue($old_metodi_pagamento['bonifico']['display_name'], $_POST['payMethods']['bonifico']['display_name']);
    $_POST['payMethods']['bonifico']['descr_data']['descr'] = $MSFrameworki18n->setFieldValue($old_metodi_pagamento['bonifico']['descr_data']['descr'], $_POST['payMethods']['bonifico']['descr_data']['descr']);
    $_POST['payMethods']['bonifico']['descr_data']['istr'] = $MSFrameworki18n->setFieldValue($old_metodi_pagamento['bonifico']['descr_data']['istr'], $_POST['payMethods']['bonifico']['descr_data']['istr']);

    $_POST['payMethods']['paypal']['display_name'] = $MSFrameworki18n->setFieldValue($old_metodi_pagamento['paypal']['display_name'], $_POST['payMethods']['paypal']['display_name']);
    $_POST['payMethods']['paypal']['descr_data']['descr'] = $MSFrameworki18n->setFieldValue($old_metodi_pagamento['paypal']['descr_data']['descr'], $_POST['payMethods']['paypal']['descr_data']['descr']);
    $_POST['payMethods']['paypal']['descr_data']['istr'] = $MSFrameworki18n->setFieldValue($old_metodi_pagamento['paypal']['descr_data']['istr'], $_POST['payMethods']['paypal']['descr_data']['istr']);

    $_POST['payMethods']['stripe']['display_name'] = $MSFrameworki18n->setFieldValue($old_metodi_pagamento['stripe']['display_name'], $_POST['payMethods']['stripe']['display_name']);
    $_POST['payMethods']['stripe']['descr_data']['descr'] = $MSFrameworki18n->setFieldValue($old_metodi_pagamento['stripe']['descr_data']['descr'], $_POST['payMethods']['stripe']['descr_data']['descr']);
    $_POST['payMethods']['stripe']['descr_data']['istr'] = $MSFrameworki18n->setFieldValue($old_metodi_pagamento['stripe']['descr_data']['istr'], $_POST['payMethods']['stripe']['descr_data']['istr']);
}

$array_to_save = array(
    "value" => json_encode(array(
        "payMethods" => $_POST['payMethods'],
        "invoices" => $_POST['invoices'],
    )),
    "type" => "settings"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`cms` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`cms` SET $stringForDB[1] WHERE type = 'settings'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
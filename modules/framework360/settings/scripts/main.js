function initForm() {
    initClassicTabsEditForm();

    $('.webPaymentBox .webPayments_settings').on('ifChanged', function (event) {
        if($(this).is(':checked')) {
            $(this).closest('.webPaymentBox').removeClass('collapsed');
        }
        else {
            $(this).closest('.webPaymentBox').addClass('collapsed');
        }
    });
}

function moduleSaveFunction() {
    web_payments_ary = {};
    $('.webPaymentBox').each(function() {
        if($(this).data('group-name') == "bonifico") {
            web_payments_ary[$(this).data('group-name')] = {
                "enable" : $('#enable_bonifico:checked').length,
                "display_name" : $('#nome_visualizzato_bonifico').val(),
                "fa_class" : $('#fa_class_bonifico').val(),
                "pay_data" : {
                    "intestatario" : $('#intestatario_bonifico').val(),
                    "conto" : $('#conto_bonifico').val(),
                    "banca" : $('#banca_bonifico').val(),
                    "filiale" : $('#filiale_bonifico').val(),
                    "iban" : $('#iban_bonifico').val(),
                    "bic_swift" : $('#bic_swift_bonifico').val(),
                },
                "descr_data" : {
                    "descr" : $('#descr_bonifico').val(),
                    "istr" : $('#istr_bonifico').val(),
                }
            }
        } else if($(this).data('group-name') == "paypal") {
            web_payments_ary[$(this).data('group-name')] = {
                "enable" : $('#enable_paypal:checked').length,
                "display_name" : $('#nome_visualizzato_paypal').val(),
                "fa_class" : $('#fa_class_paypal').val(),
                "pay_data" : {
                    "sandbox" : {
                        "client_id" : $('#sandbox_paypal_client_id').val(),
                        "secret" : $('#sandbox_paypal_secret').val(),
                    },
                    "produzione" : {
                        "client_id" : $('#produzione_paypal_client_id').val(),
                        "secret" : $('#produzione_paypal_secret').val(),
                    }
                },
                "descr_data" : {
                    "descr" : $('#descr_paypal').val(),
                    "istr" : $('#istr_paypal').val(),
                }
            }
        } else if($(this).data('group-name') == "stripe") {
            web_payments_ary[$(this).data('group-name')] = {
                "enable" : $('#enable_stripe:checked').length,
                "display_name" : $('#nome_visualizzato_stripe').val(),
                "fa_class" : $('#fa_class_stripe').val(),
                "pay_data" : {
                    "sandbox" : {
                        "public_key" : $('#sandbox_stripe_public').val(),
                        "secret_key" : $('#sandbox_stripe_secret').val(),
                        "webhook_getpayment" : $('#sandbox_stripe_webhook_getpayment').val(),
                    },
                    "produzione" : {
                        "public_key" : $('#produzione_stripe_public').val(),
                        "secret_key" : $('#produzione_stripe_secret').val(),
                        "webhook_getpayment" : $('#produzione_stripe_webhook_getpayment').val(),
                    }
                },
                "descr_data" : {
                    "descr" : $('#descr_stripe').val(),
                    "istr" : $('#istr_stripe').val(),
                }
            }
        }
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            'payMethods': web_payments_ary,
            'invoices': {
                "fatture_in_cloud" : {
                    "sandbox" : {
                        "uid" : $('#sandbox_fic_uid').val(),
                        "api_key" : $('#sandbox_fic_api_key').val(),
                    },
                    "produzione" : {
                        "uid" : $('#produzione_fic_uid').val(),
                        "api_key" : $('#produzione_fic_api_key').val(),
                    }
                }
            },
        },
        async: false,
        dataType: "json",
        success: function(data) {
            // CONTROLLI GENERALI
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            }  else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            }
            // CONTROLLI PAGAMENTO WEB
            else if(data.status == "mandatory_bonifico") {
                bootbox.error("Se abiliti il pagamento con Bonifico Bancario, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_contrassegno") {
                bootbox.error("Se abiliti il pagamento con Contrassegno, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_postepay") {
                bootbox.error("Se abiliti il pagamento con PostePay, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_paypal") {
                bootbox.error("Se abiliti il pagamento con PayPal, devi compilare tutti i dati della relativa scheda!");
            }  else if(data.status == "mandatory_nexi") {
                bootbox.error("Se abiliti il pagamento con Nexi, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "paypal_mail_not_valid") {
                bootbox.error("La mail inserita all'interno della scheda PayPal è in un formato non valido!");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
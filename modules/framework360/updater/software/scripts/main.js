function initForm() {
    $("#form").steps({
        bodyTag: "fieldset",
        enableFinishButton: false,
        enableCancelButton: false,
        stepsOrientation: "vertical",
        forceMoveForward: true,
        labels: {
            cancel: "Annulla",
            current: "step corrente:",
            pagination: "Paginazione",
            finish: "Fine",
            next: "Successivo",
            previous: "Precedente",
            loading: "Caricamento..."
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            $('#form .actions').show();
            initMSTooltip();

            if(currentIndex == 1) {
                $('.dual_select').bootstrapDualListbox({
                    selectorMinimalHeight: 160
                });

                $('.destination_container > div').on('click', function() {
                    if($(this).find('.widget').hasClass('disabled')) {
                        return false;
                    }

                    $('.destination_container .widget').removeClass('active');
                    $('.destination_container .widget').find('.fa-circle').removeClass('fas').addClass('far');

                    $(this).find('.widget').addClass('active');
                    $(this).find('.widget').find('.fa-circle').removeClass('far').addClass('fas');

                    if($('.staging_btn').hasClass('active')) {
                        $('.update_db').iCheck('enable');
                        $('.update_db').iCheck('check').iCheck('update');
                    } else {
                        $('.update_db').iCheck('uncheck').iCheck('update');
                        $('.update_db').iCheck('disable');
                    }
                })

                $('.dual_select').on('change', function() {
                    $('#fw360_production_select_from, #site_production_select_from').val('master').attr('disabled', 'disabled');

                    if($(this).val() == null || $(this).val().length == 0) {
                        $('.widget').addClass('disabled').removeClass('active');
                        $('.widget').find('.fa-circle').removeClass('fas').addClass('far');
                    } else if($(this).val().length >= 1) {
                        $('.widget').removeClass('disabled');

                        got_fw360 = 0;
                        if($(this).val().indexOf('fw360') != -1) {
                            got_fw360 = 1;
                            $('#fw360_production_select_from').attr('disabled', false);
                        }

                        if($(this).val().length-got_fw360 > 1) {
                            $('.staging_btn').addClass('disabled').removeClass('active');
                            $('.staging_btn').addClass('disabled').find('.fa-circle').removeClass('fas').addClass('far');
                        }

                        if($(this).val().length-got_fw360 == 1) {
                            $('#site_production_select_from').attr('disabled', false);
                        }
                    }

                    if($('.staging_btn').hasClass('disabled') || !$('.staging_btn').hasClass('active')) {
                        $('.update_db').iCheck('uncheck').iCheck('update');
                        $('.update_db').iCheck('disable');
                    } else {
                        $('.update_db').iCheck('enable');
                    }
                }).trigger('change');
            } else if(currentIndex == 2) {
                $('#form .actions').hide();

                var sites = [];
                $.each($('.dual_select').val(), function(k, id) {
                    sites.push({
                        "id" : id,
                        "name" : $('.dual_select option[value="' + id + '"]').html()
                    })
                });

                data = {
                    dest: $('.destination_container').find('.widget.active').data('dest'),
                    options: {
                        prod: {
                            "fw360_from" : $('#fw360_production_select_from').val(),
                            "site_from" : $('#site_production_select_from').val(),
                        },
                        staging: {
                            "update_db" : $('#update_db:checked').length,
                        }
                    },
                    sites: sites
                }

                var installed_procs = 0;
                var total_procs = 0;

                proc_list_html = "";
                proc_list_html += '<div id="clean_tmp_step" class="row"><div class="col-sm-6" style="text-align: right"><b>Pulizia archivi</b></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';

                $.each(data.sites, function(k, siteObj) {
                    proc_list_html += '<div id="site_' + siteObj.id + '" class="row"><div class="col-sm-6" style="text-align: right"><b>' + siteObj.name + '</b></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';
                    total_procs += 1;
                })

                $('#procedures_list').html(proc_list_html);

                total_procs += 1; //pulizia TMP

                $('#packages_to_install').html(total_procs);

                setTimeout(function () {
                    $.ajax({
                        url: "ajax/cleanTMP.php",
                        async: false,
                        dataType: "json",
                        success: function (data) {
                            installed_procs++;
                            $('#clean_tmp_step').find('.status').removeClass('text-warning').addClass('text-info').html('Completato.');
                            updateProgressBar(installed_procs, total_procs);
                        }
                    })

                    $.each(sites, function(k, siteObj) {
                        var cur_site_id = siteObj.id;

                        $.ajax({
                            url: "ajax/updateSite.php",
                            async: false,
                            dataType: "json",
                            data: {
                                "site" : siteObj,
                                "config" : data,
                            },
                            method: "POST",
                            success: function (data) {
                                installed_procs++;
                                $('#site_' + cur_site_id).find('.status').removeClass('text-warning').addClass('text-info').html('Completato.');
                                updateProgressBar(installed_procs, total_procs);
                            }
                        })
                    })

                    //todo: gestione errori
                    progress_bar_status = "progress-bar-default";
                    global_upd_status_str = "Aggiornamento completato con successo.";
                    $('.progress').removeClass('progress-striped').find('.progress-bar').removeClass('progress-bar-success').addClass(progress_bar_status);
                    $('#global_upd_status').html(global_upd_status_str);
                    $('#form .actions').show();

                }, 500);
            }
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            if(currentIndex == 1) {
                if($('.destination_container .active').length == 0) {
                    bootbox.error("Devi prima selezionare una destinazione per l'aggiornamento!");
                    return false;
                }
            }

            return true;
        },
        onInit: function (event, currentIndex) {
            $("#form").fadeIn();
            if($("#form > .steps li").length > 1) {
                $("#form > .steps").show();
            }
        }
    });
}

function enableLogDownload() {
    $('#downloadLogContainer').show();

    $('#downloadLog').unbind('click');
    $('#downloadLog').on('click', function() {
        window.location = 'ajax/downloadLogs.php?version=' + $('#final_sw_version').val();
    })
}

function updateProgressBar(installed_procs, total_procs) {
    $('#installed_packages').html(installed_procs);
    percentage = (installed_procs/total_procs)*100;
    $('.progress-bar').css('width', percentage+'%').attr('aria-valuenow', percentage);
}
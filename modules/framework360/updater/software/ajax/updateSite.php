<?php
/**
 * MSAdminControlPanel
 * Date: 2019-06-20
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$dest = $_POST['config']['dest'];
$options = $_POST['config']['options'][$dest];
$site_id = $_POST['site']['id'];

$debug = false;
$document_root = ($debug ? MS_DOCUMENT_ROOT . "/" : MS_PRODUCTION_DOCUMENT_ROOT);

$copy_from = "git/master";
if($site_id != "fw360") {
    $site_details = $MSFrameworkDatabase->getAssoc("SELECT customer_domain, git_name, customer_database FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE id = :id", array(":id" => $site_id), true);

    $destination_path = $document_root . ($dest == "staging" ? STAGING_DOMAIN : $site_details['customer_domain']) . "/";
    $production_path = $document_root . $site_details['customer_domain'] . "/";
} else if($site_id == "fw360") {
    $site_details = array(
        "customer_domain" => ADMIN_FOLDER,
        "git_name" => "msadmincontrolpanel",
    );

    $destination_path = $document_root . ADMIN_FOLDER_NAME . "/v" . ($dest == "staging" ? "Staging" : FRAMEWORK_VERSION) . "/";
    $production_path = $document_root . ADMIN_FOLDER_NAME . "/v" . FRAMEWORK_VERSION . "/";
}

if($site_id == "fw360" && $options['fw360_from'] == "staging" && $dest == "prod") { //sto mandando in produzione il framework copiandolo da staging
    $gitFolder = $document_root . ADMIN_FOLDER_NAME . "/vStaging/";
    $cleanGitFolder = false;
} else if($site_id != "fw360" && $options['site_from'] == "staging" && $dest == "prod") { //sto mandando in produzione un sito copiandolo da staging
    $gitFolder = $document_root . STAGING_DOMAIN . "/";
    $cleanGitFolder = false;
} else {
    //copio il sito/fw in una cartella temporanea
    $gitFolder = PATH_TO_TMP . "GitFolder/";

    mkdir($gitFolder, 0777, true);
    shell_exec("rm -rf " . $gitFolder . "*");
    shell_exec("rm -rf " . $gitFolder . ".*");
    shell_exec("cd " . $gitFolder . " && git clone --depth=1 --branch=master git@bitbucket.org:marketingstudiomainteam/" .  $site_details['git_name'] . ".git .");
    shell_exec("rm -rf " . $gitFolder . "/.gitignore");
    shell_exec("rm -rf " . $gitFolder . "/.git/");

    //copio i contenuti da non perdere (uploads e i18n) dalla cartella di destinazione (se il sito va in produzione)/dalla produzione (se il sito va in staging) all'interno della cartella temporanea
    if($site_id != "fw360") {
        shell_exec("cp -r " . ($dest == "prod" ? $destination_path : $production_path) . "uploads " . $gitFolder . "/");
        shell_exec("cp -r " . ($dest == "prod" ? $destination_path : $production_path) . "i18n " . $gitFolder . "/");
    }

    $cleanGitFolder = true;
}

//pulisco la cartella di destinazione
shell_exec("rm -rf " . $destination_path . "*");
shell_exec("rm -rf " . $destination_path . ".*");

//copio il contenuto della cartella temporanea nella destinazione finale
shell_exec("cp -r " . $gitFolder . "* " . $destination_path);
shell_exec("cp -r " . $gitFolder . ".[^.]* " . $destination_path);

//elimino alcuni files non ammessi
shell_exec("rm $destination_path" . "robots.txt"); //è gestito dalle impostazioni di FW360, non dal file

if($cleanGitFolder) {
    shell_exec("rm -rf " . $gitFolder . "*");
    shell_exec("rm -rf " . $gitFolder . ".*");
}

//copio il db di produzione del sito all'interno del db di staging
if($site_id != "fw360" && $dest == "staging" && $options['update_db'] == "1") {
    $db = $site_details['customer_database'];
    $stagingDB = $MSFrameworkDatabase->getAssoc("SELECT customer_database FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE customer_domain = :customer_domain", array(":customer_domain" => STAGING_DOMAIN), true);
    shell_exec('mysqldump ' . $db . ' --password=' . FRAMEWORK_DB_PASS . ' --user=' . FRAMEWORK_DB_USER . ' | mysql -u ' . FRAMEWORK_DB_USER . ' -p' . FRAMEWORK_DB_PASS . ' -h ' . FRAMEWORK_DB_HOST . ' ' . $stagingDB['customer_database']);
    $MSFrameworkDatabase->flushCacheByNameSpace("", STAGING_DOMAIN);
}

if($site_id == "fw360" && $dest == "prod") {
    //quando aggiorno il fw in produzione, aggiorno anche il numero versione (creo una nuova repo git temporanea sul server perchè è più facile fare il commit così rispetto alle API)
    shell_exec("rm -rf " . $gitFolder . "*");
    shell_exec("rm -rf " . $gitFolder . ".*");
    shell_exec("cd " . $gitFolder . " && git clone --depth=1 --branch=master git@bitbucket.org:marketingstudiomainteam/" .  $site_details['git_name'] . ".git .");

    $info_json = json_decode(file_get_contents($gitFolder . "/info.json"), true);
    $version_ary = explode(".", $info_json['version']);
    $version_ary[count($version_ary)-1]++;
    $info_json['version'] = implode(".", $version_ary);
    file_put_contents($gitFolder . "/info.json", json_encode($info_json));

    shell_exec("cd " . $gitFolder . " && git checkout master");
    shell_exec("cd " . $gitFolder . " && git add info.json");
    shell_exec("cd " . $gitFolder . " && git config user.name \"" . PRODUCER_NAME . "\"");
    shell_exec("cd " . $gitFolder . " && git config user.email \"" . PRODUCER_EMAIL . "\"");
    shell_exec("cd " . $gitFolder . " && git commit -m \"Aggiornato numero versione\"");
    shell_exec("cd " . $gitFolder . " && git push origin master");

    shell_exec("rm -rf " . $gitFolder . "*");
    shell_exec("rm -rf " . $gitFolder . ".*");

    // Sistemo i permessi dei file che necessitano specifici permessi per funzionare
    chmod($destination_path . "MSFramework/cron/Framework/ticket/syncWithMail.php",0755);
}

shell_exec('echo flush_all > /dev/tcp/127.0.0.1/20128'); //pulisco memcached

echo json_encode(array('status' => 'ok'));
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 09/09/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('producer_config');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">

            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Introduzione</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-12 m-b-sm">
                                    Questa procedura ti guiderà durante il processo di aggiornamento dei software (Framework360 / Siti web), consentendoti di verificare i dati ed eseguire alcune operazioni preliminari.
                                </div>

                                <div class="col-md-12">
                                    Al termine della procedura, verifica la necessità di applicare procedure di aggiornamento al database utilizzando <a href="../database">l'apposito modulo</a>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Selezione Software</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="ibox">
                                        <div class="ibox-content">
                                            <p>
                                                Seleziona i Siti / SW da aggiornare
                                            </p>

                                            <select class="form-control dual_select" multiple>
                                                <option value="fw360">[FW360] - Framework 360</option>
                                                <?php
                                                foreach($MSFrameworkDatabase->getAssoc("SELECT id, customer_domain, git_name FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE git_name != '' ORDER BY customer_domain ASC") as $customer) {
                                                    ?>
                                                    <option value="<?= $customer['id'] ?>">[SITO] - <?= $customer['customer_domain'] ?> (<?= $customer['git_name'] ?>.git)</option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row destination_container">
                                <div class="col-lg-6">
                                    <div class="widget staging_btn style1" data-dest="staging">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <i class="far fa-circle fa-5x"></i>
                                            </div>
                                            <div class="col-xs-8 text-right">
                                                <span>I SW selezionati verranno installati in</span>
                                                <h2 class="font-bold">Staging</h2>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>&#8205;</label>
                                                <div class="styled-checkbox form-control">
                                                    <label style="width: 100%;">
                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                            <input type="checkbox" id="update_db">
                                                            <i></i>
                                                        </div>
                                                        <span style="font-weight: normal;">Aggiorna database (dove applicabile)</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="widget prod_btn style1" data-dest="prod">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <i class="far fa-circle fa-5x"></i>
                                            </div>
                                            <div class="col-xs-8 text-right">
                                                <span>I SW selezionati verranno installati in</span>
                                                <h2 class="font-bold">Produzione</h2>
                                            </div>
                                        </div>
                                        <div class="row" id="production_select_container">
                                            <div class="col-sm-6">
                                                <label>Provenienza FW360</label>
                                                <select class="form-control" id="fw360_production_select_from">
                                                    <option value="staging">Copia da Staging</option>
                                                    <option value="master">Copia da git/master</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-6">
                                                <label>Provenienza Sito</label>
                                                <select class="form-control" id="site_production_select_from">
                                                    <option value="staging">Copia da Staging</option>
                                                    <option value="master">Copia da git/master</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Installazione</h1>
                        <fieldset>
                            <div id="upd_status" class="row text-center">
                                <div class="col-md-12 m-b-sm" id="global_upd_status">Avvio dell'aggiornamento in corso: <b>non chiudere questa finestra!</b></div>
                                <div class="col-md-12">Procedure completate <span id="installed_packages">0</span>/<span id="packages_to_install">0</span></div>
                                <div class="col-md-12 m-b-sm">
                                    <div class="progress progress-striped active">
                                        <div style="width: 10%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-bar progress-bar-success">
                                            <span class="sr-only">0% Completato</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 m-b-sm" id="procedures_list" style="line-height: 28px; max-height: 400px; overflow-y: auto; overflow-x: hidden; border: 1px solid #dedede; padding: 20px 0;">
                                    Recupero dati in corso...
                                </div>
                            </div>
                        </fieldset>

                        <h1>Fine</h1>
                        <fieldset>
                            <div class="row text-center">
                                <div class="col-md-12 m-b-md">La procedura di aggiornamento è terminata!</div>
                                <div class="col-md-12 m-b-sm" id="downloadBackupContainer" style="display: none;">
                                    <a class="btn btn-primary" id="downloadBackup">Scarica il backup dei database</a>
                                </div>
                                <div class="col-md-12 m-b-sm" id="downloadLogContainer" style="display: none;">
                                    <a class="btn btn-primary" id="downloadLog">Scarica i log</a>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
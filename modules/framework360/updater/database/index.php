<?php
/**
 * MSAdminControlPanel
 * Date: 09/09/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('producer_config');

$dbUpdater = new \MSFramework\Updater\dbUpdater();
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">

            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Introduzione</h1>
                        <fieldset>
                            <?php
                            if($dbUpdater->simulateProduction) {
                            ?>
                            <h4 class="red-bg text-center" style="padding: 20px;">Attenzione! La modalità di simulazione ambiente di produzione è attiva! Procedendo, verranno aggiornati i DB di produzione anche se ci si trova in Sviluppo/Staging. Per disabilitare questa modalità, impostare la variabile "simulateProduction" della classe "dbUpdater" su "false".</h4>
                            <?php } ?>

                            <div class="row">
                                <div class="col-md-12 m-b-sm">
                                    Questa procedura ti guiderà durante il processo di aggiornamento del database, consentendoti di verificare i dati ed eseguire alcune operazioni preliminari.
                                </div>

                                <div class="col-md-12">
                                    Al termine della procedura, i database saranno aggiornati alla versione <b><?= SW_VERSION ?></b>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Database</h1>
                        <fieldset>
                            <input type="hidden" id="final_sw_version" value="<?= SW_VERSION ?>" />

                            <div style="max-height: 400px; overflow-y: auto; overflow-x: hidden;">
                                <?php
                                $env_dbs = $dbUpdater->getEnvolvedProcedures();

                                if(count($env_dbs['ready']) != 0 || count($env_dbs['error']) != 0) {
                                    if(count($env_dbs['info']) != 0) {
                                        ?>
                                        <h4 class="blue-bg text-center" style="padding: 20px;">Alcune procedure hanno delle informazioni importanti da verificare prima di essere applicate.</h4>
                                        <?php
                                        foreach($env_dbs['info'] as $version => $infoData) {
                                            foreach($infoData as $procName => $info) {
                                                ?>
                                                <div class="row m-b-md">
                                                    <div class="col-md-3 text-center">
                                                        <div style="padding: 10px; border: 1px solid cornflowerblue;">
                                                            ver. <?= $version ?> <br /> <?= $procName ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-9 text-center">
                                                        <div style="padding: 10px; border: 1px solid cornflowerblue;">
                                                            <?= $info ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }

                                    if(count($env_dbs['error']) != 0) {
                                    ?>
                                        <h4 class="red-bg text-center" style="padding: 20px;">I seguenti database non possono essere aggiornati a causa di un errore. Verificare la presenza della tabella <i>_db_info</i> e la correttezza dei dati inseriti.</h4>
                                    <?php
                                        foreach($env_dbs['error'] as $envolvedDBName) {
                                            ?>
                                            <div class="row m-b-md">
                                                <div class="col-md-12 text-center">
                                                    <div style="padding: 10px; border: 1px solid red;">
                                                        <div><?= $envolvedDBName ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }

                                    if(count($env_dbs['ready']) != 0) {
                                    ?>
                                        <h4 class="navy-bg text-center" style="padding: 20px;">I seguenti database saranno aggiornati alla versione indicata.</h4>
                                    <?php
                                    }

                                    foreach ($env_dbs['ready'] as $envolvedDBName => $versionData) {
                                        $total_proc_to_install = 0;

                                        $str_tooltip = "";
                                        foreach($versionData as $an_version => $an_folders) {
                                            $str_tooltip .= "versione " . $an_version . " (" . implode(", ", $an_folders) . ") <br /> ";
                                            $total_proc_to_install += count($an_folders);
                                        }

                                        $str_tooltip = substr($str_tooltip, 0, -8);
                                        ?>
                                        <div class="row m-b-md">
                                            <div class="col-md-5 text-center">
                                                <div style="padding: 10px; border: 1px solid gray;">
                                                    <div><?= $envolvedDBName ?></div>
                                                    <div class="small">ver. <?= $dbUpdater->getDBVersion($envolvedDBName) ?></div>
                                                </div>
                                            </div>

                                            <div class="col-md-2 text-center">
                                                <div>
                                                    <?= $total_proc_to_install ?> procedur<?= ($total_proc_to_install == 1 ? "a" : "e") ?> da installare
                                                </div>
                                                <div>
                                                    <span class="ms-label-tooltip" style="float: none;"><i class="fa fa-question-circle" data-html="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= $str_tooltip ?>"></i></span>
                                                </div>
                                            </div>

                                            <div class="col-md-5 text-center">
                                                <div style="padding: 10px; border: 1px solid green;">
                                                    <div><?= $envolvedDBName ?></div>
                                                    <div class="small">ver. <?= SW_VERSION ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>

                            <?php
                            if(count($env_dbs['ready']) != 0) {
                            ?>
                                <div class="row m-b-md">
                                    <div class="col-sm-6">
                                        <label style="font-weight: normal;">&nbsp;</label>
                                        <div class="styled-checkbox form-control">
                                            <label style="width: 100%;">
                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                    <input type="checkbox" id="do_backup" class="do_backup" checked <?php if((new \MSFramework\users())->getUserDataFromSession('userlevel') != "0") { echo "disabled"; } ?>>
                                                    <i></i>
                                                </div>
                                                <span style="font-weight: normal;">Effettua backup dei database coinvolti</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>


                            <?php
                            } else {
                            ?>
                            <div class="row m-b-md">
                                <div class="col-md-12 text-center">
                                    <h4 class="navy-bg" style="padding: 20px;">Tutti i database sono aggiornati. Non è necessario avviare alcuna procedura.</h4>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                        </fieldset>

                        <?php
                        if(count($env_dbs['ready']) != 0) {
                        ?>
                        <h1>Installazione</h1>
                        <fieldset>
                            <div id="upd_status" class="row text-center">
                                <div class="col-md-12 m-b-sm" id="global_upd_status">Avvio dell'aggiornamento in corso: <b>non chiudere questa finestra!</b></div>
                                <div class="col-md-12">Procedure completate <span id="installed_packages">0</span>/<span id="packages_to_install">0</span></div>
                                <div class="col-md-12 m-b-sm">
                                    <div class="progress progress-striped active">
                                        <div style="width: 10%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-bar progress-bar-success">
                                            <span class="sr-only">0% Completato</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 m-b-sm" id="procedures_list" style="line-height: 28px; max-height: 400px; overflow-y: auto; overflow-x: hidden; border: 1px solid #dedede; padding: 20px 0;">
                                    Recupero dati in corso...
                                </div>
                            </div>
                        </fieldset>

                        <h1>Fine</h1>
                        <fieldset>
                            <div class="row text-center">
                                <div class="col-md-12 m-b-md">La procedura di aggiornamento è terminata!</div>
                                <div class="col-md-12 m-b-sm" id="downloadBackupContainer" style="display: none;">
                                    <a class="btn btn-primary" id="downloadBackup">Scarica il backup dei database</a>
                                </div>
                                <div class="col-md-12 m-b-sm" id="downloadLogContainer" style="display: none;">
                                    <a class="btn btn-primary" id="downloadLog">Scarica i log</a>
                                </div>
                            </div>
                        </fieldset>
                        <?php
                        }
                        ?>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
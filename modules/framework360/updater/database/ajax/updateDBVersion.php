<?php
/**
 * MSAdminControlPanel
 * Date: 11/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$updater = new \MSFramework\Updater\dbUpdater();

foreach($updater->getEnvolvedProcedures()['ready'] as $env_proc => $env_proc_data) {
    end($env_proc_data);
    $max_version = key($env_proc_data);

    $updater->setDBVersion($max_version, $env_proc);
}
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 20/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

shell_exec("rm -R " . PATH_TO_SQL_BACKUP . 'db_updater/');
shell_exec("rm -R " . $updater->getLogPath());

echo json_encode(array());
?>

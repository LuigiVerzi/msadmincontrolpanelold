<?php
/**
 * MSAdminControlPanel
 * Date: 11/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$updater = new \MSFramework\Updater\dbUpdater();

$folder_name = date('d-m-Y_H-i-s');
$updater_sql_path = PATH_TO_SQL_BACKUP . 'db_updater/';
$backup_path = $updater_sql_path . $folder_name . "/";

mkdir($backup_path, 0777, true);

foreach($updater->getEnvolvedProcedures()['ready'] as $db => $dbInfo) {
    $backup_filename = "sql_" . $db . "_" . date('d-m-Y_H-i-s');
    shell_exec('mysqldump ' . $db . ' --password=' . FRAMEWORK_DB_PASS . ' --user=' . FRAMEWORK_DB_USER . ' > ' . $backup_path . $backup_filename . '.sql');
    shell_exec('tar -czf ' . $backup_path . $backup_filename . '.tar.gz -C ' . $backup_path . ' ' . $backup_filename . '.sql');
    shell_exec('rm ' . $backup_path . $backup_filename . '.sql');
}

shell_exec('tar -czf ' . $updater_sql_path . 'sql_' . $folder_name . '.tar.gz  -C ' . $backup_path . " .");
shell_exec('rm -R ' . $backup_path);

echo json_encode(array(
    "archiveName" => 'sql_' . $folder_name . '.tar.gz',
    "archivePath" => PATH_TO_SQL_BACKUP . 'db_updater/sql_' . $folder_name . '.tar.gz',
    "archiveHTMLPath" => PATH_TO_SQL_BACKUP_HTML . 'db_updater/sql_' . $folder_name . '.tar.gz',
));
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 09/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$updater = new \MSFramework\Updater\dbUpdater();

$procedures = $updater->getEnvolvedProcedures()['ready'];

echo json_encode(array("packages" => $procedures));
?>

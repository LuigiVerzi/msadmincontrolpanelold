<?php
/**
 * MSAdminControlPanel
 * Date: 20/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$log_path = PATH_TO_LOGS . 'db_updater/' . $_GET['version'] . "/";

$folder_path = MS_DOCUMENT_ROOT . "/" . ADMIN_FOLDER . '/tmp/db_updater/';
$file = $folder_path . 'log_' . $_GET['version'] . '.tar.gz';
mkdir($folder_path, 0777, true);

shell_exec('tar -czf ' . $file . ' -C ' . $log_path . "/ .");

$quoted = sprintf('"%s"', addcslashes(basename($file), '"\\'));
$size   = filesize($file);

header('Content-Description: Download Log');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . $quoted);
header('Content-Transfer-Encoding: binary');
header('Connection: Keep-Alive');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . $size);
readfile($file);
?>

function initForm() {
    $("#form").steps({
        bodyTag: "fieldset",
        enableFinishButton: false,
        enableCancelButton: false,
        stepsOrientation: "vertical",
        forceMoveForward: true,
        labels: {
            cancel: "Annulla",
            current: "step corrente:",
            pagination: "Paginazione",
            finish: "Fine",
            next: "Successivo",
            previous: "Precedente",
            loading: "Caricamento..."
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            $('#form .actions').show();
            initMSTooltip();

            if(currentIndex == 2) {
                $('#form .actions').hide();

                $('#upd_status').append('');

                $.ajax({
                    url: "ajax/startDBUpdater.php",
                    async: false,
                    dataType: "json",
                    success: function(data) {
                        var installed_procs = 0;
                        var total_procs = 0;
                        var has_errors = false;
                        var has_infos = false;
                        var has_warnings = false;

                        proc_list_html = "";
                        proc_list_html += '<div id="clean_tmp_step" class="row"><div class="col-sm-6" style="text-align: right"><b>Pulizia archivi</b></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';

                        if($('#do_backup:checked').length == 1) {
                            proc_list_html += '<div id="db_backup_step" class="row"><div class="col-sm-6" style="text-align: right"><b>Backup Database</b></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';
                        }

                        grouped_updates = new Object();
                        $.each(data.packages, function(dbname, versionK) {
                            $.each(versionK, function(version, procedure_folderK) {
                                if(!(version in grouped_updates)) {
                                    grouped_updates[version] = new Object();
                                }

                                $.each(procedure_folderK, function(procedure_folderx, procedure_folder) {
                                    if(!(procedure_folder in grouped_updates[version])) {
                                        grouped_updates[version][procedure_folder] = new Array();
                                    }

                                    grouped_updates[version][procedure_folder].push(dbname);
                                })
                            })
                        })

                        $.each(grouped_updates, function(version, procedure_folderK) {
                            $.each(procedure_folderK, function(procedure_folder, databases) {
                                proc_list_html += '<div id="' + version.replace(/\./g, "") + '_' + procedure_folder + '" class="row"><div class="col-sm-6" style="text-align: right"><b>' + procedure_folder + '</b> <span class="ms-label-tooltip" style="float: none;"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="versione ' + version + ', ' + databases.length + ' database coinvolt' + (databases.length == 1 ? 'o' : 'i') + '"></i></span></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';
                                total_procs += 1;
                            })
                        })

                        proc_list_html += '<div id="version_update_step" class="row"><div class="col-sm-6" style="text-align: right"><b>Aggiornamento Versione Database</b></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';

                        $('#procedures_list').html(proc_list_html);

                        total_procs += $('#do_backup:checked').length;
                        total_procs += 1; //aggiornamento versioni db
                        total_procs += 1; //pulizia TMP

                        $('#packages_to_install').html(total_procs);

                        initMSTooltip();

                        setTimeout(function () {
                            $.ajax({
                                url: "ajax/cleanTMP.php",
                                async: false,
                                dataType: "json",
                                success: function(data) {
                                    installed_procs++;
                                    $('#clean_tmp_step').find('.status').removeClass('text-warning').addClass('text-info').html('Completato.');
                                    updateProgressBar(installed_procs, total_procs);
                                }
                            })

                            if($('#do_backup:checked').length == 1) {
                                $.ajax({
                                    url: "ajax/doBackup.php",
                                    async: false,
                                    dataType: "json",
                                    success: function(data) {
                                        installed_procs++;
                                        $('#db_backup_step').find('.status').removeClass('text-warning').addClass('text-info').html('Completato.');
                                        updateProgressBar(installed_procs, total_procs);
                                        $('#downloadBackupContainer').show();
                                        $('#downloadBackup').attr('href', data.archiveHTMLPath);
                                    }
                                })
                            }

                            $.each(grouped_updates, function(version, procedure_folderK) {
                                $.each(procedure_folderK, function(procedure_folder, databases) {
                                    $.ajax({
                                        url: $('#baseElementPathAdmin').val() + "MSFramework/frameworkUpdates/updates/" + version + "/" + procedure_folder + "/index.php",
                                        async: false,
                                        method: "POST",
                                        data: {
                                            "version": version,
                                            "procedure_folder": procedure_folder,
                                        },
                                        dataType: "json",
                                        success: function(data) {
                                            installed_procs++;

                                            str_info = "";
                                            text_class = 'text-info';
                                            if(data.got_infos.length > 0) {
                                                str_info = " Verificare log (info).";
                                                text_class = 'text-success';
                                                has_infos = true;
                                            }

                                            if(data.got_warnings.length > 0) {
                                                str_info = " Verificare log (warning).";
                                                text_class = 'text-success';
                                                has_warnings = true;
                                            }

                                            if(data.got_errors.indexOf(true) > -1) { //se nell'array data.results è presente almeno un "true" vuol dire che c'è stato un errore da qualche parte (salvato nei log)
                                                has_errors = true;
                                                $('#' + version.replace(/\./g, "") + '_' + procedure_folder).find('.status').removeClass('text-warning').addClass('text-danger').html('Errore. Verificare log.');
                                            } else {
                                                $('#' + version.replace(/\./g, "") + '_' + procedure_folder).find('.status').removeClass('text-warning').addClass(text_class).html('Completato.' + str_info);
                                            }

                                            updateProgressBar(installed_procs, total_procs);
                                        },
                                        error: function(jqXHR, exception) {
                                            has_errors = true;
                                            installed_procs++;
                                            $('#' + version.replace(/\./g, "") + '_' + procedure_folder).find('.status').removeClass('text-warning').addClass('text-danger').html('Errore ' + jqXHR.status);
                                            updateProgressBar(installed_procs, total_procs);
                                        }
                                    })
                                })
                            })

                            $.ajax({
                                url: "ajax/updateDBVersion.php",
                                async: false,
                                method: "POST",
                                success: function(data) {
                                    installed_procs++;
                                    updateProgressBar(installed_procs, total_procs);
                                    $('#version_update_step').find('.status').removeClass('text-warning').addClass('text-info').html('Completato.');

                                    progress_bar_status = "progress-bar-default";
                                    global_upd_status_str = "Aggiornamento completato con successo.";

                                    if(has_warnings) {
                                        progress_bar_status = "progress-bar-warning";
                                        global_upd_status_str = "Aggiornamento completato con avvertenze da controllare. Verificare i dettagli di seguito ed all'interno dei files di log";
                                    }

                                    if(has_errors) {
                                        progress_bar_status = "progress-bar-danger";
                                        global_upd_status_str = "Aggiornamento completato con errori. Verificare i dettagli di seguito ed all'interno dei files di log";
                                    }

                                    if(has_infos || has_warnings || has_errors) {
                                        enableLogDownload();
                                    }

                                    $('.progress').removeClass('progress-striped').find('.progress-bar').removeClass('progress-bar-success').addClass(progress_bar_status);

                                    $('#global_upd_status').html(global_upd_status_str);
                                    $('#form .actions').show();
                                }
                            })
                        }, 500);
                    }
                })
            }
        },
        onInit: function (event, currentIndex) {
            $("#form").fadeIn();
            if($("#form > .steps li").length > 1) {
                $("#form > .steps").show();
            }
        }
    });
}

function enableLogDownload() {
    $('#downloadLogContainer').show();

    $('#downloadLog').unbind('click');
    $('#downloadLog').on('click', function() {
        window.location = 'ajax/downloadLogs.php?version=' + $('#final_sw_version').val();
    })
}

function updateProgressBar(installed_procs, total_procs) {
    $('#installed_packages').html(installed_procs);
    percentage = (installed_procs/total_procs)*100;
    $('.progress-bar').css('width', percentage+'%').attr('aria-valuenow', percentage);
}
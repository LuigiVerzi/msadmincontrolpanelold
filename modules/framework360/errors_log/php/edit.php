<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.errors__php WHERE id = :id", array($_GET['id']), true);
}
if(!$r) header('Location: index.php');

$client_info = json_decode($r['client_info'], true);
$error_log = json_decode($r['error_log'], true);

$priority_info = array(
    '0' => 'inverse',
    '3' => 'warning',
    '10' => 'danger'
);

$priority_color = '';
foreach($priority_info as $priority => $color) {
    if($r['priority'] > (int)$priority) $priority_color = $color;
}

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Riepilogo Errore</h2>
                            <div class="row">
                                <div class="col-sm-1">
                                    <div style="margin-bottom: 15px;"><?= '<span class="label label-' . $priority_color . '" style="padding: 0;width: 100%; line-height: 50px; margin: auto; display: block;">' . $r['priority'] . '</span>'; ?></div>
                                </div>

                                <div class="col-sm-2">
                                    <label>Sito web</label>
                                    <div style="margin-bottom: 15px;"><a href="//<?php echo $r['website'] ?>" target="_blank"><?php echo $r['website'] ?></a></div>
                                </div>

                                <div class="col-sm-5">
                                    <label>URL errore</label>
                                    <div style="margin-bottom: 15px;"><a style="word-break: break-word;" href="<?php echo $r['error_url'] ?>" target="_blank"><?php echo str_replace(array('https://', 'http://', ADMIN_URL, $r['website']), array('', '', '', ''), $r['error_referer']); ?></a></div>
                                </div>

                                <div class="col-sm-2">
                                    <label>Data errore</label>
                                    <div style="margin-bottom: 15px;"><?= ($r['error_date'] != $r['last_error_date'] ? '<small>' : ''); ?><?php echo date("d/m/Y H:i:s", strtotime($r['error_date'])); ?><?= ($r['error_date'] != $r['last_error_date'] ? ' - ' . date("d/m/Y H:i:s", strtotime($r['last_error_date'])) : ''); ?><?= ($r['error_date'] != $r['last_error_date'] ? '</small>' : ''); ?></div>
                                </div>

                                <div class="col-sm-2">
                                    <label>&nbsp;</label>
                                    <a class="btn btn-success" onclick="markBugAsSolved('<?= $_GET['id'] ?>')">Contrassegna come risolto</a>
                                </div>
                            </div>

                            <h2 class="title-divider">Dettagli Errore</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <code style="display: block;">
                                        <b>Errore:</b><br><?= $error_log['error']; ?><hr style="margin: 10px 0;">
                                        <b>File:</b><br><?= $error_log['file']; ?><hr style="margin: 10px 0;">
                                        <b>Linea:</b><br><?= $error_log['line']; ?><hr style="margin: 10px 0;">
                                        <b>File:</b><br><?= $r['error_url']; ?>
                                    </code>
                                </div>
                            </div>


                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
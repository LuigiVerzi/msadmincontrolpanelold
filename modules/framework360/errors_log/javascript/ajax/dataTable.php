<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$priority_info = array(
    '0' => 'inverse',
    '3' => 'warning',
    '10' => 'danger'
);

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    ),
    array(
        'db' => 'id',
        'dt' => 'DT_buttonsLimit',
        'formatter' => function( $d, $row ) {
            return "edit";
        }
    ),
    array( 'db' => 'priority', 'dt' => 0,
        'formatter' => function( $d, $row ) {
            Global $priority_info;

            $priority_color = '';
            foreach($priority_info as $priority => $color) {
                if($d > (int)$priority) $priority_color = $color;
            }

            return '<span class="label label-' . $priority_color . '" style="padding: 0;width: 100%; line-height: 40px; margin: auto; display: block;">' . $d . '</span>';
        }
    ),
    array( 'db' => 'website', 'dt' => 1,
        'formatter' => function( $d, $row ) {
            return '<small><a href="' . $d . '" target="_blank">' . $d . '</a></small>';
        }
    ),
    array( 'db' => 'error_referer', 'dt' => 2,
        'formatter' => function( $d, $row ) {

            $slug = str_replace(
                array('https://', 'http://', ADMIN_URL, $row['website'], 'www.'),
                array('', '', '', '', ''),
                $d
            );

            return '<small><a href="' . $d . '" target="_blank">' . ($slug == '' ? 'Home' : $slug) . '</a></small>';
        }
    ),
    array( 'db' => 'error_log', 'dt' => 3,
        'formatter' => function( $d, $row ) {
            $error = json_decode($d, true);
            return '<code>' . $error['error'] . '<br>Linea ' . $error['line'] . ' | Colonna ' . $error['col'] . '</code>';
        }
    ),
    array( 'db' => 'error_date', 'dt' => 4,
        'formatter' => function( $d, $row ) {
            return array("display" => date("d/m/Y H:i", strtotime($d)), "sort" => strtotime($d));
        }
    ),
    array( 'db' => 'last_error_date', 'dt' => 5,
        'formatter' => function( $d, $row ) {
            return array("display" => date("d/m/Y H:i", strtotime($d)), "sort" => strtotime($d));
        }
    )
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'marke833_framework.errors__javascript', 'id', $columns, " solved != '1' " )
);
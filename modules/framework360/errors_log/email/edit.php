<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.errors__emails WHERE id = :id", array($_GET['id']), true);
}
if(!$r) header('Location: index.php');

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Riepilogo Errore</h2>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Sito web</label>
                                    <div style="margin-bottom: 15px;"><a href="//<?php echo $r['website'] ?>" target="_blank"><?php echo $r['website'] ?></a></div>
                                </div>

                                <div class="col-sm-4">
                                    <label>URL errore</label>
                                    <div style="margin-bottom: 15px;"><a href="<?php echo $r['error_url'] ?>" target="_blank"><?php echo $r['error_url'] ?></a></div>
                                </div>

                                <div class="col-sm-2">
                                    <label>Data errore</label>
                                    <div style="margin-bottom: 15px;"><?php echo date("d/m/Y H:i:s", strtotime($r['error_date'])); ?></div>
                                </div>

                                <div class="col-sm-2">
                                    <label>Ultima registrazione</label>
                                    <div style="margin-bottom: 15px;"><?php echo date("d/m/Y H:i:s", strtotime($r['last_error_date'])); ?></div>
                                </div>

                                <div class="col-sm-2">
                                    <label>&nbsp;</label>
                                    <a class="btn btn-success" onclick="markBugAsSolved('<?= $_GET['id'] ?>')">Contrassegna come risolto</a>
                                </div>
                            </div>

                            <h2 class="title-divider">Dettagli Errore</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <code style="display: block;">
                                        <?= $r['error_log']; ?>
                                    </code>
                                </div>
                            </div>

                            <h2 class="title-divider">Dettagli Email</h2>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Oggetto</label>
                                    <div style="margin-bottom: 15px;"><?= $r['email_subject']; ?></div>
                                </div>
                                <div class="col-sm-3">
                                    <label>Mittente</label>
                                    <div style="margin-bottom: 15px;"><?= $r['email_sender']; ?></div>
                                </div>
                                <div class="col-sm-3">
                                    <label>Destinatario</label>
                                    <div style="margin-bottom: 15px;"><?= implode(', ',  (json_decode($r['email_recipient']) ? json_decode($r['email_recipient'], true) : array('<b class="text-danger">Nessuno!</b>'))); ?></div>
                                </div>
                                <div class="col-sm-3">
                                    <label>Template Utilizzato</label>
                                    <div style="margin-bottom: 15px;"><?= ($r['email_template_id'] == 'custom' ? 'Personalizzato' : $r['email_template_id']); ?></div>
                                </div>
                            </div>


                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
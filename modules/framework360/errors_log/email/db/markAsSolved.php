<?php
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

if((new \MSFramework\Modules\errorTracker())->markAsSolved($_POST['pID'], 'emails')) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>

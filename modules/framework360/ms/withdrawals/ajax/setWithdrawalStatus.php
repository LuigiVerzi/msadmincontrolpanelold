<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$status = $_POST['status'];
$id = $_POST['id'];

$withdrawal_info = (new \MSFramework\MSAgency\earnings())->getCommissionDetails($id)[$id];
if(!$withdrawal_info) die('err');

if ($status) {

    (new \MSFramework\MSAgency\emails())->sendMail(
        '[msagency][user]withdrawal-accepted',
        array(
            'user_data' => $withdrawal_info['user_data'],
            'value' => $withdrawal_info['earning']
        )
    );

    $MSFrameworkDatabase->pushToDB("UPDATE " . FRAMEWORK_DB_NAME . ".ms_agency__earnings SET withdrawal_date = NOW() WHERE id = :id AND withdrawal_date = 0", array(':id' => $id));
} else {
    $MSFrameworkDatabase->pushToDB("UPDATE " . FRAMEWORK_DB_NAME . ".ms_agency__earnings SET withdrawal_date = 0 AND withdrawal_status = 0 WHERE id = :id AND withdrawal_date = 0", array(':id' => $id));
}

die('ok');

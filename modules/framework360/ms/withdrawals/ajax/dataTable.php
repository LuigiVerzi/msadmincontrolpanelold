<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function ($d, $row) {
            return $d;
        }
    ),
    array(
        'db' => 'id',
        'dt' => 'DT_buttonsLimit',
        'formatter' => function ($d, $row) {
            return 'read';
        }
    ),
    array(
        'db' => 'user_id',
        'dt' => 0,
        'formatter' => function ($d, $row) {
            return (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($d);
        }
    ),
    array(
        'db' => "(SELECT SUM(earning) FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings t2 WHERE t2.user_id = " . FRAMEWORK_DB_NAME . ".ms_agency__earnings.user_id AND t2.withdrawal_date = 0)",
        'dt' => 1,
        'formatter' => function ($d, $row) {
            return '<span class="label label-inverse">' . number_format($d, 2, ',', '.') . ' <?= CURRENCY_SYMBOL; ?></span>';
        }
    ),
    array(
        'db' => 'earning',
        'dt' => 2,
        'formatter' => function ($d, $row) {
            return '<span class="label label-primary">' . number_format($d, 2, ',', '.') . ' <?= CURRENCY_SYMBOL; ?></span>';
        }
    ),
    array(
        'db' => 'withdrawal_date',
        'dt' => 3,
        'formatter' => function ($d, $row) {
            return '<span class="label label-' . ($d > 0 ? 'success' : 'warning') . '">' . ($d > 0 ? 'Effettuato' : 'In corso') .'</span>';
        }
    ),
    array(
        'db' => 'date',
        'dt' => 4,
        'formatter' => function ($d, $row) {
            if ($d) {
                return array("display" =>  date('d/m/Y H:i', strtotime($d)), 'sort' => $d);
            } else {
                return array("display" => '<small class="text-muted">Mai</small>', 'sort' => strtotime($d));
            }
        }
    )
);

if($_GET['source'] == 'attesa') {
    echo json_encode(
        $datatableHelper::complex($_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . ".ms_agency__earnings", 'id', $columns, null, 'withdrawal_status = 1 AND withdrawal_date = 0')
    );
} else {
    echo json_encode(
        $datatableHelper::complex($_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . ".ms_agency__earnings", 'id', $columns, null, 'withdrawal_status = 1 AND withdrawal_date > 0')
    );
}
?>

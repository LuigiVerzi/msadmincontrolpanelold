<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$stati_cliente = array();

$resoconto = array(
    'attesa' => $MSFrameworkDatabase->getAssoc("SELECT SUM(earning) as val FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE withdrawal_status = 1 AND withdrawal_date = 0", array(), true)['val'],
    'elaborato' => $MSFrameworkDatabase->getAssoc("SELECT SUM(earning) as val FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE withdrawal_status = 1 AND withdrawal_date > 0", array(), true)['val']
);
?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Resoconto</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-4">
                                    <h1 class="no-margins"><?= number_format($resoconto['attesa'], 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                                    <div class="font-bold text-warning">In Attesa</div>
                                </div>

                                <div class="col-md-4">
                                    <h1 class="no-margins"><?= number_format($resoconto['elaborato'], 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                                    <div class="font-bold text-navy">Accreditati</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Prelievi in Attesa</h1>
                        <fieldset>
                            <table id="customers_with_withdrawals_table" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Bilancio Attuale</th>
                                    <th>Valore richiesto</th>
                                    <th>Stato</th>
                                    <th class="is_data">Data</th>
                                </tr>
                                </thead>
                            </table>
                        </fieldset>

                        <h1>Prelievi Effettuati</h1>
                        <fieldset>
                            <table id="customers_without_withdrawals_table" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Bilancio Attuale</th>
                                    <th>Valore richiesto</th>
                                    <th>Stato</th>
                                    <th class="is_data">Data</th>
                                </tr>
                                </thead>
                            </table>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
<script>
    globalInitForm();
</script>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(!isset($_GET['id']) || !$_GET['id']) {
    header('Location: index.php');
    die();
}

/* INFORMAZIONI PRELIEVO */
$r = $MSFrameworkDatabase->getAssoc("SELECT * FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE id = :id", array(':id' => $_GET['id']), true);

if(!$r) header('Location: index.php');

/* INFORMAZIONI UTENTE */
$customer = (New \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserDataFromDB($r['user_id']);
$bilancio = $MSFrameworkDatabase->getAssoc("SELECT SUM(earning) as totale FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = :user_id AND withdrawal_status = 0 AND withdrawal_date = 0", array(':user_id' => $r['user_id']), true)['totale'];
?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-md-3">

                    <div class="contact-box">
                        <?= (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($r['user_id']); ?>
                        <div class="contact-box-footer" style="margin-top: 15px; padding: 15px 0 0;">
                            <div class="widget style1 bg-<?= ($bilancio < 0 ? 'danger' : ($bilancio == 0 ? 'warning' : 'primary')); ?>" style="margin: 0;">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="fa fa-ticket fa-5x"></i>
                                    </div>
                                    <div class="col-xs-8 text-right">
                                        <span>Bilancio Attuale</span>
                                        <h2 class="font-bold"><?= number_format($bilancio, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <input type="hidden" id="customer_id" value="<?= $customer['id']; ?>">
                        <h1>Richiesta di prelievo</h1>
                        <fieldset>

                            <h2 class="title-divider">Info Richiesta</h2>

                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Ammontare della richiesta</label>
                                    <div class="p-xs border-top-bottom border-left-right border-size-lg text-center" style="margin-bottom: 15px;">
                                        <h3 class="font-bold" style="font-size: 30px;"><?= number_format($r['earning'], 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h3>
                                    </div>
                                </div>

                                <?php if(!empty($r['note'])) { ?>
                                    <div class="col-lg-12">
                                        <label>Note</label>
                                        <p class="form-control"><?= htmlentities($r['note']); ?></p>
                                    </div>
                                <?php } ?>

                                <div class="col-lg-12">
                                    <label>Data</label>
                                    <p class="form-control"><?= date('d/m/Y H:i:s', strtotime($r['date'])); ?></p>
                                </div>

                                <div class="col-lg-12">
                                    <h2 class="title-divider">Dati di pagamento</h2>
                                </div>
                                <?php foreach(json_decode($r['payment_data'], true) as $ref_title => $ref_value) { ?>
                                    <div class="col-lg-12">
                                        <label><?= ucfirst($ref_title); ?></label>
                                        <p class="form-control"><?= $ref_value; ?></p>
                                    </div>
                                <?php } ?>
                            </div>

                            <?php if($r['withdrawal_date'] == 0) { ?>
                                <h2 class="title-divider">Azioni</h2>
                                <div class="alert alert-info">
                                    Il pagamento <b>non</b> è automatico. Una volta cliccato il pulsante 'Accetta' bisogna assicurarsi di effettuarlo manualmente.
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-block btn-primary btn-lg updateOrderStatus" data-status="1">Accetta</a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-block btn-danger btn-lg updateOrderStatus" data-status="2">Rifiuta</a>
                                    </div>
                            </div>
                            <?php } else if($r['withdrawal_date'] > 1) { ?>
                                <div class="alert alert-success" style="margin-bottom: 0;">
                                    Il prelievo è stato accettato il <b><?= date('d/m/Y H:i:s', $r['withdrawal_date']); ?></b>.
                                </div>
                            <?php } ?>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
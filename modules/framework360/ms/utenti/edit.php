<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkUsers->getUserDataFromDB('global-' . $_GET['id']);
}

$MSFrameworkGeonames = (new \MSFramework\geonames());
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Account</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome*</label>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($r['nome']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Cognome*</label>
                                    <input id="cognome" name="cognome" type="text" class="form-control required" value="<?php echo htmlentities($r['cognome']) ?>">
                                </div>

                                <div class="col-sm-12">
                                    <label>Slug*</label>
                                    <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>">
                                </div>

                                <?php
                                $email_disabled = "";
                                if($r['email'] != "") {
                                    $email_disabled = "disabled";
                                }
                                ?>

                                <div class="col-sm-3">
                                    <label>Email*</label>
                                    <input id="email" name="email" type="text" class="form-control required email <?php echo $email_disabled ?>" value="<?php echo htmlentities($r['email']) ?>" <?php echo $email_disabled ?>>
                                </div>

                                <div class="col-sm-3">
                                    <label>Sito Web</label>
                                    <input id="website" name="website" type="text" class="form-control" value="<?php echo htmlentities($r['website']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Cellulare</label>
                                    <input id="cellulare" name="cellulare" type="text" class="form-control" value="<?php echo htmlentities($r['cellulare']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Cellulare Alternativo</label>
                                    <input id="cellulare_2" name="cellulare_2" type="text" class="form-control" value="<?php echo htmlentities($r['cellulare_2']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php require(ABSOLUTE_SW_PATH . "includes/template/geodata/geodata.php"); ?>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Bio</label>
                                    <textarea id="biografia" name="biografia" class="form-control" rows="4"><?php echo htmlentities($r['biografia']) ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12" style="margin-bottom: 40px;">
                                    <label>Foto utente</label>
                                    <?php
                                    (new \MSFramework\uploads('PROPIC', true))->initUploaderHTML("images", $r['propic']);
                                    ?>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Social</h1>
                        <fieldset>
                            <h2 class="title-divider">Profili Social</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Facebook</label>
                                    <input id="fb_profile" name="fb_profile" type="text" class="form-control" value="<?php echo htmlentities($r['fb_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Instagram</label>
                                    <input id="instagram_profile" name="instagram_profile" type="text" class="form-control" value="<?php echo htmlentities($r['instagram_profile']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Twitter</label>
                                    <input id="twitter_profile" name="twitter_profile" type="text" class="form-control" value="<?php echo htmlentities($r['twitter_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Linkedin</label>
                                    <input id="linkedin_profile" name="linkedin_profile" type="text" class="form-control" value="<?php echo htmlentities($r['linkedin_profile']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Google Plus</label>
                                    <input id="googplus_profile" name="googplus_profile" type="text" class="form-control" value="<?php echo htmlentities($r['googplus_profile']) ?>">
                                </div>
                            </div>
                        </fieldset>

                        <h1>Accesso</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Accesso</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Livello*</label>
                                    <select id="livello" name="livello" class="form-control required">
                                        <option value=""></option>
                                        <optgroup label="Ruoli Agenzia">
                                            <?php foreach($MSFrameworkUsers->getMSAgencyLevels() as $levelK => $levelV) { ?>
                                                <option value="<?php echo $levelK ?>" <?php if($r['ms_agency'] == $levelK) { echo "selected"; } ?>><?php echo $levelV ?></option>
                                            <?php } ?>
                                        </optgroup>
                                    </select>
                                </div>

                                <?php
                                $is_active_check = "checked";
                                if($r['active'] == 0 && $_GET['id'] != "") {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Utente Attivo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php
                            if($_GET['id'] != "") {
                                $pass_asterisk = "";
                                $pass_helper = "Lasciare vuoto per non modificare";
                                $pass_required = "";
                            } else {
                                $pass_asterisk = "*";
                                $pass_helper = "";
                                $pass_required = "required";
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Password<?php echo $pass_asterisk ?></label>
                                    <input id="password" name="password" type="password" class="form-control <?php echo $pass_required ?>">
                                    <span class="help-block m-b-none"><?php echo $pass_helper ?></span>
                                </div>

                                <div class="col-sm-6">
                                    <label>Conferma Password<?php echo $pass_asterisk ?></label>
                                    <input id="password_confirm" name="password_confirm" type="password" class="form-control <?php echo $pass_required ?>">
                                </div>
                            </div>
                        </fieldset>

                    </form>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
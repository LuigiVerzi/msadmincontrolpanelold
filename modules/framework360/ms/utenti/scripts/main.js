$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker(['nome', 'cognome'], 'slug', 'marke833.framework.users', $('#record_id'));
    initOrak('images', 1, 0, 0, 100, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
    manageWorldDataSelects();
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pCognome": $('#cognome').val(),
            "pEmail": $('#email').val(),
            "pCell": $('#cellulare').val(),
            "pCell2": $('#cellulare_2').val(),
            "pWebsite": $('#website').val(),
            "pFBProfile": $('#fb_profile').val(),
            "pTwitterProfile": $('#twitter_profile').val(),
            "pLinkedinProfile": $('#linkedin_profile').val(),
            "pGoogPlusProfile": $('#googplus_profile').val(),
            "pInstagramProfile": $('#instagram_profile').val(),
            "pLivello": $('#livello').val(),
            "pPass": $('#password').val(),
            "pPassConfirm": $('#password_confirm').val(),
            "pBiografia": $('#biografia').val(),
            "pIsActive": $('#is_active:checked').length,
            "pImagesAry": composeOrakImagesToSave('images'),
            "pGeoData": getGeoDataAryToSave(),
            "pSlug": $('#slug').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            geoDataErrorsOnSave(data.status);

            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "password_does_not_match") {
                bootbox.error("Le password inserite devono corrispondere!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido o è già stata utilizzata da un altro utente!");
            } else if(data.status == "site_not_valid") {
                bootbox.error("L'indirizzo web inserito è in un formato non valido! Assicurarsi di inserire http:// prima dell'indirizzo");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "password_strength") {
                bootbox.error("La password deve essere lunga almeno 8 caratteri e deve contenere almeno una lettera ed un numero!");
            } else if(data.status == "ok") {
                alert_message = "Dati salvati con successo.";
                if(data.email != "" && data.email != null) {
                    alert_message += " L'utente dovrà confermare il proprio indirizzo email prima di poter utilizzare il suo account.";
                }

                succesModuleSaveFunctionCallback(data.id, alert_message);
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();

$global_ids = array();
foreach($_POST['pID'] as $k => $v) {
    $global_ids[] = 'global-' . $v;
}

foreach($MSFrameworkUsers->getUserDetails($global_ids) as $user_id => $user) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($user['propic'], true));
}

if($MSFrameworkDatabase->deleteRow(FRAMEWORK_DB_NAME . ".users", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('PROPIC', true))->unlink($images_to_delete);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pCognome'] == "" || $_POST['pSlug'] == "" || $_POST['pEmail'] == "" || $_POST['pLivello'] == "") {
    $can_save = false;
}

if($db_action == "insert") {
    if($_POST['pPass'] == "" || $_POST['pPassConfirm'] == "") {
        $can_save = false;
    }
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

(new MSFramework\geonames())->checkDataBeforeSave();

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], FRAMEWORK_DB_NAME . ".users", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

if($_POST['pPass'] != $_POST['pPassConfirm']) {
    echo json_encode(array("status" => "password_does_not_match"));
    die();
}

if($_POST['pPass'] != "" && $MSFrameworkUsers->checkPasswordStrength($_POST['pPass']) != "") {
    echo json_encode(array("status" => "password_strength"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkUsers->getUserDataFromDB($_POST['pID'], "propic, active");
}

$mail_conflict = false;
if($db_action == "insert") {
    if($MSFrameworkUsers->checkIfMailExists($_POST['pEmail'])) {
        $mail_conflict = true;
    }

    if(!filter_var($_POST['pEmail'], FILTER_VALIDATE_EMAIL)) {
        $mail_conflict = true;
    }
}

if($mail_conflict) {
    echo json_encode(array("status" => "mail_not_valid"));
    die();
}

if($_POST['pWebsite'] != "") {
    if(!filter_var($_POST['pWebsite'], FILTER_VALIDATE_URL)) {
        echo json_encode(array("status" => "site_not_valid"));
        die();
    }
}

$uploader = new \MSFramework\uploads('PROPIC', true);
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "cognome" => $_POST['pCognome'],
    "ruolo" => 0,
    "ms_agency" => $_POST['pLivello'],
    "email" => $_POST['pEmail'],
    "cellulare" => $_POST['pCell'],
    "cellulare_2" => $_POST['pCell2'],
    "website" => $_POST['pWebsite'],
    "fb_profile" => $_POST['pFBProfile'],
    "instagram_profile" => $_POST['pInstagramProfile'],
    "twitter_profile" => $_POST['pTwitterProfile'],
    "linkedin_profile" => $_POST['pLinkedinProfile'],
    "googplus_profile" => $_POST['pGoogPlusProfile'],
    "biografia" => $_POST['pBiografia'],
    "password" => password_hash($_POST['pPass'], PASSWORD_DEFAULT),
    "active" => $_POST['pIsActive'],
    "mail_auth" => "",
    "propic" => json_encode($ary_files),
    "slug" => $_POST['pSlug'],
    "geo_data" => json_encode($_POST['pGeoData']),
);

if($db_action == "update") {
    unset($array_to_save['email']);

    if($_POST['pPass'] == "") {
        unset($array_to_save['password']);
    }
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO " . FRAMEWORK_DB_NAME . ".users ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $userID = $MSFrameworkDatabase->lastInsertId();
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE " . FRAMEWORK_DB_NAME . ".users SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $userID = $_POST['pID'];
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['propic'], true));
}

/* CERCA DI OTTENERE IL GRAVATAR DEL CLIENTE SE NON HA UN AVATAR IMPOSTATO */
if($db_action == 'insert' && !count($ary_files)) {
    (new \MSFramework\users())->setGravatarIfExist('global-' . $userID);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $userID : '')));
die();
?>

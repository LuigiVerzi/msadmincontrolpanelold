<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$uploads = new \MSFramework\uploads('PROPIC', true);

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM " . FRAMEWORK_DB_NAME . ".users") as $r) {

    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
    if(json_decode($r['propic'])) {
        $avatar_url = $uploads->path_html . json_decode($r['propic'], true)[0];
    }

    $array['data'][] = array(
        '<img src="' . $avatar_url . '" width="80">',
        $r['nome'],
        $r['cognome'],
        $MSFrameworkUsers->getMSAgencyLevels((string)$r['ms_agency']),
        $r['email'],
        "DT_RowId" => $r['id'],
        "DT_FormattedData" => $MSFrameworkUsers->getUserPreviewHTML('global-' . $r['id'])
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../../../sw-config.php');
require('../config/moduleConfig.php');

$time_conversion = array(
    'days' => array('giorn', 'o', 'i'),
    'months' => array('mes', 'e', 'i'),
    'years' => array('ann', 'o', 'i')
);

foreach((new \MSFramework\MSAgency\services())->getPackageDetails() as $r) {
    $data = array(
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        (!empty($r['category']) ? $MSFrameworki18n->getFieldValue((new \MSFramework\MSAgency\services())->getCategoryDetails($r['category'], "nome")[$r['category']]['nome']) : 'N/A'),
        '<span class="label label-success">' . number_format($r['prezzo'], 2, ',', '.') . CURRENCY_SYMBOL . '</span>',
        ($r['una_tantum'] ? '<span class="label label-inverse">Una Tantum</span>' : '<span class="label label-inverse">' . $r['duration']['value'] . '</span> <span class="label label-inverse">' . $time_conversion[$r['duration']['type']][0] . $time_conversion[$r['duration']['type']][($r['duration']['value'] === 1 ? '1' : '2')] . '</span>'),
        "DT_RowId" => $r['id']
    );
    $array['data'][] = $data;

}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

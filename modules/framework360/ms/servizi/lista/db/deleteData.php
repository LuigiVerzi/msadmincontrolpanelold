<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\MSAgency\services())->getServiceDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['images'], true), json_decode($r['banner'], true));
}

if($MSFrameworkDatabase->deleteRow("marke833_framework.ms_agency__services", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('SERVICES', true))->unlink($images_to_delete);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initOrak('images', 3, 50, 50, 100, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
    initOrak('banner', 3, 400, 200, 200, getOrakImagesToPreattach('banner'), false, ['image/jpeg', 'image/png']);
    initTinyMCE( '#long_descr');
    initTinyMCE( '#descr', {}, {height: 422});

    $('#quote_needed').on('ifToggled', function (e) {
        if($(this).is(':checked')) {
            $('.price_elements').hide();
            $('.price_elements').find('.form-control').val('');
        } else {
            $('.price_elements').show();
        }
    }).trigger('ifToggled');

    $('#una_tantum').on('ifToggled', function (e) {
        if($(this).is(':checked')) {
            $('.duration_elements').hide().find('input.form-control').val('');
        } else {
            $('.duration_elements').show();
        }
    }).trigger('ifToggled');

    $('#assign_credits').on('ifToggled', function (e) {
        if($(this).is(':checked')) {
            $('.credits_elements').show();
        } else {
            $('.credits_elements').hide().find('input.form-control').val('');
        }
    }).trigger('ifToggled');

}

function moduleSaveFunction() {

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pDescrizione": tinymce.get('descr').getContent(),
            "pDescrizioneEstesa": tinymce.get('long_descr').getContent(),
            "pTitoloDescrizione": $('#descr_title').val(),
            "pQuoteNeeded": $('#quote_needed:checked').length,
            "pPrezzo": $('#prezzo').val(),
            "pUnaTantum": $('#una_tantum:checked').length,
            "pDurata": {
                value: $('#durata_value').val(),
                type: $('#durata_type').val(),
            },
            "pCategory": $('#category').val(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pBannerAry": composeOrakImagesToSave('banner'),
            "pIsActive": $('#is_active:checked').length,
            "pCredits": $('#crediti_value').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
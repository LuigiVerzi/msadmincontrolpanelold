<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = (new \MSFramework\MSAgency\services())->getServiceDetails($_GET['id'])[$_GET['id']];
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row form-with-sidebar">
                                <div class="col-lg-3 right-options col-lg-push-9">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Il servizio richiede un preventivo?</label>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="quote_needed" class="" <?php if($r['quote_needed'] == "1") { echo "checked"; } ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Richiede preventivo</span>
                                                </label>
                                            </div>

                                            <div class="hr-line-dashed"></div>
                                        </div>

                                        <div class="col-sm-12 price_elements">
                                            <label>Prezzo <?= CURRENCY_SYMBOL; ?> (<?php echo ((new \MSFramework\Fatturazione\imposte())->getPriceType() == "0") ? "IVA Inclusa" : "Senza IVA" ?>)</label>
                                            <input type="text" id="prezzo" name="prezzo" class="form-control" value="<?php echo htmlentities($r['prezzo']) ?>">
                                        </div>


                                        <div class="col-sm-12 price_elements">
                                            <div class="hr-line-dashed"></div>
                                        </div>

                                        <div class="col-sm-12">
                                            <label>Il servizio ha una durata precisa?</label>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="una_tantum" class="" <?php if($r['una_tantum'] == "1") { echo "checked"; } ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Pagamento 'Una tantum'</span>
                                                </label>
                                            </div>

                                            <div class="hr-line-dashed"></div>
                                        </div>


                                        <div class="col-sm-6 duration_elements">
                                            <label>Durata servizio</label>
                                            <input type="text" id="durata_value" name="durata_value" class="form-control required" value="<?php echo htmlentities($r['duration']['value']) ?>" required>
                                        </div>

                                        <div class="col-sm-6 duration_elements">
                                            <label></label>
                                            <select id="durata_type" name="durata_type" class="form-control">
                                                <option value="days">Giorni</option>
                                                <option value="months" <?= ($r['duration']['type'] === 'months' ? 'selected' : ''); ?>>Mesi</option>
                                                <option value="years" <?= ($r['duration']['type'] === 'years' ? 'selected' : ''); ?>>Anni</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="hr-line-dashed"></div>

                                            <label>Assegnazione crediti</label>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="assign_credits" class="" <?php if($r['credit_to_assign'] > 0) { echo "checked"; } ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Assegna crediti all'acquisto</span>
                                                </label>
                                            </div>

                                        </div>

                                        <div class="col-sm-12 credits_elements">
                                            <label>Crediti da assegnare</label>
                                            <input type="number" id="crediti_value" name="crediti_value" class="form-control required" value="<?php echo htmlentities($r['credit_to_assign']) ?>" required>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-9 col-lg-pull-3">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Nome*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                            <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Categoria</label>
                                            <select id="category" name="category" class="form-control">
                                                <option value="">Nessuna categoria</option>
                                                <?php foreach((new \MSFramework\MSAgency\services())->getCategoryDetails() as $category) { ?>
                                                    <option value="<?= $category['id']; ?>" <?= ($r['category'] == $category['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($category['nome']); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-3 pull-right">
                                            <label> </label>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="is_active" class="" <?php if($r['is_active'] == "1" || !$r) { echo "checked"; } ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Mostra sul sito</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <h2 class="title-divider">Contenuti</h2>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>Titolo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descr_title']) ?>"></i></span>
                                                    <input type="text" id="descr_title" name="descr_title" class="form-control msShort" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['descr_title'])); ?>">
                                                </div>

                                                <div class="col-sm-12">
                                                    <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descr']) ?>"></i></span>
                                                    <textarea id="descr" name="descr" class="form-control msShort" rows="4"><?php echo $MSFrameworki18n->getFieldValue($r['descr']) ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Dettagli</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['long_descr']) ?>"></i></span>
                                            <textarea id="long_descr" name="long_descr" class="form-control" rows="4"><?php echo $MSFrameworki18n->getFieldValue($r['long_descr']) ?></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <h2 class="title-divider">Media</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Icona</label>
                                    <?php
                                    (new \MSFramework\uploads('SERVICES', true))->initUploaderHTML("images", $r['images']);
                                    ?>
                                </div>

                                <div class="col-sm-6">
                                    <label>Banner</label>
                                    <?php
                                    (new \MSFramework\uploads('SERVICES', true))->initUploaderHTML("banner", $r['banner']);
                                    ?>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
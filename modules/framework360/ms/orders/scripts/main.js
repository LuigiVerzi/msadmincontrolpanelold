$(document).ready(function() {
    loadStandardDataTable('main_table_list', true);
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initTinyMCE($('#order_note'));

    // $('#editSaveBtn').hide();

    $('.changeSeller').on('click', function (e) {
        e.preventDefault();

        showFastPicker('usermanager', function (row) {

            $('#seller-data').html(row.DT_FormattedData);
            $('#seller_id').val(row.DT_RowId);

            $('.changeSeller').css('float', 'right').html('Cambia');

            //$('#editSaveBtn').show();
            //toastr['success']('Commerciale selezionato correttamente, ricordati di salvare l\'ordine per rendere effettive le modifiche.');

            $('#fastModulePicker').modal('hide');
        }, 'commerciale', 41);

    });

    $('.changeConsultant').on('click', function (e) {
        e.preventDefault();

        var $td = $(this).closest('td');
        var $this = $(this);

        showFastPicker('fw360_utenti', function (row) {
            $td.find('.staff-data').html(row.DT_FormattedData);
            $td.find('.consultant_id').val('global-' + row.DT_RowId);

            $td.find('.changeConsultant').css('float', 'right').removeClass('btn-block').html('Cambia');

            $td.find('.commission-settings').show();

            //$('#editSaveBtn').show();
            //toastr['success']('Consulente selezionato correttamente, ricordati di salvare l\'ordine per rendere effettive le modifiche.');

            $('#fastModulePicker').modal('hide');
        }, 'consulente');

    });

    // Aggiornamento prezzo preventivo
    $('.quotePrice').on('change', function (e) {
       e.preventDefault();

       var value = parseFloat($(this).val());

       if(value > 0) {
           //$('#editSaveBtn').show();
           //toastr['success']('Preventivo aggiornato correttamente, ricordati di salvare l\'ordine per rendere effettive le modifiche.');

       } else {
           toastr['error']("Il valore assegnato al preventivo non è valido.")
       }

    });
    $('.productStatus').on('change', function (e) {
       e.preventDefault();

       //$('#editSaveBtn').show();
        //toastr['success']('Stato del prodotto aggiornato correttamente, ricordati di salvare l\'ordine per rendere effettive le modifiche.');

    });
}

function getCartInfo() {

    var product_info = {};
    $('#products_table').find('.product-row').each(function () {
        var product_id = $(this).data('id');

        var tmp = {
            consultant: $(this).find('.consultant_id').val()
        };

        if($(this).find('.consultant_commission').length) {
            tmp.consultant_commission = $(this).find('.consultant_commission').val();
        }

        if($(this).find('.quotePrice').length) {
            tmp.prezzo =$(this).find('.quotePrice').val();
        }

        if($(this).find('.productStatus').length) {
            tmp.payment_status =$(this).find('.productStatus').val();
        }

        product_info[product_id] = tmp;
    });

    return product_info;

}


function moduleSaveFunction(confirm, callback) {

    if(typeof confirm === 'undefined') confirm = false;
    if(typeof callback === 'undefined') callback = false;

    if(!confirm) {
        bootbox.confirm("Confermi di voler aggiornare l'ordine? Una volta salvato gli eventuali aggiornamenti verranno notificati ai destinatari.", function (send) {
            if(send) moduleSaveFunction(true);
        });
        return false;
    }

    var data_to_sent = {
        order_id:$('#record_id').val(),
        cart: getCartInfo(),
        seller: ($('#seller_id').length ? $('#seller_id').val() : 0),
        seller_commission: ($('#seller_commission').length ? $('#seller_commission').val() : 0),
        note: tinymce.get('order_note').getContent(),
        resend_email: (callback !== false ? 1 : 0)
    };

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: data_to_sent,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "not_fully_updated") {
                bootbox.error("Non è stato possibile aggiornare 1 o più prodotti, riprova tra qualche minuto.", function () {
                    location.reload();
                });
            } else if(data.status == "ok") {
                if(typeof(callback) === "function") {
                    callback();
                } else {
                    bootbox.alert("L'ordine è stato aggiornato correttamente.", function () {
                        location.reload();
                    });
                }
            }
        }
    });

}

function resendSummaryMail(confirm) {
    if(typeof confirm === 'undefined') confirm = false;

    if(!confirm) {
        bootbox.confirm("Confermi di voler aggiornare l'ordine e <b>reinviare l'email di resoconto al cliente</b>? Una volta salvato gli eventuali aggiornamenti verranno notificati ai destinatari.", function (send) {
            if(send) moduleSaveFunction(true, function () {
                bootbox.alert("L'ordine è stato aggiornato ed è stata inviata un'email di resoconto al cliente.", function () {
                    location.reload();
                });
            });
        });
        return false;
    }
}
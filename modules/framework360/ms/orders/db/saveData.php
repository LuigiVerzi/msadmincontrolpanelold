<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$orderDetails = (new \MSFramework\MSAgency\orders())->getOrderDetails($_POST['order_id']);
if(!$orderDetails) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$orderDetails = $orderDetails[$_POST['order_id']];
$consultants = ($orderDetails['consultant'] ? explode(',', $orderDetails['consultant']) : array());

$quoteUpdated = array();

$status = true;
foreach($_POST['cart'] as $full_id => $product) {
    $update_array = array();

    if((int)$product['prezzo'] > 0 && $product['prezzo'] != $orderDetails['cart'][$full_id]['prezzo']) {
        $update_array['prezzo'] = $product['prezzo'];
        $quoteUpdated[] = $full_id;
    }

    if($product['payment_status'] != $orderDetails['cart'][$full_id]['payment_status']) {
        $update_array['payment_status'] = $product['payment_status'];
    }

    if(isset($product['consultant_commission']) && $product['consultant_commission'] != $orderDetails['cart'][$full_id]['consultant_commission']) {
        $update_array['consultant_commission'] = $product['consultant_commission'];
    }

    if($product['consultant'] != $orderDetails['cart'][$full_id]['consultant']) {
        $update_array['consultant'] = $product['consultant'];
    }

    if((int)$orderDetails['cart'][$full_id]['una_tantum'] === 0 && $orderDetails['cart'][$full_id]['payment_status'] == 1 && $product['payment_status'] >= 2) {
       if(!(new \MSFramework\MSAgency\orders())->cancelSubscription($orderDetails['id'], $full_id)) $status = false;
    } else if($update_array) {
        if(!(new \MSFramework\MSAgency\orders())->updateOrderProducts($orderDetails['id'], $full_id, $update_array)) $status = false;
    }
}

$array_to_save = array(
    "note" => $_POST['note'],
    "seller_commission" => $_POST['seller_commission']
);

if($_POST['seller']) {
    $array_to_save['seller'] = $_POST['seller'];
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'update');
$result = $MSFrameworkDatabase->pushToDB("UPDATE " . FRAMEWORK_DB_NAME . ".ms_agency__orders SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['order_id']), $stringForDB[0]));

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$newOrderDetails = (new \MSFramework\MSAgency\orders())->getOrderDetails($orderDetails['id'])[$orderDetails['id']];

// Invia l'email con il preventivo aggiornato
$newQuoteCart = array();
foreach($quoteUpdated as $full_id) {
    $newQuoteCart[$full_id] = $newOrderDetails['cart'][$full_id];
}

if($newQuoteCart) {

    $newQuoteOrder = $newOrderDetails;
    $newQuoteOrder['cart'] = $newQuoteCart;

    // Invio la notifica al cliente
    (new \MSFramework\MSAgency\emails())->sendMail(
        '[msagency][customer]new-quote',
        array(
            'order' => $newQuoteOrder
        )
    );

    // Invio la notifica al venditore
    (new \MSFramework\MSAgency\emails())->sendMail(
        '[msagency][seller]new-quote',
        array(
            'order' => $newQuoteOrder
        )
    );
}

if($_POST['resend_email'] == 1) {
    // Invio il resoconto al cliente
    $a = (new \MSFramework\MSAgency\emails())->sendMail(
        '[msagency][customer]order-summary',
        array(
            'order' => $newOrderDetails
        )
    );
}

if(!$status) {
    echo json_encode(array("status" => "not_fully_updated"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ''));
die();
?>
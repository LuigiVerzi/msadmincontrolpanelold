<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_delete = true;
foreach((new \MSFramework\MSAgency\orders())->getOrderDetails($_POST['pID']) as $orderDetail) {
    foreach($orderDetail['cart'] as $product) {
        if((int)$product['una_tantum'] === 0 && (int)$product['payment_status'] === 1) {
            $can_delete = false;
        }
    }
}

if(!$can_delete) {
    echo json_encode(array("status" => "protected"));
    die();
}

if($MSFrameworkDatabase->deleteRow("marke833_framework.ms_agency__orders", "id", $_POST['pID'])) {
    $MSFrameworkDatabase->deleteRow("marke833_framework.ms_agency__transactions", "order_id", $_POST['pID']);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


<?php
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSAgencyOrders = new \MSFramework\MSAgency\orders();
$statusLabel = $MSAgencyOrders->getStatus();

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    ),
    array(
        'db' => 'origin_reference',
        'dt' => 'DT_OriginReference'
    ),
    array( 'db' => 'customer_reference', 'dt' => 0, 'formatter' => function($d, $row) {
        return (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))->getCustomerPreviewHTML($d);
    }),
    array( 'db' => 'seller', 'dt' => 1, 'formatter' => function($d, $row) use ($MSAgencyOrders){
        if($d && $row['origin_reference'] == $MSAgencyOrders->sellerOriginDB) {
            return (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($d);
        } else {
            return '<span class="text-muted">Nessuno</span>';
        }
    }),
    array( 'db' => 'consultant', 'dt' => 2, 'formatter' => function($d, $row) {
        if($d) {
            $return = '';
            foreach(explode(',', $d) as $k => $consultant) {
                if($k > 0) $return .= '<div class="hr-line-dashed" style="margin: 10px 0;"></div>';
                $return .= (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($consultant, (count(explode(',', $d)) > 1));
            }
            return $return;
        } else {
            return '<span class="text-muted">Non assegnati</span>';
        }
    }),
    array( 'db' => 'id', 'dt' => 3, 'formatter' => function($d, $row) use ($statusLabel) {
        $return = '';
        foreach((new \MSFramework\MSAgency\orders())->getFullOrderStatus($row['id']) as $singleStatus) {
            $return .= '<span class="label" style="margin-bottom: 5px; display: block; color: white; background: ' . $singleStatus['color'] . '; text-align: left;">' . $singleStatus['label'] . '</span>';
        }
        return $return;
    }),
    array( 'db' => 'last_update', 'dt' => 4, 'formatter' => function($d, $row) {
        return '<div style="line-height: 1.5">' . (new \MSFramework\utils())->smartdate(strtotime($d)) . '<small class="text-muted"><br>' . date('d/m/Y H:i', strtotime($d)) . '</small></div>';
    })
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.ms_agency__orders', 'id', $columns)
);
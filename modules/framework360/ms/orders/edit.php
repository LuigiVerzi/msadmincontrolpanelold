<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

function makeTable($ary) {
    $html = "<table class='table table-bordered white-bg'>";
    foreach($ary as $k => $v) {
        $real_v = $v;

        if(!is_array($real_v)) {
            $json = json_decode($v, true);
            if (json_last_error() == JSON_ERROR_NONE) {
                $real_v = $json;
            }
        }

        $html .= "<tr>";
        $html .= "<th>" . $k . "</th>";
        $html .= "<td>" . (is_array($real_v) ? makeTable($real_v) : $real_v) . "</td>";
        $html .= "</tr>";
    }

    $html .= "</table>";

    return $html;
}

$MSAgencyOrders = new \MSFramework\MSAgency\orders();

$r = array();
$transactions_data = array();
if(isset($_GET['id'])) {
    $r = $MSAgencyOrders->getOrderDetails($_GET['id'])[$_GET['id']];
    $transactions_data = $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`ms_agency__transactions` WHERE order_id = :order_id", array(':order_id' => $r['id']));
}

if(!$r) {
    header('Location: index.php');
}

$settings_data = $MSFrameworkFW->getCMSData('settings');
$framework_data = json_decode($r['framework_data'], true);

$statusLabel = (new \MSFramework\MSAgency\orders())->getStatus();

$serviceUploads = new \MSFramework\uploads('SERVICES', true);
$packageUploads = new \MSFramework\uploads('PACKAGES', true);
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Resoconto</h1>

                        <fieldset>
                            <div class="row">

                                <div class="col-md-3 col-sm-6" style="margin-bottom: 30px;">
                                    <label>Data</label>
                                    <p>
                                        <u>Creazione:</u> <?= date('d/m/Y H:i', strtotime($r['order_date'])); ?>
                                        <br>
                                        <u>Ultimo aggiornamento:</u> <?= date('d/m/Y H:i', strtotime($r['last_update'])); ?>
                                    </p>
                                </div>

                                <div class="col-md-3 col-sm-6" style="margin-bottom: 30px;">
                                    <label>Cliente</label>
                                    <?= (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))->getCustomerPreviewHTML($r['customer_reference']); ?>
                                </div>

                                <div class="col-md-3 col-sm-6" style="margin-bottom: 30px;">
                                    <?php if($r['seller']) { ?>
                                        <label>Commerciale</label>
                                        <a href="#" class="changeCustomer btn btn-sm btn-default changeSeller" style="float: <?= ($r['seller'] ? 'right' : 'none'); ?>; z-index: 10; position: relative;">Cambia</a>
                                        <div class="staff-data" id="seller-data">
                                            <input type="hidden" id="seller_id" value="<?= $r['seller']; ?>">
                                            <?= (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($r['seller']); ?>
                                            <div style="clear: both;"></div>
                                        </div>

                                        <?php if(!(new \MSFramework\MSAgency\earnings())->getCommissionByOrder($r['id'], '', 'seller')) { ?>
                                            <div class="commission-settings" style="margin-top: 10px;">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-addon">Commissione</span>
                                                    </div>
                                                    <input type="number" class="form-control" id="seller_commission" value="<?= (isset($r['seller_commission']) ? (int)$r['seller_commission'] : (new \MSFramework\MSAgency\earnings())->sellerCommission); ?>">
                                                    <div class="input-group-append">
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="commission-settings" style="margin-top: 10px;">
                                                <?php if((int)($r['seller_commission']) > 0) { ?>
                                                    <label>Commissione del <b><?= (int)($r['seller_commission']); ?>%</b></label>
                                                <?php } else { ?>
                                                    <label><b>Nessuna</b> commissione</label>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>

                                    <?php } else if($r['buyer']) { ?>
                                        <label>Origine</label>
                                        <div class="staff-data" id="seller-data">
                                            <?php $domain_info = $MSFrameworkFW->getWebsitePathsBy('customer_database', $r['origin_reference']); ?>

                                            <?php
                                            $origin_preview = $domain_info['assets']['favicon'];
                                            if(!$origin_preview) $origin_preview = $domain_info['assets']['logo'];
                                            ?>

                                            <div style="position: relative; <?= ($origin_preview ? 'padding-left: 65px;' : ''); ?>">

                                                <?php if($origin_preview) { ?>
                                                    <img src="<?= $origin_preview; ?>" width="50" height="50px" style="position: absolute; left: 0; top: 0; margin-right: 15px; object-fit: scale-down; background: #fafafa; border-radius: 3px;">
                                                <?php } ?>
                                                <h2 style="margin: 0;"><?= $domain_info['site_name']; ?></h2>
                                                <small><a href="<?= $domain_info['http_ext'] . '//' . $domain_info['url']; ?>" target="_blank"><?= $domain_info['url']; ?></a></small>
                                            </div>

                                            <div style="clear: both;"></div>
                                        </div>
                                    <?php } ?>
                                </div>

                                <div class="col-md-3 col-sm-6 text-right" style="margin-bottom: 30px;">
                                    <label>Stato ordine</label>
                                    <?php
                                    foreach((new \MSFramework\MSAgency\orders())->getFullOrderStatus($r) as $singleStatus) {
                                        echo '<span class="label" style="margin-bottom: 5px; margin-right: 5px; display: inline-block; color: white; background: ' . $singleStatus['color'] . ';">' . $singleStatus['label'] . '</span>';
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <h2 class="title-divider" style="margin-top: -15px;">Carrello</h2>
                                    <table class="table table-bordered" id="products_table" style="margin: 0;">
                                        <tbody>
                                        <?php foreach($r['cart'] as $full_id => $product) { ?>
                                            <tr class="product-row" data-id="<?= $full_id; ?>">
                                                <td width="90" class="text-center">
                                                    <?php if($product['images']) { ?>
                                                        <?php
                                                        if(stristr($full_id, 'service')) $image_base = $serviceUploads->path_html;
                                                        else $image_base = $packageUploads->path_html;
                                                        ?>
                                                        <img src="<?php echo $image_base . json_decode($product['images'], true)[0] ?>" width="80" />
                                                    <?php } ?>
                                                </td>
                                                <td class="desc">
                                                    <h3><?php echo $MSFrameworki18n->getFieldValue($product['nome']); ?></h3>
                                                    <p><?php echo $MSFrameworki18n->getFieldValue($product['descr']); ?></p>
                                                </td>

                                                <td style="vertical-align: middle; min-width: 150px;" class="consulente">
                                                    <input type="hidden" class="consultant_id" value="<?= $product['consultant']; ?>">

                                                    <?php if($product['payment_status'] < 2 || $product['consultant']) { ?>

                                                        <?php if($product['payment_status'] < 2) { ?>
                                                        <a href="#" class="changeCustomer btn btn-sm btn-default changeConsultant<?= (!$product['consultant'] ? ' btn-block ' : ''); ?>" style="float: <?= ($product['consultant'] ? 'right' : 'none'); ?>; z-index: 10; position: relative;">
                                                            <?php if($product['consultant']) { ?>
                                                                Cambia
                                                            <?php } else { ?>
                                                                Assegna consulente
                                                            <?php } ?>
                                                        </a>
                                                        <?php } ?>

                                                        <div class="staff-data" class="consultant-data">
                                                            <?php if($product['consultant']) { ?>
                                                                <?= (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($product['consultant']); ?>
                                                            <?php } ?>
                                                            <div style="clear: both;"></div>
                                                        </div>

                                                        <?php if(!(new \MSFramework\MSAgency\earnings())->getCommissionByOrder($r['id'], $full_id, 'consultant')) { ?>
                                                            <div class="commission-settings" style="margin-top: 10px; display: <?= ($product['consultant'] ? 'block' : 'none'); ?>;">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-addon">Commissione</span>
                                                                    </div>
                                                                    <input type="number" class="form-control consultant_commission" value="<?= (isset($product['consultant_commission']) ? (int)$product['consultant_commission'] : (new \MSFramework\MSAgency\earnings())->consultantCommission); ?>">
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="commission-settings" style="margin-top: 10px; display: <?= ($product['consultant'] ? 'block' : 'none'); ?>;">
                                                                <?php if((int)($product['consultant_commission']) > 0) { ?>
                                                                    <label>Commissione del <b><?= (int)($product['consultant_commission']); ?>%</b> accreditata</label>
                                                                <?php } else { ?>
                                                                    <label><b>Nessuna</b> commissione accreditata</label>
                                                                <?php } ?>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <label style="display: block; text-align: center;"><b>Nessun</b> consulente assegnato</label>
                                                    <?php } ?>

                                                </td>

                                                <?php
                                                $singleProductStatus = (new \MSFramework\MSAgency\orders())->getSingleProductStatus($product);
                                                $payment_label = '<span class="badge" style="margin-bottom: 10px; display: inline-block; color: white; background: ' . $singleProductStatus['color'] . ';">' . $singleProductStatus['label'] . '</span>';
                                                $payment_class = $singleProductStatus['class'];
                                                ?>

                                                <td style="vertical-align: middle; min-width: 100px; width: 300px; background: <?= (new \MSFramework\utils())->hex2rgba($singleProductStatus['color'], '0.15'); ?>" class="text-center">

                                                    <?= $payment_label; ?>
                                                    <div style="clear: both;"></div>

                                                    <?php if(!$product['quote_needed']) { ?>
                                                        <b><?php echo number_format($product['prezzo'], 2, ',', '.') ?> <?= CURRENCY_SYMBOL; ?></b>
                                                        <br>

                                                        <small><?= $product['payment_recurrence_string']; ?></small>

                                                        <select class="productStatus form-control" style="margin-top: 5px;">
                                                            <?php foreach((new \MSFramework\MSAgency\orders())->getStatus() as $status_id => $status) { ?>
                                                                <?php if($product['una_tantum'] == "1" || (!in_array((int)$status_id, array(2, 0, 4)) || (int)$status_id === (int)$product['payment_status'])) { ?>
                                                                    <option value="<?= $status_id; ?>" style="" <?= ($product['payment_status'] == $status_id ? 'selected' : ''); ?>><?= $status['label']; ?></option>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </select>


                                                    <?php } else { ?>
                                                        <input type="number" class="quotePrice form-control" placeholder="Inserisci prezzo..." style="color: black; background: none; border: none; border-bottom: 1px solid transparent; text-align: center; font-size: 15px; font-weight: 500;">
                                                        <div style="clear: both;"></div>
                                                        <small><?= $product['payment_recurrence_string']; ?></small>
                                                    <?php } ?>

                                                </td>
                                            </tr>
                                        <?php } ?>

                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-lg-3">
                                    <h2 class="title-divider">Dati di Fatturazione</h2>

                                    <h3><?=  $r['customer_data']['nome']; ?> <?=  $r['customer_data']['cognome']; ?></h3>

                                    <address>

                                        <div><strong><?= $r['customer_data']['ragione_sociale']; ?></strong></div>

                                        <?php if( $r['customer_data']['piva']) { ?>
                                            <abbr title="Partita Iva">P.IVA:</abbr> <?=  $r['customer_data']['piva']; ?><br>
                                        <?php } else if( $r['customer_data']['cf']) { ?>
                                            <abbr title="Partita Iva">C.F:</abbr> <?=  $r['customer_data']['cf']; ?><br>
                                        <?php } ?>

                                        <div class="hr-line-dashed"></div>

                                        <?= $r['customer_data']['indirizzo']; ?><br>
                                        <?= $r['customer_data']['comune']; ?>, <?= $r['customer_data']['citta']; ?> <?= $r['customer_data']['cap']; ?><br>

                                        <div class="hr-line-dashed"></div>

                                        <abbr title="Telefono">T:</abbr> <?=  $r['customer_data']['cellulare']; ?><br>
                                        <abbr title="Email">E:</abbr> <?=  $r['customer_data']['email']; ?>
                                    </address>

                                </div>

                                <div class="col-lg-9">
                                    <h2 class="title-divider">Note</h2>

                                    <div class="order-note">
                                        <textarea id="order_note" class="form-control"><?= htmlentities($r['note']); ?></textarea>
                                        <a href="#" class="btn btn-success pull-right" onclick="resendSummaryMail();">Re-invia email di resoconto al cliente</a>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>

                            </div>

                            <h2 class="title-divider">Link di acquisto</h2>
                            <code class="alert-info">
                                <?= CUSTOMER_DOMAIN_INFO['backend_url'] . 'modules/' . $MSSoftwareModules->getModuleDetailsFromID('ms_agency_ordini')['path'] . 'edit.php?token=' . $r['guest_token']; ?>
                            </code>

                        </fieldset>

                        <?php if($transactions_data) { ?>
                            <h1>Dettagli IPN</h1>
                            <fieldset>
                                <?php foreach($transactions_data as $transaction) { ?>
                                    <div class="ibox">
                                        <div class="ibox-title">
                                            <i class="ibox-icon fa fa-credit-card fa-3x"></i>
                                            <h5><?= $MSFrameworki18n->getFieldValue($r['cart'][json_decode($transaction['framework_data'], true)['item']['id']]['nome']); ?></h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                                <a class="fullscreen-link">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <?php echo '<div class="ibox-content" style="background-color: #fefefe; padding: 0px;">' . makeTable(array_merge(array("Metodo di Pagamento" => $MSFrameworki18n->getFieldValue($settings_data['payMethods'][$r['provider']]['display_name'], true)), json_decode($transaction['ipn_data'], true))) . "</div>"; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </fieldset>

                            <h1>Altri Dettagli</h1>
                            <fieldset>
                                <?php foreach($transactions_data as $transaction) { ?>
                                    <div class="ibox">
                                        <div class="ibox-title">
                                            <i class="ibox-icon fas fa-info-circle fa-3x"></i>
                                            <h5><?= $MSFrameworki18n->getFieldValue($r['cart'][json_decode($transaction['framework_data'], true)['item']['id']]['nome']); ?></h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                                <a class="fullscreen-link">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <?= '<div class="ibox-content" style="background-color: #fefefe; padding: 0px;">' . makeTable(json_decode($transaction['framework_data'], true)) . "</div>"; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </fieldset>
                        <?php } ?>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
<style>
    @media (max-width: 1200px) {

        #products_table {
            border: none;
        }

        #products_table tr td {
            border-bottom: 0;
            border: none;
        }

        #products_table,
        #products_table tbody,
        #products_table tr,
        #products_table tr td {
            display: block;
            width: 100% !important;
        }

        #products_table tr {
            margin-bottom: 30px;
            border: 1px solid #e7eaec;
        }

        #products_table td.consulente {
            background: #fafafa;
            border-top: 1px solid #e7eaec;
            border-bottom: 1px solid #dddddd;
        }
    }
</style>
<script>
    
    globalInitForm();
</script>
</body>
</html>
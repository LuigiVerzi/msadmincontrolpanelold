$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('nome', 'slug', 'marke833_framework`.`gdpr_scripts', $('#record_id'));

    $('#keywords').tagsInput({
        'height':'80px',
        'width':'100%',
        'interactive':true,
        'defaultText':'Aggiungi',
        'removeWithBackspace' : true
    });

    $('.rowDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi script",
        "deleteButtonText": "Elimina script",
        "beforeAddCallback": function() {
            $('.scriptDuplication').find('.in_tag').iCheck('destroy');
            $('.scriptDuplication').find('.async').iCheck('destroy');
        },
        "afterAddCallback": function () {
            $('.rowDuplication:last').find('.js_script').val('');
            $('.rowDuplication:last').find('.tag_type').val('script');
            $('.rowDuplication:last').find('.in_tag').iCheck('uncheck').iCheck('update');
            $('.rowDuplication:last').find('.async').iCheck('uncheck').iCheck('update');

            initIChecks();
            initMSTooltip();
        }
    });

}

function moduleSaveFunction() {
    scripts_ary = new Object();
    $('.stepsDuplication.scriptDuplication').each(function(row_index) {
        scripts_ary_tmp = new Array();

        script_js_val = $(this).find('.js_script').val();
        if(script_js_val == "") {
            return true;
        }

        scripts_ary_tmp.push($(this).find('.js_script').val());
        scripts_ary_tmp.push($(this).find('.tag_type').val());
        scripts_ary_tmp.push($(this).find('.in_tag:checked').length);
        scripts_ary_tmp.push($(this).find('.async:checked').length);

        scripts_ary[row_index] = scripts_ary_tmp;
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pType": $('#script_type').val(),
            "pTrackingSuiteID": $('#tracking_suite_id').val(),
            "pSlug": $('#slug').val(),
            "pDescrizione": $('#descrizione').val(),
            "pDatiRaccolti": $('#dati_raccolti').val(),
            "pLuogoTrattamento": $('#luogo_trattamento').val(),
            "pPrivacyPolicyURL": $('#privacy_policy_url').val(),
            "pOptOutURL": $('#opt_out_url').val(),
            "pKeywords": $('#keywords').val(),
            "pScripts": scripts_ary,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            }  else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
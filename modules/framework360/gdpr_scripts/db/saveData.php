<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);


$can_save = true;
if($_POST['pNome'] == "" || $_POST['pSlug'] == "" || $_POST['pDescrizione'] == "" || $_POST['pDatiRaccolti'] == "" || $_POST['pLuogoTrattamento'] == "" || $_POST['pPrivacyPolicyURL'] == "" || $_POST['pOptOutURL'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT nome, descrizione, dati_raccolti, luogo_trattamento FROM `" . FRAMEWORK_DB_NAME . "`.`gdpr_scripts` WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pDescrizione'], 'oldValue' => $r_old_data['descrizione']),
        array('currentValue' => $_POST['pDatiRaccolti'], 'oldValue' => $r_old_data['dati_raccolti']),
        array('currentValue' => $_POST['pLuogoTrattamento'], 'oldValue' => $r_old_data['luogo_trattamento']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "descrizione" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione'], $_POST['pDescrizione']),
    "dati_raccolti" => $MSFrameworki18n->setFieldValue($r_old_data['dati_raccolti'], $_POST['pDatiRaccolti']),
    "luogo_trattamento" => $MSFrameworki18n->setFieldValue($r_old_data['luogo_trattamento'], $_POST['pLuogoTrattamento']),
    "type" => $_POST['pType'],
    "tracking_suite_id" => $_POST['pTrackingSuiteID'],
    "slug" => $_POST['pSlug'],
    "privacy_policy_url" => $_POST['pPrivacyPolicyURL'],
    "opt_out_url" => $_POST['pOptOutURL'],
    "keywords" => $_POST['pKeywords'],
    "scripts" => json_encode($_POST['pScripts'], true),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`gdpr_scripts` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`gdpr_scripts` SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`gdpr_scripts`") as $r) {
    $array['data'][] = array(
        $MSFrameworki18n->getFieldValue($r['nome']),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

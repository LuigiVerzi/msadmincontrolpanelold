<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`gdpr_scripts` WHERE id = :id", array($_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome Script*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>" placeholder="Es. Google Analytics">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Slug*</label>
                                            <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($r['slug']) ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Tipo Script*</label>
                                    <select class="form-control required" id="script_type">
                                        <option value=""></option>
                                        <?php
                                        foreach((new \MSFramework\Modules\gdpr())->getCategorieGDPR() as $tk => $tv) {
                                        ?>
                                            <option value="<?= $tk ?>" <?= ($r['type'] == $tk ? "selected" : "") ?>><?= $tv['titolo'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Link Tracking Suite</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se lo script viene caricato da Tracking Suite, è possibile indicare il tipo di risorsa qui. Questo dato contribuirà alla determinazione dell'utilizzo della risorsa sul sito attraverso lo scraper."></i></span>
                                    <select class="form-control" id="tracking_suite_id">
                                        <option value=""></option>
                                        <?php
                                        foreach((new \MSFramework\tracking())->getTrackingParams() as $tk => $tv) {
                                        ?>
                                            <option value="<?= $tk ?>" <?= ($r['tracking_suite_id'] == $tk ? "selected" : "") ?>><?= $tv['name'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Descrizione Script*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione']) ?>"></i></span>
                                    <textarea id="descrizione" name="descrizione" class="form-control required" rows="4"><?php echo htmlentities($MSFrameworki18n->getFieldValue($r['descrizione'])) ?></textarea>
                                </div>

                                <div class="col-sm-6">
                                    <label>Descrizione Dati Raccolti*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione']) ?>"></i></span>
                                    <textarea id="dati_raccolti" name="dati_raccolti" class="form-control required" rows="4"><?php echo htmlentities($MSFrameworki18n->getFieldValue($r['dati_raccolti'])) ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Luogo del trattamento dei dati*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="luogo_trattamento" name="luogo_trattamento" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['luogo_trattamento'])) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>URL Privacy Policy*</label>
                                    <input id="privacy_policy_url" name="privacy_policy_url" type="text" class="form-control required" value="<?php echo htmlentities($r['privacy_policy_url']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>URL Opt-Out*</label>
                                    <input id="opt_out_url" name="opt_out_url" type="text" class="form-control required" value="<?php echo htmlentities($r['opt_out_url']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Keywords</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Queste keywords verranno utilizzate dallo scraper in aggiunta ai JS/URL specificati per determinare il reale utilizzo della risorsa GDPR sul sito."></i></span>
                                    <input id="keywords" name="keywords" type="text" class="form-control" value="<?php echo htmlentities($r['keywords']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="easyRowDuplicationContainer">
                                <?php
                                $scripts = json_decode($r['scripts'], true);
                                if(count($scripts) == 0) {
                                    $scripts[] = array();
                                }
                                ?>

                                <?php foreach($scripts as $addPhK => $addPhV) { ?>
                                    <div class="stepsDuplication rowDuplication scriptDuplication">
                                        <div class="row">
                                            <div class="col-sm-6"> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="E' possibile inserire sia il codice JS da caricare sul sito, sia un URL al file."></i></span>
                                                <label>JS/Url script da inserire</label>
                                                <textarea class="form-control js_script" rows="4"><?= $addPhV[0] ?></textarea>
                                            </div>

                                            <div class="col-sm-2">
                                                <label>Tipo TAG</label>
                                                <select class="form-control tag_type">
                                                    <option value="script" <?= ($addPhV[1] == "script" ? "selected" : "") ?>>Script</option>
                                                    <option value="noscript" <?= ($addPhV[1] == "noscript" ? "selected" : "") ?>>Noscript</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-2">
                                                <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se checkato, inserisce il valore all'interno del tag. In caso contrario, imposta il valore all'interno dell'atributo 'src'"></i></span>
                                                <div class="styled-checkbox form-control">
                                                    <label style="width: 100%;">
                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                            <input type="checkbox" class="in_tag" <?php if($addPhV[2] == "1") { echo "checked"; } ?>>
                                                            <i></i>
                                                        </div>
                                                        <span style="font-weight: normal;">In Tag</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se checkato, imposta l'attributo 'async' del tag su true"></i></span>
                                                <div class="styled-checkbox form-control">
                                                    <label style="width: 100%;">
                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                            <input type="checkbox" class="async" <?php if($addPhV[3] == "1") { echo "checked"; } ?>>
                                                            <i></i>
                                                        </div>
                                                        <span style="font-weight: normal;">Async</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>


                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
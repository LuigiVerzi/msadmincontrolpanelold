<?php
/**
 * MSAdminControlPanel
 * Date: 08/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_translate = $MSFrameworki18n->getPrimaryLangId() != $MSFrameworki18n->getCurrentLanguageDetails()['id'];

$count = 0;
foreach($MSFrameworki18n->getCurBackendTranslationsAry() as $k => $v) {
    $original = $v->getOriginal();

    $array['data'][] = array(
        '<span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . implode('<br>', $v->getReferences()[0]) . '"></i></span> <span>' . $original . '</span><code class="original" style="display: none;">' . htmlspecialchars($original) . '</code>',
        ($can_translate ? '<span class="editable"><a href="#" data-type="text" data-pk="' . $count . '">' . $v->getTranslation() . "</a></span>" : '<span class="text-muted">Non puoi tradurre nella lingua originale</span>')
    );

    $count++;
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

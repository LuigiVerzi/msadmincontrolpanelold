<?php
/**
 * MSAdminControlPanel
 * Date: 08/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$translations = $MSFrameworki18n->getCurBackendTranslationsAry();

$translation = $translations->find('', $_POST['original_value']);
$translation->setTranslation($_POST['value']);

$translations->toPoFile($MSFrameworki18n->getCurLangPoPath(false, true));
?>

$(document).ready(function() {
    initDataTable();
});

function checkForNewString() {
    $('#running_sync').show();
    $('#main_table_list_wrapper').hide();

    $.ajax({
        url: 'ajax/checkForNewStrings.php',
        async: true,
        success: function(data) {
            $('#running_sync').hide();
            $('#main_table_list_wrapper').show();
            initDataTable();
        }
    });
}

function initDataTable() {

    try {
        $('#main_table_list').DataTable().destroy();
    } catch(e) {}

    $('#main_table_list').DataTable({
        "ajax": 'ajax/dataTable.php',
        "fnDrawCallback": function( oSettings ) {
            initEditable();
            initMSTooltip();
        },
        "language": {
            "url": $('#baseElementPathAdmin').val() + "vendor/plugins/dataTables/locales/it.json"
        }
    });

}

function initEditable() {
    $('#main_table_list .editable a').editable({
        title: 'Inserisci traduzione',
        emptytext: 'Inserisci traduzione',
        url: 'ajax/updatePoFile.php',
        params: function(params) {
            params.original_value = $('a[data-pk=' + params.pk + ']').closest('tr').find('.original').text();

            return params;
        }
    });
}
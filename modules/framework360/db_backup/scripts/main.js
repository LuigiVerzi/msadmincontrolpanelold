function initForm() {
    $("#form").steps({
        bodyTag: "fieldset",
        enableFinishButton: false,
        enableCancelButton: false,
        stepsOrientation: "vertical",
        forceMoveForward: true,
        labels: {
            cancel: "Annulla",
            current: "step corrente:",
            pagination: "Paginazione",
            finish: "Fine",
            next: "Successivo",
            previous: "Precedente",
            loading: "Caricamento..."
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if(currentIndex == 0 && $('input[name=operation]:checked').val() == "backup") {
                if($('#database_backup:checked').length == 0 && $('.directory_backup:checked').length == 0) {
                    bootbox.alert('Non puoi effettuare un backup vuoto!');
                    return false;
                }
            }

            return true;
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            $('#form .actions').show();
            initMSTooltip();

            if(currentIndex == 1) {
                total_procs = 0;
                installed_procs = 0;

                $('#form .actions').hide();
                $('#upd_status').append('');

                proc_list_html = "";
                proc_list_html += '<div id="clean_tmp_step" class="row"><div class="col-sm-6" style="text-align: right"><b>Pulizia archivi</b></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';

                if($('input[name=operation]:checked').val() == "backup") {
                    proc_list_html += '<div id="db_backup_step" class="row"><div class="col-sm-6" style="text-align: right"><b>Backup Dati</b></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';
                    total_procs += 1;

                    if($('#auto_ripristino:checked').length == 1) {
                        proc_list_html += '<div id="ripristino_step" class="row"><div class="col-sm-6" style="text-align: right"><b>Ripristino Dati</b></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';
                        total_procs += 1;
                    }
                } else {
                    proc_list_html += '<div id="db_backup_step" class="row"><div class="col-sm-6" style="text-align: right"><b>Ripristino Dati</b></div> <div class="col-sm-6" style="text-align: left"><span class="status text-warning">In attesa</span></div></div>';
                    total_procs += 1;
                }

                $('#procedures_list').html(proc_list_html);

                total_procs += 1; //pulizia TMP

                $('#packages_to_install').html(total_procs);

                initMSTooltip();

                $.ajax({
                    url: "ajax/cleanTMP.php",
                    async: false,
                    dataType: "json",
                    success: function(data) {
                        installed_procs++;
                        $('#clean_tmp_step').find('.status').removeClass('text-warning').addClass('text-info').html('Completato.');
                        updateProgressBar(installed_procs, total_procs);
                    }
                })

                if($('input[name=operation]:checked').val() == "backup") {
                    directory_backup = [];
                    $('.directory_backup:checked').each(function() {
                        directory_backup.push($(this).attr('id'));
                    })

                    $.ajax({
                        url: "ajax/doBackup.php",
                        async: false,
                        dataType: "json",
                        data: {
                            db_backup: $('#database_backup:checked').length,
                            directory_backup: directory_backup,
                            fonte: $('#fonte').val(),
                        },
                        method: "POST",
                        success: function(data) {
                            installed_procs++;
                            $('#db_backup_step').find('.status').removeClass('text-warning').addClass('text-info').html('Completato.');
                            updateProgressBar(installed_procs, total_procs);
                            $('#downloadBackupContainer').show();
                            $('#downloadBackup').attr('href', data.archiveHTMLPath);

                            if($('#auto_ripristino:checked').length == 1) {
                                $.ajax({
                                    url: "ajax/restoreBackup.php",
                                    async: false,
                                    method: "POST",
                                    data: {
                                        "from_path" : data.archivePath,
                                        "destination_path" : $('#destinazione').val()
                                    },
                                    success: function(data) {
                                        installed_procs++;
                                        $('#ripristino_step').find('.status').removeClass('text-warning').addClass('text-info').html('Completato.');
                                        updateProgressBar(installed_procs, total_procs);

                                        setComplete('backup');
                                    }
                                })
                            } else {
                                setComplete('backup');
                            }
                        }
                    })
                } else {
                    $.ajax({
                        url: "ajax/restoreBackup.php",
                        async: false,
                        success: function(data) {
                            installed_procs++;
                            $('#db_backup_step').find('.status').removeClass('text-warning').addClass('text-info').html('Completato.');
                            updateProgressBar(installed_procs, total_procs);

                            setComplete('ripristino');
                        }
                    })
                }
            }
        },
        onInit: function (event, currentIndex) {
            $("#form").fadeIn();
            if($("#form > .steps li").length > 1) {
                $("#form > .steps").show();
            }
        }
    });

    Dropzone.options.dropzoneForm = {
        paramName: "file",
        maxFilesize: 100,
        multiple: false,
        dictDefaultMessage: "<strong>Seleziona un file .tar.gz da caricare</strong></br> (Oppure trascinalo qui sopra)",
        acceptedFiles: '.tar.gz',
        maxFiles:1,
        init: function() {
            this.on('addedfile', function(file) {
                $('#form .actions').hide();

                if (this.files.length > 1) {
                    this.removeFile(this.files[0]);
                }
            });

            this.on('success', function(file, response) {
                if(response == "wrong_ext") {
                    bootbox.error('Questa estensione non è ammessa');
                    this.removeFile(this.files[0]);

                    return false;
                }

                $('#form .actions').show();
            });
        }
    };

    $('input[name=operation]').on('ifChanged', function (event) {
        if($('input[name=operation]:checked').val() == "backup") {
            $('#dropzoneFormContainer').hide();
            $('#backupFormContainer').show();
            $('#form .actions').show();
        } else {
            $('#dropzoneFormContainer').show();
            $('#backupFormContainer').hide();
            $('#form .actions').hide();
        }
    })

    $('#fonte').on('change', function() {
        if($(this).val() == "cur_site") {
            $('#auto_ripristino').iCheck('uncheck');
            $('#auto_ripristino').iCheck('disable');
        } else {
            $('#auto_ripristino').iCheck('enable');
        }

        checkDestinationEnable();
    })

    $('.directory_backup, #auto_ripristino').on('ifChanged', function() {
        checkDestinationEnable();
    })

    $('#full_site_backup').on('ifChanged', function() {
        if($(this).is(':checked')) {
            $('.directory_backup:not(#full_site_backup)').iCheck('check');
            $('.directory_backup:not(#full_site_backup)').iCheck('disable');
        } else {
            $('.directory_backup:not(#full_site_backup)').iCheck('uncheck');
            $('.directory_backup:not(#full_site_backup)').iCheck('enable');
        }
    })
}

function checkDestinationEnable() {
    if($('#fonte').val() == "cur_site" || $('.directory_backup:checked').length == 0|| $('#auto_ripristino:checked').length == 0) {
        $('#destinazione').attr('disabled', 'disabled');
        $('#destination_settings_container').hide();
    } else {
        $('#destinazione').attr('disabled', false);
        $('#destination_settings_container').show();
    }
}

function setComplete(type) {
    progress_bar_status = "progress-bar-default";
    global_upd_status_str = type.charAt(0).toUpperCase() + type.slice(1) + " completato con successo.";

    $('.progress').removeClass('progress-striped').find('.progress-bar').removeClass('progress-bar-success').addClass(progress_bar_status);

    $('#global_upd_status').html(global_upd_status_str);
    $('#form .actions').show();
}

function enableLogDownload() {
    $('#downloadLogContainer').show();

    $('#downloadLog').unbind('click');
    $('#downloadLog').on('click', function() {
        window.location = 'ajax/downloadLogs.php?version=' + $('#final_sw_version').val();
    })
}

function updateProgressBar(installed_procs, total_procs) {
    $('#installed_packages').html(installed_procs);
    percentage = (installed_procs/total_procs)*100;
    $('.progress-bar').css('width', percentage+'%').attr('aria-valuenow', percentage);
}
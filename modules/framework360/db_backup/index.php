<?php
/**
 * MSAdminControlPanel
 * Date: 14/10/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('producer_config');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">

            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div id="form" class="form-horizontal wizard-big">
                        <h1>Introduzione</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-12 m-b-lg">
                                    Questa procedura ti guiderà durante il processo di backup o ripristino del database e delle directory.
                                </div>

                                <div class="col-md-12 m-b-lg">
                                    <div class="row">
                                        <div class="col-md-6">
                                            Quale operazione desideri eseguire?
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="i-checks"><label> <input type="radio" value="backup" name="operation" checked> <i></i> Backup </label></div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="i-checks"><label> <input type="radio" value="ripristino" name="operation"> <i></i> Ripristino </label></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" id="backupFormContainer">
                                    <h4 class="blue-bg text-center" style="padding: 20px;">Selezionare i dati da salvare</h4>

                                    <div class="row m-b-md">
                                        <div class="col-sm-8">
                                            <label>Origine</label>
                                            <select id="fonte" class="form-control required" required>
                                                <option value="cur_site">Sito corrente</option>
                                                <?php
                                                if($_SESSION['userData']['userlevel'] == "0") {
                                                    foreach($MSFrameworkDatabase->getAssoc("SELECT customer_domain, id, customer_database, virtual_server_name FROM " . FRAMEWORK_DB_NAME . ".customers ORDER BY customer_domain") as $site) {
                                                        $append_vsn = "";
                                                        if(substr($site['customer_database'], 0, 12) == "marke833_dev") {
                                                            if($site['virtual_server_name'] == "") {
                                                                continue;
                                                            }

                                                            $append_vsn = " (" . $site['virtual_server_name'] . ")";
                                                        }
                                                ?>
                                                    <option value="<?= $site['id'] ?>"><?= $site['customer_domain'] . $append_vsn ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-4">
                                            <label style="font-weight: normal;">&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selezionando questa voce, i dati del sito di origine verranno automaticamente importati sul dominio corrente."></i></span>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="auto_ripristino" class="auto_ripristino" disabled>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Ripristina automaticamente</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Elementi da includere nel backup</label>
                                        </div>
                                    </div>

                                    <div class="row m-b-md">
                                        <div class="col-sm-4">
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="database_backup" class="database_backup" checked>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Database</span>
                                                </label>
                                            </div>
                                        </div>

                                        <?php
                                        if($_SESSION['userData']['userlevel'] == "0") {
                                        ?>
                                        <div class="col-sm-4">
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="full_site_backup" class="directory_backup">
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Sito completo</span>
                                                </label>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="col-sm-4">
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="uploads_backup" class="directory_backup">
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Cartella uploads</span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="i18n_backup" class="directory_backup">
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Cartella i18n</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="destination_settings_container" style="display: none;">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Destinazione elementi ripristinati</label>
                                            </div>
                                        </div>

                                        <div class="row m-b-md">
                                            <div class="col-sm-4">
                                                <label>Directory</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Impostando come destinazione '/', verrà creata una nuova cartella con lo stesso nome della cartella di origine. Impostando un altro tipo di destinazione, invece, i dati della cartella di origine verranno copiati nel percorso selezionato sovrascrivendo i files uguali."></i></span>
                                                <select id="destinazione" class="form-control required" required disabled>
                                                    <option value="<?= MS_DOCUMENT_ROOT ?>"><?= str_replace(array($_SERVER['DOCUMENT_ROOT'], '/home/marke833/FRAMEWORK/'), array("", ""), MS_DOCUMENT_ROOT) ?>/</option>
                                                    <?php
                                                    $dir = new DirectoryIterator(MS_DOCUMENT_ROOT);
                                                    $tmpDestAry = array();
                                                    foreach($dir as $fileinfo) {
                                                        if (!$fileinfo->isDot() && $fileinfo->isDir() && !strstr($fileinfo->getPathname(), "cgi-bin") && !strstr($fileinfo->getPathname(), "MSAdminControlPanel") && !strstr($fileinfo->getPathname(), "MSBackups") && !strstr($fileinfo->getPathname(), "MSCommonStorage") && !strstr($fileinfo->getPathname(), "MSLogs")) {
                                                            $tmpDestAry[] = $fileinfo->getPathname();
                                                        }
                                                    }

                                                    sort($tmpDestAry);
                                                    foreach($tmpDestAry as $fileinfo) {
                                                        $clean_fileinfo = str_replace(array($_SERVER['DOCUMENT_ROOT'] . "/", '/home/marke833/FRAMEWORK/'), array("", ""), $fileinfo);
                                                    ?>
                                                        <option value="<?= $fileinfo ?>" <?= ($clean_fileinfo == $MSFrameworkCMS->getCleanHost() ? "selected" : "") ?>><?= $clean_fileinfo ?>/</option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" style="display: none;" id="dropzoneFormContainer">
                                    <h4 class="blue-bg text-center" style="padding: 20px;">Al termine del ripristino, assicurati di eseguire l'aggiornamento alla versione <b><?= SW_VERSION ?></b> attraverso l'apposito modulo</h4>

                                    <form action="ajax/uploadBackup.php" class="dropzone" id="dropzoneForm">
                                        <div class="fallback">
                                            <input name="file" type="file"/>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </fieldset>

                        <h1>Esecuzione</h1>
                        <fieldset>
                            <div id="upd_status" class="row text-center">
                                <div class="col-md-12 m-b-sm" id="global_upd_status">Avvio dell'operazione in corso: <b>non chiudere questa finestra!</b></div>
                                <div class="col-md-12">Procedure completate <span id="installed_packages">0</span>/<span id="packages_to_install">0</span></div>
                                <div class="col-md-12 m-b-sm">
                                    <div class="progress progress-striped active">
                                        <div style="width: 10%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-bar progress-bar-success">
                                            <span class="sr-only">0% Completato</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 m-b-sm" id="procedures_list" style="line-height: 28px; max-height: 400px; overflow-y: auto; overflow-x: hidden; border: 1px solid #dedede; padding: 20px 0;">
                                    Recupero dati in corso...
                                </div>
                            </div>
                        </fieldset>

                        <h1>Fine</h1>
                        <fieldset>
                            <div class="row text-center">
                                <div class="col-md-12 m-b-md">La procedura è terminata!</div>
                                <div class="col-md-12 m-b-sm" id="downloadBackupContainer" style="display: none;">
                                    <a class="btn btn-primary" id="downloadBackup">Scarica il backup dei dati</a>
                                </div>
                            </div>
                        </fieldset>
                    </div>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
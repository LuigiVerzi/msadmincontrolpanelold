<?php
/**
 * MSAdminControlPanel
 * Date: 14/10/2018
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$backup_path = MS_DOCUMENT_ROOT . "/" . ADMIN_FOLDER . '/tmp/db_backup/';
mkdir($backup_path, 0777, true);

if($_POST['from_path'] != "") {
    $path_to_extract = $_POST['from_path'];
} else {
    $path_to_extract = $backup_path . $_SESSION['db'] . "_sql_backup.tar";
}

$destination_path = MS_DOCUMENT_ROOT;
if($_POST['destination_path'] != "") {
    $destination_path = $_POST['destination_path'];
}

$copy_content = "";
if($_POST['destination_path'] != MS_DOCUMENT_ROOT) {
    $copy_content = ".";
}

shell_exec("cd " . $backup_path . " && tar -xvf " . $path_to_extract);

$dir = new DirectoryIterator($backup_path);
foreach($dir as $fileinfo) {

    if (!$fileinfo->isDot()) {
        $filename = $fileinfo->getFilename();

        if (strpos($filename, '.sql') !== false) {
            shell_exec("mysql -u " . FRAMEWORK_DB_USER . " -p" . FRAMEWORK_DB_PASS . "  -e \"DROP DATABASE " . $_SESSION['db'] . "\"");
            shell_exec("mysql -u " . FRAMEWORK_DB_USER . " -p" . FRAMEWORK_DB_PASS . "  -e \"CREATE DATABASE " . $_SESSION['db'] . "\"");

            shell_exec("mysql -u " . FRAMEWORK_DB_USER . " -p" . FRAMEWORK_DB_PASS . " -h " . FRAMEWORK_DB_HOST . " " . $_SESSION['db'] . " < " . $backup_path . $filename);
        }

        if($fileinfo->isDir()) {
            shell_exec("cp -R " . $backup_path . $filename . "/" . $copy_content . " " . $destination_path . "/");
        }
    }
}

shell_exec("rm -R " . $backup_path);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 14/10/2018
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$dest = MS_DOCUMENT_ROOT . "/" . ADMIN_FOLDER . '/tmp/db_backup/';

shell_exec("rm -R " . $dest);
mkdir($dest, 0777, true);

$info = pathinfo($_FILES['file']['name']);
$ext = $info['extension'];

if($ext != "gz") {
    die('wrong_ext');
}

move_uploaded_file($_FILES['file']['tmp_name'], $dest . $_SESSION['db'] . "_sql_backup.tar.gz");
shell_exec("gzip -d " . $dest . $_SESSION['db'] . "_sql_backup.tar.gz");
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 14/10/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$folder_name = date('d-m-Y_H-i-s');
$updater_sql_path = PATH_TO_SQL_BACKUP . 'db_backup/';
$backup_path = $updater_sql_path . $folder_name . "/";

mkdir($backup_path, 0777, true);

if($_SESSION['userData']['userlevel'] != "0" || $_POST['fonte'] == "cur_site") {
    $db = $_SESSION['db'];
} else {
    $site_data = $MSFrameworkDatabase->getAssoc("SELECT customer_domain, virtual_server_name, customer_database FROM " . FRAMEWORK_DB_NAME . ".customers WHERE id = :id", array(":id" => $_POST['fonte']), true);
    $db = $site_data['customer_database'];
}

$backup_filename = $db . "_" . date('d-m-Y_H-i-s');

$site_folder_name = "";
$site_path = "";
$files_to_tar = "";

if(count($_POST['directory_backup']) > 0) {
    if($_SESSION['userData']['userlevel'] != "0" || $_POST['fonte'] == "cur_site") {
        $site_folder_name = $MSFrameworkCMS->getVirtualServerName();
        $site_path = MAIN_SITE_FOLDERPATH;
    } else {
        if(substr($site_data['customer_database'], 0, 12) == "marke833_dev" && $site_data['virtual_server_name'] != "") { //è stato richiesto il backup di un dominio dev
            $site_folder_name = $site_data['virtual_server_name'];
            $subfolder = "SVILUPPO/" . $site_data['customer_domain'];
        } else {
            $site_folder_name = $site_data['customer_domain'];
            $subfolder = "FRAMEWORK";
        }

        $site_folder_name = str_replace(array("http://", "https://", "www.", "admin."), array("", "", "", ""), $site_folder_name);
        $site_path = realpath(dirname(SITES_HOME_FOLDER . "/" . $site_data['customer_domain']) . (FRAMEWORK_DEBUG_MODE ? '/../..' : '/..')) . "/" . $subfolder . "/" . $site_folder_name . "/";
    }

    mkdir($backup_path . $site_folder_name, 0777, true);
    if(in_array("full_site_backup", $_POST['directory_backup'])) {
        shell_exec("cp -R " . $site_path . " " . $backup_path . "/");
    } else {
        foreach($_POST['directory_backup'] as $backup_folder_name) {
            shell_exec("cp -R " . $site_path . "/" . str_replace("_backup", "", $backup_folder_name) . "/ " . $backup_path . $site_folder_name . "/");
        }
    }

    $files_to_tar .= $backup_path . $site_folder_name . '/ ';
}


if($_POST['db_backup'] == "1") {
    shell_exec('mysqldump ' . $db . ' --password=' . FRAMEWORK_DB_PASS . ' --user=' . FRAMEWORK_DB_USER . ' > ' . $backup_path . $backup_filename . '.sql');
    $files_to_tar .= $backup_path . 'sql_' . $backup_filename . '.sql ';
}

shell_exec('cd ' . $backup_path . ' && tar -czf ' . $updater_sql_path . $backup_filename . '.tar.gz .');
shell_exec('rm -R ' . $backup_path);

echo json_encode(array(
    "archiveName" => $backup_filename . '.tar.gz',
    "archivePath" => PATH_TO_SQL_BACKUP . 'db_backup/' . $backup_filename . '.tar.gz',
    "archiveHTMLPath" => PATH_TO_SQL_BACKUP_HTML . 'db_backup/' . $backup_filename . '.tar.gz',
));
?>
<?php
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    ),
    array(
        'db' => 'id',
        'dt' => 'DT_buttonsLimit',
        'formatter' => function( $d, $row ) {
            return "edit";
        }
    ),
    array( 'db' => 'quote_id', 'dt' => 0, 'formatter' => function($d, $row) {
        global $MSFrameworkFW, $MSFrameworkSaaSBase, $MSFrameworkTicket;

        $r_details = $MSFrameworkTicket->getTicketResponseDetails($d)[$d];
        $q_details = $MSFrameworkTicket->getTicketQuestionDetails($r_details['question'])[$r_details['question']];

        $website_info = $MSFrameworkFW->getWebsitePathsBy('id', str_replace("saas_", "", $q_details['domain']), strstr($q_details['domain'], "saas_"));

        $append_saas_string = "";
        if(strstr($q_details['domain'], "saas_")) {
            $MSFrameworkEnvironments = new \MSFramework\SaaS\environments(str_replace("saas_", "", $q_details['domain']));
            $append_saas_string = "<br /> <small>" . $MSFrameworkEnvironments->getDBName() . "</small>";
        }

        return '<a href="' . $website_info['http_ext'] . '//' . $website_info['url'] . '" target="_blank">' . $website_info['url'] . '</a>' . $append_saas_string;
    }),
    array( 'db' => 'framework_data', 'dt' => 1, 'formatter' => function($d, $row) {
        $r = json_decode($d, true);

        return $r['user']['email'];
    }),
    array( 'db' => 'quote_id', 'dt' => 2, 'formatter' => function($d, $row) {
        global $MSFrameworkFW, $MSFrameworkSaaSBase, $MSFrameworkTicket;

        $r_details = $MSFrameworkTicket->getTicketResponseDetails($d)[$d];
        $q_details = $MSFrameworkTicket->getTicketQuestionDetails($r_details['question'])[$r_details['question']];

        return $q_details['title'];
    }),
    array( 'db' => 'insert_date', 'dt' => 3, 'formatter' => function($d, $row) {
        return (DateTime::createFromFormat("Y-m-d H:i:s", $d))->format("d/m/Y H:i:s");
    }),
    array( 'db' => 'provider', 'dt' => 4, 'formatter' => function($d, $row) {
        global $MSFrameworkFW, $MSFrameworki18n;

        $r = $MSFrameworkFW->getCMSData('settings');
        $pay = $r['payMethods'][$d]['display_name'];
        return $MSFrameworki18n->getFieldValue($pay, true);
    }),
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.ticket__transactions', 'id', $columns)
);
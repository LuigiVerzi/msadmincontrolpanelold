<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = 'insert';

$can_save = true;

$ticket_details = (new \MSFramework\Framework\ticket)->getTicketQuestionDetails($_POST['pID']);

if(!in_array($_POST['pID'], array_keys($ticket_details))) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$ticket_id = $_POST['pID'];
$result = $MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.ticket__domande SET status = :status, have_unread = (have_unread+1) WHERE id = :id", array(':status' => $_POST['pTicketStatus'], ':id' => $ticket_id));

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$array_to_save = array(
    "question" => $ticket_id,
    "is_staff" => 1,
    "user_id" => $_SESSION['userData']['user_id'],
    "message" => json_encode(array(
        'type' => 'status_update',
        'value' => $_POST['pTicketStatus']
    )),
    "files" => json_encode(array())
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
$result = $MSFrameworkDatabase->pushToDB("INSERT INTO marke833_framework.ticket__risposte ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

// Invio un email al cliente con l'aggiornamento dello stato
if($ticket_details[$ticket_id]['notification_email'] !== "") {

    $status_label = 'In attesa di aggiornamenti';
    if($_POST['pTicketStatus'] > 0) {
        if($_POST['pTicketStatus'] == 100) {
            $status_label = 'Completato al 100%';
        } else {
            $status_label = 'Completato al ' . $_POST['pTicketStatus'] . '%';
        }
    }

    $author_info = array_values($ticket_details[$ticket_id]['messages'])[0]['author_info'];

    $emailClass = new \MSFramework\emails();
    $emailClass->forceRealRecipients = true;

    $emailClass->sendMail('ticket-status-update', array(
        'id' => $ticket_id,
        'stato' => $status_label,
        'titolo' => $_POST['pTitolo'],
        'categoria' => (new \MSFramework\Framework\ticket())->getTicketCategories()[$ticket_details[$ticket_id]['category']]['title'],
        'user_name' => $author_info['name'],
        'notification_email' => $ticket_details[$ticket_id]['notification_email']
    ));
}

echo json_encode(array("status" => "ok"));
die();
?>
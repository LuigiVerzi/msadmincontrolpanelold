<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$ticket_details = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($_POST['pID'], true)[$_POST['pID']];

$can_save = true;
if(!$ticket_details || ($_POST['pMessaggio'] == "" && $ticket_details['status'] == $_POST['pTicketStatus']) || !$_SESSION['userData']) {
    $can_save = false;
}

if((int)$_POST['pTicketStatus'] != (int)$ticket_details['status']) {
    $_POST['pMessaggio'] .= '#' . $_POST['pTicketStatus'] . '%';
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if($_POST['pPreventivo']['enabled'] == "1") {
    if($_POST['pPreventivo']['data']['price'] == "" || count($_POST['pPreventivo']['data']['pay_methods']) == 0) {
        echo json_encode(array("status" => "mandatory_data_missing"));
        die();
    }

    $_POST['pPreventivo']['data']['price'] = str_replace(",", ".", $_POST['pPreventivo']['data']['price']);
    if(!strstr($_POST['pPreventivo']['data']['price'], ".") && $_POST['pPreventivo']['data']['price'] != "") {
        $_POST['pPreventivo']['data']['price'] .= ".00";
    }

    if(!is_numeric($_POST['pPreventivo']['data']['price']) && $_POST['pPreventivo']['data']['price'] != "") {
        echo json_encode(array("status" => "quotation_price_numeric"));
        die();
    }

    if($_POST['pPreventivo']['data']['delivery_days'] != "" && (!is_numeric($_POST['pPreventivo']['data']['delivery_days']) || $_POST['pPreventivo']['data']['delivery_days'] <= 0)) {
        echo json_encode(array("status" => "quotation_days_numeric"));
        die();
    }

    $_POST['pPreventivo']['state'] = "unpaid";
} else {
    $_POST['pPreventivo'] = array();
}

$message_id = (new \MSFramework\Framework\ticket())->addReplyToTicket($_POST['pID'], $_SESSION['userData']['user_id'], $_POST['pMessaggio'], $_POST['pAllegati'], false, $_POST['pPreventivo']);

if(!$message_id) {
    json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => $message_id));
die();
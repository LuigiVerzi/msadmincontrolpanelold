<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$ticket_details = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($_POST['pID']);

if($MSFrameworkDatabase->deleteRow("marke833_framework.ticket__domande", "id", $_POST['pID'])) {

    foreach($ticket_details as $tID => $q) {
        foreach($q['messages'] as $r) {
            if (json_decode($r['files_json']) && count(json_decode($r['files_json'], true))) {
                (new \MSFramework\uploads('ASSISTENZA_TICKET'))->unlink(json_decode($r['files_json'], true));
            }
        }
    }

    $MSFrameworkDatabase->deleteRow("marke833_framework.ticket__risposte", "question", $_POST['pID']);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}

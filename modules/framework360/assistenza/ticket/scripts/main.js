$(document).ready(function() {
    loadStandardDataTable('main_table_list', true);
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initOrak('allegati', 3, 50, 50, 250, [], false, [
        'image/jpeg',
        'image/png',
        'application/pdf',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'text/plain',
        'application/zip',
        'application/x-rar',
        'text/xml',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ]);

    initTinyMCE( '#descrizione');

    $("#stato_ticket").ionRangeSlider({
        type: 'single',
        min: 0,
        max: 100,
        step: 10,
        from: $("#stato_ticket").data('current'),
        postfix: " %",
        onFinish: function(data){
            var value = data.fromNumber;
            $("#stato_ticket").data('current', value);
        }
    });

    $('#send_ticket_is_complete').on('ifToggled', function () {
        if($(this).is(':checked')) {
            $('.stato_ticket_container').css('opacity', 0.2).css('pointer-events', 'none');
        } else {
            $('.stato_ticket_container').css('opacity', 1).css('pointer-events', 'initial');
        }
    });

    $('#add_quotation').on('ifToggled', function () {
        ($(this).is(':checked') ? $('#quotation_box').show() : $('#quotation_box').hide())
    });

    $('.paymethod_col').sortable();

    $('#ticket_quotation_mark_as_paid, #ticket_quotation_mark_as_unpaid, #ticket_quotation_cancel').on('click', function() {
        $(this).addClass('disabled').attr('disabled', 'disabled')

        message_id = $(this).closest('.ticket_message_container').data('message-id');
        new_status = $(this).data('new-status');

        $.ajax({
            url: "ajax/quotations/changeStatus.php",
            method: "POST",
            data: {
                message : message_id,
                status : new_status
            },
            success: function(data) {
                bootbox.alert('Lo stato del preventivo è stato modificato', function() {
                    location.reload();
                });
            }
        })
    })

    $('#send_ticket_message').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: "db/sendMessage.php",
            type: "POST",
            data: {
                "pID": $('#record_id').val(),
                "pMessaggio": tinymce.get('descrizione').getContent(),
                "pAllegati": composeOrakImagesToSave('allegati'),
                "pTicketStatus": ($('#send_ticket_is_complete:checked').length ? 100 : $('#stato_ticket').data().current),
                "pPreventivo": {
                    "enabled" : $('#add_quotation:checked').length,
                    "data" : {
                        "price" : $('#quote_price').val(),
                        "delivery_days" : $('#quote_delivery_days').val(),
                        "pay_methods" : (function() {
                            enabled_paymethods = [];

                            $('.quote_paymethod:checked').each(function() {
                                enabled_paymethods.push($(this).attr('id'));
                            });

                            return enabled_paymethods;
                        })()
                    }
                }
            },
            async: false,
            dataType: "json",
            success: function (data) {
                if (data.status == "mandatory_data_missing") {
                    bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
                } else if (data.status == "mv_error") {
                    bootbox.error("C'è stato un problema durante il salvataggio dei degli allegati. Impossibile continuare.");
                } else if(data.status == "quotation_price_numeric") {
                    bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
                } else if(data.status == "quotation_days_numeric") {
                    bootbox.error("Il campo 'giorni stimati' deve contenere un valore numerico maggiore di zero");
                } else if (data.status == "query_error") {
                    bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
                } else if (data.status == "ok") {
                    location.reload();
                }
            }
        });
    });
}
<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkTickets = new \MSFramework\Framework\ticket();
$MSFrameworkTicketsQuotation = new \MSFramework\Framework\Ticket\quotations();
if(isset($_GET['id'])) {
    $r = $MSFrameworkTickets->getTicketQuestionDetails($_GET['id'])[$_GET['id']];
}

if(!$r) {
    header('Location: index.php');
    die();
}

$MSFrameworkUtils = new \MSFramework\utils();

$settings_data = $MSFrameworkFW->getCMSData('settings');
$fw_payMethods = $settings_data['payMethods'];

$author_info = array_values($r["messages"])[0]['author_info'];
$last_update = $MSFrameworkDatabase->getAssoc("SELECT answer_date FROM marke833_framework.ticket__risposte WHERE marke833_framework.ticket__risposte.question = :id ORDER BY id DESC", array(':id' => $r['id']), true);

// Se a visualizzare il ticket non è un superadmin allora resetto il contatore dei messaggi non letti
if($_SESSION['userData']['userlevel'] == 0) {
    $MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.ticket__domande SET admin_unread = 0 WHERE id = :id", array(':id' => $_GET['id']));
}

$status_label = array(
    'label' => 'In attesa di risposta ' . $r['status'] . '%'
);

if($r['status'] > 0) {
    if($r['status'] == 100) {
        $status_label = array(
            'label' => 'Completato al 100%',
        );
    } else {
        $status_label = array(
            'label' => 'Completato al ' . $r['status'] . '%',
        );
    }
}

$progress_slider = '<small>' . $status_label['label'] . '</small><div class="progress progress-mini"><div style="width: ' . $r['status'] . '%;" class="progress-bar"></div></div>';

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewDelEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $r['id'] ?>" />
            <input type="hidden" id="record_type" value="view" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>

                            <h2 class="title-divider">
                                <b>Ticket #<?= $r['id']; ?>:</b> <?= $r['title']; ?>
                                <span class="pull-right">
                                    <?= (new \MSFramework\Framework\ticket())->getTicketCategories()[$r['category']]['title']; ?>
                                </span>
                            </h2>

                            <div class="row">

                                <div class="col-sm-2">
                                    <label>Progresso</label>
                                    <div style="margin-bottom: 15px;" id="progress_slider_info"><?= $progress_slider; ?></div>
                                </div>


                                <div class="col-sm-6">
                                    <label>Da</label>
                                    <div class="form-control"><a href="<?= (!$author_info['is_staff'] && !$r['domain_info']['from_saas'] ? $r['domain_info']['backend_url'] .  'modules/manager/utenti/list/edit.php?id=' . $author_info['id'] : 'javascript:;'); ?>"><?= $author_info['name']; ?></a> - <small><a href="<?= $r['domain_info']['http_ext'] . '//' . $r['domain_info']['url']; ?>">(<?= $r['domain_info']['http_ext'] . '//' . $r['domain_info']['url']; ?>)</a> <?= ($r['domain_info']['from_saas'] ? ' <span title="' . $r['domain_info']['database'] . '">(' . (new \MSFramework\SaaS\environments(str_replace("saas_", "", $r['domain'])))->getOwnerReadableString() . ')' : "")  ?></span></small></div>
                                </div>

                                <div class="col-sm-2">
                                    <label>Aperto il</label>
                                    <div class="form-control"><?= date("d/m/Y H:i", strtotime($r['question_date'])); ?></div>
                                </div>

                                <div class="col-sm-2">
                                    <label>Ultimo aggiornamento</label>
                                    <div class="form-control"><?= date("d/m/Y H:i", strtotime(($last_update['answer_date'] ? $last_update['answer_date'] : $r['question_date']))); ?></div>
                                </div>

                            </div>

                            <h2 class="title-divider">Messaggi</h2>

                            <div style="margin: -15px -15px -30px; padding: 15px; background: #fafafa;">

                                <?php foreach($r["messages"] as $messaggio) { ?>
                                    <div class="social-feed-separated">
                                        <div class="social-avatar">

                                            <img alt="image" src="<?= $messaggio['author_info']['avatar']; ?>">

                                        </div>

                                        <div class="social-feed-box ticket_message_container" data-message-id="<?= $messaggio['id'] ?>">
                                            <div class="social-avatar">
                                                <b><?= $messaggio['author_info']['name']; ?></b><br>
                                                <small class="text-muted"><?= $MSFrameworkUtils->smartdate(strtotime($messaggio['answer_date'])); ?> - <?= date("d/m/Y H:i", strtotime($messaggio['answer_date'])); ?></small>
                                            </div>
                                            <div class="social-body">

                                                <?php if($messaggio['from_mail']) { ?>
                                                    <div class="from_email"><i class="fa fa-envelope" title="Messaggio inviato tramite Client di posta."></i></div>
                                                <?php } ?>

                                                <?= $messaggio['message']; ?>

                                                <?php if($messaggio['is_staff'] && $r['have_unread'] == 0) { ?>
                                                    <div class="message_seen"><i class="fa fa-eye" title="Messaggio letto"></i></div>
                                                <?php } ?>

                                                <?php if(is_array($messaggio['quotation']) && count($messaggio['quotation']) > 0) { ?>
                                                <div class="hr-line-dashed"></div>

                                                <div class="row">
                                                    <div class="col-lg-9">
                                                        <div class="row">
                                                            <div class="col-lg-2">
                                                                <label>Prezzo (IVA inclusa)</label>
                                                                <div><?= CURRENCY_SYMBOL; ?> <?= $messaggio['quotation']['data']['price'] ?></div>
                                                            </div>

                                                            <div class="col-lg-3">
                                                                <label>Data di consegna stimata</label>
                                                                <div>
                                                                    <?php
                                                                    if($messaggio['quotation']['state'] == "unpaid") {
                                                                        echo (new \DateTime())->modify("+ " . $messaggio['quotation']['data']['delivery_days'] . " days")->format("d/m/Y");
                                                                    } else if($messaggio['quotation']['state'] == "paid") {
                                                                        echo \DateTime::createFromFormat("d/m/Y H:i", $messaggio['quotation']['state_change_date'])->modify("+ " . $messaggio['quotation']['data']['delivery_days'] . " days")->format("d/m/Y");
                                                                    } else {
                                                                        echo "Mai";
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-4">
                                                                <label>Metodi di pagamento accettati</label>
                                                                <div>
                                                                    <?php
                                                                    $payMethodsString = array();
                                                                    foreach($messaggio['quotation']['data']['pay_methods'] as $paymethod) {
                                                                        $payMethodsString[] = $MSFrameworki18n->getFieldValue($fw_payMethods[$paymethod]['display_name'], true);
                                                                    }

                                                                    echo implode(", ", $payMethodsString);
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3">
                                                                <label>Stato Pagamento</label>
                                                                <div><?= $MSFrameworkTicketsQuotation->formatPaymentState($messaggio['quotation']['state']) . ($messaggio['quotation']['state_change_date'] != "" ? " in data " . $messaggio['quotation']['state_change_date'] : "") ?></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3" style="text-align: right;">
                                                        <?php
                                                        if($messaggio['quotation']['state'] == "unpaid") {
                                                        ?>
                                                        <div class="m-b-md">
                                                            <a id="ticket_quotation_mark_as_paid" class="btn btn-primary" data-new-status="paid">Contrassegna come pagato</a>
                                                        </div>

                                                        <div class="m-b-md">
                                                            <a id="ticket_quotation_cancel" class="btn btn-danger" data-new-status="canceled">Annulla preventivo</a>
                                                        </div>
                                                        <?php } ?>

                                                        <?php
                                                        if($messaggio['quotation']['state'] == "paid") {
                                                            $digitalPayData = $MSFrameworkTicketsQuotation->getDigitalPaymentData($messaggio['id']);
                                                            if($digitalPayData['ipn_data'] == "") {
                                                        ?>
                                                                <div class="m-b-md">
                                                                    <a id="ticket_quotation_mark_as_unpaid" class="btn btn-danger" data-new-status="unpaid">Contrassegna come non pagato</a>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="m-b-md">
                                                                    <a id="ticket_quotation_view_digital_payment" class="btn btn-info" data-new-status="unpaid" href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML ?>framework360/assistenza/orders/edit.php?id=<?= $digitalPayData['id'] ?>">Visualizza dettagli pagamento</a>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>

                                                        <?php
                                                        if($messaggio['quotation']['state'] == "canceled") {
                                                            ?>
                                                            <div class="m-b-md">
                                                                <a id="ticket_quotation_mark_as_unpaid" class="btn btn-info" data-new-status="unpaid">Riattiva preventivo</a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>

                                                <div class="hr-line-dashed"></div>
                                                <?php } ?>

                                                <?php if($messaggio['files']) { ?>

                                                    <div class="hr-line-dashed"></div>

                                                    <div class="row" style="margin-bottom: -20px;">
                                                        <div class="col-lg-12">
                                                            <?php foreach($messaggio['files'] as $file) { ?>
                                                                <div class="file-box">
                                                                    <div class="file">
                                                                        <a download href="<?= $file['html']['main']; ?>" target="_blank">
                                                                            <span class="corner"></span>
                                                                            <?php if(@is_array(getimagesize($file['absolute']['main']))) { ?>
                                                                                <div class="image">
                                                                                    <img alt="image" class="img-responsive" src="<?= $file['html']['thumb']; ?>">
                                                                                </div>
                                                                            <?php } else { ?>
                                                                                <div class="icon">
                                                                                    <i class="fa fa-file"></i>
                                                                                </div>
                                                                            <?php } ?>
                                                                            <div class="file-name">
                                                                                <?= $file['filename']; ?>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <h2 class="title-divider">Nuovo messaggio</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <p>
                                            Azioni rapide disponibili:
                                            <br>
                                            <small><u><?= implode('</u>, <u>', (new \MSFramework\Framework\ticket())->statusFixHashtags); ?></u> o <u>#0-100%</u></small>
                                        </p>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <textarea id="descrizione" name="descrizione" class="form-control required" placeholder="Spiegaci nel dettaglio quello di cui hai bisogno, cercheremo di risponderti il prima possibile" rows="4"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-9" style="margin-bottom: 15px;">
                                    <div class="stato_ticket_container">
                                        <label>Stato del ticket</label>
                                        <div id="stato_ticket" data-current="<?php echo htmlentities($r['status']) ?>"></div>
                                    </div>
                                </div>

                                <div class="col-sm-3 text-right">
                                    <div class="i-checks" style="margin: 0 0 5px;"><label>  Imposta come Completato <i style="margin-left: 5px;"></i> <input type="checkbox" value="1" id="send_ticket_is_complete"></label></div>
                                    <div class="i-checks" style="margin: 0 0 5px;"><label> Aggiungi preventivo <i style="margin-left: 5px;"></i> <input type="checkbox" value="1" id="add_quotation"></label></div>
                                </div>
                            </div>

                            <div class="row" id="quotation_box" style="display: none; margin-bottom: 15px;">
                                <div class="col-sm-12">
                                    <label>Preventivo</label>

                                    <div style="background-color: #fafafa; border: 1px dashed #ececec; padding: 10px;">
                                        <div class="row" style="">
                                            <div class="col-md-3">
                                                <label>Prezzo (IVA inclusa)*</label>
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon"><?= CURRENCY_SYMBOL; ?></span>
                                                    <input id="quote_price" type="text" class="form-control" />
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Giorni stimati per la realizzazione</label>
                                                <input type="number" class="form-control" id="quote_delivery_days" min="1" />
                                            </div>

                                            <div class="col-md-6">
                                                <label>Metodi di pagamento accettati*</label>

                                                <div class="row paymethod_col">
                                                    <?php
                                                    foreach($fw_payMethods as $payMethodK => $payMethod) {
                                                        ?>
                                                        <div class="col-sm-4">
                                                            <div class="styled-checkbox form-control">
                                                                <label style="width: 100%;">
                                                                    <div class="checkbox i-checks pull-right"
                                                                         style="padding-top: 0px; margin-top: 0px;">
                                                                        <input type="checkbox" class="quote_paymethod <?= ($fw_payMethods[$payMethodK]['enable'] != "1" ? "disabled" : "") ?>" id="<?= $payMethodK ?>" <?= ($fw_payMethods[$payMethodK]['enable'] != "1" ? "disabled" : ($payMethod['enable'] == "1" ? "checked" : "")) ?>>
                                                                        <i></i>
                                                                    </div>
                                                                    <span style="font-weight: normal;"><?= $MSFrameworki18n->getFieldValue($payMethod['display_name'], true) ?></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-9">
                                    <label>Allegati</label>
                                    <?php (new \MSFramework\uploads('ASSISTENZA_TICKET'))->initUploaderHTML("allegati", json_encode(array()), array(), array(), false); ?>
                                </div>

                                <div class="col-sm-3 text-right">
                                    <label>&nbsp;</label>
                                    <a id="send_ticket_message" class="btn btn-success">Aggiorna Ticket</a>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    </div>

    <script>
        
        globalInitForm();
    </script>
</body>
</html>
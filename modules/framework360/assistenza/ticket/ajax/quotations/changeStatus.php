<?php
/**
 * MSAdminControlPanel
 * Date: 2019-11-01
 */

require_once('../../../../../../sw-config.php');
require('../../config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkTicketsQuotation = new \MSFramework\Framework\Ticket\quotations();

$MSFrameworkTicketsQuotation->changeStatus($_POST['message'], $_POST['status']);
?>
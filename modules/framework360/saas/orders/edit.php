<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

function makeTable($ary) {
    $html = "<table class='table table-bordered white-bg'>";
    foreach($ary as $k => $v) {
        $real_v = $v;

        if(!is_array($real_v)) {
            $json = json_decode($v, true);
            if (json_last_error() == JSON_ERROR_NONE) {
                $real_v = $json;
            }
        }

        $html .= "<tr>";
        $html .= "<th>" . $k . "</th>";
        $html .= "<td>" . (is_array($real_v) ? makeTable($real_v) : $real_v) . "</td>";
        $html .= "</tr>";
    }

    $html .= "</table>";

    return $html;
}

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`saas__transactions` WHERE id = :id", array($_GET['id']), true);
}

$settings_data = $MSFrameworkFW->getCMSData('settings');

$framework_data = json_decode($r['framework_data'], true);

$env_id = $r['env_id'];
$MSSaaSEnvironments = new \MSFramework\SaaS\environments($env_id);
$cur_saas_id = $MSSaaSEnvironments->getSaaSIDByEnvironment();
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Dettagli Ordine</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Nome SaaS</label>
                                    <?= $MSFrameworki18n->getFieldValue($MSFrameworkCMS->getCMSData("site", false, null, $cur_saas_id)['nome']) ?>
                                </div>

                                <div class="col-sm-2">
                                    <label>Nome Ambiente</label>
                                    <?= (new \MSFramework\SaaS\environments($env_id))->getOwnerReadableString() ?>
                                </div>

                                <div class="col-sm-2">
                                    <label>Email Proprietario</label>
                                    <?= $framework_data['user']['email'] ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Database</label>
                                    <?= $MSSaaSEnvironments->getDBName() ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Data Rinnovo</label>
                                    <?= (DateTime::createFromFormat("Y-m-d H:i:s", $r['insert_date']))->format("d/m/Y H:i:s") ?>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Dettagli IPN</h1>
                        <fieldset>
                            <?php
                            echo '<div class="ibox-content" style="background-color: #fefefe; padding: 0px;">' . makeTable(array_merge(array("Metodo di Pagamento" => $MSFrameworki18n->getFieldValue($settings_data['payMethods'][$r['provider']]['display_name'], true)), json_decode($r['ipn_data'], true))) . "</div>";
                            ?>

                        </fieldset>

                        <h1>Altri Dettagli</h1>
                        <fieldset>
                            <?php
                            echo '<div class="ibox-content" style="background-color: #fefefe; padding: 0px;">' . makeTable(json_decode($r['framework_data'], true)) . "</div>";
                            ?>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    ),
    array(
        'db' => 'id',
        'dt' => 'DT_buttonsLimit',
        'formatter' => function( $d, $row ) {
            return "edit";
        }
    ),
    array( 'db' => 'env_id', 'dt' => 0, 'formatter' => function($d, $row) {
        global $MSFrameworkSaaSBase, $MSFrameworki18n, $MSFrameworkCMS;

        $MSSaaSEnvironments = new \MSFramework\SaaS\environments($d);
        $cur_saas_id = $MSSaaSEnvironments->getSaaSIDByEnvironment();

        return $MSFrameworki18n->getFieldValue($MSFrameworkCMS->getCMSData("site", false, null, $cur_saas_id)['nome']);
    }),
    array( 'db' => 'env_id', 'dt' => 1, 'formatter' => function($d, $row) {
        return (new \MSFramework\SaaS\environments($d))->getOwnerReadableString();
    }),
    array( 'db' => 'framework_data', 'dt' => 2, 'formatter' => function($d, $row) {
        $r = json_decode($d, true);

        return $r['user']['email'];
    }),
    array( 'db' => 'env_id', 'dt' => 3, 'formatter' => function($d, $row) {
        return (new \MSFramework\SaaS\environments($d))->getDBName();
    }),
    array( 'db' => 'insert_date', 'dt' => 4, 'formatter' => function($d, $row) {
        return (DateTime::createFromFormat("Y-m-d H:i:s", $d))->format("d/m/Y H:i:s");
    }),
    array( 'db' => 'provider', 'dt' => 5, 'formatter' => function($d, $row) {
        global $MSFrameworkFW, $MSFrameworki18n;

        $r = $MSFrameworkFW->getCMSData('settings');
        $pay = $r['payMethods'][$d]['display_name'];
        return $MSFrameworki18n->getFieldValue($pay, true);
    }),
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.saas__transactions', 'id', $columns)
);
<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`saas__config` WHERE id = :id", array($_GET['id']), true);
}

$settings_data = $MSFrameworkFW->getCMSData('settings');
$fw_payMethods = $settings_data['payMethods'];
$saas_payMethods = json_decode($r['pay_methods'], true);
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-info">
                        E' possibile approfondire la configurazione dei sistemi SaaS all'interno dei moduli <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML ?>framework/producer_config/">Impostazioni Framework</a> ed <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML ?>manager/site/">Impostazioni Sito</a> dopo essersi loggati come SuperAdmin sul dominio SaaS che si desidera configurare.
                    </div>

                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Dominio*</label>
                                    <input id="domain" name="domain" type="text" class="form-control required" value="<?php echo htmlentities($r['domain']) ?>">
                                </div>

                                <div class="col-sm-8">
                                    <label>Note</label>
                                    <textarea id="note" name="note" class="form-control" rows="4"><?php echo htmlentities($r['note']) ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                        </fieldset>

                        <h1>Abbonamenti</h1>
                        <fieldset>
                            <div class="easyRowDuplicationContainer">
                                <?php
                                $subscriptions = json_decode($r['subscriptions'], true);
                                if(count($subscriptions) == 0) {
                                    $subscriptions[] = array("id" => 1);
                                }

                                $max_id = 0;

                                foreach($subscriptions as $subscriptionsV) {
                                    if($subscriptionsV['id'] != "" && $subscriptionsV['id'] > $max_id) {
                                        $max_id = $subscriptionsV['id'];
                                    }
                                ?>
                                    <div class="stepsDuplication rowDuplication subscriptionsDuplication">
                                        <input type="hidden" class="abb_id" value="<?php echo htmlentities($subscriptionsV['id']) ?>" />
                                        <textarea style="display: none;" class="abb_menu_granular"><?php echo $subscriptionsV['menu_granular'] ?></textarea>

                                        <div class="row m-b">
                                            <div class="col-sm-10">
                                                <label>Nome abbonamento*</label>
                                                <input type="text" class="form-control abb_name required" value="<?php echo htmlentities($subscriptionsV['name']) ?>" required>
                                            </div>

                                            <div class="col-sm-2">
                                                <label>Icona</label>
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">fa-</span>
                                                    <input type="text" class="form-control abb_icon" value="<?php echo htmlentities($subscriptionsV['icon']) ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row m-b">
                                            <div class="col-sm-2">
                                                <label>Prezzo (IVA Inclusa)*</label>
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon"><?= CURRENCY_SYMBOL; ?></span>
                                                    <input type="text" class="form-control abb_price required" value="<?php echo htmlentities($subscriptionsV['price']) ?>" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <label>Durata*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il periodo che sarà accreditato sull'abbonamento a fronte del pagamento della somma indicata"></i></span>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <input type="number" class="form-control abb_durata_value" value="<?php echo htmlentities($subscriptionsV['durata']['value']) ?>">
                                                    </div>

                                                    <div class="col-sm-8">
                                                        <select class="form-control abb_durata_type">
                                                            <option value="days" <?= ($subscriptionsV['durata']['type'] == "days" ? "selected" : "") ?>>Giorni</option>
                                                            <option value="months" <?= ($subscriptionsV['durata']['type'] == "months" ? "selected" : "") ?>>Mesi</option>
                                                            <option value="years" <?= ($subscriptionsV['durata']['type'] == "years" ? "selected" : "") ?>>Anni</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <label>&nbsp;</label>
                                                <div class="styled-checkbox form-control">
                                                    <label style="width: 100%; padding-top: 3px;">
                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                                                            <input type="checkbox" class="abb_highlight" <?= ($subscriptionsV['highlight'] == "1" ? "checked" : "") ?>>
                                                            <i></i>
                                                        </div>
                                                        <span style="font-weight: normal;">Metti in risalto</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se selezionato, questo abbonamento non sarà mostrato agli utenti ma potrà essere utilizzato internamente al sistema."></i></span>
                                                <div class="styled-checkbox form-control">
                                                    <label style="width: 100%; padding-top: 3px;">
                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                                                            <input type="checkbox" class="abb_hidden" <?= ($subscriptionsV['hidden'] == "1" ? "checked" : "") ?>>
                                                            <i></i>
                                                        </div>
                                                        <span style="font-weight: normal;">Nascosto</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <label>&nbsp;</label>
                                                <div class="styled-checkbox form-control">
                                                    <label style="width: 100%; padding-top: 3px;">
                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                                                            <input type="checkbox" class="abb_assign_on_register" <?= ($subscriptionsV['default_on_reg'] == "1" ? "checked" : "") ?>>
                                                            <i></i>
                                                        </div>
                                                        <span style="font-weight: normal;">Prova gratuita</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row m-b">
                                            <div class="col-sm-5">
                                                <label>Descrizione Breve*</label>
                                                <textarea class="form-control add_short_descr required" required><?php echo htmlentities($subscriptionsV['short_descr']) ?></textarea>
                                            </div>

                                            <div class="col-sm-5">
                                                <label>Descrizione Estesa*</label>
                                                <textarea class="form-control add_long_descr required" required><?php echo htmlentities($subscriptionsV['long_descr']) ?></textarea>
                                            </div>

                                            <div class="col-sm-2">
                                                <label>Configurazioni</label>

                                                <a class="btn btn-primary btn-facebook btn-outline btn-menuconfig btn-block">
                                                    <i class="fas fa-bars"></i> Configura Menù
                                                </a>
                                                <textarea class="abb_menu" style="display: none;"><?php echo $subscriptionsV['menu'] ?></textarea>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>
                                    </div>
                                <?php } ?>

                                <input type="hidden" id="abb_max_id" value="<?= $max_id ?>" />
                            </div>
                        </fieldset>

                        <h1>Metodi di Pagamento</h1>
                        <fieldset>
                            <div class="row paymethod_col">
                                <?php
                                $payments_to_cycle = array();
                                foreach($saas_payMethods as $saasPay) {
                                    $fw_copy = $fw_payMethods[$saasPay['id']];
                                    if(!is_array($fw_copy)) {
                                        continue;
                                    }

                                    unset($fw_copy['enable']);
                                    $payments_to_cycle[$saasPay['id']] = array_merge($saasPay, $fw_copy);
                                }

                                foreach($fw_payMethods as $fwPayK => $fwPayV) {
                                    if(!array_key_exists($fwPayK, $payments_to_cycle)) {
                                        $fwPayV['enable'] = "0";
                                        $payments_to_cycle[$fwPayK] = $fwPayV;
                                    }
                                }

                                foreach($payments_to_cycle as $payMethodK => $payMethod) {
                                ?>
                                        <div class="col-sm-2">
                                            <label>&nbsp;</label>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right"
                                                         style="padding-top: 0px; margin-top: 0px;">
                                                        <input type="checkbox" class="paymethod <?= ($fw_payMethods[$payMethodK]['enable'] != "1" ? "disabled" : "") ?>" id="<?= $payMethodK ?>" <?= ($fw_payMethods[$payMethodK]['enable'] != "1" ? "disabled" : ($payMethod['enable'] == "1" ? "checked" : "")) ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;"><?= $MSFrameworki18n->getFieldValue($payMethod['display_name'], true) ?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <?php
                                }
                                ?>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
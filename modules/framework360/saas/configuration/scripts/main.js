$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('.rowDuplication').each(function () {
        initTinyMCEModule($(this).find('.add_short_descr'));
        initTinyMCEModule($(this).find('.add_long_descr'));
    });

    menuConfigInitBtnClick();
    menuConfigInitDefaultOnReg();
    $('.paymethod_col').sortable();

    $('.rowDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi abbonamento",
        "deleteButtonText": "Elimina abbonamento",
        "beforeAddCallback": function() {
            $('.rowDuplication').find('.abb_highlight').iCheck('destroy');
            $('.rowDuplication').find('.abb_hidden').iCheck('destroy');
            $('.rowDuplication').find('.abb_assign_on_register').iCheck('destroy');
        },
        "afterAddCallback": function ($row) {
            var $lastRow =  $row.parent().find('.rowDuplication:last');
            var newIndex = $lastRow.index();

            $lastRow.find('.abb_name').val('');
            $lastRow.find('.abb_icon').val('');
            $lastRow.find('.abb_price').val('');
            $lastRow.find('.abb_id').val('');
            $lastRow.find('.abb_menu').val('');
            $lastRow.find('.abb_menu_granular').val('');
            $lastRow.find('.abb_durata_value').val('');
            $lastRow.find('.abb_durata_type').val('');

            $lastRow.find('.abb_highlight').attr('checked', false);
            $lastRow.find('.abb_hidden').attr('checked', false);
            $lastRow.find('.abb_assign_on_register').attr('checked', false);

            initIChecks();
            menuConfigInitBtnClick();
            menuConfigInitDefaultOnReg();

            // Rimuovo TinyMCE dalle textarea HTML
            if( $lastRow.find('[role="application"]').length) {
                $lastRow.find('[role="application"]').remove();

                $lastRow.find('.add_short_descr').attr('id', '').val('').show();
                initTinyMCEModule($lastRow.find('.add_short_descr'));

                $lastRow.find('.add_long_descr').attr('id', '').val('').show();
                initTinyMCEModule($lastRow.find('.add_long_descr'));
            }
        }
    });
}

function menuConfigInitBtnClick() {
    $('.btn-menuconfig').unbind('click');
    $('.btn-menuconfig').on('click', function() {
        $curbox = $(this).closest('.subscriptionsDuplication');
        $.ajax({
            url: "ajax/getMenuStruct.php",
            method: "POST",
            data : {
                "abb_id" : $curbox.find('.abb_id').val(),
                "abb_name" : $curbox.find('.abb_name').val(),
            },
            dataType: "html",
            success: function(data) {
                var dialog = bootbox.dialog({
                    title: 'Configura Menù',
                    message: data,
                    size: 'large',
                    buttons: {
                        cancel: {
                            label: "Annulla",
                            className: 'btn-danger'
                        },
                        ok: {
                            label: "Salva",
                            className: 'btn-info',
                            callback: function(){
                                $curbox.find('.abb_menu').val(JSON.stringify($('.menu_order.active_menu').nestable('serialize')));
                            }
                        }
                    }
                });

                dialog.init(function(){
                    $('.menu_order').each(function () {
                        var role = $(this).data('role');
                        $(this).nestable({
                            group: role
                        });
                    });

                    loadMenuPreset($curbox.find('.abb_id').val(), $curbox.find('.abb_menu').val(), $curbox.index());

                    $('#add_content_menu_box .add_divider').on('click', function (e) {
                        e.preventDefault();

                        var titolo = $('<div/>').text(prompt('Inserisci il titolo del divisore')).html();
                        if(titolo.length) {
                            var html = '<li class="dd-item divider" data-id="divider" data-value="' + titolo + '"><div class="dd-handle"><span class="label label-success"><i class="fa fa-bars"></i></span> <span class="text">[DIVISORE] ' + titolo + '</span> <a class="delete" onclick="$(this).closest(\'li\').remove()">Elimina</a></div></li>';
                            $(this).closest('.role_ibox').find('.active_menu > ol').append(html);
                        }
                    });

                    $('.nestable-menu').on('click', function (e) {
                        var action = $(e.target).data('action');

                        var $dd = $(this).closest('.ibox-content')

                        if (action === 'expand-all') {
                            $dd.nestable('expandAll');
                        }
                        else if (action === 'collapse-all') {
                            $dd.nestable('collapseAll');
                        }
                    });
                })
            }
        })
    })
}

function menuConfigInitDefaultOnReg() {
    $('.abb_assign_on_register').on('ifChecked', function() {
        $('.abb_assign_on_register').not($(this)).iCheck('uncheck').iCheck('update');
    })
}

function moduleSaveFunction() {
    abb_data = [];
    $('.rowDuplication').each(function() {
        abb_data.push({
            "id" : $(this).find('.abb_id').val(),
            "name" : $(this).find('.abb_name').val(),
            "icon" : $(this).find('.abb_icon').val(),
            "price" : $(this).find('.abb_price').val(),
            "menu" : $(this).find('.abb_menu').val(),
            "menu_granular" : $(this).find('.abb_menu_granular').val(),
            "highlight" : $(this).find('.abb_highlight:checked').length,
            "hidden" : $(this).find('.abb_hidden:checked').length,
            "default_on_reg" : $(this).find('.abb_assign_on_register:checked').length,
            "durata" : {
                "value" : $(this).find('.abb_durata_value').val(),
                "type" : $(this).find('.abb_durata_type').val(),
            },
            "short_descr" : tinymce.get($(this).find('.add_short_descr').attr('id')).getContent(),
            "long_descr" : tinymce.get($(this).find('.add_long_descr').attr('id')).getContent(),
        })
    })

    $.ajax({
        url: "ajax/checkAbbStatus.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pAbbonamenti": abb_data,
            "pAbbonamentiMaxID": $('#abb_max_id').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "ok") {
                window.returned_data = data;

                if(data.reassign.length == 0) {
                    continueModuleSave(false, window.returned_data.new_abb);
                } else {
                    window.dialog = bootbox.dialog({
                        title: "E' necessaria un'ulteriore azione prima di poter proseguire!",
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Attendere...</p>'
                    });

                    window.dialog.init(function(){
                        var dialog_html = "Alcuni utenti sono abbonati ai pacchetti che stai tentando di eliminare. Seleziona di seguito quale pacchetto assegnare a tali utenti al posto di quello che stai eliminando <br /><br />";

                        $.each(data.reassign, function(k, v){
                            dialog_html += "<div class='row reassign_rows'>";

                                dialog_html += "<div class='col-md-6'>";
                                    dialog_html += "<del>" + v + "</del><input type='hidden' class='from_id' value='" + k + "'>";
                                dialog_html += "</div>";

                                dialog_html += "<div class='col-md-6'>";
                                    dialog_html += "<select class='form-control to_id'>";
                                        dialog_html += "<option value=''></option>";
                                        $.each(data.new_abb, function(new_abb_k, new_abb_v) {
                                            dialog_html += "<option value='" + new_abb_v.id + "'>" + new_abb_v.name + "</option>";
                                        })
                                    dialog_html += "</select>";
                                dialog_html += "</div>";

                            dialog_html += "</div>";
                        });

                        dialog_html += '<div class="modal-footer" style="padding-right: 0; padding-left: 0; margin-top: 20px;">' +
                            '<a class="btn btn-default" onclick="(function() { window.dialog.modal(\'hide\'); })();">Annulla</a>' +
                            '<a class="btn btn-primary" onclick="continueModuleSave(true, window.returned_data.new_abb);">Conferma</a>' +
                        '</div>'

                        dialog.find('.bootbox-body').html(dialog_html);
                    });
                }
            }
        }
    })
}

function continueModuleSave(check_reassign, abb_data) {
    if(typeof(check_reassign) == 'undefined') check_reassign = false;

    reassign = {};
    if(check_reassign) {
        $('.reassign_rows').each(function () {
            from_id = $(this).find('.from_id').val();
            to_id = $(this).find('.to_id').val();

            if (from_id == "" || to_id == "") {
                bootbox.error("Per poter compiere questa operazione, devi prima indicare un nuovo abbonamento per ogni abbonamento che hai cancellato!");
                return false;
            }

            reassign[from_id] = to_id;
        })
    }

    pay_methods = [];
    $('.paymethod').each(function() {
        pay_methods.push({
            "id" : $(this).attr('id'),
            "enable" : ($(this).is(':checked') ? '1' : '0')
        })
    })
    
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pDominio": $('#domain').val(),
            "pNote": $('#note').val(),
            "pAbbonamenti": abb_data,
            "pAbbonamentiMaxID": $('#abb_max_id').val(),
            "pAbbonamentiReassign": reassign,
            "pPayMethods": pay_methods,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_paymethod_found") {
                bootbox.error("Per poter compiere questa operazione, devi prima selezionare almeno un metodo di pagamento!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "reassign_incongruent") {
                bootbox.error("C'è un incongruenza nei dati specificati per la riassegnazione degli abbonamenti. Verificare e riprovare.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}

function initTinyMCEModule($editor) {
    initTinyMCE(
        $editor,
        {
            autoresize: false
        }
    );
}

function loadMenuPreset(abb_id, cur_ordered_menu, index) {
    $.ajax({
        url: "ajax/composeMenuPreset.php",
        type: "POST",
        data: {
            "id" : abb_id,
            "cur_ordered_menu" : cur_ordered_menu,
            "loadedModuleID" : $('#loadedModuleID').val()
        },
        async: false,
        dataType: "json",
        success: function (data) {
            $('.menu_order').nestable('destroy');

            Object.keys(data).forEach(function (abb_id) {

                var preset = data[abb_id];

                $('.menu_order.active_menu.user_level_' + abb_id).html((preset[0] != "" ? '<ol class="dd-list">' + preset[0] + '</ol>' : ''));
                $('.menu_order.unused_menu.user_level_' + abb_id).html((preset[1] != "" ? '<ol class="dd-list">' + preset[1] + '</ol>' : ''));

                // Rimuovo le voci già usate dal menù 'inutilizzati'
                $('.menu_order.unused_menu.user_level_' + abb_id + ' .dd-item').each(function () {
                    var id = $(this).data('id');
                    var $li = $(this);
                    var $ol = $(this).closest('ol');

                    if ($('.menu_order.active_menu.user_level_' + abb_id).find('.dd-item[data-id="' + id + '"]').length) {
                        $li.remove();
                        if (!$ol.find('li').length) {
                            $ol.remove();
                        }
                    }

                });

            });

            $('.granular_settings_menuicon').on('click', function() {
                openGranularSettings($(this).closest('.dd-item').data('id'), index);
            })

            $('.menu_order').nestable('init');
        }
    });
}
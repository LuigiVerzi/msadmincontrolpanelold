<?php
/**
 * MSAdminControlPanel
 * Date: 2019-09-14
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');
?>

<div class="ibox role_ibox">
    <div class="ibox-title">
        <h5>Menù per <b><?= $_POST['abb_name']; ?></b></h5>
    </div>
    <div class="ibox-content" style="background: #fafafa;">

        <div class="nestable-menu" style="margin-bottom: 15px;">
            <button type="button" data-action="expand-all" class="btn btn-white btn-sm">Espandi Tutto</button>
            <button type="button" data-action="collapse-all" class="btn btn-white btn-sm">Riduci Tutto</button>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Menù Attuale</h5>
                    </div>
                    <div class="ibox-content" style="text-align: left;">
                        <div class="dd menu_order active_menu user_level_<?= $_POST['abb_id']; ?>" data-role="<?= $_POST['abb_id']; ?>">

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Voci Inutilizzate</h5>
                    </div>
                    <div class="ibox-content" style="text-align: left;">
                        <div class="dd menu_order unused_menu user_level_<?= $_POST['abb_id']; ?>" data-role="<?= $_POST['abb_id']; ?>">

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Aggiungi Contenuti</h5>
                    </div>
                    <div class="ibox-content">
                        <div id="add_content_menu_box">
                            <a href="#" class="btn btn-primary btn-outline add_divider">Aggiungi Divisore</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$max_id = $_POST['pAbbonamentiMaxID'];
foreach($_POST['pAbbonamenti'] as $abbR) {
    if($abbR['name'] == "" || $abbR['price'] == "" || $abbR['long_descr'] == "" || $abbR['short_descr'] == "") {
        echo json_encode(array("status" => "mandatory_data_missing"));
        die();
    }

    if($abbR['id'] != "" && $abbR['id'] > $max_id) {
        $max_id = $abbR['id'];
    }
}

$new_ids = array();
$old_ids = array();
$final_abbonamenti = array();
$abb_to_reassign = array();

foreach($_POST['pAbbonamenti'] as $abbK => $abbR) {
    if($abbR['id'] == "") {
        $max_id++;
        $_POST['pAbbonamenti'][$abbK]['id'] = $max_id;
    }

    $final_abbonamenti[$_POST['pAbbonamenti'][$abbK]['id']] = $_POST['pAbbonamenti'][$abbK];

    $new_ids[] = $_POST['pAbbonamenti'][$abbK]['id'];
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT subscriptions FROM `" . FRAMEWORK_DB_NAME . "`.`saas__config` WHERE id = :id", array(":id" => $_POST['pID']), true);
    $old_abbonamenti = json_decode($r_old_data['subscriptions'], true);


    foreach($old_abbonamenti as $abbonamento) {
        $old_ids[] = $abbonamento['id'];
        $old_ids_details[$abbonamento['id']] = $abbonamento['name'];
    }

    $deleted_abbs = array_diff($old_ids, $new_ids);

    if(count($deleted_abbs) != 0) {
        foreach($deleted_abbs as $abb_id) {
            if($MSFrameworkDatabase->getCount("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE subscription = :sub", array(":sub" => $abb_id)) != 0) {
                $abb_to_reassign[$abb_id] = $old_abbonamenti[$abb_id]['name'];
            }
        }
    }
}

echo json_encode(array("status" => "ok", "reassign" => $abb_to_reassign, "old_abb" => $old_ids_details, "new_abb" => $final_abbonamenti));
die();
?>
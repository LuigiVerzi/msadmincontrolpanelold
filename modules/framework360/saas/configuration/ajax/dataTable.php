<?php
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    ),
    array( 'db' => 'domain', 'dt' => 0),
    array( 'db' => 'id', 'dt' => 1, 'formatter' => function($d, $row) {
        global $MSFrameworki18n, $MSFrameworkCMS;

        return $MSFrameworki18n->getFieldValue($MSFrameworkCMS->getCMSData("site", false, null, $d)['nome']);
    }),
    array( 'db' => 'id', 'dt' => 2, 'formatter' => function($d, $row) {
        global $MSFrameworkDatabase;
        return $MSFrameworkDatabase->getCount("SELECT id FROM " . FRAMEWORK_DB_NAME . ".saas__environments WHERE saas_id = :id", array(":id" => $d));
    }),
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.saas__config', 'id', $columns)
);
<?php
/**
 * MSAdminControlPanel
 * Date: 13/10/2018
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$sw_modules = new \MSSoftware\modules();

$menu_array = array();
$userLevelID = $_POST['id'];

$cur_order = json_decode($_POST['cur_ordered_menu'], true);
if(!is_array($cur_order)) {
    $cur_order = null;
}

$menu_array[$userLevelID] = array(
    $sw_modules->printSidebarSetterOrder(null, ($cur_order ? $cur_order : false), $userLevelID),
    $sw_modules->printSidebarSetterOrder(null, false, $userLevelID)
);

echo json_encode($menu_array);
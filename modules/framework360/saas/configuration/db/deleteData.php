<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$got_error = false;
foreach($_POST['pID'] as $id) {
    $r_config = $MSFrameworkDatabase->getAssoc("SELECT `domain` FROM `" . FRAMEWORK_DB_NAME . "`.`saas__config` WHERE id = :id", array(":id" => $id), true);
    unset($SaaSEnv);
    $SaaSEnv = new \MSFramework\SaaS\environments('front', $r_config['domain']);
    $SaaSEnv->destroy();

    if($MSFrameworkDatabase->deleteRow("`" . FRAMEWORK_DB_NAME . "`.`saas__config`", "id", $id)) {
        foreach($MSFrameworkDatabase->getAssoc("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE saas_id = :id", array(":id" => $id)) as $env) {
            unset($SaaSEnv);
            $SaaSEnv = new \MSFramework\SaaS\environments($env['id']);
            $SaaSEnv->destroy();
        }
    } else {
        $got_error = true;
    }
}

if($got_error) {
    echo json_encode(array("status" => "query_error"));
} else {
    echo json_encode(array("status" => "ok"));
}
?>


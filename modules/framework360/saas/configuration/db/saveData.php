<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;
if($_POST['pDominio'] == "") {
    $can_save = false;
}

if(count($_POST['pPayMethods']) == 0) {
    echo json_encode(array("status" => "no_paymethod_found"));
    die();
}

foreach($_POST['pAbbonamenti'] as $abbR) {
    if($abbR['name'] == "" || $abbR['price'] == "" || $abbR['long_descr'] == "" || $abbR['short_descr'] == "" || $abbR['id'] == "" || $abbR['durata']['value'] == "" || $abbR['durata']['type'] == "") {
        $can_save = false;
        break;
    }
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$force_reload = false;
$new_ids = array();
$old_ids = array();
foreach($_POST['pAbbonamenti'] as $abbK => $abbR) {
    $new_ids[] = $_POST['pAbbonamenti'][$abbK]['id'];
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT subscriptions FROM `" . FRAMEWORK_DB_NAME . "`.`saas__config` WHERE id = :id", array(":id" => $_POST['pID']), true);
    $old_abbonamenti = json_decode($r_old_data['subscriptions'], true);
    foreach($old_abbonamenti as $abbonamento) {
        $old_ids[] = $abbonamento['id'];
    }
}

if($old_ids != $new_ids) {
    $force_reload = true;
}

$deleted_abbs = array_diff($old_ids, $new_ids);
foreach($deleted_abbs as $del_abb) {
    if(!array_key_exists($del_abb, $_POST['pAbbonamentiReassign']) && $MSFrameworkDatabase->getCount("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE subscription = :sub", array(":sub" => $del_abb)) != 0) {
        echo json_encode(array("status" => "reassign_incongruent"));
        die();
    }
}

$array_to_save = array(
    "domain" => $_POST['pDominio'],
    "note" => $_POST['pNote'],
    "subscriptions" => json_encode($_POST['pAbbonamenti']),
    "pay_methods" => json_encode($_POST['pPayMethods']),
);

if($db_action == "insert") {
    $array_to_save['cms'] = json_encode(
        array("producer_config" => array(
            "languages" => json_encode(array(
                "id" => "1",
                "is_primary" => "1"
            )),
        ))
    );
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`saas__config` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    (new \MSFramework\SaaS\environments())->createFrontEnvironment($_POST['pDominio']);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`saas__config` SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

foreach($_POST['pAbbonamentiReassign'] as $from => $to) {
    $MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`saas__environments` SET subscription = :new_sub WHERE subscription = :old_sub", array(":new_sub" => $to, ":old_sub" => $from));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : ''), "force_reload" => $force_reload));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE id = :id", array($_GET['id']), true);
}

$r_settings = $MSFrameworkFW->getCMSData('settings');

$MSSaaSEnvironments = new MSFramework\SaaS\environments($_GET['id']);
$MSSaaSSubscriptions = new MSFramework\SaaS\subscriptions($r['saas_id']);
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nome SaaS</label>
                                    <div><?= $MSFrameworkSaaSBase->getSaaSDataFromDB($_GET['id'], 'name')[$_GET['id']]['name']; ?></div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Email Proprietario</label>
                                    <div><?= $r['user_email'] ?></div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Database</label>
                                    <div><?= $MSSaaSEnvironments->getDBName() ?></div>
                                </div>

                                <div class="col-sm-3 text-right">
                                    <a href="#" onclick="loginInto('<?= $r['id']; ?>')" class="btn btn-success"><i class="fa fa-sign-in"></i> Accedi all'ambiente</a>
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Note</label>
                                    <textarea id="note" name="note" class="form-control" rows="4"><?php echo htmlentities($r['note']) ?></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Abbonamento</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se disabilitato, l'account principale ed i suoi figli non potranno accedere al SaaS. Solo i SuperAdmin potranno visualizzare il sito."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="active" <?php if($r['set_offline'] == "0" || $_GET['id'] == "") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <label>Scadenza Abbonamento*</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control required" id="scadenza_abbonamento" value="<?php echo ($r['expiration_date'] != '') ? date("d/m/Y", strtotime($r['expiration_date'])) : '' ?>">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <label>Tipologia Abbonamento*</label>
                                    <select class="form-control required" id="tipo_abbonamento">
                                        <?php
                                        foreach($MSSaaSSubscriptions->getAvailSubscriptions() as $sub) {
                                        ?>
                                            <option value="<?= $sub['id'] ?>" <?= ($sub['id'] == $r['subscription'] ? "selected" : "") ?>><?= $sub['name'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Ordini</h1>
                        <fieldset>
                            <table id="transactions_table" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Metodo di Pagamento</th>
                                    <th>Nome Abbonamento</th>
                                    <th>Importo</th>
                                    <th>Nuova Scadenza</th>
                                    <th style="width: 15px;"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach((new \MSFramework\SaaS\transactions())->getTransactionForEnvirorment($_GET['id']) as $transaction) {
                                    $framework_data = json_decode($transaction['framework_data'], true);
                                    $expiration_date = \DateTime::createFromFormat("Y-m-d", $framework_data['subscription']['new_expiration_date']);
                                ?>
                                    <tr>
                                        <td> <?= \DateTime::createFromFormat("Y-m-d H:i:s", $transaction['insert_date'])->format("d/m/Y H:i:s") ?> </td>
                                        <td> <?= $MSFrameworki18n->getFieldValue($r_settings['payMethods'][$transaction['provider']]['display_name'], true) ?> </td>
                                        <td> <?= $framework_data['subscription']['name'] ?> </td>
                                        <td> <?= CURRENCY_SYMBOL; ?> <?= $framework_data['subscription']['price'] ?> </td>
                                        <td> <?= ($expiration_date ? $expiration_date->format("d/m/Y") : 'N/A'); ?> </td>
                                        <td style="width: 15px;"><div class="text-right"><a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML ?>framework360/saas/orders/edit.php?id=<?= $transaction['id']; ?>" class="btn btn-warning btn-outline btn-sm"><i class="fa fa-eye"></i></a></div></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
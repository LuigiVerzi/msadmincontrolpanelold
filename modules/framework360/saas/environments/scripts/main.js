$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('#scadenza_abbonamento').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
    });

    loadStandardDataTable('transactions_table', false, {ajax: false}, false);
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNote": $('#note').val(),
            "pScadenzaAbbonamento": $('#scadenza_abbonamento').val(),
            "pTipologiaAbbonamento": $('#tipo_abbonamento').val(),
            "pSetOffline": $('#active').not(':checked').length,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}

function loginInto(data) {
    $.ajax({
        url: "ajax/generateLoginUrl.php",
        type: "POST",
        data: {
            "pID": data
        },
        async: true,
        dataType: "text",
        success: function(url) {
            if(url.length) {
                window.open(url, '_blank');
            } else {
                toastr['error']("Non è stato possibile generare un URL di autenticazione");
            }
        }
    })
}
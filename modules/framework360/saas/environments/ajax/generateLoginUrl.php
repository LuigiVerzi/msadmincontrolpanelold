<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$siteData = $MSFrameworkFW->getWebsitePathsBy('id', $_POST['pID'], true);

if($siteData) {
    echo $MSFrameworkUsers->generateGlobalLoginRedirect($siteData['backend_url']);
}

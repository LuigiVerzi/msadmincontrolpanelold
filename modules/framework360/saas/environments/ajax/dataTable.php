<?php
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSSaaSEnvironments = new \MSFramework\SaaS\environments();

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    ),
    array(
        'db' => 'id',
        'dt' => 'DT_ExtraBtn',
        'formatter' => function($d, $row) {
            Global $MSFrameworkCMS, $MSFrameworki18n;
            return array(
                array(
                    'label' => "Accedi",
                    'btn' => 'info',
                    'icon' => 'sign-in',
                    'class' => 'loginInto',
                    'function' => 'loginInto',
                    'context' => false
                )
            );
        }
    ),
    array( 'db' => 'saas_id', 'dt' => 0, 'formatter' => function($d, $row) {
        global $MSFrameworki18n, $MSFrameworkCMS, $MSSaaSEnvironments;

        return $MSFrameworki18n->getFieldValue($MSFrameworkCMS->getCMSData("site", false, null, $d)['nome']);
    }),
    array( 'db' => 'user_email', 'dt' => 1),
    array( 'db' => 'id', 'dt' => 2, 'formatter' => function($d, $row) {
        unset($MSSaaSEnvironments);
        $MSSaaSEnvironments = new \MSFramework\SaaS\environments($d);
        return $MSSaaSEnvironments->getDBName();
    }),
    array( 'db' => 'expiration_date', 'dt' => 3, 'formatter' => function($d, $row) {
        return date("d/m/Y", strtotime($d));
    })
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.saas__environments', 'id', $columns)
);
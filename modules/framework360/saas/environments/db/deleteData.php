<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($_POST['pID'] as $id) {
    unset($SaaSEnv);
    $SaaSEnv = new \MSFramework\SaaS\environments($id);
    $SaaSEnv->destroy();
}

if($MSFrameworkDatabase->deleteRow("`" . FRAMEWORK_DB_NAME . "`.`saas__environments`", "id", $_POST['pID'])) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


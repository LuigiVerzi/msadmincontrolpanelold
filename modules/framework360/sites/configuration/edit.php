<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id']) && $_GET['id']) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE id = :id", array($_GET['id']), true);
    if($MSFrameworkUsers->getUserDataFromSession('ms_agency') == 2 && $MSFrameworkUsers->getUserDataFromSession('id') != $r['owner']) {
        header('location: index.php');
        die();
    }
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big sectionWizard">

                        <h1>Dati principali</h1>
                        <fieldset>
                            <div class="row">
                                <?php if($_GET['id'] == "") { ?>
                                <div class="col-sm-3">
                                    <label>Nome Sito</label>
                                    <input id="customer_name" name="customer_name" type="text" class="form-control required">
                                </div>
                                <?php } ?>

                                <div class="col-sm-3">
                                    <label>Dominio*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= ($_GET['id'] == "" ? 'Verrà configurato automaticamente il dominio per il sito e per l\'ACP. Sarà inoltre creata la cartella per i contenuti del sito.' : 'La modifica di questo campo modificherà automaticamente le impostazioni sul server') ?>"></i></span>
                                    <input id="customer_domain" name="customer_domain" type="text" class="form-control required" value="<?php echo htmlentities($r['customer_domain']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Database*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= ($_GET['id'] == "" ? 'Il database sarà creato automaticamente con il nome impostato e con le funzionalità scelte.' : 'La modifica di questo campo modificherà automaticamente il nome del database') ?>"></i></span>
                                    <input id="customer_database" name="customer_database" type="text" class="form-control required" value="<?php echo htmlentities($r['customer_database']) ?>" placeholder="nomedatabase">
                                </div>

                                <div class="col-sm-3">
                                    <label>Crediti</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="I crediti del cliente spendibili dentro l'ecosistema FW360"></i></span>
                                    <input id="customer_credits" name="customer_credits" type="number" class="form-control required" value="<?php echo htmlentities($r['credits']) ?>" placeholder="">
                                </div>

                                <?php if($r) { ?>
                                    <div class="col-sm-3 text-right">
                                        <a href="#" onclick="loginInto('<?= $r['id']; ?>')" class="btn btn-success"><i class="fa fa-sign-in"></i> Accedi al sito</a>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-<?= ($r && json_decode($r['ftp_info']) ? '6' : 10); ?>">
                                    <label>Note</label>
                                    <textarea id="note" name="note" class="form-control" rows="4" style="min-height: <?= ($r && json_decode($r['ftp_info']) ? '130px' : '100px'); ?>"><?php echo htmlentities($r['note']) ?></textarea>
                                </div>

                                <?php if($r && json_decode($r['ftp_info'])) { ?>
                                    <?php
                                    $ftp_info = json_decode($r['ftp_info'], true);
                                    ?>
                                    <div class="col-sm-4">
                                        <label>Dati FTP</label>
                                        <b>Server:</b>
                                        <p style="margin-bottom: 5px;"><?= $_SERVER['SERVER_ADDR']; ?></p>
                                        <b>Nome Utente:</b>
                                        <p style="margin-bottom: 5px;"><?= $ftp_info['user']; ?>@<?= $r['customer_domain']; ?></p>
                                        <b>Password:</b>
                                        <p><?= $ftp_info['password']; ?></p>
                                    </div>
                                <?php } ?>

                                <div class="col-sm-2">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se disabilitato, il sito non sarà visibile al pubblico. Solo i SuperAdmin potranno visualizzare il sito."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="active" <?php if($r['set_offline'] == "0" || $_GET['id'] == "") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <?php if(!$_GET['id']) { ?>
                            <h1>Seleziona funzionalità</h1>
                            <fieldset>
                                <div class="row">
                                    <?php
                                    $extra_functions_db = json_decode($r['extra_functions'], true);
                                    $extra_functions_list = $MSFrameworkCMS->getExtraFunctionsDetails();

                                    $active_dependencies = array();
                                    foreach($extra_functions_list as $function => $functionDet) {
                                        if(isset($functionDet['dependencies']) && $functionDet['dependencies'] && in_array($function, $extra_functions_db))
                                        {
                                            $active_dependencies = array_merge($active_dependencies, $functionDet['dependencies']);
                                        }
                                    }
                                    ?>

                                    <?php foreach($extra_functions_list as $function => $functionDet) { ?>
                                        <div class="col-sm-3">
                                            <div class="styled-checkbox form-control <?= (in_array($function, $active_dependencies) ? 'dependencies' : ''); ?>">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" data-func-name="<?= $function ?>" id="extra_function_<?= $function ?>" class="extra_function_<?= $function ?> extra_function" <?php if(in_array($function, $extra_functions_db)) { echo "checked"; } ?> <?= ($functionDet['dependencies'] ? 'data-dependencies="' . htmlentities(json_encode($functionDet['dependencies'])) . '"' : ''); ?> data-original-status="<?php echo (in_array($function, $extra_functions_db) ? "1" : "0") ?>">
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;"><?= $functionDet['name'] ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </fieldset>

                            <h1>Seleziona tema</h1>
                            <fieldset>
                                <div class="row">
                                    <?php foreach((new \MSFramework\Appearance\themes())->getThemesList() as $theme_id => $theme_info) { ?>
                                        <div class="col-md-3">
                                            <div class="ibox" style="padding: 0;">
                                                <div class="ibox-content product-box" style="padding: 0;">
                                                    <a href="#" class="theme-widget" data-preview-url="<?= (new \MSSoftware\modules())->getModuleUrlByID("appearance_themes"); ?>ajax/getThemePreview.php" style="text-decoration: none; color: inherit;" data-id="<?= $theme_id; ?>">
                                                        <img src="<?= $theme_info['preview']; ?>" class="img-responsive">
                                                        <div class="product-desc">

                                                            <span class="product-name"><?= $theme_info['name']; ?></span>
                                                            <div class="small m-t-xs">
                                                                <?= $theme_info['description']; ?>
                                                            </div>
                                                        </div>
                                                        <div class="bg-muted" style="padding: 0px 15px 15px 15px;">
                                                            <div class="m-t pull-left">
                                                                <?php foreach($theme_info['functionalities'] as $functionality) { ?>
                                                                    <small class="label label-inverse"><i class="fa fa-star"></i> <?= $functionality; ?></small>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="m-t pull-right">
                                                                <div class="styled-checkbox form-control" style="margin: -7px 0;">
                                                                    <label style="width: 100%;">
                                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                            <input type="radio" name="theme_id" value="<?= $theme_id; ?>" required>
                                                                            <i></i>
                                                                        </div>
                                                                        <span style="font-weight: normal;">Seleziona tema</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div style="clear: both;"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </fieldset>
                        <?php } ?>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;
if($_POST['pDominio'] == "" ||  !preg_match('/([a-zA-Z0-9\-_]+\.)?([a-zA-Z0-9\-_]+\.)?[a-zA-Z0-9\-_]+\.[a-zA-Z]{2,5}/', $_POST['pDominio']) || $_POST['pDatabase'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$cPanelDatabase = new \cPanel\Database\database();

$_POST['pDatabase'] = $cPanelDatabase->addPrefix($_POST['pDatabase']);
$_POST['pDominio'] = str_replace("www.", "", $_POST['pDominio']);

$cPanelAdditionalDomains = new \cPanel\Domains\additionalDomains();
$cPanelSubDomains = new \cPanel\Domains\subdomains();

$ftp_info = array();

if($db_action == "insert") {
    //Creo il DB
    $dbCreation = $MSFrameworkDatabase->copyDB(DUMMY_DB_NAME, $_POST['pDatabase'], false, true);

    if($dbCreation) {
        if (count($_POST['pExtraFunctionsDiffs']['disable']) > 0) {
            foreach ($_POST['pExtraFunctionsDiffs']['disable'] as $module) {
                $module_db_prefix = $MSFrameworkCMS->getExtraFunctionsDetails($module)['db_prefix'];
                if ($module_db_prefix != "") {
                    foreach ($MSFrameworkDatabase->getFromDummyByPrefix($module_db_prefix) as $table) {
                        $MSFrameworkDatabase->query("DROP TABLE `" . $_POST['pDatabase'] . "`.`" . $table['TABLE_NAME'] . "`");
                    }
                }
            }
        }

        // Inserisco le preferenze del FW
        $array_to_save = array(
            "value" => json_encode(array(
                "extra_functions" => json_encode($_POST['pExtraFunctions']),
                "languages" => json_encode(array(
                    array(
                        'id' => 1,
                        'is_primary' => 1
                    )
                ))
            )),
            "type" => "producer_config"
        );

        $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $MSFrameworkDatabase->pushToDB("INSERT INTO `" . $_POST['pDatabase'] . "`.`cms` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

        // Inserisco i dati principali del sito
        $array_to_save = array(
            "value" => json_encode(array(
                "nome" => $MSFrameworki18n->setFieldValue('{}', $_POST['pCustomerName'])
            )),
            "type" => "site"
        );

        $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $MSFrameworkDatabase->pushToDB("INSERT INTO `" . $_POST['pDatabase'] . "`.`cms` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    }

    createDomain($_POST['pDominio']);

    // Creo l'account FTP
    $exploded_domain = explode('.', $_POST['pDominio']);
    $ftp_user = $exploded_domain[0];

    $ftp_info = array(
        'user' => preg_replace("/[^a-zA-Z0-9]+/", "", $ftp_user),
        'password' => randomPassword()
    );

} else {
    $old_site_data = $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE id = :id", array(':id' => $_POST['pID']), true);

    if($old_site_data) {
        if($old_site_data['customer_database'] !== $_POST['pDatabase']) {
            // Rinomino il database
            $db_status = $MSFrameworkDatabase->copyDB($old_site_data['customer_database'], $_POST['pDatabase'], false, true);
            if($db_status) $cPanelDatabase->delete($old_site_data['customer_database']);
            else die(json_encode(array('status' => 'db_already_exist')));
        }

        if($old_site_data['customer_domain'] !== $_POST['pDominio']) {
            $domain_status = createDomain($_POST['pDominio']);

            if($domain_status) {

                if(count(explode(".", $old_site_data['customer_domain'])) == 2) {
                    $cPanelAdditionalDomains->delete($old_site_data['customer_domain']);
                } else {
                    $cPanelSubDomains->delete($old_site_data['customer_domain']);
                }

                $cPanelSubDomains->delete('login.' . $old_site_data['customer_domain']);

            } else {
                die(json_encode(array('status' => 'db_already_exist')));
            }

            // Creo l'account FTP
            $old_ftp_info = (json_decode($old_site_data['ftp_info']) ? json_decode($old_site_data['ftp_info'], true) : array());

            $exploded_domain = explode('.', $_POST['pDominio']);

            $ftp_user =  ($old_ftp_info['user'] ? $old_ftp_info['user'] : $exploded_domain[0]);
            $ftp_password = ($old_ftp_info['password'] ? $old_ftp_info['password'] : randomPassword());

            $ftp_info = array(
                'user' => preg_replace("/[^a-zA-Z0-9]+/", "", $ftp_user),
                'password' => $ftp_password
            );

        }
    }

}

$return_id = false;

$array_to_save = array(
    "customer_domain" => $_POST['pDominio'],
    "customer_database" => $_POST['pDatabase'],
    "credits" => $_POST['pCredits'],
    "set_offline" => $_POST['pSetOffline'],
    "note" => $_POST['pNote'],
    "owner" => $MSFrameworkUsers->getUserDataFromSession('id')
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $return_id = true;
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`customers` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $insert_id = $MSFrameworkDatabase->lastInsertId();

    if(!empty($_POST['pTheme'])) {
        (new \MSFramework\Appearance\themes())->installTheme($_POST['pTheme'], true, $insert_id);
    }

} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`customers` SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $insert_id = $_POST['pID'];
}

if($ftp_info) {
    if($db_action == "insert") $ftp_info['user'] .= $insert_id;
    $MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`customers` SET ftp_info = :ftp_info WHERE id = :id", array(":id" => $insert_id, ":ftp_info" => json_encode($ftp_info)));

    if(isset($old_ftp_info) && $old_ftp_info) {
        (new \cPanel\Ftp\ftp())->delete($old_site_data['customer_domain'], $old_ftp_info['user'])->close();
    }

    (new \cPanel\Ftp\ftp())->create($_POST['pDominio'], $ftp_info['user'], $ftp_info['password'])->close();

    $return_id = true;
}


if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($return_id ? $insert_id : '')));
die();

function createDomain($domain_name) {
    Global $cPanelSubDomains, $cPanelAdditionalDomains;

    //Creo il dominio aggiuntivo/sottodominio ed il relativo dominio di login
    $domain = explode(".", $domain_name);
    if(count($domain) == 2) {
        //è un dominio di primo livello

        $cPanelAdditionalDomains->create($domain_name);

        //creo il login per l'ACP
        $cPanelSubDomains->create('login', implode(".", $domain), "/home/marke833/FRAMEWORK/MSAdminControlPanel/v1");

    } else {
        //è un sottodominio
        $domain_name = $domain[0];
        array_shift($domain);

        // Se il dominio principale è diverso da MarketingStudio allora creo prima il dominio principale
        if($domain[0] !== 'marketingstudio') {
            $cPanelAdditionalDomains->create(implode(".", $domain));
        }

        $cPanelSubDomains->create($domain_name, implode(".", $domain), "/home/marke833/FRAMEWORK/" . $domain_name . '.' . implode(".", $domain));

        //creo il login per l'ACP
        $cPanelSubDomains->create('login', $domain_name . '.' . implode(".", $domain), "/home/marke833/FRAMEWORK/MSAdminControlPanel/v1");
    }
    
    return true;
}

function randomPassword() {
$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 10; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
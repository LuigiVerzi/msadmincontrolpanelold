<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$cPanelDatabase = new \cPanel\Database\database();
$cPanelAdditionalDomains = new \cPanel\Domains\additionalDomains();
$cPanelSubDomains = new \cPanel\Domains\subdomains();

$rs = array();
foreach($_POST['pID'] as $id) {
    $tmp = $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE id = :id", array(":id" => $id), true);
    if($tmp && !empty($tmp['customer_domain'])) $rs[] = $tmp;
}

if($MSFrameworkDatabase->deleteRow("`" . FRAMEWORK_DB_NAME . "`.`customers`", "id", $_POST['pID'])) {

    foreach($rs as $r) {
        //elimino il dominio
        $dominio = str_replace("www.", "", $r['customer_domain']);

        if (count(explode(".", $dominio)) == 2) { //è un dominio di primo livello
            $cPanelAdditionalDomains->delete($dominio);
        } else { //è un sottodominio
            $cPanelSubDomains->delete($dominio);
        }

        $cPanelSubDomains->delete('login.' . $dominio);

        //elimino la cartella
        if(!empty($dominio)) {
            shell_exec("rm -rf " . MS_PRODUCTION_DOCUMENT_ROOT . $dominio . "/");
        }

        //elimino il database
        $cPanelDatabase->delete($r['customer_database']);

        //elimino il database
        if(json_decode($r['ftp_info'])) {
            (new \cPanel\Ftp\ftp())->delete($r['customer_domain'], json_decode($r['ftp_info'], true)['user']);
        }
    }

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}

?>


$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    extraFunctionIfChanged();

    $('#customer_domain, #customer_database').on('keydown change', function () {
       $(this).val($(this).val().replace(/[^a-z|A-Z|0-9|\\.|_]+/g, ''));
    });

    $('.theme-widget').on('click', function () {
        var id = $(this).data('id');

        if(window.themeDetailsModal) {
            window.themeDetailsModal.modal('hide');
        }

        $.ajax({
            url: $(this).data('preview-url'),
            type: "POST",
            data: {
                "id": id,
                "only_preview": true
            },
            async: true,
            dataType: "text",
            success: function (html) {

                window.themeDetailsModal = bootbox.dialog({
                    title: 'Dettagli tema',
                    size: 'large',
                    message: html
                });

                $('.theme-details-slider').slick({
                    dots: true
                });

                initIChecks();

            }
        });
    });
}

function onStepChangedModule(form_obj, event, currentIndex, priorIndex) {

}

function moduleSaveFunction() {

    if($("#form").data('state').currentIndex < $("#form").data('state').stepCount-1) {
        $("#form").steps('next');
        return true;
    }

    var extra_func_ary = new Array();
    var differences_extra_func = {
        "disable": [],
        "enable": [],
    };

    $('.extra_function').each(function() {
        if($(this).is(':checked')) {
            extra_func_ary.push($(this).data('func-name'));
        } else {
            differences_extra_func['disable'].push($(this).data('func-name'));
        }
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pDominio": $('#customer_domain').val(),
            "pDatabase": $('#customer_database').val(),
            "pCredits": $('#customer_credits').val(),
            "pNote": $('#note').val(),
            "pCustomerName": $('#customer_name').val(),
            "pSetOffline": $('#active').not(':checked').length,
            "pTheme": $('[name="theme_id"]:checked').val(),
            "pExtraFunctions": extra_func_ary,
            "pExtraFunctionsDiffs": differences_extra_func,
        },
        preloader: 'Configurazione del sito web in corso',
        async: true,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "db_already_exist") {
                bootbox.error("Il nome dle database inserito è già utilizzato!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                if(data.id) {
                    bootbox.alert("Il sito web è stato creato correttamente.", function () {
                        location.href = 'edit.php?id=' + data.id;
                    });
                } else {
                    succesModuleSaveFunctionCallback(data);
                }
            }
        }
    })
}

function loginInto(data) {
    $.ajax({
        url: "ajax/generateLoginUrl.php",
        type: "POST",
        data: {
            "pID": data
        },
        async: true,
        dataType: "text",
        success: function(url) {
            if(url.length) {
                window.open(url, '_blank');
            } else {
                toastr['error']("Non è stato possibile generare un URL di autenticazione");
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$sql_where = '';
if($MSFrameworkUsers->getUserDataFromSession('ms_agency') == 2) {
    $sql_where .= "WHERE owner LIKE '" . $MSFrameworkUsers->getUserDataFromSession('id') . "'";
}

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`customers` $sql_where") as $r) {
    $array['data'][] = array(
        $r['customer_domain'],
        $r['customer_database'],
        ($r['set_offline'] == "0" ? "Si" : "No"),
        "DT_RowId" => $r['id'],
        "DT_ExtraBtn" => array(
            array(
                'label' => "Accedi su " . $r['customer_domain'],
                'btn' => 'info',
                'icon' => 'sign-in',
                'class' => 'loginInto',
                'function' => 'loginInto',
                'context' => false
            )
        )
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

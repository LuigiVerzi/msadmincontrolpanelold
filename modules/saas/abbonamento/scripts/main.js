$(document).ready(function() {
    if($('.abb-highlight').length != 0) {
        $('.abb-container:not(.abb-highlight)').css('transform', 'scale(0.93)');
    }

    $('#silly_downgrade_box, #after_ipn_message').on('click', function() {
        $(this).fadeOut();
    })

    $('.priceDetails').on('click', function() {
        cur_tooltip_index = $('.priceDetails').index(this);
        console.log(cur_tooltip_index);

        var dialog = bootbox.dialog({
            message: $('.priceDetailsTextContainer:eq(' + cur_tooltip_index + ')').html(),
            buttons: {
                cancel: {
                    label: "OK",
                    className: 'btn-info'
                }
            }
        });
    })
})
<?php
require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if($MSFrameworkSaaSEnvironments->getOwnerID() != $_SESSION['userData']['user_id'] && $_SESSION['userData']['userlevel'] != "0") {
    $MSFrameworkUsers->endUserSession();
    header("location: " . ABSOLUTE_SW_PATH_HTML . "login.php?not-logged");
    die();
}

$envData = $MSFrameworkSaaSEnvironments->getEnvironmentDataFromDB($_SESSION['SaaS']['saas__environments']['id'], "user_email, expiration_date, registration_date")[$_SESSION['SaaS']['saas__environments']['id']];
$data_scadenza_abbonamento_saas = (new \DateTime($envData['expiration_date']))->format("d/m/Y");
$data_registrazione_saas = (new \DateTime($envData['registration_date']))->format("d/m/Y");
$saas_expired = $MSFrameworkSaaSEnvironments->isExpired();

$saas_id = $MSFrameworkSaaSBase->getID();

$MSFrameworkSaaSSubscriptions = new \MSFramework\SaaS\subscriptions();
?>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if($data_registrazione_saas == $data_scadenza_abbonamento_saas) {
                    ?>
                        <div class="alert alert-info">Grazie per esserti registrato su <b><?= SW_NAME ?></b>. Per iniziare ad utilizzare i nostri servizi, seleziona un abbonamento tra quelli proposti di seguito.</div>
                    <?php
                    } else {
                    ?>
                        <div class="alert alert-<?= ($saas_expired ? 'danger' : 'info') ?>">Il tuo abbonamento <?= ($saas_expired ? "è scaduto" : "scadrà") ?> il <b><?= $data_scadenza_abbonamento_saas ?></b>. <?= ($saas_expired ? 'Per continuare ad utilizzare i nostri servizi, rinnova il tuo abbonamento selezionando il pacchetto che preferisci tra quelli proposti di seguito' : '') ?></div>
                    <?php
                    }
                    ?>
                </div>

                <?php
                if(isset($_GET['cannot_downgrade'])) {
                ?>
                <div class="col-md-12" id="silly_downgrade_box" style="cursor: pointer;">
                    <div class="widget red-bg p-lg text-center">
                        <div class="m-b-md">
                            <i class="fa fa-exclamation fa-3x"></i>
                            <h3 style="margin-top: 20px;">
                                Hai ancora dei giorni di abbonamento da sfruttare. <br /> Goditi i giorni di abbonamento residui e, se lo desideri, torna quando il tuo abbonamento sarà scaduto per effettuare il downgrade.
                            </h3>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>

            <div class="row">
                <?php
                $availSubscriptions = $MSFrameworkSaaSSubscriptions->getAvailSubscriptions(true);
                $cols = round(12/count($availSubscriptions));
                foreach($availSubscriptions as $subscriptionK => $subscription) {
                    $details_in_context = $MSFrameworkSaaSSubscriptions->getDetailsInContext($subscription['id']);
                    $description_in_context = $details_in_context[2];
                ?>
                    <div class="abb-container col-lg-<?= $cols ?> <?= ($subscription['highlight'] == '1' ? 'abb-highlight' : '') ?>">
                        <div style="position: relative;">
                            <?php
                            if($subscription['highlight'] == '1') {
                            ?>
                            <div class="ribbon ribbon-top-right"><span>CONSIGLIATO</span></div>
                            <?php } ?>
                            <div class="widget navy-bg p-lg text-center box_abbonamento_top">
                                <div class="m-b-md">
                                    <i class="fa fa-<?= $subscription['icon'] ?> fa-4x"></i>
                                    <h1 class="m-xs"><?= $subscription['name'] ?></h1>
                                    <h2 class="no-margins">
                                        <?= CURRENCY_SYMBOL; ?> <?= $details_in_context[0]['iva_inclusa'] ?>
                                    </h2>
                                    
                                    <?php
                                    if($description_in_context != "") {
                                    ?>
                                        <div class="priceDetails">Dettagli Prezzo</div>
                                        <div class="priceDetailsTextContainer" style="display: none;">
                                            <div><?= $details_in_context[2] ?></div>

                                            <div style="margin-top: 10px;">
                                                <?php
                                                if($details_in_context[4]['base'] != "" && $details_in_context[4]['differenza_giorni_residui'] != "") {
                                                ?>
                                                    <div style="margin-bottom: 10px;">
                                                        <div><b>Prezzo Base:</b> <?= CURRENCY_SYMBOL; ?> <?= number_format($details_in_context[4]['base'], 2, ".", ",") ?></div>
                                                        <div><b>Differenza per giorni residui:</b> <?= CURRENCY_SYMBOL; ?> <?= number_format($details_in_context[4]['differenza_giorni_residui'], 2, ".", ",") ?></div>
                                                    </div>
                                                <?php } ?>

                                                <div><b>Nuova data di scadenza:</b> <?= $details_in_context[1] ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div style="margin-top: 15px;"><?= $subscription['short_descr'] ?></div>
                                </div>
                            </div>
                            <div class="widget p-lg text-center box_abbonamento_bottom">
                                <div class="m-b-md">
                                    <?= $subscription['long_descr'] ?>
                                </div>

                                <div class="m-b-md renew-btn-container">
                                    <?php
                                    if($description_in_context == "") {
                                    ?>
                                        <a href="javascript:void(0)">
                                            <button style="width: 130px;" type="button" class="btn btn-outline btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hai ancora dei giorni di abbonamento da sfruttare. Goditi i giorni di abbonamento residui e, se lo desideri, torna quando il tuo abbonamento sarà scaduto per effettuare il downgrade.">
                                                <div><i class="fas fa-lock"></i></div>
                                            </button>
                                        </a>
                                    <?php
                                    } else {
                                    ?>
                                        <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML ?>payway/checkout/index.php?type=saas&id=<?= $subscription['id'] ?>">
                                            <button type="button" class="btn btn-outline btn-info btn-lg">
                                                <div><?= $details_in_context[3] ?></div>
                                                <div><small style="font-size: 65%"><?= ($description_in_context == "" ? " &nbsp " : "fino al " . $details_in_context[1]) ?></small></div>
                                            </button>
                                        </a>
                                    <?php
                                    }
                                    ?>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
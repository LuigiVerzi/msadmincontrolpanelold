<?php
/**
 * MSAdminControlPanel
 * Date: 08/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$using_primary_language = $MSFrameworki18n->getPrimaryLangId() == $MSFrameworki18n->getCurrentLanguageDetails()['id'];
$r = $MSFrameworkCMS->getCMSData('camping_season');

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Stagioni</h1>
                        <fieldset>
                            <h2 class="title-divider">Stagioni</h2>
                            <div class="easyRowDuplicationContainer">
                                <?php
                                $stagioni = $r;
                                if(count($stagioni) == 0 && $using_primary_language) {
                                    $stagioni[] = array();
                                }

                                foreach($stagioni as $stagioneK => $stagioneV) {
                                    ?>
                                    <div class="stepsDuplication rowDuplication stagioniDuplication <?= (!$using_primary_language ? 'no_edit' : ''); ?>">
                                        <div class="row">
                                            <div class="stagione_container col-sm-4">
                                                <label>Stagione</label>
                                                <select class="form-control stagione_id">
                                                    <?php foreach((new \MSFramework\Camping\stagioni())->getSeasonTypes() as $s_id=>$s_name) { ?>
                                                    <option value="<?= $s_id; ?>" <?= ($stagioneV['id'] == $s_id ? 'selected' : '') ;?>><?= $s_name; ?></option>
                                                    <?php  } ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Inizio Stagione</label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control stagione_inizio" value="<?php echo ($stagioneV['inizio'] != '') ? date("d/m", $stagioneV['inizio']) : '' ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Fine Stagione</label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control stagione_fine" value="<?php echo ($stagioneV['fine'] != '') ? date("d/m", $stagioneV['fine']) : '' ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col-sm-12">
                                                <label>Note Aggiuntive</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($stagioneV['note_aggiuntive']) ?>"></i></span>
                                                <input type="text" class="form-control note_aggiuntive" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($stagioneV['note_aggiuntive'])); ?>" placeholder="">
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                    </div>
                                <?php } ?>

                                <?php if(!$using_primary_language) { ?>
                                    <div class="alert alert-warning">Per inserire nuovi prezzi utilizza la lingua madre, successivamente potrai modificarne le traduzioni da qui.</div>
                                <?php } ?>

                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
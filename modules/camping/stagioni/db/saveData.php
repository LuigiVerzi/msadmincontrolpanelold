<?php
/**
 * MSAdminControlPanel
 * Date: 27/06/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'camping_season'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

if($db_action == "update") {
    $r_old_stagioni = $MSFrameworkCMS->getCMSData('camping_season');

}

foreach($_POST['pStagioniAry'] as $k=>$stagione_aggiuntiva) {
    $_POST['pStagioniAry'][$k]['note_aggiuntive'] = $MSFrameworki18n->setFieldValue($r_old_stagioni[$k]['note_aggiuntive'], $stagione_aggiuntiva['note_aggiuntive']);
    $_POST['pStagioniAry'][$k]['custom_nome'] = $MSFrameworki18n->setFieldValue($r_old_stagioni[$k]['custom_nome'], $stagione_aggiuntiva['custom_nome']);

    $ts_scadenza = "";

    $ary_date = array(
        'inizio' => explode("/", $stagione_aggiuntiva['inizio']),
        'fine' => explode("/", $stagione_aggiuntiva['fine'])
    );

    foreach($ary_date as $date_k=>$ary_data) {
        if (count($ary_data) != 3) json_encode(array("status" => "date_format"));

        $ts_scadenza = mktime(23, 59, 59, $ary_data[1], $ary_data[0], date('Y'));
        if (!$ts_scadenza) {
            echo json_encode(array("status" => "date_format"));
            die();
        }
        $_POST['pStagioniAry'][$k][$date_k] = $ts_scadenza;
    }
}

$array_to_save = array(
    "value" => json_encode($_POST['pStagioniAry']),
    "type" => "camping_season"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'camping_season'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
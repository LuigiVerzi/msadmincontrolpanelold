function initForm() {
    initClassicTabsEditForm();

    var datepicker_settings = {
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
        changeMonth: true,
        changeYear: false,
        showButtonPanel: true,
        format: "dd/mm",
        viewMode: "months",
        maxViewMode: "months"
    };

    $('.rowDuplication .date').datepicker(datepicker_settings);

    if(!$('.stagioniDuplication').hasClass('no_edit')) {
        $('.stagioniDuplication').easyRowDuplication({
            "addButtonClasses": "btn btn-primary",
            "deleteButtonClasses": "btn btn-warning",
            "addButtonText": "Aggiungi Nuova Stagione",
            "deleteButtonText": "Elimina Stagione",
            "afterAddCallback": function () {
                $('.stagioniDuplication.rowDuplication:last').find('input[type="text"], select').val('');
                $('.stagioniDuplication.rowDuplication:last .date').datepicker(datepicker_settings);
            }
        });
    }

    $('body').on('change', '.stagione_nome', function (e) {
        var value = $(this).val();
        var $parent = $(this).closest('.stagioniDuplication');

        if(value == 'custom') {
            $parent.find('.stagione_container').attr('class', 'stagione_container col-lg-1');
            $parent.find('.custom_nome_container').show();
        } else {
            $parent.find('.stagione_container').attr('class', 'stagione_container col-lg-3');
            $parent.find('.custom_nome_container').hide();
            $parent.find('.custom_nome_container input').val('');
        }

    });
}

function moduleSaveFunction() {

    var stagioni_ary = new Object();
    $('.stepsDuplication.stagioniDuplication').each(function(row_index) {

        var is_secondary_lang = $(this).hasClass('no_edit');

        var stagioni_ary_tmp = {
            id: $(this).find('.stagione_id').val(),
            inizio: $(this).find('.stagione_inizio').val(),
            fine: $(this).find('.stagione_fine').val(),
            note_aggiuntive: $(this).find('.note_aggiuntive').val()
        };

        if( (stagioni_ary_tmp.inizio == "" || stagioni_ary_tmp.fine == "")  && !is_secondary_lang ) {
            return true;
        }

        stagioni_ary[row_index] = stagioni_ary_tmp;
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pStagioniAry": stagioni_ary,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "hour_format_not_valid") {
                bootbox.error("Il formato dell'orario deve essere HH:MM (es. 12:05).");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 08/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM camping_offerte") as $r) {
    $array['data'][] = array(
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        $MSFrameworki18n->getFieldValue($r['slug'], true),
        (new \MSFramework\Camping\offers())->getTypes($r['type']),
        $r['notti'],
        $r['prezzo'],
        ($r['scadenza'] != '') ? date("d/m/Y", $r['scadenza']) : 'Mai',
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

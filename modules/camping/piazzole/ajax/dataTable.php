<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM camping_piazzole") as $r) {

    $image = json_decode($r['gallery']);

    $array['data'][] = array(
        '<img src="' . UPLOAD_CAMPING_PIAZZOLE_FOR_DOMAIN_HTML . "tn/" . $image[0] . '" alt="" height="auto" width="80" style="margin: auto; display: block;">',
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        $MSFrameworki18n->getFieldValue($r['slug'], true),
        CURRENCY_SYMBOL . " " . $r['prezzo'],
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

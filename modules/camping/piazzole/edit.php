<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM camping_piazzole WHERE id = :id", array(":id" => $_GET['id']), true);
}

$using_primary_language = $MSFrameworki18n->getPrimaryLangId() == $MSFrameworki18n->getCurrentLanguageDetails()['id'];
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Piazzola</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome Piazzola*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Slug*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                            <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-6">
                                    <label>Sottotitolo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['sottotitolo']) ?>"></i></span>
                                    <input id="sottotitolo" name="sottotitolo" type="text" class="form-control msShort" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['sottotitolo'])) ?>">
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label> </label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['content']) ?>"></i></span>
                                    <textarea id="pageContentText" name="pageContentText"><?php echo $MSFrameworki18n->getFieldValue($r['content']) ?></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Servizi</h1>
                        <fieldset>
                            <h2 class="title-divider">Servizi disponibili per questa piazzola</h2>

                            <?php
                            $sel_services = json_decode($r['services'], true);
                            ?>
                            <div class="row text-center">
                                <?php
                                foreach((new \MSFramework\services())->getServiceDetails() as $service) {
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($service['nome']) ?>"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="<?php echo $service['id'] ?>" class="service_checkbox" <?php if(in_array($service['id'], $sel_services)) { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;"><?php echo $MSFrameworki18n->getFieldValue($service['nome']) ?></span>
                                        </label>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                        </fieldset>

                        <h1>Prezzi</h1>
                        <fieldset>
                            <h2 class="title-divider">Prezzo Base</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Prezzo Base (1 persona) <?= CURRENCY_SYMBOL; ?>*</label>
                                    <input id="prezzo" name="prezzo" type="text" class="form-control required" value="<?php echo htmlentities($r['prezzo']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <h2 class="title-divider">Prezzi Stagionali</h2>
                            <div class="easyRowDuplicationContainer">
                                <?php
                                $prezzi_stagionali = json_decode($r['prezzi_stagionali'], true);
                                if(count($prezzi_stagionali) == 0 && $using_primary_language) {
                                    $prezzi_stagionali[] = array();
                                }

                                foreach($prezzi_stagionali as $pStagionale) {
                                    ?>
                                    <div class="stepsDuplication rowDuplication stagioniDuplication <?= (!$using_primary_language ? 'no_edit' : ''); ?>">
                                        <div class="row">
                                            <div class="stagione_container col-sm-3">
                                                <label>Stagione</label>
                                                <select class="form-control stagione_id">
                                                    <option></option>
                                                    <?php foreach((new \MSFramework\Camping\stagioni())->getSeasonDetails() as $stagione) { ?>
                                                    <option value="<?= $stagione['id']; ?>" <?= ($pStagionale['id'] == $stagione['id'] ? 'selected' : '') ;?>><?= $stagione['nome']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Prezzo Stagione <?= CURRENCY_SYMBOL; ?></label>
                                                <input type="text" class="form-control stagione_prezzo" value="<?php echo htmlentities($pStagionale['prezzo']); ?>" placeholder="">
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Note Aggiuntive</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($pStagionale['note_aggiuntive']) ?>"></i></span>
                                                <input type="text" class="form-control note_aggiuntive" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($pStagionale['note_aggiuntive'])); ?>" placeholder="">
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                    </div>
                                <?php } ?>

                                <?php if(!$using_primary_language) { ?>
                                    <div class="alert alert-warning">Per inserire nuovi prezzi utilizza la lingua madre, successivamente potrai modificarne le traduzioni da qui.</div>
                                <?php } ?>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <h2 class="title-divider">Servizi Aggiuntivi</h2>
                            <div class="easyRowDuplicationContainer">
                                <?php
                                $prezzi_aggiuntivi = json_decode($r['prezzi_aggiuntivi'], true);
                                if(count($prezzi_aggiuntivi) == 0 && $using_primary_language) {
                                    $prezzi_aggiuntivi[] = array();
                                }
                                ?>

                                <?php foreach($prezzi_aggiuntivi as $pAggiuntivoK => $pAggiuntivoV) { ?>
                                    <div class="stepsDuplication rowDuplication prezziAggiuntiviDuplication <?= (!$using_primary_language ? 'no_edit' : ''); ?>">
                                        <div class="row">

                                            <div class="col-sm-3">
                                                <label>Nome</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($pAggiuntivoV['nome']) ?>"></i></span>
                                                <input type="text" class="form-control add_price_nome" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($pAggiuntivoV['nome'])) ?>" placeholder="Titolo (Es: Ospite)">
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($pAggiuntivoV['descrizione']) ?>"></i></span>
                                                <input type="text" class="form-control add_price_desc" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($pAggiuntivoV['descrizione'])) ?>" placeholder="La descrizione del servizio aggiuntivo">
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Prezzo Servizio Aggiuntivo <?= CURRENCY_SYMBOL; ?></label>
                                                <input type="text" class="form-control add_price_value" value="<?php echo htmlentities($pAggiuntivoV['prezzo']); ?>" placeholder="">
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Stagione</label>
                                                <select class="form-control add_price_season">
                                                    <option value="">Tutte</option>
                                                    <?php foreach((new \MSFramework\Camping\stagioni())->getSeasonDetails() as $stagione) { ?>
                                                        <option value="<?= $stagione['id']; ?>" <?= ($pAggiuntivoV['stagione'] == $stagione['id'] ? 'selected' : '') ;?>><?= $stagione['nome']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                    </div>
                                <?php } ?>

                                <?php if(!$using_primary_language) { ?>
                                    <div class="alert alert-warning">Per inserire nuovi prezzi servizi aggiuntivi utilizza la lingua madre, successivamente potrai modificarne le traduzioni da qui.</div>
                                <?php } ?>
                            </div>

                        </fieldset>

                        <h1>Gallery</h1>
                        <fieldset>
                            <h2 class="title-divider">Immagini</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    (new \MSFramework\uploads('CAMPING_PIAZZOLE'))->initUploaderHTML("images", $r['gallery']);
                                    ?>
                                </div>
                            </div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <input name="image" type="file" id="upload" class="hidden" onchange="">

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
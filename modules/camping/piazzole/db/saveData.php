<?php
/**
 * MSAdminControlPanel
 * Date: 05/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pPrezzo'] == "" || $_POST['pSlug'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "camping_piazzole", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

$_POST['pPrezzo'] = str_replace(",", ".", $_POST['pPrezzo']);
if(!strstr($_POST['pPrezzo'], ".")) {
    $_POST['pPrezzo'] .= ".00";
}

if(!is_numeric($_POST['pPrezzo'])) {
    echo json_encode(array("status" => "price_numeric"));
    die();
}

$uploader = new \MSFramework\uploads('CAMPING_PIAZZOLE');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT * FROM camping_piazzole WHERE id = :id", array(":id" => $_POST['pID']), true);
    $r_old_prezzi_aggiuntivi = json_decode($r_old_data['prezzi_aggiuntivi'], true);
    $r_old_prezzi_stagione = json_decode($r_old_data['prezzi_stagionali'], true);
}

$check_language_array = array(
    array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
    array('currentValue' => $_POST['pSottotitolo'], 'oldValue' => $r_old_data['sottotitolo']),
    array('currentValue' => $_POST['pageContentText'], 'oldValue' => $r_old_data['content']),
    array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
);

if(isset($r_old_prezzi_aggiuntivi) && count($r_old_prezzi_aggiuntivi)) {
    foreach($r_old_prezzi_aggiuntivi as $k=>$prezzo_aggiuntivo) {
        $check_language_array[] = array('currentValue' => $_POST['pPrezziAggiuntiviAry'][$k]['nome'], 'oldValue' => $r_old_prezzi_aggiuntivi[$k]['nome']);
        $check_language_array[] = array('currentValue' => $_POST['pPrezziAggiuntiviAry'][$k]['descrizione'], 'oldValue' => $r_old_prezzi_aggiuntivi[$k]['descrizione']);
    }
}

if(isset($r_old_prezzi_stagione) && count($r_old_prezzi_stagione)) {
    foreach($r_old_prezzi_stagione as $k=>$prezzo_stagionale) {
        $check_language_array[] = array('currentValue' => $_POST['pStagioniAry'][$k]['note_aggiuntive'], 'oldValue' => $r_old_prezzi_stagione[$k]['note_aggiuntive']);
        $check_language_array[] = array('currentValue' => $_POST['pStagioniAry'][$k]['custom_nome'], 'oldValue' => $r_old_prezzi_stagione[$k]['custom_nome']);
    }
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField($check_language_array) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}


// Prepara le stringe tracibile dei campi aggiuntivi
foreach($_POST['pPrezziAggiuntiviAry'] as $k=>$prezzo_aggiuntivo) {
    $_POST['pPrezziAggiuntiviAry'][$k]['nome'] = $MSFrameworki18n->setFieldValue($r_old_prezzi_aggiuntivi[$k]['nome'], $prezzo_aggiuntivo['nome']);
    $_POST['pPrezziAggiuntiviAry'][$k]['descrizione'] = $MSFrameworki18n->setFieldValue($r_old_prezzi_aggiuntivi[$k]['descrizione'], $prezzo_aggiuntivo['descrizione']);

    if(!is_numeric($prezzo_aggiuntivo['prezzo'])) {
        echo json_encode(array("status" => "price_numeric"));
        die();
    }

    $prezzo_aggiuntivo['prezzo'] = str_replace(",", ".", $prezzo_aggiuntivo['prezzo']);
    if(!strstr($prezzo_aggiuntivo['prezzo'], ".")) {
        $prezzo_aggiuntivo['prezzo'] .= ".00";
    }
    $_POST['pPrezziAggiuntiviAry'][$k]['prezzo'] = $prezzo_aggiuntivo['prezzo'];
}

foreach($_POST['pStagioniAry'] as $k=>$stagione_aggiuntiva) {
    $_POST['pStagioniAry'][$k]['note_aggiuntive'] = $MSFrameworki18n->setFieldValue($r_old_prezzi_stagione[$k]['note_aggiuntive'], $stagione_aggiuntiva['note_aggiuntive']);

    if(!is_numeric($stagione_aggiuntiva['prezzo'])) {
        echo json_encode(array("status" => "price_numeric"));
        die();
    }

    $_POST['pStagioniAry'][$k]['prezzo'] = $stagione_aggiuntiva['prezzo'];
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "sottotitolo" => $MSFrameworki18n->setFieldValue($r_old_data['sottotitolo'], $_POST['pSottotitolo']),
    "prezzo" => $_POST['pPrezzo'],
    "content" => $MSFrameworki18n->setFieldValue($r_old_data['content'], $_POST['pageContentText']),
    //"category" => $_POST['pCategory'],
    "gallery" => json_encode($ary_files),
    "services" => json_encode($_POST['pServicesAry']),
    "prezzi_aggiuntivi" => json_encode($_POST['pPrezziAggiuntiviAry']),
    "prezzi_stagionali" => json_encode($_POST['pStagioniAry']),
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO camping_piazzole ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE camping_piazzole SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['gallery'], true));
}

$ref_id = ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : $_POST['pID']);
$MSFrameworkUrl->makeRedirectIfNeeded('camping-offer-' . $ref_id, (new \MSFramework\Camping\piazzole())->getURL($ref_id), ($db_action === 'update' ? (new \MSFramework\Camping\piazzole())->getURL($r_old_data) : ''));

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
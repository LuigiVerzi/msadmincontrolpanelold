$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('nome', 'slug', 'camping_piazzole', $('#record_id'));
    initOrak('images', 25, 500, 300, 200, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);


    if(!$('.stagioniDuplication').hasClass('no_edit')) {
        $('.stagioniDuplication').easyRowDuplication({
            "addButtonClasses": "btn btn-primary",
            "deleteButtonClasses": "btn btn-warning",
            "addButtonText": "Aggiungi Nuova Stagione",
            "deleteButtonText": "Elimina Stagione",
            "afterAddCallback": function () {
                $('.stagioniDuplication.rowDuplication:last').find('input[type="text"], select').val('');
            }
        });
    }
    if(!$('.prezziAggiuntiviDuplication').hasClass('no_edit')) {
        $('.prezziAggiuntiviDuplication').easyRowDuplication({
            "addButtonClasses": "btn btn-primary",
            "deleteButtonClasses": "btn btn-warning",
            "addButtonText": "Aggiungi Prezzo Aggiuntivo",
            "deleteButtonText": "Elimina Prezzo Aggiuntivo",
            "afterAddCallback": function () {
                $('.prezziAggiuntiviDuplication.rowDuplication:last').find('input[type="text"], select').val('');
            }
        });
    }

    initTinyMCE( '#pageContentText');
}

function moduleSaveFunction() {
    var services_ary = new Array();
    $('.service_checkbox:checked').each(function(k, v) {
        services_ary.push($(this).attr('id'));
    })

    var stagioni_ary = new Object();
    $('.stepsDuplication.stagioniDuplication').each(function(row_index) {

        var is_secondary_lang = $(this).hasClass('no_edit');

        var stagioni_ary_tmp = {
            id: $(this).find('.stagione_id').val(),
            prezzo: $(this).find('.stagione_prezzo').val(),
            note_aggiuntive: $(this).find('.note_aggiuntive').val()
        };

        if( (stagioni_ary_tmp.id == "")  && !is_secondary_lang ) {
            return true;
        }

        stagioni_ary[row_index] = stagioni_ary_tmp;
    })

    var prezzi_ary = new Object();
    $('.stepsDuplication.prezziAggiuntiviDuplication').each(function(row_index) {

        var is_secondary_lang = $(this).hasClass('no_edit');

        var prezzi_ary_tmp = {
            nome: $(this).find('.add_price_nome').val(),
            descrizione: $(this).find('.add_price_desc').val(),
            prezzo: $(this).find('.add_price_value').val(),
            stagione: $(this).find('.add_price_season').val()
        };

        if( (prezzi_ary_tmp.nome == "" || prezzi_ary_tmp.prezzo == "") && !is_secondary_lang ) {
            return true;
        }

        prezzi_ary[row_index] = prezzi_ary_tmp;
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pSottotitolo": $('#sottotitolo').val(),
            "pPrezzo": $('#prezzo').val(),
            //"pCategory": $('#category').val(),
            "pageContentText": tinymce.get('pageContentText').getContent(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pServicesAry": services_ary,
            "pPrezziAggiuntiviAry": prezzi_ary,
            "pStagioniAry": stagioni_ary,
            "pSlug": $('#slug').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "date_format") {
                bootbox.error("Hai inserito delle date non corrette.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
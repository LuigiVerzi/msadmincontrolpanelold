<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

if($db_action == "insert") {
    (new \MSFramework\Framework\credits())->checkCreditsForAction('tracking');
}

$can_save = true;

if($_POST['pNome'] == "") {
    $can_save = false;
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT * FROM tracking__projects WHERE id = :id", array(":id" => $_POST['pID']), true);
    $actions_values_decoded = json_decode($r_old_data['actions_value'], true);
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$formattedWebsitesDomains = array();
$formattedWebsitesPaths = array();
if($_POST['pWebSite'] != '') {
    $url_exploded = explode(',', $_POST['pWebSite']);

    foreach($url_exploded as $url)
    {
        if(!preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", $url)) {
            echo json_encode(array("status" => "url_not_valid"));
            die();
        } else {
            $host = parse_url($url);
            if(!isset($host['host'])) {
                $url = 'http://' . $url;
                $host = parse_url($url);
            }
            $formattedWebsitesDomains[] = $MSFrameworkCMS->getCleanHost($host['host']);
            $formattedWebsitesPaths[] = $MSFrameworkCMS->getCleanHost($host['host']) . (isset($host['path']) ? $host['path'] : '');
        }
    }
}

if(array_unique($formattedWebsitesDomains)) {
    $integration_websites = $MSFrameworkCMS->getCMSData('integration_settings');
    if(!$integration_websites) {
        $integration_websites = array('enabled_domains' => array());
    } else {
        $integration_websites['enabled_domains'] = explode(',', $integration_websites['enabled_domains']);
    }

    $global_website_to_update = false;
    foreach($formattedWebsitesDomains as $enabled_website) {
        if(!in_array($enabled_website, $integration_websites['enabled_domains'])) {
            $integration_websites['enabled_domains'][] = $enabled_website;
            $global_website_to_update = true;
        }
    }

    if($global_website_to_update) {
        $integration_websites['enabled_domains'] = implode(',', array_unique($integration_websites['enabled_domains']));
        $MSFrameworkCMS->setCMSData('integration_settings', $integration_websites);
    }
}

// Controlla se esistono già altri siti web con il dominio
$sql_to_search = array();
$current_clean_host = $MSFrameworkCMS->getCleanHost();
if($formattedWebsitesDomains) {
    foreach ($formattedWebsitesDomains as $single_domain) {
        $sql_to_search[] = "FIND_IN_SET('$single_domain', website)";
        if($current_clean_host == $single_domain) $sql_to_search[] = "website = ''";
    }
} else {
    $sql_to_search[] = "website = ''";
    $sql_to_search[] = "FIND_IN_SET('$current_clean_host', website)";
}

$sql_to_search = implode(' OR ', $sql_to_search);

if($MSFrameworkDatabase->getCount("SELECT * FROM tracking__projects WHERE id != :id AND ($sql_to_search)", array(":id" => ($db_action == 'update' ? $_POST['pID'] : ''))) > 0) {
    echo json_encode(array("status" => "website_already_exist"));
    die();
}

/* TRADUCO LE EVENTUALI AZIONI */
foreach($_POST['pActionsValues'] as $actionID => $actionValues) {
    $_POST['pActionsValues'][$actionID] = recursiveTranslationSearch($actionValues, $actions_values_decoded[$actionID]);
}


$array_to_save = array(
    "name" => $_POST['pNome'],
    "website" => implode(',', array_unique($formattedWebsitesPaths)),
    "actions" => json_encode($_POST['pActions']),
    "actions_value" => json_encode($_POST['pActionsValues'], JSON_FORCE_OBJECT),
    "tracking_codes" => json_encode($_POST['pTrackingCodes']),
    //"tracking_values" => json_encode($_POST['pTrackingValues']),
    "is_active" => $_POST['pIsActive']
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    if((new \MSFramework\Framework\credits())->removeCreditsForActions('tracking')) {
        $result = $MSFrameworkDatabase->pushToDB("INSERT INTO tracking__projects ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    }
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE tracking__projects SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();

function recursiveTranslationSearch($actionValues, $decodedArray) {
    Global $MSFrameworki18n;

    $values = array();

    foreach($actionValues as $inputName => $inputValue) {

        if(is_array($inputValue))
        {

            if(isset($inputValue['to_translate'])) {
                if ($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                        array('currentValue' => $inputValue['value'], 'oldValue' => $decodedArray[$inputName])
                    )) == false) {
                    echo json_encode(array("status" => "no_datalang_for_primary"));
                    die();
                }

                $inputValue = $MSFrameworki18n->setFieldValue($decodedArray[$inputName], $inputValue['value']);
            }
            else {
                $inputValue = recursiveTranslationSearch($inputValue, $decodedArray[$inputName]);
            }

        }

        $values[$inputName] = $inputValue;
    }

    return $values;

}
?>
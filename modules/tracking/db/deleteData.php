<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->deleteRow("tracking__projects", "id", $_POST['pID'])) {
    $MSFrameworkDatabase->deleteRow("tracking__stats", "project_id", $_POST['pID']);
    $MSFrameworkDatabase->deleteRow("tracking__references", "project_id", $_POST['pID']);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


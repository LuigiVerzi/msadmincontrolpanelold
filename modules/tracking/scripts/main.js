$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

window.event_detail_datatable = false;

function initForm() {
    initClassicTabsEditForm();

    $('#tipo_dominio').on('change', function () {
        var $custom_domain_container = $('#custom_domain');

        if($(this).val() == 'custom') {
            $custom_domain_container.show();
            $custom_domain_container.find('.form-control').attr('required', true).addClass('required').attr('disabled', false);
        } else {
            $custom_domain_container.hide();
            $custom_domain_container.find('.form-control').attr('required', false).removeClass('required').attr('disabled', true);
        }

        toggleFW360Elements();

    });

    $('.tracking_code').on('change', function () {
        toggleFW360Elements();
    });

    $('#website').tagsInput({
        'height':'33px',
        'width':'100%',
        'interactive':true,
        'defaultText':'Aggiungi',
        'removeWithBackspace' : true,
        'onAddTag': function (value) {
            if(!isValidURL(value)) {
                $('#website').removeTag(value);
            }
            
        }
    });

    Object.keys(tracking_actions_js).forEach(function (key) {
        tracking_actions_js[key]();
    });

    if($('#grafico_resoconto').length) {
        initStatsTab();
    }

    initializeCodeMirrors();
    initializeActions();
    updateManualTrackingJavaScripts();
    toggleFW360Elements();
}

function toggleFW360Elements() {

    if($('#tipo_dominio').val() == 'custom') {
        $('#form ul li.tracking_avanzato').removeClass('hidden');

        $('[data-onlyfw360]').each(function () {
            if($(this)[0].tagName == 'OPTION') {
                $(this).prop('disabled', true).hide().data('old_selection', '1');
                if($(this).closest('optgroup').length && !$(this).closest('optgroup').find('option:not([disabled])').length) {
                    $(this).closest('optgroup').hide();
                }
                if($(this).closest('select').val() == null) {
                    $(this).closest('select').val($(this).closest('select').find('option:not([disabled])').first().val()).change();
                }
            }
        });

    } else {
        $('#form ul li.tracking_avanzato').addClass('hidden');

        $('[data-onlyfw360]').each(function () {
            if($(this)[0].tagName == 'OPTION') {
                $(this).prop('disabled', false).show();
                if(typeof($(this).data('old_selection')) !== 'undefined' && $(this).data('old_selection') == "1") {
                    $(this).data('old_selection', '0');
                    $(this).closest('select').val($(this).val()).change();
                }
                if($(this).closest('optgroup').length) {
                    $(this).closest('optgroup').show();
                }
            }
        });
    }

    if($('#linkedin_insight_tag').val() === '') $('.action_multiple_input[data-name="linkedin_insight"]').hide();
    else $('.action_multiple_input[data-name="linkedin_insight"]').show();

    if($('#ganalytics_ua').val() === '') $('.action_multiple_input[data-name="analytics"]').hide();
    else $('.action_multiple_input[data-name="analytics"]').show();

    if($('#gads_id').val() === '') $('.action_multiple_input[data-name="gads"]').hide();
    else {
        $('.action_multiple_input[data-name="gads"]').show();

        if($('#ganalytics_ua').val() !== '') {
            $('.action_multiple_input[data-name="gads"]').find('.value_container').hide();
        } else {
            $('.action_multiple_input[data-name="gads"]').find('.value_container').show();
        }
    }

    if($('#fbpixel_id').val() === '') $('.action_multiple_input[data-name="facebook"]').hide();
    else $('.action_multiple_input[data-name="facebook"]').show();

    refreshMobileTabSwitchers();

    $(window).trigger('trackingDomainChange');

}

function initializeCodeMirrors() {
    $('.codemirror').each(function () {
        if(!$(this).parent().find('.CodeMirror').length) {
            $(this).val($.trim($(this).val()));
            CodeMirror.fromTextArea($(this)[0], {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true,
                theme: "ambiance",
                readOnly: true,
                mode: {name: "javascript", json: true}
            });
        }
    });
}

function initializeActions() {

    /* GESTIONE AZIONI */
    $('.form_actions_check').on('click', '.activation_btn', function(e) {
        e.preventDefault();

        var $action = $(this).closest('.form_actions_check');
        $action.toggleClass('active');

        if ($action.hasClass('active')) {
            $action.find('.active_actions').show();
            $action.find('.inactive_actions').hide();
            $('#action_' + $action.attr('id') + '_container, #action_' + $action.attr('id') + '_tracking_container').show();
            onFormActionShow($(this).closest('.form_actions_check').attr('id'));
        } else {
            $('#action_' + $action.attr('id') + '_container, #action_' + $action.attr('id') + '_tracking_container').hide();
            onFormActionHide($(this).closest('.form_actions_check').attr('id'));
            $action.find('.active_actions').hide();
            $action.find('.inactive_actions').show();
        }

        if ($('.action_container:visible').length) {
            $('.manage_actions_section').show();
        } else {
            $('.manage_actions_section').hide();
        }
    });

    $('.form_actions_check .edit_preference').on('click', function(event) {
        var action_id = $(this).closest('.form_actions_check').attr('id');

        $('#action_' + action_id + '_container .ibox').removeClass('collapsed');
        $('#action_' + action_id + '_container .fullscreen-link').click();
    });

    attachVisualSelectorBtn();

    $('.rowDuplication').each(function () {

        var button_text = $(this).data('button_text');
        var callbackFn = (typeof($(this).data('callback_fn')) !== 'undefined' ? $(this).data('callback_fn') : false);

        $(this).easyRowDuplication({
            "addButtonClasses": "btn btn-primary",
            "deleteButtonClasses": "btn btn-warning",
            "addButtonText": "Aggiungi " + button_text,
            "deleteButtonText": "Elimina " + button_text,
            "afterAddCallback": function ($element) {

                if(callbackFn && typeof(window['onLinkDuplication']) === 'function') {
                    window['onLinkDuplication']($element);
                }

                var $last = $element.parent().find('.rowDuplication:last');

                $last.find('.form-control').each(function () {
                    if(!$(this).closest('.tracking_preference_container').length) {
                        $(this).val(($(this).find('option[value=""]').length ? '' : $(this).find('option').first().val())).change();
                    }
                });

                $last.find('.i-checks .checked').removeClass('checked');
                $last.find('.enable_external_tracking').prop('checked', false);
                $last.find('.iCheck-helper').remove();

                if($last.find('.visual_selector_input').length) {
                    $last.find('.visual_selector_input').each(function () {
                        $(this).find('.visual_selector_btn').remove();
                        $($(this).html()).replaceAll($(this));
                    });
                }

                if($last.find('.enable_external_tracking').length) {
                    $last.find('.tracking_settings_container').hide();
                }

                $last.find('.tracking_preference_container .form-control').each(function () {
                    if(typeof($(this).attr('disabled')) === 'undefined') {
                        $(this).val('');
                        if($(this).hasClass('translation-placeholder')) {
                            $(this).removeClass('translation-placeholder');
                            $(this).attr('placeholder', '');
                        }
                    }
                });

                if($last.find('.custom_javascript_alert:visible').length) {

                    /*
                    var $js_alert_element = $last.find('.custom_javascript_alert:visible');

                    $js_alert_element.parent().find('.click_to_edit_input').off('click').parent().remove();
                    $js_alert_element.parent().find('input').show();
                    $js_alert_element.remove();
                    */
                }

                if($last.find('.codemirror').length) {
                    $last.find('.CodeMirror').remove();

                    initializeCodeMirrors();

                }

                attachVisualSelectorBtn();
                initIChecks();
            },
            "afterDeleteCallback": function () {
            },
        });
    });

    $('body').on('change', '.tracking_preference_container input', function () {

        if($(this).hasClass('primary')) {
            var replaced_val = $.trim($(this).val().replace(/[^a-z|A-Z|0-9|\(|\) ]/g, ''));
        } else {
            var replaced_val = $.trim($(this).val().replace(/[^a-z|A-Z|0-9| |\}|\{|-|_]/g, ''));
        }

        $(this).val(replaced_val);

        var $manually_js_container = $(this).closest('.tracking_preference_container').find('.track_manually_js_container');
        if($manually_js_container.length) {
            updateManualTrackingJavaScripts($manually_js_container);
        }
    });

    // Se cambiamo il metodo di intercettazione allora aggiorno lo script di tracking
    $('body').on('change', '.action_container select', function () {
        if($(this).find('option[value="track_manually"]').length) {
            var $target = ($(this).closest('.action_multiple_input').length ? $(this).closest('.action_multiple_input').find('.track_manually_js_container') : $(this).closest('.action_container').find('.track_manually_js_container'));
            updateManualTrackingJavaScripts($target);
        }
    });

    // Se vogliamo selezionare un parametro extra come valore
    $('body').on('click', '.use_extra_params', function () {
        var $destination_input = $(this).closest('div').find('input');
        var $extra_parameters = $(this).closest('.tracking_preference_container').find('[data-name="trackingsuite"] .extra_parameters_container');
        var input_name = ($(this).closest('div').find('label span') ? $(this).closest('div').find('label span').first() : $(this).closest('div').find('label')).text();

        window.extra_param_destination_input = $destination_input;

        $('#extra_parameter_select').find('.extra_params_selector').html($extra_parameters.html());
        $('#extra_parameter_select').find('.extra_parameter').attr('class', 'label badge-white extra_parameter');
        $('#extra_parameter_select').data('destination', $destination_input.attr('name')).modal('show').find('.input_name').text(input_name);
    });

    $('body').on('click', '#extra_parameter_select .extra_parameter', function (e) {
        e.preventDefault();
        var name = $(this).text();
        window.extra_param_destination_input.val('{' + name + '}');
        $('#extra_parameter_select').modal('hide');
    });
}

function updateManualTrackingJavaScripts($container) {

    var manual_update = false;
    if(typeof($container) === 'undefined') {
        $container = $('.track_manually_js_container');
        manual_update = true;
    }

    $container.each(function(i, el){

        if($(this).find('.CodeMirror').length) {
            var $action_container = ($(this).closest('.rowDuplication').length ? $(this).closest('.rowDuplication') : $(this).closest('.action_container'));
            var $tracking_preferences_container = $(this).closest('.tracking_preference_container');

            var use_manual_tracking = $action_container.find('option[value="track_manually"]:selected').length;

            if (use_manual_tracking) {
                $(this).show();
                $(this).find('[name="track_manually"]').val("1");
                $action_container.find('.custom_javascript_alert').show();
                var value = $(this).find('textarea').val();
                var codeMirror = $(this).find('.CodeMirror')[0].CodeMirror;
                var function_name = $tracking_preferences_container.find('[name="event_name"]').val();

                var old_codemirror_value = codeMirror.getValue();
                var replaced_value = value.replace('*fnName*', function_name);
                codeMirror.setValue(replaced_value);
                codeMirror.autoFormatRange({line: 0, ch: 0}, {line: codeMirror.lineCount()});

                if (old_codemirror_value.indexOf('fnName') < 0 && old_codemirror_value !== codeMirror.getValue() && function_name.length) {
                    toastr['warning']('Assicurati di aggiornarlo anche nelle eventuali funzioni JavaScript utilizzate per il Tracking Avanzato', 'Evento Aggiornato');
                }

            } else {
                $(this).hide();
                $(this).find('[name="track_manually"]').val("0");
                $action_container.find('.custom_javascript_alert').hide();
            }
        }
    });

    $('.custom_javascript_alert').each(function () {

        if($(this).css('display') == 'block') {

            var msg = $(this).find('i').data('original-title');
            var $input = $(this).closest('div').find('input');

            if(!$(this).closest('.single_row').find('.click_to_edit_input').length && $input.val().length) {

                if($(this).closest('.single_row').find('.click_to_edit_input').length) {
                    $(this).closest('.single_row').find('.click_to_edit_input').parent().remove();
                }

                $input.hide();
                $input.after('<div class="ms-label-tooltip" style="cursor: default; float: none;"><a href="#" class="click_to_edit_input form-control" data-toggle="tooltip" data-placement="top" title="" data-original-title="' + msg + '">' + $input.val() + '</a></div>');
                initMSTooltip();
                $('.click_to_edit_input').one('click', function (e) {
                    e.preventDefault();
                    $input.show();
                    $(this).parent().remove();
                    $('body > .tooltip').remove();
                });
            }

        } else {
            $(this).closest('.single_row').find('.click_to_edit_input').off('click').parent().remove();
            $(this).closest('.single_row').parent().find('input').show();
            $('body > .tooltip').remove();
        }
    });


}

function onFormActionShow(id) {
    if($('#action_' + id + '_container .track_manually_js_container').length) {
        updateManualTrackingJavaScripts($('#action_' + id + '_container .track_manually_js_container'));
    }
}

function onFormActionHide(id) {

}

function onStepChangedModule(form_obj, event, currentIndex, priorIndex) {
    var currentScroll = $('html').scrollTop();
    $('.CodeMirror').each(function(i, el) {
        el.CodeMirror.refresh();
        el.CodeMirror.autoFormatRange({line:0, ch:0}, {line: el.CodeMirror.lineCount()});
    });
    $('html').scrollTop(currentScroll);
}

function getActionsAry() {
    checked_actions = new Array();
    $('.form_actions_check.active').each(function() {
        checked_actions.push($(this).attr('id'));
    });

    return checked_actions;
}

function getActionsValues() {

    var active_actions = getActionsAry();

    var actions_values = {};

    $('.action_container').each(function () {

        var $action_container = $(this);
        var action_id = $(this).data('container-of');

        var action_values = {};

        if(active_actions.indexOf(action_id) >= 0) {

            $action_container.find('.action_input').each(function () {

                var input_val = '';

                if ($(this).attr('type') == 'checkbox') {
                    input_val = ($(this).is(':checked') ? "1" : "0");
                } else {
                    input_val = $(this).val();
                }

                if ($(this).parent().find('.fa-language').length) input_val = {to_translate: 1, value: input_val};

                action_values[$(this).attr('name')] = input_val;

            });

            var childrens_object = getChildrenActionsValues($action_container, '');

            Object.keys(childrens_object).forEach(function (key) {
                action_values[key] = childrens_object[key];
            });

            actions_values[action_id] = action_values;
        }

    });

    return actions_values;

}

function getChildrenActionsValues($action_container, $parent) {

    var action_values = {};


    $action_container.find('.action_multiple_input').each(function () {

        var $action_block = $(this);

        if($parent === '') {
            var $closest_parent = $(this).closest('.action_multiple_input:not(.rowDuplication)');
        }
        else {
            var $closest_parent = $(this).closest('.rowDuplication ');
        }

        var action_name = $(this).data('name');

        if(!$closest_parent.length || ($parent !== '' && $parent.data('name') === $closest_parent.data('name')) || $parent === '') {

            if (typeof (action_values[action_name]) == 'undefined') {
                action_values[action_name] = [];
            }

            var $multiple_container = $(this);
            var multiple_values = {};


            $multiple_container.find('.action_single_input').each(function () {

                var $single_input = $(this);
                var single_input_name = $(this).attr('name');

                if ($single_input.closest('.action_multiple_input').data('name') === $multiple_container.data('name')) {
                    var input_val = '';
                    if ($single_input.attr('type') == 'checkbox') {
                        input_val = ($single_input.is(':checked') ? "1" : "0");
                    } else {
                        input_val = $single_input.val();
                    }

                    if ($single_input.parent().find('.fa-language').length) input_val = {to_translate: 1, value: input_val};

                    multiple_values[single_input_name] = input_val;
                }

            });

            if($action_block.find('.action_multiple_input').length) {
                var children_values = getChildrenActionsValues($action_block.find('.action_multiple_input').parent(), $action_block.closest('.action_multiple_input'));

                Object.keys(children_values).forEach(function (children_key) {
                    multiple_values[children_key] = children_values[children_key];
                });

            }

            if (multiple_values !== {}) {
                if ($multiple_container.hasClass('single_row')) {
                    if(typeof(action_values[action_name]) === 'undefined' || !Object.keys(action_values[action_name]).length) {
                        action_values[action_name] = {};

                        Object.keys(multiple_values).forEach(function (mk) {
                            action_values[action_name][mk] = multiple_values[mk];
                        });
                    }
                } else {
                    action_values[action_name].push(multiple_values);
                }
            }

        }


    });

    return action_values;

}

function moduleSaveFunction() {

    var tracking_codes = {};
    $('.tracking_code').each(function () {
        var name = $(this).attr('name');
        tracking_codes[name] = $(this).val();
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pWebSite": ($('#tipo_dominio').val() == 'custom' ? $('#website').val() : ''),
            "pActions": getActionsAry(),
            "pActionsValues": getActionsValues(),
            "pTrackingCodes": tracking_codes,
            "pIsActive": $('#is_active:checked').length,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "url_not_valid") {
                bootbox.error("Hai inserito un dominio non valido.");
            } else if(data.status == "website_already_exist") {
                bootbox.error("Esiste già un progetto per il seguente sito web, aggiungi un URL diverso o modifica il progetto già esistente.");
            } else if(data.status == "ok") {
                if(data.id !== '') {
                    $("#code_to_include").show();
                    $('#form ul li.hidden').removeClass('hidden');
                    toggleFW360Elements();
                }
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}

function initStatsTab() {

    var grafico_data = $.parseJSON($('#dati_resoconto').val());
    grafico_data.colori = [];

    Object.keys(grafico_data.valori).forEach(function (val) {
        grafico_data['colori'].push('#' + (function co(lor){   return (lor +=
            [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f'][Math.floor(Math.random()*16)])
        && (lor.length == 6) ?  lor : co(lor); })(''));
    });

    new Chart($('#grafico_resoconto'), {
        type: 'doughnut',
        data: {
            datasets: [{
                data: grafico_data.valori,
                backgroundColor: grafico_data.colori
            }],
            labels: grafico_data.chiavi
        }
    });

    /* TABELLA RESOCONTO */
    var datatable_eventi = loadStandardDataTable('dettagli_eventi', false, {ajax: false}, false);

    // Array to track the ids of the details displayed rows
    $('#dettagli_eventi tbody').on( 'click', '.view_details', function (e) {
        e.preventDefault();

        var tr = $(this).closest('tr');
        var row = datatable_eventi.row( tr );
        var project_id = $(this).data('project_id');
        var event_name = $(this).data('event_name');

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            tr.find('.view_details').removeClass('btn-danger').addClass('btn-primary').find('i').attr('class', 'fa fa-eye');
            row.child.hide();
            window.event_detail_datatable.destroy();
        } else {

            if($('#dettagli_eventi tbody tr.details').length) {
                $('#dettagli_eventi tbody tr.details .view_details').click();
            }

            tr.addClass( 'details' );
            tr.find('.view_details').removeClass('btn-primary').addClass('btn-danger').find('i').attr('class', 'fa fa-close');

            row.child( '<div id="single_event_detail"></div>' ).show();
            getEventDetail(project_id, event_name);
        }
    });

    $('#dettagli_eventi').on('click', '.svuota_evento', function (e) {
        e.preventDefault();

        var event_name = $(this).data('event_name');
        var project_id = $(this).data('project_id');

        var $riga = $('#dettagli_eventi tr.details');

        bootbox.confirm('Sei sicuro di voler svuotare la lista degli eventi? Tutti i dati andranno persi definitivamente', function (result) {
            if (result) {
                $.ajax({
                    url : 'ajax/events/deleteEventHistories.php',
                    type : 'POST',
                    data : {
                        'event_name' : event_name,
                        'project_id': project_id
                    },
                    success : function(data) {
                        if(data === "1") {

                            var riga = datatable_eventi.row( $riga );
                            $riga.find('.view_details').click();
                            riga.remove();
                            datatable_eventi.draw();

                            toastr["success"]("La cronologia di tracking dell'evento " + event_name + " è stata eliminata definitivamente", "Cronologia Eliminata", {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "preventDuplicates": true,
                                "showDuration": "3000",
                                "hideDuration": "3000",
                                "timeOut": 3000,
                            });

                        }

                    }
                });
            }
        });

    });

}

function getEventDetail(project_id, event_name) {
    $.ajax({
        url : 'ajax/events/getEventDetails.php',
        type : 'POST',
        data : {
            'event' : event_name,
            'project_id': project_id
        },
        success : function(data) {
            $('#single_event_detail').html(data);
            window.event_detail_datatable = loadStandardDataTable('dettaglio_evento', true, {ajax: 'ajax/events/dataTable.php?event=' + event_name + '&project_id=' + project_id}, false);
        }
    });
}

function isValidURL(string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if (res == null)
        return false;
    else
        return true;
}
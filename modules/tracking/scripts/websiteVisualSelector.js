function initVisualSelector(preferences) {

    var connectionCheckTimeout = false;

    /* INIZIALIZZO GLI EVENTI DI RISPSOTA */
    var onIframeMessage = function (event) {

        if(event.data.cmd === 'onSelectionFunction') {

            var selector = event.data.selector;
            var is_form = event.data.is_form;
            var extra = event.data.extra;

            if (selector !== '') {
                $('#autocompiler_visual_selector_modal').data('selector', selector).data('is_form', is_form).data('extra', extra).modal('show').find('.element_selector').text(selector);

                if (typeof (preferences.onSelect) == 'function') {
                    preferences.onSelect(selector, extra);
                }
            } else {
                var return_standard = true;
                if (typeof (preferences.onSelectionError) == 'function') {
                    if (!preferences.onSelectionError()) {
                        return_standard = false;
                    }
                }
                if (return_standard) {
                    bootbox.error("Non è stato selezionato un elemento valido per questo tipo di selezione.");
                }
            }

        }
        else if(event.data.cmd === 'onIframeUnload') {
            $('#visual_selector_autocompiler_iframe').addClass('loading');
            $('#get_visual_selector_helper .iframe_container').show();
            $('#get_visual_selector_helper .iframe_error_container').hide();
        }
        else if(event.data.cmd === 'connected') {
            $('#visual_selector_autocompiler_iframe').removeClass('loading');
            if(connectionCheckTimeout !== false) {
                clearTimeout(connectionCheckTimeout);
                connectionCheckTimeout = false;

                $('#visual_selector_autocompiler_iframe').data('origin_url', event.data.url);
                $('#current_visual_selector_iframe_link').html(event.data.url);

                $('#get_visual_selector_helper .iframe_container').show();
                $('#get_visual_selector_helper .iframe_error_container').hide();
            }
        }
    };

    if(!$(".visual_selector_container").length) {
        $('#form .content').append('<div class="visual_selector_container">' + $('#visual_selector_template').val() + '</div>');
    } else {
        $("#get_visual_selector_helper").off('click');
    }

    /* IMPOSTO IL TITOLO */
    $('#get_visual_selector_helper h5 .title').text(preferences.title);

    var multiple_urls = [];
    var website_url = '';

    if($('#tipo_dominio').val() == 'default') {
        website_url = $('#baseElementPathSW').val();
        multiple_urls.push($('#baseElementPathSW').val());

    } else {
        if($('#website').val().length) {
            multiple_urls = $('#website').val().split(',');
            website_url = multiple_urls[0];
        }
    }

    if(multiple_urls.length > 1) {
        $('#visual_selector_link_website_switch').show();
        $('#visual_selector_link_website_switch').html('');
        multiple_urls.forEach(function (link, key) {
            $('#visual_selector_link_website_switch').append('<a href="#" class="btn btn-info ' + (key === 0 ? 'active' : 'btn-outline') + '" data-url="' + link + '"><span class="active_string">Sei su</span><span class="inactive_string">Passa su</span> ' + link + '</a>');
        });

    } else {
        $('#visual_selector_link_website_switch').hide();
    }

    if(typeof(preferences.url) !== 'undefined' && preferences.url.length) {
        website_url = preferences.url;
    }

    $('#current_visual_selector_iframe_link').html(website_url);

    if($(this).closest('.ibox').hasClass('fullscreen')) {
        $(this).closest('.ibox').find('.fullscreen-link').click();
    }

    if(!$('#visual_selector_autocompiler_iframe:visible').length) {
        $('#get_visual_selector_helper').show().find('.ibox').addClass('fullscreen');
        $('body').addClass('fullscreen-ibox-mode');
    }

    if(website_url.indexOf('http') < 0) {
        website_url = 'http://' + website_url;
    }

    $('#visual_selector_autocompiler_iframe').off('load').data('origin_url', website_url).on('load', function () {

        $("#visual_selector_autocompiler_iframe")[0].contentWindow.postMessage(JSON.parse(JSON.stringify({
            cmd: "initVisualSelector",
            preferences: preferences
        })), "*");

        connectionCheckTimeout = setTimeout(function () {
            $('#get_visual_selector_helper .iframe_container').hide();
            $('#get_visual_selector_helper .iframe_error_container').show().html("<div><h2>Non abbiamo trovato il codice di Tracking 😟</h2>" + $('#code_to_include').html() + "</div><a href=\"#\" style=\"margin-top: 15px;\" class=\"connection-retry btn btn-primary btn-lg\">Ho aggiunto il codice</a>");
        }, 3000);

    }).attr('src', website_url);

    $("#visual_selector_link_website_switch a").off('click').on('click', function(e) {
        e.preventDefault();

        var link = $(this).data('url');

        $('#visual_selector_link_website_switch a').removeClass('active').addClass('btn-outline');
        $(this).addClass('active').removeClass('btn-outline');

        $('#current_visual_selector_iframe_link').html($(this).data('url'));
        $('#visual_selector_autocompiler_iframe').addClass('loading').attr('src', link);

        if(connectionCheckTimeout !== false) {
            clearTimeout(connectionCheckTimeout);
            connectionCheckTimeout = false;
        }

        $('#get_visual_selector_helper .iframe_container').show();
        $('#get_visual_selector_helper .iframe_error_container').hide();

    });

    $("#get_visual_selector_helper  .connection-retry").off('click').on('click', function(e) {
        e.preventDefault();

        $('#visual_selector_autocompiler_iframe').addClass('loading').attr('src', $('#visual_selector_autocompiler_iframe').data('origin_url')).load();

        $('#get_visual_selector_helper .iframe_container').show();
        $('#get_visual_selector_helper .iframe_error_container').hide();

    });

    $("#get_visual_selector_helper .close_autocompile").off('click').on('click', function(e) {
        e.preventDefault();

        if($('.ibox.fullscreen').length <= 1) {
            $('body').removeClass('fullscreen-ibox-mode');
        }
        $('#get_visual_selector_helper').hide().find('.ibox').removeClass('fullscreen');
        $('#visual_selector_autocompiler_iframe').off('load').attr('src', 'about:blank;').off('unload');
        window.removeEventListener("message", onIframeMessage, true);
        $('#visual_selector_autocompiler_iframe').addClass('loading');

    });

    $("#confirm_visual_selector_selection").off('click').on('click', function(e) {
        e.preventDefault();

        var selection_info = $('#autocompiler_visual_selector_modal').data();
        $('#autocompiler_visual_selector_modal').modal('hide');
        preferences.onSelectionConfirm(selection_info.selector, preferences.destination_input, selection_info.is_form, selection_info.extra);
    });

    window.removeEventListener("message", onIframeMessage, true);
    window.addEventListener("message", onIframeMessage, true);

}

function attachVisualSelectorBtn() {

    $(':not(.visual_selector_input) > .visual-selector').each(function () {

        var $original_input = $(this);
        var $container = $('<div class="visual_selector_input">' + $original_input[0].outerHTML + '<a href="#" class="visual_selector_btn carrello_helper_button"><i class="fa fa-magic" aria-hidden="true"></i></a></div>').replaceAll($(this));
        var $input = $container.find('.visual-selector');

        $container.on('click', '.visual_selector_btn', function(e) {
            e.preventDefault();

            var title = $input.data('visual-title');

            var url = '';
            if($('#visual_selector_autocompiler_iframe').length && /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/.test($('#visual_selector_autocompiler_iframe').data('origin_url'))) {
                url = $('#visual_selector_autocompiler_iframe').data('origin_url');
            }

            initVisualSelector({
                title: title,
                destination_input: $input,
                url: url,
                onSelect: function(selector) {},
                onSelectionError: function() {},
                onSelectionConfirm: function(selector, $destination_input, is_form) {
                    $destination_input.val(selector);
                    if($destination_input.closest('[class*="col-md"]').next().find('.visual-selector').length && !$destination_input.closest('[class*="col-md"]').next().find('.visual-selector').val().length) {
                        $destination_input.closest('[class*="col-md"]').next().find('.visual_selector_btn').click();
                        toastr['info']("Passaggio a<br><u>" + $destination_input.closest('[class*="col-md"]').next().find('.visual-selector').data('visual-title') + "</u>", "Selezione confermata!");
                    } else {
                        $("#get_visual_selector_helper .close_autocompile").click();
                    }
                }
            });

        });

    });
}
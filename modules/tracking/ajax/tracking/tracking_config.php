<h2 class="title-divider">Preferenze</h2>

<?php

$enable_external_tracking = (is_null($duplicationK) || !isset($single_tracking_values['enable_external_tracking']) ? $action_values : $single_tracking_values)['enable_external_tracking'];

foreach($tracking_params as $platform_id => $tracking_platform) {

    $section_class = '';
    $section_style = '';
    if($platform_id !== 'trackingsuite') {
        $section_class = 'tracking_platform ' . $platform_id;
        $section_style = 'border-color: ' . $tracking_platform['color'] . ';';
    }

    echo '<div class="row action_multiple_input single_row ' . $section_class . '" data-name="' . $platform_id . '" style="' . $section_style . '">';

    if($platform_id !== 'trackingsuite') {
        echo '<h2 style="background: ' . $tracking_platform['color'] . ' !important;">Eventi ' . $tracking_platform['name'] . '</h2>';
    }

    if($tracking_platform['icon']) {
        echo '<i class="tracking_platform_icon fa ' . $tracking_platform['icon'] . '"></i>';
    }

    $default_inputs = array();
    $default_placeholders = array();
    if($action['params'] && $action['params']['platforms'][$platform_id] && $action['params']['platforms'][$platform_id]) {
        $default_inputs = (isset($action['params']['platforms'][$platform_id]['inputs']) ? $action['params']['platforms'][$platform_id]['inputs'] : array());
    }

    foreach($tracking_params[$platform_id]['params'] as $param_key => $param_values) {

        $is_blocked = false;
        $track_value = '';

        if(isset($default_inputs[$param_key]) && $default_inputs[$param_key]['value']) {
            $track_value = $default_inputs[$param_key]['value'];
            $is_blocked = ($default_inputs[$param_key]['disabled'] ? true : false);
        }

        if(!is_null($duplicationK)) {
            if(!empty($single_tracking_values[$platform_id][$param_key])) {
                $track_value = $single_tracking_values[$platform_id][$param_key];
            }
        } else if(isset($action_values[$platform_id][$param_key]) && !empty($action_values[$platform_id][$param_key])) {
            $track_value = $action_values[$platform_id][$param_key];
        }

        if(isset($default_inputs[$param_key]) && $default_inputs[$param_key]['placeholder'])  $placeholder = $default_inputs[$param_key]['placeholder'];
        else $placeholder = '';

        $append_to_label = '';
        if(isset($action['manual_tracking']) && $action['manual_tracking'] && $param_key == 'event_name') {
            $append_to_label = '<span class="ms-label-tooltip m-l-sm custom_javascript_alert"><i class="fa fa-exclamation-triangle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Attenzione! Se modifichi il nome dell\'evento devi assicurarti di aggiornarlo anche nelle eventuali funzioni di tracking msTrack."></i></span>';
        }

        if(isset($action['params']) && $action['params']['extra_params'] && $param_values['use_extra_params']) {
            $append_to_label = '<span class="ms-label-tooltip m-l-sm use_extra_params"><i class="fa fa-hand-lizard-o"></i> <small>Valore dinamico</small></span>';
        }

        $input_type = (isset($param_values['type']) ? $param_values['type'] : 'text');

        // Se non è stato impostato nessun valore, il campo è richiesto e numerico allora
        if($track_value < 1 && $input_type == "number") {

        }

        echo '<div class="col-lg-3 col-sm-6 ' . $param_key . '_container">';
        echo '<label>' . $param_values['name'] . '</label> ' . $append_to_label . ($param_values['translate'] && !$is_blocked ? '<span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . TRANSLATE_ICON_TOOLTIP . '" data-translations="' . htmlentities($track_value) . '"></i></span>' : '');
        echo '<input name="' . $param_key . '" type="' . $input_type  . '" placeholder="' . htmlentities($placeholder) . '" class="form-control action_single_input' . ($param_values['required'] == true ? ' required' : '') . ($param_values['primary'] == true ? ' primary' : '') . '" value="' . htmlentities( $MSFrameworki18n->getFieldValue($track_value) )  . '" ' . ($param_values['required'] == true ? 'required' : '') . ' ' . ($is_blocked ? 'disabled' : '') . '>';
        echo '</div>';
    }

    if($action['params']) {

        if($platform_id === 'trackingsuite' && $action['params']['extra_params']) {
            echo '<div class="col-sm-6">
                    <div class="extra_params_box">
                        <label class="params_description_label">Se disponibili saranno inviati anche i seguenti parametri extra:</label>
                        <br>
                        <div class="extra_parameters_container">
                            <span class="label label-success extra_parameter">' . implode('</span><span class="label label-success extra_parameter">', array_keys($action['params']['extra_params'])) . '</span>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>';
        } else if (isset($action['params']['platforms']) && isset($action['params']['platforms'][$platform_id]) && isset($action['params']['platforms'][$platform_id]['extra_params'])) {
            $extra_params_matches = $action['params']['platforms'][$platform_id]['extra_params'];

            foreach($extra_params_matches as $param_key => $param_values) {

                $track_value = $param_values['default'];
                $is_required = $param_values['required'];
                $placeholder = ($param_values['placeholder'] ? $param_values['placeholder'] : (!empty($param_values['match']) ? '{' . $param_values['match'] . '}' : ''));
                $input_type = (in_array($param_values['type'], array('int', 'float')) && $param_values['match'] === false ? 'number' : 'text');

                $append_to_field = '';
                if($param_values['match'] !== false ) {
                    $append_to_field .= '<span class="ms-label-tooltip m-l-sm use_extra_params"><i class="fa fa-hand-lizard-o"></i> <small>Valore dinamico</small></span>';
                }

                if(in_array($param_values['type'], array('int', 'float'))) {
                    $append_to_field .= '<span class="ms-label-tooltip m-l-sm"><i class="fa fa-sort-numeric-desc" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il valore passato deve essere necessariamente numerico!"></i></span>';
                }

                $append_to_field .= '<span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . htmlentities($param_values['description']) . '"></i></span>';

                if (!is_null($duplicationK) && !empty($single_tracking_values[$platform_id][$param_key])) {
                    $track_value = $single_tracking_values[$platform_id][$param_key];
                } else if (isset($action_values[$platform_id][$param_key]) && !empty($action_values[$platform_id][$param_key])) {
                    $track_value = $action_values[$platform_id][$param_key];
                }

                // Se non è stato impostato nessun valore, il campo è richiesto e numerico allora
                if($track_value < 1 && $input_type == "number") {
                    $track_value = 1;
                }

                echo '<div class="col-lg-3 col-sm-6 ' . $param_key . '_container">';
                echo '<label>' . $param_key . '</label> ' . $append_to_field;
                echo '<input name="' . $param_key . '" type="' . $input_type . '" ' . ($input_type == "number" ? 'min="1"' : '') . ' placeholder="' . $placeholder . '" class="form-control action_single_input' . ($is_required ? ' required' : '') . '" value="' . htmlentities($track_value) . '" ' . ($is_required ? 'required' : '') . '>';
                echo '</div>';
            }

        }
    }

    echo '</div>';

    if($platform_id == 'trackingsuite') {
        echo '<div class="tracking_settings_container" style="display: ' . ($enable_external_tracking == "1" ? 'block' : 'none') . ';">';
    }

}
echo '</div>';
?>

<?php if(isset($action['manual_tracking']) && $action['manual_tracking']) { ?>
<div class="track_manually_js_container" style="display: <?= ($enable_external_tracking == "1" ? 'block' : 'none'); ?>">
    <h2 class="title-divider">Codice da Includere</h2>

    <div class="alert alert-info" style="margin-bottom: 15px;"><?= $action['manual_tracking']['description']; ?></div>

    <?php
    $manual_fn_params_string = '';
    if(isset($action['params']) && $action['params']['extra_params']) {
        $params_to_append = array();
        $i = 1;
        foreach($action['params']['extra_params'] as $param => $info) {
            $params_to_append[] = $param . ": " . (isset($info['default']) ? (is_numeric($info['default']) ? $info['default'] : "'" . $info['default'] . "'") : "''") . ($i < count($action['params']['extra_params']) ? ',' : '') . " // (" . $info['type'] . ") " . $info['description'] . " - (" . ($info['required'] ? 'Richiesto' : 'Opzionale') . ")";
            $i++;
        }
        $manual_fn_params_string = ', {' . implode(PHP_EOL, $params_to_append) . PHP_EOL . '}';
    }
    ?>
    <textarea class="codemirror" disabled>
        <script>
            msTrack('*fnName*'<?= $manual_fn_params_string; ?>);
        </script>
    </textarea>

    <input type="hidden" class="action_single_input" name="track_manually" value="0">
</div>
<?php } ?>
<label>Tracking di terze parti</label>
<div class="styled-checkbox form-control">
    <label style="width: 100%;">
        <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
            <input type="checkbox" class="<?= (!is_null($duplicationK) ? 'action_single_input' : 'action_input') ;?> enable_external_tracking" name="enable_external_tracking" <?php if((!is_null($duplicationK) ? $single_tracking_values['enable_external_tracking'] : $action_values['enable_external_tracking']) == "1") { echo "checked"; } ?>>
            <i></i>
        </div>
        <span style="font-weight: normal;">Abilita</span>
    </label>
</div>
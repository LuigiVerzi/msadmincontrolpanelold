<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/2019
 */ ?>


<div class="easyRowDuplicationContainer">
    <?php
    $url_track = $action_values['url'];
    if(!is_array($url_track) || count($url_track) == 0) {
        $url_track[] = array();
    }
    ?>

    <?php foreach($url_track as $duplicationK => $single_tracking_values) { ?>
        <div class="stepsDuplication rowDuplication action_multiple_input" data-callback_fn="onLinkDuplication" data-button_text="Link" data-name="url" style="background: #fafafa; padding: 15px; border: 2px solid #ececec; margin-bottom: 15px;">

            <div class="row">

                <div class="col-sm-3">
                    <label>Traccia tramite*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Indica il metodo con la quale tracciare la visita nella pagina"></i></span>
                    <select name="metodo_selezione" class="form-control action_single_input cambia_metodo_selezione required">
                        <option value="link" <?= ($single_tracking_values['metodo_selezione'] == "link" ? 'selected' : ''); ?>>Tramite link di destinazione</option>
                        <option value="text" <?= ($single_tracking_values['metodo_selezione'] == "text" ? 'selected' : ''); ?>>Tramite testo contenuto</option>
                        <optgroup label="Avanzato">
                            <option value="track_manually" <?= ($single_tracking_values['metodo_selezione'] == 'track_manually' ? 'selected' : ''); ?>>Traccia azione manualmente</option>
                        </optgroup>
                    </select>
                </div>

                <div class="col-sm-6 method_container method_link" style="display: <?= ($single_tracking_values['metodo_selezione'] == "link" || !$single_tracking_values['metodo_selezione'] ? 'block' : 'none'); ?>">
                    <label>Link completo o parte del link</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il link della pagina la quale una volta visitata farà partire il tracking"></i></span>
                    <input name="link" type="text" class="form-control selettore autocompile_website_link action_single_input required" value="<?php echo htmlentities($single_tracking_values['link']); ?>" required>
                </div>

                <div class="col-sm-6 method_container  method_text" style="display: <?= ($single_tracking_values['metodo_selezione'] == "text" ? 'block' : 'none'); ?>">
                    <label>Testo contenuto nella pagina</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se la pagina di destinazione conterrà il seguente testo l'azione verrà tracciata"></i></span>
                    <input name="text" type="text" class="form-control selettore action_single_input required" value="<?php echo htmlentities($single_tracking_values['text']); ?>" required>
                </div>
                
                <div class="col-sm-3">
                    <?php include('ajax/tracking/enable_checkbox.php'); ?>
                </div>

            </div>

            <div class="carrello_selection_helper">
                <h2 class="title-divider">Indica i campi</h2>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Titolo del contenuto</label>
                                <input name="match_title" type="text" class="form-control action_single_input required visual-selector" data-visual-title="Seleziona il titolo dell\'elemento" placeholder="Seleziona l'elemento che contiene il titolo del contenuto visualizzato" value="<?php echo htmlentities($single_tracking_values['match_title']); ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Valore contenuto</label>
                                <input name="match_value" type="text" class="form-control action_single_input visual-selector" data-visual-title="Seleziona il valore del contenuto visualizzato" placeholder="Seleziona l'elemento che contiene il valore del contenuto visualizzato" value="<?php echo htmlentities($single_tracking_values['match_value']); ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>ID Prodotto</label>
                                <input name="match_product_id" type="text" class="form-control action_single_input visual-selector" data-visual-title="Selezione id del prodotto" placeholder="Il selettore dell'elemento che contiene l'id del prodotto (Es: #product_info .product_id)" value="<?php echo htmlentities($single_tracking_values['match_product_id']); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tracking_preference_container">
                <?php include('ajax/tracking/tracking_config.php'); ?>
            </div>


        </div>
    <?php } ?>

</div>

<script>
    window.tracking_actions_js['<?= $action['id']; ?>'] = function () {

        $("#action_<?= $action['id'] ?>_container").on('change', '.cambia_metodo_selezione', function () {

            $(this).closest('.action_multiple_input').find('.method_container').hide();
            $(this).closest('.action_multiple_input').find('.method_container.method_' + $(this).val()).show();
        });

        $("#action_<?= $action['id'] ?>_container").on('ifToggled', '.enable_external_tracking', function () {

            var $external_tracking_container = $(this).closest('.action_multiple_input ').find('.tracking_settings_container');

            if($(this).is(':checked')) {
                $external_tracking_container.show();
            } else {
                $external_tracking_container.hide();
            }
        });

        initPageVisitAutoLink = function($element) {

            var settings = {
                'height': '34px',
                'width': '100%',
                'interactive': true,
                'defaultText': 'Aggiungi',
                'removeWithBackspace': true,
                'minChars': 3,
                'onAddTag': function (value) {}
            };

            if($('#tipo_dominio').val() !== 'custom') {
                settings.autocomplete_url = "ajax/actions_interfaces/visita_pagina/getWebsiteLinks.php";
                settings.autocomplete = {selectFirst:true, width:'100px', autoFill:true}
            }

            if($element.parent().find('.ui-autocomplete-input').length) {

                var $fakeInput = $element.parent().find('.ui-autocomplete-input');

                var autocompleteSettings = {};

                if($('#tipo_dominio').val() !== 'custom') {
                    autocompleteSettings = settings.autocomplete;
                    autocompleteSettings.source = settings.autocomplete_url;
                    $fakeInput.autocomplete( "enable" ).autocomplete( "option", autocompleteSettings);
                } else {
                    $fakeInput.autocomplete( "disable" );
                }


            } else {
                $element.tagsInput(settings);
            }
        };

        initPageVisitAutoLink($('.autocompile_website_link'));

        $(window).on('trackingDomainChange', function () {
            $('.autocompile_website_link').each(function () {
                initPageVisitAutoLink($(this));
            });
        });

        onLinkDuplication = function($element) {
            var $last = $element.parent().find('.rowDuplication:last');

            $last.find('.tagsinput').remove();
            $last.find('[name="link"]').show().removeAttr('data-tagsinput-init').removeAttr('id');

            $last.find('.autocompile_website_link').val('');
            $last.find('[name="text"]').val('');

            initPageVisitAutoLink($last.find('.autocompile_website_link'));
        }

    };
</script>
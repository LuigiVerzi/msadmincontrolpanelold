<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/2019
 */ ?>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>Referer da tracciare*</label>
        <select id="<?= $action['id'] ?>_referer_da_tracciare" name="referer_da_tracciare" class="form-control action_input required">
           <option value="all" <?= ($action_values['referer_da_tracciare'] == "all" || !$action_values['referer_da_tracciare'] ? 'selected' : ''); ?>>Traccia tutti i siti web referenti</option>
           <option value="custom" <?= ($action_values['referer_da_tracciare'] == "custom" ? 'selected' : ''); ?>>Inserisci manualmente la lista di referenti</option>
        </select>
    </div>

    <div class="col-sm-6">

    </div>

    <div class="col-sm-3">
        <?php include('ajax/tracking/enable_checkbox.php'); ?>
    </div>
</div>

<div id="<?= $action['id'] ?>_general_external_tracking" style="display: <?= ($action_values['referer_da_tracciare'] != "custom" ? 'block' : 'none'); ?>;">
    <div class="tracking_preference_container">
        <?php include('ajax/tracking/tracking_config.php');  ?>
    </div>
</div>

<div id="<?= $action['id'] ?>_referers_to_track" style="display: <?= ($action_values['referer_da_tracciare'] == "custom" ? 'block' : 'none'); ?>;">

    <h2 class="title-divider">Referer da Tracciare</h2>

    <div class="easyRowDuplicationContainer">
        <?php
        $custom_referers = $action_values['custom_referer_url'];
        if(!is_array($custom_referers) || count($custom_referers) == 0) {
            $custom_referers[] = array();
        }
        ?>

        <?php foreach($custom_referers as $duplicationK => $single_tracking_values) { ?>
            <div class="stepsDuplication rowDuplication action_multiple_input" data-button_text="Referente" data-name="custom_referer_url" style="background: #fafafa; padding: 15px; border: 2px solid #ececec; margin-bottom: 15px;">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Sito Web</label>
                        <input type="text" name="referer" class="form-control action_single_input required" placeholder="Inserisci il dominio del sito web" value="<?php echo htmlentities($single_tracking_values['referer']) ?>" required>
                    </div>
                </div>

                <div class="tracking_preference_container">
                    <?php include('ajax/tracking/tracking_config.php'); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script>
    window.tracking_actions_js['<?= $action['id']; ?>'] = function () {

        $("#<?= $action['id'] ?>_referer_da_tracciare").on('change', function () {
            if($(this).val() == 'custom') {
                $('#<?= $action['id'] ?>_referers_to_track').show();
                $('#<?= $action['id'] ?>_general_external_tracking').hide();
            } else {
                $('#<?= $action['id'] ?>_referers_to_track').hide();
                $('#<?= $action['id'] ?>_general_external_tracking').show();
            }
        });

        $("#action_<?= $action['id']; ?>_container").on('change', '[name="referer"]', function (e) {

            var value = $(this).val();

            if(!(new RegExp("^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$")).test(value)) {
                $(this).val('');
            } else {
                value = (value.replace('www.', '').replace('://', '').replace('https', '').replace('http', '')).split('/');
                $(this).val(value[0]);
            }

        });

        $("#action_<?= $action['id'] ?>_container").on('ifToggled', '.enable_external_tracking', function () {

            var $external_tracking_container = $(this).closest('.action_container').find('.tracking_settings_container');

            if($(this).is(':checked')) {
                $external_tracking_container.show();
            } else {
                $external_tracking_container.hide();
            }
        });

    };
</script>
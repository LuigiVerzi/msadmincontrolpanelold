<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/2019
 */ ?>

<?php if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) { ?>
    <div class="alert alert-info">
        È possibile sincronizzare automaticamente il catalogo dei prodotti ecommerce sulle piattaforme Facebook Ads e Google Ads.
        <a href="<?= (new \MSSoftware\modules())->getModuleUrlByID('importazione_csv_prodotti'); ?>" target="_blank">Scopri di più</a>
    </div>
<?php } ?>

<div class="row">
    <div class="col-sm-3">
        <label>Metodo intercettazione*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Indica il metodo tramite il quale possiamo stabilire l'evento"></i></span>
        <select name="carrello_selezione" class="form-control action_input cambia_carrello_selezione required">
            <?php if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) { ?>
                <optgroup label="Automaticamente">
                    <option data-onlyfw360="1" value="auto" <?= ($action_values['carrello_selezione'] == 'auto' ? 'selected' : ''); ?>>Intercetta automaticamente</option>
                </optgroup>
            <?php } ?>
            <optgroup label="Avanzato">
                <option value="track_manually" <?= ($action_values['carrello_selezione'] == 'track_manually' ? 'selected' : ''); ?>>Traccia azione manualmente</option>
            </optgroup>
        </select>
    </div>

    <div class="col-sm-6">
        <label>Tracking Offline</label>
        <p class="text-muted">Abilitando il tracking degli acquisti sarà possibile tracciare anche le vendite Offline. Per fare ciò crea un'operazione di Vendita tramite il <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML . (new \MSSoftware\modules())->getModuleDetailsFromID("fatturazione_vendite")['path']; ?>">modulo Vendite</a>.</p>
    </div>

    <div class="col-sm-3">
        <?php include('ajax/tracking/enable_checkbox.php'); ?>
    </div>
</div>

<div class="tracking_preference_container">
    <?php include('ajax/tracking/tracking_config.php'); ?>
</div>


<script>
    window.tracking_actions_js['<?= $action['id']; ?>'] = function () {

        $("#action_<?= $action['id'] ?>_container").on('ifToggled', '.enable_external_tracking', function () {

            var $external_tracking_container = $(this).closest('.action_container').find('.tracking_settings_container');

            if($(this).is(':checked')) {
                $external_tracking_container.show();
            } else {
                $external_tracking_container.hide();
            }
        });

    };
</script>

<style>

</style>
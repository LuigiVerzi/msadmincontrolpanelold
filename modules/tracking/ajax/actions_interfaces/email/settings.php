<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/2019
 */ ?>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>Email da Tracciare*</label>
        <select id="<?= $action['id'] ?>_email_da_tracciare" name="email_da_tracciare" class="form-control action_input required">
            <option value="all" <?= ($action_values['email_da_tracciare'] == "all" || !$action_values['email_da_tracciare'] ? 'selected' : ''); ?>>Trova tutti gli indirizzi email in automatico</option>
            <option value="custom" <?= ($action_values['email_da_tracciare'] == "custom" ? 'selected' : ''); ?>>Inserisci manualmente gli indirizzi email da tracciare</option>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Copri indirizzi email</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Migliora il tracking obbligando gli utenti a cliccare per visualizzare l'indirizzo email."></i></span>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                    <input type="checkbox" name="hide_emails" class="action_input" id="<?= $action['id'] ?>_hide_emails" <?php if($action_values['hide_emails'] == "1") { echo "checked"; } ?>>
                    <i></i>
                </div>
                <span style="font-weight: normal;">Copri email</span>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-sm-12">
        <div id="<?= $action['id'] ?>_cover_text_container" style="display: <?= ($action_values['hide_emails'] == "1" ? 'block' : 'none'); ?>;">
            <label>Testo per coprire l'email</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($action_values['cover_text']) ?>"></i></span> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Copre tutti gli indirizzi email e mostra il seguente testo fino al momento del click."></i></span>
            <input id="<?= $action['id'] ?>_cover_text" name="cover_text" type="text" class="form-control action_input traducibile <?= ($action_values['hide_emails'] == "1" ? 'required' : ''); ?>" value="<?php echo htmlentities( (!empty($action_values['cover_text']) ? $MSFrameworki18n->getFieldValue($action_values['cover_text']) : 'Visualizza Email')) ?>" <?= ($action_values['hide_emails'] == "1" ? 'required' : ''); ?>>
        </div>
    </div>

    <div class="col-sm-3">
        <?php include('ajax/tracking/enable_checkbox.php'); ?>
    </div>
</div>

<div id="<?= $action['id'] ?>_general_external_tracking" style="display: <?= ($action_values['email_da_tracciare'] != "custom" ? 'block' : 'none'); ?>;">
    <div class="tracking_preference_container">
        <?php include('ajax/tracking/tracking_config.php');  ?>
    </div>
</div>

<div id="email_addresses_to_track" style="display: <?= ($action_values['email_da_tracciare'] == "custom" ? 'block' : 'none'); ?>;">

    <h2 class="title-divider">Email da Tracciare</h2>

    <div class="easyRowDuplicationContainer">
        <?php
        $custom_emails = $action_values['custom_email_addresses'];
        if(!is_array($custom_emails) || count($custom_emails) == 0) {
            $custom_emails[] = array();
        }
        ?>

        <?php foreach($custom_emails as $duplicationK => $single_tracking_values) { ?>
            <div class="stepsDuplication rowDuplication action_multiple_input" data-button_text="Email" data-name="custom_email_addresses" style="background: #fafafa; padding: 15px; border: 2px solid #ececec; margin-bottom: 15px;">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Indirizzo Email</label>
                        <input type="text" name="email_address" class="form-control action_single_input required" value="<?php echo htmlentities($single_tracking_values['email_address']) ?>" required>
                    </div>
                </div>

                <div class="tracking_preference_container">
                    <?php include('ajax/tracking/tracking_config.php'); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script>
    window.tracking_actions_js['<?= $action['id']; ?>'] = function () {

        $("#<?= $action['id'] ?>_email_da_tracciare").on('change', function () {
            if($(this).val() == 'custom') {
                $('#<?= $action['id'] ?>_addresses_to_track').show();
                $('#<?= $action['id'] ?>_general_external_tracking').hide();
            } else {
                $('#<?= $action['id'] ?>_addresses_to_track').hide();
                $('#<?= $action['id'] ?>_general_external_tracking').show();
            }
        });

        $("#<?= $action['id'] ?>_hide_emails").on('ifToggled', function () {
            if($(this).is(':checked')) {
                $('#<?= $action['id'] ?>_cover_text_container').show();
                $('#<?= $action['id'] ?>_cover_text_container').find('input').attr('required', true).addClass('required').attr('disabled', false);
            } else {
                $('#<?= $action['id'] ?>_cover_text_container').hide();
                $('#<?= $action['id'] ?>_cover_text_container').find('input').attr('required', false).removeClass('required').attr('disabled', true);
            }
        });

        $("#action_<?= $action['id'] ?>_container").on('ifToggled', '.enable_external_tracking', function () {

            var $external_tracking_container = $(this).closest('.action_container').find('.tracking_settings_container');

            if($(this).is(':checked')) {
                $external_tracking_container.show();
            } else {
                $external_tracking_container.hide();
            }
        });


        $("#action_<?= $action['id']; ?>_container").on('change', '[name="email_address"]', function (e) {
            var value = $(this).val();
            value = value.replace(/\s/g, '');
            $(this).val(value);
        });

    };
</script>
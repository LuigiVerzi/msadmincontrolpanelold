<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/2019
 */ ?>


<div class="easyRowDuplicationContainer">
    <?php
    $session_track = $action_values['sessioni'];
    if(!is_array($session_track) || count($session_track) == 0) {
        $session_track[] = array();
    }
    ?>

    <?php foreach($session_track as $duplicationK => $single_tracking_values) { ?>
        <div class="stepsDuplication rowDuplication action_multiple_input" data-button_text="Sessione" data-name="sessioni" style="background: #fafafa; padding: 15px; border: 2px solid #ececec; margin-bottom: 15px;">
            <div class="row">
                <div class="col-sm-3">
                    <label>Durata Minima*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La durata minima della sessione utente da tracciare. (Superando la seguente durata verrà tracciato l'evento)"></i></span>
                    <input name="durata_minima" type="number" class="form-control selettore action_single_input required" value="<?php echo htmlentities($single_tracking_values['durata_minima']); ?>" placeholder="<?= $MSFrameworki18n->gettext("Es: 10"); ?>" required>
                </div>

                <div class="col-sm-2">
                    <label>Unità</label>
                    <select name="unita_durata" class="form-control action_single_input required" required>
                        <option value="s" <?= ($single_tracking_values['unita_durata'] == "s" || !$single_tracking_values['unita_durata'] ? 'selected' : ''); ?>>Secondi</option>
                        <option value="m" <?= ($single_tracking_values['unita_durata'] == "m" ? 'selected' : ''); ?>>Minuti</option>
                    </select>
                </div>

                <div class="col-sm-4"></div>
                
                <div class="col-sm-3">
                    <?php include('ajax/tracking/enable_checkbox.php'); ?>
                </div>

            </div>

            <div class="tracking_preference_container">
                <?php include('ajax/tracking/tracking_config.php'); ?>
            </div>
        </div>
    <?php } ?>

</div>

<script>
    window.tracking_actions_js['<?= $action['id']; ?>'] = function () {

        $("#action_<?= $action['id'] ?>_container").on('ifToggled', '.enable_external_tracking', function () {

            var $external_tracking_container = $(this).closest('.action_multiple_input').find('.tracking_settings_container');

            if($(this).is(':checked')) {
                $external_tracking_container.show();
            } else {
                $external_tracking_container.hide();
            }
        });

    };
</script>
<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/2019
 */ ?>

<div class="easyRowDuplicationContainer">
    <?php
    $forms_to_track = $action_values['forms'];
    if(!is_array($forms_to_track) || count($forms_to_track) == 0) {
        $forms_to_track[] = array();
    }
    ?>

    <?php foreach($forms_to_track as $duplicationK => $single_tracking_values) { ?>
        <div class="stepsDuplication rowDuplication action_multiple_input" data-button_text="Form" data-name="forms" style="background: #fafafa; padding: 15px; border: 2px solid #ececec; margin-bottom: 15px;">
            <div class="row">
                <div class="col-sm-3">
                    <label>Seleziona form*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Indica il form da selezionare"></i></span>
                    <select name="form_selezione" class="form-control action_single_input cambia_form_selezione required">
                        <option value="custom" <?= ($single_tracking_values['form_selezione'] == 'custom' || !$single_tracking_values['form_selezione'] ? 'selected' : ''); ?>>Indica form tramite selettore</option>
                        <?php $fw_forms = (new \MSFramework\forms())->getFormDetails();  ?>
                        <?php if($fw_forms) { ?>
                        <optgroup label="Form Standard">
                            <?php foreach($fw_forms as $form_id => $fw_form) { ?>
                                <?php
                                $form_fields = array();
                                foreach(json_decode($fw_form['fields'], true) as $field) {
                                    $form_fields[] = $field['field_id'];
                                }
                                ?>
                                <option value="<?= $form_id; ?>" data-form_fields="<?= htmlentities(json_encode($form_fields)); ?>"<?= ($single_tracking_values['form_selezione'] == $form_id ? 'selected' : ''); ?>><?= $fw_form['nome']; ?></option>
                            <?php } ?>
                        </optgroup>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-4 form_selection_helper" style="display: <?= ($single_tracking_values['form_selezione'] == 'custom' ? 'block' : 'none'); ?>;">
                            <label>Cerca Form</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ti permette di cercare e selezionare uno dei form presenti sul sito."></i></span>
                            <a href="#" class="btn btn-info btn-outline btn-block form_helper_button"><i class="fa fa-magic" aria-hidden="true"></i> Cerca form nel sito</a>
                        </div>

                        <div class="col-sm-8 form_selection_method" style="display: <?= ($single_tracking_values['form_selezione'] == 'custom' ? 'block' : 'none'); ?>;">
                            <label>Selettore Form</label>
                            <input name="selettore" type="text" class="form-control selettore action_single_input required" value="<?php echo htmlentities($single_tracking_values['selettore']); ?>" required>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <?php include('ajax/tracking/enable_checkbox.php'); ?>
                </div>

            </div>

            <div class="tracking_preference_container">
                <?php include('ajax/tracking/tracking_config.php'); ?>
            </div>
            <div style="display: none;">
                <input name="detected_form_fields" type="hidden" class="form-control selettore action_single_input" value="<?php echo htmlentities(json_encode($single_tracking_values['detected_form_fields'])); ?>">
            </div>
        </div>
    <?php } ?>

</div>

<script>
    window.tracking_actions_js['<?= $action['id']; ?>'] = function () {

        $("#action_<?= $action['id'] ?>_container").on('change', '.cambia_form_selezione', function () {

            var $form_selection_helper = $(this).closest('.action_multiple_input').find('.form_selection_helper');
            var $form_selection_method = $(this).closest('.action_multiple_input').find('.form_selection_method');

            if($(this).val() == 'custom') {
                $form_selection_method.show();
                $form_selection_helper.show();

                <?= $action['id']; ?>_updateFormExtraParams([]);

            } else {
                $form_selection_helper.hide();
                $form_selection_method.hide();

                <?= $action['id']; ?>_updateFormExtraParams($(this).find('option:selected').data('form_fields'));

            }
        });

        $('.cambia_form_selezione').change();

        $("#action_<?= $action['id'] ?>_container").on('click', '.form_helper_button', function(e) {
           e.preventDefault();

           initVisualSelector({
               title: 'Selezione guidata del Form',
               destination_input: $(this).closest('.stepsDuplication').find('.selettore'),
               limit_tags: ['FORM'],
               include_css: "form {\n" +
                   "    opacity: 1;\n" +
                   "    box-shadow: #28a745 0px 0px 30px 0px;\n" +
                   "    position: relative;\n" +
                   "    z-index: 10;\n" +
                   "    background: white;\n" +
                   "    cursor: pointer !important;\n" +
                   "}\n" +
                   "\n" +
                   "form * {\n" +
                   "    cursor: pointer !important;\n" +
                   "}\n" +
                   "\n" +
                   "form:before {\n" +
                   "    content: 'FORM';\n" +
                   "    background: #28a745;\n" +
                   "    border: 2px solid #229c3e;\n" +
                   "    color: white;\n" +
                   "    display: block;\n" +
                   "    position: absolute;\n" +
                   "    left: 0;\n" +
                   "    right: 0;\n" +
                   "    bottom: 0;\n" +
                   "    top: 0;\n" +
                   "    margin: auto;\n" +
                   "    height: 30px;\n" +
                   "    width: 100px;\n" +
                   "    line-height: 30px;\n" +
                   "    text-align: center;\n" +
                   "    z-index: 10;\n" +
                   "    border-radius: 3px;\n" +
                   "}",
               onSelect: function(selector, extra) {},
               onSelectionError: function() {
                   bootbox.error("Non è stato selezionato nessun form valido in questa selezione.");
                   return false;
               },
               onSelectionConfirm: function(selector, $destination_input, is_form, extra) {
                   $("#get_visual_selector_helper .close_autocompile").click();
                   if(is_form > 0) {
                       $destination_input.closest('.action_multiple_input').find('[name="form_selezione"]').val(is_form).change();
                       $destination_input.val('');
                   } else {
                       $destination_input.closest('.action_multiple_input').find('[name="form_selezione"]').val('custom').change();
                       $destination_input.val(selector);
                       <?= $action['id']; ?>_updateFormExtraParams(typeof(extra.form_fields) !== 'undefined' ? extra.form_fields : []);
                   }
               }
           });

        });

        $("#action_<?= $action['id'] ?>_container").on('ifToggled', '.enable_external_tracking', function () {

            var $external_tracking_container = $(this).closest('.action_multiple_input ').find('.tracking_settings_container');

            if($(this).is(':checked')) {
                $external_tracking_container.show();
            } else {
                $external_tracking_container.hide();
            }
        });

        <?php if(isset($single_tracking_values['detected_form_fields']) && json_decode($single_tracking_values['detected_form_fields'])) { ?>
        <?= $action['id']; ?>_updateFormExtraParams(<?= $single_tracking_values['detected_form_fields']; ?>);
        <?php } ?>

    };

    function <?= $action['id']; ?>_updateFormExtraParams(extra_params) {
        $("#action_<?= $action['id'] ?>_container [name=\"detected_form_fields\"]").val(JSON.stringify(extra_params));

        var $params_container = $("#action_<?= $action['id'] ?>_container .extra_parameters_container");
        $params_container.find('.form_field').remove();

        if(extra_params.length) {
            extra_params.forEach(function (form_field) {
                $("#action_<?= $action['id'] ?>_container .extra_parameters_container .extra_parameter").first().clone().html(form_field).addClass('form_field').appendTo($params_container);
            });

        }

    }
</script>

<style>
    #form_autocompiler_iframe.loading {
        display: none;
    }

    #form_autocompiler_iframe + .loading_text {
        display: none;
        text-align: center;
        font-size: 30px;
    }

    #form_autocompiler_iframe.loading + .loading_text {
        display: block;
    }

    #form_link_website_switch > a {
        margin-right: 15px;
    }

    #form_link_website_switch :not(.active) .active_string {
        display: none;
    }

    #form_link_website_switch .active .active_string {
        display: inline;
    }

    #form_link_website_switch .active .inactive_string {
        display: none;
    }
</style>
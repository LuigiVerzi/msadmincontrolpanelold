<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/2019
 */ ?>

<div class="easyRowDuplicationContainer">
    <?php
    $click_to_track = $action_values['clicks'];
    if(!is_array($click_to_track) || count($click_to_track) == 0) {
        $click_to_track[] = array();
    }

    ?>

    <?php foreach($click_to_track as $duplicationK => $single_tracking_values) { ?>
        <div class="stepsDuplication rowDuplication action_multiple_input" data-button_text="Click" data-name="clicks" style="background: #fafafa; padding: 15px; border: 2px solid #ececec; margin-bottom: 15px;">
            <div class="row">
                <div class="col-sm-3">
                    <label>Metodo di selezione*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Indica il metodo con la quale selezionare l'elemento cliccabile"></i></span>
                    <select name="metodo_selezione" class="form-control action_single_input cambia_metodo_selezione required">
                        <option value="" <?= (!$single_tracking_values['metodo_selezione'] ? 'selected' : ''); ?>></option>

                        <option data-title="Link di destinazione" data-placeholder="Inserisci il link di destinazione (Da usare solo per i pulsanti/link)"  value="link" <?= ($single_tracking_values['metodo_selezione'] == "link" ? 'selected' : ''); ?>>Tramite Link di destinazione</option>
                        <option data-title="Classe Elemento" data-placeholder="Inserisci la classe CSS dell'elemento" value="classe" <?= ($single_tracking_values['metodo_selezione'] == "classe" ? 'selected' : ''); ?>>Tramite Classe</option>
                        <option data-title="Seleziona ID Elemento" data-placeholder="Inserisci l'ID dell'elemento" value="id" <?= ($single_tracking_values['metodo_selezione'] == "id" ? 'selected' : ''); ?>>Tramite ID Elemento</option>
                        <option data-title="Selettore CSS" data-placeholder="Es: body .slider .button" value="selettore" <?= ($single_tracking_values['metodo_selezione'] == "selettore" ? 'selected' : ''); ?>>Tramite Selettore CSS</option>
                        <option data-title="Testo dell'Elemento" data-placeholder="Inserisci il testo contenuto dall'elemento"  value="testo" <?= ($single_tracking_values['metodo_selezione'] == "testo" ? 'selected' : ''); ?>>Tramite Testo Contenuto</option>
                    </select>
                </div>

                <div class="col-sm-6 click_selection_helper" style="display: <?= (!$single_tracking_values['metodo_selezione'] ? 'block' : 'none'); ?>;">
                    <label>Compilazione guidata</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ti permette di scegliere l'elemento da cliccare in modo visivo, cliccando realmente sull'elemento."></i></span>
                    <a href="#" class="btn btn-info btn-outline btn-block click_helper_button"><i class="fa fa-magic" aria-hidden="true"></i> Selezione Visiva</a>
                </div>

                <div class="col-sm-6 click_selection_method" style="display: <?= (!$single_tracking_values['metodo_selezione'] ? 'none' : 'block'); ?>;">
                    <label><span class="dynamic_title"></span></label>
                    <input name="selettore" type="text" class="form-control selettore action_single_input required" value="<?php echo htmlentities($single_tracking_values['selettore']); ?>" required>
                </div>

                <div class="col-sm-3">
                    <?php include('ajax/tracking/enable_checkbox.php'); ?>
                </div>

            </div>

            <div class="tracking_preference_container">
                <?php include('ajax/tracking/tracking_config.php'); ?>
            </div>
        </div>
    <?php } ?>
</div>

<script>
    window.tracking_actions_js['<?= $action['id']; ?>'] = function () {

        $("#action_<?= $action['id'] ?>_container").on('change', '.cambia_metodo_selezione', function () {

            var $click_selection_helper = $(this).closest('.action_multiple_input').find('.click_selection_helper');
            var $click_selection_method = $(this).closest('.action_multiple_input').find('.click_selection_method');

            if($(this).val() !== '') {

                var selection_title = $(this).find('option:selected').data('title');
                var selection_placeholder = $(this).find('option:selected').data('placeholder');

                $click_selection_method.find('label .dynamic_title').html(selection_title);
                $click_selection_method.find('.form-control').attr('placeholder', selection_placeholder);
                $click_selection_method.show();
                $click_selection_helper.hide();

            } else {
                $click_selection_helper.show();
                $click_selection_method.hide();
            }
        });

        $('.cambia_metodo_selezione').change();

        $("#action_<?= $action['id'] ?>_container").on('click', '.click_helper_button', function(e) {
            e.preventDefault();

            initVisualSelector({
                title: 'Selezione guidata elemento Click',
                destination_input: $(this).closest('.stepsDuplication').find('.selettore'),
                onSelect: function(selector, extra) {},
                onSelectionConfirm: function(selector, $destination_input, is_form, extra) {
                    $destination_input.closest('.action_multiple_input').find('[name="metodo_selezione"]').val('selettore').change();
                    $destination_input.val(selector);
                    $("#get_visual_selector_helper .close_autocompile").click();
                }
            });

        });

        $("#action_<?= $action['id'] ?>_container").on('ifToggled', '.enable_external_tracking', function () {

            var $external_tracking_container = $(this).closest('.action_multiple_input ').find('.tracking_settings_container');

            if($(this).is(':checked')) {
                $external_tracking_container.show();
            } else {
                $external_tracking_container.hide();
            }
        });

    };
</script>
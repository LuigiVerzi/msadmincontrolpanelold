<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/2019
 */ ?>

<?php if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) { ?>
    <div class="alert alert-info">
        È possibile sincronizzare automaticamente il catalogo dei prodotti ecommerce sulle piattaforme Facebook Ads e Google Ads.
        <a href="<?= (new \MSSoftware\modules())->getModuleUrlByID('importazione_csv_prodotti'); ?>" target="_blank">Scopri di più</a>
    </div>
<?php } ?>

<div class="easyRowDuplicationContainer">
    <?php
    $carrelli_to_track = $action_values['carrelli'];
    if(!is_array($carrelli_to_track) || count($carrelli_to_track) == 0) {
        $carrelli_to_track[] = array();
    }
    ?>

    <?php foreach($carrelli_to_track as $duplicationK => $single_tracking_values) { ?>

        <div class="stepsDuplication rowDuplication action_multiple_input" data-button_text="Metodo" data-name="carrelli" style="background: #fafafa; padding: 15px; border: 2px solid #ececec; margin-bottom: 15px;">

            <div class="row">
                <div class="col-sm-3">
                    <label>Metodo intercettazione*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Indica il metodo tramite il quale possiamo stabilire l'evento"></i></span>
                    <select name="carrello_selezione" class="form-control action_single_input cambia_carrello_selezione required">
                        <option value="custom" <?= ($single_tracking_values['carrello_selezione'] == 'custom' || !$single_tracking_values['carrello_selezione'] ? 'selected' : ''); ?>>Indica gli elementi</option>
                        <?php if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) { ?>
                        <optgroup label="Automaticamente">
                            <option data-onlyfw360="1" value="auto" <?= ($single_tracking_values['carrello_selezione'] == 'auto' ? 'selected' : ''); ?>>Intercetta automaticamente</option>
                        </optgroup>
                        <?php } ?>
                        <optgroup label="Avanzato">
                            <option value="track_manually" <?= ($single_tracking_values['carrello_selezione'] == 'track_manually' ? 'selected' : ''); ?>>Traccia azione manualmente</option>
                        </optgroup>
                    </select>
                </div>


                <div class="col-sm-6">
                </div>

                <div class="col-sm-3">
                    <?php include('ajax/tracking/enable_checkbox.php'); ?>
                </div>
            </div>

            <div class="carrello_selection_helper">
                <h2 class="title-divider">Indica i campi</h2>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Pulsante aggiungi al carrello*</label>
                                <input name="add_to_cart_button" type="text" class="form-control action_single_input required visual-selector" data-visual-title="Selezione pulsante aggiungi al carrello" placeholder="Il selettore del pulsante aggiungi al carrello (Es: #product_info .add_to_cart)" value="<?php echo htmlentities($single_tracking_values['add_to_cart_button']); ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Nome Prodotto</label>
                                <input name="add_to_cart_title" type="text" class="form-control action_single_input visual-selector" data-visual-title="Selezione titolo del prodotto" placeholder="Il selettore dell'elemento che contiene il nome del prodotto (Es: #product_info .product_title)" value="<?php echo htmlentities($single_tracking_values['add_to_cart_title']); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Prezzo Prodotto</label>
                                <input name="add_to_cart_price" type="text" class="form-control action_single_input visual-selector" data-visual-title="Selezione prezzo del prodotto" placeholder="Il selettore dell'elemento che contiene il prezzo del prodotto (Es: #product_info .product_title)" value="<?php echo htmlentities($single_tracking_values['add_to_cart_price']); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Categoria Prodotto</label>
                                <input name="add_to_cart_category" type="text" class="form-control action_single_input visual-selector" data-visual-title="Selezione categoria del prodotto" placeholder="Il selettore dell'elemento che contiene la categoria del prodotto (Es: #product_info .product_category)" value="<?php echo htmlentities($single_tracking_values['add_to_cart_category']); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>ID Prodotto</label>
                                <input name="add_to_cart_product_id" type="text" class="form-control action_single_input visual-selector" data-visual-title="Selezione id del prodotto" placeholder="Il selettore dell'elemento che contiene l'id del prodotto (Es: #product_info .product_id)" value="<?php echo htmlentities($single_tracking_values['add_to_cart_product_id']); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tracking_preference_container">
                <?php include('ajax/tracking/tracking_config.php'); ?>
            </div>

        </div>
    <?php } ?>

</div>

<script>
    window.tracking_actions_js['<?= $action['id']; ?>'] = function () {

        $("#action_<?= $action['id'] ?>_container").on('change', '.cambia_carrello_selezione', function () {

            var $carrello_selection_helper = $(this).closest('.action_multiple_input').find('.carrello_selection_helper');

            if($(this).val() == 'custom') {
                $carrello_selection_helper.show();

            } else {
                $carrello_selection_helper.hide();
            }
        });

        $('.cambia_carrello_selezione').change();

        $("#action_<?= $action['id'] ?>_container").on('ifToggled', '.enable_external_tracking', function () {

            var $external_tracking_container = $(this).closest('.action_multiple_input ').find('.tracking_settings_container');

            if($(this).is(':checked')) {
                $external_tracking_container.show();
            } else {
                $external_tracking_container.hide();
            }
        });

    };
</script>

<style>
    #carrello_autocompiler_iframe.loading {
        display: none;
    }

    #carrello_autocompiler_iframe + .loading_text {
        display: none;
        text-align: center;
        font-size: 30px;
    }

    #carrello_autocompiler_iframe.loading + .loading_text {
        display: block;
    }

    #carrello_link_website_switch > a {
        margin-right: 15px;
    }

    #carrello_link_website_switch :not(.active) .active_string {
        display: none;
    }

    #carrello_link_website_switch .active .active_string {
        display: inline;
    }

    #carrello_link_website_switch .active .inactive_string {
        display: none;
    }
</style>
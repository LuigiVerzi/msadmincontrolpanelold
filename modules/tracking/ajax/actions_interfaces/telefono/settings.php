<?php
/**
 * MSAdminControlPanel
 * Date: 29/04/2019
 */ ?>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>Numeri da Tracciare*</label>
        <select id="<?= $action['id'] ?>_numeri_da_tracciare" name="numeri_da_tracciare" class="form-control action_input required">
           <option value="all" <?= ($action_values['numeri_da_tracciare'] == "all" || !$action_values['numeri_da_tracciare'] ? 'selected' : ''); ?>>Trova tutti i numeri in automatico</option>
           <option value="custom" <?= ($action_values['numeri_da_tracciare'] == "custom" ? 'selected' : ''); ?>>Inserisci manualmente i numeri da tracciare</option>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Copri numeri di telefono</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Migliora il tracking obbligando gli utenti a cliccare per visualizzare il numero."></i></span>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                    <input type="checkbox" name="hide_phones" class="action_input" id="<?= $action['id'] ?>_hide_phones" <?php if($action_values['hide_phones'] == "1") { echo "checked"; } ?>>
                    <i></i>
                </div>
                <span style="font-weight: normal;">Copri numeri</span>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-sm-12">
        <div id="<?= $action['id'] ?>_cover_text_container" style="display: <?= ($action_values['hide_phones'] == "1" ? 'block' : 'none'); ?>;">
            <label>Testo per coprire il numero</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($action_values['cover_text']) ?>"></i></span> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Copre tutti i numeri di telefono e mostra il seguente testo fino al momento del click."></i></span>
            <input id="<?= $action['id'] ?>_cover_text" name="cover_text" type="text" class="form-control action_input traducibile <?= ($action_values['hide_phones'] == "1" ? 'required' : ''); ?>" value="<?php echo htmlentities( (!empty($action_values['cover_text']) ? $MSFrameworki18n->getFieldValue($action_values['cover_text']) : 'Visualizza Telefono')) ?>" <?= ($action_values['hide_phones'] == "1" ? 'required' : ''); ?>>
        </div>
    </div>

    <div class="col-sm-3">
        <?php include('ajax/tracking/enable_checkbox.php'); ?>
    </div>
</div>

<div id="<?= $action['id'] ?>_general_external_tracking" style="display: <?= ($action_values['numeri_da_tracciare'] != "custom" ? 'block' : 'none'); ?>;">
    <div class="tracking_preference_container">
        <?php include('ajax/tracking/tracking_config.php');  ?>
    </div>
</div>

<div id="<?= $action['id'] ?>_phone_numbers_to_track" style="display: <?= ($action_values['numeri_da_tracciare'] == "custom" ? 'block' : 'none'); ?>;">

    <h2 class="title-divider">Numeri da Tracciare</h2>

    <div class="easyRowDuplicationContainer">
        <?php
        $custom_phones = $action_values['custom_phone_numbers'];
        if(!is_array($custom_phones) || count($custom_phones) == 0) {
            $custom_phones[] = array();
        }
        ?>
        
        <?php foreach($custom_phones as $duplicationK => $single_tracking_values) { ?>
            <div class="stepsDuplication rowDuplication action_multiple_input" data-button_text="Telefono" data-name="custom_phone_numbers" style="background: #fafafa; padding: 15px; border: 2px solid #ececec; margin-bottom: 15px;">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Numero Telefono</label>
                        <input type="number" name="phone_number" class="form-control action_single_input required" value="<?php echo htmlentities($single_tracking_values['phone_number']) ?>" required>
                    </div>
                </div>

                <div class="tracking_preference_container">
                    <?php include('ajax/tracking/tracking_config.php');  ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script>
    window.tracking_actions_js['<?= $action['id']; ?>'] = function () {

        $("#<?= $action['id'] ?>_numeri_da_tracciare").on('change', function () {
            if($(this).val() == 'custom') {
                $('#<?= $action['id'] ?>_phone_numbers_to_track').show();
                $('#<?= $action['id'] ?>_general_external_tracking').hide();
            } else {
                $('#<?= $action['id'] ?>_phone_numbers_to_track').hide();
                $('#<?= $action['id'] ?>_general_external_tracking').show();
            }
        });

        $("#<?= $action['id'] ?>_hide_phones").on('ifToggled', function () {

            if($(this).is(':checked')) {
                $('#<?= $action['id'] ?>_cover_text_container').show();
                $('#<?= $action['id'] ?>_cover_text_container').find('input').attr('required', true).addClass('required').attr('disabled', false);
            } else {
                $('#<?= $action['id'] ?>_cover_text_container').hide();
                $('#<?= $action['id'] ?>_cover_text_container').find('input').attr('required', false).removeClass('required').attr('disabled', true);
            }
        });

        $("#action_<?= $action['id']; ?>_container").on('change', '[name="phone_number"]', function (e) {

            var value = $(this).val();

            var match = value.match(/([0-9]{2})[0-9]{10}/);

            if(match) {
                var regex = new RegExp('^' + match[1]);
                value = value.replace(regex, '');
            }

            $(this).val(value);
        });

        $("#action_<?= $action['id'] ?>_container").on('ifToggled', '.enable_external_tracking', function () {

            var $external_tracking_container = $(this).closest('.action_container').find('.tracking_settings_container');

            if($(this).is(':checked')) {
                $external_tracking_container.show();
            } else {
                $external_tracking_container.hide();
            }
        });

    };
</script>
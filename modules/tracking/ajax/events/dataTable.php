<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();

$event_name = $_GET['event'];
$project_id = (int)$_GET['project_id'];

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array( 'db' => 'id',  'dt' => 0 ),
    array( 'db' => 'track_ref',  'dt' => 1 ,
        'formatter' => function( $d, $row ) {
            return (empty($d) ? '<small class="text-muted">N/A</small>'  : $d);
        }
    ),
    array( 'db' => 'user_ref',   'dt' => 2 ,
        'formatter' => function( $d, $row ) {
            Global $MSFrameworkCustomers;

            $info_cliente = '<small class="text-muted">N/A</small>';
            if($d > 0) {
                $cliente = $MSFrameworkCustomers->getCustomerDataFromDB($d);
                if($cliente) $info_cliente = '<a target="_blank" href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'customers/list/edit.php?id=' . $cliente['id'] . '">' . $cliente['nome'] . ' ' . $cliente['cognome'] . ' (' . $cliente['email'] . ')</a>';
                else $info_cliente = '<s>Cliente eliminato</s>';
            }

            return $info_cliente;
        }
    ),
    array( 'db' => 'user_ip', 'dt' => 3 ),
    array( 'db' => 'track_date', 'dt' => 4,
        'formatter' => function( $d, $row ) {
            return array("display" => date("d/m/Y H:i", strtotime($d)), "sort" => strtotime($d));
        }
    )
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'tracking__stats', 'id', $columns, null, array("project_id = $project_id", "track_event LIKE " . $MSFrameworkDatabase->quote($event_name)))
);

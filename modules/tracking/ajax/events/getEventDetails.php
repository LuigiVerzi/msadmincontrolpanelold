<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../../../sw-config.php');
require('../../config/moduleConfig.php');

$project_id = $_POST['project_id'];
$event_name = $_POST['event'];

?>

<div class="row">
    <div class="col-md-8">
        <h2 class="title-divider">Dettagli <b><?= htmlentities($event_name); ?></b></h2>
    </div>
    <div class="col-md-4 text-right">
        <a href="#" class="btn btn-danger btn-sm svuota_evento" data-project_id="<?= $project_id; ?>"  data-event_name="<?= htmlentities($event_name); ?>" style="margin-bottom: 15px;"><i class="fa fa-trash"></i> Svuota Statistiche</a>
    </div>
</div>

<table id="dettaglio_evento" class="display table table-striped table-bordered table-hover" style="width:100%">
    <thead>
    <tr>
        <th class="default-sort" data-sort="desc">ID</th>
        <th>Valore</th>
        <th>Cliente</th>
        <th>IP</th>
        <th class="is_data" width="150" style="width: 150px;">Data</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

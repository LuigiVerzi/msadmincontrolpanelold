<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../../../sw-config.php');
require('../../config/moduleConfig.php');


$project_id = $_POST['project_id'];
$event_name = $_POST['event_name'];

$status = $MSFrameworkDatabase->query("DELETE FROM `tracking__stats` WHERE project_id = :id AND track_event = :event", array(':id' => $project_id, ':event' => $event_name));

echo $status;

?>
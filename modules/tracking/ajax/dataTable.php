<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../../sw-config.php');
require('../config/moduleConfig.php');

$current_domain = $MSFrameworkCMS->getURLToSite();
$tracking_actions = (new \MSFramework\tracking())->getTrackingActionsInfo();

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM tracking__projects") as $r) {

    $tracking_attivi = array();
    if(json_decode($r['actions'])) {
        $actions_decoded = json_decode($r['actions'], true);

        foreach($actions_decoded as $k => $a) {
            $actions_decoded[$k] = '<span class="label label-inverse"><i class="fa ' . $tracking_actions[$a]['fa-icon'] . '"></i> ' . $tracking_actions[$a]['name'] . '</span>';
        }

        $tracking_attivi = implode(' ', $actions_decoded);
    }

    $array['data'][] = array(
        $r['name'],
        (empty($r['website']) ? '<span class="label label-success">' . $current_domain . '</span>' : '<span class="label label-warning">' . str_replace(',' , '</span> <span class="label label-warning">', $r['website']) . '</span> <div class="pull-right"><a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML .  (new \MSSoftware\modules())->getModuleDetailsFromID('integrazione')['path'] . '" target="_blank" class="btn btn-info btn-outline btn-sm"><i class="fa fa-eye"></i> Vedi JS</a></div>'),
        $tracking_attivi,
        ($r['is_active'] == "1" ? '<span class="label label-primary">Si</span>' : '<span class="label label-danger">No</span>'),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

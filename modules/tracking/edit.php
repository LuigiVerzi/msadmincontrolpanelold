<?php
/**
 * MSAdminControlPanel
 * Date: 01/03/18
 */

require_once('../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkTracking = new \MSFramework\tracking();

$tracking_codes = array();
$r = array();

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM tracking__projects WHERE id = :id", array($_GET['id']), true);
}

if($r) {
    $tracking_codes = json_decode($r['tracking_codes'], true);
}

?>

<script>
    window.tracking_actions_js = [];
    window.tracking_settings_js = [];
</script>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Impostazioni</h1>
                        <fieldset>
                            <h2 class="title-divider">Dati Progetto</h2>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Nome Progetto*</label>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($r['name']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-12" id="domain_select">
                                            <label>Domini Abilitati</label>
                                            <select id="tipo_dominio" name="tipo_dominio" class="form-control">
                                                <option value="default" <?= (empty($r['website']) ? 'selected' : ''); ?>>Utilizza su <?= SW_NAME; ?></option>
                                                <option value="custom" <?= (!empty($r['website']) ? 'selected' : ''); ?>>Inserisci un dominio esterno</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php if($r['is_active'] == "1" || $_GET['id'] == "") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div style="display: <?= (!empty($r['website']) ? 'block' : 'none;'); ?>;" id="custom_domain">
                                <div class="hr-line-dashed"></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Lista dei domini abilitati</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La seguente lista dovrà contenere i domini dei siti web esterni abilitati ad utilizzare la funzionalità di Tracking."></i></span>
                                        <input id="website" name="website" type="text" class="form-control <?= (!empty($r['website']) ? 'required' : ''); ?>" value="<?php echo htmlentities( (!empty($r['website']) ? $r['website'] : '')) ?>" <?= (empty($r['website']) ? 'disabled' : ''); ?>>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div id="code_to_include" class="alert alert-info text-center" style="display: <?= ($r ? 'block' : 'none'); ?>;">
                                    <label>Per utilizzare le funzioni di Tracking in un sito web esterno includi il codice di Integrazione se non l'hai già fatto.</label><br>
                                    <?= '<a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML .  (new \MSSoftware\modules())->getModuleDetailsFromID('integrazione')['path'] . '" target="_blank" class="btn btn-success"><i class="fa fa-cube"></i> Codice di Integrazione</a>'; ?>
                                </div>
                            </div>

                            <h2 class="title-divider">Dati Tracking</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>UA Google Analytics</label>
                                    <input id="ganalytics_ua" name="google_analytics_ua" type="text" placeholder="UA-XXXXXXXXX-X" class="form-control tracking_code" value="<?php echo htmlentities($tracking_codes['google_analytics_ua']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>ID Facebook Pixel</label>
                                    <input id="fbpixel_id" name="facebook_pixel_id" type="text" placeholder="XXXXXXXXXXXXXXXX" class="form-control tracking_code" value="<?php echo htmlentities($tracking_codes['facebook_pixel_id']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Google Ads</label>
                                    <input id="gads_id" name="gads_id" placeholder="AW-XXXXXXXXX" type="text" class="form-control tracking_code" value="<?php echo htmlentities($tracking_codes['gads_id']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Google Tag Manager</label>
                                    <input id="google_tagmanager_gtm" name="google_tagmanager_gtm" placeholder="GTM-XXXXXXX" type="text" class="form-control tracking_code" value="<?php echo htmlentities($tracking_codes['google_tagmanager_gtm']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>LinkedIn Insight Tag</label>
                                    <input id="linkedin_insight_tag" name="linkedin_insight_tag" placeholder="XXXXXXX" type="text" class="form-control tracking_code" value="<?php echo htmlentities($tracking_codes['linkedin_insight_tag']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Albacross</label>
                                    <input id="albacross" name="albacross" placeholder="XXXXXXXX" type="text" class="form-control tracking_code" value="<?php echo htmlentities($tracking_codes['albacross']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Yandex Metrica</label>
                                    <input id="yandex_metrica" name="yandex_metrica" placeholder="XXXXXXXX" type="text" class="form-control tracking_code" value="<?php echo htmlentities($tracking_codes['yandex_metrica']) ?>">
                                </div>
                            </div>

                        </fieldset>

                        <h1 class="<?= (!$r ? 'hidden' : ''); ?>">Obiettivi</h1>
                        <fieldset>
                            <h2 class="title-divider">Obiettivi</h2>
                            <div class="row" style="display: flex; flex-wrap: wrap;">
                                <?php
                                $actions_decoded = json_decode($r['actions'], true);
                                $actions_values_decoded = json_decode($r['actions_value'], true);
                                $tracking_values_decoded = json_decode($r['tracking_values'], true);

                                $tracking_params = $MSFrameworkTracking->getTrackingParams();
                                $actions_checks = $MSFrameworkTracking->getTrackingActionsInfo();

                                foreach($actions_checks as $action) {
                                    if(is_array($action['limit_to_extra_function']) && count(array_intersect($action['limit_to_extra_function'], json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions']))) == 0) {
                                        continue;
                                    }
                                    ?>

                                    <div class="col-lg-3 col-md-6 col-sm-6 notification_box" style="cursor: pointer;">
                                        <div class="form_actions_check widget <?= (in_array($action['id'], $actions_decoded) ? "active" : ""); ?> p-md text-center"  id="<?= $action['id'] ?>" style="display: <?= ($using_primary_language ? 'block' : 'none'); ?>;">
                                            <div>
                                                <i class="fa <?= $action['fa-icon'] ?> fa-3x"></i>
                                                <h1 class="m-b-md m-t-md"><?= $action['name'] ?></h1>
                                                <h4 style="font-weight: normal;">
                                                    <?= $action['descr'] ?>
                                                </h4>

                                                <div class="actions_buttons row active_actions" style="display: <?= (in_array($action['id'], $actions_decoded) ? "block" : "none"); ?>;">
                                                    <div class="col-xs-6">
                                                        <a href="#" class="btn btn-default edit_preference btn-block">Impostazioni</a>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <a href="#" class="btn btn-danger activation_btn btn-block">Disattiva</a>
                                                    </div>
                                                </div>

                                                <div class="actions_buttons inactive_actions" style="display: <?= (in_array($action['id'], $actions_decoded) ? "none" : "block"); ?>;">
                                                    <a href="#" class="btn btn-primary activation_btn btn-block">Attiva</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <?php if(!$using_primary_language) { ?>
                                <div class="alert alert-warning">
                                    Durante la modifica di una lingua secondaria non è possibile creare nuovi obiettivi.
                                </div>
                            <?php } ?>

                            <div class="manage_actions_section" style="display: <?= (count($actions_decoded) ? 'block' : 'none'); ?>;">
                                <h2 class="title-divider">Preferenze Obiettivi</h2>
                            </div>

                            <?php foreach($actions_checks as $action) { ?>
                                <div class="action_container m-t-md" data-container-of="<?= $action['id'] ?>" id="action_<?= $action['id'] ?>_container" style="display: <?= (in_array($action['id'], $actions_decoded) ? "block" : "none") ?>">
                                    <div class="ibox">
                                        <div class="ibox-title" style="background: #fefefe;">
                                            <i class="ibox-icon fa <?= $action['fa-icon'] ?> fa-3x"></i>
                                            <h5>
                                                <span class="title">Impostazioni <?= $action['name'] ?></span>
                                            </h5>
                                            <div class="ibox-tools">
                                                <a class="fullscreen-link">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content" style="background: background: #fefefe;">
                                            <?php
                                            $action_values = $actions_values_decoded[$action['id']];

                                            $duplicationK = null;
                                            $single_tracking_values = array();

                                            include("ajax/actions_interfaces/" . $action['id'] . "/settings.php");
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </fieldset>

                        <h1 class="tracking_avanzato <?= (!$r || ($r && $r['website'] === '') ? 'hidden' : ''); ?>">Tracking Avanzato</h1>
                        <fieldset>
                            <h2 class="title-divider">Introduzione</h2>
                            <div class="alert alert-info">
                                Le principali azioni possono essere monitorate senza l'utilizzo di codice, configurando gli obiettivi direttamente dalla <i>scheda Obiettivi</i>.
                                <br>
                                Tuttavia tramite l'utilizzo di alcune funzioni JavaScript è possibile ottenere un'esperienza di tracking avanzata.
                            </div>
                            <h2 class="title-divider">Funzioni Avanzate</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 70px;">

                                        </th>
                                        <th style="width: 600px;">
                                            Descrizione
                                        </th>
                                        <th>
                                            Utilizzo
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($MSFrameworkTracking->getAdvancedTrackingFunctions() as $function_name => $function_info) { ?>
                                        <tr>
                                            <td>
                                                <label class="text-navy" style="font-weight: 500;"><?= $function_name; ?></label>
                                            </td>
                                            <td style="font-size: 13px;">
                                                <p><?= $function_info['description']; ?></p>
                                                <span class="text-success"><?= $function_info['advantages']; ?></span>
                                            </td>
                                            <td>

                                                <span style="margin-bottom: 5px; display: block;"><?= $function_info['usage_example']['text']; ?></span>
                                                <textarea class="codemirror" disabled><script><?= PHP_EOL . "\t" . $function_info['usage_example']['code'] . PHP_EOL; ?></script></textarea>

                                                <h3 style="margin-top: 15px;">Parametri</h3>
                                                <table class="table table-striped parameters_table">
                                                    <thead>
                                                    <tr>
                                                        <?php
                                                        foreach($function_info['params'] as $param_type => $params) {
                                                            foreach ($params as $param_key => $param_value) {
                                                                echo '<th>' . $param_key . (is_array($param_value) ? ' <small style="font-weight: normal;">(array)</small>' : '') . '</th>';
                                                            }
                                                        }
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <?php
                                                        foreach($function_info['params'] as $param_type => $params) {
                                                            foreach ($params as $param_key => $param_value) {
                                                                echo '<td>';
                                                                if (is_array($param_value)) {
                                                                    echo '{';
                                                                    echo '<ul style="list-style: none !important; margin-left: 30px;">';
                                                                    foreach($param_value as $children_key => $children_value) {
                                                                        echo '<li><span class="text-success">' . $children_key . '</span>: \'' . $children_value . '\',</li>';
                                                                    }
                                                                    echo '</ul>';
                                                                    echo '}';
                                                                } else {
                                                                    echo '<span>' . $param_value . '</span>';
                                                                }
                                                                echo '<br><small class="text-muted">(' . ($param_type == 'required' ? 'Richiesto' : 'Opzionale') . ')</small>';
                                                                echo '</td>';
                                                            }
                                                        }
                                                        ?>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </fieldset>

                        <h1 class="<?= (!$r ? 'hidden ' : ''); ?>tab-right"><i class="fa fa-crosshairs"></i> Statistiche</h1>
                        <fieldset>
                            <?php if($r) { ?>

                                <?php
                                $resoconto_eventi = $MSFrameworkDatabase->getAssoc("SELECT track_event, MAX(track_date) as last_track_date, COUNT(*) as numero_chiamate FROM `tracking__stats` WHERE project_id = :id GROUP BY track_event ORDER BY numero_chiamate DESC", array(':id' => $r['id']));
                                ?>

                                <h2 class="title-divider">Dettagli Eventi</h2>
                                <div class="row">
                                    <div class="col-md-12">

                                        <table id="dettagli_eventi" class="display table table-striped table-bordered table-hover" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Evento</th>
                                                <th class="default-sort" data-sort="desc">Numero di Ricezioni</th>
                                                <th>Ultima ricezione</th>
                                                <th class="no-sort" style="width: 30px;" width="30"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($resoconto_eventi as $resoconto_evento) { ?>
                                                <tr>
                                                    <td><?= $resoconto_evento['track_event']; ?></td>
                                                    <td class="track_count"><?= $resoconto_evento['numero_chiamate']; ?></td>
                                                    <td class="last_track_Date"><?= ($resoconto_evento['last_track_date'] ? date("d/m/Y H:i", strtotime($resoconto_evento['last_track_date'])) : '<small class="muted">N/A</small>'); ?></td>
                                                    <td class="track_view"><a href="#" class="btn btn-primary btn-outline btn-sm view_details" data-project_id="<?= $r['id']; ?>" data-event_name="<?= $resoconto_evento['track_event']; ?>"><i class="fa fa-eye"></i></a></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                                <?php
                                $dati_resoconto = array('chiavi' => array(), 'valori' => array());

                                foreach($resoconto_eventi as $evento) {
                                    $dati_resoconto['chiavi'][] = $evento['track_event'];
                                    $dati_resoconto['valori'][] = $evento['numero_chiamate'];
                                }
                                ?>
                                <h2 class="title-divider">Resoconto</h2>
                                <div class="row">
                                    <div class="col-md-6">
                                        <textarea id="dati_resoconto" style="display: none;"><?= json_encode($dati_resoconto); ?></textarea>
                                        <div style="width: 100%;" class="x_panel"><canvas id="grafico_resoconto"></canvas></div>
                                    </div>
                                </div>

                            <?php } else { ?>
                                <div class="alert alert-info">
                                    In questa scheda verranno visualizzate le statistiche della campagna di tracking una volta salvata.
                                </div>
                            <?php } ?>
                        </fieldset>

                    </form>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<style>
    #dettagli_eventi tr.details > td {
        background: #fafafa;
    }

    #dettagli_eventi tr.details + tr > td {
        background: #fafafa;
    }

    #single_event_detail .dataTables_wrapper  {
        margin: 0;
        padding: 0;
    }

    #single_event_detail tr td {
        padding: 10px 15px;
        min-width: 120px;
    }
    #dettagli_eventi .track_view {
        width: 30px;
    }
    @media (max-width: 768px) {
        #dettagli_eventi .track_view {
            width: auto;
        }
        #dettagli_eventi tbody tr td {
            padding-left: 15px;
        }
        #dettagli_eventi_wrapper div.dataTables_filter label {
            overflow: hidden;
        }
        #dettagli_eventi h2.title-divider,
        #dettagli_eventi h2.title-with-button {
            margin: 0;
        }
        #dettagli_eventi tbody tr td > a:not(.btn) {
            border: none;
            line-height: 1.6;
        }
    }

    .stepsDuplication[data-button_text] h2 {
        background: white !important;
    }


    .tracking_settings_container .tracking_platform {
        margin: 5px 0 15px;
        border: 1px solid rgba(0, 0, 0, 0.1);
        position: relative;
        background: white;
    }

    .tracking_settings_container .tracking_platform h2 {
        margin: 0 0 15px;
        padding: 0 15px;
        padding-left: 55px;
        background: rgba(255, 255, 255, 0.1) !important;
        font-size: 16px;
        line-height: 40px;
        color: white;
    }

    .tracking_platform_icon {
        position: absolute;
        left: 0;
        top: 0;
        font-size: 20px;
        line-height: 40px;
        padding: 0;
        width: 40px;
        height: 40px;
        text-align: center;
        border-right: 1px solid rgba(255, 255, 255, 0.1);
        color: white;
    }

    .tracking_settings_container .tracking_platform:last-child {
        margin: 0;
    }

    /* VISUAL SELECTOR */
    #visual_selector_autocompiler_iframe.loading {
        display: none;
    }

    #visual_selector_autocompiler_iframe + .loading_text {
        display: none;
        text-align: center;
        font-size: 30px;
    }

    #visual_selector_autocompiler_iframe.loading + .loading_text {
        display: block;
    }

    #visual_selector_link_website_switch > a {
        margin-right: 15px;
    }

    #visual_selector_link_website_switch :not(.active) .active_string {
        display: none;
    }

    #visual_selector_link_website_switch .active .active_string {
        display: inline;
    }

    #visual_selector_link_website_switch .active .inactive_string {
        display: none;
    }

    .visual_selector_input {
        position: relative;
        margin-bottom: 15px;
    }

    .visual_selector_input .visual_selector_btn {
        background-color: #fafafa;
        position: absolute;
        right: 1px;
        top: 1px;
        line-height: 32px;
        width: 32px;
        text-align: center;
        border-left: 1px solid #e5e6e7;
        color: #06994d;
    }

    .visual_selector_input .visual_selector_btn:hover {
        background: #06994d;
        color: white;
    }

    .CodeMirror {
        height: auto;
    }

    .custom_javascript_alert {
        color: orange;
    }

    #extra_parameter_select .extra_parameter:hover {
        cursor: pointer;
        color: white;
        background: #1c84c6;
    }

    .extra_parameters_container span,
    .extra_params_selector span {
        display: inline-block;
        line-height: 33px;
        padding: 0 15px;
        float: left;
        margin: 0 3px 3px 0;
    }

</style>

<script type="text/javascript" src="scripts/websiteVisualSelector.js"></script>
<script>

    globalInitForm();
    document.domain = '<?= $MSFrameworkCMS->getCleanHost(); ?>';
</script>

<textarea id="visual_selector_template" style="display: none;">
    <div id="get_visual_selector_helper" style="display: none;">
        <div class="ibox">
            <div class="ibox-title" style="background: #fefefe;">
                <i class="ibox-icon fa fa-magic fa-3x"></i>
                <h5>
                    <span class="title">Selezione guidata elemento Click</span>
                </h5>
                <div class="ibox-tools">
                    <a class="close_autocompile">
                        <i class="fa fa-close"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="background: background: #fefefe;">
                <p>Stai navigando sul sito <b id="current_visual_selector_iframe_link">...</b></p>
                <div id="visual_selector_link_website_switch" style="display: none;">

                </div>
                <div class="hr-line-dashed"></div>
                <div class="alert alert-info">
                    Clicca un elemento tramite il <b>tasto destro</b> del mouse, oppure <b>tienilo premuto per più di 1 secondo</b> per selezionarlo automaticamente.
                </div>
                <div class="hr-line-dashed"></div>
                <div class="iframe_container">
                    <iframe id="visual_selector_autocompiler_iframe" class="loading" style="width: 100%; height: 600px;" src="about:blank;"></iframe>
                    <h3 class="loading_text">Caricamento in corso...</h3>
                </div>
                <div class="iframe_error_container" style="text-align: center;">

                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="autocompiler_visual_selector_modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="fa fa-magic modal-icon"></i>
                    <h4 class="modal-title" style="float: none;">Elemento selezionato</h4>
                    <small class="font-bold">Puoi visualizzare il selettore che abbiamo preparato qui di seguito.</small>
                </div>
                <div class="modal-body">
                    <code class="element_selector">...</code>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                    <button type="button" class="btn btn-primary" id="confirm_visual_selector_selection">Conferma selezione</button>
                </div>
            </div>
        </div>
    </div>

</textarea>

<div class="modal inmodal fade" id="extra_parameter_select" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-magic modal-icon"></i>
                <h4 class="modal-title" style="float: none;"><b class="input_name"></b> dinamico.</h4>
                <small class="font-bold">Seleziona uno dei seguenti parametri per impostarlo automaticamente come valore del campo <b class="input_name"></b></small>
            </div>
            <div class="modal-body">
                <div class="extra_params_selector">...</div>
                <div style="clear: both;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
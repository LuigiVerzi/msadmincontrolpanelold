<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('integration_settings');
$ms_tags = (new \MSFramework\Frontend\Tags\tags())->getMSTags(true);
?>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Utilizzo su domini esterni</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Domini Abilitati</label>
                                    <input class="form-control" id="website" value="<?php echo htmlentities($r['enabled_domains']) ?>" type="text">
                                </div>
                            </div>

                            <div class="alert alert-info">
                                <label>Includi il seguente codice nel sorgente nel tuo sito web per iniziare ad utilizzare le integrazioni con Framework360</label>
                                <code style="background: white; margin: 5px 0 0; border-color: #002935; color: #002935;"><?= htmlentities('<script src="' . $MSFrameworkCMS->getURLToSite() . 'm/bridge/' . ($MSFrameworkSaaSBase->isSaaSDomain() ? $MSFrameworkSaaSEnvironments->getCurrentEnvironmentID() . '/' : '') . 'embed.js' . '" crossorigin="anonymous"></script>'); ?></code>
                            </div>
                        </fieldset>
                    </form>

                    <div class="ms_tags_tabs tabs-container" style="margin-top: 15px;">
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach($ms_tags as $category_name => $category_shortcodes) { ?>
                                <li class="<?= ($category_name === 'Main' ? 'active' : ''); ?>"><a class="nav-link" data-toggle="tab" href="#tag_<?= $category_name; ?>_category"><?= (new \MSFramework\Frontend\Tags\tags())->tags_categories[$category_name]['name']; ?></a></li>
                            <?php } ?>
                        </ul>

                        <div class="tab-content">
                            <?php foreach($ms_tags as $category_name => $category_shortcodes) { ?>
                                <div role="tabpanel" id="tag_<?= $category_name; ?>_category" class="tab-pane <?= ($category_name === 'Main' ? 'active' : ''); ?>">
                                    <div class="panel-body" style="padding-bottom: 0;">
                                        <?php foreach($category_shortcodes as $single_shortcode) { ?>
                                            <div class="ibox">
                                                <div class="ibox-title" style="background: #fefefe;">
                                                    <h5>
                                                        <b class="title"><?= $single_shortcode['name']; ?></b> <small class="id">- <?= $single_shortcode['description']; ?></small>
                                                    </h5>
                                                    <div class="ibox-tools">
                                                        <a class="collapse-link">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="ibox-content" style="background: background: #fefefe;">
                                                    <textarea class="frontendShortcodeMirror" disabled>
                                                        <?= (new \MSFramework\Frontend\Tags\tags())->generateSingleTagExample($single_shortcode); ?>
                                                    </textarea>

                                                    <table class="table table-bordered white-bg">
                                                        <?= (new \MSFramework\Frontend\Tags\tags())->generateSingleTagDocumentation($single_shortcode); ?>
                                                    </table>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
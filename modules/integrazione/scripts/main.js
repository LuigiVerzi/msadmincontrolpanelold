function initForm() {
    initClassicTabsEditForm();
    $('#website').tagsInput({
        'height':'33px',
        'width':'100%',
        'interactive':true,
        'defaultText':'Aggiungi',
        'removeWithBackspace' : true,
        'onAddTag': function (value) {
            if(!isValidURL(value)) {
                $('#website').removeTag(value);
            }
        }
    });

    $('.frontendShortcodeMirror').each(function () {
        $(this).val($.trim($(this).val()));
        CodeMirror.fromTextArea($(this)[0], {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme: "icecoder",
            readOnly: true,
            mode: 'text/html'
        });
    });

    $('.CodeMirror').each(function(i, el) {
        el.CodeMirror.autoFormatRange({line:0, ch:0}, {line: el.CodeMirror.lineCount()});
        el.CodeMirror.setSelection({line:0, ch:0}, {line: 0, ch:0});
    });
    $('html').scrollTop(0);

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.preventDefault();
        $('.CodeMirror:visible').each(function(i, el) {
            var scrollPos = $('html').scrollTop();
            el.CodeMirror.refresh();
            el.CodeMirror.autoFormatRange({line:0, ch:0}, {line: el.CodeMirror.lineCount()});
            el.CodeMirror.setSelection({line:0, ch:0}, {line: 0, ch:0});
            $('html').scrollTop(scrollPos);
        });
    });
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pWebsites":  $('#website').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "missing_websites") {
                if(data.websites.length === 1) {
                    bootbox.error("Esiste un progetto di tracking collegato al seguente dominio, pertanto è impossibile eliminarlo.<code style='margin-top: 15px;'>" + data.websites[0]  + "</code>");
                } else {
                    bootbox.error("Esistono dei progetti di tracking collegati ai seguenti domini, pertanto è impossibile eliminarli.<code style='margin-top: 15px;'>" + data.websites.split(',')  + "</code>");
                }
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}

function isValidURL(string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if (res == null)
        return false;
    else
        return true;
}
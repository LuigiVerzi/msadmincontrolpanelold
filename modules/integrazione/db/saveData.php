<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'integration_settings'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

$formattedWebsites = array();
if($_POST['pWebsites'] != '') {
    $url_exploded = explode(',', $_POST['pWebsites']);
    foreach($url_exploded as $url)
    {
        if(!preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", $url)) {
            echo json_encode(array("status" => "url_not_valid"));
            die();
        } else {
            $host = parse_url($url);
            if(!isset($host['host'])) {
                $url = 'http://' . $url;
                $host = parse_url($url);
            }
            $formattedWebsites[] = $MSFrameworkCMS->getCleanHost($host['host']);
        }
    }
}

$trackingWebsiteUsed = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT website FROM tracking__projects") as $tracking_info) {
    if(!empty($tracking_info['website'])) {
        $trackingWebsiteUsed = array_unique(array_merge($trackingWebsiteUsed, explode(',', $tracking_info['website'])));
    }
}

$websites_missing = array();
foreach($trackingWebsiteUsed as $singleTrackingWebsite) {
    if(!in_array($singleTrackingWebsite, $formattedWebsites)) {
        $websites_missing[] = $singleTrackingWebsite;
    }
}

if($websites_missing) {
    echo json_encode(array("status" => "missing_websites", "websites" => implode(', ', $websites_missing)));
    die();
}

$array_to_save = array(
    "value" => json_encode(array(
        "enabled_domains" => implode(',', array_unique($formattedWebsites))
    )),
    "type" => "integration_settings"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'integration_settings'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
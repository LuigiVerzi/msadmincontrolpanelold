<?php
/**
 * MSAdminControlPanel
 * Date: 15/09/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php
                $customToolbarButton = '<a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . (new \MSSoftware\modules())->getModuleDetailsFromID("importazione_csv_clienti")['path'] . '" class="btn btn-default"><i class="fa fa-download"></i> CSV</a>';
                require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardGrid.php");
                ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">

                    <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Stato</th>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Telefono</th>
                            <th class="have_filter">Liste</th>
                            <th>Commenti</th>
                            <th class="is_data default-sort" data-sort="DESC">Data Iscrizione</th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */ ?>

<?php
require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$tags_newsletter = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__tag");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT *, (SELECT GROUP_CONCAT(tag SEPARATOR ',') FROM newsletter__tag_destinatari WHERE destinatario = newsletter__destinatari.id) as tag FROM newsletter__destinatari WHERE id = :id", array(':id' => $_GET['id']), true);
}

$customer = (new \MSFramework\customers())->getCustomerDataFromDB($_GET['id']);

if($customer) {
    $info_principali = array();
    if(!empty($customer['email'])) {
        $info_principali[] = $customer['email'];
    }
    if(!empty($customer['telefono_casa'])) {
        $info_principali[] = $customer['telefono_casa'];
    }
    if(!empty($customer['telefono_cellulare'])) {
        $info_principali[] = $customer['telefono_cellulare'];
    }

    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
    if(json_decode($customer['avatar'])) {
        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($customer['avatar'], true)[0];
    }
}

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <?php if($customer) { ?>
                    <div class="col-lg-3 col-md-4">
                        <div class="contact-box">
                            <div class="profile-image">
                                <img src="<?= $avatar_url; ?>" class="rounded-circle circle-border m-b-md" alt="profile">
                            </div>
                            <div class="profile-info">
                                <div class="">
                                    <div>
                                        <h2 class="no-margins">
                                            <?= $customer['nome']; ?> <?= $customer['cognome']; ?>
                                        </h2>
                                        <h4>Registrato dal <?= date('d/m/Y H:i', strtotime($customer['registration_date'])); ?></h4>
                                        <address class="m-t-sm">
                                            <?= implode('<br>', $info_principali); ?>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-info">
                            <b>Desideri modificare le info del Cliente?</b><br>
                            Puoi modificare le informazioni relative al cliente tramite l'apposito <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>customers/list/edit.php?id=<?= $customer['id']; ?>" target="_blank">modulo Clienti</a>.
                        </div>
                    </div>
                <?php }  ?>

                <div class="<?= ($customer ? 'col-lg-9 col-md-8' : 'col-sm-12'); ?>">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <?php if(!$customer) { ?>
                                <h2 class="title-divider">Dati Cliente</h2>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nome*</label>
                                        <input id="nome" name="nome" class="form-control required" value="<?php echo $customer['nome'] ?>" type="text" required>
                                    </div>

                                    <div class="col-sm-3">
                                        <label>Cognome</label>
                                        <input id="cognome" name="cognome" class="form-control" value="<?php echo $customer['cognome'] ?>" type="text">
                                    </div>

                                    <div class="col-sm-3">
                                        <label>Email*</label>
                                        <input id="email" name="email" class="form-control required" value="<?php echo $customer['email'] ?>" type="email" required>
                                    </div>

                                    <div class="col-sm-3">
                                        <label>Telefono</label>
                                        <input id="telefono" name="telefono" class="form-control" value="<?php echo $customer['telefono'] ?>" type="tel">
                                    </div>
                                </div>

                                <h2 class="title-divider">Preferenze Newsletter</h2>
                            <?php } ?>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Liste destinatario</label>
                                    <select class="form-control" id="tag" name="tag" multiple>
                                        <?php foreach($tags_newsletter as $tag) { ?>
                                            <option value="<?= $tag['id']; ?>" <?php if(in_array($tag['id'], explode(',', $r['tag']))) { echo "selected"; } ?>><?= $tag['nome']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="active" <?php if($r['active'] != "0") { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="txt_only" <?php if($r['txt_only'] == "1") { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Solo formato TXT</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="title-divider">Note</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Commenti <small>(Visibili solo dagli amministratori)</small></label>
                                    <textarea id="commenti" name="commenti" class="form-control"><?php echo $r['commenti'] ?></textarea>
                                </div>
                            </div>
                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
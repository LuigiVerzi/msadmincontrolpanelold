<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->deleteRow("newsletter__destinatari", "id", $_POST['pID'])) {
    $MSFrameworkDatabase->deleteRow("newsletter__tag_destinatari", "destinatario", $_POST['pID']);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>



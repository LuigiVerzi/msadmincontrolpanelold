<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;
if(isset($_POST['user']) && ($_POST['user']['pNome'] == "" || $_POST['user']['pEmail'] == "")) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(isset($_POST['user'])) {

    $mail_conflict = false;

    if ($MSFrameworkDatabase->getCount("SELECT email FROM customers WHERE email = :mail AND id != :id", array(":mail" => $_POST['user']['pEmail'], ":id" => $_POST['pID'])) != 0) {
        $mail_conflict = true;
    }

    if ($_POST['user']['pEmail'] != "") {
        if (!filter_var($_POST['user']['pEmail'], FILTER_VALIDATE_EMAIL)) {
            $mail_conflict = true;
        }
    }

    if ($mail_conflict) {
        echo json_encode(array("status" => "mail_not_valid"));
        die();
    }

    $_POST['user']['pTelefono'] = preg_replace("/[^0-9]/", "", $_POST['user']['pTelefono']);
}

$array_to_save = array(
    "commenti" => $_POST['pCommenti'],
    "txt_only" => $_POST['pTXTOnly'],
    "active" => $_POST['pActive']
);

$user_error = false;
if(isset($_POST['user'])) {
    $user_id = (new \MSFramework\customers())->registerUser($_POST['user']['pNome'], $_POST['user']['pCognome'], $_POST['user']['pEmail'], '', $_POST['user']['pTelefono']);
    $array_to_save['id'] = $user_id;
    if(!$user_id) $user_error = true;
} else {
    $user_id = $_POST['pID'];
    $array_to_save['id'] = $_POST['pID'];
    if(!(new \MSFramework\customers())->getCustomerDataFromDB($user_id)) $user_error = true;
}

if($user_error) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$db_action = ($MSFrameworkDatabase->getCount("SELECT * FROM newsletter__destinatari WHERE id = :id", array(':id' => $user_id)) ? 'update' : 'insert');

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO newsletter__destinatari ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE newsletter__destinatari SET $stringForDB[1] WHERE id = :id_dest", array_merge(array(":id_dest" => $user_id), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$subscribed_tags = array();

if($db_action != "insert") {
    $user_id = $_POST['pID'];

    foreach($MSFrameworkDatabase->getAssoc("SELECT tag FROM newsletter__tag_destinatari WHERE destinatario = :dest", array(":dest" => $user_id)) as $dest_lists) {
        $subscribed_tags[] = $dest_lists['tag'];
    }

    $delete_from_tags = array_diff($subscribed_tags, (is_array($_POST['pTag']) ? $_POST['pTag'] : array()));
    if(is_array($delete_from_tags) && count($delete_from_tags) > 0) {
        $same_db_string_ary = $MSFrameworkDatabase->composeSameDBFieldString($delete_from_tags, "OR", "tag");
        $MSFrameworkDatabase->query("DELETE FROM newsletter__tag_destinatari WHERE destinatario = :dest_id AND (" . $same_db_string_ary[0] . ")", array_merge(array(":dest_id" => $user_id), $same_db_string_ary[1]));
    }
}

if(count($_POST['pTag'])) {
    $subscribe_to_tags = array_diff($_POST['pTag'], $subscribed_tags);
    (new \MSFramework\Newsletter\subscribers())->subscribeToTag($subscribe_to_tags, $user_id);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $user_id : '')));
die();
$(document).ready(function() {
    loadStandardDataTable('main_table_list', true, {
        "initComplete": function(settings, json) {

            this.api().columns().every( function () {
                var column = this;

                if($(column.header()).hasClass('have_filter')) {

                    var select = $('<select style="margin-right: 5px;"><option value="">[' + $(column.header()).html() + '] Filtra</option></select>').prependTo($(column.header()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(this.value).draw();
                        }
                    );

                    $.ajax({
                        url: "ajax/getTags.php",
                        async: false,
                        dataType: "json",
                        success: function (data) {
                            data.forEach(function (val) {
                                select.append('<option value="' + val.id + '">' + val.nome + '</option>');
                            });
                        }
                    });
                }
            });

        }
    });
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('#telefono').keyup(function () {
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
    });

}

function moduleSaveFunction() {

    var data = {
        "pID": $('#record_id').val(),
        "pTag": $('#tag').val(),
        "pCommenti": $('#commenti').val(),
        "pTXTOnly": $('#txt_only:checked').length,
        "pActive": $('#active:checked').length
    };

    if(!$('#record_id').val().length) {
        data['user'] = {
            "pNome": $('#nome').val(),
            "pCognome": $('#cognome').val(),
            "pEmail": $('#email').val(),
            "pTelefono": $('#telefono').val()
        }
    }

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: data,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido o è già stata utilizzata da un altro cliente!");
            } else if(data.status == "phone_not_valid") {
                bootbox.error("Il numero di telefono inserito è in un formato non valido o è già stata utilizzata da un altro cliente!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                if(data.id !== '') {
                    location.href = 'edit.php?id=' + data.id;
                }
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}

/* FUNZIONI DATATABLE */
function activateRecipient(sel_row) {

    if(typeof(sel_row) == 'undefined') {
        sel_row = getSelectedRows('main_table_list');
    } else {
        sel_row = [{id: sel_row}];
    }

    if(sel_row.length > 0) {
        bootbox.confirm('Sei sicuro di voler abilitare la newsletter a ' + sel_row.length  + ' client' + (sel_row.length === 1 ? 'e' : 'i') + '?', function(result) {
            if(result) {
                var ids = [];
                sel_row.forEach(function(row, key) {
                    ids.push(row.id);
                });
                updateActivationStatus(ids, 1);
                unselectAllRow('main_table_list');
            }
        });
    } else {
        bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
        return false;
    }
}

function deactivateRecipient(sel_row) {

    if(typeof(sel_row) == 'undefined') {
        sel_row = getSelectedRows('main_table_list');
    } else {
        sel_row = [{id: sel_row}];
    }

    if(sel_row.length > 0) {
        bootbox.confirm('Sei sicuro di voler disabilitare la newsletter a ' + sel_row.length  + ' client' + (sel_row.length === 1 ? 'e' : 'i') + '?', function(result) {
            if(result) {
                var ids = [];
                sel_row.forEach(function(row, key) {
                    ids.push(row.id);
                });
                updateActivationStatus(ids, 0);
                unselectAllRow('main_table_list');
            }
        });
    } else {
        bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
        return false;
    }
}

function updateActivationStatus(sel_id, status) {
    $.ajax({
        url: "ajax/updateActivationStatus.php",
        type: "POST",
        data: {
            "pID": sel_id,
            "pStatus": status
        },
        async: false,
        dataType: "json",
        success: function(data) {
            $('.dataTable').DataTable().ajax.reload(null, false);
        }
    });
}
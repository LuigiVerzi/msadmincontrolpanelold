<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array(
        'db' => '(SELECT active FROM newsletter__destinatari WHERE newsletter__destinatari.id = customers.id)',
        'dt' => 'DT_ExtraBtn',
        'formatter' => function( $d, $row ) {
            if($d) {
                return array(
                    array(
                        'label' => 'Disiscrivi',
                        'btn' => 'danger',
                        'icon' => 'toggle-off',
                        'class' => 'deactivateUser',
                        'function' => 'deactivateRecipient',
                        'context' => true
                    )
                );
            } else {
                return array(
                    array(
                        'label' => 'Iscrivi',
                        'btn' => 'primary',
                        'icon' => 'toggle-on',
                        'class' => 'activateUser',
                        'function' => 'activateRecipient',
                        'context' => true
                    )
                );
            }
        }
    ),
    array( 'db' => 'id',  'dt' => 0 ),
    array(
        'db' => '(SELECT active FROM newsletter__destinatari WHERE newsletter__destinatari.id = customers.id)',
        'dt' => 1,
        'formatter' => function( $d, $row ) {
           return '<span class="label label-' . ($d == '1' ? 'primary' : 'danger') . '">' . ($d == '1' ? 'Iscritto' : 'Non iscritto') . '</span>';
        }
    ),
    array(
        'db' => "CONCAT(nome, ' ', cognome)",
        'dt' => 2
    ),
    array(
        'db' => 'email',
        'dt' => 3,
        'formatter' => function( $d, $row ) {
            return (!empty($d) ? $d : '<span class="text-muted">N/A</span>');
        }
    ),
    array(
        'db' => 'telefono_cellulare',
        'dt' => 4,
        'formatter' => function( $d, $row ) {
            return (!empty($d) ? $d : '<span class="text-muted">N/A</span>');
        }
    ),
    array(
        'db'        => 'id',
        'dt'        => 5,
        'formatter' => function( $d, $row ) {
            global $MSFrameworkDatabase;
            return $MSFrameworkDatabase->getAssoc("SELECT GROUP_CONCAT(nome SEPARATOR ', ') as tags FROM newsletter__tag WHERE id IN(SELECT tag FROM newsletter__tag_destinatari WHERE destinatario = $d)", array(), true)['tags'];
        },
        'customSearchPattern' => function($db, $string) {
            return "`" . $db . "` IN (SELECT destinatario FROM newsletter__tag_destinatari WHERE tag = $string)";
        }
    ),
    array( 'db' => '(SELECT commenti FROM newsletter__destinatari WHERE newsletter__destinatari.id = customers.id)',     'dt' => 6 ),
    array( 'db' => '(SELECT data_inserimento FROM newsletter__destinatari WHERE newsletter__destinatari.id = customers.id)', 'dt' => 7,
        'formatter' => function( $d, $row ) {
            if($d) {
                return array("display" => date("d/m/Y H:i", strtotime($d)), "sort" => strtotime($d));
            } else {
                return array("display" => '<span class="text-muted">N/A</span>', "sort" => '0');
            }
        }
    )
);

echo json_encode(
    $datatableHelper::simple( $_GET, $MSFrameworkDatabase, 'customers', 'id', $columns )
);
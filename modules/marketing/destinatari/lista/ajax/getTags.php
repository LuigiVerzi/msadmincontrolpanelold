<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$tags = $MSFrameworkDatabase->getAssoc("SELECT * FROM  newsletter__tag");

echo json_encode(
    $tags
);
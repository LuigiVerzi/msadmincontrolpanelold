<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkNewsletter = new \MSFramework\Newsletter\subscribers();

$ids = $_POST['pID'];
$status = $_POST['pStatus'];

$ids_in = implode(",", array_map(
    function($v) {
        return (int)$v;
    }, $ids)
);

if($status) {
    foreach ($MSFrameworkDatabase->getAssoc("SELECT * FROM customers WHERE id IN ($ids_in) AND id NOT IN (SELECT id FROM newsletter__destinatari)") as $toSub) {
        $MSFrameworkNewsletter->subscribeCustomer($toSub);
    }
}

$MSFrameworkDatabase->query("UPDATE newsletter__destinatari SET active = :status WHERE id IN ($ids_in)", array(':status' => ($status ? 1 : 0)));

die(json_encode(array('status' => 'ok')));
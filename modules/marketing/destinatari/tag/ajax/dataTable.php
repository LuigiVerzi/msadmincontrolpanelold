<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT *, (SELECT COUNT(*) FROM newsletter__tag_destinatari WHERE newsletter__tag.id = newsletter__tag_destinatari.tag AND (SELECT COUNT(*) FROM customers WHERE customers.id = newsletter__tag_destinatari.destinatario) > 0) as iscritti FROM newsletter__tag") as $r) {
    $array['data'][] = array(
        $r['id'],
        $r['nome'],
        $r['descrizione'],
        $r['iscritti'],
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

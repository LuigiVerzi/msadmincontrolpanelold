<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$list_id = $_POST['list_id'];
$sync_options = $_POST['sync_options'];

if($sync_options['behavior'] !== 'only_future') {

    if (isset($sync_options['conditions']) && count($sync_options['conditions'])) {
        $user_ids = (new \MSFramework\Newsletter\conditions())->getPassingConditionUsers($sync_options['conditions']);
    } else {
        $MSFrameworkDatabase->getAssoc("SET SESSION group_concat_max_len = 100000000;");
        $user_ids = explode(',', $MSFrameworkDatabase->getAssoc("SELECT GROUP_CONCAT(id ORDER BY id DESC SEPARATOR ',') as ids FROM `customers`", array(), true)['ids']);
    }

    if ($user_ids && (int)$list_id > 0) {
        $already_sub = $MSFrameworkDatabase->getAssoc("SELECT GROUP_CONCAT(destinatario ORDER BY id DESC SEPARATOR ',') as ids FROM newsletter__tag_destinatari WHERE destinatario  IN (" . implode(",", $user_ids) . ") AND tag = $list_id", array(), true);
        $user_ids = array_diff($user_ids, explode(',', $already_sub['ids']));
    }
}

die((string)count($user_ids));
$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $(document).ready(function () {
        $('#enable_sync').on('ifToggled', function () {
            if($(this).is(':checked')) {
                $('.sync_query_settings').show();

                if($('#disable_conditions').is(':checked')) {
                    $('.sync_query_container').hide();
                } else {
                    $('.sync_query_container').show();
                }

            } else {
                $('.sync_query_settings').hide();
                $('.sync_query_container').hide();
            }
            countContactToSync();
        });

        $('#disable_conditions').on('ifToggled', function () {
            if($(this).is(':checked')) {
                $('.sync_query_container').hide();
            } else {
                $('.sync_query_container').show();
            }
            countContactToSync();
        });

        $('#sync_behavior').on('change', function () {
            countContactToSync();
        });

        initQueryBuilder();
    });

    if($('#enable_sync').is(':checked')) {
        countContactToSync();
    }

}

function getSyncOptions() {

    var syncOptions = {};

    if($("#enable_sync:checked").length) {
        syncOptions = {
            behavior: $('#sync_behavior').val(),
            conditions: []
        };

        if (!$("#disable_conditions:checked").length) {
            var optionsValues = [];
            var is_valid = false;
            try {
                is_valid = $("#sync_query").queryBuilder("validate");
            } catch (e) {
            }

            if (is_valid) {
                optionsValues = $("#sync_query").queryBuilder("getRules");
            }

            if (!Object.keys(optionsValues).length) {
                optionsValues = '0';
            }

            syncOptions['conditions'] = optionsValues;
        }

    }

    return syncOptions;
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pSlug": $('#slug').val(),
            "pDescrizione": $('#descrizione').val(),
            "pSyncOptions": getSyncOptions()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "tag_not_valid") {
                bootbox.error("Lo slug inserito è già stato utilizzato!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {

                if(data.refresh === "1") {
                    if(data.id.length > 0) {
                        location.href = "edit.php?id=" + data.id;
                    } else {
                        location.reload();
                    }
                }
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}

function initQueryBuilder() {

    var queryFilters = JSON.parse($('#query_filters').val());

    window.displaySyncError = false;

    $('#sync_query').queryBuilder({
        plugins: {
            'bt-tooltip-errors': {},
            'bt-selectpicker': {
                container: '#automationContainer'
            },
            'filter-description': {}
        },
        lang_code: 'it',
        operators: JSON.parse($('#query_operators').val()),
        filters: queryFilters
    });

    var rules = JSON.parse($('#sync_settings').val());

    if(typeof(rules['conditions']) !== 'undefined') {
        rules = rules['conditions'];
    } else {
        rules = [];
    }

    if(Object.keys(rules).length) {
        $('#sync_query').queryBuilder('setRules', rules);
        countContactToSync();
    }

    $('#sync_query').on("afterAddRule.queryBuilder afterAddGroup.queryBuilder afterApplyGroupFlags.queryBuilder afterApplyRuleFlags.queryBuilder afterClear.queryBuilder afterCreateRuleFilters.queryBuilder afterCreateRuleInput.queryBuilder afterCreateRuleOperators.queryBuilder  afterDeleteGroup.queryBuilder afterDeleteRule.queryBuilder afterInit.queryBuilder afterReset.queryBuilder afterSetRules.queryBuilder afterUpdateGroupCondition.queryBuilder afterUpdateRuleFilter.queryBuilder afterUpdateRuleOperator.queryBuilder afterUpdateRuleValue.queryBuilder", function(event) {
        countContactToSync();
    }).on('validationError.queryBuilder', function(e, rule, error, value) {
        if(!window.displaySyncError) {
            e.preventDefault();
        }
    });
}

function countContactToSync() {

    if(window.countInProgress) {
        return false;
    }

    var destinatari_attuali = $('destinatari_attuali').html();


    window.displaySyncError = false;
    var syncOptions = getSyncOptions();
    window.displaySyncError = true;

    if(syncOptions === "0") {

        $('#destinatari').html(destinatari_attuali);
        $('#destinatari_widget').attr('class', 'widget style1 gray-bg');

        return true;
    }

    window.countInProgress = true;

    $('#destinatari').html('...');
    $('#destinatari_widget').attr('class', 'widget style1 gray-bg');

    $.ajax({
        url: "ajax/countUserToSync.php",
        type: "POST",
        data: {
            "list_id": $('#record_id').val(),
            "sync_options": syncOptions,
        },
        async: true,
        dataType: "text",
        success: function (data) {

            window.countInProgress = false;

            $('#destinatari').html(data);
            if(data > 0) {
                $('#destinatari_widget').attr('class', 'widget style1 navy-bg');
            } else {
                $('#destinatari_widget').attr('class', 'widget style1 gray-bg');
            }
        },
        error: function () {
            window.countInProgress = false;
        }
    });
}
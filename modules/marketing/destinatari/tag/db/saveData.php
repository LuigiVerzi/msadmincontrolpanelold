<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pSyncOptions']['conditions'] === "0") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$tag_conflict = false;
if($MSFrameworkDatabase->getCount("SELECT nome FROM newsletter__tag WHERE nome = :nome AND id != :id", array(":nome" => $_POST['pNome'], ":id" => $_POST['pID'])) != 0) {
    $tag_conflict = true;
}

if($tag_conflict) {
    echo json_encode(array("status" => "tag_not_valid"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "descrizione" => $_POST['pDescrizione'],
    "sync_options" => json_encode($_POST['pSyncOptions'])
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO newsletter__tag ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE newsletter__tag SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

$insert_id = ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '');

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$refresh = "0";
if($_POST['pSyncOptions']) {
    $to_refresh = (new \MSFramework\Newsletter\lists())->syncContactToLists( ($_POST['pID'] ? $_POST['pID'] : $insert_id), true );
    if($to_refresh) $refresh = "1";
}

echo json_encode(array("status" => "ok", "id" => $insert_id, "refresh" => $refresh));
die();
?>
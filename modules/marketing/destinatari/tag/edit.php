<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */ ?>

<?php
require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkTracking = new \MSFramework\tracking();
$MSFrameworkNewsletterLists = new \MSFramework\Newsletter\lists();

$syncSettings = array();
if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__tag WHERE id = :id", array(':id' => $_GET['id']), true);
    if($r) {
        $syncSettings = (json_decode($r['sync_options']) ? json_decode($r['sync_options'], true) : array());

        if($syncSettings && $syncSettings['behavior'] === 'only_presents') {
            $syncSettings = array();
        }

    }
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Dati generali</h2>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nome*</label>
                                    <input id="nome" name="nome" class="form-control required" value="<?php echo $r['nome'] ?>" type="text" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Descrizione</label>
                                    <textarea id="descrizione" name="descrizione" class="form-control"><?php echo $r['descrizione'] ?></textarea>
                                </div>
                            </div>

                            <h2 class="title-divider">Sincronizzazione contatti</h2>

                            <textarea id="sync_settings" style="display: none;"><?= json_encode($syncSettings); ?></textarea>

                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <p class="text text-info" style="margin-top: 0;">
                                        Importa automaticamente i clienti nella lista quando interagiscono con il sito web o con una qualsiasi campagna.<br>
                                        È possibile creare nuovi eventi tramite il <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>tracking/" target="_blank">modulo Tracking</a>.
                                    </p>
                                    <p class="text text-warning" style="margin-top: 0;">
                                        Saranno importati anche i contatti non iscritti alla newsletter, ma non sarà possibile inviargli email sino a quando non si saranno registrati.
                                    </p>
                                </div>

                                <div class="col-lg-3 col-sm-6 col-xs-12 sync_query_settings pull-right" style="display: <?= ($syncSettings ? 'block' : 'none'); ?>;">
                                    <div id="destinatari_widget" style="margin-top: 0;" class="widget style1 gray-bg">
                                        <div class="row">
                                            <div class="col-xs-4 text-center">
                                                <i class="fa fa-clock-o fa-4x"></i>
                                            </div>
                                            <div class="col-xs-8 text-right">
                                                <span> Da sincronizzare</span>
                                                <h2 class="font-bold" id="destinatari">0</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6 col-xs-12 pull-right">
                                    <div style="margin-top: 0;" class="widget style1">
                                        <div class="row">
                                            <div class="col-xs-4 text-center">
                                                <i class="fa fa-users fa-4x"></i>
                                            </div>
                                            <div class="col-xs-8 text-right">
                                                <span> Contatti attuali</span>
                                                <?php
                                                if($r) {
                                                    $destinatari = $MSFrameworkNewsletterLists->getSubscribersCountForTag($r['id']);
                                                    $destinatari_inattivi = $MSFrameworkNewsletterLists->getSubscribersCountForTag($r['id'], false) - $destinatari;
                                                } else {
                                                    $destinatari = 0;
                                                    $destinatari_inattivi = 0;
                                                }
                                                ?>
                                                <h2 class="font-bold"><span id="destinatari_attuali" <?= ($destinatari ? 'class="text-navy"' : ''); ?>><?= $destinatari; ?></span><?= ($destinatari_inattivi > 0 ? ' <span class="destinatari_inattivi text-danger" style="font-size: 15px; font-weight: normal;">+' . $destinatari_inattivi . ' Non iscritti</span>' : ''); ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" class="enable_sync" id="enable_sync" <?php if($syncSettings) { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Sincronizza clienti</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <select class="form-control sync_query_settings" id="sync_behavior" style="display: <?= ($syncSettings ? 'block' : 'none'); ?>;">
                                        <option value="both" <?= ($syncSettings['behavior'] == 'both' ? 'selected' : ''); ?>>
                                            Sia i contatti attuali che i futuri
                                        </option>
                                        <option value="only_presents" <?= ($syncSettings['behavior'] == 'only_presents' ? 'selected' : ''); ?>>
                                            Solo i contatti già iscritti
                                        </option>
                                        <option value="only_future" <?= ($syncSettings['behavior'] == 'only_future' ? 'selected' : ''); ?>>
                                            Solo i futuri iscritti
                                        </option>
                                    </select>
                                </div>

                                <div class="col-lg-3">
                                    <div class="styled-checkbox sync_query_settings form-control" style="display: <?= ($syncSettings ? 'block' : 'none'); ?>;">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" class="enable_sync" id="disable_conditions" <?php if($syncSettings && !$syncSettings['conditions']) { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Sincronizza tutti</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="sync_query_container" style="margin-top: 15px; display: <?= ($syncSettings && $syncSettings['conditions'] ? 'block' : 'none'); ?>;">

                                <textarea id="query_filters" style="display: none;"> <?= json_encode((new \MSFramework\Newsletter\conditions())->getConditionsQueryFilters()); ?></textarea>
                                <textarea id="query_operators" style="display: none;"> <?= json_encode((new \MSFramework\Newsletter\conditions())->getConditionsQueryOperators()); ?></textarea>

                                <div id="sync_query"></div>
                            </div>
                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
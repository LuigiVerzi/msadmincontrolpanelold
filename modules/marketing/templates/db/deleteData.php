<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\Newsletter\emails())->getTemplateDetails($_POST['pID'], 'attachments') as $r) {
    foreach(json_decode($r['attachments'], true) as $file) {
        if($file != "" && !(new \MSFramework\Newsletter\emails())->isSharedAttachment($file, 'newsletter__template', $r['id'])) {
            $images_to_delete[] = $file;
        }
    }
}

if($MSFrameworkDatabase->deleteRow("newsletter__template", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('NEWSLETTER'))->unlink($images_to_delete);

    foreach($_POST['pID'] as $id) {
        unlink(UPLOAD_NEWSLETTER_TEMPLATES_FOR_DOMAIN . 'thumb_' . $id . '.png');
    }

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>



$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');

    $('body').on('click', '.dataTableSendTestTemplate, #dataTableSendTestTemplate', function() {

        if(!$('#record_id').length) {
            if (!$('#main_table_list tbody').find('tr.selected').length || $('#main_table_list tbody').find('tr.selected').length > 1) {
                bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
                return false;
            } else {
                var sel_id = $('#main_table_list tbody').find('tr.selected').attr('id');
            }
        }

        $.ajax({
            url: $('#baseElementPathAdmin').val() + 'ajax/email-editor/SendTemplateToMailSettings.php',
            dataType: "html",
            success: function (data) {
                bootbox.dialog({
                    message: data,
                    closeButton: false,
                    buttons: {
                        cancel: {
                            label: "Annulla",
                            className: 'btn-danger'
                        },
                        ok: {
                            label: "Invia anteprima",
                            className: 'btn-info',
                            callback: function(){
                                if($('#test_send_to').val() == "") {
                                    bootbox.error("Per poter compiere questa operazione, devi impostare un destinatario!");
                                    return false;
                                }

                                var emailToSend = $('#test_send_to').val();

                                var sendFunction = function(mail_id) {

                                    if(mail_id === '') {
                                        mail_id = $('#record_id').val();
                                    }

                                    $.ajax({
                                        url: $('#baseElementPathAdmin').val() + 'ajax/email-editor/SendTemplateToMail.php',
                                        method: "POST",
                                        data: {
                                            email: emailToSend,
                                            template_id: mail_id
                                        },
                                        async: false,
                                        success: function (data) {
                                            if (data == "ok") {
                                                bootbox.alert("La mail è stata inviata con successo all'indirizzo impostato");
                                            } else {
                                                bootbox.error("Questo è davvero imbarazzante! Non posso inviare la tua mail! Contatta un amministratore di sistema per segnalare il problema.");
                                            }
                                        }
                                    });
                                };

                                if($('#record_id').length) {
                                    moduleSaveFunction(sendFunction);
                                } else {
                                    sendFunction(sel_id);
                                }

                            }
                        }
                    }
                });
            }
        });
    });
});

function initForm() {
    initClassicTabsEditForm();

    initOrak('mainImages', 10, 0, 0, 100, getOrakImagesToPreattach('mainImages'), false, ['image/jpeg', 'image/png', 'application/pdf']);

    html_editor = initEmailEditor('#versione_html', 'NEWSLETTER', window.template_shortcodes);

    $('.btn_shortcodes').on('click', function() {
        $('#shortcodes_alert').toggle();
    });

    $('#dataTableSendTestTemplate').show();

}

function moduleSaveFunction(callbackSuccess) {

    startLoadingMode('Generazione anteprima');
    var $previewBlock = $('#preview_template_html');
    var width = ($('.bal-content table.main').length ? $('.bal-content table.main').first().css('width').replace('px', '') : 600);
    $previewBlock.html(html_editor.getContentHtml()).css('width', width).css('height', width).show();

    html2canvas($previewBlock[0], {
        x: $previewBlock.offset().left,
        y: $previewBlock.offset().top
    }).then(function(canvas) {
        var templatePreview = canvas.toDataURL("image/png");
        $previewBlock.hide();

        $.ajax({
            url: "db/saveData.php",
            type: "POST",
            data: {
                "pID": $('#record_id').val(),
                "pTipologia": $('#tipologia').val(),
                "pTitolo": $('#titolo').val(),
                "pDocType": '',
                "pHTMLHead": '',
                "pBodyAttribs":'',
                "pVersioneHTML": html_editor.getContentHtml(),
                "pMainImagesAry": composeOrakImagesToSave('mainImages'),
                "pPreviewImage": templatePreview
            },
            async: false,
            dataType: "json",
            success: function(data) {
                if(data.status == "mandatory_data_missing") {
                    bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
                } else if(data.status == "mv_error") {
                    bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
                } else if(data.status == "query_error") {
                    bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
                } else if(data.status == "ok") {
                    succesModuleSaveFunctionCallback(data);
                    if(typeof(callbackSuccess) === 'function') {
                        callbackSuccess(data.id);
                    }
                }
            }
        });
    });
}

function getTemplatePreview(options) {

    startLoadingMode('Generazione anteprima');

    var $previewBlock = $('#preview_template_html');

    var width = ($('.bal-content table.main').length ? $('.bal-content table.main').first().css('width').replace('px', '') : 600);

    $previewBlock.html(html_editor.getContentHtml()).css('width', width).css('height', width).show();

    options = {
        x: $previewBlock.offset().left,
        y: $previewBlock.offset().top
    };

    html2canvas($previewBlock[0], options).then(canvas => {
        var imgageData = canvas.toDataURL("image/png");
        $previewBlock.hide();
        $('#canvas_preview').val('src', imgageData);

        endLoadingMode();
    });

}
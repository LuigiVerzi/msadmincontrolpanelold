<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../../sw-config.php');
require('../../../config/moduleConfig.php');

$Automations = new \MSFramework\Newsletter\automations();

$id_campaign = $_POST['campaign'];
$recipient_id = $_POST['recipient'];

$campaignDetails = (new \MSFramework\Newsletter\campaigns())->getCampaignDetails($id_campaign)[$id_campaign];

$recipient_details = $MSFrameworkDatabase->getAssoc("SELECT * FROM `newsletter__campagne_destinatari_status` WHERE id_campaign = :id_campaign AND id_recipient = :id_recipient", array(':id_campaign' => $id_campaign, ':id_recipient' => $recipient_id), true);

$automations_data = (json_decode($recipient_details['automations_data']) ? json_decode($recipient_details['automations_data'], true) : array());

if(!$automations_data || !$campaignDetails) {
    die('<img style="width: 100px; margin: 0 auto 20px; display: block;" src="https://image.flaticon.com/icons/svg/139/139040.svg"><h3>Non abbiamo ancora info al riguardo, torna a controllare più tardi.</h3>');
}

$diagramma = $campaignDetails['diagramma'];

foreach($diagramma as $k => $item) {
    if($item['type'] == 'condition') {
        $diagramma[$item['id'] . '_result_1'] = array('id' => $item['id'] . '_result_1', 'css' => 'condition_result_1', 'parent' => $item['id'], 'type' => 'condition_result', 'condition_result' => 1);
        $diagramma[$item['id'] . '_result_0'] = array('id' => $item['id'] . '_result_0', 'css' => 'condition_result_0', 'parent' => $item['id'], 'type' => 'condition_result', 'condition_result' => 0);
    }
}

$sort_automations = $automations_data['items'];
foreach($sort_automations as $automation_id => $automation_info) {
    $sort_automations[$automation_id]['id'] = $automation_id;
    $sort_automations[$automation_id]['block'] = $Automations->formatAutomationBlock($diagramma[$automation_id]);
}

usort($sort_automations, function ($item1, $item2) {
    return $item1['passed_date'] > $item2['passed_date'];
});

echo '<div id="automatioHistory">';
foreach($sort_automations as $automation_id => $automation_info) {

    $style = '';
    if($diagramma[$automation_info['id']]['type'] === 'condition') {
        $style = 'border-bottom: 0; padding-bottom: 0;';
    }

    $output = '<div class="automationHistoryBlock" style="' . $style . '">';
    if($diagramma[$automation_info['id']]['type'] !== 'condition_result') {
        $output .= '<div class="automationDate">' . date('d/m/Y H:i:s', $automation_info['passed_date']) . '</div>';
    }
    $output .= '<div class="automationBox type_' . $diagramma[$automation_info['id']]['type'] . ($diagramma[$automation_info['id']]['css'] ? ' ' . $diagramma[$automation_info['id']]['css'] : '') . '">';
    $output .= $automation_info['block'];
    $output .= '</div>';
    $output .= '</div>';

    echo $output;
}
echo '</div>';
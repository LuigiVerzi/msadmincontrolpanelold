<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../../sw-config.php');
require('../../../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();

$id_campaign = $_GET['id'];

$columns = array(
    array(
        'db' => 'id_recipient',
        'dt' => 'DT_RowId'
    ),
    array(
        'db' => "(SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = id_recipient)",
        'dt' => 0,
        'formatter' => function( $d, $row ) {
            Global $MSFrameworkCustomers;

            $r = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

            if(!$r) {
                return '<span class="text-muted">Utente eliminato</span>';
            }

            $info = array();
            if (!empty($r['email'])) {
                $info[] = $r['email'];
            }
            if (!empty($r['telefono_casa'])) {
                $info[] = $r['telefono_casa'];
            }
            if (!empty($r['telefono_cellulare'])) {
                $info[] = $r['telefono_cellulare'];
            }

            $return_html = '';

            $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
            if(json_decode($r['avatar'])) {
                $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
            }

            $return_html .= '<div style="position: relative; padding-left: 65px;">';
            $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
            $return_html .= '<h2 class="no-margins"><a href="#" class="showRecipientInfo" title="Percorso automazione di ' . $d . '" data-id="' . $row[0] . '" data-name="' . $d . '">' . $r['nome'] . ' ' . $r['cognome'] . '</a></h2><small>' . implode(' - ', $info) . '</small>';
            $return_html .= '</div>';

            return $return_html;
        }
    ),
    array(
        'db' => 'start_date',
        'dt' => 1,
        'formatter' => function($d, $row) {
            return array("display" => date('d/m/Y H:i', strtotime($d)), 'sort' => strtotime($d));
        }
    ),
    array(
        'db' => 'date_last_action',
        'dt' => 2,
        'formatter' => function($d, $row) {
            return array("display" => date('d/m/Y H:i', strtotime($d)), 'sort' => strtotime($d));
        }
    ),
    array(
        'db' => 'status',
        'dt' => 3,
        'formatter' => function($d, $row) {

            $status_label = 'Terminata';
            $status_class = 'danger';

            if($d === 1) {
                $status_label = 'In Corso';
                $status_class = 'primary';
            } else if($d === 2) {
                $status_label = 'Uscita forzata (loop)';
                $status_class = 'warning';
            }

            return '<span class="label label-' . $status_class . '">' . $status_label . '</span>';
        }
    )
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'newsletter__campagne_destinatari_status', 'id_recipient', $columns, null, "id_campaign = " . $id_campaign)
);
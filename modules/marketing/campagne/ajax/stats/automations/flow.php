<?php
require_once('../../../../../../sw-config.php');

$id = $_GET['id'];
$type = $_GET['type'];

/* OTTENGO I LABELS */
$minutes = (int)$_POST['minutes'];
$dateFormat = 'd/m/Y H:i';
$steps = 1;

if($minutes >= 300) {
    $steps = 60;
}
else if($minutes >= 120) {
    $steps = 30;
}

$start = new DateTime();
$end = new DateTime();
$start->setTimestamp(time() - ($minutes*60));
$labels = array();
for($i = $start; $i <= $end; $i->modify('+' . $steps . ' minute')) {
    $labels[] = (new DateTime())->setTimestamp($i->getTimestamp());
}

$sql = "SELECT * FROM (";
$sql_split = array();
$sql_split[] = "(SELECT id_campaign as campaign, start_date as date, (SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = id_recipient) as recipient_name, id_recipient as recipient_id, 'enter' as action, '' as ref FROM `newsletter__campagne_destinatari_status`)";
$sql_split[] = "(SELECT (SELECT campaign_id FROM newsletter__emails WHERE newsletter__emails.id = id_email) as campaign, date_sent as date, (SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = id_recipient) as recipient_name, id_recipient as recipient_id,  'email_inviata' as action, (SELECT nome FROM newsletter__emails WHERE newsletter__emails.id = id_email) as ref FROM `newsletter__emails_destinatari_status`)";
$sql_split[] = "(SELECT (SELECT campaign_id FROM newsletter__emails WHERE newsletter__emails.id = id_email) as campaign, date_open as date, (SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = id_recipient) as recipient_name, id_recipient as recipient_id,  'email_letta' as action, (SELECT nome FROM newsletter__emails WHERE newsletter__emails.id = id_email) as ref  FROM `newsletter__emails_destinatari_status` WHERE date_open > date_sent)";
$sql_split[] = "(SELECT (SELECT campaign_id FROM newsletter__emails WHERE newsletter__emails.id = id_email) as campaign, date_open as date, (SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = id_recipient) as recipient_name, id_recipient as recipient_id, 'email_cliccata' as action, (SELECT nome FROM newsletter__emails WHERE newsletter__emails.id = id_email) as ref FROM `newsletter__emails_destinatari_status` WHERE clicked = 1)";
$sql_split[] = "(SELECT (SELECT campaign_id FROM newsletter__sms WHERE newsletter__sms.id = id_sms) as campaign, date_sent as date, (SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = id_recipient) as recipient_name, id_recipient as recipient_id, 'sms_cliccato' as action, (SELECT name FROM newsletter__sms WHERE newsletter__sms.id = id_sms) as ref FROM `newsletter__sms_destinatari_status`)";

if($type === 'automation') {
    $sql_split[] = "(SELECT id_campaign as campaign, date_last_action as date, (SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = id_recipient) as recipient_name, id_recipient as recipient_id, 'automation_end' as action, '' as ref FROM `newsletter__campagne_destinatari_status` WHERE status = 0)";
}

$sql .= implode(" UNION ALL ", $sql_split);
$sql .= ") results";
$sql .= " WHERE (UNIX_TIMESTAMP(results.date) >= " . (time() - ((int)$_POST['minutes']*60)) . ") AND campaign = " . (int)$id;
$sql .= ' ORDER by date ASC';

$actions_labels = array(
    'enter' => '<b>%user%</b> è entrato nella campagna',
    'email_inviata' => '<b>%user%</b> ha ricevuto l\'email <u>%ref%</u>',
    'email_letta' => '<b>%user%</b> ha letto l\'email <u>%ref%</u>',
    'email_cliccata' => '<b>%user%</b> ha cliccato l\'email <u>%ref%</u>',
    'automation_end' => '<b>%user%</b> ha terminato l\'automazione',
    'sms_cliccato' => '<b>%user%</b> ha ricevuto l\'SMS <u>%ref%</u>',
);

$tipologie = array();
$azioni = $MSFrameworkDatabase->getAssoc($sql);

foreach($labels as $timestamp) {

    $currentData = array(
        'x' => $timestamp->format('D, d M y H:i:s'),
        'y' => 0,
        'formatted_date' => $timestamp->format($dateFormat),
        'formatted_actions' => array()
    );

    foreach ($azioni as $azione) {

        if(strtotime($azione['date']) > $timestamp->getTimestamp() && strtotime($azione['date']) < ($timestamp->getTimestamp() + ($steps*60)) ) {
            $currentData['y']++;


            $currentData['formatted_actions'][] =
                '<span class="tooltip-date">' . date($dateFormat, strtotime($azione['date'])) . '</span>'
                . str_replace(
                array('%user%', '%ref%'),
                array('<a href="#" class="showRecipientInfo" data-id="' . $azione['recipient_id'] . '" data-name="' . htmlentities($azione['recipient_name']) . '">' . $azione['recipient_name'] . '</a>', $azione['ref']),
                $actions_labels[$azione['action']]
            );

        }

    }

    $tipologie[] = $currentData;

}

die(json_encode($tipologie));

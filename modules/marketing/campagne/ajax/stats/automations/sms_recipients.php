<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../../sw-config.php');
require('../../../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();

$id_email = $_GET['id'];

$columns = array(
    array(
        'db' => 'id_recipient',
        'dt' => 'DT_RowId'
    ),
    array(
        'db' => "(SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = id_recipient)",
        'dt' => 0,
        'formatter' => function( $d, $row ) {
            Global $MSFrameworkCustomers;

            $r = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

            if(!$r) {
                return '<span class="text-muted">Utente eliminato</span>';
            }

            $info = array();
            if (!empty($r['email'])) {
                $info[] = $r['email'];
            }
            if (!empty($r['telefono_casa'])) {
                $info[] = $r['telefono_casa'];
            }
            if (!empty($r['telefono_cellulare'])) {
                $info[] = $r['telefono_cellulare'];
            }

            $return_html = '';

            $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
            if (json_decode($r['avatar'])) {
                $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
            }

            $return_html .= '<div style="position: relative; padding-left: 65px;">';
            $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
            $return_html .= '<h2 class="no-margins">' . $r['nome'] . ' ' . $r['cognome'] . '</h2><small>' . implode(' - ', $info) . '</small>';
            $return_html .= '</div>';

            return $return_html;
        }
    ),
    array(
        'db' => 'date_sent',
        'dt' => 1,
        'formatter' => function($d, $row) {
            return array("display" => date('d/m/Y H:i', strtotime($d)), 'sort' => strtotime($d));
        }
    ),
    array(
        'db' => 'date_received',
        'dt' => 2,
        'formatter' => function($d, $row) {

            $status_label = 'Non ricevuta';
            $status_class = 'inverse';

            if(strtotime($d) > strtotime('2001-01-01 10:00')) {
                $status_label = 'Ricevuta';
                $status_class = 'success';
            }

            return '<span class="label label-' . $status_class . '">' . $status_label . '</span>';
        }
    )
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'newsletter__sms_destinatari_status', 'id_recipient', $columns, null, "id_sms = " . $id_email)
);
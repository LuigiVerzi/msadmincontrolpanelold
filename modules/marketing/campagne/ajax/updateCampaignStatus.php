<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkNewsletter = new \MSFramework\Newsletter\campaigns();

$ids = $_POST['pID'];
$status = $_POST['pStatus'];

$ids_in = implode(",", array_map(
    function($v) {
        return (int)$v;
    }, $ids)
);

$campaignDetails = $MSFrameworkNewsletter->getCampaignDetails($ids);

if($status == 1) { // Avviata

    // Controllo le spese dei crediti
    foreach($campaignDetails as $campaignDetail) {
        foreach($campaignDetail['diagramma'] as $azione) {
            if ($azione['type'] === 'sms' && $azione['data']['message']) {
                (new \MSFramework\Framework\credits())->checkCreditsForAction('sms');
            }
        }
    }

    // Ottengo la lista degli uploads utilizzati
    $usedAttachments = array();
    foreach($campaignDetails as $campaignDetail) {

        // Se la campagna viene riavviata da una pausa non effettuo le seguenti operazioni
        if($campaignDetail['status'] > 0) continue;

        foreach($campaignDetail['diagramma'] as $azione) {
            if($azione['type'] === 'email') {
                if (isset($azione['data']['attachments']) && json_decode($azione['data']['attachments'])) {
                    $usedAttachments = array_merge($usedAttachments, json_decode($azione['data']['attachments']));
                }
            }
        }
        $usedAttachments = array_unique($usedAttachments);

        // Elimino tutti gli uploads non utilizzati
        foreach (glob(UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN . $campaignDetail['id'] . '/*') as $automationUpload) {
            if(is_file($automationUpload) && !in_array(basename($automationUpload), $usedAttachments)) {
                unlink($automationUpload);
                unlink(UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN . $campaignDetail['id'] . '/tn/' . basename($automationUpload));
            }
        }

        // Avvio la campagna
        $MSFrameworkNewsletter->startCampaign($campaignDetail['id']);
    }

} else if($status == 0) { // Bozza
    $MSFrameworkDatabase->query("DELETE FROM newsletter__campagne_destinatari_status WHERE id_campaign IN ($ids_in)");
    $MSFrameworkDatabase->query("DELETE FROM newsletter__emails_destinatari_status WHERE id_email IN (SELECT id FROM newsletter__emails WHERE newsletter__emails.campaign_id IN ($ids_in))");
    $MSFrameworkDatabase->query("DELETE FROM newsletter__emails WHERE campaign_id IN ($ids_in)");
    $MSFrameworkDatabase->query("DELETE FROM newsletter__sms WHERE campaign_id IN ($ids_in)");
    $MSFrameworkDatabase->query("DELETE FROM newsletter__sms_destinatari_status WHERE id_sms IN (SELECT id FROM newsletter__sms WHERE newsletter__sms.campaign_id IN ($ids_in))");
} elseif($status == 4) { // Archiviata
    foreach($ids as $id) {
        // Elimino tutti gli UPLOADS
        foreach(array(UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN . $id . '/tn', UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN . $id) as $dir_to_delete) {
            array_map('unlink', glob("$dir_to_delete/*.*"));
            rmdir($dir_to_delete);
        }
    }
}

$MSFrameworkDatabase->query("UPDATE newsletter__campagne SET status = :status WHERE id IN ($ids_in)", array(':status' => $status));

die(json_encode(array('status' => 'ok')));
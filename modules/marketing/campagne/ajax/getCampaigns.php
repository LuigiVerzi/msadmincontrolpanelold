<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$campaigns = new \MSFramework\Newsletter\campaigns();

$campaignsStatus = $campaigns->getCampaignsStatusLabel();
$campaignsTypes = $campaigns->getCampaignsTypes();

$statusFilter = (isset($_GET['f']) ? $_GET['f'] : '');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__campagne WHERE status " . ($statusFilter !== '' ? "= " . (int)$statusFilter : '!= 4')) as $r) {

    $extraBtn = array();

    foreach($campaignsStatus[$r['status']]['markable_to'] as $availBtn) {
        $extraBtn[] =  array(
            'label' => $campaignsStatus[$availBtn]['actionText'],
            'btn' => $campaignsStatus[$availBtn]['color'],
            'icon' => $campaignsStatus[$availBtn]['icon'],
            'class' => 'campaignSet_' . $availBtn,
            'function' => 'campaignSet_' . $availBtn,
            'context' => true
        );
    }

    // Se i pulsanti sono più di uno allora li raggruppo
    if(count($campaignsStatus[$r['status']]['markable_to']) > 1) {
        $extraBtn = array(
            array(
                'label' => "Stato",
                'btn' => 'default',
                'full' => true,
                'icon' => 'gear',
                'childrens' => $extraBtn
            )
        );
    }

    if($r['status'] == "0") {
        $extraBtn[] = array(
            'label' => "Modifica Automazione",
            'btn' => 'warning',
            'full' => true,
            'icon' => 'pencil',
            'class' => 'campaignView',
            'function' => 'campaignView',
            'context' => false
        );
    } else {
        $extraBtn[] = array(
            'label' => "Vedi Automazione",
            'btn' => 'info',
            'full' => true,
            'icon' => 'eye',
            'class' => 'campaignView',
            'function' => 'campaignView',
            'context' => false
        );
    }

    $extraBtn[] = array(
        'label' => "Duplica Automazione",
        'btn' => 'info',
        'icon' => 'files-o',
        'class' => 'campaignDuplication',
        'function' => 'campaignDuplication',
        'context' => false
    );

    $statistiche = array();

    $statistiche[] = '<span class="label label-primary" title="Contatti totali"><i class="fa fa-users"></i> ' . $MSFrameworkDatabase->getCount("SELECT id_campaign FROM `newsletter__campagne_destinatari_status` WHERE id_campaign = :id_campaign", array(':id_campaign' => $r['id'])) . '</span>';
    $statistiche[] = '<span class="label label-success" title="Email inviate"><i class="fa fa-envelope"></i> ' . $MSFrameworkDatabase->getCount("SELECT id_email FROM `newsletter__emails_destinatari_status` WHERE id_email IN(SELECT id FROM newsletter__emails WHERE campaign_id = :id_campaign)", array(':id_campaign' => $r['id'])) . '</span>';
    $statistiche[] = '<span class="label label-success" title="SMS inviati"><i class="fa fa-sms"></i> ' . $MSFrameworkDatabase->getCount("SELECT id_sms FROM `newsletter__sms_destinatari_status` WHERE id_sms IN(SELECT id FROM newsletter__sms WHERE campaign_id = :id_campaign)", array(':id_campaign' => $r['id'])) . '</span>';
    if($r['type'] === 'automation') {
        $statistiche[] = '<span class="label label-danger" title="Automazione terminata"><i class="fa fa-ban"></i> ' . $MSFrameworkDatabase->getCount("SELECT id_campaign FROM `newsletter__campagne_destinatari_status` WHERE id_campaign = :id_campaign AND status != 1", array(':id_campaign' => $r['id'])) . '</span>';
    }

    $array['data'][] = array(
        '<h2 style="margin: 0;">' . $r['nome'] . '</h2>
        <span class="label label-inverse" style="background: ' . $campaignsTypes[$r['type']]['color'] . ';"><i class="fa fa-' . $campaignsTypes[$r['type']]['icon'] . '"></i> ' . $campaignsTypes[$r['type']]['label'] . '</span>
        <span class="label label-' . $campaignsStatus[$r['status']]['color'] . '"><i class="fa fa-' . $campaignsStatus[$r['status']]['icon'] . '"></i> ' . $campaignsStatus[$r['status']]['label'] . '</span>',
        array("display" =>  date('d/m/Y H:i', strtotime($r['data_creazione'])), 'sort' => strtotime($r['data_creazione'])),
        implode(' ', $statistiche),
        "DT_RowId" => $r['id'],
        "DT_ExtraBtn" => $extraBtn,
        "DT_buttonsLimit" => "read"
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
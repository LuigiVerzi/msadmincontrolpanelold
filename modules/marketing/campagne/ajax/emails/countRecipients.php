<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../../config/moduleConfig.php');

$count = 0;

// Se selezionati conto il numero di destinatari selezionati tramite liste (Escludendo i destiantari delle liste escluse)
if(count($_POST['include_lists'])) {
    $sqlListe = $MSFrameworkDatabase->getReplacedQuery(
        "SELECT destinatario FROM newsletter__tag_destinatari include WHERE 
        " . (count($_POST['exclude_lists']) > 0 ? "NOT EXISTS(SELECT null FROM newsletter__tag_destinatari exclude WHERE include.destinatario = exclude.destinatario AND exclude.tag IN(:exclude_lists)) AND " : "") .
        "include.tag IN(:include_lists) AND (SELECT active FROM newsletter__destinatari WHERE newsletter__destinatari.id = include.destinatario) = 1 GROUP BY destinatario",
        array(':include_lists' => implode("','", $_POST['include_lists']), ':exclude_lists' => implode("','", $_POST['exclude_lists']))
    );

    $count = $MSFrameworkDatabase->getCount($sqlListe);
}

if(count($_POST['include_emails'])) {
    $include_emails = array_unique($_POST['include_emails']);

    // Inizialmente aggiungo al contatore tutti gli indirizzi inseriti manualmente
    $count += count($include_emails);

    // Controllo se alcune email inserite manualmente sono già presenti in qualche lista precedente e nel caso li rimuovo dal conteggio per evitare duplicati
    if(count($_POST['include_lists']) > 0) {
        $sqlAddMail = $MSFrameworkDatabase->getReplacedQuery(
            "SELECT id FROM newsletter__destinatari includeMaiLDest WHERE (SELECT email FROM customers WHERE customers.id = includeMaiLDest.id) IN (:include_emails) AND includeMaiLDest.id IN ($sqlListe)",
            array(':include_emails' => implode("','", $include_emails))
        );

        $count -= $MSFrameworkDatabase->getCount($sqlAddMail);
    }
}

if(count($_POST['exclude_emails'])) {
    $exclude_emails = array_unique($_POST['exclude_emails']);

    if(count($_POST['include_lists']) > 0) {
        $sqlAddMail = $MSFrameworkDatabase->getReplacedQuery(
            "SELECT id FROM newsletter__destinatari excludeMaiLDest WHERE (SELECT email FROM customers WHERE customers.id = excludeMaiLDest.id) IN (:exclude_emails) AND excludeMaiLDest.id IN ($sqlListe)",
            array(':exclude_emails' => implode("','", $exclude_emails))
        );

        $count -= $MSFrameworkDatabase->getCount($sqlAddMail);
    }
}

echo $count;
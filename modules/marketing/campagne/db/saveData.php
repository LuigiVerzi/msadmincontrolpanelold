 <?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$automationInfo = (new \MSFramework\Newsletter\campaigns())->getCampaignDetails($_POST['pID'])[$_POST['pID']];

if(!$automationInfo) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(!is_array($_POST['pDiagramma']) || !$_POST['pDiagramma']) {
    echo json_encode(array("status" => "automation_missing"));
    die();
}

if(!is_array($_POST['pOrigin']) || !$_POST['pOrigin']['tags']) {
    echo json_encode(array("status" => "origin_missing"));
    die();
}

if($automationInfo['type'] === 'standard') {
    $have_email = false;
    $have_sms = false;
    foreach ($_POST['pDiagramma'] as $azione) {
        if ($azione['type'] === 'email' && $azione['data'] && $azione['data']['nome']) {
            $have_email = true;
        } else if ($azione['type'] === 'sms' && $azione['data'] && $azione['data']['message']) {
            $have_sms = true;
        }
    }

    if (!$have_email && !$have_sms) {
        echo json_encode(array("status" => "email_missing"));
        die();
    }
}


foreach($_POST['pDiagramma'] as $azione) {
    if($azione['type'] == 'connect') {
        if($azione['data']['destination'] == 'err') {
            echo json_encode(array("status" => "connection_error"));
            die();
        }
    }
}

$array_to_save = array(
    "origin" => json_encode($_POST['pOrigin']),
    "diagramma" => json_encode($_POST['pDiagramma'])
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'update');
$result = $MSFrameworkDatabase->pushToDB("UPDATE newsletter__campagne SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ""));
die();
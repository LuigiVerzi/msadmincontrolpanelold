<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../../config/moduleConfig.php');

$type = $_POST['type'];

$c = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__campagne WHERE id = :id", array(':id' => $_POST['campaign_id']), true);

$_POST = $_POST['data'];
if(in_array($type, array('email', 'sms', 'whatsapp', 'condition', 'wait', 'wait_jump', 'connect', 'start_automation')) && file_exists('check/' . $type . '.php')) {
    include('check/' . $type . '.php');
} else {
    die(json_encode(array('status' => 'query_error')));
}
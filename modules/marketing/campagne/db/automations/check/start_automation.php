<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

$can_save = true;
if(!count($_POST['tags'])) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "behavior" => $_POST['behavior'],
    "tags" => $_POST['tags']
);

echo json_encode(array("status" => "ok", "data" => $array_to_save));
die();
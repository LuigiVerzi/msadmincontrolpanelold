<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

$can_save = true;
if(empty($_POST['pDescription']) || !is_array($_POST['pConditions'])) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "description" => $_POST['pDescription'],
    "conditions" => $_POST['pConditions'],
    "wait_value" => $_POST['pWaitValue'],
    "wait_what" => $_POST['pWaitWhat']
);

echo json_encode(array("status" => "ok", "data" => $array_to_save));
die();
<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

$can_save = true;
if($_POST['pName'] == "" || $_POST['pSender'] == "" || $_POST['pMessage'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "name" => $_POST['pName'],
    "sender" => $_POST['pSender'],
    "message" => $_POST['pMessage'],
);

echo json_encode(array("status" => "ok", "data" => $array_to_save));
die();
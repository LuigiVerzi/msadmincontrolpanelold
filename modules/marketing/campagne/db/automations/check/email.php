<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

$can_save = true;
if($_POST['pNome'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$html = new \Html2Text\Html2Text($_POST['pCustomLayoutHTML']);
$_POST['pCustomLayoutTXT'] = $html->getText();

/* Copio gli allegati presi dai template nella cartella uploads dell'automazione */
foreach($_POST['pEmailImagesAry'] as $uploadImage) {
    if(file_exists(UPLOAD_NEWSLETTER_FOR_DOMAIN . $uploadImage)) {
        copy(UPLOAD_NEWSLETTER_FOR_DOMAIN . $uploadImage, UPLOAD_TMP_FOR_DOMAIN . $uploadImage);
        copy(UPLOAD_NEWSLETTER_FOR_DOMAIN . 'tn/' . $uploadImage, UPLOAD_TMP_FOR_DOMAIN . 'tn/' . $uploadImage);
    }
}

$uploader = new \MSFramework\uploads('MARKETING_AUTOMATIONS');

$uploader->path_html .= (int)$c['id'] . '/';
$uploader->path .= (int)$c['id'] . '/';

$ary_files = $uploader->prepareForSave($_POST['pEmailImagesAry'], array('jpeg', 'png', 'gif', 'pdf', 'bmp', 'doc', 'docx'));
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "mittente" => $_POST['pMittente'],
    "smtp" => $_POST['pSmtp'],
    "custom_layout_html" => $_POST['pCustomLayoutHTML'],
    "custom_layout_txt" => $_POST['pCustomLayoutTXT'],
    "attachments" => json_encode($_POST['pEmailImagesAry'])
);


echo json_encode(array("status" => "ok", "data" => $array_to_save));
die();
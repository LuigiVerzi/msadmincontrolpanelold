<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

if((int)$_POST['pValue'] <= 0 || $_POST['pWhat'] == ""|| $_POST['pTarget'] == "") {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "value" => $_POST['pValue'],
    "what" => $_POST['pWhat'],
    "target" => $_POST['pTarget']
);

echo json_encode(array("status" => "ok", "data" => $array_to_save));
die();
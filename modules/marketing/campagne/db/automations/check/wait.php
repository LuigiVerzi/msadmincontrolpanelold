<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

$can_save = true;
if((int)$_POST['pValue'] <= 0 || $_POST['pWhat'] == "") {
    $can_save = false;
}

if($_POST['pType'] === 'date') {

    $date_check = \DateTime::createFromFormat('d/m/Y H:i', $_POST['pDate']);

    if($date_check && $date_check->format('U') > time()) {
        $date_check = $date_check->format('U');
    } else {
        echo json_encode(array("status" => "date_error"));
        die();
    }

} else if((int)$_POST['pValue'] <= 0 || $_POST['pWhat'] == "") {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "type" => $_POST['pType'],
    "date" => $date_check,
    "value" => $_POST['pValue'],
    "what" => $_POST['pWhat']
);

echo json_encode(array("status" => "ok", "data" => $array_to_save));
die();
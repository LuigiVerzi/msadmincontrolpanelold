<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;
if ($_POST['pID'] == "" || $_POST['pName'] == "") {
    $can_save = false;
}

if (!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pName'],
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'update');
$result = $MSFrameworkDatabase->pushToDB("UPDATE newsletter__campagne SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));

if (!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => $MSFrameworkDatabase->lastInsertId()));
die();
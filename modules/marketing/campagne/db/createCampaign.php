<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;

if($_POST['pNome'] == "" || !in_array($_POST['pTipo'], array_keys((new \MSFramework\Newsletter\campaigns())->getCampaignsTypes()))) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "type" => $_POST['pTipo']
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
$result = $MSFrameworkDatabase->pushToDB("INSERT INTO newsletter__campagne ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => $MSFrameworkDatabase->lastInsertId()));
die();
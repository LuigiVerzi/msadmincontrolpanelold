<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;

$origin_automation = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__campagne WHERE id = :origin_id", array(':origin_id' => $_POST['pOrigin']), true);

if ($_POST['pNome'] == "" || !$origin_automation) {
    $can_save = false;
}

if (!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "type" => $origin_automation['type'],
    "origin" => $origin_automation['origin'],
    "diagramma" => $origin_automation['diagramma']
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
$result = $MSFrameworkDatabase->pushToDB("INSERT INTO newsletter__campagne ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

if (!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$destination_id = $MSFrameworkDatabase->lastInsertId();

echo json_encode(array("status" => "ok", "id" => $MSFrameworkDatabase->lastInsertId()));
die();
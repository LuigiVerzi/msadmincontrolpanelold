<h1>Generali</h1>
<fieldset>
<div id="automationsList">

    <div id="automationContainer">
        <div id="automation_settings_container" style="display: none;">

        </div>

        <textarea id="automationOrigin" style="display: none;"><?= json_encode($origin); ?></textarea>

        <div class="hideOnEdit">
            <h1 class="title-with-button" style="margin-bottom: 0;">
                Automazioni
                <a href="#" class="btn btn-default automationZoom" data-zoom="+"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                <a href="#" class="btn btn-default automationZoom" data-zoom="-"><i class="fa fa-search-minus" aria-hidden="true"></i></a>
                <a href="#" class="btn btn-default automationFullscreen" data-zoom="-"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                <a href="#" class="btn btn-danger btn-outline automationCancelMoving" data-zoom="-"><i class="fa fa-arrows" aria-hidden="true"></i> Annulla spostamento</a>

                <div class="btn-group historyAutomation actionsGroupBtn">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><i class="fa fa-history" aria-hidden="true"></i> Cronologia</button>
                    <ul class="dropdown-menu" id="historyList">

                    </ul>
                </div>

            </h1>

            <div class="automationScroll dragscroll">
                <div id="automationsContent" class="contentList">
                    <?php include('includes/view/automation/list.php'); ?>
                </div>
            </div>
        </div>

    </div>
</div>
</fieldset>

<div class="modal inmodal" id="moveAutomationSelection" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-arrows-alt modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Copia/Muovi azione</h4>
                <div class="text-muted">Seleziona il comportamento da adottare</div>
            </div>
            <div class="modal-body" style="padding-bottom: 20px;">
                <div class="row">
                    <div class="col-sm-6">
                        <h2 style="margin: 0 0 15px;">Muovi</h2>
                        <div class="single i-checks" style="margin-bottom: 10px;"><label> <input type="radio" value="move_single" name="actionMovingBehavior" data-btncta="Muovi qui"> <i></i> Muovi singola azione </label></div>
                        <div class="all i-checks"><label> <input type="radio" value="move_all" name="actionMovingBehavior" data-btncta="Muovi qui"> <i></i> Muovi tutte le azioni a seguire </label></div>
                    </div>
                    <div class="col-sm-6">
                        <h2 style="margin: 0 0 15px;">Copia</h2>
                        <div class="single i-checks" style="margin-bottom: 10px;"><label> <input type="radio" value="copy_single" name="actionMovingBehavior" data-btncta="Copia qui"> <i></i> Copia singola azione </label></div>
                        <div class="all i-checks"><label> <input type="radio" value="copy_all" name="actionMovingBehavior" data-btncta="Copia qui"> <i></i> Copia tutte le azioni a seguire </label></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="cancelMoveAutomation" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary" id="confirmMoveAutomation" data-dismiss="modal">Continua</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="conditionResultsMissing" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-arrows-alt modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Condizione <b class="condition_name"></b></h4>
                <div class="text-muted">Sotto quale ramo desideri spostare le azioni non categorizzate?</div>
            </div>
            <div class="modal-body" style="padding-bottom: 20px;">
                <div class="row">
                    <div class="col-sm-6">
                        <button class="btn btn-primary dim btn-large-dim moveOn" type="button" data-status="1" style="width: 100%;">Si</button>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn btn-danger dim btn-large-dim moveOn" type="button" data-status="0" style="width: 100%;">No</button>
                    </div>
                    <div class="col-sm-12">
                        <button class="btn btn-default dim btn-medium-dim deleteCondition" type="button" data-status="0" style="width: 100%;">Elimina condizione</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="deleteConditionModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-question modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Vuoi eliminare la condizione <b class="condition_name"></b>?</h4>
                <div class="text-muted">Come vuoi comportarti con  le azioni successive alla condizione <b class="condition_name"></b>?</div>
            </div>
            <div class="modal-body" style="padding-bottom: 20px;">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="i-checks" style="margin-bottom: 10px;"><label> <input type="radio" value="1" name="deleteConditionBehavior"> <i></i> <b>Mantieni</b> solo il percorso <b>'Si'</b> </label></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="i-checks" style="margin-bottom: 10px;"><label> <input type="radio" value="0" name="deleteConditionBehavior"> <i></i> <b>Mantieni</b> solo il percorso <b>'No'</b> </label></div>
                    </div>
                    <div class="col-sm-12">
                        <div class="i-checks"><label> <input type="radio" value="all" name="deleteConditionBehavior"> <i></i> Elimina <b>tutte le azioni successive</b> </label></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="cancelDeleteAutomation" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary" id="confirmDeleteAutomation" data-dismiss="modal">Conferma eliminazione</button>
            </div>
        </div>
    </div>
</div>

<h1><i class="fa fa-dashboard"></i> Resoconto</h1>
<fieldset>
    <?php if($r['status'] !== 4) { ?>
        <!--
        <h1 class="title-with-button" style="margin-top: -15px;">
            Andamento in tempo reale
            <div class="text-right">
                <select type="text" id="last_actions_daterange" class="form-control">
                    <option value="5" data-interval="1">Ultimi 5 minuti</option>
                    <option value="10" data-interval="1">Ultimi 10 minuti</option>
                    <option value="30" data-step="1">Ultimi 30 minuti</option>
                    <option value="120" data-step="30">Ultime 2 ore</option>
                    <option value="300" data-step="60">Ultime 5 ore</option>
                    <option value="1440" data-step="60" selected>Ultime 24 ore</option>
                </select>
            </div>
            <span class="title-descr">
            Auto refresh tra <b id="autoRefreshTimer">10</b> secondi
        </span>
        </h1>
        <canvas id="lastActionsChart" height="350" width="100%" style="min-height: 350px; max-height: 350px; margin-top: 15px;"></canvas>
        -->
        <h1 style="margin: 26px 0;">
            Campagna <b><?= $r['nome']; ?></b>
            <small class="pull-right text-right" style="font-size: 16px;" title="<?= date('d/m/Y H:i', strtotime($r['data_creazione'])); ?>">
                <button type="button" class="btn btn-inverse btn-disabled m-l-sm pull-right" disabled="">
                    <i class="fa fa-hourglass-o"></i>
                </button>
                Campagna avviata <br><b><?= (new \MSFramework\utils())->smartdate(strtotime($r['data_creazione'])); ?></b>
            </small>
            <div style="clear: both;"></div>
        </h1>
    <?php } ?>

    <h2 class="title-divider" style="border-right: 1px solid #ececec;">Origine</h2>

    <h4 class="text-navy" style="margin: 0 0 5px;">
        <?php
        if($origin['behavior'] == 'only_presents') {
            echo 'Solo i <u>contatti già iscritti</u> alle seguenti liste:';
        } else if($origin['behavior'] == 'only_future') {
            echo 'Solo i <u>futuri contatti</u> iscritti alle seguenti liste:';
        }else if($origin['behavior'] == 'both') {
            echo 'Gli <u>attuali e futuri contatti</u> iscritti alle seguenti liste:';
        }
        ?>
    </h4>

    <ul class="tag-list" style="padding: 0">
        <?php foreach((new \MSFramework\Newsletter\lists())->getTagDetails($origin['tags']) as $tag) { ?>
            <li><a href="../destinatari/tag/edit.php?id=<?= $tag['id']; ?>" target="_blank"><i class="fa fa-tag"></i> Lista <?= $tag['nome']; ?></a></li>
        <?php } ?>
    </ul>

    <div style="clear: both;"></div>

    <div class="row">
        <div class="col-sm-6">
            <h2 class="title-divider">Contatti</h2>

            <div class="row">
                <div class="col-sm-6">
                    <canvas id="contactsStatusChart" height="140"></canvas>
                </div>
                <div class="col-sm-6">
                    <ul class="list-group clear-list m-t">
                        <li class="list-group-item" style="border-top: none;">
                            <span class="label label-default" id="contactsStatus_Total"><?= $MSFrameworkDatabase->getCount("SELECT id_recipient FROM newsletter__campagne_destinatari_status WHERE id_campaign = :id_campaign", array(':id_campaign' => $r['id'])); ?></span> Contatti totali
                        </li>
                        <li class="list-group-item">
                            <span class="label label-primary" id="contactsStatus_Active"><?= $MSFrameworkDatabase->getCount("SELECT id_recipient FROM newsletter__campagne_destinatari_status WHERE id_campaign = :id_campaign AND date_last_action > start_date AND status = 1", array(':id_campaign' => $r['id'])); ?></span> Contatti attivi
                        </li>
                        <li class="list-group-item">
                            <span class="label label-warning" id="contactsStatus_Waiting"><?= $MSFrameworkDatabase->getCount("SELECT id_recipient FROM newsletter__campagne_destinatari_status WHERE id_campaign = :id_campaign AND date_last_action <= start_date", array(':id_campaign' => $r['id'])); ?></span> Contatti in attesa
                        </li>
                        <li class="list-group-item">
                            <span class="label label-danger" id="contactsStatus_End"><?= $MSFrameworkDatabase->getCount("SELECT id_recipient FROM newsletter__campagne_destinatari_status WHERE id_campaign = :id_campaign AND status != 1", array(':id_campaign' => $r['id'])); ?></span> Contatti usciti
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <h2 class="title-divider">Email</h2>

            <div class="row">
                <div class="col-sm-6">
                    <canvas id="emailsStatusChart" height="140"></canvas>
                </div>
                <div class="col-sm-6">
                    <ul class="list-group clear-list m-t">
                        <li class="list-group-item" style="border-top: none;">
                            <span class="label label-default" id="emailsStatus_Sent"><?= $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email IN (SELECT id FROM newsletter__emails WHERE newsletter__emails.campaign_id = :id_campaign)", array(':id_campaign' => $r['id'])); ?></span> Email inviate
                        </li>
                        <li class="list-group-item">
                            <span class="label label-primary" id="emailsStatus_Read"><?= $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email IN (SELECT id FROM newsletter__emails WHERE newsletter__emails.campaign_id = :id_campaign) AND date_open > date_sent", array(':id_campaign' => $r['id'])); ?></span> Email aperte
                        </li>
                        <li class="list-group-item">
                            <span class="label label-success" id="emailsStatus_Click"><?= $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email IN (SELECT id FROM newsletter__emails WHERE newsletter__emails.campaign_id = :id_campaign) AND clicked > 0", array(':id_campaign' => $r['id'])); ?></span> Click sui link
                        </li>
                        <li class="list-group-item">
                            <span class="label label-danger" id="emailsStatus_Unsubscribe"><?= $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email IN (SELECT id FROM newsletter__emails WHERE newsletter__emails.campaign_id = :id_campaign) AND status = 3", array(':id_campaign' => $r['id'])); ?></span> Disiscrizioni
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <h1 class="title-divider">
        Contatti
    </h1>

    <table id="recipients_list" class="display table table-striped table-hover" width="100%" data-readonly="true" cellspacing="0">
        <thead>
        <tr>
            <th style="width: 300px;">Destinatario</th>
            <th class="is_data">Inizio Campagna</th>
            <th class="is_data default-sort" data-sort="DESC">Ultima azione</th>
            <th>Stato</th>
        </tr>
        </thead>
    </table>

    <?php if($MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails` WHERE campaign_id = :campaign_id", array(':campaign_id' => $r['id']))) { ?>
        <h1 class="title-divider">Email</h1>
        <?php include('includes/view/automation/stats/emails.php'); ?>
    <?php } ?>

    <?php if($MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__sms` WHERE campaign_id = :campaign_id", array(':campaign_id' => $r['id']))) { ?>
        <h1 class="title-divider">SMS</h1>
        <?php include('includes/view/automation/stats/sms.php'); ?>
    <?php } ?>

    <?php
    if(GMAPS_API_KEY != "") {
        include('includes/view/automation/stats/map.php');
    }
    ?>

</fieldset>

<h1><i class="fa fa-magic"></i> Automazione</h1>
<fieldset>
    <div id="automationsList" style="margin-top: 0;">
        <div id="automationContainer" class="view_mode">
            <div style="margin: -15px -15px -15px;">
                <h1 class="title-with-button" style="margin: 0;">
                    Automazione
                    <a href="#" class="btn btn-default automationZoom" data-zoom="+"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                    <a href="#" class="btn btn-default automationZoom" data-zoom="-"><i class="fa fa-search-minus" aria-hidden="true"></i></a>
                    <a href="#" class="btn btn-default automationFullscreen" data-zoom="-"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                </h1>
                <div class="automationScroll dragscroll" style="margin: 0;">
                    <div id="automationsContent" class="contentList">
                        <?php include('includes/view/automation/list.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>

<div class="modal inmodal" id="recipientHistory" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-history modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Percorso automazione di <span class="name"></span></h4>
                <div class="text-muted">La lista delle azioni effettuate dal contatto all'interno dell'automazione.</div>
            </div>
            <div class="modal-body" style="padding-bottom: 20px;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>
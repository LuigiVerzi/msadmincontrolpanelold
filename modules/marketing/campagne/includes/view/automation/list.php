<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

if(isset($r)) {
    $campaign_id = $r['id'];
    $campagna = $r;
    $diagramma = ($campagna['diagramma'] && json_decode($campagna['diagramma']) ? json_decode($campagna['diagramma'], true) : array());
} else {
    require_once('../../../../../../sw-config.php');
    require('../../../config/moduleConfig.php');

    $campaign_id = $_POST['c'];
    $campagna = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__campagne WHERE id = :campaign_id", array(':campaign_id' => $campaign_id), true);
    $diagramma = $_POST['diagramma'];
    $origin = $_POST['origin'];
}

$diagramma = (new \MSFramework\Newsletter\automations())->prepareDiagram($diagramma);

$automations_list = (new \MSFramework\Newsletter\automations())->drawAutomationChart($diagramma, "", ($campagna['status'] && $campagna['status'] !== 0), $origin);

if($automations_list) {
    echo $automations_list;
} else { ?>
    <div class="no_automations_found text-center p-lg">
        <i class="fa fa-magic fa-5x" aria-hidden="true"></i>
        <h1 class="m-b-md">Non hai ancora creato nessuna automazione</h1>
       <?= (new \MSFramework\Newsletter\automations())->getInsertBtn('0'); ?>
    </div>
<?php } ?>

<textarea id="automationJSON" style="display: none;"><?= json_encode($diagramma, JSON_FORCE_OBJECT); ?></textarea>

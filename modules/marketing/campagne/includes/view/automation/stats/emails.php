<?php
$i = 0;
foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `newsletter__emails` WHERE campaign_id = :campaign_id", array(':campaign_id' => $r['id'])) as $email_detail) {
    $i++;

    $clicked_email_links = $MSFrameworkDatabase->getAssoc("SELECT GROUP_CONCAT(clicked_info SEPARATOR ',') as clicked_info FROM `newsletter__emails_destinatari_status` WHERE id_email = :id_email AND clicked_info != ''", array(':id_email' => $email_detail['id']), true)['clicked_info'];

    $top_email_links = array();
    if($clicked_email_links) {
        foreach(explode(',', $clicked_email_links) as $clicked_links) {
            if(!isset($top_email_links[$clicked_links]))  $top_email_links[$clicked_links] = 0;
            $top_email_links[$clicked_links]++;
        }

        arsort($top_email_links);
    }

    $email_stats = array(
        'sent' => $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email = :id_email", array(':id_email' => $email_detail['id'])),
        'received' => $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email = :id_email AND status = 1", array(':id_email' => $email_detail['id'])),
        'not_received' => $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email = :id_email AND status = 0", array(':id_email' => $email_detail['id'])),
        'opening' => $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email = :id_email AND date_open > date_sent", array(':id_email' => $email_detail['id'])),
        'click' => $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email = :id_email AND clicked > 0", array(':id_email' => $email_detail['id'])),
        'bounced' => $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email = :id_email AND bounced > 0", array(':id_email' => $email_detail['id'])),
        'unsubscribed' => $MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE id_email = :id_email AND status = 3", array(':id_email' => $email_detail['id']))
    );
    ?>

    <div class="ibox <?= ($i > 1 ? 'collapsed' : ''); ?>">
        <div class="ibox-title">
            <i class="ibox-icon fa fa-envelope fa-3x"></i>
            <h5><?= $email_detail['nome']; ?></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="fullscreen-link">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <table class="table table-bordered text-center m-b-md">
                <tbody>
                <tr>
                    <td>
                        <i class="fa fa-envelope-o fa-2x" style="display: block; margin: 0 0 5px;"></i> <strong style="display: block;"><?= $email_stats['sent']; ?></strong> <span class="text-helper" title="Il numero di email inviate">Invii totali</span>
                    </td>
                    <td>
                        <i class="fa fa-envelope-open-o fa-2x" style="display: block; margin: 0 0 5px;"></i> <strong style="display: block;"><?= $email_stats['opening']; ?></strong> <span class="text-helper" title="Il numero di destinatari che ha aperto l'email">Apertura</span>
                    </td>
                    <td>
                        <i class="fa fa-mouse-pointer fa-2x" style="display: block; margin: 0 0 5px;"></i> <strong style="display: block;"><?= $email_stats['click']; ?></strong> <span class="text-helper" title="Si intende il numero dei click sui link all'interno dell'email">Click sui link</span>
                    </td>
                    <td>
                        <i class="fa fa-undo fa-2x" style="display: block; margin: 0 0 5px;"></i> <strong style="display: block;"><?= $email_stats['bounced']; ?></strong> <span class="text-helper" title="Il numero di email che non sono arrivate a destinazione">Rimbalzate</span>
                    </td>
                    <td>
                        <i class="fa fa-user-times fa-2x" style="display: block; margin: 0 0 5px;"></i> <strong style="display: block;"><?= $email_stats['unsubscribed']; ?></strong> <span class="text-helper" title="Il numero di persone che si sono disiscritte dalla newsletter tramite questa email">Disiscritti</span>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="row" style="margin-top: 15px;">
                <div class="col-sm-4">
                    <div>
                        <label>Tasso di consegna</label>
                        <small class="pull-right"><?= $email_stats['received']; ?>/<?= $email_stats['sent']; ?> (<?= ($email_stats['received'] > 0 ? round(($email_stats['received']/$email_stats['sent']) * 100) : 0); ?>%)</small>
                    </div>
                    <div class="progress progress-small">
                        <div style="width: <?= (($email_stats['received']/$email_stats['sent']) * 100); ?>%;" class="progress-bar"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div>
                        <label>Tasso di rimbalzo</label>
                        <small class="pull-right"><?= $email_stats['bounced']; ?>/<?= $email_stats['received']; ?> (<?= ($email_stats['bounced'] > 0 ? round(($email_stats['bounced']/$email_stats['received']) * 100) : 0); ?>%)</small>
                    </div>
                    <div class="progress progress-small">
                        <div style="width: <?= (($email_stats['bounced']/$email_stats['received']) * 100); ?>%;" class="progress-bar"></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div>
                        <label>Tasso di cancellazione</label>
                        <small class="pull-right"><?= $email_stats['unsubscribed']; ?>/<?= $email_stats['received']; ?> (<?= ($email_stats['bounced'] > 0 ? round(($email_stats['unsubscribed']/$email_stats['received']) * 100) : 0); ?>%)</small>
                    </div>
                    <div class="progress progress-small">
                        <div style="width: <?= (($email_stats['unsubscribed']/$email_stats['received']) * 100); ?>%;" class="progress-bar"></div>
                    </div>
                </div>
            </div>

            <div class="hr-line-dashed"></div>

            <div class="row">
                <div class="col-sm-4">
                    <div>
                        <label>Tasso di apertura</label>
                        <small class="pull-right"><?= $email_stats['opening']; ?>/<?= $email_stats['received']; ?> (<?= ($email_stats['opening'] > 0 ? round(($email_stats['opening']/$email_stats['received']) * 100) : 0); ?>%)</small>
                    </div>
                    <div class="progress progress-small">
                        <div style="width: <?= (($email_stats['opening']/$email_stats['received']) * 100); ?>%;" class="progress-bar"></div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div>
                        <label>Tasso di click</label>
                        <small class="pull-right"><?= $email_stats['click']; ?>/<?= $email_stats['received']; ?> (<?= ($email_stats['click'] > 0 ? round(($email_stats['click']/$email_stats['received']) * 100) : '0'); ?>%)</small>
                    </div>
                    <div class="progress progress-small">
                        <div style="width: <?= (($email_stats['click']/$email_stats['received']) * 100); ?>%;" class="progress-bar"></div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div>
                        <label>Tasso di click su apertura</label>
                        <small class="pull-right"><?= $email_stats['click']; ?>/<?= $email_stats['opening']; ?> (<?= ($email_stats['click'] > 0 ? round(($email_stats['click']/$email_stats['opening']) * 100) : '0'); ?>%)</small>
                    </div>
                    <div class="progress progress-small">
                        <div style="width: <?= (($email_stats['click']/$email_stats['opening']) * 100); ?>%;" class="progress-bar"></div>
                    </div>
                </div>
            </div>

            <div class="hr-line-dashed"></div>

            <?php if($top_email_links) { ?>
                <label>Link più cliccati</label>
                <table class="table small table-striped" width="100%" style="margin-bottom: 15px;">
                    <tbody>
                    <?php foreach($top_email_links as $link => $click) { ?>
                        <tr>
                            <td><u><?= $link; ?></u></td>
                            <td class="text-right"><b><?= $click; ?></b></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } ?>

            <div class="email_stats_tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a class="nav-link" data-toggle="tab" href="#email_<?= $i; ?>_destinatari">Destinatari</a></li>
                    <li><a class="nav-link" data-toggle="tab" href="#email_<?= $i; ?>_anteprima">Anteprima</a></li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" id="email_<?= $i; ?>_destinatari" class="tab-pane active">
                        <table id="email_<?= $email_detail['id']; ?>_recipients_list" class="display table table-striped table-hover email_recipients_table" data-id="<?= $email_detail['id']; ?>" width="100%" data-readonly="true" cellspacing="0">
                            <thead>
                            <tr>
                                <th style="width: 300px;">Destinatario</th>
                                <th class="is_data default-sort" data-sort="DESC">Data invio</th>
                                <th class="is_data">Data apertura</th>
                                <th class="">Click</th>
                                <th>Stato</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                    <div role="tabpanel" id="email_<?= $i; ?>_anteprima" class="tab-pane">
                        <h2 class="title-divider" style="margin-top: -16px;">Contenuto</h2>
                        <div style="margin-bottom: 15px;">
                            <?= $email_detail['custom_layout_html']; ?>
                        </div>

                        <div style="margin-bottom: 15px;">
                            <h2 class="title-divider">Allegati</h2>

                            <?php if($email_detail['attachments'] && json_decode($email_detail['attachments'])) { ?>
                                <?php foreach(json_decode($email_detail['attachments'], true) as $file) { ?>
                                    <div class="file-box">
                                        <div class="file">
                                            <a download href="<?= UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN_HTML . $r['id'] . '/' . $file; ?>">
                                                <span class="corner"></span>
                                                <?php if(@is_array(getimagesize(UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN . $r['id'] . '/' . $file))) { ?>
                                                    <div class="image">
                                                        <img alt="image" class="img-responsive" src="<?= UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN_HTML . $r['id'] . '/' . $file; ?>">
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="icon">
                                                        <i class="fa fa-file"></i>
                                                    </div>
                                                <?php } ?>
                                                <div class="file-name">
                                                    <?= $file; ?>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div style="clear: both;"></div>
                            <?php } else { ?>
                                <i>Nessun allegato</i>
                            <?php } ?>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
<?php } ?>
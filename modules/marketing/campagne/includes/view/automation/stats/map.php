<h1 class="title-divider">Mappa</h1>
<?php
$MSFrameworkCustomers = new \MSFramework\customers();
$MSFrameworkNewsletterCampaigns = new \MSFramework\Newsletter\campaigns();

$ary_map_data = array();

$customer_status = array(
    0 => array(
        'label' => 'Terminata',
        'class' => 'danger'
    ),
    1 => array(
        'label' => 'In corso',
        'class' => 'primary'
    ),
    2 => array(
        'label' => 'Uscita forzata (loop)',
        'class' => 'warning'
    ),
);

foreach($MSFrameworkNewsletterCampaigns->getRecipientsDataForCampaign($r['id']) as $data) {

    if ($data['latitude'] == "" || $data['longitude'] == "") {
        continue;
    }

    $user_data = $MSFrameworkCustomers->getCustomerDataFromDB($data['id_recipient']);

    if(!$user_data) {
        return '<span class="text-muted">Utente eliminato</span>';
    }

    $info = array();
    if (!empty($user_data['email'])) {
        $info[] = $user_data['email'];
    }
    if (!empty($user_data['telefono_casa'])) {
        $info[] = $user_data['telefono_casa'];
    }
    if (!empty($user_data['telefono_cellulare'])) {
        $info[] = $user_data['telefono_cellulare'];
    }

    $user_info_html = '';

    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
    if(json_decode($user_data['avatar'])) {
        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($user_data['avatar'], true)[0];
    }

    $user_info_html .= '<div style="position: relative; padding: 4px; padding-left: 65px; height: 60px; margin-bottom: 15px; border-bottom: 1px dashed #ececec;">';
    $user_info_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
    $user_info_html .= '<h2 class="no-margins"><a href="#" class="showRecipientInfo" title="Percorso automazione di ' . $user_data['nome'] . ($user_data['cognome'] ? ' ' . $user_data['cognome'] : '') . '" data-id="' . $user_data['id'] . '" data-name="' . $user_data['nome'] . ($user_data['cognome'] ? ' ' . $user_data['cognome'] : '') . '">' . $r['nome'] . ' ' . $r['cognome'] . '</a></h2><small>' . implode(' - ', $info) . '</small>';
    $user_info_html .= '</div>';

    ob_start();
    ?>

    <?= $user_info_html; ?>

    <div style="width: 100%; overflow: hidden;">

        <div class="row" style="margin-bottom: 15px;">
            <div class="col-sm-6">
                <label>Data inizio</label>
                <p><?= date("d/m/Y H:i:s", strtotime($data['start_date'])); ?></p>
            </div>

            <div class="col-sm-6">
                <label>Stato automazione</label>
                <p><span class="label label-<?= $customer_status[$data['status']]['class']; ?>"><?= $customer_status[$data['status']]['label']; ?></span></p>
            </div>
        </div>

        <div class="row" style="margin-bottom: 15px;">
            <div class="col-sm-6">
                <label>Email</label>
                <p><?= $user_data['email']; ?></p>
            </div>

            <div class="col-sm-6">
                <label>IP</label>
                <p><?= $data['ip_receiver']; ?></p>
            </div>
        </div>

        <div class="row" style="margin-bottom: 15px;">
            <div class="col-sm-6">
                <label>Comune</label>
                <p><?= $data['city']; ?></p>
            </div>

            <div class="col-sm-6">
                <label>Regione</label>
                <p><?= $data['region']; ?></p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <label>User Agent</label>
                <code><?= $data['user_agent']; ?></code>
            </div>
        </div>
    </div>

    <?php
    $info_window = ob_get_clean();

    $ary_map_data[] = array(
        "lat" => $data['latitude'],
        "lng" => $data['longitude'],
        "title" => $data['ip_receiver'],
        "info_window" => $info_window,
    );
}
?>

<?php if(count($ary_map_data) == 0) { ?>
    <div class="alert alert-info" style="margin: 0;">
        Non ci sono ancora dati da mostrare sulla mappa (al momento non abbiamo dati sui destinatari).
    </div>
<?php } else { ?>
    <textarea id="map_data" style="display: none;"><?php echo json_encode($ary_map_data) ?></textarea>
    <div id="map" style="height: 600px;"></div>
<?php } ?>
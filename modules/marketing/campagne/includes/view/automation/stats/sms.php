<?php
$i = 0;
foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `newsletter__sms` WHERE campaign_id = :campaign_id", array(':campaign_id' => $r['id'])) as $sms_detail) {
    $i++;
    ?>

    <div class="ibox <?= ($i > 1 ? 'collapsed' : ''); ?>">
        <div class="ibox-title">
            <i class="ibox-icon fa fa-envelope fa-3x"></i>
            <h5><?= $sms_detail['name']; ?></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="fullscreen-link">
                    <i class="fa fa-expand"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="sms_stats_tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a class="nav-link" data-toggle="tab" href="#sms_<?= $i; ?>_destinatari">Destinatari</a></li>
                    <li><a class="nav-link" data-toggle="tab" href="#sms_<?= $i; ?>_anteprima">Anteprima</a></li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" id="sms_<?= $i; ?>_destinatari" class="tab-pane active">
                        <table id="sms_<?= $sms_detail['id']; ?>_recipients_list" class="display table table-striped table-hover sms_recipients_table" data-id="<?= $sms_detail['id']; ?>" width="100%" data-readonly="true" cellspacing="0">
                            <thead>
                            <tr>
                                <th style="width: 300px;">Destinatario</th>
                                <th class="is_data default-sort" data-sort="DESC">Data invio</th>
                                <th>Stato</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                    <div role="tabpanel" id="sms_<?= $i; ?>_anteprima" class="tab-pane">
                        <div style="padding: 15px 0 0;">
                            <?= $sms_detail['message']; ?>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
<?php } ?>
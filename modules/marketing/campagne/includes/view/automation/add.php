<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../../sw-config.php');
require('../../../config/moduleConfig.php');

$type = $_POST['type'];
$c = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__campagne WHERE id = :id", array(':id' => $_POST['campaign_id']), true);
$data = (isset($_POST['data']) && is_array($_POST['data']) ? $_POST['data'] : array());

if(in_array($type, array('email', 'sms', 'whatsapp', 'condition', 'wait', 'wait_jump', 'start_automation')) && file_exists('edit/' . $type . '.php')) {
    include('edit/' . $type . '.php');
} else {
    die('not_found');
}
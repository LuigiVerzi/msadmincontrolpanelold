<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */
?>
<h1 class="title-with-button">
    <?= ($data ? "Modifica SMS" : "Crea SMS"); ?>
    <a class="btn btn-primary" id="saveAutomation">Salva</a>
    <a class="btn btn-default" id="cancelAutomation">Annulla</a>
</h1>

<div id="automationSteps">

    <h2>Impostazioni SMS</h2>
    <fieldset>
        <div id="scheduler_container">

            <div class="row">
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Titolo</label>
                            <input id="sms_name" class="form-control required" placeholder="Il titolo del messaggio" value="<?php echo ($data['name'] != "" ? htmlentities($data['name']) : "") ?>" type="text" required>
                        </div>
                        <div class="col-sm-12">
                            <label>Nome Mittente</label>
                            <input id="sms_sender" class="form-control required" placeholder="Il nome del mittente" value="<?php echo ($data['sender'] != "" ? htmlentities($data['sender']) : "") ?>" type="text" required>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <label>Messaggio</label>
                    <textarea id="sms_message" class="form-control required" style="height: 115px;" placeholder="Inserisci un messaggio lungo massimo 1530 caratteri"><?php echo ($data['message'] != "" ? htmlentities($data['message']) : "") ?></textarea>
                </div>
            </div>
        </div>
    </fieldset>
</div>
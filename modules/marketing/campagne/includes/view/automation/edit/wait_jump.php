<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */
?>
<h1 class="title-with-button">
    Ripetizione
    <a class="btn btn-primary" id="saveAutomation">Salva</a>
    <a class="btn btn-default" id="cancelAutomation">Annulla</a>
</h1>

<div id="automationSteps">

    <h2>Impostazioni ripetizione</h2>
    <fieldset>
        <div id="jump_scheduler_container">

            <input id="wait_jump_target" value="<?php echo $data['target'] ?>" type="hidden">

            <div class="row">
                <div class="col-sm-2 wait_jump_type_time">
                    <label>Ripeti ogni</label>
                    <input id="wait_jump_value" class="form-control required" value="<?php echo ($data['value'] != "" ? $data['value'] : "1") ?>" type="number" min="1" required>
                </div>

                <div class="col-sm-2 wait_jump_type_time">
                    <label> </label>
                    <select id="wait_jump_what" class="form-control required" required>
                        <option value="hours" <?= ($data['what'] == "hours" ? "selected" : "") ?>>Ore</option>
                        <option value="days" <?= ($data['what'] == "days" ? "selected" : "") ?>>Giorni</option>
                        <option value="weeks" <?= ($data['what'] == "weeks" ? "selected" : "") ?>>Settimane</option>
                        <option value="months" <?= ($data['what'] == "months" ? "selected" : "") ?>>Mesi</option>
                        <option value="years" <?= ($data['what'] == "years" ? "selected" : "") ?>>Anni</option>
                    </select>
                </div>
            </div>
    </fieldset>
</div>
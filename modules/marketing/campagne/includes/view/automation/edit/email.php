<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

$shortcodes_globali = (new \MSFramework\emails())->getCommonShortcodes();

?>
<h1 class="title-with-button">
    <?= ($data ? "Modifica email" : "Crea email"); ?>
    <a class="btn btn-primary" id="saveAutomation" style="display: <?= ($data ? "block" : "none"); ?>;">Avanti</a>
    <a class="btn btn-default" id="cancelAutomation">Annulla</a>
</h1>

<div id="automationSteps">

    <?php if(!$data) { ?>
    <h2>Seleziona template</h2>
    <fieldset>

        <h2 style="margin: 15px 0 15px;">Seleziona un template di partenza</h2>

        <div id="email_choose_template">
            <?php foreach((new \MSFramework\Newsletter\emails())->getTemplateDetails('', 'id, titolo,data_creazione') as $template) { ?>
                <div class="file-box">
                    <div class="file">
                        <a href="#" class="template_choose" data-id="<?= $template['id']; ?>">
                            <span class="corner"></span>
                            <div class="image">
                                <img alt="image" class="img-responsive" src="<?= UPLOAD_NEWSLETTER_TEMPLATES_FOR_DOMAIN_HTML; ?>thumb_<?= $template['id']; ?>.png">
                            </div>
                            <div class="file-name">
                                <?= $template['titolo'] ?>
                                <br>
                                <small>Creato: <?= (new \MSFramework\utils())->smartdate(strtotime($template['data_creazione'])); ?></small>
                            </div>
                        </a>
                    </div>
                </div>
            <?php } ?>

            <div class="file-box">
                <div class="file">
                    <a href="#" class="template_choose" data-id="0">
                        <span class="corner"></span>
                        <div class="icon">
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="file-name">
                            Crea nuova email
                            <br>
                            <small>Parti da un template vuoto</small>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </fieldset>
    <?php } ?>

    <h2>Contenuto email</h2>
    <fieldset>

        <div class="row">

            <div class="col-sm-6">
                <label>Oggetto della mail</label>
                <input id="nome" name="nome" class="form-control required" value="<?php echo $data['nome'] ?>" type="text" required>
            </div>

            <div class="col-sm-3">
                <label>Mittente</label>
                <select id="mittente" class="form-control">
                    <option value="">Usa mittente predefinito</option>
                    <?php foreach((new \MSFramework\Newsletter\emails())->getMittenteDetails('', 'id, nome, email') as $mittente) { ?>
                        <option value="<?= $mittente['id'] ?>" <?php if($data['mittente'] == $mittente['id']) { echo "selected"; } ?>><?= $mittente['nome'] . " (" . $mittente['email'] . ")" ?></option>
                        <?php } ?>
                </select>
            </div>

            <div class="col-sm-3">
                <label>Server SMTP</label>
                <select id="smtp" class="form-control">
                    <option value="">Usa server di sistema</option>
                    <?php foreach((new \MSFramework\emails())->getSMTPDetails('') as $smtp) { ?>
                        <option value="<?= $smtp['id'] ?>" <?php if($data['smtp'] == $smtp['id']) { echo "selected"; } ?>><?= $smtp['data']['name'] . " (" . $smtp['data']['username'] . ")" ?></option>
                        <?php } ?>
                </select>
            </div>

        </div>

        <div class="row custom_layout_container">

            <div class="col-sm-12 custom_layout_container_editor">
                <h2 class="title-with-button">
                    Template

                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-sm btn-info dropdown-toggle">Importa Template <i class="fa fa-angle-down"></i></button>
                        <ul class="dropdown-menu">
                            <?php foreach((new \MSFramework\Newsletter\emails())->getTemplateDetails('', 'id, titolo') as $template) { ?>
                                <li><a href="#" data-id="<?= $template['id'] ?>" class="importEmailtemplate"><?= $template['titolo'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </h2>

                <?= (new \MSFramework\utils())->generateShortcodesTable((new \MSFramework\Newsletter\emails())->getShortcodeList(true), true); ?>
                <div id="versione_html_personalizzata" name="versione_html_personalizzata"><?php echo $data['custom_layout_html'] ?></div>

            </div>
        </div>
    </fieldset>

    <h2>Allegati</h2>
    <fieldset>
        <div id="attachment_container">
            <?php
            $uploader = new \MSFramework\uploads('MARKETING_AUTOMATIONS');

            $uploader->path_html .= (int)$_POST['campaign_id'] . '/';
            $uploader->path .= (int)$_POST['campaign_id'] . '/';

            $uploader->initUploaderHTML("emailImages", $data['attachments']);
            ?>
        </div>
    </fieldset>

</div>

<input type="hidden" id="newsletter_shortcodes" value="<?= htmlentities(json_encode(array(
    'custom' => (new \MSFramework\Newsletter\emails())->getShortcodeList(),
    'global' => $shortcodes_globali[0]
))); ?>">
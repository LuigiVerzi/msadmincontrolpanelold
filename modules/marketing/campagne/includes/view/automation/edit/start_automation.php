<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */
?>

<h1 class="title-with-button">
    Origine dell'automazione
    <a class="btn btn-primary" id="saveAutomation">Salva</a>
    <a class="btn btn-default" id="cancelAutomation">Annulla</a>
    <span class="title-descr">
            Da quali liste entreranno all'interno dell'automazione i contatti?
        </span>
</h1>

<div id="automationSteps">

    <h2>Seleziona origine</h2>
    <fieldset>

        <div class="row">
            <div class="col-lg-12">
                <label>Comportamento</label>
                <select class="form-control" id="origin_behavior">
                    <option value="only_presents" <?= ($data['behavior'] == 'only_presents' ? 'selected' : ''); ?>>
                        Solo i contatti già iscritti
                    </option>
                    <option value="only_future" <?= ($data['behavior'] == 'only_future' ? 'selected' : ''); ?>>
                        Solo i futuri iscritti
                    </option>
                    <option value="both" <?= ($data['behavior'] == 'both' ? 'selected' : ''); ?>>
                        Sia i contatti attuali che i futuri
                    </option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h2 class="title-with-button">
                    Liste
                    <a href="#" class="btn btn-sm btn-success" onclick="showFastEditor('marketing_liste', '', onListCreated);">Crea nuova lista</a>
                </h2>
                <div class="row" id="originLists">
                    <?php include('tagList.php'); ?>
                </div>
            </div>
        </div>

    </fieldset>
</div>
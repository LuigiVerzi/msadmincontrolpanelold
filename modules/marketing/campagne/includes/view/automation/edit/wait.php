<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */
?>
<h1 class="title-with-button">
    <?= ($data ? "Modifica attesa" : "Crea attesa"); ?>
    <a class="btn btn-primary" id="saveAutomation">Salva</a>
    <a class="btn btn-default" id="cancelAutomation">Annulla</a>
</h1>

<div id="automationSteps">

    <h2>Impostazioni attesa</h2>
    <fieldset>
        <div id="scheduler_container">

            <div class="row">

                <div class="col-sm-3">
                    <label>Cosa vuoi attendere?</label>
                    <select id="wait_type" class="form-control required" required>
                        <option value="time" <?= ($data['type'] == "time" ? "selected" : "") ?>>Un periodo di tempo</option>
                        <option value="date" <?= ($data['type'] == "date" ? "selected" : "") ?>>Una data specifica</option>
                    </select>
                </div>

                <div class="col-sm-2 wait_type_time" style="display: <?= (!$data['type'] || $data['type'] == "time" ? "block" : "none") ?>;">
                    <label>Attendi</label>
                    <input id="wait_value" class="form-control required" value="<?php echo ($data['value'] != "" ? $data['value'] : "1") ?>" type="number" min="1" required>
                </div>

                <div class="col-sm-2 wait_type_time" style="display: <?= (!$data['type'] || $data['type'] == "time" ? "block" : "none") ?>;">
                    <label> </label>
                    <select id="wait_what" class="form-control required" required>
                        <option value="minutes" <?= ($data['what'] == "minutes" ? "selected" : "") ?>>Minuti</option>
                        <option value="hours" <?= ($data['what'] == "hours" ? "selected" : "") ?>>Ore</option>
                        <option value="days" <?= ($data['what'] == "days" ? "selected" : "") ?>>Giorni</option>
                        <option value="weeks" <?= ($data['what'] == "weeks" ? "selected" : "") ?>>Settimane</option>
                        <option value="months" <?= ($data['what'] == "months" ? "selected" : "") ?>>Mesi</option>
                        <option value="years" <?= ($data['what'] == "years" ? "selected" : "") ?>>Anni</option>
                    </select>
                </div>

                <div class="col-sm-2 wait_type_date" style="display: <?= ($data['type'] == "date" ? "block" : "none") ?>;">
                    <label>Inserisci data</label>
                    <input id="wait_date" class="form-control required" value="<?php echo ($data['date'] != "" ? date('d/m/Y H:i', $data['date']) : "") ?>" type="text" required>
                </div>

            </div>
        </div>
    </fieldset>
</div>
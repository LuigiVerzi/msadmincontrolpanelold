<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

if(isset($data)) {
    $origin = $data;
    $origin_tags = ($origin['tags'] ? $origin['tags'] : array());
} else {
    require_once('../../../../../../sw-config.php');
    require('../../../config/moduleConfig.php');

    $origin_tags = $_POST['selected'];
}


$avaialable_lists = (new \MSFramework\Newsletter\lists())->getTagDetails('', 'id, nome');
foreach ($avaialable_lists as $list_k => $list_v) {
    $avaialable_lists[$list_k]['count'] = (new \MSFramework\Newsletter\lists())->getSubscribersCountForTag($list_v['id']);
}

?>

<?php foreach($avaialable_lists as $tag) { ?>
    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 originListCheck">
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                    <input type="checkbox" class="tags" id="tag_<?= $tag['id'] ?>" data-tag-id="<?= $tag['id'] ?>" data-tag-count="<?= $tag['count']; ?>" <?php echo (in_array($tag['id'], $origin_tags)) ? "checked" : "" ?> />
                    <i></i>
                </div>
                <span style="font-weight: normal;"><a href="#" class="fastTagEditor" onclick="showFastEditor('marketing_liste', <?= $tag['id']; ?>, onListCreated);"><i class="fa fa-edit"></i></a> <?= $tag['nome'] ?> (<?= $tag['count'] ?>)</span>
            </label>
        </div>
    </div>
<?php } ?>
<h1>Generali</h1>
<fieldset>
<div id="automationsList">

    <div id="automationContainer">
        <div id="automation_settings_container" style="display: none;">

        </div>

        <textarea id="automationOrigin" style="display: none;"><?= json_encode($origin); ?></textarea>

        <div class="hideOnEdit">
            <div id="automationsContent" class="contentList standardList">
                <?php include('includes/view/standard/list.php'); ?>
            </div>
        </div>

    </div>
</div>
</fieldset>
<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

if(isset($r)) {
    $campaign_id = $r['id'];
    $campagna = $r;
    $diagramma = ($campagna['diagramma'] && json_decode($campagna['diagramma']) ? json_decode($campagna['diagramma'], true) : array());
} else {
    require_once('../../../../../../sw-config.php');
    require('../../../config/moduleConfig.php');

    $campaign_id = $_POST['c'];
    $campagna = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__campagne WHERE id = :campaign_id", array(':campaign_id' => $campaign_id), true);
    $diagramma = $_POST['diagramma'];
    $origin = $_POST['origin'];
}

if(!$diagramma) {
    $unique_id = uniqid();
    $diagramma = array (
        $unique_id . '_wait' => array (
            'id' => $unique_id . '_wait',
            'type' => 'wait',
            'parent' => '0',
            'data' => array (),
        ),
        $unique_id . '_email' => array (
            'id' => $unique_id . '_email',
            'type' => 'email',
            'parent' => $unique_id . '_wait',
            'data' => array()
        ),
        $unique_id . '_sms' => array (
            'id' => $unique_id . '_sms',
            'type' => 'sms',
            'parent' => $unique_id . '_email',
            'data' => array()
        ),
        $unique_id . '_repeat' => array (
            'id' => $unique_id . '_repeat',
            'type' => 'wait_jump',
            'parent' =>  $unique_id . '_sms',
            'data' => array (
                'target' => $unique_id . '_wait'
            ),
        ),
    );
}

$destinatari_info = $origin;
$wait_info = array();
$email_info = array();
$repeat_info = array();

foreach($diagramma as $single_item) {
    if($single_item['type'] === 'wait') {
        $wait_info = $single_item;
    } if($single_item['type'] === 'wait_jump') {
        $repeat_info = $single_item;
    } else if($single_item['type'] === 'email') {
        $email_info = $single_item;
    } else if($single_item['type'] === 'sms') {
        $sms_info = $single_item;
    }
}

$time_what_array = array(
    'minutes' => array('minut', 'i', 'o'),
    'hours' => array('or', 'e', 'a'),
    'days' => array('giorn', 'i', 'o'),
    'weeks' => array('settiman', 'e', 'a'),
    'months' => array('mes', 'i', 'e'),
    'years' => array('ann', 'i', 'o'),
);

?>

<h2 class="title-divider" style="margin-top: 0;">A chi?</h2>

<a href="javascript:;" class="btn btn-default" onclick="editAutomation('0','start_automation','');"><?= ($destinatari_info['tags'] ? 'Modifica destinatari' : 'Aggiungi destinatari'); ?></a>
<div style="margin-top: 15px;">
    <?php
    if($destinatari_info['tags']) {
        $behavior_label = 'ai contatti attualmente iscritti';
        if($destinatari_info['behavior'] === 'only_future') {
            $behavior_label = 'ai futuri contatti iscritti';
        } else if($destinatari_info['behavior'] === 'both') {
            $behavior_label = 'sia agli attuali che ai futuri contatti iscritti';
        }

        echo '<i>Invia <u>' . $behavior_label . '</u> nelle seguenti liste:</i>';

        $liste = array();
        foreach((new \MSFramework\Newsletter\lists())->getTagDetails($destinatari_info['tags']) as $list) {
            $liste[] = ' <i><u><b>' . $list['nome'] . '</b></u></i>';
        }

        echo implode(', ', $liste);

    } else {
        ?>
        <i>A chi desideri inviare questa campagna?</i>
    <?php } ?>
</div>

<h2 class="title-divider">Quando?</h2>

<a href="#" class="btn btn-default" onclick="editAutomation('<?= $wait_info['id']; ?>','wait','0');"><?= ($wait_info['data'] ? 'Modifica attesa' : 'Imposta attesa'); ?></a>
<?php if($wait_info['data']) { ?>
    <a href="javascript:;" class="btn btn-info btn-outline" onclick="resetWaitTime();">Nessuna attesa</a>
<?php } ?>

<div style="margin-top: 15px;">

    <?php
    $have_wait = false;
    if($wait_info['data']) {
        if ($wait_info['data']['type'] === 'time' && $wait_info['data']['value'] > 0) {
            echo '<i>Attesa di <u>' . (int)$wait_info['data']['value'] . ' ' . $time_what_array[$wait_info['data']['what']][0] . $time_what_array[$wait_info['data']['what']][($wait_info['data']['value'] == 1 ? 2 : 1)] . '</u> prima dell\'invio.</i>';
            $have_wait = true;
        } else if ($wait_info['data']['type'] === 'date') {
            $have_wait = true;
            echo '<p>Invia alle ore <b>' . date('H:i', $wait_info['data']['date']) . '</b> di giorno <b>' . date('d/m/Y', $wait_info['data']['date']) . '</b></p>';
        }
    }

    if(!$have_wait) {
        echo '<i>Invia <u>immediatamente</u>.</i>';
    }
    ?>

</div>

<div class="hr-line-dashed"></div>

<a href="#" class="btn btn-default" onclick="editAutomation('<?= $repeat_info['id']; ?>','wait_jump','<?= $email_info['id']; ?>');"><?= ($have_repeat ? 'Modifica ripetizione' : 'Imposta ripetizione'); ?></a>
<?php if($have_repeat) { ?>
    <a href="javascript:;" class="btn btn-info btn-outline" onclick="resetLoopTime();">Non ripetere</a>
<?php } ?>

<div style="margin-top: 15px;">
    <?php
    $have_repeat = false;
    if($repeat_info['data'] && $repeat_info['data']['value'] > 0) {
        echo '<i>Ripeti ogni <u>' . ((int)$repeat_info['data']['value'] > 1 ? (int)$repeat_info['data']['value'] . ' ' : '') . $time_what_array[$repeat_info['data']['what']][0] . $time_what_array[$repeat_info['data']['what']][($repeat_info['data']['value'] == 1 ? 2 : 1)] . '</u>.</i>';
        $have_repeat = true;
    }

    if(!$have_repeat) {
        echo '<i>Non ripetere invio.</i>';
    }
    ?>
</div>


<h2 class="title-divider">Cosa?</h2>
<div>
    <a href="javascript:;" style="margin-bottom: 15px;" class="btn btn-default" onclick="editAutomation('<?= $email_info['id']; ?>','email','<?= $wait_info['id']; ?>');"><?= ($email_info['data'] ? 'Modifica email' : 'Crea email'); ?></a>
    <div style="clear: both;"></div>
    <?php if($email_info['data'] && $email_info['data']['custom_layout_html']) { ?>

        <div style="margin-bottom: 15px;">
            <h3>Oggetto</h3>
            <i class="form-control"><?= $email_info['data']['nome']; ?></i>
        </div>

        <div style="margin-bottom: 15px;">
            <h3>Contenuto</h3>
            <div class="ibox-content" style="border: 1px solid #ececec;"><?= $email_info['data']['custom_layout_html']; ?></div>
        </div>

        <div style="margin-bottom: 15px;">
            <h3>Allegati</h3>

            <?php if($email_info['data']['attachments'] && json_decode($email_info['data']['attachments'])) { ?>
                <?php foreach(json_decode($email_info['data']['attachments'], true) as $file) { ?>
                    <div class="file-box">
                        <div class="file">
                            <a download href="<?= UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN_HTML . $campaign_id . '/' . $file; ?>">
                                <span class="corner"></span>
                                <?php if(@is_array(getimagesize(UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN . $campaign_id . '/' . $file))) { ?>
                                    <div class="image">
                                        <img alt="image" class="img-responsive" src="<?= UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN_HTML . $campaign_id . '/' . $file; ?>">
                                    </div>
                                <?php } else { ?>
                                    <div class="icon">
                                        <i class="fa fa-file"></i>
                                    </div>
                                <?php } ?>
                                <div class="file-name">
                                    <?= $file; ?>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
                <div style="clear: both;"></div>
            <?php } else { ?>
                <i>Nessun allegato</i>
            <?php } ?>
        </div>
    <?php } else { ?>
        <i>Disegna il contenuto della tua email.</i>
    <?php } ?>
</div>
<div class="hr-line-dashed"></div>
<div>
    <a href="javascript:;" style="margin-bottom: 15px;" class="btn btn-default" onclick="editAutomation('<?= $sms_info['id']; ?>','sms','<?= $email_info['id']; ?>');"><?= ($sms_info['data'] ? 'Modifica sms' : 'Crea sms'); ?></a>
    <div style="clear: both;"></div>
    <?php if($sms_info['data'] && $sms_info['data']['message']) { ?>

        <div style="margin-bottom: 15px;">
            <h3>Mittente</h3>
            <i class="form-control"><?= $sms_info['data']['sender']; ?></i>
        </div>

        <div style="margin-bottom: 15px;">
            <h3>Contenuto</h3>
            <div class="ibox-content" style="border: 1px solid #ececec;"><?= $sms_info['data']['message']; ?></div>
        </div>
    <?php } else { ?>
        <i>Scrivi il contenuto del tuo sms.</i>
    <?php } ?>
</div>

<textarea id="automationJSON" style="display: none;"><?= json_encode($diagramma, JSON_FORCE_OBJECT); ?></textarea>

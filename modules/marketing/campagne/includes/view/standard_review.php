<?php
$wait_info = array();
$email_info = array();
$repeat_info = array();

foreach(json_decode($r['diagramma'], true) as $single_item) {
    if($single_item['type'] === 'wait') {
        $wait_info = $single_item;
    } if($single_item['type'] === 'wait_jump') {
        $repeat_info = $single_item;
    } else if($single_item['type'] === 'email') {
        $email_info = $single_item;
    }
}
?>

<h1><i class="fa fa-dashboard"></i> Resoconto</h1>
<fieldset>
    <?php if($r['status'] !== 4) { ?>
        <!--
        <h1 class="title-with-button" style="margin-top: -15px;">
            Andamento in tempo reale
            <div class="text-right">
                <select type="text" id="last_actions_daterange" class="form-control">
                    <option value="5" data-interval="1">Ultimi 5 minuti</option>
                    <option value="10" data-interval="1">Ultimi 10 minuti</option>
                    <option value="30" data-step="1">Ultimi 30 minuti</option>
                    <option value="120" data-step="30">Ultime 2 ore</option>
                    <option value="300" data-step="60">Ultime 5 ore</option>
                    <option value="1440" data-step="60" selected>Ultime 24 ore</option>
                </select>
            </div>
            <span class="title-descr">
            Auto refresh tra <b id="autoRefreshTimer">10</b> secondi
        </span>
        </h1>
        <canvas id="lastActionsChart" height="350" width="100%" style="min-height: 350px; max-height: 350px; margin-top: 15px;"></canvas>
        -->
        <h1 style="margin: 26px 0;">
            Campagna <b><?= $r['nome']; ?></b>
            <small class="pull-right text-right" style="font-size: 16px;" title="<?= date('d/m/Y H:i', strtotime($r['data_creazione'])); ?>">
                <button type="button" class="btn btn-inverse btn-disabled m-l-sm pull-right" disabled="">
                    <i class="fa fa-hourglass-o"></i>
                </button>
                Campagna avviata <br><b><?= (new \MSFramework\utils())->smartdate(strtotime($r['data_creazione'])); ?></b>
            </small>
            <div style="clear: both;"></div>
        </h1>
    <?php } ?>

    <?php

    $time_what_array = array(
        'minutes' => array('minut', 'i', 'o'),
        'hours' => array('or', 'e', 'a'),
        'days' => array('giorn', 'i', 'o'),
        'weeks' => array('settiman', 'e', 'a'),
        'months' => array('mes', 'i', 'e'),
        'years' => array('ann', 'i', 'o'),
    );

    $wait_label = '';

    if ($wait_info['data']['type'] === 'time' && $wait_info['data']['value'] > 0) {
        $wait_label = 'dopo <b>' . (int)$wait_info['data']['value'] . ' ' . $time_what_array[$wait_info['data']['what']][0] . $time_what_array[$wait_info['data']['what']][($wait_info['data']['value'] == 1 ? 2 : 1)] . '</b> dall\'iscrizione';
    } else if ($wait_info['data']['type'] === 'date') {
        $wait_label = 'alle ore <b>' . date('H:i', $wait_info['data']['date']) . '</b> di giorno <b>' . date('d/m/Y', $wait_info['data']['date']) . '</b>';
    } else {
        $wait_label = '<b>Immediatamente</b>';
    }

    if($repeat_info['data'] && $repeat_info['data']['value'] > 0) {
        $wait_label .= ', per poi <i>reinviarla <u>ogni ' . ((int)$repeat_info['data']['value'] > 1 ? (int)$repeat_info['data']['value'] . ' ' : '') . $time_what_array[$repeat_info['data']['what']][0] . $time_what_array[$repeat_info['data']['what']][($repeat_info['data']['value'] == 1 ? 2 : 1)] . '</u></i>';
    }

    $behavior_label = 'Ai contatti attualmente iscritti';
    if($origin['behavior'] === 'only_future') {
        $behavior_label = 'Ai futuri contatti iscritti';
    } else if($origin['behavior'] === 'both') {
        $behavior_label = 'Sia agli attuali che ai futuri contatti iscritti';
    }

    echo '<p>Invia email ' . $wait_label .'.<br><u>' . $behavior_label . '</u> nelle seguenti liste:</p>';
    ?>

    <ul class="tag-list" style="padding: 0 !important;">
        <?php foreach((new \MSFramework\Newsletter\lists())->getTagDetails($origin['tags']) as $tag) { ?>
            <li><a href="../destinatari/tag/edit.php?id=<?= $tag['id']; ?>" target="_blank"><i class="fa fa-tag"></i> Lista <?= $tag['nome']; ?></a></li>
        <?php } ?>
    </ul>

    <div style="clear: both;"></div>

    <h1 class="title-divider">
        Contatti
    </h1>

    <table id="recipients_list" class="display table table-striped table-hover" width="100%" data-readonly="true" cellspacing="0">
        <thead>
        <tr>
            <th style="width: 300px;">Destinatario</th>
            <th class="is_data">Inizio Campagna</th>
            <th class="is_data default-sort" data-sort="DESC">Ultima azione</th>
            <th>Stato</th>
        </tr>
        </thead>
    </table>

    <?php if($MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__emails` WHERE campaign_id = :campaign_id", array(':campaign_id' => $r['id']))) { ?>
        <h1 class="title-divider">Email</h1>
        <?php include('includes/view/automation/stats/emails.php'); ?>
    <?php } ?>

    <?php if($MSFrameworkDatabase->getCount("SELECT * FROM `newsletter__sms` WHERE campaign_id = :campaign_id", array(':campaign_id' => $r['id']))) { ?>
        <h1 class="title-divider">SMS</h1>
        <?php include('includes/view/automation/stats/sms.php'); ?>
    <?php } ?>

    <?php
    if(GMAPS_API_KEY != "") {
        include('includes/view/automation/stats/map.php');
    }
    ?>

</fieldset>
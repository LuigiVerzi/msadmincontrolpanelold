<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */ ?>

<?php
require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = array();
if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__campagne WHERE id = :id", array(':id' => $_GET['id']), true);
}

$origin = (json_decode($r['origin']) ? json_decode($r['origin'], true) : array());

if(!$r) {
    header('Location: index.php');
    die();
}

$campaignsStatus = (new \MSFramework\Newsletter\campaigns())->getCampaignsStatusLabel();

$campaignLabel = '<span class="badge badge-' . $campaignsStatus[$r['status']]['color'] . '"><i class="fa fa-' . $campaignsStatus[$r['status']]['icon'] . '"></i> ' . $campaignsStatus[$r['status']]['label'] . '</span>';

$module_config['name'] = '<a style="color: inherit; text-decoration: none;" href="#" id="campaignNameLabel">' . $campaignLabel . ' <span id="campaignName">' . $r['nome'] . '</span> <i style="font-size: 18px;" class="fa fa-pencil-square-o"></i></a>';
$module_config['name'] .= '<div href="#" id="campaignNameEdit" style="display: none;"><input type="text" class="form-control" id="campaignNameValue" value="' . $r['nome'] . '"> <a href="#" id="updateCampaignName" class="btn btn-success btn-small" style="margin-left: 10px;">Aggiorna</a></div>';
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php
                $customToolbarButton = '';
                foreach($campaignsStatus[$r['status']]['markable_to'] as $availBtn) {
                    $customToolbarButton .=  '<li><a class="dropdown-item campaignSetStatus btn btn-outline btn-sm btn-' . $campaignsStatus[$availBtn]['color'] . '" data-status="' . $availBtn . '" id="campaignSetButton_' . $availBtn . '"><i class="fa fa-' . $campaignsStatus[$availBtn]['icon'] . '"></i> ' . $campaignsStatus[$availBtn]['actionText'] . '</a></li>';
                }

                // btnChildrens += '<li><a href="javascript:;" title="' + extraChildBtn.label + '" data-label="' + extraChildBtn.label + '"  data-fn="' + extraChildBtn.function + '"  data-icon="' + extraChildBtn.icon + '" data-context="' + (typeof (extraChildBtn.context) !== 'undefined' && extraChildBtn.context ? '1' : '0') + '" class="btn btn-' + extraChildBtn.btn + (typeof (extraChildBtn.full) == 'undefined' ? ' btn-outline' : '') + ' btn-sm ' + extraChildBtn.class + '" onclick="' + extraChildBtn.function + '(\'' + row.DT_RowId + '\');"><i class="fa fa-' + extraChildBtn.icon + '"></i> ' + extraChildBtn.label + '</a></li>';

                if($customToolbarButton !== "") {
                    $customToolbarButton = '<div class="btn-group campaignBtnGroup">
                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">Aggiorna stato <i class="fa fa-angle-down"></i></button>
                        <ul class="dropdown-menu">
                            ' . $customToolbarButton . '
                        </ul>
                    </div>';
                }

                if($r['status'] == '0') {
                    require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php");
                } else {
                    require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php");
                }
                ?>
            </div>
        </div>

        <div class="wrapper wrapper-content <?= ($r['status'] == 0 ? 'full' : ''); ?>">
            <input type="hidden" id="campaign_id" value="<?php echo $r['id'] ?>" />
            <input type="hidden" id="campaign_type" value="<?php echo $r['type'] ?>" />
            <input type="hidden" id="campaign_status" value="<?php echo $r['status'] ?>" />
            <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                    <?php
                    if($r['status'] == 0) {
                        include('includes/view/' . $r['type'] . '_draft.php');
                    } else {
                        include('includes/view/' . $r['type'] . '_review.php');
                    }
                    ?>

            </form>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    window.campaignStatusLabels = <?= json_encode((new \MSFramework\Newsletter\campaigns())->getCampaignsStatusLabel()); ?>;
    globalInitForm();
    initCampaignView(<?= ($r['status'] == 0 ? '1' : '0'); ?>);
</script>
</body>
</html>

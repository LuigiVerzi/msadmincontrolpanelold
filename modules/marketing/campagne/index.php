<?php
/**
 * MSAdminControlPanel
 * Date: 15/09/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <div class="title-action">
                    <a class="btn btn-primary" id="newCampaignBtn">Crea nuova campagna</a>
                </div>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">

                <div class="col-lg-3 col-md-4">
                    <div class="ibox" style="background: white;">
                        <div class="ibox-content mailbox-content">
                            <div class="file-manager" id="newsletterCampaignFilters">
                                <h2 class="title-divider" style="margin-top: -16px; margin-bottom: 10px;">Filtra tramite Stato</h2>

                                <a class="btn btn-block btn-default" style="margin-bottom: 10px; display: none;" id="cancelCampaignFilters" href="#">Visualizza tutto</a>

                                <ul class="folder-list" style="margin: -5px 0; padding: 0;">
                                    <?php foreach((new \MSFramework\Newsletter\campaigns())->getCampaignsStatusLabel() as $key => $label) { ?>
                                    <li><a href="" data-status="<?= $key; ?>" class="filterCampaignStatus"><i class="fa fa-<?= $label['icon']; ?>"></i> <?= $label['label']; ?></a></li>
                                    <?php } ?>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9 col-md-8">
                    <table id="campaigns_list" class="display table table-striped table-hover" width="100%" data-deleteonly="true" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Campagna</th>
                            <th class="is_data">Data Creazione</th>
                            <th>Statistiche</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    </div>
</div>

<div class="modal inmodal" id="createCampaignModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-bullhorn modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Crea nuova campagna</h4>
                <div class="text-muted">Come prima cosa inserisci un nome che possa aiutarti a ricordare l'obiettivo dell'campagna.</div>
            </div>
            <div class="modal-body" style="padding-bottom: 20px;">
                <input id="newCampaignName" type="email" placeholder="Nome campagna" class="form-control" style="margin-bottom: 15px;">
                <select id="newCampaignType" class="form-control">
                    <?php foreach((new \MSFramework\Newsletter\campaigns())->getCampaignsTypes() as $type_id => $type_values) { ?>
                        <option value="<?= $type_id; ?>"><?= $type_values['label']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary" id="createNewCampaign">Crea campagna</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="duplicateCampaignModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-table modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Duplica campagna</h4>
                <div class="text-muted">Inserisci il nome della nuova campagna.</div>
            </div>
            <div class="modal-body" style="padding-bottom: 20px;">
                <input id="duplicateCampaignName" type="email" placeholder="Nome campagna" class="form-control">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary" id="duplicateNewCampaign">Duplica campagna</button>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    window.campaignStatusLabels = <?= json_encode((new \MSFramework\Newsletter\campaigns())->getCampaignsStatusLabel()); ?>;
    initCampaignsList();
</script>
</html>
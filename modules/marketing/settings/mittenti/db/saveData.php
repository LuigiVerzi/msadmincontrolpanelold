<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pEmail'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$mail_conflict = false;
if($MSFrameworkDatabase->getCount("SELECT email FROM newsletter__mittenti WHERE email = :mail AND id != :id", array(":mail" => $_POST['pEmail'], ":id" => $_POST['pID'])) != 0) {
    $mail_conflict = true;
}

if($_POST['pEmail'] != "") {
    if (!filter_var($_POST['pEmail'], FILTER_VALIDATE_EMAIL)) {
        $mail_conflict = true;
    }
}

if($_POST['pReplyTo'] != "") {
    if (!filter_var($_POST['pReplyTo'], FILTER_VALIDATE_EMAIL)) {
        $mail_conflict = true;
    }
}

if($mail_conflict) {
    echo json_encode(array("status" => "mail_not_valid"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "email" => $_POST['pEmail'],
    "reply_to" => $_POST['pReplyTo'],
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO newsletter__mittenti ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE newsletter__mittenti SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
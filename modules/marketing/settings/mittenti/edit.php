<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */ ?>

<?php
require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__mittenti WHERE id = :id", array(':id' => $_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Nome</label>
                                    <input id="nome" name="nome" class="form-control required" value="<?php echo $r['nome'] ?>" type="text" required>
                                </div>

                                <div class="col-sm-4">
                                    <label>Email</label>
                                    <input id="email" name="email" class="form-control required" value="<?php echo $r['email'] ?>" type="email" required>
                                </div>

                                <div class="col-sm-4">
                                    <label>Reply To</label>
                                    <input id="reply_to" name="reply_to" class="form-control" value="<?php echo $r['reply_to'] ?>" type="email">
                                </div>
                            </div>
                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
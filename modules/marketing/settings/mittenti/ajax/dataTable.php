<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');


$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array( 'db' => 'id',  'dt' => 0 ),
    array( 'db' => 'nome',  'dt' => 1 ),
    array( 'db' => 'email',   'dt' => 2 ),
);

echo json_encode(
    $datatableHelper::simple( $_GET, $MSFrameworkDatabase, 'newsletter__mittenti', 'id', $columns )
);
<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */ ?>

<?php
require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('newsletter_settings');

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Monitoraggio</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="activate_monitoring" <?php if($r['activate_monitoring'] == "1" || $_GET['id'] == "") { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita monitoraggio</span>
                                        </label>
                                    </div>
                                    <small>Verrà aggiunto un tag img invisibile per monitorare il destinatario</small>
                                </div>

                                <div class="col-sm-4">
                                    <label>Cancellazione Automatica</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="automatic_delete" <?php if($r['automatic_delete'] == "1") { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilta cancellazione automatica</span>
                                        </label>
                                    </div>
                                    <small>Il cliente verrà cancellato automaticamente alla disiscrizione</small>
                                </div>

                                <div class="col-sm-4">
                                    <label>Double Opt-In Newsletter</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="disable_newsletter__doi" <?php if($r['disable_newsletter__doi'] == 1) { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Disabilita Double Opt-In Newsletter</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
function initForm() {
    initClassicTabsEditForm();
}

function moduleSaveFunction() {
    
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pActivateMonitoring": $('#activate_monitoring:checked').length,
            "pAutomaticDelete": $('#automatic_delete:checked').length,
            "pDisableNewsletterDOI": $('#disable_newsletter__doi:checked').length,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
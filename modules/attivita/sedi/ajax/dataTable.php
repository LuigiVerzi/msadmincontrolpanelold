<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM negozio_sedi") as $r) {
    $images = json_decode($r['gallery'], true);
    $array['data'][] = array(
        '<img src="' . UPLOAD_NEGOZIO_SEDI_FOR_DOMAIN_HTML . "tn/" . $images[0] . '" alt="" height="auto" width="80">',
        $MSFrameworki18n->getFieldValue($r['nome']),
        json_decode($r['geo_data'], true)['indirizzo'],
        $r['email'],
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

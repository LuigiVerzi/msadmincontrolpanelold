<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\Attivita\sedi())->getSedeDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['gallery'], true));
}

if($MSFrameworkDatabase->deleteRow("negozio_sedi", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('NEGOZIO_SEDI'))->unlink($images_to_delete);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pSlug'] == "" || $_POST['pNome'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

(new MSFramework\geonames())->checkDataBeforeSave();

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "negozio_sedi", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

$uploader = new \MSFramework\uploads('NEGOZIO_SEDI');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT gallery, nome, long_descr, short_descr, slug FROM negozio_sedi WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pLongDescr'], 'oldValue' => $r_old_data['long_descr']),
        array('currentValue' => $_POST['pShortDescr'], 'oldValue' => $r_old_data['short_descr']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "long_descr" => $MSFrameworki18n->setFieldValue($r_old_data['long_descr'], $_POST['pLongDescr']),
    "short_descr" => $MSFrameworki18n->setFieldValue($r_old_data['short_descr'], $_POST['pShortDescr']),
    "geo_data" => json_encode($_POST['pGeoData']),
    "email" => $_POST['pEmail'],
    "telefono" => $_POST['pTelefono'],
    "gallery" => json_encode($ary_files),
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO negozio_sedi ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE negozio_sedi SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        if($db_action == "insert") {
            $uploader->unlink($ary_files);
        }
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['gallery'], true));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
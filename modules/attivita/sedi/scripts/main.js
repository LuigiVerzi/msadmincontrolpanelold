$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initOrak('images', 5, 100, 100, 150, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
    manageWorldDataSelects();
    initSlugChecker('nome', 'slug', 'negozio_sedi', $('#record_id'));
    initTinyMCE( '#short_descr, #long_descr');
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pEmail": $('#email').val(),
            "pTelefono": $('#telefono').val(),
            "pWebsite": $('#website').val(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pGeoData": getGeoDataAryToSave(),
            "pLongDescr": tinymce.get('long_descr').getContent(),
            "pShortDescr": tinymce.get('short_descr').getContent(),
            "pSlug": $('#slug').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            geoDataErrorsOnSave(data.status);

            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
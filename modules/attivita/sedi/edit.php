<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM negozio_sedi WHERE id = :id", array(":id" => $_GET['id']), true);
}

$MSFrameworkGeonames = (new \MSFramework\geonames());
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Dati Sede</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome Sede*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Slug*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                            <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Email</label>
                                    <input id="email" name="email" type="text" class="form-control email" value="<?php echo htmlentities($r['email']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Telefono</label>
                                    <input id="telefono" name="telefono" type="text" class="form-control" value="<?php echo htmlentities($r['telefono']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php require(ABSOLUTE_SW_PATH . "includes/template/geodata/geodata.php"); ?>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Descrizione Breve</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['short_descr']) ?>"></i></span>
                                    <textarea id="short_descr" name="short_descr"><?php echo $MSFrameworki18n->getFieldValue($r['short_descr']) ?></textarea>
                                </div>

                                <div class="col-sm-6">
                                    <label>Descrizione Estesa</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['long_descr']) ?>"></i></span>
                                    <textarea id="long_descr" name="long_descr"><?php echo $MSFrameworki18n->getFieldValue($r['long_descr']) ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                        </fieldset>

                        <h1>Gallery</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12" style="margin-bottom: 40px;">
                                    <?php
                                    (new \MSFramework\uploads('NEGOZIO_SEDI'))->initUploaderHTML("images", $r['gallery']);
                                    ?>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
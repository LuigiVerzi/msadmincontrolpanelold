<?php
/**
 * MSAdminControlPanel
 * Date: 27/06/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('working_hours');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Orari</h1>
                        <fieldset>
                            <div class="alert alert-info">
                                Lasciare i campi apertura/chiusura vuoti durante i periodi di riposo/chiusura
                            </div>

                            <?php
                            $hours = $r['hours'];

                            $timestamp = strtotime('next Monday');
                            $days = array();
                            for ($i = 0; $i < 7; $i++) {
                                $day_name = ucfirst(strftime('%A', $timestamp));
                                $timestamp = strtotime('+1 day', $timestamp);
                            ?>
                            <div class="row day_block">
                                <div class="col-sm-12">
                                    <h3><?= $day_name ?></h3>
                                </div>

                                <div class="col-sm-12">
                                    <div class="row">

                                        <div class="col-sm-6" style="padding: 10px 30px 10px 60px;">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <span>Apertura AM</span>
                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                        <input type="text" class="form-control am_in" value="<?= $hours[$i]['am_in'] ?>">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-clock-o"></span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <span>Chiusura AM</span>
                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                        <input type="text" class="form-control am_out" value="<?= $hours[$i]['am_out'] ?>">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-clock-o"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6" style="padding: 10px 60px 10px 30px;">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <span>Apertura PM</span>
                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                        <input type="text" class="form-control pm_in" value="<?= $hours[$i]['pm_in'] ?>">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-clock-o"></span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <span>Chiusura PM</span>
                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                        <input type="text" class="form-control pm_out" value="<?= $hours[$i]['pm_out'] ?>">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-clock-o"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php } ?>
                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
function initForm() {
    initClassicTabsEditForm();

    $('.clockpicker').clockpicker();
}

function moduleSaveFunction() {
    global_hours = new Array();
    $('.day_block').each(function(index) {
        cur_hours = new Object();

        cur_hours['am_in'] = $(this).find('.am_in').val();
        cur_hours['am_out'] = $(this).find('.am_out').val();
        cur_hours['pm_in'] = $(this).find('.pm_in').val();
        cur_hours['pm_out'] = $(this).find('.pm_out').val();

        global_hours.push(cur_hours);
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pHoursAry": global_hours,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "hour_format_not_valid") {
                bootbox.error("Il formato dell'orario deve essere HH:MM (es. 12:05).");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
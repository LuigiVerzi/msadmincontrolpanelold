<?php
/**
 * MSAdminControlPanel
 * Date: 27/06/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'working_hours'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

$subgroups = array("am_in", "am_out", "pm_in", "pm_out");
foreach($_POST['pHoursAry'] as $cur_group) {
    foreach($subgroups as $sg) {
        if($cur_group[$sg] != "" && !preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $cur_group[$sg])) {
            echo json_encode(array("status" => "hour_format_not_valid"));
            die();
        }
    }
}


$array_to_save = array(
    "value" => json_encode(array(
        "hours" => $_POST['pHoursAry'],
    )),
    "type" => "working_hours"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'working_hours'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
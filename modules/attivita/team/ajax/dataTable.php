<?php
/**
 * MSAdminControlPanel
 * Date: 02/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');


foreach ($MSFrameworkDatabase->getAssoc("SELECT * FROM negozio_team_category") as $r) {
    $array['data'][] = array(
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['nome']),
        $MSFrameworki18n->getFieldValue($r['descr']),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

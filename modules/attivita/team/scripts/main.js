$(document).ready(function() {
    if($('#team_single').length) {
        if($("#record_id").val().length) {
            loadStandardDataTable('main_table_list', false, {
                "ajax": 'ajax/dataTable.php?category=' + $("#record_id").val(),
                "pageLength": 50,
                "paging": false
            });
            manageGridButtons('main_table_list', $('.inner_buttons .title-action'));
        }
    }
    else {
        loadStandardDataTable('main_table_list');
        manageGridButtons('main_table_list');
    }
    allowRowHighlights('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    if(!$('#team_single').length) {
        initOrak('images', 1, 0, 0, 100, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
        initTinyMCE( '#descrizione');

    }
    else {
        $("#dataTableNew").on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            location.href = 'edit.php?category=' + $("#record_id").val();
        });
    }

    $('#editUndoBtn').on('click', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        e.stopPropagation();

        bootbox.confirm('Sei sicuro di voler annullare? I dati non salvati andranno persi.', function(result) {
            if(result) {
                if(!$('#team_single').length) {
                    window.location.href = 'index.php?id=' + $("#category").val();
                }
                else {
                    window.location.href = '../index.php';
                }
            }
        });

        return false;
    })
}

function moduleSaveFunction() {

    if(!$('#team_single').length) {
        var url = "db/saveData.php?type=team";
        var data = {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pCognome": $('#cognome').val(),
            "pPosizione": $('#posizione').val(),
            "pCategory": $('#category').val(),
            "pDescrizione": tinymce.get('descrizione').getContent(),
            "pEmail": $('#email').val(),
            "pCell": $('#cellulare').val(),
            "pWebsite": $('#website').val(),
            "pFBProfile": $('#fb_profile').val(),
            "pTwitterProfile": $('#twitter_profile').val(),
            "pLinkedinProfile": $('#linkedin_profile').val(),
            "pGoogPlusProfile": $('#googplus_profile').val(),
            "pInstagramProfile": $('#instagram_profile').val(),
            "pImagesAry": composeOrakImagesToSave('images'),

        };
    }
    else {
        var url = "db/saveData.php?type=category";
        var data = {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pDescrizione": $('#descr').val()
        };
    }

    $.ajax({
        url: url,
        type: "POST",
        data: data,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido!");
            } else if(data.status == "site_not_valid") {
                bootbox.error("L'indirizzo web inserito è in un formato non valido! Assicurarsi di inserire http:// prima dell'indirizzo");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "ok") {
                if($("#category").length) {
                    // Salvataggio utente
                    custom_redirect = 'index.php?id=' + $("#category").val();
                } else {
                    // Salvataggio categoria
                    custom_redirect = '../index.php';

                    if(!$('#record_id').val().length) {
                        $('.no_category_hide').show();
                        $('#no_cat_alert').remove();

                        loadStandardDataTable('main_table_list', false, {
                            "ajax": 'ajax/dataTable.php?category=' + data.id,
                            "pageLength": 50,
                            "paging": false
                        });

                    }
                }

                succesModuleSaveFunctionCallback(data, '', '', custom_redirect);
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 02/07/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$categoria = (int)$_GET['category'];

foreach ($MSFrameworkDatabase->getAssoc("SELECT * FROM negozio_team WHERE category = $categoria AND category > 0") as $r) {
    $images = json_decode($r['propic'], true);

    $array['data'][] = array(
        '<img src="' . UPLOAD_NEGOZIO_TEAM_FOR_DOMAIN_HTML . "tn/" . $images[0] . '" alt="" height="auto" width="80">',
        $r['nome'],
        $r['cognome'],
        $r['email'],
        $r['cellulare'],
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

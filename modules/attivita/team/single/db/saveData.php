<?php
/**
 * MSAdminControlPanel
 * Date: 02/07/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$save_type = $_GET['type'];
$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);
$can_save = true;

if($save_type == 'category') {

    if ($_POST['pNome'] == "") {
        $can_save = false;
    }

    if (!$can_save) {
        echo json_encode(array("status" => "mandatory_data_missing"));
        die();
    }

    if ($db_action == "update") {
        $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT nome, descr FROM negozio_team_category WHERE id = :id", array(":id" => $_POST['pID']), true);
    }

    $array_to_save = array(
        "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
        "descr" => $MSFrameworki18n->setFieldValue($r_old_data['descr'], $_POST['pDescrizione']),
    );

    $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
    if ($db_action == "insert") {
        $result = $MSFrameworkDatabase->pushToDB("INSERT INTO negozio_team_category ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    } else {
        $result = $MSFrameworkDatabase->pushToDB("UPDATE negozio_team_category SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    }

    if (!$result) {
        echo json_encode(array("status" => "query_error"));
        die();
    }
}
else
{
    if ($_POST['pNome'] == "") {
        $can_save = false;
    }

    if (!$can_save) {
        echo json_encode(array("status" => "mandatory_data_missing"));
        die();
    }

    if ($_POST['pWebsite'] != "") {
        if (!filter_var($_POST['pWebsite'], FILTER_VALIDATE_URL)) {
            echo json_encode(array("status" => "site_not_valid"));
            die();
        }
    }

    $uploader = new \MSFramework\uploads('NEGOZIO_TEAM');
    $ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
    if ($ary_files === false) {
        echo json_encode(array("status" => "mv_error"));
        die();
    }

    if ($db_action == "update") {
        $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT propic, posizione FROM negozio_team WHERE id = :id", array(":id" => $_POST['pID']), true);
    }

    $array_to_save = array(
        "nome" => $_POST['pNome'],
        "cognome" => $_POST['pCognome'],
        "email" => $_POST['pEmail'],
        "posizione" => $MSFrameworki18n->setFieldValue($r_old_data['posizione'], $_POST['pPosizione']),
        "category" => $_POST['pCategory'],
        "descrizione" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione'],$_POST['pDescrizione']),
        "cellulare" => $_POST['pCell'],
        "website" => $_POST['pWebsite'],
        "fb_profile" => $_POST['pFBProfile'],
        "instagram_profile" => $_POST['pInstagramProfile'],
        "twitter_profile" => $_POST['pTwitterProfile'],
        "linkedin_profile" => $_POST['pLinkedinProfile'],
        "googplus_profile" => $_POST['pGoogPlusProfile'],
        "propic" => json_encode($ary_files),
    );

    $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
    if ($db_action == "insert") {
        $result = $MSFrameworkDatabase->pushToDB("INSERT INTO negozio_team ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    } else {
        $result = $MSFrameworkDatabase->pushToDB("UPDATE negozio_team SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    }

    if (!$result) {
        if ($db_action == "insert") {
            $uploader->unlink($ary_files);
        }

        echo json_encode(array("status" => "query_error"));
        die();
    }

    if ($db_action == "update") {
        $uploader->unlink($ary_files, json_decode($r_old_data['propic'], true));
    }
}


echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
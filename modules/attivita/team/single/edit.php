<?php
/**
 * MSAdminControlPanel
 * Date: 02/07/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$category_id = (int)$_GET['category'];

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM negozio_team WHERE id = :id", array(":id" => $_GET['id']), true);
    $category_id = $r['category'];
}

if((int)$category_id <= 0) {
    header('Location: ../index.php');
}

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Dati Utente</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-10 col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Nome*</label>
                                            <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($r['nome']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Cognome*</label>
                                            <input id="cognome" name="cognome" type="text" class="form-control" value="<?php echo htmlentities($r['cognome']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Categoria*</label>
                                            <select id="category" name="category" type="text" class="form-control required">
                                                <?php foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM negozio_team_category") as $cat) { ?>
                                                    <option value="<?= $cat['id']; ?>" <?= ($category_id == $cat['id'] ? ' selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($cat['nome']); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Posizione Ricoperta</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['posizione']) ?>"></i></span>
                                            <input id="posizione" name="posizione" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['posizione'])) ?>" placeholder="Es. Store Manager, Assistenza Clienti, Commessa, ecc..">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione']) ?>"></i></span>
                                            <textarea id="descrizione" name="descrizione" class="form-control" rows="15"><?php echo htmlentities($MSFrameworki18n->getFieldValue($r['descrizione'])) ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-3" style="margin-bottom: 40px;">
                                    <label>Foto</label>
                                    <?php
                                    (new \MSFramework\uploads('NEGOZIO_TEAM'))->initUploaderHTML("images", $r['propic']);
                                    ?>

                                </div>
                            </div>

                        </fieldset>

                        <h1>Social & Contatti</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Email</label>
                                    <input id="email" name="email" type="text" class="form-control email" value="<?php echo htmlentities($r['email']) ?>">
                                </div>

                                <div class="col-sm-4">
                                    <label>Cellulare</label>
                                    <input id="cellulare" name="cellulare" type="text" class="form-control" value="<?php echo htmlentities($r['cellulare']) ?>">
                                </div>

                                <div class="col-sm-4">
                                    <label>Sito Web</label>
                                    <input id="website" name="website" type="text" class="form-control" value="<?php echo htmlentities($r['website']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Facebook</label>
                                    <input id="fb_profile" name="fb_profile" type="text" class="form-control" value="<?php echo htmlentities($r['fb_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Instagram</label>
                                    <input id="instagram_profile" name="instagram_profile" type="text" class="form-control" value="<?php echo htmlentities($r['instagram_profile']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Twitter</label>
                                    <input id="twitter_profile" name="twitter_profile" type="text" class="form-control" value="<?php echo htmlentities($r['twitter_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Linkedin</label>
                                    <input id="linkedin_profile" name="linkedin_profile" type="text" class="form-control" value="<?php echo htmlentities($r['linkedin_profile']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Google Plus</label>
                                    <input id="googplus_profile" name="googplus_profile" type="text" class="form-control" value="<?php echo htmlentities($r['googplus_profile']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
    
</script>
</body>
</html>
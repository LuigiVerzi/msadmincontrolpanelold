<?php
/**
 * MSAdminControlPanel
 * Date: 02/07/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM negozio_team_category WHERE id = :id", array(":id" => $_GET['id']), true);
}

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />
            <input type="hidden" id="team_single" value="1" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Dati Categoria Team</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome*</label>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])); ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Descrizione</label>
                                    <input id="descr" name="descr" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['descr'])); ?>">
                                </div>
                            </div>

                            <h2 class="title-divider">Gestisci staff</h2>

                            <div style="display: <?= ($_GET['id'] ? 'block' : 'none'); ?>" class="no_category_hide">

                                <div class="row">
                                    <div class="col-lg-6"> </div>
                                    <div class="col-lg-6">
                                        <div class="inner_buttons" style="margin-top: -15px; ">
                                            <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardGrid.php"); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th style="width: 80px;"></th>
                                                <th>Nome</th>
                                                <th>Cognome</th>
                                                <th>Email</th>
                                                <th>Cellulare</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <?php if(!$_GET['id']) { ?>
                                <div id="no_cat_alert" class="alert alert-info">Prima di poter creare nuovi membri dello staff è necessario salvare le modifiche relative alla categoria.</div>
                            <?php } ?>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
    
</script>
</body>
</html>
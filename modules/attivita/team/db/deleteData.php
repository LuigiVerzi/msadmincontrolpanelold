<?php
/**
 * MSAdminControlPanel
 * Date: 02/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->deleteRow("negozio_team_category", "id", $_POST['pID'])) {
    $MSFrameworkDatabase->deleteRow("negozio_team", "category", $_POST['pID']);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = "update";
if(!$isSaaS && $MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'producer_config'") == 0) {
    $db_action = "insert";
}

$got_main_lang = false;
foreach($_POST['pLanguages'] as $v) {
    if($v['is_primary'] == "1") {
        $primary_lang_id = $v['id'];
        $got_main_lang = true;
    }
}

if(!$got_main_lang) {
    echo json_encode(array("status" => "miss_main_lang"));
    die();
}

if ($db_action == "update") {
    $r_old_data = $MSFrameworkCMS->getCMSData('producer_config');
    $old_lang_data = json_decode($r_old_data['languages'], true);
    $old_primary_lang_id = "";

    foreach ($old_lang_data as $v) {
        if ($v['is_primary'] == "1") {
            $old_primary_lang_id = $v['id'];
            break;
        }
    }

    if (!$MSFrameworki18n->canChangePrimary($old_primary_lang_id, $primary_lang_id)) {
        echo json_encode(array("status" => "can_not_change_primary"));
        die();
    }
}

if(!$isSaaS) {
    $modules_order = $_POST['pModulesOrder'];
    if (is_array($modules_order) && isset($modules_order['preset'])) {

        $array_to_save = array(
            "name" => $modules_order['name'],
            "preset" => json_encode($modules_order['preset']),
            "menu_granular" => $modules_order['granular']
        );

        if ($modules_order['id'] == 'new') {
            $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, "insert");
            $MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`menu_presets` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            $modules_order = $MSFrameworkDatabase->lastInsertId();
        } else if ($modules_order['id'] == 'custom') {
            //salvo l'ordinamento del menu
            $preset_db_action = "update";
            if ($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type LIKE 'menu_preset'") == 0) {
                $preset_db_action = "insert";
            }

            $custom_preset_to_save = array(
                "value" => json_encode($modules_order['preset']),
                "type" => "menu_preset"
            );

            $stringForDB = $MSFrameworkDatabase->createStringForDB($custom_preset_to_save, $preset_db_action);

            if ($preset_db_action == "insert") {
                $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            } else {
                $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'menu_preset'", $stringForDB[0]);
            }

            //salvo i permessi granulari
            $granular_db_action = "update";
            if ($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type LIKE 'menu_granular'") == 0) {
                $granular_db_action = "insert";
            }

            $custom_granular_to_save = array(
                "value" => $modules_order['granular'],
                "type" => "menu_granular"
            );

            $stringForDB = $MSFrameworkDatabase->createStringForDB($custom_granular_to_save, $granular_db_action);

            if ($granular_db_action == "insert") {
                $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            } else {
                $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'menu_granular'", $stringForDB[0]);
            }

            $modules_order = 'custom';
        } else {
            $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, "update");
            $result = $MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`menu_presets` SET $stringForDB[1] WHERE id = '" . (int)$modules_order['id'] . "'", $stringForDB[0]);
            $modules_order = $modules_order['id'];
        }
    }
}

$array_to_save = array(
    "templatemail_colors" =>json_encode($_POST['pMailTemplateColors']),
    "templatemail_logosize" => $_POST['pMailTemplateLogoSize'],
    "gmaps_apikey" => $_POST['pGMapsKey'],
    "gplaces_apikey" => $_POST['pGPlacesKey'],
    "ggeocode_apikey" => $_POST['pGGeocodeKey'],
    "recaptcha_public" => $_POST['pReCAPTCHAPublic'],
    "recaptcha_private" => $_POST['pReCAPTCHAPrivate'],
    "recaptcha_invisible" => $_POST['pReCAPTCHAInvisible'],
    "languages" => json_encode($_POST['pLanguages']),
    "extra_modules" => json_encode($_POST['pExtraModules']),
    "extra_modules_settings" => json_encode($_POST['pExtraModulesSettings']),
    "modules_order" => $modules_order,
    "additional_css_acp" => $_POST['pCSSEditorACP'],
    "additional_js_acp" => $_POST['pJSEditorACP'],
    "uploadSettings" => $_POST['pUploadSettings'],
    "memcached_on" => $_POST['pMemcachedOn'],
    "framework_version" => json_encode($_POST['pFrameworkVersion']),
);

if($isSaaS) {
    unset($array_to_save['modules_order']);
}

$result = $MSFrameworkCMS->setCMSData('producer_config', $array_to_save, true);

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
<?php
/**
 * MSAdminControlPanel
 * Date: 13/10/2018
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($_GET['what'] == 'getCacheOnlyCurDomain') {
    $str = implode("<br />", $MSFrameworkDatabase->getCacheStatusByNameSpace());
} else {
    $str = implode("<br />", $MSFrameworkDatabase->getCacheStatus());
}

if($str == "") {
    $str = "Nessun dato presente in cache";
}

echo $str;
?>

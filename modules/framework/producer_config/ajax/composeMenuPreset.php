<?php
/**
 * MSAdminControlPanel
 * Date: 13/10/2018
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$sw_modules = new \MSSoftware\modules();
$modules = new \MSFramework\modules();

$menu_array = array();
$preset_id = $_POST['id'];

foreach((new \MSFramework\users())->getUserLevels() as $userLevelID => $userLevelName) {
    $menu_array[$userLevelID] = array(
        ($preset_id !== 'new' ? $sw_modules->printSidebarSetterOrder(null, $preset_id, $userLevelID) : array()),
        $sw_modules->printSidebarSetterOrder(null, false, $userLevelID)
    );
}

echo json_encode(array($menu_array, json_encode($sw_modules->getCurrentGranularSettings($preset_id, $_POST['loadedModuleID']))));
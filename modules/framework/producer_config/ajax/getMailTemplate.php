<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if(isset($_POST['colors']) && isset($_POST['logo_size']) && isset($_POST['email_demo_template'])) {

    $emails = (new \MSFramework\emails());

    $producer_config = $MSFrameworkCMS->getCMSData('producer_config');
    $def_logo_size = json_decode($producer_config['templatemail_logosize'], true);

    $colors = $_POST['colors'];
    $logo_size = $_POST['logo_size'];
    $demo_template = $_POST['email_demo_template'];

    $emails->email_colors = $colors;

    $email_info = $emails->getTemplates($demo_template, true);
    $email_html = $email_info['data']['html'];

    $email_html = str_replace(
        "{logo_size}",
        $logo_size . 'px',
        $email_html
    );

    $site_url = $MSFrameworkCMS->getURLToSite();

    $email_html = $emails->replaceGlobalShortcode($email_html);
    $re = '/href="\{(([^"]+)+)\}"/m';
    $email_html = preg_replace($re, 'href="' . $site_url . '"', $email_html);
    $email_html = $emails->formatTemplateHTML($email_html);

    echo $email_html;
}
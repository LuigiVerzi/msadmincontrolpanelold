<?php
/**
 * MSAdminControlPanel
 * Date: 13/10/2018
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($_GET['what'] == 'cleanCacheOnlyCurDomain') {
    $MSFrameworkDatabase->flushCacheByNameSpace();
} else {
    $MSFrameworkDatabase->flushCache();
}
?>

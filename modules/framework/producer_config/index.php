<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('producer_config');
$modules = new \MSSoftware\modules();

$MSFrameworkGDPR = new \MSFramework\Modules\gdpr();
?>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <?php
            if(isset($_GET['must_set_lang_data'])) {
                ?>
                <input type="hidden" id="must_set_lang_data" value="must_set_lang_data" />
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Devi impostare i dati relativi alla localizzazione prima di poter utilizzare il SW.
                </div>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    if($isSaaS) {
                    ?>
                    <div class="alert alert-danger">
                        Le seguenti impostazioni verranno applicate a tutti i SaaS <b><?= SW_NAME ?></b>.
                    </div>
                    <?php } ?>

                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>ACP/Sito</h1>
                        <fieldset>
                            <h2 class="title-divider">Generale</h2>
                            <div class="row">
                                <?php
                                $customer_data = $MSFrameworkDatabase->getAssoc("SELECT virtual_server_name, set_offline FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE customer_domain = :domain", array(":domain" => $MSFrameworkCMS->getCleanHost()), true);

                                if(strstr($_SERVER['DOCUMENT_ROOT'], "/SVILUPPO/")) { ?>
                                    <div class="col-sm-6">
                                        <label>Virtual Server Name</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Simula l'esecuzione dell'ACP e del SW all'interno di un determinato dominio. Viene utilizzato soprattutto per garantire l'upload delle immagini all'interno della giusta sottocartella. Utilizzato solo in fase di SVILUPPO."></i></span>
                                        <input id="virtual_server_name" name="virtual_server_name" type="text" class="form-control" value="<?php echo htmlentities($customer_data['virtual_server_name']) ?>">
                                    </div>
                                <?php } ?>

                                <?php
                                if(!$MSFrameworkCMS->isStaging()) {
                                $framework_version = json_decode($r['framework_version'], true);
                                ?>
                                    <div class="col-sm-4">
                                        <label>Provenienza Framework</label>
                                        <select id="fw_type" class="form-control">
                                            <optgroup label="Default">
                                                <option value="default" <?= ($framework_version['type'] == "" || $framework_version['type'] == "default" ? "selected" : "") ?>>Default</option>
                                            </optgroup>
                                            <optgroup label="Sviluppo">
                                                <option value="dev_luigi" <?= ($framework_version['type'] == "dev_luigi" ? "selected" : "") ?>>Sviluppo Luigi</option>
                                                <option value="dev_luca" <?= ($framework_version['type'] == "dev_luca" ? "selected" : "") ?>>Sviluppo Luca</option>
                                                <option value="dev_vincenzo" <?= ($framework_version['type'] == "dev_vincenzo" ? "selected" : "") ?>>Sviluppo Vincenzo</option>
                                            </optgroup>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <label>Versione Framework</label>
                                        <select id="fw_version" class="form-control">
                                            <option value="1" <?= ($framework_version['version'] == "" || $framework_version['version'] == "1" ? "selected" : "") ?>>v1</option>
                                        </select>
                                    </div>
                                <?php } ?>
                            </div>

                            <h2 class="title-divider">Impostazioni</h2>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>&#8205;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="memcached_on" class="memcached_on" <?php if($MSFrameworkDatabase->cacheEnabled()) { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Memcached</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4" style="">
                                    <label>Svuota Cache DB</label>
                                    <a class="btn btn-info" id="cleanCache" cleaning_txt="Svuoto la cache...">Tutti i domini</a>
                                    <a class="btn btn-info" id="cleanCacheOnlyCurDomain" cleaning_txt="Svuoto la cache...">Solo dominio corrente</a>
                                </div>

                                <div class="col-sm-4" style="">
                                    <label>Stato Cache DB</label>
                                    <a class="btn btn-info" id="getCache" cleaning_txt="Ottengo la cache...">Tutti i domini</a>
                                    <a class="btn btn-info" id="getCacheOnlyCurDomain" cleaning_txt="Ottengo la cache...">Solo dominio corrente</a>
                                </div>
                            </div>

                            <h2 class="title-divider">Personalizzazione ACP</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h3>Custom CSS</h3>
                                    <div id="editor_css_acp" style="height: 350px;"><?php echo $r['additional_css_acp'] ?></div>
                                </div>

                                <div class="col-sm-6">
                                    <h3>Custom JS</h3>
                                    <div id="editor_js_acp" style="height: 350px;"><?php echo $r['additional_js_acp'] ?></div>
                                </div>
                            </div>

                        </fieldset>

                        <h1>Email/Form</h1>
                        <fieldset>
                            <h2 class="title-divider">Google reCAPTCHA</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Chiave Pubblica</label>
                                    <input id="recaptcha_public" name="recaptcha_public" type="text" class="form-control" value="<?php echo htmlentities($r['recaptcha_public']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Chiave Privata</label>
                                    <input id="recaptcha_private" name="recaptcha_private" type="text" class="form-control" value="<?php echo htmlentities($r['recaptcha_private']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="recaptcha_invisible" <?php if($r['recaptcha_invisible'] == 1) { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">reCaptcha Invisible</span> <br />
                                        </label>
                                    </div>
                                    <small>Assicurarsi di avere la chiave adatta</small>
                                </div>
                            </div>

                            <h2 class="title-divider">Template Email</h2>
                            <?php
                            $email_colors = json_decode($r['templatemail_colors']);
                            ?>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Colore Principale</label>
                                    <div class="input-group colorpicker-component email-colorpicker">
                                        <input id="templatemail_maincolor" name="templatemail_maincolor" type="text" class="form-control" value="<?php echo htmlentities(@$email_colors[0]) ?>">
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Colore CTA</label>
                                    <div class="input-group colorpicker-component email-colorpicker">
                                        <input id="templatemail_secondarycolor" name="templatemail_secondarycolor" type="text" class="form-control" value="<?php echo htmlentities(@$email_colors[1]) ?>">
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Colore Terziario</label>
                                    <div class="input-group colorpicker-component email-colorpicker">
                                        <input id="templatemail_thirdcolor" name="templatemail_thirdcolor" type="text" class="form-control" value="<?php echo htmlentities(@$email_colors[2]) ?>">
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>Colore Sfondo</label>
                                    <div class="input-group colorpicker-component email-colorpicker">
                                        <input id="templatemail_bgcolor" name="templatemail_bgcolor" type="text" class="form-control" value="<?php echo htmlentities(@$email_colors[3]) ?>">
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Dimensioni Logo</label>
                                    <div id="templatemail_logosize" data-current="<?php echo htmlentities($r['templatemail_logosize']) ?>"></div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="ibox collapsed" id="email_template_panel">
                                <div class="ibox-title">
                                    <h5>Anteprima
                                        <select id="email_demo_template" style="position: relative; z-index: 100; margin: -10px 0 0px 0px;  border-color: white; color: #777777;">
                                            <?php
                                            $templates_groups = (new \MSFramework\emails())->getTemplates("", false);
                                            foreach($templates_groups as $group => $templates_list) {
                                                echo '<optgroup label="' . $group . '">';
                                                foreach ($templates_list as $id => $template) {

                                                    echo '<option value="' . $id . '">' .  $template['nome'] . '</option>';
                                                }
                                                echo '</optgroup>';
                                            }
                                            ?>
                                        </select>
                                    </h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="fullscreen-link">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content" id="email_template_html">
                                </div>
                            </div>
                        </fieldset>

                        <h1>Uploads</h1>
                        <fieldset>
                            <h2 class="title-divider">Impostazioni predefinite Uploads</h2>

                            <h4>Valori Generali</h4>
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Width</label>
                                            <input id="uplSettings_crop_width" type="text" class="form-control uplSettings_crop_width" value="<?php echo htmlentities($r['uploadSettings']['crop_width']) ?>">
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Height</label>
                                            <input id="uplSettings_crop_height" type="text" class="form-control uplSettings_crop_height" value="<?php echo htmlentities($r['uploadSettings']['crop_height']) ?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Width Thumb</label>
                                            <input id="uplSettings_crop_width_thumb" type="text" class="form-control uplSettings_crop_width_thumb" value="<?php echo htmlentities($r['uploadSettings']['crop_width_thumb']) ?>">
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Height Thumb</label>
                                            <input id="uplSettings_crop_height_thumb" type="text" class="form-control uplSettings_crop_height_thumb" value="<?php echo htmlentities($r['uploadSettings']['crop_height_thumb']) ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Qualità Compressione Immagini</label>
                                            <div id="uplSettings_compression_quality" class="uplSettings_compression_quality" data-current="<?php echo htmlentities(($r['uploadSettings']['compression_quality'] ? $r['uploadSettings']['compression_quality'] : 100)) ?>"></div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-top: 8px;">
                                        <div class="col-sm-6">
                                            <label> &nbsp; </label>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" class="uplSettings_apply_watermark" id="uplSettings_apply_watermark" <?php if($r['uploadSettings']['apply_watermark'] == "1") { echo "checked"; } ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Applica Watermark</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php foreach((new MSFramework\uploads())->getCostants() as $upCostant => $upFolder) { ?>
                                <?php
                                if($upCostant == 'TMP') {
                                    continue;
                                }
                                else {
                                    $single_uploader_settings = array();
                                    if(isset($r['uploadSettings']['single']) && isset($r['uploadSettings']['single'][$upCostant])) {
                                        $single_uploader_settings = $r['uploadSettings']['single'][$upCostant];
                                    }
                                }
                                ?>

                                <div class="ibox <?php if($single_uploader_settings['enabled'] != "1") { echo "collapsed"; } ?> uploaderBox single_uploader_settings" data-key="<?= $upCostant; ?>">
                                    <div class="ibox-title">
                                        <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -5px 15px 0 0px;">
                                            <input type="checkbox" id="uplSettings_enabled_<?= $upCostant; ?>" class="uplSettings_enabled" <?php if($single_uploader_settings['enabled'] == "1") { echo "checked"; } ?>>
                                            <i></i>
                                        </div>
                                        <h5>
                                            <small>Sovrascrivi Valori per</small> <?= $upCostant; ?>
                                        </h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Width</label>
                                                        <input id="uplSettings_crop_width_<?= $upCostant; ?>" placeholder="Utilizza valori predefiniti" type="text" class="form-control uplSettings_crop_width" value="<?php echo htmlentities($single_uploader_settings['crop_width']) ?>">
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label>Height</label>
                                                        <input id="uplSettings_crop_height_<?= $upCostant; ?>" placeholder="Utilizza valori predefiniti"  type="text" class="form-control uplSettings_crop_height" value="<?php echo htmlentities($single_uploader_settings['crop_height']) ?>">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Width Thumb</label>
                                                        <input id="uplSettings_crop_width_thumb_<?= $upCostant; ?>" placeholder="Utilizza valori predefiniti"  type="text" class="form-control uplSettings_crop_width_thumb" value="<?php echo htmlentities($single_uploader_settings['crop_width_thumb']) ?>">
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label>Height Thumb</label>
                                                        <input id="uplSettings_crop_height_thumb_<?= $upCostant; ?>" placeholder="Utilizza valori predefiniti" type="text" class="form-control uplSettings_crop_height_thumb" value="<?php echo htmlentities($single_uploader_settings['crop_height_thumb']) ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label>Qualità Compressione Immagini</label>
                                                        <div id="uplSettings_compression_quality_<?= $upCostant; ?>" class="uplSettings_compression_quality" data-current="<?php echo htmlentities(($single_uploader_settings['compression_quality'] ? $single_uploader_settings['compression_quality'] : 100)) ?>"></div>
                                                    </div>
                                                </div>

                                                <div class="row" style="margin-top: 8px;">
                                                    <div class="col-sm-6">
                                                        <label> &nbsp; </label>
                                                        <div class="styled-checkbox form-control">
                                                            <label style="width: 100%;">
                                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                    <input type="checkbox" class="uplSettings_apply_watermark" id="uplSettings_apply_watermark_<?= $upCostant; ?>" <?php if($single_uploader_settings['apply_watermark'] == "1") { echo "checked"; } ?>>
                                                                    <i></i>
                                                                </div>
                                                                <span style="font-weight: normal;">Applica Watermark</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </fieldset>

                        <h1>API</h1>
                        <fieldset>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>API Key Google Maps</label>
                                    <input id="gmaps_apikey" name="gmaps_apikey" type="text" class="form-control" value="<?php echo htmlentities($r['gmaps_apikey']) ?>">
                                </div>

                                <div class="col-sm-4">
                                    <label>API Key Google Places</label>
                                    <input id="gplaces_apikey" name="gplaces_apikey" type="text" class="form-control" value="<?php echo htmlentities($r['gplaces_apikey']) ?>">
                                </div>

                                <div class="col-sm-4">
                                    <label>API Key Google Geocode</label>
                                    <input id="ggeocode_apikey" name="ggeocode_apikey" type="text" class="form-control" value="<?php echo htmlentities($r['ggeocode_apikey']) ?>">
                                </div>
                            </div>

                        </fieldset>

                        <h1>i18n</h1>
                        <?php
                        $checked_languages_global = json_decode($r['languages'], true);

                        $main_lang_id = "";
                        $used_lang_ary = array();
                        foreach($checked_languages_global as $v) {
                            if($v['is_primary'] == "1") {
                                $main_lang_id = $v['id'];
                            }

                            $used_lang_ary[] = $v['id'];
                        }
                        ?>

                        <fieldset>
                            <div class="alert alert-info">
                                <ol style="padding: 0 15px; margin-bottom: 0px;">
                                    <li>All'interno del sito, per le stringhe non tradotte, verrà visualizzata la stringa impostata per la lingua principale. In alcuni casi (ad esempio quando la stringa è un riferimento utile per identificare un elemento non ancora tradotto) questo accadrà anche all'interno del backoffice.</li>
                                    <li>Non è possibile modificare la lingua principale se la nuova lingua impostata non ha le traduzioni complete al 100%</li>
                                    <li>Non è possibile impostare una traduzione per una lingua non principale senza impostarne prima una per la lingua principale</li>
                                    <li>I campi traducibili sono contrassegnati con il simbolo <i class="fa fa-language"></i></li>
                                </ol>
                            </div>

                            <h2 class="title-divider">Lingue in evidenza</h2>
                            <div class="row">
                                <?php
                                foreach($MSFrameworki18n->getLanguagesDetails("", "*", "main") as $lang) {
                                    ?>
                                    <div class="col-sm-3">
                                        <div class="styled-checkbox form-control">
                                            <label style="width: 100%;">
                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                    <input type="checkbox" id="lang_<?php echo $lang['id'] ?>" class="lang_check" <?php if(in_array($lang['id'], $used_lang_ary)) { echo "checked"; } ?>>
                                                    <i></i>
                                                </div>
                                                <span style="font-weight: normal;"><img src="<?php echo FRAMEWORK_COMMON_CDN ?>img/languages/<?php echo $lang['flag_code'] ?>.png" style="margin-right: 5px;" /><?php echo $lang['italian_name'] ?></span>
                                            </label>
                                        </div>

                                        <div><input type="radio" name="is_primary" id="primary_<?php echo $lang['id'] ?>" class="is_primary" <?php if($main_lang_id == $lang['id']) { echo "checked"; } ?> /> Lingua principale</div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <h2 class="title-divider">Altre lingue</h2>
                            <div class="row">
                                <?php
                                foreach($MSFrameworki18n->getLanguagesDetails("", "*", "not_main") as $lang) {
                                    ?>
                                    <div class="col-sm-3">
                                        <div class="styled-checkbox form-control">
                                            <label style="width: 100%;">
                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                    <input type="checkbox" id="lang_<?php echo $lang['id'] ?>" class="lang_check" <?php if(in_array($lang['id'], $used_lang_ary)) { echo "checked"; } ?>>
                                                    <i></i>
                                                </div>
                                                <span style="font-weight: normal;"><img src="<?php echo FRAMEWORK_COMMON_CDN ?>img/languages/<?php echo $lang['flag_code'] ?>.png" style="margin-right: 5px;" /><?php echo $lang['italian_name'] ?></span>
                                            </label>
                                        </div>

                                        <div><input type="radio" name="is_primary" id="primary_<?php echo $lang['id'] ?>" class="is_primary" <?php if($main_lang_id == $lang['id']) { echo "checked"; } ?> /> Lingua principale</div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                        </fieldset>

                        <h1>Plugin Extra</h1>
                        <fieldset>
                            <div class="row">
                                <?php
                                $gotModules = (new \MSFramework\modules())->getModules();
                                foreach($gotModules as $moduleK => $moduleAry) {
                                    if($moduleAry['required']) continue;
                                    ?>
                                    <div class="col-sm-12">
                                        <label style="font-weight: normal;">&nbsp;</label>
                                        <div class="styled-checkbox form-control">
                                            <label style="width: 100%;">
                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                    <input type="checkbox" id="module_<?php echo $moduleK ?>" class="enable_module <?php echo ($moduleAry['extra_settings']) ? 'has_extra_settings' : '' ?>" data-module-id="<?php echo $moduleK ?>" <?php if((new \MSFramework\modules())->checkIfIsActive($moduleK)) { echo "checked"; } ?>>
                                                    <i></i>
                                                </div>
                                                <span style="font-weight: normal;"><?php echo $moduleAry['name'] ?></span>
                                            </label>

                                            <?php if($moduleK == 'gdpr') { ?>
                                                <div id="module_gdpr_extra_settings" class="extra_settings_container" data-extra-settings-for-module="gdpr" style="display: <?php if(!(new \MSFramework\modules())->checkIfIsActive('gdpr')) { echo "none"; } ?>;">
                                                    <?php $extra_module_settings = (new \MSFramework\modules())->getExtraModuleSettings('gdpr');  ?>
                                                    <h3>Configurazioni supplementari per il modulo "<?php echo $gotModules['gdpr']['name'] ?>"</h3>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>Abilita moduli automaticamente</label>
                                                            <div class="row">
                                                                <?php
                                                                foreach($MSFrameworkGDPR->getCategorieGDPR() as $gdpr_module_k => $gdpr_module_v) {
                                                                    ?>
                                                                    <div class="col-sm-4">
                                                                        <div class="styled-checkbox form-control">
                                                                            <label style="width: 100%;">
                                                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                                    <input type="checkbox" id="module_gdpr_<?php echo $gdpr_module_k ?>" class="module_gdpr_autoenable" data-gdpr-module-id="<?php echo $gdpr_module_k ?>" <?php if(in_array($gdpr_module_k, $extra_module_settings['auto_enabled_modules'])) { echo "checked"; } ?>>
                                                                                    <i></i>
                                                                                </div>
                                                                                <span style="font-weight: normal;"><?php echo $gdpr_module_v['titolo'] ?></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12" style="margin-top: 20px;">
                                                            <label>Accetta tutto scrollando la pagina</label>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="styled-checkbox form-control">
                                                                        <label style="width: 100%;">
                                                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                                <input type="checkbox" id="gdpr_accept_on_scroll" <?php if($extra_module_settings['accept_on_scroll']['enabled'] == "1") { echo "checked"; } ?>>
                                                                                <i></i>
                                                                            </div>
                                                                            <span style="font-weight: normal;">Abilita</span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4" id="gdpr_accept_on_scroll_notifyuser_container" style="display: <?= ($extra_module_settings['accept_on_scroll']['enabled'] == "1") ? 'block' : 'none' ?>;">
                                                                    <div class="styled-checkbox form-control">
                                                                        <label style="width: 100%;">
                                                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                                <input type="checkbox" id="gdpr_accept_on_scroll_notifyuser" <?php if($extra_module_settings['accept_on_scroll']['notifyuser'] == "1") { echo "checked"; } ?>>
                                                                                <i></i>
                                                                            </div>
                                                                            <span style="font-weight: normal;">Avvisa l'utente</span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12" style="margin-top: 20px;">
                                                            <label>Abilita/disabilita elementi nella cookie policy. <a href="javascript:void(0)" onclick="ripristinaStatoCheckGDPR();">Ripristina default</a></label>

                                                            <?php
                                                            $old_type = "";
                                                            $auto_enable_elements = $MSFrameworkGDPR->getGDPRElementsFromHTML();

                                                            foreach($MSFrameworkGDPR->groupElementsByType() as $element_type => $elements) {
                                                                if($old_type != $element_type) {
                                                                $section_data = $MSFrameworkGDPR->getCategorieGDPR($element_type);
                                                            ?>
                                                                <div class="row">
                                                                    <div class="col-sm-12"><h4><?= $section_data['titolo'] ?></h4></div>
                                                            <?php
                                                                }

                                                                foreach($elements as $elementK => $element) {

                                                                    $gdpr_info = $element['details'];
                                                                    ?>
                                                                    <div class="col-sm-4">
                                                                        <div class="styled-checkbox form-control">
                                                                            <label style="width: 100%;">
                                                                                <div class="checkbox i-checks pull-right"
                                                                                     style="padding-top: 0px;">
                                                                                    <input type="checkbox"
                                                                                           id="<?= $elementK ?>"
                                                                                        <?php if (
                                                                                               in_array($elementK, $auto_enable_elements) &&
                                                                                               !in_array($elementK, $extra_module_settings['cookie_policy']['force_hide']) &&
                                                                                               !in_array($elementK, $extra_module_settings['cookie_policy']['force_show'])
                                                                                        ) {
                                                                                            echo "indeterminate='true'";
                                                                                        } ?>

                                                                                        <?php if (
                                                                                            in_array($elementK, $auto_enable_elements)
                                                                                        ) {
                                                                                            echo "data-should-auto-enable='1'";
                                                                                        } ?>

                                                                                        <?php if (
                                                                                        in_array($elementK, $extra_module_settings['cookie_policy']['force_show'])
                                                                                        ) {
                                                                                            echo "checked";
                                                                                        } ?>
                                                                                    class="gdpr_cookie_policy_check">
                                                                                    <i></i>
                                                                                </div>
                                                                                <span style="font-weight: normal;"><?= $MSFrameworki18n->getFieldValue($gdpr_info['nome']) ?></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }

                                                                if($old_type != $element_type) {
                                                            ?>
                                                                </div>
                                                            <?php
                                                                }

                                                                $old_type = $element_type;
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else if($moduleK == 'multiportal') { ?>
                                                <div id="module_multiportal_extra_settings" class="extra_settings_container" data-extra-settings-for-module="multiportal" style="display: <?php if(!(new \MSFramework\modules())->checkIfIsActive('multiportal')) { echo "none"; } ?>;">
                                                    <?php $extra_module_settings = (new \MSFramework\modules())->getExtraModuleSettings('multiportal'); ?>
                                                    <h3>Configurazioni supplementari per il modulo "<?php echo $gotModules['multiportal']['name'] ?>"</h3>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>ID Agenzia GestionaleRe</label>
                                                            <input id="multiportal_gestionalere_agency_id" type="text" class="form-control" value="<?php echo htmlentities($extra_module_settings['gestionalere']['agency_id']) ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else if($moduleK == 'scraper') { ?>
                                                <div id="module_scraper_extra_settings" class="extra_settings_container" data-extra-settings-for-module="scraper" style="display: <?php if(!(new \MSFramework\modules())->checkIfIsActive('scraper')) { echo "none"; } ?>;">
                                                    <?php $extra_module_settings = (new \MSFramework\modules())->getExtraModuleSettings('scraper'); ?>
                                                    <h3>Configurazioni supplementari per il modulo "<?php echo $gotModules['scraper']['name'] ?>"</h3>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Tipologia</label>
                                                            <select id="scraper_type" class="form-control">
                                                                <option value="remax" <?php if($extra_module_settings['type'] == "remax") { echo "selected"; } ?>>Remax</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                        </fieldset>

                        <h1>Moduli</h1>
                        <fieldset>
                            <h2 class="title-divider">Gestione dei menù</h2>
                            <textarea style="display: none;" class="abb_menu_granular"></textarea>

                            <?php
                            if($isSaaS) {
                                ?>
                                <div class="alert alert-info">
                                    La configurazione dei moduli varia in base al tipo di abbonamento, e va completata all'interno del modulo <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML ?>framework360/saas/configuration/">Impostazioni SaaS</a>.
                                </div>
                            <?php } else {
                            $menu_presets = $modules->getMenuPresets(false);
                            $current_preset = (isset($menu_presets[$r['modules_order']]) ? $menu_presets[$r['modules_order']] : array());
                            ?>

                            <select class="form-control" id="menu_order_presets" style="margin-bottom: 10px;">
                                <option value="">Menù Predefinito</option>
                                <optgroup label="Preset Globali">
                                    <?php
                                    $standard_selected = false;

                                    foreach($menu_presets as $p_id => $preset) {
                                        $selected = ($r['modules_order'] == $p_id ? 'selected' : '');
                                        if($selected != "" && $preset['is_standard'] == "1") {
                                            $standard_selected = true;
                                        }
                                    ?>
                                        <option value="<?= $p_id; ?>" data-name="<?= htmlentities($preset['name']); ?>" data-standard="<?= $preset['is_standard'] ?>" <?= $selected; ?>><?= $preset['name']; ?></option>
                                    <?php } ?>
                                </optgroup>

                                <optgroup label="Personalizzato">
                                    <option value="custom" data-name="<?= SW_NAME; ?>" <?= ($r['modules_order'] == 'custom' ? 'selected' : ''); ?>>Personalizzato per <?= SW_NAME; ?></option>
                                </optgroup>

                                <optgroup label="Crea Nuova">
                                    <option value="new" data-name="">Crea una nuova Configurazione</option>
                                </optgroup>
                            </select>


                            <div id="modules_preset_configuration" style="display: <?= ($current_preset ? 'block' : 'none'); ?>;">

                                <div class="show_on_edit hide_for_custom">
                                    <div class="hr-line-dashed"></div>
                                    <div class="alert alert-warning"><b>Attenzione!</b> Le seguenti modifiche verranno applicate a tutti i siti web che utilizzano il seguente Preset di menù.</div>
                                    <label>Nome Configurazione</label>
                                    <input type="text" id="module_preset_name" class="form-control" placeholder="Il nome della configurazione" value="<?= $current_preset['name']; ?>">
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div id="menu-actions" style="margin-bottom: 15px; text-align: right;">
                                    <button type="button" data-action="delete_preset" class="btn btn-danger btn-outline btn-sm show_on_edit undo_edit pull-left" id="delete_preset" style="display: <?= ($standard_selected ? "none" : "block") ?>;">Elimina Definitivamente</button>
                                    <button type="button" data-action="undo_edit" class="btn btn-warning btn-outline btn-sm show_on_edit undo_edit">Annulla Modifiche</button>
                                    <button type="button" data-action="edit_preconfig" class="btn btn-white btn-sm edit_configuration">Modifica Preconfigurazione</button>
                                </div>

                                <div class="show_on_edit">

                                    <?php foreach((new \MSFramework\users())->getUserLevels() as $level_id => $level_name) { ?>
                                        <div class="ibox role_ibox" data-role="<?= $level_id ?>">
                                            <div class="ibox-title">
                                                <h5>Menù per <b><?= $level_name; ?></b></h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    <a class="fullscreen-link">
                                                        <i class="fa fa-expand"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ibox-content" style="background: #fafafa;">

                                                <div class="nestable-menu" style="margin-bottom: 15px;">
                                                    <button type="button" data-action="expand-all" class="btn btn-white btn-sm">Espandi Tutto</button>
                                                    <button type="button" data-action="collapse-all" class="btn btn-white btn-sm">Riduci Tutto</button>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="ibox">
                                                            <div class="ibox-title">
                                                                <h5>Menù Attuale</h5>
                                                            </div>
                                                            <div class="ibox-content">
                                                                <div class="dd menu_order active_menu user_level_<?= $level_id; ?>" data-role="<?= $level_id; ?>">
                                                                    <ol class="dd-list">

                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="ibox">
                                                            <div class="ibox-title">
                                                                <h5>Voci Inutilizzate</h5>
                                                            </div>
                                                            <div class="ibox-content">
                                                                <div class="dd menu_order unused_menu user_level_<?= $level_id; ?>" data-role="<?= $level_id; ?>">
                                                                    <ol class="dd-list">

                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="ibox">
                                                            <div class="ibox-title">
                                                                <h5>Aggiungi Contenuti</h5>
                                                            </div>
                                                            <div class="ibox-content">
                                                                <div id="add_content_menu_box">
                                                                    <a href="#" class="btn btn-primary btn-outline add_divider">Aggiungi Divisore</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                            <?php } ?>


                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>

<style>
    .menu_order .disabled_in_current .dd-handle {
        background-color: rgba(255, 127, 0, 0.2);
        border-color: rgba(255, 127, 0, 0.2);
    }

    .menu_order .disabled_in_current .dd-handle:after {
        content: 'Funzionalità Extra';
        float: right;
        font-size: 10px;
        opacity: 0.5;
    }
</style>

</body>
</html>
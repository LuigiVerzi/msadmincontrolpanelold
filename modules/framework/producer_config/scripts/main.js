function initForm() {
    initClassicTabsEditForm();

    CSSMode = ace.require("ace/mode/css").Mode;
    JSMode = ace.require("ace/mode/javascript").Mode;

    css_editor_acp = ace.edit("editor_css_acp");
    css_editor_acp.session.setMode(new CSSMode());

    js_editor_acp = ace.edit("editor_js_acp");
    js_editor_acp.session.setMode(new JSMode());

    $('.is_primary').on('click', function() {
        primary_lang = $(this).attr('id').replace("primary", 'lang');

        if($('#' + primary_lang + ":checked").length == "0") {
            $('#' + primary_lang).iCheck('check');
        }
    })

    $('.lang_check').on('ifUnchecked', function(event){
        primary_lang = $(this).attr('id').replace("lang", 'primary');

        $('#' + primary_lang).attr('checked', false)
    });

    $('#gdpr_accept_on_scroll').on('ifToggled', function(event){
        if($(this).is(':checked')){
            $('#gdpr_accept_on_scroll_notifyuser_container').show();
        } else {
            $('#gdpr_accept_on_scroll_notifyuser_container').hide();
            $('#gdpr_accept_on_scroll_notifyuser_container input[type=checkbox]').attr('checked', false);
            $('#gdpr_accept_on_scroll_notifyuser_container input[type=checkbox]').iCheck('uncheck').iCheck('update');
        }
    })

    $('.has_extra_settings').on('ifToggled', function(event){
        module_id = $(this).attr('data-module-id');

        if($(this).is(':checked')){
            $('#module_' + module_id + '_extra_settings').show();
        } else {
            $('#module_' + module_id + '_extra_settings').hide();
            $('#module_' + module_id + '_extra_settings input[type=text]').val('');
            $('#module_' + module_id + '_extra_settings select').val('');
            $('#module_' + module_id + '_extra_settings input[type=checkbox]').attr('checked', false);
            $('#module_' + module_id + '_extra_settings input[type=checkbox]').iCheck('uncheck').iCheck('update');
        }
    });

    $('.email-colorpicker').colorpicker().on('change', generateEmailTemplatePreview).on('changeColor', generateEmailTemplatePreview);

    $("#templatemail_logosize").ionRangeSlider({
        type: 'single',
        min: 100,
        max: 600,
        step: 10,
        from: $("#templatemail_logosize").data('current'),
        postfix: " px",
        onFinish: function(data){
            var value = data.fromNumber;
            $("#templatemail_logosize").data('current', value);
            generateEmailTemplatePreview();
        }
    });

    $('#email_demo_template').on('change', generateEmailTemplatePreview);

    $('#email_template_panel .fullscreen-link').on('click', function () {
        if(!$('#email_template_panel').hasClass('border-bottom')) {
            $('#email_template_panel .collapse-link').click();
        }
    });

    $('.uplSettings_compression_quality').each(function () {
        var $slider = $(this);
        $slider.ionRangeSlider({
            type: 'single',
            min: 10,
            max: 100,
            step: 5,
            from: $(this).data('current'),
            postfix: " %",
            onFinish: function(data){
                var value = data.fromNumber;
                $slider.data('current', value);
                if($slider.closest('.uploaderBox').length) {
                    var $checkbox = $slider.closest('.uploaderBox').find('.uplSettings_enabled');
                    $checkbox.iCheck('check');
                }

            }
        });
    })

    $('.uploaderBox .uplSettings_enabled').on('ifChanged', function (event) {
        if($(this).is(':checked')) {
            $(this).closest('.uploaderBox').removeClass('collapsed');
            $(this).closest('.uploaderBox').find('.uplSettings_compression_quality').ionRangeSlider('update');
        }
        else {
            $(this).closest('.uploaderBox').addClass('collapsed');
        }
    });

    $('#cleanCache, #cleanCacheOnlyCurDomain').on('click', function() {
        cur_id = $(this).attr('id');
        original_value = $('#' + cur_id).html();
        $('#' + cur_id).html($('#' + cur_id).attr('cleaning_txt'));
        $('#' + cur_id).addClass('disabled').attr('disabled', 'disabled');

        $.ajax({
            url: "ajax/cleanCache.php?what=" + cur_id,
            async: false,
            success: function(data) {
                bootbox.alert("Cache svuotata con successo.");
                $('#' + cur_id).html(original_value).removeClass('disabled').attr('disabled', false);
            }
        })
    })

    $('#getCache, #getCacheOnlyCurDomain').on('click', function() {
        cur_id = $(this).attr('id');
        original_value = $('#' + cur_id).html();
        $('#' + cur_id).html($('#' + cur_id).attr('cleaning_txt'));
        $('#' + cur_id).addClass('disabled').attr('disabled', 'disabled');

        $.ajax({
            url: "ajax/getCache.php?what=" + cur_id,
            async: false,
            success: function(data) {
                bootbox.alert({
                    message: data,
                    size: 'large'
                });

                $('#' + cur_id).html(original_value).removeClass('disabled').attr('disabled', false);
            }
        })
    })

    /* JS PRESET MENU */

    $('.menu_order').each(function () {
        var role = $(this).data('role');
        $(this).nestable({
            group: role
        });
    });

    loadMenuPreset();

    $('#add_content_menu_box .add_divider').on('click', function (e) {
       e.preventDefault();

       var titolo = $('<div/>').text(prompt('Inserisci il titolo del divisore')).html();
       if(titolo.length) {
           var html = '<li class="dd-item divider" data-id="divider" data-value="' + titolo + '"><div class="dd-handle"><span class="label label-success"><i class="fa fa-bars"></i></span> <span class="text">[DIVISORE] ' + titolo + '</span> <a class="delete" onclick="$(this).closest(\'li\').remove()">Elimina</a></div></li>';
           $(this).closest('.role_ibox').find('.active_menu > ol').append(html);
       }
    });

    $('#menu-actions').on('click', function (e) {
        var action = $(e.target).data('action');

        if (action === 'edit_preconfig') {
            editPreconfig();
        }
        else if (action === 'undo_edit') {
            $('#modules_preset_configuration .show_on_edit').hide();
            $('#modules_preset_configuration .edit_configuration').show();
        }
        else if (action === 'delete_preset') {
            bootbox.confirm('Elimiando questo preset, verrà impostata la visualizzazione standard su tutti i siti che lo utilizzano. Vuoi proseguire?', function (result) {
                if (result) {
                    $.ajax({
                        url: "ajax/deleteMenuPreset.php",
                        type: "POST",
                        data: {
                            "id": $('#menu_order_presets').val()
                        },
                        async: false,
                        dataType: "text",
                        success: function (data) {
                            bootbox.alert('Preset eliminato correttamente.');

                            var $option =  $('#menu_order_presets').find('option[value="' +  $('#menu_order_presets').val() + '"]');
                            var $optgroup =  $option.closest('optgroup');
                            $option.remove();
                            if(!$optgroup.find('option').length) {
                                $optgroup.remove();
                            }

                            $('#menu_order_presets').val('').change();

                        }
                    });
                }
            });
        }
    });

    $('.nestable-menu').on('click', function (e) {
        var action = $(e.target).data('action');

        var $dd = $(this).closest('.ibox-content')

        if (action === 'expand-all') {
            $dd.nestable('expandAll');
        }
        else if (action === 'collapse-all') {
            $dd.nestable('collapseAll');
        }
    });

    $('#menu_order_presets').on('change', function (e) {
        $('#modules_preset_configuration').hide();
        $('#module_preset_name').val('');

        if($(this).val() == 'new') {
            loadMenuPreset();
        } else if($(this).val() == '') {
            $('#modules_preset_configuration').hide();
        }
        else {
            $('#module_preset_name').val($(this).find('option:selected').data('name'));
            loadMenuPreset();
        }
    });

    generateEmailTemplatePreview();
}

function editPreconfig() {
    $('#modules_preset_configuration .show_on_edit').show();
    $('#modules_preset_configuration .edit_configuration').hide();

    if($('#menu_order_presets').val() == 'custom') {
        $('#modules_preset_configuration .hide_for_custom').hide();
    }
}

function loadMenuPreset() {

    if($('#menu_order_presets').val() == '') return false;

    $.ajax({
        url: "ajax/composeMenuPreset.php",
        type: "POST",
        data: {
            "id": $('#menu_order_presets').val(),
            "loadedModuleID" : $('#loadedModuleID').val()
        },
        async: false,
        dataType: "json",
        success: function (data) {
            $('#modules_preset_configuration').show();

            if($('#menu_order_presets').val() == 'new' || $('#menu_order_presets').val() == 'custom') {
                $('#modules_preset_configuration .show_on_edit').show();
                $('#modules_preset_configuration .edit_configuration').hide();
                $('#modules_preset_configuration .undo_edit').hide();
            }
            else {
                $('#modules_preset_configuration .show_on_edit').hide();
                $('#modules_preset_configuration .edit_configuration').show();
                $('#modules_preset_configuration .undo_edit').hide();
            }

            if($('#menu_order_presets').val() == 'custom') {
                $('#modules_preset_configuration .hide_for_custom').hide();
            }

            $('.menu_order').nestable('destroy');

            Object.keys(data[0]).forEach(function (user_role) {

                var preset = data[0][user_role];


                $('.menu_order.active_menu.user_level_' + user_role).html((preset[0] != "" ? '<ol class="dd-list">' + preset[0] + '</ol>' : ''));
                $('.menu_order.unused_menu.user_level_' + user_role).html((preset[1] != "" ? '<ol class="dd-list">' + preset[1] + '</ol>' : ''));

                // Rimuovo le voci già usate dal menù 'inutilizzati'
                $('.menu_order.unused_menu.user_level_' + user_role + ' .dd-item').each(function () {
                    var id = $(this).data('id');
                    var $li = $(this);
                    var $ol = $(this).closest('ol');

                    if ($('.menu_order.active_menu.user_level_' + user_role).find('.dd-item[data-id="' + id + '"]').length > 0) {
                        if($li.find('li').length) {
                            $li.addClass('todelete');
                        }
                        else {
                            $li.remove();
                        }
                    }

                });

                if(!$('.menu_order.unused_menu.user_level_' + user_role + ' .dd-item').length) {
                    $('.menu_order.unused_menu.user_level_' + user_role).find('ol').remove();
                }

                for (all_deleted = 0; all_deleted == 0;) {
                    $('.menu_order.unused_menu.user_level_' + user_role + ' .dd-item.todelete').each(function () {
                        if(!$(this).find('li')) {
                            $(this).remove();
                        }
                        else {
                            var html = $(this).find(' > ol').html();
                            $(this).after(html);
                            $(this).remove();
                        }
                    });

                    if(!$('.menu_order.unused_menu.user_level_' + user_role + ' .dd-item.todelete').length) {
                        all_deleted = 1;
                    }
                }

            });

            $('.granular_settings_menuicon').on('click', function() {
                openGranularSettings($(this).closest('.dd-item').data('id'), 0, $(this).closest('.role_ibox').data('role'));
            });

            $('.abb_menu_granular').val(data[1]);

            $('.menu_order').nestable('init');
            //editPreconfig();
        }
    });
}

function onStepChangedModule(form_obj, event, currentIndex, priorIndex) {
    if(currentIndex == 1) {
        $('#templatemail_logosize').ionRangeSlider('update');
    }
    else if(currentIndex == 2) {
        $('.uplSettings_compression_quality').ionRangeSlider('update');
    }
}

function generateEmailTemplatePreview() {

    $.ajax({
        url: "ajax/getMailTemplate.php",
        type: "POST",
        data: {
            "colors": [$('#templatemail_maincolor').val(), $('#templatemail_secondarycolor').val(), $('#templatemail_thirdcolor').val(), $('#templatemail_bgcolor').val()],
            "logo_size": $('#templatemail_logosize').data().current,
            "email_demo_template": $('#email_demo_template').val()
        },
        async: false,
        dataType: "html",
        success: function (data) {
            $('#email_template_html').html(data);
            if(!$('#email_template_panel').hasClass('border-bottom')) {
                $('#email_template_panel .collapse-link').click();
            }
        }
    });

}

function ripristinaStatoCheckGDPR() {
    $('.gdpr_cookie_policy_check').each(function() {
        $(this).iCheck('uncheck');

        if($(this).data('should-auto-enable') == "1") {
            $(this).iCheck('indeterminate');
        }
    })
}

function moduleSaveFunction(save_as_new, alert_func_disabled) {
    if(typeof(alert_func_disabled) == "undefined") {
        alert_func_disabled = true;
    }

    avail_langs = new Array();
    
    $('.lang_check:checked').each(function() {
        tmp_avail_langs = new Object();

        cur_id = $(this).attr('id').replace("lang_", '');

        tmp_avail_langs['id'] = cur_id
        if($('#primary_' + cur_id + ":checked").length == "1") {
            tmp_avail_langs['is_primary'] = "1";
        }

        avail_langs.push(tmp_avail_langs);
    });

    avail_modules = new Array();
    $('.enable_module:checked').each(function() {
        avail_modules.push($(this).attr('data-module-id'));
    })

    extra_settings = new Object();
    $('.extra_settings_container').each(function() {
        container_for_module = $(this).attr('data-extra-settings-for-module');
        extra_settings[container_for_module] = new Object();

        if(container_for_module == "onexitpopup") {
            extra_settings[container_for_module]['template'] = $('#' + container_for_module + '_template').val()
        } else if(container_for_module == "gdpr") {
            extra_settings[container_for_module]['auto_enabled_modules'] = new Array();

            $('.module_gdpr_autoenable:checked').each(function(k, v) {
                extra_settings[container_for_module]['auto_enabled_modules'].push($(this).attr('data-gdpr-module-id'));
            })

            force_gdpr_show = [];
            force_gdpr_hide = [];
            $('.gdpr_cookie_policy_check').each(function() {
                if($(this).closest('div.checkbox').find('div:first').hasClass('indeterminate')) {
                    return true;
                }

                if($(this).is(':checked')) {
                    force_gdpr_show.push($(this).attr('id'));
                } else {
                    force_gdpr_hide.push($(this).attr('id'));
                }
            })

            extra_settings[container_for_module]['cookie_policy'] = {
                force_show : force_gdpr_show,
                force_hide : force_gdpr_hide
            }

            extra_settings[container_for_module]['accept_on_scroll'] = {
                enabled : $('#gdpr_accept_on_scroll:checked').length,
                notifyuser : $('#gdpr_accept_on_scroll_notifyuser:checked').length,
            }
        } else if(container_for_module == "multiportal") {
            extra_settings[container_for_module]['gestionalere'] = {
                'agency_id' : $('#' + container_for_module + '_gestionalere_agency_id').val(),
            }
        } else if(container_for_module == "scraper") {
            extra_settings[container_for_module]['type'] = $('#' + container_for_module + '_type').val()
        }
    })

    single_uploader_settings = new Object();
    $('.single_uploader_settings').each(function() {
        uploader_constant = $(this).attr('data-key');
        single_uploader_settings[uploader_constant] = {
            "enabled": $(this).find('.uplSettings_enabled:checked').length,
            "crop_width": $(this).find('.uplSettings_crop_width').val(),
            "crop_height": $(this).find('.uplSettings_crop_height').val(),
            "crop_width_thumb": $(this).find('.uplSettings_crop_width_thumb').val(),
            "crop_height_thumb": $(this).find('.uplSettings_crop_height_thumb').val(),
            "compression_quality": $(this).find('.uplSettings_compression_quality').data().current,
            "apply_watermark": $(this).find('.uplSettings_apply_watermark:checked').length
        };
    })

    module_order = '';
    menu_in_edit_mode = false;
    if($('#menu_order_presets').val() != '') {
        if ($('#menu_order_presets').val() == 'new' || $('#menu_order_presets').val() == 'custom') {

            var menu_presets = {};
            $('.menu_order.active_menu').each(function() {
                var menu_role = $(this).data('role');
                menu_presets[menu_role] = $(this).nestable('serialize')
            });

            module_order = {
                id: $('#menu_order_presets').val(),
                name: $('#module_preset_name').val(),
                preset: menu_presets,
                granular: $('.abb_menu_granular').val()
            };
        } else {
            if($('#modules_preset_configuration .edit_configuration').css('display') == 'none') {
                var menu_presets = {};
                $('.menu_order.active_menu').each(function() {
                    var menu_role = $(this).data('role');
                    menu_presets[menu_role] = $(this).nestable('serialize')
                });

                menu_in_edit_mode = true;
                module_order = {
                    id: $('#menu_order_presets').val(),
                    name: $('#module_preset_name').val(),
                    preset: menu_presets,
                    granular: $('.abb_menu_granular').val()
                };
            }
            else {
                module_order = $('#menu_order_presets').val();
            }
        }

        if($('#menu_order_presets').val() != 'custom' && typeof(module_order) == 'object' && !$('#module_preset_name').val().length) {
            bootbox.error("Non hai assegnato nessun nome alla configurazione del menù");
            return false;
        }

    }

    alert_func_disabled_shown = false;
    alert_messages = new Array();

    if(menu_in_edit_mode && alert_func_disabled) {
        alert_func_disabled_shown = true;
        alert_messages.push('Attenzione! La configurazione del menù può essere utilizzata anche da altri siti, assicurati che sia una soluzione che può andare bene anche per gli altri.');
    }

    if(alert_messages.length) {
        bootbox.confirm(alert_messages.join("<hr>") + ' Vuoi proseguire?', function (result) {
            if (result) {
                moduleSaveFunction(save_as_new, false);
            }
        });
    }

    if(!alert_func_disabled_shown) {
        $.ajax({
            url: "db/saveData.php?&" + $('#must_set_lang_data').val(),
            type: "POST",
            data: {
                "pReCAPTCHAPublic": $('#recaptcha_public').val(),
                "pReCAPTCHAPrivate": $('#recaptcha_private').val(),
                "pReCAPTCHAInvisible": $('#recaptcha_invisible:checked').length,
                "pMailTemplateColors": [$('#templatemail_maincolor').val(), $('#templatemail_secondarycolor').val(), $('#templatemail_thirdcolor').val(), $('#templatemail_bgcolor').val()],
                "pMailTemplateLogoSize": $('#templatemail_logosize').data().current,
                "pGMapsKey": $('#gmaps_apikey').val(),
                "pGPlacesKey": $('#gplaces_apikey').val(),
                "pGGeocodeKey": $('#ggeocode_apikey').val(),
                "pVirtualServerName": $('#virtual_server_name').val(),
                "pFrameworkVersion": {
                    "type": $('#fw_type').val(),
                    "version": $('#fw_version').val(),
                },
                "pMemcachedOn": $('#memcached_on:checked').length,
                "pLanguages": avail_langs,
                "pExtraModules": avail_modules,
                "pExtraModulesSettings": extra_settings,
                "pModulesOrder": module_order,
                "pCSSEditorACP": css_editor_acp.getValue(),
                "pJSEditorACP": js_editor_acp.getValue(),
                "pUploadSettings": {
                    "crop_width": $('#uplSettings_crop_width').val(),
                    "crop_height": $('#uplSettings_crop_height').val(),
                    "crop_width_thumb": $('#uplSettings_crop_width_thumb').val(),
                    "crop_height_thumb": $('#uplSettings_crop_height_thumb').val(),
                    "compression_quality": $('#uplSettings_compression_quality').data().current,
                    "apply_watermark": $('#uplSettings_apply_watermark:checked').length,
                    "single": single_uploader_settings
                }
            },
            async: false,
            dataType: "json",
            success: function (data) {
                if (data.status == "mandatory_data_missing") {
                    bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
                } else if (data.status == "query_error") {
                    bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
                } else if (data.status == "miss_main_lang") {
                    bootbox.error("E' necessario impostare la lingua principale prima di poter salvare!");
                } else if (data.status == "can_not_change_primary") {
                    bootbox.error("E' necessario completare tutte le traduzioni della lingua che si dedisera impostare come primaria prima di poter procedere con questa operazione!");
                } else if (data.status == "ok") {
                    succesModuleSaveFunctionCallback(data);
                }
            }
        })
    }
}
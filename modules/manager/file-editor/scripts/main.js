$(document).ready(function() {
    $('#file_browser').jstree({
        'core': {
            'data': {
                'dataType': 'JSON',
                'url': "ajax/getFilesTree.php",
                'data': function (node) {
                    return {'id': node.id};
                }
            }
        }
    }).on("dblclick.jstree", function (event) {
        var $target = $(event.target).closest("li");
        var id = $target.attr('id');

        if($target.hasClass('jstree-leaf')) {
            loadFileContent(id);
        }

    });

    window.fileEditor = CodeMirror.fromTextArea($('#file_editor_source')[0], {
        lineNumbers: true,
        matchBrackets: true,
        mode: "application/x-httpd-php",
        indentUnit: 4,
        indentWithTabs: true
    });
});

function loadFileContent(id) {
    $.ajax({
        url: "ajax/getFileSource.php",
        type: "POST",
        data: {
            "id": id,
        },
        async: false,
        dataType: "text",
        success: function(html) {
            fileEditor.getDoc().setValue(html);
        }
    });
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pURLFrom": $('#from').val(),
            "pURLTo": $('#to').val(),
            "pRedirectType": $('#type').val(),
            "pIsActive": $('#is_active:checked').length,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "duplicate_from") {
                bootbox.error("Esiste già una regola con questo URL di Origine!");
            }  else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}
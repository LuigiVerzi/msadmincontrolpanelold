<?php
/**
 * MSAdminControlPanel
 * Date: 27/04/19
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

        <div class="row">

            <div class="col-lg-9 col-md-8">
                <textarea id="file_editor_source"></textarea>
            </div>

            <div class="col-lg-3 col-md-4">
                <div id="file_browser" style="font-size: 16px;"></div>
            </div>
        </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>

<style>
    .CodeMirror {
        min-height: 75vh!important;
        height: 75vh!important;
    }
</style>

</html>
<?php
require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$id = $_POST['id'];

$base_dir = CUSTOMER_DOMAIN_INFO['path'];

$full_path = $base_dir . $id;

if(!is_sub_dir($full_path, $base_dir)) {
    die('Dove vai? >.<');
}

$file_content = file_get_contents($full_path);

echo $file_content;

function is_sub_dir($path = NULL, $parent_folder = NULL) {

    if( !$path OR !$parent_folder ) {
        return false;
    }

    $path = str_replace('\\', '/', $path);
    $parent_folder = str_replace('\\', '/', $parent_folder);

    if(strpos($path, '/..') !== false) {
        return false;
    }

    if( strcasecmp($path, $parent_folder) > 0 ) {
        return true;
    }

    return false;
}
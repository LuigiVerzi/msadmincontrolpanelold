<?php
require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$id = $_GET['id'];

$base_dir = CUSTOMER_DOMAIN_INFO['path'];

if($id !== '#') {
    $dir = $base_dir . $id . '/';
} else {
    $dir = $base_dir;
}

if($base_dir !== $dir && !is_sub_dir($dir, $base_dir)) {
    die('Dove vai? >.<');
}

$files_three = array();

$files = scandir($dir);
foreach($files as $key => $value) {

    if(in_array($value, array('uploads', 'i18n', '.', '..'))) {
        continue;
    }

    $path = realpath($dir . $value);

    $files_three[] = array(
        "id" => str_replace(CUSTOMER_DOMAIN_INFO['path'], '', $path),
        "text" => $value,
        "icon" => get_file_icon($value),
        "children" => is_dir($path)
    );
}

die(json_encode($files_three));

function get_file_icon($file) {

    $icon = 'fa fa-folder';

    $ext = explode('.', $file);

    if($ext) {

        switch (end($ext)) {
            case 'php':
                $icon = 'fa fa-code';
                break;
            case 'html':
                $icon = 'fa fa-code';
                break;
        }

    }

    return $icon;

}

function is_sub_dir($path = NULL, $parent_folder = NULL) {

    if( !$path OR !$parent_folder ) {
        return false;
    }

    $path = str_replace('\\', '/', $path);
    $parent_folder = str_replace('\\', '/', $parent_folder);

    if(strpos($path, '/..') !== false) {
        return false;
    }

    if( strcasecmp($path, $parent_folder) > 0 ) {
        return true;
    }

    return false;
}
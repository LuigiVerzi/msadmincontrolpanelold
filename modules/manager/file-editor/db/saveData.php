<?php
/**
 * MSAdminControlPanel
 * Date: 27/04/19
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;
if($_POST['pURLFrom'] == "" || $_POST['pURLTo'] == "" || $_POST['pRedirectType'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if($MSFrameworkDatabase->getCount("SELECT url_from FROM redirect WHERE id != :id AND url_from = :url_from", array(":id" => $_POST['pID'], ":url_from" => $_POST['pURLFrom'])) != 0) {
    echo json_encode(array("status" => "duplicate_from"));
    die();
}

$array_to_save = array(
    "url_from" => $_POST['pURLFrom'],
    "url_to" => $_POST['pURLTo'],
    "redirect_type" => $_POST['pRedirectType'],
    "active" => $_POST['pIsActive'],
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO redirect ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE redirect SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
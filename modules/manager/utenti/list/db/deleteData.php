<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();

$MSSaaSEnvironments = new MSFramework\SaaS\environments();
if($MSFrameworkSaaSBase->isSaaSDomain()) {
    if(in_array($MSSaaSEnvironments->getOwnerID(), $_POST['pID'])) {
        echo json_encode(array("status" => "deleting_saas_owner"));
        die();
    }

    foreach ($MSFrameworkUsers->getUserDetails($_POST['pID']) as $user_id => $user) {
        $MSSaaSEnvironments->unlinkUserFromEnvironment($user_id);
    }
}

foreach($MSFrameworkUsers->getUserDetails($_POST['pID']) as $user_id => $user) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($user['propic'], true));

    if($MSFrameworkDatabase->getCount("SELECT id FROM users WHERE extra_data LIKE :owner", array(":owner" => '%"' . $user_id . '"%')) != 0) { //consulente di riferimento
        echo json_encode(array("status" => "agent_has_assistenti", "id" => $user_id));
        die();
    }

    if($MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili WHERE owner_id = :owner", array(":owner" => $user_id)) != 0) {
        echo json_encode(array("status" => "agent_has_immobili", "id" => $user_id));
        die();
    }

    if($MSFrameworkDatabase->getCount("SELECT id FROM customers WHERE owner_id = :owner", array(":owner" => $user_id)) != 0) {
        echo json_encode(array("status" => "agent_has_customers", "id" => $user_id));
        die();
    }

    if($MSFrameworkDatabase->getCount("SELECT id FROM richieste WHERE owner_id = :owner", array(":owner" => $user_id)) != 0) {
        echo json_encode(array("status" => "agent_has_requests", "id" => $user_id));
        die();
    }
}

if($MSFrameworkDatabase->deleteRow("users", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('PROPIC'))->unlink($images_to_delete);
    $MSFrameworkDatabase->deleteRow("appointments_list", "user", $_POST['pID']);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
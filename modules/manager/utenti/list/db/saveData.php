<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pCognome'] == "" || $_POST['pSlug'] == "" || $_POST['pEmail'] == "" || $_POST['pLivello'] == "" || $_POST['pIsActive'] == "" || ($_POST['pLivello'] == "realestate-6" && $_POST['pConsRif'] == "") || ((new \MSFramework\cms())->checkExtraFunctionsStatus('realestate') && $_POST['pAgenzia'] == "")) {
    $can_save = false;
}

if($db_action == "insert") {
    if($_POST['pPass'] == "" || $_POST['pPassConfirm'] == "") {
        $can_save = false;
    }
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

(new MSFramework\geonames())->checkDataBeforeSave();

$MSSaaSEnvironments = new MSFramework\SaaS\environments();
$isSaas = $MSFrameworkSaaSBase->isSaaSDomain();

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "users", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

if($_POST['pPass'] != $_POST['pPassConfirm']) {
    echo json_encode(array("status" => "password_does_not_match"));
    die();
}

if($_POST['pPass'] != "" && $MSFrameworkUsers->checkPasswordStrength($_POST['pPass']) != "") {
    echo json_encode(array("status" => "password_strength"));
    die();
}

if($db_action == "update") {
    if($_POST['pLivello'] == "realestate-6") {
        if($MSFrameworkDatabase->getCount("SELECT id FROM users WHERE extra_data LIKE :owner", array(":owner" => '%"' . $_POST['pID'] . '"%')) != 0) { //consulente di riferimento
            echo json_encode(array("status" => "agent_has_assistenti"));
            die();
        }
    }
}

$subgroups = array("am_in", "am_out", "pm_in", "pm_out");

$cycle_hours = array();
foreach($_POST['pHoursAry'] as $cur_groupK => $cur_groupV) {
    if($cur_groupK == "default") {
        $cycle_hours[] = $cur_groupV['hours'];
    } else {
        foreach($cur_groupV as $customGroupV) {
            $cycle_hours[] = $customGroupV['hours'];
        }
    }
}

foreach($cycle_hours as $cur_groupmain) {
    foreach($cur_groupmain as $cur_group) {
        foreach($subgroups as $sg) {
            if($cur_group[$sg] != "" && !preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $cur_group[$sg])) {
                echo json_encode(array("status" => "hour_format_not_valid"));
                die();
            }
        }
    }
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkUsers->getUserDataFromDB($_POST['pID'], "propic, mail_auth, enable_appointments, active");
}

$mail_conflict = false;
if($db_action == "insert") {
    if($MSFrameworkUsers->checkIfMailExists($_POST['pEmail']) || ($isSaas && $MSSaaSEnvironments->isAlreadyRegistered($_POST['pEmail']))) {
        $mail_conflict = true;
    }

    if(!filter_var($_POST['pEmail'], FILTER_VALIDATE_EMAIL)) {
        $mail_conflict = true;
    }
}

if($mail_conflict) {
    echo json_encode(array("status" => "mail_not_valid"));
    die();
}

if($_POST['pWebsite'] != "") {
    if(!filter_var($_POST['pWebsite'], FILTER_VALIDATE_URL)) {
        echo json_encode(array("status" => "site_not_valid"));
        die();
    }
}

$uploader = new \MSFramework\uploads('PROPIC');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$send_activation_mail = false;
if($db_action == "update") {
    if($r_old_data['mail_auth'] == "" && $_POST['pMailConfermata'] == "0") {
        $send_activation_mail = true;
    }
} else {
    if($_POST['pMailConfermata'] == "0") {
        $send_activation_mail = true;
    }
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "cognome" => $_POST['pCognome'],
    "ruolo" => $_POST['pLivello'],
    "email" => $_POST['pEmail'],
    "cellulare" => $_POST['pCell'],
    "cellulare_2" => $_POST['pCell2'],
    "website" => $_POST['pWebsite'],
    "fb_profile" => $_POST['pFBProfile'],
    "instagram_profile" => $_POST['pInstagramProfile'],
    "twitter_profile" => $_POST['pTwitterProfile'],
    "linkedin_profile" => $_POST['pLinkedinProfile'],
    "googplus_profile" => $_POST['pGoogPlusProfile'],
    "biografia" => $_POST['pBiografia'],
    "password" => password_hash($_POST['pPass'], PASSWORD_DEFAULT),
    "active" => $_POST['pIsActive'],
    "enable_appointments" => $_POST['pEnableAppointments'],
    "mail_auth" => sha1($_POST['pEmail'] . time()),
    "propic" => json_encode($ary_files),
    "work_hours" => json_encode($_POST['pHoursAry']),
    "limit_services" => json_encode($_POST['pServiceLimit']),
    "slug" => $_POST['pSlug'],
    "agency" => $_POST['pAgenzia'],
    "show_on_website" => $_POST['pShowOnWebsite'],
    "geo_data" => json_encode($_POST['pGeoData']),
    "extra_data" => json_encode(
        array(
            "cons_rif" => $_POST['pConsRif']
        )
    ),
);

$isSaaSOwner = ($isSaas && $MSSaaSEnvironments->getOwnerID() == $_POST['pID']);
if($isSaaSOwner) {
    unset($array_to_save['ruolo']);
}

if($_POST['pMailConfermata'] == "1") {
    $array_to_save['mail_auth'] = "";
}

if($db_action == "update") {
    unset($array_to_save['email']);

    if(!$send_activation_mail && $_POST['pMailConfermata'] == "0") {
        unset($array_to_save['mail_auth']);
    }

    if($_POST['pPass'] == "") {
        unset($array_to_save['password']);
    }
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO users ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $userID = $MSFrameworkDatabase->lastInsertId();

    if($isSaas) {
        $userlink = $MSSaaSEnvironments->linkUserToEnvironment($userID);
        if($userlink === false) {
            echo json_encode(array("status" => "saas_already_registered"));
            die();
        }
    }
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE users SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $userID = $_POST['pID'];
}

if($isSaas && $isSaaSOwner) {
    (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))->registerUser($_POST['pNome'], $_POST['pCognome'], $_POST['pEmail'], '', $_POST['pCell'], "8");
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['propic'], true));

    if($r_old_data['enable_appointments'] != "0" && $_POST['pEnableAppointments'] == "0") {
        $MSFrameworkDatabase->deleteRow("appointments_list", "user", $_POST['pID']);
    }
}

if($send_activation_mail) {
    $oth = ($isSaas ? array("env_id" => $MSSaaSEnvironments->getCurrentEnvironmentID()) : array());

    (new \MSFramework\emails())->sendMail('account-activation', array_merge($oth, array("nome" => $array_to_save['nome'], "email" => $_POST['pEmail'], "subject" => "Conferma account", "code" => $array_to_save['mail_auth'])));
    echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $userID : ''), "email" => "must_confirm"));
    die();
}

/* CERCA DI OTTENERE IL GRAVATAR DEL CLIENTE SE NON HA UN AVATAR IMPOSTATO */
if($db_action == 'insert' && !count($ary_files)) {
    (new \MSFramework\users())->setGravatarIfExist($userID);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $userID : '')));
die();
?>

<?php
/**
 * RealBox
 * Date: 24/09/17
 */

require_once('../../../../../../sw-config.php');
require('../../config/moduleConfig.php');

if($_GET['reassign_to'] != "") {
    $MSFrameworkDatabase->pushToDB("UPDATE customers SET owner_id = :new_owner WHERE owner_id = :old_owner", array(":new_owner" => $_GET['reassign_to'], ":old_owner" => $_GET['delete_user']));
} else {
    foreach($MSFrameworkDatabase->getAssoc("SELECT id FROM customers WHERE owner_id = :owner_id", array(":owner_id" => $_GET['delete_user'])) as $customer) {
        $MSFrameworkDatabase->deleteRow("richieste", "customer", $customer['id']);
    }

    $MSFrameworkDatabase->deleteRow("customers", "owner_id", $_GET['delete_user']);
}
?>



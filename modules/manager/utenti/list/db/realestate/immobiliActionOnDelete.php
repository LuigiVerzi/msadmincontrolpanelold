<?php
/**
 * RealBox
 * Date: 10/09/17
 */

require_once('../../../../../../sw-config.php');
require('../../config/moduleConfig.php');

if($_GET['reassign_to'] != "") {
    $MSFrameworkDatabase->pushToDB("UPDATE realestate_immobili SET owner_id = :new_owner WHERE owner_id = :old_owner", array(":new_owner" => $_GET['reassign_to'], ":old_owner" => $_GET['delete_user']));
} else {
    foreach($MSFrameworkDatabase->getAssoc("SELECT images, images_plan FROM realestate_immobili WHERE owner_id = :owner", array(":owner" => $_GET['delete_user'])) as $immobile) {

        foreach(json_decode($immobile['images'], true) as $file) {
            if($file == "") {
                continue;
            }

            unlink(UPLOAD_REALESTATE_IMMOBILI_FOR_DOMAIN . $file);
        }

        foreach(json_decode($immobile['images_plan'], true) as $file) {
            if($file == "") {
                continue;
            }

            unlink(UPLOAD_REALESTATE_PLANIMETRIE_FOR_DOMAIN . $file);
        }

    }

    $MSFrameworkDatabase->deleteRow("realestate_immobili", "owner_id", $_GET['delete_user']);
}
?>



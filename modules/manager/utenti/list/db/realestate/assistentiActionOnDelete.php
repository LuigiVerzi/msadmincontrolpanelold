<?php
/**
 * RealBox
 * Date: 10/09/17
 */

require_once('../../../../../../sw-config.php');
require('../../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT id FROM users WHERE extra_data LIKE :owner", array(":owner" => '%"' . $_GET['delete_user'] . '"%')) as $r) {
    $extra_data = json_decode($r['extra_data'], true);
    if($_GET['reassign_to'] != "") {
        if ($extra_data['cons_rif'] == $_GET['delete_user']) {
            $extra_data['cons_rif'] = $_GET['reassign_to'];
            $MSFrameworkDatabase->pushToDB("UPDATE users SET extra_data = :extra WHERE id = :id", array(":extra" => json_encode($extra_data), ":id" => $r['id']));
        }
    } else {
        foreach ($MSFrameworkDatabase->getAssoc("SELECT images, images_plan FROM realestate_immobili WHERE owner_id = :owner", array(":owner" => $r['id'])) as $immobile) {

            foreach (json_decode($immobile['images'], true) as $file) {
                if ($file == "") {
                    continue;
                }

                unlink(UPLOAD_REALESTATE_IMMOBILI_FOR_DOMAIN . $file);
            }

            foreach (json_decode($immobile['images_plan'], true) as $file) {
                if ($file == "") {
                    continue;
                }

                unlink(UPLOAD_REALESTATE_PLANIMETRIE_FOR_DOMAIN . $file);
            }

        }

        $MSFrameworkDatabase->deleteRow("users", "id", $assistente['id']);
        $MSFrameworkDatabase->deleteRow("realestate_immobili", "owner_id", $assistente['id']);
    }
}
?>
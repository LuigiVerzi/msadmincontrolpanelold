<?php
/**
 * RealBox
 * Date: 30/09/17
 */

require_once('../../../../../../sw-config.php');
require('../../config/moduleConfig.php');

if($_GET['reassign_to'] != "") {
    $MSFrameworkDatabase->pushToDB("UPDATE richieste SET owner_id = :new_owner WHERE owner_id = :old_owner", array(":new_owner" => $_GET['reassign_to'], ":old_owner" => $_GET['delete_user']));
} else {
    $MSFrameworkDatabase->deleteRow("richieste", "owner_id", $_GET['delete_user']);
}
?>



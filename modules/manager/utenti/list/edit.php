<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkUsers->getUserDataFromDB($_GET['id']);
}

$MSFrameworkGeonames = (new \MSFramework\geonames());

$listaSedi = (new \MSFramework\Attivita\sedi())->getSedeDetails('', 'id, nome');
$realEstateEnabled = $MSFrameworkCMS->checkExtraFunctionsStatus('realestate');

$hoursGlobal = json_decode($r['work_hours'], true);
$timestamp = strtotime('next Monday');

$MSSaaSEnvironments = new MSFramework\SaaS\environments();
$isSaaSOwner = ($MSFrameworkSaaSBase->isSaaSDomain() && $MSSaaSEnvironments->getOwnerID() == $_GET['id']);
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Account</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nome*</label>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($r['nome']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Cognome*</label>
                                    <input id="cognome" name="cognome" type="text" class="form-control required" value="<?php echo htmlentities($r['cognome']) ?>">
                                </div>

                                <?php
                                $email_disabled = "";
                                if($r['email'] != "") {
                                    $email_disabled = "disabled";
                                }
                                ?>

                                <div class="col-sm-3">
                                    <label>Email*</label>
                                    <input id="email" name="email" type="text" class="form-control required email <?php echo $email_disabled ?>" value="<?php echo htmlentities($r['email']) ?>" <?php echo $email_disabled ?>>
                                </div>

                                <div class="col-sm-3">
                                    <label>Sito Web</label>
                                    <input id="website" name="website" type="text" class="form-control" value="<?php echo htmlentities($r['website']) ?>">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <label>Slug*</label>
                                <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>">
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">


                                <div class="col-sm-3" style="display: <?= ($realEstateEnabled || count($listaSedi) ? 'block' : 'none'); ?>;">
                                    <label><?= ($realEstateEnabled ? "Agenzia" : "Sede") ?></label>
                                    <select id="agenzia" name="agenzia" class="form-control <?= ($realEstateEnabled ? "required" : "") ?>">
                                        <option value=""></option>
                                        <?php foreach((new \MSFramework\Attivita\sedi())->getSedeDetails('', 'id, nome') as $sede) { ?>
                                            <option value="<?= $sede['id'] ?>" <?= ($sede['id'] == $r['agency'] ? "selected" : "") ?>><?= $MSFrameworki18n->getFieldValue($sede['nome']) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Cellulare</label>
                                    <input id="cellulare" name="cellulare" type="text" class="form-control" value="<?php echo htmlentities($r['cellulare']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Cellulare Alternativo</label>
                                    <input id="cellulare_2" name="cellulare_2" type="text" class="form-control" value="<?php echo htmlentities($r['cellulare_2']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php require(ABSOLUTE_SW_PATH . "includes/template/geodata/geodata.php"); ?>

                            <div class="row">
                                <div class="col-sm-9">
                                    <label>Bio</label>
                                    <textarea id="biografia" name="biografia" class="form-control" rows="4"><?php echo htmlentities($r['biografia']) ?></textarea>
                                </div>

                                <?php
                                $show_on_website_check = "";
                                if($r['show_on_website'] == "1" || $_GET['id'] == "") {
                                    $show_on_website_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="show_on_website" <?php echo $show_on_website_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Mostra sul Sito</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12" style="margin-bottom: 40px;">
                                    <label>Foto utente</label>
                                    <?php
                                    (new \MSFramework\uploads('PROPIC'))->initUploaderHTML("images", $r['propic']);
                                    ?>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Social</h1>
                        <fieldset>
                            <h2 class="title-divider">Profili Social</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Facebook</label>
                                    <input id="fb_profile" name="fb_profile" type="text" class="form-control" value="<?php echo htmlentities($r['fb_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Instagram</label>
                                    <input id="instagram_profile" name="instagram_profile" type="text" class="form-control" value="<?php echo htmlentities($r['instagram_profile']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Twitter</label>
                                    <input id="twitter_profile" name="twitter_profile" type="text" class="form-control" value="<?php echo htmlentities($r['twitter_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Linkedin</label>
                                    <input id="linkedin_profile" name="linkedin_profile" type="text" class="form-control" value="<?php echo htmlentities($r['linkedin_profile']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Google Plus</label>
                                    <input id="googplus_profile" name="googplus_profile" type="text" class="form-control" value="<?php echo htmlentities($r['googplus_profile']) ?>">
                                </div>
                            </div>

                        </fieldset>
                                
                        <h1>Accesso</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Accesso</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Livello*</label> <?= ($isSaaSOwner ? '<span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Non è possibile modificare il livello del proprietario dell\'abbonamento."></i></span>' : '') ?>
                                    <select id="livello" name="livello" class="form-control required" <?= ($isSaaSOwner ? "disabled" : "") ?>>
                                        <option value=""></option>
                                        <optgroup label="Ruoli Standard">
                                        <?php
                                        foreach($MSFrameworkUsers->getUserLevels() as $levelK => $levelV) {
                                        ?>
                                            <option value="<?php echo $levelK ?>" <?php if($r['ruolo'] == $levelK) { echo "selected"; } ?>><?php echo $levelV ?></option>
                                        <?php
                                        }
                                        ?>
                                        </optgroup>
                                        <optgroup label="Ruoli Personalizzati">
                                            <?php
                                            foreach($MSFrameworkUsers->getCustomUserLevels() as $levelK => $levelV) {
                                                ?>
                                                <option value="custom-<?php echo $levelV['id'] ?>" <?php if($r['ruolo'] == 'custom-' . $levelV['id']) { echo "selected"; } ?>><?php echo $levelV['name'] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </optgroup>
                                    </select>
                                </div>

                                <?php
                                $is_active_check = "checked";
                                if($r['active'] == 0 && $_GET['id'] != "") {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Utente Attivo</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $is_mail_check = "";
                                if($r['mail_auth'] == "" && $_GET['id'] != "") {
                                    $is_mail_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="mail_active" <?php echo $is_mail_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Mail Confermata</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6" id="cons_rif_container" style="display: <?= ($r['ruolo'] != "realestate-6" ? 'none' : 'block') ?>;">
                                    <label>Consulente di Riferimento*</label>
                                    <select id="consulente_riferimento" name="consulente_riferimento" class="form-control required">
                                        <option value=""></option>
                                        <?php
                                        $extra_data = json_decode($r['extra_data'], true);
                                        foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, ruolo FROM users WHERE ruolo != 'realestate-6' AND id != :id", array(":id" => $_GET['id'])) as $cons_rif) {
                                            ?>
                                            <option value="<?php echo $cons_rif['id'] ?>" <?php if($extra_data['cons_rif'] == $cons_rif['id']) { echo "selected"; } ?>><?php echo $cons_rif['cognome'] ?> <?php echo $cons_rif['nome'] ?> (<?php echo $MSFrameworkUsers->getUserLevels($cons_rif['ruolo']) ?>)</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <?php
                            if($_GET['id'] != "") {
                                $pass_asterisk = "";
                                $pass_helper = "Lasciare vuoto per non modificare";
                                $pass_required = "";
                            } else {
                                $pass_asterisk = "*";
                                $pass_helper = "";
                                $pass_required = "required";
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Password<?php echo $pass_asterisk ?></label>
                                    <input id="password" name="password" type="password" class="form-control <?php echo $pass_required ?>">
                                    <span class="help-block m-b-none"><?php echo $pass_helper ?></span>
                                </div>

                                <div class="col-sm-6">
                                    <label>Conferma Password<?php echo $pass_asterisk ?></label>
                                    <input id="password_confirm" name="password_confirm" type="password" class="form-control <?php echo $pass_required ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                        <?php
                        if($MSFrameworkCMS->checkExtraFunctionsStatus('working_hours')) {
                        ?>
                        <h1>Orari di Lavoro</h1>
                        <fieldset>
                            <div class="alert alert-info">
                                Lasciare i campi inizio/fine vuoti durante i periodi di non lavoro
                            </div>

                            <div class="row m-b-md">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-primary" id="addNewGroup">Aggiungi nuovo periodo</button>
                                </div>
                            </div>

                            <?php
                            if(!is_array($hoursGlobal['default'])) {
                                $hoursGlobal['default'] = array();
                            }

                            foreach($hoursGlobal as $hoursGroupType => $hoursGroupTMP) {
                                if($hoursGroupType == "default") {
                                    $fakeHoursGroup = array($hoursGroupTMP);
                                } else {
                                    $fakeHoursGroup = $hoursGroupTMP;
                                }

                                foreach($fakeHoursGroup as $hoursGroup) {
                                    ?>
                                    <div class="row <?php if ($hoursGroupType == "default") {
                                        echo 'default_settings';
                                    } ?> date_group">
                                        <div class="col-lg-12">
                                            <div class="ibox collapsed">
                                                <div class="ibox-title">
                                                    <h5><?= ($hoursGroupType == "default" ? "Impostazioni di default (per periodi di lavoro non impostati)" : "Periodo di lavoro dal " . $hoursGroup['range']['from'] . " al " . $hoursGroup['range']['to']) ?></h5>
                                                    <div class="ibox-tools">
                                                        <a class="collapse-link">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="ibox-content">

                                                    <div class="row period_selection" style="display: <?= ($hoursGroupType == "default" ? "none" : "block") ?>;">
                                                        <div class="col-sm-3">
                                                            <label>Periodo dal</label>
                                                            <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control period_from" value="<?= $hoursGroup['range']['from'] ?>">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3">
                                                            <label>Periodo al</label>
                                                            <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control period_to" value="<?= $hoursGroup['range']['to'] ?>">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6 text-right removeGroupContainer" style="display: <?= ($hoursGroupType == "default" ? "none" : "block") ?>;">
                                                            <div> &nbsp;</div>
                                                            <button type="button" class="btn btn-danger removeGroup">Rimuovi periodo</button>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    $hours = $hoursGroup['hours'];

                                                    for ($i = 0; $i < 7; $i++) {
                                                        $day_name = ucfirst(strftime('%A', $timestamp));
                                                        $timestamp = strtotime('+1 day', $timestamp);
                                                        ?>
                                                        <div class="row day_block">
                                                            <div class="col-sm-12">
                                                                <h3><?= $day_name ?></h3>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <label> </label>
                                                                        <select class="form-control row_type">
                                                                            <option value="">Utilizza impostazioni da orari negozio</option>
                                                                            <option value="custom" <?= ($hours[$i]['row_type'] == "custom" ? "selected" : "") ?>>
                                                                                Personalizza orari di lavoro
                                                                            </option>
                                                                            <option value="dayoff" <?= ($hours[$i]['row_type'] == "dayoff" ? "selected" : "") ?>>
                                                                                Giorno di riposo/festivo
                                                                            </option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="custom_container"
                                                                         style="display: <?= ($hours[$i]['row_type'] == "custom" ? "block" : "none") ?>">
                                                                        <div class="col-sm-4"
                                                                             style="padding: 10px 30px 10px 60px;">
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <span>Inizio AM</span>
                                                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                                                        <input type="text" class="form-control am_in" value="<?= $hours[$i]['am_in'] ?>">
                                                                                        <span class="input-group-addon">
                                                                                            <span class="fa fa-clock-o"></span>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-sm-6">
                                                                                    <span>Fine AM</span>
                                                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                                                        <input type="text" class="form-control am_out" value="<?= $hours[$i]['am_out'] ?>">
                                                                                        <span class="input-group-addon">
                                                                                            <span class="fa fa-clock-o"></span>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-4"
                                                                             style="padding: 10px 60px 10px 30px;">
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <span>Inizio PM</span>
                                                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                                                        <input type="text" class="form-control pm_in" value="<?= $hours[$i]['pm_in'] ?>">
                                                                                        <span class="input-group-addon">
                                                                                            <span class="fa fa-clock-o"></span>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-sm-6">
                                                                                    <span>Fine PM</span>
                                                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                                                        <input type="text" class="form-control pm_out" value="<?= $hours[$i]['pm_out'] ?>">
                                                                                        <span class="input-group-addon">
                                                                                            <span class="fa fa-clock-o"></span>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="hr-line-dashed"></div>

                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                }
                            }
                            ?>
                        </fieldset>
                        <?php } ?>

                        <?php
                        if($MSFrameworkCMS->checkExtraFunctionsStatus('appointments')) {
                        ?>
                        <h1>Appuntamenti</h1>
                        <fieldset>
                            <h2 class="title-divider">Impostazioni Appuntamenti</h2>
                            <div class="row">
                                <?php
                                $enable_appointments_check = "checked";
                                if($r['enable_appointments'] == 0 && $_GET['id'] != "") {
                                    $enable_appointments_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="hidden" id="original_enable_appointments_status" value="<?php echo $r['enable_appointments'] ?>">
                                                <input type="checkbox" id="enable_appointments" <?php echo $enable_appointments_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Appuntamenti</span>
                                        </label>
                                    </div>
                                </div>

                                <?php
                                $limit_services = json_decode($r['limit_services'], true);
                                ?>
                                <div class="col-sm-3">
                                    <label>Limita servizi</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se si desidera limitare i tipi di servizi per i quali l'utente può prendere un appuntamento, impostare la tipologia di limitazione di seguito"></i></span>
                                    <select class="form-control" id="services_limit_type">
                                        <option value="0" <?= ($limit_services['type'] == "0") ? "selected" : "" ?>>Non limitare l'accesso ai servizi</option>
                                        <option value="1" <?= ($limit_services['type'] == "1") ? "selected" : "" ?>>Consenti all'utente di utilizzare solo i servizi selezionati</option>
                                        <option value="2" <?= ($limit_services['type'] == "2") ? "selected" : "" ?>>Consenti all'utente di utilizzare tutti i servizi tranne quelli selezionati</option>
                                    </select>
                                </div>

                                <div class="col-sm-6" id="services_list_container" style="display: <?= ($limit_services['type'] == "1" || $limit_services['type'] == "2") ? "block" : "none" ?>">
                                    <label>Seleziona servizi</label>
                                    <select class="form-control" id="services_limit" multiple="multiple">
                                        <?php
                                        foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome FROM servizi ORDER BY nome") as $r_service) {
                                        ?>
                                            <option value="<?= $r_service['id'] ?>" <?= (in_array($r_service['id'], $limit_services['values'])) ? "selected" : "" ?>><?= $MSFrameworki18n->getFieldValue($r_service['nome'], true) ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>
                        <?php } ?>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
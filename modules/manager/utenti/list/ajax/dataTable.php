<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM users") as $r) {

    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
    if(json_decode($r['propic'])) {
        $avatar_url = UPLOAD_PROPIC_FOR_DOMAIN_HTML . json_decode($r['propic'], true)[0];
    }

    $array['data'][] = array(
        '<img src="' . $avatar_url . '" width="80">',
        $r['nome'],
        $r['cognome'],
        $MSFrameworkUsers->getUserLevels($r['ruolo']),
        $r['email'],
        ($r['mail_auth'] == "") ? "Si" : "No",
        ($r['active'] == "1") ? "Si" : "No",
        ($r['show_on_website'] == "1") ? "Si" : "No",
        "DT_RowId" => $r['id'],
        "DT_FormattedData" => $MSFrameworkUsers->getUserPreviewHTML($r)
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

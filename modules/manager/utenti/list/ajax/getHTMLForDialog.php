<?php
/**
 * RealBox
 * Date: 10/09/17
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');
?>
<input type="hidden" id="diag_action" value="<?php echo $_GET['action'] ?>" />
<?php
if($_GET['w'] == "agent_has_immobili") {
$btn_riassegna = "";
$msg = "Questo agente è il proprietario di uno o più immobili. <br />";

if($MSFrameworkDatabase->getCount("SELECT id FROM users WHERE ruolo != 'realestate-6' AND id != :id", array(":id" => $_GET['id'])) == 0) {
    $msg .= "Non sono presenti agenti a cui poter riassegnare i suoi immobili. E' necessario eliminare gli immobili di proprietà di quest'agente prima di poter procedere";
} else {
    $msg .= "E' necessario riassegnare i suoi immobili ad un altro agente o eliminarli prima di poter procedere <br /><br />";

    $msg .= '<label>Riassegna a...</label><select id="riassegna_a" name="riassegna_a" class="form-control"><option value=""></option>';
    foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, ruolo FROM users WHERE id != :id", array(":id" => $_GET['id'])) as $user) {
       $msg .= '<option value="' . $user['id'] . '">' . $user['cognome'] . ' ' . $user['nome'] . ' (' . $MSFrameworkUsers->getUserLevels($user['ruolo']) . ')</option>';
    }
    $msg .= '</select>';

    $btn_riassegna = '<a class="btn btn-primary" onclick="immobiliActionOnDelete(\'riassegna\', \'' . $_GET['id'] . '\');">Riassegna immobili</a>';
}

$msg .= "<br /><br />Come si desidera procedere?";
?>

<div class="mb-20"> <?php echo $msg ?> </div>

<div class="modal-footer" style="padding-right: 0; padding-left: 0; margin-top: 20px;">
    <a class="btn btn-default" onclick="closeImmobiliAction()">Annulla</a> <?php echo $btn_riassegna ?> <a class="btn btn-primary" onclick="immobiliActionOnDelete('elimina', '<?php echo $_GET['id'] ?>');">Elimina immobili</a>
</div>
<?php
} else if($_GET['w'] == "agent_has_assistenti") {
    $btn_riassegna = "";
    $msg = "Questo agente è il riferimento di uno o più assistenti. <br />";

    if($MSFrameworkDatabase->getCount("SELECT id FROM users WHERE ruolo != 'realestate-6' AND id != :id", array(":id" => $_GET['id'])) == 0) {
        $msg .= "Non sono presenti agenti a cui poter riassegnare i suoi assistenti. E' necessario eliminare gli assistenti che fanno capo a quest'agente prima di poter procedere. Saranno eliminati anche gli immobili relativi a tali assistenti.";
    } else {
        $msg .= "E' necessario riassegnare gli assistenti ad un altro agente o eliminarli prima di poter procedere. In caso di eliminazione, saranno eliminati anche gli immobili relativi agli assistenti. <br /><br />";

        $msg .= '<label>Riassegna a...</label><select id="riassegna_a" name="riassegna_a" class="form-control"><option value=""></option>';
        foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, ruolo FROM users WHERE ruolo != 'realestate-6' AND id != :id", array(":id" => $_GET['id'])) as $user) {
            $msg .= '<option value="' . $user['id'] . '">' . $user['cognome'] . ' ' . $user['nome'] . ' (' . $MSFrameworkUsers->getUserLevels($user['ruolo']) . ')</option>';
        }
        $msg .= '</select>';

        $btn_riassegna = '<a class="btn btn-primary" onclick="assistentiActionOnDelete(\'riassegna\', \'' . $_GET['id'] . '\');">Riassegna assistenti</a>';
    }

    $msg .= "<br /><br />Come si desidera procedere?";
    ?>

    <div class="mb-20"> <?php echo $msg ?> </div>

    <div class="modal-footer" style="padding-right: 0; padding-left: 0; margin-top: 20px;">
        <a class="btn btn-default" onclick="closeImmobiliAction()">Annulla</a> <?php echo $btn_riassegna ?> <a class="btn btn-primary" onclick="assistentiActionOnDelete('elimina', '<?php echo $_GET['id'] ?>');">Elimina assistenti</a>
    </div>
<?php
} else if($_GET['w'] == "agent_has_customers") {
$btn_riassegna = "";
$msg = "Questo agente è il riferimento di uno o più clienti. <br />";

if($MSFrameworkDatabase->getCount("SELECT id FROM users WHERE id != :id", array(":id" => $_GET['id'])) == 0) {
    $msg .= "Non sono presenti agenti a cui poter riassegnare i suoi clienti. E' necessario eliminare i clienti che fanno capo a quest'agente prima di poter procedere. Eliminando i clienti, saranno eliminate anche tutte le richieste effettuate da tali clienti.";
} else {
    $msg .= "E' necessario riassegnare i clienti ad un altro agente o eliminarli prima di poter procedere. <br /><br />";

    $msg .= '<label>Riassegna a...</label><select id="riassegna_a" name="riassegna_a" class="form-control"><option value=""></option>';
    foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, ruolo FROM users WHERE id != :id", array(":id" => $_GET['id'])) as $user) {
        $msg .= '<option value="' . $user['id'] . '">' . $user['cognome'] . ' ' . $user['nome'] . ' (' . $MSFrameworkUsers->getUserLevels($user['ruolo']) . ')</option>';
    }
    $msg .= '</select>';

    $btn_riassegna = '<a class="btn btn-primary" onclick="clientiActionOnDelete(\'riassegna\', \'' . $_GET['id'] . '\');">Riassegna assistenti</a>';
}

$msg .= "<br /><br />Come si desidera procedere?";
?>

<div class="mb-20"> <?php echo $msg ?> </div>

<div class="modal-footer" style="padding-right: 0; padding-left: 0; margin-top: 20px;">
    <a class="btn btn-default" onclick="closeImmobiliAction()">Annulla</a> <?php echo $btn_riassegna ?> <a class="btn btn-primary" onclick="clientiActionOnDelete('elimina', '<?php echo $_GET['id'] ?>');">Elimina clienti</a>
</div>
<?php
} else if($_GET['w'] == "agent_has_requests") {
    $btn_riassegna = "";
    $msg = "Questo agente è il riferimento di una o più richieste. <br />";

    if($MSFrameworkDatabase->getCount("SELECT id FROM users WHERE id != :id", array(":id" => $_GET['id'])) == 0) {
        $msg .= "Non sono presenti agenti a cui poter riassegnare le sue richieste. E' necessario eliminare le richieste che fanno capo a quest'agente prima di poter procedere.";
    } else {
        $msg .= "E' necessario riassegnare le richieste ad un altro agente o eliminarle prima di poter procedere. <br /><br />";

        $msg .= '<label>Riassegna a...</label><select id="riassegna_a" name="riassegna_a" class="form-control"><option value=""></option>';
        foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, ruolo FROM users WHERE id != :id", array(":id" => $_GET['id'])) as $user) {
            $msg .= '<option value="' . $user['id'] . '">' . $user['cognome'] . ' ' . $user['nome'] . ' (' . $MSFrameworkUsers->getUserLevels($user['ruolo']) . ')</option>';
        }
        $msg .= '</select>';

        $btn_riassegna = '<a class="btn btn-primary" onclick="richiesteActionOnDelete(\'riassegna\', \'' . $_GET['id'] . '\');">Riassegna assistenti</a>';
    }

    $msg .= "<br /><br />Come si desidera procedere?";
    ?>

    <div class="mb-20"> <?php echo $msg ?> </div>

    <div class="modal-footer" style="padding-right: 0; padding-left: 0; margin-top: 20px;">
        <a class="btn btn-default" onclick="closeImmobiliAction()">Annulla</a> <?php echo $btn_riassegna ?> <a class="btn btn-primary" onclick="richiesteActionOnDelete('elimina', '<?php echo $_GET['id'] ?>');">Elimina richieste</a>
    </div>
    <?php
}
?>
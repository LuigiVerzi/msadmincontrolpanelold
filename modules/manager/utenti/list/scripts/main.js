$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker(['nome', 'cognome'], 'slug', 'users', $('#record_id'));

    initOrak('images', 1, 0, 0, 100, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
    initHourGroups();

    $('#livello').on('change', function () {
        if($(this).val() == "realestate-6") {
            $('#cons_rif_container').show();
            $('#consulente_riferimento').val('');
        } else {
            $('#cons_rif_container').hide();
        }
    })

    $('#addNewGroup').on('click', function() {
        base_form = $('.default_settings:first').wrap('<p/>').parent().html();
        $('.default_settings:first').unwrap();

        new_added_form = $('.default_settings').before(base_form);
        $(new_added_form).removeClass('default_settings');
        $(new_added_form).find('.ibox-title h5').html('Nuovo periodo di lavoro (selezionare le date)');
        $(new_added_form).find('select, input').val('');
        $(new_added_form).find('.period_selection').show();
        $(new_added_form).find('.removeGroupContainer').show();
        $(new_added_form).find('.row_type').trigger('change');

        initHourGroups();

        $('.collapse-link').unbind('click');
        $('.collapse-link').on('click', function (e) {
            e.preventDefault();
            var ibox = $(this).closest('div.ibox');
            var button = $(this).find('i');
            var content = ibox.children('.ibox-content');
            content.slideToggle(200);
            button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
            ibox.toggleClass('').toggleClass('border-bottom');
            setTimeout(function () {
                ibox.resize();
                ibox.find('[id^=map-]').resize();
            }, 50);
        });

        $('.date_group').find('.fa-chevron-down').parent('.collapse-link').trigger('click');
        $(new_added_form).find('.collapse-link').trigger('click');
    })

    $('#services_limit_type').on('change', function() {
        if($(this).val() == "0") {
            $('#services_list_container').hide();
            $('#services_limit').val('');
        } else {
            $('#services_list_container').show();
        }
    })

    manageWorldDataSelects();
}

function initHourGroups() {
    $('.clockpicker').clockpicker();

    $('.period_from, .period_to').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it"
    });

    $('.period_from, .period_to').unbind('change');
    $('.period_from, .period_to').on('change', function() {
        ibox = $(this).closest('.ibox');
        period_from = $(ibox).find('.period_from').val();
        period_to = $(ibox).find('.period_to').val();

        if(period_from != "" && period_to != "") {
            $(ibox).find('.ibox-title').find('h5').html('Periodo di lavoro dal ' + period_from + ' al ' + period_to);
        } else {
            $(ibox).find('.ibox-title').find('h5').html('Nuovo periodo di lavoro (selezionare le date)');
        }
    });

    $('.removeGroup').unbind('click');
    $('.removeGroup').on('click', function() {
        $(this).closest('.date_group').remove();
    })

    $('.row_type').unbind('change');
    $('.row_type').on('change', function() {
        index = $('.row_type').index($(this));

        if($(this).val() == "custom") {
            $('.custom_container:eq(' + index + ')').show();
        } else {
            $('.custom_container:eq(' + index + ')').hide();
            $('.custom_container:eq(' + index + ') .clockpicker input').val('');
        }
    })
}

function moduleSaveFunction() {
    continue_saving = true;

    if($('#original_enable_appointments_status').length != 0 && $('#original_enable_appointments_status').val() != "0" && $('#enable_appointments:checked').length == "0" && $('#record_id').val() != "") {
        continue_saving = false;

        bootbox.confirm('Rimuovendo la gestione degli appuntamenti a questo utente, tutti gli appuntamenti ad esso assegnati saranno eliminati. Continuare?', function (result) {
            if (result) {
                continueModuleSave();
            }
        })
    }

    if(continue_saving) {
        continueModuleSave();
    }
}

function continueModuleSave() {
    global_hours = new Object();
    $('.date_group').each(function() {
        if($(this).hasClass('default_settings')) {
            global_key = "default";
        } else {
            global_key = "custom";
        }

        if(global_key == "custom") {
            period_from = $(this).find('.period_from').val();
            period_to = $(this).find('.period_to').val();

            if(period_from == "" || period_to == "") {
                return true;
            }

            cur_custom_obj = {};

            if(typeof(global_hours[global_key]) == "undefined") {
                global_hours[global_key] = new Array();
            }

            cur_custom_obj['range'] = {"from" : period_from, "to" : period_to};
            cur_custom_obj['hours'] = new Array();
        } else {
            global_hours[global_key] = {};
            global_hours[global_key]['hours'] = new Array();
        }

        $(this).find('.day_block').each(function(index) {
            cur_hours = new Object();

            cur_hours['am_in'] = $(this).find('.am_in').val();
            cur_hours['am_out'] = $(this).find('.am_out').val();
            cur_hours['pm_in'] = $(this).find('.pm_in').val();
            cur_hours['pm_out'] = $(this).find('.pm_out').val();
            cur_hours['row_type'] = $(this).find('.row_type').val();

            if(global_key == "custom") {
                cur_custom_obj['hours'].push(cur_hours);
            } else {
                global_hours[global_key]['hours'].push(cur_hours);
            }
        });

        if(global_key == "custom") {
            global_hours[global_key].push(cur_custom_obj);
        }
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pCognome": $('#cognome').val(),
            "pEmail": $('#email').val(),
            "pCell": $('#cellulare').val(),
            "pCell2": $('#cellulare_2').val(),
            "pWebsite": $('#website').val(),
            "pFBProfile": $('#fb_profile').val(),
            "pTwitterProfile": $('#twitter_profile').val(),
            "pLinkedinProfile": $('#linkedin_profile').val(),
            "pGoogPlusProfile": $('#googplus_profile').val(),
            "pInstagramProfile": $('#instagram_profile').val(),
            "pLivello": $('#livello').val(),
            "pPass": $('#password').val(),
            "pPassConfirm": $('#password_confirm').val(),
            "pBiografia": $('#biografia').val(),
            "pIsActive": $('#is_active:checked').length,
            "pMailConfermata": $('#mail_active:checked').length,
            "pShowOnWebsite": $('#show_on_website:checked').length,
            "pImagesAry": composeOrakImagesToSave('images'),
            "pHoursAry": global_hours,
            "pEnableAppointments": $('#enable_appointments:checked').length,
            "pGeoData": getGeoDataAryToSave(),
            "pServiceLimit": {
                "type" : $('#services_limit_type').val(),
                "values" : $('#services_limit').val(),
            },
            "pSlug": $('#slug').val(),
            "pConsRif": $('#consulente_riferimento').val(),
            "pAgenzia": $('#agenzia').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            geoDataErrorsOnSave(data.status);

            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "password_does_not_match") {
                bootbox.error("Le password inserite devono corrispondere!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido o è già stata utilizzata da un altro utente!");
            } else if(data.status == "site_not_valid") {
                bootbox.error("L'indirizzo web inserito è in un formato non valido! Assicurarsi di inserire http:// prima dell'indirizzo");
            } else if(data.status == "saas_already_registered") {
                bootbox.error("L'indirizzo email di questo utente è già in uso. Utilizzare un indirizzo email differente.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "password_strength") {
                bootbox.error("La password deve essere lunga almeno 8 caratteri e deve contenere almeno una lettera ed un numero!");
            } else if(data.status == "hour_format_not_valid") {
                bootbox.error("Il formato dell'orario deve essere HH:MM (es. 12:05).");
            } else if(data.status == "agent_has_immobili" || data.status == "agent_has_assistenti") {//utilizzato solo per le funzionalità extra immobiliari
                showModalDelAgente(data.status, $('#record_id').val(), "save");
            } else if(data.status == "ok") {
                alert_message = "Dati salvati con successo.";
                if(data.email != "" && data.email != null) {
                    alert_message += " L'utente dovrà confermare il proprio indirizzo email prima di poter utilizzare il suo account.";
                }

                succesModuleSaveFunctionCallback(data.id, alert_message);
            }
        }
    })
}

/* AGENZIE IMMOBILIARI */
function showModalDelAgente(what, sel_id, action) {
    var useWhat = what;

    var dialog = bootbox.dialog({
        title: "E' necessaria un'ulteriore azione prima di poter proseguire!",
        message: '<p><i class="fa fa-spin fa-spinner"></i> Attendere...</p>'
    });

    dialog.init(function(){
        $.ajax({
            url: "ajax/getHTMLForDialog.php?w=" + useWhat + "&id=" + sel_id + "&action=" + action,
            dataType: "html",
            success: function(data) {
                dialog.find('.bootbox-body').html(data);
            }
        });
    });
}

function immobiliActionOnDelete(action, cur_owner) {
    msg = "Completando questa operazione, ";
    if(action == "riassegna") {
        if($('#riassegna_a').val() == "") {
            bootbox.error('Devi selezionare l\'utente a cui riassegnare gli immobili!');
            return false;
        }

        riassegna_a = $('#riassegna_a').val();
        msg += 'riassegnerai tutti gli immobili di questo utente al nuovo utente selezionato.';
    } else if(action == "elimina") {
        riassegna_a = "";
        msg += 'eliminerai tutti gli immobili di questo utente.';
    }

    msg += ' Sei sicuro di voler procedere?';

    bootbox.confirm(msg, function(result) {
        if (result) {
            $.ajax({
                url: "db/realestate/immobiliActionOnDelete.php?reassign_to=" + riassegna_a + "&delete_user=" + cur_owner + "&action=" + $('#diag_action').val(),
                async: false,
                success: function(data) {
                    var action = $('#diag_action').val();
                    closeImmobiliAction();

                    if(action == "delete") {
                        proceedWithDeletion(cur_owner);
                    } else {
                        moduleSaveFunction();
                    }

                }
            });
        }
    })
}

function assistentiActionOnDelete(action, cur_owner) {
    msg = "Completando questa operazione, ";
    if(action == "riassegna") {
        if($('#riassegna_a').val() == "") {
            bootbox.error('Devi selezionare l\'utente a cui riassegnare gli assistenti!');
            return false;
        }

        riassegna_a = $('#riassegna_a').val();
        msg += 'riassegnerai tutti gli assistenti di questo utente al nuovo utente selezionato.';
    } else if(action == "elimina") {
        riassegna_a = "";
        msg += 'eliminerai tutti gli assistenti di questo utente.';
    }

    msg += ' Sei sicuro di voler procedere?';

    bootbox.confirm(msg, function(result) {
        if (result) {
            $.ajax({
                url: "db/realestate/assistentiActionOnDelete.php?reassign_to=" + riassegna_a + "&delete_user=" + cur_owner + "&action=" + $('#diag_action').val(),
                async: false,
                success: function(data) {
                    var action = $('#diag_action').val();
                    closeImmobiliAction();

                    if(action == "delete") {
                        proceedWithDeletion(cur_owner);
                    } else {
                        moduleSaveFunction();
                    }
                }
            });
        }
    })
}

function clientiActionOnDelete(action, cur_owner) {
    msg = "Completando questa operazione, ";
    if(action == "riassegna") {
        if($('#riassegna_a').val() == "") {
            bootbox.error('Devi selezionare l\'utente a cui riassegnare i clienti!');
            return false;
        }

        riassegna_a = $('#riassegna_a').val();
        msg += 'riassegnerai tutti i clienti di questo utente al nuovo utente selezionato.';
    } else if(action == "elimina") {
        riassegna_a = "";
        msg += 'eliminerai tutti i clienti di questo utente. Eliminando i clienti, saranno eliminate anche tutte le richieste effettuate da tali clienti.';
    }

    msg += ' Sei sicuro di voler procedere?';

    bootbox.confirm(msg, function(result) {
        if (result) {
            $.ajax({
                url: "db/realestate/clientiActionOnDelete.php?reassign_to=" + riassegna_a + "&delete_user=" + cur_owner + "&action=" + $('#diag_action').val(),
                async: false,
                success: function(data) {
                    var action = $('#diag_action').val();
                    closeImmobiliAction();

                    if(action == "delete") {
                        proceedWithDeletion(cur_owner);
                    }
                }
            });
        }
    })
}

function richiesteActionOnDelete(action, cur_owner) {
    msg = "Completando questa operazione, ";
    if(action == "riassegna") {
        if($('#riassegna_a').val() == "") {
            bootbox.error('Devi selezionare l\'utente a cui riassegnare le richieste!');
            return false;
        }

        riassegna_a = $('#riassegna_a').val();
        msg += 'riassegnerai tutte le richieste di questo utente al nuovo utente selezionato.';
    } else if(action == "elimina") {
        riassegna_a = "";
        msg += 'eliminerai tutte le richieste di questo utente.';
    }

    msg += ' Sei sicuro di voler procedere?';

    bootbox.confirm(msg, function(result) {
        if (result) {
            $.ajax({
                url: "db/realestate/richiesteActionOnDelete.php?reassign_to=" + riassegna_a + "&delete_user=" + cur_owner + "&action=" + $('#diag_action').val(),
                async: false,
                success: function(data) {
                    var action = $('#diag_action').val();
                    closeImmobiliAction();

                    if(action == "delete") {
                        proceedWithDeletion(cur_owner);
                    }

                }
            });
        }
    })
}

function closeImmobiliAction() {
    $('.bootbox-close-button').trigger('click');
}
<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$original_id = $_GET['id'];
$_GET['id'] = str_replace(array("custom-", "static-"), array(), $_GET['id']);

$disabled = "";
$perms_disabled = "";
if(!empty($_GET['id'])) {
    if(strstr($original_id, "custom")) {
        $r = $MSFrameworkUsers->getCustomUserLevels($_GET['id'])[$_GET['id']];
        if($r['static_id'] != "") {
            header('location: index.php');
            die();
        }
    } else {
        $tmp_r = $MSFrameworkUsers->getUserLevels($_GET['id']);
        $r = array("name" => $tmp_r);

        $disabled = "disabled";
        $perms_disabled = "disabled";

        if($MSFrameworkSaaSBase->isSaaSDomain() && $original_id != "static-1") {
            $perms_disabled = "";

            $linked_id = $MSFrameworkDatabase->getAssoc("SELECT modules_perms, menu_granular FROM users_roles WHERE static_id = :link", array(":link" => $_GET['id']), true);
            $r['modules_perms'] = $linked_id['modules_perms'];
            $r['menu_granular'] = $linked_id['menu_granular'];
        }
    }
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php
                if($disabled == "disabled" && $perms_disabled == "disabled") {
                    require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php");
                } else {
                    require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php");
                }
                ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />
            <input type="hidden" id="original_record_id" value="<?php echo $original_id ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Ruolo</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome*</label>
                                    <input id="nome" name="nome" type="text" class="form-control required <?= $disabled ?>" value="<?php echo htmlentities($r['name']) ?>" <?= $disabled ?>>
                                </div>

                                <div class="col-sm-6">
                                    <label>Descrizione</label>
                                    <textarea id="descr" name="descr" class="form-control <?= $disabled ?>" <?= $disabled ?>><?php echo htmlentities($r['descr']) ?></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <?php if($perms_disabled != "disabled") { ?>
                        <h1>Permessi</h1>
                        <fieldset>
                            <textarea style="display: none;" class="abb_menu_granular"></textarea>

                            <?php
                            $level_id = str_replace("static-", "", $original_id);
                            $level_name = ($r['name'] != '' ? $r['name'] : "Nuovo Ruolo");
                            ?>

                            <div class="ibox role_ibox">
                                <div class="ibox-title">
                                    <h5>Menù per <b><?= $level_name; ?></b></h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="fullscreen-link">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content" style="background: #fafafa;">

                                    <div class="nestable-menu" style="margin-bottom: 15px;">
                                        <button type="button" data-action="expand-all" class="btn btn-white btn-sm">Espandi Tutto</button>
                                        <button type="button" data-action="collapse-all" class="btn btn-white btn-sm">Riduci Tutto</button>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="ibox">
                                                <div class="ibox-title">
                                                    <h5>Menù Attuale</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="dd menu_order active_menu user_level_<?= $level_id; ?>" data-role="<?= $level_id; ?>">
                                                        <ol class="dd-list">

                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="ibox">
                                                <div class="ibox-title">
                                                    <h5>Voci Inutilizzate</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="dd menu_order unused_menu user_level_<?= $level_id; ?>" data-role="<?= $level_id; ?>">
                                                        <ol class="dd-list">

                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="ibox">
                                                <div class="ibox-title">
                                                    <h5>Aggiungi Contenuti</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div id="add_content_menu_box">
                                                        <a href="#" class="btn btn-primary btn-outline add_divider">Aggiungi Divisore</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <?php } ?>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
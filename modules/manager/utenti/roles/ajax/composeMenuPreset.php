<?php
/**
 * MSAdminControlPanel
 * Date: 13/10/2018
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$sw_modules = new \MSSoftware\modules();
$modules = new \MSFramework\modules();

$menu_array = array();
$preset_id = $_POST['id'];
$userLevelID = str_replace("static-", "", $_POST['id']);

if($MSFrameworkSaaSBase->isSaaSDomain()) {
    $MSFrameworkSubscriptions = new \MSFramework\SaaS\subscriptions();
    $cur_subs = $MSFrameworkSubscriptions->getAvailSubscriptions();
    $menu_order = json_decode($cur_subs[$MSFrameworkSaaSEnvironments->getCurrentSubscription()]['menu'], true);
    $start_ary = $sw_modules->orderSidebarModules($sw_modules->getModules(), $menu_order);
    $start_preset = (strstr($preset_id, 'static-1') ? array() : $sw_modules->printSidebarSetterOrder($start_ary, false, $userLevelID, false, true));
} else {
    $start_preset = (strstr($preset_id, 'static-') ? array() : $sw_modules->printSidebarSetterOrder(null, false, $userLevelID, false, true));
}

$menu_array[$userLevelID] = array(
    $sw_modules->printSidebarSetterOrder(null, $userLevelID, $userLevelID, true, false),
    $start_preset
);

echo json_encode(array($menu_array, json_encode($sw_modules->getCurrentGranularSettings('', $_POST['loadedModuleID'], $preset_id))));
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach((new \MSFramework\users())->getUserLevels() as $k => $v) {
    $array['data'][] = array(
        $v,
        'Standard',
        $MSFrameworkDatabase->getCount("SELECT id FROM users WHERE ruolo = :ruolo", array(":ruolo" => $k)),
        "DT_RowId" => 'static-' . $k,
        "DT_buttonsLimit" => 'read'
    );
}


foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM users_roles WHERE static_id = ''") as $r) {
    $array['data'][] = array(
        $r['name'],
        'Personalizzato',
        $MSFrameworkDatabase->getCount("SELECT id FROM users WHERE ruolo = :ruolo", array(":ruolo" => 'custom-' . $r['id'])),
        "DT_RowId" => 'custom-' . $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

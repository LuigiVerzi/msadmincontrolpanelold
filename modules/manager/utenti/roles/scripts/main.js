$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('.menu_order').each(function () {
        var role = $(this).data('role');
        $(this).nestable({
            group: role
        });
    });

    loadMenuPreset();

    $('#add_content_menu_box .add_divider').on('click', function (e) {
        e.preventDefault();

        var titolo = $('<div/>').text(prompt('Inserisci il titolo del divisore')).html();
        if(titolo.length) {
            var html = '<li class="dd-item divider" data-id="divider" data-value="' + titolo + '"><div class="dd-handle"><span class="label label-success"><i class="fa fa-bars"></i></span> <span class="text">[DIVISORE] ' + titolo + '</span> <a class="delete" onclick="$(this).closest(\'li\').remove()">Elimina</a></div></li>';
            $(this).closest('.role_ibox').find('.active_menu > ol').append(html);
        }
    });

    $('.nestable-menu').on('click', function (e) {
        var action = $(e.target).data('action');

        var $dd = $(this).closest('.ibox-content')

        if (action === 'expand-all') {
            $dd.nestable('expandAll');
        }
        else if (action === 'collapse-all') {
            $dd.nestable('collapseAll');
        }
    });
}

function loadMenuPreset() {
    $.ajax({
        url: "ajax/composeMenuPreset.php",
        type: "POST",
        data: {
            "id": $('#original_record_id').val(),
            "loadedModuleID" : $('#loadedModuleID').val()
        },
        async: false,
        dataType: "json",
        success: function (data) {
            $('#modules_preset_configuration').show();

            if($('#menu_order_presets').val() == 'new' || $('#menu_order_presets').val() == 'custom') {
                $('#modules_preset_configuration .show_on_edit').show();
                $('#modules_preset_configuration .edit_configuration').hide();
                $('#modules_preset_configuration .undo_edit').hide();
            }
            else {
                $('#modules_preset_configuration .show_on_edit').hide();
                $('#modules_preset_configuration .edit_configuration').show();
                $('#modules_preset_configuration .undo_edit').hide();
            }

            if($('#menu_order_presets').val() == 'custom') {
                $('#modules_preset_configuration .hide_for_custom').hide();
            }

            $('.menu_order').nestable('destroy');

            Object.keys(data[0]).forEach(function (user_role) {

                var preset = data[0][user_role];

                $('.menu_order.active_menu.user_level_' + user_role).html((preset[0] != "" ? '<ol class="dd-list">' + preset[0] + '</ol>' : ''));
                $('.menu_order.unused_menu.user_level_' + user_role).html((preset[1] != "" ? '<ol class="dd-list">' + preset[1] + '</ol>' : ''));

                // Rimuovo le voci già usate dal menù 'inutilizzati'
                $('.menu_order.unused_menu.user_level_' + user_role + ' .dd-item').each(function () {
                    var id = $(this).data('id');
                    var $li = $(this);
                    var $ol = $(this).closest('ol');

                    if ($('.menu_order.active_menu.user_level_' + user_role).find('.dd-item[data-id="' + id + '"]').length) {
                        $li.remove();
                        if (!$ol.find('li').length) {
                            $ol.remove();
                        }
                    }

                });

            });

            $('.granular_settings_menuicon').on('click', function() {
                openGranularSettings($(this).closest('.dd-item').data('id'));
            })
            $('.abb_menu_granular').val(data[1]);

            $('.menu_order').nestable('init');
        }
    });
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pOriginalID": $('#original_record_id').val(),
            "pNome": $('#nome').val(),
            "pDescrizione": $('#descr').val(),
            "pModulesPerms": $('.menu_order.active_menu').nestable('serialize'),
            "pModulesGranular": $('.abb_menu_granular').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "site_not_valid") {
                bootbox.error("L'indirizzo web inserito è in un formato non valido! Assicurarsi di inserire http:// prima dell'indirizzo");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
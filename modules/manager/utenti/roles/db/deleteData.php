<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($_POST['pID'] as $id) {
    if (!strstr($id, "custom-")) {
        echo json_encode(array("status" => "protected"));
        die();
    }
}

if($MSFrameworkDatabase->getCount("SELECT id FROM users WHERE ruolo = :ruolo", array(":ruolo" => $_POST['pID'])) != 0) {
    echo json_encode(array("status" => "user_with_role"));
    die();
}

if($MSFrameworkDatabase->deleteRow("users_roles", "id", str_replace("custom-", "", $_POST['pID']))) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;

if($_POST['pNome'] == "") {
    $can_save = false;
}

$original_id = $_POST['pOriginalID'];
$disabled = false;
$perms_disabled = false;

if(strstr($original_id, "static")) {
    $disabled = true;
    $perms_disabled = true;

    if($MSFrameworkSaaSBase->isSaaSDomain() && $original_id != "static-1") {
        $perms_disabled = false;
    }

    $static_id = str_replace("static-", "", $original_id);
    $linked_id = $MSFrameworkDatabase->getAssoc("SELECT id FROM users_roles WHERE static_id = :link", array(":link" => $static_id), true);
    $_POST['pID'] = $linked_id['static_link'];
}

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "name" => $_POST['pNome'],
    "descr" => $_POST['pDescrizione'],
    "modules_perms" => json_encode($_POST['pModulesPerms']),
    "menu_granular" => $_POST['pModulesGranular'],
);

if(strstr($original_id, "static")) {
    $array_to_save['static_id'] = $static_id;
}

if($disabled) {
    unset($array_to_save['name']);
    unset($array_to_save['descr']);
}

if($perms_disabled) {
    unset($array_to_save['modules_perms']);
    unset($array_to_save['menu_granular']);
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO users_roles ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE users_roles SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
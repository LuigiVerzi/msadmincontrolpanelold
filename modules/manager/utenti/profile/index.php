<?php
require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkGeonames = (new \MSFramework\geonames());
$r = $MSFrameworkUsers->getUserDataFromDB($_SESSION['userData']['user_id']);
?>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_SESSION['userData']['user_id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Account</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome*</label>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo $r['nome'] ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Cognome*</label>
                                    <input id="cognome" name="cognome" type="text" class="form-control required" value="<?php echo $r['cognome'] ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Email*</label>
                                    <input id="email" name="email" type="text" class="form-control required email disabled" value="<?php echo $r['email'] ?>" disabled>
                                </div>

                                <div class="col-sm-6">
                                    <label>Sito Web</label>
                                    <input id="website" name="website" type="text" class="form-control" value="<?php echo $r['website'] ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Cellulare</label>
                                    <input id="cellulare" name="cellulare" type="text" class="form-control required" value="<?php echo $r['cellulare'] ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Cellulare Alternativo</label>
                                    <input id="cellulare_2" name="cellulare_2" type="text" class="form-control" value="<?php echo $r['cellulare_2'] ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php require(ABSOLUTE_SW_PATH . "includes/template/geodata/geodata.php"); ?>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Biografia</label>
                                    <textarea class="form-control" id="biografia" rows="12" cols="5"><?php echo $r['biografia'] ?></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12" style="margin-bottom: 40px;">
                                    <label>Foto utente</label>
                                    <?php (new \MSFramework\uploads('PROPIC', $_SESSION['userData']['is_global']))->initUploaderHTML("images", $r['propic']); ?>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Social</h1>
                        <fieldset>
                            <h2 class="title-divider">Profili Social</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Facebook</label>
                                    <input id="fb_profile" name="fb_profile" type="text" class="form-control" value="<?php echo $r['fb_profile'] ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Instagram</label>
                                    <input id="instagram_profile" name="instagram_profile" type="text" class="form-control" value="<?php echo $r['instagram_profile'] ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Twitter</label>
                                    <input id="twitter_profile" name="twitter_profile" type="text" class="form-control" value="<?php echo $r['twitter_profile'] ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Linkedin</label>
                                    <input id="linkedin_profile" name="linkedin_profile" type="text" class="form-control" value="<?php echo $r['linkedin_profile'] ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Google Plus</label>
                                    <input id="googplus_profile" name="googplus_profile" type="text" class="form-control" value="<?php echo $r['googplus_profile'] ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                        <h1>Accesso</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Accesso</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Livello*</label>
                                    <input id="livello" name="livello" type="text" class="form-control required disabled" value="<?php echo $MSFrameworkUsers->getUserLevels($r['ruolo']) ?>" disabled>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Password</label>
                                    <input id="password" name="password" type="password" class="form-control">
                                    <span class="help-block m-b-none">Lasciare vuoto per non modificare</span>
                                </div>

                                <div class="col-sm-6">
                                    <label>Conferma Password</label>
                                    <input id="password_confirm" name="password_confirm" type="password" class="form-control">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                        </fieldset>

                    </form>

                </div>
            </div>
        </div>
        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;
if(trim($_POST['pNome']) == "" || trim($_POST['pCognome']) == "" || trim($_POST['pCell']) == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

(new MSFramework\geonames())->checkDataBeforeSave();

if($_POST['pPass'] != $_POST['pPassConfirm']) {
    echo json_encode(array("status" => "password_does_not_match"));
    die();
}

if($_POST['pPass'] != "" && $MSFrameworkUsers->checkPasswordStrength($_POST['pPass']) != "") {
    echo json_encode(array("status" => "password_strength"));
    die();
}

if($_POST['pID'] != $_SESSION['userData']['user_id']) {
    echo json_encode(array("status" => "id_does_not_match"));
    die();
}

if($_POST['pWebsite'] != "") {
    if(!filter_var($_POST['pWebsite'], FILTER_VALIDATE_URL)) {
        echo json_encode(array("status" => "site_not_valid"));
        die();
    }
}

$user_id = $_SESSION['userData']['user_id'];

$r_old_data = $MSFrameworkUsers->getUserDetails($user_id, 'propic')[$user_id];

$uploader = new \MSFramework\uploads('PROPIC', $_SESSION['userData']['is_global']);
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "cognome" => $_POST['pCognome'],
    "cellulare" => $_POST['pCell'],
    "cellulare_2" => $_POST['pCell2'],
    "website" => $_POST['pWebsite'],
    "geo_data" => json_encode($_POST['pGeoData']),
    "fb_profile" => $_POST['pFBProfile'],
    "instagram_profile" => $_POST['pInstagramProfile'],
    "twitter_profile" => $_POST['pTwitterProfile'],
    "linkedin_profile" => $_POST['pLinkedinProfile'],
    "googplus_profile" => $_POST['pGoogPlusProfile'],
    "password" => password_hash($_POST['pPass'], PASSWORD_DEFAULT),
    "propic" => json_encode($ary_files),
    "biografia" => $_POST['pBiografia'],
);

if($_POST['pPass'] == "") {
    unset($array_to_save['password']);
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'update');
$result = $MSFrameworkDatabase->pushToDB("UPDATE " . $MSFrameworkUsers->getUsersTable($user_id) . " SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => str_replace('global-', '', $_POST['pID'])), $stringForDB[0]));

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$uploader->unlink($ary_files, json_decode($r_old_data['propic'], true));

echo json_encode(array("status" => "ok", "id" => ""));
die();
?>

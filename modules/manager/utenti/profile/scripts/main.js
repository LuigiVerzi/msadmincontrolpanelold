function initForm() {
    initClassicTabsEditForm();

    initOrak('images', 1, 0, 0, 100, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);

    manageWorldDataSelects();
}

function moduleSaveFunction() {
    global_hours = new Object();
    $('.date_group').each(function() {
        if($(this).hasClass('default_settings')) {
            global_key = "default";
        } else {
            global_key = "custom";
        }

        if(global_key == "custom") {
            period_from = $(this).find('.period_from').val();
            period_to = $(this).find('.period_to').val();

            if(period_from == "" || period_to == "") {
                return true;
            }

            cur_custom_obj = {};

            if(typeof(global_hours[global_key]) == "undefined") {
                global_hours[global_key] = new Array();
            }

            cur_custom_obj['range'] = {"from" : period_from, "to" : period_to};
            cur_custom_obj['hours'] = new Array();
        } else {
            global_hours[global_key] = {};
            global_hours[global_key]['hours'] = new Array();
        }

        $(this).find('.day_block').each(function(index) {
            cur_hours = new Object();

            cur_hours['am_in'] = $(this).find('.am_in').val();
            cur_hours['am_out'] = $(this).find('.am_out').val();
            cur_hours['pm_in'] = $(this).find('.pm_in').val();
            cur_hours['pm_out'] = $(this).find('.pm_out').val();
            cur_hours['row_type'] = $(this).find('.row_type').val();

            if(global_key == "custom") {
                cur_custom_obj['hours'].push(cur_hours);
            } else {
                global_hours[global_key]['hours'].push(cur_hours);
            }
        });

        if(global_key == "custom") {
            global_hours[global_key].push(cur_custom_obj);
        }
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pCognome": $('#cognome').val(),
            "pCell": $('#cellulare').val(),
            "pCell2": $('#cellulare_2').val(),
            "pWebsite": $('#website').val(),
            "pFBProfile": $('#fb_profile').val(),
            "pTwitterProfile": $('#twitter_profile').val(),
            "pLinkedinProfile": $('#linkedin_profile').val(),
            "pGoogPlusProfile": $('#googplus_profile').val(),
            "pInstagramProfile": $('#instagram_profile').val(),
            "pPass": $('#password').val(),
            "pPassConfirm": $('#password_confirm').val(),
            "pBiografia": $('#biografia').val(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pGeoData": getGeoDataAryToSave(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            geoDataErrorsOnSave(data.status);

            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "password_does_not_match") {
                bootbox.error("Le password inserite devono corrispondere!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido o è già stata utilizzata da un altro utente!");
            } else if(data.status == "site_not_valid") {
                bootbox.error("L'indirizzo web inserito è in un formato non valido! Assicurarsi di inserire http:// prima dell'indirizzo");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "id_does_not_match") {
                bootbox.error("Non puoi modificare il profilo di un utente diverso dal tuo!");
            } else if(data.status == "password_strength") {
                bootbox.error("La password deve essere lunga almeno 8 caratteri e deve contenere almeno una lettera ed un numero!");
            } else if(data.status == "ok") {
                alert_message = "Dati salvati con successo.";
                if(data.email != "" && data.email != null) {
                    alert_message += " L'utente dovrà confermare il proprio indirizzo email prima di poter utilizzare il suo account.";
                }

                succesModuleSaveFunctionCallback(data.id, alert_message);
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($_POST['pEmail'] != "" && !filter_var($_POST['pEmail'], FILTER_VALIDATE_EMAIL)) {
    echo json_encode(array("status" => "mail_not_valid"));
    die();
}

$db_action = "update";
if(!$isSaaS && $MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'site'") == 0) {
    $db_action = "insert";
}

(new MSFramework\geonames())->checkDataBeforeSave();

$uploader = new \MSFramework\uploads('LOGOS');
$ary_files['logo'] = "";
$t_logo = $uploader->prepareForSave($_POST['pImagesAry'], array("png", "jpeg", "jpg", "svg"));
if($t_logo === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
} else {
    if($t_logo[0] != "") {
        $ary_files['logo'] = $t_logo[0];
    }
}

$uploaderLogoFooter = new \MSFramework\uploads('LOGOS');
$ary_files['logo_footer'] = "";
$t_logofooter = $uploaderLogoFooter->prepareForSave($_POST['pLogoFooterAry'], array("png", "jpeg", "jpg", "svg"));
if($t_logofooter === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
} else {
    if($t_logofooter[0] != "") {
        $ary_files['logo_footer'] = $t_logofooter[0];
    }
}

$uploaderFavicon = new \MSFramework\uploads('LOGOS');
$ary_files['favicon'] = "";
$t_favicon = $uploaderFavicon->prepareForSave($_POST['pFavicon'], array("png", "ico"));
if($ary_files['favicon'] === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
} else {
    if($t_favicon[0] != "") {
        $ary_files['favicon'] = $t_favicon[0];
    }
}

$uploaderWatermark = new \MSFramework\uploads('LOGOS');
$ary_files['watermark'] = "";
$t_watermark = $uploaderWatermark->prepareForSave($_POST['pWatermark'], array("png", "jpeg", "jpg", "svg"));
if($t_watermark === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
} else {
    if($t_watermark[0] != "") {
        $ary_files['watermark'] = $t_watermark[0];
    }
}

$uploaderLoginBG = new \MSFramework\uploads('LOGOS');
$ary_files['login_bg'] = "";
$t_login_bg = $uploaderLoginBG->prepareForSave($_POST['pLoginBG']);
if($t_login_bg === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
} else {
    if($t_login_bg[0] != "") {
        $ary_files['login_bg'] = $t_login_bg[0];
    }
}

$uploaderLoginLogo = new \MSFramework\uploads('LOGOS');
$ary_files['login_logo'] = "";
$t_login_logo = $uploaderLoginBG->prepareForSave($_POST['pLoginLogo']);
if($t_login_logo === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
} else {
    if($t_login_logo[0] != "") {
        $ary_files['login_logo'] = $t_login_logo[0];
    }
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkCMS->getCMSData('site');
    $old_phones = json_decode($r_old_data['additional_phones'], true);
}

foreach($_POST['pPhonesAry'] as $k_phone => $cur_phone) {
    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $cur_phone[1], 'oldValue' => $old_phones[$k_phone][1]),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $_POST['pPhonesAry'][$k_phone][1] =  $MSFrameworki18n->setFieldValue($old_phones[$k_phone][1], $cur_phone[1]);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pMotto'], 'oldValue' => $r_old_data['motto']),
        array('currentValue' => $_POST['pLongDescr'], 'oldValue' => $r_old_data['long_descr']),
        array('currentValue' => $_POST['pShortDescr'], 'oldValue' => $r_old_data['short_descr']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "value" => json_encode(array(
        "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
        "motto" => $MSFrameworki18n->setFieldValue($r_old_data['motto'], $_POST['pMotto']),
        "telefono" => $_POST['pTelefono'],
        "cellulare" => $_POST['pCellulare'],
        "fax" => $_POST['pFAX'],
        "email" => $_POST['pEmail'],
        "pec" => $_POST['pPEC'],
        "rag_soc" => $_POST['pRagSoc'],
        "piva" => $_POST['pPIVA'],
        "geo_data" => json_encode($_POST['pGeoData']),
        "fb_profile" => $_POST['pFBProfile'],
        "fb_page_id" => $_POST['pFBPageID'],
        "ig_profile_id" => $_POST['pIGProfileID'],
        "instagram_profile" => $_POST['pInstagramProfile'],
        "twitter_profile" => $_POST['pTwitterProfile'],
        "linkedin_profile" => $_POST['pLinkedinProfile'],
        "googplus_profile" => $_POST['pGoogPlusProfile'],
        "skype_profile" => $_POST['pSkypeProfile'],
        "youtube_profile" => $_POST['pYoutubeProfile'],
        "header_logo_width" => $_POST['pHeaderLogoWidth'],
        "use_logo_in_login" => $_POST['pUseLogoInLogin'],
        "sitemap" => json_encode($_POST['pSitemap']),
        "registration" => json_encode($_POST['pRegistration']),
        "customers" => json_encode($_POST['pCustomers']),
        "acp" => json_encode($_POST['pACP']),
        "additional_phones" => json_encode($_POST['pPhonesAry']),
        "logos" => json_encode($ary_files),
        "long_descr" => $MSFrameworki18n->setFieldValue($r_old_data['long_descr'], $_POST['pLongDescr']),
        "short_descr" => $MSFrameworki18n->setFieldValue($r_old_data['short_descr'], $_POST['pShortDescr']),
        "additional_css" => $_POST['pCSSEditor'],
        "robots_txt" => $_POST['pRobotsEditor'],
        "additional_js" => $_POST['pJSEditor'],
    )),
    "type" => "site"
);

if(!$isSaaS) {
    $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
}

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    if($isSaaS) {
        $r_old_cms = $MSFrameworkDatabase->getAssoc("SELECT cms FROM `" . FRAMEWORK_DB_NAME . "`.`saas__config` WHERE id = :id", array(":id" => $MSFrameworkSaaSBase->getID()), true);
        $old_cms_data = json_decode($r_old_cms['cms'], true);
        $old_cms_data['site'] = json_decode($array_to_save['value'], true);
        $stringForDB = $MSFrameworkDatabase->createStringForDB(array("cms" => json_encode($old_cms_data)), $db_action);

        $result = $MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`saas__config` SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $MSFrameworkSaaSBase->getID()), $stringForDB[0]));
    } else {
        $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'site'", $stringForDB[0]);
    }
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}


if($isSaaS) {
    $MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`saas__config` SET set_offline = :set_offline WHERE id = :id", array(":id" => $MSFrameworkSaaSBase->getID(), ":set_offline" => $_POST['pSetOffline']));
} else {
    $str_customer_domain = "";
    $array_customer_domain = array();
    if(strstr($_SERVER['DOCUMENT_ROOT'], "/SVILUPPO/") && $_POST['pVirtualServerName'] != "") {
        $str_customer_domain .= ', virtual_server_name = :vsn ';
        $array_customer_domain[':vsn'] = $_POST['pVirtualServerName'];
    }

    $MSFrameworkDatabase->pushToDB("UPDATE  `" . FRAMEWORK_DB_NAME . "`.`customers` SET set_offline = :set_offline $str_customer_domain WHERE customer_domain = :domain", array_merge($array_customer_domain, array(":domain" => $MSFrameworkCMS->getCleanHost(), ":set_offline" => $_POST['pSetOffline'])));
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['logos'], true));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
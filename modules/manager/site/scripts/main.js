function initForm() {
    initClassicTabsEditForm();

    initOrak('images', 1, 40, 40, 100, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png', 'image/svg+xml']);
    initOrak('logo_footer', 1, 40, 40, 100, getOrakImagesToPreattach('logo_footer'), false, ['image/jpeg', 'image/png', 'image/svg+xml']);
    initOrak('favicon', 1, 16, 16, 16, getOrakImagesToPreattach('favicon'), false, ['image/png', 'image/x-icon', 'image/vnd.microsoft.icon']);
    initOrak('watermark', 1, 120, 120, 50, getOrakImagesToPreattach('watermark'), false, ['image/jpeg', 'image/png', 'image/svg+xml']);
    initOrak('login_bg', 1, 960, 540, 320, getOrakImagesToPreattach('login_bg'), false, ['image/jpeg', 'image/png']);
    initOrak('login_logo', 1, 40, 40, 100, getOrakImagesToPreattach('login_logo'), false, ['image/jpeg', 'image/png']);

    manageWorldDataSelects();

    CSSMode = ace.require("ace/mode/css").Mode;
    TEXTMode = ace.require("ace/mode/text").Mode;
    JSMode = ace.require("ace/mode/javascript").Mode;

    css_editor = ace.edit("editor_css");
    css_editor.session.setMode(new CSSMode());

    js_editor = ace.edit("editor_js");
    js_editor.session.setMode(new JSMode());

    editor_robots = ace.edit("editor_robots");
    editor_robots.session.setMode(new TEXTMode());

    $('.rowDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi telefono",
        "deleteButtonText": "Elimina telefono",
        "afterAddCallback": function() {
            $('.rowDuplication:last').find('.add_phone_val').val('');
            $('.rowDuplication:last').find('.add_phone_descr').val('');
        },
        "afterDeleteCallback": function() {
        },
    });

    initTinyMCE( '#short_descr, #long_descr', {small: true, autoresize: false}, {height: 350});

    $('#auto_generate_sitemap').on('ifChanged', function(event){
        if($('#auto_generate_sitemap:checked').length == 1) {
            $('#sitemap_cron_container').show();
        } else {
            $('#sitemap_cron_container').hide();
        }
    });

    $('#generateSitemap').on('click', function() {

        window.dialog = bootbox.dialog({
            message: '<p class="text-center">Generazione della sitemap ' + ($('#auto_send_sitemap:checked').length == 1 ? "ed invio ai motori di ricerca " : "") + 'in corso. Attendere...</p>',
            closeButton: false
        });

        $.ajax({
            url: "../../../MSFramework/cron/Produzione/sitemap/generateSitemap.php?from_acp",
            dataType: "json",
            preloader: 'Creazione sitemap in corso',
            success: function(data) {
                $('#last_upd_time').html(data.upd_date);
                window.dialog.modal('hide');
                bootbox.alert('Sitemap aggiornata con successo!');
            },
            error: function() {
                window.dialog.modal('hide');
                bootbox.error('Errore durante l\'aggiornamento della sitemap.');
            }
        });
    })

    $('#enable_registration').on('ifToggled', function(event){
        if($(this).is(':checked')){
            $('.registration_settings').show();
        } else {
            $('.registration_settings').hide();

            $('#default_level').val($('#default_level option:eq(0)').attr('value'));

            $('#can_choose_level').attr('checked', false);
            $('#can_choose_level').iCheck('uncheck').iCheck('update');

            $('#request_mail_confirm, #request_admin_confirm').attr('checked', false);
            $('#request_mail_confirm, #request_admin_confirm').iCheck('uncheck').iCheck('update');

            chooseLevelToggled();
        }
    })

    $('#can_choose_level').on('ifToggled', function(event){
        chooseLevelToggled();
    })
}

function chooseLevelToggled() {
    if($('#can_choose_level').is(':checked')){
        $('.registration_exclude_level_settings').show();
    } else {
        $('.registration_exclude_level_settings').hide();

        $('#exclude_levels').attr('checked', false);
        $('#exclude_levels').iCheck('uncheck').iCheck('update');
    }
}

function moduleSaveFunction() {
    phone_ary = new Object();
    $('.stepsDuplication.phoneDuplication').each(function(row_index) {
        phone_ary_tmp = new Array();

        phone_val = $(this).find('.add_phone_val').val();
        if(phone_val == "") {
            return true;
        }

        phone_ary_tmp.push($(this).find('.add_phone_val').val());
        phone_ary_tmp.push($(this).find('.add_phone_descr').val());

        phone_ary[row_index] = phone_ary_tmp;
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pNome": $('#nome').val(),
            "pMotto": $('#motto').val(),
            "pTelefono": $('#telefono').val(),
            "pCellulare": $('#cellulare').val(),
            "pFAX": $('#fax').val(),
            "pEmail": $('#email').val(),
            "pPEC": $('#pec').val(),
            "pRagSoc": $('#rag_soc').val(),
            "pPIVA": $('#piva').val(),
            "pGeoData": getGeoDataAryToSave(),
            "pFBProfile": $('#fb_profile').val(),
            "pFBPageID": $('#fb_page_id').val(),
            "pIGProfileID": $('#ig_profile_id').val(),
            "pTwitterProfile": $('#twitter_profile').val(),
            "pLinkedinProfile": $('#linkedin_profile').val(),
            "pGoogPlusProfile": $('#googplus_profile').val(),
            "pInstagramProfile": $('#instagram_profile').val(),
            "pSkypeProfile": $('#skype_profile').val(),
            "pYoutubeProfile": $('#youtube_profile').val(),
            "pHeaderLogoWidth": $('#header_logo_width').val(),
            "pUseLogoInLogin": $('#use_logo_in_login').val(),
            "pSitemap": {
                auto_generate : $('#auto_generate_sitemap:checked').length,
                auto_send : $('#auto_send_sitemap:checked').length,
                regen_freq : $('#sitemap_regen_freq').val(),
            },
            "pRegistration": {
                enable : $('#enable_registration:checked').length,
                default_level : $('#default_level').val(),
                request_mail_confirm : $('#request_mail_confirm:checked').length,
                request_admin_confirm : $('#request_admin_confirm:checked').length,
                can_choose_level : $('#can_choose_level:checked').length,
                exclude_levels : $('#exclude_levels').val(),

            },
            "pCustomers": {
                notify_via_mail : $('#customer_notification:checked').length

            },
            "pACP": {
                exit_after_save : $('#exit_after_save:checked').length

            },
            "pImagesAry": composeOrakImagesToSave('images'),
            "pLogoFooterAry": composeOrakImagesToSave('logo_footer'),
            "pFavicon": composeOrakImagesToSave('favicon'),
            "pWatermark": composeOrakImagesToSave('watermark'),
            "pLoginBG": composeOrakImagesToSave('login_bg'),
            "pLoginLogo": composeOrakImagesToSave('login_logo'),
            "pPhonesAry": phone_ary,
            "pLongDescr": tinymce.get('long_descr').getContent(),
            "pShortDescr": tinymce.get('short_descr').getContent(),
            "pCSSEditor": css_editor.getValue(),
            "pJSEditor": js_editor.getValue(),
            "pRobotsEditor": editor_robots.getValue(),
            "pSetOffline": $('#set_offline:checked').length
        },
        async: false,
        dataType: "json",
        success: function(data) {
            geoDataErrorsOnSave(data.status);

            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                $('#exitAfterSave').val($('#exit_after_save:checked').length);

                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('site');
$customer_data = $MSFrameworkDatabase->getAssoc("SELECT virtual_server_name, set_offline FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE customer_domain = :domain", array(":domain" => $MSFrameworkCMS->getCleanHost()), true);
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <?php
                    if($isSaaS) {
                        ?>
                        <div class="alert alert-danger">
                            Le seguenti impostazioni verranno applicate a tutti i SaaS <b><?= SW_NAME ?></b>.
                        </div>
                    <?php } ?>

                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Dati Sito</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Motto</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['motto']) ?>"></i></span>
                                    <input id="motto" name="motto" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['motto'])) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Descrizione Breve</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['short_descr']) ?>"></i></span>
                                    <textarea id="short_descr" name="short_descr"><?php echo $MSFrameworki18n->getFieldValue($r['short_descr']) ?></textarea>
                                </div>

                                <div class="col-sm-6">
                                    <label>Descrizione Estesa</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['long_descr']) ?>"></i></span>
                                    <textarea id="long_descr" name="long_descr"><?php echo $MSFrameworki18n->getFieldValue($r['long_descr']) ?></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Dati Azienda</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Generali</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Ragione Sociale</label>
                                    <input id="rag_soc" name="rag_soc" type="text" class="form-control" value="<?php echo htmlentities($r['rag_soc']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Partita IVA</label>
                                    <input id="piva" name="piva" type="text" class="form-control" value="<?php echo htmlentities($r['piva']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <label>Telefono</label>
                                    <input id="telefono" name="telefono" type="text" class="form-control" value="<?php echo htmlentities($r['telefono']) ?>">
                                </div>

                                <div class="col-sm-2">
                                    <label>Cellulare</label>
                                    <input id="cellulare" name="cellulare" type="text" class="form-control" value="<?php echo htmlentities($r['cellulare']) ?>">
                                </div>

                                <div class="col-sm-2">
                                    <label>FAX</label>
                                    <input id="fax" name="fax" type="text" class="form-control" value="<?php echo htmlentities($r['fax']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Email</label>
                                    <input id="email" name="email" type="text" class="form-control email" value="<?php echo htmlentities($r['email']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>PEC</label>
                                    <input id="pec" name="pec" type="text" class="form-control email" value="<?php echo htmlentities($r['pec']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="easyRowDuplicationContainer">
                                <?php
                                $additional_phones = json_decode($r['additional_phones'], true);
                                if(count($additional_phones) == 0) {
                                    $additional_phones[] = array();
                                }

                                foreach($additional_phones as $addPhK => $addPhV) {
                                ?>
                                <div class="stepsDuplication rowDuplication phoneDuplication">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Numero Telefono Addizionale</label>
                                            <input type="text" class="form-control add_phone_val" value="<?php echo htmlentities($addPhV[0]) ?>">
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Descrizione Telefono Addizionale</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($addPhV[1]) ?>"></i></span>
                                            <input type="text" class="form-control add_phone_descr" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($addPhV[1])) ?>" placeholder="Es: Direzione">
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php require(ABSOLUTE_SW_PATH . "includes/template/geodata/geodata.php"); ?>

                        </fieldset>

                        <h1>Social</h1>
                        <fieldset>
                            <h2 class="title-divider">Profili Social</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Facebook</label>
                                    <input id="fb_profile" name="fb_profile" type="text" class="form-control" value="<?php echo htmlentities($r['fb_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>ID Pagina Facebook</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Anche le funzioni di condivisione automatica su Facebook utilizzeranno questa pagina come destinazione delle condivisioni."></i></span>
                                    <input id="fb_page_id" name="fb_page_id" type="text" class="form-control" value="<?php echo htmlentities($r['fb_page_id']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Instagram</label>
                                    <input id="instagram_profile" name="instagram_profile" type="text" class="form-control" value="<?php echo htmlentities($r['instagram_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>ID Profilo Instagram</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Anche le funzioni di condivisione automatica su Instagram utilizzeranno questo profilo come destinazione delle condivisioni."></i></span>
                                    <input id="ig_profile_id" name="ig_profile_id" type="text" class="form-control" value="<?php echo htmlentities($r['ig_profile_id']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Twitter</label>
                                    <input id="twitter_profile" name="twitter_profile" type="text" class="form-control" value="<?php echo htmlentities($r['twitter_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Linkedin</label>
                                    <input id="linkedin_profile" name="linkedin_profile" type="text" class="form-control" value="<?php echo htmlentities($r['linkedin_profile']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Skype</label>
                                    <input id="skype_profile" name="skype_profile" type="text" class="form-control" value="<?php echo htmlentities($r['skype_profile']) ?>">
                                </div>

                                <div class="col-sm-6">
                                    <label>Profilo Youtube</label>
                                    <input id="youtube_profile" name="youtube_profile" type="text" class="form-control" value="<?php echo htmlentities($r['youtube_profile']) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Profilo Google Plus</label>
                                    <input id="googplus_profile" name="googplus_profile" type="text" class="form-control" value="<?php echo htmlentities($r['googplus_profile']) ?>">
                                </div>
                            </div>

                        </fieldset>

                        <h1>Sitemap</h1>
                        <fieldset>
                            <h2 class="title-divider">Impostazioni Sitemap</h2>
                            <?php
                            $sitemap_settings = json_decode($r['sitemap'], true);
                            ?>
                            <div class="row">
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary" id="generateSitemap">Genera Sitemap</button>
                                    <small>Ultimo aggiornamento: <span id="last_upd_time"><?= (!filemtime(MAIN_SITE_FOLDERPATH . "sitemap.xml") ? "Mai" : date("d/m/Y H:i", filemtime(MAIN_SITE_FOLDERPATH . "sitemap.xml"))) ?></span></small>
                                </div>

                                <div class="col-sm-4">
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="auto_generate_sitemap" <?php echo ($sitemap_settings['auto_generate'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Aggiorna automaticamente</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="auto_send_sitemap" <?php echo ($sitemap_settings['auto_send'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Invia ai motori di ricerca</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="sitemap_cron_container" style="display: <?php echo ($sitemap_settings['auto_generate'] == "1" ? "block" : "none") ?>">
                                <div class="col-sm-12">
                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Frequenza aggiornamento</label>
                                    <select id="sitemap_regen_freq" class="form-control">
                                        <option value="1" <?= ($sitemap_settings['regen_freq'] == "1" ? "selected" : "") ?>>Una volta al giorno</option>
                                        <option value="2" <?= ($sitemap_settings['regen_freq'] == "2" ? "selected" : "") ?>>Una volta a settimana</option>
                                        <option value="3" <?= ($sitemap_settings['regen_freq'] == "3" ? "selected" : "") ?>>Due volte al mese</option>
                                        <option value="4" <?= ($sitemap_settings['regen_freq'] == "4" ? "selected" : "") ?>>Una volta al mese</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Loghi, Favicon & Watermark</h1>
                        <fieldset>
                            <div class="row">
                                <?php
                                $loghi = json_decode($r['logos'], true);
                                foreach($loghi as $curlogoK => $val) {
                                    if($val == "" || $val == null) {
                                        $loghi[$curlogoK] = array();
                                    } else if(!is_array($val)) {
                                        $loghi[$curlogoK] = array($val);
                                    }
                                }
                                ?>
                                <div class="col-sm-4" style="margin-bottom: 40px;">
                                    <label>Logo Principale</label>
                                    <?php
                                    (new \MSFramework\uploads('LOGOS'))->initUploaderHTML("images", json_encode($loghi['logo']));
                                    ?>
                                </div>

                                <div class="col-sm-4" style="margin-bottom: 40px;">
                                    <label>Logo Secondario</label>
                                    <?php
                                    (new \MSFramework\uploads('LOGOS'))->initUploaderHTML("logo_footer", json_encode($loghi['logo_footer']));
                                    ?>
                                </div>

                                <div class="col-sm-4">
                                    <label>Favicon</label>
                                    <?php
                                    (new \MSFramework\uploads('LOGOS'))->initUploaderHTML("favicon", json_encode($loghi['favicon']));
                                    ?>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Watermark</label>
                                    <?php
                                    (new \MSFramework\uploads('LOGOS'))->initUploaderHTML("watermark", json_encode($loghi['watermark']));
                                    ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Sfondo Login</label>
                                    <?php
                                    (new \MSFramework\uploads('LOGOS'))->initUploaderHTML("login_bg", json_encode($loghi['login_bg']));
                                    ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Logo Login</label>
                                    <?php
                                    (new \MSFramework\uploads('LOGOS'))->initUploaderHTML("login_logo", json_encode($loghi['login_logo']));
                                    ?>
                                </div>
                            </div>

                            <h2 class="title-divider">Impostazioni Loghi</h2>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Width logo header (con unità di misura)</label>
                                    <input id="header_logo_width" name="header_logo_width" type="text" class="form-control" value="<?php echo htmlentities($r['header_logo_width']) ?>">
                                </div>

                                <div class="col-sm-4">
                                    <label>Logo da utilizzare in pagina login</label>
                                    <select id="use_logo_in_login" class="form-control">
                                        <option value="0" <?= ($r['use_logo_in_login'] == "0" ? "selected" : "") ?>>Utilizza logo di default Framework360</option>
                                        <option value="1" <?= ($r['use_logo_in_login'] == "1" || $r['use_main_logo_in_login'] == "1" ? "selected" : "") ?>>Utilizza logo principale sito</option>
                                        <option value="2" <?= ($r['use_logo_in_login'] == "2" ? "selected" : "") ?>>Utilizza logo login</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Registrazione</h1>
                        <fieldset>
                            <h2 class="title-divider">Registrazione Utenti</h2>
                            <?php
                            $registration_settings = json_decode($r['registration'], true);
                            $registration_settings_display = ($registration_settings['enable'] == "1" ? "block" : "none");
                            ?>

                            <div class="alert alert-info">
                                Queste impostazioni si riferiscono alla registrazione degli Utenti, intesi come staff del sito web.
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selezionando questa casella, chiunque potrà registrarsi al portale"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_registration" <?php echo ($registration_settings['enable'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita registrazione</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3 registration_settings" style="display: <?= $registration_settings_display ?>">
                                    <label>Livello di default*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Gli utenti appena registrati acquisiranno il livello selezionato in questo campo (se non da loro selezionabile)"></i></span>
                                    <select id="default_level" class="form-control">
                                        <?php
                                        foreach((new \MSFramework\users())->getUserLevels('', true) as $levelK => $levelV) {
                                        ?>
                                            <option value="<?= $levelK ?>" <?= ($registration_settings['default_level'] == $levelK ? "selected" : "") ?>><?= $levelV ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-3 registration_settings" style="display: <?= $registration_settings_display ?>">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="L'account dell'utente sarà attivato solo a seguito della conferma dell'indirizzo email"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="request_mail_confirm" <?php echo ($registration_settings['request_mail_confirm'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Richiedi conferma email</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3 registration_settings" style="display: <?= $registration_settings_display ?>">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="L'account dell'utente sarà attivato solo a seguito di una verifica manuale di un Admin"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="request_admin_confirm" <?php echo ($registration_settings['request_admin_confirm'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Richiedi conferma Admin</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row registration_settings" style="display: <?= $registration_settings_display ?>">
                                <div class="col-sm-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="can_choose_level" <?php echo ($registration_settings['can_choose_level'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Consenti selezione livello</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-8 registration_exclude_level_settings" style="display: <?= ($registration_settings['can_choose_level'] == "1" ? "block" : "none") ?>">
                                    <label>Escludi livelli dalla selezione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Gli utenti che si stanno registrando potranno utilizzare solo i livelli non impostati in questa lista."></i></span>
                                    <select id="exclude_levels" class="form-control" multiple>
                                        <?php
                                        foreach((new \MSFramework\users())->getUserLevels('', true) as $levelK => $levelV) {
                                            ?>
                                            <option value="<?= $levelK ?>" <?= (in_array($levelK, $registration_settings['exclude_levels']) ? "selected" : "") ?>><?= $levelV ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <h2 class="title-divider">Registrazione Clienti</h2>
                            <?php
                            $customer_settings = ($r['customers'] && json_decode($r['customers']) ? json_decode($r['customers'], true) : array());
                            ?>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selezionando questa casella, verrà inviata un email di notifica quando un nuovo cliente si registrerà."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="customer_notification" <?php echo ($customer_settings['notify_via_mail'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Notifica registrazione via Email</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <h1>ACP</h1>
                        <fieldset>
                            <h2 class="title-divider">Impostazioni Admin Control Panel</h2>
                            <?php
                            $acp_settings = json_decode($r['acp'], true);
                            ?>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selezionando questa casella, dopo aver salvato un record, si verrà reindirizzati alla pagina precedente."></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="exit_after_save" <?php echo ($acp_settings['exit_after_save'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Esci dal record dopo il salvataggio</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <h1>Avanzate</h1>
                        <fieldset>
                            <h2 class="title-divider">Impostazioni Avanzate</h2>

                            <div class="row">
                                <div class="col-sm-3 pull-right">
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="set_offline" class="set_offline" <?php if($customer_data['set_offline'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Imposta Offline</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-danger">
                                Le seguenti impostazioni richiedono delle <b>conoscenze avanzate</b> per essere utilizzate, nel caso in cui il codice non risulti funzionante l'intero funzionamento del sito <b>potrebbe risultare alterato</b>.
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <h3>Custom CSS</h3>
                                    <div id="editor_css" style="height: 350px;"><?php echo $r['additional_css'] ?></div>
                                </div>

                                <div class="col-sm-4">
                                    <h3>Custom JS</h3>
                                    <div id="editor_js" style="height: 350px;"><?php echo $r['additional_js'] ?></div>
                                </div>

                                <div class="col-sm-4">
                                    <h3>robots.txt</h3>
                                    <div id="editor_robots" style="height: 350px;"><?php echo $r['robots_txt'] ?></div>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
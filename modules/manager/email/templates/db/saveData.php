<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$template_info = array();
if(isset($_POST['pID']) && !empty($_POST['pID'])) {
    $template_info = (new \MSFramework\emails())->getTemplates($_POST['pID'], true);
}

$can_save = true;
if($_POST['pOggetto'] == "" || $_POST['pVersioneHTML'] == "" || !$template_info) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$db_action = (!isset($template_info['data']) || $template_info['data']['origin'] == 'common' ? 'insert' : 'update');

// Creo la versione testuale estrapolando il testo dall'HTML
$html = new \Html2Text\Html2Text($_POST['pVersioneHTML']);
$versione_testuale =  $html->getText();

$array_to_save = array(
    "subject" => $_POST['pOggetto'],
    "txt" => $versione_testuale,
    "html" => $_POST['pVersioneHTML'],
    "settings" => json_encode($_POST['pSettings'])
);

if($db_action == 'insert') {
    $array_to_save['id'] = $_POST['pID'];
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == 'insert') {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO email_templates ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE email_templates SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
$(document).ready(function() {
});

function initForm() {

    html_editor = initEmailEditor('#versione_html', 'NEWSLETTER');

    $('.btn_expand').on('click', function() {
        if($('.tabs_col').css('display') != 'none') {
            $('.tabs_col').hide();
            $('.btn_expand').html('<i class="fa fa-arrow-left"></i> Mostra lista Template');
        } else {
            $('.tabs_col').show();
            $('.btn_expand').html('<i class="fa fa-arrows-alt"></i> Nascondi lista Template');
        }
    });

    $('.btn_save').on('click', function() {
        moduleSaveFunction();
    });

    $('.btn_shortcodes').on('click', function() {
        $('#shortcodes_alert').toggle();
    });

    $('.tabs_col .nav-tabs li a').on('click', function() {
        $('#select_tempate_label').html('Selezionare un template da modificare').show();
        $('#template_forms').hide();
        $('#topbtns_container').hide();
        $('.tabs_col .template_item').removeClass('font-bold');
    });

    $('.tabs_col .template_item').on('click', function() {

        if($('#tabs_col').hasClass('loading_template')) return false;

        $('#select_tempate_label').html('Carico i dati per il template selezionato...').show();
        $('#template_forms').hide();
        $('#topbtns_container').hide();
        $('.tabs_col .template_item').removeClass('font-bold');
        $(this).addClass('font-bold');

        $('.tabs_col').addClass('loading_template').css('opacity', 0.8);

        $.ajax({
            url: "ajax/getTemplate.php?p=" + $(this).data('parent') + "&i=" + $(this).data('item'),
            async: true,
            dataType: "json",
            success: function(data) {

                $('.tabs_col').removeClass('loading_template').css('opacity', 1);

                if(data.status != false) {
                    $('#topbtns_container').show();
                    $('#select_tempate_label').hide();
                    $('#template_forms').show();

                    $('.btn_expand').click();

                    initCommonInterface(data);
                } else {
                    $('#template_forms').hide();
                    $('#select_tempate_label').html('Errore durante il caricamento dei dati').show();
                }
            },
            error: function() {
                $('.tabs_col').removeClass('loading_template').css('opacity', 1);
                $('#template_forms').hide();
                $('#select_tempate_label').html('Errore durante il caricamento dei dati').show();
            }
        });
    });

}

function initCommonInterface(data) {

    $('#record_id').val(data.id);
    $('#oggetto').val(data.oggetto);
    $('#form_title').html(data.title);
    $('#shortcodes_list').html(data.shortcodes_list);

    html_editor.setHtmlContent(data.html);

    html_editor.setShortcodesList(data.shortcodes_json);
    html_editor.composeShortcodesView();

    if(data.settings.length) {
        $('#email_preferences').show().find('.email_preferences_content').html(data.settings);
    } else {
        $('#email_preferences').hide().find('.email_preferences_content').html('');
    }

}

/*
function initForm() {
    initClassicTabsEditForm();
    html_editor = initEmailEditor('#versione_html', 'NEWSLETTER');
}
*/

function moduleSaveFunction() {

    var emailSettings = {};
    $('#email_preferences .email_setting').each(function () {
        emailSettings[$(this).attr('name')] = $(this).val();
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pOggetto": $('#oggetto').val(),
            "pVersioneHTML": html_editor.getContentHtml(),
            "pSettings": emailSettings
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data.id, '', '', function () {
                    $('.tabs_col .active a').click();
                    if($('.tabs_col').css('display') == 'none') {
                        $('.btn_expand').click();
                    }
                });
            }
        }
    })
}
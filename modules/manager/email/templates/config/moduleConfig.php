<?php
/**
 * MSAdminControlPanel
 * Date: 15/09/18
 */

$module_id = 'manage_email_templates';
$module_config = $MSSoftwareModules->getModuleDetailsFromID($module_id);
$MSSoftwareModules->accessGranted($module_id, true);
$groups_info = (new \MSFramework\emails())->getTemplateGroupsStylesInfo();
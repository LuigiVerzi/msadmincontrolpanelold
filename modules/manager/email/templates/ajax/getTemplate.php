<?php
require_once('../../../../../sw-config.php');
require_once('../config/moduleConfig.php');

$emails = new \MSFramework\emails();

$template_parent = $_GET['p'];
$template_id = $_GET['i'];
$template = $emails->getTemplates($template_id, ($template_id != ''));

$ary_to_return = array(
    'status' => false
);

if($template['data']) {

    $ary_to_return = array(
        'status' => true,
        'id' => $template_id,
        'title' => $groups_info[$template_parent]['label'] . ' > ' . htmlentities($template['nome']),
        'oggetto' => htmlentities(($template['data']['subject'] ? $template['data']['subject'] : $template['subject'])),
        'txt' => $template['data']['txt'],
        'html' => $template['data']['html']
    );

    $shortcodes_list = '';
    foreach($template['shortcodes'][0] as $shortcode => $descrizione) {
        $shortcodes_list .= '<b>' . $shortcode . '</b> - ' . $descrizione . '<br>';
    }
    $ary_to_return['shortcodes_list'] = $shortcodes_list;

    $ary_to_return['shortcodes_json'] = array(
        'custom' => array_keys($template['shortcodes'][0]),
        'global' => $emails->getCommonShortcodes()[0]
    );

    $ary_to_return['settings'] = array();
    if(isset($template['settings']) && $template['settings']) {

        $current_template_settings = (json_decode($template['data']['settings']) ? json_decode($template['data']['settings'], true) : array());

        ob_start();

        echo '<div class="row">';
        foreach($template['settings'] as $setting_id => $setting_info) {

            if(isset($current_template_settings[$setting_id])) {
                $setting_info['default'] = $current_template_settings[$setting_id];
            }

            include('settingsBox.php');
        }
        echo '</div>';

        $ary_to_return['settings'] = ob_get_clean();
    }

}

die(json_encode($ary_to_return));
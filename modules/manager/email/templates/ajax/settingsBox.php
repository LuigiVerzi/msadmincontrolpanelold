<div class="col-sm-4">
    <label><?= $setting_info['label']; ?></label>

    <?php if($setting_info['type'] === 'select') { ?>
        <select id="<?= $setting_id; ?>" name="<?= $setting_id; ?>" class="form-control email_setting">
            <?php foreach($setting_info['values'] as $value => $text) { ?>
                <option value="<?= $value; ?>" <?= ($setting_info['default'] == $value ? 'selected' : ''); ?>><?= $text; ?></option>
            <?php } ?>
        </select>
    <?php } else { ?>
    <input id="<?= $setting_id; ?>" name="<?= $setting_id; ?>" class="form-control email_setting" value="<?= $setting_info['default']; ?>" type="text">
    <?php } ?>
</div>
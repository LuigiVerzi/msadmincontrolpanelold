<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$emails = new \MSFramework\emails();
$templates = $emails->getTemplates('', true);
$shortcodes_globali = (new \MSFramework\emails())->getCommonShortcodes();

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">

            </div>
        </div>

        <div class="wrapper wrapper-content">

                <div class="tabs_col">
                    <div class="tabs-container">

                        <div class="tabs" style="margin-bottom: 15px;">
                            <ul class="nav nav-tabs" style="width: 100% !important;">
                                <?php
                                foreach($templates as $template_group => $templates_list) {
                                    if ($template_group == 'main' || $MSFrameworkCMS->checkExtraFunctionsStatus($template_group)) {
                                        echo '<li class="' . ($template_group == 'main' ? 'active' : '') . '" ><a data-toggle="tab" href="#tab-' . $template_group . '"><i style="width: 24px; text-align: center; padding: 5px; border-radius: 3px; background: ' . $groups_info[$template_group]['background'] . '; color: ' . $groups_info[$template_group]['color'] . ';" class="fa ' . $groups_info[$template_group]['icon'] . '"></i>' . $groups_info[$template_group]['label'] . '</a></li>';
                                    }
                                }
                                ?>
                            </ul>

                            <div class="tab-content">
                                <?php
                                foreach($templates as $template_group => $templates_list) {
                                    if ($template_group == 'main' || $MSFrameworkCMS->checkExtraFunctionsStatus($template_group)) {
                                        echo '<div id="tab-' . $template_group . '" class="tab-pane ' . ($template_group == 'main' ? 'active' : '') . '"><div class="panel-body" style="width: 100% !">';
                                        foreach ($templates_list as $templates_id => $template_info) {
                                            echo '<div class="template_item" data-parent="' . $template_group . '" data-item="' . $templates_id . '">' . (!$template_info['data'] ? '<i class="fa fa-warning"></i> ' : '') . $template_info['nome'] . '<span style="float: right"><small style="display: block;margin: -5px 0;"><b>Ultimo Aggiornamento:</b><br>' . ($template_info['data'] && $template_info['data']['origin'] == 'custom' ? (new \MSFramework\utils())->smartdate(strtotime($template_info['data']['last_update'])) : 'Mai') . '</small> </span></div>';
                                        }
                                        echo '</div></div>';
                                    }
                                }
                                ?>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="content_col">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content p-md">
                            <div id="topbtns_container" style="background: #fafafa; padding: 15px;margin: -15px -15px 30px;border-bottom: 1px solid #ececec;height: 60px;display: none;">
                                <div class="ms-label-tooltip" style="margin-left: 15px;"><a class="btn btn-primary btn-sm btn_save"> <i class="fa fa-floppy-o"></i> Salva Modifiche </a></div>
                                <div class="ms-label-tooltip" style="margin-left: 15px;"><a class="btn btn-default btn-sm btn_shortcodes"><i class="fa fa-code"></i> Shortcodes</a></div>
                                <div class="ms-label-tooltip" style="float: left;"><a class="btn btn-default btn-sm btn_expand"> <i class="fa fa-arrows-alt"></i> Nascondi lista Tempate </a></div>
                                <div style="clear: both;" class="m-b-md"></div>
                            </div>

                            <div id="template_container">
                                <div class="text-center" id="select_tempate_label">Selezionare un template da modificare</div>

                                <div id="template_forms" style="display: none;">

                                    <h2 class="title-divider m-b-lg" style="margin-top: -31px;" id="form_title"></h2>

                                    <div class="alert alert-info" id="shortcodes_alert" style="padding: 10px; display: none;">
                                        <h3>Lista shortcodes Specifici</h3>
                                        <p id="shortcodes_list">

                                        </p>
                                        <hr>
                                        <h3>Lista shortcodes Globali</h3>
                                        <p>
                                            <?php foreach($shortcodes_globali[0] as $k => $shortcode) { ?>
                                                <b><?= $shortcode; ?></b> - <?= $shortcodes_globali[1][$k]; ?><br>
                                            <?php } ?>
                                        </p>
                                    </div>

                                    <input type="hidden" id="record_id" value="" />

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Oggetto della mail</label>
                                            <input id="oggetto" name="oggetto" class="form-control required" value="" type="text" required>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Versione HTML</label>
                                            <div id="versione_html" name="versione_html"></div>
                                        </div>
                                    </div>

                                    <div id="email_preferences" style="display: none;">
                                        <h2 class="title-divider">Preferenze Email</h2>
                                        <div class="email_preferences_content">

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
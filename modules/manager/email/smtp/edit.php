<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */ ?>

<?php
require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = array();
if(isset($_GET['id']) && !empty($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM email_config WHERE id = :id", array(':id' => $_GET['id']), true);
    if($r) {
        $r['data'] = json_decode($r['data'], true);
    }
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php
                $customToolbarButton = '<a class="btn btn-info" id="checkSMTPConnection" checking_txt="Verifico connessione...">Verifica connessione</a>';
                require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php");
                ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>In Uscita</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Nome Connessione*</label>
                                    <input id="name" name="name" class="form-control required" value="<?php echo $r['data']['name'] ?>" type="text" required>
                                </div>

                                <div class="col-sm-4">
                                    <label>Server</label>
                                    <input id="host" name="host" class="form-control required" placeholder="Es: smtp.office365.com" value="<?php echo $r['data']['host'] ?>" type="text" required>
                                </div>

                                <div class="col-sm-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_mailbox" <?php if($r['enable_mailbox'] == "1") { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita MailBox</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>SMTP Login</label>
                                    <input id="login" name="login" class="form-control required" value="<?php echo $r['data']['login'] ?>" type="text" required>
                                </div>

                                <div class="col-sm-4">
                                    <label>SMTP Password</label>
                                    <input id="password" name="password" class="form-control required" value="<?php echo $r['data']['password'] ?>" type="text" required>
                                </div>

                                <div class="col-sm-2">
                                    <label>Crittografia</label>
                                    <select id="smtpsecure" class="form-control required">
                                        <option value="">Nessuna</option>
                                        <option value="ssl" <?php if($r['data']['smtpsecure'] == "ssl") { echo "selected"; } ?>>SSL</option>
                                        <option value="tls" <?php if($r['data']['smtpsecure'] == "tls") { echo "selected"; } ?>>TLS</option>
                                    </select>
                                </div>

                                <div class="col-sm-2">
                                    <label>Porta</label>
                                    <input id="port" name="port" class="form-control" value="<?php echo $r['data']['port'] ?>" type="text" required>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Server in Entrata</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Server</label>
                                    <input id="bmh_server" name="bmh_server" class="form-control required" placeholder="Es: outlook.office365.com" value="<?php echo $r['data']['bmh_server'] ?>" type="text">
                                </div>

                                <div class="col-sm-4">
                                    <label>Indirizzo Email</label>
                                    <input id="username" name="username" class="form-control required" value="<?php echo $r['data']['username'] ?>" type="email">
                                </div>

                                <div class="col-sm-4">
                                    <label>Password*</label>
                                    <input id="bmh_password" name="bmh_password" class="form-control required" value="<?php echo $r['data']['bmh_password'] ?>" type="text">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Protocollo</label>
                                    <select id="bmh_service" class="form-control">
                                        <option value="imap" <?php if($r['data']['bmh_service'] == "imap") { echo "selected"; } ?>>IMAP</option>
                                        <option value="pop3" <?php if($r['data']['bmh_service'] == "pop3") { echo "selected"; } ?>>POP3</option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <label>Porta</label>
                                    <input id="bmh_port" name="bmh_port" class="form-control" value="<?php echo $r['data']['bmh_port'] ?>" type="text" required>
                                </div>

                                <div class="col-sm-4">
                                    <label>Crittografia</label>
                                    <select id="bmh_soption" class="form-control">
                                        <option value="">Nessuna</option>
                                        <option value="tls" <?php if($r['data']['bmh_soption'] == "tls") { echo "selected"; } ?>>TLS</option>
                                        <option value="notls" <?php if($r['data']['bmh_soption'] == "notls") { echo "selected"; } ?>>NOTLS</option>
                                        <option value="ssl/novalidate-cert" <?php if($r['data']['bmh_soption'] == "ssl/novalidate-cert") { echo "selected"; } ?>>SSL</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
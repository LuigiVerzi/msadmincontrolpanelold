<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */
require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

require_once(FRAMEWORK_ABSOLUTE_PATH . 'PHPMailer/phpmailer.php');
require_once(FRAMEWORK_ABSOLUTE_PATH . 'PHPMailer/phpmailer.smtp.php');

$mail = new \PHPMailer;
//$mail->SMTPDebug = 3;

if (!empty($_POST['smtpsecure'])) {
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
}

$mail->isSMTP();

$mail->Host = $_POST['host'];
$mail->Port = $_POST['port'];
$mail->SMTPAuth = true;
$mail->AuthType = 'LOGIN';
$mail->Username = $_POST['login'];
$mail->Password = $_POST['password'];

$mail->SMTPSecure = $_POST['smtpsecure'];

$mail->Timeout = 5;

if($mail->smtpConnect()) {
    $status = "ok";
} else {
    $status = "smtp_err";
}

if($status !== 'ok') die($status);

if(!(new \MSFramework\mailBox())->getImapConnection($_POST)) {
    $status = "imap_err";
}

die($status);

?>

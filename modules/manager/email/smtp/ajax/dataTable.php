<?php
/**
 * MSAdminControlPanel
 * Date: 27/04/19
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM email_config") as $r) {
    $array['data'][] = array(
        $r['id'],
        json_decode($r['data'], true)['name'] . ($r['is_default'] == "1" ? '<span class="label label-priamary">Predefinito</span>' : ''),
        json_decode($r['data'], true)['host'],
        ($r['enable_mailbox'] == "1") ? "Si" : "No",
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

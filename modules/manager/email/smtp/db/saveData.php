<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$can_save = true;

if($_POST['pServerData']['name'] == "" || $_POST['pServerData']['host'] == "" || $_POST['pServerData']['username'] == "" || $_POST['pServerData']['smtpsecure'] == "" || $_POST['pServerData']['port'] == "" || $_POST['pServerData']['bmh_password'] == "" || $_POST['pServerData']['bmh_server'] == "" || $_POST['pServerData']['login'] == "" || $_POST['pServerData']['password'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$mail_post_names = array('plogin', 'pusername');
$mail_conflict = false;

foreach($mail_post_names as $mailpost) {
    if($_POST[$mailpost] != "") {
        if (!filter_var($_POST[$mailpost], FILTER_VALIDATE_EMAIL)) {
            $mail_conflict = true;
            break;
        }
    }
}

if($mail_conflict) {
    echo json_encode(array("status" => "mail_not_valid"));
    die();
}

if(!$MSFrameworkDatabase->getCount("SELECT * FROM email_config WHERE id = :id", array(':id' => $_POST['pID']))) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

$array_to_save = array(
    "data" => json_encode($_POST['pServerData']),
    "enable_mailbox" => $_POST['pEnableMailBox']
);


$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO email_config ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE email_config SET $stringForDB[1] WHERE id = :id", array_merge($stringForDB[0], array(':id' => $_POST['pID'])));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$status = $MSFrameworkDatabase->deleteRow("email_config", "id", $_POST['pID']);
foreach($_POST['pID'] as $server_id) $MSFrameworkDatabase->query("DELETE FROM mailbox__attachments WHERE email_id IN (SELECT id FROM mailbox__messages WHERE server = :server)", array(':server' => $server_id));
$MSFrameworkDatabase->deleteRow("mailbox__messages", "server", $_POST['pID']);

if($status) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>

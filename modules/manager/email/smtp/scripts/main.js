$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('#checkSMTPConnection').on('click', function() {
        original_save_value = $('#checkSMTPConnection').html();
        $('#checkSMTPConnection').html($('#checkSMTPConnection').attr('checking_txt')).addClass('disabled').attr('disabled', 'disabled');
        testSMTPConnection(false);
        $('#checkSMTPConnection').html(original_save_value).removeClass('disabled').attr('disabled', false);
    });
}

function testSMTPConnection(from_save) {
    to_return = false;

    $.ajax({
        url: "ajax/checkSMTP.php",
        method: "POST",
        async: true,
        data: getServerData()['pServerData'],
        preloader: 'Verifica della connessione in corso',
        success: function(data) {
            if(data == "ok") {
                to_return = true;
                if(!from_save) {
                    bootbox.alert("La connessione è stata stabilita correttamente.");
                } else {
                    moduleSaveFunction(true);
                }
            } else {

                if(data == "smtp_err") {
                    bootbox.error("Impossibile connettersi al server In Uscita. Verificare i dati e riprovare!");
                } else {
                    bootbox.error("Impossibile connettersi al server In Entrata. Verificare i dati e riprovare!");
                }

                to_return = false;
            }
        }
    });

    return to_return;
}

function moduleSaveFunction(from_check) {

    if(typeof(from_check) === 'undefined') from_check = false;

    if(!from_check) {
        testSMTPConnection(true);
        return false;
    }

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: getServerData(),
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido o è già stata utilizzata da un altro cliente!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}

function getServerData() {
    return {
        "pID": $('#record_id').val(),
        "pServerData": {
            "name": $('#name').val(),
            "host": $('#host').val(),
            "verifypeer": $('#verifypeer:checked').length,
            "login": $('#login').val(),
            "password": $('#password').val(),
            "username": $('#username').val(),
            "smtpsecure": $('#smtpsecure').val(),
            "port": $('#port').val(),
            "bmh_server": $('#bmh_server').val(),
            "bmh_port": $('#bmh_port').val(),
            "bmh_service": $('#bmh_service').val(),
            "bmh_soption": $('#bmh_soption').val(),
            "bmh_password": $('#bmh_password').val()
        },
        "pEnableMailBox": $('#enable_mailbox:checked').length
    };
}
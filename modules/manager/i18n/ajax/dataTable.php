<?php
/**
 * MSAdminControlPanel
 * Date: 08/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$count = 0;
foreach($MSFrameworki18n->getCurTranslationsAry() as $k => $v) {
    $array['data'][] = array(
        '<span class="original">' . $v->getOriginal() . "</span>",
        '<span class="editable"><a href="#" data-type="text" data-pk="' . $count . '" data-original="' . $v->getOriginal() . '">' . $v->getTranslation() . "</a></span>",
    );

    $count++;
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

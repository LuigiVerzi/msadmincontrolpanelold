$(document).ready(function() {
    $.ajax({
        url: 'ajax/checkForNewStrings.php',
        async: false,
        success: function(data) {
            $('#running_sync').hide();
            $('#main_table_list').show();

            $('#main_table_list').DataTable({
                "ajax": 'ajax/dataTable.php',
                "fnDrawCallback": function( oSettings ) {
                   initEditable();
                },
                "language": {
                    "url": $('#baseElementPathAdmin').val() + "vendor/plugins/dataTables/locales/it.json"
                }
            });

            $('#main_table_list').on('init.dt', function() {
                initEditable();
            });
        }
    })
});

function initEditable() {
    $('#main_table_list .editable a').editable({
        title: 'Inserisci traduzione',
        emptytext: 'Inserisci traduzione',
        url: 'ajax/updatePoFile.php',
        params: function(params) {
            params.original_value = $('a[data-pk=' + params.pk + ']').closest('tr').find('.original').text();

            return params;
        }
    });
}
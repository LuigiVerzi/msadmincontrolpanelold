<?php
/**
 * MSAdminControlPanel
 * Date: 08/04/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div id="running_sync">
                    <h5>Sincronizzo le stringhe con il frontoffice</h5>
                    <div class="progress progress-striped active">
                        <div style="width: 75%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-success">
                        </div>
                    </div>
                </div>

                <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="display: none;">
                    <thead>
                    <tr>
                        <th style="width: 50%;">Stringa Originale</th>
                        <th style="width: 50%;">Stringa Tradotta (<?php echo $MSFrameworki18n->getLanguagesDetails(USING_LANGUAGE_CODE, 'italian_name')[USING_LANGUAGE_CODE]['italian_name'] ?>)</th>
                    </tr>
                    </thead>
                </table>

            </div>
        </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 27/04/19
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$site_url = $MSFrameworkCMS->getURLToSite();
foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM redirect") as $r) {
    $array['data'][] = array(
        $site_url.$r['url_from'],
        $r['url_to'],
        $r['redirect_type'],
        ($r['active'] == "1") ? "Si" : "No",
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

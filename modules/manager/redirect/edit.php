<?php
/**
 * MSAdminControlPanel
 * Date: 27/04/19
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM redirect WHERE id = :id", array($_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-5">
                                    <label>URL di Origine*</label>
                                    <div class="input-group m-b">
                                        <span class="input-group-addon"><?= (new \MSFramework\cms())->getURLToSite() ?></span>
                                        <input id="from" name="from" type="text" class="form-control required" value="<?php echo htmlentities($r['url_from']) ?>">
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>URL di Destinazione*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Per effettuare un redirect esterno, specificare l'URL comprensivo di protocollo (es: http://www.example.com). Tutti gli altri casi verranno trattati come redirect interni."></i></span>
                                    <input id="to" name="to" type="text" class="form-control required" value="<?php echo htmlentities($r['url_to']) ?>">
                                </div>

                                <div class="col-sm-2">
                                    <label>Tipologia*</label>
                                    <select id="type" class="form-control required">
                                        <option value="301" <?php if($r['redirect_type'] == "301") { echo "selected"; } ?>>Redirect 301</option>
                                        <option value="302" <?php if($r['redirect_type'] == "302") { echo "selected"; } ?>>Redirect 302</option>
                                    </select>
                                </div>

                                <?php
                                $is_active_check = "checked";
                                if($r['active'] == 0 && $_GET['id'] != "") {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-2">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 27/04/19
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->deleteRow("redirect", "id", $_POST['pID'])) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


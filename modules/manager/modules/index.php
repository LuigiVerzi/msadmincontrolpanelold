<?php
/**
 * MSAdminControlPanel
 * Date: 26/05/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('modules_settings');
$r_settings = json_decode($r['settings'], true);
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <?php
                        $has_tab = false;
                        foreach((new \MSFramework\modules())->getModules() as $moduleK => $moduleV) {

                            $module_settings = (new \MSFramework\modules())->getModuleSettings($moduleK);

                            if(!(new \MSFramework\modules())->checkIfIsActive($moduleK) || !$module_settings) {
                                continue;
                            }

                            $has_tab = true;
                        ?>
                            <h1><?php echo $moduleV['name'] ?></h1>
                            <fieldset data-module-id="<?php echo $moduleK ?>">
                                <div class="row">
                                    <?php
                                    $count = 0;
                                    foreach((new \MSFramework\modules())->getModuleSettings($moduleK) as $settingK => $settingV) {

                                        $field_value = (new \MSFramework\modules())->getSettingValue($moduleK, $settingK);

                                        $html_helper = "";
                                        if($settingV['helper'] != "") {
                                            $html_helper = ' <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . $settingV['helper'] . '"></i></span>';
                                        }

                                        $html_translations = "";
                                        if($settingV['translations']) {
                                            $html_translations = ' <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . TRANSLATE_ICON_TOOLTIP . '" data-translations="' . htmlentities($field_value) . '"></i></span>';
                                            $field_value = $MSFrameworki18n->getFieldValue($field_value);
                                        }

                                        $input_attributes = "";
                                        if(is_array($settingV['input_attributes'])) {
                                            foreach($settingV['input_attributes'] as $inAttrK => $inAttrV) {
                                                $input_attributes .= " " . $inAttrK . "=" . $inAttrV . " ";
                                            }
                                        }

                                        $check_to_show = "";
                                        if(is_array($settingV['check_to_show']) && count($settingV['check_to_show'])) {
                                            $check_to_show = 'data-check_to_show="' . implode(' ', $settingV['check_to_show']) . '"';
                                        }

                                        if($settingV['input_type'] == "text" || $settingV['input_type'] == "number") {
                                        ?>
                                        <div class="col-sm-<?= (isset($settingV['input_column']) ? $settingV['input_column'] : '6'); ?> m-b" <?= $check_to_show; ?>>
                                            <label><?php echo $settingV['name'] ?><?php echo ($settingV['mandatory']) ? "*" : "" ?></label><?php echo $html_helper . $html_translations ?>
                                            <input id="<?php echo $moduleK . '_' . $settingK ?>" data-module-id="<?php echo $moduleK ?>" data-setting-id="<?php echo $settingK ?>" type="<?php echo $settingV['input_type'] ?>" class="<?php echo $moduleK  ?>-setting form-control <?php echo ($settingV['mandatory']) ? "required" : "" ?> <?php echo $input_classes ?>" value="<?php echo $field_value ?>" <?php echo $input_attributes ?>>
                                        </div>
                                    <?php } else if($settingV['input_type'] == "select") {
                                        ?>
                                        <div class="col-sm-<?= (isset($settingV['input_column']) ? $settingV['input_column'] : '6'); ?> m-b" <?= $check_to_show; ?>>
                                            <label><?php echo $settingV['name'] ?><?php echo ($settingV['mandatory']) ? "*" : "" ?></label><?php echo $html_helper . $html_translations ?>
                                            <select id="<?php echo $moduleK . '_' . $settingK ?>" data-module-id="<?php echo $moduleK ?>" data-setting-id="<?php echo $settingK ?>" type="<?php echo $settingV['input_type'] ?>" class="<?php echo $moduleK  ?>-setting form-control <?php echo ($settingV['mandatory']) ? "required" : "" ?> <?php echo $input_classes ?>" <?php echo $input_attributes ?>>
                                            <?php foreach($settingV['input_values'] as $input_value => $input_text) { ?>
                                                <option value="<?= $input_value; ?>" <?= ($field_value == $input_value ? 'selected' : ''); ?>><?= $input_text; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    <?php } else if($settingV['input_type'] == "conditions") {
                                        ?>
                                        <div class="col-sm-<?= (isset($settingV['input_column']) ? $settingV['input_column'] : '6'); ?> m-b" <?= $check_to_show; ?>>
                                            <label><?php echo $settingV['name'] ?><?php echo ($settingV['mandatory']) ? "*" : "" ?></label><?php echo $html_helper . $html_translations ?>
                                            <textarea id="<?php echo $moduleK . '_' . $settingK ?>" data-module-id="<?php echo $moduleK ?>" data-setting-id="<?php echo $settingK ?>" type="<?php echo $settingV['input_type'] ?>" class="<?php echo $moduleK  ?>-setting form-control query-builder <?php echo ($settingV['mandatory']) ? "required" : "" ?> <?php echo $input_classes ?>" <?php echo $input_attributes ?>><?= $field_value; ?></textarea>
                                        </div>
                                    <?php } else if($settingV['input_type'] == "checkbox") { ?>
                                        <div class="col-sm-<?= (isset($settingV['input_column']) ? $settingV['input_column'] : '6'); ?> m-b" <?= $check_to_show; ?>>
                                            <label>&nbsp;</label> <?php echo $html_helper ?>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="<?php echo $moduleK . '_' . $settingK ?>" data-module-id="<?php echo $moduleK ?>" data-setting-id="<?php echo $settingK ?>" <?php echo ($field_value) ? "checked" : "" ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;"><?php echo $settingV['name'] ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    <?php }  else if($settingV['input_type'] == "image") { ?>
                                        <div class="col-sm-<?= (isset($settingV['input_column']) ? $settingV['input_column'] : '6'); ?> m-b" <?= $check_to_show; ?>>
                                            <input type="hidden" id="<?php echo $moduleK . '_' . $settingK ?>_max_uploads" value="<?php echo ($settingV['uploader_settings']['max_uploads'] != "" ? $settingV['uploader_settings']['max_uploads'] : "1") ?>" />
                                            <input type="hidden" id="<?php echo $moduleK . '_' . $settingK ?>_min_width" value="<?php echo ($settingV['uploader_settings']['min_width'] != "" ? $settingV['uploader_settings']['min_width'] : "250") ?>" />
                                            <input type="hidden" id="<?php echo $moduleK . '_' . $settingK ?>_min_height" value="<?php echo ($settingV['uploader_settings']['min_height'] != "" ? $settingV['uploader_settings']['min_height'] : "250") ?>" />
                                            <input type="hidden" id="<?php echo $moduleK . '_' . $settingK ?>_tn_width" value="<?php echo ($settingV['uploader_settings']['tn_width'] != "" ? $settingV['uploader_settings']['tn_width'] : "100") ?>" />

                                            <label><?php echo $settingV['name'] ?></label>

                                            <?php
                                            (new \MSFramework\uploads('MODULES'))->initUploaderHTML($moduleK . '_' . $settingK, json_encode($field_value), array(), array('element' => array("setting-id" => $settingK)));
                                            ?>
                                        </div>
                                    <?php
                                        } else if($settingV['input_type'] == "title") {
                                    ?>
                                    <div class="col-sm-12">
                                        <?php if($count > 0) { ?>
                                        <div class="hr-line-dashed"></div>
                                        <?php } ?>
                                        <h2 class="title-divider"><?= $settingV['name']; ?></h2>
                                    </div>
                                    <?php }
                                        $count++;
                                    }
                                    ?>

                                </div>

                                <div class="hr-line-dashed"></div>
                            </fieldset>
                        <?php } ?>

                        <?php
                        if(!$has_tab) {
                        ?>
                        <h1>Moduli Extra</h1>
                        <fieldset>
                            <div class="alert alert-info">
                                I moduli extra ti consentono di aggiungere funzionalità extra al tuo sito. Per il momento, non è stato abilitato alcun modulo extra per il tuo sito. Torna in questo modulo quando ti verrà abilitato almeno un modulo extra per poterlo configurare!
                            </div>
                        </fieldset>
                        <?php
                        }
                        ?>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<textarea id="query_filters" style="display: none;"> <?= json_encode((new \MSFramework\Newsletter\conditions())->getConditionsQueryFilters()); ?></textarea>
<textarea id="query_operators" style="display: none;"> <?= json_encode((new \MSFramework\Newsletter\conditions())->getConditionsQueryOperators()); ?></textarea>

<script>
    
    globalInitForm();
</script>
</body>
</html>
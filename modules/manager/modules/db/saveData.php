<?php
/**
 * MSAdminControlPanel
 * Date: 26/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'modules_settings'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkCMS->getCMSData('modules_settings');

    $r_old_data_settings = json_decode($r_old_data['settings'], true);
}

$uploader = new \MSFramework\uploads('MODULES');

$old_img = array();
$modules = (new \MSFramework\modules())->getModules();
foreach($_POST['pSettings'] as $moduleK => $moduleV) {
    $module_settings = (new \MSFramework\modules())->getModuleSettings($moduleK);

    foreach($moduleV as $settingK => $settingV) {
        $cur_settigs = $module_settings[$settingK];
        $r_old_data_for_module = $r_old_data_settings[$moduleK][$settingK];

        if($cur_settigs['mandatory'] && $settingV == "") {
            echo json_encode(array("status" => "mandatory_data_missing"));
            die();
        }

        if($cur_settigs['translations']) {
            if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                    array('currentValue' => $settingV, 'oldValue' => $r_old_data_for_module),
                )) == false) {
                echo json_encode(array("status" => "no_datalang_for_primary"));
                die();
            }

            $_POST['pSettings'][$moduleK][$settingK] = $MSFrameworki18n->setFieldValue($r_old_data_for_module, $settingV);
        }

        if($cur_settigs['input_type'] == "image") {
            if(is_array($r_old_data_for_module)) {
                $old_img = array_merge($old_img, $r_old_data_for_module);
            }

            $ary_files = $uploader->prepareForSave($settingV);
            if($ary_files === false) {
                echo json_encode(array("status" => "mv_error"));
                die();
            }
        }
    }
}

$array_to_save = array(
    "value" => json_encode(array(
        "settings" => json_encode($_POST['pSettings'])
    )),
    "type" => "modules_settings"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'modules_settings'", $stringForDB[0]);
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, $old_img);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
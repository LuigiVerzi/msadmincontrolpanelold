function initForm() {
    initClassicTabsEditForm();

    $('div[orakuploader="on"]').each(function() {
        orak_id = $(this).attr('id');
        initOrak(orak_id, $('#' + orak_id + "_max_uploads").val(), $('#' + orak_id + "_min_width").val(), $('#' + orak_id + "_min_height").val(), $('#' + orak_id + "_tn_width").val(), getOrakImagesToPreattach(orak_id), false, ['image/jpeg', 'image/png']);
    });

    checkInputToShow();
    $('input[type="checkbox"]').on('ifChanged', function() {
        checkInputToShow();
    });
    $('select').on('change', function() {
        checkInputToShow();
    });

    if($('.query-builder').length) {
        var queryFilters = JSON.parse($('#query_filters').val());

        window.displaySyncError = false;

        $('.query-builder').each(function () {
            var $textarea = $(this);
            var $queryDiv = $('<div class=""></div>');

            $queryDiv.queryBuilder({
                plugins: {
                    'bt-tooltip-errors': {},
                    'bt-selectpicker': {},
                    'filter-description': {}
                },
                lang_code: 'it',
                operators: JSON.parse($('#query_operators').val()),
                filters: queryFilters
            }).on('change', function (e) {
                $textarea.val(JSON.stringify($queryDiv.queryBuilder("getRules")));
            });

            var rules = [];

            try {
                rules = JSON.parse($textarea.val());
            } catch (e) {}

            if(typeof(rules) !== 'object' || rules == null) {
                rules = [];
            }

            if (Object.keys(rules).length) {
                $queryDiv.queryBuilder('setRules', rules);
            }

            $queryDiv.insertAfter($textarea);
            $textarea.hide();
        });
    }
}

function moduleSaveFunction() {
    module_settings = new Object;
    $('fieldset').each(function() {
        module_id = $(this).attr('data-module-id');
        module_settings[module_id] = new Object;

        $(this).find('input,select,textarea').each(function() {
            setting_id = $(this).attr('data-setting-id');

            if($(this)[0].tagName === 'TEXTAREA' || $(this)[0].tagName === 'SELECT' || $(this).attr('type') == "text" || $(this).attr('type') == "number") {
                module_settings[module_id][setting_id] = $(this).val()
            } else if($(this).attr('type') == "checkbox") {
                module_settings[module_id][setting_id] = $(this).is(":checked");
            } else if($(this).attr('type') == "hidden") {
                if($(this).closest('div').hasClass('multibox') && $(this).closest('div').hasClass('file')) {
                    //è un uploader
                    orak_id = $(this).closest('.ui-sortable').attr('id');
                    module_settings[module_id][orak_id.replace(module_id + "_", '')] = new Array();

                    $('input[name^=' + orak_id + ']').each(function(k, v) {
                        module_settings[module_id][orak_id.replace(module_id + "_", '')].push($(this).val());
                    })
                }
            }
        })
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pSettings": module_settings,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}

function checkInputToShow() {
    $('[data-check_to_show]').each(function () {

        var input_to_check = $(this).data('check_to_show').split(" ");
        var show_input = false;

        input_to_check.forEach(function (id) {
            if($('[data-setting-id="' + id + '"]')[0].tagName === 'INPUT' && $('[data-setting-id="' + id + '"]').prop('checked')) {
                show_input = true;
            } else if($('[data-setting-id="' + id + '"]')[0].tagName === 'SELECT' && $('[data-setting-id="' + id + '"]').val() != "0") {
                show_input = true;
            }
        });
        if(!show_input) $(this).hide();
        else $(this).show();
    });
}
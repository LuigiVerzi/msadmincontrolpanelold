$(document).ready(function() {
    loadStandardDataTable('main_table_list', false, {
        dom: 'Blfrtip',
        buttons: [ 'csv', 'excel', 'pdf', 'print']
    });
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
}
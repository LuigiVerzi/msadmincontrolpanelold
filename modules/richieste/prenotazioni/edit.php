<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM prenotazioni WHERE id = :id", array($_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <?php

                        //OTTENGO LA REFERENZA
                        $referenza = json_decode($r['referenza'], true);
                        $extra_functions_db = json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true);

                        if (in_array("camping", $extra_functions_db)) {

                            $piazzolaClass = new MSFramework\Camping\piazzole();
                            $offerClass = new MSFramework\Camping\offers();

                            if($referenza['origin'] == 'piazzola') {
                                $referenza['piazzola'] = $piazzolaClass->getPiazzolaDetails($referenza['reference'])[$referenza['reference']];
                                $referenza['piazzola']['url'] = $MSFrameworkCMS->getURLToSite() . str_replace(ABSOLUTE_SW_PATH_HTML, '', $piazzolaClass->getURL($referenza['reference']));
                            }
                            else if($referenza['origin'] == 'offer') {
                                $referenza['offer'] = $offerClass->getOfferDetails($referenza['reference'])[$referenza['reference']];
                                $referenza['offer']['url'] = $MSFrameworkCMS->getURLToSite() . str_replace(ABSOLUTE_SW_PATH_HTML, '', $offerClass->getURL($referenza['reference']));
                            }
                            else if($referenza['origin'] == 'piazzola_with_offer') {
                                $referenza['offer'] = $offerClass->getOfferDetails($referenza['offer_reference'])[$referenza['offer_reference']];
                                $referenza['offer']['url'] = $MSFrameworkCMS->getURLToSite() . str_replace(ABSOLUTE_SW_PATH_HTML, '', $offerClass->getURL($referenza['offer_reference']));

                                $referenza['piazzola'] = $piazzolaClass->getPiazzolaDetails($referenza['piazzola_reference'])[$referenza['piazzola_reference']];
                                $referenza['piazzola']['url'] = $MSFrameworkCMS->getURLToSite() .str_replace(ABSOLUTE_SW_PATH_HTML, '',  $piazzolaClass->getURL($referenza['piazzola_reference']));
                            }

                        }
                        if (in_array("hotel", $extra_functions_db)) {

                            $roomClass = new MSFramework\Hotels\rooms();
                            $offerClass = new MSFramework\Hotels\offers();

                            if($referenza['origin'] == 'room') {
                                $referenza['room'] = $roomClass->getRoomDetails($referenza['reference'])[$referenza['reference']];
                                $referenza['room']['url'] = $MSFrameworkCMS->getURLToSite() . str_replace(ABSOLUTE_SW_PATH_HTML, '', $roomClass->getURL($referenza['reference']));
                            }
                            else if($referenza['origin'] == 'offer') {
                                $referenza['offer'] = $offerClass->getOfferDetails($referenza['reference'])[$referenza['reference']];
                                $referenza['offer']['url'] = $MSFrameworkCMS->getURLToSite() . str_replace(ABSOLUTE_SW_PATH_HTML, '', $offerClass->getURL($referenza['reference']));
                            }
                            else if($referenza['origin'] == 'room_with_offer') {
                                $referenza['offer'] = $offerClass->getOfferDetails($referenza['offer_reference'])[$referenza['offer_reference']];
                                $referenza['offer']['url'] = $MSFrameworkCMS->getURLToSite() . str_replace(ABSOLUTE_SW_PATH_HTML, '', $offerClass->getURL($referenza['offer_reference']));

                                $referenza['room'] = $roomClass->getRoomDetails($referenza['room_reference'])[$referenza['room_reference']];
                                $referenza['room']['url'] = $MSFrameworkCMS->getURLToSite() . str_replace(ABSOLUTE_SW_PATH_HTML, '', $roomClass->getURL($referenza['room_reference']));
                            }
                        }
                        // FORMATTO LE DATE
                        $arrivo = new DateTime($r['arrivo']);
                        $partenza = new DateTime($r['partenza']);
                        $notti = $partenza->diff($arrivo)->format("%a");
                        ?>
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Dati Prenotazione</h2>
                            <div class="row">

                                <div class="col-sm-3">
                                    <label>Oggetto della prenotazione</label> <br />
                                    <?php if (in_array("camping", $extra_functions_db)) { ?>
                                        <?php if($referenza['piazzola']) {?>
                                            <i>Piazzola: </i><a href="<?= $referenza['piazzola']['url']; ?>"><?= $MSFrameworki18n->getFieldValue($referenza['piazzola']['nome']); ?></a>
                                        <?php } ?>
                                        <?= ($referenza['offer'] && $referenza['piazzola'] ? '<br>' : ''); ?>
                                        <?php if($referenza['offer']) { ?>
                                            <i>Offerta: </i><a href="<?= $referenza['offer']['url']; ?>"><?= $MSFrameworki18n->getFieldValue($referenza['offer']['nome']); ?></a>
                                        <?php } ?>
                                        <?php if(!$referenza['offer'] && !$referenza['piazzola']) { ?>
                                            <i>Piazzola non specificata</i>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <?php if($referenza['room']) {?>
                                            <i>Stanza: </i><a href="<?= $referenza['room']['url']; ?>"><?= $MSFrameworki18n->getFieldValue($referenza['room']['nome']); ?></a>
                                        <?php } ?>
                                        <?= ($referenza['offer'] && $referenza['room'] ? '<br>' : ''); ?>
                                        <?php if($referenza['offer']) { ?>
                                            <i>Offerta: </i><a href="<?= $referenza['offer']['url']; ?>"><?= $MSFrameworki18n->getFieldValue($referenza['offer']['nome']); ?></a>
                                        <?php } ?>
                                        <?php if(!$referenza['offer'] && !$referenza['room']) { ?>
                                            <i>Stanza non specificata</i>
                                        <?php } ?>
                                    <?php } ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Data di Arrivo</label> <br />
                                    <?php echo $arrivo->format('d/m/Y'); ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Data di Partenza</label> <br />
                                    <?php echo $partenza->format('d/m/Y'); ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Numero di Notti</label> <br />
                                    <?= $notti; ?>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Adulti</label> <br />
                                    <?= json_decode($r['ospiti'])[0]; ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Bambini</label> <br />
                                    <?= json_decode($r['ospiti'])[1]; ?>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php if(json_decode($r['info_aggiuntive'])) { ?>
                                <h2 class="title-divider">Info Aggiuntive</h2>
                                <div class="row">
                                <?php foreach(json_decode($r['info_aggiuntive'], true) as $ad_name => $ad_val) { ?>
                                    <div class="col-sm-3">
                                        <label><?= $ad_name; ?></label> <br />
                                        <?= $ad_val; ?>
                                        <div class="hr-line-dashed"></div>
                                    </div>
                                <?php } ?>
                                </div>
                            <?php } else { ?>
                                <?php if(!empty($r['info_aggiuntive'])) { ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <?php echo str_replace("<hr>", "<div class=\"hr-line-dashed\"></div>", $r['info_aggiuntive']); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>

                            <h2 class="title-divider">Info Cliente</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nome</label> <br />
                                    <?php echo htmlentities($r['nome']); ?> <?php echo htmlentities($r['cognome']); ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Email</label> <br />
                                    <?php echo htmlentities($r['email']); ?>
                                </div>

                                <div class="col-sm-2">
                                    <label>Cellulare</label> <br />
                                   <?php echo htmlentities($r['telefono']); ?>
                                </div>

                                <div class="col-sm-2">
                                    <label>Data Creazione</label>  <br />
                                    <?php echo date("d/m/Y H:i:s", strtotime($r['creation_time'])) ?>
                                </div>

                                <div class="col-sm-2">
                                    <label>Lingua</label>  <br />
                                    <?php echo $MSFrameworki18n->getLanguagesDetails($r['lang'], "italian_name")[$r['lang']]['italian_name'] ?>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
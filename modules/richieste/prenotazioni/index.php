<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewDelGrid.php"); ?>
                <div class="btn-group pull-right" style="padding-top: 10px;" id="dataTableExport">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false">Esporta</button>
                    <ul class="dropdown-menu" x-placement="bottom-start">
                        <li><a class="dropdown-item" href="#" data-type="csv">Esporta in CSV</a></li>
                        <li><a class="dropdown-item" href="#" data-type="excel">Esporta in Excel</a></li>
                        <li><a class="dropdown-item" href="#" data-type="pdf">Esporta in PDF</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#" data-type="print">Stampa</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">

                    <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Cognome</th>
                            <th>Email</th>
                            <th>Telefono</th>
                            <th class="is_data">Date</th>
                            <th>Ospiti</th>
                            <th class="is_data default-sort" data-sort="desc">Data prenotazione</th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
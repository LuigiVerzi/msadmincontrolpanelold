<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM prenotazioni ORDER BY creation_time desc") as $r) {
    $array['data'][] = array(
        $r['nome'],
        $r['cognome'],
        $r['email'],
        $r['telefono'],
        array("display" => 'Dal <b>' .  date("d/m/Y", strtotime($r['arrivo'])) . '</b> al <b>' .date("d/m/Y", strtotime($r['partenza'])) . '</b>', "sort" => strtotime($r['arrivo'])),
        json_decode($r['ospiti'])[0] . ' adulti, ' . json_decode($r['ospiti'])[1] . ' bambini',
        array("display" => date("d/m/Y H:i", strtotime($r['creation_time'])), "sort" => strtotime($r['creation_time'])),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

$(document).ready(function() {
    loadStandardDataTable('main_table_list', false);
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
}
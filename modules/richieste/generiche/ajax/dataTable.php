<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$array = array('data' => array());

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM richieste WHERE type = 'generic' AND (owner_id = :owner OR owner_id = '') ORDER BY creation_time desc", array(":owner" => (new \MSFramework\users())->getUserDataFromSession('user_id'))) as $r) {
    $array['data'][] = array(
        htmlentities($r['nome']),
        htmlentities($r['contatto']),
        (strlen($r['messaggio']) > 120) ? htmlentities(substr($r['messaggio'], 0, 117)) . '...' : htmlentities($r['messaggio']),
        array("display" => date("d/m/Y H:i", $r['creation_time']), "sort" => $r['creation_time']),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => array());
}

echo json_encode($array);
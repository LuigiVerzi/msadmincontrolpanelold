$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('#sendTicket').on('click', function (e) {
       e.preventDefault();

        $.ajax({
            url: $('#baseElementPathSW').val() + "ajax/modules.php?id=ticket",
            type: "POST",
            async: false,
            data: {
                action: "sendTicketViaMail",
                pID: $('#record_id').val(),
                ticketID: $("p[title|='Ticket ID']").html()
            },
            dataType: 'json',
            success: function(data) {
                if(data.status == "error") {
                    bootbox.error("Si è verificato un errore durante l'invio del biglietto tramite email.");
                } else if(data.status == "ok") {
                    bootbox.alert("Il biglietto è stato inviato correttamente al seguente indirizzo: " + data.email);
                } else if(data.status == "not_active") {
                    bootbox.error("Per poter inviare un biglietto via email devi prima attivarlo e salvare le modifiche.");
                }
            }
        })

    });

}


function moduleSaveFunction() {

    if($('#record_id').val() == "") {
        var save_data = {
            "pContatto": $('#ticket_contatto').val(),
            "pNome": $('#ticket_nome').val(),
            "pStato": $('#ticket_status').val(),
            "pQuantity": $('#ticket_quantity').val(),
            "pMessaggio": $('#ticket_messaggio').val()
        };
    }
    else
    {
        var save_data = {
            "pID": $('#record_id').val(),
            "pStato": $('#ticket_status').val()
        };
    }

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: save_data,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
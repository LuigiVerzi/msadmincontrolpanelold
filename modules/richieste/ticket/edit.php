<?php
/**
 * MSAdminControlPanel
 * Date: 21/08/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM richieste WHERE id = :id and type = 'ticket'", array($_GET['id']), true);
}
if(!$r) header('Location: new.php');
?>
<body class="skin-1">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
               <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
                <div class="title-action">
                    <a class="btn btn-info pull-right" id="sendTicket" saving_txt="Invio in corso...">Invia Biglietto</a>
                </div>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <?php
                        $form_data = json_decode($r['form_data'], true);
                        ?>
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Riepilogo Richiesta</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nome</label> <br />
                                    <?php echo $r['nome'] ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Contatto</label> <br />
                                    <?php echo $r['contatto'] ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Data Creazione</label>  <br />
                                    <?php echo date("d/m/Y", $r['creation_time']) ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Lingua</label>  <br />
                                    <?php echo $MSFrameworki18n->getLanguagesDetails($r['lang'], "italian_name")[$r['lang']]['italian_name'] ?>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Messaggio</label> <br />
                                    <?php echo $r['messaggio'] ?>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php
                            if(is_array($form_data) && count($form_data) != 0) {
                            ?>
                            <h2 class="title-divider">Informazioni Addizionali</h2>
                            <?php
                                $counter = 0;
                                foreach($form_data as $k => $v) {
                                    if($counter%4 == 0) {
                                ?>
                                    <div class="row">
                                <?php
                                    }
                                ?>
                                        <div class="col-sm-3">
                                            <label><?php echo $k ?></label> <br />
                                            <?php
                                            if($k == "Stato") {
                                            ?>
                                                <select id="ticket_status" class="form-control">
                                                    <option value="1" <?php if($v == "1") { echo "selected"; } ?>>Attivo</option>
                                                    <option value="0" <?php if($v == "0") { echo "selected"; } ?>>Non Attivo</option>
                                                </select>
                                            <?php
                                            } else {
                                                echo '<p title="' . $k . '">' . $v . '</p>';
                                            }
                                            ?>

                                        </div>
                                <?php
                                    $counter++;

                                    if($counter%4 == 0 || $counter == count($form_data)) {
                                ?>
                                        </div>

                                        <div class="hr-line-dashed"></div>
                                <?php
                                    }
                                }
                            } ?>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
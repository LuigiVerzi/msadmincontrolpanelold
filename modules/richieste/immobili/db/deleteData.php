<?php
/**
 * MSAdminControlPanel
 * Date: 23/06/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->getCount("SELECT id FROM richieste WHERE id = :id AND (owner = '' OR owner = :owner)", array(":id" => $_POST['pID'], ":owner" => (new \MSFramework\users())->getUserDataFromSession('user_id'))) == 0) {
    echo json_encode(array("status" => "not_allowed"));
    die();
}

if($MSFrameworkDatabase->deleteRow("richieste", "id", $_POST['pID'])) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$array = array('data' => array());

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM richieste WHERE type = 'property' AND (owner_id = :owner OR owner_id = '') ORDER BY creation_time desc", array(":owner" => (new \MSFramework\users())->getUserDataFromSession('user_id'))) as $r) {
    $array['data'][] = array(
        htmlspecialchars($r['nome']),
        htmlspecialchars($r['contatto']),
        (strlen($r['messaggio']) > 120) ? htmlspecialchars(substr($r['messaggio'], 0, 117)) . '...' : htmlspecialchars($r['messaggio']),
        array("display" => date("d/m/Y H:i", $r['creation_time']), "sort" => $r['creation_time']),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => array());
}

echo json_encode($array);
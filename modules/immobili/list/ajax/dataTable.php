<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$cur_user_id = $MSFrameworkUsers->getUserDataFromSession('user_id');
$MSFrameworkSedi = new \MSFramework\Attivita\sedi();

$where_str = " WHERE id != '' ";
$where_ary = array();
if($MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-5" || $MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-6") {
    $where_str .= " AND owner_id = :owner_id";
    $where_ary = array_merge($where_ary, array(":owner_id" => $cur_user_id));
}

$MSFrameworkSedi = new \MSFramework\Attivita\sedi();
if($MSFrameworkUsers->getUserDataFromSession('userlevel') != "0") {
    $where_str .= " AND agency = :agency";
    $where_ary = array_merge($where_ary, array(":agency" => $MSFrameworkSedi->getUserAgency($cur_user_id)));
}

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM realestate_immobili $where_str ORDER BY creation_time DESC", $where_ary) as $r) {
    $tipo_vendita = "";
    if($r['tipo_vendita'] == "1") {
        $tipo_vendita = "Affitto";
    } else if($r['tipo_vendita'] == "0") {
        $tipo_vendita = "Vendita";
    }

    $images = json_decode($r['images'], true);
    $geodata = json_decode($r['geo_data'], true);

    $array['data'][] = array(
        ($r['in_evidenza'] == "0" ? '<i class="fa fa-star-o"></i>' : '<i class="fa fa-star"></i>'),
        '<img src="' . UPLOAD_REALESTATE_IMMOBILI_FOR_DOMAIN_HTML . "tn/" . $images[0] . '" alt="" height="80" width="80">',
        $r['riferimento'] . "<br />" . (new \MSFramework\RealEstate\immobili())->getStatoVendita((string)$r['stato_vendita']),
        $tipo_vendita,
        $MSFrameworki18n->getFieldValue((new \MSFramework\RealEstate\categories())->getCategoryParent($r['category'], true)['nome'], true),
        (new \MSFramework\geonames())->getDettagliComune($geodata['comune'])['name'] . "<br />" . $geodata['indirizzo'] . "<br />" . (new \MSFramework\geonames())->getDettagliProvincia($geodata['provincia'])['name'],
        $r['superficie'] . "mq",
        "<b><?= CURRENCY_SYMBOL; ?> " . $r['prezzo'] . "</b>",
        date("d/m/Y H:i", $r['creation_time']),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 21/10/17
 */

require_once('../../../../../../sw-config.php');

$scraperUtils = new \Scraper\utils();
$MSFrameworkImmobili = (new \MSFramework\RealEstate\immobili());
$MSFrameworkGeonames = (new \MSFramework\geonames());
$MSFrameworkCategories = (new \MSFramework\RealEstate\categories());

$randIP = $scraperUtils->random_valid_public_ip();
$doc = \Scraper\hQuery::fromUrl('https://www.remax.it/' . $_GET['code'], array(
    'Accept'     => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'User-Agent' => (new \Scraper\userAgent)->generate(),
    'REMOTE_ADDR' => $randIP,
    'HTTP_X_FORWARDED_FOR' => $randIP,
));

if(!$doc || $_GET['code'] == "") {
    echo json_encode(array("status" => "url_error"));
    die();
}

$energy_class_assoc = array(
    "A4" => "0",
    "A2" => "0",
    "A3" => "0",
    "A1" => "1",
    "B" => "2",
    "C" => "3",
    "D" => "4",
    "E" => "5",
    "F" => "6",
    "G" => "7",
);

$title = $scraperUtils->getSelectorContent('div.unit-overview__title');
$attributes_to_search = array("mq", "vani", "camere da letto", "bagni", "classe energetica");
$attributes = array();
foreach(explode(",", $scraperUtils->getSelectorContent('span.unit-overview__subtitle')) as $subEl) {
    foreach($attributes_to_search as $attributeK => $attribute) {
        if(stristr($subEl, $attribute)) {
            $attributes[str_replace(" ", "_", $attribute)] = trim(str_ireplace($attribute, "", $subEl));
            unset($attributes_to_search[$attributeK]);
        }
    }
}


foreach($doc->find('div.unit-feature--medium') as $subElAry) {
    $elText = $subElAry->text();

    foreach($attributes_to_search as $attributeK => $attribute) {
        if(stristr($elText, $attribute)) {
            $attributes[str_replace(" ", "_", $attribute)] = trim(str_ireplace($attribute, "", $elText));
            unset($attributes_to_search[$attributeK]);
        }
    }
}

if($attributes['classe_energetica'] != "") {
    $attributes['classe_energetica'] = (array_key_exists($attributes['classe_energetica'], $energy_class_assoc) ? $energy_class_assoc[$attributes['classe_energetica']] : "8");
}

if($attributes['bagni'] != "") {
    $attributes['bagni'] = ($attributes['bagni'] <= 3 ? $attributes['bagni'] : "+3");
}

$results = array();

//Ottengo i dati dai breadcrumbs
$breadCrumbs = $doc->find('ul.breadcrumbs > li');
if($breadCrumbs != null) {
    $giro_bread = 0;
    foreach($breadCrumbs as $k => $breadCrumb) {
        if($giro_bread == 2) {
            $results['immobile_in'] = (strtolower($breadCrumb->text()) == "vendita" ? "0" : "1");
        } else if($giro_bread == 4) {
            $comune = $breadCrumb->text();
            $id_comune = $MSFrameworkGeonames->searchForComuneID($comune);
        } else if($giro_bread == 5) {
            $categoria_immobile = $breadCrumb->text();
        } else if($giro_bread == 6) {
            $title = $breadCrumb->text();
        }

        $giro_bread++;
    }
}

$results = array_merge(array(
    "title" => $title,
    "price" => str_replace(array(CURRENCY_SYMBOL, "."), array("", ""), $scraperUtils->getSelectorContent('span.unit-overview__price')),
    "ita_descr" => $scraperUtils->getSelectorContent('div.unit-page__description', 'clean_html'),
    "address" => array("textual" => $scraperUtils->getSelectorContent('div.unit-overview__location')),
    "attributes" => $attributes,
), $results);

foreach($MSFrameworkCategories->getCategoryDetails() as $catID => $catV) {
    $catV['nome'] = json_decode($catV['nome'], true);

    if(stristr($categoria_immobile, $catV['nome']['it_IT']) && !$MSFrameworkCategories->isCategoryParent($catV['id'])) {
        $results['category'] = $catID;
    }
}

if($id_comune != "") {
    $dettagli_comune = $MSFrameworkGeonames->getDettagliComune($id_comune, "country, admin1, admin2, name");

    $results['address']['comune'] = $id_comune;
    $results['address']['comune_textual'] = $dettagli_comune['name'];
}

if($results['address']['comune'] != "") {
    $results['address']['provincia'] = $MSFrameworkGeonames->searchForProvinciaID($dettagli_comune['country'] . "." . $dettagli_comune['admin1'] . "." . $dettagli_comune['admin2'], "code");
}

if($results['address']['provincia'] != "") {
    $results['address']['regione'] = $MSFrameworkGeonames->searchForRegioneID($dettagli_comune['country'] . "." . $dettagli_comune['admin1'], "code");
}

/*$youtubeVideo = $scraperUtils->getSelectorContent('div#gallery-map-youtube:parent meta', 'attr', array("itemprop" => "embedURL"), 'content')['embedURL'];
if($youtubeVideo != "") {
    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $youtubeVideo, $matches);
    $results['video'] = "https://youtu.be/" . $matches[1];
}*/

//Ottengo i dati di latitidine e longitudine dallo script che carica la mappa di Google
/*$googleMaps = $scraperUtils->getSelectorContent('div#gallery-map-map > script');
if($googleMaps != "") {
    $lat_pos = strpos($googleMaps, "lat = ");
    $lat_semicolon_pos = strpos($googleMaps, ";", $lat_pos);
    $lat = substr($googleMaps, $lat_pos+6, ($lat_semicolon_pos-$lat_pos-6));

    $lng_pos = strpos($googleMaps, "lng = ");
    $lng_semicolon_pos = strpos($googleMaps, ";", $lng_pos);
    $lng = substr($googleMaps, $lng_pos+6, ($lng_semicolon_pos-$lng_pos-6));

    $results['address']['lat'] = $lat;
    $results['address']['lng'] = $lng;
}*/

/*
//Imposto le caratteristiche nel formato di RealBox
/*
 * questo array mi permette di associare le denominazioni delle "Caratteristiche/Servizi Immobile" di realbox con quelle di remax.
 * nella chiave c'è la stringa presente in realbox, nel valore c'è quella presente in remax.it

$caratteristiche = $doc->find('div.features-container span.feature-item');
$tmp_caratteristiche = array();
if($caratteristiche != null) {
    foreach($caratteristiche as $caratteristica) {
        $tmp_caratteristiche[] = $caratteristica->text();
    }
}

$tmp_caratteristiche[] = "Portone automatico";
$assoc_caratteristiche = array(
    "Riscaldamento Autonomo" => "Riscaldamento - Autonomo",
    "Riscaldamento Centralizzato" => "Riscaldamento - Centralizzato",
    "Linea Internet" => "Linea ADSL",
    "Accessibile PMR" => "Accessibile Persone Diversamente Abili",
    "Posto Auto Riservato" => "Posto Auto",
    "Vicinanze Centro città" => "Centro città",
);

$results['caratteristiche'] = array();
foreach($MSFrameworkImmobili->getElencoServizi() as $k => $v) {
    foreach($v['services'] as $serviceK => $serviceV) {
        if(in_array($assoc_caratteristiche[$serviceV], $tmp_caratteristiche) || in_array($serviceV, $tmp_caratteristiche)) {
            $results['caratteristiche'][] = $k . "_" . $serviceK;
        }
    }
}
*/

$results['immagini'] = array();
$results['planimetrie'] = array();

//Ottengo gli URL delle immagini
/*
$immagini = $doc->find('div.head-slider div.slider__bigslide');
if($immagini != null) {
    foreach($immagini as $immagineContainer) {
        if($immagineContainer->find('.slider__overlay--video') == null) { //se è presente la classe slider__overlay--video vuol dire che è un video, lo tratto dopo
            if($immagineContainer->find('figure.slider__image')->find('img') != null) {
                $src = $immagineContainer->find('figure.slider__image img')->attr('src');
                if ($src != "") $results['immagini'][] = $src;
            }
        }
    }
}

//Ottengo gli URL delle planimetrie
$immagini = $doc->find('div.planimetries-slider div.swiper-slide');
if($immagini != null) {
    foreach($immagini as $immagineContainer) {
        if($immagineContainer->find('.slider__overlay--video') == null) { //se è presente la classe slider__overlay--video vuol dire che è un video
            $src = $immagineContainer->find('figure.slider__image > img')->attr('src');
            if($src != "") $results['planimetrie'][] = $src;
        }
    }
}
*/

//su remax.it usano vue, alcune informazioni devono essere necessariamente estrapolate dalla funzione che gestisce il FW.
foreach($doc->find('script') as $script_tag) {
    $new_func_text = "window.__NUXT__=(function";
    if(substr($script_tag->text(), 0, strlen($new_func_text)) == $new_func_text) {
        $got_text = $script_tag->text();

        //cerco video youtube
        $search = true;
        $last_pos = -1;
        $search_string = "youtu.be\u002F";

        do {
            $search = strpos($got_text, $search_string, $last_pos+1);
            if($search) {
                $last_pos = $search;
                $closing = strpos($got_text, '"', $last_pos);

                $results['video'] = "https://youtu.be/" . substr($got_text, $search+strlen($search_string), $closing-($search+strlen($search_string)));
            }

        } while($search);

        //cerco immagini immobile
        $search = true;
        $last_pos = -1;
        $search_string = "images:[";

        do {
            $search = strpos($got_text, $search_string, $last_pos+1);
            if($search) {
                $last_pos = $search;
                $closing = strpos($got_text, ']', $last_pos);

                $found = substr($got_text, $search+strlen($search_string), $closing-($search+strlen($search_string)));
                $found = preg_replace('/\\\\u([0-9A-F]+)/', '&#x$1;', $found);
                $found = html_entity_decode($found, ENT_COMPAT, 'UTF-8');

                foreach(array("desktop", "tablet", "mobile") as $device) {
                    $sub_search = true;
                    $sub_last_pos = -1;
                    $sub_search_string = $device . ':';
                    $img_found = false;

                    do {
                        $sub_search = strpos($found, $sub_search_string, $sub_last_pos + 1);
                        if ($sub_search) {
                            $sub_last_pos = $sub_search;
                            $sub_closing = strpos($found, '"', $sub_last_pos + strlen($sub_search_string) + 1);
                            $sub_found = substr($found, $sub_search + strlen($sub_search_string) + 1, $sub_closing - ($sub_search + strlen($sub_search_string)) - 1);

                            if(substr($sub_found, 0, 4) == "http") {
                                $results['immagini'][str_replace(array("__desktop", "__tablet", "__mobile"), array(), $sub_found)] = str_replace(array("__desktop", "__tablet", "__mobile"), array(), $sub_found);
                            }

                        }

                    } while ($sub_search);
                }
            }

        } while($search);

        if(count($results['immagini']) != 0) {
            $results['immagini'] = array_values($results['immagini']);
        }


        //cerco planimetrie immobile
        $search = true;
        $last_pos = -1;
        $search_string = "planimetries:[";

        do {
            $search = strpos($got_text, $search_string, $last_pos+1);
            if($search) {
                $last_pos = $search;
                $closing = strpos($got_text, ']', $last_pos);

                $found = substr($got_text, $search+strlen($search_string), $closing-($search+strlen($search_string)));
                $found = preg_replace('/\\\\u([0-9A-F]+)/', '&#x$1;', $found);
                $found = html_entity_decode($found, ENT_COMPAT, 'UTF-8');

                foreach(array("desktop", "tablet", "mobile") as $device) {
                    $sub_search = true;
                    $sub_last_pos = -1;
                    $sub_search_string = $device . ':';
                    $img_found = false;

                    do {
                        $sub_search = strpos($found, $sub_search_string, $sub_last_pos + 1);
                        if ($sub_search) {
                            $sub_last_pos = $sub_search;
                            $sub_closing = strpos($found, '"', $sub_last_pos + strlen($sub_search_string) + 1);
                            $sub_found = substr($found, $sub_search + strlen($sub_search_string) + 1, $sub_closing - ($sub_search + strlen($sub_search_string)) - 1);

                            if(substr($sub_found, 0, 4) == "http") {
                                $results['planimetrie'][str_replace(array("__desktop", "__tablet", "__mobile"), array(), $sub_found)] = str_replace(array("__desktop", "__tablet", "__mobile"), array(), $sub_found);
                            }

                        }

                    } while ($sub_search);
                }
            }

        } while($search);

        if(count($results['planimetrie']) != 0) {
            $results['planimetrie'] = array_values($results['planimetrie']);
        }


        //cerco coordinate immobile
        $search = true;
        $last_pos = -1;
        $search_string = "location:[";

        do {
            $search = strpos($got_text, $search_string, $last_pos+1);
            if($search) {
                $last_pos = $search;
                $closing = strpos($got_text, ']', $last_pos);

                $found = substr($got_text, $search+strlen($search_string), $closing-($search+strlen($search_string)));
                $ary_coords = explode(",", $found);

                if(preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $ary_coords[1])) {
                    $results['address']['lat'] = $ary_coords[1];
                }

                if(preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $ary_coords[0])) {
                    $results['address']['lng'] = $ary_coords[0];
                }

                if($results['address']['lat'] != "" && $results['address']['lng'] != "") {
                    $coord_address = $MSFrameworkGeonames->getAddressFromLatLng($results['address']['lat'], $results['address']['lng']);
                    if($coord_address) {
                        $results['address']['textual'] = $coord_address[1]['long_name'] . ", " . $coord_address[0]['long_name'];
                        $results['address']['cap'] = $coord_address[6]['long_name'];
                    }
                }
            }

        } while($search);


        $params_text = "}}(";
        $params = explode(",", substr($got_text, strpos($got_text, $params_text)+strlen($params_text), -3));
    }
}

echo json_encode(array("status" => "ok", "results" => $results));
?>
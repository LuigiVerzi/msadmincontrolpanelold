<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);
$MSFrameworkImmobili = new \MSFramework\RealEstate\immobili();
$MSFrameworkAgents = new \MSFramework\RealEstate\agents();

if(((new \MSFramework\users())->getUserDataFromSession('userlevel') == "realestate-5" || (new \MSFramework\users())->getUserDataFromSession('userlevel') == "realestate-6") && $db_action == "update") {
    $r_owner = $MSFrameworkImmobili->getImmobileDataFromDB($_POST['pID'], "owner_id");
    if($r_owner['owner_id'] != (new \MSFramework\users())->getUserDataFromSession('user_id')) {
        echo json_encode(array("status" => "not_allowed"));
        die();
    }
}

$MSFrameworkSedi = new \MSFramework\Attivita\sedi();
if($MSFrameworkUsers->getUserDataFromSession('userlevel') != "0") {
    if($_POST['pAgency'] != $MSFrameworkSedi->getUserAgency($MSFrameworkUsers->getUserDataFromSession('user_id'))) {
        echo json_encode(array("status" => "not_allowed"));
        die();
    }
}

$can_save = true;
if($_POST['pSlug'] == "" || $_POST['pCategory'] == "" || $_POST['pTitolo'] == "" || $_POST['pTipoVendita'] == "" || $_POST['pAgency'] == "" || $_POST['pPrezzo'] == "" || $_POST['pSuperficie'] == "" || $_POST['pDescrizione'] == "" || ($MSFrameworkAgents->canChangeOwnerId() && $_POST['pAgenteRiferimento'] == "")) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

(new MSFramework\geonames())->checkDataBeforeSave(true);

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "realestate_immobili", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

$array_prezzi = array("pPrezzo", "pStimaPrezzo");
foreach($array_prezzi as $post_prezzo) {
    $_POST[$post_prezzo] = str_replace(",", ".", $_POST[$post_prezzo]);
    if (!strstr($_POST[$post_prezzo], ".") && $_POST[$post_prezzo] != "") {
        $_POST[$post_prezzo] .= ".00";
    }

    if (!is_numeric($_POST[$post_prezzo]) && $_POST[$post_prezzo] != "") {
        echo json_encode(array("status" => "data_format_not_valid"));
        die();
    }
}

if(!is_numeric($_POST['pSuperficie']) || ($_POST['pAnnoCostr'] != "" && !is_numeric($_POST['pAnnoCostr']))) {
    echo json_encode(array("status" => "data_format_not_valid"));
    die();
}

if($db_action == "update") {
    $where_id_check_rife = " AND id != :id ";
    $where_id_check_rife_ary = array(":id" => $_POST['pID']);
} else {
    $where_id_check_rife = " AND id != '' ";
    $where_id_check_rife_ary = array();
}

if($_POST['pRiferimento'] != "") {
    if($MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili WHERE riferimento = :rife $where_id_check_rife", array_merge($where_id_check_rife_ary, array(":rife" => $_POST['pRiferimento']))) != 0) {
        echo json_encode(array("status" => "rife_already_exists"));
        die();
    }
}

if(!$MSFrameworkAgents->canChangeOwnerId()) {
    $owner_id = $_SESSION['userData']['user_id'];
} else {
    $owner_id = $_POST['pAgenteRiferimento'];
}

$uploader = new \MSFramework\uploads('REALESTATE_IMMOBILI');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$uploaderPlanimetrie = new \MSFramework\uploads('REALESTATE_PLANIMETRIE');
$ary_filesPlanimetrie = $uploaderPlanimetrie->prepareForSave($_POST['pImagesPlanAry']);
if($ary_filesPlanimetrie === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT images, images_plan, titolo, descrizione, slug FROM realestate_immobili WHERE id = :id", array(":id" => $_POST['pID']), true);

    $stato_vendita = $_POST['pStatoVendita'];
} else {
    $stato_vendita = "0";
}

$_POST['pTitolo'] = ucfirst(strtolower($_POST['pTitolo']));
$_POST['pTitolo'] = preg_replace_callback('/[.!?].*?\w/', create_function('$matches', 'return strtoupper($matches[0]);'),$_POST['pTitolo']);

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pTitolo'], 'oldValue' => $r_old_data['titolo']),
        array('currentValue' => $_POST['pDescrizione'], 'oldValue' => $r_old_data['descrizione']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$set_in_evidenza = "0";

if($_POST['pEvidenza'] == "1") {
    $dati_agente = $MSFrameworkUsers->getUserDataFromDB($owner_id, 'ruolo, extra_data');
    $owner_where = "";
    $owner_where_ary = array();

    if($dati_agente['ruolo'] == "realestate-6") {
        $id_riferimento = json_decode($dati_agente['extra_data'], true)['cons_rif'];
    } else {
        $id_riferimento = $owner_id;
    }

    $ary_owners[] = $id_riferimento;
    foreach ($MSFrameworkDatabase->getAssoc("SELECT id FROM users WHERE extra_data LIKE :cons_rif", array(":cons_rif" => '%"cons_rif":"' . $id_riferimento . '"%')) as $assistente) {
        $ary_owners[] = $assistente['id'];
    }

    $assistente_ary = $MSFrameworkDatabase->composeSameDBFieldString($ary_owners, "OR", "owner_id");
    $owner_where = " AND ($assistente_ary[0])";
    $owner_where_ary = $assistente_ary[1];

    $c = $MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili WHERE in_evidenza = '1' AND id != :id $owner_where", array_merge($owner_where_ary, array(":id" => $_POST['pID'])));

    if($c == 0) {
        $set_in_evidenza = "1";
    } else {
        if($_POST['pRemoveOthEvidenza'] == "true") {
            $MSFrameworkDatabase->pushToDB("UPDATE realestate_immobili SET in_evidenza = '0' WHERE id != '' $owner_where", $owner_where_ary);
            $set_in_evidenza = "1";
        }
    }
}

$cur_time = time();

$array_to_save = array(
    "category" => $_POST['pCategory'],
    "tipo_vendita" => $_POST['pTipoVendita'],
    "titolo" => $MSFrameworki18n->setFieldValue($r_old_data['titolo'], $_POST['pTitolo']),
    "riferimento" => trim($_POST['pRiferimento']),
    "geo_data" => json_encode($_POST['pGeoData']),
    "prezzo" => $_POST['pPrezzo'],
    "stima_prezzo" => $_POST['pStimaPrezzo'],
    "trattativa_riservata" => $_POST['pTrattRis'],
    "superficie" => $_POST['pSuperficie'],
    "anno_costruzione" => $_POST['pAnnoCostr'],
    "caratteristiche" => json_encode($_POST['pCaratteristiche']),
    "descrizione" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione'], $_POST['pDescrizione']),
    "servizi" => json_encode($_POST['pServizi']),
    "dati_mutuo" => json_encode($_POST['pMutuo']),
    "images" => json_encode($ary_files),
    "images_plan" => json_encode($ary_filesPlanimetrie),
    "youtube_url" => $_POST['pYouTube'],
    "owner_id" => $owner_id,
    "in_evidenza" => $set_in_evidenza,
    "landing_page" => $_POST['pLandingPage'],
    "agency" => $_POST['pAgency'],
    "stato_vendita" => $stato_vendita,
    "creation_time" => $cur_time,
    "last_update_time" => $cur_time,
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
);

if($db_action == "update") {
    unset($array_to_save['creation_time']);

    // Anche se in molti casi gli immobili sono modificabili solo da chi li ha creati, resta la possibilità che un admin modifichi l'immobile. in questo caso, non devo perdere il "proprietario" reale dell'immobile

    if(!$MSFrameworkAgents->canChangeOwnerId()) {
        unset($array_to_save['owner_id']);
    }
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO realestate_immobili ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE realestate_immobili SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
        $uploader->unlink($ary_filesPlanimetrie);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['images'], true));
    $uploader->unlink($ary_filesPlanimetrie, json_decode($r_old_data['images_plan'], true));
}

$ref_id = ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : $_POST['pID']);
$MSFrameworkUrl->makeRedirectIfNeeded('realestate-immobile-' . $ref_id, (new \MSFramework\RealEstate\immobili())->dettaglioImmobileURL($ref_id), ($db_action === 'update' ? (new \MSFramework\RealEstate\immobili())->dettaglioImmobileURL($r_old_data) : ''));

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 06/09/17
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');
$MSFrameworkAgents = (new \MSFramework\RealEstate\agents());

if(!$MSFrameworkAgents->canChangeOwnerId()) {
    $owner_id = $_SESSION['userData']['user_id'];
} else {
    $owner_id = $_GET['agent'];
}

$dati_agente = $MSFrameworkAgents->getUserDataFromDB($owner_id);
$owner_where = "";
$owner_where_ary = array();

if($dati_agente['ruolo'] == "6") {
    $id_riferimento = $dati_agente['cons_rif'];
} else {
    $id_riferimento = $owner_id;
}

$ary_owners[] = $id_riferimento;
foreach ($MSFrameworkDatabase->getAssoc("SELECT id FROM users WHERE extra_data LIKE :cons_rif", array(":cons_rif" => '%"cons_rif":"' . $id_riferimento . '"%')) as $assistente) {
    $ary_owners[] = $assistente['id'];
}

$assistente_ary = $MSFrameworkDatabase->composeSameDBFieldString($ary_owners, "OR", "owner_id");
$owner_where = " AND ($assistente_ary[0])";
$owner_where_ary = $assistente_ary[1];

echo $MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili WHERE in_evidenza = '1' AND id != :id $owner_where", array_merge($owner_where_ary, array(":id" => $_GET['id'])));
?>
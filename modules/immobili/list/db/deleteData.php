<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array('immobile' => array(), 'planimetrie' => array());
foreach((new \MSFramework\RealEstate\immobili())->getImmobiliDetails($_POST['pID']) as $r) {
    $images_to_delete['immobile'] = array_merge_recursive($images_to_delete, json_decode($r['images'], true));
    $images_to_delete['planimetrie'] = array_merge_recursive($images_to_delete,json_decode($r['images_plan'], true));

    if (((new \MSFramework\users())->getUserDataFromSession('userlevel') == "realestate-5" || (new \MSFramework\users())->getUserDataFromSession('userlevel') == "realestate-6")) {
        if ($r['owner_id'] != (new \MSFramework\users())->getUserDataFromSession('user_id')) {
            echo json_encode(array("status" => "not_allowed"));
            die();
        }
    }

    $MSFrameworkSedi = new \MSFramework\Attivita\sedi();
    if ($MSFrameworkUsers->getUserDataFromSession('userlevel') != "0") {
        if ($r['agency'] != $MSFrameworkSedi->getUserAgency($MSFrameworkUsers->getUserDataFromSession('user_id'))) {
            echo json_encode(array("status" => "not_allowed"));
            die();
        }
    }
}

if($MSFrameworkDatabase->deleteRow("realestate_immobili", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('REALESTATE_IMMOBILI'))->unlink($images_to_delete['immobile']);
    (new \MSFramework\uploads('REALESTATE_PLANIMETRIE'))->unlink($images_to_delete['planimetrie']);
    $MSFrameworkUrl->deleteRedirectsByReferences('realestate-immobile', $_POST['pID']);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
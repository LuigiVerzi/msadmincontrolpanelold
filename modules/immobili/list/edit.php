<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkImmobili = (new \MSFramework\RealEstate\immobili());
$MSFrameworkAgents = (new \MSFramework\RealEstate\agents());
$MSFrameworkGeonames = (new \MSFramework\geonames());
$MSFrameworkSedi = new \MSFramework\Attivita\sedi();

$scraperActive = (new \MSFramework\modules())->checkIfIsActive('scraper');

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM realestate_immobili WHERE id = :id", array(":id" => $_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Tipologia Annuncio</h1>
                        <fieldset>
                            <h2 class="title-divider">Selezione Tipologia</h2>

                            <div class="row">

                                <div class="col-sm-6">
                                    <label>Categoria Immobile*</label>
                                    <select id="category" class="form-control required">
                                        <option value=""></option>
                                        <?php
                                        echo (new \MSFramework\RealEstate\categories)->composeHTMLMenu('select', explode(',', $r['category']));
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <label>Agenzia*</label>
                                    <select id="agency" class="form-control required">
                                        <?php
                                        $sedi_disponibili = (new \MSFramework\Attivita\sedi())->getSedeDetails($MSFrameworkSedi->getUserAgency($MSFrameworkUsers->getUserDataFromSession('user_id')), 'id, nome');
                                        if(count($sedi_disponibili) > 1) {
                                            ?>
                                            <option value=""></option>
                                            <?php
                                        }

                                        foreach($sedi_disponibili as $sede) {
                                        ?>
                                            <option value="<?= $sede['id'] ?>" <?= ($r['agency'] == $sede['id'] ? 'selected' : '') ?>><?= $MSFrameworki18n->getFieldValue($sede['nome']) ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-2">
                                    <label>Immobile in*</label>
                                    <select id="tipo_vendita" class="form-control required">
                                        <option value=""></option>
                                        <option value="0" <?php if($r['tipo_vendita'] == "0") { echo "selected"; } ?>>Vendita</option>
                                        <option value="1" <?php if($r['tipo_vendita'] == "1") { echo "selected"; } ?>>Affitto</option>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-7">
                                    <label>Titolo Immobile*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['titolo']) ?>"></i></span>
                                    <input type="text" id="titolo_immobile" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['titolo'])) ?>"/>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Slug*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                            <input id="slug" name="slug" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-<?= ($scraperActive ? "2" : "4") ?>">
                                    <label>Riferimento Immobile</label>
                                    <input type="text" id="riferimento_immobile" class="form-control" value="<?php echo $r['riferimento'] ?>" />
                                </div>

                                <?php if($scraperActive) { ?>
                                <div class="col-sm-2" style="display: none;" id="scraper_btn">
                                    <input type="hidden" id="scraper" value="<?php echo ucfirst((new \MSFramework\modules())->getExtraModuleSettings('scraper')['type']) ?>" />

                                    <label></label>
                                    <div style="margin-top: 4px;">
                                        <input type="button" class="btn btn-info" value="Recupera dati da <?php echo ucfirst((new \MSFramework\modules())->getExtraModuleSettings('scraper')['type']) ?>" onclick="launchScraperImmobili()" />
                                    </div>
                                </div>
                                <?php } ?>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="in_evidenza" <?php if($r['in_evidenza'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">In Evidenza</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <?php require(ABSOLUTE_SW_PATH . "includes/template/geodata/geodata.php"); ?>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Prezzo Immobile (<?= CURRENCY_SYMBOL; ?>)*</label>
                                    <input type="text" id="prezzo_immobile" name="prezzo_immobile" class="form-control required number" value="<?php echo $r['prezzo'] ?>" />
                                </div>

                                <div class="col-sm-4">
                                    <label>Stima Prezzo Immobile (<?= CURRENCY_SYMBOL; ?>)</label>
                                    <input type="text" id="stima_prezzo_immobile" name="stima_prezzo_immobile" class="form-control number" value="<?php echo $r['stima_prezzo'] ?>" />
                                </div>

                                <div class="col-sm-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="trattativa_riservata" <?php if($r['trattativa_riservata'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Trattativa Riservata</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Superficie (mq)*</label>
                                    <input type="text" id="superficie_immobile" name="superficie_immobile" class="form-control required number" value="<?php echo $r['superficie'] ?>" />
                                </div>

                                <div class="col-sm-2">
                                    <label>Anno costruzione</label>
                                    <input type="text" id="anno_costruzione" name="anno_costruzione" class="form-control number" value="<?php echo $r['anno_costruzione'] ?>" />
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <?php
                                if($MSFrameworkAgents->canChangeOwnerId()) {
                                    ?>
                                    <div class="col-sm-6">
                                        <label>Agente di riferimento*</label>
                                        <select id="agente_riferimento" name="agente_riferimento" class="form-control required">
                                            <option value=""></option>
                                            <?php
                                            if($r['owner_id'] == "") {
                                                $r['owner_id'] = $MSFrameworkAgents->getUserDataFromSession('user_id');
                                            }

                                            foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, ruolo FROM users ORDER BY cognome, nome") as $user) {
                                                ?>
                                                <option value="<?php echo $user['id'] ?>" <?php if($user['id'] == $r['owner_id']) { echo "selected"; } ?>><?php echo $user['cognome'] ?> <?php echo $user['nome'] ?> (<?php echo $MSFrameworkAgents->getUserLevels($user['ruolo']) ?>)</option>
                                                <?php
                                            }
                                            ?>
                                        </select>

                                        <span>Sei un <?php echo $MSFrameworkAgents->getUserLevels($MSFrameworkAgents->getUserDataFromSession('userlevel')) ?>, puoi assegnare questo immobile a qualunque agente.</span>
                                    </div>
                                    <?php
                                } else {
                                    if($_GET['id'] != "") {
                                        $owner_data = $MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, ruolo FROM users WHERE id = :id", array(":id" => $r['owner_id']), true);
                                        ?>
                                        <div class="col-sm-6">
                                            <label>Agente di riferimento</label>
                                            <div><?php echo $owner_data['cognome'] ?> <?php echo $owner_data['nome'] ?>
                                                (<?php echo $MSFrameworkAgents->getUserLevels($owner_data['ruolo']) ?>)
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>

                                <?php
                                if($_GET['id'] != "") {
                                    ?>
                                    <div class="col-sm-3">
                                        <label>Stato Vendita*</label>
                                        <select id="stato_vendita" class="form-control required">
                                            <?php
                                            foreach($MSFrameworkImmobili->getStatoVendita() as $k => $v) {
                                                ?>
                                                <option value="<?php echo $k ?>" <?php if($r['stato_vendita'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                <?php } ?>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="landing_page" <?php if($r['landing_page'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Landing Page</span>
                                        </label>
                                    </div>
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>

                        </fieldset>

                        <h1>Caratteristiche</h1>
                        <fieldset>
                            <?php
                            $caratteristiche = json_decode($r['caratteristiche'], true);
                            ?>
                            <h2 class="title-divider">Selezione Caratteristiche</h2>
                            <div class="row">
                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>Stato attuale immobile</label>
                                    <select id="stato_immobile" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getStatiImmobili() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($caratteristiche['stato_immobile'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>Totale locali*</label>
                                    <input id="totale_locali" class="form-control required" type="number" value="<?php echo $caratteristiche['totale_locali'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>Stato arredamento</label>
                                    <select id="stato_arredamento" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getStatiArredamento() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($caratteristiche['stato_arredamento'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>Classe energetica</label>
                                    <select id="classe_energetica" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getClassiEnergetiche() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($caratteristiche['classe_energetica'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-4">
                                    <label>Piani totali (0 terra)</label>
                                    <input id="piani_totali" class="form-control" type="number" value="<?php echo $caratteristiche['piani_totali'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>Piano</label>
                                    <select id="piano" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getElencoPiani() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($caratteristiche['piano'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-4">
                                    <label>Riscaldamento</label>
                                    <select id="riscaldamento" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getTipiRiscaldamento() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($caratteristiche['riscaldamento'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-3">
                                    <label>Posti auto</label>
                                    <input id="posti_auto" class="form-control" type="number" value="<?php echo $caratteristiche['posti_auto'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-3">
                                    <label>Posti auto garage</label>
                                    <input id="posti_auto_garage" class="form-control" type="number" value="<?php echo $caratteristiche['posti_auto_garage'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-4">
                                    <label>Cucina</label>
                                    <select id="cucina" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getTipiCucina() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($caratteristiche['cucina'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-4">
                                    <label>Box/garage</label>
                                    <select id="box_garage" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getTipiGarage() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($caratteristiche['box_garage'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4 block-caratteristiche">
                                    <label>Mq. Box/garage</label>
                                    <input id="box_garage_mq" class="form-control" type="number" value="<?php echo $caratteristiche['box_garage_mq'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>Bagni</label>
                                    <select id="bagni" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        for($x_bagni = 1; $x_bagni <= 3; $x_bagni++) {
                                            ?>
                                            <option value="<?php echo $x_bagni ?>" <?php if($caratteristiche['bagni'] == (string)$x_bagni) { echo "selected"; } ?>><?php echo $x_bagni ?></option>
                                            <?php
                                        }
                                        ?>

                                        <option value="+3">&gt;3</option>
                                    </select>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>Camere da letto</label>
                                    <input id="camere_da_letto" class="form-control" type="number" value="<?php echo $caratteristiche['camere_da_letto'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>Giardino</label>
                                    <select id="giardino" class="form-control">
                                        <option value="">Scegli</option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getTipiGiardino() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($caratteristiche['giardino'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-4 block-caratteristiche">
                                    <label>Mq. Giardino</label>
                                    <input id="giardino_mq" class="form-control" type="number" value="<?php echo $caratteristiche['giardino_mq'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_reddito" <?php if($caratteristiche['check_reddito'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">A reddito</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_reception" <?php if($caratteristiche['check_reception'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Reception</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_allarme" <?php if($caratteristiche['check_allarme'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Allarme</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_cablato" <?php if($caratteristiche['check_cablato'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Cablato</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_ascensore" <?php if($caratteristiche['check_ascensore'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Ascensore</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche">
                                    <label>Mq. Ascensore</label>
                                    <input name="liftMq" id="liftMq" class="form-control" type="number" value="<?php echo $caratteristiche['liftMq'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_terrazzo" <?php if($caratteristiche['check_terrazzo'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Terrazzo</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche">
                                    <label>Mq. Terrazzo</label>
                                    <input id="terrazzo_mq" class="form-control" type="number" value="<?php echo $caratteristiche['terrazzo_mq'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_balcone" <?php if($caratteristiche['check_balcone'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Balcone</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche">
                                    <label>Mq. Balcone</label>
                                    <input id="balcone_mq" class="form-control" type="number" value="<?php echo $caratteristiche['balcone_mq'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_cantina" <?php if($caratteristiche['check_cantina'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Cantina</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche">
                                    <label>Mq. Cantina</label>
                                    <input id="cantina_mq" class="form-control" type="number" value="<?php echo $caratteristiche['cantina_mq'] ?>">
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_condizionatore" <?php if($caratteristiche['check_condizionatore'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Condizionatore</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="check_spese_condominiali" <?php if($caratteristiche['check_spese_condominiali'] == "1") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Spese condominiali</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-4 block-caratteristiche">
                                    <label>Importo spese condominiali</label>
                                    <input id="spese_condominiali_importo" class="form-control" type="number" value="<?php echo $caratteristiche['spese_condominiali_importo'] ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 block-caratteristiche block-avail-0 block-avail-1 block-avail-2 block-avail-3 block-avail-4 block-avail-5">
                                    <label>Descrizione immobile*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione']) ?>"></i></span>
                                    <textarea class="form-control required" id="descrizione_immobile" rows="10" cols="5"><?php echo htmlentities($MSFrameworki18n->getFieldValue($r['descrizione'])) ?></textarea>
                                </div>
                            </div>

                        </fieldset>

                        <h1>Servizi</h1>
                        <fieldset>
                            <?php
                            $servizi = json_decode($r['servizi'], true);
                            ?>
                            <h2 class="title-divider">Selezione Servizi</h2>

                            <?php
                            foreach($MSFrameworkImmobili->getElencoServizi() as $k => $v) {
                                ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label><?php echo $v['service_groupname'] ?></label>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="row" style="margin-bottom: 25px;">
                                            <?php foreach($v['services'] as $serviceK => $serviceV) { ?>
                                                <div class="col-sm-4 block-service" style="margin-bottom: 20px;">

                                                    <div class="styled-checkbox form-control">
                                                        <label style="width: 100%;">
                                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                <input type="checkbox" id="<?php echo $k . "_" . $serviceK ?>" <?php if(array_key_exists($k . "_" . $serviceK, $servizi)) { echo "checked"; } ?>>
                                                                <i></i>
                                                            </div>
                                                            <span style="font-weight: normal;"><?php echo $serviceV ?></span>
                                                        </label>
                                                    </div>

                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </fieldset>

                        <h1>Finanziario</h1>
                        <fieldset>
                            <?php
                            $mutuo = json_decode($r['dati_mutuo'], true);
                            ?>

                            <h2 class="title-divider">Situazione finanziaria mutuo</h2>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label><?php echo $MSFrameworkImmobili->getDatiMutuoFields('attuale') ?></label>
                                    <input type="text" id="mutuo_attuale" class="form-control" value="<?php echo $mutuo['attuale'] ?>" />
                                </div>

                                <div class="col-sm-4">
                                    <label><?php echo $MSFrameworkImmobili->getDatiMutuoFields('differenza_saldo') ?> (<?= CURRENCY_SYMBOL; ?>)</label>
                                    <input type="text" id="mutuo_diff_a_saldo" class="form-control numeric" value="<?php echo $mutuo['differenza_saldo'] ?>" />
                                </div>

                                <div class="col-sm-4">
                                    <label><?php echo $MSFrameworkImmobili->getDatiMutuoFields('tipo') ?></label>
                                    <select id="mutuo_tipo" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getTipiMutui() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($mutuo['tipo'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label><?php echo $MSFrameworkImmobili->getDatiMutuoFields('residuo_mutuo') ?> (<?= CURRENCY_SYMBOL; ?>)</label>
                                    <input type="text" id="residuo_mutuo" class="form-control numeric" value="<?php echo $mutuo['residuo_mutuo'] ?>" />
                                </div>

                                <div class="col-sm-4">
                                    <label><?php echo $MSFrameworkImmobili->getDatiMutuoFields('residuo_anni') ?></label>
                                    <input type="text" id="residuo_anni" class="form-control numeric" value="<?php echo $mutuo['residuo_anni'] ?>" />
                                </div>

                                <div class="col-sm-4">
                                    <label><?php echo $MSFrameworkImmobili->getDatiMutuoFields('period') ?></label>
                                    <select id="mutuo_period_rata" class="form-control">
                                        <option value=""></option>
                                        <?php
                                        foreach($MSFrameworkImmobili->getPeriodicitaRataMutui() as $k => $v) {
                                            ?>
                                            <option value="<?php echo $k ?>" <?php if($mutuo['period'] == (string)$k) { echo "selected"; } ?>><?php echo $v ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label><?php echo $MSFrameworkImmobili->getDatiMutuoFields('pagamento') ?></label>
                                    <input type="text" id="mutuo_pagamento" class="form-control" value="<?php echo $mutuo['pagamento'] ?>" />
                                </div>

                                <div class="col-sm-6">
                                    <label><?php echo $MSFrameworkImmobili->getDatiMutuoFields('banca') ?></label>
                                    <input type="text" id="mutuo_banca" class="form-control" value="<?php echo $mutuo['banca'] ?>" />
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                        </fieldset>

                        <textarea style="display: none;" id="prev_upl_images"><?php echo $r['images'] ?></textarea>
                        <textarea style="display: none;" id="prev_upl_plan_images"><?php echo $r['images_plan'] ?></textarea>

                        <h1>Foto & Video</h1>
                        <fieldset>
                            <h2 class="title-divider">Foto & Video</h2>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Video immobile (URL YouTube)</label>
                                    <input type="text" id="youtube_url" class="form-control" value="<?php echo $r['youtube_url'] ?>" />
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12" style="margin-bottom: 40px;">
                                    <label>Foto immobile (minimo 500x300 pixel) - Il watermark sarà inserito in base a quanto settato in "Impostazioni Upload".</label>

                                    <?php
                                    (new \MSFramework\uploads('REALESTATE_IMMOBILI'))->initUploaderHTML("images", $only_photo);
                                    ?>
                                </div>

                                <div class="col-sm-12">
                                    <label>Planimetria (minimo 500x300 pixel)</label>

                                    <?php
                                    (new \MSFramework\uploads('REALESTATE_PLANIMETRIE'))->initUploaderHTML("plan_images", $only_photo);
                                    ?>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('titolo_immobile', 'slug', 'realestate_immobili', $('#record_id'));

    $('#category').on('change', function() {
        loadCaratteristiche(true);
    })

    $('#box_garage').on('change', function() {
        checkGarage();
    });

    $('#giardino').on('change', function() {
        checkGiardino();
    });

    $('#check_ascensore').on('ifChanged', function(event){
        checkAscensore();
    });

    $('#check_terrazzo').on('ifChanged', function(event){
        checkTerrazzo();
    });

    $('#check_spese_condominiali').on('ifChanged', function(event){
        checkSpeseCondominiali();
    });

    $('#check_balcone').on('ifChanged', function(event){
        checkBalcone();
    });

    $('#check_cantina').on('ifChanged', function(event){
        checkCantina();
    });

    $('#landing_page').on('ifToggled', function(event){
        if($(this).is(':checked')){
            $('#landing_url_container').show();
        } else {
            $('#landing_url_container').hide();
        }
    });

    if($('#scraper').val() != "") {
        $('#riferimento_immobile').on('keyup', function() {
            if($('#riferimento_immobile').val() != "") {
                $('#titolo_immobile').parent('div').removeClass('col-sm-5').addClass('col-sm-3');
                $('#scraper_btn').show();
            } else {
                $('#titolo_immobile').parent('div').removeClass('col-sm-3').addClass('col-sm-5');
                $('#scraper_btn').hide();
            }
        })

        $('#riferimento_immobile').trigger('keyup');
    }

    manageWorldDataSelects();
    loadCaratteristiche(false);

    initOrak('images', 25, 500, 300, 200, getOrakImagesToPreattach('images'), true, ['image/jpeg', 'image/png']);
    initOrak('plan_images', 25, 500, 300, 200, getOrakImagesToPreattach('plan_images'), false, ['image/jpeg', 'image/png']);

    checkAscensore();
    checkTerrazzo();
    checkSpeseCondominiali();
    checkBalcone();
    checkCantina();
    checkGarage();
    checkGiardino();
}

function launchScraperImmobili() {
    bootbox.confirm('Stai per avviare il recupero dei dati da ' + $('#scraper').val() + '. Questa operazione potrebbe richiedere qualche minuto, a seconda dei dati coinvolti. Tutti i dati eventualmente già inseriti, saranno sovrascritti dai nuovi dati. Continuare?', function(result) {
        if(result) {
            var dialog = bootbox.dialog({
                message: '<p class="text-center">Recupero dei dati in corso. Questa operazione potrebbe richiedere qualche minuto. Attendere...</p><div id="bootbox_import_progress" class="text-center">Ottengo i dati... <br /></div>',
                closeButton: false
            });

            if(typeof(scraper_download) != "undefined") {
                scraper_download.abort();
            }

            var scraper_download = $.ajax({
                url: "ajax/scrapers/" + $('#scraper').val().toLowerCase() + "/immobili.php?code=" + $('#riferimento_immobile').val(),
                async: true,
                dataType: "json",
                success: function(data) {
                    if(data.status == "url_error") {
                        dialog.modal('hide');
                        bootbox.error('Impossibile recuperare i dati. Verificare il codice "Riferimento Immobile" e riprovare.');
                    } else if(data.status == "ok") {
                        scraper_results = data.results;

                        $('#bootbox_import_progress').append('Aggiorno i dati... <br />');
                        $('#tipo_vendita').val(scraper_results.immobile_in);
                        $('#superficie_immobile').val(scraper_results.attributes.mq);

                        $('#anno_costruzione').val(scraper_results.attributes.anno_costruzione);
                        $('#prezzo_immobile').val(scraper_results.price);
                        $('#titolo_immobile').val(scraper_results.title).trigger('keyup');

                        $('#regione_sel').val(scraper_results.address.regione).trigger('change');
                        $('#provincia_sel').val(scraper_results.address.provincia).trigger('change');
                        $('#comune_sel').val(scraper_results.address.comune).trigger("chosen:updated").trigger('change');
                        $('#indirizzo_hidden').val(scraper_results.address.textual);
                        $('#indirizzo_lat').val(scraper_results.address.lat);
                        $('#indirizzo_lng').val(scraper_results.address.lng);
                        $('#indirizzo').val(scraper_results.address.textual);
                        $('#cap').val(scraper_results.address.cap);


                        $('#category').val(scraper_results.category).trigger('change');
                        $('#youtube_url').val(scraper_results.video);

                        $('.block-service input[type=checkbox]').attr('checked', '').iCheck('uncheck');
                        $(scraper_results.caratteristiche).each(function (carK, carV) {
                            $('.block-service #' + carV).attr('checked', 'checked').iCheck('check');
                        });

                        //I campi seguenti vengono resettati quando cambia il tipo di immobile!
                        $('#descrizione_immobile').val(scraper_results.ita_descr);
                        $('#totale_locali').val(scraper_results.attributes.vani);
                        $('#bagni').val(scraper_results.attributes.bagni);
                        $('#camere_da_letto').val(scraper_results.attributes.camere_da_letto);
                        $('#classe_energetica').val(scraper_results.attributes.classe_energetica);

                        var additional_import_str = "";

                        if(scraper_results.immagini.length != 0) {
                            $('#bootbox_import_progress').append('Scarico le immagini... (<span id="booxbox_img_cur">0</span>/' + scraper_results.immagini.length + ') <br />');
                            var has_image_errors = false;
                            var import_max_uploads = 25;
                            var import_done_uploads = 0;

                            var importedImgs = new Array();
                            $(scraper_results.immagini).each(function (img_urlk, image_url) {
                                $.ajax({
                                    url: $('#baseElementPathAdmin').val() + 'vendor/plugins/orakuploader/orakuploader.php?get_from_url=1&url=' + encodeURIComponent(image_url) + '&thumbnail_size=200&thumbnail_path=' + $('#baseElementPathTmpFolder').val() + 'tn&watermark=true&orakuploader_min_width=500&orakuploader_min_height=300&orakuploader_allowed_ext=image/jpeg,image/png&path=' + $('#baseElementPathAdmin').val() + 'js/plugins/orakuploader/',
                                    async: false,
                                    success: function (data) {
                                        error_descr = "";
                                        if (data == "heavy_file" || data == "min_size") {
                                            has_image_errors = true
                                        } else {
                                            import_done_uploads++;
                                            $('#booxbox_img_cur').html(import_done_uploads);
                                            importedImgs.push(data);
                                        }
                                    }
                                })

                                if (import_max_uploads == import_done_uploads) {
                                    additional_import_str += "Sono state importate solo le prime " + import_max_uploads + " immagini consentite come limite massimo.<br />";
                                    return false;
                                }
                            });

                            if (has_image_errors) {
                                additional_import_str += "Una o più immagini non sono state importate in quanto non rispettavano gli standard minimi richiesti. <br />";
                            }
                        }

                        if(scraper_results.planimetrie.length != 0) {
                            $('#bootbox_import_progress').append('Scarico le planimetrie... (<span id="booxbox_plan_cur">0</span>/' + scraper_results.planimetrie.length + ') <br />');
                            var has_plan_errors = false;
                            var import_max_uploads = 25;
                            var import_done_uploads = 0;

                            var importedPlans = new Array();
                            $(scraper_results.planimetrie).each(function (img_urlk, image_url) {
                                $.ajax({
                                    url: $('#baseElementPathAdmin').val() + 'vendor/plugins/orakuploader/orakuploader.php?get_from_url=1&url=' + encodeURIComponent(image_url) + '&thumbnail_size=200&thumbnail_path=' + $('#baseElementPathTmpFolder').val() + 'tn&watermark=true&orakuploader_min_width=500&orakuploader_min_height=300&orakuploader_allowed_ext=image/jpeg,image/png&path=' + $('#baseElementPathAdmin').val() + 'js/plugins/orakuploader/',
                                    async: false,
                                    success: function (data) {
                                        error_descr = "";
                                        if (data == "customerr_heavy_file" || data == "customerr_min_size") {
                                            has_plan_errors = true
                                        } else {
                                            import_done_uploads++;
                                            $('#booxbox_plan_cur').html(import_done_uploads);
                                            importedPlans.push(data);
                                        }
                                    }
                                })

                                if (import_max_uploads == import_done_uploads) {
                                    additional_import_str += "Sono state importate solo le prime " + import_max_uploads + " planimetrie consentite come limite massimo.<br />";
                                    return false;
                                }
                            });

                            if (has_plan_errors) {
                                additional_import_str += "Una o più planimetrie non sono state importate in quanto non rispettavano gli standard minimi richiesti. <br />";
                            }
                        }

                        if(additional_import_str != "") {
                            additional_import_str = "<br /><br />Attenzione! " + additional_import_str;
                        }

                        //modifico al volo il path per le anteprime utilizzato da orakuploader in quanto (solo in questo caso) le anteprime vanno caricate dalla TMP in fase di init
                        import_saved_realpath = $('#images_realPath').val();
                        $('#images_realPath').val($('#baseElementPathTmpFolder').val());
                        $('#images').html('');

                        initOrak('images', 25, 500, 300, 200, importedImgs, true, ['image/jpeg', 'image/png']);
                        $('#images_realPath').val(import_saved_realpath);


                        import_saved_realpath_plans = $('#plan_images').val();
                        $('#plan_images_realPath').val($('#baseElementPathTmpFolder').val());
                        $('#plan_images').html('');

                        initOrak('plan_images', 25, 500, 300, 200, importedPlans, true, ['image/jpeg', 'image/png']);
                        $('#plan_images_realPath').val(import_saved_realpath_plans);


                        dialog.modal('hide');
                        bootbox.alert('Dati importati con successo! Verificarne la correttezza prima del salvataggio!' + additional_import_str);
                    }
                },
                error: function() {
                    dialog.modal('hide');
                    bootbox.error('Si è verificato un errore durante il recupero dei dati. Riprovare più tardi!');
                }
            })
        }
    })
}

function checkGiardino() {
    if($('#giardino').val() == "" || $('#giardino').val() == "2") {
        $('#giardino_mq').closest('div.block-caratteristiche').hide();
        $('#giardino_mq').val('');
    } else {
        $('#giardino_mq').closest('div.block-caratteristiche').show();
    }
}

function checkGarage() {
    if($('#box_garage').val() == "" || $('#box_garage').val() == "3") {
        $('#box_garage_mq').closest('div.block-caratteristiche').hide();
        $('#box_garage_mq').val('');
    } else {
        $('#box_garage_mq').closest('div.block-caratteristiche').show();
    }
}

function checkAscensore() {
    if($('#check_ascensore:checked').length == 0) {
        $('#liftMq').closest('div.block-caratteristiche').hide();
        $('#liftMq').val('');
    } else {
        $('#liftMq').parent('div.block-caratteristiche').show();
    }
}

function checkCantina() {
    if($('#check_cantina:checked').length == 0) {
        $('#cantina_mq').closest('div.block-caratteristiche').hide();
        $('#cantina_mq').val('');
    } else {
        $('#cantina_mq').parent('div.block-caratteristiche').show();
    }
}

function checkTerrazzo() {
    if($('#check_terrazzo:checked').length == 0) {
        $('#terrazzo_mq').closest('div.block-caratteristiche').hide();
        $('#terrazzo_mq').val('');
    } else {
        $('#terrazzo_mq').parent('div.block-caratteristiche').show();
    }
}

function checkSpeseCondominiali() {
    if($('#check_spese_condominiali:checked').length == 0) {
        $('#spese_condominiali_importo').closest('div.block-caratteristiche').hide();
        $('#spese_condominiali_importo').val('');
    } else {
        $('#spese_condominiali_importo').parent('div.block-caratteristiche').show();
    }
}

function checkBalcone() {
    if($('#check_balcone:checked').length == 0) {
        $('#balcone_mq').closest('div.block-caratteristiche').hide();
        $('#balcone_mq').val('');
    } else {
        $('#balcone_mq').parent('div.block-caratteristiche').show();
    }
}

function loadCaratteristiche(clean) {
    if(clean) {
        $('.block-caratteristiche').each(function() {
            $(this).find('textarea, input[type=text], input[type=number], select').not('#descrizione_immobile').val('');
            $(this).find('input[type=checkbox]').attr('checked', '').iCheck('uncheck');
        })
    }

    $('.block-caratteristiche').hide();

    $('.block-avail-' + $('#category option:selected').data('macro')).show();
}

function moduleSaveFunction() {
    if($('#in_evidenza:checked').length == 1) {
        $.ajax({
            url: "db/checkEvidenza.php?id=" + $('#record_id').val() + "&agent=" + $('#agente_riferimento').val(),
            async: false,
            success: function(data) {
                if(data == "0") {
                    continueSaving(false);
                } else {
                    var dialog = bootbox.dialog({
                        title: 'Hai già immobili in evidenza',
                        message: "Ci sono già immobili in evidenza per questo agente (o un suo assistente). Puoi avere un solo immobile in evidenza. Cosa desideri fare?",
                        buttons: {
                            cancel: {
                                label: "Tieni questo in evidenza",
                                className: 'btn-info',
                                callback: function(){
                                    continueSaving(true);
                                }
                            },
                            noclose: {
                                label: "Tieni l'altro in evidenza",
                                className: 'btn-info',
                                callback: function(){
                                    continueSaving(false);
                                }
                            }
                        }
                    });
                }
            }
        })
    } else {
        continueSaving(false);
    }
}

function continueSaving(remove_oth_evidenza) {
    ary_caratteristiche = new Object();
    $('.block-caratteristiche').each(function(k, v) {
        if($(this).find('input[type=text]').length == 1) {
            if($(this).find('input').val() == "") {
                return true;
            }

            ary_caratteristiche[$(this).find('input').attr('id')] = $(this).find('input').val();
        }

        if($(this).find('input[type=number]').length == 1) {
            if($(this).find('input').val() == "") {
                return true;
            }

            ary_caratteristiche[$(this).find('input').attr('id')] = $(this).find('input').val();
        }

        if($(this).find('select').length == 1) {
            if($(this).find('select').val() == "") {
                return true;
            }

            ary_caratteristiche[$(this).find('select').attr('id')] = $(this).find('select').val();
        }

        if($(this).find('textarea').length == 1) {
            if($(this).find('textarea').val() == "" || $(this).find('textarea').attr('id') == "descrizione_immobile") {
                return true;
            }

            ary_caratteristiche[$(this).find('textarea').attr('id')] = $(this).find('textarea').val();
        }

        if($(this).find('input[type=checkbox]').length == 1) {
            if($(this).find('input[type=checkbox]:checked').length == 0) {
                return true;
            }

            ary_caratteristiche[$(this).find('input[type=checkbox]:checked').attr('id')] = "1";
        }
    });

    ary_services = new Object();
    $('.block-service').each(function(k, v) {
        if($(this).find('input[type=checkbox]').length == 1) {
            if($(this).find('input[type=checkbox]:checked').length == 0) {
                return true;
            }

            ary_services[$(this).find('input[type=checkbox]:checked').attr('id')] = "1";
        }
    });

    dati_mutuo = {
        "attuale": $('#mutuo_attuale').val(),
        "differenza_saldo": $('#mutuo_diff_a_saldo').val(),
        "tipo": $('#mutuo_tipo').val(),
        "residuo_mutuo": $('#residuo_mutuo').val(),
        "residuo_anni": $('#residuo_anni').val(),
        "period": $('#mutuo_period_rata').val(),
        "pagamento": $('#mutuo_pagamento').val(),
        "banca": $('#mutuo_banca').val(),
    };

    images_ary = new Array();
    $('input[name^=images]').each(function(k, v) {
        images_ary.push($(this).val());
    })

    images_plan_ary = new Array();
    $('input[name^=plan_images]').each(function(k, v) {
        images_plan_ary.push($(this).val());
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pCategory": $('#category').val(),
            "pTipoVendita": $('#tipo_vendita').val(),
            "pTitolo": $('#titolo_immobile').val(),
            "pRiferimento": $('#riferimento_immobile').val(),
            "pGeoData": getGeoDataAryToSave(),
            "pPrezzo": $('#prezzo_immobile').val(),
            "pStimaPrezzo": $('#stima_prezzo_immobile').val(),
            "pTrattRis": $('#trattativa_riservata:checked').length,
            "pSuperficie": $('#superficie_immobile').val(),
            "pAnnoCostr": $('#anno_costruzione').val(),
            "pAgenteRiferimento": $('#agente_riferimento').val(),
            "pStatoVendita": $('#stato_vendita').val(),
            "pCaratteristiche": ary_caratteristiche,
            "pServizi": ary_services,
            "pDescrizione": $('#descrizione_immobile').val(),
            "pMutuo": dati_mutuo,
            "pYouTube": $('#youtube_url').val(),
            "pEvidenza": $('#in_evidenza:checked').length,
            "pLandingPage": $('#landing_page:checked').length,
            "pRemoveOthEvidenza": remove_oth_evidenza,
            "pImagesAry": composeOrakImagesToSave('images'),
            "pImagesPlanAry": composeOrakImagesToSave('plan_images'),
            "pSlug": $('#slug').val(),
            "pAgency": $('#agency').val(),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            geoDataErrorsOnSave(data.status);

            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "data_format_not_valid") {
                bootbox.error("Il formato dei dati inseriti non è valido. Verificare e riprovare.");
            } else if(data.status == "rife_already_exists") {
                bootbox.error("Il codice \"Riferimento Immobile\" inserito è già stato utilizzato. Verificare e riprovare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "can_not_edit_demo_version") {
                bootbox.error("Questa è una versione demo. Non puoi modificare questi dati.");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
<?php
/**
 * RealBox
 * Date: 11/11/17
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkAnalytics = new \MSFramework\RealEstate\analytics();
$MSFrameworkImmobili = (new \MSFramework\RealEstate\immobili());

$owner_limit = "";
if($MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-5" || $MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-6") {
    $owner_limit = $MSFrameworkUsers->getUserDataFromSession('user_id');
}

foreach($MSFrameworkAnalytics->getTopViewedImmobili(10, "id, tipo_vendita, images, in_evidenza, riferimento, stato_vendita, prezzo, creation_time, visite, contatti", $owner_limit) as $r) {
    $images = json_decode($r['images'], true);

    if($r['in_evidenza'] == "0") {
        $star_icon = '<i class="fa fa-star-o"></i>';
    } else {
        $star_icon = '<i class="fa fa-star"></i>';
    }

    $array['data'][] = array(
        $star_icon,
        '<img src="' . UPLOAD_REALESTATE_IMMOBILI_FOR_DOMAIN_HTML . "tn/" . $images[0] . '" alt="" height="80" width="80">',
        $r['riferimento'] . "<br />" . $MSFrameworkImmobili->getStatoVendita($r['stato_vendita']),
        "<b><?= CURRENCY_SYMBOL; ?> " . $r['prezzo'] . "</b>",
        date("d/m/Y H:i", $r['creation_time']),
        $r['visite'],
        $r['contatti'],
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

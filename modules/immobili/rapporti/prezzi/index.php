<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Prezzo minimo</h5>
                                    </div>
                                    <?php
                                    $r = $MSFrameworkDatabase->getAssoc("SELECT MIN(prezzo) as price FROM realestate_immobili WHERE stato_vendita = '0'", array(), true);
                                    ?>
                                    <div class="ibox-content ibox-content-no-border-radius">
                                        <h1 class="no-margins"><?= CURRENCY_SYMBOL; ?> <?php echo ($r['price'] != "") ? $r['price'] : '0.00'; ?></h1>
                                        <small> </small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Prezzo massimo</h5>
                                    </div>
                                    <?php
                                    $r = $MSFrameworkDatabase->getAssoc("SELECT MAX(prezzo) as price FROM realestate_immobili WHERE stato_vendita = '0'", array(), true);
                                    ?>
                                    <div class="ibox-content ibox-content-no-border-radius">
                                        <h1 class="no-margins"><?= CURRENCY_SYMBOL; ?> <?php echo ($r['price'] != "") ? $r['price'] : '0.00'; ?></h1>
                                        <small> </small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Prezzo medio</h5>
                                    </div>
                                    <?php
                                    $r = $MSFrameworkDatabase->getAssoc("SELECT AVG(prezzo) as price FROM realestate_immobili WHERE stato_vendita = '0'", array(), true);
                                    ?>
                                    <div class="ibox-content ibox-content-no-border-radius">
                                        <h1 class="no-margins"><?= CURRENCY_SYMBOL; ?> <?php echo ($r['price'] != "") ? round($r['price'], 2) : '0.00'; ?></h1>
                                        <small> </small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Prezzo medio ricerche (min)</h5>
                                    </div>
                                    <?php
                                    $r = $MSFrameworkDatabase->getAssoc("SELECT AVG(price_from) as price FROM realestate_ricerche WHERE price_from != '0'", array(), true);
                                    ?>
                                    <div class="ibox-content ibox-content-no-border-radius">
                                        <h1 class="no-margins"><?= CURRENCY_SYMBOL; ?> <?php echo ($r['price'] != "") ? round($r['price'], 2) : '0.00'; ?></h1>
                                        <small>Il prezzo medio (minimo) cercato dagli utenti</small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Prezzo medio ricerche (max)</h5>
                                    </div>
                                    <?php
                                    $r = $MSFrameworkDatabase->getAssoc("SELECT AVG(price_to) as price FROM realestate_ricerche WHERE price_to != '0'", array(), true);
                                    ?>
                                    <div class="ibox-content ibox-content-no-border-radius">
                                        <h1 class="no-margins"><?= CURRENCY_SYMBOL; ?> <?php echo ($r['price'] != "") ? round($r['price'], 2) : '0.00'; ?></h1>
                                        <small>Il prezzo medio (massimo) cercato dagli utenti</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 12/04/2019
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkUsers->getUserDataFromSession('userlevel') === "realestate-3" || $MSFrameworkUsers->getUserDataFromSession('userlevel') === "0" || $MSFrameworkUsers->getUserDataFromSession('userlevel') === "1") {
    if ($MSFrameworkDatabase->deleteRow("realestate_ricerche", "id", $_POST['pID'])) {
        echo json_encode(array("status" => "ok"));
    } else {
        echo json_encode(array("status" => "query_error"));
    }
} else {
    echo json_encode(array("status" => "not_allowed"));
    die();
}
?>


<?php
/**
 * MSAdminControlPanel
 * Date: 12/04/2019
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkUsers->getUserDataFromSession('userlevel') === "realestate-3" || $MSFrameworkUsers->getUserDataFromSession('userlevel') === "0" || $MSFrameworkUsers->getUserDataFromSession('userlevel') === "1") {
    if($MSFrameworkDatabase->query("DELETE FROM realestate_ricerche")) {
        echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
    } else {
        echo json_encode(array("status" => "query_error"));
    }
} else {
    echo json_encode(array("status" => "not_allowed"));
    die();
}
?>
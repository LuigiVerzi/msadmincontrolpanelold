$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function deleteAll() {
    $.ajax({
        url: "db/massDelete.php",
        type: "POST",
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "ok") {
                bootbox.alert("Le ricerche sono state eliminate correttamente", function () {
                    window.location.reload();
                });
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            }
        }
    });
}
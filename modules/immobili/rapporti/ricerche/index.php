<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <?php
            if($MSFrameworkUsers->getUserDataFromSession('userlevel') === "realestate-3" || $MSFrameworkUsers->getUserDataFromSession('userlevel') === "0" || $MSFrameworkUsers->getUserDataFromSession('userlevel') === "1") {
            ?>
            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewDelGrid.php"); ?>
                <a href="#" data-toggle="modal" data-target="#modalEliminaRicerche" style="margin-left: 5px; margin-top: 4px;" class="btn btn-danger btn-outline pull-right"><i class="fa fa-trash-o"></i> Pulisci</a>
            </div>
            <?php } ?>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">

                    <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0" data-deleteonly="true">
                        <thead>
                        <tr>
                            <th style="width: 20%;">Parole chiave</th>
                            <th style="width: 15%;">Tipo Immobile</th>
                            <th style="width: 10%;">Tipo Vendita</th>
                            <th style="width: 10%;">Comune</th>
                            <th style="width: 10%;">Prezzo da / a</th>
                            <th style="width: 10%;">MQ da / a</th>
                            <th style="width: 10%;">Risultati ottenuti</th>
                            <th style="width: 10%;">Data ricerca</th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<div class="modal inmodal fade" id="modalEliminaRicerche" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Chiudi</span></button>
                <h4 class="modal-title">Elimina Ricerche</h4>
            </div>
            <div class="modal-body">
                <p>
                    Proseguendo eliminerai tutti i dati relativi alle ricerche, <b>l'operazione non può essere annullata</b>.
                </p>
                <p>
                    Sei sicuro di voler procedere?
                </p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteAll()">Elimina Ricerche</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
<?php
/**
 * RealBox
 * Date: 11/11/17
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$realEstateCategories = new \MSFramework\RealEstate\categories();

foreach((new \MSFramework\RealEstate\analytics())->getSearches(30) as $r) {

    $array['data'][] = array(
        $r['keywords'],
        $MSFrameworki18n->getFieldValue($realEstateCategories->getCategoryDetails($r['category'])[$r['category']]['nome']),
        ($r['tipo_vendita'] == "1" ? "Affitto" : 'Vendita'),
        $r['comune'],
        $r['price_from'] . " / " . $r['price_to'],
        $r['mq_from'] . " / " . $r['mq_to'],
        $r['results'],
        date("d/m/Y H:i", $r['creation_time']),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkImmobili = new \MSFramework\RealEstate\immobili();
$MSFrameworkGeonames = new \MSFramework\geonames();
$MSFrameworkAnalytics = new \MSFramework\RealEstate\analytics();

?>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <?php
                                $where_str = " WHERE id != '' ";
                                $where_ary = array();
                                if($MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-5" || $MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-6") {
                                    $where_str .= " AND owner_id = :owner_id";
                                    $where_ary = array_merge($where_ary, array(":owner_id" => $MSFrameworkUsers->getUserDataFromSession('user_id')));
                                }

                                foreach($MSFrameworkDatabase->getAssoc("SELECT titolo, id, tipo_vendita, images, geo_data FROM realestate_immobili $where_str", $where_ary) as $immobile) {
                                    $tipo_vendita = "";
                                    if ($immobile['tipo_vendita'] == "1") {
                                        $tipo_vendita = "Affitto";
                                    } else if ($immobile['tipo_vendita'] == "0") {
                                        $tipo_vendita = "Vendita";
                                    }

                                    $immagini = json_decode($immobile['images'], true);
                                    $geodata = json_decode($immobile['geo_data'], true);

                                    $ary_immobili_caricati_map_data[] = array(
                                        "lat" => $geodata['lat'],
                                        "lng" => $geodata['lng'],
                                        "title" => $immobile['titolo'],
                                        "info_window" => '<div class="ppt-list list-vw" style="width: 640px;">
                                        <figure>
                                            <span class="tag left text-uppercase bg-dark">' . $MSFrameworkImmobili->getPrice($immobile['id']) . '</span>
                                            <span class="tag right text-uppercase primary-bg">' . $tipo_vendita . '</span>
                                            <a href="' . $MSFrameworkImmobili->dettaglioImmobileURL($immobile['id']) . '" class="image-effect overlay">
                                                <img src="' . UPLOAD_REALESTATE_IMMOBILI_FOR_DOMAIN_HTML . $immagini[0] . '" alt="" width="278" height="auto" onerror="this.onerror=null;this.src=\'' . ABSOLUTE_PATH_HTML . 'assets/images/p-lv-1.jpg\';">
                                            </a>
                                        </figure>
                                        <!--fig-->
                                    
                                        <div class="content">
                                            <h4 class="mb-0"><a href="' . $MSFrameworkImmobili->dettaglioImmobileURL($immobile['id']) . '">' . $immobile['titolo'] . '</a></h4>
                                            <div class="mb-15">' . $MSFrameworkGeonames->getDettagliProvincia($geodata['provincia'])['name'] . ' / ' . $MSFrameworkGeonames->getDettagliComune($geodata['comune'])['name'] . '</div>
                                            <br />
                                            <a href="' . $MSFrameworkImmobili->dettaglioImmobileURL($immobile['id']) . '" class="btn btn-sucess faa-parent animated-hover">
                                                Vedi immobile <i class="fa fa-long-arrow-right faa-passing"></i>
                                            </a>
                                        </div>
                                        <!--content-->
                                    </div>',
                                    );
                                }

                                ?>
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Immobili caricati</h5>
                                    </div>
                                    <textarea id="immobili_caricati_map_data" style="display: none;"><?php echo json_encode($ary_immobili_caricati_map_data) ?></textarea>
                                    <div class="ibox-content ibox-content-no-border-radius">
                                        <h1 class="no-margins"><div id="immobili_caricati_map" style="height: 400px;"></div></h1>
                                        <small> </small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <?php
                                foreach($MSFrameworkAnalytics->getSearches(-1, 'comune', ' AND (comune != "")') as $r) {
                                    $coord_data = $MSFrameworkGeonames->getDettagliComune($MSFrameworkGeonames->searchForComuneID($r['comune']), 'latitude, longitude, name');
                                    if($coord_data['name'] == "") {
                                        continue;
                                    }

                                    $ary_immobili_cercati_map_data[] = array(
                                        "lat" => $coord_data['latitude'],
                                        "lng" => $coord_data['longitude'],
                                        "title" => 'Città di ' . $coord_data['name'],
                                        "info_window" => 'Città di ' . $coord_data['name'],
                                    );
                                }
                                ?>

                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Immobili cercati</h5>
                                    </div>

                                    <textarea id="immobili_cercati_map_data" style="display: none;"><?php echo json_encode($ary_immobili_cercati_map_data) ?></textarea>
                                    <div class="ibox-content ibox-content-no-border-radius">
                                        <h1 class="no-margins"><div id="immobili_cercati_map" style="height: 400px;"></div></h1>
                                        <small> </small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <?php
                                foreach($MSFrameworkAnalytics->getSearches(-1, 'user_ip') as $r) {
                                    $ip_data = json_decode($r['user_ip'], true);

                                    $ary_provenienza_ricerche_map_data[] = array(
                                        "lat" => $ip_data[1]['lat'],
                                        "lng" => $ip_data[1]['lng'],
                                        "title" => 'Ricerca proveniente da ' . $ip_data[1]['name'] . " (posizione approssimativa)",
                                        "info_window" => 'Ricerca proveniente da ' . $ip_data[1]['name'] . " (posizione approssimativa)",
                                    );
                                }
                                ?>

                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Provenienza ricerche</h5>
                                    </div>

                                    <textarea id="provenienza_ricerche_map_data" style="display: none;"><?php echo json_encode($ary_provenienza_ricerche_map_data) ?></textarea>
                                    <div class="ibox-content ibox-content-no-border-radius">
                                        <h1 class="no-margins"><div id="provenienza_ricerche_map" style="height: 400px;"></div></h1>
                                        <small> </small>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
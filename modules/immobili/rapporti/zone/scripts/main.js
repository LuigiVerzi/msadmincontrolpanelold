function initMap() {
    initMaps('immobili_caricati_map');
    initMaps('immobili_cercati_map');
    initMaps('provenienza_ricerche_map');
}

function initMaps(map_id) {
    var bounds = new google.maps.LatLngBounds();

    mapData = $.parseJSON($('#' + map_id + '_data').val()) || new Array();
    locations = new Array();
    $(mapData).each(function(k, v) {
        tmp_loc = new Array();

        tmp_loc.lat = v.lat*1;
        tmp_loc.lng = v.lng*1;
        tmp_loc.title = v.title;

        tmp_loc.infowindow = new google.maps.InfoWindow({
            content: '<div id="content">'+
            v.info_window +
            '</div>'
        });

        locations.push(tmp_loc);

        bounds.extend(new google.maps.LatLng(v.lat*1, v.lng*1));
    });

    var map = new google.maps.Map(document.getElementById(map_id), {
        /*
         zoom: 7,
         center: {lat: 41.854603, lng: 14.20132000000001}
         */
    });

    map.fitBounds(bounds); //centro dinamicamente la mappa in base alle coordinate di tutti gli immobili presenti

    var markers = locations.map(function (location, i) {
        marker = new google.maps.Marker({
            position: location,
            title: location.title
        });

        marker.addListener('click', function() {
            location.infowindow.open(map, this);
        });

        return marker;
    });

    var markerCluster = new MarkerClusterer(map, markers, {imagePath: $('#baseElementPathAdmin').val() + "vendor/js/google/maps/markerclusterer/img/"});
}
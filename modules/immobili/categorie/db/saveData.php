<?php
/**
 * MSAdminControlPanel
 * Date: 17/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pSlug'] == "" || $_POST['pMacro'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if(!$MSFrameworkUrl->checkSlugAvailability($_POST['pSlug'], "realestate_categories", $_POST['pID'])) {
    $_POST['pSlug'] = $MSFrameworkUrl->findNextAvailableSlug($_POST['pSlug']);
}

mkdir(UPLOAD_REALESTATE_CATEGORIES_FOR_DOMAIN, 0777, true);
mkdir(UPLOAD_REALESTATE_CATEGORIES_FOR_DOMAIN . "tn/", 0777, true);

$uploader = new \MSFramework\uploads('realestate_categories');
$ary_files = $uploader->prepareForSave($_POST['pImagesAry']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$uploaderBanner = new \MSFramework\uploads('realestate_categories');
$ary_banner = $uploaderBanner->prepareForSave($_POST['pBannerAry']);
if($ary_banner === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT nome, slug, descr, images, banner FROM realestate_categories WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug']),
        array('currentValue' => $_POST['pDescr'], 'oldValue' => $r_old_data['descr']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
    "descr" => $MSFrameworki18n->setFieldValue($r_old_data['descr'], $_POST['pDescr']),
    "menu_colors" => json_encode(array("bgColor" => $_POST['pMenuBGColor'], "color" => $_POST['pMenuColor'], "mouseover" => $_POST['pMenuMouseOver'])),
    "images" => json_encode($ary_files),
    "banner" => json_encode($ary_banner),
    "multiportal_links" => json_encode($_POST['pMultiportal']),
    "macro" => $_POST['pMacro'],
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO realestate_categories ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $last_insert_id = $MSFrameworkDatabase->lastInsertId();
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE realestate_categories SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $last_insert_id = $_POST['pID'];
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
        $uploaderBanner->unlink($ary_banner);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['images'], true));
    $uploaderBanner->unlink($ary_banner, json_decode($r_old_data['banner'], true));
}

echo json_encode(array("status" => "ok", "id" => $last_insert_id, "last_insert_id" => $last_insert_id, "last_insert_name" => $_POST['pNome']));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 17/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$childs_id = array($_POST['pID']);
foreach((new \MSFramework\RealEstate\categories())->getCategoryChildrens($_POST['pID']) as $child) {
    $childs_id[] = $child['id'];
}

$same_db_string_ary = $MSFrameworkDatabase->composeSameDBFieldString($childs_id, "OR", "category");
if($MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili WHERE id != ''  AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) != 0) {
    echo json_encode(array("status" => "category_with_data"));
    die();
}

$r = $MSFrameworkDatabase->getAssoc("SELECT images, banner FROM realestate_categories WHERE id = :id", array(":id" => $_POST['pID']), true);
if($MSFrameworkDatabase->deleteRow("realestate_categories", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('REALESTATE_CATEGORIES'))->unlink(json_decode($r['images'], true));
    (new \MSFramework\uploads('REALESTATE_CATEGORIES'))->unlink(json_decode($r['banner'], true));

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
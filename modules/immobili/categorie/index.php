<?php
/**
 * MSAdminControlPanel
 * Date: 17/04/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('menu');

$categoryDetails = (new \MSFramework\RealEstate\categories)->getCategoryDetails();

function composeMenuOrder($ary_menu, $menu_type, $ind_level = 1) {
    global $categoryDetails, $MSFrameworki18n;

    if(!is_array($ary_menu)) {
        return "";
    }

    if($menu_type == "order") {
        $html = '<ol class="dd-list">';

        foreach($ary_menu as $cur_menu) {
            $html .= '<li class="dd-item" data-id="' . $cur_menu['id'] . '"><div class="dd-handle">' . $MSFrameworki18n->getFieldValue($categoryDetails[$cur_menu['id']]['nome']) . '</div>';

            if(is_array($cur_menu['children'])) {
                $html .= composeMenuOrder($cur_menu['children'], $menu_type);
            }

            $html .= "</li>";
        }

        $html .= "</ol>";
    } else if($menu_type == "select") {
        $html = "";

        foreach($ary_menu as $cur_menu) {
            $nbsp = "";
            for($x=1; $x<$ind_level; $x++) {
                $nbsp .= "&nbsp;";
            }

            $html .= '<option value="' . $cur_menu['id'] . '">' . $nbsp . " - " . $MSFrameworki18n->getFieldValue($categoryDetails[$cur_menu['id']]['nome']) . '</option>';

            if(is_array($cur_menu['children'])) {
                $ind_level = $ind_level*3;
                $html .= composeMenuOrder($cur_menu['children'], $menu_type, $ind_level);
            }

            if($ind_level > 0) {
                $ind_level = $ind_level/3;
            }
        }
    }
    return $html;
}

function displayAllOrderedIds($arr) {
    $ids = array();
    if ($arr) {
        foreach ($arr as $value) {
            if(isset($value['id'])) $ids[] = $value['id'];
            if (isset($value['children'])) {
                $children_array = displayAllOrderedIds($value['children']);
                $ids = array_merge($ids, $children_array);
            }
        }
    }
    return $ids;
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Gestione Categorie</h1>
                        <fieldset>

                            <div class="row">
                                <div class="col-md-6">
                                    <div><?php echo ($_GET['id'] == "") ? "Aggiungi" : "Modifica"?> Categoria</div>
                                    <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

                                    <div class="row m-t">
                                        <div class="col-sm-12">
                                            <label>Nome*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($categoryDetails[$_GET['id']]['nome']) ?>"></i></span>
                                            <input type="text" class="form-control required" id="nome" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($categoryDetails[$_GET['id']]['nome'])) ?>">
                                        </div>

                                        <div class="col-sm-12">
                                            <label>Slug*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($categoryDetails[$_GET['id']]['slug']) ?>"></i></span>
                                            <input type="text" class="form-control required" id="slug" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($categoryDetails[$_GET['id']]['slug'])) ?>">
                                        </div>
                                    </div>

                                    <div class="row m-t">
                                        <?php if($_GET['id'] == "") { ?>
                                        <div class="col-sm-12">
                                            <label>Genitore</label>
                                            <select id="parent" name="parent" class="form-control">
                                                <option value="">- Nessun Genitore -</option>
                                                <?php echo composeMenuOrder($MSFrameworkCMS->getCMSData('realestate_cats_order')['list'], "select") ?>
                                            </select>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <div class="row m-t">
                                        <div class="col-sm-12">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($categoryDetails[$_GET['id']]['descr']) ?>"></i></span>
                                            <textarea id="description" class="form-control"><?php echo $MSFrameworki18n->getFieldValue($categoryDetails[$_GET['id']]['descr']) ?></textarea>
                                        </div>
                                    </div>


                                    <div class="row m-t">
                                        <?php
                                        $extra_module_settings = (new \MSFramework\modules())->getExtraModuleSettings('multiportal');
                                        if($extra_module_settings['gestionalere']['agency_id'] != "") {
                                        $multiportal_links = json_decode($categoryDetails[$_GET['id']]['multiportal_links'], true);
                                        ?>
                                        <div class="col-sm-4">
                                            <label>ID GestionaleRe</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="L'integrazione multiportale con GestionaleRe è attiva per questo sito. Per associare questa categoria alla corrispondente di GestionaleRe, inserire qui l'ID fornito da GestionaleRe."></i></span>
                                            <input type="text" class="form-control" id="gestionalere_cat_id" value="<?php echo htmlentities($multiportal_links['gestionalere']) ?>">
                                        </div>
                                        <?php } ?>

                                        <div class="col-sm-6">
                                            <label>Macrocategoria di appartenenza*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="In base alla macrocategoria selezionata, dove necessario, verranno mostrate caratteristiche dell'immobile pertinenti in base alla selezione"></i></span>
                                            <select id="macro" class="form-control">
                                                <option value=""></option>
                                                <option value="0" <?= ($categoryDetails[$_GET['id']]['macro'] == "0" ? "selected" : "") ?>>Residenziale</option>
                                                <option value="1" <?= ($categoryDetails[$_GET['id']]['macro'] == "1" ? "selected" : "") ?>>Immobili di Lusso</option>
                                                <option value="2" <?= ($categoryDetails[$_GET['id']]['macro'] == "2" ? "selected" : "") ?>>Immobili all'asta</option>
                                                <option value="3" <?= ($categoryDetails[$_GET['id']]['macro'] == "3" ? "selected" : "") ?>>Commerciale</option>
                                                <option value="4" <?= ($categoryDetails[$_GET['id']]['macro'] == "4" ? "selected" : "") ?>>Nuove Costruzioni</option>
                                                <option value="5" <?= ($categoryDetails[$_GET['id']]['macro'] == "5" ? "selected" : "") ?>>Terreni</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="row m-t">
                                        <div class="col-sm-12">
                                            <label>Personalizzazione Menu</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>Sfondo</label>
                                            <div class="input-group colorpicker-component">
                                                <input id="menu_bgcolor" name="menu_bgcolor" type="text" class="form-control" value="<?php echo htmlentities(@json_decode($categoryDetails[$_GET['id']]['menu_colors'], true)['bgColor']) ?>">
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label>Mouseover</label>
                                            <div class="input-group colorpicker-component">
                                                <input id="menu_mouseover" name="menu_mouseover" type="text" class="form-control" value="<?php echo htmlentities(@json_decode($categoryDetails[$_GET['id']]['menu_colors'], true)['mouseover']) ?>">
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label>Testo</label>
                                            <div class="input-group colorpicker-component">
                                                <input id="menu_color" name="menu_color" type="text" class="form-control" value="<?php echo htmlentities(@json_decode($categoryDetails[$_GET['id']]['menu_colors'], true)['color']) ?>">
                                                <span class="input-group-addon"><i></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row m-t">
                                        <div class="col-sm-6">
                                            <label>Icona</label>
                                            <?php
                                            (new \MSFramework\uploads('REALESTATE_CATEGORIES'))->initUploaderHTML("images", $categoryDetails[$_GET['id']]['images']);
                                            ?>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Banner</label>
                                            <?php
                                            (new \MSFramework\uploads('REALESTATE_CATEGORIES'))->initUploaderHTML("banner", $categoryDetails[$_GET['id']]['banner']);
                                            ?>
                                        </div>
                                    </div>

                                    <?php
                                    if($_GET['id'] != "") {
                                    ?>
                                    <div class="row m-t">
                                        <div class="col-sm-12">
                                            <a class="btn btn-danger" id="delCatBtn">Elimina Categoria</a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>

                                <div class="col-md-6">
                                    <div>Ordinamento Categorie</div>
                                    <div class="dd" id="finalList">
                                        <?php echo composeMenuOrder($MSFrameworkCMS->getCMSData('realestate_cats_order')['list'], "order") ?>
                                        <?php
                                        /*
                                        $ordered_ids = displayAllOrderedIds($MSFrameworkCMS->getCMSData('realestate_cats_order')['list']);
                                        foreach((new \MSFramework\Ecommerce\categories())->getCategoryDetails() as $cat) {
                                            if(!in_array($cat['id'], $ordered_ids)) {
                                                echo '<li class="dd-item" data-id="' . $cat['id'] . '"><div class="dd-handle">' . $MSFrameworki18n->getFieldValue($cat['nome']) . '</div>';
                                            }
                                        }
                                        */
                                        ?>
                                    </div>
                                </div>
                            </div>
                            
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
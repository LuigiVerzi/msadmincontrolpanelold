function initForm() {
    initClassicTabsEditForm();
    initSlugChecker('nome', 'slug', 'realestate_categories', $('#record_id'));

    initOrak('images', 1, 100, 100, 200, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
    initOrak('banner', 1, 400, 200, 200, getOrakImagesToPreattach('banner'), false, ['image/jpeg', 'image/png']);

    $('#finalList').nestable({
        callback: function(l,e){
            saveCatsOrder(true, '');
        },
        onClick: function(l,e) {
            window.location.href = 'index.php?id=' + $(e).data('id');
        }
    });

    $('#delCatBtn').on('click', function() {
        sel_id = $('#record_id').val();
        if(typeof(sel_id) == "undefined" || sel_id == "") {
            bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
            return false;
        }

        bootbox.confirm('Sei sicuro di voler eliminare? I dati non saranno recuperabili.', function(result) {
            if(result) {
                $.ajax({
                    url: "db/deleteData.php",
                    type: "POST",
                    data: {
                        "pID": sel_id,
                    },
                    async: false,
                    dataType: "json",
                    success: function(data) {
                        if(data.status == "ok") {
                            $('#finalList').nestable('remove', sel_id, true);
                            saveCatsOrder(true, data);
                        } else if(data.status == "query_error") {
                            bootbox.error("Questo è davvero imbarazzante! Non posso eliminare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
                        } else if(data.status == "not_allowed") {
                            bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
                        } else if(data.status == "category_with_data") {
                            bootbox.error("Impossibile eliminare questa categoria in quanto sono presenti altri elementi che fanno riferimento ad essa.");
                        }
                    }
                })
            }
        })
    })

    $('.colorpicker-component').colorpicker();
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pSlug": $('#slug').val(),
            "pParent": $('#parent').val(),
            "pDescr": $('#description').val(),
            "pMenuBGColor": $('#menu_bgcolor').val(),
            "pMenuColor": $('#menu_color').val(),
            "pMenuMouseOver": $('#menu_mouseover').val(),
            "pMacro": $('#macro').val(),
            "pImagesAry": composeOrakImagesToSave('images'),
            "pBannerAry": composeOrakImagesToSave('banner'),
            "pMultiportal": {
                'gestionalere' : $('#gestionalere_cat_id').val()
            },
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "ok") {
                saveCatsOrder(false, data);
            }
        }
    })
}

function saveCatsOrder(from_callback, data) {
    if(!from_callback) {
        if($('#finalList .dd-empty').length != 0) {
            $('#finalList').append('<ol class="dd-list"></ol>');
            $('#finalList .dd-empty').remove();
        }

        if($('#record_id').val() == "") {
            if($('#parent').val() == "") {
                finalListData = {"id" : data.last_insert_id};
            } else {
                finalListData = {"id" : data.last_insert_id, "parent_id" : $('#parent').val()};
            }

            $('#finalList').nestable('add', finalListData);
        }

        $('#finalList').find('.dd-item[data-id=' + data.last_insert_id + ']').find('.dd-handle').html(data.last_insert_name);
    }

    $.ajax({
        url: "db/saveOrderData.php",
        type: "POST",
        data: {
            "pFinalList": $('#finalList').nestable('serialize'),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok" && !from_callback) {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
$(document).ready(function() {
    loadStandardDataTable('affiliated_table_list', true, {"ajax": 'ajax/dataTable.php?source=affiliated'});
    allowRowHighlights('affiliated_table_list');
    manageGridButtons('affiliated_table_list');

    loadStandardDataTable('not_affiliated_table_list', true, {"ajax": 'ajax/dataTable.php?source=not_affiliated'});
    allowRowHighlights('not_affiliated_table_list');
    manageGridButtons('not_affiliated_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    if($('#sponsor').length) {
        $('#sponsor').select2({
            ajax: {
                url: "ajax/getCustomersSelect.php",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.items
                    }
                },
                cache: true
            },
            placeholder: 'Cerca un Cliente',
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 0
        });
    }
    loadStandardDataTable('utenti_sponsorizzati', false, {ajax: false}, false);

}

function moduleSaveFunction() {

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pSponsor": $('#sponsor').val(),
            "pAffiliationCode": $('#affiliation_code').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data non è valido. Impossibile continuare.");
            } else if(data.status == "past_expiration_date") {
                bootbox.error("La data di scadenza deve essere successiva a quella di inizio.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
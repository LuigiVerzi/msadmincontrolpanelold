<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();

if($_GET['source'] == 'affiliated') {
    $columns = array(
        array(
            'db' => 'id',
            'dt' => 'DT_buttonsLimit',
            'formatter' => function ($d, $row) {
                return 'edit';
            }
        ),
        array(
            'db' => 'id',
            'dt' => 'DT_RowId',
            'formatter' => function ($d, $row) {
                return $d;
            }
        ),
        array('db' => 'CONCAT(nome,cognome,email)', 'dt' => 0,
            'formatter' => function ($d, $row) {
                Global $MSFrameworkCustomers;

                $r = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

                $info = array();
                if (!empty($r['email'])) {
                    $info[] = $r['email'];
                }
                if (!empty($r['telefono_casa'])) {
                    $info[] = $r['telefono_casa'];
                }
                if (!empty($r['telefono_cellulare'])) {
                    $info[] = $r['telefono_cellulare'];
                }

                $return_html = '';

                $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
                if(json_decode($r['avatar'])) {
                    $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
                }

                $return_html .= '<div style="position: relative; padding-left: 65px;">';
                $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
                $return_html .= '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
                $return_html .= '</div>';

                return $return_html;

            }
        ),
        array('db' => 'id', 'dt' => 1,
            'customOrder' => '(SELECT sponsored_by FROM affiliations__customers WHERE affiliations__customers.id = customers.id)',
            'formatter' => function ($d, $row) {
                Global $MSFrameworkCustomers, $MSFrameworkDatabase;

                $sponsor_info = $MSFrameworkDatabase->getAssoc("SELECT * FROM affiliations__customers WHERE id = :id", array(':id' => $d), true);

                $r = 0;
                if ($d) $r = $MSFrameworkCustomers->getCustomerDataFromDB($sponsor_info['sponsored_by']);

                if ($r) {
                    $info = array();
                    if (!empty($r['email'])) {
                        $info[] = $r['email'];
                    }
                    if (!empty($r['telefono_casa'])) {
                        $info[] = $r['telefono_casa'];
                    }
                    if (!empty($r['telefono_cellulare'])) {
                        $info[] = $r['telefono_cellulare'];
                    }

                    $return_html = '';

                    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
                    if(json_decode($r['avatar'])) {
                        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
                    }

                    $return_html .= '<div style="position: relative; padding-left: 65px;">';
                    $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
                    $return_html .= '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
                    $return_html .= '</div>';

                    return $return_html;

                } else {
                    return '<span class="text-muted">Nessuno</span>';
                }
            }
        ),
        array('db' => 'id', 'dt' => 2,
            'customOrder' => '(SELECT COUNT(*) FROM affiliations__customers WHERE affiliations__customers.sponsored_by = customers.id)',
            'formatter' => function ($d, $row) {
                Global $MSFrameworkDatabase;
                return $MSFrameworkDatabase->getCount("SELECT id FROM affiliations__customers WHERE sponsored_by = :sponsored_by", array(':sponsored_by' => $d));

            }
        ),
        array('db' => '(SELECT affiliation_code FROM affiliations__customers WHERE affiliations__customers.id = customers.id)', 'dt' => 3,
            'formatter' => function ($d, $row) {
                Global $MSFrameworkDatabase;
                if ($d) {
                    return '<span class="label label-primary">' . $d . '</span>';
                } else {
                    return '<span class="label label-danger">N/A</span>';
                }
            }
        )
    );

    echo json_encode(
        $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'customers', 'id', $columns, null, 'customers.id IN (SELECT id FROM affiliations__customers) AND (SELECT affiliation_code FROM affiliations__customers WHERE id = customers.id) != \'\'')
    );

} else {
    $columns = array(
        array(
            'db' => 'id',
            'dt' => 'DT_buttonsLimit',
            'formatter' => function ($d, $row) {
                return 'edit';
            }
        ),
        array(
            'db' => 'id',
            'dt' => 'DT_RowId',
            'formatter' => function ($d, $row) {
                return $d;
            }
        ),
        array('db' => 'CONCAT(nome,cognome,email)', 'dt' => 0,
            'formatter' => function ($d, $row) {
                Global $MSFrameworkCustomers;

                $r = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

                $info = array();
                if (!empty($r['email'])) {
                    $info[] = $r['email'];
                }
                if (!empty($r['telefono_casa'])) {
                    $info[] = $r['telefono_casa'];
                }
                if (!empty($r['telefono_cellulare'])) {
                    $info[] = $r['telefono_cellulare'];
                }

                $return_html = '';

                $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
                if(json_decode($r['avatar'])) {
                    $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
                }

                $return_html .= '<div style="position: relative; padding-left: 65px;">';
                $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
                $return_html .= '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
                $return_html .= '</div>';

                return $return_html;

            }
        ),
        array('db' => 'id', 'dt' => 1,
            'customOrder' => '(SELECT sponsored_by FROM affiliations__customers WHERE affiliations__customers.id = customers.id)',
            'formatter' => function ($d, $row) {
                Global $MSFrameworkCustomers, $MSFrameworkDatabase;

                $sponsor_info = $MSFrameworkDatabase->getAssoc("SELECT * FROM affiliations__customers WHERE id = :id", array(':id' => $d), true);

                $r = 0;
                if ($d) $r = $MSFrameworkCustomers->getCustomerDataFromDB($sponsor_info['sponsored_by']);

                if ($r) {
                    $info = array();
                    if (!empty($r['email'])) {
                        $info[] = $r['email'];
                    }
                    if (!empty($r['telefono_casa'])) {
                        $info[] = $r['telefono_casa'];
                    }
                    if (!empty($r['telefono_cellulare'])) {
                        $info[] = $r['telefono_cellulare'];
                    }

                    $return_html = '';

                    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
                    if(json_decode($r['avatar'])) {
                        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
                    }

                    $return_html .= '<div style="position: relative; padding-left: 65px;">';
                    $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
                    $return_html .= '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
                    $return_html .= '</div>';

                    return $return_html;

                } else {
                    return '<span class="text-muted">Nessuno</span>';
                }


            }
        )
    );

    echo json_encode(
        $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'customers', 'id', $columns, null, 'customers.id NOT IN (SELECT id FROM affiliations__customers) OR (SELECT affiliation_code FROM affiliations__customers WHERE id = customers.id) = \'\'')
    );
}

<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                    <h1>Affiliati</h1>
                    <fieldset>
                        <table id="affiliated_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Cliente</th>
                                <th>Sponsorizzato Da</th>
                                <th>Utenti Sponsorizzati</th>
                                <th>Codice Affiliazione</th>
                            </tr>
                            </thead>
                        </table>
                    </fieldset>

                    <h1>Non Affiliati</h1>
                    <fieldset>
                        <table id="not_affiliated_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Cliente</th>
                                <th>Sponsorizzato Da</th>
                            </tr>
                            </thead>
                        </table>
                    </fieldset>
                </form>

            </div>
        </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
<script>
    globalInitForm();
</script>
</html>
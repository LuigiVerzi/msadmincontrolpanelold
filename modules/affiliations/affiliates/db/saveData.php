<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = ($MSFrameworkDatabase->getCount("SELECT * from affiliations__customers WHERE id = :id", array(':id' => $_POST['pID'])) ? 'update' : 'insert');

$can_save = true;

if($_POST['pID'] == "" || $_POST['pSponsor'] == "" || $_POST['pAffiliationCode'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

/* AGGIORNAMENTO CODICE SPONSOR */
$clean_code = $MSFrameworkUrl->cleanString($_POST['pAffiliationCode']);

// Controllo se i campi sono compilati correttamente
if(empty($clean_code) || strlen($clean_code) < 3) {
    echo json_encode(array("status" => "input_error", "input" => "affiliation_code", "message" => $MSFrameworki18n->gettext('Il codice deve contenere almeno 3 caratteri.')));
    die();
}

if($clean_code != strtolower($_POST['pAffiliationCode'])) {
    echo json_encode(array("status" => "input_error", "input" => "affiliation_code", "message" => $MSFrameworki18n->gettext('Il codice cliente non ammette caratteri speciali e spazi.')));
    die();
}

$customer_id = (new \MSFramework\Affiliations\customers())->getCustomerIDByCode($clean_code);

if($customer_id > 0 && $customer_id != $_POST['pID']) {
    echo json_encode(array("status" => "input_error", "input" => "affiliation_code", "message" => $MSFrameworki18n->gettext('Questo codice utente è già utilizzato.')));
    die();
}


$array_to_save = array(
    "affiliation_code" => $_POST['pAffiliationCode'],
    "sponsored_by" => $_POST['pSponsor']
);

if($db_action == 'insert') {
    $array_to_save["id"] = $_POST['pID'];
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO affiliations__customers ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE affiliations__customers SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
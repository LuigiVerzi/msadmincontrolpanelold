<?php
/**
 * MSAdminControlPanel
 * Date: 07/03/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = array();
if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT *, customers.nome, customers.cognome, customers.email, customers.telefono_casa, customers.telefono_cellulare FROM customers LEFT JOIN affiliations__customers ON customers.id = affiliations__customers.id WHERE customers.id = :id", array(':id' => $_GET['id']), true);
}

if(!$r) {
    header('Location: index.php');
    die();
}

$sponsor_details = array();
if($r['sponsored_by']) {
    $sponsor_detail = (new \MSFramework\customers())->getCustomerDataFromDB($r['sponsored_by']);
    $sponsor_details['id'] = $r['sponsored_by'];
    $sponsor_details['value'] = $sponsor_detail['nome'] . ' ' . $sponsor_detail['cognome'] . ' (' . $sponsor_detail['email']  . ')';
}

$clienti_sponsorizzati = $MSFrameworkDatabase->getAssoc('SELECT * FROM affiliations__customers WHERE sponsored_by = :id', array(':id' => $r['id']));

$MSFrameworkCustomers = new \MSFramework\customers();

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Generali</h1>
                        <fieldset>

                            <h2 class="title-divider">Modifica sponsorizzazione</h2>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Cliente</label>
                                    <?php echo '<p class="form-control"><a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'customers/list/edit.php?id=' . $r['id'] . '" target="_blank">' . $r['nome'] . ' ' . $r['cognome'] . ' (' . $r['email']  . ')</a></p>'; ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Sponsor*</label>
                                    <select id="sponsor" name="sponsor" class="form-control chosen-select required" data-placeholder="Cerca tra i clienti">
                                        <?php if($sponsor_details) { ?>
                                            <option value="<?= $sponsor_details['id']; ?>" selected><?= $sponsor_details['value'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Codice Sponsor*</label>
                                    <input type="text" class="form-control required" id="affiliation_code" name="affiliation_code" value="<?php echo htmlentities($r['affiliation_code']); ?>">
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <h2 class="title-divider">Lista clienti sponsorizzati</h2>

                            <table id="utenti_sponsorizzati" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Codice Sponsor</th>
                                    <th>Data Sponsorizzazione</th>
                                    <th style="width: 35px;"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($clienti_sponsorizzati as $cliente_sponsorizzato) { ?>

                                    <?php
                                    $cliente = $MSFrameworkCustomers->getCustomerDataFromDB($cliente_sponsorizzato['id']);

                                    $info_cliente = array();
                                    if (!empty($cliente['email'])) {
                                        $info_cliente[] = $cliente['email'];
                                    }
                                    if (!empty($cliente['telefono_casa'])) {
                                        $info_cliente[] = $cliente['telefono_casa'];
                                    }
                                    if (!empty($cliente['telefono_cellulare'])) {
                                        $info_cliente[] = $cliente['telefono_cellulare'];
                                    }
                                    ?>
                                    <tr>
                                        <td><?= '<h2 class="no-margins">' . $cliente['nome'] . ' ' . $cliente['cognome'] . '</h2><small>' . implode(' - ', $info_cliente) . '</small>'; ?></td>
                                        <td><?= $cliente_sponsorizzato['affiliation_code']; ?></td>
                                        <td><?= date("d/m/Y H:i", strtotime($cliente['registration_date'])); ?></td>
                                        <td><div class="text-right"><a href="edit.php?id=<?= $cliente_sponsorizzato['id']; ?>" class="btn btn-warning btn-outline btn-sm"><i class="fa fa-pencil"></i></a></div></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
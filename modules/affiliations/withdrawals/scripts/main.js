$(document).ready(function() {
    if(!$('#main_table_list').hasClass('balance_table')) {
        loadStandardDataTable('customers_with_withdrawals_table', true, {"ajax": 'ajax/dataTable.php?source=attesa'});
        allowRowHighlights('customers_with_withdrawals_table');
        manageGridButtons('customers_with_withdrawals_table');

        loadStandardDataTable('customers_without_withdrawals_table', true, {"ajax": 'ajax/dataTable.php?source=completati'});
        allowRowHighlights('customers_without_withdrawals_table');
        manageGridButtons('customers_without_withdrawals_table');
    }
});

function initForm() {
    initClassicTabsEditForm();


    $('.updateOrderStatus').on('click', function () {
        $.ajax({
            url: "ajax/setWithdrawalStatus.php",
            type: "POST",
            data: {
                "id": $('#record_id').val(),
                "status": $(this).data('status')
            },
            async: true,
            success: function () {
                window.location.reload();
            }
        });
    });
    $('#valore').change();
}
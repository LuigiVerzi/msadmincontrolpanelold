<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(!isset($_GET['id']) || !$_GET['id']) {
    header('Location: index.php');
    die();
}

/* INFORMAZIONI PRELIEVO */
$r = $MSFrameworkDatabase->getAssoc("SELECT * FROM affiliations__withdrawals WHERE id = :id", array(':id' => $_GET['id']), true);
if(!$r) header('Location: index.php');

if(json_decode($r['referenza_pagamento'])) {
    $r['referenza_pagamento'] = json_decode($r['referenza_pagamento'], true);
}

/* INFORMAZIONI UTENTE */
$customer = (New \MSFramework\customers())->getCustomerDataFromDB($r['cliente']);
$bilancio = (new \MSFramework\customers())->getCustomerBalance($customer['id']);

$info_localita = array();

if(!empty($customer['indirizzo'])) {
    $info_localita[] = $customer['indirizzo'];
}
if(!empty($customer['citta'])) {
    $info_localita[] = $customer['citta'];
}
if(!empty($customer['provincia'])) {
    $info_localita[] = $customer['cap'];
}

$info_principali = array();
if(!empty($customer['email'])) {
    $info_principali[] = $customer['email'];
}
if(!empty($customer['telefono_casa'])) {
    $info_principali[] = $customer['telefono_casa'];
}
if(!empty($customer['telefono_cellulare'])) {
    $info_principali[] = $customer['telefono_cellulare'];
}

$avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
if(json_decode($customer['avatar'])) {
    $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($customer['avatar'], true)[0];
}

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-md-3">

                    <div class="contact-box">
                        <div class="profile-image">
                            <img src="<?= $avatar_url; ?>" class="rounded-circle circle-border m-b-md" alt="profile">
                        </div>
                        <div class="profile-info">
                            <div class="">
                                <div>
                                    <h2 class="no-margins">
                                        <?= $customer['nome']; ?> <?= $customer['cognome']; ?>
                                    </h2>
                                    <h4>Registrato dal <?= date('d/m/Y H:i', strtotime($customer['registration_date'])); ?></h4>
                                    <address class="m-t-sm">
                                        <?= implode('<br>', $info_principali); ?>
                                    </address>
                                    <address class="m-t-md">
                                        <?= implode('<br>', $info_localita); ?>
                                    </address>
                                </div>
                            </div>
                        </div>
                        <div class="contact-box-footer" style="padding: 10px 0 0;">
                            <div class="widget style1 bg-<?= ($bilancio < 0 ? 'danger' : ($bilancio == 0 ? 'warning' : 'primary')); ?>" style="margin: 0;">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="fa fa-ticket fa-5x"></i>
                                    </div>
                                    <div class="col-xs-8 text-right">
                                        <span>Bilancio Attuale</span>
                                        <h2 class="font-bold"><?= number_format($bilancio, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <input type="hidden" id="customer_id" value="<?= $customer['id']; ?>">
                        <h1><?= ($r ? 'Modifica' : 'Nuovo'); ?> Debito/Credito</h1>
                        <fieldset>

                            <h2 class="title-divider">Info Richiesta</h2>

                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Ammontare della richiesta</label>
                                    <div class="p-xs border-top-bottom border-left-right border-size-lg text-center" style="margin-bottom: 15px;">
                                        <h3 class="font-bold" style="font-size: 30px;"><?= number_format($r['valore'], 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h3>
                                    </div>
                                </div>

                                <?php if(!empty($r['note'])) { ?>
                                    <div class="col-lg-12">
                                        <label>Note</label>
                                        <p class="form-control"><?= htmlentities($r['note']); ?></p>
                                    </div>
                                <?php } ?>

                                <div class="col-lg-12">
                                    <label>Data</label>
                                    <p class="form-control"><?= date('d/m/Y H:i:s', strtotime($r['data'])); ?></p>
                                </div>

                                <div class="col-lg-12">
                                    <label>Metodo Pagamento</label>
                                    <p class="form-control"><?= ucfirst($r['metodo_pagamento']); ?></p>
                                </div>
                                <?php if(is_array($r['referenza_pagamento'])) { ?>
                                    <?php foreach($r['referenza_pagamento'] as $ref_title => $ref_value) { ?>
                                        <div class="col-lg-12">
                                            <label><?= ucfirst($ref_title); ?></label>
                                            <p class="form-control"><?= $ref_value; ?></p>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>

                            <?php if($r['status'] == 0) { ?>
                                <h2 class="title-divider">Azioni</h2>
                                <div class="alert alert-info">
                                    Il pagamento <b>non</b> è automatico. Una volta cliccato il pulsante 'Accetta' bisogna assicurarsi di effettuarlo manualmente.
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-block btn-primary btn-lg updateOrderStatus" data-status="1">Accetta</a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#" class="btn btn-block btn-danger btn-lg updateOrderStatus" data-status="2">Rifiuta</a>
                                    </div>
                            </div>
                            <?php } else if($r['status'] == 1) { ?>
                                <div class="alert alert-success">
                                    Il prelievo è stato accettato.
                                </div>
                            <?php } else if($r['status'] == 2) { ?>
                                <div class="alert alert-danger">
                                    Il prelievo è stato rifiutato.
                                </div>
                            <?php }?>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>

    globalInitForm();
</script>
</body>
</html>
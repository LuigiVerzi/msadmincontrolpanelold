<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$id = $_POST['id'];
$status = $_POST['status'];

$balance = (new \MSFramework\Affiliations\commissions())->updateWithdrawalStatus($id, $status);

die((float)$balance);

<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();
$MSFrameworkAffiliationCommission = new \MSFramework\Affiliations\commissions();

$status_color = array(
    '0' => 'warning',
    '1' => 'primary',
    '2' => 'danger'
);

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function ($d, $row) {
            return $d;
        }
    ),
    array(
        'db' => 'cliente',
        'customOrder' => '(SELECT CONCAT(customers.nome,customers.cognome,customers.email) FROM customers WHERE customers.id = affiliations__withdrawals.cliente)',
        'dt' => 0,
        'formatter' => function ($d, $row) {
            Global $MSFrameworkCustomers;
            $r = $MSFrameworkCustomers->getCustomerDataFromDB($d);

            $info = array();
            if (!empty($r['email'])) {
                $info[] = $r['email'];
            }
            if (!empty($r['telefono_casa'])) {
                $info[] = $r['telefono_casa'];
            }
            if (!empty($r['telefono_cellulare'])) {
                $info[] = $r['telefono_cellulare'];
            }

            return '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
        }
    ),
    array(
        'db' => 'cliente',
        'customOrder' => '(SELECT (SELECT SUM(valore) FROM customers_balance WHERE customers_balance.tipo = \'credito\' AND customers_balance.cliente = affiliations__withdrawals.cliente)-(SELECT SUM(valore) FROM customers_balance WHERE customers_balance.tipo = \'debito\' AND customers_balance.cliente = affiliations__withdrawals.cliente))',
        'dt' => 1,
        'formatter' => function ($d, $row) {
            Global $MSFrameworkCustomers;
            $balance = $MSFrameworkCustomers->getCustomerBalance($d);
            return '<span class="label label-inverse">' . number_format($balance, 2, ',', '.') . ' <?= CURRENCY_SYMBOL; ?></span>';
        }
    ),
    array(
        'db' => 'valore',
        'dt' => 2,
        'formatter' => function ($d, $row) {
            return '<span class="label label-primary">' . number_format($d, 2, ',', '.') . ' <?= CURRENCY_SYMBOL; ?></span>';
        }
    ),
    array(
        'db' => 'status',
        'dt' => 3,
        'formatter' => function ($d, $row) {
            Global $MSFrameworkAffiliationCommission, $status_color;
            $status = $MSFrameworkAffiliationCommission->getWithdrawalStatus((string)$d);
            return '<span class="label label-' . $status_color[$d] . '">' . $status .'</span>';
        }
    ),
    array(
        'db' => 'data',
        'dt' => 4,
        'formatter' => function ($d, $row) {
            if ($d) {
                return array("display" =>  date('d/m/Y H:i', strtotime($d)), 'sort' => strtotime($d));
            } else {
                return array("display" => '<small class="text-muted">Mai</small>', 'sort' => strtotime($d));
            }
        }
    )
);

if($_GET['source'] == 'attesa') {
    echo json_encode(
        $datatableHelper::complex($_GET, $MSFrameworkDatabase, 'affiliations__withdrawals', 'id', $columns, null, 'status = 0')
    );
} else {
    echo json_encode(
        $datatableHelper::complex($_GET, $MSFrameworkDatabase, 'affiliations__withdrawals', 'id', $columns, null, 'status != 0')
    );
}
?>

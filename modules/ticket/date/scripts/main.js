$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('.bigliettiDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi Biglietto",
        "deleteButtonText": "Elimina Biglietto",
        "afterAddCallback": function (origin) {
            $('.bigliettiDuplication.rowDuplication:last').prepend('<div class="hr-line-dashed"></div>').find('input[type="text"], select').val('');
            $('.bigliettiDuplication.rowDuplication:last').find('[name]').each(function () {
                $(this).attr('name', $(this).attr('name').replace(/[0-9]/g, $('.bigliettiDuplication.rowDuplication:last').index()+1));
            });
        }
    });
    
    initTinyMCE(
        '#descrizione',
        {page: false}
    );

    $('.input-group.date').datetimepicker({
        keyboardNavigation: false,
        autoclose: true,
        startDate: new Date(),
        format: 'dd/mm/yyyy hh:ii'
    });
}


function moduleSaveFunction() {

    var biglietti = new Object();
    $('.stepsDuplication.bigliettiDuplication').each(function(row_index) {

        var biglietti_ary_tmp = {
            nome: $(this).find('.biglietto_nome').val(),
            prezzo: $(this).find('.biglietto_prezzo').val(),
            note_aggiuntive: $(this).find('.note_aggiuntive').val()
        };

        if(biglietti_ary_tmp.id == "") {
            return true;
        }

        biglietti[row_index] = biglietti_ary_tmp;
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pTitolo": $('#titolo').val(),
            "pDescrizione": tinymce.get('descrizione').getContent(),
            "pLuogo": $('#luogo').val(),
            "pData": $('#data').val(),
            "pDataFine": $('#data_fine').val(),
            "pBiglietti": biglietti,
            "pStato": $('#stato').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "date_format") {
                bootbox.error("La data inserita non è valida! Inserisci una data valida.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}
<?php
/**
 * MSAdminControlPanel
 * Date: 21/08/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pLuogo'] == "" || $_POST['pTitolo'] == "" || $_POST['pData'] == "" || !count($_POST['pBiglietti']) || $_POST['pStato'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

foreach($_POST['pBiglietti'] as $k => $ticket) {

    $_POST['pBiglietti'][$k]['prezzo'] = str_replace(",", ".", $_POST['pBiglietti'][$k]['prezzo']);

    if(!strstr($_POST['pBiglietti'][$k]['prezzo'], ".")) {
        $_POST['pBiglietti'][$k]['prezzo'] .= ".00";
    }

    if(!is_numeric($_POST['pBiglietti'][$k]['prezzo'])) {
        echo json_encode(array("status" => "price_numeric"));
        die();
    }

}

$date = DateTime::createFromFormat('d/m/Y H:i', $_POST['pData']);
$data_fine = DateTime::createFromFormat('d/m/Y H:i', $_POST['pDataFine']);

if(!$date || !$data_fine) {
    echo json_encode(array("status" => "date_format"));
    die();
}

$array_to_save = array(
    "titolo" => $_POST['pTitolo'],
    "descrizione" => $_POST['pDescrizione'],
    "luogo" => $_POST['pLuogo'],
    "data" => $date->getTimestamp(),
    "data_fine" => $data_fine->getTimestamp(),
    "biglietti" => json_encode($_POST['pBiglietti']),
    "attivo" => $_POST['pStato']
);


$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO ticket_date ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE ticket_date SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
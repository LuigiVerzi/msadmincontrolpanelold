<?php
/**
 * MSAdminControlPanel
 * Date: 21/08/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM ticket_date WHERE id = :id", array($_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
               <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Informazioni Evento</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Titolo</label>
                                    <input id="titolo" name="titolo" type="text" class="form-control required" value="<?= htmlentities($r['titolo']); ?>">
                                </div>
                                <div class="col-sm-3">
                                    <label>Luogo</label>
                                    <input id="luogo" name="luogo" type="text" class="form-control required" value="<?= htmlentities($r['luogo']); ?>">
                                </div>
                                <div class="col-sm-2">
                                    <label>Data</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control required" id="data" value="<?php echo ($r['data'] != '') ? date("d/m/Y H:i", $r['data']) : '' ?>">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label>Data Fine</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control required" id="data_fine" value="<?php echo ($r['data_fine'] != '') ? date("d/m/Y H:i", $r['data_fine']) : '' ?>">
                                    </div>
                                </div>
                                
                                <div class="col-sm-2">
                                    <label>Stato</label>
                                    <select id="stato" class="form-control">
                                        <option value="1" <?= ($r['attivo'] == 1 || !isset($r['attivo']) ? 'selected' : '') ;?>>Attivo</option>
                                        <option value="0" <?= ($r['attivo'] == 0 ? 'selected' : '') ;?>>Non Attivo</option>
                                    </select>
                                </div>
                            </div>

                            <h2 class="title-divider">Biglietti</h2>
                            
                            <div class="easyRowDuplicationContainer">
                                <?php
                                $lista_biglietti = json_decode($r['biglietti'], true);
                                if(count($lista_biglietti) == 0) {
                                    $lista_biglietti[] = array();
                                }
                                ?>

                                <?php foreach($lista_biglietti as $k => $pBiglietto) { ?>
                                    <div class="stepsDuplication rowDuplication bigliettiDuplication">

                                        <?php if($k > 0) { ?>
                                        <div class="hr-line-dashed"></div>
                                        <?php } ?>

                                        <div class="row">
                                            <div class="biglietto_container col-sm-3">
                                                <label>Nome Biglietto</label>
                                                <input type="text" class="form-control biglietto_nome required" name="biglietto_nome<?= $k; ?>" value="<?php echo htmlentities($pBiglietto['nome']); ?>" placeholder="">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Prezzo <?= CURRENCY_SYMBOL; ?></label>
                                                <input type="text" class="form-control biglietto_prezzo required" name="biglietto_prezzo<?= $k; ?>" value="<?php echo htmlentities($pBiglietto['prezzo']); ?>" placeholder="">
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Note Aggiuntive</label>
                                                <input type="text" class="form-control note_aggiuntive" name="note_aggiuntive<?= $k; ?>" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($pBiglietto['note_aggiuntive'])); ?>" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <h2 class="title-divider">Descrizione</h2>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Descrizione</label>
                                    <textarea id="descrizione" name="descrizione"><?php echo $r['descrizione'] ?></textarea>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
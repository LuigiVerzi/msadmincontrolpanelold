<?php
/**
 * MSAdminControlPanel
 * Date: 21/08/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM ticket_date") as $r) {

    $prezzi = array('min' => 0, 'max' => 0);
    foreach(json_decode($r['biglietti'], true) as $ticket) {
        if($ticket['prezzo'] < $prezzi['min'] || $prezzi['min'] == 0) {
            $prezzi['min'] = $ticket['prezzo'];
        }

        if($ticket['prezzo'] > $prezzi['max']) {
            $prezzi['max'] = $ticket['prezzo'];
        }
    }

    $array['data'][] = array(
        $r['titolo'],
        $r['luogo'],
        ($prezzi['min'] == $prezzi['max'] ? $prezzi['min'] : $prezzi['min'] . ' - ' . $prezzi['max']) . ' ' . CURRENCY_SYMBOL,
        array("display" => date("d/m/Y H:i", $r['data']), "sort" => $r['data']),
        '<span style="background: ' . ($r['attivo'] == "1" ? "#8BC34A" : "red") . '; padding: 3px 10px; border-radius: 5px; color: white;">' . ($r['attivo'] == "1" ? "Attivo" : "Non Attivo") . '</span>',
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

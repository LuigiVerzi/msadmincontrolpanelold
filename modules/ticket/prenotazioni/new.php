<?php
/**
 * MSAdminControlPanel
 * Date: 21/08/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$eventsList = (new \MSFramework\Ticket\events())->getEventDetails();

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Aggiungi Nuovo Ticket</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Contatto</label>
                                    <input id="ticket_contatto" name="ticket_contatto" type="email" class="form-control required" value="">
                                </div>
                                <div class="col-sm-3">
                                    <label>Nome</label>
                                    <input id="ticket_nome" name="ticket_nome" type="text" class="form-control required" value="">
                                </div>
                                <div class="col-sm-3">
                                    <label>Stato</label>
                                    <select id="ticket_status" class="form-control">
                                        <option value="1">Attivo</option>
                                        <option value="0">Non Attivo</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label>Quantità</label>
                                    <select id="ticket_quantity" class="form-control">
                                        <?php for ($i = 1; $i <= 10; $i++) { ?>
                                            <option value="<?= $i; ?>"><?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Evento</label>
                                    <select name="evento" id="ticket_evento" class="form-control" required="required">
                                        <?php foreach($eventsList as $event) { ?>
                                            <option data-ticket="<?= htmlentities(json_encode($event['biglietti'])); ?>" value="<?= $event['id']; ?>">EVENTO: <?= $event['titolo']; ?> - <?= date("d/m/Y H:i", $event['data']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Biglietto</label>
                                    <select name="evento" id="ticket_biglietto" class="form-control" required="required">

                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Messaggio del Cliente</label>
                                    <textarea id="ticket_messaggio" name="ticket_messaggio" class="form-control required"></textarea>
                                </div>
                            </div>


                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Allega messaggio al Ticket</label>
                                    <textarea id="ticket_allegato" name="ticket_allegato" class="form-control required"></textarea>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 21/08/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM richieste WHERE id = :id and type = 'ticket'", array($_GET['id']), true);
}

if(!$r) header('Location: new.php');

$eventsList = (new \MSFramework\Ticket\events())->getEventDetails();
$currentEvent = (in_array($form_data['Evento'], array_keys($eventsList)) ? $eventsList[$form_data['Evento']] : array());

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
               <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
                <div class="title-action">
                    <a class="btn btn-info pull-right" id="sendTicket" saving_txt="Invio in corso...">Invia Biglietto</a>
                </div>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <?php
                        $form_data = json_decode($r['form_data'], true);
                        ?>
                        <h1>Generali</h1>
                        <fieldset>
                            <h2 class="title-divider">Riepilogo Richiesta</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nome</label>
                                    <input id="ticket_nome" name="ticket_nome" type="text" class="form-control required" value="<?= htmlentities($r['nome']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Contatto</label>
                                    <input id="ticket_contatto" name="ticket_contatto" type="email" class="form-control required" value="<?= htmlentities($r['contatto']); ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Data Creazione</label>
                                    <?php echo date("d/m/Y", $r['creation_time']) ?>
                                </div>

                                <div class="col-sm-3">
                                    <label>Lingua</label>
                                    <?php echo $MSFrameworki18n->getLanguagesDetails($r['lang'], "italian_name")[$r['lang']]['italian_name'] ?>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Messaggio allegato dal Cliente</label>
                                    <textarea id="ticket_messaggio" name="ticket_messaggio" class="form-control required"><?= htmlentities($r['messaggio']); ?></textarea>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Stato</label>
                                    <select id="ticket_status" class="form-control">
                                        <option value="1" <?php if($form_data['Stato'] == "1" || !isset($form_data['Stato'])) { echo "selected"; } ?>>Attivo</option>
                                        <option value="0" <?php if($form_data['Stato'] == "0") { echo "selected"; } ?>>Non Attivo</option>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Evento</label>
                                    <select name="evento" id="ticket_evento" class="form-control" required="required">
                                        <?php if(!$currentEvent) { ?>
                                            <option value="<?= $form_data['Evento']; ?>" selected>Evento non più disponibile</option>
                                        <?php } ?>
                                        <?php foreach($eventsList as $event) { ?>
                                            <option value="<?= $event['id']; ?>" <?= ($form_data['Evento'] == $event['id'] ? 'selected' : ''); ?>>EVENTO: <?= $event['titolo']; ?> - <?= date("d/m/Y H:i", $event['data']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Quantità</label>
                                    <select id="ticket_quantity" class="form-control">
                                        <?php for ($i = 1; $i <= 10; $i++) { ?>
                                            <option value="<?= $i; ?>" <?= ($form_data['Quantità'] == $i ? 'selected' : ''); ?>><?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Ticket ID</label>
                                    <p id="ticket_id"><?= $form_data['Ticket ID']; ?></p>
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="biglietto_container col-sm-3">
                                    <label>Nome Biglietto</label>
                                    <input type="text" class="form-control biglietto_nome required" id="nome_biglietto" value="<?php echo htmlentities($form_data['Biglietto']['nome']); ?>" placeholder="">
                                </div>
                                <div class="col-sm-3">
                                    <label>Prezzo <?= CURRENCY_SYMBOL; ?></label>
                                    <input type="text" class="form-control biglietto_prezzo required" id="prezzo_biglietto" value="<?php echo htmlentities($form_data['Biglietto']['prezzo']); ?>" placeholder="">
                                </div>
                                <div class="col-sm-6">
                                    <label>Note Aggiuntive</label>
                                    <input type="text" class="form-control note_aggiuntive" id="note_biglietto" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($form_data['Biglietto']['note_aggiuntive'])); ?>" placeholder="">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Allega messaggio al Ticket</label>
                                    <textarea id="ticket_allegato" name="ticket_allegato" class="form-control"><?= (isset($form_data['Allegato']) ? htmlentities($form_data['Allegato']) : ''); ?></textarea>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
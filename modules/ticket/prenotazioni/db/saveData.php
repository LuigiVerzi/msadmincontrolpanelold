<?php
/**
 * MSAdminControlPanel
 * Date: 21/08/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pContatto'] == "" || $_POST['pNome'] == "" || $_POST['pStato'] == "" || $_POST['pEvento'] == "" || $_POST['pQuantity'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$_POST['pBiglietto']['prezzo'] = str_replace(",", ".", $_POST['pBiglietto']['prezzo']);

if(!strstr($_POST['pBiglietto']['prezzo'], ".")) {
    $_POST['pBiglietto']['prezzo'] .= ".00";
}

if(!is_numeric($_POST['pBiglietto']['prezzo'])) {
    echo json_encode(array("status" => "price_numeric"));
    die();
}

if($db_action == "update") {
    $r = $MSFrameworkDatabase->getAssoc("SELECT form_data FROM richieste WHERE id = :id", array(":id" => $_POST['pID']), true);
    $form_data = json_decode($r['form_data'], true);

    if ($form_data['Stato'] == 0 && $_POST['pStato'] == 1 && $form_data['Ticket ID'] == 'N/D') {
        $form_data['Ticket ID'] = 'TM' . time();
    }

    $form_data['Stato'] = $_POST['pStato'];
    $form_data['Evento'] = $_POST['pEvento'];
    $form_data['Quantità'] = $_POST['pQuantity'];
    $form_data['Allegato'] = $_POST['pAllegato'];
    $form_data['Biglietto'] = $_POST['pBiglietto'];

    $array_to_save = array(
        "type" => 'ticket',
        "nome" => $_POST['pNome'],
        "contatto" => $_POST['pContatto'],
        "messaggio" => $_POST['pMessaggio'],
        "creation_time" => time(),
        "lang" => USING_LANGUAGE_CODE,
        "form_data" => json_encode($form_data)
    );
}
else
{
    $array_to_save = array(
        "type" => 'ticket',
        "nome" => $_POST['pNome'],
        "contatto" => $_POST['pContatto'],
        "messaggio" => $_POST['pMessaggio'],
        "creation_time" => time(),
        "lang" => USING_LANGUAGE_CODE,
        "form_data" => json_encode(array(
            'Stato' => $_POST['pStato'],
            'Evento' => $_POST['pEvento'],
            'Ticket ID' => 'TM' . time(),
            'Quantità' => $_POST['pQuantity'],
            'Allegato' => $_POST['pAllegato'],
            'Biglietto' => $_POST['pBiglietto'],
        )),
    );
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO richieste ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE richieste SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 21/08/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM richieste WHERE type = 'ticket' ORDER BY creation_time desc") as $r) {
    $form_data = json_decode($r['form_data'], true);

    $eventInfo = (new \MSFramework\Ticket\events())->getEventDetails($form_data['Evento'])[$form_data['Evento']];

    $array['data'][] = array(
        $r['nome'],
        $r['contatto'],
        ($eventInfo ? $eventInfo['titolo'] : 'N/A'),
        (strlen($r['messaggio']) > 120) ? substr($r['messaggio'], 0, 117) . '...' : $r['messaggio'],
        array("display" => date("d/m/Y H:i", $r['creation_time']), "sort" => $r['creation_time']),
        '<span style="background: ' . ($form_data['Stato'] == "1" ? "#8BC34A" : "red") . '; padding: 3px 10px; border-radius: 5px; color: white;">' . ($form_data['Stato'] == "1" ? "Attivo" : "Non Attivo") . '</span>',
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

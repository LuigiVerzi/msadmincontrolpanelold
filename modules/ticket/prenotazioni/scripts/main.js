$(document).ready(function() {
    window.invia_dopo_salvataggio = false;
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    if($('#ticket_biglietto').length) {

        $('#ticket_evento').on('change', function () {
            var tickets = $(this).find('option:selected').data('ticket');

            var ticket_options = '';
            tickets.forEach(function (ticket, k) {
                ticket_options += '<option data-nome="' + ticket.nome + '" data-prezzo="' + ticket.prezzo + '" data-note_aggiuntive="' + ticket.note_aggiuntive + '" value="' + k + '">' + ticket.nome + ' - ' + ticket.prezzo + '€ ' + ticket.note_aggiuntive + '</option>';
            });

            $('#ticket_biglietto').html(ticket_options);

        }).change();

    }

    $('#sendTicket').on('click', function (e) {
       e.preventDefault();
        window.invia_dopo_salvataggio = true;
        moduleSaveFunction();
    });

}

function moduleSaveFunction() {
    var biglietti_ary = {};

    if($('#ticket_biglietto').length) {
        biglietti_ary = $('#ticket_biglietto').find('option:selected').data();
    } else {
        biglietti_ary = {
            nome: $('.biglietto_nome').val(),
            prezzo: $('.biglietto_prezzo').val(),
            note_aggiuntive: $('.note_aggiuntive').val()
        };
    }

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pContatto": $('#ticket_contatto').val(),
            "pNome": $('#ticket_nome').val(),
            "pStato": $('#ticket_status').val(),
            "pQuantity": $('#ticket_quantity').val(),
            "pMessaggio": $('#ticket_messaggio').val(),
            "pAllegato": $('#ticket_allegato').val(),
            "pEvento": $('#ticket_evento').val(),
            "pBiglietto": biglietti_ary
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "price_numeric") {
                bootbox.error("Il campo 'prezzo' deve contenere un valore numerico");
            } else if(data.status == "ok") {
                if(window.invia_dopo_salvataggio) {
                    sendTicketToCustomer();
                } else {
                    succesModuleSaveFunctionCallback(data);
                    if(data.id.length && $('#ticket_biglietto').length) {
                        location.href = 'edit.php?id=' + data.id;
                    }
                }
            }
            window.invia_dopo_salvataggio = false;
        }
    });
}

function sendTicketToCustomer() {
    $.ajax({
        url: $('#baseElementPathSW').val() + "m/ticket",
        type: "POST",
        async: false,
        data: {
            action: "sendTicketViaMail",
            pID: $('#record_id').val(),
            ticketID: $('#ticket_id').html()
        },
        dataType: 'json',
        success: function(data) {
            if(data.status == "query_error") {
                bootbox.error("Si è verificato un errore durante l'invio del biglietto tramite email.");
            } else if(data.status == "ok") {
                bootbox.alert("Il biglietto è stato inviato correttamente al seguente indirizzo: " + data.email);
            } else if(data.status == "not_active") {
                bootbox.error("Per poter inviare un biglietto via email devi prima attivarlo e salvare le modifiche.");
            }
        }
    });
}
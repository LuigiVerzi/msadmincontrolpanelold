<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkCustomers = new \MSFramework\customers();

if(isset($_GET['seduta'])) {

    $columns = array(
        array(
            'db' => 'id',
            'dt' => 'DT_RowId',
            'formatter' => function( $d, $row ) {
                return $d;
            }
        ),
        array(
            'db' => 'valore',
            'dt' => 0,
            'formatter' => function($d, $row) {
                return  '<span class="label label-' . ($d < 0 ? 'danger' : ($d == 0 ? 'warning' : 'primary')) . '">' . abs($d) . ' ' .   ($d < 0 ? 'Rimosse' : 'Aggiunte') . '</span>';

            }
        ),
        array(
            'db' => 'note',
            'dt' => 1,
            'formatter' => function($d, $row) {
                return (new \MSFramework\customers())->parseBalanceNotes($d);
            }
        ),
        array(
            'db' => 'data',
            'dt' => 2,
            'formatter' => function($d, $row) {
                return array("display" => date("d/m/Y H:i", strtotime($d)), "sort" => strtotime($d));
            }
        ),
        array(
            'db' => 'id',
            'dt' => 3,
            'formatter' => function($d, $row) {
                return '<div class="text-right"><a href="javascript:;" class="btn btn-danger btn-outline btn-sm" onclick="deleteRecord(' . $d . ');"><i class="fa fa-trash-o"></i></a></div>';
            }
        )
    );

    echo json_encode(
        $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'beautycenter_sedute', 'cliente', $columns, null, 'cliente = ' . (int)$_GET['seduta'])
    );

}
else
{

    $columns = array(
        array(
            'db' => 'id',
            'dt' => 'DT_RowId',
            'formatter' => function( $d, $row ) {
                return $d;
            }
        ),
        array(
            'db' => 'CONCAT(nome,cognome)',
            'dt' => 0,
            'formatter' => function($d, $row) {

                Global $MSFrameworkCustomers;

                $r = $MSFrameworkCustomers->getCustomerDataFromDB($row[0]);

                $info = array();
                if (!empty($r['email'])) {
                    $info[] = $r['email'];
                }
                if (!empty($r['telefono_casa'])) {
                    $info[] = $r['telefono_casa'];
                }
                if (!empty($r['telefono_cellulare'])) {
                    $info[] = $r['telefono_cellulare'];
                }

                $return_html = '';

                $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
                if(json_decode($r['avatar'])) {
                    $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($r['avatar'], true)[0];
                }

                $return_html .= '<div style="position: relative; padding-left: 65px;">';
                $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
                $return_html .= '<h2 class="no-margins">' . htmlentities($r['nome'] . ' ' . $r['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
                $return_html .= '</div>';

                return $return_html;
            }
        ),
        array(
            'db' => 'id',
            'dt' => 1,
            'customOrder' => '(SELECT SUM(beautycenter_sedute.valore) FROM beautycenter_sedute WHERE beautycenter_sedute.cliente = customers.id)',
            'formatter' => function($d, $row) {

                $bilancio = (new \MSFramework\BeautyCenter\sedute())->getCustomerSessionsBalance($d);
                $array_servizi = array();
                if($bilancio) {
                    foreach($bilancio as $b) {
                        if($b['sedute'] != 0) {
                            $info_servizio = json_decode($b['ref'], true);
                            $array_servizi[] =  '<span class="label label-' . ($b['sedute'] < 0 ? 'danger' : ($b['sedute'] == 0 ? 'warning' : 'primary')) . '"><b>' . $b['sedute'] . '</b> ' . (new \MSFramework\i18n())->getFieldValue($info_servizio['nome']) . '</span>';
                        }
                    }
                    $bilancio_string = implode(' ', $array_servizi);
                }
                else {
                    $bilancio_string = '<span class="label label-danger">Nessuno</span>';
                }
                return $bilancio_string;
            }
        ),
        array(
            'db' => 'id',
            'dt' => 2,
            'customOrder' => '(SELECT MAX(data) FROM beautycenter_sedute WHERE beautycenter_sedute.cliente = customers.id)',
            'formatter' => function($d, $row) {
                $r = (new \MSFramework\database())->getAssoc("SELECT MAX(data) as ultima_operazione FROM beautycenter_sedute WHERE beautycenter_sedute.cliente = :id ORDER BY data DESC", array(':id' => $d), true);
                return ($r['ultima_operazione'] ? $r['ultima_operazione'] : 'Mai');
            }
        )
    );

    echo json_encode(
        $datatableHelper::simple( $_GET, $MSFrameworkDatabase, 'customers', 'id', $columns )
    );
}
?>

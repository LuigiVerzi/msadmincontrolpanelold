<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$sedute_rimanenti = 0;
$sedute_fatte = 0;

$rimanenti_sql = $MSFrameworkDatabase->getAssoc("SELECT SUM(valore) as sedute_rimanenti FROM beautycenter_sedute", array(), true);
if($rimanenti_sql) $sedute_rimanenti = (int)$rimanenti_sql['sedute_rimanenti'];

$fatte_sql = $MSFrameworkDatabase->getAssoc("SELECT SUM(valore) as sedute_fatte FROM beautycenter_sedute WHERE valore < 0", array(), true);
if($fatte_sql) $sedute_fatte = (int)$fatte_sql['sedute_fatte'];
?>

<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>
            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">

                    <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Cliente</th>
                            <th>Sedute Rimanenti  <span class="label label-success pull-right"><?= $sedute_rimanenti; ?> <small>totali</small></span></th>
                            <th class="default-sort" data-sort="DESC">Ultimo Aggiornamento</th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
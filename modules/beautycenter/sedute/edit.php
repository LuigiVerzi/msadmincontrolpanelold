<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = (New \MSFramework\customers())->getCustomerDataFromDB($_GET['id']);
    $bilancio = (new \MSFramework\BeautyCenter\sedute())->getCustomerSessionsBalance($r['id']);

    $info_localita = array();

    if(!empty($r['indirizzo'])) {
        $info_localita[] = $r['indirizzo'];
    }
    if(!empty($r['citta'])) {
        $info_localita[] = $r['citta'];
    }
    if(!empty($r['provincia'])) {
        $info_localita[] = $r['cap'];
    }

    $info_principali = array();
    if(!empty($r['email'])) {
        $info_principali[] = $r['email'];
    }
    if(!empty($r['telefono_casa'])) {
        $info_principali[] = $r['telefono_casa'];
    }
    if(!empty($r['telefono_cellulare'])) {
        $info_principali[] = $r['telefono_cellulare'];
    }

    $array_servizi = array();
    $bilancio_string = '';
    if($bilancio) {

        foreach($bilancio as $b) {
            if($b['sedute'] != 0) {
                $info_servizio = json_decode($b['ref'], true);
                $array_servizi[] =  '<span class="label label-' . ($b['sedute'] < 0 ? 'danger' : ($b['sedute'] == 0 ? 'warning' : 'primary')) . '"><b>' . $b['sedute'] . '</b> ' . $MSFrameworki18n->getFieldValue($info_servizio['nome']) . '</span>';
            }
        }

        $bilancio_string = implode(' ', $array_servizi);

    }
    else {
        $bilancio_string = '<span class="label label-danger">Nessuno</span>';
    }

}

if(!isset($r) || !$r) {
    header('Location: index.php');
}

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">

                <div class="col-md-3">

                    <div class="contact-box">

                        <h3 class="m-b-xs"><strong><?= $r['nome']; ?> <?= $r['cognome']; ?></strong></h3>

                        <address class="m-t-md">
                            <?= implode('<br>', $info_principali); ?>
                        </address>

                        <address class="m-t-md">
                            <?= implode('<br>', $info_localita); ?>
                        </address>

                        <div class="contact-box-footer" style="padding: 10px 0 0;">
                            <?= $bilancio_string; ?>
                        </div>

                    </div>

                </div>

                <div class="col-lg-9">

                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5 style="line-height: 33px; font-size: 14px;">Lista Movimenti</h5>
                            <div class="ibox-tools" style="margin-bottom: 6px;">
                                <a class="btn btn-primary" id="dataTableNew" href="seduta.php?customer=<?= $_GET['id']; ?>">Aggiungi</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <input type="hidden" id="customer_id" value="<?= $_GET['id']; ?>">
                            <table id="main_table_list" class="balance_table display table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="margin-bottom: -30px;">
                                <thead>
                                <tr>
                                    <th>Valore</th>
                                    <th>Note</th>
                                    <th class="default-sort is_data" data-sort="DESC">Data</th>
                                    <th class="no-sort" style="width: 35px;" width="35px"></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
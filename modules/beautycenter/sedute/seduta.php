<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    header('Location: index.php');
    die();
}

$r = array();
$customer = (New \MSFramework\customers())->getCustomerDataFromDB($_GET['customer']);

if(!$customer) {
    header('Location: index.php');
}

$info_localita = array();

if(!empty($customer['indirizzo'])) {
    $info_localita[] = $customer['indirizzo'];
}
if(!empty($customer['citta'])) {
    $info_localita[] = $customer['citta'];
}
if(!empty($customer['provincia'])) {
    $info_localita[] = $customer['cap'];
}

$info_principali = array();
if(!empty($customer['email'])) {
    $info_principali[] = $customer['email'];
}
if(!empty($customer['telefono_casa'])) {
    $info_principali[] = $customer['telefono_casa'];
}
if(!empty($customer['telefono_cellulare'])) {
    $info_principali[] = $customer['telefono_cellulare'];
}

$services_json = array();

$lista_servizi = (new \MSFramework\services())->getServiceDetails();
foreach($lista_servizi as $s) {
    $services_json['service_' . $s['id']] = $s;
}
$lista_pacchetti = array();
$lista_pacchetti = (new \MSFramework\BeautyCenter\offers())->getOfferDetails();
foreach ($lista_pacchetti as $k => $l) {
    if(!isset($l['extra_fields']['sedute']) || $l['extra_fields']['sedute'] < 1) {
        unset($lista_pacchetti[$k]);
    }
    $services_json['offer_' . $l['id']] = $l;
}

$vecchi_servizi = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM beautycenter_sedute WHERE cliente = :id", array(":id" => $customer['id'])) as $o_s) {
    $ref = json_decode($o_s['ref'], true);
    if($ref) {
        $vecchi_servizi[$o_s['tipo']] = array(
            'id' => $ref['id'],
            'nome' => $ref['nome'],
            'type' => explode($o_s['tipo'], '_')[0]
        );
        $services_json[$o_s['tipo']] = $ref;
    }
}

?>
<body class="skin-1 fixed-sidebar">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-md-3">

                    <div class="contact-box">

                        <h3 class="m-b-xs"><strong><?= $customer['nome']; ?> <?= $customer['cognome']; ?></strong></h3>

                        <address class="m-t-md">
                            <?= implode('<br>', $info_principali); ?>
                        </address>

                        <address class="m-t-md">
                            <?= implode('<br>', $info_localita); ?>
                        </address>
                    </div>
                </div>
                <div class="col-lg-9">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <input type="hidden" id="customer_id" value="<?= $customer['id']; ?>">
                        <h1>Aggiungi/Rimuovi Sedute</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Tipologia</label>
                                    <select id="tipologia" name="tipologia" class="form-control">
                                        <option value="+">Aggiungi Sedute</option>
                                        <?php if($vecchi_servizi) { ?>
                                        <option value="-">Rimuovi Sedute</option>
                                        <?php } ?>
                                    </select>
                                    <div class="hr-line-dashed"></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Servizio</label>
                                            <select id="servizio" name="servizio" class="form-control">
                                                <optgroup label="Usati" id="recenti">
                                                <?php foreach($vecchi_servizi as $k=>$s) { ?>
                                                    <option value="<?= $k ?>" data-type="<?= $s['type']; ?>" data-id="<?= $s['id']; ?>"><?= $MSFrameworki18n->getFieldValue($s['nome']); ?></option>
                                                <?php } ?>
                                                </optgroup>
                                                <optgroup label="Servizi">
                                                <?php foreach($lista_servizi as $s) { ?>
                                                    <option value="service_<?= $s['id']; ?>" data-type="service" data-id="<?= $s['id']; ?>"><?= $MSFrameworki18n->getFieldValue($s['nome']); ?></option>
                                                <?php } ?>
                                                </optgroup>
                                                <optgroup label="Pacchetti">
                                                <?php foreach($lista_pacchetti as $p) { ?>
                                                    <option value="offer_<?= $p['id']; ?>" data-type="service" data-id="<?= $p['id']; ?>"><?= $MSFrameworki18n->getFieldValue($p['nome']); ?></option>
                                                <?php } ?>
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>N. Sedute</label>
                                            <select id="valore" name="valore" class="form-control">
                                                <?php for($i = 1; $i <= 10; $i++) { ?>
                                                    <option value="<?= $i; ?>"><?= $i; ?> sedute</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <label>Note Interne</label>
                                    <textarea id="note" name="note" class="form-control">Inserito manualmente...</textarea>
                                </div>
                                <div class="col-sm-6">
                                    <label>Numero di sedute per questo servizio <small>(successivo al salvataggio)</small></label>
                                    <div class="p-xs border-top-bottom border-left-right border-size-lg text-center" id="newBalance" data-current="<?= (int)$r['valore']; ?>"><span class="text-info">...</span></div>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <textarea id="services_json" style="display: none;"><?= json_encode($services_json); ?></textarea>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
$(document).ready(function() {
    if(!$('#main_table_list').hasClass('balance_table')) {
        loadStandardDataTable('main_table_list', true);
        allowRowHighlights('main_table_list');
        manageGridButtons('main_table_list');
    }
});

function initForm() {
    initClassicTabsEditForm();

    if($('#record_id').length) {
        $('#valore, #tipologia, #servizio').on('change', function () {

            $.ajax({
                url: "ajax/getCustomerBalance.php",
                type: "POST",
                data: {
                    "id": $('#customer_id').val(),
                    "tipo": $('#servizio').val()
                },
                async: true,
                success: function (bilancio_attuale) {
                    bilancio_attuale = parseFloat(bilancio_attuale);
                    if ($('#tipologia').val() == '-') {
                        bilancio_attuale += parseFloat(-$('#valore').val());
                    }
                    else {
                        bilancio_attuale += parseFloat($('#valore').val());
                    }
                    $('#newBalance').html('<span class="text-' + (parseFloat(bilancio_attuale) < 0 ? 'danger' : 'info') + '">' + parseFloat(bilancio_attuale) + '</span>');
                }
            });
        });
        $('#valore').change();

        $('#tipologia').on('change', function () {
           if($(this).val() == '+') {
               $('#servizio optgroup').prop('disabled', false);
           }
           else {
               $('#servizio optgroup').not('#recenti').prop('disabled', true);
               if(!$('#servizio #recenti option:selected').length) $('#servizio').val($('#servizio #recenti option').first().val());
           }
        });

    }
    else {
        loadStandardDataTable('main_table_list', true, {
            "ajax": 'ajax/dataTable.php?seduta=' + $('#customer_id').val(),
            "searching": false,
            "lengthChange" : false
        }, false);
    }

}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pCliente": $('#customer_id').val(),
            "pServizio": $('#servizio').val(),
            "pRef": JSON.parse($('#services_json').val())[$("#servizio").val()],
            "pValore": $('#tipologia').val() + $('#valore').val(),
            "pNote": $('#note').val()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
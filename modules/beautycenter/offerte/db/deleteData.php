<?php
/**
 * MSAdminControlPanel
 * Date: 08/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\BeautyCenter\offers())->getOfferDetails($_POST['pID']) as $r) {
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['gallery'], true));
}

if($MSFrameworkDatabase->deleteRow("beautycenter_offerte", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('BEAUTYOFFERSGALLERY'))->unlink($images_to_delete);

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


<?php
/**
 * MSAdminControlPanel
 * Date: 08/03/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM beautycenter_offerte") as $r) {
    $array['data'][] = array(
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['nome'], true),
        $MSFrameworki18n->getFieldValue($r['slug'], true),
        (new \MSFramework\BeautyCenter\offers())->getTypes($r['type']),
        $r['prezzo'],
        ($r['scadenza'] != '') ? date("d/m/Y", $r['scadenza']) : 'Mai',
        ($r['active'] != '0') ? 'Si' : 'No',
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

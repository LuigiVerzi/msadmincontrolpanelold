<?php
/**
 * MSAdminControlPanel
 * Date: 12/12/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM beautycenter_cabine") as $r) {
    $array['data'][] = array(
        $MSFrameworki18n->getFieldValue($r['nome']),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

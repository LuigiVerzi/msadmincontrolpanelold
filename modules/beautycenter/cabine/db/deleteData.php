<?php
/**
 * MSAdminControlPanel
 * Date: 12/12/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($MSFrameworkDatabase->deleteRow("beautycenter_cabine", "id", $_POST['pID'])) {
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>
function initForm() {
    window.themeDetailsModal = false;

    $('#themes_list').on('click', '.theme-widget', function () {
        var id = $(this).data('id');

        if(window.themeDetailsModal) {
            window.themeDetailsModal.modal('hide');
        }

        $.ajax({
            url: "ajax/getThemePreview.php",
            type: "POST",
            data: {
                "id": id
            },
            async: true,
            dataType: "text",
            success: function (html) {

                window.themeDetailsModal = bootbox.dialog({
                    title: 'Dettagli tema',
                    size: 'large',
                    message: html
                });

                $('.theme-details-slider').slick({
                    dots: true
                });

                initIChecks();

            }
        });
    });

    $("body").on('ifToggled', '#theme_importDemoContent', function () {
        if($(this).is(':checked')) {
            $('#theme_demoAlert').show();
        } else {
            $('#theme_demoAlert').hide();
        }
    });
}

function deleteTemplate(theme_id) {
    bootbox.hideAll();

    bootbox.confirm("Proseguendo il tema verrà eliminato definitivamente, l'operazione non è reversibile.<br>Sei sicuro di voler eliminare il tema?", function (install) {
        if(install) {
            $.ajax({
                url: "db/themeDelete.php",
                type: "POST",
                data: {
                    "theme_id": theme_id,
                },
                async: true,
                dataType: "json",
                success: function (data) {
                    if (data.status == "ok") {
                        bootbox.alert("Il tema è stato eliminato dalla libreria.", function () {
                            location.reload();
                        });
                    } else {
                        bootbox.error("Si è verificato un errore durante l'eliminazione del tema.")
                    }
                }
            });
        }
    });
}

function themeInstall(theme_id, theme_name, import_demo) {
    bootbox.hideAll();

    bootbox.confirm("Proseguendo l'installazione sovrascriverà <b>tutti i file</b> del tuo sito web, " + (!import_demo ? "il database invece sarà mantenuto" : "<b>i contenuti demo sostituiranno tutti i contenuti attuali</b>") + ", l'operazione non è reversibile.<br>Sei sicuro di voler installare il tema <b>'" + theme_name + "'</b>?", function (install) {

        if(install) {

            $.ajax({
                url: "db/themeInstall.php",
                type: "POST",
                data: {
                    "theme_id": theme_id,
                    "install_content": import_demo,
                },
                preloader: 'Installazione del tema in corso',
                async: true,
                dataType: "json",
                success: function (data) {
                    if (data.status == "ok") {
                        bootbox.alert("Il tema è stato installato correttamente, stai per essere reindirizzato alle preferenze.", function () {
                            location.href = '../preferences';
                        });
                    } else {
                        bootbox.error("Si è verificato un errore durante l'installazione del tema.")
                    }
                }
            });

        }

    });
}
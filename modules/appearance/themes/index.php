<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$currentThemeID = $MSFrameworkCMS->getCMSData('theme_info')['id'];
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <div class="title-action">
                    <?php if($_SESSION['userData']['userlevel'] == 0) { ?>

                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-cloud-upload"></i> Esporta Tema</button>
                            <ul class="dropdown-menu" style="left: auto; right: 0;">
                                <li><a class="dropdown-item" href="export/?new"><i class="fa fa-plus"></i> Salva come nuovo</a></li>
                                <?php if($currentThemeID) { ?>
                                    <li class="dropdown-divider"></li>
                                    <li><a class="dropdown-item" href="export/?replace"><i class="fa fa-pencil"></i> Sostituisci attuale</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div id="themes_list">
                <div class="row">
                <?php foreach((new \MSFramework\Appearance\themes())->getThemesList() as $theme_id => $theme_info) { ?>
                    <div class="col-md-3">
                        <div class="ibox" style="padding: 0;">
                            <div class="ibox-content product-box" style="padding: 0;">
                                <a href="#" class="theme-widget" style="text-decoration: none; color: inherit;" data-id="<?= $theme_id; ?>">
                                    <img src="<?= $theme_info['preview']; ?>" class="img-responsive">
                                    <div class="product-desc">

                                        <?php if($currentThemeID == $theme_id) { ?>
                                            <span class="pull-right badge badge-success">Attivo</span>
                                        <?php } ?>

                                        <span class="product-name"><?= $theme_info['name']; ?></span>
                                        <div class="small m-t-xs">
                                            <?= $theme_info['description']; ?>
                                        </div>
                                    </div>
                                    <div class="bg-muted" style="padding: 0px 15px 15px 15px;">
                                        <div class="m-t pull-left">
                                            <?php foreach($theme_info['functionalities'] as $functionality) { ?>
                                                <small class="label label-inverse"><i class="fa fa-star"></i> <?= $functionality; ?></small>
                                            <?php } ?>
                                        </div>
                                        <div class="m-t pull-right">
                                            <span class="btn btn-xs btn-outline btn-success">Dettagli <i class="fa fa-long-arrow-right"></i></span>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
            </div>

        </div>
        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    </div>
</div>
<script>
    globalInitForm();
</script>
</body>
</html>
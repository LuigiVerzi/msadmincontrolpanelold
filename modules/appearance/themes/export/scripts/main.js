function initForm() {
    initClassicTabsEditForm();
    initTinyMCE($('#dettagli'));

    $('#theme_id').on('keyup change', function () {
       $(this).val($(this).val().replace(/([^A-Za-z0-9\-\_]+)/g, ''));
    });

    initOrak('themePreviews', 4, 0, 0, 1000, getOrakImagesToPreattach('themePreviews'), false, ['image/jpeg', 'image/png']);

    /* CREAZIONE ANTEPRIME */
    $('.previewLinks').chosen({search_contains: true});
    $('.generate_web_preview').show().on('click', function (e) {
        e.preventDefault();

        var searchLinks = [];
        $('.previewLinks').each(function () {
            if($(this).val()) searchLinks.push($(this).val());
        });

        if(!searchLinks.length) {
            toastr['error']("Seleziona almeno un link per generare l'anteprima.");
            return false;
        }

        var $button = $(this);
        var $orak = $('.orakUploaderContainer');

        $button.hide().after('<div id="previewProgress"><h5>Sto generando automaticamente le immagini. Potrebbe volerci qualche minuto...</h5><div class="progress"><div class="progress-bar progress-bar-striped progress-bar-success" style="width: 5%" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div></div></div>');
        $orak.css('opacity', 0.5).css('pointer-events', 'none');

        $('#previewProgress .progress-bar').css('width', '10%');
        var progressInterval = setInterval(function () {
            if($('#previewProgress .progress-bar').length) {

                var newProgress = parseInt($('#previewProgress .progress-bar').css('width'))+5;
                if(newProgress <= 95) {
                    $('#previewProgress .progress-bar').css('width', newProgress + '%');
                }
            }
        }, 1000);

        $.ajax({
            url: "ajax/generatePreview.php",
            type: "POST",
            data: {links: searchLinks},
            async: true,
            dataType: "json",
            success: function (data) {
                clearInterval(progressInterval);

                //modifico al volo il path per le anteprime utilizzato da orakuploader in quanto (solo in questo caso) le anteprime vanno caricate dalla TMP in fase di init
                import_saved_realpath = $('#themePreviews_realPath').val();
                $('#themePreviews_realPath').val($('#baseElementPathTmpFolder').val());
                $('#themePreviews').html('');

                initOrak('themePreviews', 10, 515, 515, 153, data, false, ['image/jpeg', 'image/png', 'image/svg+xml']);
                $('#themePreviews_realPath').val(import_saved_realpath);

                $button.show('loading').parent().find('#previewProgress').remove();
                $orak.css('opacity', 1).css('pointer-events', 'initial');
            },
            error: function (e) {
                clearInterval(progressInterval);

                $button.show('loading').parent().find('#previewProgress').remove();
                $orak.css('opacity', 1).css('pointer-events', 'initial');
            }
        });
    });
}

function moduleSaveFunction() {

    if($('#previewProgress').length) {
        toastr['error']("È in corso la generazione delle anteprime, attendi il caricamento prima di salvare.s");
    }

    $.ajax({
        url: "db/buildTheme.php",
        type: "POST",
        data: {
            "pID": $('#theme_id').val(),
            "pTitolo": $('#titolo').val(),
            "pSuggestedFor": $('#suggested_for').val(),
            "pTableToExport": $('#theme_table_export').val(),
            "pDescription": $('#descrizione').val(),
            "pDetails": tinymce.editors["dettagli"].getContent(),
            "pPreviews": composeOrakImagesToSave('themePreviews'),
            "pExplicitReplace": $('#explicit_replace').val()
        },
        preloader: "Creazione dell'archivo in corso",
        async: true,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            }  else if(data.status == "theme_exists") {
                bootbox.confirm("L'ID inserito è già utilizzato da un'altro tema. Sei sicuro di volerlo sovrascrivere?<br><b>NB:</b> Procedendo non sarà più possibile annullare l'azione", function (confirm) {
                    if(confirm) {
                        $('#explicit_replace').val('1');
                        $('#theme_id').attr('disabled', 'disabled').prop('disabled', 'disabled');
                        moduleSaveFunction();
                    }
                });
            } else if(data.status == "ok") {
                bootbox.alert({
                        title: 'Libreria dei temi aggiornata',
                        size: 'medium',
                        message: "<h4>Backup disponibile.</h4>" +
                            "<p>È stato preparato un backup del tema, clicca qui sotto per scaricarlo.</p>" +
                            "<hr>" +
                            "<p class=\"text-center\">" +
                            "<a href=\"" + data.download + "\"><i class=\"fa fa-download big-icon\"></i></a>" +
                            "</p>" +
                            "</div>",
                        callback: function () {
                            location.href = "../";
                        }
                    }
                );
            }
        }
    })
}
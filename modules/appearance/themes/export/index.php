<?php
/**
 * MSAdminControlPanel
 * Date: 23/09/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(!file_exists(CUSTOMER_DOMAIN_INFO['path'] . 'sitemap.xml')) {
    file_get_contents($MSFrameworkCMS->getURLToSite(1) . '/MSFramework/cron/Produzione/sitemap/generateSitemap.php?from_acp');
}

$all_urls = json_decode(json_encode(simplexml_load_file(CUSTOMER_DOMAIN_INFO['path'] . 'sitemap.xml')), TRUE);

$r = array();
if(isset($_GET['replace'])) {
    $theme_info = $MSFrameworkCMS->getCMSData('theme_info');
    if($theme_info) {
        $r = (new \MSFramework\Appearance\themes())->getThemeInfo($theme_info['id']);
    }
}

$images_to_preattach = array();
if($r) {
    foreach(array_merge(array($r['preview']), $r['images']) as $image) {
        if(!$image) continue;
        $image_id = end(explode('/', $image));
        $image_name = $r['id'] . '_' . $image_id;
        copy($r['path'] . '/preview/' . $image_id, UPLOAD_TMP_FOR_DOMAIN . 'tn/' . $image_name);
        $images_to_preattach[] = $image_name;
    }
}

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <input type="hidden" id="explicit_replace" value="<?= ($r ? '1' : '0'); ?>">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>Titolo Tema</label>
                                            <input class="form-control required" id="titolo" type="text" value="<?= htmlentities($r['name']); ?>">
                                        </div>
                                        <div class="col-sm-4">
                                            <label>ID</label>
                                            <input class="form-control required" id="theme_id" type="text" value="<?= htmlentities($r['id']); ?>" <?= ($r ? 'disabled' : ''); ?>>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Consigliato per</label>
                                            <select class="form-control" id="suggested_for" multiple="multiple">
                                                <?php foreach($MSFrameworkCMS->getExtraFunctionsDetails() as $extraFunctionID => $extraFunction) { ?>
                                                    <option value="<?= $extraFunctionID; ?>" <?= (($r && in_array($extraFunctionID, $r['functionalities'])) || (!$r && $MSFrameworkCMS->checkExtraFunctionsStatus($extraFunctionID)) ? 'selected' : '') ;?>><?= $extraFunction['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label>Dati da esportare</label>
                                            <select class="form-control" id="theme_table_export" multiple="multiple">
                                                <?php foreach((new \MSFramework\Appearance\themes())->getTableToExport(true) as $tableCat => $tableList) { ?>
                                                    <optgroup label="<?= $tableCat; ?>">
                                                        <?php foreach($tableList as $tableID => $tableValue) { ?>
                                                            <option value="<?= $tableID; ?>" <?= (($r && in_array($tableID, $r['dummy_data'])) || !$r ? 'selected' : '') ;?>><?= $tableValue['title']; ?></option>
                                                        <?php } ?>
                                                    </optgroup>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <label>Descrizione Breve</label>
                                    <textarea class="form-control required" id="descrizione" rows="5" style="min-height: 192px;"><?= htmlentities($r['description']); ?></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <label>Dettagli</label>
                                    <textarea class="form-control" id="dettagli" rows="5"><?= htmlentities($r['description']); ?></textarea>
                                </div>
                            </div>

                            <h2 class="title-divider">Anteprime</h2>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Anteprima 1</label>

                                    <select class="form-control previewLinks">
                                        <option value="">Seleziona un link</option>
                                        <?php foreach($all_urls['url'] as $url) { ?>
                                        <option value="<?= $url['loc']; ?>"><?= $url['loc']; ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                                <div class="col-sm-3">
                                    <label>Anteprima 2</label>

                                    <select class="form-control previewLinks">
                                        <option value="">Seleziona un link</option>
                                        <?php foreach($all_urls['url'] as $url) { ?>
                                            <option value="<?= $url['loc']; ?>"><?= $url['loc']; ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                                <div class="col-sm-3">
                                    <label>Anteprima 3</label>
                                    <select class="form-control previewLinks">
                                        <option value="">Seleziona un link</option>
                                        <?php foreach($all_urls['url'] as $url) { ?>
                                            <option value="<?= $url['loc']; ?>"><?= $url['loc']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <a class="btn btn-app web_template_button generate_web_preview mobile" style="margin: 5px 0 0;">
                                        <i class="fa fa-desktop"></i> Genera anteprime
                                    </a>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Oppure carica manualmente</label>
                                    <?php (new \MSFramework\uploads('APPEARANCE_THEME'))->initUploaderHTML("themePreviews", $images_to_preattach); ?>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
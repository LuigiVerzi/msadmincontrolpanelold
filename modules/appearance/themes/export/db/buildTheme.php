<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

if(empty($_POST['pID']) || empty($_POST['pTitolo'])) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$common_themes_path = PROD_SITES_HOME_FOLDER . 'MSCommonStorage/MSThemes/';
$theme_tmp_path = UPLOAD_TMP_FOR_DOMAIN . 'theme_export/';
$customer_website_path = CUSTOMER_DOMAIN_INFO['path'];

$theme_id = $MSFrameworkUrl->cleanString($_POST['pID']);

// Controllo l'esistenza del tema
if(!$_POST['pExplicitReplace'] && file_exists($common_themes_path . $theme_id)) {
    die(json_encode(array('status' => 'theme_exists')));
}

// Controllo sullo slug
if(empty($theme_id)) {
    die(json_encode(array('status' => 'mandatory_data_missing')));
}

// Elimino il contenuto della cartella temporanea dei temi
shell_exec('rm -f -r ' . $theme_tmp_path . '*');

// Creo le cartelle dei temi
mkdir($theme_tmp_path, 0777, true);
mkdir($theme_tmp_path . 'settings/', 0777, true);
mkdir($theme_tmp_path . 'files/', 0777, true);
mkdir($theme_tmp_path . 'uploads/', 0777, true);
mkdir($theme_tmp_path . 'uploads/img/', 0777, true);
mkdir($theme_tmp_path . 'preview/', 0777, true);

mkdir($common_themes_path . $theme_id . '/', 0777, true);
mkdir($common_themes_path . $theme_id . '/', 0777, true);

$currentThemeSettings = $MSFrameworkCMS->getCMSData('theme_info')['settings'];
if(!$currentThemeSettings) $currentThemeSettings = array();

// Prendo la lista di tutte le tabelle del tema
$table_dummy = array();
foreach($_POST['pTableToExport'] as $tableToExport) {
    foreach ($MSFrameworkDatabase->getFromDummyByPrefix($tableToExport) as $table) {
        $table_dummy[] = $table['TABLE_NAME'];
    }
}
$table_dummy = array_unique($table_dummy);

$info_json = array(
    "name" => $_POST['pTitolo'],
    "description" => $_POST['pDescription'],
    "details" => $_POST['pDetails'],
    "version" => 1.0,
    "functionalities" => $_POST['pSuggestedFor'],
    "default_settings" => $currentThemeSettings,
    "dummy_data" => $table_dummy,
);

// Creo il file info.json
file_put_contents($theme_tmp_path . 'info.json', json_encode($info_json));

// Creo le anteprime
foreach($_POST['pPreviews'] as $k => $preview) {
    if (file_exists(UPLOAD_TMP_FOR_DOMAIN . 'tn/' . $preview)) {
        copy(UPLOAD_TMP_FOR_DOMAIN . 'tn/' . $preview, $theme_tmp_path . 'preview/' . preg_replace('/^([^.]+)/i', ($k === 0 ? 'main' : $k), $preview));
    }
}

// Copio tutti i file del tema dalla cartella del cliente alla cartella temporanea
shell_exec("cp -r " . $customer_website_path . "www/. " . $theme_tmp_path . "files/");

// Copio i file di configurazione del tema dalla cartella del cliente alla cartella temporanea
shell_exec("cp -r " . $customer_website_path . "settings/. " . $theme_tmp_path . "settings/");

$tableList = (new \MSFramework\Appearance\themes())->getTableToExport(true);

$dummyDataSQL = array();
foreach($table_dummy as $single_table) {
    shell_exec('mysqldump --user=' . FRAMEWORK_DB_USER . ' --password=' . FRAMEWORK_DB_PASS . ' --host=' . FRAMEWORK_DB_HOST . ' --no-create-info --skip-add-drop-table --skip-lock-tables --complete-insert ' . $_SESSION['db'] . ' ' . $single_table . ' > ' . UPLOAD_TMP_FOR_DOMAIN . $single_table . '.sql');
    $dummyDataSQL[$single_table] = file_get_contents(UPLOAD_TMP_FOR_DOMAIN . $single_table . '.sql');
    unlink(UPLOAD_TMP_FOR_DOMAIN . $single_table . '.sql');
}

$uploadsPaths = array();
foreach($_POST['pTableToExport'] as $tableToExport) {
    foreach($MSFrameworkDatabase->getFromDummyByPrefix($tableToExport) as $table) {
        foreach($tableList as $tableCat => $tableValues) {
            if(isset($tableValues[$tableToExport])) {
                $uploadsPaths = array_merge($uploadsPaths, $tableValues[$tableToExport]['uploads']);
            }
        }
    }
}

// Elimino le definizioni doppie degli uploads
$uploadsPaths = array_unique($uploadsPaths);

// Copio tutti gli uploads nella cartella del tema
foreach($uploadsPaths as $uploadDir) {
    $realPath = constant("UPLOAD_" . strtoupper($uploadDir) . "_FOR_DOMAIN");
    $tmpPath = (new \MSFramework\uploads())->getCostants()[$uploadDir][0];

    mkdir($theme_tmp_path . "uploads/" . $tmpPath, 0777, true);

    // Copio tutti gli uploads dalla cartella del cliente alla cartella temporanea
    shell_exec("cp -r " . $realPath . ". " . $theme_tmp_path . "uploads/" . $tmpPath);
}

// Aggiungo le SQL dentro il file install.sql
mkdir($theme_tmp_path . "install/", 0777, true);
foreach($dummyDataSQL as $table_name => $sql) {
    file_put_contents($theme_tmp_path . 'install/' . $table_name . '.sql', $sql);
}

// Creo un archivio con i file del tema
shell_exec('cd ' . $theme_tmp_path . ' && tar -czf ' . UPLOAD_TMP_FOR_DOMAIN . $theme_id . '.tar.gz .');

// Sposto il tema sulla cartella Common dei temi
$target_theme_dir = $common_themes_path . $theme_id . "/";
shell_exec('rm -R ' . $target_theme_dir);
shell_exec("cp -r " . $theme_tmp_path . ". " . $target_theme_dir);
shell_exec('rm -R ' . $theme_tmp_path);

$archive_url = UPLOAD_TMP_FOR_DOMAIN_HTML . $theme_id . '.tar.gz';

if(!file_exists(UPLOAD_TMP_FOR_DOMAIN . $theme_id . '.tar.gz')) {
    echo json_encode(array("status" => "error"));
    die();
}

$MSFrameworkCMS->setCMSData('theme_info', array('id' => $theme_id), true);

echo json_encode(array("status" => "ok", 'download' => $archive_url));
die();
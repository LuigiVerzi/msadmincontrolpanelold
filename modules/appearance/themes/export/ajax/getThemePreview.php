<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$theme_id = $_POST['id'];

$theme_info = (new \MSFramework\Appearance\themes())->getThemeInfo($theme_id);

?>

<div class="product-detail">
    <div class="row">
            <div class="col-md-8">
                <div class="product-images theme-details-slider">
                    <div>
                        <img src="<?= $theme_info['preview']; ?>" class="img-responsive" style="width: 100%;">
                    </div>
                    <?php foreach($theme_info['images'] as $image) { ?>
                    <div>
                        <img src="<?= $image; ?>" class="img-responsive" style="width: 100%;">
                    </div>
                    <?php } ?>
                </div>

            </div>
            <div class="col-md-4">

                <h2 class="font-bold m-b-xs">
                    <?= $theme_info['name']; ?>
                </h2>

                <small><?= $theme_info['description']; ?></small>

                <hr>

                <div id="themeDetails">
                    <h4>Descrizione</h4>
                    <div class="small text-muted">
                        <?= $theme_info['details']; ?>
                    </div>

                    <hr>

                    <h4>Consigliato per</h4>
                    <div class="small">
                        <?php foreach($theme_info['functionalities'] as $functionality) { ?>
                            <small class="label label-inverse"><i class="fa fa-star"></i> <?= $functionality; ?></small>
                        <?php } ?>
                    </div>

                    <hr>

                    <div>
                        <button class="btn btn-primary btn-block" onclick="$('#themeDetails').fadeOut(function() {
                            $('#themePreinstallation').fadeIn();
                        });">Installazione <i class="fa fa-angle-right"></i></button>
                    </div>
                </div>
                <div id="themePreinstallation" style="display: none;">
                    <h4>Personalizza Installazione</h4>

                    <div class="styled-checkbox form-control">
                        <label style="width: 100%;">
                            <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin: 0;">
                                <input type="checkbox" id="theme_importDemoContent">
                                <i></i>
                            </div>
                            <span style="font-weight: normal;">Importa contenuti demo</span>
                        </label>
                    </div>

                    <hr>

                    <div class="btn-group" style="width: 100%;">
                        <button class="btn btn-default" style="width: 40%;" onclick="$('#themePreinstallation').fadeOut(function() {
                            $('#themeDetails').fadeIn();
                        });">Annulla</button>
                        <button class="btn btn-primary" style="width: 60%;" onclick="themeInstall('<?= $theme_info['id']; ?>',  '<?= htmlentities($theme_info['name']); ?>', $('#theme_importDemoContent:checked').length);"><i class="fa fa-download"></i> Installa</button>
                    </div>
                </div>

            </div>
        </div>
</div>
<?php
/**
 * Marketing Studio
 * Date: 26/01/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

mkdir(UPLOAD_TMP_FOR_DOMAIN, 0777, true);
mkdir(UPLOAD_TMP_FOR_DOMAIN . "tn/", 0777, true);

$images = array();

$urlToDownload = 'http://api.screenshotlayer.com/api/capture?access_key=ea696e5ec2e453ef45fe1489c865e855&viewport=1280x1280&width=800&delay=2&force=1&url=';

foreach($_POST['links'] as $k => $link) {
    $name = time() . '_' . $k . '_preview.png';

   $data = file_get_contents($urlToDownload . urlencode($link));
   file_put_contents(UPLOAD_TMP_FOR_DOMAIN . 'tn/' . $name, $data);
   $images[] = $name;
}

echo json_encode($images);
die();
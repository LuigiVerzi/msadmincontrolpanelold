<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($_SESSION['userData']['userlevel'] != 0) {
    die();
}

$theme_id = $_POST['theme_id'];
$themeDetails = (new \MSFramework\Appearance\themes())->getThemeInfo($theme_id);

$status = true;

if($themeDetails) {
    shell_exec('rm -f -r ' . $themeDetails['path'] . '/');
} else {
    $status = false;
}

if(!$status) {
    echo json_encode(array("status" => "error"));
    die();
}

echo json_encode(array("status" => "ok"));
die();
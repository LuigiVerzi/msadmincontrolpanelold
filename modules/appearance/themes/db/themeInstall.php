<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$theme_id = $_POST['theme_id'];
$install_content = $_POST['install_content'];

$status = (new \MSFramework\Appearance\themes())->installTheme($theme_id, $install_content);

if(!$status) {
    echo json_encode(array("status" => "error"));
    die();
}

echo json_encode(array("status" => "ok"));
die();
<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if(count($_POST['pExtraFunctionsDiffs']['enable']) > 0) {
    foreach($_POST['pExtraFunctionsDiffs']['enable'] as $module) {
        $MSFrameworkCMS->enableExtraFunction($module);
    }
}

if(count($_POST['pExtraFunctionsDiffs']['disable']) > 0) {
    foreach($_POST['pExtraFunctionsDiffs']['disable'] as $module) {
        $MSFrameworkCMS->disableExtraFunction($module);
    }
}

$result = $MSFrameworkCMS->setCMSData("producer_config", array(
    "extra_functions" => json_encode($_POST['pExtraFunctions'])
), true);

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok"));
die();
?>
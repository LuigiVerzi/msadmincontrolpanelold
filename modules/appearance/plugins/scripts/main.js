function initForm() {
    initClassicTabsEditForm();
    extraFunctionIfChanged();
}

function moduleSaveFunction(save_as_new, alert_func_disabled) {
    if(typeof(alert_func_disabled) == "undefined") {
        alert_func_disabled = true;
    }

    extra_func_ary = new Array();
    differences_extra_func = {
        "disable": [],
        "enable": [],
    };

    $('.extra_function').each(function() {
        cur_status = 0;
        if($(this).is(':checked')) {
            extra_func_ary.push($(this).data('func-name'));
            cur_status = 1;
        }

        old_status = $(this).data('original-status');
        if(old_status != cur_status) {
            if(cur_status == 0) {
                differences_extra_func['disable'].push($(this).data('func-name'));
            } else {
                differences_extra_func['enable'].push($(this).data('func-name'));
            }

        }
    });

    alert_func_disabled_shown = false;
    alert_messages = new Array();

    if(differences_extra_func['disable'].length != 0 && alert_func_disabled) {
        alert_func_disabled_shown = true;
        alert_messages.push('Attenzione! Hai disabilitato una o più funzionalità. Procedendo, tutti i dati inseriti nei moduli di tali funzionalità andranno persi e non saranno recuperabili.');
    }

    if(alert_messages.length) {
        bootbox.confirm(alert_messages.join("<hr>") + ' Vuoi proseguire?', function (result) {
            if (result) {
                moduleSaveFunction(save_as_new, false);
            }
        });
    }

    if(!alert_func_disabled_shown) {
        $.ajax({
            url: "db/saveData.php",
            type: "POST",
            data: {
                "pExtraFunctions": extra_func_ary,
                "pExtraFunctionsDiffs": differences_extra_func,
            },
            preloader: 'Configurazione del sito web in corso',
            async: true,
            dataType: "json",
            success: function (data) {
                if (data.status == "mandatory_data_missing") {
                    bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
                } else if (data.status == "not_allowed") {
                    bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
                } else if (data.status == "query_error") {
                    bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
                } else if (data.status == "ok") {
                    succesModuleSaveFunctionCallback(data);
                    location.reload();
                }
            }
        });
    }

}
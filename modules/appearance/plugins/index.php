<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <?php

                                $extra_functions_db = json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true);
                                $extra_functions_list = $MSFrameworkCMS->getExtraFunctionsDetails();

                                $active_dependencies = array();
                                foreach($extra_functions_list as $function => $functionDet) {
                                    if(isset($functionDet['dependencies']) && $functionDet['dependencies'] && in_array($function, $extra_functions_db))
                                    {
                                        $active_dependencies = array_merge($active_dependencies, $functionDet['dependencies']);
                                    }
                                }
                                ?>
                                <?php foreach($extra_functions_list as $function => $functionDet) { ?>
                                    <div class="col-sm-3">
                                        <div class="styled-checkbox form-control <?= (in_array($function, $active_dependencies) ? 'dependencies' : ''); ?>">
                                            <label style="width: 100%;">
                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                    <input type="checkbox" data-func-name="<?= $function ?>" id="extra_function_<?= $function ?>" class="extra_function_<?= $function ?> extra_function" <?php if(in_array($function, $extra_functions_db)) { echo "checked"; } ?> <?= ($functionDet['dependencies'] ? 'data-dependencies="' . htmlentities(json_encode($functionDet['dependencies'])) . '"' : ''); ?> data-original-status="<?php echo (in_array($function, $extra_functions_db) ? "1" : "0") ?>">
                                                    <i></i>
                                                </div>
                                                <span style="font-weight: normal;"><?= $functionDet['name'] ?></span>
                                            </label>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    </div>
</div>
<script>
    globalInitForm();
</script>
</body>
</html>
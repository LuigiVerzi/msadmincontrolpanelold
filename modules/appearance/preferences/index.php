<?php
/**
 * MSAdminControlPanel
 * Date: 19/05/19
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$themeInfo = $MSFrameworkCMS->getCMSData("theme_info");
$themeData = $themeInfo['settings'];
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content" id="themeSettings">
            <input type="hidden" id="theme_id" value="<?php echo $themeInfo['id'] ?>" />

            <script>
                window.themeJS = [];
            </script>

            <?php
            if($themeInfo['id']) {
                $themeDetails = (new \MSFramework\Appearance\themes())->getThemeInfo($themeInfo['id']);

                $settingsPath = $themeDetails['path'] . '/settings';

                if(file_exists(CUSTOMER_DOMAIN_INFO['path'] . 'settings/')) {
                    $settingsPath = CUSTOMER_DOMAIN_INFO['path'] . 'settings';
                }

                $settings_files = array();
                foreach(glob($settingsPath . '/*') as $settings_file) {
                    $file_id = str_replace('.php', '', end(explode('/', $settings_file)));
                    $settings_name = 'Generali';
                    $file_content = file_get_contents($settings_file);

                    if (preg_match_all('/\/*Tab:([^\n]+)/m', $file_content, $matches, PREG_SET_ORDER, 0)) {
                        $settings_name = trim($matches[0][1]);
                    }

                    $is_main = false;
                    if (preg_match_all('/\/*Main:([^\n]+)/m', $file_content, $matches, PREG_SET_ORDER, 0)) {
                        if(trim($matches[0][1]) == 1) {
                            $is_main = true;
                        }
                    }

                    $file_data = array(
                        'id' => $file_id,
                        'name' => $settings_name,
                        'main' => $is_main,
                        'path' => $settings_file
                    );

                    if($is_main) {
                        array_unshift($settings_files, $file_data);
                    } else {
                        $settings_files[] = $file_data;
                    }
                }

                if($settings_files) {
                    echo '<form method="get" id="form" action="#" class="form-horizontal wizard-big">';
                    foreach ($settings_files as $settings_file) {
                        echo '<h1>' . $settings_file['name'] . '</h1>';
                        echo '<fieldset>';
                        echo '<div class="settings-group" data-name="' . $settings_file['id'] . '">';
                        include($settings_file['path']);
                        echo '</div>';
                        echo '</fieldset>';
                    }
                    echo '</form>';

                    echo '<textarea id="currentThemeSettings" style="display: none;">' . htmlentities(json_encode(($themeData ? $themeData : array()))) . '</textarea>';
                }
            }
            ?>

            <?php if(!$settings_files) { ?>
                <div class="alert alert-danger">
                    Attualmente non hai installato nessun tema personalizzabile. Per utilizzare le configurazioni <a href="../themes">installa un tema</a> prima.
                </div>
            <?php } ?>

        </div>
        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
    </div>
</div>

<div id="demoOrakUploader" style="display: none;">
    <?php (new \MSFramework\uploads('APPEARANCE_THEME'))->initUploaderHTML("demoOrakUploader", array()); ?>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$settings = $_POST['pThemeData'];

$old_settings_json = json_encode($MSFrameworkCMS->getCMSData('theme_info'));

$uploader = new \MSFramework\uploads('APPEARANCE_THEME');

$settings = json_decode($uploader->saveFromBlob(json_encode($settings)), true);

$MSFrameworkCMS->setCMSData("theme_info", array(
    'settings' => $settings
), true);

$uploader->unlinkFromHTML(json_encode($settings), $old_settings_json);

echo json_encode(array("status" => "ok"));
die();
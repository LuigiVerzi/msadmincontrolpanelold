function initForm() {
    initClassicTabsEditForm();
    setThemeSettings();
    initThemeSettings();
}

function initThemeSettings() {
    var $themeSettings = $('#themeSettings');

    if($themeSettings.find('.tinymce').length) {
        $themeSettings.find('.tinymce').each(function () {
            var tinymce_id = element.getUniqueID();
            $(this).attr('id', tinymce_id);

            var tinyMCEOptions = {};
            if(Object.keys(element.settings.shortcodes).length) {
                tinyMCEOptions.shortcodes = {custom: Object.keys(element.settings.shortcodes)};
            }

            initTinyMCE($('#' + tinymce_id), tinyMCEOptions);
        });
    }

    if($themeSettings.find('.colorpicker-component').length) {
        $themeSettings.find('.colorpicker-component').colorpicker().on('changeColor', function(event) {
            $(this).find('input').change();
        });
    }

    $themeSettings.find('.uploader').each(function () {
        var id = $(this).attr('name');
        var limit = (typeof($(this).data('limit')) !== 'undefined' ? $(this).data('limit') : 1);

        var value = $(this).data('value');

        $(this).html($('#demoOrakUploader').html().replace(/demoOrakUploader/g, $(this).attr('name'))).find('#prev_upl_' + $(this).attr('name')).val(value);

        initOrak(id, limit, 15, 15, 200, getOrakImagesToPreattach(id), false, ['image/jpeg', 'image/png']);
    });

    if($themeSettings.find('.colorpicker').length) {
        $themeSettings.find('.colorpicker').colorpicker();
    }

    themeJS.forEach(function ($fn) {
        $fn();
    });
}

function setThemeSettings() {
    var data = JSON.parse($('#currentThemeSettings').val());

    var walked = [];
    var stack = [{obj: data, stack: ''}];
    while(stack.length > 0)
    {
        var item = stack.pop();
        var obj = item.obj;
        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
                if (typeof obj[property] == "object") {
                    var alreadyFound = false;
                    for(var i = 0; i < walked.length; i++) {
                        if (walked[i] === obj[property])
                        {
                            alreadyFound = true;
                            break;
                        }
                    }
                    if (!alreadyFound) {
                        walked.push(obj[property]);
                        stack.push({obj: obj[property], stack: item.stack + '[data-name="' + property + '"] '});
                    }
                }
                else
                {
                    setFieldValue($(item.stack + '[name="' + property + '"]'), obj[property]);
                }
            }
        }
    }
}

function setFieldValue($field, value) {
    if($field.attr('type') === 'checkbox' || $field.attr('type') === 'radio') {
        if(value == 1) {
            $field.prop('checked', true);
        }
    } else if($field.hasClass('uploader')) {
        $field.data('value', value);
    } else {
        $field.val(value);
    }
}

function getSettings() {
    var getChildren = function ($parent) {
        var tmpData = {};

        $parent.find('.settings-group').not($parent.find('.settings-group .settings-group')).each(function () {

            var group = $(this).data('name');
            if(typeof(tmpData[group]) === 'undefined') {
                tmpData[group] = {};
            }

            $(this).find('.theme-data[name]').not($(this).find('.settings-group .theme-data[name]')).each(function () {
                var name = $(this).attr('name');
                var value = '';

                if($(this).hasClass('tinymce')) {
                    value = tinymce.get($(this).attr('id')).getContent();
                } else if($(this).attr('type') === 'checkbox' || $(this).attr('type') === 'radio') {
                    if($(this).is(':checked')) value = $(this).val();
                } else if($(this).hasClass('uploader')) {
                    value = JSON.stringify(composeOrakImagesToSave($(this).attr('name')));
                } else {
                    value = $(this).val();
                }

                tmpData[group][name] = value;
            });

            if($(this).find('.settings-group').length) {
                tmpData[group] = Object.assign(tmpData[group], getChildren($(this)));
            }

        });

        return tmpData;
    };

    return getChildren($('#themeSettings'));
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pThemeData": getSettings(),
        },
        async: true,
        dataType: "json",
        success: function (data) {
            if (data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if (data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if (data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}
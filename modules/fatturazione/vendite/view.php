<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$impostazioni_fatture = $MSFrameworkCMS->getCMSData('fatturazione_fatture');
if(isset($_GET['id']) && !empty($_GET['id'])) {
    $r = (new \MSFramework\Fatturazione\vendite())->ottieniDettagliVendita($_GET['id'])[$_GET['id']];
}

if(!isset($r) || !$r) {
    header('Location: index.php');
}

$customer_info = $r['dati_cliente'];
$carrello_info = $r['carrello'];
$sedute_utilizzate = json_decode($r['sedute_utilizzate'], true);
$operator_info = (new \MSFramework\users())->getUserDataFromDB($r['operatore']);
$impostazioni_documento = $r['info_documento'];
$crediti_utente = (new \MSFramework\customers())->getCustomerBalance($r['cliente']);

$totale_carrello = 0.0;
$totale_pagato = 0.0;

foreach($carrello_info as $articolo) {
    $totale_carrello += $articolo['subtotal'];
    $totale_pagato += $articolo['prezzo_pagato'];
}

$document_change_types = array(
    'ricevuta' => array('fattura', 'preventivo'),
    'fattura' => array('ricevuta', 'preventivo'),
    'preventivo' => array('fattura', 'ricevuta')
);

$document_change_btns = array(
    'ricevuta' => 'Ricevuta',
    'fattura' => 'Fattura',
    'preventivo' => 'Preventivo',
    '' => 'Documento',
);


?>

<style>
    #iframe_fattura {
        width: 100%;
        height: 100vh;
    }
</style>

<script>
    function iconError(img) {
        var type = $(img).closest('.add_to_cart').data('type');
        $(img).attr('src', $('#baseElementPathAdmin').val() + 'modules/fatturazione/vendite/images/' + type + '.png');
    }
</script>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">

                <?php
                if(in_array($r['tipo_documento'], array_keys($document_change_types)) || empty($r['tipo_documento'])) {
                    $customToolbarButton = '<a class="btn btn-warning" href="edit.php?id=' . $_GET['id'] . '&force_edit">Modifica</a>';
                }
                ?>

                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardViewDelEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">

                <div class="col-lg-9">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <?php if(isset($impostazioni_documento['source'])) { ?>
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5><?= ucfirst($r['tipo_documento']); ?></h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <iframe id="iframe_fattura" src="<?= $impostazioni_documento['source']; ?>"></iframe>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="ibox" style="margin: 0;">
                            <div class="ibox-content">

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Data Operazione</label>
                                        <p><?= date("d/m/Y H:i", strtotime($r['data'])); ?></p>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Operatore</label>
                                        <p><?= $operator_info['nome'] . ' ' . $operator_info['cognome']; ?></p>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Tipo di Documento</label>
                                        <p><?= (!empty($r['tipo_documento']) ? (new \MSFramework\Fatturazione\vendite())->ottieniTipiDocumento($r['tipo_documento']) : '<span class="text-muted">Nessuno</span>'); ?></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <h2 class="title-divider">Dati Cliente</h2>
                                        <p>
                                            <?php echo $customer_info['nome'] ?> <?php echo $customer_info['cognome'] ?>
                                            <?php echo ($customer_info['sesso'] == "m" ? ', Uomo' : ($customer_info['sesso'] == "f" ? ', Donna' : '')); ?>
                                            <?php echo ($customer_info['data_nascita'] != '') ? ', ' . $customer_info['data_nascita'] : '' ?>
                                        </p>
                                        <p>
                                            <?php echo implode(', ', array_filter(array($customer_info['email'], $customer_info['telefono_cellulare'], $customer_info['telefono_casa']))); ?>
                                        </p>
                                        <p>
                                            <?php echo implode(', ', array_filter(array($customer_info['indirizzo'], $customer_info['citta'], $customer_info['provincia'], $customer_info['cap']))); ?>
                                        </p>
                                    </div>
                                    <div class="col-lg-6">
                                        <h2 class="title-divider">Dati Fatturazione</h2>
                                        <p><?php echo (!empty($customer_info['fatturazione']['ragione_sociale']) ? $customer_info['fatturazione']['ragione_sociale'] : $customer_info['nome'] . " " . $customer_info['cognome']) ?></p>
                                        <p>
                                            <?php echo (!empty($customer_info['fatturazione']['piva']) ? 'P.IVA: ' . $customer_info['fatturazione']['piva'] : '') ?>
                                            <?php echo (!empty($customer_info['fatturazione']['cf']) ? (!empty($customer_info['fatturazione']['piva']) ? ' - ' : '') . ' C.F: ' . $customer_info['fatturazione']['cf'] : '') ?>
                                        </p>
                                        <p>
                                            <?php echo (!empty($customer_info['fatturazione']['indirizzo']) ? $customer_info['fatturazione']['indirizzo'] : '') ?>
                                            <?php echo (!empty($customer_info['fatturazione']['citta']) ? (!empty($customer_info['fatturazione']['indirizzo']) ? ', ' : '') . $customer_info['fatturazione']['citta'] : '') ?>
                                            <?php echo (!empty($customer_info['fatturazione']['provincia']) ? ', ' . $customer_info['fatturazione']['provincia'] : '') ?>
                                            <?php echo (!empty($customer_info['fatturazione']['cap']) ? ', ' . $customer_info['fatturazione']['cap'] : '') ?>
                                        </p>
                                    </div>
                                </div>

                                <?php
                                if(is_array($carrello_info) && count($carrello_info) != 0) {
                                ?>

                                <h2 class="title-divider">Articoli acquistati</h2>
                                <div class="table-responsive m-t">
                                    <table class="table table-bordered table-striped" id="tabella_articoli">
                                        <thead>
                                        <tr>
                                            <th>Nome Articolo</th>
                                            <th style="width: 100px;">Qta.</th>
                                            <th style="width: 150px;">Prezzo (Senza IVA)</th>
                                            <th style="width: 150px;">Sconto</th>
                                            <th style="width: 100px;">Imposta</th>
                                            <th>Totale</th>
                                            <th>Totale Pagato</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($carrello_info as $carrello) { ?>
                                            <tr id="<?= $carrello['type']; ?>_<?= $carrello['id']; ?>">
                                                <td>
                                                    <input type="hidden" name="nome" class="form-control" value="<?= $carrello['nome']; ?>">
                                                    <?= $carrello['nome']; ?>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="qty" class="form-control" value="<?= $carrello['qty']; ?>">
                                                    <?= $carrello['qty']; ?>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="hidden" name="prezzo" class="form-control" value="<?= $carrello['prezzo']; ?>">
                                                        <?= number_format($carrello['prezzo'], 2, ',', '.'); ?><?= CURRENCY_SYMBOL; ?>
                                                    </div>
                                                </td>
                                                <td style="padding-right: 70px; position:relative;">
                                                    <input type="hidden" name="sconto" class="form-control" value="<?= $carrello['sconto']; ?>">
                                                    <?= $carrello['sconto']; ?><?= $carrello['tipo_sconto']; ?>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="hidden" name="imposta" class="form-control" value="<?= $carrello['imposta']; ?>">
                                                        <?= $carrello['imposta']; ?>%
                                                    </div>
                                                </td>
                                                <td class="subtotal">
                                                    <input type="hidden" name="no_tax_subtotal" value="<?= $carrello['no_tax_subtotal']; ?>">
                                                    <input type="hidden" name="subtotal" value="<?= $carrello['subtotal']; ?>">
                                                    <span><?= number_format($carrello['prezzo_subtotal'], 2, ',', '.'); ?><?= CURRENCY_SYMBOL; ?></span>
                                                    <input type="hidden" name="type" value="<?= $carrello['type']; ?>">
                                                    <input type="hidden" name="id" value="<?= $carrello['id']; ?>">
                                                    <input type="hidden" name="prezzo_iva_inclusa" value="<?= $carrello['prezzo_tax']; ?>">
                                                    <input type="hidden" name="valore_imposta" value="<?= $carrello['valore_imposta']; ?>">
                                                </td>
                                                <td>
                                                    <span><?= number_format($carrello['prezzo_pagato'], 2, ',', '.'); ?><?= CURRENCY_SYMBOL; ?></span>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } ?>

                                <?php if(count($sedute_utilizzate) != 0) { ?>

                                <h2 class="title-divider">Sedute Effettuate</h2>
                                <div class="table-responsive m-t">
                                    <table class="table table-bordered table-striped" id="tabella_sedute">
                                        <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th style="width: 100px;">Qta.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($sedute_utilizzate as $seduta) { ?>
                                            <tr>
                                                <td>
                                                    <?= $seduta['name']; ?>
                                                </td>
                                                <td>
                                                    <?= $seduta['qty']; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } ?>

                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-lg-3">

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Riepilogo Vendita</h5>
                        </div>
                        <div class="ibox-content">
                            <?php if($impostazioni_documento['usa_crediti'] == 1) { ?>
                                <div id="customer_credits_box">
                                    <?php if($impostazioni_documento['valore_credito'] > 0) { ?>
                                        Pagati <b class="text-navy" id="crediti_detraibili"><?= number_format(abs($impostazioni_documento['valore_credito']), 2, ',', '.'); ?><?= CURRENCY_SYMBOL; ?></b> tramite i crediti
                                    <?php } else { ?>
                                        Aggiunti <b class="text-danger" id="crediti_detraibili"><?= number_format(abs($impostazioni_documento['valore_credito']), 2, ',', '.'); ?><?= CURRENCY_SYMBOL; ?></b> di debiti
                                    <?php } ?>
                                </div>
                            <?php } ?>

                            <div class="row">
                                <div class="col-md-6">
                                    <span>Da pagare</span>
                                    <h2 class="font-bold" id="totale_resoconto">
                                        <?= number_format($totale_carrello, 2, ',', '.'); ?><?= CURRENCY_SYMBOL; ?>
                                    </h2>
                                </div>

                                <div class="col-md-6">
                                    <span>Pagato</span>
                                    <h2 class="font-bold" id="totale_pagato">
                                        <?= number_format($totale_pagato, 2, ",", ".") ?><?= CURRENCY_SYMBOL; ?>
                                    </h2>
                                </div>
                            </div>

                            <?php
                            if($totale_pagato != $totale_carrello) {
                                $debt_str = "";
                                if($totale_pagato < $totale_carrello) {
                                    $debt_str = 'Al cliente sono stati aggiunti <b class="text-danger" id="crediti_detraibili">' . ($totale_carrello-$totale_pagato) . '<?= CURRENCY_SYMBOL; ?></b> di debito';
                                } else if($totale_pagato > $totale_carrello) {
                                    $debt_str = 'Al cliente sono stati aggiunti <b class="text-danger" id="crediti_detraibili">' . ($totale_pagato-$totale_carrello) . '<?= CURRENCY_SYMBOL; ?></b> di credito';
                                }
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="credit_variation"><?= $debt_str ?></div>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if(isset($impostazioni_documento['pagamento'])) { ?>
                            Pagato tramite <b><?php echo (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamento()[$impostazioni_documento['pagamento']]['nome']; ?></b>
                            <?php } ?>
                        </div>
                    </div>

                    <?php if(count($sedute_utilizzate) != 0) { ?>
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Sedute Effettuate</h5>
                        </div>
                        <div class="ibox-content" style="padding-bottom: 50px;">
                            <table id="main_table_list" class="balance_table display table table-striped table-bordered table-hover"width="100%" cellspacing="0" style="margin-bottom: -30px;">
                                <tbody>
                            <?php
                            foreach($sedute_utilizzate as $seduta) {
                            ?>
                                <tr>
                                    <td width="70%">
                                        <span class="service_name"><?= $seduta['name'] ?></span>
                                    </td>
                                    <td width="30%">
                                        <?= $seduta['qty'] ?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php } ?>

                    <div id="impostazioni_documento">
                        <?php if($r['tipo_documento'] && in_array($r['tipo_documento'], array('scontrino', 'fattura', 'scontrino_non_fiscale', 'ricevuta', 'preventivo'))) { ?>

                            <?php if($r['tipo_documento'] == 'fattura') { ?>
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5>Fattura #<?php echo (int)$impostazioni_documento['numero']; ?></h5>
                                    </div>
                                    <div class="ibox-content">
                                        <?php if(!empty($impostazioni_documento['note_interne'])) { ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>Note Interne</label>
                                                    <p><?php echo htmlentities($impostazioni_documento['note_interne']); ?></p>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                        <?php } ?>
                                        Fattura <span class="text-navy"><i class="fa fa-check"></i> generata</span> il <b><?= date("d/m/Y H:i", strtotime($r['data'])); ?></b>
                                    </div>
                                </div>
                            <?php } else if($r['tipo_documento'] == 'ricevuta') { ?>
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5>Ricevuta</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <?php if(!empty($impostazioni_documento['note_interne'])) { ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>Note Interne</label>
                                                    <p><?php echo htmlentities($impostazioni_documento['note_interne']); ?></p>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                        <?php } ?>
                                        Ricevuta <span class="text-navy"><i class="fa fa-check"></i> generata</span> il <b><?= date("d/m/Y H:i", strtotime($r['data'])); ?></b>
                                    </div>
                                </div>
                            <?php } else if($r['tipo_documento'] == 'preventivo') { ?>
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5>Preventivo</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <?php if(!empty($impostazioni_documento['note_interne'])) { ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>Note Interne</label>
                                                    <p><?php echo htmlentities($impostazioni_documento['note_interne']); ?></p>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                        <?php } ?>
                                        Preventivo <span class="text-navy"><i class="fa fa-check"></i> generato</span> il <b><?= date("d/m/Y H:i", strtotime($r['data'])); ?></b>
                                    </div>
                                </div>
                            <?php } else if($r['tipo_documento'] == 'scontrino') { ?>
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5>Scontrino</h5>
                                    </div>
                                    <div class="ibox-content text-center">
                                        Scontrino <span class="text-navy"><i class="fa fa-check"></i> generato</span> il <b><?= date("d/m/Y H:i", strtotime($r['data'])); ?></b>
                                    </div>
                                </div>
                            <?php } else if($r['tipo_documento'] == 'scontrino_non_fiscale') { ?>
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5>Scontrino Non Fiscale</h5>
                                    </div>
                                    <div class="ibox-content text-center">
                                        Scontrino <span class="text-navy"><i class="fa fa-check"></i> generato</span> il <b><?= date("d/m/Y H:i", strtotime($r['data'])); ?></b>
                                    </div>
                                </div>
                            <?php } ?>

                        <?php } ?>

                        <?php if(in_array($r['tipo_documento'], array_keys($document_change_types)) || $r['tipo_documento'] === '') { ?>
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Documento</h5>
                                </div>
                                <div class="ibox-content">

                                    <?php if($r['sent_date'] === '0000-00-00 00:00:00') { ?>
                                    <a href="#" class="btn btn-primary btn-block" id="sendDocumentViaEmail"><i class="fa fa-send"></i> Invia via Email al cliente</a>
                                    <?php } else { ?>
                                        Documento <span class="text-navy"><i class="fa fa-check"></i> inviato</span> il <b><?= date("d/m/Y H:i", strtotime($r['sent_date'])); ?></b>
                                        <div class="hr-line-dashed"></div>
                                        <a href="#" class="btn btn-info btn-block" id="sendDocumentViaEmail"><i class="fa fa-send"></i> Invia nuovamente</a>
                                    <?php } ?>
                                    <div class="row">
                                        <?php foreach($document_change_types[$r['tipo_documento']] as $change_btn) { ?>
                                            <div class="col-12"><div class="hr-line-dashed"></div></div>
                                            <div class="col-sm-12 m-b-sm">
                                                <a href="#" class="btn btn-info btn-block change_document_type" data-target="<?= $change_btn; ?>"><i class="fa fa-file-o"></i> Genera <?= $document_change_btns[$change_btn]; ?></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <?php
                    if($r['sources']) {

                        $sources_to_search = array(
                            'referer' => array(
                                'icon' => 'fa fa-globe',
                                'name' => 'Referente',
                                'color' => '#2f2f2f'
                            ),
                            'gclid' => array(
                                'name' => 'Google Ads gclid',
                                'icon' => 'fa fa-google',
                                'color' => '#13a365'
                            ),
                            'fbclid' => array(
                                'name' => 'Facebook fbclid',
                                'icon' => 'fa fa-facebook',
                                'color' => '#3C5A99'
                            )
                        );

                        $sources_html = '';
                        foreach($sources_to_search as $search_key => $search_values) {
                            if(isset($r['sources'][$search_key])) {
                                $sources_html .= '<tr><td><span class="label" style="color: white; background: ' . $search_values['color'] . ';"><i class="' . $search_values['icon'] . '"></i> ' . $search_values['name'] . '</span></td><td>' . date('d/m/Y H:i', $r['sources'][$search_key]['date']) . '</td></tr>';
                            }
                        }
                    }
                    ?>

                    <?php if($sources_html) { ?>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Origine</th>
                                <th>Referenza</th>
                                <th>Data</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?= $sources_html; ?>
                            </tbody>
                        </table>
                    <?php } ?>


                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
<script>
    globalInitForm();
</script>
</body>
</html>
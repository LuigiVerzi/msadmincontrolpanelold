<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = array();
$customer_info = array('fatturazione' => array());
$carrello_info = array(array());
$impostazioni_documento = array();
$crediti_utente = 0.0;

$impostazioni_fatture = $MSFrameworkCMS->getCMSData('fatturazione_fatture');

if(isset($_GET['id']) && !empty($_GET['id'])) {
    $doc_details = (new \MSFramework\Fatturazione\fatture())->getDocumentDetails($_GET['id'])[$_GET['id']];
    if((in_array($doc_details['tipo_documento'], array('fattura', 'ricevuta', 'preventivo')) || empty($r['tipo_documento'])) && !isset($_GET['force_edit'])) {
        header('Location: view.php?id=' . $_GET['id']);
    }
}

$servizi = array();
$categorie_servizi = (new \MSFramework\services())->getCategoryDetails();

foreach((new \MSFramework\services())->getServiceDetails() as $servizio) {
    $servizio['category_name'] = (isset($categorie_servizi[$servizio['category']]) ? $MSFrameworki18n->getFieldValue($categorie_servizi[$servizio['category']]['nome']) : 'Servizio');
    $servizio['images'] = json_decode($servizio['images'], true);
    $servizio['imposta'] = $servizio['extra_fields']['imposta'];
    $servizi[$servizio['id']] = $servizio;
}
usort($servizi, function ($a, $b) {
    return $a['category'] - $b['category'];
});

/* OTTIENE I PRODOTTI ECOMMERCE */
$prodotti = array();
if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) {
    foreach ((new \MSFramework\Ecommerce\products())->getProductsList(array("filters" => array()))['products'] as $prodotto) {
        $category = explode(',', $prodotto['category'])[0];
        $prodotto['category_name'] = (new \MSFramework\Ecommerce\categories())->getCategoryDetails($category)[$category]['nome'];
        $prodotto['gallery'] = json_decode($prodotto['gallery'], true);
        $prodotti[] = $prodotto;
    }
}

/* OTTIENE I PACCHETTI */
$pacchetti = array();
if($MSFrameworkCMS->checkExtraFunctionsStatus('beautycenter')) {
    foreach ((new \MSFramework\BeautyCenter\offers())->getOfferDetails() as $offerta) {

        $valore = $offerta['prezzo'];

        $pacchetti[] = array(
            'id' => $offerta['id'],
            'nome' => $offerta['nome'],
            'tipo' => $offerta['type'],
            'valore' => $valore,
            'senza_iva' => ($offerta['type'] != 'sconto' ? floatval($offerta['prezzo_formattato']['no_tax']) : floatval($valore)),
            'images' => $offerta["gallery_friendly"],
            'imposta' => (isset($offerta['extra_fields']['imposta']) ? $offerta['extra_fields']['imposta'] : 0)
        );

    }
}

/* OTTIENE GLI ARTICOLI FREQUENTI */
$articoli_frequenti = (new \MSFramework\Fatturazione\vendite())->ottieniArticolPiuVenduti();

$users_where_str = "";
$users_where_ary = array();
if((new \MSFramework\users())->getUserDataFromSession('userlevel') != "0" && (new \MSFramework\users())->getUserDataFromSession('userlevel') != "1") {
    $users_where_str = " AND id = :id ";
    $users_where_ary = array(":id" => (new \MSFramework\users())->getUserDataFromSession('user_id'));
}

$preload_customer = "";
if($_GET['event_id'] != "") {
    //Il modulo è stato aperto tramite lo shortcut del calendario, recupero i dati dell'evento e precarico i relativi dati nell'interfaccia
    $event_r = $MSFrameworkDatabase->getAssoc("SELECT * FROM appointments_list WHERE id = :id", array(":id" => $_GET['event_id']), true);

    $event_r_customer = (new \MSFramework\customers())->getCustomerDataFromDB($event_r['customer'], 'id, nome, cognome, dati_fatturazione, email, telefono_cellulare, telefono_casa');
    $event_r_customer['dati_fatturazione'] = json_decode($event_r_customer['dati_fatturazione'], true);
    $customer_select_str = '<b>' . $event_r_customer['nome'] . ' ' . $event_r_customer['cognome'] . '</b>' . (!empty($event_r_customer['dati_fatturazione']['ragione_sociale']) ? ' - ' . $event_r_customer['dati_fatturazione']['ragione_sociale'] : '') . ($event_r_customer['email'] ? ' (' . $event_r_customer['email'] . ')' : '') . ($event_r_customer['telefono_cellulare'] ? ' (' . $event_r_customer['telefono_cellulare'] . ')' : '') . ($event_r_customer['telefono_casa'] ? ' (' . $event_r_customer['telefono_casa'] . ')' : '');

    $preload_services = explode(",", $event_r['services']);
    $preload_customer = $event_r['customer'];
}

if($_GET['id'] != "") {
    //siamo in fase di modifica della fattura
    $event_r_customer = (new \MSFramework\customers())->getCustomerDataFromDB($doc_details['cliente'], 'id, nome, cognome, dati_fatturazione, email, telefono_cellulare, telefono_casa');
    $event_r_customer['dati_fatturazione'] = json_decode($event_r_customer['dati_fatturazione'], true);
    $customer_select_str = '<b>' . $event_r_customer['nome'] . ' ' . $event_r_customer['cognome'] . '</b>' . (!empty($event_r_customer['dati_fatturazione']['ragione_sociale']) ? ' - ' . $event_r_customer['dati_fatturazione']['ragione_sociale'] : '') . ($event_r_customer['email'] ? ' (' . $event_r_customer['email'] . ')' : '') . ($event_r_customer['telefono_cellulare'] ? ' (' . $event_r_customer['telefono_cellulare'] . ')' : '') . ($event_r_customer['telefono_casa'] ? ' (' . $event_r_customer['telefono_casa'] . ')' : '');

    $preload_customer = $doc_details['cliente'];
    $preload_doc_type = $doc_details['tipo_documento'];
    $preload_doc_info = $doc_details['info_documento'];
    $preload_cart = json_decode($doc_details['carrello'], true);
}

$info_cliente = array();
if($preload_customer) {
    $info_cliente = (new \MSFramework\customers())->getCustomerDataFromDB($preload_customer);
}

if($info_cliente) {
    $info = array();
    if (!empty($info_cliente['email'])) {
        $info[] = $info_cliente['email'];
    }
    if (!empty($info_cliente['telefono_casa'])) {
        $info[] = $info_cliente['telefono_casa'];
    }
    if (!empty($info_cliente['telefono_cellulare'])) {
        $info[] = $info_cliente['telefono_cellulare'];
    }

    $anteprima_cliente = '';

    $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
    if(json_decode($info_cliente['avatar'])) {
        $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($info_cliente['avatar'], true)[0];
    }

    $anteprima_cliente .= '<div style="position: relative; padding-left: 65px;">';
    $anteprima_cliente .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
    $anteprima_cliente .= '<h2 class="no-margins">' . htmlentities($info_cliente['nome'] . ' ' . $info_cliente['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
    $anteprima_cliente .= '</div>';
}

?>

<style>
    .cliente_box:not(.show_edit) .col-lg-9 {
        width: 100% !important;
    }

    .cliente_box.show_edit #editCustomer {
        display: block !important;
    }

    .cliente_box .chosen-container-single {
        width: 100% !important;
    }

    .add_to_cart {
        text-align: left;
        margin-bottom: 15px;
        height: 93px;
    }

    .add_to_cart .category {
        display: block;
        text-align: left;
        font-size: 13px;
        text-overflow: ellipsis;
        overflow: hidden;
    }

    .add_to_cart .name {
        font-weight: 500;
        text-overflow: ellipsis;
        overflow: auto;
        display: block;
        white-space: initial;
        width: 100%;
        max-height: 40px;
    }

    .add_to_cart .price {
        font-weight: 500;
        text-align: right;
        color: #299946;
    }

    .add_to_cart .icon {
        display: block;
        width: 58px;
        height: 58px;
        float: left;
        margin-left: -5px;
        margin-right: 10px;
        border-radius: 3px;
        object-fit: cover;
        margin-top: 11px;
    }

    .add_to_cart .details_container {
        margin-left: 65px;
        width: 78%;
    }

    #articoli_e_servizi {
        margin-top: 15px;
    }

    #iframe_fattura {
        width: 100%;
        height: 70vh;
        height: calc(100vh - 280px);
    }
</style>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />
            <input type="hidden" id="event_id" value="<?php echo $_GET['event_id'] ?>" />

            <div class="row">

                <div class="col-lg-9">

                    <form method="get" id="form" action="#">

                        <div class="ibox" style="margin: 0;">
                            <div class="ibox-content">
                                <div class="row">

                                    <input type="hidden" id="cliente" value="<?= $info_cliente['id']; ?>">

                                    <div class="col-sm-6 cliente_box" id="customerInfo">
                                        <label>Cliente</label>

                                        <?php if($info_cliente) { ?>
                                            <div class="data" style="margin-bottom: 15px; float: left;">
                                                <?= $anteprima_cliente; ?>
                                            </div>
                                        <?php } else { ?>
                                            <div class="data" style="margin-bottom: 15px; float: left; display: none;"></div>
                                        <?php } ?>

                                        <a href="#" class="btn btn-sm btn-default createCustomer">
                                            Crea nuovo
                                        </a>

                                        <a href="#" class="changeCustomer btn btn-sm btn-default selectCustomer" style="margin-right: 5px;">
                                            <?php if($info_cliente) { ?>
                                                Cambia
                                            <?php } else { ?>
                                                Seleziona cliente
                                            <?php } ?>
                                        </a>

                                        <a href="#" id="editCustomer" class="editCustomer btn btn-warning btn-sm btn-outline" style="margin-right: 5px; display: none;">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>

                                    <div class="col-sm-3">
                                        <label>Operatore</label>
                                        <select id="operatore" name="operatore" class="form-control">
                                            <?php
                                            $got_selected = false;
                                            foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome FROM users WHERE id != '' $users_where_str", $users_where_ary) as $operatore) {
                                                $set_selected = "";

                                                if(!$got_selected) {
                                                    if ($event_r['user'] != "") {
                                                        $operatore_to_check = $event_r['user'];
                                                    } else {
                                                        $operatore_to_check = $_SESSION['userData']['id'];
                                                    }

                                                    $set_selected = ($operatore_to_check == $operatore['id'] ? "selected" : "");
                                                    if ($set_selected != "") {
                                                        $got_selected = true;
                                                    }
                                                }
                                                ?>
                                                <option value="<?= $operatore['id']; ?>" <?= $set_selected; ?>><?= $operatore['nome'] . ' ' . $operatore['cognome']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Tipo di Documento</label>
                                        <select id="tipo_documento" name="tipo_documento" class="form-control" <?= ($preload_doc_type != '' ? "disabled" : "") ?>>
                                            <option value="">Nessuno</option>
                                            <?php
                                            foreach((new \MSFramework\Fatturazione\vendite())->ottieniTipiDocumento() as $group => $docType) {
                                                ?>
                                                <optgroup label="<?= $group ?>">
                                                    <?php
                                                    foreach($docType as $docK => $docName) {
                                                        ?>
                                                        <option value="<?= $docK ?>" <?= ($preload_doc_type == $docK ? "selected" : "") ?>><?= $docName ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </optgroup>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="ibox" id="customer_info" style="margin-top: 15px; display: none;">
                                    <div class="ibox-content" style="background: #fafafa; padding: 30px;">

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h2 style="margin: 0 0 15px">Dati Cliente</h2>
                                                <div class="info_cliente">
                                                    <input name="id" type="hidden" value="">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Nome</label>
                                                            <input id="nome" name="nome" type="text" class="form-control required">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Cognome</label>
                                                            <input id="cognome" name="cognome" type="text" class="form-control">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Sesso</label>
                                                            <select id="sesso" name="sesso" class="form-control">
                                                                <option value="">Non specificato</option>
                                                                <option value="m">Maschio</option>
                                                                <option value="f">Femmina</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Data di Nascita</label>
                                                            <div class="input-group date scadenza">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="data_nascita" id="data_nascita">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Indirizzo</label>
                                                            <input id="indirizzo" name="indirizzo" type="text" class="form-control">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Città</label>
                                                            <input id="citta" name="citta" type="text" class="form-control">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Provincia</label>
                                                            <input id="provincia" name="provincia" type="text" class="form-control">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>CAP</label>
                                                            <input id="cap" name="cap" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label>Email</label>
                                                            <input id="email" name="email" type="text" class="form-control required">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label>Telefono Casa</label>
                                                            <input id="telefono_casa" name="telefono_casa" type="text" class="form-control">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label>Telefono Cellulare</label>
                                                            <input id="telefono_cellulare" name="telefono_cellulare" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <h2 style="margin: 0 0 15px">Dati Fatturazione</h2>
                                                <div class="info_fatturazione">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>Ragione Sociale</label>
                                                            <input id="ragione_sociale" placeholder="Se vuoto, saranno utilizzati nome e cognome" name="ragione_sociale" type="text" class="form-control fatturazione">
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <label>Partita IVA</label>
                                                            <input id="piva" name="piva" placeholder="Richiesto per Fatturazione" type="text" class="form-control fatturazione"">
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <label>Codice Fiscale</label>
                                                            <input id="cf" name="cf" type="text" placeholder="Richiesto se P.IVA mancante" class="form-control fatturazione">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>Indirizzo</label>
                                                            <input id="indirizzo_fatturazione" placeholder="Richiesto per Fatturazione" name="indirizzo" type="text" class="form-control fatturazione">
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <label>Città</label>
                                                            <input id="citta_fatturazione" placeholder="Richiesto per Fatturazione" name="citta" type="text" class="form-control fatturazione">
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <label>Provincia</label>
                                                            <input id="provincia_fatturazione" placeholder="Richiesto per Fatturazione" name="provincia" type="text" class="form-control fatturazione">
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <label>CAP</label>
                                                            <input id="cap_fatturazione" placeholder="Richiesto per Fatturazione" name="cap" type="text" class="form-control fatturazione">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="btn btn-primary pull-right" id="closeCustomer" style="margin-top: 15px; margin-left: 15px;">Salva</a>
                                        <a class="btn btn-success pull-right" id="saveCustomer" style="margin-top: 15px;">Salva & Aggiorna in Anagrafica</a>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>

                                <h2 class="title-divider">Articoli acquistati</h2>
                                <div class="table-responsive m-t">
                                    <table class="table table-bordered" id="tabella_articoli">
                                        <thead>
                                        <tr>
                                            <th>Nome Articolo</th>
                                            <th style="width: 100px;">Qta.</th>
                                            <th style="width: 150px;">Prezzo unitario (Senza IVA)</th>
                                            <th style="width: 150px;">Sconto</th>
                                            <th style="width: 100px;">Imposta</th>
                                            <th>Totale</th>
                                            <th>Totale Pagato</th>
                                            <th style="width: 50px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $preload_rows = array();
                                        if($_GET['event_id'] != "") {
                                            foreach ($preload_services as $service_to_preload) {
                                                $service_type = "";
                                                $tr_id = "";
                                                $service_name = "";
                                                $service_details = array();

                                                if ($service_to_preload != "") {
                                                    $service_details = (new \MSFramework\services())->getServiceDetails($service_to_preload, "nome, prezzo, extra_fields")[$service_to_preload];
                                                    $service_name = $MSFrameworki18n->getFieldValue($service_details['nome'], true, true);

                                                    $service_type = "servizio";
                                                    $tr_id = $service_type . "_" . $service_to_preload;
                                                }

                                                $preload_rows['id'] = $tr_id;
                                                $preload_rows['nome'] = $service_name;
                                                $preload_rows['qty'] = "1";
                                                $preload_rows['prezzo'] = ($service_details['prezzo_formattato']['no_tax'] != "" ? $service_details['prezzo_formattato']['no_tax'] : "0");
                                                $preload_rows['sconto'] = "0";
                                                $preload_rows['tipo_sconto'] = CURRENCY_SYMBOL;
                                                $preload_rows['imposta'] = ($service_details['extra_fields']['imposta'] != "" ? $service_details['extra_fields']['imposta'] : "");
                                                $preload_rows['prezzo_subtotal'] = ($service_details['prezzo_formattato']['cart_price'] != "" ? $service_details['prezzo_formattato']['cart_price'] : "0");
                                                $preload_rows['prezzo_pagato'] = ($service_details['prezzo_formattato']['cart_price'] != "" ? $service_details['prezzo_formattato']['cart_price'] : "0");
                                            }
                                        }

                                        if($_GET['id'] != "") {

                                            foreach ($preload_cart as $cartAryID => $cart_to_preload) {
                                                foreach($cart_to_preload as $cartK => $cartV) {
                                                    $preload_rows[$cartAryID][$cartK] = $cartV;
                                                }

                                                $preload_rows[$cartAryID]['composed_id'] = $cart_to_preload['type'] . "_" . $cart_to_preload['id'];
                                            }

                                        }

                                        if(!is_array($preload_rows) || count($preload_rows) == 0) {
                                            $preload_rows = array('');
                                        }

                                        foreach($preload_rows as $preloaded_row) {
                                        ?>
                                        <tr id="<?= $preloaded_row['composed_id'] ?>">
                                            <td width="30%">
                                                <input type="text" name="nome" class="form-control" value="<?= $preloaded_row['nome'] ?>">
                                            </td>
                                            <td>
                                                <input type="number" name="qty" class="form-control" value="<?= $preloaded_row['qty'] ?>" min="1">
                                            </td>
                                            <td width="10%">
                                                <div class="input-group">
                                                    <input type="text" name="prezzo" class="form-control" value="<?= (int)$preloaded_row['prezzo'] ?>">
                                                    <div class="input-group-append">
                                                        <span class="input-group-addon"><?= CURRENCY_SYMBOL; ?></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="padding-right: 70px; position:relative;">
                                                <input type="number" name="sconto" class="form-control" value="<?= $preloaded_row['sconto'] ?>">

                                                <select name="tipo_sconto" class="form-control" style="position: absolute; right: 10px; top: 8px; padding: 4px !important; width: 50px; min-width: auto;">
                                                    <option value="€" <?= ($preloaded_row['tipo_sconto'] == "€" ? "selected" : "")?>><?= CURRENCY_SYMBOL; ?></option>
                                                    <option value="%" <?= ($preloaded_row['tipo_sconto'] == "%" ? "selected" : "")?>>%</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="number" name="imposta" class="form-control" value="<?= $preloaded_row['imposta'] ?>">
                                                    <div class="input-group-append">
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td width="10%">
                                                <div class="input-group">
                                                    <input type="text" name="prezzo_subtotal" class="form-control" value="<?= $preloaded_row['prezzo_subtotal'] ?>">
                                                    <div class="input-group-append">
                                                        <span class="input-group-addon"><?= CURRENCY_SYMBOL; ?></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td width="10%">
                                                <div class="input-group">
                                                    <input type="text" name="prezzo_pagato" class="form-control" value="<?= $preloaded_row['prezzo_pagato'] ?>">
                                                    <div class="input-group-append">
                                                        <span class="input-group-addon"><?= CURRENCY_SYMBOL; ?></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td width="10%" align="center">
                                                <input type="hidden" name="no_tax_subtotal" value="">
                                                <input type="hidden" name="subtotal" value="">
                                                <input type="hidden" name="type" value="<?= $preloaded_row['type'] ?>">
                                                <input type="hidden" name="id" value="<?= $preloaded_row['id'] ?>">
                                                <input type="hidden" name="prezzo_iva_inclusa" value="">
                                                <input type="hidden" name="valore_imposta" value="">
                                                <input type="hidden" name="gift_to" value="<?= $preloaded_row['gift_to'] ?>">
                                                <a href="#" class="btn btn-danger btn-outline btn-sm remove_from_cart"><i class="fa fa-trash"></i></a>
                                                <a href="#" class="btn btn-info btn-outline btn-sm add_as_gift"><i class="fa fa-gift"></i></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                    <a href="#" class="btn btn-primary btn-sm btn-outline pull-right" id="add_new_custom_product" style="margin: 15px 0 0;"><i class="fa fa-plus"></i> Aggiungi articolo</a>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="tabs-container" id="articoli_e_servizi">
                        <ul class="nav nav-tabs" role="tablist">
                            <?php if($articoli_frequenti) { ?>
                                <li><a class="nav-link" data-toggle="tab" href="#articoli_frequenti">Frequenti</a></li>
                                <?php $tab_active = 1; ?>
                            <?php } ?>
                            <?php if($servizi) { ?>
                                <li><a class="nav-link" data-toggle="tab" href="#lista_servizi">Servizi</a></li>
                                <?php $tab_active = 1; ?>
                            <?php } ?>
                            <?php if($prodotti) { ?>
                                <li><a class="nav-link" data-toggle="tab" href="#lista_prodotti">Prodotti</a></li>
                                <?php $tab_active = 1; ?>
                            <?php } ?>
                            <?php if($pacchetti) { ?>
                                <li><a class="nav-link" data-toggle="tab" href="#lista_pacchetti">Pacchetti</a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php if($articoli_frequenti) { ?>
                                <div role="tabpanel" id="articoli_frequenti" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="row search-container">
                                            <?php foreach($articoli_frequenti as $a_f) {
                                                if($a_f['type'] == "prodotto") {
                                                    $dati_articolo_frequente = $prodotti[$a_f['id']];
                                                    $path_immagine = UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML;
                                                } else if($a_f['type'] == "servizio") {
                                                    $dati_articolo_frequente = $servizi[$a_f['id']];
                                                    $path_immagine = UPLOAD_SERVICES_FOR_DOMAIN_HTML;
                                                } else if($a_f['type'] == "pacchetto") {
                                                    $dati_articolo_frequente = $pacchetti[$a_f['id']];
                                                    $path_immagine = UPLOAD_SERVICES_FOR_DOMAIN_HTML;
                                                }
                                                ?>
                                                <div class="col-lg-4 col-sm-6">
                                                    <a class="btn btn-block btn-white add_to_cart" data-id="<?= $a_f['id']; ?>" data-type="<?= $a_f['type']; ?>" data-price="<?= (float)$a_f['prezzo']; ?>" data-imposta="<?= floatval($a_f['imposta']); ?>">
                                                        <img class="icon" src="<?= $path_immagine; ?>tn/<?= $dati_articolo_frequente['images'][0]; ?>">
                                                        <div class="details_container">
                                                            <span class="name"><?= $MSFrameworki18n->getFieldValue($a_f['nome']); ?></span></td>
                                                            <span class="category"><?= ucwords(str_replace("_", " ", $a_f['type'])); ?></span>
                                                            <span class="price"><?= number_format((float)$a_f['prezzo_iva_inclusa'], 2, ',', '.'); ?><?= CURRENCY_SYMBOL; ?></span>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($servizi) { ?>
                                <div role="tabpanel" id="lista_servizi" class="tab-pane <?=(!$articoli_frequenti ? 'active' : ''); ?>">
                                    <div class="panel-body">
                                        <input type="text" class="form-control filter_tables" placeholder="Cerca servizio..." style="margin-bottom: 10px;">
                                        <div class="row search-container">
                                            <?php foreach($servizi as $servizio) { ?>
                                                <div class="col-lg-4 col-sm-6">
                                                    <a class="btn btn-block btn-white add_to_cart" data-id="<?= $servizio['id']; ?>" data-type="servizio" data-price="<?= (float)$servizio['prezzo_formattato']['no_tax']; ?>" data-imposta="<?= floatval($servizio['imposta']); ?>">
                                                        <img class="icon" src="<?= UPLOAD_SERVICES_FOR_DOMAIN_HTML; ?>tn/<?= $servizio['images'][0]; ?>">
                                                        <div class="details_container">
                                                            <span class="name"><?= $MSFrameworki18n->getFieldValue($servizio['nome']); ?></span></td>
                                                            <span class="category"><?= $MSFrameworki18n->getFieldValue($servizio['category_name']); ?></span>
                                                            <span class="price"><?= number_format((float)$servizio['prezzo_formattato']['tax'], 2, ',', '.'); ?><?= CURRENCY_SYMBOL; ?></span>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($prodotti) { ?>
                                <div role="tabpanel" id="lista_prodotti" class="tab-pane">
                                    <div class="panel-body">
                                        <input type="text" class="form-control filter_tables" placeholder="Cerca prodotto..." style="margin-bottom: 10px;">
                                        <div class="row search-container">
                                            <?php foreach($prodotti as $prodotto) { ?>
                                                <div class="col-lg-4 col-sm-6">
                                                    <a class="btn btn-block btn-white add_to_cart" data-id="<?= $prodotto['id']; ?>" data-type="prodotto<?= ($prodotto["formattedAttributes"]['variations'] ? '_variabile' : ''); ?>" data-price="<?= (float)(count($prodotto['prezzo_promo']['no_tax']) ? $prodotto['prezzo_promo']['no_tax'][0] . '.' . $prodotto['prezzo_promo']['no_tax'][1] : $prodotto['prezzo']['no_tax'][0] . '.' . $prodotto['prezzo']['no_tax'][1]); ?>" data-imposta="<?= floatval($prodotto['imposta']); ?>">
                                                        <img class="icon" src="<?= UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML; ?>tn/<?= $prodotto['gallery'][0]; ?>">
                                                        <div class="details_container">
                                                            <span class="name"><?= $MSFrameworki18n->getFieldValue($prodotto['nome']); ?></span>
                                                            <span class="category"><?= $MSFrameworki18n->getFieldValue($prodotto['category_name']); ?></span>
                                                            <?php if($prodotto["formattedAttributes"]['variations']) { ?>
                                                                <span class="price">Più variazioni disponibili</span>
                                                            <?php } else { ?>
                                                                <span class="price"><?= (count($prodotto['prezzo_promo']['tax']) ? $prodotto['prezzo_promo']['tax'][0] . ',' . $prodotto['prezzo_promo']['tax'][1] : $prodotto['prezzo']['tax'][0] . ',' . $prodotto['prezzo']['tax'][1]); ?><?= CURRENCY_SYMBOL; ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($pacchetti) { ?>
                                <div role="tabpanel" id="lista_pacchetti" class="tab-pane">
                                    <div class="panel-body">
                                        <input type="text" class="form-control filter_tables" placeholder="Cerca pacchetto/offerta..." style="margin-bottom: 10px;">
                                        <div class="row search-container">
                                            <?php foreach($pacchetti as $pacchetto) { ?>
                                                <div class="col-lg-4 col-sm-6">
                                                    <a class="btn btn-block btn-white add_to_cart" data-id="<?= $pacchetto['id']; ?>" data-type="<?= $pacchetto['tipo']; ?>" data-price="<?= (float)$pacchetto['senza_iva']; ?>" data-method="<?= (strpos($pacchetto['valore'], '%') !== false ? '%' : CURRENCY_SYMBOL); ?>" data-imposta="<?= $pacchetto['imposta']; ?>">
                                                        <img class="icon" src="<?= UPLOAD_SERVICES_FOR_DOMAIN_HTML; ?>tn/<?= $prodotto['images'][0]; ?>">
                                                        <div class="details_container">
                                                            <span class="name"><?= $MSFrameworki18n->getFieldValue($pacchetto['nome']); ?></span>
                                                            <span class="category"><?= ucfirst($pacchetto['tipo']); ?></span>
                                                            <span class="price"><?= $pacchetto['valore']; ?></span>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">

                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Riepilogo Vendita</h5>
                        </div>
                        <div class="ibox-content">
                            <div id="customer_credits_box" style="display: <?= ($crediti_utente > 0 ? 'block' : 'none'); ?>;">
                                <div id="show_customer_credit_logs_modal" style="display: none;"></div>
                                <span class="credit_debit_title">Credito Cliente</span>  <span class="ms-label-tooltip m-l-sm" id="show_customer_credit_logs"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fare click qui per visualizzare uno storico dei movimenti crediti/debiti per questo cliente"></i></span>
                                <h3 class="text-navy" id="crediti_cliente">...</h3>
                                <input type="hidden" name="valore_credito" id="valore_credito_cliente" class="extra_documento">
                                <div class="hr-line-dashed"></div>
                                <div class="i-checks"><label style="font-weight: normal;"><input type="checkbox" value="" id="paga_tramite_crediti" name="usa_crediti" class="extra_documento" data-max="<?= (float)$crediti_utente; ?>"><i></i> <span class="checkbox_text"></span></label></div>
                                <div class="hr-line-dashed"></div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <span>Da pagare</span>
                                    <h2 class="font-bold" id="totale_resoconto">0<?= CURRENCY_SYMBOL; ?></h2>
                                </div>

                                <div class="col-md-6">
                                    <span>Pagato</span>
                                    <h2 class="font-bold" id="totale_pagato">0<?= CURRENCY_SYMBOL; ?></h2>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="credit_variation" style="display: none;"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="ibox" id="sedute_effettuate" style="display: none;">
                        <div class="ibox-title">
                            <h5>Sedute Effettuate</h5>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se il cliente ha speso una seduta acquistata (in questa operazione o in una delle precedenti) o ricevuta in regalo da altri clienti, è possibile stornarla da qui."></i></span>
                        </div>
                        <div class="ibox-content" style="padding-bottom: 50px;">

                        </div>
                    </div>

                    <div class="ibox" id="regali_utilizzati" style="display: none;">
                        <div class="ibox-title">
                            <h5>Regali Utilizzati</h5>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se il cliente ha utilizzato un regalo che aveva ricevuto, è possibile stornarlo da qui."></i></span>
                        </div>
                        <div class="ibox-content" style="padding-bottom: 50px;">

                        </div>
                    </div>

                    <div class="ibox" id="tracking_offline" style="display: none;">
                        <div class="ibox-title">
                            <h5>Tracking Vendita</h5> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="La vendita del cliente verrà tracciata su Google Analytics."></i></span>
                        </div>
                        <div class="ibox-content">

                        </div>
                    </div>

                    <div id="impostazioni_documento">
                        <?php
                        if($r['tipo_documento'] && in_array($r['tipo_documento'], array('scontrino', 'fattura', 'scontrino_non_fiscale', 'ricevuta'))) {
                            include('ajax/impostazioni/' . $r['tipo_documento'] . '.php');
                        }
                        else {
                            $r = array();
                            include('ajax/impostazioni/scontrino.php');
                        }
                        ?>
                    </div>

                    <textarea style="display: none;" id="json_impostazioni_documento" data-type="<?= $preload_doc_type ?>"><?= $preload_doc_info ?></textarea>

                </div>
            </div>

            <div class="modal inmodal fade" id="selectArticleModal" tabindex="-1" role="dialog"  aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Chiudi</span></button>
                            <h4 class="modal-title">Pacchetto Offerta</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="valore_sconto" value="">
                            <input type="hidden" id="metodo_sconto" value="">
                            <p style="text-align: center;">
                                <strong class="offer_name"></strong><br><br>
                                In quale articolo desideri applicare lo sconto di <b class="offer_value">0%</b>?
                            </p>

                            <div class="hr-line-dashed"></div>

                            <div class="offer_articles_list"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="selectVariationModal" tabindex="-1" role="dialog"  aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Chiudi</span></button>
                            <h4 class="modal-title">Seleziona Variazione</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="id_prodotto" value="">
                            <p style="text-align: center;">
                                Seleziona la variazione desiderata per <strong class="product_name"></strong>.
                            </p>

                            <div class="hr-line-dashed"></div>

                            <div class="variations_box"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                            <button type="button" class="btn btn-success add_variation">Aggiungi</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="assignGiftToUser" tabindex="-1" role="dialog" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Chiudi</span></button>
                            <h4 class="modal-title">Seleziona Cliente</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                Seleziona a quale cliente regalare il prodotto selezionato.
                            </p>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Cliente*</label>
                                    <select id="cliente_forgift" name="cliente_forgift" class="form-control chosen-select" data-placeholder="Cerca tra i clienti o aggiungine uno nuovo">
                                        <?php
                                        if (count($preload_cart) != 0) {
                                            foreach($preload_cart as $cur_cart) {
                                                $event_r_customer_gift = (new \MSFramework\customers())->getCustomerDataFromDB($cur_cart['gift_to'], 'id, nome, cognome, dati_fatturazione, email, telefono_cellulare, telefono_casa');
                                                $event_r_customer_gift['dati_fatturazione'] = json_decode($event_r_customer_gift['dati_fatturazione'], true);

                                                $customer_select_str = '<b>' . $event_r_customer_gift['nome'] . ' ' . $event_r_customer_gift['cognome'] . '</b>' . (!empty($event_r_customer_gift['dati_fatturazione']['ragione_sociale']) ? ' - ' . $event_r_customer_gift['dati_fatturazione']['ragione_sociale'] : '') . ($event_r_customer_gift['email'] ? ' (' . $event_r_customer_gift['email'] . ')' : '') . ($event_r_customer_gift['telefono_cellulare'] ? ' (' . $event_r_customer_gift['telefono_cellulare'] . ')' : '') . ($event_r_customer_gift['telefono_casa'] ? ' (' . $event_r_customer_gift['telefono_casa'] . ')' : '');
                                        ?>
                                        <option value="<?= $cur_cart['gift_to'] ?>" selected><?= $customer_select_str ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-lg-3">
                                    <label> &nbsp; </label>
                                    <a class="btn btn-block btn-white" id="editCustomerForGift" style="display: none;">Modifica </a>
                                </div>

                                <div class="col-lg-3">
                                    <label> &nbsp; </label>
                                    <a class="btn btn-block btn-danger" id="removeCustomerForGift" style="display: none;">Rimuovi Regalo </a>
                                </div>
                            </div>

                            <div id="customer_info_forgift" class="m-t-md" style="display: none;">

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                            <button type="button" class="btn btn-success save_gift">Salva Impostazioni Regalo</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
<script>
    
    globalInitForm();
</script>
</body>
</html>
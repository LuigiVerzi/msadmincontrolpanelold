<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$tipologia = $_POST['pType'];

if(in_array($tipologia, array('fiscali', 'non-fiscali'))) {

    $status = array();

    if($tipologia == 'fiscali') {
        $status[] = $MSFrameworkDatabase->deleteRow("fatturazione_vendite", "tipo_documento", "fattura");
        $status[] = $MSFrameworkDatabase->deleteRow("fatturazione_vendite", "tipo_documento", "scontrino");
    }
    else {
        $status[] = $MSFrameworkDatabase->deleteRow("fatturazione_vendite", "tipo_documento", "ricevuta");
        $status[] = $MSFrameworkDatabase->deleteRow("fatturazione_vendite", "tipo_documento", "preventivo");
        $status[] = $MSFrameworkDatabase->deleteRow("fatturazione_vendite", "tipo_documento", "scontrino_non_fiscale");
        $status[] = $MSFrameworkDatabase->deleteRow("fatturazione_vendite", "tipo_documento", null);
    }

    if (in_array(true, $status)) {
        die('ok');
    } else {
        die('error');
    }
}
else
{
    die('error');
}
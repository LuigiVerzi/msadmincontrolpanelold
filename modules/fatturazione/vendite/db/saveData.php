<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$services_class = new \MSFramework\services();
$customers_class = new \MSFramework\customers();
$ecommerceProducts = new \MSFramework\Ecommerce\products();
$seduteBeautyCenter = new \MSFramework\BeautyCenter\sedute();

$extra_functions_db = json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true);
if (in_array("beautycenter", $extra_functions_db)) {
    $offers_class = new \MSFramework\BeautyCenter\offers();
} else if (in_array("hotel", $extra_functions_db)) {
    $offers_class = new \MSFramework\Hotels\offers();
} else if (in_array("camping", $extra_functions_db)) {
    $offers_class = new \MSFramework\Camping\offers();
}

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pCliente'] == "" || (!count($_POST['pCarrello']) && count($_POST['pSeduteUtilizzate']) == 0 && count($_POST['pRegaliUtilizzati']) == 0 && $_POST['pID'] == "")) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if($db_action == 'update') {
    if(in_array($_POST['pDocumento'], array('scontrino', 'scontrino_non_fiscale'))) {
        die();
    }

    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT carrello, sedute_utilizzate, regali_utilizzati FROM fatturazione_vendite WHERE id = :id", array(":id" => $_POST['pID']), true);

    $r_old_carrello = json_decode($r_old_data['carrello'], true);
    $old_carrello_by_id = array();
    $old_gifts = array();
    foreach($r_old_carrello as $old_c) {
        $old_carrello_by_id[$old_c['type'] . "_" . $old_c['id']] = $old_c;

        if($old_c['gift_to'] != "") {
            if($old_c['type'] == 'prodotto') {
                $warehouse_status = $ecommerceProducts->getWarehouseStatus($old_c['id']);
                $product_details = $ecommerceProducts->getProductDetails($old_c['id'])[$old_c['id']];

                $ref = $product_details;
            } else if($old_c['type'] == 'prodotto_variabile') {
                $variation_ary = $ecommerceProducts->convertProductVariationStringToArray($old_c['id']);
                $product_details = $ecommerceProducts->getProductVariationInfo($variation_ary['product_id'], $variation_ary['variation']);

                $ref = $product_details;
            } else if($old_c['type'] == 'servizio') {
                $info_servizio = $services_class->getServiceDetails($old_c['id'])[$old_c['id']];
                $ref = $info_servizio;
            } else if($old_c['type'] == 'pacchetto') {
                $info_pacchetto = $offers_class->getOfferDetails($old_c['id'])[$old_c['id']];
                $ref = $info_pacchetto;
            }

            $old_gifts[$old_c['gift_to']][] = array('id' => $old_c['type'] . "_" . $old_c['id'], "qty" => $old_c['qty'], "ref" => $ref);
        }
    }

    $r_old_sedute_utilizzate = json_decode($r_old_data['sedute_utilizzate'], true);
    $old_sedute_utilizzate_by_id = array();
    foreach($r_old_sedute_utilizzate as $old_c) {
        $old_sedute_utilizzate_by_id[$old_c['id']] = $old_c;
    }

    $r_old_regali_utilizzati = json_decode($r_old_data['regali_utilizzati'], true);
    $old_regali_utilizzati_by_id = array();
    foreach($r_old_regali_utilizzati as $old_c) {
        $old_regali_utilizzati_by_id[$old_c['id']] = $old_c;
    }
}

$customer_info = $customers_class->getCustomerDataFromDB($_POST['pCliente'], "nome, cognome");

//VERIFICO LA DISPONIBILITA' IN MAGAZZINO PER I PRODOTTI INSERITI NEL CARRELLO E SALVO I DATI PER UN EVENTUALE APPLICAZIONE DI CREDITI/DEBITI IN BASE A QUANTO DOVUTO/PAGATO
$total = 0;
$total_paid = 0;
$products_oversell_array = array();
$gifts = array();

foreach($_POST['pCarrello'] as $articolo) {
    $cur_qty = $articolo['qty'];

    if($articolo['type'] == 'prodotto') {
        $warehouse_status = $ecommerceProducts->getWarehouseStatus($articolo['id']);
        $product_details = $ecommerceProducts->getProductDetails($articolo['id'])[$articolo['id']];
        if($warehouse_status != "unlimited" && $articolo['qty'] > $warehouse_status) {
            $products_oversell_array[] = array("name" => $articolo['nome'], "max" => $warehouse_status);
        }

        $ref = $product_details;
    } else if($articolo['type'] == 'prodotto_variabile') {
        $variation_ary = $ecommerceProducts->convertProductVariationStringToArray($articolo['id']);
        $warehouse_status = $ecommerceProducts->getWarehouseStatus($variation_ary);

        $product_details = $ecommerceProducts->getProductVariationInfo($variation_ary['product_id'], $variation_ary['variation']);

        if($warehouse_status != "unlimited" && $articolo['qty'] > $warehouse_status) {
            $products_oversell_array[] = array("name" => $articolo['nome'], "max" => $warehouse_status);
        }

        $ref = $product_details;
    } else if($articolo['type'] == 'servizio') {
        $info_servizio = $services_class->getServiceDetails($articolo['id'])[$articolo['id']];
        /*if($info_servizio && isset($info_servizio['extra_fields']['sessioni']) && intval($info_servizio['extra_fields']['sessioni']) > 1) {
            $cur_qty = ($info_servizio['extra_fields']['sessioni']*$articolo['qty']);
        }*/

        $ref = $info_servizio;
    } else if($articolo['type'] == 'pacchetto') {
        $info_pacchetto = $offers_class->getOfferDetails($articolo['id'])[$articolo['id']];
        /*if($info_pacchetto && isset($info_pacchetto['extra_fields']['sedute']) && intval($info_pacchetto['extra_fields']['sedute']) > 0) {
            $cur_qty = ($info_pacchetto['extra_fields']['sedute']*$articolo['qty']);
        }*/

        $ref = $info_pacchetto;
    }

    $total += $articolo['prezzo_subtotal'];
    $total_paid += $articolo['prezzo_pagato'];

    if($articolo['gift_to'] != "") {
        $gifts[$articolo['gift_to']][] = array('id' => $articolo['type'] . "_" . $articolo['id'], "qty" => $cur_qty, "ref" => $ref);
    }
}

if(count($products_oversell_array) > 0) {
    echo json_encode(array("status" => "products_oversell", "products_oversell_details" => $products_oversell_array));
    die();
}

if($_POST['pJustCheckValid'] == "1") {
    echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
    die();
}

$array_to_save = array(
    "cliente" => $_POST['pCliente'],
    "dati_cliente" => json_encode($_POST['pDatiCliente']),
    "operatore" => $_POST['pOperatore'],
    "tipo_documento" => $_POST['pDocumento'],
    "info_documento" => json_encode($_POST['pInfoDocumento']),
    "carrello" => json_encode($_POST['pCarrello']),
    "stato" => $_POST['pActive'],
    "sedute_utilizzate" => json_encode($_POST['pSeduteUtilizzate']),
    "regali_utilizzati" => json_encode($_POST['pRegaliUtilizzati']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO fatturazione_vendite ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    $inserted_id = $MSFrameworkDatabase->lastInsertId();
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE fatturazione_vendite SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
    $inserted_id = $_POST['pID'];
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

$extra_status = array();

$tag_link_to_op = "{RIF_OP=" .$inserted_id . "}";

// Controlla gli articoli acquisti per assegnare eventuali crediti / sedute etc...
$cur_done_carrello = array();
foreach($_POST['pCarrello'] as $articolo) {
    $art_id = $articolo['type'] . "_" . $articolo['id'];

    $assign_to_user = ($articolo['gift_to'] == "" ? $_POST['pCliente'] : $articolo['gift_to']);
    $notes_buy_or_gift = ($articolo['gift_to'] == "" ? "Acquisto" : "Ricevuto regalo");
    $status_type = ($articolo['gift_to'] == "" ? "buy" : "gift");
    $warehouse_action = "subtract";
    if(array_key_exists($art_id, $old_carrello_by_id)) {
        $articolo['qty'] = $articolo['qty']-$old_carrello_by_id[$art_id]['qty'];
        if($articolo['qty'] == 0) {
            continue;
        }

        $notes_buy_or_gift = "Modifica manuale fattura";
        $status_type = "buy";
        $warehouse_action = ($articolo['qty'] < 0 ? "sum" : "subtract");
    }

    $cur_done_carrello[$art_id] = $articolo;

    if($articolo['type'] == 'servizio') {
        $info_servizio = $services_class->getServiceDetails($articolo['id'])[$articolo['id']];
        if($info_servizio) {

            if(isset($info_servizio['extra_fields']['crediti']) && intval($info_servizio['extra_fields']['crediti']) > 0) {
                $extra_status[] = array(
                    'status' => $customers_class->addNewOperation($_POST['pCliente'], ($info_servizio['extra_fields']['crediti'] * $articolo['qty']), ($articolo['qty'] > 1 ? $articolo['qty'] .'*' : '') . $notes_buy_or_gift . ' ' . $MSFrameworki18n->getFieldValue($info_servizio['nome']) . ' ' . $tag_link_to_op),
                    'nome' => $MSFrameworki18n->getFieldValue($info_servizio['nome']),
                    'type' => $status_type
                );
            }
            if(isset($info_servizio['extra_fields']['sessioni']) && intval($info_servizio['extra_fields']['sessioni']) > 1) {
                $extra_status[] = array(
                    'status' => $seduteBeautyCenter->updateSessionsQuantity($assign_to_user, ($info_servizio['extra_fields']['sessioni']*$articolo['qty']), 'service', $info_servizio, ($articolo['qty'] > 1 ? $articolo['qty'] .'*' : '') . $notes_buy_or_gift . ' ' . $MSFrameworki18n->getFieldValue($info_servizio['nome']) . ' ' . $tag_link_to_op),
                    'nome' => $MSFrameworki18n->getFieldValue($info_servizio['nome']),
                    'type' => $status_type
                );
            }
        }
    }
    else if($articolo['type'] == 'pacchetto') {
        $info_pacchetto = $offers_class->getOfferDetails($articolo['id'])[$articolo['id']];
        if($info_pacchetto && isset($info_pacchetto['extra_fields']['sedute']) && intval($info_pacchetto['extra_fields']['sedute']) > 0) {
            $extra_status[] = array(
                'status' => $seduteBeautyCenter->updateSessionsQuantity($assign_to_user, ($info_pacchetto['extra_fields']['sedute']*$articolo['qty']), 'offer', $info_pacchetto, ($articolo['qty'] > 1 ? $articolo['qty'] .'*' : '') . $notes_buy_or_gift . ' ' . $MSFrameworki18n->getFieldValue($info_pacchetto['nome']) . ' ' . $tag_link_to_op),
                'nome' => $MSFrameworki18n->getFieldValue($info_pacchetto['nome']),
                'type' => $status_type
            );
        }
    } else if($articolo['type'] == 'prodotto') {
        $ecommerceProducts->updateWarehouseQty($articolo['id'], $articolo['qty'], $warehouse_action);
    } else if($articolo['type'] == 'prodotto_variabile') {
        $ecommerceProducts->updateWarehouseQty($ecommerceProducts->convertProductVariationStringToArray($articolo['id']), $articolo['qty'], $warehouse_action);
    }
}

// Aggiungo/Rimuovo crediti al cliente in caso di utilizzo
if($_POST['pInfoDocumento']['usa_crediti'] == 1) {
    $customers_class->addNewOperation($_POST['pCliente'], -$_POST['pInfoDocumento']['valore_credito'], ($_POST['pInfoDocumento']['valore_credito'] > 0 ? 'Pagamento tramite crediti' : 'Debito saldato') . ' ' . $tag_link_to_op);
}

//Se il cliente ha pagato di più/di meno di quanto dovuto, aggiungo il credito/debito
if($total_paid != $total) {
    $customers_class->addNewOperation($_POST['pCliente'], $total_paid-$total, ($total_paid-$total > 0 ? 'Compenso pagamento extra' : 'Debito per pagamento inferiore') . ' ' . $tag_link_to_op);
}

// Scalo le sedute utilizzate
$cur_done_sedute = array();
foreach($_POST['pSeduteUtilizzate'] as $seduta) {
    $notes_seduta = "Utilizzo";
    $notes_regalo = "Utilizzo regalo";
    if(array_key_exists($seduta['id'], $old_sedute_utilizzate_by_id)) {
        $seduta['qty'] = $seduta['qty']-$old_sedute_utilizzate_by_id[$seduta['id']]['qty'];
        if($seduta['qty'] == 0) {
            continue;
        }

        $notes_seduta = "Modifica manuale fattura";
        $notes_regalo = "Modifica manuale fattura";
    }

    $cur_done_sedute[$art_id] = $seduta;

    if($seduta['type'] == "service") {
        $info_servizio = $services_class->getServiceDetails($seduta['clean-id'])[$seduta['clean-id']];
        $gift_type = "servizio";
    } else if($seduta['type'] == "offer") {
        $info_servizio = $offers_class->getOfferDetails($seduta['clean-id'])[$seduta['clean-id']];
        $gift_type = "pacchetto";
    }

    $seduteBeautyCenter->updateSessionsQuantity($_POST['pCliente'], -$seduta['qty'], $seduta['type'], $info_servizio, ($seduta['qty'] > 1 ? $seduta['qty'] .'*' : '') . $notes_seduta . ' ' . $MSFrameworki18n->getFieldValue($info_servizio['nome']) . ' ' . $tag_link_to_op);

    if($customers_class->giftsLeft($_POST['pCliente'], $gift_type . "_" . $seduta['clean-id']) > 0) {
        $customers_class->addNewGiftOperation($_POST['pCliente'], '', $gift_type . "_" . $seduta['clean-id'], -$seduta['qty'], $info_servizio, $notes_regalo . ' ' . $tag_link_to_op);
    }
}

// Scalo i regali utilizzati
$cur_done_regali_utilizzati = array();
foreach($_POST['pRegaliUtilizzati'] as $gift) {
    $notes_regalo = "Utilizzo regalo";
    if(array_key_exists($gift['id'], $old_regali_utilizzati_by_id)) {
        $gift['qty'] = $gift['qty']-$old_regali_utilizzati_by_id[$gift['id']]['qty'];
        if($gift['qty'] == 0) {
            continue;
        }

        $notes_regalo = "Modifica manuale fattura";
    }

    $expl_id = explode("_", $gift['id']);

    $cur_done_regali_utilizzati[$art_id] = $gift;

    if($expl_id[0] == 'prodotto' && $expl_id[1] == 'variabile') {
        $variation_ary = $ecommerceProducts->convertProductVariationStringToArray($expl_id[2]);
        $ref = $ecommerceProducts->getProductVariationInfo($variation_ary['product_id'], $variation_ary['variation']);
    } else if($expl_id[0] == 'prodotto') {
        $ref = $ecommerceProducts->getProductDetails($expl_id[1])[$expl_id[1]];
    }

    $customers_class->addNewGiftOperation($_POST['pCliente'], '', $gift['id'], -$gift['qty'], $ref, $notes_regalo . ' ' . $tag_link_to_op);
}


//Aggiungo i regali
$cur_done_regali = array();
foreach($gifts as $giftK => $giftAry) {
    foreach($giftAry as $gift) {
        $notes_regalo = "Ricevuto regalo da";
        if(array_key_exists($giftK, $old_gifts)) {
            $cur_old_gifts = $old_gifts[$giftK];

            $id_col = array_search($gift['id'], array_column($cur_old_gifts, 'id'));
            if($id_col) {
                $gift['qty'] = $gift['qty']-$cur_old_gifts[$id_col]['qty'];
                $notes_regalo = "Modifica manuale fattura";
            }
        }

        if($gift['qty'] != 0) {
            $customers_class->addNewGiftOperation($giftK, $_POST['pCliente'], $gift['id'], $gift['qty'], $gift['ref'], $notes_regalo . ' ' . $customer_info['nome'] . " " . $customer_info['cognome'] . " " . $tag_link_to_op);
        }

        $cur_done_regali[$giftK][] = $gift;
    }
}

//Gestisco i record che non sono più presenti/impostati su 0 (es: eliminazione della riga di un prodotto dalla fattura)
$notes_buy_or_gift = "Modifica manuale fattura";
foreach(array_diff_key($old_carrello_by_id, $cur_done_carrello) as $lostItem) {
    $assign_to_user = ($lostItem['gift_to'] == "" ? $_POST['pCliente'] : $lostItem['gift_to']);

    if($lostItem['type'] == 'servizio') {
        $info_servizio = $services_class->getServiceDetails($lostItem['id'])[$lostItem['id']];
        if($info_servizio) {
            if(isset($info_servizio['extra_fields']['crediti']) && intval($info_servizio['extra_fields']['crediti']) > 0) {
                $customers_class->addNewOperation($_POST['pCliente'], ($info_servizio['extra_fields']['crediti'] * -$lostItem['qty']), (-$lostItem['qty'] > 1 ? -$lostItem['qty'] .'*' : '') . $notes_buy_or_gift . ' ' . $MSFrameworki18n->getFieldValue($info_servizio['nome']) . ' ' . $tag_link_to_op);
            }

            if(isset($info_servizio['extra_fields']['sessioni']) && intval($info_servizio['extra_fields']['sessioni']) > 1) {
                $seduteBeautyCenter->updateSessionsQuantity($assign_to_user, ($info_servizio['extra_fields']['sessioni']*-$lostItem['qty']), 'service', $info_servizio, (-$lostItem['qty'] > 1 ? -$lostItem['qty'] .'*' : '') . $notes_buy_or_gift . ' ' . $MSFrameworki18n->getFieldValue($info_servizio['nome']) . ' ' . $tag_link_to_op);
            }
        }
    }
    else if($lostItem['type'] == 'pacchetto') {
        $info_pacchetto = $offers_class->getOfferDetails($lostItem['id'])[$lostItem['id']];
        if($info_pacchetto && isset($info_pacchetto['extra_fields']['sedute']) && intval($info_pacchetto['extra_fields']['sedute']) > 0) {
            $seduteBeautyCenter->updateSessionsQuantity($assign_to_user, ($info_pacchetto['extra_fields']['sedute']*-$lostItem['qty']), 'offer', $info_pacchetto, (-$lostItem['qty'] > 1 ? -$lostItem['qty'] .'*' : '') . $notes_buy_or_gift . ' ' . $MSFrameworki18n->getFieldValue($info_pacchetto['nome']) . ' ' . $tag_link_to_op);
        }
    } else if($lostItem['type'] == 'prodotto') {
        $ecommerceProducts->updateWarehouseQty($lostItem['id'], $lostItem['qty'], $warehouse_action);
    } else if($lostItem['type'] == 'prodotto_variabile') {
        $ecommerceProducts->updateWarehouseQty($ecommerceProducts->convertProductVariationStringToArray($lostItem['id']), $lostItem['qty'], $warehouse_action);
    }
}

foreach(array_diff_key($old_sedute_utilizzate_by_id, $cur_done_sedute) as $lostItem) {
    if($lostItem['type'] == "service") {
        $info_servizio = $services_class->getServiceDetails($lostItem['clean-id'])[$lostItem['clean-id']];
        $gift_type = "servizio";
    } else if($lostItem['type'] == "offer") {
        $info_servizio = $offers_class->getOfferDetails($lostItem['clean-id'])[$lostItem['clean-id']];
        $gift_type = "pacchetto";
    }

    $seduteBeautyCenter->updateSessionsQuantity($_POST['pCliente'], $lostItem['qty'], $lostItem['type'], $info_servizio, ($lostItem['qty'] > 1 ? $lostItem['qty'] .'*' : '') . $notes_buy_or_gift . ' ' . $MSFrameworki18n->getFieldValue($info_servizio['nome']) . ' ' . $tag_link_to_op);
}

foreach(array_diff_key($old_regali_utilizzati_by_id, $cur_done_regali_utilizzati) as $lostItem) {
    $expl_id = explode("_", $lostItem['id']);

    $cur_done_regali_utilizzati[$art_id] = $lostItem;

    if($expl_id[0] == 'prodotto' && $expl_id[1] == 'variabile') {
        $variation_ary = $ecommerceProducts->convertProductVariationStringToArray($expl_id[2]);
        $ref = $ecommerceProducts->getProductVariationInfo($variation_ary['product_id'], $variation_ary['variation']);
    } else if($expl_id[0] == 'prodotto') {
        $ref = $ecommerceProducts->getProductDetails($expl_id[1])[$expl_id[1]];
    }

    $customers_class->addNewGiftOperation($_POST['pCliente'], '', $lostItem['id'], $lostItem['qty'], $ref, $notes_buy_or_gift . ' ' . $tag_link_to_op);
}

$old_gifts_flat_ary = array();
foreach($old_gifts as $customerID => $ary) {
    foreach($ary as $data) {
        $old_gifts_flat_ary[$customerID . "|" . $data['id']] = $data;
    }
}

$cur_done_regali_flat_ary = array();
foreach($cur_done_regali as $customerID => $ary) {
    foreach($ary as $data) {
        $cur_done_regali_flat_ary[$customerID . "|" . $data['id']] = $data;
    }
}

foreach(array_diff_key($old_gifts_flat_ary, $cur_done_regali_flat_ary) as $lostItemK => $lostItem) {
    $itemAry = explode("|", $lostItemK);
    $customerID = $itemAry[0];
    $giftID = $itemAry[1];

    $customers_class->addNewGiftOperation($customerID, '', $lostItem['id'], -$lostItem['qty'], $lostItem['ref'], $notes_buy_or_gift . ' ' . $customer_info['nome'] . " " . $customer_info['cognome'] . " " . $tag_link_to_op);
}

if($db_action === 'insert') {
    (new \MSFramework\tracking())->trackPurchase($inserted_id);
}

echo json_encode(array("status" => "ok", "id" => $inserted_id, "extra_status" => $extra_status));
die();
?>
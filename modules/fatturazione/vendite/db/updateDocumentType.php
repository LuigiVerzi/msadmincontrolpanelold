<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$document_change_types = array(
    'ricevuta' => array('fattura', 'preventivo'),
    'fattura' => array('ricevuta', 'preventivo'),
    'preventivo' => array('fattura', 'ricevuta')
);

$target = $_POST['pTarget'];

$r = $MSFrameworkDatabase->getAssoc("SELECT * FROM fatturazione_vendite WHERE id = :id", array(":id" => $_POST['pID']), true);;

if(!$r) {
    echo "mandatory_data_missing";
    die();
}

if(!in_array($target, $document_change_types[$r['tipo_documento']])) {
    echo "invalid_target";
    die();
}

$nextDocumentNumber = (new \MSFramework\Fatturazione\fatture())->getNextDocumentNumber($target);

$info_documento = json_decode($r['info_documento'], true);
$info_documento['numero'] = $nextDocumentNumber;

/* GENERA IL NUOVO DOCUMENTO */
$impostazioni_fatture = $MSFrameworkCMS->getCMSData('fatturazione_fatture');
$info_azienda = $MSFrameworkCMS->getCMSData('site');

$logo_url = '';
if($impostazioni_fatture['logo_fattura'] && json_decode($impostazioni_fatture['logo_fattura'])) {
    if(json_decode($impostazioni_fatture['logo_fattura'])[0]) {
        $logo_url = UPLOAD_FATTURAZIONE_FATTURE_FOR_DOMAIN . json_decode($impostazioni_fatture['logo_fattura'])[0];
    }
}

if($logo_url == '') {
    $logo = json_decode($MSFrameworkCMS->getCMSData('site')["logos"], true)['logo'];
    $logo_url = UPLOAD_LOGOS_FOR_DOMAIN . $logo;
}

$cliente = $r['cliente'];
$info_cliente = json_decode($r['dati_cliente'], true);
$info_carrello = json_decode($r['carrello'], true);
$sedute_utilizzate = (int)$r['sedute_utilizzate'];
$crediti_cliente = (new \MSFramework\customers())->getCustomerBalance($cliente);
$anno_fattura = DateTime::createFromFormat('d/m/Y', $info_documento['data']);
if(!$anno_fattura) {
    $anno_fattura = new DateTime(date('Y-m-d H:i'));
}

/* PREPARO I DATI DA MOSTRARE */
$info_indirizzo_cliente = array(
    $info_cliente['fatturazione']['indirizzo'],
    $info_cliente['fatturazione']['citta'],
    $info_cliente['fatturazione']['provincia'],
    $info_cliente['fatturazione']['cap']
);

$info_indirizzo_cliente = array_filter($info_indirizzo_cliente);

$dati_cliente = array(
    (!empty($info_cliente['fatturazione']['ragione_sociale']) ? $info_cliente['fatturazione']['ragione_sociale'] : $info_cliente['nome'] . ' ' . $info_cliente['cognome']),
    (!empty($info_cliente['fatturazione']['piva']) ? $info_cliente['fatturazione']['piva'] : $info_cliente['fatturazione']['cf']),
    $info_indirizzo_cliente,
    $info_cliente['telefono_casa']
);

$dati_cliente = array_filter($dati_cliente);

ob_start();
include('../ajax/stampa/' . $target . '/pdf/index.php');
$info_documento['source'] = ob_get_clean();
ob_flush();


$MSFrameworkDatabase->query("UPDATE fatturazione_vendite SET tipo_documento = :target, info_documento = :info_documento WHERE id = :id", array(":id" => $_POST['pID'], ':target' => $target, ':info_documento' => json_encode($info_documento)));

echo "ok";
die();
?>
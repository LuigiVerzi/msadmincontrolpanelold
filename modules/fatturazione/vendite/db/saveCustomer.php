<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pEmail'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$mail_conflict = false;
if($MSFrameworkDatabase->getCount("SELECT email FROM customers WHERE email = :mail AND id != :id", array(":mail" => $_POST['pEmail'], ":id" => $_POST['pID'])) != 0) {
    $mail_conflict = true;
}

if(!filter_var($_POST['pEmail'], FILTER_VALIDATE_EMAIL)) {
    $mail_conflict = true;
}

if($mail_conflict) {
    echo json_encode(array("status" => "mail_not_valid"));
    die();
}


$ts_scadenza = "";
if($_POST['pDataNascita'] != "") {
    $ary_scadenza = explode("/", $_POST['pDataNascita']);
    $ts_scadenza = mktime(23, 59, 59, $ary_scadenza[1], $ary_scadenza[0], $ary_scadenza[2]);
    if(!$ts_scadenza) {
        $ts_scadenza = '';
    }
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "cognome" => $_POST['pCognome'],
    "email" => $_POST['pEmail'],
    "data_nascita" => $ts_scadenza,
    "telefono_casa" => $_POST['pTelefonoCasa'],
    "telefono_cellulare" => $_POST['pTelefonoCellulare'],
    "indirizzo" => $_POST['pIndirizzo'],
    "citta" => $_POST['pCitta'],
    "provincia" => $_POST['pProvincia'],
    "cap" => $_POST['pCAP'],
    "provenienza" => $_POST['pProvenienza'],
    "sesso" => $_POST['pSesso'],
    "dati_fatturazione" => json_encode($_POST['pFatturazione']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO customers ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE customers SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>

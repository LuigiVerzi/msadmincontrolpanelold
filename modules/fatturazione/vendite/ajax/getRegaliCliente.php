<?php
/**
 * MSAdminControlPanel
 * Date: 31/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$id = (int)$_POST['id'];
$products_class = new \MSFramework\Ecommerce\products();
$invoices_class = new \MSFramework\Fatturazione\fatture();

$doc_details = ($_POST['pRecordID'] != "" ? json_decode($invoices_class->getDocumentDetails($_POST['pRecordID'], 'regali_utilizzati')[$_POST['pRecordID']]['regali_utilizzati'], true) : array());

$old_regali_by_id = array();
foreach($doc_details as $old_regalo) {
    $old_regali_by_id[$old_regalo['id']] = $old_regalo;
}

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM customers_gift WHERE gift_id NOT LIKE 'servizio_%' AND gift_id NOT LIKE 'pacchetto_%'") as $r) {
    $prod_id = str_replace(array("prodotto_variabile_", "prodotto_"), array(), $r['gift_id']);
    $expl_id = explode("_", $r['gift_id']);

    if($expl_id[0] == "prodotto" && $expl_id[1] == "variabile") {
        $variation_ary = $products_class->convertProductVariationStringToArray($prod_id);
        $nome = $products_class->getProductVariationInfo($variation_ary['product_id'], $variation_ary['variation'])['nome'];
    } else if($expl_id[0] == "prodotto") {
        $nome = $products_class->getProductDetails($prod_id, 'nome')[$prod_id]['nome'];
    }

    $bilancio_regali[$r['gift_id']] = array(
        "sedute" => $bilancio_regali[$r['gift_id']]['sedute']+$r['qty'],
        "ref" => array(
            "clean_id" => $prod_id,
            "id" => $r['gift_id'],
            "nome" => $nome,
        )
    );
}

foreach($old_regali_by_id as $regaloID => $regalo) {
    if(!array_key_exists($regaloID, $bilancio_regali)) {
        $bilancio_regali[$regaloID] = array(
            "sedute" => $regalo['qty'],
            "ref" => array(
                "clean_id" => $regalo['clean-id'],
                "id" => $regaloID,
                "nome" => $regalo['name'],
            )
        );

    } else {
        $bilancio_regali[$regaloID]['sedute'] += $regalo['qty'];
    }
}

if ($bilancio_regali) {
    ?>
    <table id="main_table_list" class="balance_table display table table-striped table-bordered table-hover"
           width="100%" cellspacing="0" style="margin-bottom: -30px;">
        <tbody>
        <?php
        foreach ($bilancio_regali as $b) {
            if ($b['sedute'] != 0) {
                $info_prodotto = $b['ref'];
                ?>
                <tr>
                    <td width="70%">
                        <span
                            class="product_name"><?= $MSFrameworki18n->getFieldValue($info_prodotto['nome']) ?></span>
                    </td>
                    <td width="30%">
                        <select class="form-control utilizzo_regalo"
                                data-id-prodotto="<?php echo $info_prodotto['id'] ?>"
                                data-clean-id-prodotto="<?php echo $info_prodotto['clean_id'] ?>"
                                id="utilizzo_<?php echo $info_prodotto['id'] ?>">
                            <?php
                            for ($x = 0; $x <= $b['sedute']; $x++) {
                                $selected = "";
                                if($_POST['pRecordID'] != "") {
                                    $selected = (array_key_exists($info_prodotto['id'], $old_regali_by_id) && $x == $old_regali_by_id[$info_prodotto['id']]['qty'] ? "selected" : "");
                                }
                                ?>
                                <option value="<?= $x ?>" <?= $selected ?>><?= $x ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
    <?php
}

$customer['regali_disponibili'] = ob_get_contents();
ob_clean();
die(json_encode($customer));
?>

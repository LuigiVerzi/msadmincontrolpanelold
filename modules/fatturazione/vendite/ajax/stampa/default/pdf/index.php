<?php
/**
 * MSAdminControlPanel
 * Date: 21/12/2018
 */

$pdf = new \MSFramework\Fatturazione\Output\PDF\pdf($info_documento['oggetto']);
$table = new \Interpid\PdfLib\Table($pdf);

$default_font_size = 11;

$pdf->AddPage();
if(is_file($logo_url)) {
    $pdf->Image($logo_url, 15, 10, 0, 10);
}

//set font to arial, bold, 14pt
$pdf->SetFont($pdf->headerFont,'B', $default_font_size);

$pdf->Cell(130	,5, '',0,1);

//Cell(width , height , text , border , end line , [align] )

$pdf->Cell(130	,5, $info_azienda['rag_soc'],0,0);
$pdf->Cell(59	,5,'Documento non fiscale',0,1);//end of line

//set font to arial, regular, 12pt
$pdf->SetFont($pdf->headerFont,'', $default_font_size);

$pdf->Cell(130	,5, $MSFrameworkCMS->getFullAddress(),0,0);

$pdf->Cell(130	,5,"",0,0);


$pdf->SetFont($pdf->headerFont,'', $default_font_size);
$pdf->Cell(25	,5, 'Data: ' . $info_documento['data'],0,1);
$pdf->SetFont($pdf->headerFont,'', $default_font_size);

$pdf->Cell(130	,5,'P.IVA:  ' . $info_azienda['piva'],0,1);

$pdf->Cell(130	,5,'Telefono: ' . (!empty($info_azienda['telefono']) ? $info_azienda['telefono'] : $info_azienda['cellulare']),0,1);

if(!empty($info_azienda['fax'])) {
    $pdf->Cell(130, 5, 'Fax: ' . $info_azienda['fax'] . '', 0, 1);
}
//make a dummy empty cell as a vertical spacer
$pdf->Cell(189	,10,'',0,1);//end of line

//billing address
$pdf->SetFont($pdf->headerFont,'B', $default_font_size);
$pdf->Cell(100	,5,'Cliente:',0,1);//end of line
$pdf->SetFont($pdf->headerFont,'', $default_font_size);

foreach($dati_cliente as $riga) {
    if(is_array($riga)) {
        $pdf->Cell(90, 5, implode(', ', $riga), 0, 1);
    } else {
        $pdf->Cell(90, 5, $riga, 0, 1);
    }
}

//make a dummy empty cell as a vertical spacer
$pdf->Cell(189	,10,'',0,1);//end of line


$pdf->ln(8);
$table->initialize([73, 12, 25, 20, 30, 20]);
$table->addHeader(array(
    [ 'TEXT' => 'Descrizione' ],
    [ 'TEXT' => 'Qty.' ],
    [ 'TEXT' => 'Prezzo Uni.' ],
    [ 'TEXT' => 'Sconto' ],
    [ 'TEXT' => 'Importo' ],
    [ 'TEXT' => 'Iva' ],
));

$tax = 0; //total tax
$amount = 0; //total amount

foreach($info_carrello as $carrello) {


    $table->addRow(array(
        [
            'TEXT' => $carrello['nome']
        ],
        [
            'TEXT' => $carrello['qty']
        ],
        [
            'TEXT' => $pdf->formatPrice($carrello['prezzo'])
        ],
        [
            'TEXT' => ($carrello['sconto'] ? $carrello['sconto'] . $carrello['tipo_sconto'] : '-')
        ],
        [
            'TEXT' => $pdf->formatPrice($carrello['subtotal'] - $carrello['valore_imposta'])
        ],
        [
            'TEXT' => ($carrello['imposta'] > 0 ? round($carrello['imposta']) . '%' : '-')
        ],
    ));

    //accumulate tax and amount
    $tax += $carrello['valore_imposta'];
    $amount += ($carrello['subtotal'] - ($carrello['valore_imposta']));

}
$table->close();


//summary
$pdf->Cell(1	,15,'',0,1);

$pdf->Cell(91	,5,'',0,0);
$pdf->Cell(35	,5,'Imponibile',0,0, 'R');
$pdf->Cell(4	,5,CURRENCY_SYMBOL,1,0);
$pdf->Cell(30	,5,$pdf->formatPrice($amount),1,1,'R');//end of line

$pdf->Cell(91	,5,'',0,0);
$pdf->Cell(35	,5,'Imposte IVA',0,0, 'R');
$pdf->Cell(4	,5,CURRENCY_SYMBOL,1,0);
$pdf->Cell(30	,5,$pdf->formatPrice($tax),1,1,'R');//end of line


$pdf->Cell(91	,5,'',0,0);
$pdf->SetFont($pdf->headerFont,'B',14);
$pdf->Cell(35	,5,'TOTALE',0,0, 'R');
$pdf->SetFont($pdf->headerFont,'',12);
$pdf->Cell(4	,5,CURRENCY_SYMBOL,1,0);
$pdf->Cell(30	,5,$pdf->formatPrice($amount + $tax),1,1,'R');//end of line

if(is_array($sedute_utilizzate) && count($sedute_utilizzate) > 0) {
    $pdf->ln(15);
    $pdf->Cell(100	,5,'Sedute Effettuate',0,1);

    $table->initialize([$pdf->widthFromPercentage("70"), $pdf->widthFromPercentage("30")]);
    $table->addHeader(array(
        [ 'TEXT' => 'Nome' ],
        [ 'TEXT' => 'Qty.' ],
    ));

    foreach($sedute_utilizzate as $seduta) {
        $table->addRow(array(
            [
                'TEXT' => $seduta['name']
            ],
            [
                'TEXT' => $seduta['qty']
            ],
        ));
    }
    $table->close();
}

echo 'data:application/pdf;base64,' . base64_encode($pdf->getOutput('S'));
?>

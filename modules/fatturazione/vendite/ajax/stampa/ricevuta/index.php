<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../../../sw-config.php');
require('../../../config/moduleConfig.php');

$fatture = new \MSFramework\Fatturazione\fatture();

$impostazioni_fatture = $MSFrameworkCMS->getCMSData('fatturazione_fatture');
$info_azienda = $MSFrameworkCMS->getCMSData('site');

$logo_url = '';
if($impostazioni_fatture['logo_fattura'] && json_decode($impostazioni_fatture['logo_fattura'])) {
    if(json_decode($impostazioni_fatture['logo_fattura'])[0]) {
        $logo_url = UPLOAD_FATTURAZIONE_FATTURE_FOR_DOMAIN . json_decode($impostazioni_fatture['logo_fattura'])[0];
    }
}

if($logo_url == '') {
    $logo = json_decode($MSFrameworkCMS->getCMSData('site')["logos"], true)['logo'];
    $logo_url = UPLOAD_LOGOS_FOR_DOMAIN . $logo;
}

$cliente = $_POST['cliente'];
$info_cliente = $_POST['info_cliente'];
$info_carrello = $_POST['carrello'];
$info_documento = $_POST['info_documento'];
$sedute_utilizzate = $_POST['sedute_utilizzate'];

$anno_fattura = DateTime::createFromFormat('d/m/Y', $info_documento['data']);

/* PREPARO I DATI DA MOSTRARE */
$info_indirizzo_cliente = array(
    $info_cliente['indirizzo'],
    $info_cliente['citta'],
    $info_cliente['provincia'],
    $info_cliente['cap']
);

$info_indirizzo_cliente = array_filter($info_indirizzo_cliente);

$dati_cliente = array(
    (!empty($info_cliente['fatturazione']['ragione_sociale']) ? $info_cliente['fatturazione']['ragione_sociale'] : $info_cliente['nome'] . ' ' . $info_cliente['cognome']),
    (!empty($info_cliente['fatturazione']['piva']) ? $info_cliente['fatturazione']['piva'] : $info_cliente['fatturazione']['cf']),
    $info_indirizzo_cliente,
    $info_cliente['telefono_casa']
);

$dati_cliente = array_filter($dati_cliente);

$ary_to_return = array('status' => 'error');

if (!$fatture->checkIDAvailability($_POST['id_vendita'], $info_documento['numero'], 'ricevuta')) {
    $ary_to_return = array('status' => 'invalid_number', 'number' => $fatture->getNextDocumentNumber('ricevuta'), 'error' => "Il numero selezionato è stato utilizzato già per un'altra ricevuta pertanto è stato incrementato automaticamente.");
} else {
    ob_start();
    include('pdf/index.php');
    $ary_to_return = array('status' => 'ok', 'source' => ob_get_clean());
    ob_flush();
}

die(json_encode($ary_to_return));


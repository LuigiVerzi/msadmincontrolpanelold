<?php
/**
 * MSAdminControlPanel
 * Date: 29/12/18
 */

require_once('../../../../../../sw-config.php');
require('../../../config/moduleConfig.php');

$cliente = $_POST['cliente'];
$info_cliente = $_POST['info_cliente'];
$info_carrello = $_POST['carrello'];
$info_documento = $_POST['info_documento'];
$sedute_utilizzate = $_POST['sedute_utilizzate'];

$ary_to_return = (new \MSFramework\Fatturazione\casse())->generaOutputCassa($info_carrello, $info_documento);

die(json_encode($ary_to_return));


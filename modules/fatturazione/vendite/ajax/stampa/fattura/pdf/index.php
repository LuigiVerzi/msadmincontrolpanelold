<?php
/**
 * MSAdminControlPanel
 * Date: 21/12/2018
 */

$pdf = new \MSFramework\Fatturazione\Output\PDF\pdf($info_documento['oggetto']);
$table = new \Interpid\PdfLib\Table($pdf);

$default_font_size = 11;

$pdf->AddPage();
if(is_file($logo_url)) {
    $pdf->Image($logo_url, 15, 10, 0, 10);
}

//set font to arial, bold, 14pt
$pdf->SetFont($pdf->headerFont,'B', $default_font_size);

$pdf->Cell(130	,5, '',0,1);

//Cell(width , height , text , border , end line , [align] )

$pdf->Cell(130	,5, $info_azienda['rag_soc'],0,0);
$pdf->Cell(59	,5,'Fattura',0,1);//end of line

//set font to arial, regular, 12pt
$pdf->SetFont($pdf->headerFont,'', $default_font_size);

$pdf->Cell(130	,5, $MSFrameworkCMS->getFullAddress(),0,0);

$pdf->Cell(34	,5, 'Fattura nr. ' . $info_documento['numero'] . '/' . $anno_fattura->format('Y'),0,1); //end of line

$pdf->Cell(130	,5,"",0,0);


$pdf->SetFont($pdf->headerFont,'', $default_font_size);
$pdf->Cell(25	,5, 'Data: ' . $info_documento['data'],0,1);
$pdf->SetFont($pdf->headerFont,'', $default_font_size);

$pdf->Cell(130	,5,'P.IVA:  ' . $info_azienda['piva'],0,1);

$pdf->Cell(130	,5,'Telefono: ' . (!empty($info_azienda['telefono']) ? $info_azienda['telefono'] : $info_azienda['cellulare']),0,1);

if(!empty($info_azienda['fax'])) {
    $pdf->Cell(130, 5, 'Fax: ' . $info_azienda['fax'] . '', 0, 1);
}
//make a dummy empty cell as a vertical spacer
$pdf->Cell(189	,10,'',0,1);//end of line

//billing address
$pdf->SetFont($pdf->headerFont,'B', $default_font_size);
$pdf->Cell(100	,5,'Cliente:',0,1);//end of line
$pdf->SetFont($pdf->headerFont,'', $default_font_size);

foreach($dati_cliente as $riga) {
    if(is_array($riga)) {
        $pdf->Cell(90, 5, implode(', ', $riga), 0, 1);
    } else {
        $pdf->Cell(90, 5, $riga, 0, 1);
    }
}

//make a dummy empty cell as a vertical spacer
$pdf->Cell(189	,10,'',0,1);//end of line


$pdf->ln(8);
$table->initialize([73, 12, 25, 20, 30, 20]
);
$table->addHeader(array(
    [ 'TEXT' => 'Descrizione' ],
    [ 'TEXT' => 'Qty.' ],
    [ 'TEXT' => 'Prezzo Uni.' ],
    [ 'TEXT' => 'Sconto' ],
    [ 'TEXT' => 'Importo' ],
    [ 'TEXT' => 'Iva' ],
));

$taxes = array(); //total tax
$amount = 0; //total amount

$total = 0;
$total_paid = 0;

foreach($info_carrello as $carrello) {


    $table->addRow(array(
        [
            'TEXT' => $carrello['nome']
        ],
        [
            'TEXT' => $carrello['qty']
        ],
        [
            'TEXT' => $pdf->formatPrice($carrello['prezzo'])
        ],
        [
            'TEXT' => ($carrello['sconto'] ? $carrello['sconto'] . $carrello['tipo_sconto'] : '-')
        ],
        [
            'TEXT' => $pdf->formatPrice($carrello['subtotal'] - $carrello['valore_imposta'])
        ],
        [
            'TEXT' => ($carrello['imposta'] > 0 ? round($carrello['imposta']) . '%' : '-')
        ],
    ));

    $taxes[round($carrello['imposta'])] = floatval($taxes[round($carrello['imposta'])]) + $carrello['valore_imposta'];

    //accumulate tax and amount
    $amount += ($carrello['subtotal'] - ($carrello['valore_imposta']));

    $total += $carrello['prezzo_subtotal'];
    $total_paid += $carrello['prezzo_pagato'];

}

if($info_documento['usa_crediti'] == 1) {

    $imposta_standard = 22;
    $valore_effettivo = -$info_documento['valore_credito'];

    if($valore_effettivo < 0) {
        // TODO: CAPIRE COME IMPOSTARE SCONTO
    }
    else {
        $table->addRow(array(
            [
                'TEXT' => 'Precedente Debito Cliente'
            ],
            [
                'TEXT' => '1'
            ],
            [
                'TEXT' => $pdf->formatPrice((new \MSFramework\Fatturazione\imposte())->removeTax($valore_effettivo, $imposta_standard))
            ],
            [
                'TEXT' => '-'
            ],
            [
                'TEXT' => $pdf->formatPrice((new \MSFramework\Fatturazione\imposte())->removeTax($valore_effettivo, $imposta_standard))
            ],
            [
                'TEXT' => $imposta_standard . '%'
            ],
        ));

        $taxes[round($imposta_standard)] = floatval($taxes[$imposta_standard]) + ( $valore_effettivo - (new \MSFramework\Fatturazione\imposte())->removeTax($valore_effettivo, $imposta_standard));
        $amount += (new \MSFramework\Fatturazione\imposte())->removeTax($valore_effettivo, $imposta_standard);
    }

}

$table->close();


//summary
$pdf->Cell(1	,15,'',0,1);

$pdf->Cell(91	,5,'',0,0);
$pdf->Cell(35	,5,'Imponibile',0,0, 'R');
$pdf->Cell(4	,5,CURRENCY_SYMBOL,1,0);
$pdf->Cell(30	,5,$pdf->formatPrice($amount),1,1,'R');//end of line

// Applico lo sconto incondizionato se il cliente ha già dei crediti
if($info_documento['usa_crediti'] == 1 && $info_documento['valore_credito'] > 0) {
    $valore_effettivo = -$info_documento['valore_credito'];
    $pdf->Cell(91, 5, '', 0, 0);
    $pdf->Cell(35, 5, 'Sconto Incondizionato', 0, 0, 'R');
    $pdf->Cell(4, 5, CURRENCY_SYMBOL, 1, 0);
    $pdf->Cell(30, 5, $pdf->formatPrice($valore_effettivo), 1, 1, 'R');//end of line
    $amount += $valore_effettivo;
}

// Se il cliente ha pagato meno di quanto dovuto detraggo quanto non pagato dalla fattura
if($total_paid < $total) {
    $valore_effettivo = $total_paid-$total;
    $pdf->Cell(91, 5, '', 0, 0);
    $pdf->Cell(35, 5, 'Nota di debito', 0, 0, 'R');
    $pdf->Cell(4, 5, CURRENCY_SYMBOL, 1, 0);
    $pdf->Cell(30, 5, $pdf->formatPrice($valore_effettivo), 1, 1, 'R');//end of line
    $amount += $valore_effettivo;
}

$lista_imposte = (new \MSFramework\Fatturazione\imposte())->getImposte();

foreach($taxes as $percentuale => $tax) {

    $nome_imposta = 'IVA ' . $percentuale . '%';
    if(isset($lista_imposte[$percentuale])) {
        $nome_imposta = $lista_imposte[$percentuale][0];
        if(strpos($lista_imposte[$percentuale][0], $percentuale) === false) {
            $nome_imposta .= ' ' . $percentuale . '%';
        }
    }

    $pdf->Cell(91, 5, '', 0, 0);
    $pdf->Cell(35, 5, 'Totale ' . $nome_imposta , 0, 0, 'R');
    $pdf->Cell(4, 5, CURRENCY_SYMBOL, 1, 0);
    $pdf->Cell(30, 5, $pdf->formatPrice($tax), 1, 1, 'R');//end of line
}

$pdf->Cell(91	,5,'',0,0);
$pdf->SetFont($pdf->headerFont,'B',14);
$pdf->Cell(35	,5,'TOTALE FATTURA',0,0, 'R');
$pdf->SetFont($pdf->headerFont,'',12);
$pdf->Cell(4	,5,CURRENCY_SYMBOL,1,0);

// Calcolo il Totale con IVA
$totale = $amount;
foreach($taxes as $tax) {
    $totale += $tax;
}
$pdf->Cell(30	,5,$pdf->formatPrice($totale),1,1,'R');//end of line

if(is_array($sedute_utilizzate) && count($sedute_utilizzate) > 0) {
    $pdf->ln(15);
    $pdf->Cell(100	,5,'Sedute Effettuate',0,1);

    $table->initialize([$pdf->widthFromPercentage("70"), $pdf->widthFromPercentage("30")]);
    $table->addHeader(array(
        [ 'TEXT' => 'Nome' ],
        [ 'TEXT' => 'Qty.' ],
    ));

    foreach($sedute_utilizzate as $seduta) {
        $table->addRow(array(
            [
                'TEXT' => $seduta['name']
            ],
            [
                'TEXT' => $seduta['qty']
            ],
        ));
    }
    $table->close();
}

// Note Aggiuntive
if(!empty($info_documento['attach_notes'])) {
    $pdf->ln(8);
    $table->initialize([180]);
    $table->addHeader(array(['TEXT' => 'NOTE']));
    $table->addRow(array(['TEXT' => $info_documento['attach_notes']]));
    $table->close();
}
echo 'data:application/pdf;base64,' . base64_encode($pdf->getOutput('S'));
?>

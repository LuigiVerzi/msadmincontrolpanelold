<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array(
        'db' => 'tipo_documento',
        'dt' => 'DT_buttonsLimit',
        'formatter' => function( $d, $row ) {
            return (!empty($d) && !in_array($d, array('fattura', 'ricevuta', 'preventivo')) ? "read|delete" : "");
        }
    ),
    array(
        'db' => 'sources',
        'dt' => 'DT_Sources',
        'formatter' => function( $d, $row ) {
            return (json_decode($d) ? json_decode($d, true) : array());
        }
    ),
    array(
        'db' => 'id',
        'dt' => 0,
        'formatter' => function( $d, $row ) {
           return $d;
        }
    ),
    array(
        'db' => 'dati_cliente',
        'dt' => 1,
        'formatter' => function( $d, $row ) {
            $json = json_decode($d, true);
            $full_name = $json['nome'] . ' ' . $json['cognome'];

            $html_to_return = '';

           if(!(new \MSFramework\customers())->getCustomerDataFromDB($json['id'])) $html_to_return .= '<s style="opacity: 0.7;">' . $full_name . '</s>';
           else $html_to_return .= '<a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'customers/list/edit.php?id=' . $json['id'] . '">' . $full_name . '</a>';

           if(json_decode($row['sources'])) {
               $sources = json_decode($row['sources'], true);

               $sources_to_search = array(
                   'referer' => array(
                       'icon' => 'fa fa-globe',
                       'name' => 'Referente',
                       'color' => '#2f2f2f'
                   ),
                   'gclid' => array(
                       'name' => 'Google Ads gclid',
                       'icon' => 'fa fa-google',
                       'color' => '#13a365'
                   ),
                   'fbclid' => array(
                       'name' => 'Facebook fbclid',
                       'icon' => 'fa fa-facebook',
                       'color' => '#3C5A99'
                   )
               );

               foreach($sources_to_search as $search_key => $search_values) {
                   if($sources[$search_key]) {
                       $html_to_return .= '<span title="' . $search_values['name'] . ': ' . htmlentities($sources[$search_key]['ref']) . ' - Data: ' . date('d/m/Y H:i', $sources[$search_key]['date']) . '" class="label" style="margin-left: 5px; float: right; color: white; background: ' . $search_values['color'] . ';"><i class="' . $search_values['icon'] . '"></i></span>';
                   }
               }

           }

           return $html_to_return;
        }
    ),
    array(
        'db' => 'carrello',
        'dt' => 2,
        'formatter' => function( $d, $row ) {
            $json = json_decode($d, true);
            $totale = 0;
            foreach($json as $c) {
                $totale += (float)$c['subtotal'];
            }
            return $totale . CURRENCY_SYMBOL;
        }
    ),
    array(
        'db' => 'tipo_documento',
        'dt' => 3,
        'formatter' => function( $d, $row ) {
            return !empty($d) ? ucwords(str_replace('_', ' ', $d)) : '<span class="text-muted">Nessuno</span>';
        },
        'customSearchPattern' => function($db, $string) {

            if(is_numeric($string))
            {
                if($string == 0) {
                    return "1";
                } else {
                    if($string == 1) $string = implode("','", array('fattura', 'scontrino'));
                    else $string = implode("','", array('ricevuta', 'scontrino_non_fiscale'));
                    return "`" . $db . "` IN('$string')";
                }
            }
            else {
                return "`" . $db . "` = '$string'";
            }
        }
    ),
    array(
        'db' => 'data',
        'dt' => 4,
        'formatter' => function( $d, $row ) {
           return date("d/m/Y H:i", strtotime($d));
        }
    ),
    array(
        'db' => 'stato',
        'dt' => 5,
        'formatter' => function( $d, $row ) {
            return ($d == "1") ? "Si" : "No";
        }
    ),
);

echo json_encode(
    $datatableHelper::simple( $_GET, $MSFrameworkDatabase, 'fatturazione_vendite', 'id', $columns )
);
?>

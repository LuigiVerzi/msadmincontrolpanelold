<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$MSFrameworkDatabase->flushCacheByNameSpace('customers');
$lista_clienti = $MSFrameworkDatabase->getAssoc("SELECT * FROM customers ORDER BY id DESC");

foreach($lista_clienti as $k => $c) {
    $lista_clienti[$k]['dati_fatturazione'] = json_decode($c['dati_fatturazione'], true);
}

$clienti_recenti = array_slice($lista_clienti, 0, 5);
$altri_clienti = array_slice($lista_clienti, 5);

die(json_encode(array($clienti_recenti, $altri_clienti)));

?>
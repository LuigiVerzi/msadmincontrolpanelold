<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$term = $_GET['q'];

$return = array(
    'items' => array()
);

if(strlen($term) < 3) {
    $return['items'][] = array('id' => 'new', 'text' => '<b>Crea Nuovo Cliente</b>');
    die(json_encode($return));
}

$limit = "";
if(strlen($term) <= 5) {
    $limit = " LIMIT 10";
}

$where_res = $MSFrameworkDatabase->composeSearch($term, array("nome", "cognome"));

$lista_clienti = $MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, dati_fatturazione, email, telefono_cellulare, telefono_casa FROM customers WHERE id != '' $where_res[0]  ORDER BY cognome, nome $limit", $where_res[1]);

foreach($lista_clienti as $c) {
    $c['dati_fatturazione'] = json_decode($c['dati_fatturazione'], true);
    $return['items'][] = array(
        'id' => $c['id'],
        'text' => '<b>' . $c['nome'] . ' ' . $c['cognome'] . '</b>' . (!empty($c['dati_fatturazione']['ragione_sociale']) ? ' - ' . $c['dati_fatturazione']['ragione_sociale'] : '') . ($c['email'] ? ' (' . $c['email'] . ')' : '') . ($c['telefono_cellulare'] ? ' (' . $c['telefono_cellulare'] . ')' : '') . ($c['telefono_casa'] ? ' (' . $c['telefono_casa'] . ')' : '')
    );
}

die(json_encode($return));

?>
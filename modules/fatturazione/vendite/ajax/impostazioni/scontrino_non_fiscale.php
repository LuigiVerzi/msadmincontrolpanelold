<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/18
 */

if(!isset($r)) {
    require_once('../../../../../sw-config.php');
    require('../../config/moduleConfig.php');

    $impostazioni_documento = array();
    if(json_decode($_POST['settings']))  {
        $impostazioni_documento = json_decode($_POST['settings'], true);
    }
}
?>

<div class="ibox">
    <div class="ibox-title">
        <h5>Dati Scontrino - NON FISCALE</h5>
    </div>
    <div class="ibox-content">

        <div class="row">
            <div class="col-sm-12">
                <label>Cassa</label>
                <select name="cassa" class="form-control impostazione_documento parent">
                    <option value="">Manuale</option>
                    <?php foreach((new \MSFramework\Fatturazione\casse())->ottieniCasseAttive(2) as $id => $cassa) { ?>
                        <option value="<?= $cassa['id']; ?>" <?= ($cassa['id'] == $impostazioni_documento['cassa'] ? 'selected' : ''); ?>><?= $cassa['nome']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <label>Note Interne</label>
                <textarea name="note_interne" type="text" class="form-control impostazione_documento" placeholder="Note visibile solamente dagli operatori" rows="5"><?php echo htmlentities($impostazioni_documento['note_interne']); ?></textarea>
            </div>
        </div>

        <div style="clear: both;"></div>

    </div>
</div>

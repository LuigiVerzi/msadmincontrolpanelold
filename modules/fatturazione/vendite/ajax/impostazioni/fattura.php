<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/18
 */

if(!isset($r)) {
    require_once('../../../../../sw-config.php');
    require('../../config/moduleConfig.php');

    $impostazioni_documento = array();
    if(json_decode($_POST['settings']))  {
        $impostazioni_documento = json_decode($_POST['settings'], true);
    }
    $impostazioni_fatture = $MSFrameworkCMS->getCMSData('fatturazione_fatture');

    $r_cliente = (new \MSFramework\customers())->getCustomerDataFromDB($_POST['customer_id'], "nome, cognome");
}
?>

<input type="hidden" id="source" name="source" value="" class="impostazione_documento">

<div class="ibox">
    <div class="ibox-title">
        <h5>Metodo di Pagamento</h5>
    </div>
    <div class="ibox-content">

        <div class="row">
            <div class="col-sm-12">
                <label>Metodo di Pagamento</label>
                <select name="pagamento" class="form-control impostazione_documento">
                    <?php foreach((new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamento() as $id => $metodo) { ?>
                        <option value="<?= $id; ?>" <?= ($id == $impostazioni_documento['pagamento'] ? 'selected' : ''); ?>><?= $metodo['nome']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>

<div class="ibox">
    <div class="ibox-title">
        <h5>Dati Fattura</h5>
    </div>
    <div class="ibox-content">

        <div class="row">
            <div class="col-sm-6">
                <label>Data*</label>
                <input name="data" type="text" class="form-control impostazione_documento date required" value="<?php echo ($impostazioni_documento['data'] ? htmlentities($impostazioni_documento['data']) : date('d/m/Y')); ?>">
            </div>
            <div class="col-sm-6">
                <label>Data Pagamento*</label>
                <input name="data_pagamento" type="text" class="form-control impostazione_documento date required" value="<?php echo ($impostazioni_documento['data_pagamento'] ? htmlentities($impostazioni_documento['data_pagamento']) : date('d/m/Y')); ?>">
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <label>Numero</label>
                <input name="numero" type="number" class="form-control impostazione_documento required" value="<?php echo ($impostazioni_documento ? (int)$impostazioni_documento['numero'] : (new \MSFramework\Fatturazione\fatture())->getNextDocumentNumber('fattura') ); ?>" <?= ($_POST['id'] != "" ? "disabled" : "") ?>>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <label>Oggetto</label>
                <input name="oggetto" type="text" class="form-control impostazione_documento" placeholder="Oggetto che apparirà nel documento" value="<?php echo htmlentities(($impostazioni_documento['oggetto'] == "" ? ($_POST['customer_id'] != "" ? $r_cliente['nome'] . " " . $r_cliente['cognome'] : "") : $impostazioni_documento['oggetto'])); ?>">
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <label>Note Interne</label>
                <textarea name="note_interne" type="text" class="form-control impostazione_documento" placeholder="Note visibile solamente dagli operatori" rows="5"><?php echo htmlentities($impostazioni_documento['note_interne']); ?></textarea>
            </div>
        </div>

        <div style="clear: both;"></div>

    </div>
</div>

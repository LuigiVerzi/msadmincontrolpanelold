<?php
/**
 * MSAdminControlPanel
 * Date: 31/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$document_id = $_POST['pID'];

$r = (new \MSFramework\Fatturazione\fatture())->getDocumentDetails($document_id);

if(!in_array($document_id, array_keys($r))) {
    die('document_not_found');
}

$r = $r[$document_id];

$db_customer = (new \MSFramework\customers())->getCustomerDataFromDB($r['cliente']);
if(!$db_customer) {
    die('customer_not_found');
}

$info_documento = json_decode($r['info_documento'], true);
$info_cliente = json_decode($r['dati_cliente'], true);

$emails = new \MSFramework\Fatturazione\emails();
$emails->mail->addStringAttachment(base64_decode(str_replace('data:application/pdf;base64,', '', $info_documento['source'])), (!empty($r['tipo_documento']) ? $r['tipo_documento'] : 'default') . '.pdf');

$emails->sendMail(
    ($r['tipo_documento'] ? $r['tipo_documento'] : 'standard') . '-cliente',
    array(
        'email' => $db_customer['email'],
        'nome' => $info_cliente['nome'] . ' ' . $info_cliente['cognome'],
        'data_documento' => date('d/m/Y H:i', strtotime($r['data'])),
        'numero_documento' => $info_documento['numero'],
        'oggetto' => $info_documento['oggetto'],
        'messaggio' => $_POST['pMessaggio']
    )
);

$MSFrameworkDatabase->query('UPDATE fatturazione_vendite SET sent_date = NOW() WHERE id = :id', array(':id' => $document_id));

die('ok');
<?php
/**
 * MSAdminControlPanel
 * Date: 31/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$rererenze_utente = $MSFrameworkDatabase->getAssoc("SELECT project_id, track_references FROM tracking__references WHERE user_ref = :user_id", array(':user_id' => $_POST['id']));

$progetti_attivi = array();
if($rererenze_utente) {
    foreach($rererenze_utente as $referenza) {
        $progetto_ref = (new \MSFramework\tracking())->getTrackingProjectDetails($referenza['project_id']);

        if(in_array('acquisto', $progetto_ref['actions'])) {

            $acquisto_value = $progetto_ref['actions_value']['acquisto'];

            if($acquisto_value['enable_external_tracking'] === "1") {
                if (!empty($progetto_ref['tracking_codes']['google_analytics_ua'])) {
                    $progetti_attivi[] = '<span class="label label-inverse" style="background: #ED750A; margin-right: 5px;"><i class="fa fa-google"></i> Google Analytics</span>';
                }
                if (!empty($progetto_ref['tracking_codes']['facebook_pixel_id'])) {
                    $progetti_attivi[] = '<span class="label label-inverse" style="background: #3C5A99; margin-right: 5px;"><i class="fa fa-facebook"></i> Facebook Pixel</span>';
                }
            }
        }
    }
}

if($progetti_attivi) {
    echo '<div style="margin: 0 0 10px;">La vendita verrà tracciata su:</div>';
    echo implode(' ', $progetti_attivi);
}

die();
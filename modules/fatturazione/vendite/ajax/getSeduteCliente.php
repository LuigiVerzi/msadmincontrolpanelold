<?php
/**
 * MSAdminControlPanel
 * Date: 31/01/2019
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if(!$MSFrameworkCMS->checkExtraFunctionsStatus('beautycenter')) {
    die('');
}

if($_POST['pEventID'] != "") {
    $event_r = $MSFrameworkDatabase->getAssoc("SELECT services FROM appointments_list WHERE id = :id", array(":id" => $_POST['pEventID']), true);
    $event_r_services = explode(",", $event_r['services']);
}

$invoices_class = new \MSFramework\Fatturazione\fatture();
$doc_details = ($_POST['pRecordID'] != "" ? json_decode($invoices_class->getDocumentDetails($_POST['pRecordID'], 'sedute_utilizzate')[$_POST['pRecordID']]['sedute_utilizzate'], true) : array());
$old_sedute_by_id = array();
foreach($doc_details as $old_seduta) {
    $old_sedute_by_id[$old_seduta['id']] = $old_seduta;
}

$services_class = new \MSFramework\services();
$offers_class = new \MSFramework\BeautyCenter\offers();

$id = (int)$_POST['id'];
$carrello = $_POST['pCarrello'];

$on_the_fly_packs = array();
$bilancio_sedute = array();

foreach($carrello as $articolo) {
    if($articolo['gift_to'] != "") {
        continue;
    }

    if($articolo['type'] == 'servizio') {
        $info_servizio = $services_class->getServiceDetails($articolo['id'])[$articolo['id']];

        if (isset($info_servizio['extra_fields']['sessioni']) && intval($info_servizio['extra_fields']['sessioni']) > 1) {
            $on_the_fly_packs['service_' . $articolo['id']]['qty'] += ($info_servizio['extra_fields']['sessioni']*$articolo['qty']);
            $on_the_fly_packs['service_' . $articolo['id']]['nome'] = $articolo['nome'];
            $on_the_fly_packs['service_' . $articolo['id']]['type'] = $articolo['type'];
        }
    } else if($articolo['type'] == 'pacchetto') {
        $info_pacchetto = $offers_class->getOfferDetails($articolo['id'])[$articolo['id']];

        if($info_pacchetto && isset($info_pacchetto['extra_fields']['sedute']) && intval($info_pacchetto['extra_fields']['sedute']) > 0) {
            $on_the_fly_packs['offer_' . $articolo['id']]['qty'] += ($info_pacchetto['extra_fields']['sedute']*$articolo['qty']);
            $on_the_fly_packs['offer_' . $articolo['id']]['nome'] = $articolo['nome'];
            $on_the_fly_packs['offer_' . $articolo['id']]['type'] = $articolo['type'];
        }
    }
}

foreach($old_sedute_by_id as $articoloID => $articolo) {
    if($articolo['type'] == 'service') {
        $info_servizio = $services_class->getServiceDetails($articolo['clean-id'])[$articolo['clean-id']];

        if (isset($info_servizio['extra_fields']['sessioni']) && intval($info_servizio['extra_fields']['sessioni']) > 1) {
            $on_the_fly_packs['service_' . $articolo['clean-id']]['qty'] += $articolo['qty'];
            $on_the_fly_packs['service_' . $articolo['clean-id']]['nome'] = $articolo['nome'];
            $on_the_fly_packs['service_' . $articolo['clean-id']]['type'] = $articolo['type'];
        }
    } else if($articolo['type'] == 'offer') {
        $info_pacchetto = $offers_class->getOfferDetails($articolo['clean-id'])[$articolo['clean-id']];

        if($info_pacchetto && isset($info_pacchetto['extra_fields']['sedute']) && intval($info_pacchetto['extra_fields']['sedute']) > 0) {
            $on_the_fly_packs['offer_' . $articolo['clean-id']]['qty'] += $articolo['qty'];
            $on_the_fly_packs['offer_' . $articolo['clean-id']]['nome'] = $articolo['nome'];
            $on_the_fly_packs['offer_' . $articolo['clean-id']]['type'] = $articolo['type'];
        }
    }
}

if($id != "") {
    $customer = (new \MSFramework\customers())->getCustomerDataFromDB($id);
    ob_start();
    $bilancio_sedute = (new \MSFramework\BeautyCenter\sedute())->getCustomerSessionsBalance($id);
}

foreach($bilancio_sedute as $sedutaK => $seduta) {
    $ref = json_decode($seduta['ref'], true);
    $ref["clean_id"] = str_replace(array("service_", "offer_"), array(), $seduta['tipo']);
    $ref["id"] = $seduta['tipo'];

    $bilancio_sedute[$sedutaK]['ref'] = json_encode($ref);
}

foreach($on_the_fly_packs as $cur_packK => $cur_packV) {
    foreach($bilancio_sedute as $sedutaK => $seduta) {
        if($seduta['tipo'] == $cur_packK) {
            $bilancio_sedute[$sedutaK]['sedute'] += $cur_packV['qty'];
            continue 2;
        }
    }

    $bilancio_sedute[] = array(
        "sedute" => $cur_packV['qty'],
        "ref" => json_encode(
            array(
                "clean_id" => str_replace(array("servizio_", "pacchetto_"), array(), $cur_packK),
                "id" => $cur_packK,
                "nome" => json_encode(array("it_IT" => $cur_packV['nome'])),
            )
        )
    );
}

if ($bilancio_sedute) {
    ?>
    <table id="main_table_list" class="balance_table display table table-striped table-bordered table-hover"
           width="100%" cellspacing="0" style="margin-bottom: -30px;">
        <tbody>
        <?php
        foreach ($bilancio_sedute as $b) {
            if ($b['sedute'] != 0) {
                $info_servizio = json_decode($b['ref'], true);

                $exploded_id = explode("_", $info_servizio['id']);
                ?>
                <tr>
                    <td width="70%">
                        <span
                            class="service_name"><?= $MSFrameworki18n->getFieldValue($info_servizio['nome']) ?></span>
                    </td>
                    <td width="30%">
                        <select class="form-control utilizzo_seduta"
                                data-id-servizio="<?php echo $info_servizio['id'] ?>"
                                data-clean-id-servizio="<?php echo $info_servizio['clean_id'] ?>"
                                data-tipo-servizio="<?php echo $exploded_id[0] ?>"
                                id="utilizzo_seduta_<?php echo $info_servizio['id'] ?>">
                            <?php
                            for ($x = 0; $x <= $b['sedute']; $x++) {
                                $selected = "";
                                if($_POST['pRecordID'] != "") {
                                    $selected = (array_key_exists($info_servizio['id'], $old_sedute_by_id) && $x == $old_sedute_by_id[$info_servizio['id']]['qty'] ? "selected" : "");
                                } else {
                                    $selected = ($x == "1" && in_array($info_servizio['clean_id'], $event_r_services) ? "selected" : "");
                                }

                                ?>
                                <option value="<?= $x ?>" <?= $selected ?>><?= $x ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
    <?php
}

$customer['sedute_disponibili'] = ob_get_contents();
ob_clean();
die(json_encode($customer));
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 18/04/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$id = (int)$_POST['id'];

$customer = (new \MSFramework\customers())->getCustomerDataFromDB($id);

$customer['data_nascita'] = ($customer['data_nascita'] != '' ? date("d/m/Y", $customer['data_nascita']) : '');
$customer['crediti'] = (new \MSFramework\customers())->getCustomerBalance($customer['id']);
ob_start();
?>
<table id="main_table_list" class="balance_table display table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="margin-bottom: -15px;">
    <thead>
    <tr>
        <th>Tipologia</th>
        <th>Valore</th>
        <th>Note</th>
        <th class="default-sort is_data" data-sort="DESC">Data</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach((new \MSFramework\customers())->getCustomerBalanceHistory($id)['log'] as $operation) {
    ?>
    <tr>
        <td><span class="label label-<?= ($operation['tipo'] == 'debito' ? 'danger' : 'primary') ?>"><?= ucfirst($operation['tipo']) ?></span></td>
        <td><?= number_format($operation['valore'], 2, ',', '.') . ' ' . CURRENCY_SYMBOL ?></td>
        <td><?= $operation['note'] ?></td>
        <td><?= date("d/m/Y H:i", strtotime($operation['data'])) ?></td>
    </tr>
    <?php
    }
    ?>
    </tbody>
</table>
<?php
$customer['crediti_history'] = ob_get_contents();
ob_clean();

ob_start();

$bilancio_sedute = false;
if($MSFrameworkCMS->checkExtraFunctionsStatus('beautycenter')) {
    $bilancio_sedute = (new \MSFramework\BeautyCenter\sedute())->getCustomerSessionsBalance($id);
}

if($bilancio_sedute) {
    ?>
    <table id="main_table_list" class="balance_table display table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="margin-bottom: -15px;">
        <tbody>
        <?php
        foreach($bilancio_sedute as $b) {
            if($b['sedute'] != 0) {
                $info_servizio = json_decode($b['ref'], true);
            ?>
            <tr>
                <td width="70%">
                    <span class="service_name"><?= $MSFrameworki18n->getFieldValue($info_servizio['nome']) ?></span>
                </td>
                <td width="30%">
                    <select class="form-control utilizzo_seduta" data-id-servizio="<?php echo $info_servizio['id'] ?>" id="utilizzo_seduta_<?php echo $info_servizio['id'] ?>">
                    <?php
                    for($x=0; $x<=$b['sedute']; $x++) {
                    ?>
                        <option value="<?= $x ?>"><?= $x ?></option>
                    <?php
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <?php
            }
        }
        ?>
        </tbody>
    </table>
    <?php
}
$customer['sedute_disponibili'] = ob_get_contents();
ob_clean();

$customer['anteprima'] = $customer['html_preview'];

$fields_to_hide = array('passowrd', 'mail_auth', 'last_login');
foreach($fields_to_hide as $hide_field) {
    unset($customer[$hide_field]);
}

die(json_encode($customer));
?>

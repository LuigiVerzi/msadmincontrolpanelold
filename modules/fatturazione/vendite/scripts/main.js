$(document).ready(function() {
    if($('#main_table_list').length && !$('#record_id').length) {
        loadStandardDataTable('main_table_list', true, {
            "initComplete": function (settings, json) {

                this.api().columns().every(function () {
                    var column = this;

                    if ($(column.header()).hasClass('have_filter')) {

                        var select = $('<select style="margin: 1px 0;"><option value="">[' + $(column.header()).html() + '] Filtra</option></select>').prependTo($(column.header()).empty())
                            .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                    column.search(this.value).draw();
                    }
                );

                        select.html('<optgroup label="Fiscalità"><option value="0">[DOCUMENTI] Tutti</option><option value="1">[DOCUMENTI] Fiscali</option><option value="2">[DOCUMENTI] Non Fiscale</option></optgroup><optgroup label="Tipo Documento"><option value="">[DOCUMENTO] Senza documento</option><option value="fattura">[DOCUMENTO] Fattura</option><option value="scontrino">[DOCUMENTO] Scontrino - Fiscale</option><option value="ricevuta">[DOCUMENTO] Ricevuta</option><option value="scontrino_non_fiscale">[DOCUMENTO] Scontrino - Non Fiscale</option></optgroup>');
                    }
                });

            }
        });
        allowRowHighlights('main_table_list');
        manageGridButtons('main_table_list');
    }
});

function deleteAll() {

    var tipologia = $('#select_eliminazione_vendite').val();

    if(tipologia == '')
    {
        bootbox.error("Per effettuare un'eliminazione di massa devi selezionare la tipologia di vendite che desideri eliminare.");
    }
    else
    {
        bootbox.confirm("Proseguendo eliminerai tutti i dati relativi alle vendite <b>" + (tipologia == 'fiscali' ? 'Fiscali' : 'Non Fiscali') + "</b>, l'operazione non può essere annullata. Sei sicuro di voler procedere?", function (result)
        {
            if (result) {

                $.ajax({
                    url: "db/massDelete.php",
                    type: "POST",
                    data: {
                        "pType": tipologia
                    },
                    async: false,
                    success: function(data) {
                        if(data == "ok") {
                            bootbox.alert("Le vendite " + (tipologia == 'fiscali' ? 'Fiscali' : 'Non Fiscali') + " sono state eliminate correttamente", function () {
                                window.location.reload();
                            });
                        }
                        else
                        {
                            bootbox.error("Si è verificato un errore durante l'eliminazione delle vendite");
                        }
                    }
                });

            }
        });
    }
}

/* INIZIO FUNZIONI PAGINA VENDITA */

function initForm() {

    if(!$('#tipo_documento').length) {
        return initView();
    }

    window.customerForceClosed = false;

    $('#paga_tramite_crediti').iCheck({
        checkboxClass: 'icheckbox_square-green'
    });

    $('body').on('ifChanged', '#paga_tramite_crediti', calculateTableSummary);

    $('#customer_info .date').datepicker({
        keyboardNavigation: false,
        autoclose: true,
        language: "it",
        endDate: new Date()
    });

    $('.selectCustomer').on('click', function (e) {
        e.preventDefault();
        showFastPicker('manage_customers', function (row) {

            $('#customerInfo .data').html(row.DT_FormattedData).show();
            loadCustomerInfo(row.DT_RowId);

            toastr['success']('Cliente selezionato con successo');
            $('#fastModulePicker').modal('hide');
        }, 'cliente');

    });

    $('.createCustomer').on('click', function (e) {
        e.preventDefault();
        showFastEditor('manage_customers', '', function (data) {
            loadCustomerInfo(data.id);
            toastr['success']('Cliente creato con successo');
            $('#fastModuleEditor').modal('hide');
        });

    });

    if($('#cliente').val() != '') {
        loadCustomerInfo($('#cliente').val());
    }

    $('#tipo_documento').on('change', function (e) {
        var value = $(this).val();
        loadDocumentInfo(value);
        updateSaveText();
    });
    loadDocumentInfo($('#tipo_documento').val());
    updateSaveText();

    $('#impostazioni_documento').on('change', '.parent', function () {

        var $childrens = $('#impostazioni_documento').find('[data-parent="' + $(this).attr('name') + '"]');

        if($(this).val() != "") $childrens.show();
        else $childrens.hide();

    });

    calculateTableSummary();

    $('#customer_info #saveCustomer').on('click', function () {
        fastCustomerSave();
    });

    $('#customer_info #closeCustomer').on('click', function () {
        $('#customer_info').hide();
        $('.cliente_box').addClass('show_edit');

        if(!window.customerForceClosed) {
            toastr['success']("Dati cliente aggiornati. Ricordati di salvare la vendita per rendere effettive le modifiche.");
        }
    });

    $('#editCustomer').on('click', function () {
        $('#customer_info').show().find('.ibox-content').show();
        $('.cliente_box').removeClass('show_edit');
    });

    $('.filter_tables').on('input keyup', function () {
        var value = $(this).val();
        var $search_container = $(this).parent().find('.search-container');
        var type = $(this).closest('.tab-pane').attr('id');
        searchInTable($search_container, type, value);
    });

    $('#articoli_e_servizi').find('img').each(function(){
        var img = new Image();
        var $img = $(this);
        var type = $img.closest('.add_to_cart').data('type');
        img.onerror = function() {
            $img.attr('src', $('#baseElementPathAdmin').val() + 'modules/fatturazione/vendite/images/' + type + '.png');
        };
        img.src = $(this).attr('src');
    });

    /* AGGIUNGE ARTICOLI AL CARRELLO */
    $('#add_new_custom_product').on('click', function (e) {
        e.preventDefault();
        addArticleToCart('', '', '', '', '');
    });

    $('#articoli_e_servizi .nav-tabs li').first().find('a').click();

    $('#articoli_e_servizi ').on('click', '.add_to_cart', function () {

        var name = $(this).find('.name').text();
        var data = $(this).data();

        if(data.type == 'sconto') {

            var html = '';
            $("#tabella_articoli tbody tr").each(function () {
                if(typeof($(this).attr('id')) != 'undefined') {
                    html += '<a class="btn btn-white btn-block apply_offer" data-id="' + $(this).attr('id') + '" style="margin-bottom: 15px;">' + $(this).find('[name="nome"]').val() + '</a>';
                }
            });

            if(html.length) {
                $('#selectArticleModal .offer_name').text(name);
                $('#selectArticleModal .offer_value').text(data.price + data.method);
                $('#selectArticleModal .offer_articles_list').html(html);
                $('#selectArticleModal #valore_sconto').val(data.price);
                $('#selectArticleModal #metodo_sconto').val(data.method);
                $('#selectArticleModal').modal('show');
            }
            else {
                bootbox.error("Non hai selezionato nessun articolo sul quale è possibile applicare lo sconto.");
            }

        }
        else if(data.type == 'prodotto_variabile') {
            composeVariationModal(data.id, name);
        }
        else {
            addArticleToCart(data.id, data.type, name, data.price, data.imposta);
        }

    });

    $('#selectArticleModal').on('click', '.apply_offer', function (e) {
        $('#selectArticleModal').modal('hide');
        applyOfferToArticle($('#' + $(this).data('id')), $('#selectArticleModal #valore_sconto').val(), $('#selectArticleModal #metodo_sconto').val());
    });

    $('#selectVariationModal').on('click', '.add_variation', function (e) {
        addProductVariationToCart();
    });

    $("#tabella_articoli").on('change', 'input, select', function() {
        calculateTableSummary($(this));
    })

    $("#tabella_articoli, #tabella_articoli").on('keyup', 'input, select', function() {
        calculateTableSummary($(this), 'keyup');
    })

    $("#tabella_articoli").on('click', '.remove_from_cart', function (e) {
        e.preventDefault();

        var $tr = $(this).closest('tr');

        if($("#tabella_articoli tbody tr").length > 1) {
            $tr.remove();
        }
        else {
            $tr.attr('id', '');
            $tr.find('input').val('');
            $tr.find('[name="tipo_sconto"]').val('€');
            $tr.find('[name="prezzo"]').val('0.00');
            $tr.find('[name="prezzo_subtotal"]').val('0.00');
            $tr.find('[name="prezzo_pagato"]').val('0.00');
            $tr.find('[name="qty"]').val('1');
            $tr.find('[name="sconto"]').val('0').change();
        }

        calculateTableSummary();

    });

    $("#tabella_articoli").on('click', '.add_as_gift', function (e) {
        e.preventDefault();

        var $tr = $(this).closest('tr');
        cur_gift_user_id = $tr.find('[name="gift_to"]').val();

        product_id = $tr.attr('id');
        if(typeof(product_id) == "undefined" || product_id == "") {
            bootbox.error("Per poter compiere questa operazione, devi prima selezionare un prodotto da regalare!");
            return true;
        }

        $.fn.modal.Constructor.prototype.enforceFocus = function() {};

        $('#assignGiftToUser').unbind('shown.bs.modal');
        $('#assignGiftToUser').on('shown.bs.modal', function (e) {
            if ($('#cliente_forgift').hasClass("select2-hidden-accessible")) {
                $('#cliente_forgift').select2('destroy');
            }

            $('#cliente_forgift').select2({
                ajax: {
                    url: "ajax/getCustomersSelect.php",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.items
                        }
                    },
                    cache: true
                },
                placeholder: 'Cerca un Cliente',
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 0
            }).trigger('change.select2');
        })

        $('#assignGiftToUser').unbind('show.bs.modal');
        $('#assignGiftToUser').on('show.bs.modal', function (e) {
            $('#customer_info_forgift').html($('#customer_info').html());
            $('#customer_info_forgift input').val('');
            $('#customer_info_forgift').hide();
            $('#assignGiftToUser #editCustomerForGift, #assignGiftToUser #removeCustomerForGift').hide();

            $('#customer_info_forgift #saveCustomer').unbind('click');
            $('#customer_info_forgift #saveCustomer').on('click', function() { fastCustomerSave('_forgift'); $('#assignGiftToUser #editCustomerForGift, #assignGiftToUser #removeCustomerForGift').show(); $('#customer_info_forgift').hide(); });
            $('#customer_info_forgift').hide();
            $('#customer_info_forgift #closeCustomer').remove();

            $('#cliente_forgift').unbind('change');
            $('#cliente_forgift').on('change', function(e) {
                var id = $(this).val();

                $('#customer_info_forgift input').val('');
                $('#customer_info_forgift').hide();

                if(id == "new") {
                    $('#customer_info_forgift #saveCustomer').text('Crea Nuovo Cliente');
                    $('#customer_info_forgift').show();
                    $('#assignGiftToUser #editCustomerForGift, #assignGiftToUser #removeCustomerForGift').hide();
                } else {
                    $.ajax({
                        url: "ajax/getCustomer.php",
                        type: "POST",
                        data: {"id": id},
                        async: false,
                        dataType: "json",
                        success: function (mydata) {
                            if(!mydata.dati_fatturazione) {
                                return true;
                            }

                            var data = mydata;
                            setTimeout(function() {
                                var dati_fatturazione = JSON.parse(data.dati_fatturazione);

                                for (var k in data) {
                                    if ($('#customer_info_forgift').find('.info_cliente [name="' + k + '"]').length) {
                                        $('#customer_info_forgift').find('.info_cliente [name="' + k + '"]').val(data[k]).change();
                                    }
                                }

                                for (var k in dati_fatturazione) {
                                    if ($('#customer_info_forgift').find('.info_fatturazione [name="' + k + '"]').length) {
                                        $('#customer_info_forgift').find('.info_fatturazione [name="' + k + '"]').val(dati_fatturazione[k]);
                                    }
                                }

                                $('#customer_info_forgift #saveCustomer').text('Salva & Aggiorna in Anagrafica');
                                $('#assignGiftToUser #editCustomerForGift, #assignGiftToUser #removeCustomerForGift').show();
                            }, 300)

                        }
                    });

                }

            });

            $('#cliente_forgift').val(cur_gift_user_id).trigger('change');

            $('#assignGiftToUser .save_gift').unbind('click');
            $('#assignGiftToUser .save_gift').on('click', function (e) {
                var id = $('#cliente_forgift').val();
                if(id == "new" || id == "" || id == null) {
                    $tr.find('[name="gift_to"]').val('');
                    $tr.find('.add_as_gift').addClass('btn-outline');
                } else {
                    $tr.find('[name="gift_to"]').val(id);
                    $tr.find('.add_as_gift').removeClass('btn-outline');
                }

                getSeduteCliente();

                $('#assignGiftToUser').modal('hide');

            });

            $('#assignGiftToUser #editCustomerForGift').unbind('click');
            $('#assignGiftToUser #editCustomerForGift').on('click', function (e) {
                $('#customer_info_forgift').show();
                $('#assignGiftToUser #editCustomerForGift, #assignGiftToUser #removeCustomerForGift').hide();
            });

            $('#assignGiftToUser #removeCustomerForGift').unbind('click');
            $('#assignGiftToUser #removeCustomerForGift').on('click', function (e) {
                $('#cliente_forgift').val('').trigger('change');
                $('#customer_info_forgift').hide();
                $('#assignGiftToUser #editCustomerForGift, #assignGiftToUser #removeCustomerForGift').hide();
            });
        })

        $('#assignGiftToUser').modal('show');

    });
}

function updateSaveText() {
    $('#editSaveBtn').html('Salva ' + $('#tipo_documento').val().charAt(0).toUpperCase() + $('#tipo_documento').val().slice(1));
}

function moduleSaveFunction() {
    stampaVendita();
}

function saveOperation(justCheckValidFn) {

    justCheckValid = 0;
    if(typeof(justCheckValidFn) === "function") {
        justCheckValid = 1;
    }

    startLoadingMode();

    var to_return = "";

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pCliente": $('#cliente').val(),
            "pDatiCliente": getCustomerObject(),
            "pOperatore": $('#operatore').val(),
            "pDocumento": $('#tipo_documento').val(),
            "pInfoDocumento": getDocumentInfo(),
            "pCarrello": getCart(),
            "pActive": 1,
            "pJustCheckValid": justCheckValid,
            "pSeduteUtilizzate": getSeduteUtilizzate(),
            "pRegaliUtilizzati": getRegaliUtilizzati(),

        },
        async: true,
        dataType: "json",
        success: function(data) {

            endLoadingMode();

            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "products_oversell") {
                var oversell_ary = [];
                data.products_oversell_details.forEach(function (val) {
                    oversell_ary.push(val.name + " (max " + val.max + " pezzi)");
                });

                bootbox.error("Non ci sono abbastanza pezzi disponibili in magazzino per i seguenti prodotti: <br /><br />" + oversell_ary.join('<br />'));
            } else if(data.status == "ok") {
                if(justCheckValid == 0) {
                    if(data.extra_status.length) {
                        var extra_actions = [];
                        var extra_actions_gifts = [];
                        var alert_str = "";
                        data.extra_status.forEach(function (val) {
                            if(val.type == "buy") {
                                extra_actions.push('<b>' + val.nome + '</b>');
                            } else if(val.type == "gift") {
                                extra_actions_gifts.push('<b>' + val.nome + '</b>');
                            }
                        });

                        if(extra_actions.length != 0) {
                            if(alert_str != "") {
                                alert_str += "<br />";
                            }
                            alert_str += 'I seguenti pacchetti/servizi sono stati applicati correttamente al cliente: <br /> ' + extra_actions.join(', ');
                        }

                        if(extra_actions_gifts.length != 0) {
                            if(alert_str != "") {
                                alert_str += "<br />";
                            }
                            alert_str += 'Il cliente ha acquistato i seguenti pacchetti/servizio omaggio che sono stati applicati correttamente ai rispettivi destinatari: <br /> ' + extra_actions_gifts.join(', ');
                        }

                        bootbox.alert(alert_str, function () {
                            window.location.href = 'view.php?id=' + data.id;
                        });
                    }
                    else
                    {
                        window.location.href = 'view.php?id=' + data.id;
                    }
                } else {
                    justCheckValidFn(data['status']);
                }
            }

            to_return = data.status;
        }
    });

    return to_return;
}

function getDocumentInfo() {
    var infoDocumento = {};
    $("#impostazioni_documento .impostazione_documento, .extra_documento").each(function(i, v){
        var name = $(this).attr('name');
        if(typeof($(this).attr('type')) != 'undefined' && $(this).attr('type') == 'checkbox')
        {
            var value = ($(this).prop('checked') ? 1 : 0);
        }
        else
        {
            var value = $(this).val();
        }
        infoDocumento[name] = value;
    });
    return infoDocumento;
}

function loadDocumentInfo(value) {

    if(!value.length) {
        value = 'default';
    }

    var impostazioni_documento_data = '';
    if($('#json_impostazioni_documento').data('type') == value) {
        impostazioni_documento_data = $('#json_impostazioni_documento').html();
    }

    $.ajax({
        url: "ajax/impostazioni/" + value + ".php",
        type: "POST",
        data: {
            id: $("#record_id").val(),
            settings: impostazioni_documento_data,
            customer_id: $('#cliente').val(),
        },
        async: true,
        success: function(data) {
            $('#impostazioni_documento').html(data);

            var todayDate = new Date().getDate();
            var startDate= new Date(new Date().setDate(todayDate - 60));
            var endDate =  new Date(new Date().setDate(todayDate + 60));
            $('#impostazioni_documento .date').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                startDate : startDate,
                endDate : endDate
            });

        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#tipo_documento').val($('#tipo_documento option').first().val()).change();
        }
    });

    return impostazioni_documento_data;
}

function getSeduteUtilizzate() {
    sedute_utilizzate = [];
    $('.utilizzo_seduta').each(function() {
        if($(this).val() != 0) {
            sedute_utilizzate.push({
                "id": $(this).data('id-servizio'),
                "clean-id": $(this).data('clean-id-servizio'),
                "name": $(this).closest('tr').find('.service_name').html(),
                "qty": $(this).val(),
                "type": $(this).data('tipo-servizio'),
            })
        }
    });

    return sedute_utilizzate;
}

function getRegaliUtilizzati() {
    regali_utilizzati = [];
    $('.utilizzo_regalo').each(function() {
        if($(this).val() != 0) {
            regali_utilizzati.push({
                "id": $(this).data('id-prodotto'),
                "clean-id": $(this).data('clean-id-prodotto'),
                "name": $(this).closest('table').find('.product_name').html(),
                "qty": $(this).val(),
            })
        }
    });

    return regali_utilizzati;
}

function getCart() {
    var carrello = [];
    $("#tabella_articoli tbody tr").each(function(i, v){
        carrello[i] = {};
        $(this).find('input, select').each(function(ii, vv) {
            carrello[i][$(this).attr('name')] = $(this).val();
        });
    });

    carrello.forEach(function (val, i) {
        if(val.nome == "" || val.qty == "" || val.prezzo == "") {
            delete carrello[i];
        }
    });

    return carrello;
}

function getCustomerObject(for_salve, suffix) {
    if(typeof (for_salve) == 'undefined') for_salve = 0;
    if(typeof (suffix) == 'undefined') suffix = "";

    if(for_salve) {

        var customer_id = $('#cliente' + suffix).val();

        customer_object = {
            "pID": customer_id,
            "pNome": $('#customer_info' + suffix).find('#nome').val(),
            "pCognome": $('#customer_info' + suffix).find('#cognome').val(),
            "pDataNascita": $('#customer_info' + suffix).find('#data_nascita').val(),
            "pTelefonoCasa": $('#customer_info' + suffix).find('#telefono_casa').val(),
            "pTelefonoCellulare": $('#customer_info' + suffix).find('#telefono_cellulare').val(),
            "pIndirizzo": $('#customer_info' + suffix).find('#indirizzo').val(),
            "pCitta": $('#customer_info' + suffix).find('#citta').val(),
            "pProvincia": $('#customer_info' + suffix).find('#provincia').val(),
            "pCAP": $('#customer_info' + suffix).find('#cap').val(),
            "pProvenienza": $('#customer_info' + suffix).find('#provenienza').val(),
            "pSesso": $('#customer_info' + suffix).find('#sesso').val(),
            "pEmail": $('#customer_info' + suffix).find('#email').val(),
            "pFatturazione": {
                "ragione_sociale": $('#customer_info' + suffix).find('#ragione_sociale').val(),
                "piva": $('#customer_info' + suffix).find('#piva').val(),
                "cf": $('#customer_info' + suffix).find('#cf').val(),
                "indirizzo": $('#customer_info' + suffix).find('#indirizzo_fatturazione').val(),
                "citta": $('#customer_info' + suffix).find('#citta_fatturazione').val(),
                "provincia": $('#customer_info' + suffix).find('#provincia_fatturazione').val(),
                "cap": $('#customer_info' + suffix).find('#cap_fatturazione').val()
            }
        };
    }
    else {

        var customer_object = {};
        $("#customer_info" + suffix + " .info_cliente input, #customer_info" + suffix + " .info_cliente select").each(function(){
            customer_object[$(this).attr('name')] =  $(this).val();
        });

        customer_object['fatturazione'] = {}
        $("#customer_info" + suffix + " .info_fatturazione input, #customer_info" + suffix + " .info_fatturazione select").each(function(){
            customer_object['fatturazione'][$(this).attr('name')] =  $(this).val();
        });
    }

    return customer_object;
}

function loadCustomerInfo(id) {

    $('#customerInfo #customer_id').val(id);

    $('.changeCustomer').html('Cambia').css('float', 'right');
    $('.editCustomer').show().css('float', 'right');
    $('.createCustomer').show().css('float', 'right');
    $('#cliente').val(id);

    if(id != $('#customer_info').find('[name="id"]').val()) {

        $('#customer_info').find('input').val('');
        $('#customer_info').find('select').val('').change();
        $('#show_customer_credit_logs_modal').html('');

        $('#sedute_effettuate').hide();
        $('#regali_utilizzati').hide();
        $('#sedute_effettuate .ibox-content').html('');
        $('#regali_utilizzati .ibox-content').html('');

        $.ajax({
            url: "ajax/getCustomer.php",
            type: "POST",
            data: {"id": id},
            async: false,
            dataType: "json",
            success: function (data) {

                $('#customerInfo .data').html(data.anteprima).show();

                for (var k in data) {
                    if ($('#customer_info').find('.info_cliente [name="' + k + '"]').length) {
                        $('#customer_info').find('.info_cliente [name="' + k + '"]').val(data[k]).change();
                    }
                }

                var dati_fatturazione = {};

                try {
                    var dati_fatturazione = JSON.parse(data.dati_fatturazione);
                } catch(e) {}

                for (var k in dati_fatturazione) {
                    if ($('#customer_info').find('.info_fatturazione [name="' + k + '"]').length) {
                        $('#customer_info').find('.info_fatturazione [name="' + k + '"]').val(dati_fatturazione[k]);
                    }
                }

                getSeduteCliente();
                getOfflineTrackings();
                getRegaliCliente();
                recomposeBoxDatiDocumento();

                $('#customer_info #saveCustomer').text('Salva & Aggiorna in Anagrafica');
                $('#customer_info #closeCustomer').show();

                $('#paga_tramite_crediti').data('max', data.crediti);
                $('#show_customer_credit_logs_modal').html(data.crediti_history);
                calculateTableSummary();

            }
        });
    }
}

function recomposeBoxDatiDocumento() {
    if($('#tipo_documento').val() == "fattura" || $('#tipo_documento').val() == "ricevuta") {
        loadDocumentInfo($('#tipo_documento').val());
    }
}

function getSeduteCliente() {
    if($('#cliente').val() != "") {
        $.ajax({
            url: "ajax/getSeduteCliente.php",
            type: "POST",
            data: {
                "id": $('#cliente').val(),
                "pCarrello": getCart(),
                "pEventID": $('#event_id').val(),
                "pRecordID": $('#record_id').val(),
            },
            async: true,
            dataType: "json",
            success: function (data) {
                if (data.sedute_disponibili != "") {
                    $('#sedute_effettuate').show();
                    $('#sedute_effettuate .ibox-content').html(data.sedute_disponibili);
                }
            }
        })
    }
}

function getOfflineTrackings() {
    $('#tracking_offline').hide();

    if($('#cliente').val() != "") {
        $.ajax({
            url: "ajax/getOfflineTrackings.php",
            type: "POST",
            data: {
                "id": $('#cliente').val()
            },
            async: true,
            success: function (data) {
                if (data.length) {
                    $('#tracking_offline').show();
                    $('#tracking_offline .ibox-content').html(data);
                }
            }
        })
    }
}

function getRegaliCliente() {
    $.ajax({
        url: "ajax/getRegaliCliente.php",
        type: "POST",
        data: {
            "id": $('#cliente').val(),
            "pRecordID": $('#record_id').val(),
        },
        async: true,
        dataType: "json",
        success: function (data) {
            if (data.regali_disponibili != "") {
                $('#regali_utilizzati').show();
                $('#regali_utilizzati .ibox-content').html(data.regali_disponibili);
            }
        }
    })
}

function fastCustomerSave(suffix) {
    if(typeof (suffix) == 'undefined') suffix = "";

    $.ajax({
        url: "db/saveCustomer.php",
        type: "POST",
        data: getCustomerObject(1, suffix),
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido o è già stata utilizzata da un altro utente!");
            } else if(data.status == "expire_not_valid") {
                bootbox.error("Il formato della data di nascita non è valido. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {

                if(suffix == "") {
                    $('#customer_info').hide();
                    $('.cliente_box').addClass('show_edit');
                }
                bootbox.alert("I dati del cliente sono stati aggiornati.");
                loadCustomerInfo($('#cliente' + suffix).val());
            }
        }
    })
}

function getCustomerSelect(select_last, suffix) {

    if(typeof(select_last) == 'undefined') select_last = false;
    if(typeof(suffix) == 'undefined') suffix = '';

    $.ajax({
        url: "ajax/getCustomersList.php",
        type: "POST",
        data: {},
        async: true,
        dataType: "json",
        success: function(data) {

            var recenti = '';

            recenti += '<optgroup label="Recenti">';
            data[0].forEach(function (user) {

                if(user.dati_fatturazione == null) {
                    user.dati_fatturazione = {};
                }

                recenti += '<option value="' + user.id + '">' + user.nome + ' ' + user.cognome + (typeof(user.dati_fatturazione.ragione_sociale) !== 'undefined' && user.dati_fatturazione.ragione_sociale.length ? ' - ' + user.dati_fatturazione.ragione_sociale : '') + (user.telefono_cellulare.length ? ' (' + user.telefono_cellulare + ')' : '') + '</option>';
            });
            recenti += '</optgroup>';

            var clienti = '';
            if(typeof(data[1]) !== 'undefined' &&  data[1].length) {
                clienti += '<optgroup label="Clienti">';
                data[1].forEach(function (user) {

                    if(user.dati_fatturazione == null) {
                        user.dati_fatturazione = {};
                    }

                    clienti += '<option value="' + user.id + '">' + user.nome + ' ' + user.cognome + (typeof(user.dati_fatturazione.ragione_sociale) !== 'undefined' && user.dati_fatturazione && user.dati_fatturazione.ragione_sociale.length ? ' - ' + user.dati_fatturazione.ragione_sociale : '') + (user.telefono_cellulare.length ? ' (' + user.telefono_cellulare + ')' : '') + '</option>';
                });
                clienti += '</optgroup>';
            }

            if(recenti.length) {
                if($('#cliente' + suffix + ' optgroup[label="Recenti"]').length) {
                    $('#cliente' + suffix + ' optgroup[label="Recenti"]').replaceWith(recenti);
                }
                else {
                    $('#cliente' + suffix).append(recenti);
                }
            }

            if(clienti.length) {
                if($('#cliente' + suffix + ' optgroup[label="Clienti"]').length) {
                    $('#cliente' + suffix + ' optgroup[label="Clienti"]').replaceWith(clienti);
                }
                else {
                    $('#cliente' + suffix).append(clienti);
                }
            }

            $('#cliente' + suffix).trigger("chosen:updated");

            if(select_last) {
                $('#cliente' + suffix).val($('#cliente' + suffix + ' optgroup').eq(0).find('option').first().val()).change();
                $('#cliente' + suffix).trigger("chosen:updated");
            }

        }
    })
}

function composeVariationModal(id, nome) {
    $.ajax({
        url: "ajax/getProductVariations.php",
        type: "POST",
        data: {
            "id": id,
            "type": 'html_select'
        },
        async: true,
        success: function (data) {

            $('#selectVariationModal #id_prodotto').val(id);
            $('#selectVariationModal .product_name').text(nome);
            $('#selectVariationModal .variations_box').html(data);
            $('#selectVariationModal').modal('show');

        }
    });
}

function addProductVariationToCart() {

    var product_id = $('#selectVariationModal #id_prodotto').val();
    var product_name = $('#selectVariationModal .product_name').text();
    var unique_id = product_id;
    var variations = new Array;
    var empty_variations = false;

    $('#selectVariationModal .variations_box select').each(function () {
        variations.push({
            id: $(this).data('id'),
            value: $(this).val()
        });
        if($(this).val() == '') empty_variations = true;
        unique_id += '_' + $(this).data('id') + '_' + $(this).val();
        product_name += ' - ' + $(this).data('name') + ': ' + $(this).find('option:selected').text();
    });

    if(!empty_variations) {
        $.ajax({
            url: "ajax/getProductVariations.php",
            type: 'POST',
            data: {
                id: product_id,
                variations: variations,
                type: 'variation_detail'
            },
            dataType: 'json',
            success: function (data) {

                var numeric_subtotal = 0;

                var imposta = data.imposta;

                if(data.prezzo_promo.shop_price.length) {
                    numeric_subtotal = data.prezzo_promo.no_tax[0] + '.' + data.prezzo_promo.no_tax[1];
                }
                else {
                    numeric_subtotal = data.prezzo.no_tax[0] + '.' + data.prezzo.no_tax[1];
                }

                addArticleToCart(unique_id, 'prodotto_variabile', product_name, numeric_subtotal, imposta);

                $('#selectVariationModal').modal('hide');
            }
        });

    }
}

function addArticleToCart(id, type, name, price, imposta) {
    var $tabella = $('#tabella_articoli');

    if(typeof(imposta) == 'undefined') imposta = 0;

    var update_quantity = false;
    if(type.length && $tabella.find('#' + type + '_' + id).length) {
        update_quantity = true;
        $last_row = $tabella.find('#' + type + '_' + id);
    } else {

        if(!type.length) {
            type = 'custom';
        }

        $last_row = $('#tabella_articoli tr').last();
        if ($last_row.find('[name="nome"]').val().length) {
            $('#tabella_articoli tbody').append($last_row.clone());
            $last_row = $('#tabella_articoli tr').last();
        }

        $last_row.find('.add_as_gift').addClass('btn-outline');
        $last_row.find('[name="gift_to"]').val('');
    }

    $last_row.attr('id', type + '_' + id);

    if(update_quantity) {
        $last_row.find('[name="qty"]').val(parseInt($last_row.find('[name="qty"]').val())+1);
    }
    else
    {
        $last_row.find('[name="nome"]').val(name);
        $last_row.find('[name="qty"]').val(1);
        $last_row.find('[name="prezzo"]').val((price*1).toFixed(2));
        $last_row.find('[name="sconto"]').val(0);
        $last_row.find('[name="imposta"]').val(imposta);
        $last_row.find('[name="type"]').val(type);
        $last_row.find('[name="id"]').val(id);
    }

    calculateTableSummary();
}

function applyOfferToArticle($tr, valore, metodo) {
    $tr.find('[name="sconto"]').val(valore);
    $tr.find('[name="tipo_sconto"]').val(metodo).change();
}

function calculateTableSummary(who_changed, evt) {
    if(typeof(who_changed) == "undefined") {
        who_changed = "";
    }

    if(typeof(evt) == "undefined") {
        evt = "";
    }

    var total = 0;
    var total_paid = 0;
    var crediti_cliente = $('#paga_tramite_crediti').data('max');

    $('#tabella_articoli tbody tr').each(function () {
        var price = parseFloat($(this).find('[name="prezzo"]').val());
        var price_subtotal = parseFloat($(this).find('[name="prezzo_subtotal"]').val());
        var price_paid = parseFloat($(this).find('[name="prezzo_pagato"]').val());
        var qty = $(this).find('[name="qty"]').val();
        var sconto = $(this).find('[name="sconto"]').val();
        var tipo_sconto = $(this).find('[name="tipo_sconto"]').val();
        var imposta = $(this).find('[name="imposta"]').val();

        if($(this).find('[name="gift_to"]').val() != "") {
            $(this).find('.add_as_gift').removeClass('btn-outline');
        }

        if(who_changed != "" && $(who_changed).attr('name') == 'prezzo_subtotal') {
            price = parseFloat(((100*price_subtotal)/((imposta*1)+100))/qty);
        }

        var subtotal = price*qty;

        if(sconto > 0) {
            if(tipo_sconto == '%') {
                subtotal = (subtotal-(sconto*subtotal/100));
            }
            else {
                subtotal = subtotal-sconto;
            }
        }

        var no_tax_subtotal = subtotal;

        var valore_imposta = parseFloat( ((subtotal*imposta)/100).toFixed(2) );

        if(isNaN(subtotal)) subtotal = 0;

        subtotal += valore_imposta;

        subtotal = parseFloat(subtotal.toFixed(2));

        total+=subtotal;

        $(this).find('[name="prezzo_iva_inclusa"]').val((price+(price*imposta)/100).toFixed(2));
        $(this).find('[name="valore_imposta"]').val(valore_imposta);
        $(this).find('[name="subtotal"]').val(subtotal);
        $(this).find('[name="no_tax_subtotal"]').val(no_tax_subtotal.toFixed(2));
        if(($(who_changed).attr('name') != 'prezzo_subtotal' && evt == "keyup") || evt != "keyup") {
            $(this).find('[name="prezzo_subtotal"]').val(subtotal.toFixed(2));
        }

        if(($(who_changed).attr('name') != 'prezzo' && evt == "keyup") || evt != "keyup") {
            $(this).find('[name="prezzo"]').val(price.toFixed(2));
        }

        if($(who_changed).attr('name') != 'prezzo_pagato') {
            $(this).find('[name="prezzo_pagato"]').val(subtotal.toFixed(2));
        }


        total_paid+=$(this).find('[name="prezzo_pagato"]').val()*1;
    });

    if(total_paid < total) {
        $('.credit_variation').html('Al cliente saranno aggiunti <b class="text-danger" id="crediti_detraibili">' + Math.abs((total-total_paid).toFixed(2)) + '<?= CURRENCY_SYMBOL; ?></b> di debito').show();
    } else if(total_paid > total) {
        $('.credit_variation').html('Al cliente saranno aggiunti <b class="text-navy" id="crediti_detraibili">' + Math.abs((total_paid-total).toFixed(2)) + '<?= CURRENCY_SYMBOL; ?></b> di credito').show();
    } else {
        $('.credit_variation').html('').hide();
    }

    var credit_debt_sum = 0;

    if(crediti_cliente > 0) {

        var crediti_detraibili = 0.0;
        if(crediti_cliente > total) {
            crediti_detraibili = total;
        } else {
            crediti_detraibili = crediti_cliente;
        }

        $('#valore_credito_cliente').val(crediti_detraibili);

        $('#customer_credits_box').show();
        $('#customer_credits_box .checkbox_text').html('Paga <b class="text-navy" id="crediti_detraibili">' + crediti_detraibili.toFixed(2) + '<?= CURRENCY_SYMBOL; ?></b> tramite i crediti');
        $('#crediti_cliente').html(parseFloat(crediti_cliente).toFixed(2) + $('#currentCurrencySymbol').val()).attr('class', 'text-navy');
        $('#customer_credits_box .credit_debit_title').html('Credito Cliente');

        if($('#paga_tramite_crediti').prop('checked')) {
            total -= parseFloat(crediti_detraibili);
            credit_debt_sum -= parseFloat(crediti_detraibili);
        }

    } else if(crediti_cliente < 0) {
        $('#valore_credito_cliente').val(crediti_cliente.toFixed(2));
        $('#customer_credits_box').show();
        $('#customer_credits_box .checkbox_text').html('Aggiungi i <b class="text-danger" id="crediti_detraibili">' + Math.abs(crediti_cliente.toFixed(2)) + '<?= CURRENCY_SYMBOL; ?></b> del debito del cliente');
        $('#crediti_cliente').html(Math.abs(crediti_cliente.toFixed(2)) + $('#currentCurrencySymbol').val()).attr('class', 'text-danger');
        $('#customer_credits_box .credit_debit_title').html('Debito Cliente');

        if($('#paga_tramite_crediti').prop('checked')) {
            total += Math.abs(crediti_cliente.toFixed(2));
            credit_debt_sum += Math.abs(crediti_cliente.toFixed(2));
            $('#customer_credits_box').show();
        }

    } else {
        $('#customer_credits_box').hide();
    }

    $('#show_customer_credit_logs').unbind('click');
    $('#show_customer_credit_logs').on('click', function() {
        bootbox.alert({
            message: $('#show_customer_credit_logs_modal').html(),
            hideHeader: true,
            size: 'large'
        });
    })

    $('#totale_resoconto').text(parseFloat(total).toFixed(2) + $('#currentCurrencySymbol').val());
    $('#totale_pagato').text(parseFloat(total_paid+credit_debt_sum).toFixed(2) + $('#currentCurrencySymbol').val());

    getSeduteCliente();
}

function searchInTable($container, id, query) {
    query = query.toLowerCase();

    if(typeof(window.search_call) !== 'undefined' && window.search_call !== false) {
        window.search_call.abort();
    }

    window.search_call = $.ajax({
        url: "ajax/search.php",
        type: "POST",
        data: {
            "type": id,
            "query": query
        },
        async: true,
        dataType: "html",
        success: function (html) {
            $container.html(html);

            $container.find('img').on('error', function (e) {
                var type = $(this).closest('.add_to_cart').data('type');
                $(this).attr('src', $('#baseElementPathAdmin').val() + 'modules/fatturazione/vendite/images/' + type + '.png');
            });
        }
    });

    /*
    $buttons.each(function () {
        var row_text = $(this).text().toLowerCase();
        if(row_text.indexOf(query) > 0 || !query.length) {
            $(this).closest('div').show();
        }
        else $(this).closest('div').hide();
    });
    */
}

function stampaVendita() {

    var tipo = $('#tipo_documento').val();
    var id_cliente = $("#cliente").val();
    var infoCliente = getCustomerObject();
    var infoDocumento = getDocumentInfo();
    var carrello = getCart();
    var sedute_utilizzate = getSeduteUtilizzate();
    var regali_utilizzati = getRegaliUtilizzati();

    var document_data = {};

    if(id_cliente == '') {
        bootbox.error('Non hai selezionato nessun cliente.');
        return false;
    }

    if(!Object.keys(carrello).length && sedute_utilizzate.length == 0 && regali_utilizzati.length == 0 && $('#record_id').val() == "") {
        bootbox.error('Non ci sono prodotti nel carrello, non è stata scalata alcuna seduta o non sono stati scalati regali.');
        return false;
    }

    $('#impostazioni_documento .required').each(function () {
       if($(this).val() == '') $(this).addClass('error').one('focus change input', function () {
           $(this).removeClass('error');
       });
    });

    if($('#impostazioni_documento .required.error').length) {
        bootbox.error('Compila tutte le informazioni del documento richieste.');
        return false;
    }

    if(tipo == 'fattura') {

        var dati_mancanti = false;

        [['piva', 'cf']].forEach(function (key) {

            if(Array.isArray(key)) {
                var entrambi_mancanti = true;
                key.forEach(function (key2) {
                    if (infoCliente.fatturazione[key2].length == 0 && entrambi_mancanti) {
                        $("#customer_info .info_fatturazione").find('[name="' + key2 + '"]').addClass('error').one('focus change input', function () {
                            $('.info_fatturazione [name="' + key2.join('"], .info_fatturazione [name="') + '"]').removeClass('error');
                        });
                    }
                    else {
                        entrambi_mancanti = false;
                    }
                });

                if(entrambi_mancanti) dati_mancanti = true;
            }
            else
            {
                if (infoCliente.fatturazione[key].length == 0) {
                    dati_mancanti = true;
                    $("#customer_info .info_fatturazione").find('[name="' + key + '"]').addClass('error').one('focus change input', function () {
                        $(this).removeClass('error');
                    });
                }
            }
        });

        if(dati_mancanti) {
            bootbox.error('Compila i dati di fatturazione richiesti.');

            if($('#customer_info').css('display') == 'none') {
                $('.cliente_box').removeClass('show_edit');
                $('#customer_info').show().find('.ibox-content').show();
            }

            return false;
        }

        if($('#customer_info').css('display') == 'block') {
            window.customerForceClosed = true;
            $('#closeCustomer').click();
            window.customerForceClosed = false;
        }

        document_data = {
            id_vendita: $('#record_id').val(),
            tipo: 'fattura',
            cliente: id_cliente,
            info_cliente: infoCliente,
            carrello: carrello,
            info_documento: infoDocumento
        };

    }
    else if(tipo == 'ricevuta' || tipo == 'preventivo') {

        var dati_mancanti = false;

        ['nome'].forEach(function (key) {

            if(Array.isArray(key)) {
                var entrambi_mancanti = true;
                key.forEach(function (key2) {
                    if (infoCliente[key2].length == 0 && entrambi_mancanti) {
                        $("#customer_info .info_cliente").find('[name="' + key2 + '"]').addClass('error').one('focus change input', function () {
                            $('.info_cliente [name="' + key2.join('"], .info_fatturazione [name="') + '"]').removeClass('error');
                        });
                    }
                    else {
                        entrambi_mancanti = false;
                    }
                });

                if(entrambi_mancanti) dati_mancanti = true;
            }
            else
            {
                if (infoCliente[key].length == 0) {
                    dati_mancanti = true;
                    $("#customer_info .info_cliente").find('[name="' + key + '"]').addClass('error').one('focus change input', function () {
                        $(this).removeClass('error');
                    });
                }
            }
        });

        if(dati_mancanti) {
            bootbox.error('Compila i dati richiesti relativi al cliente.');

            if($('#customer_info').css('display') == 'none') {
                $('.cliente_box').removeClass('show_edit');
                $('#customer_info').show().find('.ibox-content').show();
            }

            return false;
        }

        if($('#customer_info').css('display') == 'block') {
            $('#closeCustomer').click();
        }

        document_data = {
            id_vendita: $('#record_id').val(),
            tipo: 'ricevuta',
            cliente: id_cliente,
            info_cliente: infoCliente,
            carrello: carrello,
            info_documento: infoDocumento,
        };

        if(tipo == 'ricevuta') {
            document_data.sedute_utilizzate = getSeduteUtilizzate();
        }

    }
    else {
        document_data = {
            id_vendita: $('#record_id').val(),
            tipo: tipo,
            cliente: id_cliente,
            info_cliente: infoCliente,
            carrello: carrello,
            info_documento: infoDocumento,
            sedute_utilizzate: getSeduteUtilizzate(),
        };
    }

    /* APRE IL POPUP PER LA STAMPA/DOWNLOAD DEL DOCUMENTO */
    checkValidity = saveOperation(function (status) {
        if(status === 'ok') {
            printDocument(tipo, document_data);
        }
    });
}

function printDocument(type, data) {

    if(type == 'fattura' || type == 'ricevuta' || type == 'preventivo') {
        $.ajax({
            url: "ajax/stampa/" + type + "/index.php",
            type: "POST",
            data: data,
            async: false,
            dataType: 'json',
            success: function(data) {
                if (data.status == 'ok') {
                    bootbox.alert(type.charAt(0).toUpperCase() + type.slice(1) + ' generata correttamente', function () {
                        $('#impostazioni_documento #source').val(data.source);
                        saveOperation();
                    });
                } else {
                    bootbox.error(data.error);
                    if(data.status == 'invalid_number') {
                        $('#impostazioni_documento [name="numero"]').val(data.number).change();
                    }
                }
            }
        });
    }
    else if(type == 'scontrino' || type == 'scontrino_non_fiscale') {
        $.ajax({
            url: "ajax/stampa/scontrino/index.php",
            type: "POST",
            data: data,
            async: false,
            dataType: 'json',
            success: function(data) {
                if (data.status == 'ok') {
                    if(data.connection_info.method == 'webserver') {
                        printByWebServer(data.connection_info.ip, data.output, data.config_outputs);
                    } else if(data.connection_info.method == 'manual') {
                        saveOperation();
                    }
                } else {
                    bootbox.error(data.error);
                    if(data.status == 'invalid_number') {
                        $('#impostazioni_documento [name="numero"]').val(data.number).change();
                    }
                }
            }
        });
    }
    else if(!type.length) {
        $.ajax({
            url: "ajax/stampa/default/index.php",
            type: "POST",
            data: data,
            async: false,
            dataType: 'json',
            success: function(data) {
                if (data.status == 'ok') {
                    bootbox.alert('Documento salvato correttamente', function () {
                        $('#impostazioni_documento #source').val(data.source);
                        saveOperation();
                    });
                } else {
                    bootbox.error(data.error);
                    if(data.status == 'invalid_number') {
                        $('#impostazioni_documento [name="numero"]').val(data.number).change();
                    }
                }
            }
        });
    }
}

function printByWebServer(ip, data, config_outputs) {
    bootbox.alert("Invio delle configurazioni di stampa in corso...");

    var config_i = 0;
    config_outputs.forEach(function (config_data) {

        sendWebServerCommands(ip, config_data, function (config_result) {
            config_i++;
            if (config_i == config_outputs.length) {
                bootbox.hideAll();
                bootbox.confirm("Stampante inizializzata, proseguire con la stampa?", function (result) {
                    if (result) {
                        bootbox.alert("Invio della stampa in corso...");
                        sendWebServerCommands(ip, data, function (result) {
                            var json_data = xmlToJson(result);
                            var Request = json_data.Service.Request;
                            bootbox.hideAll();

                            if (Request.printerError['#text'] == '0') {
                                bootbox.alert("Stampa ricevuta correttamente dalla stampante.");
                                saveOperation();
                            }
                            else {
                                var error_string = "Si è verificato un errore inaspettato durante la stampa, se il problema persisten contattare il supporto tecnico.";

                                if (Request.paperEnd['#text'] == '1') {
                                    error_string = "La stampante ha esaurito la carta, riprova la stampa dopo averla sostituita.";
                                }
                                else if (Request.coverOpen['#text'] == '1') {
                                    error_string = "Il coperchio della stampante risulta aperto, riprova la stampa dopo averlo chiuso.";
                                }
                                else if (Request.busy['#text'] == '1') {
                                    error_string = "La stampante può effettuare un'operazione alla volta. Attendi il completamento della stampa attuale e riprova.";
                                }
                                bootbox.error(error_string);
                            }
                        });
                    }
                });
            }
        });

    });
}

function sendWebServerCommands(ip, data, return_function) {
    $.ajax({
        url: ip,
        type: "POST",
        data: data,
        async: false,
        dataType: "xml",
        contentType: "application/xml",
        success: function (data) {
            return_function(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            bootbox.hideAll();
            bootbox.error('Non è stato possibile stabilire una connessione con la stampante (IP: <b>' + ip + '</b>). Assicurati che sia connessa, o modifica le <a href="../settings/casse/edit.php?id=' + $('#impostazioni_documento [name="cassa"]').val() + '">impostazioni della stampante</a>.');
        }
    });
}

/* VIEW */
function initView() {
    $(document).ready(function () {

        $('#impostazioni_documento').on('click', '.change_document_type', function (e) {
            e.preventDefault();

            var document_type = $(this).data('target');

            bootbox.confirm("Proseguendo il documento verrà trasformato in '" + (document_type === 'fattura' ? 'Fattura' : (document_type === 'preventivo' ? 'Preventivo' : 'Ricevuta')) + "'. Vuoi continuare?", function (result)
            {
                if (result) {

                    $.ajax({
                        url: "db/updateDocumentType.php",
                        type: "POST",
                        data: {
                            "pID": $('#record_id').val(),
                            "pTarget": document_type
                        },
                        async: false,
                        success: function(data) {
                            if(data == "ok") {
                                bootbox.alert("Il documento è stato convertito in '" + (document_type === 'fattura' ? 'Fattura' : (document_type === 'preventivo' ? 'Preventivo' : 'Ricevuta')) + "'", function () {
                                    window.location.reload();
                                });
                            }
                            else
                            {
                                bootbox.error("Si è verificato un errore durante la modifica del documento");
                            }
                        }
                    });

                }
            });

        });

        $('#sendDocumentViaEmail').on('click', function (e) {
            e.preventDefault();

            bootbox.prompt({
                title: "Vuoi allegare un messaggio insieme all'email?",
                inputType: 'textarea',
                callback: function (result) {
                    if(result !== null) {
                        $.ajax({
                            url: "ajax/sendDocumentViaEmail.php",
                            type: "POST",
                            data: {
                                "pID": $('#record_id').val(),
                                "pMessaggio": result
                            },
                            async: false,
                            success: function(data) {
                                if(data == "ok") {
                                    bootbox.alert("Il documento è stato inviato correttamente via email", function () {});
                                    $('#sendDocumentViaEmail').replaceWith('Documento <span class="text-navy"><i class="fa fa-check"></i> inviato</span> correttamente');
                                } else if(data == "customer_not_found") {
                                    bootbox.error("Non è stato possibile trovare l'indirizzo email del cliente, probabilmente il suo account è stato eliminato.");
                                } else {
                                    bootbox.error("Si è verificato un errore durante l'invio del documento");
                                }
                            }
                        });
                    }
                }
            });
        });

    });
}

/* UTILS */

function isJson(data) {
    try {
        JSON.parse(data);
        return true;
    } catch (e) {
        return false;
    }
}

function xmlToJson(xml) {

    // Create the return object
    var obj = {};

    if (xml.nodeType == 1) { // element
        // do attributes
        if (xml.attributes.length > 0) {
            obj["@attributes"] = {};
            for (var j = 0; j < xml.attributes.length; j++) {
                var attribute = xml.attributes.item(j);
                obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
            }
        }
    } else if (xml.nodeType == 3) { // text
        obj = xml.nodeValue;
    }

    // do children
    if (xml.hasChildNodes()) {
        for(var i = 0; i < xml.childNodes.length; i++) {
            var item = xml.childNodes.item(i);
            var nodeName = item.nodeName;
            if (typeof(obj[nodeName]) == "undefined") {
                obj[nodeName] = xmlToJson(item);
            } else {
                if (typeof(obj[nodeName].push) == "undefined") {
                    var old = obj[nodeName];
                    obj[nodeName] = [];
                    obj[nodeName].push(old);
                }
                obj[nodeName].push(xmlToJson(item));
            }
        }
    }
    return obj;
};
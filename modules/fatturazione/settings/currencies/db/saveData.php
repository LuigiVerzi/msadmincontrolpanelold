<?php
/**
 * MSAdminControlPanel
 * Date: 01/05/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');


$got_main_currency = false;
foreach($_POST['pCurrencies'] as $v) {
    if($v['is_primary'] == "1") {
        $got_main_currency = true;
    }
}

if(!$got_main_currency) {
    echo json_encode(array("status" => "miss_main_currency"));
    die();
}

$MSFrameworkCMS->setCMSData('currencies', $_POST['pCurrencies'], false);

echo json_encode(array("status" => "ok"));
die();
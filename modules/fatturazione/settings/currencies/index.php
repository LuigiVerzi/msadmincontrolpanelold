<?php
/**
 * MSAdminControlPanel
 * Date: 03/06/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$checked_currencies_global = $MSFrameworkCMS->getCMSData('currencies');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <?php
                        $main_currency_id = $MSFrameworkCurrencies->primary_currency_code;
                        $used_currency_ary = array();

                        if($checked_currencies_global) {
                            foreach ($checked_currencies_global as $v) {
                                if ($v['is_primary'] == "1") {
                                    $main_currency_id = $v['code'];
                                }

                                $used_currency_ary[] = $v['code'];
                            }
                        }

                        if(!$used_currency_ary) $used_currency_ary = array($main_currency_id);

                        ?>
                        <h1>Valute</h1>
                        <fieldset>
                            <h2 class="title-divider">Valute in evidenza</h2>
                            <div class="row">
                                <?php foreach($MSFrameworkCurrencies->getCurrenciesDetails() as $currency) {
                                    if(!$currency['is_main']) continue;
                                    ?>
                                    <div class="col-sm-3">
                                        <div class="styled-checkbox form-control">
                                            <label style="width: 100%;">
                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                    <input type="checkbox" id="currency_<?php echo $currency['code'] ?>" class="currency_check" <?php if(in_array($currency['code'], $used_currency_ary)) { echo "checked"; } ?>>
                                                    <i></i>
                                                </div>
                                                <span style="font-weight: normal;"><img src="<?php echo FRAMEWORK_COMMON_CDN ?>img/currencies/<?php echo strtolower($currency['code']) ?>.png" style="width: 25px; margin-right: 5px;" />
                                                    <?php echo $currency['name'] ?> <small style="float: right;" title="Tasso di conversione rispetto l'Euro"><i><?= $currency['eur_rate']; ?></i></small>
                                                </span>
                                            </label>
                                        </div>

                                        <div style="margin: -10px 0 15px;"><input type="radio" name="is_primary" id="primary_<?php echo $currency['code'] ?>" class="is_primary" <?php if($main_currency_id == $currency['code']) { echo "checked"; } ?> /> Valuta principale</div>
                                    </div>
                                <?php } ?>
                            </div>

                            <h2 class="title-divider">Altre valute</h2>
                            <div class="row">
                                <?php foreach($MSFrameworkCurrencies->getCurrenciesDetails() as $currency) {
                                    if($currency['is_main']) continue;
                                    ?>
                                    <div class="col-sm-3">
                                        <div class="styled-checkbox form-control">
                                            <label style="width: 100%;">
                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                    <input type="checkbox" id="currency_<?php echo $currency['code'] ?>" class="currency_check" <?php if(in_array($currency['code'], $used_currency_ary)) { echo "checked"; } ?>>
                                                    <i></i>
                                                </div>
                                                <span style="font-weight: normal;"><img src="<?php echo FRAMEWORK_COMMON_CDN ?>img/currencies/<?php echo strtolower($currency['code']) ?>.png" style="width: 25px; margin-right: 5px;" />
                                                    <?php echo $currency['name'] ?> <small style="float: right;" title="Tasso di conversione rispetto l'Euro"><i><?= $currency['eur_rate']; ?></i></small>
                                                </span>
                                            </label>
                                        </div>

                                        <div style="margin: -10px 0 15px;"><input type="radio" name="is_primary" id="primary_<?php echo $currency['code'] ?>" class="is_primary" <?php if($main_currency_id == $currency['code']) { echo "checked"; } ?> /> Valuta principale</div>
                                    </div>
                                <?php } ?>
                            </div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
function initForm() {
    initClassicTabsEditForm();

    $('.is_primary').on('click', function() {
        var primary_currency = $(this).attr('id').replace("primary", 'currency');
        if($('#' + primary_currency + ":checked").length == "0") {
            $('#' + primary_currency).iCheck('check');
        }
    });

    $('.currency_check').on('ifUnchecked', function(event){
        var primary_currency = $(this).attr('id').replace("currency", 'primary');
        $('#' + primary_currency).attr('checked', false)
    });
}


function moduleSaveFunction() {

    var avail_currencies = new Array();
    $('.currency_check:checked').each(function() {
        
        var tmp_avail_currencies = new Object();
        var cur_id = $(this).attr('id').replace("currency_", '');
        tmp_avail_currencies['code'] = cur_id;
        if($('#primary_' + cur_id + ":checked").length == "1") {
            tmp_avail_currencies['is_primary'] = "1";
        }
        avail_currencies.push(tmp_avail_currencies);
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pCurrencies": avail_currencies
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if (data.status == "miss_main_currency") {
                bootbox.error("E' necessario impostare la valuta principale prima di poter salvare!");
            } else if (data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if (data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
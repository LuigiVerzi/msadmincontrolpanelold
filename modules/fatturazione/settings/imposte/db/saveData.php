<?php
/**
 * MSAdminControlPanel
 * Date: 01/05/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$old_aliquote = array();
if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'fatturazione_imposte'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
    $r_old_data = $MSFrameworkCMS->getCMSData('fatturazione_imposte');

    $old_aliquote = json_decode($r_old_data['aliquote'], true);
}

$final_aliquota = array();
foreach($_POST['pAliquoteData'] as $k => $v) {
    $aliquota_id = $k;
    if(!is_numeric($aliquota_id) && $aliquota_id == "") {
        echo json_encode(array("status" => "mandatory_data_missing"));
        die();
    }

    $aliquota_id = str_replace(",", ".", $aliquota_id);
    if(!strstr($aliquota_id, ".")) {
        $aliquota_id .= ".00";
    }

    if(!is_numeric($aliquota_id)) {
        echo json_encode(array("status" => "aliquota_numeric"));
        die();
    }

    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $v[0], 'oldValue' => $old_aliquote[$aliquota_id][0]),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $final_aliquota[$aliquota_id][0] = $MSFrameworki18n->setFieldValue($old_aliquote[$aliquota_id][0], $v[0]);
    $final_aliquota[$aliquota_id][1] = $v[1];
}

if(count($final_aliquota) == 0) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$array_to_save = array(
    "value" => json_encode(array(
        "aliquote" => json_encode($final_aliquota),
        "price_type" => $_POST['pPriceType'],
        "price_type_shop" => $_POST['pPriceTypeShop'],
        "price_type_cart" => $_POST['pPriceTypeCart'],
        "show_tot_type" => $_POST['pShowTotType'],
        "price_suffix" => $MSFrameworki18n->setFieldValue($r_old_data['price_suffix'], $_POST['pPriceSuffix'])
    )),
    "type" => "fatturazione_imposte"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'fatturazione_imposte'", $stringForDB[0]);
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

/* TODO: DA COLLEGARE UNA VOLTA ELIMINATA LA VECCHIA SEZIONE
if($db_action == "update") {
    $aliquote_eliminate = array_diff_key($old_aliquote, $final_aliquota);
    if (count($aliquote_eliminate) != 0) {
        foreach ($aliquote_eliminate as $aliqK => $aliqV) {
            $MSFrameworkDatabase->pushToDB("UPDATE ecommerce_products SET imposta = '' WHERE imposta = :imposta", array(":imposta" => $aliqK));
        }
    }
}
*/

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
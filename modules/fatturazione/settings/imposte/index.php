<?php
/**
 * MSAdminControlPanel
 * Date: 03/06/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = $MSFrameworkCMS->getCMSData('fatturazione_imposte');
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Impostazioni Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Inserimento prezzi</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eventuali modifiche a questo campo impatteranno sui prodotti già esistenti modificandone il prezzo finale"></i></span>
                                    <select id="price_type" name="price_type" class="form-control">
                                        <option value="0" <?php if($r['price_type'] == "0") { echo "selected"; } ?>>I prezzi sono comprensivi di imposte</option>
                                        <option value="1" <?php if($r['price_type'] == "1") { echo "selected"; } ?>>I prezzi sono al netto di imposta</option>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Visualizzazione negozio</label>
                                    <select id="price_type_shop" name="price_type_shop" class="form-control">
                                        <option value="0" <?php if($r['price_type_shop'] == "0") { echo "selected"; } ?>>Prezzi imposte incluse</option>
                                        <option value="1" <?php if($r['price_type_shop'] == "1") { echo "selected"; } ?>>Prezzi imposte escluse</option>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Visualizzazione carrello</label>
                                    <select id="price_type_cart" name="price_type_cart" class="form-control">
                                        <option value="0" <?php if($r['price_type_cart'] == "0") { echo "selected"; } ?>>Prezzi imposte incluse</option>
                                        <option value="1" <?php if($r['price_type_cart'] == "1") { echo "selected"; } ?>>Prezzi imposte escluse</option>
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Totale imposte</label>
                                    <select id="show_tot_type" name="show_tot_type" class="form-control">
                                        <option value="0" <?php if($r['show_tot_type'] == "0") { echo "selected"; } ?>>Mostra come totale unico</option>
                                        <option value="1" <?php if($r['show_tot_type'] == "1") { echo "selected"; } ?>>Mostra per singolo oggetto</option>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Suffisso prezzo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Definisce un testo da visualizzare dopo il prezzo (es: IVA Inclusa)"></i></span> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['price_suffix']) ?>"></i></span>
                                    <input id="price_suffix" name="price_suffix" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['price_suffix'])) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                        </fieldset>

                        <h1>Impostazioni Aliquote</h1>
                        <fieldset>
                            <div class="easyRowDuplicationContainer">
                                <?php
                                $r_aliquote = json_decode($r['aliquote'], true);
                                if(count($r_aliquote) == 0) {
                                    $r_aliquote[] = array('', '1');
                                }

                                foreach($r_aliquote as $dataK => $dataV) {
                                    ?>
                                    <div class="stepsDuplication rowDuplication aliquoteDuplication">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <label>Aliquota %*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci l'importo dell'aliquota fiscale"></i></span>
                                                <input type="text" class="form-control percent_aliquota" value="<?php echo htmlentities($dataK) ?>">
                                            </div>

                                            <div class="col-sm-8">
                                                <label>Nome Aliquota</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci il nome dell'aliquota che sarà mostrato al cliente (es: IVA 22%)"></i></span>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($dataV[0]) ?>"></i></span>
                                                <input type="text" class="form-control name_aliquota" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($dataV[0])) ?>">
                                            </div>

                                            <div class="col-sm-2">
                                                <label>&#8205;</label>
                                                <div class="styled-checkbox form-control">
                                                    <label style="width: 100%;">
                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                            <input type="radio" name="imposteRadio" class="set_default" <?php if($dataV[1] == "1") { echo "checked"; } ?>>
                                                            <i></i>
                                                        </div>
                                                        <span style="font-weight: normal;">Imposta Predefinita</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
function initForm() {
    initClassicTabsEditForm();

    $('.rowDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi aliquota",
        "deleteButtonText": "Elimina aliquota",
        "beforeAddCallback": function() {
            $('.rowDuplication').find('.set_default').iCheck('destroy');
        },
        "afterAddCallback": function() {
            $('.rowDuplication:last').find('.percent_aliquota').val('');
            $('.rowDuplication:last').find('.name_aliquota').val('');
            $('.rowDuplication:last').find('.set_default').iCheck('uncheck');

            initIChecks();
        }
    });

}


function moduleSaveFunction() {
    var data_ary = new Object();
    $('.stepsDuplication.aliquoteDuplication').each(function(row_index) {
        var data_val = $(this).find('.percent_aliquota').val();
        if(data_val == "") {
            return true;
        }

        var data_ary_tmp = [$(this).find('.name_aliquota').val(), ($(this).find('.set_default').prop('checked') ? '1' : '0')];
        data_ary[data_val] = data_ary_tmp;
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pPriceType": $('#price_type').val(),
            "pPriceTypeShop": $('#price_type_shop').val(),
            "pPriceTypeCart": $('#price_type_cart').val(),
            "pShowTotType": $('#show_tot_type').val(),
            "pPriceSuffix": $('#price_suffix').val(),
            "pAliquoteData": data_ary
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "aliquota_numeric") {
                bootbox.error("Il campo 'aliquota' deve contenere un valore numerico");
            } else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
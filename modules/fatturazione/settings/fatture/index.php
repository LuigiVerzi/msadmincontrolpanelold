<?php
/**
 * MSAdminControlPanel
 * Date: 03/06/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$invoice_data = $MSFrameworkCMS->getCMSData('fatturazione_fatture');

$ecommerce_data = array();
if($invoice_data['ecommerce'] && json_decode($invoice_data['ecommerce'])) {
    $ecommerce_data = json_decode($invoice_data['ecommerce'], true);
}

$r_pagamenti_web = json_decode($invoice_data['webPayments'], true);

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-7">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-5">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Dati Generali</h1>
                        <fieldset>

                            <h2 class="title-divider">Impostazioni Fatture</h2>

                            <div class="row">
                                <?php
                                $enable_invoices_check = "";
                                if($invoice_data['enable'] == 1) {
                                    $enable_invoices_check = "checked";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_invoices" <?php echo $enable_invoices_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Fatturazione</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div id="invoices_settings" style="display: <?php echo ($invoice_data['enable'] == 1) ? 'block' : 'none' ?>;">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Numero attuale fattura</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se noti dei conflitti riguardo il numero identificato delle fatture puoi reimpostare qui l'ID attuale."></i></span>
                                        <input type="text" class="form-control" data-y="<?= date('Y'); ?>" id="invoice_current_number" value="<?php echo intval($invoice_data['invoice_current_number']) ?>">
                                        <br>
                                        <label>Prefisso</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Definisce un testo da visualizzare prima del numero progressivo della fattura (es. EC-)"></i></span>
                                        <input id="invoice_prefix" name="invoice_prefix" type="text" class="form-control" value="<?php echo htmlentities($invoice_data['prefix']) ?>">
                                    </div>
                                    <div class="col-sm-9">
                                        <label>Note da allegare</label><span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Aggiunge delle note aggiuntive nel footer di ogni fattura"></i></span> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($invoice_data['attach_note']) ?>"></i></span>
                                        <textarea id="attach_note" class="form-control" rows="5" style="max-height: 111px;"><?php echo $MSFrameworki18n->getFieldValue($invoice_data['attach_note']) ?></textarea>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="row">
                                    <div class="col-sm-12 logo_fattura">
                                        <label>Logo Fattura</label>
                                        <?php (new \MSFramework\uploads('FATTURAZIONE_FATTURE'))->initUploaderHTML("logo_fattura", $invoice_data['logo_fattura']); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <?php if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) { ?>
                            <h1>Fatture Ecommerce</h1>
                            <fieldset>
                                <h2 class="title-divider">Fatture Ecommerce</h2>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Invia la fattura al cliente</label>
                                        <select id="invoice_when_to_send" name="invoice_when_to_send" class="form-control">
                                            <option value="0" <?php if($ecommerce_data['when_to_send'] == "0") { echo "selected"; } ?>>Manualmente, dal modulo ordini</option>
                                            <option value="1" <?php if($ecommerce_data['when_to_send'] == "1") { echo "selected"; } ?>>Automaticamente, al momento del pagamento</option>
                                            <option value="2" <?php if($ecommerce_data['when_to_send'] == "2") { echo "selected"; } ?>>Automaticamente, al momento della spedizione</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-4">
                                        <label>Formato allegato email</label>
                                        <select id="invoice_attachment_format" name="invoice_attachment_format" class="form-control">
                                            <option value="0" <?php if($ecommerce_data['attachment_format'] == "0") { echo "selected"; } ?>>PDF</option>
                                            <option value="1" <?php if($ecommerce_data['attachment_format'] == "1") { echo "selected"; } ?>>HTML</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Invia una copia</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserire il proprio indirizzo email se si desidera ricevere una copia della fattura."></i></span>
                                        <input type="text" class="form-control" id="invoice_mail_cc" value="<?php echo htmlentities($ecommerce_data['mail_cc']) ?>">
                                    </div>
                                </div>
                            </fieldset>
                        <?php } ?>

                        <h1>Pagamenti Fisici</h1>
                        <fieldset>
                            <h2 class="title-divider">Metodi di Pagamento</h2>
                            <div class="easyRowDuplicationContainer">
                                <?php
                                $r_metodi_pagamento = json_decode($invoice_data['payMethods'], true);
                                if(count($r_metodi_pagamento) == 0) {
                                    $r_metodi_pagamento[] = array();
                                }

                                foreach($r_metodi_pagamento as $dataK => $dataV) {
                                    ?>
                                    <div class="stepsDuplication rowDuplication metodiPagamentoDuplication">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <label>Nome Metodo di Pagamento</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($dataV[0]) ?>"></i></span>
                                                <input type="text" class="form-control pay_method_name" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($dataV['name'])) ?>">
                                            </div>

                                            <div class="col-sm-3">
                                                <?php
                                                $pay_method_change_check = "";
                                                if($dataV['manage_change'] == 1) {
                                                    $pay_method_change_check = "checked";
                                                }
                                                ?>
                                                <label>&nbsp;</label>
                                                <div class="styled-checkbox form-control">
                                                    <label style="width: 100%;">
                                                        <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                            <input type="checkbox" class="pay_method_change" <?php echo $pay_method_change_check ?>>
                                                            <i></i>
                                                        </div>
                                                        <span style="font-weight: normal;">Gestisce resto</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </fieldset>

                        <h1>Pagamenti Web</h1>
                        <fieldset>

                            <div class="ibox <?php if($r_pagamenti_web['enable_bonifico'] != "1") { echo "collapsed"; } ?> webPaymentBox single_uploader_settings">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -10px 10px 0 -3px;">
                                        <input type="checkbox" id="enable_bonifico" class="webPayments_settings" <?php if($r_pagamenti_web['enable_bonifico'] == "1") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Abilita Bonifico Bancario
                                    </h5>
                                </div>
                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['bonifico_data']['descrizione']) ?>"></i></span>
                                            <textarea id="descr_bonifico" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['bonifico_data']['descrizione']) ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Istruzioni</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['bonifico_data']['istruzioni']) ?>"></i></span>
                                            <textarea id="istr_bonifico" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['bonifico_data']['istruzioni']) ?></textarea>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Intestatario Conto</label>
                                            <input id="intestatario_bonifico" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['bonifico_data']['intestatario']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Numero Conto</label>
                                            <input id="conto_bonifico" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['bonifico_data']['conto']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Nome Banca</label>
                                            <input id="banca_bonifico" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['bonifico_data']['banca']) ?>">
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Filiale</label>
                                            <input id="filiale_bonifico" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['bonifico_data']['filiale']) ?>">
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label>IBAN</label>
                                            <input id="iban_bonifico" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['bonifico_data']['iban']) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>BIC / Swift</label>
                                            <input id="bic_swift_bonifico" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['bonifico_data']['bic_swift']) ?>">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="ibox <?php if($r_pagamenti_web['enable_postepay'] != "1") { echo "collapsed"; } ?> webPaymentBox single_uploader_settings">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -10px 10px 0 -3px;">
                                        <input type="checkbox" id="enable_postepay" class="webPayments_settings" <?php if($r_pagamenti_web['enable_postepay'] == "1") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Abilita PostePay
                                    </h5>
                                </div>
                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['postepay_data']['descrizione']) ?>"></i></span>
                                            <textarea id="descr_postepay" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['postepay_data']['descrizione']) ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Istruzioni</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['postepay_data']['istruzioni']) ?>"></i></span>
                                            <textarea id="istr_postepay" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['postepay_data']['istruzioni']) ?></textarea>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>Intestatario Carta</label>
                                            <input id="intestatario_postepay" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['postepay_data']['intestatario']) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>Numero Carta</label>
                                            <input id="numero_postepay" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['postepay_data']['numero']) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>C.F. Intestatario</label>
                                            <input id="cf_postepay" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['postepay_data']['cf']) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="ibox <?php if($r_pagamenti_web['enable_contrassegno'] != "1") { echo "collapsed"; } ?> webPaymentBox single_uploader_settings">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -10px 10px 0 -3px;">
                                        <input type="checkbox" id="enable_contrassegno" class="webPayments_settings" <?php if($r_pagamenti_web['enable_contrassegno'] == "1") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Abilita Contrassegno
                                    </h5>
                                </div>
                                <div class="ibox-content">

                                    <?php if($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) { ?>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Abilita per i metodi di spedizione</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Se il contrassegno è disponibile solo per alcuni metodi di spedizione, specificali qui. Lascia vuoto per tutti i metodi di spedizione."></i></span>
                                                <select id="contrassegno_ship_methods" class="form-control" multiple="multiple" width="100%">
                                                    <?php
                                                    $limit_to_shipments = json_decode($r_pagamenti_web['contrassegno_data']['limit_to_shipments'], true);
                                                    foreach((new \MSFramework\Ecommerce\shipping())->getShippingDetails('', "id, nome") as $shipmentK => $shipmentV) {
                                                        ?>
                                                        <option value="<?php echo $shipmentV['id'] ?>" <?php if(in_array($shipmentV['id'], $limit_to_shipments)) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($shipmentV['nome'], true) ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                    <?php } ?>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['contrassegno_data']['descrizione']) ?>"></i></span>
                                            <textarea id="descr_contrassegno" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['contrassegno_data']['descrizione']) ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Istruzioni</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['contrassegno_data']['istruzioni']) ?>"></i></span>
                                            <textarea id="istr_contrassegno" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['contrassegno_data']['istruzioni']) ?></textarea>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label>Applica Costo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="È possibile impostare una tassa da applicare ai clienti che usano questo metodo di pagamento"></i></span>
                                            <input id="rincaro_contrassegno" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['contrassegno_data']['rincaro']) ?>">
                                        </div>
                                        <div class="col-xs-1">
                                            <label> &nbsp; </label>
                                            <select id="rincaro_tipo_contrassegno" type="text" class="form-control">
                                                <option <?php echo $r_pagamenti_web['contrassegno_data']['rincaro_tipo'] == CURRENCY_SYMBOL ? 'selected' : '' ?>><?= CURRENCY_SYMBOL; ?></option>
                                                <option <?php echo $r_pagamenti_web['contrassegno_data']['rincaro_tipo'] == '%' ? 'selected' : '' ?>>%</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <label> Applica Aliquota al Rincaro </label>
                                            <select id="rincaro_aliquota_contrassegno" type="text" class="form-control">
                                                <option value="default">Usa impostazioni Predefinite</option>
                                                <option value="">- Nessuna imposta -</option>
                                                <?php
                                                $num_imp = count((new \MSFramework\Ecommerce\imposte())->getImposte());
                                                foreach((new \MSFramework\Ecommerce\imposte())->getImposte() as $impK => $impV) {
                                                    ?>
                                                    <option value="<?php echo $impK ?>" <?php if($r['imposta'] == $impK || $num_imp == 1) { echo "selected"; } ?>><?php echo $MSFrameworki18n->getFieldValue($impV[0], true) ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="ibox <?php if($r_pagamenti_web['enable_paypal'] != "1") { echo "collapsed"; } ?> webPaymentBox single_uploader_settings">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -10px 10px 0 -3px;">
                                        <input type="checkbox" id="enable_paypal" class="webPayments_settings" <?php if($r_pagamenti_web['enable_paypal'] == "1") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Abilita PayPal
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>Email PayPal</label>
                                            <input id="email_paypal" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['paypal_data']['email']) ?>">
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['paypal_data']['descrizione']) ?>"></i></span>
                                            <textarea id="descr_paypal" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['paypal_data']['descrizione']) ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Istruzioni</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['paypal_data']['istruzioni']) ?>"></i></span>
                                            <textarea id="istr_paypal" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['paypal_data']['istruzioni']) ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>



                            <div class="ibox <?php if($r_pagamenti_web['enable_nexi'] != "1") { echo "collapsed"; } ?> webPaymentBox single_uploader_settings">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -10px 10px 0 -3px;">
                                        <input type="checkbox" id="enable_nexi" class="webPayments_settings" <?php if($r_pagamenti_web['enable_nexi'] == "1") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Abilita Nexi
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <label>Alias Nexi</label>
                                            <input id="alias_nexi" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['nexi_data']['alias']) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>Secret Key</label>
                                            <input id="secret_key_nexi" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['nexi_data']['secret_key']) ?>">
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['nexi_data']['descrizione']) ?>"></i></span>
                                            <textarea id="descr_nexi" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['nexi_data']['descrizione']) ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Istruzioni</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['nexi_data']['istruzioni']) ?>"></i></span>
                                            <textarea id="istr_nexi" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['nexi_data']['istruzioni']) ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="ibox <?php if($r_pagamenti_web['enable_stripe'] != "1") { echo "collapsed"; } ?> webPaymentBox single_uploader_settings">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -10px 10px 0 -3px;">
                                        <input type="checkbox" id="enable_stripe" class="webPayments_settings" <?php if($r_pagamenti_web['enable_stripe'] == "1") { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                        Abilita Stripe
                                    </h5>
                                </div>
                                <div class="ibox-content">

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <label>Publishable Key</label>
                                            <input id="publishable_key_stripe" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['stripe_data']['publishable_key']) ?>">
                                        </div>

                                        <div class="col-sm-4">
                                            <label>Secret Key</label>
                                            <input id="secret_key_stripe" type="text" class="form-control" value="<?php echo htmlentities($r_pagamenti_web['stripe_data']['secret_key']) ?>">
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Descrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['stripe_data']['descrizione']) ?>"></i></span>
                                            <textarea id="descr_stripe" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['stripe_data']['descrizione']) ?></textarea>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Istruzioni</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r_pagamenti_web['stripe_data']['istruzioni']) ?>"></i></span>
                                            <textarea id="istr_stripe" class="form-control" rows="5"><?php echo $MSFrameworki18n->getFieldValue($r_pagamenti_web['stripe_data']['istruzioni']) ?></textarea>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <label>Logo (Quadrato)</label>
                                    <?php (new \MSFramework\uploads('ECOMMERCE_PAYMENTS_LOGOS'))->initUploaderHTML("stripe_logo", ($r_pagamenti_web['stripe_data']['logo'] ? $r_pagamenti_web['stripe_data']['logo'] : json_encode(array()) )); ?>

                                </div>
                            </div>

                        </fieldset>

                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
function initForm() {
    initClassicTabsEditForm();
    initOrak('logo_fattura', 1, 500, 300, 200, getOrakImagesToPreattach('logo_fattura'), false, ['image/jpeg', 'image/png']);
    if($('#stripe_logo').length) {
        initOrak('stripe_logo', 1, 40, 40, 100, getOrakImagesToPreattach('stripe_logo'), false, ['image/jpeg', 'image/png', 'image/svg+xml']);
    }

    $('#enable_invoices').on('ifToggled', function(event){
        if($(this).is(':checked')){
            $('#invoices_settings').show();
            $('form.tabcontrol > .steps > ul > li:not(.current)').show();
        } else {
            $('#invoices_settings').hide();
            $('form.tabcontrol > .steps > ul > li:not(.current)').hide();
        }
    });

    if(!$('#enable_invoices').is(':checked')) {
        $('form.tabcontrol > .steps > ul > li:not(.current)').hide();
    }

    $("#invoice_current_number").TouchSpin({
        min: 0,
        step: 1,
        decimals: 0,
        prefix: $("#invoice_current_number").data('y'),
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });

    $('.rowDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi metodo",
        "deleteButtonText": "Elimina metodo",
        "beforeAddCallback": function() {
            $('.metodiPagamentoDuplication').find('.pay_method_change').iCheck('destroy');
        },
        "afterAddCallback": function() {
            $('.rowDuplication:last').find('.pay_method_name').val('');
            $('.metodiPagamentoDuplication:last').find('.pay_method_change').attr('checked', false);

            initIChecks();
        }
    });

    // PAGAMENTI WEB
    $('.webPaymentBox .webPayments_settings').on('ifChanged', function (event) {
        if($(this).is(':checked')) {
            $(this).closest('.webPaymentBox').removeClass('collapsed');
        }
        else {
            $(this).closest('.webPaymentBox').addClass('collapsed');
        }
    });
}

function moduleSaveFunction() {
    var ecommerce_fields = {};

    if($('#invoice_mail_cc').length) {
        ecommerce_fields = {
            'when_to_send': $('#invoice_when_to_send').val(),
            'attachment_format': $('#invoice_attachment_format').val(),
            'mail_cc': $('#invoice_mail_cc').val()
        };
    }

    var pay_methods_ary = new Array();
    $('.stepsDuplication.metodiPagamentoDuplication').each(function(row_index) {
        if($(this).find('.pay_method_name').val() == "") {
            return true;
        }

        pay_methods_ary_tmp = new Object();

        pay_methods_ary_tmp['name'] = $(this).find('.pay_method_name').val();
        pay_methods_ary_tmp['manage_change'] = $(this).find('.pay_method_change:checked').length;

        pay_methods_ary.push(pay_methods_ary_tmp);
    });

    var web_payments_ary = {
        "pEnableCoupons": $('#enable_coupons:checked').length,
        "pGuestCheckout": $('#enable_guest_checkout:checked').length,

        "pEnableBonifico": $('#enable_bonifico:checked').length,
        "pDescrBonifico": $('#descr_bonifico').val(),
        "pIstrBonifico": $('#istr_bonifico').val(),
        "pIntestatarioBonifico": $('#intestatario_bonifico').val(),
        "pContoBonifico": $('#conto_bonifico').val(),
        "pBancaBonifico": $('#banca_bonifico').val(),
        "pFilialeBonifico": $('#filiale_bonifico').val(),
        "pIBANBonifico": $('#iban_bonifico').val(),
        "pBICBonifico": $('#bic_swift_bonifico').val(),

        "pEnablePostepay": $('#enable_postepay:checked').length,
        "pDescrPostepay": $('#descr_postepay').val(),
        "pIstrPostepay": $('#istr_postepay').val(),
        "pIntestatarioPostepay": $('#intestatario_postepay').val(),
        "pNumeroPostepay": $('#numero_postepay').val(),
        "pCFPostepay": $('#cf_postepay').val(),

        "pEnableContrassegno": $('#enable_contrassegno:checked').length,
        "pDescrContrassegno": $('#descr_contrassegno').val(),
        "pIstrContrassegno": $('#istr_contrassegno').val(),
        "pShipMethodsContrassegno": $('#contrassegno_ship_methods').val(),
        "pRincaroContrassegno": $('#rincaro_contrassegno').val(),
        "pTipoRincaroContrassegno": $('#rincaro_tipo_contrassegno').val(),
        "pAliquotaRincaroContrassegno": $('#rincaro_aliquota_contrassegno').val(),

        "pEnablePayPal": $('#enable_paypal:checked').length,
        "pDescrPayPal": $('#descr_paypal').val(),
        "pIstrPayPal": $('#istr_paypal').val(),
        "pEmailPayPal": $('#email_paypal').val(),

        "pEnableNexi": $('#enable_nexi:checked').length,
        "pDescrNexi": $('#descr_nexi').val(),
        "pIstrNexi": $('#istr_nexi').val(),
        "pAliasNexi": $('#alias_nexi').val(),
        "pSecretKeyNexi": $('#secret_key_nexi').val(),

        "pEnableStripe": $('#enable_stripe:checked').length,
        "pDescrStripe": $('#descr_stripe').val(),
        "pIstrStripe": $('#istr_stripe').val(),
        "pLogoStripe": composeOrakImagesToSave('stripe_logo'),
        "pPublishableKeyStripe": $('#publishable_key_stripe').val(),
        "pSecretKeyStripe": $('#secret_key_stripe').val(),
    }

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            'enable': $('#enable_invoices:checked').length,
            'attach_note': $('#attach_note').val(),
            'prefix': $('#invoice_prefix').val(),
            'when_to_send': $('#invoice_when_to_send').val(),
            'attachment_format': $('#invoice_attachment_format').val(),
            'mail_cc': $('#invoice_mail_cc').val(),
            'invoice_current_number': $('#invoice_current_number').val(),
            'logo_fattura': composeOrakImagesToSave('logo_fattura'),
            'ecommerce': ecommerce_fields,
            'payMethods': pay_methods_ary,
            'webPayments': web_payments_ary
        },
        async: false,
        dataType: "json",
        success: function(data) {
            // CONTROLLI GENERALI
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            }
            else if(data.status == "mail_not_valid") {
                bootbox.error("La mail inserita è in un formato non valido!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            }  else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            }
            // CONTROLLI PAGAMENTO WEB
            else if(data.status == "mandatory_bonifico") {
                bootbox.error("Se abiliti il pagamento con Bonifico Bancario, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_contrassegno") {
                bootbox.error("Se abiliti il pagamento con Contrassegno, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_postepay") {
                bootbox.error("Se abiliti il pagamento con PostePay, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "mandatory_paypal") {
                bootbox.error("Se abiliti il pagamento con PayPal, devi compilare tutti i dati della relativa scheda!");
            }  else if(data.status == "mandatory_nexi") {
                bootbox.error("Se abiliti il pagamento con Nexi, devi compilare tutti i dati della relativa scheda!");
            } else if(data.status == "paypal_mail_not_valid") {
                bootbox.error("La mail inserita all'interno della scheda PayPal è in un formato non valido!");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
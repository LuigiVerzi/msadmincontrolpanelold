<?php
/**
 * MSAdminControlPanel
 * Date: 01/05/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$old_aliquote = array();
if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'fatturazione_fatture'") == 0) {
    $db_action = "insert";
} else {
    $db_action = "update";
    $r_old_data = $MSFrameworkCMS->getCMSData('fatturazione_fatture');

    $old_metodi_pagamento = json_decode($r_old_data['payMethods'], true);
    $old_pagamenti_web = json_decode($r_old_data['webPayments'], true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['attach_note'], 'oldValue' => $r_old_data['attach_note'])
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

if($_POST['mail_cc'] != "" && !filter_var($_POST['mail_cc'], FILTER_VALIDATE_EMAIL)) {
    echo json_encode(array("status" => "mail_not_valid"));
    die();
}

$uploader = new \MSFramework\uploads('FATTURAZIONE_FATTURE');
$ary_files = $uploader->prepareForSave($_POST['logo_fattura']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$_POST['attach_note'] = $MSFrameworki18n->setFieldValue($r_old_data['attach_note'], $_POST['attach_note']);


$gotNames = array();
foreach($_POST['payMethods'] as $kField => $vField) {
    if(in_array($vField['name'], $gotNames)) {
        continue; //i nomi dei campi devono essere univoci! se ne esiste uno uguale, lo salto!
    }

    $gotNames[] = $vField['name'];

    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $vField['name'], 'oldValue' => $old_metodi_pagamento[$kField]['name']),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $_POST['payMethods'][$kField]['name'] =  $MSFrameworki18n->setFieldValue($old_metodi_pagamento[$kField]['name'], $vField['name']);
}

if($_POST['webPayments']) {
    if($_POST['webPayments']['pEnableBonifico'] == "1" && ($_POST['webPayments']['pDescrBonifico'] == "" || $_POST['webPayments']['pIstrBonifico'] == "" || $_POST['webPayments']['pIntestatarioBonifico'] == "" || $_POST['webPayments']['pContoBonifico'] == "" || $_POST['webPayments']['pBancaBonifico'] == "" || $_POST['webPayments']['pFilialeBonifico'] == "" || $_POST['webPayments']['pIBANBonifico'] == "" || $_POST['webPayments']['pBICBonifico'] == "")) {
        echo json_encode(array("status" => "mandatory_bonifico"));
        die();
    }

    if($_POST['webPayments']['pEnableContrassegno'] == "1" && ($_POST['webPayments']['pDescrContrassegno'] == "" || $_POST['webPayments']['pIstrContrassegno'] == "")) {
        echo json_encode(array("status" => "mandatory_contrassegno"));
        die();
    }

    if($_POST['webPayments']['pEnablePostepay'] == "1" && ($_POST['webPayments']['pDescrPostepay'] == "" || $_POST['webPayments']['pIstrPostepay'] == "" || $_POST['webPayments']['pIntestatarioPostepay'] == "" || $_POST['webPayments']['pNumeroPostepay'] == "" || $_POST['webPayments']['pCFPostepay'] == "")) {
        echo json_encode(array("status" => "mandatory_postepay"));
        die();
    }

    if($_POST['webPayments']['pEnablePayPal'] == "1" && ($_POST['webPayments']['pDescrPayPal'] == "" || $_POST['webPayments']['pIstrPayPal'] == "" || $_POST['webPayments']['pEmailPayPal'] == "")) {
        echo json_encode(array("status" => "mandatory_paypal"));
        die();
    }

    if(!filter_var($_POST['webPayments']['pEmailPayPal'], FILTER_VALIDATE_EMAIL) && $_POST['webPayments']['pEmailPayPal'] != "") {
        echo json_encode(array("status" => "paypal_mail_not_valid"));
        die();
    }

    if($_POST['webPayments']['pEnableNexi'] == "1" && ($_POST['webPayments']['pDescrNexi'] == "" || $_POST['webPayments']['pIstrNexi'] == "" || $_POST['webPayments']['pAliasNexi'] == "" || $_POST['webPayments']['pSecretKeyNexi'] == "")) {
        echo json_encode(array("status" => "mandatory_nexi"));
        die();
    }

    if($_POST['webPayments']['pEnableStripe'] == "1" && ($_POST['webPayments']['pDescrStripe'] == "" || $_POST['webPayments']['pIstrStripe'] == "" || $_POST['webPayments']['pPublishableKeyStripe'] == "" || $_POST['webPayments']['pSecretKeyStripe'] == "")) {
        echo json_encode(array("status" => "mandatory_stripe"));
        die();
    }

    $uploader = new \MSFramework\uploads('ECOMMERCE_PAYMENTS_LOGOS');
    $stripe_logo = $uploader->prepareForSave($_POST['webPayments']['pLogoStripe'], array("png", "jpeg", "jpg", "svg"));
    if($stripe_logo === false) {
        echo json_encode(array("status" => "mv_error"));
        die();
    }

    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $_POST['webPayments']['pDescrBonifico'], 'oldValue' => $old_pagamenti_web['bonifico_data']['descrizione']),
            array('currentValue' => $_POST['webPayments']['pIstrBonifico'], 'oldValue' => $old_pagamenti_web['bonifico_data']['istruzioni']),
            array('currentValue' => $_POST['webPayments']['pDescrPostepay'], 'oldValue' => $old_pagamenti_web['postepay_data']['descrizione']),
            array('currentValue' => $_POST['webPayments']['pIstrPostepay'], 'oldValue' => $old_pagamenti_web['postepay_data']['istruzioni']),
            array('currentValue' => $_POST['webPayments']['pDescrContrassegno'], 'oldValue' => $old_pagamenti_web['contrassegno_data']['descrizione']),
            array('currentValue' => $_POST['webPayments']['pIstrContrassegno'], 'oldValue' => $old_pagamenti_web['contrassegno_data']['istruzioni']),
            array('currentValue' => $_POST['webPayments']['pDescrPayPal'], 'oldValue' => $old_pagamenti_web['paypal_data']['descrizione']),
            array('currentValue' => $_POST['webPayments']['pIstrPayPal'], 'oldValue' => $old_pagamenti_web['paypal_data']['istruzioni']),
            array('currentValue' => $_POST['webPayments']['pDescrNexi'], 'oldValue' => $old_pagamenti_web['nexi_data']['descrizione']),
            array('currentValue' => $_POST['webPayments']['pIstrNexi'], 'oldValue' => $old_pagamenti_web['nexi_data']['istruzioni']),
            array('currentValue' => $_POST['webPayments']['pDescrStripe'], 'oldValue' => $old_pagamenti_web['stripe_data']['descrizione']),
            array('currentValue' => $_POST['webPayments']['pIstrStripe'], 'oldValue' => $old_pagamenti_web['stripe_data']['istruzioni']),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $_POST['webPayments'] = array(
        "enable_coupons" => $_POST['webPayments']['pEnableCoupons'],
        "enable_guest_checkout" => $_POST['webPayments']['pGuestCheckout'],
        "enable_bonifico" => $_POST['webPayments']['pEnableBonifico'],
        "bonifico_data" => array(
            "descrizione" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['bonifico_data']['descrizione'], $_POST['webPayments']['pDescrBonifico']),
            "istruzioni" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['bonifico_data']['istruzioni'], $_POST['webPayments']['pIstrBonifico']),
            "intestatario" => $_POST['webPayments']['pIntestatarioBonifico'],
            "conto" => $_POST['webPayments']['pContoBonifico'],
            "banca" => $_POST['webPayments']['pBancaBonifico'],
            "filiale" => $_POST['webPayments']['pFilialeBonifico'],
            "iban" => $_POST['webPayments']['pIBANBonifico'],
            "bic_swift" => $_POST['webPayments']['pBICBonifico'],
        ),
        "enable_postepay" => $_POST['webPayments']['pEnablePostepay'],
        "postepay_data" => array(
            "descrizione" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['postepay_data']['descrizione'], $_POST['webPayments']['pDescrPostepay']),
            "istruzioni" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['postepay_data']['istruzioni'], $_POST['webPayments']['pIstrPostepay']),
            "intestatario" => $_POST['webPayments']['pIntestatarioPostepay'],
            "numero" => $_POST['webPayments']['pNumeroPostepay'],
            "cf" => $_POST['webPayments']['pCFPostepay'],
        ),
        "enable_contrassegno" => $_POST['webPayments']['pEnableContrassegno'],
        "contrassegno_data" => array(
            "descrizione" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['contrassegno_data']['descrizione'], $_POST['webPayments']['pDescrContrassegno']),
            "istruzioni" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['contrassegno_data']['istruzioni'], $_POST['webPayments']['pIstrContrassegno']),
            "limit_to_shipments" => json_encode($_POST['webPayments']['pShipMethodsContrassegno']),
            "rincaro" => (float)$_POST['webPayments']['pRincaroContrassegno'],
            "tipo_rincaro" => $_POST['webPayments']['pTipoRincaroContrassegno'],
            "aliquota_rincaro" => $_POST['webPayments']['pAliquotaRincaroContrassegno'],
        ),
        "enable_paypal" => $_POST['webPayments']['pEnablePayPal'],
        "paypal_data" => array(
            "descrizione" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['paypal_data']['descrizione'], $_POST['webPayments']['pDescrPayPal']),
            "istruzioni" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['paypal_data']['istruzioni'], $_POST['webPayments']['pIstrPayPal']),
            "email" => $_POST['webPayments']['pEmailPayPal'],
        ),
        "enable_nexi" => $_POST['webPayments']['pEnableNexi'],
        "nexi_data" => array(
            "descrizione" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['nexi_data']['descrizione'], $_POST['webPayments']['pDescrNexi']),
            "istruzioni" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['nexi_data']['istruzioni'], $_POST['webPayments']['pIstrNexi']),
            "alias" => $_POST['webPayments']['pAliasNexi'],
            "secret_key" => $_POST['webPayments']['pSecretKeyNexi'],
        ),
        "enable_stripe" => $_POST['webPayments']['pEnableStripe'],
        "stripe_data" => array(
            "descrizione" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['stripe_data']['descrizione'], $_POST['webPayments']['pDescrStripe']),
            "istruzioni" => $MSFrameworki18n->setFieldValue($old_pagamenti_web['stripe_data']['istruzioni'], $_POST['webPayments']['pIstrStripe']),
            "publishable_key" => $_POST['webPayments']['pPublishableKeyStripe'],
            "secret_key" => $_POST['webPayments']['pSecretKeyStripe'],
            "logo" => json_encode($stripe_logo)
        )
    );

}

$array_to_save = array(
    "value" => json_encode(array(
        "enable" => $_POST['enable'],
        "attach_note" => $_POST['attach_note'],
        "prefix" => $_POST['prefix'],
        "invoice_current_number" => $_POST['invoice_current_number'],
        "logo_fattura" => json_encode($ary_files),
        "ecommerce" => json_encode($_POST['ecommerce']),
        "payMethods" => json_encode($_POST['payMethods']),
        "webPayments" => json_encode($_POST['webPayments'])
    )),
    "type" => "fatturazione_fatture"
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'fatturazione_fatture'", $stringForDB[0]);
}

if(!$result) {

    if($db_action == "insert") {
        if ($_POST['webPayments']) {
            $uploader->unlink($stripe_logo);
        }
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['logo_fattura'], true));

    if($_POST['webPayments']) {
        $uploader->unlink($stripe_logo, json_decode($old_pagamenti_web['stripe_data']['logo'], true));
    }

}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
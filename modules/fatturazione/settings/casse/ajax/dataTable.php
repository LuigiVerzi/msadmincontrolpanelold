<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM fatturazione_casse") as $r) {
    $array['data'][] = array(
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['nome']),
        $MSFrameworki18n->getFieldValue($r['descrizione']),
        (new \MSFramework\Fatturazione\casse())->ottieniDispositiviSupportati($r['dispositivo'])['nome'],
        ($r['fiscale'] == 1 ? 'Fiscale' : 'Non Fiscale'),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

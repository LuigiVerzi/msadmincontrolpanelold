<?php

$connection_info = array();
if(isset($_POST['ajaxCall'])) {
    require_once('../../../../../../sw-config.php');
    require('../../config/moduleConfig.php');
}
else {
    if(json_decode($r['dati_connessione'])) {
        $connection_info = json_decode($r['dati_connessione'], true);
    }
}
?>

<h2 class="title-divider">Dati Connessione WebServer</h2>
<input type="hidden" class="connection_settings" name="method" value="webserver">

<div class="row">
    <div class="col-sm-3">
        <label>URL WebServer</label>
        <input id="webserver_ip" name="ip" type="text" class="connection_settings form-control required" value="<?php echo htmlentities($connection_info['ip']); ?>" placeholder="Es: 127.0.0.1:8080/server.cgi">
    </div>
</div>
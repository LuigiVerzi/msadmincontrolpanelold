$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    initOrak('logo_scontrino', 1, 500, 300, 200, getOrakImagesToPreattach('logo_scontrino'), false, ['image/jpeg', 'image/png']);

    $('#dispositivo').on('change', function () {

        if($(this).val() == '') {
            $('.impostazioni_connessione .alert').show();
            $('.impostazioni_connessione .settings_fields').html('');
        }
        else {
            $.ajax({
                url: "ajax/connection_blocks/" + $('#dispositivo option:selected').data('connessione') + ".php",
                type: "POST",
                data: {
                    ajaxCall: true
                },
                async: true,
                dataType: "html",
                success: function (html) {
                    $('.impostazioni_connessione .alert').hide();
                    $('.impostazioni_connessione .settings_fields').html(html);
                }
            });
        }
    });

}

function moduleSaveFunction() {

    var dati_connessione = {};
    $('.connection_settings').each(function () {
        var name = $(this).attr('name');
        dati_connessione[name] = $(this).val();
    });

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pDispositivo": $('#dispositivo').val(),
            "pDescrizione": $('#descrizione').val(),
            "pMessaggioPromozionale": $('#messaggio_promozionale').val(),
            "pLogoScontrino": composeOrakImagesToSave('logo_scontrino'),
            "pDatiConnessione": dati_connessione,
            "pFiscale": ($('#fiscale').prop('checked') ? 1 : 0),
            "pActive": ($('#is_active').prop('checked') ? 1 : 0)

        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}
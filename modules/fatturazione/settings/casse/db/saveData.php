<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;

if($_POST['pNome'] == "" || $_POST['pDispositivo'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$uploader = new \MSFramework\uploads('FATTURAZIONE_CASSE');
$ary_files = $uploader->prepareForSave($_POST['pLogoScontrino']);
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT logo_scontrino FROM fatturazione_casse WHERE id = :id", array(":id" => $_POST['pID']), true);
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "descrizione" => $_POST['pDescrizione'],
    "dispositivo" => $_POST['pDispositivo'],
    "logo_scontrino" => json_encode($ary_files),
    "messaggio_promozionale" => $_POST['pMessaggioPromozionale'],
    "dati_connessione" => json_encode($_POST['pDatiConnessione']),
    "fiscale" => $_POST['pFiscale'],
    "is_active" => $_POST['pActive']
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO fatturazione_casse ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE fatturazione_casse SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        if($db_action == "insert") {
            $uploader->unlink($ary_files);
        }
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, json_decode($r_old_data['logo_scontrino'], true));
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../../sw-config.php');
require('../config/moduleConfig.php');

if(!is_array($_POST['pID'])) {
    $_POST['pID'] = array($_POST['pID']);
}

$images_to_delete = array();
foreach($_POST['pID'] as $single_id) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT logo_scontrino FROM fatturazione_casse WHERE id = :id", array(":id" => $single_id), true);
    $images_to_delete = array_merge_recursive ($images_to_delete, json_decode($r['logo_scontrino'], true));
}

if($MSFrameworkDatabase->deleteRow("fatturazione_casse", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('FATTURAZIONE_CASSE'))->unlink($images_to_delete);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>

?>
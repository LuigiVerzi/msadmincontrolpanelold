<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require_once('../../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM fatturazione_casse WHERE id = :id", array(":id" => $_GET['id']), true);
}

$dispositivi = (new \MSFramework\Fatturazione\casse())->ottieniDispositiviSupportati();

?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        
                        <h1>Dati Cassa</h1>
                        <fieldset>
                            <h2 class="title-divider">Dati Generali</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nome Cassa*</label>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($r['nome']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>Dispositivo*</label>
                                    <select id="dispositivo" name="dispositivo" class="form-control">
                                        <option></option>
                                        <?php foreach($dispositivi as $id => $dispositivo) { ?>
                                            <option value="<?= $id; ?>" data-connessione="<?= $dispositivo['connection_type']; ?>" <?= ($r['dispositivo'] == $id ? 'selected' : ''); ?>><?= $dispositivo['nome']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label>Descrizione Cassa</label>
                                    <input id="descrizione" name="descrizione" type="text" class="form-control" value="<?php echo htmlentities($r['descrizione']) ?>">
                                </div>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?= (!$r || $r['is_active'] ? 'checked' : ''); ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Cassa Attiva</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <h2 class="title-divider">Fisco</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Usa come cassa Fiscale</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="fiscale" <?= ($r && $r['fiscale'] ? 'checked' : ''); ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Cassa Fiscale</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <h2 class="title-divider">Personalizzazione</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Messaggio Promozionale</label>
                                    <textarea id="messaggio_promozionale" name="messaggio_promozionale" placeholder="Messaggio promozionale da stampare a fine scontrino. (Max. 200 caratteri)" class="form-control" style="height: 105px;"><?php echo htmlentities($r['messaggio_promozionale']); ?></textarea>
                                </div>

                                <div class="col-sm-3 logo_cassa" style="margin-bottom: 40px;">
                                    <label>Logo Cassa</label>
                                    <?php (new \MSFramework\uploads('FATTURAZIONE_CASSE'))->initUploaderHTML("logo_scontrino", $r['logo_scontrino']); ?>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Connettività</h1>
                        <fieldset class="impostazioni_connessione">

                            <div class="settings_fields">
                            <?php
                            if($r) {
                                include("ajax/connection_blocks/" . $dispositivi[$r['dispositivo']]['connection_type'] . '.php');
                                $no_settings_display = "none";
                            } else {
                                $no_settings_display = "block";
                            }
                            ?>
                            </div>

                            <div class="alert alert-warning" style="display: <?= $no_settings_display; ?>; margin-bottom: 0;">
                                Per configurare i dati di connessione della cassa seleziona prima un modello tra quelli supportati.
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 25/10/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM forms WHERE id = :id", array($_GET['id']), true);
}

if(isset($_GET['import'])) {
    $globalForm = $MSFrameworkDatabase->getAssoc('SELECT * FROM marke833_framework.forms WHERE id = :id', array(':id' => $_GET['import']), true);
    if($globalForm) {
        $current_id = '';
        if($r) $current_id = $r['id'];
        $r = $globalForm;
        $r['id'] = $current_id;
        $r['imported'] = true;
    } else {
        header('Location: index.php');
    }
}

$shortcodes_globali = (new \MSFramework\emails())->getCommonShortcodes();

$MSFrameworkForms = new \MSFramework\forms();

?>
<body class="skin-1 fixed-sidebar pace-done form_builder">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php
                if($_SESSION['userData']['userlevel'] == '0') {
                    $customToolbarButton = '<a class="btn btn-info" id="dataTableImportGlobalForm">Importa form predefinito</a>';
                }
                require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php");
                ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome Form*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Questo nome è solo un riferimento per identificare facilmente il form. Non verrà visualizzato dagli utenti."></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($r['nome']) ?>">
                                </div>

                                <?php
                                $is_active_check = "checked";
                                if($r['is_active'] == 0 && $_GET['id'] != "") {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="enable_captcha" <?= ($r['enable_captcha'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Abilita Captcha</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Testo pulsante submit*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['submit_btn_text']) ?>"></i></span>
                                    <input id="submit_btn_text" name="submit_btn_text" type="text" class="form-control msShort required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['submit_btn_text'])) ?>">
                                </div>
                            </div>

                            <h2 class="title-divider">Campi Nascosti</h2>
                            <textarea style="display: none;" id="hidden_field_template">
                                <div class="modal inmodal fade" id="hidden_modal_{id}" tabindex="-1" role="dialog"  aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Input Nascosto</h4>
                                                </div>
                                                <div class="modal-body">

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger btn-sm delete_hidden_field">Elimina Campo</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Aggiorna</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </textarea>
                            <div id="hidden_fields_container">
                                <?php
                                $fields = json_decode($r['fields'], true);

                                foreach($fields as $k => $v) {
                                    if($v['input_type'] != 'hidden') unset($fields[$k]);
                                }
                                $i = 0;
                                $link_to_formfields_hidden_options = "";
                                $link_to_formfields_hidden_checkbox = "";
                                foreach($fields as $k => $v) {
                                    $i++;
                                    $link_to_formfields_hidden_options .= '<option value="' . $v['field_id'] . '">' . $MSFrameworki18n->getFieldValue($v['name'], true) . '</option>';
                                    $link_to_formfields_hidden_checkbox .= '<label><input type="checkbox" value="' . $v['field_id'] . '"> ' . $MSFrameworki18n->getFieldValue($v['name'], true) . '</label>';
                                ?>
                                    <button type="button" class="btn btn-default btn-sm" style="margin-bottom: 3px;" data-toggle="modal" data-target="#hidden_modal_<?= $i; ?>"><?= $v['field_id']; ?></button>
                                    <div class="modal inmodal fade" id="hidden_modal_<?= $i; ?>" tabindex="-1" role="dialog"  aria-hidden="true">

                                        <input type="hidden" class="field_uniqueid" value="<?php echo htmlentities($v['unique_id']) ?>">

                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Input Nascosto</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-sm-4 field_name_container">
                                                            <label>Nome campo*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($v['name']) ?>"></i></span>
                                                            <input type="text" class="form-control field_name required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($v['name'])) ?>" data-main-lang-value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($v['name'], true)) ?>">
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <label>ID campo*</label>
                                                            <input type="text" value="<?= htmlentities($v['field_id']) ?>" class="form-control field_id required" />
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <label>Valore predefinito</label>
                                                            <input type="text" class="form-control msShort field_default" value="<?php echo $v['default']; ?>">
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger btn-sm delete_hidden_field">Elimina Campo</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Aggiorna</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php if($using_primary_language) { ?>
                                <button type="button" data-toggle="modal" data-target="#new_hidden_modal" class="btn btn-outline btn-primary btn-sm" style="margin-bottom: 3px;">Aggiungi Campo Nascosto</button>
                                <div class="modal inmodal fade" id="new_hidden_modal" tabindex="-1" role="dialog"  aria-hidden="true">

                                    <input type="hidden" class="field_uniqueid" value="<?php echo uniqid('u') ?>">

                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Chiudi</span></button>
                                                <h4 class="modal-title">Aggiungi Input Nascosto</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-4 field_name_container">
                                                        <label>Nome campo*</label>
                                                        <input type="text" class="form-control field_name required" name="" value="">
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label>ID campo*</label>
                                                        <input type="text" value="" class="form-control field_id required" />
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label>Valore predefinito</label>
                                                        <input type="text" class="form-control msShort field_default" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                                                <button type="button" class="btn btn-primary add_hidden_input">Aggiorna</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <h2 class="title-divider">Campi Visibili</h2>

                            <div class="easyRowDuplicationContainer">
                                <?php
                                $fields = json_decode($r['fields'], true);

                                foreach($fields as $k => $v) {
                                    if($v['input_type'] == 'hidden') unset($fields[$k]);
                                }

                                if(count($fields) == 0) {
                                    $fields[] = array("field_id" => round(microtime(true) * 1000));
                                }

                                $link_to_formfields_options = '<option value=""></option>';
                                $link_to_formfields_checkboxes = "";
                                $i = 0;
                                foreach($fields as $k => $v) {
                                    $i++;
                                    $link_to_formfields_options .= '<option value="' . $v['field_id'] . '">' . $MSFrameworki18n->getFieldValue($v['name'], true) . '</option>';
                                    $link_to_formfields_checkboxes .= '<label><input type="checkbox" value="' . $v['field_id'] . '"> ' . $MSFrameworki18n->getFieldValue($v['name'], true) . '</label>';
                                    ?>
                                    <div class="stepsDuplication rowDuplication fieldsDuplication m-b-md <?= (!$using_primary_language ? 'no_edit' : ''); ?>">
                                        <div class="ibox collapsed">
                                            <div class="ibox-title" style="background: #fefefe;">
                                                <h5>
                                                    <span class="title"><?php echo htmlentities($MSFrameworki18n->getFieldValue($v['name'])) ?></span> <small class="id">- <?php echo htmlentities($MSFrameworki18n->getFieldValue($v['field_id'])) ?></small>
                                                </h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    <?php if($using_primary_language) { ?>
                                                    <a class="close-link">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="ibox-content" style="background: background: #fefefe;">

                                                <input type="hidden" class="field_uniqueid" value="<?php echo htmlentities($v['unique_id']); ?>">

                                                <div class="row">
                                                    <div class="col-sm-4 field_name_container">
                                                        <label>Nome campo*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($v['name']) ?>"></i></span>
                                                        <input type="text" class="form-control field_name required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($v['name'])) ?>" data-main-lang-value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($v['name'], true)) ?>">
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <label>ID campo*</label>
                                                        <input type="text" value="<?= htmlentities($v['field_id']) ?>" class="form-control field_id required" />
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <label>Tipologia campo</label>
                                                        <select id="field_type" class="form-control field_type">
                                                            <?php
                                                            foreach($MSFrameworkForms->getFieldTypes() as $fieldK => $fieldV) {
                                                                $cur_display_type = "display_type_" . $fieldK;
                                                                $cur_values = "values_" . $fieldK;

                                                                $$cur_values = array();
                                                                if($v['type'] == $fieldK || ($v['type'] == "" && $fieldK == "0")) {
                                                                    $$cur_display_type = " display: block; ";

                                                                    foreach($v as $subKV => $subVV) {
                                                                        if($fieldK == "0") {
                                                                            $$cur_values[$v['input_type']][$subKV] = $subVV;
                                                                        } else {
                                                                            $$cur_values[$subKV] = $subVV;
                                                                        }

                                                                    }
                                                                } else {
                                                                    $$cur_display_type = " display: none; ";
                                                                    $$cur_values = "";
                                                                }
                                                            ?>
                                                                <option value="<?php echo $fieldK ?>" <?php if($v['type'] == (string)$fieldK) { echo "selected"; } ?>><?php echo $fieldV ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-3 input_type_select_container">
                                                        <label>Tipologia input</label>
                                                        <?php
                                                            $input_types = array("text", "number", "checkbox", "date", "radio", "email", "password");
                                                        ?>
                                                        <select id="input_type" class="form-control input_type">
                                                            <?php foreach($input_types as $input_type) { ?>
                                                                <option value="<?= $input_type ?>" <?php if($v['input_type'] == (string)$input_type) { echo "selected"; } ?>><?php echo $input_type ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row m-t-md type_container_overall">
                                                    <div class="type_container type_0_container" style="<?php echo $display_type_0 ?>">
                                                        <div class="input_type_container input_type_text m-b-md col-md-12" style="display: none;">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Placeholder</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($values_0['text']['placeholder']) ?>"></i></span>
                                                                    <input type="text" class="form-control msShort field_placeholder" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($values_0['text']['placeholder'])) ?>">
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <label>Valore predefinito</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($values_0['text']['default']) ?>"></i></span>
                                                                    <input type="text" class="form-control msShort field_default" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($values_0['text']['default'])) ?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="input_type_container input_type_email m-b-md col-md-12" style="display: none;">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Placeholder</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($values_0['email']['placeholder']) ?>"></i></span>
                                                                    <input type="text" class="form-control msShort field_placeholder" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($values_0['email']['placeholder'])) ?>">
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <label>Valore predefinito</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($values_0['email']['default']) ?>"></i></span>
                                                                    <input type="text" class="form-control msShort field_default" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($values_0['email']['default'])) ?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="input_type_container input_type_number m-b-md col-md-12" style="display: none;">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Placeholder</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($values_0['number']['placeholder']) ?>"></i></span>
                                                                    <input type="text" class="form-control msShort field_placeholder" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($values_0['number']['placeholder'])) ?>">
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <label>Valore predefinito</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($values_0['number']['default']) ?>"></i></span>
                                                                    <input type="text" class="form-control msShort field_default" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($values_0['number']['default'])) ?>">
                                                                </div>
                                                            </div>

                                                            <div class="row m-t-md">
                                                                <div class="col-sm-3">
                                                                    <label>Valore minimo</label>
                                                                    <input type="number" class="form-control field_number_min" value="<?php echo $values_0['number']['min'] ?>">
                                                                </div>

                                                                <div class="col-sm-3">
                                                                    <label>Valore massimo</label>
                                                                    <input type="number" class="form-control field_number_max" value="<?php echo $values_0['number']['max'] ?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="input_type_container input_type_hidden m-b-md col-md-12" style="display: none;">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label>Valore predefinito</label>
                                                                    <input type="text" class="form-control msShort field_default" value="<?php echo htmlentities($values_0['hidden']['default']); ?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="input_type_container input_type_checkbox m-b-md col-md-12" style="display: none;">
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <label>&nbsp;</label>
                                                                    <div class="styled-checkbox form-control">
                                                                        <label style="width: 100%;">
                                                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                                <input type="checkbox" class="field_checkbox_checked" <?= ($values_0['checkbox']['default_checked'] == "1" ? "checked" : "") ?>>
                                                                                <i></i>
                                                                            </div>
                                                                            <span style="font-weight: normal;">Checkato default</span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="input_type_container input_type_radio m-b-md col-md-12" style="display: none;">
                                                            <div class="row">
                                                                <div class="easyRowDuplicationContainer col-sm-12">
                                                                    <?php

                                                                    if(!is_array($values_0['radio'])) {
                                                                        $values_0['radio']= array('data' => array(array()));
                                                                    }

                                                                    foreach($values_0['radio']['data'] as $cur_values) {
                                                                        ?>
                                                                        <div class="stepsDuplication rowDuplication radioDuplication">
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <label>Valore radio*</label><span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Questo campo non è visibile agli utenti, ma serve ad identificare il valore della select selezionato. Non è possibile creare due righe con lo stesso valore."></i></span>
                                                                                    <input type="text" class="form-control field_value" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($cur_values['value'])) ?>">
                                                                                </div>

                                                                                <div class="col-sm-6">
                                                                                    <label>Testo radio</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($cur_values['text']) ?>"></i></span>
                                                                                    <input type="text" class="form-control field_text" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($cur_values['text'])) ?>">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="type_container type_1_container" style="<?php echo $display_type_1 ?>">
                                                        <div class="col-sm-4">
                                                            <label>Placeholder</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($values_1['placeholder']); ?>"></i></span>
                                                            <input type="text" class="form-control msShort field_placeholder" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($values_1['placeholder'])) ?>">
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <label>Valore predefinito</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($values_1['default']); ?>"></i></span>
                                                            <input type="text" class="form-control msShort field_default" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($values_1['default'])) ?>">
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <label>Numero di righe</label>
                                                            <input type="number" class="form-control field_rows" value="<?php echo ($values_1['rows'] == "" ? "4" : $values_1['rows']) ?>">
                                                        </div>
                                                    </div>

                                                    <div class="type_container type_2_container" style="<?php echo $display_type_2 ?>">
                                                        <div class="easyRowDuplicationContainer col-sm-12">
                                                            <?php
                                                            if(!is_array($values_2['data'])) {
                                                                $values_2 = array('data' => array(array()));
                                                            }

                                                            foreach($values_2['data'] as $cur_values) {
                                                            ?>
                                                            <div class="stepsDuplication rowDuplication optionsDuplication">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <label>Valore option*</label><span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Questo campo non è visibile agli utenti, ma serve ad identificare il valore della select selezionato. Non è possibile creare due righe con lo stesso valore."></i></span>
                                                                        <input type="text" class="form-control field_value" value="<?php echo htmlentities($cur_values['value']) ?>">
                                                                    </div>

                                                                    <div class="col-sm-6">
                                                                        <label>Testo option</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($cur_values['text']) ?>"></i></span>
                                                                        <input type="text" class="form-control msShort field_text" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($cur_values['text'])) ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row m-t-md">

                                                    <div class="col-sm-3">
                                                        <label>Descrizione Campo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($v['description']) ?>"></i></span> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci una descrizione che possa aiutare i visitatori a comprendere meglio il contenuto del campo"></i></span>
                                                        <input type="text" class="form-control msShort field_description" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($v['description'])); ?>">
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <label>Classi Input</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Digitare i nomi delle classi da aggiungere al campo, separati da uno spazio."></i></span>
                                                        <input type="text" class="form-control field_add_class" value="<?php echo htmlentities($v['add_class']) ?>">
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <label>Classi Contenitore</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Digitare i nomi delle classi da aggiungere al box che contiene l'input"></i></span>
                                                        <input type="text" class="form-control field_add_container_class" value="<?php echo htmlentities($v['add_container_class']) ?>">
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <label>Larghezza campo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Seleziona la larghezza da assegnare al campo"></i></span>

                                                        <select class="form-control field_add_container_size" data-cols="2" data-placeholder="Modifica dimensione" multiple>
                                                            <?php foreach(array('Desktop' => 'ms-col-', 'Tablet' => 'ms-col-md-', 'Mobile' => 'ms-col-xs-') as $device => $device_prefix) { ?>
                                                            <optgroup label="<?= $device; ?>">
                                                                <?php foreach(array('12' => '1/1', '6' => '1/2', '4' => '1/3', '3' => '1/4') as $size_val => $size_text) { ?>
                                                                    <option value="<?= $device_prefix . $size_val; ?>" <?php if(in_array($device_prefix . $size_val, $v['add_container_size'])) { echo "selected"; } ?>><?= $size_text; ?></option>
                                                                <?php } ?>
                                                            </optgroup>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <label>&nbsp;</label>
                                                        <div class="styled-checkbox form-control">
                                                            <label style="width: 100%;">
                                                                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                    <input type="checkbox" class="field_mandatory" <?= ($v['mandatory'] == "1" ? "checked" : "") ?>>
                                                                    <i></i>
                                                                </div>
                                                                <span style="font-weight: normal;">Obbligatorio</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                <?php }

                                if(!empty($link_to_formfields_hidden_options)) {
                                        $link_to_formfields_options = "<option value=''></option><optgroup label='Campi Visibili'>" . $link_to_formfields_options . "</optgroup>" . "<optgroup label='Campi Nascosti'>" . $link_to_formfields_hidden_options . "</optgroup>";
                                }
                                if(!empty($link_to_formfields_hidden_checkbox)) {
                                    $link_to_formfields_checkboxes .= $link_to_formfields_hidden_checkbox;
                                }
                                $fields = json_decode($r['fields'], true);
                                ?>
                            </div>

                            <?php if(!$using_primary_language) { ?>
                                <div class="hr-line-dashed" style="margin-top: 10px;"></div>
                                <div class="alert alert-warning">Per inserire nuovi campi utilizza la lingua madre, successivamente potrai modificarne le traduzioni da qui.</div>
                            <?php } ?>


                        </fieldset>

                        <h1>Azioni</h1>
                        <fieldset>
                            <div class="row" style="display: <?= ($using_primary_language ? 'block' : 'none'); ?>;">
                                <?php
                                $actions_decoded = json_decode($r['actions'], true);
                                $actions_values_decoded = json_decode($r['actions_values'], true);

                                $actions_checks = $MSFrameworkForms->getFormActionsInfo();

                                foreach($actions_checks as $action) {
                                    if(is_array($action['limit_to_extra_function']) && count(array_intersect($action['limit_to_extra_function'], json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions']))) == 0) {
                                        continue;
                                    }
                                ?>

                                    <div class="col-lg-4 col-md-6 col-sm-6 notification_box" style="cursor: pointer;">
                                        <div class="form_actions_check widget <?= (in_array($action['id'], $actions_decoded) ? "active" : ""); ?> p-md text-center"  id="<?= $action['id'] ?>" >
                                            <div>
                                                <i class="fa <?= $action['fa-icon'] ?> fa-3x"></i>
                                                <h1 class="m-b-md m-t-md"><?= $action['name'] ?></h1>
                                                <h4 style="font-weight: normal;">
                                                    <?= $action['descr'] ?>
                                                </h4>

                                                <div class="actions_buttons row active_actions" style="display: <?= (in_array($action['id'], $actions_decoded) ? "block" : "none"); ?>;">
                                                    <div class="col-xs-6">
                                                    <a href="#" class="btn btn-default edit_preference btn-block">Impostazioni</a>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <a href="#" class="btn btn-danger activation_btn btn-block">Disattiva</a>
                                                    </div>
                                                </div>

                                                <div class="actions_buttons inactive_actions" style="display: <?= (in_array($action['id'], $actions_decoded) ? "none" : "block"); ?>;">
                                                    <a href="#" class="btn btn-primary activation_btn btn-block">Attiva</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <?php if(!$using_primary_language) { ?>
                                <div class="alert alert-warning">
                                    Durante la modifica di una lingua secondaria non è possibile abilitare nuove azioni.
                                </div>
                            <?php } ?>

                            <div class="manage_actions_section" style="display: <?= (count($actions_decoded) ? 'block' : 'none'); ?>;">
                                <h2 class="title-divider">Gestisci Preference</h2>
                            </div>

                            <?php
                            foreach($actions_checks as $action) {
                                if($action['id'] == "quote") { //viene gestito in un tab a parte
                                    continue;
                                }
                            ?>
                            <div class="action_container m-t-md" data-container-of="<?= $action['id'] ?>" id="action_<?= $action['id'] ?>_container" style="display: <?= (in_array($action['id'], $actions_decoded) ? "block" : "none") ?>">
                                <div class="ibox">
                                    <div class="ibox-title" style="background: #fefefe;">
                                        <i class="ibox-icon fa <?= $action['fa-icon'] ?> fa-3x"></i>
                                        <h5>
                                            <span class="title">Impostazioni <?= $action['name'] ?></span>
                                        </h5>
                                        <div class="ibox-tools">
                                            <a class="fullscreen-link">
                                                <i class="fa fa-expand"></i>
                                            </a>
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content" style="background: background: #fefefe;">
                                        <?php include("ajax/actions_interfaces/" . $action['id'] . ".php"); ?>

                                        <?php if(isset($action['return_message'])) { ?>

                                            <?php
                                            $manage_messages = (isset($actions_values_decoded[$action['id']]['messages']) && is_array($actions_values_decoded[$action['id']]['messages'])) || (!isset($actions_values_decoded[$action['id']]['messages']) && $action['return_message']['show']);
                                            $manage_template = ($manage_messages && isset($actions_values_decoded[$action['id']]['messages']) && is_array(end($actions_values_decoded[$action['id']]['messages'])['template']));
                                            ?>

                                            <h2 class="title-divider">Gestione Messaggi</h2>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Gestione dei Messaggi</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Attivando questa opzione ai clienti verrà mostrato il seguente messaggio (di errore o di successo) dopo l'invio del form. (Abilitandone più di uno verranno mostrati uno dopo l'altro)"></i></span>
                                                    <div class="styled-checkbox form-control">
                                                        <label style="width: 100%;">
                                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                <input type="checkbox" class="manage_messages" <?= ( $manage_messages ? "checked" : "") ?>>
                                                                <i></i>
                                                            </div>
                                                            <span style="font-weight: normal;">Mostra un messaggio dopo l'invio</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 manage_messages_container" style="display: <?= ( $manage_messages ? "block" : "none") ?>;">
                                                    <label>Template Messaggio</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Permette di modificare il template HTML tramite la quale sarà visualizzato il messaggio"></i></span>
                                                    <div class="styled-checkbox form-control">
                                                        <label style="width: 100%;">
                                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                                <input type="checkbox" class="manage_template" <?= ( $manage_template ? "checked" : "") ?>>
                                                                <i></i>
                                                            </div>
                                                            <span style="font-weight: normal;">Modifica Template Messaggio</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="manage_messages_container" style="display: <?= ( $manage_messages ? "block" : "none") ?>;">

                                                <div class="hr-line-dashed"></div>

                                                <div class="manage_template_container" style="display: <?= ( $manage_template ? "block" : "none") ?>;">

                                                    <label>Personalizzate Template Messaggio</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Usa questo campo per modificare il codice HTML dentro la quale verrà mostrato il messaggio. Le aree evidenziate sono i contenuti dinamici che verranno sostituiti con i valori reali."></i></span>
                                                    <div class="message_template form-control">
                                                        <?php


                                                        $template = $action['return_message']['template'];

                                                        if(isset($actions_values_decoded[$action['id']]['messages']) && isset(end($actions_values_decoded[$action['id']]['messages'])['template']) && is_array(end($actions_values_decoded[$action['id']]['messages'])['template'])) {
                                                            $template = end($actions_values_decoded[$action['id']]['messages'])['template'];
                                                        }


                                                        foreach($template as $tp) {
                                                            if($tp[0] == '[') {
                                                                echo '<strong>' . htmlentities($tp) . '</strong>';
                                                            }
                                                            else {
                                                                echo '<span contenteditable>' . htmlentities($tp) . '</span>';
                                                            }
                                                        }
                                                        ?>
                                                    </div>

                                                    <div class="hr-line-dashed"></div>
                                                </div>

                                                <?php foreach($action['return_message']['strings'] as $message_type => $strings_array) { ?>

                                                    <?php
                                                    if(isset($strings_array['titolo'])) {
                                                        $strings_array = array($strings_array);
                                                    }
                                                    ?>
                                                    <?php foreach($strings_array as $key => $strings) { ?>

                                                        <?php
                                                        $messages = array(
                                                            'titolo' => $strings['titolo'],
                                                            'message' => $strings['message']
                                                        );

                                                        if(isset($actions_values_decoded[$action['id']]['messages']) && isset($actions_values_decoded[$action['id']]['messages'][$message_type . (!is_numeric($key) ? '_' . $key : '')])) {
                                                            $messages = $actions_values_decoded[$action['id']]['messages'][$message_type . (!is_numeric($key) ? '_' . $key : '')];
                                                        }
                                                        ?>

                                                        <div class="message_strings alert alert-<?= $message_type; ?>" data-type="<?= $message_type . (!is_numeric($key) ? '_' . $key : ''); ?>">
                                                            <h2 class="title-divider"><?= $strings['label']; ?></h2>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Titolo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($messages['titolo']) ?>"></i></span>
                                                                    <input type="text" class="form-control msShort message_field" name="titolo" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($messages['titolo'])) ?>">
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label>Testo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($messages['message']) ?>"></i></span>
                                                                    <textarea class="form-control msShort message_field" name="message"><?php echo htmlentities($MSFrameworki18n->getFieldValue($messages['message'])) ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>

                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </fieldset>

                        <h1>Preventivo</h1>
                        <fieldset id="quote_fieldset">
                            <?php
                                $_POST['pIncludedFromEdit'] = "true";
                                $_POST['pCurFormData'] = $fields;
                                $cur_actions_values = json_decode($r['actions_values'], true);
                                $_POST['pCurQuoteData'] = $cur_actions_values['quote'];
                            ?>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Prezzo Base</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci il prezzo di base al quale saranno sommati/sottratti i valori derivati dalla generazione del preventivo."></i></span>
                                    <input id="base_price" name="base_price" type="text" class="form-control" value="<?= $_POST['pCurQuoteData']['base_price'] ?>"/>
                                </div>
                                <div class="col-sm-4">
                                    <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Il preventivo non verrà generato automaticamente se non inizializzato tramite JavaScript"></i></span>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="force_manually" <?= ($_POST['pCurQuoteData']['force_manually'] == "1" ? "checked" : "") ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Non generare automaticamente</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div id="quote_rows_container">
                                <?php
                                    require('ajax/quoteRowsHTML.php')
                                ?>
                            </div>
                        </fieldset>

                        <h1>Eventi</h1>
                        <fieldset id="eventi">

                            <?php
                            $form_events = $MSFrameworkForms->getDynamicsHooksInfo();
                            $events_decoded = json_decode($r['events'], true);
                            ?>

                            <div class="alert alert-danger">
                                Le seguenti impostazioni richiedono delle <b>nozioni di JavaScript</b> per essere utilizzate, nel caso in cui il codice non risulti funzionante l'intero funzionamento del sito <b>potrebbe risultare alterato</b>.
                            </div>

                            <div id="eventsList">
                                <?php foreach($form_events as $event_id => $form_event) { ?>
                            <div class="ibox <?php if(!$events_decoded[$event_id]) { echo "collapsed"; } ?> eventBox" data-key="<?= $event_id; ?>">
                                <div class="ibox-title">
                                    <div class="checkbox i-checks pull-left" style="position: relative; z-index: 10; margin: -5px 15px 0 0px;">
                                        <input type="checkbox" id="eventSettings_<?= $event_id; ?>" class="eventSettings_enabled" <?php if($events_decoded[$event_id]) { echo "checked"; } ?>>
                                        <i></i>
                                    </div>
                                    <h5>
                                       <span><?= $form_event['nome']; ?></span>
                                    </h5>
                                    <small style="float: right;background: white; padding: 4px 10px; border: 1px solid #ececec"><?= $form_event['descrizione']; ?></small>
                                    <div style="clear: both;"></div>
                                </div>
                                <div class="ibox-content">

                                    <code style="display: block; padding: 10px; border-radius: 0; padding-left: 45px;">

                                        <?php
                                        $array_data_variables = array();
                                        foreach($form_event['passed_data'] as $data_key => $data_descr) {
                                            $array_data_variables[] = '<span class="ms-label-tooltip" style="float: none; margin-right: 5px;"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . htmlentities($data_descr) . '"></i></span><b>' . $data_key . '</b>';
                                        }
                                        ?>

                                        function <?= $event_id; ?>(<b><?= implode(', ', $array_data_variables); ?></b>) {
                                    </code>

                                    <div class="hook_editor"><?= ($events_decoded[$event_id] && !empty($events_decoded[$event_id]['js']) ? $events_decoded[$event_id]['js'] : ''); ?></div>


                                    <code style="display: block; padding: 10px; border-radius: 0; padding-left: 45px;">
                                        }
                                    </code>

                                    <?php if($form_event['can_format']) { ?>
                                        <div class="alert alert-info" style="margin-top: 15px;">
                                            Ritornando un array <b>form_data</b> modificato è possibile passare il nuovo array con i valori modificati agli eventi successivi.
                                        </div>
                                    <?php } ?>

                                    <?php if($form_event['can_stop']) { ?>
                                        <div class="alert alert-warning" style="margin-top: 15px;">
                                            Impostando un <i>return <b>false</b>;</i> alla fine della funzione verranno bloccati tutti gli eventi successivi a questo.
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <?php } ?>
                            </div>

                        </fieldset>
                        <?php if($r && $r['id']) { ?>
                            <h1 class="tab-right"><i class="fa fa-history" aria-hidden="true"></i> Cronologia Invio</h1>
                            <fieldset class="histories">
                                <table id="form_submission_histories" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th class="default-sort" data-sort="DESC">Data Invio</th>
                                        <th>Utente</th>
                                        <th>Stato</th>
                                        <th>Lingua</th>
                                        <th class="no-sort" width="60" style="width: 60px !important;"></th>
                                    </tr>
                                    </thead>
                                </table>
                            </fieldset>
                        <?php } ?>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    window.global_shortcodes = <?= json_encode($shortcodes_globali[0]); ?>;
    
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 25/10/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;
if($_POST['pNome'] == "" ||
    (in_array("newsletter_subscribe", $_POST['pActions']) && ($_POST['pActionsValues']['newsletter_subscribe']['name'] == "" || $_POST['pActionsValues']['newsletter_subscribe']['email'] == "")) ||
    (in_array("reservation", $_POST['pActions']) && ($_POST['pActionsValues']['reservation']['name'] == "" || $_POST['pActionsValues']['reservation']['surname'] == "" || $_POST['pActionsValues']['reservation']['email'] == "" || $_POST['pActionsValues']['reservation']['date_from'] == "" || $_POST['pActionsValues']['reservation']['date_to'] == "" || $_POST['pActionsValues']['reservation']['adults'] == "" || $_POST['pActionsValues']['reservation']['children'] == "")) ||
    (in_array("reply_to_mail", $_POST['pActions']) && ($_POST['pActionsValues']['reply_to_mail']['name'] == "" || $_POST['pActionsValues']['reply_to_mail']['email'] == "" || $_POST['pActionsValues']['reply_to_mail']['subject'] == "")) ||
    (in_array("generic_request", $_POST['pActions']) && ($_POST['pActionsValues']['generic_request']['name'] == "" || $_POST['pActionsValues']['generic_request']['email'] == ""))
) {
    $can_save = false;
}

$got_ids = array();
foreach($_POST['pFields'] as $k=>$field) {

    if($field['name'] == "") {
        if($field['type'] == 0 && $field['input_type'] == "hidden")
        {
            $_POST['pFields'][$k]['name'] = $field['field_id'];
        }
        else
        {
            $can_save = false;
            break;
        }
    }

    if(in_array($field['field_id'], $got_ids)) {
        echo json_encode(array("status" => "duplicate_field_id"));
        die();
    }

    $got_ids[] = $field['field_id'];
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$duplicates_check_sql = array('', array());
if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT fields, nome, submit_btn_text, actions_values FROM forms WHERE id = :id", array(":id" => $_POST['pID']), true);
    $old_fields = json_decode($r_old_data['fields'], true);

    $duplicates_check_sql = array(" AND id != :id ", array(":id" => $_POST['pID']));

    $actions_values_decoded = json_decode($r_old_data['actions_values'], true);
    $old_reply_to_mail_attachments = json_decode($actions_values_decoded['reply_to_mail']['attachments'], true);
}


if($MSFrameworkDatabase->getCount("SELECT nome FROM forms WHERE nome = :nome " . $duplicates_check_sql[0], array_merge($duplicates_check_sql[1], array(":nome" => $_POST['pNome']))) != 0) {
    echo json_encode(array("status" => "same_name_exists"));
    die();
}

$old_fields_by_unique = array();
foreach($old_fields as $oldField) {
    $old_fields_by_unique[$oldField['unique_id']] = $oldField;
}

$gotNames = array();
foreach($_POST['pFields'] as $kField => $vField) {
    if(in_array($vField['name'], $gotNames)) {
        continue; //i nomi dei campi devono essere univoci! se ne esiste uno uguale, lo salto!
    }

    $unique_id = $vField['unique_id'];

    $gotNames[] = $vField['name'];

    //controlli di sicurezza per le traduzioni
    if($vField['type'] != 0 || $vField['input_type'] != "hidden")
    {
        if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $vField['name'], 'oldValue' => $old_fields_by_unique[$unique_id]['name']),
            array('currentValue' => $vField['description'], 'oldValue' => $old_fields_by_unique[$unique_id]['description'])
        )) == false) {
            echo json_encode(array("status" => "no_datalang_for_primary"));
            die();
        }
    }

    $_POST['pFields'][$kField]['name'] = $MSFrameworki18n->setFieldValue($old_fields_by_unique[$unique_id]['name'], $vField['name']);
    $_POST['pFields'][$kField]['description'] = $MSFrameworki18n->setFieldValue($old_fields_by_unique[$unique_id]['description'], $vField['description']);

    if(($vField['type'] == "0" || $vField['type'] == "1") && $vField['input_type'] != "hidden" && $vField['input_type'] != "checkbox" && $vField['input_type'] != "radio") {
        if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                array('currentValue' => $vField['placeholder'], 'oldValue' => $old_fields_by_unique[$unique_id]['placeholder']),
                array('currentValue' => $vField['default'], 'oldValue' => $old_fields_by_unique[$unique_id]['default']),
            )) == false) {
            echo json_encode(array("status" => "no_datalang_for_primary"));
            die();
        }

        $_POST['pFields'][$kField]['placeholder'] = $MSFrameworki18n->setFieldValue($old_fields_by_unique[$unique_id]['placeholder'], $vField['placeholder']);
        $_POST['pFields'][$kField]['default'] = $MSFrameworki18n->setFieldValue($old_fields_by_unique[$unique_id]['default'], $vField['default']);
    } else if($vField['type'] == "2" || ($vField['type'] == "0" && $vField['input_type'] == "radio")) {
        foreach($vField['data'] as $cur_dataK => $cur_dataV) {
            if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                    array('currentValue' => $cur_dataV['text'], 'oldValue' => $old_fields_by_unique[$unique_id]['data'][$cur_dataK]['text']),
                )) == false) {
                echo json_encode(array("status" => "no_datalang_for_primary"));
                die();
            }

            $_POST['pFields'][$kField]['data'][$cur_dataK]['text'] = $MSFrameworki18n->setFieldValue($old_fields_by_unique[$unique_id]['data'][$cur_dataK]['text'], $cur_dataV['text']);
        }
    }
}

/* Controllo traduzioni per Messaggi Azioni */
foreach($_POST['pActionsValues'] as $kActions => $vAction) {
    if(is_array($vAction['messages'])) {
        foreach($vAction['messages'] as $statusK => $statusMessage) {

            if ($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                    array('currentValue' => $statusMessage['titolo'], 'oldValue' => $actions_values_decoded[$kActions]['messages'][$statusK]['titolo']),
                    array('currentValue' => $statusMessage['message'], 'oldValue' => $actions_values_decoded[$kActions]['messages'][$statusK]['message'])
                )) == false) {
                echo json_encode(array("status" => "no_datalang_for_primary"));
                die();
            }

            $_POST['pActionsValues'][$kActions]['messages'][$statusK]['titolo'] = $MSFrameworki18n->setFieldValue($actions_values_decoded[$kActions]['messages'][$statusK]['titolo'], $statusMessage['titolo']);
            $_POST['pActionsValues'][$kActions]['messages'][$statusK]['message'] = $MSFrameworki18n->setFieldValue($actions_values_decoded[$kActions]['messages'][$statusK]['message'], $statusMessage['message']);

        }
    }
}

if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pSubmitBtnTXT'], 'oldValue' => $r_old_data['submit_btn_text'])
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

if(in_array("reply_to_mail", $_POST['pActions'])) {

    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $_POST['pActionsValues']['reply_to_mail']['subject'], 'oldValue' => $actions_values_decoded['reply_to_mail']['subject']),
            array('currentValue' => $_POST['pActionsValues']['reply_to_mail']['custom_html'], 'oldValue' => $actions_values_decoded['reply_to_mail']['custom_html']),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $html = new \Html2Text\Html2Text($_POST['pActionsValues']['reply_to_mail']['custom_html']);
    $versione_testuale =  $html->getText();

    $_POST['pActionsValues']['reply_to_mail']['subject'] = $MSFrameworki18n->setFieldValue($actions_values_decoded['reply_to_mail']['subject'], $_POST['pActionsValues']['reply_to_mail']['subject']);
    $_POST['pActionsValues']['reply_to_mail']['custom_html'] = $MSFrameworki18n->setFieldValue($actions_values_decoded['reply_to_mail']['custom_html'], $_POST['pActionsValues']['reply_to_mail']['custom_html']);
    $_POST['pActionsValues']['reply_to_mail']['custom_txt'] = $MSFrameworki18n->setFieldValue($actions_values_decoded['reply_to_mail']['custom_txt'], $versione_testuale);
}

if(in_array("generic_request", $_POST['pActions']) || in_array("reservation", $_POST['pActions'])) {

    foreach ($_POST['pActionsValues'] as $actionKey => $actionValues) {

        if (isset($actionValues['use_custom_mail']) && $actionValues['use_custom_mail'] == 1) {

            if ($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                    array('currentValue' => $_POST['pActionsValues'][$actionKey]['custom_mail']['custom_subject'], 'oldValue' => $actions_values_decoded[$actionKey]['custom_mail']['custom_subject']),
                    array('currentValue' => $_POST['pActionsValues'][$actionKey]['custom_mail']['custom_txt'], 'oldValue' => $actions_values_decoded[$actionKey]['custom_mail']['custom_txt']),
                    array('currentValue' => $_POST['pActionsValues'][$actionKey]['custom_mail']['custom_html'], 'oldValue' => $actions_values_decoded[$actionKey]['custom_mail']['custom_html']),
                )) == false) {
                echo json_encode(array("status" => "no_datalang_for_primary"));
                die();
            }

            $html = new \Html2Text\Html2Text($_POST['pActionsValues'][$actionKey]['custom_mail']['custom_html']);
            $versione_testuale =  $html->getText();

            $_POST['pActionsValues'][$actionKey]['custom_mail']['custom_subject'] = $MSFrameworki18n->setFieldValue($actions_values_decoded[$actionKey]['custom_mail']['custom_subject'], $_POST['pActionsValues'][$actionKey]['custom_mail']['custom_subject']);
            $_POST['pActionsValues'][$actionKey]['custom_mail']['custom_html'] = $MSFrameworki18n->setFieldValue($actions_values_decoded[$actionKey]['custom_mail']['custom_html'], $_POST['pActionsValues'][$actionKey]['custom_mail']['custom_html']);
            $_POST['pActionsValues'][$actionKey]['custom_mail']['custom_txt'] = $MSFrameworki18n->setFieldValue($actions_values_decoded[$actionKey]['custom_mail']['custom_txt'], $versione_testuale);
        }
    }
}

if(in_array("quote", $_POST['pActions'])) {
    foreach($_POST['pActionsValues']['quote']['conditions'] as $quoteKey => $quoteValue)
    {
        if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                array('currentValue' => $quoteValue['action']['ref'], 'oldValue' => $actions_values_decoded['quote']['conditions'][$quoteKey]['action']['ref']),
            )) == false) {
            echo json_encode(array("status" => "no_datalang_for_primary"));
            die();
        }

        $_POST['pActionsValues']['quote']['conditions'][$quoteKey]['action']['ref'] = $MSFrameworki18n->setFieldValue($actions_values_decoded['quote']['conditions'][$quoteKey]['action']['ref'], $MSFrameworki18n->getFieldValue($quoteValue['action']['ref']));
    }
}

/* CARICA GLI ALLEGATI */
if(in_array("reply_to_mail", $_POST['pActions'])) {
    $uploader = new \MSFramework\uploads('FORMS');

    $ary_files = $uploader->prepareForSave($_POST['pActionsValues']['reply_to_mail']['attachments'], array("png", "jpeg", "jpg", "pdf"));
    $_POST['pActionsValues']['reply_to_mail']['attachments'] = json_encode($ary_files);
    if($ary_files === false) {
        echo json_encode(array("status" => "mv_error"));
        die();
    }
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "is_active" => $_POST['pIsActive'],
    "fields" => json_encode($_POST['pFields']),
    "actions" => json_encode($_POST['pActions']),
    "actions_values" => json_encode($_POST['pActionsValues']),
    "events" => json_encode($_POST['pEvents']),
    "enable_captcha" => $_POST['pEnableCaptcha'],
    "submit_btn_text" => $MSFrameworki18n->setFieldValue($r_old_data['submit_btn_text'], $_POST['pSubmitBtnTXT']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO forms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE forms SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if(in_array("reply_to_mail", $_POST['pActions'])) {
        if ($db_action == "insert") {
            $uploader->unlink($ary_files);
        }
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if(in_array("reply_to_mail", $_POST['pActions'])) {
    if ($db_action == "update") {
        $uploader->unlink($ary_files, $old_reply_to_mail_attachments);
    }
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
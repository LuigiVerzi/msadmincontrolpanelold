<?php
/**
 * MSAdminControlPanel
 * Date: 06/11/2018
 */ ?>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>Nome*</label>
        <select id="<?= $action['id'] ?>_nome" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['nome'] . '"', 'value="' . $actions_values_decoded[$action['id']]['nome'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Cognome</label>
        <select id="<?= $action['id'] ?>_cognome" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['cognome'] . '"', 'value="' . $actions_values_decoded[$action['id']]['cognome'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Email*</label>
        <select id="<?= $action['id'] ?>_email" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['email'] . '"', 'value="' . $actions_values_decoded[$action['id']]['email'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Cellulare</label>
        <select id="<?= $action['id'] ?>_telefono_cellulare" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['telefono_cellulare'] . '"', 'value="' . $actions_values_decoded[$action['id']]['telefono_cellulare'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Sito Web</label>
        <select id="<?= $action['id'] ?>_sito_web" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['sito_web'] . '"', 'value="' . $actions_values_decoded[$action['id']]['sito_web'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Data di Nascita</label>
        <select id="<?= $action['id'] ?>_data_nascita" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['data_nascita'] . '"', 'value="' . $actions_values_decoded[$action['id']]['data_nascita'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>Indirizzo</label>
        <select id="<?= $action['id'] ?>_indirizzo" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['indirizzo'] . '"', 'value="' . $actions_values_decoded[$action['id']]['indirizzo'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Stato</label>
        <select id="<?= $action['id'] ?>_stato" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['stato'] . '"', 'value="' . $actions_values_decoded[$action['id']]['stato'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Regione</label>
        <select id="<?= $action['id'] ?>_regione" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['regione'] . '"', 'value="' . $actions_values_decoded[$action['id']]['regione'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Citta</label>
        <select id="<?= $action['id'] ?>_citta" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['citta'] . '"', 'value="' . $actions_values_decoded[$action['id']]['citta'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Provincia</label>
        <select id="<?= $action['id'] ?>_provincia" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['provincia'] . '"', 'value="' . $actions_values_decoded[$action['id']]['provincia'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>CAP</label>
        <select id="<?= $action['id'] ?>_cap" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['cap'] . '"', 'value="' . $actions_values_decoded[$action['id']]['cap'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>
</div>

<h2 class="title-divider">Dati di fatturazione</h2>
<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>Ragione sociale</label>
        <select id="<?= $action['id'] ?>_ragione_sociale" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['ragione_sociale'] . '"', 'value="' . $actions_values_decoded[$action['id']]['ragione_sociale'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>P. IVA</label>
        <select id="<?= $action['id'] ?>_piva" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['piva'] . '"', 'value="' . $actions_values_decoded[$action['id']]['piva'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Codice Fiscale</label>
        <select id="<?= $action['id'] ?>_cf" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['cf'] . '"', 'value="' . $actions_values_decoded[$action['id']]['cf'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>Indirizzo</label>
        <select id="<?= $action['id'] ?>_indirizzo_azienda" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['indirizzo_azienda'] . '"', 'value="' . $actions_values_decoded[$action['id']]['indirizzo_azienda'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Stato</label>
        <select id="<?= $action['id'] ?>_stato_azienda" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['stato_azienda'] . '"', 'value="' . $actions_values_decoded[$action['id']]['stato_azienda'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Regione</label>
        <select id="<?= $action['id'] ?>_regione_azienda" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['regione_azienda'] . '"', 'value="' . $actions_values_decoded[$action['id']]['regione_azienda'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Citta</label>
        <select id="<?= $action['id'] ?>_citta_azienda" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['citta_azienda'] . '"', 'value="' . $actions_values_decoded[$action['id']]['citta_azienda'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Provincia</label>
        <select id="<?= $action['id'] ?>_provincia" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['provincia'] . '"', 'value="' . $actions_values_decoded[$action['id']]['provincia'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>CAP</label>
        <select id="<?= $action['id'] ?>_cap_azienda" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['cap_azienda'] . '"', 'value="' . $actions_values_decoded[$action['id']]['cap_azienda'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>
</div>

<h2 class="title-divider">Accesso</h2>
<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>L'utente deve potersi loggare?</label>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                    <input type="checkbox"  id="<?= $action['id'] ?>_enable_login" data-target=".anagrafica_login_info" class="toggle_target" <?php echo ($actions_values_decoded[$action['id']]['enable_login'] == 1 ? 'checked' : ''); ?>>
                    <i></i>
                </div>
                <span style="font-weight: normal;">Abilita Accesso</span>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-sm-6 anagrafica_login_info" style="display: <?php echo ($actions_values_decoded[$action['id']]['enable_login'] == 1 ? 'block' : 'none'); ?>;">
        <label>Password</label>
        <select id="<?= $action['id'] ?>_password" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['password'] . '"', 'value="' . $actions_values_decoded[$action['id']]['password'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6 anagrafica_login_info" style="display: <?php echo ($actions_values_decoded[$action['id']]['enable_login'] == 1 ? 'block' : 'none'); ?>;">
        <label>Password</label>
        <select id="<?= $action['id'] ?>_ripeti_password" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['ripeti_password'] . '"', 'value="' . $actions_values_decoded[$action['id']]['ripeti_password'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Accesso dopo il login</label>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                    <input type="checkbox"  id="<?= $action['id'] ?>_auto_login" <?php echo ($actions_values_decoded[$action['id']]['auto_login'] == 1 ? 'checked' : ''); ?>>
                    <i></i>
                </div>
                <span style="font-weight: normal;">Accedi Automaticamente</span>
            </label>
        </div>
    </div>
</div>
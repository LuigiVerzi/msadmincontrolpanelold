<?php
/**
 * MSAdminControlPanel
 * Date: 07/11/2018
 */ ?>

<div class="row">
    <div class="col-sm-6">
        <label>Nome Destinatario*</label>
        <select id="<?= $action['id'] ?>_name" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['name'] . '"', 'value="' . $actions_values_decoded[$action['id']]['name'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-sm-6">
        <label>Email Destinatario*</label>
        <select id="<?= $action['id'] ?>_email" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['email'] . '"', 'value="' . $actions_values_decoded[$action['id']]['email'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>
</div>

<div class="row m-t-md">
    <div class="col-sm-4">
        <label>Oggetto*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($actions_values_decoded[$action['id']]['subject']) ?>"></i></span>
        <input id="<?= $action['id'] ?>_subject" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($actions_values_decoded[$action['id']]['subject'])) ?>" required>
    </div>

    <div class="col-sm-4">
        <label>Indirizzo Mittente</label>
        <input id="<?= $action['id'] ?>_reply_to_addr" type="text" class="form-control" value="<?php echo $actions_values_decoded[$action['id']]['reply_to_addr'] ?>">
    </div>

    <div class="col-sm-4">
        <label>Nome Mittente</label>
        <input id="<?= $action['id'] ?>_reply_to_name" type="text" class="form-control" value="<?php echo $actions_values_decoded[$action['id']]['reply_to_name'] ?>">
    </div>
</div>

<h2 class="title-with-button">
    Gestione Email
    <a class="btn btn-default btn-sm btn_shortcodes" onclick="shortCodesModal('<?= $action['id'] ?>')"><i class="fa fa-code"></i> Shortcodes</a>
</h2>

<div class="alert alert-info shortcodes_alert" style="padding: 10px; display: none;">
    <h3>Lista shortcodes Campi</h3>
    <p class="fields_shortcodes">

    </p>
    <hr>
    <h3>Lista shortcodes Globali</h3>
    <p>
        <?php foreach($shortcodes_globali[0] as $k => $shortcode) { ?>
            <b><?= $shortcode; ?></b> - <?= $shortcodes_globali[1][$k]; ?><br>
        <?php } ?>
    </p>
</div>

<div class="row m-t-md <?= $action['id'] ?>_custom_layout_container">
    <div class="col-sm-12 <?= $action['id'] ?>_custom_layout_container_editor">
        <label>Versione HTML</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($actions_values_decoded[$action['id']]['custom_html']); ?>"></i></span>
        <div id="<?= $action['id'] ?>_versione_html_personalizzata" class="editor_template_html"><?php echo (new \MSFramework\emails())->formatTemplateForEditor($MSFrameworki18n->getFieldValue($actions_values_decoded[$action['id']]['custom_html'])); ?></div>
    </div>
</div>

<div class="row m-t-md">
    <div class="col-sm-12">
        <label>Allegati Email</label>
        <div id="<?= $action['id'] ?>_attachment_container">
            <?php
            (new \MSFramework\uploads('FORMS'))->initUploaderHTML("mainImages", $actions_values_decoded[$action['id']]['attachments']);
            $attachments = ob_get_contents();
            ?>
        </div>
    </div>
</div>
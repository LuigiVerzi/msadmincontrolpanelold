<?php
/**
 * MSAdminControlPanel
 * Date: 06/11/2018
 */ ?>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>Nome*</label>
        <select id="<?= $action['id'] ?>_name" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['name'] . '"', 'value="' . $actions_values_decoded[$action['id']]['name'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Cognome</label>
        <select id="<?= $action['id'] ?>_surname" class="form-control link_to_formfield">
            <?= str_replace(
                array(
                    '<option value=""></option>',
                    'value="' . $actions_values_decoded[$action['id']]['surname'] . '"'
                ),
                array(
                    '<option value="">Usa solo il campo \'Nome\'</option>',
                    'value="' . $actions_values_decoded[$action['id']]['surname'] . '" selected'
                ),
                $link_to_formfields_options
            ) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Email Iscrizione*</label>
        <select id="<?= $action['id'] ?>_email" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['email'] . '"', 'value="' . $actions_values_decoded[$action['id']]['email'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Checkbox Autorizzazione Iscrizione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Indica il checkbox che l'utente dovrà accettare per iscriversi alla newsletter"></i></span>
        <select id="<?= $action['id'] ?>_checkbox_allow_subscribe" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['checkbox_allow_subscribe'] . '"', 'value="' . $actions_values_decoded[$action['id']]['checkbox_allow_subscribe'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Liste statiche</label>
        <div class="checkboxes" id="<?= $action['id'] ?>_tag">
            <?php foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome FROM newsletter__tag") as $tag) { ?>
                <label style="margin-right: 15px;"><input type="checkbox" value="<?= $tag['id']; ?>" <?php if(in_array($tag['id'], $actions_values_decoded[$action['id']]['tag'])) { echo "checked"; } ?>> <?= $tag['nome']; ?></label>
            <?php } ?>
        </div>
    </div>

    <div class="col-sm-9">
        <label>Liste dinamiche</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selezionando uno o più campi, l'email del cliente verrà aggiunta in una lista (o inserita in una lista già esistente) denominata come il valore corrispondente a quel campo."></i></span>
        <div class="link_to_formfield checkboxes" id="<?= $action['id'] ?>_additional_tag">
            <?php
            $link_to_formfields_checkboxes_dyn = $link_to_formfields_checkboxes;
            foreach($actions_values_decoded[$action['id']]['additional_tag'] as $single_tag) {
                $link_to_formfields_checkboxes_dyn = str_replace('value="' . $single_tag . '"', 'value="' . $single_tag . '" checked', $link_to_formfields_checkboxes_dyn);
            }
            echo $link_to_formfields_checkboxes_dyn;
            ?>
        </div>
    </div>
</div>
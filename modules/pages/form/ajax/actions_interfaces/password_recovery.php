<?php
/**
 * MSAdminControlPanel
 * Date: 06/11/2018
 */ ?>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>Email*</label>
        <select id="<?= $action['id'] ?>_email" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['email'] . '"', 'value="' . $actions_values_decoded[$action['id']]['email'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>
</div>
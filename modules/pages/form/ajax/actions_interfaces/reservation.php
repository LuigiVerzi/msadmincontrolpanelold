<?php
/**
 * MSAdminControlPanel
 * Date: 06/11/2018
 */ ?>

<div class="row">
    <div class="col-sm-4">
        <label>Nome*</label>
        <select id="<?= $action['id'] ?>_name" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['name'] . '"', 'value="' . $actions_values_decoded[$action['id']]['name'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-sm-4">
        <label>Cognome*</label>
        <select id="<?= $action['id'] ?>_surname" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['surname'] . '"', 'value="' . $actions_values_decoded[$action['id']]['surname'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-sm-4">
        <label>Email Destinatario*</label>
        <select id="<?= $action['id'] ?>_email" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['email'] . '"', 'value="' . $actions_values_decoded[$action['id']]['email'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>
</div>
<div class="row m-t-md">

    <div class="col-lg-3 col-sm-6">
        <label>Data Arrivo*</label>
        <select id="<?= $action['id'] ?>_date_from" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['date_from'] . '"', 'value="' . $actions_values_decoded[$action['id']]['date_from'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Data Partenza*</label>
        <select id="<?= $action['id'] ?>_date_to" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['date_to'] . '"', 'value="' . $actions_values_decoded[$action['id']]['date_to'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Adulti*</label>
        <select id="<?= $action['id'] ?>_adults" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['adults'] . '"', 'value="' . $actions_values_decoded[$action['id']]['adults'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Bambini*</label>
        <select id="<?= $action['id'] ?>_children" class="form-control link_to_formfield required">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['children'] . '"', 'value="' . $actions_values_decoded[$action['id']]['children'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

</div>
<div class="row m-t-md">

    <div class="col-lg-3 col-sm-6">
        <label>Cellulare</label>
        <select id="<?= $action['id'] ?>_cell_phone" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['cell_phone'] . '"', 'value="' . $actions_values_decoded[$action['id']]['cell_phone'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-3 col-sm-6">
        <label>Origine</label>
        <select id="<?= $action['id'] ?>_origin" class="form-control link_to_formfield">
            <?= str_replace('value="' . $actions_values_decoded[$action['id']]['origin'] . '"', 'value="' . $actions_values_decoded[$action['id']]['origin'] . '" selected', $link_to_formfields_options) ?>
        </select>
    </div>

    <div class="col-lg-6 col-sm-6">
        <label>Corpo della prenotazione</label>
        <div class="link_to_formfield checkboxes" id="<?= $action['id'] ?>_body">
            <?php
            $link_to_formfields_checkboxes_dyn = $link_to_formfields_checkboxes;
            foreach($actions_values_decoded[$action['id']]['body'] as $single_tag) {
                $link_to_formfields_checkboxes_dyn = str_replace('value="' . $single_tag . '"', 'value="' . $single_tag . '" checked', $link_to_formfields_checkboxes_dyn);
            }
            echo $link_to_formfields_checkboxes_dyn;
            ?>
        </div>
    </div>
</div>

<div class="hr-line-dashed"></div>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Attenzione! Disabilitato questo campo NON verrà inviata nessuna email al momento dell'invio del form."></i></span>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                    <input type="checkbox" id="<?= $action['id'] ?>_default_mail" class="notifica_via_mail" <?= ($actions_values_decoded[$action['id']]['default_mail'] != "0" ? "checked" : "") ?>>
                    <i></i>
                </div>
                <span style="font-weight: normal;">Notifica via email</span>
            </label>
        </div>
    </div>

    <div class="col-lg-3 col-sm-6 use_custom_mail_container" style="display: <?= ($actions_values_decoded[$action['id']]['default_mail'] != "0" ? "block" : "none") ?>">
        <label>&nbsp;</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Selezionando questo campo sarà possibile personalizzare il template dell'email."></i></span>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                    <input type="checkbox" id="<?= $action['id'] ?>_use_custom_mail" class="use_custom_mail" <?= ($actions_values_decoded[$action['id']]['use_custom_mail'] == "1" ? "checked" : "") ?>>
                    <i></i>
                </div>
                <span style="font-weight: normal;">Personalizza email</span>
            </label>
        </div>
    </div>
</div>

<?php
// Il seguente array servirà a sostituire gli shortcode default del template con gli shortcode dinamici generali dal match dell'azione
// 'SHORTCODE TEMPLATE DEFAULT' => 'ID SELECT
$shortcode_match = array(
    'name' => 'name',
    'surname' => 'surname',
    'email' => 'email',
    'phone' => 'cell_phone',
    'arrival' => 'date_from',
    'departure' => 'date_to',
    'adults' => 'adults',
    'children' => 'children',
    'request' => 'body'
)
?>

<div class="template_personalizzato shortcode_replace" data-shortcodes_match="<?= htmlentities(json_encode($shortcode_match)); ?>" style="display: <?= ($actions_values_decoded[$action['id']]['use_custom_mail'] == "1" ? "block" : "none") ?>;">

    <h2 class="title-with-button">
        Gestione Email
        <a class="btn btn-default btn-sm btn_shortcodes" onclick="shortCodesModal('<?= $action['id'] ?>')"><i class="fa fa-code"></i> Shortcodes</a>
    </h2>

    <div class="alert alert-info shortcodes_alert" style="padding: 10px; display: none;">
        <h3>Lista shortcodes Campi</h3>
        <p class="fields_shortcodes">

        </p>
        <hr>
        <h3>Lista shortcodes Globali</h3>
        <p>
            <?php foreach($shortcodes_globali[0] as $k => $shortcode) { ?>
                <b><?= $shortcode; ?></b> - <?= $shortcodes_globali[1][$k]; ?><br>
            <?php } ?>
        </p>
    </div>

    <?php

    if($actions_values_decoded[$action['id']]['use_custom_mail'] == "1" && strlen($MSFrameworki18n->getFieldValue($actions_values_decoded[$action['id']]['custom_mail']['custom_html'])) > 10) {
        $template_html = $actions_values_decoded[$action['id']]['custom_mail']['custom_html'];
        $template_txt = $actions_values_decoded[$action['id']]['custom_mail']['custom_txt'];
        $template_subject = $actions_values_decoded[$action['id']]['custom_mail']['custom_subject'];
    }
    else {

        $contact_form_template = (new \MSFramework\emails())->getTemplates('hotel_booking_request', true);

        $template_html = $contact_form_template['data']['html'];
        $template_txt = $contact_form_template['data']['txt'];
        $template_subject = $contact_form_template['data']['subject'];
    }
    ?>

    <div class="row">
        <div class="col-sm-12">
            <label>Oggetto*</label> <span class="ms-label-tooltip m-l-sm required"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($template_subject) ?>"></i></span>
            <input id="<?= $action['id'] ?>_custom_subject" type="text" class="editor_template_subject form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($template_subject)) ?>" required>
        </div>
    </div>

    <div class="row m-t-md <?= $action['id'] ?>_custom_layout_container">
        <div class="col-sm-12 <?= $action['id'] ?>_custom_layout_container_editor">
            <label>Versione HTML</label> <span class="ms-label-tooltip m-l-sm required"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($template_html); ?>"></i></span>
            <div id="<?= $action['id'] ?>_versione_html_personalizzata" class="editor_template_html"><?php echo (new \MSFramework\emails())->formatTemplateForEditor($MSFrameworki18n->getFieldValue($template_html)); ?></div>
        </div>
    </div>

</div>
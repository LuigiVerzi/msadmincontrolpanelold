<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../../sw-config.php');
require('../../config/moduleConfig.php');

$formActions = (new \MSFramework\forms())->getFormActionsInfo();

?>

<div class="modal inmodal" id="globalFormModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-id-card-o modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Importa Form Globali</h4>
                <small class="font-bold">Seleziona uno tra i form seguenti per continuare con l'importazione.</small>
            </div>
            <div class="modal-body">

                <?php foreach($MSFrameworkDatabase->getAssoc('SELECT * FROM marke833_framework.forms') as $form) { ?>
                    <a href="#" class="btn btn-default btn-block selectGlobalForm" data-id="<?= $form['id']; ?>" style="padding: 10px 15px; text-align: right; display: flex; align-items: center; justify-content: center;">
                       <h3 style="text-align: left; flex: 1; margin: 0;"><?= $form['nome']; ?></h3>
                        <div class="actions">
                            <?php foreach(json_decode($form['actions'], true) as $action) { ?>
                                <span style="margin-left: 5px;" class="label label-inverse"><i class="fa <?= $formActions[$action]['fa-icon']; ?>"></i> <?= $formActions[$action]['name']; ?></span>
                            <?php } ?>
                        </div>
                    </a>

                <?php } ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
            </div>
        </div>
    </div>
</div>

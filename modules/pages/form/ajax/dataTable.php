<?php
/**
 * MSAdminControlPanel
 * Date: 25/10/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM forms") as $r) {
    $array['data'][] = array(
        $r['nome'],
        ($r['is_active'] == "1") ? "Si" : "No",
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 28/10/2018
 */

if(!isset($_POST['pIncludedFromEdit'])) {
    require_once('../../../../sw-config.php');
    require('../config/moduleConfig.php');
}

$ary_inputs = array();
foreach($_POST['pCurFormData'] as $curFormRow) {
    if(!isset($_POST['pIncludedFromEdit'])) {
        $name = $curFormRow['name'];
    } else {
        $name = $MSFrameworki18n->getFieldValue($curFormRow['name'], true);
    }

    $ary_inputs[$curFormRow['field_id']] = array("name" => $name);
}

if(!is_array($_POST['pCurQuoteData']['conditions'])) {
    $_POST['pCurQuoteData']['conditions'][] = array('');
}
?>

<div class="easyRowDuplicationContainer">
    <?php
    foreach($_POST['pCurQuoteData']['conditions'] as $actionMainBlockK => $actionMainBlock) {
        $condition_actions = "";
        $done_actionblock = false;
    ?>

    <div class="stepsDuplication rowDuplication actionDuplication m-t-md m-b-md" style="background-color: #f8c5c6; padding: 20px; border: 1px solid rgba(0, 0, 0, 0.1);">

        <div class="easyRowDuplicationContainer">
            <?php
            foreach($actionMainBlock as $quoteMainBlockK => $quoteMainBlock) {
                if($quoteMainBlockK == "action") {
                    $condition_actions = $quoteMainBlock;

                    if($done_actionblock) {
                        continue;
                    }
                }

                $done_actionblock = true;
            ?>

            <div class="stepsDuplication rowDuplication quoteDuplication m-t-md m-b-md" style="background-color: #d1e1f8; padding: 20px; border: 1px solid rgba(0, 0, 0, 0.1);">

                <div class="easyRowDuplicationContainer">
                    <?php
                    $concat_value = "";
                    $done_block = false;

                    foreach($quoteMainBlock as $quoteSubBlockK => $quoteSubBlock) {
                        $quoteSubBlockValue = $quoteSubBlock;

                        if($quoteSubBlockK === "concat") {
                            $concat_value = $quoteSubBlock;
                            $quoteSubBlockValue = array();

                            if($done_block) {
                                continue;
                            }
                        }

                        $done_block = true;
                    ?>
                    <div class="stepsDuplication rowDuplication subconditionDuplication m-b-md" style="background-color: #e0f8f8; padding: 20px; border: 1px solid rgba(0, 0, 0, 0.1);">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Se</label>
                                <select class="form-control condition_field" data-type="<?= $inputNameV['type'] ?>" data-input-type="<?= $inputNameV['input_type'] ?>">
                                    <option value=""></option>
                                    <?php
                                    foreach($ary_inputs as $inputName => $inputNameV) {
                                    ?>
                                        <option value="<?= $inputName ?>" <?= ($inputName == $quoteSubBlockValue['field'] ? "selected" : "") ?>><?= $inputNameV['name'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label> &nbsp; </label>
                                <select class="form-control condition_operator">
                                    <option value="equal" <?= ('equal' == $quoteSubBlockValue['operator'] ? "selected" : "") ?>>E' uguale</option>
                                    <option value="not_equal" <?= ('not_equal' == $quoteSubBlockValue['operator'] ? "selected" : "") ?>>E' diverso</option>
                                    <option value="is_more_than" <?= ('is_more_than' == $quoteSubBlockValue['operator'] ? "selected" : "") ?>>E' maggiore</option>
                                    <option value="is_less_than" <?= ('is_less_than' == $quoteSubBlockValue['operator'] ? "selected" : "") ?>>E' minore</option>
                                    <option value="is_checked" <?= ('is_checked' == $quoteSubBlockValue['operator'] ? "selected" : "") ?>>E' checkato</option>
                                    <option value="is_not_checked" <?= ('is_not_checked' == $quoteSubBlockValue['operator'] ? "selected" : "") ?>>Non è checkato</option>
                                </select>
                            </div>

                            <div class="col-sm-6 condition_value_container" style="display: <?= ($quoteSubBlockValue['operator'] == 'is_checked' || $quoteSubBlockValue['operator'] == 'is_not_checked' ? 'none' : 'block') ?>;">
                                <label>Valore</label>
                                <input type="text" class="form-control condition_value" value="<?= $quoteSubBlockValue['value'] ?>">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-push-5 col-sm-2">
                                <label> &nbsp; </label>
                                <select class="form-control concat">
                                    <option value="and" <?= ('and' == $quoteSubBlockValue['concat'] ? "selected" : "") ?>>AND (&&)</option>
                                    <option value="or" <?= ('or' == $quoteSubBlockValue['concat'] ? "selected" : "") ?>>OR (||)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="row">
                    <div class="col-md-push-5 col-sm-2">
                        <label> &nbsp; </label>
                        <select class="form-control group_concat">
                            <option value="and" <?= ('and' == $concat_value ? "selected" : "") ?>>AND (&&)</option>
                            <option value="or" <?= ('or' == $concat_value ? "selected" : "") ?>>OR (||)</option>
                        </select>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
        </div>


        <div class="row">
            <div class="col-sm-4">
                <label>Riferimento</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language condition_ref_multilang" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities((isset($condition_actions['ref_multilang']) ? $condition_actions['ref_multilang'] : $condition_actions['ref'])); ?>"></i></span>
                <input type="text" class="form-control condition_ref" placeholder="Il riferimento da mostrare nel resoconto (Es: Pernottamenti)" value="<?= $MSFrameworki18n->getFieldValue($condition_actions['ref']); ?>">
            </div>
        </div>

        <div class="hr-line-dashed"></div>

        <div class="row">
            <div class="col-sm-3">
                <label>Azione da compiere</label>
                <select class="form-control condition_action_type">
                    <option value="discount_percentage" <?= ('discount_percentage' == $condition_actions['type'] ? "selected" : "") ?>>Applica sconto %</option>
                    <option value="discount_eur" <?= ('discount_eur' == $condition_actions['type'] ? "selected" : "") ?>>Applica sconto <?= CURRENCY_SYMBOL; ?></option>
                    <option value="supplement_percentage" <?= ('supplement_percentage' == $condition_actions['type'] ? "selected" : "") ?>>Applica supplemento %</option>
                    <option value="supplement_eur" <?= ('supplement_eur' == $condition_actions['type'] ? "selected" : "") ?>>Applica supplemento <?= CURRENCY_SYMBOL; ?></option>
                    <option value="formula" <?= ('formula' == $condition_actions['type'] ? "selected" : "") ?>>Applica formula</option>
                </select>
            </div>

            <div class="col-sm-9">
                <label>Valore</label>
                <input type="text" class="form-control condition_action_value" value="<?= $condition_actions['value'] ?>">
            </div>
        </div>

    </div>
    <?php } ?>
</div>
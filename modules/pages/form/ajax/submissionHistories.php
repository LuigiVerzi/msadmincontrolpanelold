<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$form_id = $_GET['form_id'];
$form_details = (new \MSFramework\forms())->getFormDetails($form_id)[$form_id];

$languges_details = $MSFrameworki18n->getLanguagesDetails();

$form_fields = json_decode($form_details['fields'], true);
$form_fields_names = array();

foreach ($form_fields as $form_field) {
    $form_fields_names[$form_field['field_id']] = $MSFrameworki18n->getFieldValue($form_field['name']);
}

$form_fields_names['gdpr_form_checkbox'] = 'GDPR Checkbox';
$form_fields_names['submit_origin'] = 'Origine Invio';

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array( 'db' => 'submit_date',  'dt' => 0,
        'formatter' => function( $d, $row ) {
            return date('d/m/Y H:i', strtotime($d)) . '<br><small style="display: block; line-height: 1;">' . (new \MSFramework\utils())->smartdate(strtotime($d)) . '</small>';
        }
    ),
    array( 'db' => 'user_ref',  'dt' => 1 ),
    array( 'db' => 'form_results',  'dt' => 2,
        'formatter' => function( $d, $row ) {

            $d = json_decode($d, true);

            $status_label_html = array();
            foreach($d as $status_key => $status_label) {
                if ($status_label) {
                    $status_label_html[] = '<span class="label label-' . ($status_label['status'] == 'true' || $status_label['status'] == 1 ? 'primary' : ($status_label['status'] == 2 ? 'warning' : 'danger')) . '" title="' . htmlentities($status_label['titolo']) . '&#013;' . htmlentities($status_label['message']) . '">' . $status_key . '</span>';
                }
            }

            return implode(' ', $status_label_html);

        }
    ),
    array( 'db' => 'lang',   'dt' => 3,
        'formatter' => function( $d, $row ) {
        Global $languges_details;
            return '<img src="' . FRAMEWORK_COMMON_CDN . 'img/languages/' . $languges_details[$d]['flag_code'] . '.png"> ' . $languges_details[$d]['italian_name'];
        }
    ),
    array( 'db' => 'id', 'dt' => 4,
        'formatter' => function( $d, $row ) {
            return '<a href="#" class="btn btn-default btn-block btn-sm">Dettagli</a>';
        }
    ),
    array( 'db' => 'form_data', 'dt' => 5,
        'formatter' => function( $d, $row ) {
        Global $form_fields_names;
        $form_data = json_decode($d, true);

            $form_html_values = array();
            foreach($form_data as $field_name => $field_value) {
                $form_html_values[] = '<h3 style="margin: 0;">' . $form_fields_names[$field_name] . '</h3><p>' . $field_value . '</p>';
            }

            return implode('<hr>', $form_html_values);

        }
    ),

    /*
    array( 'db' => 'registration_date', 'dt' => 5,
        'formatter' => function( $d, $row ) {
            return array("display" => date("d/m/Y H:i", strtotime($d)), "sort" => strtotime($d));
        }
    )
        */
);


echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, 'forms_histories', 'id', $columns, null, 'form_id = ' . $form_id)
);

$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');

    $('body').on('click', '.dataTableImportGlobalForm, #dataTableImportGlobalForm', function (e) {
       e.preventDefault();

        $('#globalFormModal').remove();

        $.ajax({
            url: "ajax/global/getGlobalFormsModal.php",
            type: "POST",
            async: false,
            dataType: "text",
            success: function(data) {
                $('body').append(data);
                $('#globalFormModal').modal('show').one('click', '.selectGlobalForm', function (e) {
                    e.preventDefault();

                    var form_id = $(this).data('id');
                    var current_id = ($('#record_id').length ? $('#record_id').val() : '');

                    if(current_id !== '') {
                        bootbox.confirm({
                            message: 'Vuoi sostituire il form attuale con uno predefinito? Proseguendo perderai le modifiche attuali',
                            buttons: {
                                confirm: {
                                    label: 'Si, importa form',
                                    className: 'btn-success',
                                },
                                cancel: {
                                    label: 'No, annulla',
                                    className: 'btn-danger',
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    location.href = 'edit.php?id=' + current_id + '&import=' + form_id;
                                }
                            }
                        });
                    } else {
                        location.href = 'edit.php?id=' + current_id + '&import=' + form_id;
                    }

                    $('#globalFormModal').modal('hide');

                });

            }
        })

    });
});

function initForm() {
    initClassicTabsEditForm();
    checkQuoteTab();

    $('#hidden_fields_container').on('click', '.delete_hidden_field', function (e) {
        e.preventDefault();

        var modal_id = $(this).closest('.modal').attr('id');

        $('#' + modal_id).one('hidden.bs.modal', function () {
            $('[data-target="#' + modal_id + '"]').remove();
            $('#' + modal_id).remove();
        });

        $('#' + modal_id).modal('hide');

    });

    $('#hidden_fields_container').on('input', '.modal .field_name', function (e) {
        e.preventDefault();

        var modal_id = $(this).closest('.modal').attr('id');
        $('[data-target="#' + modal_id + '"]').text($(this).val());

    });

    $('#form-p-0').on('click', '[data-target="#new_hidden_modal"]', function (e) {
        $("#new_hidden_modal").find('input').val('');
        $("#new_hidden_modal").find('.field_uniqueid').val('u' + Math.random().toString(36).substr(2, 9));
    });

    $('#form-p-0').on('click', '.add_hidden_input', function (e) {
        e.preventDefault();

        var $modal = $(this).closest('.modal');
        var new_key = $('#hidden_fields_container').find('button').length + 1;

        var field_name = $modal.find('.field_name').val();
        var field_id = $modal.find('.field_id').val();
        var field_default = $modal.find('.field_default').val();


        if (!field_name.length || !field_id.length) {
            bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
        }
        else {
            $('#hidden_fields_container').append('<button type="button" class="btn btn-default btn-sm" style="margin-bottom: 3px;" data-toggle="modal" data-target="#hidden_modal_' + new_key + '">' + field_id + '</button>' + $('#hidden_field_template').val().replace('{id}', new_key));
            $('#hidden_fields_container #hidden_modal_' + new_key).find('.modal-body').html($modal.find('.modal-body').html());
            $('#hidden_fields_container #hidden_modal_' + new_key).find('.field_name').val(field_name);
            $('#hidden_fields_container #hidden_modal_' + new_key).find('.field_id').val(field_id);
            $('#hidden_fields_container #hidden_modal_' + new_key).find('.field_default').val(field_default);
            initFieldTypeChange();
            $('#hidden_fields_container #hidden_modal_' + new_key).find('.field_name').blur();
            $modal.modal('hide');
        }

    });

    $('.fieldsDuplication .required').each(function () {
        $(this).attr('name', 'r_' + Math.random().toString(36).substring(2) + '_' + Math.random().toString(36).substring(2));
    });

    if (!$('.fieldsDuplication').hasClass('no_edit')) {
        $('.fieldsDuplication').easyRowDuplication({
            "addButtonClasses": "btn btn-info",
            "deleteButtonClasses": "btn btn-warning",
            "addButtonText": "Aggiungi campo",
            "deleteButtonText": "Elimina campo",
            "sortable": true,
            "sortableOptions": {
                "sortHolderClass": "ibox-title"
            },
            "beforeAddCallback": function () {
                $('.fieldsDuplication').find('.field_checkbox_checked').iCheck('destroy');
                $('.fieldsDuplication').find('.field_mandatory').iCheck('destroy');
            },
            "afterAddCallback": function () {

                // Aggiungo un parametro name a caso necessario per far funzionare correttamente jquery validation
                $('.fieldsDuplication:last').find('.required').each(function () {
                    $(this).attr('name', 'r_' + Math.random().toString(36).substring(2) + '_' + Math.random().toString(36).substring(2));
                });

                $('.fieldsDuplication:last').find('.ibox').removeClass('collapsed').addClass('border-bottom');
                $('.fieldsDuplication:last').find('.ibox .ibox-content').show();
                $('.fieldsDuplication:last').find('.ibox-title h5 .title').html('');
                $('.fieldsDuplication:last').find('.field_uniqueid').val('u' + Math.random().toString(36).substr(2, 9));
                $('.fieldsDuplication:last').find('.field_name').val('');
                $('.fieldsDuplication:last').find('.field_name').attr('data-main-lang-value', '');
                $('.fieldsDuplication:last').find('.field_add_class').val('');
                $('.fieldsDuplication:last').find('.field_add_container_class').val('');
                $('.fieldsDuplication:last').find('.field_add_container_size ').val('').attr('class', 'form-control field_add_container_size no-plugin');
                $('.fieldsDuplication:last').find('.ms-options-wrap').remove();
                $('.fieldsDuplication:last').find('.field_description').val('');
                $('.fieldsDuplication:last').find('.field_mandatory').iCheck('uncheck');
                $('.fieldsDuplication:last').find('.field_type').val('0').trigger('change');
                $('.fieldsDuplication:last').find('.input_type').val('text').trigger('change');

                $('.fieldsDuplication:last').find('.field_placeholder').val('');
                $('.fieldsDuplication:last').find('.field_default').val('');
                $('.fieldsDuplication:last').find('.field_value').val('');
                $('.fieldsDuplication:last').find('.field_text').val('');
                $('.fieldsDuplication:last').find('.field_number_min').val('');
                $('.fieldsDuplication:last').find('.field_number_max').val('');
                $('.fieldsDuplication:last').find('.field_checkbox_checked').iCheck('uncheck');
                $('.fieldsDuplication:last').find('.field_rows').val('4');

                var field_id = (+new Date());

                $('.fieldsDuplication:last').find('.field_id').val(field_id);
                $('.fieldsDuplication:last').find('.ibox-title h5 .id').html('- ' + field_id);

                initFieldsDuplication(true, 'options');
                initFieldsDuplication(true, 'radio');
                initIChecks();
                initFieldTypeChange();
                initSizeSelects();

                $('.fieldsDuplication:last .collapse-link').on('click', function (e) {
                    e.preventDefault();
                    var ibox = $(this).closest('div.ibox');
                    var button = $(this).find('i');
                    var content = ibox.children('.ibox-content');
                    content.slideToggle(200);
                    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                    ibox.toggleClass('').toggleClass('border-bottom');
                    setTimeout(function () {
                        ibox.resize();
                        ibox.find('[id^=map-]').resize();
                    }, 50);
                });

                if ($('#quote.active').length == 1) {
                    composeQuoteRows();
                }
            },
        });
    }

    $('#form-p-0').on('click', '.ibox .close-link', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $(this).closest('.stepsDuplication').remove();
    });

    $('#form-p-0').on('change', '.ibox .field_name, .ibox .field_id', function (e) {

        var titolo = $(this).closest('.ibox').find('.field_name').val();
        var id = $(this).closest('.ibox').find('.field_id').val();

        if(!titolo.length) titolo = '<span class="text-danger">Nome Mancante</span>';
        if(!id.length) id = '<span class="text-danger">ID Mancante</span>';

        $(this).closest('.ibox').find('.ibox-title h5 .title').html(titolo);
        $(this).closest('.ibox').find('.ibox-title h5 .id').html('- ' + id);
        initFieldTypeChange();

    });

    initFieldTypeChange();

    setTimeout(function() {

        /* GESTIONE AZIONI */
        $('.form_actions_check').on('click', '.activation_btn', function(e) {
            e.preventDefault();

            var $action = $(this).closest('.form_actions_check');
            $action.toggleClass('active');

            if($action.hasClass('active')) {
                $action.find('.active_actions').show();
                $action.find('.inactive_actions').hide();
                $('#action_' +$action.attr('id') + '_container').show();
                onFormActionShow($(this).attr('id'));
            } else {
                $('#action_' +$action.attr('id') + '_container').hide();
                onFormActionHide($(this).attr('id'));
                $action.find('.active_actions').hide();
                $action.find('.inactive_actions').show();
            }

            if($('.action_container:visible').length) {
                $('.manage_actions_section').show();
            }
            else {
                $('.manage_actions_section').hide();
            }

            if($action.attr('id') == 'quote') {
                checkQuoteTab();
            }
            else if($action.attr('id') == 'reply_to_mail') {

            }
            else if($action.attr('id') == 'generic_request') {

            }
        });

        $('.form_actions_check .edit_preference').on('click', function(event) {
            var action_id = $(this).closest('.form_actions_check').attr('id');

            if(action_id == 'quote') {
                $('#form .steps ul li:eq(2) a').click();
            }
            else {
                $('#action_' + action_id + '_container .ibox').removeClass('collapsed');
                $('#action_' + action_id + '_container .fullscreen-link').click();
            }
        });

        $('.action_container .manage_messages').on('ifToggled', function(event) {
            var $message_container = $(this).closest('.action_container').find('.manage_messages_container');
            if($(this).is(':checked')) $message_container.show();
            else $message_container.hide();
        });

        $('.action_container .manage_template').on('ifToggled', function(event) {
            var $message_container = $(this).closest('.action_container').find('.manage_template_container');
            if($(this).is(':checked')) $message_container.show();
            else $message_container.hide();
        });

        $('.action_container .toggle_target').on('ifToggled', function(event) {
            var $target = $($(this).data('target'));
            if($(this).is(':checked')) $target.show();
            else $target.hide();
        });

        /* EMAIL PERSONALIZZATE */
        $('.action_container .notifica_via_mail').on('ifToggled', function(event) {
            var $use_custom_mail_container = $(this).closest('.action_container').find('.use_custom_mail_container');
            if($(this).is(':checked')) $use_custom_mail_container.show();
            else {
                $use_custom_mail_container.hide();
                $use_custom_mail_container.find('.use_custom_mail').iCheck('uncheck');
            }
        });


        var refreshOnLink = function(e) {
            var container = $(this).closest('.action_container').data('container-of');

            onFormActionShow(container);

            /* DISABILITATO IL RESTO PER ADESSO
            var this_value = $(this).val();

            var duplicate = 0;
            $(this).closest('.action_container').find('.link_to_formfield').each(function () {
                if($(this).val() == this_value) duplicate++;
            });

            if($(this).val() != '' && duplicate < 2) {
                replaceDefaultTemplateShortcodes(container);
            }
            */
        };

        $('.action_container .use_custom_mail').on('ifToggled', function(event) {
            var $action_container = $(this).closest('.action_container');
            var $custom_mail_container = $action_container.find('.template_personalizzato');

            if ($(this).is(':checked')) {

                var fields_ok = true;

                $action_container.find('.link_to_formfield.required').each(function () {
                    if($(this).val() == '') {
                        fields_ok = false;
                        $(this).addClass('error').one('change', function () {
                            $(this).removeClass('error');
                        });
                    }
                });

                if(fields_ok) {
                    $custom_mail_container.show();
                    onFormActionShow($action_container.data('container-of'));
                    $action_container.find('.link_to_formfield').on('change', refreshOnLink);

                }
                else
                {
                    bootbox.error("Prima di iniziare a personalizzare il template assicurati di aver collegato i campi richiesti.");
                    setTimeout(function(){ $action_container.find('.use_custom_mail').iCheck('uncheck');}, 1);
                }

            }
            else
            {
                $custom_mail_container.hide();
                $action_container.find('.link_to_formfield').off('change', refreshOnLink);
            }

        });

        /* GESTIONE EVENTI */
        $('.eventBox .eventSettings_enabled').on('ifChanged', function (event) {
            if($(this).is(':checked')) {
                $(this).closest('.eventBox').removeClass('collapsed');

                $('.ace_editor:visible').css('height', '300px');
                hook_editor.forEach(function(s_editor) {
                    s_editor.resize();
                });

            }
            else {
                $(this).closest('.eventBox').addClass('collapsed');
            }
        });

        JSMode = ace.require("ace/mode/javascript").Mode;

        hook_editor = [];
        $('.hook_editor').each(function () {
            $(this).closest('.eventBox').data('ace_id', hook_editor.length)
            var single_editor = ace.edit($(this)[0], {
                minLines: 5
            });
            single_editor.session.setMode(new JSMode());
            hook_editor.push(single_editor);
        });

    }, 500);

    initFieldsDuplication(false, 'options');
    initFieldsDuplication(false, 'radio');
    initSizeSelects();

    window.versione_html_personalizzata = {};
    $('.editor_template_html').each(function () {
        var id = $(this).attr('id');
        window.versione_html_personalizzata[id] = initEmailEditor('#' + id, 'NEWSLETTER', getEditorShortcodes());
        if(id !== 'reply_to_mail_versione_html_personalizzata') {
            window.versione_html_personalizzata[id].setShowSettingsLoadTemplate(false);
        }
    });

    $('.emailTags').tagsInput({
        'height':'35px',
        'width':'100%',
        'interactive':true,
        'defaultText':'Aggiungi indirizzo email',
        'removeWithBackspace' : true,
        'onAddTag': function (value) {
            if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value)) {
                $(this).removeTag(value);
            }
        }
    });


    initOrak('mainImages', 10, 0, 0, 100, getOrakImagesToPreattach('mainImages'), false, ['image/jpeg', 'image/png', 'application/pdf']);

    $('.field_type').trigger('change');
    $('.input_type').trigger('change');

    $.validator.setDefaults({
        showErrors: function (errorMap, errorList) {
            $('.required').each(function () {
                if($(this).val() == '' && $(this).closest('.ibox-content').length && $(this).closest('.ibox-content').css('display') != 'block') {
                    $(this).closest('.ibox').find('.collapse-link').click();
                    $(this).addClass('error');
                    $("#form").valid();
                }
            });
            this.defaultShowErrors();
        }
    });

    /* TABELLA CRONOLOGIA */
    var datatableFormat = function(d) {
        return '<table cellpadding="5" cellspacing="0" border="0" style="width: 100%; border: 1px solid #ececec;">'+
        '<tr style="background: white;">'+
        '<td style="padding: 15px;">'+d[5]+'</td>'+
        '</tr>'+
        '</table>'
    }

    var form_submission_table = loadStandardDataTable('form_submission_histories', true, {ajax: 'ajax/submissionHistories.php?form_id=' + $('#record_id').val()}, false);
    $('#form_submission_histories tbody').on('click', 'td', function () {
        var tr = $(this).closest('tr');
        var row = form_submission_table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( datatableFormat(row.data()) ).show();
            tr.addClass('shown');
        }
    } );

}

function initFieldsDuplication(last, type) {
    //questa funzione gestisce la duplicazione delle options e dei radio
    selector = "";
    if(last) {
        selector = ':last';
    }

    $('.' + type + 'Duplication' + selector).easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-danger",
        "addButtonText": "Aggiungi opzione",
        "deleteButtonText": "Elimina opzione",
        "afterAddCallback": function(dup_element) {
            $(dup_element).closest('.fieldsDuplication').find('.' + type + 'Duplication:last').find('.field_value').val('');
            $(dup_element).closest('.fieldsDuplication').find('.' + type + 'Duplication:last').find('.field_text').val('');

            if($('#quote.active').length == 1) {
                composeQuoteRows();
            }
        }
    });
}

function initFieldTypeChange () {

    if($('#quote.active').length == 1) {
        composeQuoteRows();
    }

    ary_form_fields = getFormAry(true, true) || new Object();

    /* CREO LE OPTIONS PER IL SELECT */
    link_to_formfield_options = "<option value=''></option><optgroup label='Campi Visibili'>";
    var first_hidden = true;
    for(k in ary_form_fields) {
        if(ary_form_fields[k]['type'] == 0 && ary_form_fields[k]['input_type'] == 'hidden' && first_hidden) {
            link_to_formfield_options += "</optgroup><optgroup label='Campi Nascosti'>";
            first_hidden = false;
        }
        link_to_formfield_options += "<option data-unique='" + ary_form_fields[k]['unique_id'] + "' value='" + ary_form_fields[k]['field_id'] + "'>" + ary_form_fields[k]['name'] + "</option>";
    }
    link_to_formfield_options += "</optgroup>";

    /* CREO LE OPTIONS PER LE CHECKBOX */
    link_to_formfield_checkbox = "";
    for(k in ary_form_fields) {
        link_to_formfield_checkbox += '<label><input type="checkbox" value="' + ary_form_fields[k]['field_id'] + '" > ' + ary_form_fields[k]['name'] + '</label>';
    }

    // Lo assegno una volta sola qui sopra per evitare di farlo richiamare troppe volte rallentando
    var form_field = getFormAry(false, true);
    var cur_val = '';

    $('#action_generic_request_container select.link_to_formfield').off('change');

    //ricompongo i link nel tab azioni
    $('.link_to_formfield').each(function() {
        var $block = $(this);
        if($block.hasClass('checkboxes'))
        {
            cur_val = new Array();
            $block.find('input:checked').each(function () {
                cur_val.push($(this).val());
            });

            $block.html(link_to_formfield_checkbox);

            $block.find('input').each(function () {
                if($.inArray($(this).val(), cur_val) >= 0) {
                    $(this).prop('checked', true);
                }
            });
        }
        else {

            cur_val = $block.val();
            if(cur_val.length > 0)
            {

                if(!Object.keys(getFieldBy('field_id', cur_val, form_field)).length) {
                    var new_val = getFieldBy('unique_id', $block.find('option:selected').data('unique'), form_field);

                    if(Object.keys(new_val).length > 0) {
                        cur_val = new_val['field_id'];
                    }
                }
            }

            var empty_text = ($block.find('[value=""]').length ? $block.find('[value=""]').text() : '');
            $block.html(link_to_formfield_options.replace("<option value=''></option>", "<option value=''>" + empty_text + "</option>")).val(cur_val).change();
        }
    });

    // Nascondo dalle info aggiuntive i campi già selezionati
    $('#action_generic_request_container select.link_to_formfield').on('change', initFieldTypeChange);
    $('#action_generic_request_container').find('select.link_to_formfield').each(function () {
       if($(this).val().length) {
           $('#generic_request_additional_info [value="' + $(this).val() + '"]').parent().remove();
       }
    });


    $('.field_type').unbind('change');
    $('.field_type').on('change', function() {
        index = $(".field_type").index($(this));

        $('.type_container_overall:eq(' + index + ') .type_container').hide();
        $('.type_container_overall:eq(' + index + ') .type_' + $(this).val() + '_container').show();

        if($(this).val() == "0") {
            $('.input_type_select_container:eq(' + index + ')').show();
            $('.field_name_container:eq(' + index + ')').removeClass('col-sm-9').addClass('col-sm-6');
        } else {
            $('.input_type_select_container:eq(' + index + ')').hide();
            $('.field_name_container:eq(' + index + ')').removeClass('col-sm-6').addClass('col-sm-9');
        }

        $('.input_type:eq(' + index + ')').trigger('change');
    })

    if(typeof(window.versione_html_personalizzata) !== 'undefined' && Object.keys(window.versione_html_personalizzata).length) {
        Object.keys(window.versione_html_personalizzata).forEach(function (key) {
            window.versione_html_personalizzata[key].setShortcodesList(getEditorShortcodes());
            window.versione_html_personalizzata[key].composeShortcodesView();
        });
    }

    initInputTypeChange();
}

function initInputTypeChange() {
    $('.input_type').unbind('change');
    $('.input_type').on('change', function() {
        index = $(".input_type").index($(this));

        $('.type_container_overall:eq(' + index + ') .input_type_container').hide();
        $('.type_container_overall:eq(' + index + ') .input_type_' + $(this).val()).show();
    })
}

function initSizeSelects() {
    $('.field_add_container_size:not(.jqmsLoaded)').multiselect({
        columns: 2,
        texts: {
            placeholder: 'Modifica larghezza'
        },
        onOptionClick: function( element, option ) {
            if($(option).is(':checked')) {
                $(option).closest('ul').find('[type="checkbox"]:checked').not(option).click();
            }
        }
    });
}

function getFormAry(HTMLPreview, show_common_shortcodes) {
    if(typeof(HTMLPreview) == "undefined") {
        HTMLPreview = false;
    }

    if(typeof(show_common_shortcodes) == "undefined") {
        show_common_shortcodes = false;
    }

    fields_ary = new Array();
    $('.stepsDuplication.fieldsDuplication .ibox').each(function(row_index) {
        fields_ary_tmp = new Object();

        if(HTMLPreview) {
            field_name = ($(this).find('.field_name').val() != "" ? $(this).find('.field_name').val() : $(this).find('.field_name').attr('data-main-lang-value'));
        } else {
            field_name = $(this).find('.field_name').val();
        }

        fields_ary_tmp['unique_id'] =  $(this).find('.field_uniqueid').val();
        fields_ary_tmp['name'] = field_name;
        fields_ary_tmp['description'] = $(this).find('.field_description').val();
        fields_ary_tmp['type'] = $(this).find('.field_type').val();
        fields_ary_tmp['field_id'] = $(this).find('.field_id').val();
        fields_ary_tmp['add_class'] = $(this).find('.field_add_class').val();
        fields_ary_tmp['add_container_class'] = $(this).find('.field_add_container_class').val();
        fields_ary_tmp['add_container_size'] = $(this).find('.field_add_container_size').val();
        fields_ary_tmp['mandatory'] = $(this).find('.field_mandatory:checked').length;

        if($(this).find('.field_type').val() == "0") {
            fields_ary_tmp['input_type'] = $(this).find('.input_type').val();
        }

        if($(this).find('.field_type').val() == "0") {
            cur_container = $(this).find('.type_' + $(this).find('.field_type').val() + '_container').find('.input_type_' + $(this).find('.input_type').val());

            if($(this).find('.input_type').val() == "text" || $(this).find('.input_type').val() == "number" || $(this).find('.input_type').val() == "email") {
                fields_ary_tmp['placeholder'] = $(cur_container).find('.field_placeholder').val();
                fields_ary_tmp['default'] = $(cur_container).find('.field_default').val();

                if($(this).find('.input_type').val() == "number") {
                    fields_ary_tmp['min'] = $(cur_container).find('.field_number_min').val();
                    fields_ary_tmp['max'] = $(cur_container).find('.field_number_max').val();
                }
            } else if($(this).find('.input_type').val() == "checkbox") {
                fields_ary_tmp['default_checked'] = $(cur_container).find('.field_checkbox_checked:checked').length;
            } else if($(this).find('.input_type').val() == "hidden") {
                fields_ary_tmp['default'] = $(cur_container).find('.field_default').val();
            } else if($(this).find('.input_type').val() == "radio") {
                ary_cycle_data = new Array();
                $(cur_container).find('.rowDuplication').each(function(k, v) {
                    fields_ary_obj = new Object();
                    fields_ary_obj['value'] = $(this).find('.field_value').val();
                    fields_ary_obj['text'] = $(this).find('.field_text').val();
                    ary_cycle_data.push(fields_ary_obj);
                })

                fields_ary_tmp['data'] = ary_cycle_data;
            }
        } else if($(this).find('.field_type').val() == "1") {
            fields_ary_tmp['placeholder'] = $(this).find('.type_' + $(this).find('.field_type').val() + '_container').find('.field_placeholder').val();
            fields_ary_tmp['default'] = $(this).find('.type_' + $(this).find('.field_type').val() + '_container').find('.field_default').val();
            fields_ary_tmp['rows'] = $(this).find('.type_' + $(this).find('.field_type').val() + '_container').find('.field_rows').val();
        } else if($(this).find('.field_type').val() == "2") {
            ary_cycle_data = new Array();
            $(this).find('.type_' + $(this).find('.field_type').val() + '_container').find('.rowDuplication').each(function(k, v) {
                fields_ary_obj = new Object();
                fields_ary_obj['value'] = $(this).find('.field_value').val();
                fields_ary_obj['text'] = $(this).find('.field_text').val();
                ary_cycle_data.push(fields_ary_obj);
            })

            fields_ary_tmp['data'] = ary_cycle_data;
        }

        fields_ary.push(fields_ary_tmp);
    });

    $('#hidden_fields_container .modal').each(function(row_index) {
        fields_ary_tmp = new Object();

        fields_ary_tmp['unique_id'] = $(this).find('.field_uniqueid').val();
        fields_ary_tmp['name'] = $(this).find('.field_name').val();
        fields_ary_tmp['type'] = 0;
        fields_ary_tmp['field_id'] = $(this).find('.field_id').val();
        fields_ary_tmp['input_type'] = 'hidden';
        fields_ary_tmp['default'] = $(this).find('.field_default').val();

        fields_ary.push(fields_ary_tmp);
    });

    if(show_common_shortcodes) {
        fields_ary.push({
            unique_id: 'site-link450660',
            name: 'Link pagina invio Form',
            type: 0,
            field_id: 'site-link',
            input_type: 'hidden',
            not_save: true,
            default: ''
        });
    }
    return fields_ary;
}

function getFieldBy(what, value, form_ary) {
    if(typeof(form_ary) == 'undefined') {
        form_ary = getFormAry();
    }

    var value_to_return = {};

    form_ary.forEach(function (field) {
       if(field[what] == value) {
           value_to_return = field;
       }
    });

    return value_to_return;

}

function getActionsAry() {
    checked_actions = new Array();
    $('.form_actions_check.active').each(function() {
        checked_actions.push($(this).attr('id'));
    });

    return checked_actions;
}

function getActionsMessagesAry(action_id) {
    var $action_block = $('#action_' + action_id + '_container');

    var action_messages = {};

    if($action_block.find('.manage_messages:checked').length) {

        var template = [];
        if($action_block.find('.manage_template:checked').length) {
            $action_block.find('.message_template span, .message_template strong').each(function () {
                template.push($(this).text());
            });
        }

        $action_block.find('.message_strings').each(function () {

            action_messages[$(this).data('type')] = {
                titolo: $(this).find('.message_field[name="titolo"]').val(),
                message: $(this).find('.message_field[name="message"]').val(),
                template: template
            }
        });
    }
    else {
        action_messages = 0;
    }

    return action_messages;

}

function getActionsValuesAry() {
    actions_values_obj = new Object();

    $('.form_actions_check.active').each(function() {
        container_of = $(this).attr('id');
        container_data = new Object();

        if(container_of == "quote") {
            container_data = getQuoteAry();
        } else if(container_of == "anagrafica") {
            container_data = {
                /* DATI GENERALI */
                "nome" : $('#' + container_of + '_nome').val(),
                "cognome" : $('#' + container_of + '_cognome').val(),
                "email" : $('#' + container_of + '_email').val(),
                "telefono_cellulare" : $('#' + container_of + '_telefono_cellulare').val(),
                "sito_web" : $('#' + container_of + '_sito_web').val(),
                "data_nascita" : $('#' + container_of + '_data_nascita').val(),
                /* DATI INDIRIZZI */
                "indirizzo" : $('#' + container_of + '_indirizzo').val(),
                "stato" : $('#' + container_of + '_stato').val(),
                "regione" : $('#' + container_of + '_regione').val(),
                "citta" : $('#' + container_of + '_citta').val(),
                "provincia" : $('#' + container_of + '_provincia').val(),
                "cap" : $('#' + container_of + '_cap').val(),
                /* DATI AZIENDA */
                "ragione_sociale" : $('#' + container_of + '_ragione_sociale').val(),
                "piva" : $('#' + container_of + '_piva').val(),
                "cf" : $('#' + container_of + '_cf').val(),
                /* DATI INDIRIZZI AZIENDA */
                "indirizzo_azienda" : $('#' + container_of + '_indirizzo_azienda').val(),
                "stato_azienda" : $('#' + container_of + '_stato_azienda').val(),
                "regione_azienda" : $('#' + container_of + '_regione_azienda').val(),
                "citta_azienda" : $('#' + container_of + '_citta_azienda').val(),
                "provincia_azienda" : $('#' + container_of + '_provincia_azienda').val(),
                "cap_azienda" : $('#' + container_of + '_cap_azienda').val(),
                /* DATI ACCESSO */
                "enable_login" : ($('#' + container_of + '_enable_login:checked').length ? '1' : '0'),
                "password" : ($('#' + container_of + '_enable_login:checked').length ? $('#' + container_of + '_password').val() : ''),
                "ripeti_password" : ($('#' + container_of + '_enable_login:checked').length ? $('#' + container_of + '_ripeti_password').val() : ''),
                "auto_login" : ($('#' + container_of + '_auto_login:checked').length ? '1' : '0')
            }
        } else if(container_of == "newsletter_subscribe") {

            var tags = new Array();
            $('#' + container_of + '_tag').find('input:checked').each(function () {
                tags.push($(this).val());
            });

            var additional_tags = new Array();
            $('#' + container_of + '_additional_tag').find('input:checked').each(function () {
                additional_tags.push($(this).val());
            });

            container_data = {
                "name" : $('#' + container_of + '_name').val(),
                "surname" : $('#' + container_of + '_surname').val(),
                "email" : $('#' + container_of + '_email').val(),
                "tag" : tags,
                "additional_tag" : additional_tags,
                "checkbox_allow_subscribe" : $('#' + container_of + '_checkbox_allow_subscribe').val(),
            }

        } else if(container_of == "reservation") {

            var custom_mail = {};
            if( $('#' + container_of + '_default_mail:checked').length && $('#' + container_of + '_use_custom_mail:checked').length ) {
                custom_mail = {
                    custom_html: window.versione_html_personalizzata[container_of + '_versione_html_personalizzata'].getContentHtml(),
                    custom_subject: $('#' + container_of + '_custom_subject').val(),
                }
            }

            var body = new Array();
            $('#' + container_of + '_body').find('input:checked').each(function () {
                body.push($(this).val());
            });

            container_data = {
                "name" : $('#' + container_of + '_name').val(),
                "surname" : $('#' + container_of + '_surname').val(),
                "email" : $('#' + container_of + '_email').val(),
                "date_from" : $('#' + container_of + '_date_from').val(),
                "date_to" : $('#' + container_of + '_date_to').val(),
                "adults" : $('#' + container_of + '_adults').val(),
                "children" : $('#' + container_of + '_children').val(),
                "cell_phone" : $('#' + container_of + '_cell_phone').val(),
                "origin" : $('#' + container_of + '_origin').val(),
                "body" : body,
                "default_mail" : $('#' + container_of + '_default_mail:checked').length,
                "use_custom_mail" : $('#' + container_of + '_use_custom_mail:checked').length,
                "custom_mail": custom_mail
            }
        } else if(container_of == "reply_to_mail") {
            container_data = {
                "name" : $('#' + container_of + '_name').val(),
                "email" : $('#' + container_of + '_email').val(),
                "attachments" : composeOrakImagesToSave('mainImages'),
                "custom_html" : window.versione_html_personalizzata[container_of + '_versione_html_personalizzata'].getContentHtml(),
                "subject" : $('#' + container_of + '_subject').val(),
                "reply_to_addr" : $('#' + container_of + '_reply_to_addr').val(),
                "reply_to_name" : $('#' + container_of + '_reply_to_name').val(),
            }
        } else if(container_of == "generic_request") {

            var additional_info = new Array();
            $('#' + container_of + '_additional_info').find('input:checked').each(function () {
                additional_info.push($(this).val());
            });

            var custom_mail = {};
            if( $('#' + container_of + '_default_mail:checked').length && $('#' + container_of + '_use_custom_mail:checked').length ) {
                custom_mail = {
                    custom_html: window.versione_html_personalizzata[container_of + '_versione_html_personalizzata'].getContentHtml(),
                    custom_subject: $('#' + container_of + '_custom_subject').val(),
                }
            }

            container_data = {
                "name" : $('#' + container_of + '_name').val(),
                "surname" : $('#' + container_of + '_surname').val(),
                "email" : $('#' + container_of + '_email').val(),
                "subject" : $('#' + container_of + '_subject').val(),
                "body" : $('#' + container_of + '_body').val(),
                "additional_info" : additional_info,
                "default_mail" : $('#' + container_of + '_default_mail:checked').length,
                "use_custom_mail" : $('#' + container_of + '_use_custom_mail:checked').length,
                "custom_recipient" : $('#' + container_of + '_custom_recipient').val(),
                "custom_sender_name" : $('#' + container_of + '_custom_sender_name').val(),
                "custom_mail": custom_mail
            }
        } else if(container_of == "property_info") {

            var additional_info = new Array();
            $('#' + container_of + '_additional_info').find('input:checked').each(function () {
                additional_info.push($(this).val());
            });

            var custom_mail = {};
            if( $('#' + container_of + '_default_mail:checked').length && $('#' + container_of + '_use_custom_mail:checked').length ) {
                custom_mail = {
                    custom_html: window.versione_html_personalizzata[container_of + '_versione_html_personalizzata'].getContentHtml(),
                    custom_subject: $('#' + container_of + '_custom_subject').val(),
                }
            }

            container_data = {
                "name" : $('#' + container_of + '_name').val(),
                "surname" : $('#' + container_of + '_surname').val(),
                "email" : $('#' + container_of + '_email').val(),
                "cell_phone" : $('#' + container_of + '_cell_phone').val(),
                "subject" : $('#' + container_of + '_subject').val(),
                "body" : $('#' + container_of + '_body').val(),
                "additional_info" : additional_info,
                "default_mail" : $('#' + container_of + '_default_mail:checked').length,
                "use_custom_mail" : $('#' + container_of + '_use_custom_mail:checked').length,
                "custom_mail": custom_mail
            }
        } else if(container_of == "login") {
            container_data = {
                "email" : $('#' + container_of + '_email').val(),
                "password" : $('#' + container_of + '_password').val(),
                "rimani_connesso" : $('#' + container_of + '_rimani_connesso').val(),
            }
        } else if(container_of == "password_recovery") {
            container_data = {
                "email" : $('#' + container_of + '_email').val()
            }
        }

        container_data['messages'] = getActionsMessagesAry(container_of);

        actions_values_obj[container_of] = container_data;
    })

    return actions_values_obj;
}

function getEventsValueAry() {

    var event_values_ary = {};

    $('#eventsList .eventBox').each(function () {
       var $eventBlock = $(this);

       if($eventBlock.find('.eventSettings_enabled:checked').length) {
           var js_source = hook_editor[$eventBlock.data('ace_id')].getValue();
           event_values_ary[$eventBlock.data('key')] = {
               js: js_source
           }
       }

    });

    return event_values_ary;
}

function moduleSaveFunction(ignore_warning) {

    if(typeof(ignore_warning) == 'undefined') ignore_warning = false;

    if(!ignore_warning) {

        var active_return_messages = 0;
        var hook_syntax_errors = [];

        var error_messages = [];


        $('.manage_messages:checked').each(function () {
            if($(this).closest('.action_container').css('display') == 'block') {
                active_return_messages++;
            }
        });

        if(active_return_messages == 0) {
            error_messages.push("Non hai impostato nessun messaggio di successo o di errore da mostrare dopo l'invio del form, continuando l'utente visualizzerà dei messaggi predefiniti.<hr>Desideri salvare comunque?");
        }


        hook_editor.forEach(function(s_editor, key) {
            s_editor.getSession().getAnnotations().forEach(function (error) {
                hook_syntax_errors.push("<b>" + $('.eventBox').eq(key).find('h5 span').html() + "</b><br>Riga: " + error.row + ", Colonna: " + error.column + "<br>Errore: <i>" + error.text + "</i>");
            });
        });

        if(hook_syntax_errors.length) {
            error_messages.push("Sembra ci siano degli errori di Sintassi nei seguenti Hook (Eventi).<hr><div class='text-left'>" + hook_syntax_errors.join('<hr>') + '</div><hr>Desideri salvare comunque?');
        }

        if(error_messages.length) {
            bootbox.confirm({
                message: error_messages.join('<hr>'),
                buttons: {
                    confirm: {
                        label: 'Si, salva le modifiche',
                        className: 'btn-success',
                    },
                    cancel: {
                        label: 'No, annulla',
                        className: 'btn-danger',
                    }
                },
                callback: function (result) {
                    if (result) {
                        moduleSaveFunction(true);
                    }
                }
            });

            return true;
        }

    }

    var hidden_errors = false;
    $('#hidden_fields_container').find('.required').each(function () {
        if(!$(this).val().length) {
            $('[data-target="#' + $(this).closest('.modal').attr('id') + '"]').addClass('required error').on('click', function () {
                $(this).removeClass('required error');
            });
            hidden_errors = true;
        }
    });

    if(hidden_errors) {
        $('#editSaveBtn').click();
        return false;
    }

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pIsActive": $('#is_active:checked').length,
            "pSubmitBtnTXT": $('#submit_btn_text').val(),
            "pEnableCaptcha": $('#enable_captcha:checked').length,
            "pFields": getFormAry(),
            "pQuote": getQuoteAry(),
            "pActions": getActionsAry(),
            "pActionsValues": getActionsValuesAry(),
            "pEvents": getEventsValueAry()
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "duplicate_field_id") {
                bootbox.error("I valori dei campi 'ID Campo' devono essere univoci.");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}

function shortCodesModal(container_of, force_show) {

    if(typeof(force_show) == 'undefined') {
        force_show = false;
    }

    shortcodes_avail = new Object();

    if(container_of == "reply_to_mail" || container_of == "generic_request" || container_of == "reservation") {
        getFormAry(false, true).forEach(function (val) {
            shortcodes_avail[val.field_id] = val.name;
        });
    }

    if(Object.keys(shortcodes_avail).length == 0) {
        bootbox.error('Impossibile determinare gli shortcodes per questa sezione');
        return false;
    }

    str = "";
    for(short in shortcodes_avail) {
        str += "<b>{" + short + "}</b> - " + shortcodes_avail[short] + "<br>";
    }

    var $container_block = $('#action_' + container_of + '_container');
    $container_block.find('.fields_shortcodes').html(str);

    if(force_show) $container_block.find('.shortcodes_alert').show();
    else $container_block.find('.shortcodes_alert').toggle();

}

/*
* FUNZIONI PER GENERAZIONE PREVENTIVO
*/
function composeQuoteRows() {
    $.ajax({
        url: "ajax/quoteRowsHTML.php",
        type: "POST",
        data: {
            "pCurFormData": getFormAry(true),
            "pCurQuoteData": getQuoteAry(),
        },
        async: false,
        dataType: "html",
        success: function(data) {
            $('#quote_rows_container').html(data);

            //initIChecks();
            initMSTooltip();

            initActionsDuplication(false);
            initRulesDuplication(false);
            initSubRulesDuplication(false);

            hideConcats();
        }
    })
}

function initActionsDuplication(last) {
    selector = "";
    if(last) {
        selector = ':last';
    }

    $('.actionDuplication' + selector).easyRowDuplication({
        "addButtonClasses": "btn btn-info",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi azione",
        "deleteButtonText": "Elimina azione",
        "afterAddCallback": function() {
            $('.actionDuplication:last').find('.quoteDuplication').not(':first').remove();
            $('.actionDuplication:last').find('.quoteDuplication:last').find('> .customDelete').remove();
            $('.actionDuplication:last').find('.quoteDuplication:last').find('.subconditionDuplication').not(':first').remove();
            $('.actionDuplication:last').find('.quoteDuplication:last').find('.group_concat').hide();
            $('.actionDuplication:last').find('.quoteDuplication:last').find('.subconditionDuplication').find('.concat').hide();

            $('.actionDuplication:last').find('.quoteDuplication:last').find('.subconditionDuplication:last').find('.condition_field').val('');
            $('.actionDuplication:last').find('.quoteDuplication:last').find('.subconditionDuplication:last').find('.condition_operator').val('equal');
            $('.actionDuplication:last').find('.quoteDuplication:last').find('.subconditionDuplication:last').find('.condition_value').val('');

            $('.actionDuplication:last').find('.condition_action_type').val($('.actionDuplication:last').find('.condition_action_type').find('option:first').attr('value'));
            $('.actionDuplication:last').find('.condition_action_value').val('');

            initRulesDuplication(true);
        },
    });

}

function initRulesDuplication(last) {
    selector = "";
    if(last) {
        selector = ':last';
    }

    $('.quoteDuplication' + selector).easyRowDuplication({
        "addButtonClasses": "btn btn-info",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi condizione",
        "deleteButtonText": "Elimina condizione",
        "afterAddCallback": function(dup_element) {
            $('.quoteDuplication').find('.group_concat').show();

            $(dup_element).closest('.actionDuplication').find('.subconditionDuplication:last').find('.condition_field').val('');
            $(dup_element).closest('.actionDuplication').find('.subconditionDuplication:last').find('.condition_operator').val('equal');
            $(dup_element).closest('.actionDuplication').find('.subconditionDuplication:last').find('.condition_value').val('');

            $(dup_element).closest('.actionDuplication').find('.quoteDuplication:last').find('.subconditionDuplication').not(":first").remove();

            $('.actionDuplication').each(function() {
                $(this).find('.quoteDuplication:last').find('.group_concat').hide();
                $(this).find('.quoteDuplication:last').find('.subconditionDuplication').find('.concat').hide();

                initSubRulesDuplication(true, this);
            })
        },
        "afterDeleteCallback": function() {
            hideConcats();
        },
    });

    if(last) {
        initSubRulesDuplication(true);
    }
}

function initSubRulesDuplication(last, parent_group) {
    //questa funzione gestisce la duplicazione delle sotto-condizioni del preventivo
    selector = "";
    if(last) {
        selector = ':last';
    }

    if(typeof(parent_group) != "undefined") {
        selector_to_use = $(parent_group).find('.subconditionDuplication' + selector);
    } else {
        selector_to_use = $('.subconditionDuplication' + selector);
    }

    selector_to_use.easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-danger",
        "addButtonText": "Aggiungi sotto-condizione",
        "deleteButtonText": "Elimina sotto-condizione",
        "afterAddCallback": function(dup_element) {
            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication').find('.concat').show();
            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication').find('.concat').attr('disabled', false);

            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication:last').find('.concat').hide();
            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication').find('.concat').not(':first').attr('disabled', 'disabled');
            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication').find('.concat').not(':first').val($(dup_element).closest('.quoteDuplication').find('.subconditionDuplication').find('.concat:first').val());

            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication').find('.concat:first').unbind('change');
            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication').find('.concat:first').on('change', function() {
                $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication').find('.concat').not(':first').val($(dup_element).closest('.quoteDuplication').find('.subconditionDuplication').find('.concat:first').val());
            })

            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication:last').find('.condition_field').val('');
            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication:last').find('.condition_operator').val('equal');
            $(dup_element).closest('.quoteDuplication').find('.subconditionDuplication:last').find('.condition_value').val('');

            manageConditionOperatorChange();
        },
        "afterDeleteCallback": function() {
            hideConcats();
        },
    });

    manageConditionOperatorChange();
}

function getQuoteAry() {
    quoteAry = new Object();

    quoteAry['base_price'] = $('#base_price').val();
    quoteAry['force_manually'] = $('#force_manually:checked').length;
    quoteAry['conditions'] = new Array();

    $('.actionDuplication').each(function(action_index) {
        actionsAry = new Object();

        $(this).find('.quoteDuplication').each(function(index) {
            groupConditions = new Object();
            this_parent = this;

                $(this).find('.subconditionDuplication').each(function(sub_index) {
                curConditions = new Object();
                curConditions['field'] = $(this).find('.condition_field').val();
                curConditions['operator'] = $(this).find('.condition_operator').val();

                if($(this).find('.condition_operator').val() != "is_checked" && $(this).find('.condition_operator').val() != "is_not_checked") {
                    curConditions['value'] = $(this).find('.condition_value').val();
                }

                curConditions['concat'] = $(this).find('.concat').val(); //salva la concatenazione tra un sotto-gruppo ed un altro

                groupConditions[sub_index] = (curConditions);
            })

            groupConditions['concat'] = $(this).find('.group_concat').val(); //salva la concatenazione tra due gruppi principali
            actionsAry[index] = groupConditions;
            last_index = index;
        })

        actionsAry['action'] = {
            "ref" : $(this).find('.condition_ref').val(),
            "type" : $(this).find('.condition_action_type').val(),
            "value" : $(this).find('.condition_action_value').val()
        };

        var json_lang = $(this).find('.condition_ref_multilang').data('translations');

        try {
            JSON.stringify(json_lang);
        }
        catch (error) {
            json_lang = {};
        }

        json_lang[$("#currentLangCode").val()] = actionsAry['action']['ref'];
        actionsAry['action']['ref'] = JSON.stringify(json_lang);

        quoteAry['conditions'].push(actionsAry);

    });

    return quoteAry;
}

function manageConditionOperatorChange() {
    $('.condition_operator').unbind('change');
    $('.condition_operator').on('change', function() {
        index = $(".condition_operator").index($(this));

        if($(this).val() == "is_checked" || $(this).val() == "is_not_checked") {
            $('.subconditionDuplication:eq(' + index + ') .condition_value_container').hide();
        } else {
            $('.subconditionDuplication:eq(' + index + ') .condition_value_container').show();
        }
    });
}

function checkQuoteTab() {
    if($('#quote.active').length == 1) {
        composeQuoteRows();
        $('#form ul li:eq(2)').show();
    } else {
        $('#form ul li:eq(2)').hide();
    }
}

function hideConcats() {
    $('.quoteDuplication').find('.group_concat').show();
    $('.quoteDuplication:last').find('.group_concat').hide();

    $('.quoteDuplication').each(function() {
        $(this).find('.subconditionDuplication:last').find('.concat').hide();
    })

    $('.actionDuplication').each(function() {
        $(this).find('.quoteDuplication:last').find('.group_concat:last').hide();
    })
}

/*
* FUNZIONI MISC
*/

function onFormActionShow(id) {

    if($('#action_' + id + '_container').find('.shortcode_replace').length) {
        replaceDefaultTemplateShortcodes(id);
    }

    if($('#action_' + id + '_container').find('.shortcodes_alert').length && $('#action_' + id + '_container').find('.shortcodes_alert').css('display') == 'block') {
        shortCodesModal(id, true);
    }
}

function onFormActionHide(id) {
    if(id == 'reply_to_mail') {

    }
}

function replaceDefaultTemplateShortcodes(id) {

    var shortcodes_match = $('#action_' + id + '_container').find('.shortcode_replace').data('shortcodes_match');
    //var new_shortcodes_match =  {};

    var html_template = $('#action_' + id + '_container').find('.editor_template_html .email-editor-elements-sortable').html();
    var subject_template = $('#action_' + id + '_container').find('.editor_template_subject').val();

    Object.keys(shortcodes_match).forEach(function (shortcode_key) {
        var shortcode_value = $('#' + id + '_' + shortcodes_match[shortcode_key]).val();
        var regex =  new RegExp('{(' + shortcode_key + ')}', "gi");

       // new_shortcodes_match[shortcode_value] = shortcodes_match[shortcode_key];
        if(shortcode_value.length) {
            html_template = html_template.replace(regex, '{' + shortcode_value + '}');
            subject_template = subject_template.replace(regex, '{' + shortcode_value + '}');
        }

    });

    $('#action_' + id + '_container').find('.editor_template_html .email-editor-elements-sortable').html(html_template);
    $('#action_' + id + '_container').find('.editor_template_subject').val(subject_template);

    //$('#action_' + id + '_container').find('.shortcode_replace').data('shortcodes_match', new_shortcodes_match);
    window.versione_html_personalizzata[id + '_versione_html_personalizzata'].composeShortcodesView();

}

function onStepChangedModule(form_obj, event, currentIndex, priorIndex) {

    $('.shortcodes_alert:visible').each(function () {
        shortCodesModal($(this).closest('.action_container').data('container-of'), true);
    });

    if($('.ace_editor:visible').length) {
        $('.ace_editor:visible').css('height', '300px');
        hook_editor.forEach(function (s_editor) {
            s_editor.resize();
        });
    }
}

function getEditorShortcodes() {

    var custom_shortcodes = [];
    getFormAry(false, true).forEach(function (val) {
        custom_shortcodes.push('{' + val.field_id + '}');
    });

    return {global: window.global_shortcodes, custom: custom_shortcodes};
}
<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$MSFrameworkPages = new \MSFramework\pages();

$page_types = $MSFrameworkPages->getTypes();
$current_page_type = array_values($page_types)[0];

$short_codes_list = array();

$sel_db = "";
$is_global_page = false;
if(strstr($_GET['id'], "global-")) {
    $sel_db = "`" . FRAMEWORK_DB_NAME . "`.";
    $is_global_page = true;
}

$is_private_page = false;
if(strstr($_GET['id'], "static-")) {
    $is_private_page = true;
}

$r = array();
if(isset($_GET['id'])) {
    if($is_private_page) {
        $r = $MSFrameworkPages->getPrivatePageData(str_replace('static-', '', $_GET['id']));

        if($r['details']['shortcodes']) {
            foreach($r['details']['shortcodes'] as $sk => $sv) {
                $short_codes_list[$sk] = $sv['description'];
            }
        }

    } else {
        $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM " . $sel_db . "pagine WHERE id = :id", array(":id" => str_replace("global-", "", $_GET['id'])), true);
    }

    if(!$r) {
        $is_private_page = false;
        $r = array();
    }

    $r['seo'] = (json_decode($r['seo']) ? json_decode($r['seo'], true) : array());
}

if($r) {
    $current_page_type = $page_types[$r['type']];
    if ($r['hidden'] == 1 && $_SESSION['userData']['userlevel'] != "0") {
        unset($r);
        $_GET['id'] = "";
    }
} else if(isset($_GET['relation'])) {
    $passed_page_key = '';
    $passed_page_type = array();
    $passed_page_relation = array();
    foreach($page_types as $pk => $pv) {
        if($pv['related_to'] == $_GET['relation']) {
            $passed_page_key = $pk;
            $current_page_type = $pv;
            $passed_page_relation = array('id' => $_GET['relation_id']);
        }
    }
}

function recursiveHierarchySelect($page, $selected, $level = 0, $parent_slug = '', $parent_divider = '/', $disabled = false) {
    Global $MSFrameworki18n, $r;

    $options = array();

    if($r && $r['id'] == $page['id']) {
        $disabled = true;
    }

    $prepend = '';
    for($i = 0; $i < $level; $i++) {
        $prepend .= '-';
    }

    if($level == 0) $options[] = '<option style="margin: 3px 5px; height: 1px; border-top: 1px dashed rgb(236, 236, 236); padding: 0; color: transparent;" value="" disabled>.</option>';

    $current_divider =  (!empty($page['slug_divider']) ? $page['slug_divider'] : $parent_divider) ;

    $options[] = '<option value="' . $page['id'] . '" style="' . ($level == 0 ? 'font-weight: 500;' : '') . '" data-slug="' . $parent_slug . ($parent_slug != '' ? $current_divider : '') .  $MSFrameworki18n->getFieldValue($page['slug']) . '" data-divider="' .  $current_divider  . '" ' . ($selected == $page['id'] ? 'selected' : '') . ' ' . ($disabled ? 'disabled' : '') . '>' . ($prepend ? $prepend . ' ' : '') . $MSFrameworki18n->getFieldValue($page['titolo']) . '</option>';
    if($page['childrens']) {
        foreach($page['childrens'] as $children) {
            $options[] = recursiveHierarchySelect($children, $selected, $level+1, $MSFrameworki18n->getFieldValue($page['slug']), $current_divider, $disabled);
        }
    }

    return implode('', $options);

}
?>

<body class="skin-1 fixed-sidebar pace-done">

<style>
    .CodeMirror-sizer {
        min-height: 250px !important;
    }
</style>

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />
            <input type="hidden" id="global_id" value="<?php echo $r['global_id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Pagina</h1>
                        <fieldset>
                            <div class="row form-with-sidebar">

                                <div class="col-xs-12 col-sm-12 col-lg-3 col-lg-push-9 right-options">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Tipologia pagina*</label>
                                            <select id="page_type" name="page_type" class="form-control required" <?php echo ($r['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?> data-old="<?= $r['type']; ?>">
                                                <?php foreach($page_types as $typeK => $typeV) { ?>
                                                    <option value="<?php echo $typeK ?>" <?php if($r['type'] == $typeK || $passed_page_key == $typeK) { echo "selected"; } ?>><?php echo $typeV['nome'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <?php if(!$is_private_page) { ?>
                                        <div class="col-sm-12 parent_container">
                                            <label>Pagina Genitore</label>
                                            <select id="pagina_genitore" name="pagina_genitore" class="form-control chosen-select" data-placeholder="Nessuna pagina genitore...">
                                                <option value="" style="color: #9c9393;">Nessuna</option>
                                                <?php foreach($MSFrameworkPages->getPagesHierarchy() as $page) {
                                                    echo recursiveHierarchySelect($page, ($r['parent'] ? $r['parent'] : (isset($_GET['parent']) && is_numeric($_GET['parent']) ? $_GET['parent'] : '')), 0);
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-12 slug_divider" data-optional data-default-view="0">
                                            <label>Divisore Slug</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Nelle pagine che contengono pagine figlie, permette di scegliere se far dividere l'URL della pagina da uno slash (/) o da un trattino (-)."></i></span>
                                            <select id="divisore_slug" name="divisore_slug" class="form-control">
                                                <option value="" class="eredita" style="display: <?= ($r['parent'] && $r['parent'] > 0 ? 'block' : 'none'); ?>;">Eredita da Genitore</option>
                                                <option value="/" <?= ($r['slug_divider'] == '/' || ($r['slug_divider'] != '-' && (!$r['parent'] || (int)$r['parent'] == 0)) ? 'selected' : ''); ?>>Usa Slash (/)</option>
                                                <option value="-" <?= ($r['slug_divider'] == '-' ? 'selected' : ''); ?>>Usa Trattino (-)</option>
                                            </select>
                                        </div>
                                        <?php } ?>

                                        <div class="col-sm-12 slug_divider" data-optional data-default-view="0">
                                            <label>CSS Personalizzato</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Permette di inserire del css personalizzato nella pagina"></i></span>
                                            <textarea id="custom_page_css" class="form-control"><?= $r['additional_css']; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-lg-9 col-lg-pull-3">
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <label>Titolo Pagina*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['titolo']) ?>"></i></span>
                                            <input id="titolo" name="titolo" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['titolo'])) ?>" <?php echo ($r['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Sottotitolo pagina</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['sottotitolo']) ?>"></i></span>
                                            <input id="sottotitolo" name="sottotitolo" type="text" class="form-control" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['sottotitolo'])) ?>" <?php echo ($r['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>>
                                        </div>

                                        <?php if(!$is_private_page || isset($r['details']['slug'])) { ?>
                                            <div class="col-sm-12">
                                                <label>Slug*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['slug']) ?>"></i></span>
                                                <input id="slug" name="slug" type="text" class="form-control required serpUpdate" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['slug'])) ?>" <?php echo ($r['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="col-attached-left">

                                        <div id="content_editor_body" style="display: <?= (isset($current_page_type['hide_content']) && $current_page_type['hide_content'] ? 'none' : 'block'); ?>;">

                                            <h2 class="title-divider">
                                                Contenuto
                                            </h2>

                                            <?php if($short_codes_list) {
                                                echo (new \MSFramework\utils())->generateShortcodesTable($short_codes_list, true);
                                            } ?>

                                            <?php if($is_private_page) { ?>
                                                <textarea id="privatePageBlocks" style="display: none"><?php echo json_encode($r['details']['blocks']) ?></textarea>
                                                <textarea id="privatePageShortcodes" style="display: none"><?php echo json_encode($short_codes_list) ?></textarea>
                                            <?php } else { ?>
                                                <div class="row">
                                                    <div class="col-sm-12" id="short_content_container" style="display: <?= ($current_page_type['use_short_content'] ? 'block' : 'none'); ?>;">
                                                        <label>Contenuto Breve</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['short_content']) ?>"></i></span>
                                                        <textarea id="pageShortContentText" name="pageShortContentText"><?php echo $MSFrameworki18n->getFieldValue($r['short_content']) ?></textarea>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>Corpo Pagina</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['content']) ?>"></i></span>
                                                    <textarea id="pageContentText" name="pageContentText"><?php echo $MSFrameworki18n->getFieldValue($r['content']) ?></textarea>
                                                </div>
                                            </div>

                                            <textarea id="pageTypes" style="display: none"><?= json_encode($page_types); ?></textarea>

                                        </div>


                                        <div id="info_aggiuntive">
                                            <?php include('ajax/getExtraFields.php'); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1>SEO</h1>
                        <fieldset>
                            <?php if(!$r['details']['have_seo']) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>TAG Title</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['seo']['title']) ?>"></i></span>
                                    <input id="seo_titolo" name="seo_titolo" type="text" class="form-control serpUpdate" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['seo']['title'])) ?>" <?php echo ($r['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>TAG Description</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['seo']['description']) ?>"></i></span>
                                    <textarea id="seo_description" name="seo_description" type="text" class="form-control serpUpdate" <?php echo ($r['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>><?php echo htmlentities($MSFrameworki18n->getFieldValue($r['seo']['description'])) ?></textarea>
                                </div>
                            </div>

                            <div class="ibox">
                                <div class="ibox-title" style="background: #fefefe;">
                                    <i class="ibox-icon fa fa-google fa-3x"></i>
                                    <h5>
                                        <span class="title">Anteprima</span>
                                    </h5>
                                </div>
                                <div class="ibox-content">


                                    <div id="serpPreview"></div>


                                </div>
                            </div>
                            <?php } else { ?>
                                <div class="alert alert-info" style="margin: 0;">
                                    Le impostazioni SEO di questa pagina vengono ereditate dai dati impostati nel contenuto originale.
                                </div>
                            <?php } ?>
                        </fieldset>

                        <?php if(!$is_private_page) { ?>
                            <h1>Gallery</h1>
                            <fieldset>
                                <?php
                                $gallery_settings = json_decode($r['gallery'], true);
                                ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Tipologia gallery</label>
                                        <select class="form-control" id="gallery_type">
                                            <option value="" <?php if($gallery_settings['type'] == "") { echo "selected"; } ?>>Nessuna gallery</option>
                                            <option value="0" <?php if($gallery_settings['type'] == "0") { echo "selected"; } ?>>Importa da gallery esistente</option>
                                            <option value="1" <?php if($gallery_settings['type'] == "1") { echo "selected"; } ?>>Imposta gallery personalizzata</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row group_gallery" id="group_gallery_0" style="display: none;">
                                    <div class="col-sm-6">
                                        <label>Seleziona gallery*</label>
                                        <select class="form-control" id="import_gallery">
                                            <option value=""></option>
                                            <?php
                                            foreach((new \MSFramework\gallery())->getGalleryDetails() as $gallery_details) {
                                                ?>
                                                <option value="<?= $gallery_details['id'] ?>" <?php if($gallery_settings['type'] == "0" && $gallery_settings['gallery'] == $gallery_details['id']) { echo "selected"; } ?>><?= $MSFrameworki18n->getFieldValue($gallery_details['titolo'], true) ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row group_gallery" id="group_gallery_1" style="display: none;">
                                    <div class="col-sm-12">
                                        <h2 class="title-divider">Immagini</h2>
                                        <?php (new \MSFramework\uploads('PAGEGALLERY'))->initUploaderHTML("images", ($gallery_settings['type'] == "1") ? json_encode($gallery_settings['gallery']) : ''); ?>
                                    </div>
                                </div>

                            </fieldset>

                            <?php if($_SESSION['userData']['userlevel'] == "0") { ?>
                                <h1>Configurazioni Produttore</h1>
                                <fieldset>
                                    <div class="row">
                                        <?php
                                        $is_protected_check = "";
                                        if($r['protected'] == 1) {
                                            $is_protected_check = "checked";
                                        }
                                        ?>
                                        <div class="col-sm-3">
                                            <label>&nbsp;</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Proteggendo la pagina, questa sarà eliminabile solo da un SuperAdmin. Anche il titolo e lo slug saranno modificabili solo da un SuperAdmin."></i></span>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="is_protected" <?php echo $is_protected_check ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Proteggi Pagina</span>
                                                </label>
                                            </div>
                                        </div>

                                        <?php
                                        $is_hidden_check = "";
                                        if($r['hidden'] == 1) {
                                            $is_hidden_check = "checked";
                                        }
                                        ?>
                                        <div class="col-sm-3">
                                            <label>&nbsp;</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Nascondendo la pagina, questa sarà visibile nel backoffice solo da un SuperAdmin."></i></span>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="is_hidden" <?php echo $is_hidden_check ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Nascondi Pagina</span>
                                                </label>
                                            </div>
                                        </div>

                                        <?php
                                        $is_hide_header_footer_check = "";
                                        if($r['hide_header_footer'] == 1) {
                                            $is_hide_header_footer_check = "checked";
                                        }
                                        ?>
                                        <div class="col-sm-3">
                                            <label>&nbsp;</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Abilitando questa impostazione, l'header ed il footer standard per le pagine sarà nascosto."></i></span>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="is_hide_header_footer" <?php echo $is_hide_header_footer_check ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Nascondi Header/Footer</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            <?php } ?>
                        <?php } ?>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<style>
    .slug_settings_container .slug_divider {
        /* display: none; */
    }

    .slug_settings_container.show_settings .slug_divider,
    .slug_settings_container.show_settings .parent_container {
        display: block;
        width: 50% !important;
    }

</style>

<script>
    globalInitForm();
</script>
</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$where_hidden = "";
if($_SESSION['userData']['userlevel'] != "0") {
    $where_hidden =  " AND hidden = 0 ";
}

$parent_id = '';
if(isset($_GET['parent'])) {
    $parent_id = $_GET['parent'];
}

$array = array("data" => array());

$page_types = $MSFrameworkPages->getTypes();

$site_ids = array();
foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM pagine WHERE id != '' AND parent = :parent_id $where_hidden", array(':parent_id' => $parent_id)) as $r) {
    if($r['global_id'] != "") {
        $site_ids[] = $r['global_id'];
    }

    $title = $MSFrameworki18n->getFieldValue($r['titolo'], true);

    $children_count = $MSFrameworkDatabase->getCount("SELECT * FROM pagine WHERE id != '' AND parent = " . $r['id'] . " $where_hidden");

    if($children_count) {
        $title .= '<a href="#" class="children_pages" style="float: right;" data-id="' . $r['id'] . '" data-name="' . htmlentities($title) . '">Mostra pagine figlie (' . $children_count . ')</a>';
    }

    $arrayTMP['data'][] = array(
        $title,
        $MSFrameworki18n->getFieldValue($r['slug'], true),
        $page_types[$r['type']]['nome'] . ($r['global_id'] != "" ? " (sovrascrive la globale)" : ""),
        array('display' => (new \MSFramework\utils())->smartdate($r['last_update']), 'sort' => $r['last_update']),
        "DT_RowId" => $r['id']
    );
}

$same_db_string_ary = $MSFrameworkDatabase->composeSameDBFieldString($site_ids, "AND", "global_id", "!=");
if (!is_array($same_db_string_ary[1])) {
    $same_db_string_ary = array(" id != '' ", array());
}

if($parent_id == '') {
    foreach ($MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`pagine` WHERE id != '' AND (" . $same_db_string_ary[0] . ") $where_hidden", $same_db_string_ary[1]) as $r) {
        $array['data'][] = array(
            $MSFrameworki18n->getFieldValue($r['titolo'], true),
            $MSFrameworki18n->getFieldValue($r['slug'], true),
            $page_types[$r['type']]['nome'] . " (globale)",
            array('display' => (new \MSFramework\utils())->smartdate($r['last_update']), 'sort' => $r['last_update']),
            "DT_RowId" => 'global-' . $r['id'],
            "DT_buttonsLimit" => 'edit'
        );
    }

    if($MSFrameworkCMS->getCMSData('theme_info')) {
        $groups_info = (new \MSFramework\pages())->privatePages;
        foreach ($groups_info as $group_id => $page_group) {
            if (!$page_group['limit_to_extra_function'] || $MSFrameworkCMS->checkExtraFunctionsStatus($page_group['limit_to_extra_function'])) {
                foreach ($page_group['pages'] as $pages_id => $page_info) {

                    $pageData = $MSFrameworkPages->getPrivatePageData($pages_id);

                    $array['data'][] = array(
                        ($pageData['titolo'] ? '<span style="line-height: 1;">' . $MSFrameworki18n->getFieldValue($pageData['titolo']) . '<small class="text-muted" style="display: block;">' . $page_info['title'] . '</small></span>' : $page_info['title']),
                        ($pageData['details']['slug'] ? $MSFrameworki18n->getFieldValue($pageData['slug']) : '<span class="text-muted">Slug dinamico</span>'),
                        '<span class="label" style="text-align: center; background: ' . $page_group['background'] . '; color: ' . $page_group['color'] . ';"><i class="fa ' . $page_group['icon'] . '"></i> ' . $page_group['title'] . '</span>',
                        ($pageData['last_update'] ? array('display' => (new \MSFramework\utils())->smartdate(strtotime($pageData['last_update'])), 'sort' => strtotime($pageData['last_update'])) : array('display' => 'Mai', 'sort' => 0)),
                        "DT_RowId" => 'static-' . $pages_id,
                        "DT_buttonsLimit" => 'edit'
                    );
                }
            }
        }
    }

}

if(is_array($arrayTMP['data'])) {
    $array['data'] = array_merge($array['data'], $arrayTMP['data']);
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

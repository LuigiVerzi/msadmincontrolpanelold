<?php
/**
 * MSAdminControlPanel
 * Date: 16/03/2019
 */

$page_data = array();
$info_aggiuntive = array();

// Nel caso in cui il file è stato incluso direttamente prendo eredito i dati dalla pagina edit.php altrimenti li ottengo tramite i parametri passati
if(isset($r)) {
    $page_data = $r;
    if($page_data) {
        $current_page_type = $page_types[$page_data['type']];
    } else if(isset($passed_page_key)) {
        $info_aggiuntive['page_relation'] = $passed_page_relation;
    }

} else {
    require_once('../../../../sw-config.php');
    require('../config/moduleConfig.php');

    $page_types = $MSFrameworkPages->getTypes();
    $current_page_type = array_values($page_types)[0];

    $page_id = $_POST['page_id'];
    $page_type = $_POST['page_type'];
    $current_page_type = $page_types[$page_type];

    if(is_numeric($page_id)) {
        $page_data = $MSFrameworkDatabase->getAssoc("SELECT info_aggiuntive FROM pagine WHERE id = :id", array(":id" => $page_id), true);
    }
}

if($page_data) {
    $info_aggiuntive = json_decode($page_data['info_aggiuntive'], true);
}

?>

<?php if($current_page_type) {?>


    <?php
    if($current_page_type['related_to']) {
        $related_to_info = $MSFrameworkPages->getTypesRelations($current_page_type['related_to']);
        $current_page_relation = (isset($info_aggiuntive['page_relation']) ? $info_aggiuntive['page_relation']['id'] : '');

        echo '<h2 class="title-divider">Preferenze ' . $related_to_info['nome'] . '</h2>';
        echo ' <div class="extra_settings" data-id="page_relation">';

        echo '<div class="row">';

        echo '<div class="col-sm-9"><label>' . $related_to_info['label'] . '</label>';
        echo '<select class="extra_field_input chosen-select form-control ' . (isset($passed_page_key) ? 'to_refresh ' : ''). ($page_data && $page_data['parent'] > 0 ? '' : 'required') . '" id="page_relation_value" name="id" ' . ($page_data && $page_data['parent'] > 0 ? '' : 'required') . '>';

        echo '<option value="" data-original="Seleziona un ' . $related_to_info['nome'] . '">' . ($page_data && $page_data['parent'] > 0 ? 'Eredita da Genitore' : 'Seleziona un ' . $related_to_info['nome']) . '</option>';

        echo '<optgroup label="Lista ' . $related_to_info['nome'] . '">';
        foreach($related_to_info['get']() as $relation_id => $relation_value) {
            echo '<option value="' . $relation_id . '" ' . ($relation_id == $current_page_relation ? ' selected' : '') . '>' . $MSFrameworki18n->getFieldValue($relation_value['nome']) . '</option>';
        }
        echo '</optgroup>';

        echo '</select>';
        echo '</div>';

        echo '<div class="col-sm-3"><label></label>';
        echo '<a style="display: ' . ($current_page_relation ? 'block' : 'none') . ';" href="#" target="_blank" data-target="' . $related_to_info['module_id'] . '" class="btn btn-default" id="edit_relative_element">Modifica ' . $related_to_info['nome'] . '</a>';
        echo '</div>';

        echo '</div>';
    }
    ?>

    <?php if(isset($current_page_type['extra_fields'])) { ?>
        <?php foreach($current_page_type['extra_fields'] as $extra_info_id => $extra_info) { ?>
            <h2 class="title-divider"><?= $extra_info['title']; ?></h2>

            <div class="extra_settings" data-id="<?= $extra_info_id; ?>">

                <?php
                if(isset($extra_info['attr']) && in_array('duplicate', $extra_info['attr'])) {
                    $current_fields_value = $info_aggiuntive[$extra_info_id];
                    echo '<div class="easyRowDuplicationContainer">';
                }
                else {
                    $current_fields_value = array($info_aggiuntive[$extra_info_id]);
                }

                if(!$info_aggiuntive[$extra_info_id]) {
                    $current_fields_value = array(array());
                }

                for($extra_field_index = 0; $extra_field_index < count($current_fields_value); $extra_field_index++) {

                    $row_fields = $current_fields_value[$extra_field_index];

                    if (isset($extra_info['attr']) && in_array('duplicate', $extra_info['attr'])) {
                        echo '<div class="stepsDuplication rowDuplication fieldsDuplication" style="padding: 15px; background: #fafafa; border: 1px solid #e5e6e7; margin-bottom: 15px;">';
                    }
                    ?>

                    <div class="row">
                        <?php foreach ($extra_info['fields'] as $field_id => $field) { ?>

                            <?php
                            // Aggiungo il prefisso del genitore per assicurarmi che l'ID sia unico
                            $unique_id = $extra_info_id . '_' . $field_id;

                            // Aggiungo l'Index della riga per evitare duplicati
                            if (in_array('duplicate', $extra_info['attr'])) {
                                $unique_id .= '_' . $extra_field_index;
                            }
                            ?>

                            <div class="col-sm-<?= (isset($field['columns']) ? $field['columns'] : intval((12 / count($extra_info['fields'])))); ?>">

                                <?php

                                $current_field_value = $row_fields[$field_id];

                                $translation_circle = "";
                                if ($field['multilang']) {
                                    $translation_circle = '<span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . TRANSLATE_ICON_TOOLTIP . '" data-translations="' . htmlentities($current_field_value) . '"></i></span>';
                                    $current_field_value = $MSFrameworki18n->getFieldValue($current_field_value);
                                }


                                ?>

                                <label for="extra_field_<?= $unique_id; ?>"><?= $field['name']; ?></label> <?= $translation_circle; ?>
                                <?php if ($field['type'] == 'input' || $field['type'] == 'text' || $field['type'] == 'number') { ?>
                                    <input id="extra_field_<?= $unique_id; ?>" name="<?= $field_id; ?>"
                                           placeholder="<?= htmlentities($field['placeholder']); ?>" type="<?= ($field['type'] == 'number' ? 'number' : 'text'); ?>"
                                           class="extra_field_input form-control <?= ($field['required'] ? 'required' : ''); ?>"
                                           value="<?= $current_field_value; ?>" <?= ($field['required'] ? 'required' : ''); ?>>
                                <?php } else if ($field['type'] == 'colorpicker') { ?>
                                    <div class="input-group colorpicker-component">
                                        <input id="extra_field_<?= $unique_id; ?>" name="<?= $field_id; ?>"
                                               placeholder="<?= htmlentities($field['placeholder']); ?>" type="text"
                                               class="extra_field_input form-control colorpicker <?= ($field['required'] ? 'required' : ''); ?>"
                                               value="<?= $current_field_value; ?>" <?= ($field['required'] ? 'required' : ''); ?>>
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                <?php } else if ($field['type'] == 'textarea' || $field['type'] == 'textarea_html') { ?>
                                    <textarea id="extra_field_<?= $unique_id; ?>" name="<?= $field_id; ?>"
                                              placeholder="<?= htmlentities($field['placeholder']); ?>"
                                              class="extra_field_input form-control <?= ($field['type'] == 'textarea_html' ? 'wysiwyg' : ''); ?> <?= ($field['required'] ? 'required' : ''); ?>" <?= ($field['required'] ? 'required' : ''); ?>><?= $current_field_value; ?></textarea>
                                <?php } else if ($field['type'] == 'select') { ?>
                                    <select id="extra_field_<?= $unique_id; ?>" name="<?= $field_id; ?>"
                                            class="extra_field_input form-control <?= ($field['required'] ? 'required' : ''); ?>" <?= ($field['required'] ? 'required' : ''); ?>>
                                        <?php foreach ($field['values'] as $opt_val => $opt_text) { ?>
                                            <option value="<?= $opt_val; ?>" <?= ($current_field_value == $opt_val ? 'selected' : (isset($field['default']) && $field['default'] == $opt_val ? 'selected' : '')); ?>><?= $opt_text; ?></option>
                                        <?php } ?>
                                    </select>
                                <?php } else if ($field['type'] == 'uploader') { ?>
                                    <div class="extra_image_container" data-allowed="<?= (isset($field['allowed_ext']) ? htmlentities(json_encode($field['allowed_ext'])) : '[]'); ?>" data-max="<?= (isset($field['files_max']) ? $field['files_max'] : 1); ?>">
                                        <?php (new \MSFramework\uploads('PAGEWIDGET'))->initUploaderHTML("extra_field_" . $unique_id, json_encode(($current_field_value ? $current_field_value : array())), array("name" => $field_id, "realPath" => "extra_field_realPath", "prevUploaded" => "prev_upl_extra_field", "element" => "extra_field", "container" => "extra_field_input extra_field_container")); ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>

                    <?php
                    if (isset($extra_info['attr']) && in_array('duplicate', $extra_info['attr'])) {
                        echo '</div>';
                    }

                }

                if (isset($extra_info['attr']) && in_array('duplicate', $extra_info['attr'])) {
                    echo '</div>';
                }
                ?>

            </div>
        <?php } ?>
    <?php } ?>

<?php } ?>
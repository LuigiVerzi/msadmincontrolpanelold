<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardGrid.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div id="pagine_genitore">

                    <div id="pagine_figlie" style="display: none;">
                        <a href="#" class="btn btn-default" id="goToParentPage"><i class="fa fa-arrow-left"></i> Torna su <b></b></a>
                        <h2 class="title-divider" style="background: white; padding: 15px 15px; border: 1px solid #ececec; margin: 15px 0 30px;">Stai visualizzando: <b></b></h2>
                    </div>

                    <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Titolo</th>
                            <th>Slug</th>
                            <th>Tipologia</th>
                            <th class="is_data default-sort" data-sort="DESC">Ultima Modifica</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>
</body>
</html>
$(document).ready(function() {
    window.mainDataTable = loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');

    window.hierarchy_breadcrumb = [];

    $('#pagine_genitore').on('click', '.children_pages', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var name = $(this).data('name');

        window.hierarchy_breadcrumb.push({
            id: id,
            name: name
        });

        loadChildrenPages(hierarchy_breadcrumb);
    });

    $('#pagine_genitore').on('click', '#goToParentPage', function (e) {
        window.hierarchy_breadcrumb.pop();
        loadChildrenPages(hierarchy_breadcrumb);
    });

    $('#main_table_list').on('dataTableNew', function (e, ctrl) {

        if(window.hierarchy_breadcrumb.length > 0) {
            var parent_id = window.hierarchy_breadcrumb[window.hierarchy_breadcrumb.length-1].id;
            location.href = 'edit.php?parent=' + parent_id;
        } else {
            location.href = 'edit.php';
        }

        return false;
    });
});

function loadChildrenPages(breadrumb) {

    $('#pagine_genitore').fadeOut(function () {

        $('#main_table_list').one('drawCallback', function () {
            $('#pagine_genitore').fadeIn();
        });

        if (!breadrumb.length) {
            mainDataTable.ajax.url('ajax/dataTable.php').load();
            $('#pagine_figlie').hide();
        } else {
            var last_item = breadrumb.slice(-1)[0];
            var parent_item = (breadrumb.length >= 2 ? breadrumb.slice(-2) : false);


            if (parent_item.length > 1) {
                $('#pagine_figlie #goToParentPage b').text(parent_item[0].name);

                var breadcrumb_text = [];
                parent_item.forEach(function (current_page) {
                    breadcrumb_text.push(current_page.name);
                });
                $('#pagine_figlie h2 b').text(breadcrumb_text.join(' > '));

            } else {
                $('#pagine_figlie #goToParentPage b').text('Tutte le pagine');
                $('#pagine_figlie h2 b').text(last_item.name);
            }

            mainDataTable.ajax.url('ajax/dataTable.php?parent=' + last_item.id).load();
            $('#pagine_figlie').show();
        }

        unselectAllRow('main_table_list');

    });

}

function initForm() {
    initClassicTabsEditForm();

    if($('#record_id').val().indexOf('static') < 0) {
        initSlugChecker('titolo', 'slug', 'pagine', $('#record_id'));
        updateSlugParent('slug', $('#pagina_genitore').val(), $('#pagina_genitore').find('option:selected').data('slug'), ($('#divisore_slug').val() ? $('#divisore_slug').val() : $('#pagina_genitore').find('option:selected').data('divider')), false);

        $('#gallery_type').on('change', function() {
            $('.group_gallery').hide();

            $('#group_gallery_' + $(this).val()).show();
        }).trigger('change');

        $('#pagina_genitore').chosen().on('change', function () {

            var value = $(this).val();

            if(value > 0) {
                $('.eredita').show();
                $('#page_relation_value').attr('required', false).find('[value=""]').text("Eredita da Genitore");
            } else {
                $('.eredita').hide();
                if ($('#page_relation_value').length) {
                    $('#page_relation_value').attr('required', true).find('[value=""]').text($('#page_relation_value [value=""]').data('original'));
                }
            }

            var $divisore_slug = $('#divisore_slug');
            if(!$('#record_id').val().length) {
                $divisore_slug.val($divisore_slug.find('option:visible').first().val()).change();
            }

            updateSlugParent('slug', $(this).val(), $(this).find('option:selected').data('slug'), ($('#divisore_slug').val() ? $('#divisore_slug').val() : $('#pagina_genitore').find('option:selected').data('divider')));
        });
        $('#divisore_slug').on('change', function () {
            updateSlugParent('slug', $('#pagina_genitore').val(), $('#pagina_genitore').find('option:selected').data('slug'), ($('#divisore_slug').val() ? $('#divisore_slug').val() : $('#pagina_genitore').find('option:selected').data('divider')));
        });
        initGalleryUploader();

        initTinyMCE(
            '#pageShortContentText',
            {page: true}
        );

        initExtraFields();

        window.visual = $('#pageContentText').visualBuilder({
            uploads: 'PAGEGALLERY'
        });

    } else {
        if($('#slug').length) initSlugChecker('titolo', 'slug', 'pagine_private', $('#record_id'));
        window.visual = $('#pageContentText').visualBuilder({
            uploads: 'PAGEGALLERY',
            private_mode: true,
            private_blocks: JSON.parse($('#privatePageBlocks').val()),
            shortcodes: JSON.parse($('#privatePageShortcodes').val())
        });

    }

    $('#page_type').on('change', function (e) {

        if($('#page_type').data('old') === "none") {
            $('#page_type').data('old', $(this).val());
            return true;
        }

        var value = $(this).val();

        if(window.extra_fields_edited) {
            e.preventDefault();

            var edited_fields = "[" + $('#info_aggiuntive h2').map(function(){return $(this).text()}).get().join(', ') + "]";

            bootbox.confirm({
                message: "Attenzione! Hai effettuato delle modifiche alla pagina, continuando perderai le seguenti modifiche:<br><b>" + edited_fields + "</b>",
                buttons: {
                    confirm: {
                        label: 'Rimani nella pagina',
                        className: 'btn-success',
                    },
                    cancel: {
                        label: 'Voglio cambiare comunque',
                        className: 'btn-danger',
                    }
                },
                callback: function (result) {
                    if(result === true) {
                        var old_value = $('#page_type').data('old');
                        $('#page_type').data('old', 'none');
                        $('#page_type').val(old_value).change();
                    } else {
                        loadExtraFields(value);
                    }
                    return true;
                }
            });
        }
        else {
            loadExtraFields(value);
        }

    });

    if(!$('#custom_page_css').val().length) {
        $('#custom_page_css').val('\n');
    }

    window.customPageCSS = CodeMirror.fromTextArea($('#custom_page_css')[0], {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true,
        mode: 'css',
    });

    var inputs = [{
        preview_title:  [$('#seo_titolo')],
        preview_url: false,
        preview_description: [$('#seo_description')]
    },{
        preview_title: [$('#titolo')],
        preview_url: [$('#baseElementPathSW'), $('#slug')],
        preview_description: [$('#sottotitolo')]
    }];

    $("#serpPreview").serpPreview(inputs);
}

function initGalleryUploader() {
    initOrak('images', 25, 500, 300, 200, getOrakImagesToPreattach('images'), false, ['image/jpeg', 'image/png']);
}

function moduleSaveFunction() {
    if($('#record_id').val() != "" && $('#record_id').val().indexOf('global-') != -1) {
        if($('#userLevel').val() != "0") {
            bootbox.confirm('Questa è una pagina standard globale. Proseguendo, le modifiche apportate verranno copiate in una versione ad-hoc per il sito e non riceveranno automaticamente eventuali aggiornamenti apportati alla pagina globale. Vuoi proseguire?', function (result) {
                if (result) {
                    continueModuleSave();
                }
            });
        } else {
            var dialog = bootbox.dialog({
                message: "Questa è una pagina standard globale. Puoi applicare le modifiche apportate solo al sito in questione o globalmente a tutti i siti. Come desideri procedere?",
                buttons: {
                    cancel: {
                        label: "Annulla",
                        className: 'btn-danger'
                    },
                    just_this: {
                        label: "Applica a sito corrente",
                        className: 'btn-info',
                        callback: function(){
                            continueModuleSave();
                        }
                    },
                    apply_global: {
                        label: "Applica globalmente",
                        className: 'btn-info',
                        callback: function(){
                            continueModuleSave('global');
                        }
                    }
                }
            });
        }
    } else {
        continueModuleSave();
    }
}

function continueModuleSave(save_type) {
    if(typeof(save_type) == "undefined") save_type = "standard";

    var save_data = {};
    var url = '';

    if($('#record_id').val().indexOf('static-') >= 0) {
        save_data = {
            "pID": $('#record_id').val(),
            "pTitolo": $('#titolo').val(),
            "pSottoTitolo": $('#sottotitolo').val(),
            "pPageType": $('#page_type').val(),
            "pageContentText": $('#pageContentText').val(),
            "pSlug": ($('#slug').length ? $('#slug').val() : ''),
            "pAdditionalCSS": window.customPageCSS.getValue(),
        };

        url = 'db/savePrivateData.php';

    } else {
        gallery_settings = new Object();
        gallery_settings['type'] = $('#gallery_type').val();
        gallery_settings['gallery'] = "";

        if ($('#gallery_type').val() == "0") {
            gallery_settings['gallery'] = $('#import_gallery').val();
        } else if ($('#gallery_type').val() == "1") {
            gallery_settings['gallery'] = composeOrakImagesToSave('images');
        }

        save_data = {
            "pID": $('#record_id').val(),
            "pGlobalID": $('#global_id').val(),
            "pParent": $('#pagina_genitore').val(),
            "pTitolo": $('#titolo').val(),
            "pSottoTitolo": $('#sottotitolo').val(),
            "pPageType": $('#page_type').val(),
            "pageContentText": $('#pageContentText').val(),
            "pageShortContentText": tinymce.get('pageShortContentText').getContent(),
            "pSlug": ($('#slug').length ? $('#slug').val() : ''),
            "pSlugDivider": $('#divisore_slug').val(),
            "pGallerySettings": gallery_settings,
            "pAdditionalInfos": getExtraFieldsValues(),
            "pAdditionalCSS": window.customPageCSS.getValue(),
            "pProtected": $('#is_protected:checked').length,
            "pHidden": $('#is_hidden:checked').length,
            "pHideHeaderFooter": $('#is_hide_header_footer:checked').length,
            "pSaveType": save_type,
        };

        url = 'db/saveData.php';
    }

    save_data['pSeoData'] = {
      title: $('#seo_titolo').val(),
      description: $('#seo_description').val(),
    };

    $.ajax({
        url: url,
        type: "POST",
        data: save_data,
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);

                if($('#exitAfterSave').val() != "1") {
                    //se l'utente sceglie di continuare con la modifica ma ha selezionato una gallery già esistente, svuoto il contenuto delle gallery personalizzate
                    if($('#gallery_type').length && $('#gallery_type').val() == "0") {
                        $('#prev_upl_images').val('');
                        $('#images_container').html('<div id="images" orakuploader="on"></div>');
                        initGalleryUploader();
                    }
                }
            }
        }
    });
}

/* FUNZIONI CAMPI EXTRA */

function getExtraFieldsValues() {

    var extra_fields = {};

    $('#info_aggiuntive .extra_settings').each(function() {

        var $setting_block = $(this);
        var setting_id = $(this).data('id');

        // Creo un array se possiede più righe oppure un object
        var multiline = false;
        if($setting_block.find('.rowDuplication').length) {
            multiline = true;
        }

        extra_fields[setting_id] = {};

        $setting_block.find('.extra_field_input').each(function () {

            if($(this).hasClass('wysiwyg'))
            { // Editor HTML
                var name = $(this).attr('name');
                var value = tinymce.get($(this).attr('id')).getContent();
            }
            else if($(this).hasClass('extra_field_container'))
            { // Immagine
                var name = $(this).data('name');
                var value = new Array();
                $(this).find('input[name^=extra_field]').each(function(k, v) {
                    value.push($(this).val());
                });
            }
            else if(typeof($(this).attr('type')) !== 'undefined' && $(this).attr('type') === 'checkbox')
            {
                var name = $(this).attr('name');
                var value = ($(this).prop('checked') ? '1' : '0');
            }
            else
            { // Input Standard (SELECT/TEXTAREA/INPUT)
                var name = $(this).attr('name');
                var value = $(this).val();
            }

            var index = 0; // La imposto anche nei campi che hanno una sola riga di dati per ottimizzare l'elaborazione; Verrà tolta durante l'elaborazione dei dati
            if(multiline) {
                index = $(this).closest('.rowDuplication').index();
            }

            if (typeof(extra_fields[setting_id][index]) == "undefined")  extra_fields[setting_id][index] = {};
            extra_fields[setting_id][index][name] = value;

        });

    });

    return extra_fields;
}

function loadExtraFields(type) {

    var current_type_details = JSON.parse($('#pageTypes').val())[type];

    if(current_type_details['hide_content']) {
        $('#content_editor_body').hide();
    } else {
        $('#content_editor_body').show();
    }

    if(current_type_details['use_short_content']) {
        $('#short_content_container').show();
    } else {
        $('#short_content_container').hide();
    }

    $.ajax({
        url: "ajax/getExtraFields.php",
        type: "POST",
        data: {
            "page_id": $("#record_id").val(),
            "page_type": type
        },
        async: false,
        dataType: "text",
        success: function (data) {
            $("#info_aggiuntive").html(data);
            setTimeout(function () {
                initExtraFields();
            }, 500);
        }
    });
}

function initExtraFields() {

    // Distruggo tutti i vecchi TinyMCE
    for (var i = tinymce.editors.length - 1 ; i > -1 ; i--) {
        var ed_id = tinymce.editors[i].id;

        if(ed_id !== "pageContentText" && ed_id !== "pageShortContentText") {
            tinyMCE.execCommand("mceRemoveEditor", true, ed_id);
        }
    }

    window.extra_fields_edited = false;
    $('#info_aggiuntive').find('input,textarea').on('change', function () {
        window.extra_fields_edited = true;
    });

    // inizializzo i vari uploader
    $('#info_aggiuntive .extra_image_container').each(function () {
        var max_images = $(this).data('max');
        var allowed_ext = $(this).data('allowed');
        inizializeExtraFieldsUploader($(this).find('.extra_field'), max_images, allowed_ext);
    });

    // Inizializzo i vari editor HTML
    $('#info_aggiuntive textarea.wysiwyg').each(function () {
       inizializeExtraFieldsEditor($(this));
    });

    $('#info_aggiuntive .colorpicker-component').colorpicker();

    $('#info_aggiuntive .rowDuplication').each(function () {
        $rowDuplication = $(this);

        $rowDuplication.easyRowDuplication({
            "addButtonClasses": "btn btn-primary",
            "deleteButtonClasses": "btn btn-warning",
            "addButtonText": "Aggiungi elemento",
            "deleteButtonText": "Elimina elemento",
            "afterAddCallback": function($row) {

                window.extra_fields_edited = true;

                var $lastRow =  $row.parent().find('.rowDuplication:last');
                var newIndex = $lastRow.index();

                /* SOSTITUISCO TUTTI GLI INDEX */
                $lastRow.find('> .row').html($lastRow.find('> .row').html().replace(/_([0-9]+)/gm, '_' + newIndex));

                // Resetto i valori degli input
                $lastRow.find('.extra_field_input').val('');

                // Rimuovo TinyMCE dalle textarea HTML
                if( $lastRow.find('[role="application"]').length) {
                    $lastRow.find('[role="application"]').remove();
                    $lastRow.find('.wysiwyg').show();
                    inizializeExtraFieldsEditor($lastRow.find('.wysiwyg'));
                }

                /* RIMUOVO COLORPICKER */
                $lastRow.find('.colorpicker-component').removeClass('colorpicker-element');
                $lastRow.find('.colorpicker-component input-group-addon').html('');
                $lastRow.find('.colorpicker-component').colorpicker();

                // Resetto l'uploader e lo inizializzo nuovamente
                if($lastRow.find('.extra_image_container').length) {
                    $lastRow.find('.extra_image_container').each(function () {
                        var $imageContainer = $(this).closest('.extra_image_container');
                        var max_files = $imageContainer.data('max');
                        var allowed_ext = $imageContainer.data('allowed');

                        $imageContainer.find('.clone_item, [id$="DDArea"], .irs').remove();
                        $imageContainer.find('.orakUploaderCompressionRange').show().removeClass('loaded');
                        $imageContainer.find('.prev_upl_extra_field').val('');

                        var image_id = $imageContainer.find('.prev_upl_extra_field').attr('id').replace('prev_upl_', '');

                        $imageContainer.find('.extra_field_container').append('<div id="' + image_id +  '" class="' + image_id + ' extra_field" orakuploader="on"></div>');
                        inizializeExtraFieldsUploader($imageContainer.find('.extra_field'), max_files, allowed_ext);
                    });
                }


            },
            "afterDeleteCallback": function($row) {
                window.extra_fields_edited = true;

                // Distruggo l'eventuale TinyMCE allegato
                for (var i = tinymce.editors.length - 1 ; i > -1 ; i--) {
                    var ed_id = tinymce.editors[i].id;
                    if(!$("#" + ed_id).length) {
                        tinyMCE.execCommand("mceRemoveEditor", true, ed_id);
                    }
                }
            },
        });

    });

    prepareFormLabels($('#info_aggiuntive'));
    initIChecks();

    if($('#info_aggiuntive #page_relation_value').length) {

        $('#info_aggiuntive #page_relation_value').chosen().on('change', function (e) {
            if($(this).val() != '') {
                $('#edit_relative_element').show();
            } else {
                $('#edit_relative_element').hide();
            }

            if(!$('#titolo').hasClass('manually_set') && !$('#record_id').val().length) {
                if($(this).val()) {
                    $('#titolo').val($(this).find('option:selected').text()).change();
                } else {
                    $('#titolo').val('').change();
                }
            }
        }).change();

        $('#edit_relative_element').on('click', function (e) {
            e.preventDefault();

            showFastEditor($(this).data('target'), $('#info_aggiuntive #page_relation_value').val());
        });
    }

}

function inizializeExtraFieldsUploader($extra_field, max_files, allowed_ext) {

    var id = $extra_field.attr('id');
    var imagesToPreAttach = $extra_field.parent().find('.prev_upl_extra_field').val();

    if(!allowed_ext.length) {
        allowed_ext = ['image/jpeg', 'image/png'];
    }

    var preattach_array_extra_field = new Array();
    try {
        img_to_cycle = $.parseJSON(imagesToPreAttach) || new Array();
        $(img_to_cycle).each(function(k, v) {
            preattach_array_extra_field.push(v);
        })
    } catch (e) {}


    initOrak(id, max_files, 50, 50, 200, preattach_array_extra_field, false, allowed_ext);
}

function inizializeExtraFieldsEditor($editor) {

    initTinyMCE(
        $editor,
        {
            autoresize: false
        },
        {
            init_instance_callback: function (editor) {
                editor.on('change', function (e) {
                    window.extra_fields_edited = true;
                });
            }
        }
    );
}
<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($_POST['pSaveType'] == "global" && $_SESSION['userData']['userlevel'] != "0") {
    echo json_encode(array("status" => "not_allowed"));
    die();
}

if(strstr($_POST['pID'], "global-") && $_POST['pSaveType'] != "global") {
    $db_action = "insert";
} else {
    $db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);
}

$table_used = "pagine";
$saving_global = false;
if(strstr($_POST['pID'], "global-") && $_POST['pSaveType'] == "global") {
    $saving_global = true;
    $table_used = "`" . FRAMEWORK_DB_NAME . "`.`pagine`";
    $_POST['pID'] = str_replace("global-", "", $_POST['pID']);
}

$can_save = true;
if($_POST['pTitolo'] == "" || $_POST['pPageType'] == "" || $_POST['pSlug'] == "" || ($_POST['pGallerySettings']['type'] == "0" && $_POST['pGallerySettings']['gallery'] == "")) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$page_type = $MSFrameworkPages->getTypes($_POST['pPageType']);

$uploader = new \MSFramework\uploads('PAGEGALLERY');
$ary_files = array();

if($_POST['pGallerySettings']['type'] == "1") {
    $ary_files = $uploader->prepareForSave($_POST['pGallerySettings']['gallery']);
    if($ary_files === false) {
        echo json_encode(array("status" => "mv_error"));
        die();
    }
}

$_POST['pageContentText'] = $uploader->saveFromBlob($_POST['pageContentText']);

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT * FROM $table_used WHERE id = :id", array(":id" => $_POST['pID']), true);
    if($r_old_data['hidden'] == 1 && $_SESSION['userData']['userlevel'] != "0") {
        echo json_encode(array("status" => "not_allowed"));
        die();
    }

    $old_info_aggiuntive = json_decode($r_old_data['info_aggiuntive'], true);
    $old_widgetimg = array();

    array_walk($old_info_aggiuntive, function($val, $key) {
        global $page_type, $old_widgetimg;

        foreach($page_type["extra_fields"][$key]['fields'] as $ck => $cv) {
            if($cv["type"] == 'uploader') {
                if(is_numeric(array_keys($val)[0])) {
                    foreach($val as $multiline_key => $multiline_val) {
                        foreach($multiline_val[$ck] as $single_image) {
                            $old_widgetimg[] = $single_image;
                        }
                    }
                }
                else {
                    foreach($val[$ck] as $single_image) {
                        $old_widgetimg[] = $single_image;
                    }
                }
            }
        }
    });

    $old_img = array();
    $old_img_settings = json_decode($r_old_data['gallery'], true);
    if($old_img_settings['type'] == "1") {
        $old_img = $old_img_settings['gallery'];
    }
}


$images_from_fields = array();
foreach($_POST['pAdditionalInfos'] as $sezione_info_id => $righe_sezione) {

    foreach($righe_sezione as $riga_sezione => $campi_sezione) {

        $is_multirow = (in_array('duplicate', $page_type['extra_fields'][$sezione_info_id]['attr']));
        foreach ($campi_sezione as $id_campo_sezione => $valore_campo_sezione) {

            $dettagli_campo = $page_type['extra_fields'][$sezione_info_id]['fields'][$id_campo_sezione];

            if (isset($dettagli_campo['multilang']) && $dettagli_campo['multilang']) {

                if ($is_multirow) {
                    $old_value = $old_info_aggiuntive[$sezione_info_id][$riga_sezione][$id_campo_sezione];
                } else {
                    $old_value = $old_info_aggiuntive[$sezione_info_id][$id_campo_sezione];
                }

                //controlli di sicurezza per le traduzioni
                if ($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                        array('currentValue' => $valore_campo_sezione, 'oldValue' => $old_value),
                    )) == false) {
                    echo json_encode(array("status" => "no_datalang_for_primary"));
                    die();
                }

                $_POST['pAdditionalInfos'][$sezione_info_id][$riga_sezione][$id_campo_sezione] = $MSFrameworki18n->setFieldValue($old_value, $valore_campo_sezione);

            }

            if (isset($dettagli_campo['type']) && $dettagli_campo['type'] == 'uploader') {
                foreach ($valore_campo_sezione as $single_file) {
                    $images_from_fields[] = $single_file;
                }
            }


        }

        // Se il campo non è multiriga allora rimuovo la chiave [0] e lo abbasso di un livello
        if(!$is_multirow) {
            $_POST['pAdditionalInfos'][$sezione_info_id] =  $_POST['pAdditionalInfos'][$sezione_info_id][0];
        }

    }

}

$uploaderWidget = new \MSFramework\uploads('PAGEWIDGET');
$ary_widgetfiles = $uploaderWidget->prepareForSave($images_from_fields, array("png", "jpeg", "jpg", "pdf", "rar", "zip", "doc"));
if($ary_widgetfiles === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pTitolo'], 'oldValue' => $r_old_data['titolo']),
        array('currentValue' => $_POST['pSottoTitolo'], 'oldValue' => $r_old_data['sottotitolo']),
        array('currentValue' => $_POST['pageContentText'], 'oldValue' => $r_old_data['content']),
        array('currentValue' => $_POST['pageShortContentText'], 'oldValue' => $r_old_data['short_content']),
        array('currentValue' => $_POST['pSlug'], 'oldValue' => $r_old_data['slug'])
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$old_seo_data = (json_decode($r_old_data['seo']) ? json_decode($r_old_data['seo'], true) : array());
foreach($_POST['pSeoData'] as $seo_k => $seo_v) {
    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $seo_v, 'oldValue' => $old_seo_data[$seo_k]),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $_POST['pSeoData'][$seo_k] = $MSFrameworki18n->setFieldValue($old_seo_data[$seo_k], $seo_v);
}

if($db_action == "insert" && $_SESSION['userData']['userlevel'] != "0") {
    $_POST['pProtected'] = "0";
    $_POST['pHidden'] = "0";
}

$parent_check = '';
if($_POST['pParent']) {
    $parent_check = $MSFrameworkPages->getPageDetails($_POST['pParent']);
    if(!isset($parent_check[$_POST['pParent']])) {
        $_POST['pParent'] = '';
    }
}

$array_to_save = array(
    "titolo" => $MSFrameworki18n->setFieldValue($r_old_data['titolo'], $_POST['pTitolo']),
    "sottotitolo" => $MSFrameworki18n->setFieldValue($r_old_data['sottotitolo'], $_POST['pSottoTitolo']),
    "content" => $MSFrameworki18n->setFieldValue($r_old_data['content'], $_POST['pageContentText']),
    "short_content" => $MSFrameworki18n->setFieldValue($r_old_data['short_content'], $_POST['pageShortContentText']),
    "type" => $_POST['pPageType'],
    "protected" => $_POST['pProtected'],
    "hidden" => $_POST['pHidden'],
    "global_id" => $_POST['pGlobalID'],
    "parent" => $_POST['pParent'],
    "hide_header_footer" => $_POST['pHideHeaderFooter'],
    "slug" => $MSFrameworki18n->setFieldValue($r_old_data['slug'], $_POST['pSlug']),
    "slug_divider" => $_POST['pSlugDivider'],
    "gallery" => json_encode($_POST['pGallerySettings']),
    "info_aggiuntive" => json_encode($_POST['pAdditionalInfos']),
    "additional_css" => $_POST['pAdditionalCSS'],
    "seo" => json_encode($_POST['pSeoData']),
    "last_update" => time(),
);



/* CONTROLLA LO SLUG PRIMA DI SALVARE */
$slug_record_id = $_POST['pID'];

$complete_slug = $MSFrameworkPages->getPageParents($array_to_save);

$slug_to_check = array($_POST['pSlug']);
if($complete_slug) {
    $slug_to_check = array_merge($complete_slug, $slug_to_check);
}

if(!$MSFrameworkUrl->checkSlugAvailability($slug_to_check, ($saving_global ? substr($table_used, 1, -1) : $table_used), $slug_record_id)) {
    $array_to_save['slug'] = $MSFrameworki18n->setFieldValue($r_old_data['slug'], $MSFrameworkUrl->findNextAvailableSlug($slug_to_check));
}

if($r_old_data['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") {
    unset($array_to_save['titolo']);
    unset($array_to_save['sottotitolo']);
    unset($array_to_save['type']);
    unset($array_to_save['slug']);
}

if($db_action == "update" && $_SESSION['userData']['userlevel'] != "0") {
    unset($array_to_save['protected']);
    unset($array_to_save['hidden']);
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO $table_used ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE $table_used SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}


if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
        $uploaderWidget->unlink($ary_widgetfiles);

        $uploader->unlinkFromHTML($_POST['pContent']);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, $old_img);
    $uploaderWidget->unlink($ary_widgetfiles, $old_widgetimg);

    $uploader->unlinkFromHTML($_POST['pageContentText'], $r_old_data['content']);
}

$page_id = ($saving_global ? 'global-' : '') . ($db_action == 'insert' ?  $MSFrameworkDatabase->lastInsertId() : $_POST['pID']);
$MSFrameworkPages->url_cache = array();
$MSFrameworkUrl->makeRedirectIfNeeded('page-' . $page_id, $MSFrameworkPages->getURL($page_id), ($db_action === 'update' ? $MSFrameworkPages->getURL($r_old_data) : ''));

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $page_id : '')));
die();

function recursive_keys(array $array, array $path = array()) {
    $result = array();
    foreach ($array as $key => $val) {
        $currentPath = array_merge($path, array($key));
        if (is_array($val)) {
            $result = array_merge($result, recursive_keys($val, $currentPath));
        } else {
            $result[] = join('_', $currentPath);
        }
    }
    return $result;
}
?>
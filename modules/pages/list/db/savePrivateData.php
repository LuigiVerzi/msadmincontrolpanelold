<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$_POST['pID'] = str_replace('static-', '', $_POST['pID']);

$page_info = array();
if(isset($_POST['pID']) && !empty($_POST['pID'])) {
    $page_info = (new \MSFramework\pages())->getPrivatePageData($_POST['pID']);
}

$can_save = true;
if($_POST['pID'] == "" || $_POST['pageContentText'] == "" || !$page_info) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$old_data = (new \MSFramework\pages())->getPrivatePageData($_POST['pID']);

//controlli di sicurezza per le traduzioni
$lang_to_check = array(
    array('currentValue' => $_POST['pTitolo'], 'oldValue' => $old_data['titolo']),
    array('currentValue' => $_POST['pSottoTitolo'], 'oldValue' => $old_data['sottotitolo']),
    array('currentValue' => $_POST['pageContentText'], 'oldValue' => $old_data['content'])
);

if(isset($old_data['details']['slug'])) {
    $lang_to_check[] = array('currentValue' => $_POST['pSlug'], 'oldValue' => $old_data['slug']);
}

if($MSFrameworki18n->checkIfPrimaryHasValueForField($lang_to_check) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}


$old_seo_data = (json_decode($old_data['seo']) ? json_decode($old_data['seo'], true) : array());
foreach($_POST['pSeoData'] as $seo_k => $seo_v) {
    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
            array('currentValue' => $seo_v, 'oldValue' => $old_seo_data[$seo_k]),
        )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $_POST['pSeoData'][$seo_k] = $MSFrameworki18n->setFieldValue($old_seo_data[$seo_k], $seo_v);
}

$uploader = new \MSFramework\uploads('PAGEGALLERY');
$_POST['pageContentText'] = $uploader->saveFromBlob($_POST['pageContentText']);

// Converto le referenze degli uploads
$array_to_save = array(
    "id" => $_POST['pID'],
    "titolo" => $MSFrameworki18n->setFieldValue($old_data['titolo'], $_POST['pTitolo']),
    "sottotitolo" => $MSFrameworki18n->setFieldValue($old_data['sottotitolo'], $_POST['pSottotitolo']),
    "content" => $MSFrameworki18n->setFieldValue($old_data['content'], $_POST['pageContentText']),
    "type" => $_POST['pPageType'],
    "additional_css" => $_POST['pAdditionalCSS'],
    "seo" => json_encode($_POST['pSeoData'])
);

if(isset($old_data['details']['slug'])) {
    $array_to_save["slug"] = $MSFrameworki18n->setFieldValue($old_data['slug'], $_POST['pSlug']);
}

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
$result = $MSFrameworkDatabase->pushToDB("DELETE FROM pagine_private WHERE id = :id", array(':id' => $array_to_save['id']));
$result = $MSFrameworkDatabase->pushToDB("INSERT INTO pagine_private ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

if(!$result) {
    $uploader->unlinkFromHTML($_POST['pageContentText']);
    echo json_encode(array("status" => "query_error"));
    die();
}

if($old_data['content']) {
    $uploader->unlinkFromHTML($_POST['pageContentText'], $old_data['content']);
}

echo json_encode(array("status" => "ok", "id" => ''));
die();
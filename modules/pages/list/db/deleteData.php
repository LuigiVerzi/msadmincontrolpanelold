<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array('PAGEGALLERY' => array(), 'PAGEWIDGET' => array());
$contents_html = array();

foreach($MSFrameworkPages->getPageDetails($_POST['pID']) as $r_old_data) {

    if ($r_old_data['protected'] == 1 && $_SESSION['userData']['userlevel'] != "0") {
        echo json_encode(array("status" => "protected"));
        die();
    }

    if ($r_old_data['hidden'] == 1 && $_SESSION['userData']['userlevel'] != "0") {
        echo json_encode(array("status" => "not_allowed"));
        die();
    }

    if (strstr($_POST['pID'], "global-")) {
        echo json_encode(array("status" => "not_allowed"));
        die();
    }

    $gallery_settings = json_decode($r_old_data['gallery'], true);
    if($gallery_settings['type'] == "1") {
        $images_to_delete['PAGEGALLERY'] = array_merge_recursive($images_to_delete['PAGEGALLERY'], $gallery_settings['gallery']);
    }

    foreach($r_old_data['info_aggiuntive'] as $info_aggiuntiva) {
        foreach($info_aggiuntiva as $parametro) {
            if(is_array($parametro) && preg_match("/[0-9]{10}.*/", $parametro[0])) {
                $images_to_delete['PAGEWIDGET'] = array_merge_recursive($images_to_delete['PAGEWIDGET'], $parametro);
            }
        }
    }

    $contents_html[] = $r_old_data['content'];

}

if($MSFrameworkDatabase->deleteRow("pagine", "id", $_POST['pID'])) {

    foreach($images_to_delete as $uploader_key => $images) {
        (new \MSFramework\uploads($uploader_key))->unlink($images);
    }

    foreach($contents_html as $html) {
        (new \MSFramework\uploads('PAGEGALLERY'))->unlinkFromHTML($html);
    }

    $MSFrameworkUrl->deleteRedirectsByReferences('page', $_POST['pID']);

    // Rimuovo il parent nelle pagine figlie
    $MSFrameworkDatabase->pushToDB("UPDATE pagine SET parent = 0 WHERE parent IN(" . implode(',', $_POST['pID']) . ")");

    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


<?php
/**
 * MSAdminControlPanel
 * Date: 18/05/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    if($_SESSION['userData']['userlevel'] != "0" && $_GET['id'] == '') {
        header("location: " . ABSOLUTE_ADMIN_MODULES_PATH_HTML . "pages/widget/index.php");
        die();
    }

    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM widget WHERE id = :id", array($_GET['id']), true);
}
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome*</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Questo nome è solo un riferimento per identificare facilmente il widget. Non verrà visualizzato dagli utenti. Solo i SuperAdmin possono modificare il nome di un Widget."></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($r['nome']) ?>" <?php echo ($_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>>
                                </div>

                                <?php
                                $is_active_check = "checked";
                                if($r['is_active'] == 0 && $_GET['id'] != "") {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-3">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="easyRowDuplicationContainer">
                                <?php
                                $fields = json_decode($r['fields'], true);
                                if(count($fields) == 0) {
                                    $fields[] = array();
                                }

                                foreach($fields as $k => $v) {
                                    ?>
                                    <div class="stepsDuplication rowDuplication fieldsDuplication">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Nome campo</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Questo nome è solo un riferimento per identificare facilmente il campo. Non verrà visualizzato dagli utenti. Solo i SuperAdmin possono modificare il nome di un campo, aggiungerne di nuovi o modificarne la tipologia."></i></span>
                                                <input type="text" class="form-control field_name" value="<?php echo htmlentities($v['name']) ?>" <?php echo ($_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>>
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Tipologia campo</label>
                                                <select id="field_type" class="form-control field_type" <?php echo ($_SESSION['userData']['userlevel'] != "0") ? "disabled" : "" ?>>
                                                    <?php
                                                    foreach((new \MSFramework\widget())->getFieldTypes() as $fieldK => $fieldV) {
                                                        $cur_display_type = "display_type_" . $fieldK;
                                                        $cur_set_value = "set_value_" . $fieldK;

                                                        if($v['type'] == $fieldK || ($v['type'] == "" && $fieldK == "0")) {
                                                            $$cur_display_type = " display: block; ";
                                                            $$cur_set_value = $v['value'];
                                                            $value_for_translation = $v['value'];
                                                        } else {
                                                            $$cur_display_type = " display: none; ";
                                                            $$cur_set_value = "";
                                                        }
                                                    ?>
                                                        <option value="<?php echo $fieldK ?>" <?php if($v['type'] == (string)$fieldK) { echo "selected"; } ?>><?php echo $fieldV ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-sm-5 type_container_overall">
                                                <label>Valore campo</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($value_for_translation) ?>"></i></span>

                                                <div class="type_container type_0_container" style="<?php echo $display_type_0 ?>">
                                                    <input type="text" class="form-control field_value_txt msShort" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($set_value_0)) ?>">
                                                </div>

                                                <div class="type_container type_1_container" style="<?php echo $display_type_1 ?>">
                                                    <?php
                                                    (new \MSFramework\uploads('WIDGET'))->initUploaderHTML("image_" . $k, json_encode($set_value_1), array("realPath" => "image_realPath", "prevUploaded" => "prev_upl_image", "element" => "image", "container" => "image_container"));
                                                    ?>
                                                </div>

                                                <div class="type_container type_2_container" style="<?php echo $display_type_2 ?>">
                                                    <textarea class="form-control textarea_value_txt"><?php echo $MSFrameworki18n->getFieldValue($set_value_2) ?></textarea>
                                                </div>

                                                <div class="type_container type_3_container" style="<?php echo $display_type_3 ?>">
                                                    <select class="form-control field_value_txt">
                                                        <?php
                                                            foreach((new \MSFramework\menu())->getMenuDetails() as $value) {
                                                        ?>
                                                            <option value="<?= $value['id'] ?>" <?php echo ($set_value_3 == $value['id'] ? "selected" : "") ?>><?= $MSFrameworki18n->getFieldValue($value['nome'], true) ?></option>
                                                        <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
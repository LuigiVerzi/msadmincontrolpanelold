<?php
/**
 * MSAdminControlPanel
 * Date: 18/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);
if($db_action == "insert" && $_SESSION['userData']['userlevel'] != "0") {
    echo json_encode(array("status" => "not_allowed"));
    die();
}

$can_save = true;

if($_POST['pNome'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

$duplicates_check_sql = array('', array());
if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT fields, nome FROM widget WHERE id = :id", array(":id" => $_POST['pID']), true);

    $old_fields = json_decode($r_old_data['fields'], true);
    foreach($old_fields as $old_field_v) {
        if($old_field_v['type'] == "1") {
            foreach($old_field_v['value'] as $single_file) {
                $old_img[] = $single_file;
            }
        }
    }

    $duplicates_check_sql = array(" AND id != :id ", array(":id" => $_POST['pID']));
}

if($_POST['pNome'] != $r_old_data['nome'] && $_SESSION['userData']['userlevel'] != "0") {
    echo json_encode(array("status" => "not_allowed"));
    die();
}

if($MSFrameworkDatabase->getCount("SELECT nome FROM widget WHERE nome = :nome " . $duplicates_check_sql[0], array_merge($duplicates_check_sql[1], array(":nome" => $_POST['pNome']))) != 0) {
    echo json_encode(array("status" => "same_name_exists"));
    die();
}

$gotNames = array();
$images_from_fields = array();
foreach($_POST['pFields'] as $kField => $vField) {
    if(in_array($vField['name'], $gotNames)) {
        continue; //i nomi dei campi devono essere univoci! se ne esiste uno uguale, lo salto!
    }

    $gotNames[] = $vField['name'];

    //controlli di sicurezza per il livello utente
    if(($vField['type'] != $old_fields[$kField]['type'] || $vField['name'] != $old_fields[$kField]['name']) && $_SESSION['userData']['userlevel'] != "0") {
        echo json_encode(array("status" => "not_allowed"));
        die();
    }

    if($vField['type'] == "0" || $vField['type'] == "2") {
        //controlli di sicurezza per le traduzioni
        if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                array('currentValue' => $vField['value'], 'oldValue' => $old_fields[$kField]['value']),
            )) == false) {
            echo json_encode(array("status" => "no_datalang_for_primary"));
            die();
        }

        $_POST['pFields'][$kField]['value'] =  $MSFrameworki18n->setFieldValue($old_fields[$kField]['value'], $vField['value']);
    } else if($vField['type'] == "1") {
        foreach($vField['value'] as $single_file) {
            $images_from_fields[] = $single_file;
        }
    }
}

$uploader = new \MSFramework\uploads('WIDGET');
$ary_files = $uploader->prepareForSave($images_from_fields, array("png", "jpeg", "jpg", "svg"));
if($ary_files === false) {
    echo json_encode(array("status" => "mv_error"));
    die();
}

$array_to_save = array(
    "nome" => $_POST['pNome'],
    "is_active" => $_POST['pIsActive'],
    "fields" => json_encode($_POST['pFields']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO widget ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE widget SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, $old_img);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 18/05/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

if($_SESSION['userData']['userlevel'] != "0") {
    echo json_encode(array("status" => "not_allowed"));
    die();
}

$images_to_delete = array();
foreach((new \MSFramework\widget())->getFieldsByWidgetID($_POST['pID']) as $r) {
    foreach(json_decode($r['fields'], true) as $old_field_v) {
        if($old_field_v['type'] == "1") {
            $images_to_delete = array_merge_recursive ($images_to_delete, $old_field_v['value']);
        }
    }
}

if($MSFrameworkDatabase->deleteRow("widget", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('WIDGET'))->unlink($images_to_delete);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('.rowDuplication').easyRowDuplication({
        "addButtonClasses": "btn btn-primary",
        "deleteButtonClasses": "btn btn-warning",
        "addButtonText": "Aggiungi campo",
        "deleteButtonText": "Elimina campo",
        "afterAddCallback": function() {
            initFieldTypeChange();

            $('.rowDuplication:last').find('.field_name').val('');
            $('.rowDuplication:last').find('.field_type').val('0').trigger('change');
            $('.rowDuplication:last').find('.field_value_txt').val('');

            $('.rowDuplication:last').find('.type_2_container').html('<textarea class="form-control textarea_value_txt"></textarea>');

            $('.rowDuplication:last').find('.prev_upl_image').html('');
            $('.rowDuplication:last').find('.image_container .clone_item').remove();
            $('.rowDuplication:last').find('.image_container [id$="DDArea"]').remove();
            $('.rowDuplication:last').find('.image_container').append('<div id="image_x" class="image" orakuploader="on"></div>');
        },
        "afterDeleteCallback": function() {
        },
    });

    initFieldTypeChange();

    $('.field_type').trigger('change');
}

function initFieldTypeChange() {
    $('.field_type').unbind('change');
    $('.field_type').on('change', function() {
        index = $(".field_type").index($(this));

        $('.type_container_overall:eq(' + index + ') .type_container').hide();
        $('.type_' + $(this).val() + '_container:eq(' + index + ')').show();

        if($(this).val() == "0" || $(this).val() == "2") {
            $('.type_container_overall:eq(' + index + ')').find('.ms-label-tooltip').show();
        } else {
            $('.type_container_overall:eq(' + index + ')').find('.ms-label-tooltip').hide();
        }

        if($(this).val() == "1") {
            $('.image_container:eq(' + index + ') .image').attr('id', 'image_' + index);
            $('.image_container:eq(' + index + ') .image_realPath').attr('id', 'image_' + index + "_realPath");

            preattach_array_image = new Array();
            if($('.prev_upl_image:eq(' + index + ')').val() != "") {
                img_to_cycle = $.parseJSON($('.prev_upl_image:eq(' + index + ')').val()) || new Array();
                $(img_to_cycle).each(function(k, v) {
                    preattach_array_image.push(v);
                })
            }

            initOrak('image_' + index, 1, 100, 50, 200, preattach_array_image, false, ['image/jpeg', 'image/png', 'image/svg+xml']);
        }

        if($(this).val() == "2") {
            initTinyMCE( '.textarea_value_txt:eq(' + index + ')');
        }
    })
}

function moduleSaveFunction() {
    fields_ary = new Array();
    $('.stepsDuplication.fieldsDuplication').each(function(row_index) {
        fields_ary_tmp = new Object();

        field_name = $(this).find('.field_name').val();
        if(field_name == "") {
            return true;
        }

        fields_ary_tmp['name'] = field_name;
        fields_ary_tmp['type'] = $(this).find('.field_type').val();
        if($(this).find('.field_type').val() == "0" || $(this).find('.field_type').val() == "3") {
            fields_ary_tmp['value'] = $(this).find('.type_' + $(this).find('.field_type').val() + '_container').find('.field_value_txt').val();
        } else if($(this).find('.field_type').val() == "1") {
            images_ary = new Array();
            $(this).find('input[name^=image]').each(function(k, v) {
                images_ary.push($(this).val());
            })

            fields_ary_tmp['value'] = images_ary;
        } else if($(this).find('.field_type').val() == "2") {
            tinymce.triggerSave();
            fields_ary_tmp['value'] = $('.textarea_value_txt:eq(' + row_index + ')').val();
        }

        fields_ary.push(fields_ary_tmp);
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pIsActive": $('#is_active:checked').length,
            "pFields": fields_ary,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "same_name_exists") {
                bootbox.error("Esiste già un widget con questo nome. Scegliere un nome diverso per continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
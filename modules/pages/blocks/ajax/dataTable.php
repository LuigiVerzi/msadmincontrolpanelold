<?php
/**
 * MSAdminControlPanel
 * Date: 25/10/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM blocks") as $r) {
    $array['data'][] = array(
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['nome']),
        array("display" => date("d/m/Y H:i", strtotime($r['last_edit_date'])), "sort" => strtotime($r['last_edit_date'])),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

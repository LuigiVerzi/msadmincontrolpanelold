<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach((new \MSFramework\popup())->getPopupDetails($_POST['pID']) as $r_old_data) {
    $contents_html[] = $r_old_data['content'];
}

if($MSFrameworkDatabase->deleteRow("popup", "id", $_POST['pID'])) {
    foreach($contents_html as $html) {
        (new \MSFramework\uploads('POPUP'))->unlinkFromHTML($html);
    }
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


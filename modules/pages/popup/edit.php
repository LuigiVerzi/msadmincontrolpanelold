<?php
/**
 * MSAdminControlPanel
 * Date: 18/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

$r = array();
if(isset($_GET['id']) && !empty($_GET['id'])) {
    $r = (new \MSFramework\popup())->getPopupDetails($_GET['id']);
    if($r) $r = $r[$_GET['id']];
}
?>

<body class="skin-1 fixed-sidebar pace-done">

<style>
    .CodeMirror-sizer {
        min-height: 250px !important;
    }
</style>

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">
                        <h1>Contenuto</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-9 col-lg-10">
                                    <label>Titolo</label>
                                    <input id="titolo" name="titolo" type="text" class="form-control required" value="<?php echo htmlentities($r['titolo']) ?>">
                                </div>

                                <div class="col-sm-3 col-lg-2">
                                    <label> </label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php if($r['active'] == "1" || $_GET['id'] == "") { echo "checked"; } ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Mostra automaticamente</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="only_for_inactive <?= (!$r ? 'without_data' : ''); ?>" style="margin-bottom: 15px;">
                                <label>Utilizza il seguente codice JS per mostrare il Popup.</label>
                                <code id="jsCode">initMSPopup(<?= $r['id']; ?>)</code>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Contenuto</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['content']) ?>"></i></span>
                                    <textarea id="content" name="content" type="text" class="form-control required"><?php echo htmlentities($MSFrameworki18n->getFieldValue($r['content'])) ?></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <h1 class="only_for_active">Condizioni</h1>
                        <fieldset>

                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="styled-checkbox sync_query_settings form-control" style="margin: 0;">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="disable_conditions" <?php if(!$r['options']['conditions']) { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Mostra a tutti</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="sync_query_container" style="margin-top: 15px; display: <?= ($r['options']['conditions']? 'block' : 'none'); ?>;">
                                <textarea id="sync_settings" style="display: none;"><?= json_encode($r['options']['conditions']); ?></textarea>
                                <textarea id="query_filters" style="display: none;"> <?= json_encode((new \MSFramework\Newsletter\conditions())->getConditionsQueryFilters()); ?></textarea>
                                <textarea id="query_operators" style="display: none;"> <?= json_encode((new \MSFramework\Newsletter\conditions())->getConditionsQueryOperators()); ?></textarea>

                                <h2 class="title-divider">Gestisci condizioni</h2>

                                <div class="alert alert-info">
                                    Impostando delle condizioni, il cliente non visualizzerà il Popup sino a quando tutte le condizioni non saranno soddisfatte.
                                </div>

                                <div id="sync_query"></div>
                            </div>

                            <h2 class="title-divider">Limita tramite Link</h2>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Mostra SOLO sulle seguenti pagine</label>
                                    <input type="text" id="links_to_include" class="form-control linkAutocomplete" value="<?= htmlentities($r['options']['pages_to_include']); ?>">
                                </div>

                                <div class="col-lg-12">
                                    <label>NON mostrare sulle seguenti pagine</label>
                                    <input type="text" id="links_to_exclude" class="form-control linkAutocomplete" value="<?= htmlentities($r['options']['pages_to_exclude']); ?>">
                                </div>
                            </div>

                        </fieldset>

                        <h1 class="only_for_active">Comportamento</h1>
                        <fieldset>

                            <div class="row">
                                <div class="col-lg-3">
                                    <label>Visualizzazione</label>
                                    <select id="behavior" class="form-control" style="margin-bottom: 0;">
                                        <option value="immediately" <?= ($r['options']['behavior']['mode'] === 'immediately' ? 'selected' : ''); ?>>All'apertura della pagina</option>
                                        <option value="on_exit"" <?= ($r['options']['behavior']['mode'] === 'on_exit' ? 'selected' : ''); ?>>Prima della chiusura della pagina</option>
                                    </select>
                                </div>

                                <div class="col-lg-3 pull-right">
                                    <label> </label>
                                    <div class="styled-checkbox form-control" style="margin: 0;">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="force_showing" <?php if($r['options']['behavior']['force_showing']) { echo "checked"; } ?> />
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Mostra insistentemente</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <h2 class="title-divider">Preferenze</h2>
                            <div class="show_behavior_box" data-type="immediately" style="display: <?= (!isset($r['options']['behavior']['mode']) || $r['options']['behavior']['mode'] === 'immediately' ? 'block' : 'none'); ?>">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label>Attesa <small>(secondi)</small></label>
                                        <input name="wait" type="number" placeholder="Lascia vuoto per mostare subito" class="form-control" value="<?php echo htmlentities($r['options']['behavior']['wait']) ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="show_behavior_box" data-type="on_exit" style="display: <?= ($r['options']['behavior']['mode'] === 'on_exit' ? 'block' : 'none'); ?>">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label>Permanenza minima <small>(secondi)</small></label>
                                        <input name="wait" type="number" placeholder="Lascia vuoto per mostare subito" class="form-control" value="<?php echo htmlentities($r['options']['behavior']['wait']) ?>">
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Aspetto</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-3">
                                    <label>Larghezza massima <small>(px)</small></label>
                                    <input id="max-width" type="number" class="form-control" placeholder="1520" value="<?php echo htmlentities($r['options']['appearance']['max-width']) ?>">
                                </div>
                                <div class="col-lg-3">
                                    <label>Overlay</label>
                                    <select class="form-control" id="overlay">
                                        <option value="">Nessuno</option>
                                        <option value="light" <?= ($r['options']['appearance']['overlay'] === 'light' ? 'selected' : ''); ?>>Chiaro</option>
                                        <option value="dark" <?= ($r['options']['appearance']['overlay'] === 'dark' ? 'selected' : ''); ?>>Scuro</option>
                                    </select>
                                </div>
                            </div>

                            <h2 class="title-divider">Animazioni</h2>

                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Animazione in entrata</label>
                                    <select class="form-control" id="in_animation">
                                        <optgroup label="Bouncing Entrances">
                                            <option value="bounceIn" <?= ($r['options']['appearance']['in-animation'] === 'bounceIn' ? 'selected' : ''); ?>>bounceIn</option>
                                            <option value="bounceInDown" <?= ($r['options']['appearance']['in-animation'] === 'bounceInDown' ? 'selected' : ''); ?>>bounceInDown</option>
                                            <option value="bounceInLeft" <?= ($r['options']['appearance']['in-animation'] === 'bounceInLeft' ? 'selected' : ''); ?>>bounceInLeft</option>
                                            <option value="bounceInRight" <?= ($r['options']['appearance']['in-animation'] === 'bounceInRight' ? 'selected' : ''); ?>>bounceInRight</option>
                                            <option value="bounceInUp" <?= ($r['options']['appearance']['in-animation'] === 'bounceInUp' ? 'selected' : ''); ?>>bounceInUp</option>
                                        </optgroup>

                                        <optgroup label="Fading Entrances">
                                            <option value="fadeIn" <?= ($r['options']['appearance']['in-animation'] === 'fadeIn' ? 'selected' : ''); ?>>fadeIn</option>
                                            <option value="fadeInDown" <?= ($r['options']['appearance']['in-animation'] === 'fadeInDown' ? 'selected' : ''); ?>>fadeInDown</option>
                                            <option value="fadeInDownBig" <?= ($r['options']['appearance']['in-animation'] === 'fadeInDownBig' ? 'selected' : ''); ?>>fadeInDownBig</option>
                                            <option value="fadeInLeft" <?= ($r['options']['appearance']['in-animation'] === 'fadeInLeft' ? 'selected' : ''); ?>>fadeInLeft</option>
                                            <option value="fadeInLeftBig" <?= ($r['options']['appearance']['in-animation'] === 'fadeInLeftBig' ? 'selected' : ''); ?>>fadeInLeftBig</option>
                                            <option value="fadeInRight" <?= ($r['options']['appearance']['in-animation'] === 'fadeInRight' ? 'selected' : ''); ?>>fadeInRight</option>
                                            <option value="fadeInRightBig" <?= ($r['options']['appearance']['in-animation'] === 'fadeInRightBig' ? 'selected' : ''); ?>>fadeInRightBig</option>
                                            <option value="fadeInUp" <?= ($r['options']['appearance']['in-animation'] === 'fadeInUp' ? 'selected' : ''); ?>>fadeInUp</option>
                                            <option value="fadeInUpBig" <?= ($r['options']['appearance']['in-animation'] === 'fadeInUpBig' ? 'selected' : ''); ?>>fadeInUpBig</option>
                                        </optgroup>

                                        <optgroup label="Flippers">
                                            <option value="flip" <?= ($r['options']['appearance']['in-animation'] === 'flip' ? 'selected' : ''); ?>>flip</option>
                                            <option value="flipInX" <?= ($r['options']['appearance']['in-animation'] === 'flipInX' ? 'selected' : ''); ?>>flipInX</option>
                                            <option value="flipInY" <?= ($r['options']['appearance']['in-animation'] === 'flipInY' ? 'selected' : ''); ?>>flipInY</option>
                                            <option value="flipOutX" <?= ($r['options']['appearance']['in-animation'] === 'flipOutX' ? 'selected' : ''); ?>>flipOutX</option>
                                            <option value="flipOutY" <?= ($r['options']['appearance']['in-animation'] === 'flipOutY' ? 'selected' : ''); ?>>flipOutY</option>
                                        </optgroup>

                                        <optgroup label="Lightspeed">
                                            <option value="lightSpeedIn" <?= ($r['options']['appearance']['in-animation'] === 'lightSpeedIn' ? 'selected' : ''); ?>>lightSpeedIn</option>
                                            <option value="lightSpeedOut" <?= ($r['options']['appearance']['in-animation'] === 'lightSpeedOut' ? 'selected' : ''); ?>>lightSpeedOut</option>
                                        </optgroup>

                                        <optgroup label="Rotating Entrances">
                                            <option value="rotateIn" <?= ($r['options']['appearance']['in-animation'] === 'rotateIn' ? 'selected' : ''); ?>>rotateIn</option>
                                            <option value="rotateInDownLeft" <?= ($r['options']['appearance']['in-animation'] === 'rotateInDownLeft' ? 'selected' : ''); ?>>rotateInDownLeft</option>
                                            <option value="rotateInDownRight" <?= ($r['options']['appearance']['in-animation'] === 'rotateInDownRight' ? 'selected' : ''); ?>>rotateInDownRight</option>
                                            <option value="rotateInUpLeft" <?= ($r['options']['appearance']['in-animation'] === 'rotateInUpLeft' ? 'selected' : ''); ?>>rotateInUpLeft</option>
                                            <option value="rotateInUpRight" <?= ($r['options']['appearance']['in-animation'] === 'rotateInUpRight' ? 'selected' : ''); ?>>rotateInUpRight</option>
                                        </optgroup>

                                        <optgroup label="Sliding Entrances">
                                            <option value="slideInUp" <?= ($r['options']['appearance']['in-animation'] === 'slideInUp' ? 'selected' : ''); ?>>slideInUp</option>
                                            <option value="slideInDown" <?= ($r['options']['appearance']['in-animation'] === 'slideInDown' ? 'selected' : ''); ?>>slideInDown</option>
                                            <option value="slideInLeft" <?= ($r['options']['appearance']['in-animation'] === 'slideInLeft' ? 'selected' : ''); ?>>slideInLeft</option>
                                            <option value="slideInRight" <?= ($r['options']['appearance']['in-animation'] === 'slideInRight' ? 'selected' : ''); ?>>slideInRight</option>

                                        </optgroup>

                                        <optgroup label="Zoom Entrances">
                                            <option value="zoomIn" <?= ($r['options']['appearance']['in-animation'] === 'zoomIn' ? 'selected' : ''); ?>>zoomIn</option>
                                            <option value="zoomInDown" <?= ($r['options']['appearance']['in-animation'] === 'zoomInDown' ? 'selected' : ''); ?>>zoomInDown</option>
                                            <option value="zoomInLeft" <?= ($r['options']['appearance']['in-animation'] === 'zoomInLeft' ? 'selected' : ''); ?>>zoomInLeft</option>
                                            <option value="zoomInRight" <?= ($r['options']['appearance']['in-animation'] === 'zoomInRight' ? 'selected' : ''); ?>>zoomInRight</option>
                                            <option value="zoomInUp" <?= ($r['options']['appearance']['in-animation'] === 'zoomInUp' ? 'selected' : ''); ?>>zoomInUp</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Animazione in uscita</label>
                                    <select class="form-control" id="out_animation">

                                        <optgroup label="Bouncing Exits">
                                            <option value="bounceOut" <?= ($r['options']['appearance']['out-animation'] === 'bounceOut' ? 'selected' : ''); ?>>bounceOut</option>
                                            <option value="bounceOutDown" <?= ($r['options']['appearance']['out-animation'] === 'bounceOutDown' ? 'selected' : ''); ?>>bounceOutDown</option>
                                            <option value="bounceOutLeft" <?= ($r['options']['appearance']['out-animation'] === 'bounceOutLeft' ? 'selected' : ''); ?>>bounceOutLeft</option>
                                            <option value="bounceOutRight" <?= ($r['options']['appearance']['out-animation'] === 'bounceOutRight' ? 'selected' : ''); ?>>bounceOutRight</option>
                                            <option value="bounceOutUp" <?= ($r['options']['appearance']['out-animation'] === 'bounceOutUp' ? 'selected' : ''); ?>>bounceOutUp</option>
                                        </optgroup>

                                        <optgroup label="Fading Exits">
                                            <option value="fadeOut" <?= ($r['options']['appearance']['out-animation'] === 'fadeOut' ? 'selected' : ''); ?>>fadeOut</option>
                                            <option value="fadeOutDown" <?= ($r['options']['appearance']['out-animation'] === 'fadeOutDown' ? 'selected' : ''); ?>>fadeOutDown</option>
                                            <option value="fadeOutDownBig" <?= ($r['options']['appearance']['out-animation'] === 'fadeOutDownBig' ? 'selected' : ''); ?>>fadeOutDownBig</option>
                                            <option value="fadeOutLeft" <?= ($r['options']['appearance']['out-animation'] === 'fadeOutLeft' ? 'selected' : ''); ?>>fadeOutLeft</option>
                                            <option value="fadeOutLeftBig" <?= ($r['options']['appearance']['out-animation'] === 'fadeOutLeftBig' ? 'selected' : ''); ?>>fadeOutLeftBig</option>
                                            <option value="fadeOutRight" <?= ($r['options']['appearance']['out-animation'] === 'fadeOutRight' ? 'selected' : ''); ?>>fadeOutRight</option>
                                            <option value="fadeOutRightBig" <?= ($r['options']['appearance']['out-animation'] === 'fadeOutRightBig' ? 'selected' : ''); ?>>fadeOutRightBig</option>
                                            <option value="fadeOutUp" <?= ($r['options']['appearance']['out-animation'] === 'fadeOutUp' ? 'selected' : ''); ?>>fadeOutUp</option>
                                            <option value="fadeOutUpBig" <?= ($r['options']['appearance']['out-animation'] === 'fadeOutUpBig' ? 'selected' : ''); ?>>fadeOutUpBig</option>
                                        </optgroup>

                                        <optgroup label="Rotating Exits">
                                            <option value="rotateOut" <?= ($r['options']['appearance']['out-animation'] === 'rotateOut' ? 'selected' : ''); ?>>rotateOut</option>
                                            <option value="rotateOutDownLeft" <?= ($r['options']['appearance']['out-animation'] === 'rotateOutDownLeft' ? 'selected' : ''); ?>>rotateOutDownLeft</option>
                                            <option value="rotateOutDownRight" <?= ($r['options']['appearance']['out-animation'] === 'rotateOutDownRight' ? 'selected' : ''); ?>>rotateOutDownRight</option>
                                            <option value="rotateOutUpLeft" <?= ($r['options']['appearance']['out-animation'] === 'rotateOutUpLeft' ? 'selected' : ''); ?>>rotateOutUpLeft</option>
                                            <option value="rotateOutUpRight" <?= ($r['options']['appearance']['out-animation'] === 'rotateOutUpRight' ? 'selected' : ''); ?>>rotateOutUpRight</option>
                                        </optgroup>

                                        <optgroup label="Sliding Exits">
                                            <option value="slideOutUp" <?= ($r['options']['appearance']['out-animation'] === 'slideOutUp' ? 'selected' : ''); ?>>slideOutUp</option>
                                            <option value="slideOutDown" <?= ($r['options']['appearance']['out-animation'] === 'slideOutDown' ? 'selected' : ''); ?>>slideOutDown</option>
                                            <option value="slideOutLeft" <?= ($r['options']['appearance']['out-animation'] === 'slideOutLeft' ? 'selected' : ''); ?>>slideOutLeft</option>
                                            <option value="slideOutRight" <?= ($r['options']['appearance']['out-animation'] === 'slideOutRight' ? 'selected' : ''); ?>>slideOutRight</option>
                                        </optgroup>

                                        <optgroup label="Zoom Exits">
                                            <option value="zoomOut" <?= ($r['options']['appearance']['out-animation'] === 'zoomOut' ? 'selected' : ''); ?>>zoomOut</option>
                                            <option value="zoomOutDown" <?= ($r['options']['appearance']['out-animation'] === 'zoomOutDown' ? 'selected' : ''); ?>>zoomOutDown</option>
                                            <option value="zoomOutLeft" <?= ($r['options']['appearance']['out-animation'] === 'zoomOutLeft' ? 'selected' : ''); ?>>zoomOutLeft</option>
                                            <option value="zoomOutRight" <?= ($r['options']['appearance']['out-animation'] === 'zoomOutRight' ? 'selected' : ''); ?>>zoomOutRight</option>
                                            <option value="zoomOutUp" <?= ($r['options']['appearance']['out-animation'] === 'zoomOutUp' ? 'selected' : ''); ?>>zoomOutUp</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    globalInitForm();
</script>
</body>
</html>
$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();
    window.visual = $('#content').visualBuilder({exclude_blocks: ['global-blocks'], uploads: 'POPUP'});

    $('#disable_conditions').on('ifToggled', function () {
        if($(this).is(':checked')) {
            $('.sync_query_container').hide();
        } else {
            $('.sync_query_container').show();
        }
    });

    $('#is_active').on('ifToggled', function () {
        if($(this).is(':checked')) {
            $('.only_for_active').show();
            $('.only_for_inactive').hide();
        } else {
            $('.only_for_active').hide();
            $('.only_for_inactive').not('.without_data').show();
        }
    }).trigger('ifToggled');

    var settings = {
        'height': '34px',
        'width': '100%',
        'interactive': true,
        'defaultText': 'Aggiungi',
        'removeWithBackspace': true,
        'minChars': 3,
        'onAddTag': function (value) {},
        'autocomplete_url': "ajax/getWebsiteLinks.php",
        'autocomplete': {selectFirst:true, width:'100px', autoFill:true}
    };

    $('.linkAutocomplete').tagsInput(settings);

    initQueryBuilder();

    $('#behavior').on('change', function (e) {
        $('.show_behavior_box').hide();
        $('.show_behavior_box[data-type="' + $(this).val() + '"]').show();
    });
}

function initQueryBuilder() {

    var queryFilters = JSON.parse($('#query_filters').val());
    var rules = JSON.parse($('#sync_settings').val());

    window.displaySyncError = false;

    $('#sync_query').queryBuilder({
        plugins: {
            'bt-tooltip-errors': {},
            'bt-selectpicker': {
                container: '#automationContainer'
            },
            'filter-description': {}
        },
        lang_code: 'it',
        lang: {
            operators: {
                received: 'tracciato',
                with_values: 'inviato con il valore',
            }
        },
        operators: JSON.parse($('#query_operators').val()),
        filters: queryFilters
    });

    try {
        if (Object.keys(rules).length) {
            $('#sync_query').queryBuilder('setRules', rules);
        }
    } catch(e) {}
}

function getOptions() {

    var syncOptions = {
        appearance: {
            'max-width': $('#max-width').val(),
            'in-animation': $('#in_animation').val(),
            'out-animation': $('#out_animation').val(),
            'overlay': $('#overlay').val(),
        }
    };

    if($('#is_active:checked').length) {
        syncOptions = Object.assign(syncOptions, {  pages_to_include: $('#links_to_include').val(),
            pages_to_exclude: $('#links_to_exclude').val(),
            conditions: [],
            behavior: {
                mode: $('#behavior').val(),
                force_showing: $('#force_showing:checked').length,
            }
        });

        if($('#behavior').val() === 'immediately') {
            syncOptions.behavior.wait = $('.show_behavior_box[data-type="immediately"] [name="wait"]').val();
        } else if($('#behavior').val() === 'on_exit') {
            syncOptions.behavior.wait = $('.show_behavior_box[data-type="on_exit"] [name="wait"]').val();
        }

        if (!$("#disable_conditions:checked").length) {
            var optionsValues = [];
            var is_valid = false;
            try {
                is_valid = $("#sync_query").queryBuilder("validate");
            } catch (e) {
            }

            if (is_valid) {
                optionsValues = $("#sync_query").queryBuilder("getRules");
            }

            if (!Object.keys(optionsValues).length) {
                optionsValues = '0';
            }

            syncOptions['conditions'] = optionsValues;
        }
    }

    return syncOptions;
}

function moduleSaveFunction() {
    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pTitolo": $('#titolo').val(),
            "pContent": $('#content').val(),
            "pOptions": getOptions(),
            "pActive": $('#is_active:checked').length,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {

                if(data.id.length) {
                    $('#jsCode').closest('.without_data').removeClass('without_data');
                    $('#jsCode').html('initMSPopup(' + data.id + ')');
                    $('#is_active').trigger('ifToggled');
                }

                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
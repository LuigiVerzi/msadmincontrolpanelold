<?php
require_once('../../../../sw-config.php');

if(!file_exists(CUSTOMER_DOMAIN_INFO['path'] . 'sitemap.xml')) {
    file_get_contents($MSFrameworkCMS->getURLToSite(1) . '/MSFramework/cron/Produzione/sitemap/generateSitemap.php?from_acp');
}

$all_urls = json_decode(json_encode(simplexml_load_file(CUSTOMER_DOMAIN_INFO['path'] . 'sitemap.xml')), TRUE);

$query = $_GET['term'];

$return_urls = array();

if($all_urls && $all_urls['url']) {
    foreach ($all_urls['url'] as $url) {
        if (stristr($url['loc'], $query)) {
            $return_urls[] = array(
                'id' => $url['loc'],
                'label' => $url['loc'],
                'value' => $url['loc']
            );
        }
    }
}

echo json_encode($return_urls);
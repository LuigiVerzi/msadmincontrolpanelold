<?php
/**
 * MSAdminControlPanel
 * Date: 25/10/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM popup") as $r) {
    $array['data'][] = array(
        $r['id'],
        $MSFrameworki18n->getFieldValue($r['titolo']),
        '<span class="label label-' . ($r['active'] == '1' ? 'primary' : 'inverse') . '">' . ($r['active'] == '1' ? 'Automatica' : 'Manuale') . '</span>',
        array("display" => date("d/m/Y H:i", strtotime($r['creation_date'])), "sort" => strtotime($r['creation_date'])),
        "DT_RowId" => $r['id']
    );
}

if(!is_array($array['data'])) {
    $array = array("data" => "");
}

echo json_encode($array);
?>

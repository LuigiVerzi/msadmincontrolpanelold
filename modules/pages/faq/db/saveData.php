<?php
/**
 * MSAdminControlPanel
 * Date: 06/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;
if($_POST['pNome'] == "" || !$_POST['pQuestions']) {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT nome, descrizione, questions FROM faq WHERE id = :id", array(":id" => $_POST['pID']), true);

    $old_questions = json_decode($r_old_data['questions'], true);

    foreach($old_questions as $old_field_v) {
        foreach($old_field_v[2] as $file) {
            $old_img[] = $file;
        }
    }
}

$ary_files = array();
foreach($_POST['pQuestions'] as $k_question => $cur_question) {

    // Controllo che il campo non sia vuoto
    if(empty($cur_question[0]) || empty($cur_question[1])) {
        echo json_encode(array("status" => "mandatory_data_missing"));
    }

    //controlli di sicurezza per le traduzioni
    if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $cur_question[0], 'oldValue' => $old_questions[$k_question][0]),
        array('currentValue' => $cur_question[1], 'oldValue' => $old_questions[$k_question][1]),
    )) == false) {
        echo json_encode(array("status" => "no_datalang_for_primary"));
        die();
    }

    $_POST['pQuestions'][$k_question][0] = $MSFrameworki18n->setFieldValue($old_questions[$k_question][0], $cur_question[0]);
    $_POST['pQuestions'][$k_question][1] = $MSFrameworki18n->setFieldValue($old_questions[$k_question][1], $cur_question[1]);

    $uploader = new \MSFramework\uploads('FAQ');
    $cur_files = $uploader->prepareForSave($cur_question[2], array("png", "jpeg", "jpg", "svg"));

    if($cur_files === false) {
        echo json_encode(array("status" => "mv_error"));
        die();
    }

    $ary_files = array_merge($ary_files, $cur_files);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
        array('currentValue' => $_POST['pDescrizione'], 'oldValue' => $r_old_data['descrizione']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "descrizione" => $MSFrameworki18n->setFieldValue($r_old_data['descrizione'], $_POST['pDescrizione']),
    "is_active" => $_POST['pIsActive'],
    "questions" => json_encode($_POST['pQuestions']),
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO faq ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE faq SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    if($db_action == "insert") {
        $uploader->unlink($ary_files);
    }

    echo json_encode(array("status" => "query_error"));
    die();
}

if($db_action == "update") {
    $uploader->unlink($ary_files, $old_img);
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();
?>
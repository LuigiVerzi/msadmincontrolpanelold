<?php
/**
 * MSAdminControlPanel
 * Date: 06/07/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$images_to_delete = array();
foreach((new \MSFramework\faq())->getFAQDetails($_POST['pID']) as $r) {
    foreach($r['questions'] as $old_field_v) {
        $images_to_delete = array_merge_recursive($images_to_delete, $old_field_v[2]);
    }
}

if($MSFrameworkDatabase->deleteRow("faq", "id", $_POST['pID'])) {
    (new \MSFramework\uploads('FAQ'))->unlink($images_to_delete);
    echo json_encode(array("status" => "ok"));
} else {
    echo json_encode(array("status" => "query_error"));
}
?>


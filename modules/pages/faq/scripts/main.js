$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    if(!$('.questionsDuplication').hasClass('no_edit')) {
        $('.rowDuplication').easyRowDuplication({
            "addButtonClasses": "btn btn-primary",
            "deleteButtonClasses": "btn btn-warning",
            "addButtonText": "Aggiungi domanda",
            "deleteButtonText": "Elimina domanda",
            "afterAddCallback": function () {
                $('.rowDuplication:last').find('.question').val('').attr('name', 'question_' + $('.rowDuplication').length);
                $('.rowDuplication:last').find('.answer').val('').attr('name', 'answer_' + $('.rowDuplication').length);

                $('.rowDuplication:last').find('.prev_upl_image').html('');
                $('.rowDuplication:last').find('.image_container .clone_item').remove();
                $('.rowDuplication:last').find('.image_container [id$="DDArea"]').remove();
                $('.rowDuplication:last').find('.image_container').append('<div id="image_x" class="image" orakuploader="on"></div>');

                initUploaders();
            },
            "afterDeleteCallback": function () {
            }
        });
    }

    initUploaders();

}

function initUploaders() {
    $('.image_container').each(function(index) {
        if($('.image_container:eq(' + index + ')').find('div[id$="DDArea"]').length != 0) {
            return true;
        }

        $('.image_container:eq(' + index + ') .image').attr('id', 'image_' + index);
        $('.image_container:eq(' + index + ') .image_realPath').attr('id', 'image_' + index + "_realPath");

        preattach_array_image = new Array();
        if($('.prev_upl_image:eq(' + index + ')').val() != "") {
            img_to_cycle = $.parseJSON($('.prev_upl_image:eq(' + index + ')').val()) || new Array();
            $(img_to_cycle).each(function(k, v) {
                preattach_array_image.push(v);
            })
        }

        initOrak('image_' + index, 1, 200, 200, 200, preattach_array_image, false, ['image/jpeg', 'image/png', 'image/svg+xml']);
    })
}

function moduleSaveFunction() {
    questions_ary = new Object();
    $('.stepsDuplication.questionsDuplication').each(function(row_index) {
        questions_ary_tmp = new Array();

        question_val = $(this).find('.question').val();
        answer_val = $(this).find('.answer').val();
        if(question_val == "" || answer_val == "") {
            return true;
        }

        images_ary = new Array();
        $(this).find('input[name^=image]').each(function(k, v) {
            images_ary.push($(this).val());
        })

        questions_ary_tmp.push($(this).find('.question').val());
        questions_ary_tmp.push($(this).find('.answer').val());
        questions_ary_tmp.push(images_ary);

        questions_ary[row_index] = questions_ary_tmp;
    })

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pDescrizione": $('#descrizione').val(),
            "pIsActive": $('#is_active:checked').length,
            "pQuestions": questions_ary,
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            } else if(data.status == "mv_error") {
                bootbox.error("C'è stato un problema durante il salvataggio delle immagini. Impossibile continuare.");
            }  else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    })
}
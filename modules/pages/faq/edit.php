<?php
/**
 * MSAdminControlPanel
 * Date: 06/07/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM faq WHERE id = :id", array($_GET['id']), true);
}

$using_primary_language = $MSFrameworki18n->getPrimaryLangId() == $MSFrameworki18n->getCurrentLanguageDetails()['id'];
?>
<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Generali</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Nome Sezione*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control msShort required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>" placeholder="Es. Spedizioni, Metodi di Pagamento, Resi & Rimborsi, ecc.">
                                </div>

                                <div class="col-sm-4">
                                    <label>Descrizione Sezione</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['descrizione']) ?>"></i></span>
                                    <textarea id="descrizione" name="descrizione" class="form-control msShort"><?php echo htmlentities($MSFrameworki18n->getFieldValue($r['descrizione'])) ?></textarea>
                                </div>

                                <?php
                                $is_active_check = "checked";
                                if($r['is_active'] == 0 && $_GET['id'] != "") {
                                    $is_active_check = "";
                                }
                                ?>
                                <div class="col-sm-4">
                                    <label>&nbsp;</label>
                                    <div class="styled-checkbox form-control">
                                        <label style="width: 100%;">
                                            <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                <input type="checkbox" id="is_active" <?php echo $is_active_check ?>>
                                                <i></i>
                                            </div>
                                            <span style="font-weight: normal;">Attivo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="easyRowDuplicationContainer">
                                <?php
                                $questions = json_decode($r['questions'], true);
                                if(count($questions) == 0 && $using_primary_language) {
                                    $questions[] = array();
                                }
                                ?>

                                <?php foreach($questions as $addPhK => $addPhV) { ?>
                                    <div class="stepsDuplication rowDuplication questionsDuplication <?= (!$using_primary_language ? 'no_edit' : ''); ?>">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Domanda*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($addPhV[0]) ?>"></i></span>
                                                <input type="text" class="form-control question msShort required" name="question_<?= $addPhK; ?>" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($addPhV[0])) ?>" required>
                                            </div>

                                            <div class="col-sm-5">
                                                <label>Risposta*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($addPhV[1]) ?>"></i></span>
                                                <textarea class="form-control answer msShort required" name="answer_<?= $addPhK; ?>" required><?php echo $MSFrameworki18n->getFieldValue($addPhV[1]) ?></textarea>
                                            </div>

                                            <div class="col-sm-3">
                                                <?php
                                                (new \MSFramework\uploads('FAQ'))->initUploaderHTML("image_" . $addPhK, json_encode($addPhV[2]), array("realPath" => "image_realPath", "prevUploaded" => "prev_upl_image", "element" => "image", "container" => "image_container"));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if(!$using_primary_language) { ?>
                                    <div class="hr-line-dashed" style="margin-top: 10px;"></div>
                                    <div class="alert alert-warning">Per inserire nuove FAQ utilizza la lingua madre, successivamente potrai modificarne le traduzioni da qui.</div>
                                <?php } ?>

                            </div>


                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<script>
    
    globalInitForm();
</script>
</body>
</html>
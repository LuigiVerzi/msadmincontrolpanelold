<?php
/**
 * MSAdminControlPanel
 * Date: 13/10/18
 */

require_once('../../../../sw-config.php');
require('../config/moduleConfig.php');

$db_action = $MSFrameworkDatabase->checkInsertOrUpdate($_POST['pID']);

$can_save = true;
if($_POST['pNome'] == "") {
    $can_save = false;
}

if(!$can_save) {
    echo json_encode(array("status" => "mandatory_data_missing"));
    die();
}

if($db_action == "update") {
    $r_old_data = $MSFrameworkDatabase->getAssoc("SELECT nome FROM menu WHERE id = :id", array(":id" => $_POST['pID']), true);
}

//controlli di sicurezza per le traduzioni
if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
        array('currentValue' => $_POST['pNome'], 'oldValue' => $r_old_data['nome']),
    )) == false) {
    echo json_encode(array("status" => "no_datalang_for_primary"));
    die();
}

foreach(getFlatMenuWithParentsKeys($_POST['pFinalList']) as $flat_voice) {

    $menu_value = $flat_voice[0];
    $menu_keys = $flat_voice[1];

    if($menu_value['type'] == 'custom') {

        if($MSFrameworki18n->checkIfPrimaryHasValueForField(array(
                array('currentValue' => $menu_value['title'], 'oldValue' => json_encode($menu_value['multilang_title'])),
            )) == false) {
            echo json_encode(array("status" => "no_datalang_for_primary"));
            die();
        }

        $path_eval = '';
        if(count($menu_keys) > 1) {
            $tmp_key = $menu_keys[0];
            unset($menu_keys[0]);
            $path_eval = $tmp_key . "]['children'][" . implode("'children'][", $menu_keys);
        } else {
            $path_eval = implode('][', $menu_keys);
        }

        $multilang_title = $MSFrameworki18n->setFieldValue(json_encode($menu_value['multilang_title']), $menu_value['title']);
        $key_path = '$_POST[\'pFinalList\'][' . $path_eval .'][\'multilang_title\']';

        eval($key_path . ' = $multilang_title;');
    }
}

$array_to_save = array(
    "nome" => $MSFrameworki18n->setFieldValue($r_old_data['nome'], $_POST['pNome']),
    "list" => json_encode($_POST['pFinalList'])
);

$stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);
if($db_action == "insert") {
    $result = $MSFrameworkDatabase->pushToDB("INSERT INTO menu ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
} else {
    $result = $MSFrameworkDatabase->pushToDB("UPDATE menu SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $_POST['pID']), $stringForDB[0]));
}

if(!$result) {
    echo json_encode(array("status" => "query_error"));
    die();
}

echo json_encode(array("status" => "ok", "id" => ($db_action == 'insert' ? $MSFrameworkDatabase->lastInsertId() : '')));
die();


function getFlatMenuWithParentsKeys($ary_menu, $parent_keys = array()) {

    $flat_menu = array();

    foreach($ary_menu as $key => $cur_menu) {

        $children = $cur_menu['children'];
        unset($cur_menu['children']);

        $current_keys = $parent_keys;
        $current_keys[] = $key;

        $flat_menu[] = array($cur_menu, $current_keys);

        if(is_array($children)) {
           $flat_menu = array_merge($flat_menu, getFlatMenuWithParentsKeys($children, $current_keys));
        }
    }

    return $flat_menu;

}

?>

$(document).ready(function() {
    loadStandardDataTable('main_table_list');
    allowRowHighlights('main_table_list');
    manageGridButtons('main_table_list');
});

function initForm() {
    initClassicTabsEditForm();

    $('.availList, #finalList').nestable();

    $('#finalList').on('click', '.dd-handle', function (e) {
        if(!$(e.target).hasClass('delete')) {
            editMenuItem($(this).closest('.dd-item'));
        }
    });

    $('#custom_menu_form').on('click', '#custom_menu_submit', function (e) {
       e.preventDefault();

       var $form = $(this).closest('.form_box');

        $form.find('.has-error').removeClass('error');

        $form.find('input').each(function () {
            if(!$(this).val().length) {
                $(this).parent().addClass('has-error');
                $(this).one('focus keypress', function () {
                   $(this).parent().removeClass('has-error');
                });
            }
        });

        var titolo = $('#custom_menu_title').val();
        var link = $('#custom_menu_link').val();

        if(!$form.find('.has-error').length) {

            if($('#finalList').find('.dd-empty').length) {
                $('#finalList').html('<ol class="dd-list"></ol>');
            }

            $('#finalList > ol').append('<li class="dd-item custom" data-title="' + titolo + '" data-link="' + link + '" data-type="custom"><div class="dd-handle"><b>CUSTOM</b> <span>' + titolo + '</span> <a class="edit"><i class="fa fa-angle-down"></i></a> <a class="delete" onclick="$(this).closest(\'li\').remove()">Rimuovi</a></div></li>');

            $form.find('input').val('');

        }

    });

}

function moduleSaveFunction() {

    if($('.menu_options').length) {
        editMenuItem($('.menu_options').closest('.dd-item'));
    }

    $.ajax({
        url: "db/saveData.php",
        type: "POST",
        data: {
            "pID": $('#record_id').val(),
            "pNome": $('#nome').val(),
            "pFinalList": $('#finalList').nestable('serialize'),
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "mandatory_data_missing") {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "no_datalang_for_primary") {
                bootbox.error("Per compiere questa operazione, devi prima compilare gli stessi campi per la lingua principale");
            }  else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso salvare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "ok") {
                succesModuleSaveFunctionCallback(data);
            }
        }
    });
}

function removeMenuItem (obj) {
    var $item = $(obj).closest('.dd-item');
    var $item_ol = $item.closest('.dd-list');
    var type = $item.data('type');

    var $destination = $('.availList.' + type);

    if($destination.find('.dd-empty').length) {
        $destination.html('<ol class="dd-list"></ol>');
    }

    $item.appendTo($destination.find('ol'));

    if(!$item_ol.find('li').length) {
        $item_ol.replaceWith("<div class=\"dd-empty\"></div>");
    }

}

function editMenuItem ($item) {

    var settings = (typeof($item.data('settings')) !== 'undefined' ? $item.data('settings') : {});
    var main_settings = $item.data();

    if($item.find('> .menu_options').length) {
        $item.find('> .menu_options').remove();
        $item.find('> .dd-handle .edit i').attr('class', 'fa fa-angle-down');
    } else {

        $item.find('> .dd-handle .edit i').attr('class', 'fa fa-angle-up');

        var settings_template = $('#menu_options_template').val();
        $item.find('> .dd-handle').after(settings_template);

        var $options = $item.find('> .menu_options');

        $options.find('[data-for-type]').each(function () {
            if($(this).data('for-type') !== $item.data('type')) {
                $(this).remove();
            }
        });

        $options.find('[name]').each(function () {

            var name = $(this).attr('name');
            var field_type_data = ($(this).hasClass('main_settings') ? main_settings : settings);

            if(typeof(field_type_data[name]) !== 'undefined') {

                if($(this)[0].tagName === 'INPUT') {
                    if($(this).attr('type') == 'checkbox' && field_type_data[name] == 1) {
                        $(this).prop('checked', true);
                    } else {
                        $(this).val(field_type_data[name])
                    }
                } else {
                    $(this).val(field_type_data[name])
                }

                if($(this).parent().find('[data-translations]').length) {
                    $(this).parent().find('[data-translations]').attr('data-translations', JSON.stringify(field_type_data['multilang_' + name]));
                }

            }

        });

        initIChecks();
        initMSTooltip();
        initMSShortcodeBtns();
        prepareFormLabels($options);
    }
}

function saveMenuOptions($item) {
    var settings = {};
    var main_settings = $item.data();

    $item.find('> .menu_options [name]').each(function () {
        var name = $(this).attr('name');
        var value = '';

        if($(this)[0].tagName === 'INPUT') {
            if($(this).attr('type') === 'checkbox') {
                value = $(this).prop('checked') ? 1 : 0;
            } else {
                value = $(this).val();
            }
        } else {
            value = $(this).val();
        }

        if($(this).hasClass('main_settings')) {
            main_settings[name] = value;
        } else {
            settings[name] = value;
        }

        if($(this).parent().find('[data-translations]').length) {

            var multilang_json = $(this).parent().find('[data-translations]').data('translations');

            if(typeof(multilang_json) !== 'object') {
                multilang_json = {};
            }

            multilang_json[$("#currentLangCode").val()] = value;

            if($(this).hasClass('main_settings')) {
                main_settings['multilang_' + name] = multilang_json;
            } else {
                settings['multilang_' + name] = multilang_json;
            }
        }
    });

    if(main_settings.type === 'custom' && !main_settings.title.length) {
        $item.find('> .menu_options [name="title"]').addClass('error').one('focus keypress', function () {
           $(this).removeClass('error');
        });
        return false;
    }

    $item.data('settings', settings);

    Object.keys(main_settings).forEach(function(data_key) {
        $item.data(data_key, main_settings[data_key]);
    });

    if(!$item.data('title').length && typeof($item.data('original_title')) !== 'undefined') {
        $item.find('> .dd-handle > span').text($item.data('original_title'));
    } else {
        $item.find('> .dd-handle > span').text($item.data('title'));
    }

    toastr['success']('Menù aggiornato');

    editMenuItem($item);
}
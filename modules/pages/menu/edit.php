<?php
/**
 * MSAdminControlPanel
 * Date: 21/02/18
 */

require_once('../../../sw-config.php');
require('config/moduleConfig.php');
require(ABSOLUTE_SW_PATH . "includes/template/header.php");

if(isset($_GET['id'])) {
    $r = $MSFrameworkDatabase->getAssoc("SELECT * FROM menu WHERE id = :id", array($_GET['id']), true);
}

$menuClass = new \MSFramework\menu($_GET['id']);

function printVoiceStructure($page_type, $page_info, $list = array()) {

    if(!$list) {
        $list = $page_info['list'];
    }

    $html = '<ol class="dd-list">';

    foreach($list as $pagina) {
        $html .= '<li class="dd-item" data-id="' . $pagina['id'] . '" data-type="' . $page_type . '">';
        $html .= '<div class="dd-handle"><b>' . $page_info['name'] . '</b> <span>' . $pagina['name'] .'</span><a class="edit"><i class="fa fa-gear"></i></a> <a class="delete" onclick="removeMenuItem(this)">Rimuovi</a></div>';

        if (is_array($pagina['children'])) {
            $html .= printVoiceStructure($page_type, $page_info, $pagina['children']);
        }

        $html .= '</li>';
    }

    $html .= '</ol>';

  return $html;
}
?>

<style>


    .dd-handle {
        color: inherit !important;
        border: 1px dashed #e7eaec;
        background: #f3f3f4 !important;
        padding: 10px;
        height: 40px;
    }

    .dd-handle b {
        font-size: 10px;
        font-weight: 500;
        background: rgba(0, 0, 0, 0.75);
        border: 1px solid black;
        padding: 3px 5px;
        line-height: 1.3;
        color: white;
        border-radius: 3px;
        margin-right: 5px;
    }

    [data-type="custom"] > .dd-handle > b {
        background: rgb(57, 151, 192);
        border: 1px solid #168abc;
    }

    .dd-handle a {
        float: right;
        font-size: 11px;
        color: #8d8d8d;
        font-weight: 500;
        opacity: 0.6;
        border: 1px solid;
        padding: 0px 5px;
        border-radius: 3px;
        margin-left: 5px;
    }

    .dd-handle a:hover {
        opacity: 1;
        text-decoration: none;
    }

    .dd-handle a.delete:hover {
        color: #db1d31;
    }

    .dd-handle a.edit:hover {
        color: #242424;
    }

    .dd-item > button {
        font-family: 'Font Awesome 5 Free';
        height: 34px;
        width: 33px;
        color: #c1c1c1;
    }
    .dd-item > button:before {
        content: "\f067";
    }
    .dd-item > button[data-action="collapse"]:before {
        content: "\f068";
    }
    .dd-list .delete, .dd-list .edit {
        display: none;
    }

    #finalList .dd-list .delete,
    #finalList .dd-list .edit {
        display: inline-block;
    }

    .dd-item .menu_options {
        background: #fafafa;
        padding: 15px;
        margin: -6px 0 5px;
        border: 1px dashed #e7eaec;
        border-top: 4px solid #f3f3f4;
        border-radius: 0 0 3px 3px;
    }

    .dd-item .menu_options .form-control {
        margin-bottom: 10px;
    }

    .dd-item .menu_options label {
        font-size: 12px;
        margin: 0 0 5px;
    }

</style>

<body class="skin-1 fixed-sidebar pace-done">

<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg">
        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/breadcrumbs.php"); ?>
            </div>

            <div class="col-sm-8">
                <?php require(ABSOLUTE_SW_PATH . "includes/template/toolbarButtons/standardEdit.php"); ?>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <input type="hidden" id="record_id" value="<?php echo $_GET['id'] ?>" />

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" id="form" action="#" class="form-horizontal wizard-big">

                        <h1>Ordinamento menu</h1>
                        <fieldset>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nome Menu*</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations="<?php echo htmlentities($r['nome']) ?>"></i></span>
                                    <input id="nome" name="nome" type="text" class="form-control required" value="<?php echo htmlentities($MSFrameworki18n->getFieldValue($r['nome'])) ?>">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="panel-group" id="accordion">

                                        <?php foreach($menuClass->getMenuVoices() as $page_type => $page_info) { ?>

                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#menu_<?= $page_type; ?>"><?= $page_info['name']; ?></a>
                                                    </h5>
                                                </div>
                                                <div id="menu_<?= $page_type; ?>" class="panel-collapse collapse <?= ($page_type == 'pagina' ? 'in' : ''); ?>">
                                                    <div class="panel-body">

                                                        <div class="dd availList <?= $page_type; ?>" id="availList<?= $page_type; ?>">
                                                            <?php echo printVoiceStructure($page_type, $page_info); ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        <?php } ?>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h5 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#menu_custom" id="menu_custom_toggle">Personalizzato</a>
                                                </h5>
                                            </div>
                                            <div id="menu_custom" class="panel-collapse collapse">
                                                <div class="panel-body">

                                                    <div id="custom_menu_form">
                                                        <div class="custom_menu_add form_box">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <input type="text" placeholder="Inserisci il titolo" id="custom_menu_title" class="form-control msShort">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" placeholder="Inserisci un link" id="custom_menu_link" class="form-control msShort">
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <a href="#" class="btn btn-block btn-primary" id="custom_menu_submit">Aggiungi Link</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <?php
                                function composeMenuOrder($ary_menu) {
                                    Global $menuClass;

                                    $menuVoices = (new \MSFramework\menu())->getMenuVoices();

                                    if(!is_array($ary_menu)) {
                                        return "";
                                    }

                                    $html = '<ol class="dd-list">';

                                    foreach($ary_menu as $cur_menu) {

                                        $cur_menu['settings'] = (isset($cur_menu['settings']) && is_array($cur_menu['settings']) ? $cur_menu['settings'] : array());

                                        $data_menu = array(
                                            'id' => $cur_menu['id'],
                                            'type' => $cur_menu['type'],
                                            'settings' => json_encode($cur_menu['settings'])
                                        );

                                        $link_to_append = '<a class="delete" onclick="removeMenuItem(this)">Rimuovi</a>';

                                        $menu_info = $menuClass->getMenuVoiceDetails($cur_menu);
                                        $data_menu['title'] = $menu_info['title'];
                                        $data_menu['multilang_title'] = $menu_info['multilang_title'];
                                        $data_menu['original_title'] = $menu_info['original_title'];

                                        if($cur_menu['type'] == 'custom') {
                                            $data_menu['multilang_title']  = $cur_menu["multilang_title"];
                                            $data_menu['link']  = $cur_menu["link"];
                                            unset($data_menu['id']);
                                            $link_to_append = '<a class="delete" onclick="$(this).closest(\'li\').remove()">Rimuovi</a>';
                                        }

                                        $link_to_append = '<a class="edit"><i class="fa fa-angle-down"></i></a> ' . $link_to_append;

                                        $data_html = array();
                                        foreach($data_menu as $data_key => $data_value) {
                                            $data_html[] = 'data-' . $data_key . '="' . htmlentities($data_value) . '"';
                                        }

                                        $html .= '<li class="dd-item" ' . implode(' ', $data_html) . '><div class="dd-handle"><b>' . $menuVoices[$data_menu['type']]['name'] . '</b> <span>' . $data_menu['title'] . '</span>' . $link_to_append . '</div>';

                                        if (is_array($cur_menu['children'])) {
                                            $html .= composeMenuOrder($cur_menu['children']);
                                        }

                                        $html .= "</li>";

                                    }

                                    $html .= "</ol>";

                                    return $html;
                                }
                                ?>

                                <div class="col-md-8">
                                    <div class="col-attached-right">
                                        <h2 class="title-divider" style="margin-top: 0;">Ordinamento Menu</h2>
                                        <div class="dd" id="finalList">
                                            <?php echo composeMenuOrder(json_decode($r['list'], true)) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>

    </div>
</div>

<textarea id="menu_options_template" style="display: none;">
    <div class="menu_options">
        <div class="row">

            <div class="col-md-6">
                <label>Etichetta</label>  <span class="ms-label-tooltip m-l-sm"><i class="fa fa-language" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo TRANSLATE_ICON_TOOLTIP ?>" data-translations=""></i></span>
                <input name="title" type="text" class="form-control main_settings translation" value="">
            </div>

            <div class="col-md-6">
                <label>Classe CSS</label>
                <input name="css" type="text" class="form-control" value="">
            </div>

            <div class="col-md-12" data-for-type="custom">
                <label>URL</label>
                <input name="link" type="text" class="form-control main_settings msShort" value="">
            </div>

            <div class="col-md-12">
                <div class="hr-line-dashed" style="margin: 0 0 10px;"></div>
            </div>

             <div class="col-md-6">
                <a href="javascript:;" class="btn btn-sm btn-success saveMenuOptions" style="margin: 2px 0px 0px;" onclick="saveMenuOptions($(this).closest('.dd-item'));">Salva</a>
            </div>

            <div class="col-md-6">
                <div class="styled-checkbox form-control" style="margin: 0;">
                    <label style="width: 100%;">
                        <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                            <input type="checkbox" name="new_tab" >
                            <i></i>
                        </div>
                        <span style="font-weight: normal;">Apri in un'altra scheda</span>
                    </label>
                </div>
            </div>
        </div>
    </div>
</textarea>

<script>
    globalInitForm();
</script>
</body>
</html>
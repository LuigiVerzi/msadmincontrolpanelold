<?php
define("DO_NOT_CHECK_OFFLINE", true);
$_GET['must_set_lang_data'] = "";

require('sw-config.php');

if(!isset($_GET['do-logout']) && isset($_SESSION['userData']['userlevel'])) {
    header("location: index.php");
    die();
} else {
    header('isLogin: 1');
}

session_destroy();
session_start();

$r_settings = $MSFrameworkCMS->getCMSData('site');
$r_settings_reg = json_decode($r_settings['registration'], true);

$logos = json_decode($r_settings["logos"], true);

if($_POST['username'] != "" && $_POST['password'] != "") {
    $checkCredentials = $MSFrameworkUsers->checkCredentials($_POST['username'], $_POST['password']);
    if($checkCredentials[0]) {
        if($checkCredentials[1] != "saas_superadmin") {
            header("location: " . (isset($_POST['redirect_after']) ? $_POST['redirect_after'] : 'index.php'));
            unset($_SESSION['redirect_after_login']);
        } else {
            header("location: login/saasSelector.php");
        }
        die();
    }
}

$str_error = "";
if($checkCredentials[1] != "") {
    $str_error = $checkCredentials[1];
} else if(isset($_GET['not-logged'])) {
    $str_error = "Attenzione! La sessione è scaduta o non disponi dei privilegi necessari per visualizzare la pagina richiesta.";
} else if(isset($_GET['empty-saas-info'])) {
    $str_error = "Attenzione! Non è stato possibile determinare le informazioni relative al SaaS.";
} else if(isset($_GET['do-logout'])) {
    $MSFrameworkUsers->endUserSession();
}

if($_GET['a'] == "activate-customer") {
    if((new \MSFramework\customers())->activateAccount($_GET['mail_auth'])) {
        $str_confirm = "Il tuo indirizzo email è stato verificato con successo!";
    } else {
        $str_error = "Attenzione! Non è stato possibile attivare questo account.";
    }
}

if(isset($_GET['reset-ok'])) {
    $str_confirm = "La password è stata resettata con successo. Puoi accedere con le nuove credenziali!";
}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo SW_NAME ?> | Login</title>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/favicon.png" />

    <link href="vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="vendor/css/animate.css" rel="stylesheet">
    <link href="vendor/css/style.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/colors-aquamarine.css" rel="stylesheet">

</head>

<body class="login-bg" style="<?= ($logos['login_bg'] != '' ? 'background-image: url(' . htmlentities(UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logos['login_bg']) . ');' : '') ?>">
    <div class="overlay"></div>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div class="logo-name"><img src="<?= $MSFrameworkCMS->getLoginLogoPath(); ?>" alt="MS" class="img-responsive"  onerror="this.onerror=null;this.src='<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/logo.svg';" /></div>
        <div class="login-content">
            <h3>Benvenuto</h3>
            <p>Inserisci i dati per gestire il tuo <?= ($isSaaS ? "account" : "sito") ?>:</p>
            <?php if($str_error != "") { ?>
                <div class="alert alert-danger">
                    <?php echo $str_error ?>
                </div>
            <?php } ?>
            <?php if($str_confirm != "") { ?>
                <div class="alert alert-success">
                    <?php echo $str_confirm ?>
                </div>
            <?php } ?>
            <?php if($_GET['a'] != "activate-customer") { ?>
            <form class="space-30" role="form" action="login.php" method="post">

                <?php
                if(!empty($_SESSION['redirect_after_login'])) {
                    echo '<input type="hidden" name="redirect_after" id="login_referer" value="' . $_SESSION['redirect_after_login'] . '">';
                }
                ?>
                <div class="form-group">
                    <input type="email" class="form-control" name="username" placeholder="Indirizzo email" required="" value="<?= $_POST['username']  ?>">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Entra</button>
                <div><a href="login/resetPassword.php">Password Dimenticata?</a></div>
                <?php
                if($r_settings_reg['enable'] == "1") {
                ?>
                <div><a href="login/register.php" class="btn btn-primary btn-outline block full-width m-t">Registrati</a></div>
                <?php } ?>
            </form>
            <?php } else { ?>
                <script>
                    setTimeout(function () {
                        location.href = '<?= (new \MSFramework\cms())->getURLToSite(); ?>';
                    }, 1000);
                </script>
            <?php } ?>
            <p class="m-t"> <small>Copyright <?php echo SW_NAME ?> &copy; <?php echo date("Y") ?> - All rights reserved</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="vendor/js/jquery-2.1.1.js"></script>
    <script src="vendor/js/bootstrap.min.js"></script>

</body>

</html>

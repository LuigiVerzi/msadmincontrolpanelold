<?php
require_once('../sw-config.php');

$module_id = $_GET['module'];
$module_details = $MSSoftwareModules->getModuleDetailsFromID($module_id);
$title = $_GET['title'];

$module_url = ABSOLUTE_ADMIN_MODULES_PATH_HTML . $module_details['path'] . 'index.php?fromFastEditor&fromFastPicker=1';

if($_GET['source'] && $_GET['source'] != CUSTOMER_DOMAIN_INFO['id']) {
    $module_url = $MSFrameworkFW->getWebsitePathsBy('id', $_GET['source'])['backend_url'] . 'modules/' . $module_details['path'] . 'index.php?fromFastEditor&fromFastPicker=1';
    $module_url = $MSFrameworkUsers->generateGlobalLoginRedirect($module_url);
}

?>

<div class="modal inmodal" id="fastModulePicker" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-<?= $module_details['fa_icon']; ?> modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Seleziona <?= $title; ?></h4>
            </div>
            <div class="modal-body" style="padding: 0;">
                <div class="circlePreloader fastEditorPreloader"><div></div></div>
                <iframe style="display: none;" src="<?= $module_url ?>" id="fastModuleEditorIframe"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="fastModuleEditor_Cancel" data-dismiss="modal">Annulla</button>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * MSAdminControlPanel
 * Date: 30/09/2018
 */

/*
 * Doc Utili:
 * Test users: https://developers.facebook.com/docs/apps/test-users/ (id: 103047300671984, mail: marketing_xcvkqvt_user@tfbnw.net, pass: demo1234)
 * Test pages: https://developers.facebook.com/docs/apps/test-pages (access token: EAAaZBDKrvWR8BABD2JHhgkWrXySnMZCOjZBdfFAoKLZCcwuyBhsPqDk1gf3ZBXeV8QyK1QgccfotefO10Hs4X6kjoRXlHwa80d1pFo67YVSMcDPkm1cFFx0pEBsyawK6s44imhQ9srZABZBytDzJfGijJAPQg8IPw9qqE5eUHwvAeumKO4kd73r2MUJmJZBo0cjiIw452wfmkx3OJDl2ZBtA2VF0yjaJqyFSmfPT0HZC081WTIAbGZBZA83J)
 * ID Pagina: 2260451300908039
 */

if($_GET['auth'] != "kjhi8yJGjytu75GH") {
    die('Accesso negato');
}

require_once('../../../../sw-config.php');

$fb = new \Facebook\Facebook([
    'app_id' => '1897811476961567',
    'app_secret' => 'b6e7967dc2a152736c806bae9093bf0e',
    'default_graph_version' => 'v3.0',
]);


$url = (object) array('cover_photo' => array(
    'url' => "http://vincenzo.marketingstudio.it/MSAdminControlPanel/v1/ajax/socialSharer/facebook/img/fb_page_cover.png",
    'focus_y' => 1.0,
    'focus_x' => 1.0,
    'zoom_scale_x' => 1.0,
    'zoom_scale_y' => 1.0,
));

try {
    $response = $fb->post(
        '/103047300671984/accounts',
        array(
            'access_token' =>'EAAaZBDKrvWR8BABD2JHhgkWrXySnMZCOjZBdfFAoKLZCcwuyBhsPqDk1gf3ZBXeV8QyK1QgccfotefO10Hs4X6kjoRXlHwa80d1pFo67YVSMcDPkm1cFFx0pEBsyawK6s44imhQ9srZABZBytDzJfGijJAPQg8IPw9qqE5eUHwvAeumKO4kd73r2MUJmJZBo0cjiIw452wfmkx3OJDl2ZBtA2VF0yjaJqyFSmfPT0HZC081WTIAbGZBZA83J',
            'name' =>'Marketing Studio Dev Page',
            'picture' => "http://vincenzo.marketingstudio.it/MSAdminControlPanel/v1/ajax/socialSharer/facebook/img/profilo.png",
            'about' => "Marketing Studio Test App",
            'category' => "2211", //software
            'cover_photo' => $url->cover_photo,
        )
    );
} catch(Facebook\Exceptions\FacebookResponseException $e) {
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

$graphNode = $response->getGraphNode();
print_r($graphNode);
?>

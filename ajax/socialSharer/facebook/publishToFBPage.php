<?php
/**
 * MSAdminControlPanel
 * Date: 30/09/2018
 */

require_once('../../../sw-config.php');

$r_site = $MSFrameworkCMS->getCMSData('site');
$fb_page_id = trim($r_site['fb_page_id']);
if(!(new \MSFramework\Sharer\sharer())->canShare($_POST['module_id']) || $_POST['record_id'] == "") {
    die('not_allowed');
}

$fb = new \Facebook\Facebook([
    'app_id' => '1897811476961567',
    'app_secret' => 'b6e7967dc2a152736c806bae9093bf0e',
    'default_graph_version' => 'v3.0',
]);

try {
    $page_token = $fb->get(
        '/' . $fb_page_id . '?fields=access_token',
        $_POST['token']
    );

    $page_token_body = $page_token->getDecodedBody();

    if($page_token_body['access_token'] == "") {
        die('can_not_get_page_access_token');
    }

    foreach($_POST['pictures'] as $picture) {
        $attached_media[] = json_encode(array("media_fbid" =>  $picture));
    }

    $short_url = "";
    if($_POST['include_short_link'] == "1") {
        if($_POST['module_id'] == "blog_articoli") {
            $url = (new \MSFramework\Blog\posts())->getURL($_POST['record_id']);
        } else if($_POST['module_id'] == "ecommerce_prodottiprodotti") {
            $url = (new \MSFramework\Ecommerce\products())->getURL($_POST['record_id']);
        } else if($_POST['module_id'] == "realestate_immobili") {
            $url = (new \MSFramework\RealEstate\immobili())->dettaglioImmobileURL($_POST['record_id']);
        }

        $url = str_replace("/" . ADMIN_FOLDER . "/", (new \MSFramework\cms())->getURLToSite(), $url);

        $short_url = (new \MSFramework\url())->shortenURL($url);
    }

    if($short_url != "") {
        $short_url = "\n\n" . (new \MSFramework\cms())->getURLToSite() . substr($short_url, 1);
    }

    $response = $fb->post(
        '/' . $fb_page_id . '/feed',
        array(
            'message' => $_POST['message'] . $short_url,
            'access_token' => $page_token_body['access_token'],
            'attached_media' => $attached_media
        )
    );
} catch(\Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    //echo 'Graph returned an error: ' . $e->getMessage();
    die('graph_error');
} catch(\Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    //echo 'Facebook SDK returned an error: ' . $e->getMessage();
    die('sdk_error');
}
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 30/09/2018
 */

require_once('../../../sw-config.php');

$r_site = $MSFrameworkCMS->getCMSData('site');
$fb_page_id = trim($r_site['fb_page_id']);
if(!(new \MSFramework\Sharer\sharer())->canShare($_POST['module_id']) || $_POST['record_id'] == "") {
    die('not_allowed');
}

$fb = new \Facebook\Facebook([
    'app_id' => '1897811476961567',
    'app_secret' => 'b6e7967dc2a152736c806bae9093bf0e',
    'default_graph_version' => 'v3.0',
]);

try {
    $page_token = $fb->get(
        '/' . $fb_page_id . '?fields=access_token',
        $_POST['token']
    );

    $page_token_body = $page_token->getDecodedBody();

    if($page_token_body['access_token'] == "") {
        die('can_not_get_page_access_token');
    }

    $response = $fb->post(
        '/' . $fb_page_id . '/photos',
        array(
            'url' => "http:" . $_POST['picture'],
            'caption' => $_POST['message'],
            'published' => false,
            'access_token' => $page_token_body['access_token'],
        )
    );

    $image_id_body = $response->getDecodedBody();
    echo $image_id_body['id'];

} catch(\Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(\Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}
?>

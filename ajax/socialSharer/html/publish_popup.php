<?php
/**
 * MSAdminControlPanel
 * Date: 30/09/2018
 */

require_once('../../../sw-config.php');

if($_POST['record_id'] == "") {
    die('must_select_row');
}

$sharerClass = new \MSFramework\Sharer\sharer();

$images = null;
if($_POST['module_id'] == "blog_articoli") {
    $curModuleClass = new \MSFramework\Blog\posts();
    $images_col_name = "images";
    $img_path = UPLOAD_BLOG_POSTS_FOR_DOMAIN_HTML;

    $r = $curModuleClass->getBlogArticleByID($_POST['record_id']);

    $preloaded_descr = $MSFrameworki18n->getFieldValue($r['titolo'], true) . "&#13;&#10;" . $MSFrameworki18n->getFieldValue($r['descrizione_breve'], true);

    $page_url = $curModuleClass->getURL($_POST['record_id']);
} else if($_POST['module_id'] == "ecommerce_prodottiprodotti") {
    $curModuleClass = new \MSFramework\Ecommerce\products();
    $images_col_name = "gallery";
    $img_path = UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML;

    $r = $MSFrameworkDatabase->getAssoc("SELECT gallery, nome, short_descr FROM ecommerce_products WHERE id = :id", array(":id" => $_POST['record_id']), true);

    $preloaded_descr = $MSFrameworki18n->getFieldValue($r['nome'], true) . "&#13;&#10;" . $MSFrameworki18n->getFieldValue(strip_tags($r['short_descr']), true);

    $page_url = $curModuleClass->getURL($_POST['record_id']);
} else if($_POST['module_id'] == "realestate_immobili") {
    $curModuleClass = new \MSFramework\RealEstate\immobili();
    $images_col_name = "images";
    $img_path = UPLOAD_REALESTATE_IMMOBILI_FOR_DOMAIN_HTML;

    $r = $MSFrameworkDatabase->getAssoc("SELECT images, titolo, descrizione FROM realestate_immobili WHERE id = :id", array(":id" => $_POST['record_id']), true);

    $preloaded_descr = $MSFrameworki18n->getFieldValue($r['titolo'], true) . "&#13;&#10;" . $MSFrameworki18n->getFieldValue(strip_tags($r['descrizione']), true);
    $page_url = $curModuleClass->dettaglioImmobileURL($_POST['record_id']);
}

if(!is_array($r)) {
    die('unable_to_share');
}

$r_images = json_decode($r[$images_col_name], true);
if($_POST['module_id'] == "blog_articoli") {
    $r_images = $r_images['icon'];
}

$images = (count(array_filter($r_images)) == 0 ? null : array_filter($r_images));
$page_url = str_replace("/" . ADMIN_FOLDER . "/", (new \MSFramework\cms())->getURLToSite(), $page_url);

?>

<div id="fb_publish_popup">
    <div class="row">
        <div class="col-sm-12">
            <a href="javascript:void(0)" class="show_el_url">Mostra URL elemento</a>
            <span class="el_url" style="display: none;"><a href="<?= $page_url ?>" target="_blank"><?= $page_url ?></a></span>
        </div>
    </div>

    <?php
    if($sharerClass->canShareOnSocial()) {
    ?>
        <div class="row m-t-md">
            <div class="col-sm-12">
                <textarea id="fb_message" rows="4" style="width: 100%" placeholder="Cosa vuoi scrivere in questo post?"><?= $preloaded_descr ?></textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <label>&nbsp;</label>
                <div class="styled-checkbox form-control">
                    <label style="width: 100%; padding-top: 3px;">
                        <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                            <input type="checkbox" id="fb_add_link">
                            <i></i>
                        </div>
                        <span style="font-weight: normal;">Aggiungi short link</span>
                    </label>
                </div>
            </div>

            <?php
            if(is_array($images)) {
            ?>
            <div class="col-sm-6">
                <label>&nbsp;</label>
                <div class="styled-checkbox form-control">
                    <label style="width: 100%; padding-top: 3px;">
                        <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                            <input type="checkbox" id="fb_add_images" checked>
                            <i></i>
                        </div>
                        <span style="font-weight: normal;">Aggiungi immagini</span>
                    </label>
                </div>
            </div>
            <?php } ?>
        </div>

        <?php
        if(is_array($images)) {
        ?>
        <div class="row" id="fb_images_preview" style="height: 200px; overflow: auto; margin-top: 15px;">
            <?php
            foreach($images as $image) {
            ?>
                <div class="col-sm-3 fb_images_cloner m-b">
                    <img src="<?= $img_path . "tn/" . $image ?>" class="picture_uploaded" style="width: 100%;" />
                </div>
            <?php
            }
            ?>
        </div>
        <?php } ?>

        <div class="row m-t-md">
            <div class="col-sm-12 m-b-sm">
                <div>Dove vuoi condividere questo elemento?</div>
            </div>

            <div class="col-sm-4">
                <div class="styled-checkbox form-control <?= (!$sharerClass->canShareOnSocial('facebook') ? 'ms-label-tooltip' : '') ?>">
                    <label style="width: 100%; padding-top: 3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="La condivisione su Facebook non è disponibile se non è stato impostato l'ID del profilo nelle impostazioni.">
                        <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                            <input type="checkbox" id="share_on_fb" <?= ($sharerClass->canShareOnSocial('facebook') ? "checked" : "disabled") ?>>
                            <i></i>
                        </div>
                        <span style="font-weight: normal;"><i class="fa fa-facebook" style="margin-right: 10px;"></i>Facebook</span>
                    </label>
                </div>
            </div>

            <?php
            /*
            <div class="col-sm-4">
                <div class="styled-checkbox form-control <?= (!is_array($images) || !$sharerClass->canShareOnSocial('instagram') ? 'ms-label-tooltip keep_locked' : '') ?>">
                    <label style="width: 100%; padding-top: 3px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="La condivisione su Instagram non è disponibile se l'elemento non contiene immagini o se non è stato impostato l'ID del profilo nelle impostazioni.">
                        <div class="checkbox i-checks pull-right" style="padding-top: 0px; margin-top: 0px;">
                            <input type="checkbox" id="share_on_instagram" <?= (is_array($images) && $sharerClass->canShareOnSocial('instagram') ? "checked" : "disabled") ?>>
                            <i></i>
                        </div>
                        <span style="font-weight: normal;"><i class="fa fa-instagram" style="margin-right: 10px;"></i>Instagram</span>
                    </label>
                </div>
            </div>
            */ ?>
        </div>
    <?php } else { ?>
        <div class="row m-t-md">
            <div class="col-sm-12">
                <span>Per utilizzare le funzionalità di condivisione sui social, assicurati di effettuare la connessione sicura (https) e di aver fornito le informazioni necessarie all'interno delle impostazioni.</span>
            </div>
        </div>
    <?php
    } ?>
</div>
<?php
require_once('../sw-config.php');

$module_id = $_GET['module'];
$record_id = $_GET['record'];

$module_details = $MSSoftwareModules->getModuleDetailsFromID($module_id);

$module_url = ABSOLUTE_ADMIN_MODULES_PATH_HTML. $module_details['path'] . 'edit.php?id=' . (is_numeric($record_id) ? $record_id : '') . '&fromFastEditor';

if($_GET['source'] && $_GET['source'] != CUSTOMER_DOMAIN_INFO['id']) {
    $module_url = $MSFrameworkFW->getWebsitePathsBy('id', $_GET['source'])['backend_url'] . 'modules/' . $module_details['path'] . 'edit.php?id=' . (is_numeric($record_id) ? $record_id : '') . '&fromFastEditor&fromFastPicker=1';
    $module_url = $MSFrameworkUsers->generateGlobalLoginRedirect($module_url);
}
?>

<div class="modal inmodal" id="fastModuleEditor" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-<?= $module_details['fa_icon']; ?> modal-icon"></i>
                <h4 class="modal-title" style="float: none;"><?= (is_numeric($record_id) ? 'Modifica' : 'Creazione'); ?> <?= $module_details['name']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="circlePreloader fastEditorPreloader"><div></div></div>
                <iframe style="display: none;" src="<?= $module_url; ?>" id="fastModuleEditorIframe"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="fastModuleEditor_Cancel" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary" id="fastModuleEditor_Save">Salva</button>
            </div>
        </div>
    </div>
</div>
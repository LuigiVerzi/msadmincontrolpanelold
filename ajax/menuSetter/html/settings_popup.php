<?php
/**
 * MSAdminControlPanel
 * Date: 2019-09-28
 */

require_once('../../../sw-config.php');

$MSFrameworkModules = new \MSSoftware\modules();

echo $MSFrameworkModules->getHTMLGranularSettingsFields($_GET['module_id'], json_decode($_POST['on_the_fly_data'], true), $_GET['loadedModuleID'], $_GET['abb_id'], $_GET['role_id']);
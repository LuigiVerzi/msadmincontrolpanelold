<?php
/**
 * MSAdminControlPanel
 * Date: 31/03/18
 */

require_once('../sw-config.php');

if($MSFrameworkUsers->getUserDataFromSession('userlevel') !== "0") die();

$module_info = $MSSoftwareModules->getModuleDetailsFromID($_POST['module_id']);
$fields_status = $_POST['fields_status'];

if(is_array($fields_status)) {

    $current_fields_status = $MSFrameworkCMS->getCMSData("modules_fields_status");

    $ary_to_set = array();
    if(is_array($current_fields_status)) {
        $ary_to_set = $current_fields_status;
    }

    $ary_to_set[$module_info['id']] = $fields_status;

    $MSFrameworkCMS->setCMSData("modules_fields_status", $ary_to_set);

}

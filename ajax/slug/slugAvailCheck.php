<?php
/**
 * MSAdminControlPanel
 * Date: 24/03/18
 */

require_once('../../sw-config.php');

$slug = explode('/', $_POST['slug']);
$single_slug = $_POST['single_slug'];
$db_table = $_POST['db_table'];
$record_id = $_POST['record_id'];

$result = $MSFrameworkUrl->checkSlugAvailability($slug, $db_table, $record_id);
if(!$result) {
    $next_avail = $MSFrameworkUrl->findNextAvailableSlug($slug, "", 1, $single_slug);
}

echo json_encode(array("result" => $result, "next" => $next_avail));
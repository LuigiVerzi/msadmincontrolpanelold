<?php
require_once('../../../sw-config.php');

$query = $_GET['q'];

$currentProducts = (new \MSFramework\Ecommerce\products())->getProductsList(0, 1, array(), $query, array());

$return_products = array(
    'total_counts' => 0,
    'incomplete_results' => false,
    'items' => array()
);

if(count($currentProducts['products'])) {
    foreach($currentProducts['products'] as $product) {

        $variazioni = array();
        foreach( $product['formattedAttributes']['variations'] as $id=>$variation) {
            $valori = array();
            foreach ($variation['value'] as $value => $nome) {
                $valori[$value] = $MSFrameworki18n->getFieldValue($nome);
            }
            $variazioni[$id] = array(
                'nome' => $MSFrameworki18n->getFieldValue($variation['nome']),
                'value' => $valori
            );
        }

        $return_products['items'][] = array(
            'id' => $product['id'],
            'nome' => $MSFrameworki18n->getFieldValue($product['nome']),
            'avatar' => UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML . json_decode($product['gallery'], true)[0],
            'variazioni' =>  $variazioni
        );
    }
}

echo json_encode($return_products);
?>
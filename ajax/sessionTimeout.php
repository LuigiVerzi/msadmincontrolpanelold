<?php
require_once('../sw-config.php');

$action = $_GET['action'];

if($action == 'checkLogin') {
    $email = $_POST['email'];
    $password = $_POST['password'];

    $checkCredentials = $MSFrameworkUsers->checkCredentials($email, $password);
    $result = "0";
    if($checkCredentials[0]) {
        $result = ($checkCredentials[1] == "saas_superadmin" ? $checkCredentials[1] : "1");
    }

    echo $result;
    die();
} else if($action == 'getModal') { ?>
<div class="modal inmodal" id="sessionTimeoutModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Sessione Scaduta</h4>
                <small class="font-bold">La tua sessione è scaduta, effettua nuovamente il login per continuare.</small>
            </div>
            <div class="modal-body">

                <div class="alert alert-danger" id="sessionTimeoutAlert" style="display: none;">I dati di accesso inseriti sono errati.</div>

                <div class="form-group">
                    <label>Email</label>
                    <input id="sessionTimeoutEmail" type="email" placeholder="Inserisci la tua email" class="form-control">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input id="sessionTimeoutPassword" type="password" placeholder="Inserisci la tua password" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="checkSessionLogin">Accedi</button>
            </div>
        </div>
    </div>
</div>
<?php } ?>

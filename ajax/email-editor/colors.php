<?php
require_once('../../sw-config.php');

$producer_config = (new \MSFramework\cms())->getCMSData('producer_config');
$email_colors = json_decode($producer_config['templatemail_colors'], true);

echo '<option value="0" data-color="' . $email_colors[0] . '" style="background: ' . $email_colors[0] . ';">Colore Principale</option>';
echo '<option value="1" data-color="' . $email_colors[1] . '" style="background: ' . $email_colors[1] . ';">Colore CTA</option>';
echo '<option value="2" data-color="' . $email_colors[2] . '" style="background: ' . $email_colors[2] . ';">Colore Terziario</option>';
echo '<option value="3" data-color="' . $email_colors[3] . '" style="background: ' . $email_colors[3] . ';">Colore Sfondo</option>';
echo '<option value="custom" data-color="black">Personalizzato</option>';
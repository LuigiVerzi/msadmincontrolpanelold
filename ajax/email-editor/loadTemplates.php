<?php
require_once('../../sw-config.php');

$emails = new \MSFramework\emails();

$template_id = $_POST['id'];
$templates = $emails->getTemplates($template_id, ($template_id != ''));
$groups_info = $emails->getTemplateGroupsStylesInfo();

if($templates['data']) {
    echo $templates['data']['html'];
} else {

    echo '<div class="tabs-container"><div class="tabs-left"><ul class="nav nav-tabs" style="width: 40% !important;">';
    foreach($templates as $template_group => $templates_list) {
        echo '<li class="' . ($template_group == 'main' ? 'active' : '') .'"><a data-toggle="tab" href="#tab-tl-' . $template_group . '"><i style="width: 24px; text-align: center; padding: 5px; border-radius: 3px; background: ' . $groups_info[$template_group]['background'] . '; color: ' . $groups_info[$template_group]['color'] . ';" class="fa ' . $groups_info[$template_group]['icon'] . '"></i>' . $groups_info[$template_group]['label'] . '</a></li>';
    }
    echo '</ul><div class="tab-content">';
    foreach($templates as $template_group => $templates_list) {

        echo '<div id="tab-tl-' . $template_group . '" class="tab-pane ' . ($template_group == 'main' ? 'active' : '') . '"><div class="panel-body" style="width: 60% !important; margin-left: 40%;">';
        foreach($templates_list as $templates_id => $template_info) {
            echo '<div class="template_item import_template_button" data-id="' . $templates_id . '">' . $template_info['nome'] . '</div>';
        }
        echo '</div></div>';
    }
    echo '</div></div></div>';
}
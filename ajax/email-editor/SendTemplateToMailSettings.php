<?php
/**
 * MSAdminControlPanel
 * Date: 19/09/18
 */

require_once('../../sw-config.php');
?>

<div class="row text-left">
    <div class="col-sm-12">
        <label>Destinatario</label>
        <input id="test_send_to" name="test_send_to" class="form-control required" type="text" required>
        <div class="small">Inserisci un indirizzo email al quale inviare il template selezionato. Verranno utilizzati i dati SMTP indicati nel modulo "Impostazioni".</div>
    </div>
</div>

<?php
/**
 * MSAdminControlPanel
 * Date: 19/09/18
 */

require_once('../../sw-config.php');

if($_POST['template_id']) {
    $row = (new \MSFramework\Newsletter\emails())->getTemplateDetails($_POST['template_id'])[$_POST['template_id']];

    $email_subject = $row['titolo'] . " [Questo è solo un test. L'oggetto corrente verrà sostituito dal titolo della campagna!]";
    $doctype = $row['doctype'];
    $email_html_header = $row['html_header'];
    $body_attribs = $row['body_attribs'];
    $email_content_html = $row['versione_html'];
    $email_content_txt = $row['versione_testuale'];

    $html_doctype = ($doctype != "") ? "<" . stripslashes($doctype) . ">" : "";
    $html_header = ($email_html_header != "") ? "<head>" . stripslashes($email_html_header) . "</head>" : "";
    $html_version = $html_doctype . "<html>" . $html_header . "<body " . stripslashes($body_attribs) . ">" . stripslashes($email_content_html) . "</body></html>";
    $txt_version = $email_content_txt;

    $attachments = json_decode($row['attachments'], true);
    $final_attachments = array();
    foreach ($attachments as $attachment) {
        $final_attachments[] = array("path" => UPLOAD_NEWSLETTER_FOR_DOMAIN . $attachment, "name" => $attachment);
    }
} else if($_POST['template_content']) {
    $email_subject = '[Questo è solo un test.]';
    $html_version = $_POST['template_content'];

    $html = new \Html2Text\Html2Text($html_version);
    $versione_testuale =  $html->getText();
    $txt_version = $versione_testuale;
}

$email = new \MSFramework\emails();
if($email->sendCustomMail($email_subject, $html_version, trim($_POST['email']), array(), $txt_version)) {
    echo "ok";
} else {
    echo "err";
}

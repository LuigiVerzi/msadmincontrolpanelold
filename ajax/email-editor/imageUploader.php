<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../sw-config.php');

$action = $_GET['action'];
$target = $_GET['dir'];

$frontend_url = (new \MSFramework\cms())->getURLToSite();
$targetUploader = new \MSFramework\uploads($target);

if($target == 'FRAMEWORK') {

    $target_dir =  PATH_TO_COMMON_STORAGE . 'emails/uploads/';
    $target_dir_html = PATH_TO_COMMON_STORAGE_HTML . 'emails/uploads/';

    $targetUploader->path = $target_dir;
    $targetUploader->path_html = $target_dir_html;
}
else {
    $target_dir = constant('UPLOAD_' . $target . '_FOR_DOMAIN');
    if (is_null($target_dir)) die();
    $target_dir_html = (strpos($frontend_url, 'https://') !== false ? 'https:' : 'http:') . constant('UPLOAD_' . $target . '_FOR_DOMAIN_HTML');
}

if($action == 'upload') {

    $ary_files = $targetUploader->prepareForSave($_POST['images']);

    if($ary_files === false) {
        echo json_encode(array("status" => "mv_error"));
        die();
    }

    echo json_encode($ary_files);
}
else if($action == 'get') {

    $dir_files = glob($target_dir . "/*");
    $images_path = array_filter( $dir_files, function($file) {
        return preg_match( '/\.(gif|jpg|png)$/i', $file );
    } );

    /* SOSTIUISCO LA PATH CON L'URL E LI FORMATTO PER L'EDITOR */
    $formatted_images = array_values(array_map( function($file) {
        global $target_dir_html;
        return $target_dir_html . basename($file);
    }, $images_path ));

    die(json_encode($formatted_images));
}
else if($action == 'delete') {

    $image_url = $_POST['id'];
    $file_name = basename($image_url);

    if( file_exists($target_dir . $file_name) &&  getimagesize($target_dir . $file_name) ) {
        unlink($target_dir . $file_name);
        unlink($target_dir . 'tn/'. $file_name);
    }
}
else if($action == 'uploader') {

    $image_url = $_POST['id'];
    $file_name = basename($image_url);

    $dir_files = glob($target_dir . "/*");

    $images_path = array_filter( $dir_files, function($file) {
        return preg_match( '/\.(gif|jpg|png)$/i', $file );
    } );

    /* SOSTIUISCO LA PATH CON L'URL E LI FORMATTO PER L'EDITOR */
    $formatted_images = array_values(array_map( function($file) {
        return basename($file);
    }, $images_path ));

    ?>
    <div id="emailEditorImagesModal" class="modal fade" role="dialog">
        <input type="hidden" id="constant_editor_upload" value="<?= $target; ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Gestione Immagini</h4>
                </div>
                <div class="modal-body">
                    <?php $targetUploader->initUploaderHTML("emailEditorUploader", json_encode($formatted_images)); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                </div>
            </div>

        </div>
    </div>
<?php
}

function normalizeChars($s) {
    $replace = array(
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'Ae', 'Å'=>'A', 'Æ'=>'A', 'Ă'=>'A', 'Ą' => 'A', 'ą' => 'a',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'ae', 'å'=>'a', 'ă'=>'a', 'æ'=>'ae',
        'þ'=>'b', 'Þ'=>'B',
        'Ç'=>'C', 'ç'=>'c', 'Ć' => 'C', 'ć' => 'c',
        'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ę' => 'E', 'ę' => 'e',
        'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e',
        'Ğ'=>'G', 'ğ'=>'g',
        'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'İ'=>'I', 'ı'=>'i', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i',
        'Ł' => 'L', 'ł' => 'l',
        'Ñ'=>'N', 'Ń' => 'N', 'ń' => 'n',
        'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe', 'Ø'=>'O', 'ö'=>'oe', 'ø'=>'o',
        'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
        'Š'=>'S', 'š'=>'s', 'Ş'=>'S', 'ș'=>'s', 'Ș'=>'S', 'ş'=>'s', 'ß'=>'ss', 'Ś' => 'S', 'ś' => 's',
        'ț'=>'t', 'Ț'=>'T',
        'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'Ue',
        'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'ue',
        'Ý'=>'Y',
        'ý'=>'y', 'ý'=>'y', 'ÿ'=>'y',
        'Ž'=>'Z', 'ž'=>'z', 'Ż' => 'Z', 'ż' => 'z', 'Ź' => 'Z', 'ź' => 'z',
        '\'' => '', '[' => '-', ']' => '-'
    );
    return strtr($s, $replace);
}

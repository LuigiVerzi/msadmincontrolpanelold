<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../sw-config.php');

$id_template = $_GET['t'];
$r = $MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__template WHERE id = :id", array(':id' => $_GET['t']), true);

if (!$r) {
    exit('Impossibile determinare la mail da visualizzare');
}

$emails = new \MSFramework\emails();

$email_html = $r['versione_html'];
$email_html = $emails->replaceGlobalShortcode($email_html);

// show email on web browser
echo "
    <!DOCTYPE html>
    <html>
        <head>
            <title>".$r['titolo']."</title>
            <meta http-equiv='content-type' content='text/html;charset=utf-8' />
        </head>
        <body>
            " . $emails->formatTemplateHTML($email_html) . "
        </body>
    </html>
";
?>

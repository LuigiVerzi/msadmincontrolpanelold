<?php
/**
 * MSAdminControlPanel
 * Date: 18/03/18
 */

require_once('../../sw-config.php');

if((new \MSFramework\Newsletter\subscribers())->confirmAuth($_GET['auth'])) {
    $msg = "La tua iscrizione è stata confermata con successo! Grazie!";
} else {
    $msg = "Impossibile confermare l'iscrizione. Il codice inserito non esiste o è già stato confermato.";
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo SW_NAME ?> | Iscrizione Newsletter</title>

    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/animate.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/style.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/colors-aquamarine.css" rel="stylesheet">

</head>

<body class="gray-bg">

<div class="lock-word animated fadeInDown">
    <span class="first-word"></span><span></span>
</div>
<div class="middle-box text-center lockscreen animated fadeInDown">
    <div>
        <div class="m-b-md">
            <img alt="image" class="img-circle circle-border" src="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/user.png" width="128px">
        </div>
        <h3>Iscrizione Newsletter</h3>
        <p><?php echo $msg ?></p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/js/jquery-2.1.1.js"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/js/bootstrap.min.js"></script>

</body>
</html>
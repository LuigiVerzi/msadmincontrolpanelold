<?php
/**
 * MSAdminControlPanel
 * Date: 18/03/18
 */

require_once('../../sw-config.php');

$check_activation = ($MSFrameworkUsers->activateAccount($_GET['auth'], $_GET['env_id']) || (new \MSFramework\customers())->activateAccount($_GET['auth']));

if($check_activation) {
    $msg = "Il tuo account è stato confermato correttamente! Grazie!";
} else {
    $msg = "Impossibile confermare l'iscrizione. Il codice inserito non esiste o il tuo account è già stato confermato.";
}

$siteCMSData = $MSFrameworkCMS->getCMSData('site');
$logos = json_decode($siteCMSData["logos"], true);
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo SW_NAME ?> | Attivazione Account</title>

    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/animate.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/style.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/custom.css" rel="stylesheet">

    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/colors-aquamarine.css" rel="stylesheet">

</head>

<body class="login-bg" style="<?= ($logos['login_bg'] != '' ? 'background-image: url(' . htmlentities(UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logos['login_bg']) . ');' : '') ?>">
<div class="overlay"></div>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div class="logo-name"><img src="<?= $MSFrameworkCMS->getLoginLogoPath(); ?>" alt="MS" class="img-responsive"  onerror="this.onerror=null;this.src='<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/logo.svg';" /></div>
    <div class="login-content">
        <h3>Attivazione Account</h3>
        <p><?php echo $msg ?></p>
        <br>
        <a href="<?= (new \MSFramework\cms())->getURLToSite(); ?>" class="btn btn-primary" style="width: 100%">Torna su<br><?= SW_NAME; ?></a>

        <p class="m-t"> <small>Copyright <?php echo SW_NAME ?> &copy; <?php echo date("Y") ?> - All rights reserved</small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="vendor/js/jquery-2.1.1.js"></script>
<script src="vendor/js/bootstrap.min.js"></script>

</body>
</html>
<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../sw-config.php');

$id_campaign = $_GET['c'];
$id_recipient = $_GET['r'];

if ($_GET['c'] == "" || $_GET['r'] == "") {
    exit('Impossibile determinare la mail da visualizzare');
}

$emails = (new \MSFramework\Newsletter\emails());

$emailDetails = $emails->getEmailDetails($id_campaign)[$id_campaign];
$userDetails = $emails->MSFrameworkCustomers->getCustomerDataFromDB($id_recipient);

if(!$emailDetails || !$userDetails) return false;

$preparedValues = $emails->prepareEmailForUser($emailDetails, $userDetails);

$html = strtr($preparedValues[0]['custom_layout_html'], $preparedValues[1]);
$common_shortcodes = (new \MSFramework\emails())->getCommonShortcodes();
$html = str_replace($common_shortcodes[0], $common_shortcodes[1], $html);
?>

<!DOCTYPE html>
<html>
<head>
    <title><?= $emailDetails['nome']; ?></title>
    <meta http-equiv='content-type' content='text/html;charset=utf-8' />
</head>
<body>
<?= $html ?>
</body>
</html>
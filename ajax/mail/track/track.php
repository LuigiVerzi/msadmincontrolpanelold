<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

require_once('../../../sw-config.php');

$id_email = $_GET['c'];
$id_recipient = $_GET['r'];

if ($_GET['c'] == "" || $_GET['r'] == "") {
    exit();
}

if($_GET['type'] == "view") {
    (new \MSFramework\Newsletter\emails())->trackMailOpened($id_email, $id_recipient);
} else if($_GET['type'] == "click") {
    (new \MSFramework\Newsletter\emails())->trackMailLinkClicked($id_email, $id_recipient, $_GET['link']);
} else if($_GET['type'] == "unsubscribe") {
    (new \MSFramework\Newsletter\emails())->trackMailUnsubscribe($id_email, $id_recipient);
    die('Operazione completata con successo');
}

if($_GET['type'] != "unsubscribe") {
    (new \MSFramework\Newsletter\emails())->trackGeoLocationMailOpened($id_email, $id_recipient);
}

/* STAMPO IL PIXEL PNG */
header('Content-Type: image/png');
echo base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=');

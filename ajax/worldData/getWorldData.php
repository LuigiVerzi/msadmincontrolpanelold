<?php
/**
 * MSAdminControlPanel
 * Date: 14/08/17
 */

require_once('../../sw-config.php');

$MSFrameworkGeonames = (new \MSFramework\geonames());

$html = "<option value=''></option>";
if($_GET['search'] == "regione") {
    foreach($MSFrameworkGeonames->getRegionsList($_GET['id'], "geonameid, name") as $v) {

    $html .= '<option value="' . $v['geonameid'] . '">' . $v['name'] . '</option>';

    }
}

if($_GET['search'] == "provincia") {
    foreach($MSFrameworkGeonames->getProvinceList($_GET['id'], "geonameid, name") as $v) {

        $html .= '<option value="' . $v['geonameid'] . '">' . $v['name'] . '</option>';

    }
}

if($_GET['search'] == "comune") {
    foreach($MSFrameworkGeonames->getComuniList($_GET['id'], "geonameid, name") as $v) {

        $html .= '<option value="' . $v['geonameid'] . '">' . $v['name'] . '</option>';

    }
}

echo $html;
?>

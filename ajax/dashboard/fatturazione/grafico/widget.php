<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('fatturazione_vendite')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-4 col-md-push-8">
                    <div class="text-right">
                        <input type="text" name="daterange" id="fatturazione_daterange" value="Ultimi 30 giorni" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-8 col-md-pull-4">
                    <h3 class="font-bold no-margins">
                        Andamento Vendite
                    </h3>
                    <small class="order_chart_time">Stai visualizzando <b>Ultimi 30 giorni</b>.</small>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <canvas id="orderChart" height="300"></canvas>
        </div>
        <div class="ibox-content">
            <div class="stat-list row">
                <div class="col-xs-6">
                    <h2 class="no-margins" id="vendite_chart_total_earn"></h2>
                    <small>Entrate stimate</small>
                    <div class="progress progress-mini">
                        <div class="progress-bar" style="width: 100%;"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <h2 class="no-margins" id="vendite_chart_total_vendite"></h2>
                    <small>Ordini totali</small>
                    <div class="progress progress-mini">
                        <div class="progress-bar" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <small>
                <strong>NB:</strong> I valori espressi intendono il totale dei carrelli e non tengono conto delle spese e tassazioni.
            </small>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th>Ultime vendite</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($MSFrameworkDatabase->getAssoc("SELECT fatturazione_vendite.id, fatturazione_vendite.carrello, fatturazione_vendite.data, fatturazione_vendite.dati_cliente FROM fatturazione_vendite ORDER BY data DESC LIMIT 5") as $vendita) { ?>

                    <?php
                    $dati_cliente = json_decode($vendita['dati_cliente'], true);

                    $prodotti = json_decode($vendita['carrello'], true);
                    $titoli_prodotti = array();
                    $totale_prodotti = 0.0;

                    foreach($prodotti as $prodotto) {
                        $titoli_prodotti[] = $prodotto['nome'];
                        $totale_prodotti += $prodotto['prezzo_subtotal'];
                    }
                    $titoli_prodotti = implode('<br><br>', $titoli_prodotti);
                    ?>

                    <tr>
                        <td><?= $dati_cliente['nome'] . ' ' . $dati_cliente['cognome']; ?></td>
                        <td><?= number_format($totale_prodotti, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></td>
                        <td><i class="fa fa-clock-o"></i> <?= (new \MSFramework\utils())->smartdate(strtotime($vendita['data'])); ?></td>
                        <td class="text-navy"><a href="modules/fatturazione/vendite/edit.php?id=<?= $vendita['id']; ?>" class="text-navy">Vedi <i class="fa fa-arrow-right"></i></a></td>
                        <td><span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= htmlentities($titoli_prodotti); ?>"></i></span></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    window.widgets_functions.push(function () {

        /* INIZIALIZZAZIONE GRAFICO ORDINI */
        $('#fatturazione_daterange').daterangepicker({
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY',
                applyLabel: "Cerca",
                cancelLabel: "Annulla",
                fromLabel: "Da",
                toLabel: "A",
                customRangeLabel: "Personalizzata",
            },
            ranges: {
                'Ultimi 7 giorni': [moment().subtract(6, 'days'), moment()],
                'Ultimi 30 giorni': [moment().subtract(30, 'days'), moment()],
                'Questo mese': [moment().startOf('month'), moment().endOf('month')],
                'Mese precedente': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Dall\'inizio': [0, moment()]
            },
            showCustomRangeLabel: false,
            autoUpdateInput: false
        }, function(start, end, label) {
            updateOrderGraph(start, end);
            $('#fatturazione_daterange').val(label);
            $('.order_chart_time b').html(label);
        });
        updateOrderGraph(moment().subtract(30, 'days'), moment());

    });

    function updateOrderGraph(start, end) {

        $.ajax({
            url: "<?= ABSOLUTE_SW_PATH_HTML . $widget_info['path']; ?>/ajax.php?action=vendite",
            type: 'post',
            data: {start: start.format('YYYY-MM-DD'), end: end.format('YYYY-MM-DD')},
            async: true,
            dataType: 'json',
            success: function(data) {

                if(typeof(window.order_chart) != 'undefined') window.order_chart.destroy();

                $('#vendite_chart_total_earn').html(data.earn_this_period + '<?= CURRENCY_SYMBOL; ?>');
                $('#vendite_chart_total_vendite').html(data.orders_this_period);

                var array_data = data.graph.data;
                var lineData = {
                    labels: data.labels,
                    datasets: [
                        {
                            label: "Preventivo",
                            backgroundColor: "rgba(150,150,150,0.5)",
                            borderColor: "rgba(150,150,150,0.7)",
                            pointBackgroundColor: "rgba(150,150,150,1)",
                            pointBorderColor: "#fff",
                            data: array_data[0]
                        },
                        {
                            label: "Ricevuta",
                            backgroundColor: "rgba(103,58,183,0.5)",
                            borderColor: "rgba(103,58,183,0.7)",
                            pointBackgroundColor: "rgba(103,58,183,1)",
                            pointBorderColor: "#fff",
                            data: array_data[1]
                        },
                        {
                            label: "Fattura",
                            backgroundColor: "rgba(255,152,0,0.5)",
                            borderColor: "rgba(255,152,0,1)",
                            pointBackgroundColor: "rgba(255,152,0,1)",
                            pointBorderColor: "#fff",
                            data: array_data[2]
                        },
                        {
                            label: "Scontrino",
                            backgroundColor: "rgba(33,150,243,0.5)",
                            borderColor: "rgba(33,150,243,1)",
                            pointBackgroundColor: "rgba(33,150,243,1)",
                            pointBorderColor: "#fff",
                            data: array_data[3]
                        },
                        {
                            label: "Scontrino non Fiscale",
                            backgroundColor: "rgba(139,195,74,0.5)",
                            borderColor: "rgba(139,195,74,1)",
                            pointBackgroundColor: "rgba(139,195,74,1)",
                            pointBorderColor: "#fff",
                            data: array_data[4]
                        },
                        {
                            label: "Senza documento",
                            backgroundColor: "rgba(0,0,0,0.5)",
                            borderColor: "rgba(0,0,0,1)",
                            pointBackgroundColor: "rgba(0,0,0,1)",
                            pointBorderColor: "#fff",
                            data: array_data[5]
                        }
                    ]
                };

                var lineOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + '<?= CURRENCY_SYMBOL; ?>';
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return Math.round(value * 100) / 100 + '<?= CURRENCY_SYMBOL; ?>';
                                }
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                autoSkip: true,
                                maxTicksLimit: 7
                            }
                        }]
                    }
                };

                var ctx = document.getElementById("orderChart").getContext("2d");
                window.order_chart = new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
                window.dashboardMasonry.masonry()
            }
        });
    }

</script>
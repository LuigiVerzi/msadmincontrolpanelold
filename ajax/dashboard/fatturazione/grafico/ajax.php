<?php
require_once('../../../../sw-config.php');

$action = $_GET['action'];

if($action == 'vendite') {
    $ordini = array('data' => array());

    $fatturazione_vendite = new MSFramework\Fatturazione\vendite();

    if(strtotime($_POST['start']) <= strtotime('2000-01-01')) {
       $date = $MSFrameworkDatabase->getAssoc("SELECT data FROM `fatturazione_vendite` ORDER BY data ASC LIMIT 1", array(), true)['data'];

       if($date) {
           $_POST['start'] = date('Y-m-d', strtotime($date));
       }
    }

    $ordini['data'][0] = $fatturazione_vendite->getDateRangeChart($_POST['start'], $_POST['end'], 'preventivo');
    $ordini['data'][1] = $fatturazione_vendite->getDateRangeChart($_POST['start'], $_POST['end'], 'ricevuta');
    $ordini['data'][2] = $fatturazione_vendite->getDateRangeChart($_POST['start'], $_POST['end'], 'fattura');
    $ordini['data'][3] = $fatturazione_vendite->getDateRangeChart($_POST['start'], $_POST['end'], 'scontrino');
    $ordini['data'][4] = $fatturazione_vendite->getDateRangeChart($_POST['start'], $_POST['end'], 'scontrino_non_fiscale');
    $ordini['data'][5] = $fatturazione_vendite->getDateRangeChart($_POST['start'], $_POST['end'], '');

    $earn_this_period = 0;
    $orders_this_period = 0;
    foreach($ordini['data'] as $k=>$giorni) {
        foreach($giorni as $k2 => $giorno) {
            $earn_this_period += $giorno['earn'];
            $orders_this_period += $giorno['orders'];
            $ordini['data'][$k][$k2] = $giorno['earn'];
        }
    }

    $start = new DateTime($_POST['start']);
    $end = new DateTime($_POST['end']);

    $nomi_mesi = array('Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic');

    $labels = array();
    for($i = $start; $i <= $end; $i->modify('+1 day')) {
        $labels[] = (int)$i->format("d") . ' ' . $nomi_mesi[(int)$i->format("m")-1];
    }


    die(json_encode(array('graph' => $ordini, 'labels' => $labels, 'earn_this_period' => number_format($earn_this_period, 2, ',', '.'), 'orders_this_period' => $orders_this_period)));
}
<?php
require_once('../../../sw-config.php');

$action = $_GET['action'];

if($action == 'appointments_recap') {
    $start = $_POST['start'];
    $end = $_POST['end'];

    $users_where_str = "";
    $users_where_ary = array();
    if((new \MSFramework\users())->getUserDataFromSession('userlevel') != "0" && (new \MSFramework\users())->getUserDataFromSession('userlevel') != "1") {
        $users_where_str = " AND id = :id ";
        $users_where_ary = array(":id" => (new \MSFramework\users())->getUserDataFromSession('user_id'));
    }

    $html = "<table class=\"table table-bordered\">";
    $got_appointments = false;
    $old_date = "";
    $service_cache = array();

    foreach($MSFrameworkDatabase->getAssoc("SELECT id, date(start_date) as day, start_date as full_date, `user`, customer, services, cabin FROM appointments_list WHERE date(start_date) >= '$start' AND date(start_date) <= '$end' $users_where_str ORDER BY start_date asc", $users_where_ary) as $r) {
        $got_appointments = true;

        if($old_date != $r['day']) {
            $html .= "<table class=\"table table-bordered\" style='margin-bottom: 0;'><thead><tr><th colspan='2' style='text-align: center;'>Appuntamenti per " . strftime("%A %d %B %Y", strtotime($r['day'])) . "</th></tr></thead>";
        }

        $customer_data = (new \MSFramework\customers())->getCustomerDataFromDB($r['customer'], "cognome, nome, telefono_casa, telefono_cellulare");

        if($r['user'] != "-1") {
            $user_data = (new \MSFramework\users())->getUserDataFromDB($r['user'], "cognome, nome");
        } else {
            $user_data = array("cognome" => "Operatore", "nome" => "Generico");
        }

        $cabin_str = "Cabina Generica";
        if($r['cabin'] != "-1") {
            $r_cabin = $MSFrameworkDatabase->getAssoc("SELECT nome FROM beautycenter_cabine WHERE id = :id", array(":id" => $r['cabin']), true);
            $cabin_str = "Cabina " . $MSFrameworki18n->getFieldValue($r_cabin['nome'], true);
        }

        $services_str = "";
        foreach(explode(",", $r['services']) as $service) {
            if(!in_array($service, $service_cache)) {
                $services_data = (new \MSFramework\services())->getServiceDetails($service, "nome")[$service];
                foreach($services_data as $serviceK => $serviceV) {
                    $service_cache[$service][$serviceK] = $serviceV;
                }
            }

            $services_str .= $MSFrameworki18n->getFieldValue($service_cache[$service]['nome'], true) . ", ";
        }
        $services_str = ($services_str != "" ? " - " . substr($services_str, 0, -2) : "");

        $telefono_str = "";
        $telefono_str .= ($customer_data['telefono_casa'] != "" ? "Telefono casa: " . $customer_data['telefono_casa'] . ", " : "");
        $telefono_str .= ($customer_data['telefono_cellulare'] != "" ? "Telefono cellulare: " . $customer_data['telefono_cellulare'] . ", " : "");
        if($telefono_str != "") {
            $telefono_str = '<div style="margin-top: 5px;"><b>Contatti:</b> ' . substr($telefono_str, 0, -2) . "</div>";
        }

        $html .= "<tbody>
                    <tr>
                        <td width='20%' style='vertical-align: middle;'>
                            Ore <br /> <h4>" . date("H:i", strtotime($r['full_date'])) . "</h4>
                        </td>
                        <td style='vertical-align: middle;'>
                            <b>" . $customer_data['cognome'] . " " . $customer_data['nome'] . "</b>" . $services_str . "<br />
                            <small>con " . $user_data['cognome'] . " " . $user_data['nome'] . ", " . $cabin_str . "</small>
                            <small>" . $telefono_str . "</small>
                        </td>
                    </tr>
                </tbody>";

        $old_date = $r['day'];

        if($old_date != $r['day']) {
            $html .= "</table>";
        }
    }

    $html .= "</table>";

    if(!$got_appointments) {
        die('<div align="center">Nessun appuntamento per questo range di date</div>');
    }

    echo $html;
}
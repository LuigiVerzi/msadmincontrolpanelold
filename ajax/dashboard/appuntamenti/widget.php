<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('appuntamenti')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-warning pull-right" href="modules/appointments/list">Vai al calendario</a>
            <h5>Appuntamenti</h5>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-xs-4">
                    <small class="stats-label">Appuntamenti Oggi</small>
                    <h4><?= $MSFrameworkDatabase->getCount("SELECT id FROM appointments_list WHERE date(start_date) = :cur_date", array(":cur_date" => time("Y-m-d"))); ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Appuntamenti Da Fare</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT id FROM appointments_list WHERE date(start_date) >= :cur_date", array(":cur_date" => time("Y-m-d"))); ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Appuntamenti Conclusi</small>
                    <h4><?= $MSFrameworkDatabase->getCount("SELECT id FROM appointments_list WHERE date(start_date) < :cur_date", array(":cur_date" => time("Y-m-d"))); ?></h4>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-md-4 col-md-push-8">
                    <div class="text-right">
                        <input type="text" name="daterange" id="appointments_recap_daterange" value="Oggi" class="form-control m-b-sm"/>
                    </div>
                </div>
                <div class="col-md-8 col-md-pull-4">
                    <h3 class="font-bold no-margins">
                        Riepilogo appuntamenti
                    </h3>
                    <small id="appointments_recap_chart_time" style="display: none;">Tra il giorno <b><span id="appointments_recap_datefrom"><?= date("d/m/Y") ?></span></b> ed il giorno <b><span id="appointments_recap_dateto"><?= date("d/m/Y") ?></span></b>.</small>
                    <small id="appointments_recap_chart_time_today">Oggi, <b><span id="appointments_recap_datefrom"><?= date("d/m/Y") ?></span></b>.</small>
                </div>
            </div>
            <div class="m-t-sm">
                <div class="row">
                    <div class="col-md-12">
                        <div id="appointments_recap_chart">
                            <div align="center">Carico la lista degli appuntamenti...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    window.widgets_functions.push(
        function () {
            /* INIZIALIZZAZIONE GRAFICO RIEPILOGO APPUNTAMENTI */
            $('#appointments_recap_daterange').daterangepicker({
                startDate: moment(),
                endDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY',
                    applyLabel: "Cerca",
                    cancelLabel: "Annulla",
                    fromLabel: "Da",
                    toLabel: "A",
                    customRangeLabel: "Personalizzata",
                },
                ranges: {
                    'Oggi': [moment(), moment()],
                    'Prossimi due giorni': [moment(), moment().add(2, "days")],
                    'Prossimi cinque giorni': [moment(), moment().add(5, "days")],
                    'Prossimi dieci giorni': [moment(), moment().add(10, "days")],
                },
                showCustomRangeLabel: false,
                autoUpdateInput: false
            }, function (start, end, label) {
                updateAppointmentsRecapGraph(start, end);
                $('#appointments_recap_daterange').val(label);
            });
            updateAppointmentsRecapGraph(moment(), moment());
        }
    );

    function updateAppointmentsRecapGraph(start, end) {
        if(start.format('DD/MM/YYYY') == end.format('DD/MM/YYYY')) {
            $('#appointments_recap_chart_time_today').show();
            $('#appointments_recap_chart_time').hide();
        } else {
            $('#appointments_recap_chart_time_today').hide();
            $('#appointments_recap_chart_time').show();
        }

        $('#appointments_recap_datefrom').html(start.format('DD/MM/YYYY'));
        $('#appointments_recap_dateto').html(end.format('DD/MM/YYYY'));
        $('#appointments_recap_chart').html('<div align="center">Carico la lista degli appuntamenti...</div>');

        $.ajax({
            url: "<?= ABSOLUTE_SW_PATH_HTML . $widget_info['path']; ?>/ajax.php?action=appointments_recap",
            type: 'post',
            data: {start: start.format('YYYY-MM-DD'), end: end.format('YYYY-MM-DD')},
            async: true,
            dataType: 'html',
            success: function(data) {
                $('#appointments_recap_chart').html(data);
            }
        });
    }
</script>
<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('tracking_eventi');
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-4 col-md-push-8">
                    <div class="text-right">
                        <input type="text" name="daterange" id="tracking_daterange" value="Ultimi 30 giorni" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-8 col-md-pull-4">
                    <h3 class="font-bold no-margins">
                        Resoconto Tracking
                    </h3>
                    <small class="tracking_chart_time">Stai visualizzando <b>Ultimi 30 giorni</b>.</small>
                </div>
            </div>
        </div>
        <div class="ibox-content">
           <canvas id="grafico_resoconto"></canvas>
        </div>
        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th>Nome Evento</th>
                    <th>Utilizzi</th>
                    <th>Ultimo Utilizzo</th>
                </tr>
                </thead>
                <tbody id="tracking_summary_table">

                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    window.widgets_functions.push(function () {

        /* INIZIALIZZAZIONE GRAFICO TRACKING */
        $('#tracking_daterange').daterangepicker({
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY',
                applyLabel: "Cerca",
                cancelLabel: "Annulla",
                fromLabel: "Da",
                toLabel: "A",
                customRangeLabel: "Personalizzata",
            },
            ranges: {
                'Ultimi 7 giorni': [moment().subtract(6, 'days'), moment()],
                'Ultimi 30 giorni': [moment().subtract(30, 'days'), moment()],
                'Questo mese': [moment().startOf('month'), moment().endOf('month')],
                'Mese precedente': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Dall\'inizio': [0, moment()]
            },
            showCustomRangeLabel: false,
            autoUpdateInput: false
        }, function(start, end, label) {
            updateTrackingGraph(start, end);
            $('#tracking_daterange').val(label);
            $('.tracking_chart_time b').html(label);
        });
        updateTrackingGraph(moment().subtract(30, 'days'), moment());

    });

    function updateTrackingGraph(start, end) {

        $.ajax({
            url: "<?= ABSOLUTE_SW_PATH_HTML . $widget_info['path']; ?>/ajax.php?action=ordini",
            type: 'post',
            data: {start: start.format('YYYY-MM-DD'), end: end.format('YYYY-MM-DD')},
            async: true,
            dataType: 'json',
            success: function(grafico_data) {

                grafico_data.colori = [];

                Object.keys(grafico_data.valori).forEach(function (val) {
                    grafico_data['colori'].push('#' + (function co(lor){   return (lor +=
                        [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f'][Math.floor(Math.random()*16)])
                    && (lor.length == 6) ?  lor : co(lor); })(''));
                });

                if(!grafico_data.valori.length) {
                    $('#grafico_resoconto').parent().hide();
                } else {
                    $('#grafico_resoconto').parent().show();
                }

                var ctx = document.getElementById("grafico_resoconto").getContext("2d");
                window.tracking_chart = new Chart(ctx,
                    {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                data: grafico_data.valori,
                                backgroundColor: grafico_data.colori
                            }],
                            labels: grafico_data.chiavi
                        }
                    }
                );

                $('#tracking_summary_table').html(grafico_data.tabella);
                window.dashboardMasonry.masonry()
            }
        });
    }

</script>
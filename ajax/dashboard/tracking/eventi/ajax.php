<?php
require_once('../../../../sw-config.php');

$action = $_GET['action'];

if($action == 'ordini') {


    if(strtotime($_POST['start']) <= strtotime('2000-01-01')) {
        $_POST['start'] = $MSFrameworkDatabase->getAssoc("SELECT track_date FROM tracking__stats ORDER BY id ASC LIMIT 1", array(), true)['track_date'];
    }

    $start = new DateTime($_POST['start']);
    $end = new DateTime($_POST['end']);

    $nomi_mesi = array('Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic');

    $date_where = array();
    $date_where[] = "track_date > '" . $start->format('Y-m-d') . "'";
    $date_where[] = "track_date < '" . $end->format('Y-m-d 23:59') . "'";

    $date_where = implode(' AND ', $date_where);

    $eventi = $MSFrameworkDatabase->getAssoc("SELECT track_event, MAX(track_date) as last_track_date, COUNT(*) as numero_chiamate FROM `tracking__stats` WHERE $date_where GROUP BY track_event ORDER BY numero_chiamate DESC");


    $dati_resoconto = array('chiavi' => array(), 'valori' => array(), 'tabella' => '');
    foreach($eventi as $evento) {
        $dati_resoconto['chiavi'][] = $evento['track_event'];
        $dati_resoconto['valori'][] = $evento['numero_chiamate'];

        $dati_resoconto['tabella'] .= '<tr><td>' . htmlentities($evento['track_event']) . '</td><td>' . $evento['numero_chiamate'] . '</td><td>' . ($evento['last_track_date'] ? date("d/m/Y H:i", strtotime($evento['last_track_date'])) : '<small class="muted">N/A</small>') . '</td></tr>';

    }

    if($dati_resoconto['tabella'] == '') {
        $dati_resoconto['tabella'] = '<tr><td colspan="3"><span class="text-muted text-center">Nessun dato da mostrare</span></td></tr>';
    }



    die(json_encode($dati_resoconto));
}
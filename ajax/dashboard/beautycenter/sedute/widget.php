<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('beautycenter_sedute');

$sedute_rimanenti = 0;
$sedute_fatte = 0;
$servizio_piu_usato = '';

$rimanenti_sql = $MSFrameworkDatabase->getAssoc("SELECT SUM(valore) as sedute_rimanenti FROM beautycenter_sedute", array(), true);
if($rimanenti_sql) $sedute_rimanenti = (int)$rimanenti_sql['sedute_rimanenti'];

$fatte_sql = $MSFrameworkDatabase->getAssoc("SELECT SUM(valore) as sedute_fatte FROM beautycenter_sedute WHERE valore < 0", array(), true);
if($fatte_sql) $sedute_fatte = (int)$fatte_sql['sedute_fatte'];

$servizio_piu_usato_sql = $MSFrameworkDatabase->getAssoc("SELECT tipo, ref FROM `beautycenter_sedute` GROUP BY tipo ORDER BY COUNT(tipo) DESC LIMIT 1", array(), true);
if($servizio_piu_usato_sql) {

    $services_type = explode('_', $servizio_piu_usato_sql['tipo'])[0];
    $ref = json_decode($servizio_piu_usato_sql['ref'], true);

    $ref_link = '';
    if($services_type == 'service') {
        $ref_info = (new \MSFramework\services())->getServiceDetails($ref['id']);
        if(isset($ref_info[$ref['id']])) {
            $ref_link = 'modules/servizi/list/edit.php?id=' . $ref['id'];
        }
    }
    else {
        $ref_info = (new \MSFramework\BeautyCenter\offers())->getOfferDetails($ref['id']);
        if(isset($ref_info[$ref['id']])) {
            $ref_link = 'modules/beautycenter/offerte/edit.php?id=' . $ref['id'];
        }
    }

    $servizio_piu_usato = (!empty($ref_link) ? '<a href="' . $ref_link . '">' : '') . $MSFrameworki18n->getFieldValue($ref['nome']) . (!empty($ref_link) ? '</a>' : '');
}

?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-inverse pull-right" href="modules/beautycenter/sedute">Vedi Sedute</a>
            <h5>Sedute Clienti</h5>
        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-xs-4">
                    <small class="stats-label">Da Fare</small>
                    <h4><?= $sedute_rimanenti; ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Fatte</small>
                    <h4><?= $sedute_fatte; ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Servizio Più Richiesto</small>
                    <h4><?= $servizio_piu_usato; ?></h4>
                </div>
            </div>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th>Ultimi Movimenti</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($MSFrameworkDatabase->getAssoc("SELECT *, (SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = beautycenter_sedute.cliente) as cliente FROM beautycenter_sedute ORDER BY id DESC LIMIT 5") as $v) { ?>
                    <tr>
                        <td><span class="label label-<?= ($v['valore'] < 0 ? 'danger' : 'primary'); ?>"><?= ($v['valore'] < 0 ? '-' : '+'); ?> <?= abs($v['valore']); ?> sedute</span></td>
                        <td><?= $v['cliente']; ?></td>
                        <td><?= (new \MSFramework\utils())->smartdate(strtotime($v['data'])); ?></td>
                        <td><span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= htmlentities((new \MSFramework\customers())->parseBalanceNotes($v['note'])); ?>"></i></span></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {

    });

</script>
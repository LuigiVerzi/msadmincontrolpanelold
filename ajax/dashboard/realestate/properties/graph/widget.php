<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('realestate_total_properties');

$MSFrameworkUsers = new \MSFramework\users();
$MSFrameworkImmobiliCat = new \MSFramework\RealEstate\categories();

$where_str = "";
$where_ary = array();
if(($MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-5" || $MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-6")) {
    $where_str .= " AND owner_id = :owner_id";
    $where_ary = array_merge($where_ary, array(":owner_id" => $MSFrameworkUsers->getUserDataFromSession('user_id')));
}

$total_immobili = 0;
$total_immobili_attivi = 0;
$total_immobili_venduti = 0;
$total_immobili_non_venduti = 0;
foreach($MSFrameworkDatabase->getAssoc("SELECT id, category, stato_vendita FROM realestate_immobili WHERE id != '' $where_str", $where_ary) as $immobile) {
    $total_immobili++;

    if($immobile['stato_vendita'] == "0") {
        $total_immobili_attivi++;
    } else if($immobile['stato_vendita'] == "1") {
        $total_immobili_venduti++;
    } else if($immobile['stato_vendita'] == "2") {
        $total_immobili_non_venduti++;
    }

    $total_by_type[$MSFrameworkImmobiliCat->getCategoryParent($immobile['category'], true)['id']][$immobile['stato_vendita']][] = $immobile['id'];
}
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-info pull-right" href="modules/immobili/list">Vedi Immobili</a>
            <h5>Grafico Immobili</h5>
        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-12">
                    <div>
                        <canvas id="doughnutChart" height="240"></canvas>
                    </div>
                </div>

                <div class="col-lg-12" style="margin-top: 20px;">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Tipologia</th>
                            <th>Totali</th>
                            <th>%</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $fill_colors = array(
                            "#9be1a5",
                            "#dedede",
                            "#b5b8cf",
                            "#ebe886",
                            "#c295cf",
                            "#338ecf",
                        );

                        $count = 0;
                        foreach($MSFrameworkImmobiliCat->getOnlyParents() as $catK => $catDet) {
                            ?>
                            <input type="hidden" id="count_imm_active_<?php echo $count ?>" value="<?php echo count($total_by_type[$catK]['0']); ?>" />
                            <input type="hidden" id="name_imm_<?php echo $count ?>" value="<?php echo $MSFrameworki18n->getFieldValue($catDet['nome']); ?>" />
                            <input type="hidden" id="color_imm_<?php echo $count ?>" value="<?php echo $fill_colors[$count]; ?>" />
                            <tr>
                                <td><span class="pie"><?php echo count($total_by_type[$catK]['0']); ?>/<?php echo $total_immobili_attivi ?></span> <?php echo $MSFrameworki18n->getFieldValue($catDet['nome']) ?></td>
                                <td><?php echo count($total_by_type[$catK]['0']); ?></td>
                                <td><?php echo round((count($total_by_type[$catK]['0'])/$total_immobili_attivi)*100, 2) ?>%</td>
                            </tr>
                            <?php
                            $count++;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {
        doughnutDataValues = new Array();
        doughnutDataColors = new Array();
        doughnutDataLabels = new Array();

        for(x=0; x<=5; x++) {
            $("span.pie:eq(" + x + ")").peity("pie", {
                fill: [$('#color_imm_' + x).val(), '#d7d7d7', '#ffffff']
            })

            doughnutDataValues.push($('#count_imm_active_' + x).val()*1);
            doughnutDataColors.push($('#color_imm_' + x).val());
            doughnutDataLabels.push($('#name_imm_' + x).val());
        }


        var ctx = document.getElementById("doughnutChart").getContext("2d");
        //var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);

        var myDoughnutChart = new Chart(document.getElementById("doughnutChart"), {
            type: 'pie',
            data: {
                datasets: [{
                    data: doughnutDataValues,
                    backgroundColor: doughnutDataColors
                }],
                labels: doughnutDataLabels
            },
            options: {
                segmentShowStroke: true,
                segmentStrokeColor: "#fff",
                segmentStrokeWidth: 2,
                percentageInnerCutout: 45, // This is 0 for Pie charts
                animationSteps: 100,
                animationEasing: "easeOutBounce",
                animateRotate: true,
                animateScale: false,
                responsive: true,
            }
        });
        window.dashboardMasonry.masonry();
    });
</script>
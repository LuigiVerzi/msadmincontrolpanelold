<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('realestate_total_properties');

$MSFrameworkUsers = new \MSFramework\users();

$where_str = "";
$where_ary = array();
if(($MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-5" || $MSFrameworkUsers->getUserDataFromSession('userlevel') == "realestate-6")) {
    $where_str .= " AND owner_id = :owner_id";
    $where_ary = array_merge($where_ary, array(":owner_id" => $MSFrameworkUsers->getUserDataFromSession('user_id')));
}
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-info pull-right" href="modules/immobili/list">Vedi Immobili</a>
            <h5>Immobili</h5>
        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-xs-3">
                    <small class="stats-label">Totali</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili WHERE id != '' $where_str", $where_ary); ?></h4>
                </div>
                <div class="col-xs-3">
                    <small class="stats-label">Attivi</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili WHERE id != '' AND stato_vendita = '0' $where_str", $where_ary); ?></h4>
                </div>
                <div class="col-xs-3">
                    <small class="stats-label">Venduti</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili WHERE id != '' AND stato_vendita = '1' $where_str", $where_ary); ?></h4>
                </div>
                <div class="col-xs-3">
                    <small class="stats-label">Non Venduti</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT id FROM realestate_immobili WHERE id != '' AND stato_vendita = '2' $where_str", $where_ary); ?></h4>
                </div>
            </div>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th colspan="4">Ultimi Immobili</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($MSFrameworkDatabase->getAssoc("SELECT id, titolo, images, geo_data, creation_time FROM realestate_immobili WHERE id != '' $where_str ORDER BY creation_time desc LIMIT 4", $where_ary) as $last_ins) {
                    $images = json_decode($last_ins['images'], true);
                ?>
                    <tr>
                        <td><div style="height: 70px; width: 110px"><img src="<?php echo UPLOAD_REALESTATE_IMMOBILI_FOR_DOMAIN_HTML . $images[0] ?>" alt="" width="110" height="70"></div></td>
                        <td><?= $MSFrameworki18n->getFieldValue($last_ins['titolo']); ?></td>
                        <td><i class="fa fa-clock-o"></i> <?= (new \MSFramework\utils())->smartdate($last_ins['creation_time']); ?></td>
                        <td class="text-warning"><a href="modules/immobili/list/edit.php?id=<?= $last_ins['id']; ?>" class="text-warning">Modifica <i class="fa fa-arrow-right"></i></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th colspan="4">Ultime Modifiche</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($MSFrameworkDatabase->getAssoc("SELECT id, titolo, images, geo_data, creation_time FROM realestate_immobili WHERE id != '' AND last_update_time != creation_time $where_str ORDER BY last_update_time desc LIMIT 2", $where_ary) as $last_ins) {
                    $images = json_decode($last_ins['images'], true);
                    ?>
                    <tr>
                        <td><div style="height: 70px; width: 110px"><img src="<?php echo UPLOAD_REALESTATE_IMMOBILI_FOR_DOMAIN_HTML . $images[0] ?>" alt="" width="110" height="70"></div></td>
                        <td><?= $MSFrameworki18n->getFieldValue($last_ins['titolo']); ?></td>
                        <td><i class="fa fa-clock-o"></i> <?= (new \MSFramework\utils())->smartdate($last_ins['creation_time']); ?></td>
                        <td class="text-warning"><a href="modules/immobili/list/edit.php?id=<?= $last_ins['id']; ?>" class="text-warning">Modifica <i class="fa fa-arrow-right"></i></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {

    });

</script>
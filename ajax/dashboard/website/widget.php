<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('website')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-danger pull-right" href="javascript:void(0);" onclick="delStatsPopup()">Azzera Statistiche</a>
            <h5>Sito Web</h5>
        </div>
        <div class="ibox-content">

            <div class="web_stats_table" style="background: #fafafa; margin: -15px; padding: 15px 15px 10px;">
                <div class="row">
                    <div class="col-xs-4">
                        <a type="button" href="modules/pages/list/" class="btn btn-default m-r-sm">
                            <?php echo $MSFrameworkDatabase->getAssoc("SELECT COUNT(id) as count FROM pagine", array(), true)['count']; ?>
                        </a>
                        Pagine
                    </div>
                    <div class="col-xs-4">
                        <a type="button" href="modules/recensioni/" class="btn btn-warning m-r-sm">
                            <?php echo $MSFrameworkDatabase->getAssoc("SELECT COUNT(id) as count FROM recensioni", array(), true)['count']; ?>
                        </a>
                        Recensioni
                    </div>
                    <div class="col-xs-4">
                        <a type="button" href="modules/richieste/generiche/" class="btn btn-success m-r-sm">
                            <?php echo $MSFrameworkDatabase->getAssoc("SELECT COUNT(id) as count FROM richieste WHERE type = 'generic'", array(), true)['count']; ?>
                        </a>
                        Richieste
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <div class="row">

                <div class="col-xs-4">
                    <small class="stats-label">Visite ultimi 7 giorni</small>
                    <h4><?= (int)$MSFrameworkDatabase->getAssoc("SELECT SUM(n_visite) as visits  FROM `stats_visitatori` WHERE STR_TO_DATE(CONCAT(day, ' ', hour, ':', minute), '%Y-%m-%d %H:%i') > (NOW() - INTERVAL 7 DAY) ", array(), true)['visits']; ?></h4>
                </div>

                <div class="col-xs-4">
                    <small class="stats-label">Visite ultimi 30 giorni</small>
                    <h4><?= (int)$MSFrameworkDatabase->getAssoc("SELECT SUM(n_visite) as visits FROM `stats_visitatori` WHERE STR_TO_DATE(CONCAT(day, ' ', hour, ':', minute), '%Y-%m-%d %H:%i') > (NOW() - INTERVAL 30 DAY)", array(), true)['visits']; ?></h4>
                </div>

                <div class="col-xs-4">
                    <small class="stats-label">Permanenza media</small>
                    <h4>
                        <?php
                        $tempo_secondi = (int)$MSFrameworkDatabase->getAssoc('SELECT avg(tempo_medio) as tempo_medio FROM stats_visitatori WHERE tempo_medio > 0', array(), true)['tempo_medio'];
                        $dt_diff = (new \DateTime('@0'))->diff((new \DateTime("@$tempo_secondi")));

                        $ary_tempi = array("a" => "g", "h" => "h", "i" => "m", "s" => "s");
                        $str_tempo = "";
                        foreach($ary_tempi as $tK => $tV) {
                            $str_tempo .= ($dt_diff->format('%' . $tK) != "0" ? $dt_diff->format('%' . $tK) . $tV . ", " : "");
                        }

                        echo ($str_tempo != "" ? substr($str_tempo, 0, -2) : '0s');
                        ?>
                    </h4>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <h5>Visite ultime 24h</h5>
            <h2><?= (int)$MSFrameworkDatabase->getAssoc("SELECT SUM(n_visite) as visits FROM `stats_visitatori` WHERE STR_TO_DATE(CONCAT(day, ' ', hour, ':', minute), '%Y-%m-%d %H:%i') > (NOW() - INTERVAL 24 HOUR)", array(), true)['visits']; ?></h2>
            <div id="today_visits_sparkline"></div>
        </div>

    </div>
</div>

<div id="stats_del_bootbox" style="display: none;">
    <div class="row">
        <div class="col-sm-12">
            <label>Seleziona periodo</label>
            <select id="stats_del_period" class="form-control">
                <option value="1">Elimina tutto</option>
                <option value="2">Elimina mese corrente</option>
                <option value="3">Elimina mese precedente</option>
                <option value="4">Elimina anno corrente</option>
                <option value="5">Elimina anno precedente</option>
                <option value="6">Elimina intervallo di date personalizzato</option>
            </select>
        </div>
    </div>

    <div class="row" id="date_range" style="display: none; margin-top: 20px;">
        <div class="col-sm-6">
            <label>Dal</label>
            <form class="bootbox-form"><input class="bootbox-input bootbox-input-date form-control" id="stats_del_period_from" autocomplete="off" type="date"></form>
        </div>

        <div class="col-sm-6">
            <label>Al</label>
            <form class="bootbox-form"><input class="bootbox-input bootbox-input-date form-control" id="stats_del_period_to" autocomplete="off" type="date"></form>
        </div>
    </div>
</div>

<?php

/* OTTENGO IL GRAFICO DELLE VISITE NELLE ULTIME 24H */
$today_visits = $MSFrameworkDatabase->getAssoc("SELECT hour, SUM(n_visite) as visite_totali FROM `stats_visitatori` WHERE STR_TO_DATE(CONCAT(day, ' ', hour, ':', minute), '%Y-%m-%d %H:%i') > (NOW() - INTERVAL 24 HOUR) GROUP BY hour");

$begin = (new DateTime(date()))->modify('-1 day');
$end = new DateTime(date());

$today_visits_label = array();
$today_visits_chart = array();

for($i = $begin; $i <= $end; $i->modify('+1 hour')){
    $hour = $i->format('H');
    $today_visits_label[$hour] = $hour . ':00';
    $today_visits_chart[$hour] = 0;

    foreach($today_visits as $hour_visit) {
        if($hour_visit['hour'] == $hour) $today_visits_chart[$hour] = (int)$hour_visit['visite_totali'];
    }

}
$today_visits_trend = (new \MSFramework\utils())->getArrayValuesTrend($today_visits_chart);
$today_visits_color = ($today_visits_trend == 1 ? '1ab394' : ($today_visits_trend == 0 ? 'ed5565' : '23c6c8'));
?>

<script>
    window.widgets_functions.push(function () {
        var sparklineCharts = function() {
            $("#today_visits_sparkline").sparkline(<?= json_encode(array_values($today_visits_chart)); ?>, {
                type: 'line',
                width: '100%',
                height: '85',
                lineColor: '#<?= $today_visits_color; ?>',
                fillColor: "#ffffff",
                tooltipFormat: 'Ore {{offset:offset}}<br><b>{{y:val}} visite</b>',
                tooltipValueLookups: {
                    'offset': <?= json_encode(array_values($today_visits_label)); ?>
                }
            });
        }

        var sparkResize;
        $(window).resize(function(e) {
            clearTimeout(sparkResize);
            sparkResize = setTimeout(sparklineCharts, 500);
        });
        sparklineCharts();

    });

    function delStatsPopup() {
        var dialog = bootbox.dialog({
            title: 'Seleziona un periodo o elimina tutto',
            message: $('#stats_del_bootbox').html(),
            buttons: {
                cancel: {
                    label: "Annulla",
                    className: 'btn-info'
                },
                ok: {
                    label: "Azzera statistiche",
                    className: 'btn-danger',
                    callback: function(){
                        $.ajax({
                            url: "ajax/dashboard/website/ajax.php?action=deleteStats",
                            type: 'post',
                            data: {
                                type: $('.modal-dialog #stats_del_period').val(),
                                from: $('.modal-dialog #stats_del_period_from').val(),
                                to: $('.modal-dialog #stats_del_period_to').val(),
                            },
                            async: true,
                            datatype: "json",
                            success: function (data) {
                                if(data.status == "invalid_format") {
                                    bootbox.error('Il formato delle date selezionate non è corretto!');
                                } else if(data.status == "invalid_period") {
                                    bootbox.error('Il periodo selezionato non è valido!');
                                } else {
                                    bootbox.alert("Le statistiche per il periodo selezionato sono state azzerate correttamente", function(){ location.reload(); })
                                }
                            }
                        })
                    }
                }
            }
        });

        dialog.init(function(){
            $('.modal-dialog #stats_del_period').unbind('change');
            $('.modal-dialog #stats_del_period').on('change', function() {
                $('.modal-dialog #date_range').hide();
                if($(this).val() == "6") {
                    $('.modal-dialog #date_range').show();
                }
            })
        });
    }
</script>
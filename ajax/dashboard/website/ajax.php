<?php
require_once('../../../sw-config.php');

$action = $_GET['action'];

if($action == 'deleteStats') {
    if($_POST['type'] == "1") {
        $MSFrameworkDatabase->getAssoc("DELETE FROM stats_visitatori");
    } else {
        if($_POST['type'] == "2") {
            $from = date('Y-m-01');
            $to = date('Y-m-t');
        } else if($_POST['type'] == "3") {
            $prev_month = strtotime('-1 month');

            $from = date('Y-m-01', $prev_month);
            $to  = date('Y-m-t', $prev_month);
        } else if($_POST['type'] == "4") {
            $from = date('Y-01-01');
            $to  = date('Y-12-31');
        } else if($_POST['type'] == "5") {
            $prev_year = strtotime('-1 year');

            $from = date('Y-01-01', $prev_year);
            $to  = date('Y-12-31', $prev_year);
        } else if($_POST['type'] == "6") {
            $from = false;
            $to = false;

            if(strtotime($_POST['from'])) {
                $from = date("Y-m-d", strtotime($_POST['from']));
            }

            if(strtotime($_POST['to'])) {
                $to = date("Y-m-d", strtotime($_POST['to']));
            }

            if(!$from || !$to) {
                echo json_encode(array("status" => "invalid_format"));
                die();
            }

            if(strtotime($from) > strtotime($to)) {
                echo json_encode(array("status" => "invalid_period"));
                die();
            }
        }

        $MSFrameworkDatabase->getAssoc("DELETE FROM stats_visitatori WHERE day BETWEEN :from AND :to", array(":from" => $from, ":to" => $to));
    }

    echo json_encode(array("status" => "ok"));
}
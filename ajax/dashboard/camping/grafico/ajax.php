<?php
require_once('../../../../sw-config.php');

$action = $_GET['action'];

if($action == 'prenotazioni') {

    $prenotazioni = array('data' => array());

    if(strtotime($_POST['start']) <= strtotime('2000-01-01')) {
        $_POST['start'] = date('Y-m-d', strtotime($MSFrameworkDatabase->getAssoc("SELECT creation_time FROM prenotazioni ORDER BY id ASC LIMIT 1", array(), true)['creation_time']));
    }

    $start = $_POST['start'];
    $end = $_POST['end'];

    $grafico_ordini = array();
    foreach($MSFrameworkDatabase->getAssoc("SELECT ospiti, date(creation_time) as day FROM prenotazioni WHERE creation_time >= '$start' AND creation_time <= '$end'") as $r) {

        if(!isset($grafico_ordini[$r['day']])) $grafico_ordini[$r['day']] = array('booking' => 0, 'ospiti' => array(0, 0));

        $ospiti = json_decode($r['ospiti'], true);

        $grafico_ordini[$r['day']]['booking'] += 1;
        $grafico_ordini[$r['day']]['ospiti'][0] += $ospiti[0];
        $grafico_ordini[$r['day']]['ospiti'][1] += $ospiti[1];
    }

    $start =  new \DateTime($start);
    $end =  new \DateTime($end);

    for($i = $start; $i <= $end; $i->modify('+1 day')) {
        if(isset($grafico_ordini[$i->format("Y-m-d")])) $prenotazioni['data'][] = $grafico_ordini[$i->format("Y-m-d")];
        else $prenotazioni['data'][] = array('booking' => 0, 'ospiti' => array(0, 0));
    }

    $children_this_period = 0;
    $adult_this_period = 0;
    $booking_this_period = 0;

    foreach($prenotazioni['data'] as $k=>$giorno) {
        $children_this_period += $giorno['ospiti'][0];
        $adult_this_period += $giorno['ospiti'][1];
        $prenotazioni['data'][$k] = $giorno['booking'];
    }

    $start = new DateTime($_POST['start']);
    $end = new DateTime($_POST['end']);

    $nomi_mesi = array('Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic');

    $labels = array();
    for($i = $start; $i <= $end; $i->modify('+1 day')) {
        $labels[] = (int)$i->format("d") . ' ' . $nomi_mesi[(int)$i->format("m")-1];
    }

    die(json_encode(array('graph' => $prenotazioni, 'labels' => $labels, 'children_this_period' => $children_this_period, 'adult_this_period' => $adult_this_period)));
}
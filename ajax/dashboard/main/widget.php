<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('main');

?>

<div class="row dashboard-header">

    <div class="col-md-3 col-sm-4">
        <h2 style="margin: 0;">Bentornato <?= $userDetails['nome']; ?> <?= $userDetails['cognome']; ?></h2>
        <small>Ecco le schede che utilizzi maggiormente</small>
        <ul class="list-group clear-list m-t" style="margin-bottom: 15px;">
            <?php
            $i = 0;
            if($most_used_modules) {
                foreach (array_slice($most_used_modules, 0, 6) as $moduleID => $modulePriority) {
                    $i++;
                    $mostUsedModuleDetails = $MSSoftwareModules->getModuleDetailsFromID($moduleID);

                    if(!$mostUsedModuleDetails) {
                        continue;
                    }

                    $moduleColors = array(
                        '1' => 'primary',
                        '2' => 'success',
                        '3' => 'warning',
                        '4' => 'inverse',
                        '5' => 'inverse'
                    )[$i];

                    echo '<li class="list-group-item ' . ($i == 1 ? 'fist-item' : '') . '">
                                <a style="display: block;" href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . $mostUsedModuleDetails['path'] . '">
                                    <span class="label label-' . $moduleColors . '"><i style="width: 15px" class="fa fa-' . $mostUsedModuleDetails['fa_icon'] . '"></i></span> ' . $mostUsedModuleDetails['name'] . '
                                </a>
                            </li>';
                }
            } else {
                echo '<li class="list-group-item fist-item"><div class="text-center"><h4 style="margin-top: 10px; color: #b8b8b8;">Inizia a navigare tra i moduli per visualizzare i più utilizzati qui.</h4><i class="fa fa-history big-icon"></i></div></li>';
            }
            ?>
        </ul>
    </div>
    <div class="col-md-6 col-sm-8">
        <div class="row">
            <div class="col-xs-8 ">
                <h3 class="font-bold no-margins">
                    Avvenimenti recenti
                </h3>
                <small class="last_actions_chart_time">Stai visualizzando <b>Ultimi 7 giorni</b>.</small>
            </div>
            <div class="col-xs-4">
                <div class="text-right">
                    <input type="text" name="daterange" id="last_actions_daterange" value="Ultimi 7 giorni" class="form-control"/>
                </div>
            </div>
        </div>

        <canvas id="lastActionsChart" height="100" width="100%" style="min-height: 265px; max-height: 265px; margin-top: 15px;"></canvas>

    </div>

    <div class="col-md-3 col-sm-12">
        <div class="last_activities_streambox" data-current_page="1"></div>
    </div>
</div>



<script>
    window.widgets_functions.push(function () {
        $('.last_activities_streambox').on('scroll', function () {

            if($(this).hasClass('loading')) return false;

            if(($(this)[0].scrollHeight - 50) < $(this).height()+ $(this).scrollTop()) {
                var page = $(this).data('current_page');

                $(this).addClass('loading');
                loadDashboardCustomersHistory(page);
            }

        });
        loadDashboardCustomersHistory(1);

        /* INIZIALIZZAZIONE GRAFICO ORDINI */
        $('#last_actions_daterange').daterangepicker({
            startDate: moment().subtract(6, 'days'),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY',
                applyLabel: "Cerca",
                cancelLabel: "Annulla",
                fromLabel: "Da",
                toLabel: "A",
                customRangeLabel: "Personalizzata",
            },
            ranges: {
                'Ultimi 7 giorni': [moment().subtract(6, 'days'), moment()],
                'Ultimi 30 giorni': [moment().subtract(30, 'days'), moment()],
                'Questo mese': [moment().startOf('month'), moment().endOf('month')],
                'Mese precedente': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Quest\'anno': [moment().startOf('year'), moment().endOf('year')]
            },
            showCustomRangeLabel: false,
            autoUpdateInput: false
        }, function(start, end, label) {
            updateLastActionsOrderGraph(start, end);
            $('#last_actions_daterange').val(label);
            $('.last_actions_chart_time b').html(label);
        });
        updateLastActionsOrderGraph(moment().subtract(6, 'days'), moment());

    });

    function updateLastActionsOrderGraph(start, end) {

        $.ajax({
            url: "<?= ABSOLUTE_SW_PATH_HTML . $widget_info['path']; ?>/ajax.php?action=grafico",
            type: 'post',
            data: {start: start.format('YYYY-MM-DD'), end: end.format('YYYY-MM-DD')},
            async: true,
            dataType: 'json',
            success: function(data) {

                if(typeof(window.last_actions_chart) != 'undefined') window.last_actions_chart.destroy();

                var lineData = {
                    labels: data.labels,
                    datasets: data.graph
                };

                var lineOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.datasets[tooltipItems.datasetIndex].label.replace(/(<([^>]+)>)/ig,"") +': ' + tooltipItems.yLabel;
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return Math.round(value * 100) / 100;
                                }
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                autoSkip: true,
                                maxTicksLimit: 7
                            }
                        }]
                    }
                };

                var ctx = document.getElementById("lastActionsChart").getContext("2d");
                window.last_actions_chart = new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
                window.dashboardMasonry.masonry()
            }
        });
    }


    function loadDashboardCustomersHistory(page) {
        $.ajax({
            url: "<?= ABSOLUTE_SW_PATH_HTML . $widget_info['path']; ?>/ajax.php?action=history",
            type: "POST",
            data: {
                "page": page
            },
            async: true,
            success: function(data) {
                if(data != 'end') {
                    var $block = $('.last_activities_streambox');
                    $block.removeClass('loading');
                    $block.data('current_page', ($block.data('current_page')+1));
                    $block.append(data);
                    initMSTooltip();
                }
            }
        });
    }

</script>
<?php
require_once('../../../sw-config.php');

$action = $_GET['action'];

$MSFrameworkCustomers = new \MSFramework\customers();

if($action == 'history') {
    $page = $_POST['page'];
    $histories = $MSFrameworkCustomers->getCustomerHistories('*', $page);
    if(!$histories) {
        if($page == 1) {
            die('<div class="text-center"><h2 style="margin-top: 65px; color: #b8b8b8;">Nessun evento recente</h2><i class="fa fa-comment-o big-icon"></i></div>');
        } else {
            die('end');
        }
    }
    ?>
    <div class="activity-stream">

        <?php foreach($histories as $action) { ?>

            <?php
            $customer_info = array();
            if(is_numeric($action['user']['id'])) {
                $customer_info = $MSFrameworkCustomers->getCustomerDataFromDB($action['user']['id']);
            } else if(!empty($action['user']['email'])) {
                $customer_info = $MSFrameworkCustomers->getCustomerDataByEmail($action['user']['email']);
            }
            ?>

            <div class="stream">
                <div class="stream-badge">
                    <i class="fa fa-<?= $action['timeline']['icon']; ?> <?= $action['timeline']['bg']; ?>-bg text-white"></i>
                </div>
                <div class="stream-panel">
                    <div class="stream-info">
                        <?php
                        $avatar_url = ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png';
                        ?>
                        <?php if($customer_info) { ?>
                            <?php
                            if(json_decode($customer_info['avatar'])) {
                                $avatar_url = UPLOAD_CUSTOMER_AVATAR_FOR_DOMAIN_HTML . json_decode($customer_info['avatar'], true)[0];
                            }
                            ?>
                            <a href="#">
                                <img src="<?= $avatar_url; ?>">
                            </a>
                            <span><a href="modules/customers/histories/edit.php?id=<?= $customer_info['id']; ?>"><?= (!empty($customer_info['nome']) ? htmlentities($customer_info['nome'] . ' ' . $customer_info['cognome']) : $customer_info['email']); ?></a></span>
                        <?php } else { ?>
                            <img src="<?= $avatar_url; ?>">
                            <span><small class="text-muted" style="text-decoration: line-through;"><?= htmlentities($action['user']['email']); ?></small></span>
                        <?php } ?>
                        <span class="date" title="<?= date('d/m/Y H:i', $action['date']); ?>"><?= (new \MSFramework\utils())->smartdate($action['date']); ?></span>
                        <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= htmlentities($action['description']); ?>"></i></span>
                    </div>
                    <?= $action['name']; ?>
                </div>
            </div>
        <?php } ?>
    </div>
<?php
} else if($_GET['action'] == 'grafico') {
    $ordini = array('data' => array());

    $sql = $MSFrameworkCustomers->getCustomerHistorySQL();
    $sql .= "AND date >= " . strtotime($_POST['start']) . " AND date <= " . strtotime($_POST['end'] . " 23:59") . "";
    $sql .= ' ORDER by date ASC';

    $tipologie = array();
    $azioni = $MSFrameworkDatabase->getAssoc($sql);

    $all_actions = array();

    foreach($azioni as $azione) {

        $history = $MSFrameworkCustomers->formatCustomerHistories($azione);

        $type_key = array_search($history['name'], array_column($tipologie, 'label'));
        if($type_key === FALSE) {
            $tipologie[] = array(
                "label" => $history['name'],
                "backgroundColor" => (new \MSFramework\utils())->hex2rgba($history['timeline']['hex'], 0.5),
                "borderColor" =>  (new \MSFramework\utils())->hex2rgba($history['timeline']['hex'], 0.7),
                "pointBackgroundColor" => $history['timeline']['hex'],
                "pointBorderColor" => "#fff",
                "data" => array()
            );
            $type_key = array_search($history['name'], array_column($tipologie, 'label'));
        }

        if(!isset($all_actions[$type_key][date('Y-m-d', $azione['date'])])) {
            $all_actions[$type_key][date('Y-m-d', $azione['date'])] = 1;
        } else {
            $all_actions[$type_key][date('Y-m-d', $azione['date'])]++;
        }

    }


    $start = new DateTime($_POST['start']);
    $end = new DateTime($_POST['end']);

    $nomi_mesi = array('Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic');

    $labels = array();
    for($i = $start; $i <= $end; $i->modify('+1 day')) {
        foreach($tipologie as $k => $tipologia) {
            array_push($tipologie[$k]['data'], (isset($all_actions[$k][$i->format("Y-m-d")]) ? $all_actions[$k][$i->format("Y-m-d")] : 0));
        }
        $labels[] = (int)$i->format("d") . ' ' . $nomi_mesi[(int)$i->format("m")-1] . (round($dateDifference / (60 * 60 * 24)) > 60 ? ' ' . (int)$i->format("Y") : '');
    }


    die(json_encode(array('graph' => $tipologie, 'labels' => $labels)));
}
?>

<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('ms_agency_guadagni')
?>

<div class="dashboard-widget" style="width: 100%;">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-4 col-md-push-8">
                    <div class="text-right">
                        <input type="text" name="daterange" id="ms_agency_guadagni_daterange" value="Ultimi 30 giorni" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-8 col-md-pull-4">
                    <h3 class="font-bold no-margins">
                        Andamento Guadagni
                    </h3>
                    <small class="ms_agency_guadagni_chart_time">Stai visualizzando <b>Ultimi 30 giorni</b>.</small>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-8">
                    <canvas id="msAgencyGuadagniChart" height="300"></canvas>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-sm-6"  style="margin-bottom: 10px;">
                            <h2 class="no-margins" id="ms_agency_guadagni_chart_total_earn"></h2>
                            <small>Guadagni</small>
                            <div class="progress progress-mini">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                        <div class="col-sm-6" style="margin-bottom: 10px;">
                            <h2 class="no-margins" id="ms_agency_guadagni_chart_total_vendite"></h2>
                            <small>Ordini</small>
                            <div class="progress progress-mini">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>

                    <?php
                    $totale = $MSFrameworkDatabase->getAssoc("SELECT SUM(earning) as totale FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = :user_id AND withdrawal_date = 0", array(':user_id' => $MSFrameworkUsers->getUserDataFromSession('id')), true)['totale'];
                    $totale_prelevato = $MSFrameworkDatabase->getAssoc("SELECT SUM(earning) as totale FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = :user_id AND withdrawal_date != 0", array(':user_id' => $MSFrameworkUsers->getUserDataFromSession('id')), true)['totale'];
                    ?>

                    <div class="ibox" style="margin-top: 10px; margin-bottom: 10px;">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-6">

                                    <h1 class="no-margins text-navy"><?= number_format($totale, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                                    <small>Credito disponibile</small>

                                </div>
                                <div class="col-lg-6">
                                    <?php if($totale > 0) { ?>
                                    <a style="margin-top: 3px;" href="<?= $MSSoftwareModules->getModuleUrlByID("ms_agency_guadagni"); ?>" class="btn btn-block btn-lg btn-primary"><i class="fas fa-credit-card"></i> Richiedi prelievo</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox" style="margin: 0;">
                        <div class="ibox-content">
                            <h1 class="no-margins text-success"><?= number_format($totale_prelevato, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h1>
                            <small>Credito prelevato</small>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.widgets_functions.push(function () {

        /* INIZIALIZZAZIONE GRAFICO ORDINI */
        $('#ms_agency_guadagni_daterange').daterangepicker({
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY',
                applyLabel: "Cerca",
                cancelLabel: "Annulla",
                fromLabel: "Da",
                toLabel: "A",
                customRangeLabel: "Personalizzata",
            },
            ranges: {
                'Ultimi 7 giorni': [moment().subtract(6, 'days'), moment()],
                'Ultimi 30 giorni': [moment().subtract(30, 'days'), moment()],
                'Questo mese': [moment().startOf('month'), moment().endOf('month')],
                'Mese precedente': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Dall\'inizio': [0, moment()]
            },
            showCustomRangeLabel: false,
            autoUpdateInput: false
        }, function(start, end, label) {
            updateMSAgencyGuadagniGraph(start, end);
            $('#ms_agency_guadagni_daterange').val(label);
            $('.ms_agency_guadagni_chart_time b').html(label);
        });
        updateMSAgencyGuadagniGraph(moment().subtract(30, 'days'), moment());

    });

    function updateMSAgencyGuadagniGraph(start, end) {

        $.ajax({
            url: "<?= ABSOLUTE_SW_PATH_HTML . $widget_info['path']; ?>/ajax.php?action=vendite",
            type: 'post',
            data: {start: start.format('YYYY-MM-DD'), end: end.format('YYYY-MM-DD')},
            async: true,
            dataType: 'json',
            success: function(data) {

                if(typeof(window.ms_agency_guadagni_chart) != 'undefined') window.ms_agency_guadagni_chart.destroy();

                $('#ms_agency_guadagni_chart_total_earn').html(data.earn_this_period + '<?= CURRENCY_SYMBOL; ?>');
                $('#ms_agency_guadagni_chart_total_vendite').html(data.orders_this_period);

                $('#ms_agency_guadagni_chart_total_earn_past').html(data.earn_past_period + '<?= CURRENCY_SYMBOL; ?>');
                $('#ms_agency_guadagni_chart_total_vendite_past').html(data.orders_past_period);

                var lineData = {
                    labels: data.labels,
                    datasets: [
                        {
                            label: "Preventivo",
                            backgroundColor: "rgba(26, 179, 148,0.5)",
                            borderColor: "rgba(26, 179, 148,0.7)",
                            pointBackgroundColor: "rgba(26, 179, 148,1)",
                            pointBorderColor: "#fff",
                            data:  data.graph.data
                        }
                    ]
                };

                var lineOptions = {
                    legend: {
                        display: false
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + '<?= CURRENCY_SYMBOL; ?>';
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return Math.round(value * 100) / 100 + '<?= CURRENCY_SYMBOL; ?>';
                                }
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                autoSkip: true,
                                maxTicksLimit: 7
                            }
                        }]
                    }
                };

                var ctx = document.getElementById("msAgencyGuadagniChart").getContext("2d");
                window.ms_agency_guadagni_chart = new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
                if(typeof(window.dashboardMasonry) !== 'undefined') {
                    window.dashboardMasonry.masonry();
                }
            }
        });
    }

</script>
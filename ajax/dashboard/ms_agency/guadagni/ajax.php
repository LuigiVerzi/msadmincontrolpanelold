<?php
require_once('../../../../sw-config.php');

$action = $_GET['action'];

if($action == 'vendite') {
    $ordini = array('data' => array());

    $MSAgencyEarnings = new MSFramework\MSAgency\earnings();

    if(strtotime($_POST['start']) <= strtotime('2000-01-01')) {
       $date = $MSFrameworkDatabase->getAssoc("SELECT date FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE user_id = '" . $MSFrameworkUsers->getUserDataFromSession('id') . "' ORDER BY date ASC LIMIT 1", array(), true)['date'];

       if($date) {
           $_POST['start'] = date('Y-m-d', strtotime($date));
       }
    }

    $secondi_differenza = (int)(strtotime($_POST['end']) - strtotime($_POST['start']));

    $ordini['data'] = $MSAgencyEarnings->getDateRangeChart($_POST['start'], $_POST['end']); // Periodo attuale


    $earn_this_period = 0;
    $orders_this_period = 0;

    foreach($ordini['data'] as $k => $giorno) {

        $earn_this_period += $giorno['earn'];
        $orders_this_period += $giorno['orders'];

        $ordini['data'][$k] = $giorno['earn'];
    }

    $start = new DateTime($_POST['start']);
    $end = new DateTime($_POST['end']);

    $nomi_mesi = array('Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic');

    $labels = array();
    for($i = $start; $i <= $end; $i->modify('+1 day')) {
        $labels[] = (int)$i->format("d") . ' ' . $nomi_mesi[(int)$i->format("m")-1];
    }


    die(json_encode(array(
        'graph' => $ordini,
        'labels' => $labels,
        'earn_this_period' => number_format($earn_this_period, 2, ',', '.'),
        'orders_this_period' => $orders_this_period
    )));
}
<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('hotel_grafico');
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">

        <div class="ibox-content">
            <div class="row">
                <div class="col-md-4 col-md-push-8">
                    <div class="text-right">
                        <input type="text" name="daterange" id="booking_daterange" value="Ultimi 30 giorni" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-8 col-md-pull-4">
                    <h3 class="font-bold no-margins">
                        Prenotazioni Ricevute
                    </h3>
                    <small class="booking_chart_time">Stai visualizzando <b>Ultimi 30 giorni</b>.</small>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <canvas id="bookingChart" height="300"></canvas>
        </div>

        <div class="ibox-content">
            <div class="stat-list row">
                <div class="col-xs-6">
                    <h2 class="no-margins" id="booking_total_adult"></h2>
                    <small>Posti Letto Adulti</small>
                    <div class="progress progress-mini">
                        <div class="progress-bar" style="width: 100%;"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <h2 class="no-margins" id="booking_total_children"></h2>
                    <small>Posti Letto bambini</small>
                    <div class="progress progress-mini">
                        <div class="progress-bar" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <small>
                <strong>NB:</strong> Questi dati non devono considerarsi prenotazioni effettive ma solo richieste di prenotazioni.
            </small>
        </div>

    </div>
</div>

<script>
    window.widgets_functions.push(function () {

        /* INIZIALIZZAZIONE GRAFICO ORDINI */
        $('#booking_daterange').daterangepicker({
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY',
                applyLabel: "Cerca",
                cancelLabel: "Annulla",
                fromLabel: "Da",
                toLabel: "A",
                customRangeLabel: "Personalizzata",
            },
            ranges: {
                'Ultimi 7 giorni': [moment().subtract(6, 'days'), moment()],
                'Ultimi 30 giorni': [moment().subtract(30, 'days'), moment()],
                'Questo mese': [moment().startOf('month'), moment().endOf('month')],
                'Mese precedente': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Dall\'inizio': [0, moment()]
            },
            showCustomRangeLabel: false,
            autoUpdateInput: false
        }, function(start, end, label) {
            updateBookingGraph(start, end);
            $('#booking_daterange').val(label);
            $('.booking_chart_time b').html(label);
        });
        updateBookingGraph(moment().subtract(30, 'days'), moment());

    });

    function updateBookingGraph(start, end) {

        $('#booking_graph_start_date').html(start.format('DD/MM/YYYY'));

        $.ajax({
            url: "<?= ABSOLUTE_SW_PATH_HTML . $widget_info['path']; ?>/ajax.php?action=prenotazioni",
            type: 'post',
            data: {start: start.format('YYYY-MM-DD'), end: end.format('YYYY-MM-DD')},
            async: true,
            dataType: 'json',
            success: function(data) {

                if(typeof(window.booking_chart) != 'undefined') window.booking_chart.destroy();

                $('#booking_total_children').html(data.children_this_period);
                $('#booking_total_adult').html(data.adult_this_period);

                var array_data = data.graph.data;
                var lineData = {
                    labels: data.labels,
                    datasets: [
                        {
                            label: "Prenotazioni",
                            backgroundColor: "rgba(139,195,74,0.5)",
                            borderColor: "rgba(139,195,74,0.7)",
                            pointBackgroundColor: "rgba(139,195,74,1)",
                            pointBorderColor: "#fff",
                            data: array_data
                        }
                    ]
                };

                var lineOptions = {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    maintainAspectRatio: false,
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel;
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0,
                                stepSize: 1
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                autoSkip: true,
                                maxTicksLimit: 7
                            }
                        }]
                    }
                };

                var ctx = document.getElementById("bookingChart").getContext("2d");
                window.booking_chart = new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
                window.dashboardMasonry.masonry()
            }
        });
    }

</script>
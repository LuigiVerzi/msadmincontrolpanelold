<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('richieste_generiche')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-info pull-right" href="modules/richieste/generiche">Vedi Richieste</a>
            <h5>Richieste Ricevute</h5>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <tbody>
                <?php foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, contatto, messaggio, creation_time FROM richieste WHERE type LIKE 'generic' ORDER BY id DESC LIMIT 5") as $richiesta) { ?>
                    <tr>
                        <td><?= $richiesta['nome']; ?></td>
                        <td><i class="fa fa-clock-o"></i> <?= (new \MSFramework\utils())->smartdate($richiesta['creation_time']); ?></td>
                        <td class="text-navy"><a href="modules/richieste/generiche/edit.php?id=<?= $richiesta['id']; ?>" class="text-navy">Vedi <i class="fa fa-arrow-right"></i></a></td>
                        <td><span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= htmlentities($richiesta['messaggio']); ?>"></i></span></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {

    });

</script>
<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('customers_histories')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-inverse pull-right" href="modules/customers/histories">Vedi Cronologie</a>
            <h5>Cronologia Clienti</h5>
        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-xs-4">
                    <small class="stats-label">Clienti Totali</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT * FROM customers"); ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Non Convalidati</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT * FROM customers WHERE mail_auth != ''"); ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Iscritti alla newsletter</small>
                    <h4><?= $MSFrameworkDatabase->getAssoc("SELECT COUNT(id) as count FROM newsletter__destinatari WHERE auth = ''", array(), true)['count']; ?></h4>
                </div>
            </div>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Azione</th>
                    <th>Data</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach((new \MSFramework\customers())->getCustomerHistories('') as $action) { ?>

                    <?php
                    $customer_info = array();
                    if(is_numeric($action['user']['id'])) {
                        $customer_info = (new \MSFramework\customers())->getCustomerDataFromDB($action['user']['id']);
                    } else if(!empty($action['user']['email'])) {
                        $customer_info = (new \MSFramework\customers())->getCustomerDataByEmail($action['user']['email']);
                    }
                    ?>

                    <tr>
                        <?php if($customer_info) { ?>
                        <td><a href="modules/customers/list/edit.php?id=<?= $customer_info['id']; ?>"><?= $customer_info['nome'] . ' ' . $customer_info['cognome']; ?></a></td>
                        <?php } else { ?>
                        <td><small class="text-muted" style="text-decoration: line-through;"><?= $action['user']['email']; ?></small></td>
                        <?php } ?>
                        <td><?= $action['name']; ?></td>
                        <td><i class="fa fa-clock-o"></i> <?= (new \MSFramework\utils())->smartdate($action['date']); ?></td>
                        <td class="text-navy"><a href="modules/customers/histories/edit.php?id=<?= $action['user']['id']; ?>" class="text-navy">Vedi <i class="fa fa-arrow-right"></i></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {

    });

</script>
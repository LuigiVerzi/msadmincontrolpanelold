<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('customers')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-info pull-right" href="modules/customers/list">Vedi Clienti</a>
            <h5>Clienti</h5>
        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-xs-4">
                    <small class="stats-label">Clienti Totali</small>
                    <h4><?= (int)$MSFrameworkDatabase->getAssoc("SELECT COUNT(*) as total FROM customers", array(), true)['total']; ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Non Convalidati</small>
                    <h4><?= (int)$MSFrameworkDatabase->getAssoc("SELECT COUNT(*) as total FROM customers WHERE mail_auth != ''")['total']; ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Iscritti alla newsletter</small>
                    <h4><?= $MSFrameworkDatabase->getAssoc("SELECT COUNT(id) as count FROM newsletter__destinatari WHERE auth = ''", array(), true)['count']; ?></h4>
                </div>
            </div>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th>Ultimi Clienti</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, registration_date FROM customers ORDER BY id DESC LIMIT 5") as $user) { ?>
                    <tr>
                        <td><?= $user['nome'] . ' ' . $user['cognome']; ?></td>
                        <td><i class="fa fa-clock-o"></i> <?= (new \MSFramework\utils())->smartdate(strtotime($user['registration_date'])); ?></td>
                        <td class="text-navy"><a href="modules/customers/list/edit.php?id=<?= $user['id']; ?>" class="text-navy">Vedi <i class="fa fa-arrow-right"></i></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {

    });

</script>
<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('customers_bilancio');

$stati_cliente = array();

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM customers_balance") as $operazione) {
    if(!isset($stati_cliente[$operazione['cliente']])) $stati_cliente[$operazione['cliente']] = 0.0;
    if($operazione['tipo'] == 'debito')  $stati_cliente[$operazione['cliente']] -= (float)$operazione['valore'];
    else $stati_cliente[$operazione['cliente']] += (float)$operazione['valore'];
}

$crediti = 0.0;
$debiti = 0.0;

foreach($stati_cliente as $stato_cliente) {
    if($stato_cliente > 0) {
        $crediti += abs($stato_cliente);
    }
    else {
        $debiti += abs($stato_cliente);
    }
}

$bilancio = $debiti-$crediti;

?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-success pull-right" href="modules/customers/balance">Vedi Bilancio</a>
            <h5>Bilancio Clienti</h5>
        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-xs-4">
                    <small class="stats-label">Debiti Clienti</small>
                    <h4><?= number_format($debiti, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Crediti Clienti</small>
                    <h4><?= number_format($crediti, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Bilancio Complessivo</small>
                    <h4 class="text-<?= ($bilancio >= 0 ? 'navy' : 'danger'); ?>"><?= number_format($bilancio, 2, ',', '.'); ?> <?= CURRENCY_SYMBOL; ?></h4>
                </div>
            </div>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th>Ultimi Movimenti</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($MSFrameworkDatabase->getAssoc("SELECT *, (SELECT CONCAT(nome, ' ', cognome) FROM customers WHERE customers.id = customers_balance.cliente) as cliente FROM customers_balance ORDER BY id DESC LIMIT 5") as $v) { ?>
                    <tr>
                        <td><?= $v['cliente']; ?></td>
                        <td><span class="label label-<?= ($v['tipo'] == 'debito' ? 'danger' : 'primary'); ?>"><?= ucfirst($v['tipo']); ?></span></td>
                        <td class="text-<?= ($v['tipo'] == 'debito' ? 'danger' : 'navy'); ?>"><?= number_format($v['valore'], 2, ',', '.'); ?></td>
                        <td><?= (new \MSFramework\utils())->smartdate(strtotime($v['data'])); ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {

    });

</script>
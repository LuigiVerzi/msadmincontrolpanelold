<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('blog_articoli')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-warning pull-right" href="modules/blog/articoli/">Vedi Articoli</a>
            <h5>Articoli</h5>
        </div>

        <div class="ibox-content">

            <div class="web_stats_table" style="background: #fafafa; margin: -15px; padding: 15px 15px 10px;">
                <div class="row">
                    <div class="col-xs-4">
                        <a type="button" href="modules/blog/articoli/" class="btn btn-default m-r-sm">
                            <?= (int)$MSFrameworkDatabase->getCount("SELECT * FROM blog_posts"); ?>
                        </a>
                        Articoli
                    </div>
                    <div class="col-xs-4">
                        <a type="button" href="modules/blog/categorie/" class="btn btn-warning m-r-sm">
                            <?= (int)$MSFrameworkDatabase->getCount("SELECT * FROM blog_categories"); ?>
                        </a>
                        Categorie
                    </div>
                    <div class="col-xs-4">
                        <a type="button" href="modules/blog/preferenze/" class="btn btn-primary m-r-sm btn-block">
                            Vedi Preferenze
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-xs-4">
                    <small class="stats-label">Letti nell'ultima ora</small>
                    <h4><?= (int)$MSFrameworkDatabase->getAssoc("SELECT SUM(n_visite) as visits FROM `blog_stats` WHERE STR_TO_DATE(CONCAT(day, ' ', hour, ':', minute), '%Y-%m-%d %H:%i') > (NOW() - INTERVAL 1 HOUR)", array(), true)['visits']; ?></h4>
                </div>

                <div class="col-xs-4">
                    <small class="stats-label">Letti negli ultimi 7 giorni</small>
                    <h4><?= (int)$MSFrameworkDatabase->getAssoc("SELECT SUM(n_visite) as visits FROM `blog_stats` WHERE STR_TO_DATE(CONCAT(day, ' ', hour, ':', minute), '%Y-%m-%d %H:%i') > (NOW() - INTERVAL 7 DAY) ", array(), true)['visits']; ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Tempo lettura (media)</small>
                    <h4>
                        <?php
                        $tempo_secondi = (int)$MSFrameworkDatabase->getAssoc('SELECT avg(tempo_medio) as tempo_medio FROM blog_stats WHERE tempo_medio > 0', array(), true)['tempo_medio'];

                        $dt_diff = (new \DateTime('@0'))->diff((new \DateTime("@$tempo_secondi")));

                        $ary_tempi = array("a" => "g", "h" => "h", "i" => "m", "s" => "s");
                        $str_tempo = "";
                        foreach($ary_tempi as $tK => $tV) {
                            $str_tempo .= ($dt_diff->format('%' . $tK) != "0" ? $dt_diff->format('%' . $tK) . $tV . ", " : "");
                        }

                        echo ($str_tempo != "" ? substr($str_tempo, 0, -2) : '0s');
                        ?>
                    </h4>
                </div>
            </div>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th>Articoli più letti</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach((new \MSFramework\Blog\posts())->getMostReadArticles(5) as $a) { ?>

                    <?php
                    $dt_diff = (new \DateTime('@0'))->diff((new \DateTime("@" . (int)$a['watch_time'])));

                    $ary_tempi = array("a" => "g", "h" => "h", "i" => "m", "s" => "s");
                    $str_tempo = "";
                    foreach($ary_tempi as $tK => $tV) {
                        $str_tempo .= ($dt_diff->format('%' . $tK) != "0" ? $dt_diff->format('%' . $tK) . $tV . ", " : "");
                    }

                    $str_tempo = ($str_tempo != "" ? substr($str_tempo, 0, -2) : '0s');
                    ?>

                    <tr>
                        <td><a href="modules/blog/articoli/edit.php?id=<?= $a['id']; ?>"><?= $MSFrameworki18n->getFieldValue($a['titolo']); ?></a></td>
                        <td>
                            <b><?= $a['n_visits']; ?></b> visit<?= ($a['n_visits'] != 1 ? 'e' : 'a'); ?> <small>(ultimi 30 gg)</small><br>
                            <b><?= $a['visite']; ?></b> visit<?= ($a['visite'] != 1 ? 'e' : 'a'); ?> <small>(totali)</small><br>
                        </td>
                        <td>
                            <b><?= $str_tempo; ?></b> <small>lettura media</small>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {

    });

</script>
<?php
require_once('../../../../sw-config.php');

$action = $_GET['action'];

if($action == 'ordini') {
    $ordini = array('data' => array());

    $MSFrameworkEcommerceOrders =  new MSFramework\Ecommerce\orders();

    if(strtotime($_POST['start']) <= strtotime('2000-01-01')) {
        $_POST['start'] = $MSFrameworkDatabase->getAssoc("SELECT order_date FROM ecommerce_orders ORDER BY id ASC LIMIT 1", array(), true)['order_date'];
    }

    $ordini['data'][0] = $MSFrameworkEcommerceOrders->getDateRangeChart($_POST['start'], $_POST['end'], 0);
    $ordini['data'][1] = $MSFrameworkEcommerceOrders->getDateRangeChart($_POST['start'], $_POST['end'], 1);
    $ordini['data'][2] = $MSFrameworkEcommerceOrders->getDateRangeChart($_POST['start'], $_POST['end'], 2);
    $ordini['data'][3] = $MSFrameworkEcommerceOrders->getDateRangeChart($_POST['start'], $_POST['end'], 3);
    $ordini['data'][4] = $MSFrameworkEcommerceOrders->getDateRangeChart($_POST['start'], $_POST['end'], 4);

    $dateDifference = strtotime($_POST['end'])-strtotime($_POST['start']);

    $earn_this_period = 0;
    $orders_this_period = 0;
    foreach($ordini['data'] as $k=>$giorni) {
        foreach($giorni as $k2 => $giorno) {
            $earn_this_period += $giorno['earn'];
            $orders_this_period += $giorno['orders'];
            if($k != 4) {
                $ordini['data'][$k][$k2] = $giorno['earn'];
            }
        }
    }

    $start = new DateTime($_POST['start']);
    $end = new DateTime($_POST['end']);

    $nomi_mesi = array('Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic');

    $labels = array();
    for($i = $start; $i <= $end; $i->modify('+1 day')) {
        $labels[] = (int)$i->format("d") . ' ' . $nomi_mesi[(int)$i->format("m")-1] . (round($dateDifference / (60 * 60 * 24)) > 60 ? ' ' . (int)$i->format("Y") : '');
    }


    die(json_encode(array('graph' => $ordini, 'labels' => $labels, 'earn_this_period' => number_format($earn_this_period, 2, ',', '.'), 'orders_this_period' => $orders_this_period)));
}
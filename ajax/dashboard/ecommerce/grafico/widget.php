<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('ecommerce_grafico')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-4 col-md-push-8">
                    <div class="text-right">
                        <input type="text" name="daterange" id="ecommerce_order_daterange" value="Ultimi 30 giorni" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-8 col-md-pull-4">
                    <h3 class="font-bold no-margins">
                        Andamento degli ordini
                    </h3>
                    <small class="ecommerce_order_chart_time">Stai visualizzando <b>Ultimi 30 giorni</b>.</small>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <canvas id="ecommerceOrderChart" height="300"></canvas>
        </div>
        <div class="ibox-content">
            <div class="stat-list row">
                <div class="col-xs-6">
                    <h2 class="no-margins" id="ecommerce_orders_chart_total_earn"></h2>
                    <small>Entrate stimate</small>
                    <div class="progress progress-mini">
                        <div class="progress-bar" style="width: 100%;"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <h2 class="no-margins" id="ecommerce_orders_chart_total_orders"></h2>
                    <small>Ordini totali</small>
                    <div class="progress progress-mini">
                        <div class="progress-bar" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <small>
                <strong>NB:</strong> I valori espressi intendono il totale dei carrelli e non tengono conto delle spese e tassazioni.
            </small>
        </div>
    </div>
</div>

<script>
    window.widgets_functions.push(function () {

        /* INIZIALIZZAZIONE GRAFICO ORDINI */
        $('#ecommerce_order_daterange').daterangepicker({
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY',
                applyLabel: "Cerca",
                cancelLabel: "Annulla",
                fromLabel: "Da",
                toLabel: "A",
                customRangeLabel: "Personalizzata",
            },
            ranges: {
                'Ultimi 7 giorni': [moment().subtract(6, 'days'), moment()],
                'Ultimi 30 giorni': [moment().subtract(30, 'days'), moment()],
                'Questo mese': [moment().startOf('month'), moment().endOf('month')],
                'Mese precedente': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Quest\'anno': [moment().startOf('year'), moment().endOf('year')],
                'Dall\'inizio': [0, moment()],
            },
            showCustomRangeLabel: false,
            autoUpdateInput: false
        }, function(start, end, label) {
            updateEcommerceOrderGraph(start, end);
            $('#ecommerce_order_daterange').val(label);
            $('.ecommerce_order_chart_time b').html(label);
        });
        updateEcommerceOrderGraph(moment().subtract(30, 'days'), moment());

    });

    function updateEcommerceOrderGraph(start, end) {

        $.ajax({
            url: "<?= ABSOLUTE_SW_PATH_HTML . $widget_info['path']; ?>/ajax.php?action=ordini",
            type: 'post',
            data: {start: start.format('YYYY-MM-DD'), end: end.format('YYYY-MM-DD')},
            async: true,
            dataType: 'json',
            success: function(data) {

                if(typeof(window.ecommerce_order_chart) != 'undefined') window.ecommerce_order_chart.destroy();

                $('#ecommerce_orders_chart_total_earn').html(data.earn_this_period + '<?= CURRENCY_SYMBOL; ?>');
                $('#ecommerce_orders_chart_total_orders').html(data.orders_this_period);

                var array_data = data.graph.data;
                var lineData = {
                    labels: data.labels,
                    datasets: [
                        {
                            label: "In attesa di pagamento",
                            backgroundColor: "rgba(244,67,54,0.5)",
                            borderColor: "rgba(244,67,54,0.7)",
                            pointBackgroundColor: "rgba(244,67,54,1)",
                            pointBorderColor: "#fff",
                            data: array_data[0]
                        },
                        {
                            label: "In elaborazione",
                            backgroundColor: "rgba(255,152,0,0.5)",
                            borderColor: "rgba(255,152,0,1)",
                            pointBackgroundColor: "rgba(255,152,0,1)",
                            pointBorderColor: "#fff",
                            data: array_data[1]
                        },
                        {
                            label: "Spedito",
                            backgroundColor: "rgba(33,150,243,0.5)",
                            borderColor: "rgba(33,150,243,1)",
                            pointBackgroundColor: "rgba(33,150,243,1)",
                            pointBorderColor: "#fff",
                            data: array_data[2]
                        },
                        {
                            label: "Consegnato",
                            backgroundColor: "rgba(139,195,74,0.5)",
                            borderColor: "rgba(139,195,74,1)",
                            pointBackgroundColor: "rgba(139,195,74,1)",
                            pointBorderColor: "#fff",
                            data: array_data[3]
                        },
                        {
                            label: "Annullato",
                            backgroundColor: "rgba(78,70,69,0.5)",
                            borderColor: "rgba(78,70,69,1)",
                            pointBackgroundColor: "rgba(78,70,69,1)",
                            pointBorderColor: "#fff",
                            data: array_data[4]
                        }
                    ]
                };

                var lineOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + '<?= CURRENCY_SYMBOL; ?>';
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return Math.round(value * 100) / 100 + '<?= CURRENCY_SYMBOL; ?>'
                                }
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                autoSkip: true,
                                maxTicksLimit: 7
                            }
                        }]
                    }
                };

                var ctx = document.getElementById("ecommerceOrderChart").getContext("2d");
                window.ecommerce_order_chart = new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
                window.dashboardMasonry.masonry()
            }
        });
    }

</script>
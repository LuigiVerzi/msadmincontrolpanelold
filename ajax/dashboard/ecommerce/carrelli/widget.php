<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('ecommerce_carrelli')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-success pull-right" href="modules/customers/list">Vedi Carrelli</a>
            <h5>Carrelli Abbandonati</h5>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th>Ultimi Carrelli</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($MSFrameworkDatabase->getAssoc("SELECT ecommerce_users_extra.id, ecommerce_users_extra.carrello, ecommerce_users_extra.edit_date, customers.nome, customers.cognome FROM `ecommerce_users_extra` LEFT JOIN customers ON customers.id = ecommerce_users_extra.id WHERE carrello LIKE '[{\"product_id\"%' ORDER BY edit_date DESC LIMIT 5") as $user) { ?>

                    <?php
                    $prodotti = json_decode($user['carrello'], true);
                    $n_prodotti = count($prodotti);
                    $titoli_prodotti = array();

                    foreach($prodotti as $prodotto) {
                        $titoli_prodotti[] = $prodotto['nome'];
                    }
                    $titoli_prodotti = implode('<br><br>', $titoli_prodotti);
                    ?>

                    <tr>
                        <td><?= $user['nome'] . ' ' . $user['cognome']; ?></td>
                        <td><?= $n_prodotti . ' prodott' . ($n_prodotti == 1 ? 'o' : 'i'); ?></td>
                        <td><i class="fa fa-clock-o"></i> <?= (new \MSFramework\utils())->smartdate(strtotime($user['edit_date'])); ?></td>
                        <td class="text-navy"><a href="modules/customers/list/edit.php?id=<?= $user['id']; ?>" class="text-navy">Vedi <i class="fa fa-arrow-right"></i></a></td>
                        <td><span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= htmlentities($titoli_prodotti); ?>"></i></span></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {

    });

</script>
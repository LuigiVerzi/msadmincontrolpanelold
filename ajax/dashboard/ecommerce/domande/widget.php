<?php
$widget_info = (new \MSSoftware\dashboard())->getWidgetByID('customers')
?>

<div class="dashboard-widget">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="label label-warning pull-right" href="modules/ecommerce/prodotti/domande">Vedi Domande</a>
            <h5>Q&A Prodotti</h5>
        </div>

        <div class="ibox-content">
            <div class="row">
                <div class="col-xs-4">
                    <small class="stats-label text-danger">In Attesa di Risposta</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_product_questions WHERE answer LIKE ''"); ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label text-navy">Con Risposta</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_product_questions WHERE answer NOT LIKE ''"); ?></h4>
                </div>
                <div class="col-xs-4">
                    <small class="stats-label">Totali</small>
                    <h4><?= (int)$MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_product_questions"); ?></h4>
                </div>
            </div>
        </div>

        <div class="ibox-content table-responsive">
            <table class="table table-hover no-margins">
                <thead>
                <tr>
                    <th>Ultime Domande</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_questions ORDER BY id DESC LIMIT 5") as $q) { ?>

                    <?php
                    $fake_customer = false;
                    $user_name = '<i>Non Trovato</i>';

                    if($q['user_id'] != 0)  $user_info = (new \MSFramework\customers())->getCustomerDataFromDB($q['user_id']);
                    else {
                        $fake_customer = true;
                        $user_info = json_decode($q['fake_customer'], true);
                    }

                    if($user_info) {
                        $user_name = $user_info['nome'] . ' ' . $user_info['cognome'];
                    }

                    $info_prodotto = (new \MSFramework\Ecommerce\products())->getProductDetails($q['product_id'])[$q['product_id']];

                    ?>

                    <tr>
                        <td><a href="modules/ecommerce/prodotti/list/edit.php?id=<?= $info_prodotto['id']; ?>"><?= $MSFrameworki18n->getFieldValue($info_prodotto['nome']); ?></a></td>
                        <td>
                            <?php if($fake_customer) { ?>
                                <i class="fa fa-warning text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Questa domanda è stata inserita manualmente."></i>
                            <?php } ?>
                            <?= $user_name; ?>
                        </td>
                        <td>
                            <?php if(!empty($q['answer'])) { ?>
                                <span class="label label-primary">Risposto</span>
                            <?php } else { ?>
                                <span class="label label-danger">In Attesa</span>
                            <?php }?>
                        </td>
                        <td><i class="fa fa-clock-o"></i> <?= (new \MSFramework\utils())->smartdate(strtotime($q['question_date'])); ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>


<?php

?>

<script>
    window.widgets_functions.push(function () {

    });

</script>
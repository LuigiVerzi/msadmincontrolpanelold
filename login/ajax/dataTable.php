<?php
require_once('../../sw-config.php');

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId'
    ),
    array( 'db' => 'id', 'dt' => 0, 'formatter' => function($d, $row) {
        return (new \MSFramework\SaaS\environments($d))->getOwnerReadableString();
    }),
    array( 'db' => 'user_email', 'dt' => 1),
    array( 'db' => 'id', 'dt' => 2, 'formatter' => function($d, $row) {
        unset($MSSaaSEnvironments);
        $MSSaaSEnvironments = new \MSFramework\SaaS\environments($d);
        return $MSSaaSEnvironments->getDBName();
    }),
    array( 'db' => 'expiration_date', 'dt' => 3, 'formatter' => function($d, $row) {
        return date("d/m/Y", strtotime($d));
    }),
    array( 'db' => 'id', 'dt' => 4, 'formatter' => function($d, $row) {
        unset($MSSaaSEnvironments);
        $MSSaaSEnvironments = new \MSFramework\SaaS\environments($d);
        $cur_sub = $MSSaaSEnvironments->getCurrentSubscription();
        $cur_saas_id = $MSSaaSEnvironments->getSaaSIDByEnvironment();

        $MSSaaSSubscriptions = new \MSFramework\SaaS\subscriptions($cur_saas_id);
        $ary_subs = $MSSaaSSubscriptions->getAvailSubscriptions();

        return $ary_subs[$cur_sub]['name'];
    })
);

echo json_encode(
    $datatableHelper::complex( $_GET, $MSFrameworkDatabase, FRAMEWORK_DB_NAME . '.saas__environments', 'id', $columns, null, " saas_id = '" . $MSFrameworkSaaSBase->getID() . "'")
);
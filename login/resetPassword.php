<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require('../sw-config.php');

$show_newpassword_box = false;

$r_settings = $MSFrameworkCMS->getCMSData('site');
$logos = json_decode($r_settings["logos"], true);

if($_POST['send_mail_with_token'] == "1") {
    if($MSFrameworkUsers->checkIfMailExists($_POST['resetting_email'])) {
        $MSFrameworkUsers->generatePassResetToken($_POST['resetting_email']);

        $oth = array();
        if($MSFrameworkSaaSBase->isSaaSDomain()) {
            $env_id = $MSFrameworkSaaSEnvironments->getEnvIDByMail($_POST['resetting_email']);
            $oth = array("env_id" => $env_id);
        }
        if((new \MSFramework\emails())->sendMail('pass-reset', array_merge($oth, array("email" => $_POST['resetting_email'])))) {
            $show_newpassword_box = true;
        } else {
            $str_error = "C'è stato un errore durante l'invio della mail. Contatta un amministratore di sistema.";
            $show_newpassword_box = false;
        }
    } else {
        $str_error = "Questo indirizzo non è associato ad alcun utente.";
        $show_newpassword_box = false;
    }
}

if($_GET['t'] != "") {
    $show_newpassword_box = true;
}

if($_POST['doreset'] == "1") {
    if ($_POST['secure_token'] == "" || $_POST['password'] == "" || $_POST['ripeti_password'] == "") {
        $str_error = "Tutti i campi sono obbligatori.";
    } else {
        if ($_POST['password'] != $_POST['ripeti_password']) {
            $str_error = "Le password inserite non corrispondono.";
        }

        if($MSFrameworkSaaSBase->isSaaSDomain() && $_POST['env_id'] == "") {
            $str_error = "Non sono riuscito a determinare l'ID dell'ambiente.";
        }

        if($_POST['origin'] == 'customer') {
            if($MSFrameworkSaaSBase->isSaaSDomain()) {
                $db_name = (new \MSFramework\SaaS\environments($_POST['env_id']))->getDBName();
                $set_table = "`$db_name`.`customers`";
            } else {
                $set_table = 'customers';
            }

            if ($MSFrameworkDatabase->getCount("SELECT passreset_token FROM $set_table WHERE passreset_token = :token", array(":token" => $_POST['secure_token'])) != 1) {
                $str_error = "Il token inserito non è valido";
            }

            $checkStrength = (new \MSFramework\customers())->checkPasswordStrength($_POST['password']);
        } else {
            if($MSFrameworkSaaSBase->isSaaSDomain()) {
                $db_name = (new \MSFramework\SaaS\environments($_POST['env_id']))->getDBName();
                $set_table = "`$db_name`.`users`";
            } else {
                $set_table = 'users';
            }

            $userToken = $MSFrameworkDatabase->getCount("SELECT passreset_token FROM $set_table WHERE passreset_token = :token", array(":token" => $_POST['secure_token']));
            $superadminToken = $MSFrameworkDatabase->getCount("SELECT passreset_token FROM `" . FRAMEWORK_DB_NAME . "`.`users` WHERE passreset_token = :token", array(":token" => $_POST['secure_token']));

            if($userToken != 1 && $superadminToken != 1) {
                $str_error = "Il token inserito non è valido";
            }

            $checkStrength = $MSFrameworkUsers->checkPasswordStrength($_POST['password']);
        }

        if ($checkStrength != "") {
            $str_error = $checkStrength;
        }

    }

    if ($str_error != "") {
        $str_error .= " <br /> Utilizza nuovamente il link che hai ricevuto via mail per ripetere la procedura!";
        $show_newpassword_box = true;
    } else {

        if($_POST['origin'] == 'customer') {

            if ($MSFrameworkDatabase->pushToDB("UPDATE $set_table SET password = :pass, passreset_token = '' WHERE passreset_token = :token", array(":token" => $_POST['secure_token'], ":pass" => password_hash($_POST['password'], PASSWORD_DEFAULT)))) {
                header("location: " . $MSFrameworkCMS->getURLToSite());
            } else {
                $str_error .= " C'è stato un errore durante il reset. Contatta un amministratore di sistema.";
            }
        }
        else {
            $table = ($superadminToken > 0 ?  "`" . FRAMEWORK_DB_NAME . "`.`users`" : $set_table);

            if ($MSFrameworkDatabase->pushToDB("UPDATE $table SET password = :pass, passreset_token = '' WHERE passreset_token = :token", array(":token" => $_POST['secure_token'], ":pass" => password_hash($_POST['password'], PASSWORD_DEFAULT)))) {
                header("location: ../login.php?reset-ok");
            } else {
                $str_error .= " C'è stato un errore durante il reset. Contatta un amministratore di sistema.";
            }
        }
    }
}
?>


<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo SW_NAME ?> Admin | Reset Password</title>

    <link rel="shortcut icon" type="image/png" href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/favicon.png" />

    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/animate.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/style.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/colors-aquamarine.css" rel="stylesheet">

</head>

<body class="login-bg resetpass-bg" style="<?= ($logos['login_bg'] != '' ? 'background-image: url(' . htmlentities(UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logos['login_bg']) . ');' : '') ?>">
<div class="overlay"></div>
<div class="animated fadeInDown">
    <div class="loginColumns animated fadeInDown">
        <div class="logo-name"><img src="<?= $MSFrameworkCMS->getLoginLogoPath(); ?>" alt="MS" class="img-responsive"  onerror="this.onerror=null;this.src='<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/logo.svg';" /></div>
        <div class="row">

            <?php if($str_error != "") { ?>
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <?php echo $str_error ?>
                    </div>
                </div>
            <?php } ?>

            <?php
            if(!$show_newpassword_box) {
                ?>
                <div class="col-md-6">
                    <div class="middle-box loginscreen">
                        <h2 class="font-bold">Reset Password</h2>

                        <p>
                            Se hai dimenticato la password per accedere al tuo account, inserisci l'indirizzo email che hai utilizzato per registrarti a <?php echo SW_NAME ?> nel box.
                        </p>

                        <p>
                            Ti invieremo un'email con un link che ti permetterà di resettare la tua password.
                        </p>

                        <p>
                            <small>Se l'email non arriva nel giro di qualche minuto, controlla anche la casella SPAM!</small>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="middle-box loginscreen">
                        <form class="m-t" role="form" action="resetPassword.php" method="post">
                            <input type="hidden" name="send_mail_with_token" class="form-control" value="1">
                            <div class="form-group">
                                <input type="email" name="resetting_email" class="form-control" placeholder="Indirizzo email" required>
                            </div>
                            <button type="submit" class="btn btn-primary block full-width m-b">Invia email</button>

                            <p class="text-muted text-center">
                                <small>Non vuoi più resettare la password?</small>
                            </p>
                            <a class="btn btn-sm btn-white btn-block" href="<?= ABSOLUTE_SW_PATH_HTML ?>login.php">Torna al Login</a>
                        </form>
                    </div>
                </div>
            <?php } else { ?>
                <?php if($_GET['t'] == "") { ?>
                    <?php if($str_error == "") { ?>
                        <div class="col-md-6 middle-box loginscreen">
                            <h2 class="font-bold">Link inviato!</h2>

                            <p>
                                Abbiamo inviato un'email all'indirizzo che ci hai indicato.
                            </p>

                            <p>
                                Clicca sul link che trovi al suo interno o inserisci il token di sicurezza qui a fianco per poter procedere con il reset della password!
                            </p>

                            <p>
                                <small>Se l'email non arriva nel giro di qualche minuto, controlla anche la casella SPAM!</small>
                            </p>
                        </div>
                    <?php } else { ?>
                        <div class="col-md-6">
                            <div class="middle-box loginscreen">
                                <h2 class="font-bold">C'è qualche problema</h2>

                                <p>
                                    Controlla e correggi gli errori per poter resettare la password.
                                </p>

                                <p>
                                    Il link che ti abbiamo inviato è ancora attivo. Puoi riutilizzarlo per ripetere la procedura
                                </p>
                            </div>
                        </div>

                    <?php } ?>

                <?php } else {?>
                    <div class="col-md-6">
                        <div class="middle-box loginscreen">
                            <h2 class="font-bold">Scegli la password</h2>
                            <p>
                                Scegli la nuova password da associare al tuo account e clicca su "Reset"
                            </p>
                        </div>
                    </div>
                <?php } ?>

                <div class="col-md-6">
                    <div class="middle-box loginscreen">
                        <form class="m-t" role="form" action="resetPassword.php" method="post">
                            <?php if($_GET['t'] == "") { ?>
                                <div class="form-group">
                                    <input type="text" name="secure_token" class="form-control" placeholder="Token di sicurezza" required>
                                </div>
                                <br />
                            <?php } else {?>
                                <input type="hidden" name="secure_token" class="form-control" placeholder="Token di sicurezza" value="<?php echo $_GET['t'] ?>" required>
                            <?php } ?>

                            <?php if($_GET['o'] == "customer" || $_POST['origin'] == "customer") { ?>
                                <input type="hidden" name="origin" value="customer" required>
                            <?php } ?>

                            <?php if($_GET['env_id'] != "") { ?>
                                <input type="hidden" name="env_id" value="<?= $_GET['env_id'] ?>" required>
                            <?php } else if($env_id != "") {
                            ?>
                                <input type="hidden" name="env_id" value="<?= $env_id ?>" required>
                            <?php
                            } ?>
                            <input type="hidden" name="doreset" class="form-control" value="1" required>
                            <input type="hidden" name="resetting_email" class="form-control" value="<?php echo $_POST['resetting_email'] ?>" required>

                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <input type="password" name="ripeti_password" class="form-control" placeholder="Ripeti Password" required>
                            </div>
                            <button type="submit" class="btn btn-primary block full-width m-b">Reset</button>

                            <br />
                            <p class="text-muted text-center">
                                <small>Non vuoi più resettare la password?</small>
                            </p>
                            <?php if($_GET['o'] == "customer" || $_POST['origin'] == "customer") { ?>
                                <a class="btn btn-sm btn-white btn-block" href="<?= $MSFrameworkCMS->getURLToSite(); ?>">Torna su <?= SW_NAME; ?></a>
                            <?php } else { ?>
                                <a class="btn btn-sm btn-white btn-block" href="<?= ABSOLUTE_SW_PATH_HTML ?>login.php">Torna al Login</a>
                            <?php } ?>
                        </form>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-12 text-left">
                <small>Copyright <?php echo SW_NAME ?> &copy; <?php echo date("Y") ?> - All rights reserved</small>
            </div>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/js/jquery-2.1.1.js"></script>
<script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/js/bootstrap.min.js"></script>

</body>

</html>
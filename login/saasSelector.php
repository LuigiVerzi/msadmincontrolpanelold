<?php
/**
 * MSAdminControlPanel
 * Date: 09/07/18
 */

require('../sw-config.php');

if($_SESSION['userData']['userlevel'] == "0") {
    unset($_SESSION['SaaS']);
} else {
    $MSFrameworkUsers->endUserSession();
}

$MSSaaSEnvironments = new \MSFramework\SaaS\environments($_GET['id']);
$saas_id = $MSFrameworkSaaSBase->getID();

if($_GET['id'] != "") {
    $_SESSION['SaaS']['saas__environments']['database'] = $MSSaaSEnvironments->getDBName();
    $_SESSION['SaaS']['saas__environments']['id'] = $_GET['id'];
    header("location: ../index.php");
    die();
}

$r_settings = $MSFrameworkCMS->getCMSData('site');
$logos = json_decode($r_settings["logos"], true);

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo SW_NAME ?> Admin | Selezione Ambiente SaaS</title>

    <link rel="shortcut icon" type="image/png" href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/favicon.png" />

    <input type="hidden" id='baseElementPathAdmin' value="<?php echo ABSOLUTE_SW_PATH_HTML ?>" />

    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/fontawesome5/css/all.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/fontawesome5/css/v4-shims.css" rel="stylesheet">

    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/animate.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/style.css" rel="stylesheet">

    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/contextMenu/jquery.contextMenu.min.css" rel="stylesheet">

    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/dataTables/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/dataTables/dataTables.checkboxes.css" rel="stylesheet">

    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>assets/css/colors-aquamarine.css" rel="stylesheet">
</head>

<body class="login-bg" style="<?= ($logos['login_bg'] != '' ? 'background-image: url(' . htmlentities(UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logos['login_bg']) . ');' : '') ?>">
<div class="overlay"></div>
<div class="logo-name"><img src="<?= $MSFrameworkCMS->getLoginLogoPath(); ?>" alt="MS" class="img-responsive"  onerror="this.onerror=null;this.src='<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/logo.svg';" /></div>

<div class="middle-box loginscreen saasselector" style="display: none;">
    <div class="login-content">

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-8">
                <h2>Seleziona Ambiente SaaS</h2>
                <ol class="breadcrumb">
                    <li>Ciao <?= $_SESSION['userData']['nome'] ?>! Seleziona l'ambiente del Saas "<?= $MSFrameworki18n->getFieldValue($MSFrameworkCMS->getCMSData("site")['nome']) ?>" sul quale desideri effettuare le operazioni.</li>
                </ol>
            </div>

            <div class="col-sm-4">
                <div class="title-action">
                    <a class="btn btn-white" id="goToSaaSLogin" href="<?= ABSOLUTE_SW_PATH_HTML ?>login.php?do-logout">Torna al Login</a>
                    <a class="btn btn-primary" id="goToSaaSFront" href="saasSelector.php?id=front">Ambiente FrontOffice</a>
                    <a class="btn btn-warning" id="dataTableEdit">Modifica</a>
                </div>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <table id="main_table_list" class="display table table-striped table-bordered table-hover" width="100%" cellspacing="0" data-saasselector="true" data-multiselect="false">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Database</th>
                    <th>Data Scadenza</th>
                    <th>Tipo Abbonamento</th>
                </tr>
                </thead>
            </table>
        </div>

        <p class="m-t text-center"> <small>Copyright <?php echo SW_NAME ?> &copy; <?php echo date("Y") ?> - All rights reserved</small> </p>

    </div>

    <!-- Mainly scripts -->
    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/js/jquery-2.1.1.js"></script>
    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/js/bootstrap.min.js"></script>

    <script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/iCheck/icheck.min.js"></script>

    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/contextMenu/jquery.contextMenu.min.js"></script>
    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/contextMenu/jquery.ui.position.js"></script>

    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/dataTables/datatables.min.js"></script>
    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/dataTables/dataTables.responsive.min.js"></script>
    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/dataTables/responsive.bootstrap.min.js"></script>
    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/dataTables/dataTables.checkboxes.min.js"></script>
    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/ddslick/jquery.ddslick.min.js"></script>
    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>assets/js/main.js"></script>
    <script src="<?= ABSOLUTE_SW_PATH_HTML ?>assets/js/dataTablesUtils.js"></script>

    <script>
        $('.loginscreen').show();
        loadStandardDataTable('main_table_list');
        allowRowHighlights('main_table_list');
        manageGridButtons('main_table_list');

        $('#main_table_list tbody').on('dblclick', 'tr', function (e) {
            window.location.href = 'saasSelector.php?id=' + $(this).attr('id');
        })
    </script>
</body>
</html>
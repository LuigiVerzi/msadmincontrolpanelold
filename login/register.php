<?php
require('../sw-config.php');

if(isset($_SESSION['userData']['userlevel'])) {
    header("location: index.php");
    die();
}

$MSSaaSEnvironments = new MSFramework\SaaS\environments();

$r_settings = $MSFrameworkCMS->getCMSData('site');
$r_settings_reg = json_decode($r_settings['registration'], true);

$logos = json_decode($r_settings["logos"], true);

$str_error = "";
$mail_conflict = false;
if(isset($_POST['nome']) && isset($_POST['cognome']) && isset($_POST['email']) && isset($_POST['email_confirm']) && isset($_POST['password']) && isset($_POST['password_confirm']) && isset($_POST['cellulare'])) {
    if($r_settings_reg['enable'] != "1") {
        $str_error = "La registrazione su questo sito è disabilitata.";
    } else {
        if (trim($_POST['nome']) == "" || trim($_POST['cognome']) == "" || trim($_POST['email']) == "" || trim($_POST['email_confirm']) == "" || trim($_POST['password']) == "" || trim($_POST['password_confirm']) == "" || trim($_POST['cellulare']) == "" || ($r_settings_reg['can_choose_level'] == "1" && $_POST['livello'] == "")) {
            $str_error = "Attenzione! Tutti i campi sono obbligatori!";
        } else {
            if($r_settings_reg['can_choose_level'] != "1") {
                $_POST['livello'] = $r_settings_reg['default_level'];
            }

            if($_POST['livello'] == "realestate-6" && $_POST['consulente_riferimento'] == "") {
                $str_error = "Attenzione! Tutti i campi sono obbligatori!";
            }

            if($MSFrameworkCMS->checkExtraFunctionsStatus('realestate') && $_POST['agency'] == "") {
                $str_error = "Attenzione! E' necessario selezionare un'agenzia di appartenenza!";
            }

            if ($_POST['password'] != $_POST['password_confirm']) {
                $str_error = "Attenzione! Le password inserite non corrispondono!";
            } else {
                $pass_strength = $MSFrameworkUsers->checkPasswordStrength($_POST['password']);
                if ($pass_strength != "") {
                    $str_error = "Attenzione! " . $pass_strength;
                }
            }

            if(in_array($_POST['livello'], $r_settings_reg['exclude_levels']) || $_POST['livello'] == "0") {
                $str_error = "Attenzione! Non è possibile utilizzare il livello selezionato per la creazione di un nuovo account.";
            }

            if ($_POST['email'] != $_POST['email_confirm']) {
                $str_error = "Attenzione! Le email inserite non corrispondono!";
            } else {
                if($MSFrameworkUsers->checkIfMailExists($_POST['email'])) {
                    $mail_conflict = true;
                }

                if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    $mail_conflict = true;
                }

                if ($mail_conflict) {
                    $str_error = "Attenzione! L'indirizzo email non è valido o è già stato utilizzato da un altro utente.";
                }
            }

            $check_captcha = false;
            if(trim($r_settings['captcha_public']) != "" && trim($r_settings['captcha_private']) != "") {
                $check_captcha = true;
            }

            if($check_captcha) {
                $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".trim($r_settings['captcha_private'])."&response=".$_POST["g-recaptcha-response"]."&remoteip=".$_SERVER["REMOTE_ADDR"]), true);

                if ($response["success"] == false) {
                    $str_error = "Attenzione! Impossibile validare il captcha con Google!";
                }
            }

            if ($str_error == "") {
                if($isSaaS) {
                    $user_id = $MSSaaSEnvironments->createNewEnvironment($_POST['nome'], $_POST['cognome'], $_POST['email'], $_POST['password'], $_POST['livello'], $_POST['cellulare']);
                } else {
                    $user_id = $MSFrameworkUsers->registerUser($_POST['nome'], $_POST['cognome'], $_POST['email'], $_POST['password'], $_POST['cellulare'], $_POST['livello'], array("cons_rif" => $_POST['consulente_riferimento']), array("agency" => $_POST['agency']), true);
                }

                if($user_id) {
                    $str_confirm_mail_confirm = "";

                    if($r_settings_reg['request_mail_confirm'] == "1") {
                        $str_confirm_mail_confirm .= " Controlla la tua casella di posta per attivarlo.";
                    }

                    if($r_settings_reg['request_admin_confirm'] == "1") {
                        $str_confirm_mail_confirm .= " Dovrai attendere l'attivazione da parte di un Admin per accedere.";
                    }

                    $str_confirm = "Il tuo account è stato creato con successo!" . $str_confirm_mail_confirm;
                } else {
                    $str_error = "Attenzione! Si è verificato un errore in fase di registrazione" . ($isSaaS ? ' o l\'indirizzo email fornito risulta già registrato' : '') . ". Riprovare.";
                }
            }
        }
    }

}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo SW_NAME ?> | Registrazione</title>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/favicon.png" />

    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/animate.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/css/style.css" rel="stylesheet">
    <link href="<?= ABSOLUTE_SW_PATH_HTML ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/colors-aquamarine.css" rel="stylesheet">

</head>

<body class="login-bg" style="<?= ($logos['login_bg'] != '' ? 'background-image: url(' . htmlentities(UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logos['login_bg']) . ');' : '') ?>">
<div class="overlay"></div>
<div class="middle-box text-center loginscreen animated fadeInDown" style="width: 550px;">
    <div>
        <div class="logo-name"><img src="<?= $MSFrameworkCMS->getLoginLogoPath(); ?>" alt="MS" class="img-responsive"  onerror="this.onerror=null;this.src='<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/logo.svg';" /></div>
        <div class="login-content">
        <?php
        if($r_settings_reg['enable'] != "1") {
        ?>
            <div class="alert alert-danger">
               La registrazione su questo sito è disabilitata.
            </div>

            <a class="btn btn-sm btn-white btn-block" href="<?= ABSOLUTE_SW_PATH_HTML ?>login.php">Torna al Login</a>
        <?php
        } else {
        ?>

            <h3>Registrazione</h3>
            <?php
            if($str_confirm == "") {
            ?>
            <p>Inserisci i dati per registrarti.</p>
            <?php } ?>

            <?php if($str_error != "") { ?>
                <div class="alert alert-danger">
                    <?php echo $str_error ?>
                </div>
            <?php } ?>

            <?php if($str_confirm != "") { ?>
                <div>
                    <div class="alert alert-success">
                        <?php echo $str_confirm ?>
                    </div>

                    <div>
                        <a class="btn btn-sm btn-white btn-block" href="<?= ABSOLUTE_SW_PATH_HTML ?>login.php">Torna al Login</a>
                    </div>
                </div>
            <?php } ?>

            <?php
            if($str_confirm == "") {
            ?>
                <form class="m-t" role="form" action="register.php" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nome" placeholder="Nome" required="" style="width: 49%; float: left; margin-right: 2%; min-width: auto;" value="<?php echo $_POST['nome'] ?>">
                        <input type="text" class="form-control" name="cognome" placeholder="Cognome" required="" style="width: 49%; min-width: auto;" value="<?php echo $_POST['cognome'] ?>">
                    </div>

                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email" required="" style="width: 49%; float: left; margin-right: 2%; min-width: auto;" value="<?php echo $_POST['email'] ?>">
                        <input type="email" class="form-control" name="email_confirm" placeholder="Conferma Email" required="" style="width: 49%; min-width: auto;">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="cellulare" placeholder="Cellulare" required="" style="width: 100%; margin-right: 2%; min-width: auto;" value="<?php echo $_POST['cellulare'] ?>">
                    </div>

                    <?php
                    if($r_settings_reg['can_choose_level'] == "1") {
                    ?>
                    <div class="form-group">
                        <select name="livello" class="form-control required" style="width: 49%; float: left; margin-right: 2%; min-width: auto;" required="">
                            <option value="">Livello</option>
                            <?php
                            foreach($MSFrameworkUsers->getUserLevels() as $levelK => $levelV) {
                                if(in_array($levelK, $r_settings_reg['exclude_levels'])) {
                                    continue;
                                }
                                ?>
                                <option value="<?php echo $levelK ?>" <?php if($_POST['livello'] == $levelK) { echo "selected"; } ?>><?php echo $levelV ?></option>
                                <?php
                            }
                            ?>
                        </select>

                        <select name="consulente_riferimento" class="form-control" style="width: 49%; visibility: <?= ($r['ruolo'] != "realestate-6" ? "hidden" : "visible") ?>; min-width: auto;">
                            <option value="">Consulente Riferimento</option>
                            <?php
                            foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome, cognome, ruolo FROM users WHERE ruolo != 'realestate-6'") as $cons_rif) {
                                ?>
                                <option value="<?php echo $cons_rif['id'] ?>" <?php if($_POST['consulente_riferimento'] == $cons_rif['id']) { echo "selected"; } ?>><?php echo $cons_rif['cognome'] ?> <?php echo $cons_rif['nome'] ?> (<?php echo $MSFrameworkUsers->getUserLevels($cons_rif['ruolo']) ?>)</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div style="clear: both"></div>

                    <?php } ?>

                    <?php
                    if($MSFrameworkCMS->checkExtraFunctionsStatus('realestate')) {
                        $MSFrameworkSedi = new \MSFramework\Attivita\sedi();
                    ?>
                    <div class="form-group">
                        <select name="agency" class="form-control">
                            <?php
                            $sedi_disponibili = (new \MSFramework\Attivita\sedi())->getSedeDetails($MSFrameworkSedi->getUserAgency($MSFrameworkUsers->getUserDataFromSession('user_id')), 'id, nome');
                            if(count($sedi_disponibili) > 1) {
                            ?>
                                <option value="">Agenzia</option>
                            <?php
                            }

                            foreach($sedi_disponibili as $sede) {
                            ?>
                                <option value="<?= $sede['id'] ?>" <?= ($_POST['agency'] == $sede['id'] ? 'selected' : '') ?>><?= $MSFrameworki18n->getFieldValue($sede['nome']) ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <?php } ?>

                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" required="" style="width: 49%; float: left; margin-right: 2%; min-width: auto;">
                        <input type="password" class="form-control" name="password_confirm" placeholder="Conferma Password" required="" style="width: 49%; min-width: auto;">
                    </div>

                    <?php
                    if(trim($r_settings['captcha_public']) != "" && trim($r_settings['captcha_private']) != "") {
                        ?>
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="<?php echo trim($r_settings['captcha_public']) ?>" style="margin-left: 110px;"></div>
                        </div>
                    <?php } ?>

                    <button type="submit" class="btn btn-primary block full-width m-b">Registrati</button>

                    <a class="btn btn-sm btn-white btn-block" href="<?= ABSOLUTE_SW_PATH_HTML ?>login.php">Torna al Login</a>
                </form>
            <?php } ?>
            <p class="m-t"> <small>Copyright <?php echo SW_NAME ?> &copy; <?php echo date("Y") ?> - All rights reserved</small> </p>
        <?php } ?>

        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/js/jquery-2.1.1.js"></script>
<script src="<?= ABSOLUTE_SW_PATH_HTML ?>vendor/js/bootstrap.min.js"></script>

<script>
    $('select[name=livello]').on('change', function () {
        if($(this).val() == "realestate-6") {
            $('select[name=consulente_riferimento]').css('visibility', 'visible');
            $('select[name=consulente_riferimento]').val('');
        } else {
            $('select[name=consulente_riferimento]').css('visibility', 'hidden');
        }
    })
</script>

</body>

</html>
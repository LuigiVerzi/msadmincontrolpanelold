<?php
if($_SESSION['userData']['userlevel'] == "") { //l'utente non è loggato, lascio andare le cose in modo da reindirizzarlo al login anche se il sito è offline
    define("DO_NOT_CHECK_OFFLINE", true);
}

require_once('sw-config.php');

$module_id = 'dashboard';
$module_config = $MSSoftwareModules->getModuleDetailsFromID($module_id);
$MSSoftwareModules->accessGranted($module_id, true);

$dashboard = new \MSSoftware\dashboard();
$widgets = $dashboard->getWidgetsToShow();

require(ABSOLUTE_SW_PATH . "includes/template/header.php");
?>

<style>
    #dashboard-widgets .ibox {
        margin-bottom: 0;
    }

    #dashboard-widgets .ibox-content.table-responsive {
        padding: 0;
    }

    #dashboard-widgets .ibox-content.table-responsive thead {
        background: #fafafa;
    }

    #dashboard-widgets .ibox-content.table-responsive th {
        border-color: #ececec;
    }

    .dashboard-widget {
        width: 33.3%;
        padding: 0 5px 10px;
    }

    @media only screen and (max-width: 1280px) {
        .dashboard-widget {
            width: 50% !important;
        }
    }

    @media only screen and (max-width: 960px) {
        .dashboard-widget {
            width: 100% !important;
            padding: 0;
        }
    }

</style>

<script>
    window.widgets_functions = [];
</script>

<body class="skin-1 fixed-sidebar pace-done">
<div id="wrapper">

    <?php require(ABSOLUTE_SW_PATH . "includes/template/sidebar.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">

        <?php require(ABSOLUTE_SW_PATH . "includes/template/topbar.php"); ?>

        <div class="wrapper wrapper-content">
            <?php
            if($MSFrameworkSaaSBase->isSaaSDomain() && $MSFrameworkSaaSEnvironments->isExpired() && $_SESSION['userData']['userlevel'] != "0") {
                header("location: " . ABSOLUTE_ADMIN_MODULES_PATH_HTML . "saas/abbonamento/");
                die();
            } else {

                foreach($widgets as $widget) {
                    if(isset($widget['full']) && $widget['full']) include(ABSOLUTE_SW_PATH . $widget['path'] . '/widget.php');
                }

                if(($MSFrameworkSaaSBase->isSaaSDomain() && $_SESSION['userData']['userlevel'] == "0") || (!$MSFrameworkSaaSBase->isSaaSDomain() && (in_array($module_config['id'], (new \MSSoftware\modules())->getEnabledModuleIDs()) || $_SESSION['userData']['userlevel'] == "0"))) {
                    if(count((new \MSFramework\Updater\dbUpdater())->getEnvolvedProcedures()['ready']) != 0 || count((new \MSFramework\Updater\dbUpdater())->getEnvolvedProcedures()['error']) != 0) { ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-danger">
                                    E' necessario avviare la procedura di aggiornamento database al più presto, in caso contrario alcune funzioni potrebbero essere non disponibili. Il database verrà aggiornato alla versione <b><?= SW_VERSION ?></b>.
                                    <a href="<?php echo ABSOLUTE_ADMIN_MODULES_PATH_HTML ?>framework360/updater/database/">Eseguire l'aggiornamento adesso?</a>
                                </div>
                            </div>
                        </div>
                    <?php }
                } ?>

                <div id="dashboard-widgets" style="margin: 0; visibility: hidden;">
                    <?php foreach($widgets as $widget) {
                        if(!isset($widget['full']) ||  !$widget['full']) include(ABSOLUTE_SW_PATH . $widget['path'] . '/widget.php');
                    } ?>
                </div>
            <?php
            }
            ?>
        </div>


        <?php require(ABSOLUTE_SW_PATH . "includes/template/footer.php"); ?>
        <script src="<?= ABSOLUTE_SW_PATH_HTML; ?>vendor/plugins/masonary/masonry.pkgd.min.js"></script>

        <script>
            $(document).ready(function() {
                window.dashboardMasonry = $('#dashboard-widgets').masonry({
                    // options
                    itemSelector: '.dashboard-widget',
                    columnWidth: '.dashboard-widget',
                    gutter: 0,
                    percentPosition: false
                });

                window.widgets_functions.forEach(function (widget_function) {
                    widget_function();
                });

                $('#dashboard-widgets').css('visibility', 'visible');
            });
        </script>
    </div>
</div>

</body>
</html>
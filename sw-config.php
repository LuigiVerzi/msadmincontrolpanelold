<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */
define("FRAMEWORK_VERSION", substr(end(explode("/", dirname(__FILE__))), 1));
define("ADMIN_FOLDER_NAME", "MSAdminControlPanel");
define("ADMIN_FOLDER", ADMIN_FOLDER_NAME . "/v" . FRAMEWORK_VERSION);
define("FRAMEWORK_FOLDER", ADMIN_FOLDER . "/MSFramework");

setlocale(LC_TIME, 'it_IT.UTF-8');
date_default_timezone_set('Europe/Rome');

define("MS_PRODUCTION_DOCUMENT_ROOT", str_replace(
    array(str_replace("www.", "", $_SERVER['HTTP_HOST']), ADMIN_FOLDER, "SVILUPPO"),
    array("", "", "FRAMEWORK"),
    $_SERVER['DOCUMENT_ROOT']
));

if(strstr($_SERVER['DOCUMENT_ROOT'], "/SVILUPPO/")) {
    define("MS_DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT']);
} else {
    define("MS_DOCUMENT_ROOT", MS_PRODUCTION_DOCUMENT_ROOT);
}

define("PATH_TO_FRAMEWORK", MS_DOCUMENT_ROOT . "/" . FRAMEWORK_FOLDER . "/");

define("REAL_SW_PATH", dirname( __FILE__ ) . "/"); //questa costante viene sfruttata in framework-config!
define("DUMMY_DB_NAME", "marke833_framework_dummy_customer"); //è il database dummy dal quale vengono prelevate le tabelle quando vengono abilitate/disabilitate le funzionalità
define("DEV_PREFIX_DB_NAME", "marke833_dev_"); //è il prefisso assegnato ai db considerati "di sviluppo"

define("MARKETINGSTUDIO_DB_NAME", "marke833_marketingstudio"); //il nome del db del sito di Marketing Studio

require(PATH_TO_FRAMEWORK . "framework-config.php");

define("PATH_TO_BACKUP", PATH_TO_COMMON_STORAGE . "/MSBackups/");
define("PATH_TO_SQL_BACKUP", PATH_TO_BACKUP . "SQL/");
define("PATH_TO_LOGS", PATH_TO_COMMON_STORAGE . "/MSLogs/");
define("PATH_TO_TMP", PATH_TO_COMMON_STORAGE . "/MSTemp/");

define("PATH_TO_BACKUP_HTML", PATH_TO_COMMON_STORAGE_HTML . str_replace(PATH_TO_COMMON_STORAGE, "", PATH_TO_BACKUP));
define("PATH_TO_SQL_BACKUP_HTML", PATH_TO_BACKUP_HTML .  str_replace(PATH_TO_BACKUP, "", PATH_TO_SQL_BACKUP));
define("PATH_TO_LOGS_HTML", PATH_TO_COMMON_STORAGE_HTML . str_replace(PATH_TO_COMMON_STORAGE, "", PATH_TO_LOGS));


define("TRANSLATE_ICON_TOOLTIP", "E' possibile tradurre questo campo selezionando la lingua dall'apposito menu in alto a destra. Fare click per visualizzare le traduzioni già impostate.");

define("GMAPS_API_KEY", $MSFrameworkCMS->getCMSData('producer_config')['gmaps_apikey']); //La chive API per l'utilizzo di Google Maps
define("GPLACES_API_KEY", $MSFrameworkCMS->getCMSData('producer_config')['gplaces_apikey']); //API KEY per Google Places. Fondamentale per ottenere gli indirizzi! (impostare limitazione per HTTP referrer su API Dashboard!)
define("GGEOCODE_API_KEY", $MSFrameworkCMS->getCMSData('producer_config')['ggeocode_apikey']); //API KEY per Google Geocode. Opzionale. (impostare limitazione per IP su API Dashboard!)

if(FRAMEWORK_DEBUG_MODE || $MSFrameworkCMS->isStaging()) {
    define("PAYMETHODS_USE_KEY", "sandbox");
} else {
    define("PAYMETHODS_USE_KEY", "produzione");
}

$datatableHelper = new datatableHelper();
$MSFrameworkTicket = new \MSFramework\Framework\ticket();
$MSSoftwareModules = new MSSoftware\modules();

if($_GET['globalLoginToken']) {
    if(isset($_GET['env'])) {
        $MSSaaSEnvironments = new \MSFramework\SaaS\environments($_GET['env']);
        $_SESSION['SaaS']['saas__environments']['database'] = $MSSaaSEnvironments->getDBName();
        $_SESSION['SaaS']['saas__environments']['id'] = $_GET['env'];
    }
    $MSFrameworkUsers->checkGlobalLoginToken($_GET['globalLoginToken']);
}

$isSaaS = $MSFrameworkSaaSBase->isSaaSDomain();
if((!$isSaaS || ($isSaaS && $_SESSION['SaaS']['saas__environments']['id'] != "")) && ($MSFrameworki18n->getPrimaryLangId() == "" && !isset($_GET['must_set_lang_data']))) {
    header("location: " . ABSOLUTE_ADMIN_MODULES_PATH_HTML . "framework/producer_config/index.php?must_set_lang_data");
}

$using_primary_language = ($MSFrameworki18n->getPrimaryLangId() == $MSFrameworki18n->getCurrentLanguageDetails()['id']);
$userDetails = $MSFrameworkUsers->getUserDetails($_SESSION['userData']['user_id'])[$_SESSION['userData']['user_id']];

// Permette di copiare anche gli uploads quando si copia una voce di un modulo
$uploadsCopyMode = false;
if(end(explode('/', $_SERVER["SCRIPT_FILENAME"])) === 'saveData.php') {
    if($_POST['pID'] === 'copy') {
        $uploadsCopyMode = true;
        $_POST['pID'] = '';
    }
}
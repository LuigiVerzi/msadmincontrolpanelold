<div class="row">
<?php
$themes = glob($widgetDetails['path'] . '/styles/*');

foreach($themes as $k => $theme) {
    $theme_id = basename($theme);
    $theme_info = json_decode(file_get_contents($theme . '/info.json'), true);
    $theme_icon =  $MSFrameworkCMS->getURLToSite(1) . 'vendor/' . explode('vendor/', $theme)[1] . '/preview.png';

    if(file_exists(ABSOLUTE_SW_PATH . 'vendor/' . explode('vendor/', $theme)[1] . '/preview.gif')) {
        $theme_icon =  $MSFrameworkCMS->getURLToSite(1) . 'vendor/' . explode('vendor/', $theme)[1] . '/preview.gif';
    }

    echo '<div class="col-md-3 col-sm-4 col-xs-2">';
    echo '<div class="visualBuilderThemeBtn">';
    echo '<input class="widget-data" id="' . $theme_id . '" type="radio" name="style" ' . ($widgetData['theme']['style'] === $theme_id || (!$widgetData['theme']['style'] && $k === 0) ? 'checked' : '') . ' value="' . $theme_id . '"/>';
    echo '<label for="' . $theme_id . '">';
    echo '<img src="' . $theme_icon . '" class="theme_icon"><h4>' . $theme_info['title'] . '</h4><small>' . $theme_info['description'] . '</small></a>';
    echo '</label>';
    echo '</div>';
    echo '</div>';
}
?>
</div>
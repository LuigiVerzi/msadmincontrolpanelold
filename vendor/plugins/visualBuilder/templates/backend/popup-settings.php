<?php
$popupDesignDatas = array(
    'global' => $widgetData['design'],
    'tablet' => ($widgetData['design']['tablet'] ? $widgetData['design']['tablet'] : array()),
    'mobile' => ($widgetData['design']['mobile'] ? $widgetData['design']['mobile'] : array()),
)
?>

<div class="row">
    <div class="col-sm-4">

        <div class="row">
            <div class="col-lg-12">
                <label>Versione da modificare</label>
                <select class="form-control designModeChange">
                    <option value="global">Globale</option>
                    <option value="tablet">Tablet</option>
                    <option value="mobile">Mobile</option>
                </select>
            </div>
        </div>

        <?php foreach($popupDesignDatas as $designKey => $designValues) { ?>
            <div class="design-group <?= ($designKey !== 'global' ? 'widget-group' : ''); ?>" data-name="<?= $designKey; ?>" style="<?= ($designKey !== 'global' ? 'display: none;' : ''); ?>">

                <?php if($designKey !== 'global') { ?>
                    <div class="alert alert-info">
                        <b>Non è necessario compilare tutti i campi.</b>
                        I campi vuoti erediteranno i valori dello stile superiore.
                    </div>
                <?php } ?>

                <?php
                $sameBorder = false;
                if(
                    count(array_unique($designValues['margin'])) === 2
                    &&
                    count(array_unique($designValues['padding'])) === 2
                    &&
                    count(array_unique($designValues['border']['width'])) === 2
                ) {
                    $sameBorder = true;
                }
                ?>

                <div class="editModeContainer">
                    <div class="msVisualDesignBoxPreview">
                        <div class="widget-group" data-name="margin">
                            <span class="boxModeLabel">Margine</span>
                            <select class="suffix-select margin-suffix form-control widget-data" name="suffix">
                                <option value="px">px</option>
                                <option value="%" <?= ($designValues['margin']['suffix'] === '%' ? 'selected' : ''); ?>>%</option>
                            </select>
                            <input type="number" min="-500" max="1000" class="widget-data form-control updateBoxPreview editTypeBox" data-type="simple" value="<?= ($sameBorder ? $designValues['margin']['left'] : ''); ?>" placeholder="-">
                            <input type="number" min="-500" max="1000" name="left" value="<?= $designValues['margin']['left']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                            <input type="number" min="-500" max="1000" name="top" value="<?= $designValues['margin']['top']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                            <input type="number" min="-500" max="1000" name="right" value="<?= $designValues['margin']['right']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                            <input type="number" min="-500" max="1000" name="bottom" value="<?= $designValues['margin']['bottom']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                        </div>

                        <div class="msVisualDesignBoxContent">

                            <div class="widget-group" data-name="border">
                                <div class="widget-group" data-name="width">
                                    <span class="boxModeLabel">Bordo</span>
                                    <select class="suffix-select border-suffix form-control widget-data" name="suffix">
                                        <option value="px">px</option>
                                        <option value="%" <?= ($designValues['border']['width']['suffix'] === '%' ? 'selected' : ''); ?>>%</option>
                                    </select>
                                    <input type="number" min="0" max="100" class="widget-data form-control updateBoxPreview editTypeBox" data-type="simple" value="<?= ($sameBorder ? $designValues['border']['width']['left'] : ''); ?>" placeholder="-">
                                    <input type="number" min="0" max="100" name="left" value="<?= $designValues['border']['width']['left']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                                    <input type="number" min="0" max="100" name="top" value="<?= $designValues['border']['width']['top']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                                    <input type="number" min="0" max="100" name="right" value="<?= $designValues['border']['width']['right']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                                    <input type="number" min="0" max="100" name="bottom" value="<?= $designValues['border']['width']['bottom']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                                </div>
                            </div>

                            <div class="msVisualDesignBoxInside">
                                <div class="widget-group" data-name="padding">
                                    <span class="boxModeLabel">Padding</span>
                                    <select class="suffix-select padding-suffix form-control widget-data" name="suffix">
                                        <option value="px">px</option>
                                        <option value="%" <?= ($designValues['padding']['suffix'] === '%' ? 'selected' : ''); ?>>%</option>
                                    </select>
                                    <input type="number" min="0" max="100" class="widget-data form-control updateBoxPreview editTypeBox" data-type="simple" value="<?= ($sameBorder ? $designValues['padding']['left'] : ''); ?>" placeholder="-">
                                    <input type="number" min="0" max="100" name="left" value="<?= $designValues['padding']['left']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                                    <input type="number" min="0" max="100" name="top" value="<?= $designValues['padding']['top']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                                    <input type="number" min="0" max="100" name="right" value="<?= $designValues['padding']['right']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                                    <input type="number" min="0" max="100" name="bottom" value="<?= $designValues['padding']['bottom']; ?>" class="widget-data form-control updateBoxPreview editTypeBox" data-type="advanced" placeholder="-">
                                </div>
                                Contenuto del blocco
                            </div>
                        </div>
                    </div>
                    <label class="text-right pull-right"><input type="checkbox" class="editInAdvancedMode" <?= ($sameBorder === false ? 'checked' : ''); ?>> Modalità avanzata</label>
                </div>

            </div>
        <?php } ?>

    </div>

    <div class="col-sm-8" style="background: white; margin: -15px 0;  border-left: 1px solid #ececec;">

        <?php foreach($popupDesignDatas as $designKey => $designValues) { ?>
            <div class="design-group <?= ($designKey !== 'global' ? 'widget-group' : ''); ?>" data-name="<?= $designKey; ?>" style="<?= ($designKey !== 'global' ? 'display: none;' : ''); ?>">

                <h2 class="title-divider" style="border-top: 0; margin-top: 0px;">
                    Bordo
                </h2>
                <div class="widget-group editModeContainer" data-name="border">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Stile</label>
                            <select class="form-control widget-data updateBoxPreview" name="style">
                                <option value="">Predefinito</option>
                                <option value="none" <?= ($designValues['border']['style'] === 'none' ? ' selected' : ''); ?>>Nessuno</option>
                                <option value="solid" <?= ($designValues['border']['style'] === 'solid' ? ' selected' : ''); ?>>Solido</option>
                                <option value="dotted" <?= ($designValues['border']['style'] === 'dotted' ? ' selected' : ''); ?>>Dotted</option>
                                <option value="dashed" <?= ($designValues['border']['style'] === 'dashed' ? ' selected' : ''); ?>>Dashed</option>
                                <option value="double" <?= ($designValues['border']['style'] === 'double' ? ' selected' : ''); ?>>Double</option>
                                <option value="groove" <?= ($designValues['border']['style'] === 'groove' ? ' selected' : ''); ?>>Groove</option>
                                <option value="ridge" <?= ($designValues['border']['style'] === 'ridge' ? ' selected' : ''); ?>>Ridge</option>
                                <option value="inset" <?= ($designValues['border']['style'] === 'inset' ? ' selected' : ''); ?>>Inset</option>
                                <option value="outset" <?= ($designValues['border']['style'] === 'outset' ? ' selected' : ''); ?>>Outset</option>
                                <option value="initial" <?= ($designValues['border']['style'] === 'initial' ? ' selected' : ''); ?>>Initial</option>
                                <option value="inherit" <?= ($designValues['border']['style'] === 'inherit' ? ' selected' : ''); ?>>Inherit</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Colore</label>
                            <div class="input-group colorpicker-component">
                                <input class="form-control widget-data updateBoxPreview" name="color" type="text" value="<?= ($designValues['border']['color']); ?>">
                                <span class="input-group-addon"><i></i></span>
                            </div>
                        </div>
                    </div>
                </div>

                <h2 class="title-divider">
                    Sfondo
                </h2>
                <div class="widget-group" data-name="background">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Dimensione</label>
                                    <select class="form-control widget-data" name="size">
                                        <option value="">Predefinito</option>
                                        <?php
                                        foreach(array('auto', 'cover', 'contain', 'initial', 'inherit') as $mode) {
                                            echo '<option value="' . $mode . '" ' . ($designValues['background']['size'] === $mode ? 'selected' : '') . '>' . $mode . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-lg-6">
                                    <label>Ripeti</label>
                                    <select class="form-control widget-data" name="repeat">
                                        <option value="">Predefinito</option>
                                        <?php
                                        foreach(array('no-repeat', 'repeat', 'repeat-x', 'repeat-y', 'initial', 'inherit') as $mode) {
                                            echo '<option value="' . $mode . '" ' . ($designValues['background']['repeat'] === $mode ? 'selected' : '') . '>' . $mode . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-lg-6">
                                    <label>Posizione</label>
                                    <select class="form-control widget-data" name="position">
                                        <option value="">Predefinito</option>
                                        <?php
                                        foreach(array('left top', 'left center', 'left bottom', 'right top', 'right center', 'right bottom', 'center top', 'center center', 'center bottom', 'initial', 'inherit') as $mode) {
                                            echo '<option value="' . $mode . '" ' . ($designValues['background']['position'] === $mode ? 'selected' : '') . '>' . $mode . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-lg-6">
                                    <label>Attaccamento</label>
                                    <select class="form-control widget-data" name="attachment">
                                        <option value="">Predefinito</option>
                                        <?php
                                        foreach(array('scroll', 'fixed', 'local', 'initial', 'inherit') as $mode) {
                                            echo '<option value="' . $mode . '" ' . ($designValues['background']['attachment'] === $mode ? 'selected' : '') . '>' . $mode . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="col-lg-12">
                                <label>Immagine</label>
                                <div class="widget-data uploader" name="image" data-id="design_background_image_<?=$designKey; ?>">
                                    <?php (new \MSFramework\uploads($_POST['settings']['uploads'], $_POST['settings']['uploads_common']))->initUploaderHTML("design_background_image_" . $designKey, (is_array($designValues['background']['image']['files']) ? $designValues['background']['image']['files'] : array())); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h2 class="title-divider">Overlay</h2>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Colore</label>
                            <div class="input-group colorpicker-component">
                                <input class="form-control widget-data updateBoxPreview" name="color" type="text" value="<?= ($designValues['background']['color']); ?>">
                                <span class="input-group-addon"><i></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label>Opacità</label>
                            <select class="form-control widget-data" name="opacity">
                                <option value="">Predefinito</option>
                                <?php
                                for($i = 100; $i >= 0; $i--) {
                                    echo '<option value="' . $i . '" ' . ($designValues['background']['opacity'] === (string)$i ? 'selected' : '') . '>' . $i . '%</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="widget-group editModeContainer" data-name="border-radius">
                    <h2 class="title-with-button">
                        Angoli arrotondati
                        <select class="border-radius-suffix pull-right form-control widget-data" name="suffix" style="width: 70px; margin: -4px -10px;">
                            <option value="px">px</option>
                            <option value="%" <?= ($designValues['border-radius']['suffix'] === '%' ? 'selected' : ''); ?>>%</option>
                        </select>
                    </h2>

                    <?php
                    $sameBorder = false;
                    if(count(array_unique($designValues['border-radius'])) === 2) {
                        $sameBorder = $designValues['border-radius']['top-left'];
                    } else if(!$designValues['border-radius']) {
                        $sameBorder = '';
                    }
                    ?>

                    <div class="row">
                        <div class="col-md-12 editTypeBox" data-type="simple">
                            <label>Dimensione</label>
                            <input type="number" min="1" max="100" class="form-control updateBoxPreview" value="<?= ($sameBorder ? $sameBorder : ''); ?>" placeholder="Predefinito">
                        </div>

                        <div class="col-md-3 col-sm-6 editTypeBox" data-type="advanced">
                            <label>Top Left</label>
                            <input type="number" min="1" max="100" name="top-left" value="<?= $designValues['border-radius']['top-left']; ?>" class="widget-data form-control updateBoxPreview" placeholder="Predefinito">
                        </div>

                        <div class="col-md-3 col-sm-6 editTypeBox" data-type="advanced">
                            <label>Top Right</label>
                            <input type="number" min="1" max="100" name="top-right" value="<?= $designValues['border-radius']['top-right']; ?>" class="widget-data form-control updateBoxPreview" placeholder="Predefinito">
                        </div>

                        <div class="col-md-3 col-sm-6 editTypeBox" data-type="advanced">
                            <label>Bottom Left</label>
                            <input type="number" min="1" max="100" name="bottom-left" value="<?= $designValues['border-radius']['bottom-left']; ?>" class="widget-data form-control updateBoxPreview" placeholder="Predefinito">
                        </div>

                        <div class="col-md-3 col-sm-6 editTypeBox" data-type="advanced">
                            <label>Bottom Right</label>
                            <input type="number" min="1" max="100" name="bottom-right" value="<?= $designValues['border-radius']['bottom-right']; ?>" class="widget-data form-control updateBoxPreview" placeholder="Predefinito">
                        </div>
                    </div>

                    <label class="text-right pull-right"><input type="checkbox" class="editInAdvancedMode" <?= ($sameBorder === false ? 'checked' : ''); ?>> Modalità avanzata</label>
                    <div style="clear: both;"></div>
                </div>

                <h2 class="title-divider">
                    Dimensioni
                </h2>

                <div class="widget-group" data-name="sizes">

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Width</label>
                            <input type="text" class="widget-data form-control size-input" name="width" placeholder="Es: 100px o 100%" value="<?= $designValues['sizes']['width']; ?>">
                        </div>
                        <div class="col-lg-6">
                            <label>Height</label>
                            <input type="text" class="widget-data form-control size-input" name="height" placeholder="Es: 100px o 100%" value="<?= $designValues['sizes']['height']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <label>Max Width</label>
                            <input type="text" class="widget-data form-control size-input" name="max-width" placeholder="Es: 100px o 100%" value="<?= $designValues['sizes']['max-width']; ?>">
                        </div>
                        <div class="col-lg-3">
                            <label>Max Height</label>
                            <input type="text" class="widget-data form-control size-input" name="max-height" placeholder="Es: 100px o 100%" value="<?= $designValues['sizes']['max-height']; ?>">
                        </div>
                        <div class="col-lg-3">
                            <label>Min Width</label>
                            <input type="text" class="widget-data form-control size-input" name="min-width" placeholder="Es: 100px o 100%" value="<?= $designValues['sizes']['min-width']; ?>">
                        </div>
                        <div class="col-lg-3">
                            <label>Min Height</label>
                            <input type="text" class="widget-data form-control size-input" name="min-height" placeholder="Es: 100px o 100%" value="<?= $designValues['sizes']['min-height']; ?>">
                        </div>
                    </div>
                </div>

                <h2 class="title-divider">
                    Contenuto
                </h2>

                <div class="widget-group" data-name="content">
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Allineamento</label>
                            <select class="form-control widget-data" name="align">
                                <option value="">Predefinito</option>
                                <?php foreach(array('left' => 'Sinistra', 'center' => 'Centro', 'right' => 'Destra') as $posVal => $posName) { ?>
                                    <option value="<?= $posVal; ?>" <?= ($designValues['content']['align'] == $posVal ? 'selected' : ''); ?>><?= $posName; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <?php if($designKey === 'global') { ?>
                <h2 class="title-divider">
                    Attributi
                </h2>
                <div class="widget-group" data-name="attributes">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>ID Elemento</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci un ID da applicare all'elemento. Assicurati che sia univoco."></i></span>
                                    <input type="text" class="widget-data form-control" name="id" value="<?= htmlentities($designValues['attributes']['id']); ?>" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Classi CSS</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inserisci una lista di classi CSS separate semplicemente da uno spazio."></i></span>
                                    <input type="text" class="widget-data form-control" name="classes" value="<?= htmlentities($designValues['attributes']['classes']); ?>" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        <?php } ?>

    </div>
</div>
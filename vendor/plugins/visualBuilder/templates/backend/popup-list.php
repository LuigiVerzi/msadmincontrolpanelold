<?php
$blocksList = $MSFrameworkVisualBuilder->getBlocksList(false, $_POST['settings']['structure_mode'], $_POST['settings']['private_blocks'], $_POST['settings']['exclude_blocks']);

foreach($blocksList as $k => $v) {
    if(($k === 'main' && !isset($blocksList['structure'])) || $k === 'structure') {
        unset($blocksList[$k]);
        $blocksList = array_merge(array($k => $v), $blocksList);
    }
}

$uniqueID = uniqid('nav');
?>

<div class="modal inmodal visualBuilderBlocksModal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="float: none; margin: 0;">Aggiungi contenuto</h4>
                <small>Seleziona un elemento per aggiungerlo dentro il blocco selezionato.</small>
            </div>
            <div class="modal-body">
                <div class="tabs-container tabs-container-header">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php $navActive = false; ?>
                        <?php foreach($blocksList as $k => $categoryInfo) { ?>
                            <li class="<?= (!$navActive ? 'active' : ''); ?>"><a class="nav-link" data-toggle="tab" href="#tab-<?= $k; ?>-<?= $uniqueID; ?>"><?= $categoryInfo['title']; ?></a></li>
                            <?php $navActive = true; ?>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <?php $navActive = false; ?>
                        <?php foreach($blocksList as $k => $categoryInfo) { ?>
                            <div role="tabpanel" id="tab-<?= $k; ?>-<?= $uniqueID; ?>" class="tab-pane <?= (!$navActive ? 'active' : ''); ?>">
                                <div class="panel-body">
                                    <div class="row">
                                        <?php
                                        foreach($categoryInfo['blocks'] as $blockInfo) {
                                            include('parts/popup_btn.php');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php $navActive = true; ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>


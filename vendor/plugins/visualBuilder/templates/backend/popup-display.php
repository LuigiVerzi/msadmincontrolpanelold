<div class="widget-group" data-name="display" style="background: white; margin: -15px -15px 0; border-bottom: 1px solid #ececec; padding: 15px;">
    <h2 class="title-divider">Visualizzazione tramite data</h2>
    <div class="widget-group" data-name="dates">
        <div class="row">
            <div class="col-lg-6">
                <label>Mostra solo dopo data</label>
                <input class="form-control widget-data datetimepicker" name="hide-before" type="text" placeholder="Formato: d/m/Y H:i"  value="<?= htmlentities($widgetData['display']['dates']['hide-before']); ?>">
            </div>
            <div class="col-lg-6">
                <label>Nascondi solo dopo data</label>
                <input class="form-control widget-data datetimepicker" name="hide-after" type="text" placeholder="Formato: d/m/Y H:i"  value="<?= htmlentities($widgetData['display']['dates']['hide-after']); ?>">
            </div>
        </div>
    </div>
</div>

<div class="widget-group" data-name="responsive" style="margin: 0 -15px;">
    <?php if($_POST['popup'] === 'col') { ?>
        <table class="display table table-striped table-bordered" style="margin: 0 !important;" id="visualBuilderResponsiveOptions">
            <thead>
            <tr class="text-center">
                <th>Dispositivo</th>
                <th>Offset</th>
                <th>Larghezza</th>
                <th>Nascondi</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach(array('desktop' => '', 'tablet' => '-md', 'mobile' => '-sm') as $device => $sh) { ?>
                <tr class="widget-group" data-name="<?= $device; ?>">
                    <td style="background: #fafafa; vertical-align: middle;" class="text-center">
                        <i class="fa fa-<?= $device; ?> fa-2x btn-block"></i>
                        <?= ucfirst($device); ?>
                    </td>
                    <td>
                        <select class="widget-data form-control" name="offset">
                            <option value="" style="color: #ccc;">Impostazioni predefinite</option>
                            <option value="ms-col-offset-0" style="color: #ccc;" <?= ($widgetData['responsive'][$device]['offset'] == '0' ? 'selected' : ''); ?>>Nessun offset</option>
                            <optgroup label="Sposta a sinistra">
                                <?php for($i = -12; $i <= -1; $i++) { ?>
                                    <option value="<?= $i; ?>" <?= ($widgetData['responsive'][$device]['offset'] == $i ? 'selected' : ''); ?>>- <?= abs($i); ?> colonn<?= (abs($i) > 1 ? 'e' : 'a'); ?></option>
                                <?php } ?>
                            </optgroup>
                            <optgroup label="Sposta a destra">
                                <?php for($i = 1; $i <= 12; $i++) { ?>
                                    <option value="<?= $i; ?>" <?= ($widgetData['responsive'][$device]['offset'] == $i ? 'selected' : ''); ?>><?= $i; ?> colonn<?= ($i > 1 ? 'e' : 'a'); ?></option>
                                <?php } ?>
                            </optgroup>
                        </select>
                    </td>
                    <td>
                        <select class="widget-data form-control" name="col">
                            <?php if($device !== 'desktop') { ?>
                                <option value="" style="color: #ccc;">Impostazioni predefinite</option>
                            <?php } ?>
                            <?php for($i = 1; $i <= 12; $i++) { ?>
                                <option value="<?= $i; ?>" <?= ($widgetData['responsive'][$device]['col'] == $i ? 'selected' : ''); ?>><?= $i; ?> colonn<?= ($i > 1 ? 'e' : 'a'); ?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td class="text-center" style="vertical-align: middle;">
                        <label><input type="checkbox" name="hide" value="1" class="widget-data i-checks" <?= ($widgetData['responsive'][$device]['hide'] == 1 ? 'checked' : ''); ?>></label>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <h2 class="title-divider" style="margin: 0 0 15px">Nascondi su dispositivi</h2>
        <table class="display table table-striped table-bordered" style="margin: 0 !important;" id="visualBuilderResponsiveOptions">
            <thead>
            <tr class="text-center">
                <?php foreach(array('desktop' => '', 'tablet' => '-md', 'mobile' => '-sm') as $device => $sh) { ?>
                    <th style="background: #fafafa; vertical-align: middle;" class="text-center">
                        <i class="fa fa-<?= $device; ?> fa-2x btn-block"></i>
                        <?= ucfirst($device); ?>
                    </th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>

            <tr>
                <?php foreach(array('desktop' => '', 'tablet' => '-md', 'mobile' => '-sm') as $device => $sh) { ?>
                    <td class="widget-group text-center" data-name="<?= $device; ?>" style="vertical-align: middle;">
                        <label><input type="checkbox" name="hide" value="1" class="widget-data i-checks" <?= ($widgetData['responsive'][$device]['hide'] == 1 ? 'checked' : ''); ?>></label>
                    </td>
                <?php } ?>
            </tr>
            </tbody>
        </table>
    <?php } ?>
</div>
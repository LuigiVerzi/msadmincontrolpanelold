<div class="visualBuilderWidgetContent" data-name="[action-title]" data-icon="[action-icon]">
    <div class="visualBuilderDesignPreview actionPreview"></div>
    <div class="visualBuilderWidgetContentActions">
        <a href="#" class="visualBuilderWidgetContentAction_Btn" data-action="move" title="Muovi" style="cursor: move;"><i class="fa fa-arrows"></i></a>
        <a href="#" class="visualBuilderWidgetContentAction_Btn" data-action="edit" title="Modifica"><i class="fa fa-pencil"></i></a>
        <a href="#" class="visualBuilderWidgetContentAction_Btn" data-action="duplicate" title="Duplica"><i class="fa fa-files-o"></i></a>
        <a href="#" class="visualBuilderWidgetContentAction_Btn" data-action="delete" title="Elimina"><i class="fa fa-trash"></i></a>
    </div>
    [action-content]
    <script type="application/json" class="widget-data">[action-data]</script>
</div>
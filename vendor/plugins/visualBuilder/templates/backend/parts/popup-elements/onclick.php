<h2 class="title-divider">
    Evento al click
</h2>

<div class="widget-group" data-name="onclick">
    <div class="row">
        <div class="col-lg-12">
            <label>Evento</label>
            <select class="widget-data form-control" name="event">
                <?php foreach(
                    array(
                        '' => 'Nessuno',
                        'open_page' => 'Apri pagina',
                        'open_popup' => 'Apri popup',
                        'open_link' => 'Apri link'
                    ) as $ev_v => $ev_t) {
                    echo '<option value="' . $ev_v . '" ' . ($widgetData['data']['onclick']['event'] == $ev_v ? 'selected' : '') . '>' . $ev_t . '</option>';
                } ?>
            </select>
        </div>
    </div>

    <div class="widget-group onClickEventSettings" data-name="open_link" style="display: none;">
        <div class="row">
            <div class="col-lg-6">
                <label>URL</label>
                <input type="text" class="widget-data form-control msShort " name="url" placeholder="" value="<?= $widgetData['data']['onclick']['open_link']['url']; ?>">
            </div>
            <div class="col-lg-6">
                <label>Target</label>
                <select class="widget-data form-control" name="target">
                    <?php foreach(
                        array(
                            '' => 'Predefinito',
                            '_self' => 'Apri nella stessa scheda',
                            '_blank' => 'Apri in una nuova scheda',
                            'popup' => 'Apri dentro un Popup'
                        ) as $ev_v => $ev_t) {
                        echo '<option value="' . $ev_v . '" ' . ($widgetData['data']['onclick']['open_link']['target'] == $ev_v ? 'selected' : '') . '>' . $ev_t . '</option>';
                    } ?>
                </select>
            </div>
        </div>
    </div>

    <div class="widget-group onClickEventSettings" data-name="open_popup" style="display: none;">
        <div class="row">

            <?php if($_POST['settings']["use_relative_page"] == 1) { ?>
                <div class="col-lg-12">
                    <div class="alert alert-warning">Selezionando 'Apri pagina collegata', il click sull'elemento punterà ad una eventuale pagina collegata con l'oggetto in questione.
                        <br>
                        Nel caso in cui non esistesse nessuna pagina relativa, il pulsante non verrà mostrato.
                    </div>
                </div>
            <?php } ?>

            <div class="col-lg-9">
                <label>Popup</label>
                <select class="form-control widget-data" name="id" id="id_popup">
                    <?php if($_POST['settings']["use_relative_page"] == 1) { ?>
                        <option value="relative" class="alert-info">Apri pagina collegata</option>
                    <?php } else { ?>
                        <option value="">Seleziona popup/pagina</option>
                    <?php } ?>
                    <optgroup label="Popup">
                        <?php foreach((new \MSFramework\popup())->getPopupDetails() as $popup) { ?>
                        <option value="<?= $popup['id']; ?>" <?= ($widgetData['data']['onclick']['open_popup']['id'] == $popup['id'] ? 'selected' : ''); ?>><?= $popup['titolo']; ?></option>
                    <?php } ?>
                    </optgroup>
                    <optgroup label="Pagine">
                        <?php foreach((new \MSFramework\pages())->getPageDetails() as $page) { ?>
                            <option value="page_<?= $page['id']; ?>" <?= ($widgetData['data']['onclick']['open_popup']['id'] == 'page_' . $page['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($page['titolo']); ?></option>
                        <?php } ?>
                    </optgroup>
                </select>
            </div>

            <div class="col-lg-3">
                <label> </label>
                <div class="text-right" style="display: flex;">
                    <a href="#" class="btn btn-warning btn-outline" id="editCurrentPopup" style="width: 100%; margin-right: 5px; display: <?= ($widgetData['data']['onclick']['open_popup']['id'] && stristr('page', $widgetData['data']['onclick']['open_popup']['id']) && $widgetData['data']['onclick']['open_popup']['id'] !== 'relative' ? 'inline-block' : 'none'); ?>;">Modifica</a>
                    <a href="#" class="btn btn-default" id="creatNewPopup" style="width: 100%;">Crea nuovo</a>
                </div>
            </div>
        </div>

        <div class="widget-group popup-btn-settings ibox-content" data-name="appearance" style="margin-top: 15px; display: <?= ($widgetData['data']['onclick']['open_popup']['id'] === 'relative' || strpos('page', $widgetData['data']['onclick']['open_popup']['id']) ? 'block' : 'none') ; ?>">

            <h2 class="title-divider">Aspetto popup</h2>

            <div class="row">
                <div class="col-lg-3">
                    <label>Larghezza massima <small>(px)</small></label>
                    <input name="max-width" type="number" class="form-control widget-data" placeholder="1520" value="<?php echo htmlentities($widgetData['data']['onclick']['open_popup']['appearance']['max-width']) ?>">
                </div>
                <div class="col-lg-3">
                    <label>Overlay</label>
                    <select class="form-control widget-data" name="overlay">
                        <option value="">Nessuno</option>
                        <option value="light" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['overlay'] === 'light' ? 'selected' : ''); ?>>Chiaro</option>
                        <option value="dark" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['overlay'] === 'dark' ? 'selected' : ''); ?>>Scuro</option>
                    </select>
                </div>
            </div>

            <h2 class="title-divider">Animazioni</h2>

            <div class="row">
                <div class="col-lg-6">
                    <label>Animazione in entrata</label>
                    <select class="form-control widget-data" name="in-animation">
                        <optgroup label="Bouncing Entrances">
                            <option value="bounceIn" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'bounceIn' ? 'selected' : ''); ?>>bounceIn</option>
                            <option value="bounceInDown" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'bounceInDown' ? 'selected' : ''); ?>>bounceInDown</option>
                            <option value="bounceInLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'bounceInLeft' ? 'selected' : ''); ?>>bounceInLeft</option>
                            <option value="bounceInRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'bounceInRight' ? 'selected' : ''); ?>>bounceInRight</option>
                            <option value="bounceInUp" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'bounceInUp' ? 'selected' : ''); ?>>bounceInUp</option>
                        </optgroup>

                        <optgroup label="Fading Entrances">
                            <option value="fadeIn" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'fadeIn' ? 'selected' : ''); ?>>fadeIn</option>
                            <option value="fadeInDown" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'fadeInDown' ? 'selected' : ''); ?>>fadeInDown</option>
                            <option value="fadeInDownBig" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'fadeInDownBig' ? 'selected' : ''); ?>>fadeInDownBig</option>
                            <option value="fadeInLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'fadeInLeft' ? 'selected' : ''); ?>>fadeInLeft</option>
                            <option value="fadeInLeftBig" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'fadeInLeftBig' ? 'selected' : ''); ?>>fadeInLeftBig</option>
                            <option value="fadeInRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'fadeInRight' ? 'selected' : ''); ?>>fadeInRight</option>
                            <option value="fadeInRightBig" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'fadeInRightBig' ? 'selected' : ''); ?>>fadeInRightBig</option>
                            <option value="fadeInUp" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'fadeInUp' ? 'selected' : ''); ?>>fadeInUp</option>
                            <option value="fadeInUpBig" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'fadeInUpBig' ? 'selected' : ''); ?>>fadeInUpBig</option>
                        </optgroup>

                        <optgroup label="Flippers">
                            <option value="flip" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'flip' ? 'selected' : ''); ?>>flip</option>
                            <option value="flipInX" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'flipInX' ? 'selected' : ''); ?>>flipInX</option>
                            <option value="flipInY" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'flipInY' ? 'selected' : ''); ?>>flipInY</option>
                            <option value="flipOutX" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'flipOutX' ? 'selected' : ''); ?>>flipOutX</option>
                            <option value="flipOutY" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'flipOutY' ? 'selected' : ''); ?>>flipOutY</option>
                        </optgroup>

                        <optgroup label="Lightspeed">
                            <option value="lightSpeedIn" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'lightSpeedIn' ? 'selected' : ''); ?>>lightSpeedIn</option>
                            <option value="lightSpeedOut" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'lightSpeedOut' ? 'selected' : ''); ?>>lightSpeedOut</option>
                        </optgroup>

                        <optgroup label="Rotating Entrances">
                            <option value="rotateIn" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'rotateIn' ? 'selected' : ''); ?>>rotateIn</option>
                            <option value="rotateInDownLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'rotateInDownLeft' ? 'selected' : ''); ?>>rotateInDownLeft</option>
                            <option value="rotateInDownRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'rotateInDownRight' ? 'selected' : ''); ?>>rotateInDownRight</option>
                            <option value="rotateInUpLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'rotateInUpLeft' ? 'selected' : ''); ?>>rotateInUpLeft</option>
                            <option value="rotateInUpRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'rotateInUpRight' ? 'selected' : ''); ?>>rotateInUpRight</option>
                        </optgroup>

                        <optgroup label="Sliding Entrances">
                            <option value="slideInUp" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'slideInUp' ? 'selected' : ''); ?>>slideInUp</option>
                            <option value="slideInDown" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'slideInDown' ? 'selected' : ''); ?>>slideInDown</option>
                            <option value="slideInLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'slideInLeft' ? 'selected' : ''); ?>>slideInLeft</option>
                            <option value="slideInRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'slideInRight' ? 'selected' : ''); ?>>slideInRight</option>

                        </optgroup>

                        <optgroup label="Zoom Entrances">
                            <option value="zoomIn" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'zoomIn' ? 'selected' : ''); ?>>zoomIn</option>
                            <option value="zoomInDown" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'zoomInDown' ? 'selected' : ''); ?>>zoomInDown</option>
                            <option value="zoomInLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'zoomInLeft' ? 'selected' : ''); ?>>zoomInLeft</option>
                            <option value="zoomInRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'zoomInRight' ? 'selected' : ''); ?>>zoomInRight</option>
                            <option value="zoomInUp" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['in-animation'] === 'zoomInUp' ? 'selected' : ''); ?>>zoomInUp</option>
                        </optgroup>
                    </select>
                </div>
                <div class="col-lg-6">
                    <label>Animazione in uscita</label>
                    <select class="form-control widget-data" name="out-animation">

                        <optgroup label="Bouncing Exits">
                            <option value="bounceOut" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'bounceOut' ? 'selected' : ''); ?>>bounceOut</option>
                            <option value="bounceOutDown" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'bounceOutDown' ? 'selected' : ''); ?>>bounceOutDown</option>
                            <option value="bounceOutLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'bounceOutLeft' ? 'selected' : ''); ?>>bounceOutLeft</option>
                            <option value="bounceOutRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'bounceOutRight' ? 'selected' : ''); ?>>bounceOutRight</option>
                            <option value="bounceOutUp" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'bounceOutUp' ? 'selected' : ''); ?>>bounceOutUp</option>
                        </optgroup>

                        <optgroup label="Fading Exits">
                            <option value="fadeOut" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'fadeOut' ? 'selected' : ''); ?>>fadeOut</option>
                            <option value="fadeOutDown" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'fadeOutDown' ? 'selected' : ''); ?>>fadeOutDown</option>
                            <option value="fadeOutDownBig" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'fadeOutDownBig' ? 'selected' : ''); ?>>fadeOutDownBig</option>
                            <option value="fadeOutLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'fadeOutLeft' ? 'selected' : ''); ?>>fadeOutLeft</option>
                            <option value="fadeOutLeftBig" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'fadeOutLeftBig' ? 'selected' : ''); ?>>fadeOutLeftBig</option>
                            <option value="fadeOutRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'fadeOutRight' ? 'selected' : ''); ?>>fadeOutRight</option>
                            <option value="fadeOutRightBig" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'fadeOutRightBig' ? 'selected' : ''); ?>>fadeOutRightBig</option>
                            <option value="fadeOutUp" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'fadeOutUp' ? 'selected' : ''); ?>>fadeOutUp</option>
                            <option value="fadeOutUpBig" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'fadeOutUpBig' ? 'selected' : ''); ?>>fadeOutUpBig</option>
                        </optgroup>

                        <optgroup label="Rotating Exits">
                            <option value="rotateOut" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'rotateOut' ? 'selected' : ''); ?>>rotateOut</option>
                            <option value="rotateOutDownLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'rotateOutDownLeft' ? 'selected' : ''); ?>>rotateOutDownLeft</option>
                            <option value="rotateOutDownRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'rotateOutDownRight' ? 'selected' : ''); ?>>rotateOutDownRight</option>
                            <option value="rotateOutUpLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'rotateOutUpLeft' ? 'selected' : ''); ?>>rotateOutUpLeft</option>
                            <option value="rotateOutUpRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'rotateOutUpRight' ? 'selected' : ''); ?>>rotateOutUpRight</option>
                        </optgroup>

                        <optgroup label="Sliding Exits">
                            <option value="slideOutUp" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'slideOutUp' ? 'selected' : ''); ?>>slideOutUp</option>
                            <option value="slideOutDown" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'slideOutDown' ? 'selected' : ''); ?>>slideOutDown</option>
                            <option value="slideOutLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'slideOutLeft' ? 'selected' : ''); ?>>slideOutLeft</option>
                            <option value="slideOutRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'slideOutRight' ? 'selected' : ''); ?>>slideOutRight</option>
                        </optgroup>

                        <optgroup label="Zoom Exits">
                            <option value="zoomOut" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'zoomOut' ? 'selected' : ''); ?>>zoomOut</option>
                            <option value="zoomOutDown" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'zoomOutDown' ? 'selected' : ''); ?>>zoomOutDown</option>
                            <option value="zoomOutLeft" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'zoomOutLeft' ? 'selected' : ''); ?>>zoomOutLeft</option>
                            <option value="zoomOutRight" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'zoomOutRight' ? 'selected' : ''); ?>>zoomOutRight</option>
                            <option value="zoomOutUp" <?= ($widgetData['data']['onclick']['open_popup']['appearance']['out-animation'] === 'zoomOutUp' ? 'selected' : ''); ?>>zoomOutUp</option>
                        </optgroup>
                    </select>
                </div>
            </div>
        </div>

    </div>

    <div class="widget-group onClickEventSettings" data-name="open_page" style="display: none;">
        <div class="row">

            <?php if($_POST['settings']["use_relative_page"] == 1) { ?>
                <div class="col-lg-12">
                    <div class="alert alert-warning">Selezionando 'Apri pagina collegata', il click sull'elemento punterà ad una eventuale pagina collegata con l'oggetto in questione.
                        <br>
                        Nel caso in cui non esistesse nessuna pagina relativa, il pulsante non verrà mostrato.
                    </div>
                </div>
            <?php } ?>

            <div class="col-lg-6">
                <label>Pagina</label>
                <select class="form-control widget-data" name="id" id="id_page">
                    <?php if($_POST['settings']["use_relative_page"] == 1) { ?>
                        <option value="relative" class="alert-info">Apri pagina collegata</option>
                    <?php } else { ?>
                        <option value="">Seleziona pagina</option>
                    <?php } ?>

                    <?php foreach((new \MSFramework\pages())->getPageDetails() as $page) { ?>
                        <option value="<?= $page['id']; ?>" <?= ($widgetData['data']['onclick']['open_page']['id'] == $page['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($page['titolo']); ?></option>
                    <?php } ?>
                </select>

                <div class="text-right">
                    <a href="#" class="btn btn-sm btn-warning btn-outline" id="editCurrentPage" style="display: <?= ($widgetData['data']['onclick']['open_page']['id'] ? 'inline-block' : 'none'); ?>;">Modifica</a>
                    <a href="#" class="btn btn-sm btn-default" id="createNewPage">Crea nuova</a>
                </div>
            </div>

            <div class="col-lg-6">
                <label>Target</label>
                <select class="widget-data form-control" name="target">
                    <?php foreach(
                        array(
                            '' => 'Predefinito',
                            '_self' => 'Apri nella stessa scheda',
                            '_blank' => 'Apri in una nuova scheda'
                        ) as $ev_v => $ev_t) {
                        echo '<option value="' . $ev_v . '" ' . ($widgetData['data']['onclick']['open_link']['target'] == $ev_v ? 'selected' : '') . '>' . $ev_t . '</option>';
                    } ?>
                </select>
            </div>
        </div>
    </div>
</div>
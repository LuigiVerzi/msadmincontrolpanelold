<h2 class="title-divider">Metodo visualizzazione</h2>
<div class="widget-group" data-name="view-mode">
    <div class="row">
        <div class="col-lg-12">
            <label>Metodo visualizzazione</label>
            <select class="form-control widget-data" name="mode">
                <option value="grid" <?= ($widgetData['data']['view-mode']['mode'] == 'grid' ? 'selected' : ''); ?>>Griglia</option>
                <option value="carousel" <?= ($widgetData['data']['view-mode']['mode'] == 'carousel' ? 'selected' : ''); ?>>Carosello</option>
            </select>
        </div>

        <div class="col-lg-12">
            <label>Mostra miniature</label>
            <select class="form-control widget-data" name="thumbnails">
                <option value="">No</option>
                <option value="top" <?= ($widgetData['data']['view-mode']['thumbnails'] == 'top' ? 'selected' : ''); ?>>Sopra</option>
                <option value="bottom" <?= ($widgetData['data']['view-mode']['thumbnails'] == 'bottom' ? 'selected' : ''); ?>>Sotto</option>
            </select>
        </div>

        <div class="col-lg-12">
            <div class="widget-group" data-name="grid" style="display: none;">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="title-divider">Impostazioni griglia</h2>
                    </div>

                    <div class="col-lg-4">
                        <label>Colonne per riga</label>
                        <select class="form-control widget-data" name="cols">
                            <option value="ms-col-12" <?= ($widgetData['data']['view-mode']['grid']['cols'] == 'ms-col-12' ? 'selected' : ''); ?>>1</option>
                            <option value="ms-col-2" <?= ($widgetData['data']['view-mode']['grid']['cols'] == 'ms-col-2' ? 'selected' : ''); ?>>6</option>
                            <option value="ms-col-3" <?= ($widgetData['data']['view-mode']['grid']['cols'] == 'ms-col-3' ? 'selected' : ''); ?>>4</option>
                            <option value="ms-col-4" <?= ($widgetData['data']['view-mode']['grid']['cols'] == 'ms-col-4' ? 'selected' : ''); ?>>3</option>
                            <option value="ms-col-6" <?= ($widgetData['data']['view-mode']['grid']['cols'] == 'ms-col-6' ? 'selected' : ''); ?>>2</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label>Colonne (Tablet)</label>
                        <select class="form-control widget-data" name="md-cols">
                            <option value="">Eredita da Desktop</option>
                            <option value="ms-col-md-12" <?= ($widgetData['data']['view-mode']['grid']['md-cols'] == 'ms-col-md-12' ? 'selected' : ''); ?>>1</option>
                            <option value="ms-col-md-3" <?= ($widgetData['data']['view-mode']['grid']['md-cols'] == 'ms-col-md-3' ? 'selected' : ''); ?>>4</option>
                            <option value="ms-col-md-4" <?= ($widgetData['data']['view-mode']['grid']['md-cols'] == 'ms-col-md-4' ? 'selected' : ''); ?>>3</option>
                            <option value="ms-col-md-6" <?= ($widgetData['data']['view-mode']['grid']['md-cols'] == 'ms-col-md-6' ? 'selected' : ''); ?>>2</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label>Colonne (Mobile)</label>
                        <select class="form-control widget-data" name="sm-cols">
                            <option value="ms-col-sm-12" <?= ($widgetData['data']['view-mode']['grid']['sm-cols'] == 'ms-col-sm-12' ? 'selected' : ''); ?>>1</option>
                            <option value="ms-col-sm-3" <?= ($widgetData['data']['view-mode']['grid']['sm-cols'] == 'ms-col-sm-3' ? 'selected' : ''); ?>>4</option>
                            <option value="ms-col-sm-4" <?= ($widgetData['data']['view-mode']['grid']['sm-cols'] == 'ms-col-sm-4' ? 'selected' : ''); ?>>3</option>
                            <option value="ms-col-sm-6" <?= ($widgetData['data']['view-mode']['grid']['sm-cols'] == 'ms-col-sm-6' ? 'selected' : ''); ?>>2</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="widget-group" data-name="carousel" style="display: none;">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="title-divider">Impostazioni carosello</h2>
                    </div>
                    <div class="col-lg-4">
                        <label>Colonne carosello</label>
                        <select class="form-control widget-data" name="cols">
                            <option value="6" <?= ($widgetData['data']['view-mode']['carousel']['cols'] == '6' ? 'selected' : ''); ?>>6</option>
                            <option value="5" <?= ($widgetData['data']['view-mode']['carousel']['cols'] == '5' ? 'selected' : ''); ?>>5</option>
                            <option value="4" <?= ($widgetData['data']['view-mode']['carousel']['cols'] == '4' ? 'selected' : ''); ?>>4</option>
                            <option value="3" <?= ($widgetData['data']['view-mode']['carousel']['cols'] == '3' ? 'selected' : ''); ?>>3</option>
                            <option value="2" <?= ($widgetData['data']['view-mode']['carousel']['cols'] == '2' ? 'selected' : ''); ?>>2</option>
                            <option value="1" <?= ($widgetData['data']['view-mode']['carousel']['cols'] == '1' ? 'selected' : ''); ?>>1</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label>Colonne (Tablet)</label>
                        <select class="form-control widget-data" name="md-cols">
                            <option value="6" <?= ($widgetData['data']['view-mode']['carousel']['md-cols'] == '6' ? 'selected' : ''); ?>>6</option>
                            <option value="5" <?= ($widgetData['data']['view-mode']['carousel']['md-cols'] == '5' ? 'selected' : ''); ?>>5</option>
                            <option value="4" <?= ($widgetData['data']['view-mode']['carousel']['md-cols'] == '4' ? 'selected' : ''); ?>>4</option>
                            <option value="3" <?= ($widgetData['data']['view-mode']['carousel']['md-cols'] == '3' ? 'selected' : ''); ?>>3</option>
                            <option value="2" <?= ($widgetData['data']['view-mode']['carousel']['md-cols'] == '2' ? 'selected' : ''); ?>>2</option>
                            <option value="1" <?= ($widgetData['data']['view-mode']['carousel']['md-cols'] == '1' ? 'selected' : ''); ?>>1</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label>Colonne (Mobile)</label>
                        <select class="form-control widget-data" name="sm-cols">
                            <option value="6" <?= ($widgetData['data']['view-mode']['carousel']['sm-cols'] == '6' ? 'selected' : ''); ?>>6</option>
                            <option value="5" <?= ($widgetData['data']['view-mode']['carousel']['sm-cols'] == '5' ? 'selected' : ''); ?>>5</option>
                            <option value="4" <?= ($widgetData['data']['view-mode']['carousel']['sm-cols'] == '4' ? 'selected' : ''); ?>>4</option>
                            <option value="3" <?= ($widgetData['data']['view-mode']['carousel']['sm-cols'] == '3' ? 'selected' : ''); ?>>3</option>
                            <option value="2" <?= ($widgetData['data']['view-mode']['carousel']['sm-cols'] == '2' ? 'selected' : ''); ?>>2</option>
                            <option value="1" <?= ($widgetData['data']['view-mode']['carousel']['sm-cols'] == '1' ? 'selected' : ''); ?>>1</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <label>Autoplay</label>
                        <select class="form-control widget-data" name="autoplay">
                            <option value="0" <?= ($widgetData['data']['view-mode']['carousel']['autoplay'] == '0' ? 'selected' : ''); ?>>No</option>
                            <option value="1" <?= ($widgetData['data']['view-mode']['carousel']['autoplay'] == '1' ? 'selected' : ''); ?>>Si</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <label>Mostra dots</label>
                        <select class="form-control widget-data" name="dots">
                            <option value="0" <?= ($widgetData['data']['view-mode']['carousel']['dots'] == '0' ? 'selected' : ''); ?>>No</option>
                            <option value="1" <?= ($widgetData['data']['view-mode']['carousel']['dots'] == '1' ? 'selected' : ''); ?>>Si</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <label>Loop</label>
                        <select class="form-control widget-data" name="loop">
                            <option value="0" <?= ($widgetData['data']['view-mode']['carousel']['loop'] == '0' ? 'selected' : ''); ?>>No</option>
                            <option value="1" <?= ($widgetData['data']['view-mode']['carousel']['loop'] == '1' ? 'selected' : ''); ?>>Si</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('[data-name="view-mode"] .widget-data[name="mode"]').on('change', function () {
            $('[data-name="view-mode"] .widget-group').hide();
            $('[data-name="view-mode"] .widget-group[data-name="' + $(this).val() + '"]').show();
        }).change();
    });
</script>
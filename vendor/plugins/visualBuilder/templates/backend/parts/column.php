<div class="visualBuilderColumn" data-col="[col-size]">
    <div class="visualBuilderColumnBody">
        <div class="visualBuilderDesignPreview columnPreview"></div>
        <div class="visualBuilderColumnActions">
            <a href="#" class="visualBuilderColumnActionBtn" data-action="add"><i class="fa fa-plus"></i></a>
            <a href="#" class="visualBuilderColumnActionBtn" data-action="edit"><i class="fa fa-gear"></i></a>
            <a href="#" class="visualBuilderColumnActionBtn" data-action="delete"><i class="fa fa-trash"></i></a>
        </div>
        <div class="visualBuilderColumnResizeHandler"><i class="fa fa-arrows-h" aria-hidden="true"></i></div>
        <div class="visualBuilderColumnContent">
            [col-content]
        </div>
        <div class="visualBuilderColumnFooter">
            <a href="#" class="visualBuilderAddWidget_Btn"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <script type="application/json" class="col-data" style="display: none;">[col-data]</script>
</div>
<div class="col-lg-3 col-md-4 col-sm-12">
    <a class="addPopupWidget_Btn block_<?= $blockInfo['id']; ?>" data-id="<?= $blockInfo['id']; ?>" data-name="<?= htmlentities($blockInfo['title']); ?>">
        <img class="addPopupWidget_Icon" src="<?= $blockInfo['url']; ?>/icon.png">
        <b><?= $blockInfo['title']; ?></b>
        <small><?= $blockInfo['description']; ?></small>
    </a>
</div>
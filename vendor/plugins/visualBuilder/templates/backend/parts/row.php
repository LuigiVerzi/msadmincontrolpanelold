<div class="visualBuilderRow">
    <div class="visualBuilderRowControls">
        <div class="leftControls">
            <a href="#" class="visualBuilderControlBtn" data-action="move" title="Muovi riga">
                <i class="fa fa-arrows" aria-hidden="true"></i>
            </a>
            <a href="#" class="visualBuilderControlBtn" data-action="size">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <span class="controlTooltip visualBuilderSizesList">
                    <span data-size="12">1/1</span>
                    <span data-size="6;6">1/2 + 1/2</span>
                    <span data-size="3;3;3;3">1/4 + 1/4 + 1/4 + 1/4</span>
                    <span data-size="3;3;6">1/4 + 1/4 + 2/4</span>
                </span>
            </a>
            <a href="#" class="visualBuilderControlBtn" data-action="add" title="Aggiungi colonna">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </a>
        </div>

        <div class="visualBuilderDesignPreview rowPreview"></div>
        <div class="rightControls">
            <a href="#" class="visualBuilderControlBtn" data-action="edit">
                <i class="fa fa-gear" aria-hidden="true"></i>
            </a>
            <a href="#" class="visualBuilderControlBtn" data-action="duplicate">
                <i class="fa fa-files-o" aria-hidden="true"></i>
            </a>
            <a href="#" class="visualBuilderControlBtn" data-action="delete">
                <i class="fa fa-trash" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    <div class="visualBuilderRowContent">
        [row-content]
    </div>

    <a href="#" class="visualBuilderAddNewRow">
        <i class="fa fa-plus"></i>
    </a>
    <script type="application/json" class="row-data" style="display: none;">[row-data]</script>
</div>
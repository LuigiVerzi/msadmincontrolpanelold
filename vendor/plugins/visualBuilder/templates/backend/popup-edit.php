<?php
include('../../../../../sw-config.php');
?>

<div class="modal inmodal visualBuilderBlockEditModal" data-keyboard="false" data-backdrop="static" data-target="" data-id="" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="float: none; margin: 0;"><span class="widgetAction"></span> <span class="widgetName"></span></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary visualBuilderBlockEdit_SaveBtn" data-dismiss="modal">Salva</button>
            </div>
        </div>
    </div>
</div>


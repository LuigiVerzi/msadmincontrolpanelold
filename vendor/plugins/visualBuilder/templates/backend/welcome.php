<div class="welcomeBox" style="display: <?= ($data ? 'none' : 'block'); ?>;">
    <h2>Benvenuto nel Visual Builder</h2>
    <h3>Aggiungi un contenuto per iniziare 🙂</h3>

    <div class="startNewProject">
        <a href="#" class="btn btn-info visualBuilder_addRowBtn">Aggiungi una riga</a>
        <a href="#" class="btn btn-primary visualBuilder_addEditorBtn">Aggiungi editor di testo</a>
        <a href="#" class="btn btn-warning visualBuilder_loadTemplateBtn">Carica un template</a>
    </div>

</div>
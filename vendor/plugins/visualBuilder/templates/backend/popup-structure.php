<div class="alert alert-info">
    La seguente configurazione si riferisce ad un singolo elemento. Essa verrà ripetuta per ogni elemento da mostrare.
    <a href="#" class="pull-right btn btn-sm btn-default" onclick="$('.structureShortcodes').toggle();" style="margin: -5px 0;">Mostra Shortcodes</a>
</div>

<div class="structureShortcodes" style="margin-top: 15px; display: none;">
    <table class="table table-bordered small">
        <tr>
            <th style="background-color: #ececec;" colspan="2">Lista shortcodes Disponibili</th>
        </tr>
        <?php foreach($widgetDetails['shortcodes'] as $shortcode => $descr) { ?>
            <tr>
                <td width="210" style="background-color: #f4f4f4;"><?= $shortcode; ?></td> <td><?= $descr; ?></td>
            </tr>
        <?php } ?>
    </table>
</div>

<textarea class="visualBlockStructure widget-data" data-shortcodes="<?= htmlentities(json_encode($widgetDetails['shortcodes'])); ?>" data-use-relative-page="<?= htmlentities($widgetDetails['use_relative_page']); ?>" name="data" data-type="<?= $widgetDetails['id']; ?>"><?= $widgetData['structure']['data']; ?></textarea>
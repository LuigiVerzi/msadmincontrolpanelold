<?php

$offerClass = new \MSFramework\BeautyCenter\offers();
$structureHtml = $this->drawVisualContent(json_decode($widgetData['structure']['data'], true), true);

$itemClasses = array();
foreach($widgetData['data']['classes'] as $classes) {
    if(!empty($classes)) $itemClasses[] = $classes;
}

echo '<div class="ms-row">';
foreach($offerClass->getOfferDetails('', ($widgetData['data']['show-settings']['expired'] == "1" ? false : true)) as $offerDetail) {

    if($widgetData['data']['show-settings']['type'] && $widgetData['data']['show-settings']['type'] !== "") {
        if($offerDetail['type'] !== $widgetData['data']['show-settings']['type']) continue;
    }

    echo '<div class="' . implode(' ', $itemClasses) . '">';

    echo strtr($structureHtml, array(
        '{offer-title}' => $MSFrameworki18n->getFieldValue($offerDetail['nome']),
        '{offer-description}' => $MSFrameworki18n->getFieldValue($offerDetail['short_content']),
        '{offer-details}' => $MSFrameworki18n->getFieldValue($offerDetail['content']),
        '{offer-price}' => number_format($offerDetail['prezzo'], 2, ',', '.'),
        '{offer-expiration}' => ($offerDetail['scadenza'] ? date($offerDetail['scadenza']) : ''),
        '{offer-image}' => $offerDetail['gallery_friendly'][0]['html']['main'],
        '{relative-page-url}' => $offerClass->getURL($offerDetail),

        '{rand}' => uniqid() // Serve per far funzionare l'apertura delle pagine dentro i popup
    ));

    echo '</div>';

}
echo '</div>';
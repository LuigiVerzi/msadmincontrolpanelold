<?php
if($widgetData['data']['show-settings']['type'] === 'sconto') $show_type_descr = 'tutti gli sconti';
if($widgetData['data']['show-settings']['type'] === 'pacchetti') $show_type_descr = 'tutti i pacchetti';
if($widgetData['data']['show-settings']['type'] === 'convenzione') $show_type_descr = 'tutte le convenzioni';
else $show_type_descr = 'tutte le offerte';

?>

<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <b class="visualWidget-title">Offerte - Centro estetico</b>
    <small class="visualWidget-descr">Mostra <u><?= $show_type_descr; ?></u> disponibili</small>
</div>
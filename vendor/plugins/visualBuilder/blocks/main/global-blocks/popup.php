<div class="row">

    <div class="col-lg-12">
        <label>Seleziona blocco</label>
        <select class="form-control widget-data" name="block" id="block_id">
            <option value="">Seleziona blocco</option>
            <?php foreach((new \MSFramework\blocks())->getBlockDetails() as $block) { ?>
                <option value="<?= $block['id']; ?>" <?= ($widgetData['data']['block'] == $block['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($block['nome']); ?></option>
            <?php } ?>
        </select>

        <div class="text-right">
            <a href="#" class="btn btn-sm btn-warning btn-outline" id="editCurrentBlock" style="display: <?= ($widgetData['data']['block'] ? 'inline-block' : 'none'); ?>;">Modifica</a>
            <a href="#" class="btn btn-sm btn-default" id="creatNewBlock">Crea nuovo</a>
        </div>
    </div>

</div>

<script>
    visualBuilderPopupJS.push(function () {

        $('#creatNewBlock').on('click', function () {
            showFastEditor('contenuti_blocchi', '', function (result) {
                if(result.id > 0) {
                    $('#block_id').append('<option value="' + result.id + '">Blocco creato alle ore ' + new Date().toLocaleTimeString('it-IT', {timeStyle: 'short'}) + '</a>').val(result.id).change();
                    $('#fastModuleEditor').modal('toggle');
                    toastr['success']('Block creato correttamente');
                }
            });
        });

        $('#block_id').on('change', function (e) {
            if($(this).val() !== "") {
                $('#editCurrentBlock').css('display', 'inline-block');
            } else {
                $('#editCurrentBlock').hide();
            }
        });

        $('#editCurrentBlock').on('click', function () {
            showFastEditor('contenuti_blocchi', $('#block_id').val(), function (result) {
                $('#fastModuleEditor').modal('toggle');
                toastr['success']('Blocco aggiornato correttamente');
            });
        });
        
    });
</script>
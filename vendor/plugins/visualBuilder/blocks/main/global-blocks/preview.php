<?php
$globalBlockDetails = (new \MSFramework\blocks())->getBlockDetails($widgetData['data']['block']);
if($globalBlockDetails) $globalBlockDetails = $globalBlockDetails[$widgetData['data']['block']];
?>

<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <?php if($globalBlockDetails) { ?>
    <b class="visualWidget-title">Blocco <?= $globalBlockDetails['nome']; ?></b>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Blocco mancante!</b>
        <small class="visualWidget-descr text-danger">Il blocco ID <?= $widgetData['data']['block']; ?> non è stato trovato, probabilmente è stato eliminato.</small>
    <?php } ?>
</div>
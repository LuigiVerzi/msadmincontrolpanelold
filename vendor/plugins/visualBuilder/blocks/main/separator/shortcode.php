<?php
$hrStyles = array();
$hrClasses = array();

if(!empty($widgetData['data']['color'])) $hrClasses[] = htmlentities($widgetData['data']['color']);

// Stili personalizzati
$hrStyles[] = 'padding: 0px';
$hrStyles[] = 'border-width: 0px';

if($widgetData['data']['align'] === 'left') {
    $hrStyles[] = 'margin: 0';
} else if($widgetData['data']['align'] === 'right') {
    $hrStyles[] = 'margin: 0 0 0 auto';
} else {
    $hrStyles[] = 'margin: 0 auto';
}

if(!empty($widgetData['data']['width'])) $hrStyles[] = htmlentities('width: ' . $widgetData['data']['width']);
if(!empty($widgetData['data']['height'])) $hrStyles[] = htmlentities('border-top-width: ' . $widgetData['data']['height'] . 'px');
if(!empty($widgetData['data']['border-style'])) $hrStyles[] = htmlentities('border-top-style: ' . $widgetData['data']['border-style']);

if(in_array('ms-border-custom', $hrClasses)) {
    $hrStyles[] = htmlentities('border-color: ' . $widgetData['data']['custom-color']);
}

if(!empty($widgetData['data']['title'])) {
    $hrStyles[] = 'flex: 0 ' . $widgetData['data']['width'];
    $hrStyles[] = 'margin: 0';
}

$divisor_html = '<hr class="ms-divider '. implode(" ", $hrClasses) . '" style="' . implode(";", $hrStyles) . '"/>';

if(empty($widgetData['data']['title'])) {
    echo $divisor_html;
} else {
    echo '<div class="ms-divider-with-title ms-divider-' . $widgetData['data']['title-position'] . '">';

    if($widgetData['data']['title-position'] === 'left') {
        echo '<span>' . htmlentities($widgetData['data']['title']) . '</span>' . $divisor_html;
    } else  if($widgetData['data']['title-position'] === 'center') {
        echo $divisor_html . '<span>' . htmlentities($widgetData['data']['title']) . '</span>' . $divisor_html;
    } else {
        echo $divisor_html . '<span>' . htmlentities($widgetData['data']['title']) . '</span>';
    }

    echo '</div>';
}
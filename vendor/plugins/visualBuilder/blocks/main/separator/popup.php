<div class="row">
    <div class="col-lg-3">
        <label>Colore</label>
        <select class="form-control widget-data" name="color">
            <optgroup label="Stili predefiniti">
            <option value="">Default</option>
            <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                <option class="ms-bg-<?= $btn_style; ?>" value="ms-border-<?= $btn_style; ?>" <?= ($widgetData['data']['color'] == 'ms-border-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
            <?php } ?>
            </optgroup>
            <optgroup label="Personalizzato">
                <option value="ms-border-custom" <?= ($widgetData['data']['color'] == 'ms-border-custom' ? 'selected' : ''); ?>>Personalizzato</option>
            </optgroup>
        </select>
    </div>

    <div class="col-lg-3 btn-colors-settings" style="display: <?= ($widgetData['data']['color'] == 'ms-border-custom' ? 'block' : 'none'); ?>;">
        <div class="row">
            <div class="col-lg-6">
                <label>Colore</label>
                <div class="input-group colorpicker-component">
                    <input class="form-control widget-data" name="custom-color" type="text" value="<?= $widgetData['data']['custom-color']; ?>">
                    <span class="input-group-addon"><i></i></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <label>Stile bordo</label>
        <select class="form-control widget-data" name="border-style">
            <option class="" value="">Predefinito</option>
            <option class="dashed" value="dashed" <?= ($widgetData['data']['border-style'] == 'dashed' ? 'selected' : ''); ?>>Dashed</option>
            <option class="dotted" value="dotted" <?= ($widgetData['data']['border-style'] == 'dotted' ? 'selected' : ''); ?>>Dotted</option>
            <option class="double" value="double" <?= ($widgetData['data']['border-style'] == 'double' ? 'selected' : ''); ?>>Double</option>
            <option class="shadow" value="shadow" <?= ($widgetData['data']['border-style'] == 'shadow' ? 'selected' : ''); ?>>Shadow</option>
        </select>
    </div>
    <div class="col-lg-3">
        <label>Altezza</label>
        <select class="form-control widget-data" name="height">
            <?php for($i = 1; $i <= 10; $i++) { ?>
                <option value="<?= $i; ?>" <?= ($widgetData['data']['height'] === (string)$i ? 'selected' : ''); ?>><?= $i; ?>px</option>
            <?php } ?>
        </select>
    </div>

    <div class="col-lg-3">
        <label>Larghezza</label>
        <input type="text" class="form-control widget-data size-input" placeholder="Es: 100px o 100%" name="width" value="<?= htmlentities($widgetData['data']['width']); ?>">
    </div>

    <div class="col-lg-3">
        <label>Allineamento</label>
        <select class="form-control widget-data" name="align">
            <?php foreach(array('center' => 'Centro', 'right' => 'Destra', 'left' => 'Sinistra') as $posVal => $posName) { ?>
                <option value="<?= $posVal; ?>" <?= ($widgetData['data']['align'] == $posVal ? 'selected' : ''); ?>><?= $posName; ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label>Titolo divisore</label>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                    <input type="checkbox" id="use_title_divider" <?php if(!empty(htmlentities($widgetData['data']['title']))) { echo "checked"; } ?>>
                    <i></i>
                </div>
                <span style="font-weight: normal;">Mostra titolo</span>
            </label>
        </div>
    </div>
    <div class="col-lg-4 use_divider_title">
        <label>Titolo divisore</label>
        <input type="text" class="form-control widget-data" placeholder="" name="title" value="<?= htmlentities($widgetData['data']['title']); ?>">
    </div>
    <div class="col-lg-4 use_divider_title">
        <label>Posizione titolo</label>
        <select class="form-control widget-data" name="title-position">
            <optgroup label="Stili predefiniti">
                <?php foreach(array('left' => 'Sinistra', 'center' => 'Centro', 'right' => 'Destra') as $posVal => $posName) { ?>
                    <option value="<?= $posVal; ?>" <?= ($widgetData['data']['title-position'] == $posVal ? 'selected' : ''); ?>><?= $posName; ?></option>
                <?php } ?>
            </optgroup>
        </select>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {

        $('[name="color"]').on('change', function () {
           if($(this).val() === 'ms-border-custom') {
                $('.btn-colors-settings').show();
           } else {
                $('.btn-colors-settings').hide().find('widget-data').val('');
           }
        });

        $('#use_title_divider').on('ifToggled', function () {

            if($(this).is(':checked')) {
                $('.use_divider_title').show();
            } else {
                $('.use_divider_title').hide();
                $('.use_divider_title .widget-data').val('');
            }

        }).trigger('ifToggled');

        initMSShortcodeBtns();
        
    });
</script>
<div class="row">
    <div class="col-lg-12">
        <label>Testo pulsante</label>
        <input type="text" class="form-control widget-data" name="text" value="<?= htmlentities($widgetData['data']['text']); ?>">
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label>Colore pulsante</label>
        <select class="form-control widget-data" name="color">
            <optgroup label="Stili predefiniti">
            <option value="">Default</option>
            <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                <option class="ms-bg-<?= $btn_style; ?>" value="ms-btn-<?= $btn_style; ?>" <?= ($widgetData['data']['color'] == 'ms-btn-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
            <?php } ?>
            </optgroup>
            <optgroup label="Personalizzato">
                <option value="ms-btn-custom" <?= ($widgetData['data']['color'] == 'ms-btn-custom' ? 'selected' : ''); ?>>Personalizzato</option>
            </optgroup>
        </select>
    </div>

    <div class="col-lg-8 btn-colors-settings widget-group" data-name="custom-style" style="display: <?= ($widgetData['data']['color'] == 'ms-btn-custom' ? 'block' : 'none'); ?>;">
        <div class="row">
            <div class="col-lg-6">
                <label>Colore testo</label>
                <div class="input-group colorpicker-component">
                    <input class="form-control widget-data" name="text-color" type="text" value="<?= ($widgetData['data']['custom-style']['text-color']); ?>">
                    <span class="input-group-addon"><i></i></span>
                </div>
            </div>
            <div class="col-lg-6">
                <label>Colore sfondo</label>
                <div class="input-group colorpicker-component">
                    <input class="form-control widget-data" name="bg-color" type="text" value="<?= ($widgetData['data']['custom-style']['bg-color']); ?>">
                    <span class="input-group-addon"><i></i></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <label>Angoli arrotondati</label>
        <select class="form-control widget-data" name="border-radius">
            <option value="">Predefinito</option>
            <?php for($i = 0; $i <= 25; $i++) { ?>
                <option value="<?= $i; ?>" <?= ($widgetData['data']['border-radius'] === (string)$i ? 'selected' : ''); ?>><?= $i; ?>px</option>
            <?php } ?>
        </select>
    </div>
    <div class="col-lg-4">
        <label>Classe pulsante</label>
        <input type="text" class="form-control widget-data" name="class" value="<?= htmlentities($widgetData['data']['class']); ?>">
    </div>
</div>


<script>
    visualBuilderPopupJS.push(function () {

        $('[name="color"]').on('change', function () {
           if($(this).val() === 'ms-btn-custom') {
                $('.btn-colors-settings').show();
           } else {
                $('.btn-colors-settings').hide().find('widget-data').val('');
           }
        });

        initMSShortcodeBtns();
        
    });
</script>
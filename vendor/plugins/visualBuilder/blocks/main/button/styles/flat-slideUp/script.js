$(document).ready(function () {

    $('.ms-block-theme-flat-slideUp [class*="ms-btn"]').each(function () {
        var text = $(this).text();
        $(this).html('<span data-hover="' + text + '">' + text + '</span>');
    });

});
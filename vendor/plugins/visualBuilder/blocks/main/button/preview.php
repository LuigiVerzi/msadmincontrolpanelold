<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <?php if(!empty($widgetData['data']['text'])) { ?>
    <b class="visualWidget-title">Pulsante</b>
    <small class="visualWidget-descr">Testo: <u><?= htmlentities($widgetData['data']['text']); ?></u></small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Pulsante!</b>
        <small class="visualWidget-descr text-danger">Testo pulsante mancante.</small>
    <?php } ?>
</div>
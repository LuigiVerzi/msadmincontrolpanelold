<?php
$btnStyles = array();
$btnClasses = array();

$btnParams = array();

if($widgetData['data']['onclick']) {
    $btnParams = $this->getOnClickParams($widgetData['data']['onclick']);
} else if($blockDetails['onclick']) {
    $btnParams = $this->getOnClickParams(array('event' => 'custom', 'action' => $blockDetails['onclick']));
}

if($btnParams) {

    if((string)$widgetData['data']['border-radius'] !== "") $btnStyles[] = 'border-radius: ' . $widgetData['data']['border-radius'] . 'px';

    if(!empty($widgetData['data']['color'])) $btnClasses[] = htmlentities($widgetData['data']['color']);
    if(!empty($widgetData['data']['class'])) $btnClasses[] = htmlentities($widgetData['data']['class']);

    // Stili personalizzati
    if(in_array('ms-btn-custom', $btnClasses)) {
        $btnStyles[] = htmlentities('color: ' . $widgetData['data']['custom-style']['text-color']);
        $btnStyles[] = htmlentities('background: ' . $widgetData['data']['custom-style']['bg-color']);
    }

    $page_id = '';

    if($widgetData['data']['onclick']['event'] == 'open_popup') {

        $page_id = $widgetData['data']['onclick']['open_popup']['id'];
        $page_content = '<ms-page-content id="' . str_replace('page_', '', $page_id) . '"/>';

        if($page_id === 'relative') {
            $page_id = 'page_{rand}';
            $page_content = '<ms-page-content url="{relative-page-url}"/>';
        }

        if (stristr($page_id, 'page')) {

            $popupStyles = array();
            if(!empty($widgetData['data']['onclick']['open_popup']['appearance']['max-width'])) {
                $popupStyles[] = 'max-width: ' . $widgetData['data']['onclick']['open_popup']['appearance']['max-width'] . 'px';
            }
            $popupStyles[] = 'padding: 30px;';

            $popupClasses = array();
            if(!empty($widgetData['data']['onclick']['open_popup']['appearance']['overlay'])) {
                $popupClasses[] = $widgetData['data']['onclick']['open_popup']['appearance']['overlay'] . 'Overlay';
            }

            $widgetData['data']['onclick']['open_popup']['id'] = 'popup_' . $page_id;
            echo (new \MSFramework\popup())->drawPopupHtml($widgetData['data']['onclick']['open_popup']['id'], $page_content, $popupClasses, $popupStyles, $widgetData['data']['onclick']['open_popup']['appearance']);
        }

    }

    ?>

    <a <?= $btnParams; ?> class="ms-btn <?= implode(' ', $btnClasses); ?>" style="<?= implode(';', $btnStyles); ?>"><?= htmlentities($widgetData['data']['text']); ?></a>

<?php } ?>
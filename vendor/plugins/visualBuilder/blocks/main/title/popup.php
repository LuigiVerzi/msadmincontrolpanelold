<div class="row">
    <div class="col-lg-12">
        <label>Testo</label>
        <input class="form-control widget-data" name="text" type="text" value="<?= ($widgetData['data']['text']); ?>">
    </div>

    <div class="col-lg-6">
        <label>Tag elemento</label>
        <select class="form-control widget-data" name="tag">
            <?php foreach(array('h1', 'h2', 'h3', 'h4', 'h5', 'p', 'b') as $tag) { ?>
                <option value="<?= $tag; ?>" <?= ($widgetData['data']['tag'] == $tag ? 'selected' : ''); ?>><?= $tag; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-lg-6">
        <label>Posizione elemento</label>
        <select class="form-control widget-data" name="align">
            <?php foreach(array('left' => 'Sinistra', 'center' => 'Centro', 'right' => 'Destra') as $posVal => $posName) { ?>
                <option value="<?= $posVal; ?>" <?= ($widgetData['data']['align'] == $posVal ? 'selected' : ''); ?>><?= $posName; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-lg-6">
        <label>Colore testo</label>
        <select class="form-control widget-data" name="color">
            <optgroup label="Stili predefiniti">
                <option value="">Usa colori tema</option>
                <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                    <option class="ms-bg-<?= $btn_style; ?>" value="ms-text-<?= $btn_style; ?>" <?= ($widgetData['data']['color'] == 'ms-text-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
                <?php } ?>
            </optgroup>
            <optgroup label="Personalizzato">
                <option value="ms-text-custom" <?= ($widgetData['data']['color'] == 'ms-text-custom' ? 'selected' : ''); ?>>Personalizzato</option>
            </optgroup>
        </select>
    </div>

    <div class="col-lg-6 btn-colors-settings" style="display: <?= ($widgetData['data']['color'] == 'ms-text-custom' ? 'block' : 'none'); ?>;">
        <label>Colore personalizzato</label>
        <div class="input-group colorpicker-component">
            <input class="form-control widget-data" name="custom-color" type="text" value="<?= ($widgetData['data']['custom-color']); ?>">
            <span class="input-group-addon"><i></i></span>
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('[name="color"]').on('change', function () {
            if($(this).val() === 'ms-text-custom') {
                $('.btn-colors-settings').show();
            } else {
                $('.btn-colors-settings').hide().find('widget-data').val('');
            }
        });
    });
</script>
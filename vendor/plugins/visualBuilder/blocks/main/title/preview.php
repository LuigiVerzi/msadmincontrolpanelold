<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon' . ($blockDetails['is_html'] ? '_html' : '') . '.png'; ?>">
    <?php if(!empty($widgetData['data']['text'])) { ?>
        <b class="visualWidget-title"><?= $widgetData['data']['text']; ?></b>
        <small class="visualWidget-descr"><?= (!$blockDetails['is_html'] ? 'Tag: <u>' . $widgetData['data']['tag'] . '</u>, ' : ''); ?>Allineamento: <u><?= $widgetData['data']['align']; ?></u></small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Titolo mancante!</b>
        <small class="visualWidget-descr text-danger">Testo del titolo mancante.</small>
    <?php } ?>
</div>
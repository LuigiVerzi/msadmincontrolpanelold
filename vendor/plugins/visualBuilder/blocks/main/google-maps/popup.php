<div class="row">
    <div class="col-lg-10 col-sm-9">
        <label>Posizione</label>
        <input name="location" id="maps_location" class="widget-data form-control" value="<?= htmlentities($widgetData['data']['location']); ?>">
        <input name="lat" type="hidden" id="maps_lat" class="widget-data" value="<?= htmlentities($widgetData['data']['lat']); ?>">
        <input name="lng" type="hidden" id="maps_lng" class="widget-data" value="<?= htmlentities($widgetData['data']['lng']); ?>">
    </div>
    <div class="col-lg-2 col-sm-3">
        <label>Zoom</label>
        <select name="zoom" class="widget-data form-control">
            <?php for($i = 19; $i >= 1; $i--) { ?>
                <option value="<?= $i; ?>" <?= ($widgetData['data']['zoom'] == $i ? 'selected' : ''); ?>><?= $i; ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-lg-10 col-sm-9">
        <label>Altezza minima</label>
        <input name="min-height" class="widget-data form-control" value="<?= htmlentities($widgetData['data']['min-height']); ?>">
    </div>
    <div class="col-lg-2 col-sm-3">
        <label></label>
        <select name="min-height-prefix" class="widget-data form-control">
            <option value="px">px</option>
            <option value="%" <?= ($widgetData['data']['min-height-prefix'] === '%' ? 'selected' : ''); ?>>%</option>
        </select>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {

        var autocomplete = new google.maps.places.Autocomplete((document.getElementById('maps_location')), {
                types: ['geocode'],
            }
        );

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            $("#maps_lat").val(place.geometry.location.lat());
            $("#maps_lng").val(place.geometry.location.lng());
        });

    });
</script>
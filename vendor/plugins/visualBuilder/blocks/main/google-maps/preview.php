<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <?php if(!empty($widgetData['data']['location'])) { ?>
        <b class="visualWidget-title">Mappa località</b>
        <small class="visualWidget-descr"><?= htmlentities($widgetData['data']['location']); ?></small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Località errata!</b>
        <small class="visualWidget-descr text-danger">Non hai incluso nessuna località valida.</small>
    <?php } ?>
</div>

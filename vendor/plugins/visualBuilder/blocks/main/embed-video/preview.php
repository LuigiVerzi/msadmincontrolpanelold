<?php
include('videoInfo.php');
?>
<div class="visualWidget-intro">
    <?php if($videoInfo) { ?>
        <img class="visualWidget-icon" src="<?=  $videoInfo['small-thumb']; ?>">
        <b class="visualWidget-title">Video <?= ($widgetData['data']['view-mode'] === 'embed' ? 'Incorporato' : 'Popup'); ?></b>
        <small class="visualWidget-descr">Da <u><?= $embedSource; ?></u>, in <u><?= $widgetData['data']['ratio']; ?></u></small>
    <?php } else { ?>
        <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
        <b class="visualWidget-title text-danger">Video mancante!</b>
        <small class="visualWidget-descr text-danger">Il link del video inserito non è corretto.</small>
    <?php } ?>
</div>
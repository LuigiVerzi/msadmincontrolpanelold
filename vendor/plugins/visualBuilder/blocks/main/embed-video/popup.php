<div class="row">
    <div class="col-lg-6">
        <label>Link al video</label>
        <input id="embedVideoUrl" type="text" class="form-control widget-data" placeholder="URL del video da incorporare" name="url" value="<?= htmlentities($widgetData['data']['url']); ?>">
        <span class="text-muted" style="display: block; margin: -10px 0 15px;">Accettati: <u>YouTube</u>, <u>Vimeo</u></span>
    </div>

    <div class="col-lg-3">
        <label>Visualizzazione</label>
        <select class="form-control widget-data" name="view-mode">
            <option value="embed">Incorporata</option>
            <!-- <option value="modal" <?= ($widgetData['data']['view-mode'] == 'modal' ? 'selected' : ''); ?>>Popup</option> -->
        </select>
    </div>
    <div class="col-lg-3">
        <label>Proporzioni</label>
        <select class="form-control widget-data" name="ratio">
            <option value="16:9">16:9</option>
            <option value="4:3" <?= ($widgetData['data']['ratio'] == '4:3' ? 'selected' : ''); ?>>4:3</option>
            <option value="2.35:1" <?= ($widgetData['data']['ratio'] == '2.35:1' ? 'selected' : ''); ?>>2.35:1</option>
        </select>
    </div>
</div>

<div class="widget-group videoSettingsBox" data-name="YouTube" style="display: none;">
    <div class="row">
        <div class="col-lg-3">
            <label>Autoplay</label>
            <select class="form-control widget-data" name="autoplay">
                <option value="0">No</option>
                <option value="1" <?= ($widgetData['data']['YouTube']['autoplay'] == '1' ? 'selected' : ''); ?>>Si</option>
            </select>
        </div>
        <div class="col-lg-3">
            <label>Controlli</label>
            <select class="form-control widget-data" name="controls">
                <option value="1">Mostra</option>
                <option value="0" <?= ($widgetData['data']['YouTube']['controls'] == '0' ? 'selected' : ''); ?>>Nascondi</option>
            </select>
        </div>
        <div class="col-lg-3">
            <label>Inizio video</label>
            <input class="form-control widget-data" name="start" type="time" min="0:00:00" step="1" value="<?= ($widgetData['data']['YouTube']['start'] ? $widgetData['data']['YouTube']['start'] : '00:00:00'); ?>">
        </div>
    </div>
</div>

<div class="widget-group videoSettingsBox" data-name="Vimeo" style="display: none;">
    <div class="row">
        <div class="col-lg-3">
            <label>Autoplay</label>
            <select class="form-control widget-data" name="autoplay">
                <option value="0">No</option>
                <option value="1" <?= ($widgetData['data']['Vimeo']['autoplay'] == '1' ? 'selected' : ''); ?>>Si</option>
            </select>
        </div>
        <div class="col-lg-3">
            <label>Inizio video</label>
            <input class="form-control widget-data" name="t" type="time" min="0:00:00" step="1" value="<?= ($widgetData['data']['Vimeo']['t'] ? $widgetData['data']['Vimeo']['t'] : '00:00:00'); ?>">
        </div>
        <div class="col-lg-2">
            <label>Audio</label>
            <select class="form-control widget-data" name="mute">
                <option value="0">Si</option>
                <option value="1" <?= ($widgetData['data']['Vimeo']['mute'] == '1' ? 'selected' : ''); ?>>No</option>
            </select>
        </div>
        <div class="col-lg-2">
            <label>Titolo video</label>
            <select class="form-control widget-data" name="title">
                <option value="1">Mostra</option>
                <option value="0" <?= ($widgetData['data']['Vimeo']['title'] == '0' ? 'selected' : ''); ?>>Nascondi</option>
            </select>
        </div>
        <div class="col-lg-2">
            <label>Sfondo</label>
            <select class="form-control widget-data" name="transparent">
                <option value="0">Nero</option>
                <option value="1" <?= ($widgetData['data']['Vimeo']['transparent'] == '1' ? 'selected' : ''); ?>>Bianco</option>
            </select>
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {

        var ShowVideoSettings = function () {
            var videoUrl = $(this).val();

            $('.videoSettingsBox').hide();
            if(videoUrl.indexOf('youtu') >= 0) {
                $('.videoSettingsBox[data-name="YouTube"]').show();
            } else if(videoUrl.indexOf('vimeo')  >= 0) {
                $('.videoSettingsBox[data-name="Vimeo"]').show();
            }

        };

        $('#embedVideoUrl').on('change', ShowVideoSettings).change();
    });
</script>
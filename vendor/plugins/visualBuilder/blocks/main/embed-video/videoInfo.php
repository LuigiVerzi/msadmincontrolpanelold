<?php
$embedVideo = $widgetData['data']['url'];
$embedSource = '';
$videoInfo = array();
if(stristr($embedVideo, 'youtu')) {
    $embedSource = 'YouTube';
} else if(stristr($embedVideo, 'vimeo')) {
    $embedSource = 'Vimeo';
}

if($embedSource === 'YouTube' && preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $embedVideo, $matches)) {


    $urlParams = $widgetData['data']['YouTube'];
    list($hours, $minutes, $seconds) = explode(':', $urlParams['start'], 3);
    $urlParams['start'] = $seconds + ($minutes * 60) + ($hours * 3600);

    $videoInfo['id'] = $matches[0];
    $videoInfo['embed_url'] = 'https://www.youtube.com/embed/' . $matches[0] . '?autoplay=0&' . http_build_query($urlParams);
    $videoInfo['small-thumb'] = 'https://img.youtube.com/vi/' . $matches[0] . '/sddefault.jpg';
    $videoInfo['large-thumb'] = 'https://img.youtube.com/vi/' . $matches[0] . '/maxresdefault.jpg';
}else if($embedSource === 'Vimeo' && preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $embedVideo, $matches)) {

    $urlParams = $widgetData['data']['Vimeo'];
    list($hours, $minutes, $seconds) = explode(':', $urlParams['t'], 3);
    $time = $hours . 'h' . $minutes . 'm' . $seconds . 's';
    unset($urlParams['t']);

    $videoInfo['id'] = $matches[5];
    $videoInfo['embed_url'] = 'https://player.vimeo.com/video/' . $matches[5] . '?autopause=1&' . http_build_query($urlParams) . '#t=' . $time;
    $videoInfo['small-thumb'] = json_decode(file_get_contents("http://vimeo.com/api/v2/video/" . $matches[5] . ".json"), true)[0]['thumbnail_small'];
    $videoInfo['large-thumb'] = json_decode(file_get_contents("http://vimeo.com/api/v2/video/" . $matches[5] . ".json"), true)[0]['thumbnail_large'];
}
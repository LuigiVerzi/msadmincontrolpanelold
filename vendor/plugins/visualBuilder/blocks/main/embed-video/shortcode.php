<?php
include('videoInfo.php');
if($videoInfo) {
    echo '<div class="ms-embed-video" data-align="' . $widgetData['data']['position'] . '" data-proportion="' . $widgetData['data']['ratio'] . '"><iframe frameborder="0" scrolling="no" marginheight="0" src="' . htmlentities($videoInfo['embed_url']) . '" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
} else {
    echo '<div class="parserShortcodeError">Non è stato inserito un URL di un video valido.</div>';
}


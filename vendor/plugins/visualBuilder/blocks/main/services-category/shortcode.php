<?php

$structureHtml = $this->drawVisualContent(json_decode($widgetData['structure']['data'], true), true);

$itemClasses = array();
foreach($widgetData['data']['classes'] as $classes) {
    if(!empty($classes)) $itemClasses[] = $classes;
}

echo '<div class="ms-row">';
foreach((new \MSFramework\services())->getCategoryDetails() as $serviceCategory) {

    echo '<div class="' . implode(' ', $itemClasses) . '">';

    $page_relation_url = '';
    if(stristr($structureHtml, '{service-category-page-url}') || stristr($structureHtml, '{relative-page-url}')) {
        $page_relation_url = '';

        $page_relation_id = (new \MSFramework\pages())->getPageIDsByRelation('service_category', false, $serviceCategory['id']);
        if($page_relation_id) {
            $page_relation_url =  (new \MSFramework\pages())->getURL(end($page_relation_id));
        }

    }

    echo strtr($structureHtml, array(
            '{service-category-title}' => $MSFrameworki18n->getFieldValue($serviceCategory['nome']),
            '{service-category-description}' => $MSFrameworki18n->getFieldValue($serviceCategory['descr']),
            '{service-category-details}' => $MSFrameworki18n->getFieldValue($serviceCategory['long_descr']),
            '{service-category-image}' => $serviceCategory['gallery_friendly'][0]['html']['main'],
            '{service-category-page-url}' => $page_relation_url,
            '{relative-page-url}' => $page_relation_url,

            '{rand}' => uniqid() // Serve per far funzionare l'apertura delle pagine dentro i popup
    ));

    echo '</div>';

}
echo '</div>';
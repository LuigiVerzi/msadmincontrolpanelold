<div class="row">
    <div class="col-lg-12">
        <div class="widget-group" data-name="classes">
            <div class="row">
                <div class="col-lg-4">
                    <label>Colonne per riga</label>
                    <select class="form-control widget-data" name="cols">
                        <option value="ms-col-12" <?= ($widgetData['data']['classes']['cols'] == 'ms-col-12' ? 'selected' : ''); ?>>1</option>
                        <option value="ms-col-3" <?= ($widgetData['data']['classes']['cols'] == 'ms-col-3' ? 'selected' : ''); ?>>4</option>
                        <option value="ms-col-4" <?= ($widgetData['data']['classes']['cols'] == 'ms-col-4' ? 'selected' : ''); ?>>3</option>
                        <option value="ms-col-6" <?= ($widgetData['data']['classes']['cols'] == 'ms-col-6' ? 'selected' : ''); ?>>2</option>
                    </select>
                </div>
                <div class="col-lg-4">
                    <label>Colonne (Tablet)</label>
                    <select class="form-control widget-data" name="md-cols">
                        <option value="">Eredita da Desktop</option>
                        <option value="ms-col-md-12" <?= ($widgetData['data']['classes']['md-cols'] == 'ms-col-md-12' ? 'selected' : ''); ?>>1</option>
                        <option value="ms-col-md-3" <?= ($widgetData['data']['classes']['md-cols'] == 'ms-col-md-3' ? 'selected' : ''); ?>>4</option>
                        <option value="ms-col-md-4" <?= ($widgetData['data']['classes']['md-cols'] == 'ms-col-md-4' ? 'selected' : ''); ?>>3</option>
                        <option value="ms-col-md-6" <?= ($widgetData['data']['classes']['md-cols'] == 'ms-col-md-6' ? 'selected' : ''); ?>>2</option>
                    </select>
                </div>
                <div class="col-lg-4">
                    <label>Colonne (Mobile)</label>
                    <select class="form-control widget-data" name="sm-cols">
                        <option value="ms-col-sm-12" <?= ($widgetData['data']['classes']['sm-cols'] == 'ms-col-sm-12' ? 'selected' : ''); ?>>1</option>
                        <option value="ms-col-sm-3" <?= ($widgetData['data']['classes']['sm-cols'] == 'ms-col-sm-3' ? 'selected' : ''); ?>>4</option>
                        <option value="ms-col-sm-4" <?= ($widgetData['data']['classes']['sm-cols'] == 'ms-col-sm-4' ? 'selected' : ''); ?>>3</option>
                        <option value="ms-col-sm-6" <?= ($widgetData['data']['classes']['sm-cols'] == 'ms-col-sm-6' ? 'selected' : ''); ?>>2</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
    });
</script>
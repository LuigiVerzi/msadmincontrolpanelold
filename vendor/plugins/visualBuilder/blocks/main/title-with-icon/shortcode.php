<?php
$iconStyle = array();
$iconClass = array();

$iconStyle[] = 'font-size: ' . $widgetData['data']['icon']['px'] . 'px';

if($widgetData['data']['icon']['color'] === 'ms-text-custom') {
    $iconStyle[] = 'color: ' . $widgetData['data']['icon']['custom-color'];
    $iconClass[] = 'ms-text-custom';
} else if($widgetData['data']['icon']['color'] !== '') {
    $iconClass[] = $widgetData['data']['icon']['color'];
}

$titleStyle = array();
$titleClass = array();

if($widgetData['data']['title']['color'] === 'ms-text-custom') {
    $titleStyle[] = 'color: ' . $widgetData['data']['title']['custom-color'];
    $titleClass[] = 'ms-text-custom';
} else if($widgetData['data']['title']['color'] !== '') {
    $titleClass[] = $widgetData['data']['title']['color'];
}

$subtitleStyle = array();
$subtitleClass = array();

if($widgetData['data']['subtitle']['color'] === 'ms-text-custom') {
    $subtitleStyle[] = 'color: ' . $widgetData['data']['subtitle']['custom-color'];
    $subtitleClass[] = 'ms-text-custom';
} else if($widgetData['data']['subtitle']['color'] !== '') {
    $subtitleClass[] = $widgetData['data']['subtitle']['color'];
}

/* HTML ICONA */
$iconHTML = '<i style="' . implode(';', $iconStyle) . '" class="' . $widgetData['data']['icon']['value'] . ' ' . implode(' ', $iconClass) . ' ms-title-icon"></i>';

/* HTML TITOLO */
$titleHTML = '<' . $widgetData['data']['title']['tag'] . ' style="' . implode(';', $titleStyle) . '" class="' . implode(' ', $titleClass) . '">';
$titleHTML .= $widgetData['data']['title']['text'];
$titleHTML .= '</' . $widgetData['data']['title']['tag'] . '>';

/* HTML SOTTOTITOLO */
$subtitleHTML = '<div style="' . implode(';', $subtitleStyle) . '" class="' . implode(' ', $subtitleClass) . '">';
$subtitleHTML .= $widgetData['data']['subtitle']['text'];
$subtitleHTML .= '</div>';

$blockHTML = $titleHTML . $subtitleHTML;

if($widgetData['data']['icon']['align'] === 'left' || $widgetData['data']['icon']['align'] === 'top') {
    $blockHTML = $iconHTML . $blockHTML;
} else {
    $blockHTML .= $iconHTML;
}

echo '<div class="ms-title-with-icon ms-title-icon-align-' . $widgetData['data']['icon']['align'] . '">' . $blockHTML . '</div>';
<h2 class="title-divider" style="margin-top: -15px; border-top: 0;">Icona</h2>
<div class="widget-group" data-name="icon">
    <div class="row">

        <div class="col-lg-4">
            <label>Seleziona icona</label>
            <input class="form-control widget-data icon-picker" name="value" type="text" value="<?= ($widgetData['data']['icon']['value']); ?>">
        </div>

        <div class="col-lg-3">
            <label>Posizione elemento</label>
            <select class="form-control widget-data" name="align">
                <?php foreach(array('left' => 'Sinistra', 'right' => 'Destra', 'top' => 'Centro', 'bottom' => 'Sotto' ) as $posVal => $posName) { ?>
                    <option value="<?= $posVal; ?>" <?= ($widgetData['data']['icon']['align'] == $posVal ? 'selected' : ''); ?>><?= $posName; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-lg-3">
            <label>Colore icona</label>
            <select class="form-control widget-data" name="color">
                <optgroup label="Stili predefiniti">
                    <option value="">Usa colori tema</option>
                    <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                        <option class="ms-bg-<?= $btn_style; ?>" value="ms-text-<?= $btn_style; ?>" <?= ($widgetData['data']['icon']['color'] == 'ms-text-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
                    <?php } ?>
                </optgroup>
                <optgroup label="Personalizzato">
                    <option value="ms-text-custom" <?= ($widgetData['data']['icon']['color'] == 'ms-text-custom' ? 'selected' : ''); ?>>Personalizzato</option>
                </optgroup>
            </select>
        </div>

        <div class="col-lg-2">
            <label>Dimensione</label>
            <select class="form-control widget-data" name="px">
                <?php for($i = 10; $i <= 50; $i++) { ?>
                    <option value="<?= $i; ?>"  <?= ($widgetData['data']['icon']['px'] == $i ? 'selected' : ''); ?>><?= $i; ?>px</option>
                <?php } ?>
            </select>
        </div>

        <div class="col-lg-6 btn-colors-settings" style="display: <?= ($widgetData['data']['color'] == 'ms-text-custom' ? 'block' : 'none'); ?>;">
            <label>Colore personalizzato</label>
            <div class="input-group colorpicker-component">
                <input class="form-control widget-data" name="custom-color" type="text" value="<?= ($widgetData['data']['icon']['custom-color']); ?>">
                <span class="input-group-addon"><i></i></span>
            </div>
        </div>
    </div>
</div>

<h2 class="title-divider">Titolo</h2>
<div class="widget-group" data-name="title">
    <div class="row">
        <div class="col-lg-4">
            <label>Titolo</label>
            <input class="form-control widget-data" name="text" type="text" value="<?= ($widgetData['data']['title']['text']); ?>">
        </div>

        <div class="col-lg-4">
            <label>Tag elemento</label>
            <select class="form-control widget-data" name="tag">
                <?php foreach(array('h1', 'h2', 'h3', 'h4', 'h5', 'p', 'b') as $tag) { ?>
                    <option value="<?= $tag; ?>" <?= ($widgetData['data']['title']['tag'] == $tag ? 'selected' : ''); ?>><?= $tag; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-lg-4">
            <label>Colore testo</label>
            <select class="form-control widget-data" name="color">
                <optgroup label="Stili predefiniti">
                    <option value="">Usa colori tema</option>
                    <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                        <option class="ms-bg-<?= $btn_style; ?>" value="ms-text-<?= $btn_style; ?>" <?= ($widgetData['data']['title']['color'] == 'ms-text-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
                    <?php } ?>
                </optgroup>
                <optgroup label="Personalizzato">
                    <option value="ms-text-custom" <?= ($widgetData['data']['title']['color'] == 'ms-text-custom' ? 'selected' : ''); ?>>Personalizzato</option>
                </optgroup>
            </select>
        </div>

        <div class="col-lg-6 btn-colors-settings" style="display: <?= ($widgetData['data']['title']['color'] == 'ms-text-custom' ? 'block' : 'none'); ?>;">
            <label>Colore personalizzato</label>
            <div class="input-group colorpicker-component">
                <input class="form-control widget-data" name="custom-color" type="text" value="<?= ($widgetData['data']['title']['custom-color']); ?>">
                <span class="input-group-addon"><i></i></span>
            </div>
        </div>
    </div>
</div>

<h2 class="title-divider">Sottotitolo</h2>
<div class="widget-group" data-name="subtitle">
    <div class="row">
        <div class="col-lg-12">
            <label>Sottotitolo</label>
            <textarea class="form-control widget-data tinymce" name="text"><?= ($widgetData['data']['subtitle']['text']); ?></textarea>
        </div>

        <div class="col-lg-6">
            <label>Colore testo</label>
            <select class="form-control widget-data" name="color">
                <optgroup label="Stili predefiniti">
                    <option value="">Usa colori tema</option>
                    <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                        <option class="ms-bg-<?= $btn_style; ?>" value="ms-text-<?= $btn_style; ?>" <?= ($widgetData['data']['subtitle']['color'] == 'ms-text-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
                    <?php } ?>
                </optgroup>
                <optgroup label="Personalizzato">
                    <option value="ms-text-custom" <?= ($widgetData['data']['subtitle']['color'] == 'ms-text-custom' ? 'selected' : ''); ?>>Personalizzato</option>
                </optgroup>
            </select>
        </div>

        <div class="col-lg-6 btn-colors-settings" style="display: <?= ($widgetData['data']['subtitle']['color'] == 'ms-text-custom' ? 'block' : 'none'); ?>;">
            <label>Colore personalizzato</label>
            <div class="input-group colorpicker-component">
                <input class="form-control widget-data" name="custom-color" type="text" value="<?= ($widgetData['data']['subtitle']['custom-color']); ?>">
                <span class="input-group-addon"><i></i></span>
            </div>
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('[name="color"]').on('change', function () {
            if($(this).val() === 'ms-text-custom') {
                $(this).closest('.widget-group').find('.btn-colors-settings').show();
            } else {
                $(this).closest('.widget-group').find('.btn-colors-settings').hide().find('widget-data').val('');
            }
        });
    });
</script>
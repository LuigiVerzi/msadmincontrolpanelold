<?php
$iconStyle = array();
$iconClass = array();

if($widgetData['data']['icon']['color'] === 'ms-text-custom') {
    $iconStyle[] = 'color: ' . $widgetData['data']['icon']['custom-color'];
    $iconClass[] = 'ms-text-custom';
} else if($widgetData['data']['icon']['color'] !== '') {
    $iconClass[] = $widgetData['data']['icon']['color'];
}

$titleStyle = array();
$titleClass = array();

if($widgetData['data']['title']['color'] === 'ms-text-custom') {
    $titleStyle[] = 'color: ' . $widgetData['data']['title']['custom-color'];
    $titleClass[] = 'ms-text-custom';
} else if($widgetData['data']['title']['color'] !== '') {
    $titleClass[] = $widgetData['data']['title']['color'];
}
?>

<div class="visualWidget-intro">

    <?php if($widgetData['data']['icon']['value']) { ?>
        <i class="visualWidget-icon <?= $widgetData['data']['icon']['value']; ?> <?= implode(' ', $iconClass); ?>" style="<?= implode(';', $iconStyle); ?>"></i>
    <?php } else { ?>
        <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon' . ($blockDetails['is_html'] ? '_html' : '') . '.png'; ?>">
    <?php } ?>

    <?php if(!empty($widgetData['data']['title']['text'])) { ?>
        <b class="visualWidget-title <?= implode(' ', $titleClass); ?>" style="<?= implode(';', $titleStyle); ?>"><?= $widgetData['data']['title']['text']; ?></b>
        <small class="visualWidget-descr"><?= (!$blockDetails['is_html'] ? 'Tag: <u>' . $widgetData['data']['title']['tag'] . '</u>, ' : ''); ?>Allineamento icona: <u><?= $widgetData['data']['icon']['align']; ?></u></small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Titolo mancante!</b>
        <small class="visualWidget-descr text-danger">Testo del titolo mancante.</small>
    <?php } ?>
</div>
<?php if($_POST['settings']["structure_mode"] && $_POST['settings']["shortcodes"]) { ?>

    <div class="alert alert-info">
        È possibile utilizzare degli shortcodes all'interno del seguente editor di testo.
        <a href="#" class="pull-right btn btn-sm btn-default" onclick="$('.tinymceShortcodes').toggle();" style="margin: -5px 0;">Mostra Shortcodes</a>
    </div>

    <div class="tinymceShortcodes" style="margin-top: 15px; display: none;">
        <table class="table table-bordered small">
            <tr>
                <th style="background-color: #ececec;" colspan="2">Lista shortcodes Disponibili</th>
            </tr>
            <?php foreach($_POST['settings']['shortcodes'] as $shortcode => $descr) { ?>
                <tr>
                    <td width="210" style="background-color: #f4f4f4;"><?= $shortcode; ?></td> <td><?= $descr; ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
<?php } ?>


<div class="row">
    <div class="col-lg-12">
        <label>Contenuto</label>
        <textarea name="content" class="widget-data tinymce"><?php echo $MSFrameworki18n->getFieldValue($widgetData['data']['content']) ?></textarea>
    </div>
</div>
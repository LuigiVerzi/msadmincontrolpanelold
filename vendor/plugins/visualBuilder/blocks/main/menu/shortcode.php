<<?= $widgetData['data']['container']['tag']; ?> class="<?= htmlentities($widgetData['data']['container']['class']); ?>">
    <ms-menu id="<?= $widgetData['data']['id']; ?>">

        <ms-menu-class type="main_element_append_class" class="<?= htmlentities($widgetData['data']['item']['class']); ?>"/>
        <ms-menu-class type="main_element_active_class" class="<?= htmlentities($widgetData['data']['item']['active-class']); ?>"/>
        <ms-menu-class type="menu-item-has-children" class="<?= htmlentities($widgetData['data']['item']['sub-class']); ?>"/>
        <ms-menu-class type="sub_element_append_class" class="<?= htmlentities($widgetData['data']['sub-item']['voice']['class']); ?>"/>

        <ms-menu-element type="main_element" value="<?= htmlentities($widgetData['data']['item']['tag']); ?>"/>
        <ms-menu-element type="sub_element" value="<?= htmlentities($widgetData['data']['sub-item']['container']['tag']); ?>"/>

        <?php if(!empty($widgetData['data']['sub-item']['arrow'])) { ?>
            <ms-menu-element type="arrow_right" value='<?= '<span class="' . htmlentities($widgetData['data']['sub-item']['arrow']) . '"></span>'; ?>'/>
        <?php } ?>

        <?php if(!empty($widgetData['data']['item']['arrow'])) { ?>
            <ms-menu-element type="arrow_down" value='<?= '<span class="' . htmlentities($widgetData['data']['item']['arrow']) . '"></span>'; ?>'/>
        <?php } ?>

    </ms-menu>
</<?= $widgetData['data']['container']['tag']; ?>>
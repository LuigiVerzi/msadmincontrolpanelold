<?php
$menuDetails = (new \MSFramework\menu())->getMenuDetails($widgetData['data']['id']);
if($menuDetails) $menuDetails = $menuDetails[$widgetData['data']['id']];
?>

<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <?php if($menuDetails) { ?>
    <b class="visualWidget-title"><?= (new \MSFramework\i18n())->getFieldValue($menuDetails['nome']); ?></b>
    <small class="visualWidget-descr">Menù</small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Menù mancante!</b>
        <small class="visualWidget-descr text-danger">Il Menù ID <?= $widgetData['data']['id']; ?> non è stata trovato, probabilmente è stato eliminato.</small>
    <?php } ?>
</div>
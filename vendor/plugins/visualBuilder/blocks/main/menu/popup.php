<div class="row">
    <div class="col-lg-12">
        <label>Seleziona Menù</label>
        <select class="form-control widget-data required" id="id_menu" name="id">
            <option value="">Seleziona Menù</option>
            <?php foreach((new \MSFramework\menu())->getMenuDetails() as $menu) { ?>
                <option value="<?= $menu['id']; ?>" <?= ($widgetData['data']['id'] == $menu['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($menu['nome']); ?></option>
            <?php } ?>
        </select>

        <div class="text-right">
            <a href="#" class="btn btn-sm btn-warning btn-outline" id="editCurrentMenu" style="display: <?= ($widgetData['data']['id'] ? 'inline-block' : 'none'); ?>;">Modifica</a>
            <a href="#" class="btn btn-sm btn-default" id="editCurrentMenu">Crea nuovo</a>
        </div>
    </div>
</div>

<h2 class="title-divider">Contenitore</h2>
<div class="row widget-group" data-name="container">
    <div class="col-lg-3">
        <label>Tag</label>
        <select class="form-control widget-data" name="tag">
            <?php foreach(array('ul', 'div', 'p') as $tag) { ?>
                <option value="<?= $tag; ?>" <?= ($widgetData['data']['container']['tag'] == $tag ? 'selected' : ''); ?>><?= $tag; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-lg-3">
        <label>Classe</label>
        <input type="text" class="form-control widget-data" name="class" value="<?= htmlentities($widgetData['data']['container']['class']); ?>">
    </div>
</div>

<h2 class="title-divider">Voce</h2>
<div class="row widget-group" data-name="item">
    <div class="col-lg-3">
        <label>Tag</label>
        <select class="form-control widget-data" name="tag">
            <?php foreach(array('li', 'div', 'p', 'b') as $tag) { ?>
                <option value="<?= $tag; ?>" <?= ($widgetData['data']['item']['tag'] == $tag ? 'selected' : ''); ?>><?= $tag; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-lg-3">
        <label>Classe</label>
        <input type="text" class="form-control widget-data" name="class" value="<?= htmlentities($widgetData['data']['item']['class']); ?>">
    </div>
    <div class="col-lg-3">
        <label>Classe attivo</label>
        <input type="text" class="form-control widget-data" name="active-class" value="<?= htmlentities($widgetData['data']['item']['active-class']); ?>">
    </div>
    <div class="col-lg-3">
        <label>Classe con sottoelementi</label>
        <input type="text" class="form-control widget-data" name="sub-class" value="<?= htmlentities($widgetData['data']['item']['sub-class']); ?>">
    </div>
    <div class="col-lg-3">
        <label>Icona dropdown</label>
        <input class="form-control widget-data icon-picker" name="arrow" placeholder="fa fa-caret-down" type="text" value="<?= ($widgetData['data']['item']['arrow']); ?>">
    </div>
</div>

<h2 class="title-divider">Sotto elementi</h2>
<div class="row widget-group" data-name="sub-item">
    <div class="col-lg-3 widget-group" data-name="container">
        <label>Tag contenitore</label>
        <select class="form-control widget-data" name="tag">
            <?php foreach(array('ul', 'div', 'p') as $tag) { ?>
                <option value="<?= $tag; ?>" <?= ($widgetData['data']['sub-item']['container']['tag'] == $tag ? 'selected' : ''); ?>><?= $tag; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-lg-3 widget-group" data-name="container">
        <label>Classe contenitore</label>
        <input type="text" class="form-control widget-data" name="class" value="<?= htmlentities($widgetData['data']['sub-item']['container']['class']); ?>">
    </div>
    <div class="col-lg-3 widget-group" data-name="voice">
        <label>Tag voci</label>
        <select class="form-control widget-data" name="tag">
            <?php foreach(array('li', 'div', 'p', 'b') as $tag) { ?>
                <option value="<?= $tag; ?>" <?= ($widgetData['data']['sub-item']['voice']['tag'] == $tag ? 'selected' : ''); ?>><?= $tag; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-lg-3 widget-group" data-name="voice">
        <label>Classe voci</label>
        <input type="text" class="form-control widget-data" name="class" value="<?= htmlentities($widgetData['data']['sub-item']['voice']['class']); ?>">
    </div>
    <div class="col-lg-3">
        <label>Icona dropdown</label>
        <input class="form-control widget-data icon-picker" name="arrow" placeholder="fa fa-caret-right" type="text" value="<?= ($widgetData['data']['sub-item']['arrow']); ?>">
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('#editCurrentMenu').on('click', function () {
           showFastEditor('contenuti_faq', '', function (result) {
               if(result.id > 0) {
                   $('#id_menu').append('<option value="' + result.id + '">FAQ creata alle ore ' + new Date().toLocaleTimeString('it-IT', {timeStyle: 'short'}) + '</a>').val(result.id).change();
                   $('#fastModuleEditor').modal('toggle');
                   toastr['success']('FAQ creata correttamente');
               }
           });
        });

        $('#id_menu').on('change', function (e) {
            if($(this).val() !== "") {
                $('#editCurrentMenu').css('display', 'inline-block');
            } else {
                $('#editCurrentMenu').hide();
            }
        });

        $('#editCurrentMenu').on('click', function () {
           showFastEditor('contenuti_menu', $('#id_menu').val(), function (result) {
               $('#fastModuleEditor').modal('toggle');
               toastr['success']('Menù aggiornato correttamente');
           });
        });
    });
</script>
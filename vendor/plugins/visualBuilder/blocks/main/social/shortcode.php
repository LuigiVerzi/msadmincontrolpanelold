<?php
Global $MSFrameworkPages, $MSFrameworkParser;

$MSFrameworkParser->loadLibrary('fontAwesome');

$color_class = '';
$color_style = '';

if($widgetData['data']['color'] === 'ms-text-custom') {
    $color_style = 'color: ' . $widgetData['data']['custom-color'];
    $color_class = 'ms-text-custom';
} else if($widgetData['data']['color'] !== '') {
    $color_class = $widgetData['data']['color'] . ' ms-text-custom';
}

$shortcodes = $MSFrameworkPages->getCommonShortcodes(true);
foreach($widgetData['data']['socials'] as $social_id => $social_value) {
    if(!empty($social_value) && in_array('{' . $social_id . '}', array_keys($shortcodes))) {

        $real_value = '';
        if(!in_array(trim($social_value), array_keys($shortcodes))) {
            $real_value = $social_value;
        } else if(!empty($shortcodes[$social_value])) {
            $real_value = $shortcodes[$social_value];
        }

        if(!empty($real_value)) {
            echo '<a target="_blank" href="' . htmlentities($real_value) . '" class="ms-social-btn ' . $social_id . '"><i class="' . $color_class . '" style="' . $color_style . '"></i></a>';
        }

    }
}
?>
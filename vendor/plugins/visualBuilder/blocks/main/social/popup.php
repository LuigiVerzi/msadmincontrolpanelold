<?php
$socialProfiles = array();
foreach($MSFrameworkPages->getCommonShortcodes(false) as $social_id => $social_v) {
    if(stristr($social_id, 'ms-social')) {
        $socialProfiles[trim($social_id, '{}')] = array_merge($social_v, array('shortcode' => $social_id));
    }
}
?>

<div class="alert alert-info">
    Utilizza gli shortcode se vuoi far ereditare il valore direttamente dalle preferenze del sito. Oppure lascia un input vuoto per nascondere l'icona.
</div>

<div class="widget-group" data-name="socials">
    <div class="row">
        <?php foreach($socialProfiles as $s_id => $social) { ?>
            <div class="col-lg-12">
                <label><?= $social['title']; ?> <small class="pull-right text-muted"><?= $social['value']; ?></small></label>
                <input type="text" class="form-control widget-data" name="<?= $s_id; ?>" value="<?= htmlentities(($widgetData['data'][$s_id] ? $widgetData['data'][$s_id]  : $social['shortcode'])); ?>">
            </div>
        <?php } ?>
    </div>
</div>

<h2 class="title-divider">
    Colori
</h2>

<div class="row">
    <div class="col-lg-6">
        <label>Colori</label>
        <select class="form-control widget-data" name="color">
            <optgroup label="Stili predefiniti">
                <option value="">Usa colori tema</option>
                <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                    <option class="ms-bg-<?= $btn_style; ?>" value="ms-text-<?= $btn_style; ?>" <?= ($widgetData['data']['color'] == 'ms-text-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
                <?php } ?>
            </optgroup>
            <optgroup label="Personalizzato">
                <option value="ms-text-custom" <?= ($widgetData['data']['color'] == 'ms-text-custom' ? 'selected' : ''); ?>>Personalizzato</option>
            </optgroup>
        </select>
    </div>
    <div class="col-lg-6 btn-colors-settings" style="display: <?= ($widgetData['data']['color'] == 'ms-text-custom' ? 'block' : 'none'); ?>;">
        <label>Colore personalizzato</label>
        <div class="input-group colorpicker-component">
            <input class="form-control widget-data" name="custom-color" type="text" value="<?= ($widgetData['data']['custom-color']); ?>">
            <span class="input-group-addon"><i></i></span>
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('[name="color"]').on('change', function () {
            if($(this).val() === 'ms-text-custom') {
                $('.btn-colors-settings').show();
            } else {
                $('.btn-colors-settings').hide().find('widget-data').val('');
            }
        });
    });
</script>
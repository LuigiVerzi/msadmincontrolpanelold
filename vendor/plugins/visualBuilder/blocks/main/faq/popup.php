<div class="row">
    <div class="col-lg-12">
        <label>Seleziona FAQ</label>
        <select class="form-control widget-data required" id="id_faq" name="id">
            <option value="">Seleziona FAQ</option>
            <?php foreach((new \MSFramework\faq())->getFAQDetails() as $faq) { ?>
                <option value="<?= $faq['id']; ?>" <?= ($widgetData['data']['id'] == $faq['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($faq['nome']); ?></option>
            <?php } ?>
        </select>

        <div class="text-right">
            <a href="#" class="btn btn-sm btn-warning btn-outline" id="editCurrentFaq" style="display: <?= ($widgetData['data']['id'] ? 'inline-block' : 'none'); ?>;">Modifica</a>
            <a href="#" class="btn btn-sm btn-default" id="createNewFaq">Crea nuova</a>
        </div>
    </div>

    <div class="col-lg-12">
        <label>Metodo visualizzazione</label>
        <select class="form-control widget-data" name="mode">
            <option value="standard" <?= ($widgetData['data']['mode'] == 'standard' ? 'selected' : ''); ?>>Standard</option>
            <option value="accordion" <?= ($widgetData['data']['mode'] == 'accordion' ? 'selected' : ''); ?>>Fisarmonica</option>
        </select>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('.widget-group[data-name="' + $('.widget-data[name="mode"]').val() + '_settings"]').show();

        $('#createNewFaq').on('click', function () {
           showFastEditor('contenuti_faq', '', function (result) {
               if(result.id > 0) {
                   $('#id_faq').append('<option value="' + result.id + '">FAQ creata alle ore ' + new Date().toLocaleTimeString('it-IT', {timeStyle: 'short'}) + '</a>').val(result.id).change();
                   $('#fastModuleEditor').modal('toggle');
                   toastr['success']('FAQ creata correttamente');
               }
           });
        });

        $('#id_faq').on('change', function (e) {
            if($(this).val() !== "") {
                $('#editCurrentFaq').css('display', 'inline-block');
            } else {
                $('#editCurrentFaq').hide();
            }
        });

        $('#editCurrentFaq').on('click', function () {
           showFastEditor('contenuti_faq', $('#id_faq').val(), function (result) {
               $('#fastModuleEditor').modal('toggle');
               toastr['success']('FAQ aggiornata correttamente');
           });
        });
    });
</script>
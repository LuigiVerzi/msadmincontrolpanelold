<?php
$faqDetails = (new \MSFramework\faq())->getFAQDetails($widgetData['data']['id']);
if($faqDetails) $faqDetails = $faqDetails[$widgetData['data']['id']];
?>

<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <?php if($faqDetails) { ?>
    <b class="visualWidget-title">FAQ <?= (new \MSFramework\i18n())->getFieldValue($faqDetails['titolo']); ?></b>
    <small class="visualWidget-descr">FAQ <?= ($widgetData['data']['mode'] === 'standard' ? 'Standard' : 'Fisarmonica'); ?> con <u><?= count($faqDetails['questions']); ?></u> domand<?= (count($faqDetails['questions']) == 1 ? 'a' : 'e'); ?></small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">FAQ mancante!</b>
        <small class="visualWidget-descr text-danger">La FAQ ID <?= $widgetData['data']['id']; ?> non è stata trovata, probabilmente è stata eliminata.</small>
    <?php } ?>
</div>
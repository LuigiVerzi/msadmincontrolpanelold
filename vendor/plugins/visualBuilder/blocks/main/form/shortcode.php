<ms-form id="<?= $widgetData['data']['form']; ?>" hide-label="<?= ($widgetData['data']['hide-label'] ? '1' : '0') ?>">
    <ms-field-class type="button" class="<?= $widgetData['data']['form_submit_class'] . ' ' . $widgetData['data']['form_submit_style']; ?>"/>
</ms-form>
<div class="row">
    <div class="col-lg-12">
        <label>Seleziona form</label>
        <select class="form-control widget-data" name="form" id="id_form">
            <option value="">Seleziona form</option>
            <?php foreach((new \MSFramework\forms())->getFormDetails() as $form) { ?>
                <option value="<?= $form['id']; ?>" <?= ($widgetData['data']['form'] == $form['id'] ? 'selected' : ''); ?>><?= $form['nome']; ?></option>
            <?php } ?>
        </select>

        <div class="text-right">
            <a href="#" class="btn btn-sm btn-warning btn-outline" id="editCurrentForm" style="display: <?= ($widgetData['data']['form'] ? 'inline-block' : 'none'); ?>;">Modifica</a>
            <a href="#" class="btn btn-sm btn-default" id="creatNewForm">Crea nuovo</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label>Nascondi 'Label'</label>
        <select class="form-control widget-data" name="hide-label">
            <option value="0">No</option>
            <option value="1" <?= ($widgetData['data']['hide-label'] === '1' ? 'selected' : ''); ?>>Si</option>
        </select>
    </div>
    <div class="col-lg-4">
        <label>Stile pulsante Invio</label>
        <select class="form-control widget-data" name="form_submit_style">
            <option value="">Default</option>
            <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                <option class="ms-bg-<?= $btn_style; ?>" value="ms-btn-<?= $btn_style; ?>" <?= ($widgetData['data']['form_submit_style'] == 'ms-btn-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-lg-4">
        <label>Classe pulsante Invio</label>
        <input type="text" class="form-control widget-data" name="form_submit_class" value="<?= htmlentities($widgetData['data']['form_submit_class']); ?>">
    </div>
</div>


<script>
    visualBuilderPopupJS.push(function () {

        $('#creatNewForm').on('click', function () {
            showFastEditor('contenuti_form', '', function (result) {
                if(result.id > 0) {
                    $('#id_form').append('<option value="' + result.id + '">Form creato alle ore ' + new Date().toLocaleTimeString('it-IT', {timeStyle: 'short'}) + '</a>').val(result.id).change();
                    $('#fastModuleEditor').modal('toggle');
                    toastr['success']('Form creato correttamente');
                }
            });
        });

        $('#id_form').on('change', function (e) {
            if($(this).val() !== "") {
                $('#editCurrentForm').css('display', 'inline-block');
            } else {
                $('#editCurrentForm').hide();
            }
        });

        $('#editCurrentForm').on('click', function () {
            showFastEditor('contenuti_form', $('#id_form').val(), function (result) {
                $('#fastModuleEditor').modal('toggle');
                toastr['success']('Form aggiornato correttamente');
            });
        });
        
    });
</script>
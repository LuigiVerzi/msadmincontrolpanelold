<?php
$formDetails = (new \MSFramework\forms())->getFormDetails($widgetData['data']['form']);
if($formDetails) $formDetails = $formDetails[$widgetData['data']['form']];
?>

<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <?php if($formDetails) { ?>
    <b class="visualWidget-title">Form <?= $formDetails['nome']; ?></b>
    <small class="visualWidget-descr">Azioni del form: <u><?= implode('</u>, <u>', json_decode($formDetails['actions'], true)); ?></u></small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Form mancante!</b>
        <small class="visualWidget-descr text-danger">Il form ID <?= $widgetData['data']['form']; ?> non è stato trovato, probabilmente è stato eliminato.</small>
    <?php } ?>
</div>
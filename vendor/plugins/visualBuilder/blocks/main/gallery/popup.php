<div class="row">

    <?php if(!isset($widgetDetails['shortcode-images'])) { ?>
    <div class="col-lg-12">
        <label>Seleziona galleria</label>
        <select class="form-control widget-data required" id="id_galleria" name="id">
            <option value="">Seleziona una galleria</option>
            <?php foreach((new \MSFramework\gallery())->getGalleryDetails() as $gallery) { ?>
                <option value="<?= $gallery['id']; ?>" <?= ($widgetData['data']['id'] == $gallery['id'] ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($gallery['titolo']); ?></option>
            <?php } ?>
        </select>

        <div class="text-right">
            <a href="#" class="btn btn-sm btn-warning btn-outline" id="editCurrentGallery" style="display: <?= ($widgetData['data']['id'] ? 'inline-block' : 'none'); ?>;">Modifica</a>
            <a href="#" class="btn btn-sm btn-default" id="creatNewGallery">Crea nuova</a>
        </div>
    </div>
    <?php } ?>

    <div class="col-lg-12">
        <label>Immagini da mostrare</label>
        <select class="form-control widget-data" name="limit">
            <option value="" <?= ($widgetData['data']['limit'] == '' ? 'selected' : ''); ?>>Tutte</option>
            <?php for($i = 1; $i <= 50; $i++) { ?>
                <option value="<?= $i; ?>" <?= ($widgetData['data']['limit'] == $i ? 'selected' : ''); ?>><?= $i; ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('#creatNewGallery').on('click', function () {
           showFastEditor('gallery_gallery', '', function (result) {
               if(result.id > 0) {
                   $('#id_galleria').append('<option value="' + result.id + '">Galleria creata alle ore ' + new Date().toLocaleTimeString('it-IT', {timeStyle: 'short'}) + '</a>').val(result.id).change();
                   $('#fastModuleEditor').modal('toggle');
                   toastr['success']('Galleria creata correttamente');
               }
           });
        });

        $('#id_galleria').on('change', function (e) {
            if($(this).val() !== "") {
                $('#editCurrentGallery').css('display', 'inline-block');
            } else {
                $('#editCurrentGallery').hide();
            }
        });

        $('#editCurrentGallery').on('click', function () {
           showFastEditor('gallery_gallery', $('#id_galleria').val(), function (result) {
               $('#fastModuleEditor').modal('toggle');
               toastr['success']('Galleria aggiornata correttamente');
           });
        });
    });
</script>
<ms-gallery
        id="<?= $widgetData['data']['id']; ?>"
        images="<?= ($blockDetails['shortcode-images'] ? $blockDetails['shortcode-images'] : ''); ?>"
        show-mode="<?= $widgetData['data']['view-mode']['mode']; ?>"
        limit="<?= $widgetData['data']['limit']; ?>"
        columns="<?= $widgetData['data']['view-mode'][$widgetData['data']['view-mode']['mode']]['cols']; ?>"
        md-columns="<?= $widgetData['data']['view-mode'][$widgetData['data']['view-mode']['mode']]['md-cols']; ?>"
        sm-columns="<?= $widgetData['data']['view-mode'][$widgetData['data']['view-mode']['mode']]['sm-cols']; ?>"
        autoplay="<?= $widgetData['data']['carousel_settings']['autoplay']; ?>"
        dots="<?= $widgetData['data']['carousel_settings']['dots']; ?>"
        thumbnails="<?= $widgetData['data']['view-mode']['thumbnails']; ?>">
</ms-gallery>
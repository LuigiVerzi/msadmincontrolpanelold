<?php
if(!isset($blockDetails['shortcode-images'])) {
    $galleryDetails = (new \MSFramework\gallery())->getGalleryDetails($widgetData['data']['id']);
    if ($galleryDetails) $galleryDetails = $galleryDetails[$widgetData['data']['id']];
}
?>

<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <?php if($galleryDetails) { ?>
        <b class="visualWidget-title">Galleria <?= (new \MSFramework\i18n())->getFieldValue($galleryDetails['titolo']); ?></b>
        <small class="visualWidget-descr"><?= ($widgetData['data']['view-mode']['mode'] === 'carousel' ? 'Carosello' : 'Griglia'); ?> di <u><?= ($widgetData['data']['limit'] && count($galleryDetails['gallery_friendly']) > $widgetData['data']['limit'] ? $widgetData['data']['limit'] : count($galleryDetails['gallery_friendly'])); ?></u> immagin<?= (count($galleryDetails['gallery_friendly']) == 1 ? 'e' : 'i'); ?></small>
    <?php } else if(isset($blockDetails['shortcode-images'])) { ?>
        <b class="visualWidget-title">Galleria dinamica</b>
        <small class="visualWidget-descr"><?= ($widgetData['data']['view-mode']['mode'] === 'carousel' ? 'Carosello' : 'Griglia'); ?> con <u>immagini dinamiche</u></small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Galleria mancante!</b>
        <small class="visualWidget-descr text-danger">La galleria ID <?= $widgetData['data']['id']; ?> non è stata trovata, probabilmente è stato eliminato.</small>
    <?php } ?>
</div>
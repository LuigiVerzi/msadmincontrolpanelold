<?php
$tabTitles = array();
foreach($widgetData['data']['tabs'] as $tab_values) {
    $tabTitles[] = $tab_values['title'];
}
?>
<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <?php if(array_keys($widgetData['data']['tabs'])) { ?>
        <b class="visualWidget-title">Tabs</b>
        <small class="visualWidget-descr">Tab presenti: <u><?= implode('</u>, <u>', $tabTitles); ?></u></small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Tabs</b>
        <small class="visualWidget-descr text-danger">Testo pulsante mancante.</small>
    <?php } ?>
</div>
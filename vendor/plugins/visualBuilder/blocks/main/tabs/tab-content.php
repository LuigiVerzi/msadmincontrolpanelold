<div role="tabpanel" id="<?= $tab_id; ?>" class="msWidgetTabPanel tab-pane <?= ($i === 1 ? 'active' : ''); ?>">
    <div class="panel-body">
        <div class="widget-group" data-name="<?= $tab_id; ?>">

            <a href="#" class="btn btn-danger btn-small pull-right deleteTab">Elimina</a>
            <a href="#" class="btn btn-default btn-small pull-right moveBtn moveRight" style="margin-right: 5px;"><i class="fa fa-arrow-right"></i></a>
            <a href="#" class="btn btn-default btn-small pull-right moveBtn moveLeft" style="margin-right: 5px;"><i class="fa fa-arrow-left"></i></a>

            <div class="row">
                <div class="col-lg-4">
                    <label>Titolo TAB</label>
                    <input type="text" class="form-control widget-data" name="title" value="<?= $tab_values['title']; ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <label>Contenuto</label>
                    <textarea class="form-control widget-data attachVisualBuilder" name="data"><?= $tab_values['data']; ?></textarea>
                </div>
            </div>

        </div>
    </div>
</div>
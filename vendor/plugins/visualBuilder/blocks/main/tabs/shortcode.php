<div class="ms-tabs <?= ($widgetData['data']['view']['tab-position'] === 'left' ? 'ms-tabs-vertical' : ''); ?>">
    <?php foreach(array_reverse($widgetData['data']['tabs']) as $tabID => $tabValues) { ?>
        <div class="ms-tab-element">
            <h1 class="ms-tab-title"><?= $tabValues['title']; ?></h1>
            <div class="ms-tab-content">
                <?= $this->drawVisualContent(json_decode($tabValues['data'], true), true); ?>
            </div>
        </div>
    <?php } ?>
</div>
<?php
if(!$widgetData['data']['tabs']) {
    $widgetData['data']['tabs'] = array(
        'ms_tab_0' => array()
    );
}

$i = 0;
?>

<div class="widget-group" data-name="view">
    <div class="row">
        <div class="col-lg-12">
            <label>Posizione tab</label>
            <select class="form-control widget-data" name="tab-position">
                <option value="top">Sopra</option>
                <option value="left" <?= ($widgetData['data']['view']['tab-position'] == 'left' ? 'selected' : ''); ?>>Sinistra</option>
            </select>
        </div>
    </div>
</div>

<div class="widget-group" data-name="tabs">

    <div class="tabs-container tabWidgetTabsContainer">
        <ul class="nav nav-tabs tabWidgetTabs" role="tablist">
            <?php $i = 0; foreach($widgetData['data']['tabs'] as $tab_id => $tab_values) { $i++;?>
                <li class="<?= ($i === 1 ? 'active' : ''); ?>"><a class="nav-link" data-toggle="tab" href="#<?= $tab_id; ?>"><?= ($tab_values['title'] ? $tab_values['title'] : 'Senza nome'); ?></a></li>
            <?php } ?>
            <li class=""><a class="nav-link addNewTab" href="#"><i class="fa fa-plus"></i></a></li>
        </ul>

        <div class="tab-content" id="tabBlockContent">
            <?php
            $i = 0;
            foreach($widgetData['data']['tabs'] as $tab_id => $tab_values) {
                $i++;
                include('tab-content.php');
            }
            ?>
        </div>
    </div>

</div>

<div id="tabContentEmptyTemplate" style="display: none;">
    <?
    $tab_id = 'replaceValue';
    $i = 0;
    $tab_values = array();
    ob_start();
    include('tab-content.php');
    echo str_replace('attachVisualBuilder', 'attachVisualBuilderTMP', ob_get_clean());
    ?>
</div>

<script>
    visualBuilderPopupJS.push(function () {

        var refreshTabBtns = function() {
            if($('.tabWidgetTabsContainer li').length > 1) $('.deleteTab').show();
            else $('.deleteTab').hide();

            $('#tabBlockContent .msWidgetTabPanel .moveBtn').show();
            $('#tabBlockContent .msWidgetTabPanel:first-child .moveLeft').hide();
            $('#tabBlockContent .msWidgetTabPanel:last-child .moveRight').hide();

        };

        $('.addNewTab').on('click', function () {
            var uniqueID = 'ms_tab_' + (new Date().getUTCMilliseconds());
            $(this).parent().before('<li class=""><a class="nav-link" data-toggle="tab" href="#' + uniqueID + '">Senza nome</a></li>');
            $('#tabBlockContent').append($('#tabContentEmptyTemplate').html().replace(/replaceValue/gm, uniqueID).replace('attachVisualBuilderTMP', 'attachVisualBuilder'));
            $('[href="#' + uniqueID + '"]').click();

            $('[data-name="' + uniqueID + '"]').find('.attachVisualBuilder').visualBuilder(<?= json_encode($_POST['settings']); ?>);
            refreshTabBtns();
        });

        $('.tabWidgetTabsContainer').on('change', '[name="title"]', function (e) {
            var title = $(this).val();

            if(!title.length) title = 'Senza titolo';

            $('.tabWidgetTabs > .active .nav-link').text(title);
        }).on('click', '.deleteTab', function (e) {

            if($('.tabWidgetTabs > li').length <= 1) return false;

            if(confirm("Sei sicuro di voler eliminare il tab?")) {
                $('.tabWidgetTabs > li.active, .tabWidgetTabsContainer .msWidgetTabPanel.active').remove();
                $('.tabWidgetTabs > li').first().children('a').click();
                refreshTabBtns();
            }
        }).on('click', '.moveBtn', function (e) {

            var $tabTitle = $('.tabWidgetTabs > li.active');
            var $tabContent = $('.tabWidgetTabsContainer .msWidgetTabPanel.active');

            if($(this).hasClass('moveLeft')) {
                $tabContent.insertBefore($tabContent.prev());
                $tabTitle.insertBefore($tabTitle.prev());
            } else {
                $tabContent.insertAfter($tabContent.next());
                $tabTitle.insertAfter($tabTitle.next());
            }

            refreshTabBtns();

        });

        refreshTabBtns();

    });
</script>
<input type="hidden" class="widget-data" name="upload_path" value="<?= $_POST['settings']['uploads']; ?>">
<input type="hidden" class="widget-data" name="upload_common" value="<?= $_POST['settings']['uploads_common']; ?>">

<div class="row">
    <div class="col-lg-8">
        <label>Seleziona immagine</label>
        <div class="widget-data uploader" name="image" data-id="immagine_singola" data-limit="1">
            <?php (new \MSFramework\uploads($_POST['settings']['uploads'], $_POST['settings']['uploads_common']))->initUploaderHTML("immagine_singola", (is_array($widgetData['data']['image']['files']) ? $widgetData['data']['image']['files'] : array())); ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="widget-group" data-name="styles">
            <label>Adattamento</label>
            <select class="widget-data form-control" name="object-fit">
                <?php
                foreach(
                    array('' => 'Default',
                        'cover' => 'Cover',
                        'contain' => 'Contain',
                        'fill' => 'Fill',
                        'scale-down' => 'Scale Down') as $fit_k => $fit_v) {
                    echo '<option value="' . $fit_k . '" ' . ($widgetData['data']['styles']['object-fit'] == $fit_k ? 'selected' : '') . '>' . $fit_v . '</option>';
                }
                ?>
            </select>

            <label>Posizione</label>
            <select class="widget-data form-control" name="object-position">
                <?php
                foreach(array('', 'center', 'left', 'right', 'top', 'bottom' , 'center left', 'center right', 'cernter top', 'center bottom') as $fit_v) {
                    echo '<option value="' . $fit_v . '" ' . ($widgetData['data']['styles']['object-position'] == $fit_v ? 'selected' : '') . '>' . (!empty($fit_v) ? $fit_v : 'Default') . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
</div>

<h2 class="title-divider">
    Dimensioni
</h2>

<div class="widget-group" data-name="styles">

    <div class="row">
        <div class="col-lg-6">
            <label>Width</label>
            <input type="text" class="widget-data form-control size-input" name="width" placeholder="Es: 100px o 100%" value="<?= $widgetData['data']['styles']['width']; ?>">
        </div>
        <div class="col-lg-6">
            <label>Height</label>
            <input type="text" class="widget-data form-control size-input" name="height" placeholder="Es: 100px o 100%" value="<?= $widgetData['data']['styles']['height']; ?>">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <label>Max Width</label>
            <input type="text" class="widget-data form-control size-input" name="max-width" placeholder="Es: 100px o 100%" value="<?= $widgetData['data']['styles']['max-width']; ?>">
        </div>
        <div class="col-lg-3">
            <label>Max Height</label>
            <input type="text" class="widget-data form-control size-input" name="max-height" placeholder="Es: 100px o 100%" value="<?= $widgetData['data']['styles']['max-height']; ?>">
        </div>
        <div class="col-lg-3">
            <label>Min Width</label>
            <input type="text" class="widget-data form-control size-input" name="min-width" placeholder="Es: 100px o 100%" value="<?= $widgetData['data']['styles']['min-width']; ?>">
        </div>
        <div class="col-lg-3">
            <label>Min Height</label>
            <input type="text" class="widget-data form-control size-input" name="min-height" placeholder="Es: 100px o 100%" value="<?= $widgetData['data']['styles']['min-height']; ?>">
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {

    });
</script>
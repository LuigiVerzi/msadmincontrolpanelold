<?php
$previewImage = $blockDetails['url'] . '/icon.png';

$tmpUploader = new \MSFramework\uploads($widgetData['data']['image']['path'], $widgetData['data']['image']['is_common']);

if($widgetData['data']['image']['files']) {
    if(file_exists($tmpUploader->path . $widgetData['data']['image']['files'][0])) {
        $previewImage = $tmpUploader->path_html . $widgetData['data']['image']['files'][0];
    } else if(file_exists(UPLOAD_TMP_FOR_DOMAIN . $widgetData['data']['image']['files'][0])) {
        $previewImage = UPLOAD_TMP_FOR_DOMAIN_HTML . $widgetData['data']['image']['files'][0];
    }
}
?>
<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $previewImage; ?>">
    <?php if($widgetData['data']['image']['files']) { ?>
    <b class="visualWidget-title">Immagine singola</b>
    <small class="visualWidget-descr"></small>
    <?php } else { ?>
        <b class="visualWidget-title text-danger">Immagine mancante!</b>
        <small class="visualWidget-descr text-danger">Non è stata caricata nessuna immagine.</small>
    <?php } ?>
</div>
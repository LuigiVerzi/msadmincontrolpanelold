<?php
$image_styles = array();
foreach($widgetData['data']['styles'] as $style => $val) {
    if(!empty($val)) $image_styles[] = $style . ': ' . $val;
}
?>

<?php
if($widgetData['data']['onclick']['event']) {
    echo '<a ' . $this->getOnClickParams($widgetData['data']['onclick']) . '>';
}

$tmpUploader = new \MSFramework\uploads( $widgetData['data']['upload_path'], $widgetData['data']['upload_common']);
?>
<img style="<?= implode(';', $image_styles); ?>" src="<?= $tmpUploader->path_html; ?><?= $widgetData['data']['image']['files'][0]; ?>">
<?php
if($widgetData['data']['onclick']['event']) {
    echo '</a>';
}
?>
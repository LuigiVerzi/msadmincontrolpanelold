<?php

$structureHtml = $this->drawVisualContent(json_decode($widgetData['structure']['data'], true), true);

$itemClasses = array();
foreach($widgetData['data']['classes'] as $classes) {
    if(!empty($classes)) $itemClasses[] = $classes;
}

$serviceLists = array();
if($widgetData['data']['show']['category']) {
    $serviceIds = (new \MSFramework\services())->getServicesIDsByCategory($widgetData['data']['show']['category']);
    if($serviceIds) $serviceLists = (new \MSFramework\services())->getServiceDetails($serviceIds);
} else {
    $serviceLists = (new \MSFramework\services())->getServiceDetails();
}

echo '<div class="ms-row">';
foreach($serviceLists as $serviceDetails) {

    $page_relation_url = '';
    if(stristr($structureHtml, '{service-category-page-url}') || stristr($structureHtml, '{relative-page-url}')) {
        $page_relation_url = '';
        $page_relation_id = (new \MSFramework\pages())->getPageIDsByRelation('services', false, $serviceDetails['id']);
        if($page_relation_id) {
            $page_relation_url =  (new \MSFramework\pages())->getURL(end($page_relation_id));
        }
    }

    echo '<div class="' . implode(' ', $itemClasses) . '">';
    echo strtr($structureHtml, array(
        '{service-title}' => $MSFrameworki18n->getFieldValue($serviceDetails['nome']),
        '{service-description}' => $MSFrameworki18n->getFieldValue($serviceDetails['descr']),
        '{service-details}' => $MSFrameworki18n->getFieldValue($serviceDetails['long_descr']),
        '{service-image}' => $serviceDetails['gallery_friendly'][0]['html']['main'],
        '{service-banner}' => $serviceDetails['gallery_friendly_banner'][0]['html']['main'],
        '{service-price}' => $serviceDetails['prezzo'],
        '{service-page-url}' => $page_relation_url,
        '{relative-page-url}' => $page_relation_url,

        '{rand}' => uniqid() // Serve per far funzionare l'apertura delle pagine dentro i popup
    ));
    echo '</div>';
}
echo '</div>';
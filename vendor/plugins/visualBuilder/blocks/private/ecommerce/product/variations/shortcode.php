<ms-product-variations>
    <div class="row">
        <ms-cycle>
            <div class="ms-col-6 variation_box">
                <div class="attribute dynamic">
                    <label>{ms-variation-title}:</label>
                    <div class="attribute_value">
                        <select class="ms-input ms-product-variation-select" data-id="{ms-variation-id}">
                            {ms-variation-options}
                        </select>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </ms-cycle>
    </div>
</ms-product-variations>
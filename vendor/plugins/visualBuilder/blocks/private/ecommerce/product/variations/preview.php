<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <b class="visualWidget-title"><?= $blockDetails['title']; ?></b>
    <small class="visualWidget-descr">Selettore delle variazioni disponibili</small>
</div>
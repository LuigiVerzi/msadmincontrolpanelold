<div class="row">
    <div class="col-lg-12">
        <div class="widget-group" data-name="class">
            <h2 class="title-divider">Classi pulsanti</h2>
            <div class="row">
                <div class="col-lg-3">
                    <label>Contenitore</label>
                    <input class="form-control widget-data" type="text" name="container" placeholder="pagination" value="<?= $widgetData['data']['class']['container']; ?>">
                </div>
                <div class="col-lg-3">
                    <label>Elemento</label>
                    <input class="form-control widget-data" type="text" name="item" placeholder="" value="<?= $widgetData['data']['class']['item']; ?>">
                </div>
                <div class="col-lg-3">
                    <label>Pagina attiva</label>
                    <input class="form-control widget-data" type="text" name="active" placeholder="active" value="<?= $widgetData['data']['class']['active']; ?>">
                </div>
                <div class="col-lg-3">
                    <label>Disabilitato</label>
                    <input class="form-control widget-data" type="text" name="disabled" placeholder="disabled" value="<?= $widgetData['data']['class']['disabled']; ?>">
                </div>
            </div>
        </div>
    </div>
</div>
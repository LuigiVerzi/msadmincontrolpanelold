<?php

$paginationQuery = array();
foreach($_GET as $k => $p) {
    if(in_array($k, array('page', 'order', 'filters'))) {
        $paginationQuery[$k] = $p;
    }
}

$paginationHTML = (new \MSFramework\Ecommerce\categories())->composeHTMLPagination($paginationQuery, $blockDetails['search-info']['total_products'], $blockDetails['search-info']['page']);

if($widgetData['data']['class']['container']) $paginationHTML = str_replace('"pagination"', '"' . htmlentities($widgetData['data']['class']['container']) . '"', $paginationHTML);
if($widgetData['data']['class']['active']) $paginationHTML = str_replace('active', 'active ' . htmlentities($widgetData['data']['class']['active']), $paginationHTML);
if($widgetData['data']['class']['disabled']) $paginationHTML = str_replace('disabled', 'disabled ' . htmlentities($widgetData['data']['class']['disabled']), $paginationHTML);
if($widgetData['data']['class']['item']) $paginationHTML = str_replace('class="', 'class="' . htmlentities($widgetData['data']['class']['item']) . ' ', $paginationHTML);

echo $paginationHTML;
<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon' . ($blockDetails['is_html'] ? '_html' : '') . '.png'; ?>">
    <b class="visualWidget-title"><?= $blockDetails['title']; ?></b>
    <small class="visualWidget-descr"><?= (isset($widgetData['data']['value']) ? 'Data: <u>' . $widgetData['data']['value'] . '</u>, ' : ''); ?>Allineamento: <u><?= $widgetData['data']['align']; ?></u></small>
</div>
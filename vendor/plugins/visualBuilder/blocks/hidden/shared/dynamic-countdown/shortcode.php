<?php
$titleStyle = array();
$titleClass = array();
if(!empty($widgetData['data']['align'])) $titleStyle[] = 'text-align: ' . $widgetData['data']['align'];

if($widgetData['data']['color'] === 'ms-text-custom') {
    $titleStyle[] = 'color: ' . $widgetData['data']['custom-color'];
    $titleClass[] = 'ms-text-custom';
} else if($widgetData['data']['color'] !== '') {
    $titleClass[] = $widgetData['data']['color'];
}

$formatString = array();
foreach($widgetData['data']['format'] as $k => $f) {
    $formatString[] = '<span>' . (!$widgetData['data']['value-position'] ? '<b>%' . $k . '</b>' : '') . htmlentities($f) . ($widgetData['data']['value-position'] === 'bottom' ? '<b>%' . $k . '</b>' : '') . '</span>';
}

$uniqueCountdownID = 'ms-countdown' . uniqid();
echo '<div style="' . implode(';', $titleStyle) . '" class="ms-countdown ' . implode(' ', $titleClass) . '" id="' . $uniqueCountdownID . '" data-time="' . (isset($blockDetails["shortcode-text"]) ? $blockDetails["shortcode-text"] : $widgetData['data']['value']) . '"></div>';

$MSFrameworkParser->loadLibrary('countdown');

$countdown_js = array();

$countdown_js[] = "var countDownDate = MS_ItalianDateToInternation($('#" . $uniqueCountdownID . "').data('time'));";

if($widgetData['data']['after-deadline'] == "hide") {
    $countdown_js[] = "try {
        var dateObj = new Date(countDownDate);
        if((new Date(countDownDate)).getTime() < (new Date()).getTime()) {
            $('#" . $uniqueCountdownID . "').remove();
        }
    } catch(e) {}";
}

$countdown_js[] = "$('#" . $uniqueCountdownID . "').countdown(countDownDate, function(event) {
    $(this).html(event.strftime('" . implode('', $formatString) . "'));
});;";

$MSFrameworkParser->addScript(implode(PHP_EOL, $countdown_js));

$MSFrameworkParser->addScript("function MS_ItalianDateToInternation(date) {
    new_date = date;
    try {
        var exploded = date.split(' ');
        var exploded_date = exploded[0].split('/');
        new_date = exploded_date[2] + '-' + exploded_date[1] + '-' + exploded_date[0] + (exploded.length > 1 ? ' ' + exploded[1] : '');
    } catch(e) {}
    return new_date;
}");
<?php if(!isset($widgetDetails['shortcode-text'])) { ?>
    <div class="row">
        <div class="col-lg-8">
            <label>Data</label>
            <input class="form-control widget-data datetimepicker" readonly data-format="" name="value" type="text" placeholder="Data di scadenza (Formato: d/m/Y H:i)"  value="<?= htmlentities($widgetData['data']['value']); ?>">
        </div>
        <div class="col-lg-4">
            <label>Dopo la scadenza</label>
            <select class="form-control widget-data" name="after-deadline">
                <option value="">Continua a mostrare</option>
                <option value="hide" <?= ($widgetData['data']['after-deadline'] == "hide" ? 'selected' : ''); ?>>Nascondi</option>
            </select>
        </div>
    </div>
    <h2 class="title-divider">Formato</h2>
<?php } ?>


<div class="widget-group" data-name="format">
    <div class="row">
        <div class="col-lg-3">
            <label>Giorni</label>
            <input class="form-control widget-data" name="D" type="text" placeholder="g"  value="<?= htmlentities(($widgetData['data']['format']['D'] ? $widgetData['data']['format']['D'] : 'g')); ?>">
        </div>
        <div class="col-lg-3">
            <label>Ore</label>
            <input class="form-control widget-data" name="H" type="text" placeholder="o"  value="<?= htmlentities(($widgetData['data']['format']['H'] ? $widgetData['data']['format']['H'] : 'o')); ?>">
        </div>
        <div class="col-lg-3">
            <label>Minuti</label>
            <input class="form-control widget-data" name="M" type="text" placeholder="m"  value="<?= htmlentities(($widgetData['data']['format']['M'] ? $widgetData['data']['format']['M'] : 'm')); ?>">
        </div>
        <div class="col-lg-3">
            <label>Secondi</label>
            <input class="form-control widget-data" name="S" type="text" placeholder="s"  value="<?= htmlentities(($widgetData['data']['format']['S'] ? $widgetData['data']['format']['S'] : 's')); ?>">
        </div>
    </div>
</div>

<h2 class="title-divider">Elemento</h2>

<div class="row">

    <div class="col-lg-6">
        <label>Posizione countdown</label>
        <select class="form-control widget-data" name="align">
            <?php foreach(array('left' => 'Sinistra', 'center' => 'Centro', 'right' => 'Destra') as $posVal => $posName) { ?>
                <option value="<?= $posVal; ?>" <?= ($widgetData['data']['align'] == $posVal ? 'selected' : ''); ?>><?= $posName; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-lg-6">
        <label>Posizione valori</label>
        <select class="form-control widget-data" name="value-position">
            <?php foreach(array('' => 'Sopra', 'bottom' => 'Sotto') as $posVal => $posName) { ?>
                <option value="<?= $posVal; ?>" <?= ($widgetData['data']['value-position'] == $posVal ? 'selected' : ''); ?>><?= $posName; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-lg-6">
        <label>Colore testo</label>
        <select class="form-control widget-data" name="color">
            <optgroup label="Stili predefiniti">
                <option value="">Usa colori tema</option>
                <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                    <option class="ms-bg-<?= $btn_style; ?>" value="ms-text-<?= $btn_style; ?>" <?= ($widgetData['data']['color'] == 'ms-text-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
                <?php } ?>
            </optgroup>
            <optgroup label="Personalizzato">
                <option value="ms-text-custom" <?= ($widgetData['data']['color'] == 'ms-text-custom' ? 'selected' : ''); ?>>Personalizzato</option>
            </optgroup>
        </select>
    </div>

    <div class="col-lg-6 btn-colors-settings" style="display: <?= ($widgetData['data']['color'] == 'ms-text-custom' ? 'block' : 'none'); ?>;">
        <label>Colore personalizzato</label>
        <div class="input-group colorpicker-component">
            <input class="form-control widget-data" name="custom-color" type="text" value="<?= ($widgetData['data']['custom-color']); ?>">
            <span class="input-group-addon"><i></i></span>
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('[name="color"]').on('change', function () {
            if($(this).val() === 'ms-text-custom') {
                $('.btn-colors-settings').show();
            } else {
                $('.btn-colors-settings').hide().find('widget-data').val('');
            }
        });
    });
</script>
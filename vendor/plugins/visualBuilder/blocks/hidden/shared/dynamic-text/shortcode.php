<?php
$titleStyle = array();
$titleClass = array();
if(!empty($widgetData['data']['align'])) $titleStyle[] = 'text-align: ' . $widgetData['data']['align'];

if($widgetData['data']['color'] === 'ms-text-custom') {
    $titleStyle[] = 'color: ' . $widgetData['data']['custom-color'];
    $titleClass[] = 'ms-text-custom';
} else if($widgetData['data']['color'] !== '') {
    $titleClass[] = $widgetData['data']['color'];
}


if($blockDetails['is_html']) {
    echo '<div style="' . implode(';', $titleStyle) . '" class="' . implode(' ', $titleClass) . '">';
    echo $blockDetails["shortcode-text"];
    echo '</div>';
} else {
    echo '<' . $widgetData['data']['tag'] . ' style="display: block; ' . implode(';', $titleStyle) . '" class="' . implode(' ', $titleClass) . '">';
    echo $blockDetails["shortcode-text"];
    echo '</' . $widgetData['data']['tag'] . '>';
}
<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon' . ($blockDetails['is_html'] ? '_html' : '') . '.png'; ?>">
    <b class="visualWidget-title"><?= $blockDetails['title']; ?></b>
    <small class="visualWidget-descr"><?= (!$blockDetails['is_html'] ? 'Tag: <u>' . $widgetData['data']['tag'] . '</u>, ' : ''); ?>Allineamento: <u><?= $widgetData['data']['align']; ?></u></small>
</div>
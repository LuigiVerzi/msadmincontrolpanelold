<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon' . ($blockDetails['is_html'] ? '_html' : '') . '.png'; ?>">
    <b class="visualWidget-title"><?= $blockDetails['title']; ?></b>
    <small class="visualWidget-descr"><?= (isset($widgetData['data']['value']) ? 'Valore: <u>' . (float)$widgetData['data']['value'] . '</u>, ' : ''); ?>Valuta: <u><?= ($widgetData['data']['format']['currency'] ? $widgetData['data']['format']['currency'] : CURRENCY_SYMBOL); ?></u>, Tag: <u><?= $widgetData['data']['tag']; ?></u>, Allineamento: <u><?= $widgetData['data']['align']; ?></u></small>
</div>
<?php if(!isset($widgetDetails['shortcode-text'])) { ?>
    <div class="row">
        <div class="col-lg-12">
            <label>Valore</label>
            <input class="form-control widget-data" name="value" type="number" placeholder="Il valore del prezzo"  value="<?= htmlentities($widgetData['data']['value']); ?>">
        </div>
    </div>
    <h2 class="title-divider">Formato</h2>
<?php } ?>

<div class="widget-group" data-name="format">
    <div class="row">
        <div class="col-lg-3">
            <label>Valuta</label>
            <input class="form-control widget-data" name="currency" type="text" placeholder="<?= CURRENCY_SYMBOL; ?>"  value="<?= htmlentities($widgetData['data']['format']['currency']); ?>">
        </div>
        <div class="col-lg-3">
            <label>Posizione Valuta</label>
            <select class="form-control widget-data" name="currency_position" type="text">
                <option value="right">Destra</option>
                <option value="left" <?= ($widgetData['data']['format']['currency_position'] === 'left' ? 'selected' : ''); ?>>Sinistra</option>
            </select>
        </div>
        <div class="col-lg-3">
            <label>Separatore decimali</label>
            <input class="form-control widget-data" name="dec_point" type="text" placeholder="," value="<?= htmlentities($widgetData['data']['format']['dec_point']); ?>">
        </div>
        <div class="col-lg-3">
            <label>Separatore migliaia</label>
            <input class="form-control widget-data" name="thousand_sep" type="text" placeholder="." value="<?= htmlentities($widgetData['data']['format']['thousand_sep']); ?>">
        </div>
        <div class="col-lg-12">
            <label>Stringa prezzo mancante</label>
            <input class="form-control widget-data" name="no_price_string" type="text" placeholder="" value="<?= htmlentities($widgetData['data']['format']['no_price_string']); ?>">
        </div>
    </div>
</div>

<h2 class="title-divider">Elemento</h2>

<div class="row">
    <div class="col-lg-4">
        <label>Tag elemento</label>
        <select class="form-control widget-data" name="tag">
            <?php foreach(array('h1', 'h2', 'h3', 'h4', 'h5', 'p', 'b', 'u') as $tag) { ?>
                <option value="<?= $tag; ?>" <?= ($widgetData['data']['tag'] == $tag ? 'selected' : ''); ?>><?= $tag; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-lg-4">
        <label>Posizione elemento</label>
        <select class="form-control widget-data" name="align">
            <?php foreach(array('left' => 'Sinistra', 'center' => 'Centro', 'right' => 'Destra') as $posVal => $posName) { ?>
                <option value="<?= $posVal; ?>" <?= ($widgetData['data']['align'] == $posVal ? 'selected' : ''); ?>><?= $posName; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-lg-4">
        <label>Colore testo</label>
        <select class="form-control widget-data" name="color">
            <optgroup label="Stili predefiniti">
                <option value="">Usa colori tema</option>
                <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                    <option class="ms-bg-<?= $btn_style; ?>" value="ms-text-<?= $btn_style; ?>" <?= ($widgetData['data']['color'] == 'ms-text-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
                <?php } ?>
            </optgroup>
            <optgroup label="Personalizzato">
                <option value="ms-text-custom" <?= ($widgetData['data']['color'] == 'ms-text-custom' ? 'selected' : ''); ?>>Personalizzato</option>
            </optgroup>
        </select>
    </div>

    <div class="col-lg-6 btn-colors-settings" style="display: <?= ($widgetData['data']['color'] == 'ms-text-custom' ? 'block' : 'none'); ?>;">
        <label>Colore personalizzato</label>
        <div class="input-group colorpicker-component">
            <input class="form-control widget-data" name="custom-color" type="text" value="<?= ($widgetData['data']['custom-color']); ?>">
            <span class="input-group-addon"><i></i></span>
        </div>
    </div>
</div>

<h2 class="title-divider">Prezzo scontato</h2>
<div class="widget-group" data-name="sale">

    <?php if(!isset($widgetDetails['shortcode-alternative-text'])) { ?>
        <div class="row">
            <div class="col-lg-12">
                <label>Valore</label>
                <input class="form-control widget-data" name="value" type="number" placeholder="Il valore del prezzo scontato"  value="<?= htmlentities($widgetData['data']['sale']['value']); ?>">
            </div>
        </div>
    <?php } ?>

    <div class="alert alert-info">
        Se viene passato un prezzo scontato lo stile del prezzo verrà sostiutito con i parametri impostati sotto.
    </div>

    <div class="row">
        <div class="col-lg-4">

            <div class="widget-group ibox" data-name="old">
                <div class="ibox-title">
                    <h5>Vecchio prezzo</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Tag elemento</label>
                            <select class="form-control widget-data" name="tag">
                                <?php foreach(array('del', 'h1', 'h2', 'h3', 'h4', 'h5', 'p', 'b', 'u') as $tag) { ?>
                                    <option value="<?= $tag; ?>" <?= ($widgetData['data']['sale']['old']['tag'] == $tag ? 'selected' : ''); ?>><?= $tag; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <label>Classe personalizzata</label>
                            <input class="form-control widget-data" name="class" type="text" value="<?= ($widgetData['data']['sale']['old']['class']); ?>">
                        </div>

                        <div class="col-lg-6">
                            <label>Colore testo</label>
                            <select class="form-control widget-data" name="color">
                                <optgroup label="Stili predefiniti">
                                    <option value="">Usa colori tema</option>
                                    <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                                        <option class="ms-bg-<?= $btn_style; ?>" value="ms-text-<?= $btn_style; ?>" <?= ($widgetData['data']['sale']['old']['color'] == 'ms-text-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
                                    <?php } ?>
                                </optgroup>
                                <optgroup label="Personalizzato">
                                    <option value="ms-text-custom" <?= ($widgetData['data']['sale']['old']['color'] == 'ms-text-custom' ? 'selected' : ''); ?>>Personalizzato</option>
                                </optgroup>
                            </select>
                        </div>

                        <div class="col-lg-6 btn-colors-settings" style="display: <?= ($widgetData['data']['sale']['old']['color'] == 'ms-text-custom' ? 'block' : 'none'); ?>;">
                            <label>Colore personalizzato</label>
                            <div class="input-group colorpicker-component">
                                <input class="form-control widget-data" name="custom-color" type="text" value="<?= ($widgetData['data']['sale']['old']['custom-color']); ?>">
                                <span class="input-group-addon"><i></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Divisore</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <label>Diviso da</label>
                            <input class="form-control widget-data" name="divider" type="text" placeholder=" "  value="<?= htmlentities($widgetData['data']['sale']['divider']); ?>">
                        </div>
                        <div class="col-lg-12">
                            <label>Prezzo scontato a </label>
                            <select class="form-control widget-data" name="align">
                                <?php foreach(array('right' => 'Destra', 'left' => 'Sinistra') as $posVal => $posName) { ?>
                                    <option value="<?= $posVal; ?>" <?= ($widgetData['data']['sale']['align'] == $posVal ? 'selected' : ''); ?>><?= $posName; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="widget-group ibox" data-name="new">
                <div class="ibox-title">
                    <h5>Prezzo scontato</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">

                        <div class="col-lg-6">
                            <label>Tag nuovo prezzo</label>
                            <select class="form-control widget-data" name="tag">
                                <?php foreach(array('h1', 'h2', 'h3', 'h4', 'h5', 'p', 'b') as $tag) { ?>
                                    <option value="<?= $tag; ?>" <?= ($widgetData['data']['sale']['new']['tag'] == $tag ? 'selected' : ''); ?>><?= $tag; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <label>Classe personalizzata</label>
                            <input class="form-control widget-data" name="class" type="text" value="<?= ($widgetData['data']['sale']['new']['class']); ?>">
                        </div>

                        <div class="col-lg-6">
                            <label>Colore testo</label>
                            <select class="form-control widget-data" name="color">
                                <optgroup label="Stili predefiniti">
                                    <option value="">Usa colori tema</option>
                                    <?php foreach(array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark') as $btn_style) { ?>
                                        <option class="ms-bg-<?= $btn_style; ?>" value="ms-text-<?= $btn_style; ?>" <?= ($widgetData['data']['sale']['new']['color'] == 'ms-text-' . $btn_style ? 'selected' : ''); ?>><?= ucfirst($btn_style); ?></option>
                                    <?php } ?>
                                </optgroup>
                                <optgroup label="Personalizzato">
                                    <option value="ms-text-custom" <?= ($widgetData['data']['sale']['new']['color'] == 'ms-text-custom' ? 'selected' : ''); ?>>Personalizzato</option>
                                </optgroup>
                            </select>
                        </div>

                        <div class="col-lg-6 btn-colors-settings" style="display: <?= ($widgetData['data']['sale']['new']['color'] == 'ms-text-custom' ? 'block' : 'none'); ?>;">
                            <label>Colore personalizzato</label>
                            <div class="input-group colorpicker-component">
                                <input class="form-control widget-data" name="custom-color" type="text" value="<?= ($widgetData['data']['sale']['new']['custom-color']); ?>">
                                <span class="input-group-addon"><i></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('[name="color"]').on('change', function () {
            if($(this).val() === 'ms-text-custom') {
                $(this).closest('.widget-group').find('.btn-colors-settings').first().show();
            } else {
                $(this).closest('.widget-group').find('.btn-colors-settings').first().hide().find('widget-data').val('');
            }
        });
    });
</script>
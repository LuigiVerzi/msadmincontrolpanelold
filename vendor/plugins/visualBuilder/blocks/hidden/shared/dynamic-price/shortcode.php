<?php

if(!function_exists('generatePriceElement')) {
    function generatePriceElement($value, $params, $format, $type = 'normal') {
        $titleStyle = array();
        $titleClass = array();
        if (!empty($params['align'])) $titleStyle[] = 'text-align: ' . $params['align'];

        if ($params['color'] === 'ms-text-custom') {
            $titleStyle[] = 'color: ' . $params['custom-color'];
            $titleClass[] = 'ms-text-custom';
        } else if ($params['color'] !== '') {
            $titleClass[] = $params['color'];
        }

        if($params['class']) {
            $titleClass[] = $params['class'];
        }

        $dec_point = ($format['dec_point'] ? $format['dec_point'] : ',');
        $thousand_sep = ($format['thousand_sep'] ? $format['thousand_sep'] : '.');
        $currency = ($format['currency'] ? $format['currency'] : CURRENCY_SYMBOL);
        $no_price_string = ($format['no_price_string'] ? $format['no_price_string'] : '');

        $price_string = '<span data-MSFunction="PriceFormat" data-ms_price_type="' . $type . '" data-dec_point="' . $dec_point . '" data-thousand_sep="' . $thousand_sep . '" data-no_price_string="' . $no_price_string . '">' . $value . '</span>';

        if ($format['currency_position'] == 'left') {
            $price_string = '<span class="currency-symbol">' . $currency . '</span>' . $price_string;
        } else {
            $price_string .= '<span class="currency-symbol">' . $currency . '</span>';
        }

        $return = '<' . $params['tag'] . ' style="' . implode(';', $titleStyle) . '" class="ms-price-element ms-price-element-' . $type . ' ' . implode(' ', $titleClass) . '">';
        $return .= $price_string;
        $return .= '</' . $params['tag'] . '>';

        return $return;
    }
}

$value = (isset($blockDetails["shortcode-text"]) ? $blockDetails["shortcode-text"] : $widgetData['data']['value']);
$sale_value = (isset($blockDetails["shortcode-alternative-text"]) ? $blockDetails["shortcode-alternative-text"] : $widgetData['data']['sale']['value']);

echo generatePriceElement($value, $widgetData['data'], $widgetData['data']['format']);

echo '<div class="ms-price-sale-container" style="display: none;">';

$old_price = generatePriceElement($value, $widgetData['data']['sale']['old'], $widgetData['data']['format'], 'sale');
$new_price = generatePriceElement($sale_value, $widgetData['data']['sale']['new'], $widgetData['data']['format'], 'sale');

if($widgetData['data']['sale']['align'] === 'left') {
    echo $new_price . $widgetData['data']['sale']['divider'] . $old_price;
} else {
    echo $old_price . $widgetData['data']['sale']['divider'] . $new_price;
}

echo '</div>';

$MSFrameworkParser->addScript("jQuery('[data-MSFunction=\"PriceFormat\"]').each(function () {
    var element = $(this);
    var price_type = element.data('ms_price_type');   
    var value = MS_PriceFormat(element.text(), 2, element.data('dec_point'), element.data('thousands_sep'), element.data('no_price_string'));    
    if(isNaN(parseFloat(value)) || parseFloat(value) <= 0) {    
        if(price_type === 'normal') {
            element.closest('.ms-block-price').find('.ms-price-element-normal .currency-symbol').remove();
        } else {
            element.closest('.ms-block-price').find('.ms-price-sale-container').remove();
        }
    }
   if(element.length) element.replaceWith(value);
});

var haveSales = $('.ms-price-element-normal + .ms-price-sale-container').parent();
haveSales.find('.ms-price-element-normal').remove();
haveSales.find('.ms-price-sale-container').show();
");
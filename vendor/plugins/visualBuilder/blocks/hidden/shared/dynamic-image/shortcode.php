<?php
$image_styles = array();
foreach($widgetData['data']['styles'] as $style => $val) {
    if(!empty($val)) $image_styles[] = $style . ': ' . $val;
}
?>

<?php
if($widgetData['data']['onclick']['event']) {
    echo '<a style="' . implode(';', $image_styles) . '" ' . $this->getOnClickParams($widgetData['data']['onclick']) . '>';
}
?>
    <img style="<?= implode(';', $image_styles); ?>" src="<?= $blockDetails["shortcode-image"]; ?>">
<?php
if($widgetData['data']['onclick']['event']) {
    echo '</a>';
}
?>
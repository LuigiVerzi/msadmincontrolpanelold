<div class="row">
    <div class="col-lg-12">
        <label>Larghezza</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imposta la larghezza della riga e del relativo contenuto"></i></span>
        <select class="form-control widget-data" name="width">
            <option value="">Default</option>
            <option value="full-width" <?= ($widgetData['data']['width'] == 'full-width' ? 'selected' : ''); ?>>Allarga riga</option>
            <option value="full-width-and-content" <?= ($widgetData['data']['width'] == 'full-width-and-content' ? 'selected' : ''); ?>>Allarga riga (e contenuto)</option>
        </select>
    </div>
</div>
<?php
if($widgetData['data']['show-settings']['ids']) $show_type_descr = count(explode(',', $widgetData['data']['show-settings']['ids'])) . ' categorie';
else $show_type_descr = 'tutte le categorie';

if($widgetData['data']['view-mode']['mode'] === 'grid') {
    $show_mode_descr = 'una griglia';
}
else $show_mode_descr = 'un carosello';

?>

<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <b class="visualWidget-title">Categorie - Ecommerce</b>
    <small class="visualWidget-descr">Mostra <?= $show_mode_descr; ?> <u><?= $show_type_descr; ?></u></small>
</div>
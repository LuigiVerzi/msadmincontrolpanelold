<?php

$categoryClass = new \MSFramework\Ecommerce\categories();
$structureHtml = $this->drawVisualContent(json_decode($widgetData['structure']['data'], true), true);

$itemClasses = array();
if($widgetData['data']['view-mode']['mode'] === 'grid') {
    foreach ($widgetData['data']['view-mode']['grid'] as $classes) {
        if (!empty($classes)) $itemClasses[] = $classes;
    }
} else {
    $itemClasses[] = 'ms-carousel-item';
    $MSFrameworkParser->loadLibrary('owl-slider');
}

$ids_to_show = '';
if($widgetData['data']['show-settings']['mode'] === 'ids') {
    $ids_to_show = explode(',', $widgetData['data']['show-settings']['ids']);
}

if($widgetData['data']['view-mode']['mode'] === 'grid') {
    echo '<div class="ms-row">';
} else {
    echo '<div class="ms-carousel" data-carousel-settings=\'' . htmlentities(json_encode($widgetData['data']['view-mode']['carousel'])) . '\'>';
}

foreach($categoryClass->getcategoryDetails($ids_to_show) as $categoryDetail) {

    if($widgetData['data']['show-settings']['mode'] == 'parents') {
        if(!$categoryClass->isCategoryParent($categoryDetail['id'])) continue;
    }

    echo '<div class="' . implode(' ', $itemClasses) . '">';

    echo strtr($structureHtml, array(
        '{category-title}' => $MSFrameworki18n->getFieldValue($categoryDetail['nome']),
        '{category-description}' => $MSFrameworki18n->getFieldValue($categoryDetail['descr']),
        '{category-details}' => $MSFrameworki18n->getFieldValue($categoryDetail['html_descr']),
        '{category-image}' => $categoryDetail['gallery_friendly']['images'][0]['html']['main'],
        '{category-banner}' => $categoryDetail['gallery_friendly']['banner'][0]['html']['main'],
        '{relative-page-url}' => $categoryClass->getURL($categoryDetail),

        '{rand}' => uniqid() // Serve per far funzionare l'apertura delle pagine dentro i popup
    ));

    echo '</div>';

}
echo '</div>';
<div class="row">
    <div class="col-lg-12">
        <div class="widget-group" data-name="show-settings">
            <div class="row">
                <div class="col-lg-12">
                    <label>Categorie da mostrare</label>
                    <select class="form-control widget-data" name="mode">
                        <option value="parents">Categorie genitori</option>
                        <option value="all">Tutte</option>
                        <option value="ids" <?= ($widgetData['data']['show-settings']['mode'] == "ids" ? 'selected' : ''); ?>>Seleziona manualmente</option>
                    </select>
                </div>
                <div class="col-lg-12" data-show-mode="ids">
                    <label>Seleziona Categorie</label>
                    <select class="form-control widget-data" name="ids" multiple>
                        <?php foreach((new \MSFramework\Ecommerce\categories())->getCategoryDetails() as $category_id => $category_info) { ?>
                            <option value="<?= $category_id; ?>" <?= (in_array($category_id, $widgetData['data']['show-settings']['ids']) ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($category_info['nome']); ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    visualBuilderPopupJS.push(function () {
        $('[data-name="show-settings"] [name="mode"]').on('change', function () {
            if($(this).val() === 'ids') $('[data-show-mode="ids"]').show();
            else $('[data-show-mode="ids"]').hide();
        }).change();
    });
</script>
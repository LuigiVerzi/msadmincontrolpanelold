<?php
if($blockDetails['products-list']) {
    $show_type_descr = 'dei prodotti relativi';
} else {
    if ($widgetData['data']['show-settings']['category']) {
        $show_type_descr = 'della categoria ' . $MSFrameworki18n->getFieldValue((new \MSFramework\Ecommerce\categories())->getCategoryDetails($widgetData['data']['show-settings']['category'])[$widgetData['data']['show-settings']['category']]['nome']);
    } else $show_type_descr = 'di tutti i prodotti';
}

if ($widgetData['data']['view-mode']['mode'] === 'grid') {
    $show_mode_descr = 'una griglia';
} else $show_mode_descr = 'un carosello';

?>

<div class="visualWidget-intro">
    <img class="visualWidget-icon" src="<?= $blockDetails['url'] . '/icon.png'; ?>">
    <b class="visualWidget-title">Prodotti - Ecommerce</b>
    <small class="visualWidget-descr">Mostra <?= $show_mode_descr; ?> <u><?= $show_type_descr; ?></u></small>
</div>
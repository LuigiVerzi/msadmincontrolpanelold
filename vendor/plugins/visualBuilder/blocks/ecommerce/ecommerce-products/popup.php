<?php if(!$widgetDetails['products-list']) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="widget-group" data-name="show-settings">
                <div class="row">
                    <div class="col-lg-12">
                        <label>Prodotti da mostrare</label>
                        <select class="form-control widget-data" name="mode">
                            <option value="all">Tutti</option>
                            <option value="category" <?= ($widgetData['data']['show-settings']['mode'] == "category" ? 'selected' : ''); ?>>Seleziona categoria</option>
                        </select>
                    </div>
                    <div class="col-lg-12" data-show-mode="category">
                        <label>Seleziona Categorie</label>
                        <select class="form-control widget-data" name="category">
                            <?php foreach((new \MSFramework\Ecommerce\categories())->getCategoryDetails() as $category_id => $category_info) { ?>
                                <option value="<?= $category_id; ?>" <?= ($widgetData['data']['show-settings']['category'] == $category_id ? 'selected' : ''); ?>><?= $MSFrameworki18n->getFieldValue($category_info['nome']); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h2 class="title-divider">Ordina</h2>
    <div class="row">
        <div class="col-lg-12">
            <div class="widget-group" data-name="sort">
                <div class="row">
                    <div class="col-lg-4">
                        <label>Ordina per</label>
                        <select class="form-control widget-data" name="mode">
                            <?php foreach(
                                array(
                                    'date' => 'Data',
                                    'price' => 'Prezzo',
                                    'stock' => 'Quantità',
                                    'popularity' => 'Popolarità',
                                ) as $k => $v
                            ) { ?>
                                <option value="<?= $k; ?>" <?= ($widgetData['data']['sort']['mode'] == $k ? 'selected' : ''); ?>><?= $v; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label>Ordine</label>
                        <select class="form-control widget-data" name="order">
                            <option value="asc">Ascendente</option>
                            <option value="desc" <?= ($widgetData['data']['sort']['order'] == "desc" ? 'selected' : ''); ?>>Descendente</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label>Limite</label>
                        <input class="form-control widget-data" type="number" name="limit" value="<?= $widgetData['data']['sort']['limit']; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        visualBuilderPopupJS.push(function () {
            $('[data-name="show-settings"] [name="mode"]').on('change', function () {
                if($(this).val() === 'category') $('[data-show-mode="category"]').show();
                else $('[data-show-mode="category"]').hide().find('select').val('');
            }).change();
        });
    </script>
<?php } ?>
<?php

$productClass = new \MSFramework\Ecommerce\products();
$structureHtml = $this->drawVisualContent(json_decode($widgetData['structure']['data'], true), true);

$itemClasses = array();
if($widgetData['data']['view-mode']['mode'] === 'grid') {
    foreach ($widgetData['data']['view-mode']['grid'] as $classes) {
        if (!empty($classes)) $itemClasses[] = $classes;
    }
} else {
    $itemClasses[] = 'ms-carousel-item';
    $MSFrameworkParser->loadLibrary('owl-slider');
}

$category_to_show = '';
if(!$blockDetails['products-list']) {
    if ($widgetData['data']['show-settings']['mode'] === 'category') {
        $category_to_show = $widgetData['data']['show-settings']['category'];
    }
}

if($widgetData['data']['view-mode']['mode'] === 'grid') {
    echo '<div class="ms-row">';
} else {
    echo '<div class="ms-carousel" data-carousel-settings=\'' . htmlentities(json_encode($widgetData['data']['view-mode']['carousel'])) . '\'>';
}

$currentProducts = array();
if($blockDetails['products-list']) {
    $currentProducts = $blockDetails['products-list'];
} else {
    $currentProducts = $productClass->getProductsList(array(
        'category' => (int)$category_to_show,
        'limit' => (int)$widgetData['data']['sort']['limit'],
        'order' => array($widgetData['data']['sort']['mode'], strtoupper($widgetData['data']['sort']['order']))
    ));
}

foreach($currentProducts['products'] as $productDetail) {

    echo '<div class="' . implode(' ', $itemClasses) . '">';

    echo strtr($structureHtml, array(
        '{product-title}' => $MSFrameworki18n->getFieldValue($productDetail['nome']),
        '{product-description}' => $MSFrameworki18n->getFieldValue($productDetail['short_descr']),
        '{product-details}' => $MSFrameworki18n->getFieldValue($productDetail['long_descr']),
        '{product-price}' => (float)implode('.', $productDetail['prezzo']['shop_price']),
        '{product-offer-price}' => (float)implode('.', $productDetail['prezzo_promo']['shop_price']),
        '{product-image}' => $productDetail['gallery_friendly'][0]['html']['main'],
        '{relative-page-url}' => $productClass->getURL($productDetail),

        '{rand}' => uniqid() // Serve per far funzionare l'apertura delle pagine dentro i popup
    ));

    echo '</div>';

}
echo '</div>';
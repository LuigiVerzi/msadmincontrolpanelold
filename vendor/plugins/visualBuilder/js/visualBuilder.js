$.fn.visualBuilder = function(options) {

    var element = this;
    var $visualContainer = false;

    window.visualBuilderPopupJS = false;

    this.$textArea = $(this);

    element.$textArea.hide();

    this.settings = $.extend({
        pluginURL: $('#baseElementPathAdmin').val() + 'vendor/plugins/visualBuilder/',
        endPoint: $('#baseElementPathAdmin').val() + 'vendor/plugins/visualBuilder/ajax/visualBuilder.php',
        defaultColSize: 12,
        addWidgetBtn: '<a href="#" class="visualBuilderAddWidget_Btn"><i class="fa fa-plus"></i><span>Aggiungi contenuto</span></a>',
        popupLoadingString: '<div class="visualBuilderPopupLoading"><div></div></div>',
        exclude_blocks: [],
        shortcodes: {},
        use_relative_page: false,
        structure_mode: 0,
        private_mode: 0,
        private_blocks: [],
        uploads: 'PAGEGALLERY',
        uploads_common: 0,
    }, options);

    this.currentSelection = {
        target: false,
        widgetName: ''
    };

    var templates = {
        row: '',
        col: ''
    };

    this.loadData = function (data, callback) {
        if(typeof(data) === 'undefined') data = {};
        if(typeof(callback) === 'undefined') callback = function (templateHtml) {};

        if(typeof(data) !== 'object') {
            data = data
                .replace('<!--VISUAL-BUILDER-START-->', '')
                .replace('<!--VISUAL-BUILDER-END-->', '');
            try {
                data = JSON.parse(data)
            } catch (e) {
                if (data.length) {
                    data = {tpl: 'simple_editor', content: data};
                }
            }
        }

        $.ajax({
            url: element.settings.endPoint,
            type: "POST",
            data: {
                "action": 'drawBuilder',
                "firstInit": ($visualContainer === false ? "1" : "0"),
                "settings": element.settings,
                "data": JSON.stringify(data)
            },
            async: true,
            dataType: "text",
            success: callback
        });

    };

    /* Lista funzioni */
    this.init = function () {

        var data = element.$textArea.val();

        if (element.$textArea.parent().find('.fa-language')) {
            element.$textArea.parent().find('.fa-language').hide();
            if($('#usingPrimaryLang').val() === "0") {
                try {
                    var visualBuilderPrimaryLang = element.$textArea.parent().find('.fa-language').data('translations');

                    if(visualBuilderPrimaryLang[$('#primaryLangCode').val()].length) {
                        element.$textArea.parent().find('.fa-language').after('<a href="#" class="btn btn-info btn-sm visualBuilderImportFromMainLang">Importa da ' + $('#primaryLangFullName').val() + '</a>');

                        $('.visualBuilderImportFromMainLang').on('click', function (e) {
                            e.preventDefault();
                            bootbox.dialog({
                                message: "Vuoi importare la traduzione dalla lingua principale? Proseguendo perderai le eventuali modifiche effettuate.",
                                closeButton: false,
                                buttons: {
                                    cancel: {
                                        label: "Annulla",
                                        className: 'btn-danger'
                                    },
                                    ok: {
                                        label: "Importa traduzione",
                                        className: 'btn-info',
                                        callback: function () {
                                            element.loadData(visualBuilderPrimaryLang[$('#primaryLangCode').val()],function (templateHtml) {
                                                $visualContainer.find('.welcomeBox').hide();
                                                $visualContainer.find('> .visualBuilderContent').html(templateHtml).show();
                                                element.onContentDraw();
                                            });
                                        }
                                    }
                                }
                            });
                        });
                    }
                } catch(e) {}
            }
        }

        this.loadData(data, function (templateHtml) {
            element.$textArea.hide().before(templateHtml);
            $visualContainer = element.$textArea.parent().find('> .visualBuilderContainer');
            element.onDraw();

            $('.row-data').each(function () {
               try {
                   var data = JSON.parse($(this).html());
                   var $previewElement = $(this).closest('.visualBuilderRow').find('.visualBuilderDesignPreview.rowPreview');
                   element.composeDesignPreview($previewElement, data);
               } catch (e) {}
            });

            $('.col-data').each(function () {
               try {
                   var data = JSON.parse($(this).html());
                   var $previewElement = $(this).closest('.visualBuilderColumn').find('.visualBuilderDesignPreview.columnPreview');
                   element.composeDesignPreview($previewElement, data);
               } catch (e) {}
            });

            $('.widget-data').each(function () {
               try {
                   var data = JSON.parse($(this).html());
                   var $previewElement = $(this).closest('.visualBuilderWidgetContent').find('.visualBuilderDesignPreview.actionPreview');
                   element.composeDesignPreview($previewElement, data);
               } catch (e) {}
            });

        })
    };

    this.destroy = function () {
        $visualContainer.find('> .visualBuilderBlockEditModal').find('*').off();
        $visualContainer.off().find('*').off();
        $visualContainer.remove();
        element.$textArea.show();

    };

    this.onDraw = function () {
        templates.row =  $visualContainer.find('.visualBuilderTemplates.emptyRow').val();
        templates.col =  $visualContainer.find('.visualBuilderTemplates.emptyColumn').val();

        $visualContainer.find('> .visualBuilderContent').sortable({
            handle: ".visualBuilderControlBtn[data-action=\"move\"]",
            appendTo: 'body',
            helper: function(event, ui) {
                return "<div class='visualBuilderDraggingItem visualBuilderDraggingRow'><img class='visualBuilderDraggingIcon' src='" + element.settings.pluginURL + "images/row_icon.png'> <b class='visualBuilderDraggingTitle'>Riga</b></div>";
            },
            start: function( event, ui ) {
                $visualContainer.addClass('sorting');
            },
            stop: function( event, ui ) {
                $visualContainer.removeClass('sorting');
                element.onContentDraw();
            },
            update: function( event, ui ) {
                element.onContentDraw(false);
            },
            out: function( event, ui ) {
                element.onContentDraw(false);
            },
            over: function( event, ui ) {
                element.onContentDraw(false);
            }
        });

        element.onContentDraw();

        this.initEvents();
    };

    this.initEvents = function () {

        /* Eventi pulsanti di benvenuto */
        $visualContainer.find('> .welcomeBox').on('click', '.visualBuilder_addRowBtn', function (e) {
            e.preventDefault();

            element.insertRow();
        });

        $visualContainer.find('> .welcomeBox').on('click', '.visualBuilder_addEditorBtn', function (e) {
            e.preventDefault();

            element.loadData({tpl: 'simple_editor'}, function (templateHtml) {
                $visualContainer.find('> .welcomeBox').hide();
                $visualContainer.find('> .visualBuilderContent').html(templateHtml).show();
                element.onContentDraw();
            });
        });

        $visualContainer.find('> .welcomeBox').on('click', '.visualBuilder_loadTemplateBtn', function (e) {
            e.preventDefault();
        });

        /* Eventi pulsanti riga */
        $visualContainer.find('> .visualBuilderContent').on('click', ' .visualBuilderAddNewRow', function (e) {
            e.preventDefault();
            element.insertRow();
        });

        $visualContainer.find('> .visualBuilderContent').on('click', ' .visualBuilderControlBtn', function (e) {
            e.preventDefault();

            var $row = $(this).closest('.visualBuilderRow');

            var action = $(this).data('action');

            if(action === 'delete') {
                if($row.find('.visualBuilderColumn').not('.empty').length) {
                    if (!confirm("Sei sicuro di voler eliminare la riga?")) return false;
                }
                $row.remove();
                if(!$visualContainer.find('.visualBuilderRow').length) {
                    $visualContainer.find('> .visualBuilderContent').hide();
                    $visualContainer.find('.welcomeBox').show();
                }
            } else if(action === 'duplicate') {
                var $clonedRow = $row.clone();
                $row.after($clonedRow);
            } else if(action === 'add') {
                element.insertCol($row, 12);
            } else if(action === 'edit') {
                element.currentSelection.target = $row;
                element.addWidget();
            }

            element.onContentDraw();
        });

        $visualContainer.find('> .visualBuilderContent').on('click', ' .visualBuilderControlBtn .visualBuilderSizesList > span', function (e) {
            e.preventDefault();
            var $row = $(this).closest('.visualBuilderRow');

            var size = $(this).data('size');
            var sizes = (isNaN(size) ? size.split(';') : [size]);

            element.setColumns($row, sizes);
        });

        /* Eventi pulsanti colonna */
        $visualContainer.find('> .visualBuilderContent').on('click', ' .visualBuilderColumnActionBtn', function (e) {
            e.preventDefault();

            var $col = $(this).closest('.visualBuilderColumn');
            var action = $(this).data('action');

            if(action === 'delete') {
                if($col.find('.visualBuilderWidgetContent[data-name]').length) {
                    if (!confirm("Sei sicuro di voler eliminare la colonna?")) return false;
                }
                var $row = $col.closest('.visualBuilderRow');
                $col.remove();
                if(!$row.find('.visualBuilderColumn').length) {
                    element.insertCol($row, 12);
                }
            } else if(action === 'duplicate') {
                var $clonedCol = $col.clone();
                $col.after($clonedCol);
            } else if(action === 'add') {
                element.currentSelection.target = $col.find('.visualBuilderColumnContent');
                element.currentSelection.target_position = 'top';
                element.openWidgetPopup();
            } else if(action === 'edit') {
                element.currentSelection.target = $col;
                element.addWidget();
            }

            element.onContentDraw();
        });

        /* Eventi creazione contenuti */
        $visualContainer.find('> .visualBuilderContent').on('click', '.visualBuilderAddWidget_Btn', function (e) {
            e.preventDefault();

            element.currentSelection.target = $(this).closest('.visualBuilderColumn').find('.visualBuilderColumnContent');
            element.currentSelection.target_position = 'bottom';
            element.openWidgetPopup();
        });

        $visualContainer.find('> .visualBuilderBlocksModal').on('click', '.addPopupWidget_Btn', function (e) {
            e.preventDefault();
            var widget_id = $(this).data('id');
            element.currentSelection.widgetName = $(this).data('name');

            element.addWidget(widget_id);
        });

        $visualContainer.find('> .visualBuilderBlockEditModal ').on('click', '.visualBuilderBlockEdit_SaveBtn', function (e) {
            e.preventDefault();
            element.saveWidgetData();
        });

        /* Eventi azioni */
        $visualContainer.find('> .visualBuilderContent').on('click', ' .visualBuilderWidgetContentAction_Btn', function (e) {
            e.preventDefault();

            var $widget = $(this).closest('.visualBuilderWidgetContent');
            var action = $(this).data('action');

            if(action === 'delete') {

                if(!confirm("Sei sicuro di voler eliminare l'elemento '" + $widget.data('name') + "'?")) return false;

                $widget.remove();
            } else if(action === 'duplicate') {
                var $clonedWidget = $widget.clone();
                $widget.after($clonedWidget);
            } else if(action === 'edit') {
                element.currentSelection.widgetName = $widget.data('name');
                element.currentSelection.target = $widget;
                element.addWidget();
            }

            element.onContentDraw();
        });

        $visualContainer.on('click', '> .modal [data-dismiss="modal"]', function(e) {
            e.stopImmediatePropagation();

            $(this).closest('.modal').modal('hide');

        });

        $(document).on('show.bs.modal', function (e) {
            if($(e.target).parent().closest('.modal').length) {
               // $(e.target).parent().closest('.modal').css('opacity', 0);
            }
        });

        $(document).on('hidden.bs.modal', function (e) {
            if(!$('.modal:visible').length) {
                $('.modal-backdrop').hide();
            }
        });
    };

    /* Gestione contenuti */
    this.insertRow = function () {

        if(!$visualContainer.find('> .visualBuilderContent').is(':visible')) {
            $visualContainer.find('> .welcomeBox').hide();
            $visualContainer.find('> .visualBuilderContent').show();
        }

        $visualContainer.find('> .visualBuilderContent').append(
            templates.row
                .replace('[row-data]','{}')
                .replace('[row-content]',
                    templates.col
                        .replace('[col-size]', element.settings.defaultColSize)
                        .replace('[col-content]', '')
                        .replace('[col-data]', '{}')
                        .replace('[col-footer-content]', element.settings.addWidgetBtn)
                )
        );

        element.onContentDraw();

    };

    this.insertCol = function ($row, size) {
        if(typeof(size) === 'undefined') size = 12;

        $row.find('.visualBuilderRowContent').append(
            templates.col
                .replace('[col-size]', size)
                .replace('[col-content]', '')
                .replace('[col-data]', '{}')
                .replace('[col-footer-content]', element.settings.addWidgetBtn)
        );

        element.onContentDraw();
    };

    this.setColumns = function ($row, sizes) {

        var html = '';

        var content_to_move = [];
        var columns_datas = [];

        var index = 0;
        $row.find('.visualBuilderColumnContent').each(function () {
            index++;
            content_to_move[index] = [];
            columns_datas[index] = ($row.find('> .col-data').length ? $row.find('> .col-data').html() : '{}');

            if($(this).find('.visualBuilderWidgetContent').length) {
                if (index >= sizes.length) {
                    content_to_move[sizes.length].push($.trim($(this).html()));
                } else {
                    content_to_move[index].push($.trim($(this).html()));
                }
            }
        });

        index = 0;
        sizes.forEach(function (size) {
            index++;
            html += templates.col
                .replace('[col-size]', size)
                .replace('[col-data]', typeof(columns_datas[index]) !== 'undefined' ? columns_datas[index] : '{}')
                .replace('[col-content]', (typeof(content_to_move[index]) !== 'undefined' ? content_to_move[index].join('') : ''))
                .replace('[col-footer-content]', element.settings.addWidgetBtn)
        });

        $row.find('.visualBuilderRowContent').html(html);

        element.onContentDraw();
    };

    this.onContentDraw = function (save) {
        if(typeof(save) === 'undefined') {
            save = true;
        }

        $visualContainer.find('> .visualBuilderContent .visualBuilderColumn').each(function () {
            if($(this).find('.visualBuilderWidgetContent').length) {
                $(this).removeClass('empty');
            } else {
                $(this).addClass('empty');
            }
        });

        $visualContainer.find('> .visualBuilderContent .visualBuilderColumn').each(function () {
            if($(this).hasClass('empty') && !$(this).find('.visualBuilderWidgetContent').length  && !$(this).find('.visualBuilderColumnContent .visualBuilderAddWidget_Btn').length) {
                $(this).find('.visualBuilderColumnContent').html(element.settings.addWidgetBtn);
            } else if(!$(this).hasClass('empty') && $(this).find('.visualBuilderColumnContent .visualBuilderAddWidget_Btn').length) {
                $(this).find('.visualBuilderColumnContent .visualBuilderAddWidget_Btn').remove();
            }
        });

        $visualContainer.find('> .visualBuilderContent .visualBuilderRowContent').each(function () {
            if(!$(this).find('.visualBuilderColumn').length) {
                element.insertCol($(this).closest('.visualBuilderRow'), 12);
            }
        });

        $visualContainer.find('> .visualBuilderContent .visualBuilderWidgetContent').each(function () {
            if($(this).outerHeight() === 250) {
                $(this).addClass('too_large');
            } else if($(this).hasClass('too_large')) {
                $(this).removeClass('too_large');
            }
        });


        /* Rende gli widget trascinabili */
        if($visualContainer.find('> .visualBuilderContent .visualBuilderColumnContent').not(':ui-sortable').length) {
            $visualContainer.find('> .visualBuilderContent .visualBuilderColumnContent:ui-sortable').sortable('destroy');

            $visualContainer.find('.visualBuilderColumnContent').sortable({
                connectWith: ".visualBuilderColumnContent",
                cursorAt: { left: 15, top: 15 },
                appendTo: 'body',
                items : '.visualBuilderWidgetContent',
                helper: function(event, ui){
                    var icon = ui.data('icon');
                    var label = ui.data('name');
                    return "<div class='visualBuilderDraggingItem'><img class='visualBuilderDraggingIcon' src='" + icon + "'> <b class='visualBuilderDraggingTitle'>" + label + "</b></div>";
                },
                start: function( event, ui ) {
                    $visualContainer.addClass('sorting');
                },
                stop: function( event, ui ) {
                    $visualContainer.removeClass('sorting');
                    element.onContentDraw();
                },
                update: function( event, ui ) {
                    element.onContentDraw(false);
                },
                out: function( event, ui ) {
                    element.onContentDraw(false);
                },
                over: function( event, ui ) {
                    element.onContentDraw(false);
                }
            });
        }

        /* Rendo le colonne trascinabili */
        if($visualContainer.find('> .visualBuilderContent .visualBuilderRowContent').not(':ui-sortable').length) {
            $visualContainer.find('> .visualBuilderContent .visualBuilderRowContent:ui-sortable').sortable('destroy');

            $visualContainer.find('> .visualBuilderContent .visualBuilderRowContent').sortable({
                connectWith: ".visualBuilderRowContent",
                cursorAt: { left: 15, top: 15 },
                appendTo: 'body',
                items : '.visualBuilderColumn',
                helper: function(event, ui){
                    return "<div class='visualBuilderDraggingItem visualBuilderDraggingCol'><img class='visualBuilderDraggingIcon' src='" + element.settings.pluginURL + "images/col_icon.png'> <b class='visualBuilderDraggingTitle'>Colonna</b></div>";
                },
                start: function( event, ui ) {
                    ui.placeholder.css({
                        'width': '',
                        'height': $(ui.item).height(),
                    }).attr('data-col', $(ui.item).data('col')).addClass('visualBuilderColumn');
                    $visualContainer.addClass('sorting');
                },
                stop: function( event, ui ) {
                    $visualContainer.removeClass('sorting');
                    element.onContentDraw();
                },
                update: function( event, ui ) {
                    element.onContentDraw(false);
                },
                out: function( event, ui ) {
                    element.onContentDraw(false);
                },
                over: function( event, ui ) {
                    element.onContentDraw(false);
                }
            });
        }

        /* Rendo le colonne ridimensionabili */
        if($visualContainer.find('> .visualBuilderContent .visualBuilderColumn').not(':ui-resizable').length) {
            $visualContainer.find('> .visualBuilderContent .visualBuilderColumn:ui-resizable').resizable('destroy');

            $visualContainer.find('> .visualBuilderContent .visualBuilderColumn').each(function () {
                $(this).resizable({
                    grid: $visualContainer.find('> .visualBuilderContent .visualBuilderRowContent').width()/12,
                    handles: {
                        e: $(this).find('.visualBuilderColumnResizeHandler')
                    },
                    resize: function( event, ui ) {
                        ui.size.height = 'auto';
                        var colSize = Math.round(12/(ui.element.parent().width()/ui.size.width));
                        if(colSize > 12) colSize = 12;
                        else if(colSize < 1) colSize = 1;
                        ui.element.css('width', '').css('height', '').attr('data-col', colSize).data('col', colSize);
                        element.onContentDraw();
                    }
                });
            });
        }

        $visualContainer.find('> .visualBuilderBlockEditModal').find('*').off();
        $visualContainer.find('.visualBuilderContainer').off().find('*').off();

        if(save) {
            element.saveData();
        }
    };

    this.openWidgetPopup = function () {
        $visualContainer.find('> .visualBuilderBlocksModal').modal('show');
    };

    this.addWidget = function (widget) {

        visualBuilderPopupJS = [];

        $visualContainer.find('> .visualBuilderBlocksModal').modal('hide');

        $visualContainer.find('> .visualBuilderBlockEditModal .modal-title .widgetName').html(element.currentSelection.widgetName);
        $visualContainer.find('> .visualBuilderBlockEditModal .modal-title .widgetAction').html('Aggiungi');

        $visualContainer.find('> .visualBuilderBlockEditModal .modal-body').html(element.settings.popupLoadingString);
        $visualContainer.find('> .visualBuilderBlockEditModal').modal('show');

        var data = {};

        if(element.currentSelection.target.hasClass('visualBuilderWidgetContent')) {
            data = JSON.parse(element.currentSelection.target.find('.widget-data').html());
            widget = data.data.type;
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-title .widgetAction').html('Modifica');
        } else if(element.currentSelection.target.hasClass('visualBuilderRow')) {
            data = (element.currentSelection.target.find('.row-data').length ? JSON.parse(element.currentSelection.target.find('.row-data').html()) : {});
            widget = 'row';
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-title .widgetName').html('');
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-title .widgetAction').html('Modifica riga');
        } else if(element.currentSelection.target.hasClass('visualBuilderColumn')) {
            data = (element.currentSelection.target.find('.col-data').length ? JSON.parse(element.currentSelection.target.find('.col-data').html()) : {});
            widget = 'col';
            if(typeof(data.responsive) === 'undefined') data.responsive = {desktop: {}, tablet: {}, mobile: {}};
            data.responsive.desktop.col = element.currentSelection.target.data('col');
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-title .widgetName').html('');
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-title .widgetAction').html('Modifica colonna');
        }

        $.ajax({
            url: element.settings.endPoint,
            type: "POST",
            data: {
                "action": 'getPopupContent',
                "popup": widget,
                "data": data,
                "settings": element.settings
            },
            async: true,
            dataType: "text",
            success: function (popup_content) {
                $visualContainer.find('> .visualBuilderBlockEditModal .modal-body').html(popup_content);
                element.onWidgetPopupInit();
            }
        });
    };

    this.onWidgetPopupInit = function () {
        if($visualContainer.find('> .visualBuilderBlockEditModal .modal-body .tinymce').length) {
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .tinymce').each(function () {
                var tinymce_id = element.getUniqueID();
                $(this).attr('id', tinymce_id);

                var tinyMCEOptions = {};
                if(Object.keys(element.settings.shortcodes).length) {
                    tinyMCEOptions.shortcodes = {custom: Object.keys(element.settings.shortcodes)};
                }

                initTinyMCE($('#' + tinymce_id), tinyMCEOptions);
            });
        }

        $('select option[class*="ms-bg"]').closest('select').on('change', function () {
            if($(this).find('option[value="' + $(this).val() + '"]').length) {
                $(this).css('background', $(this).find('option[value="' + $(this).val() + '"]').css('background'));
            }
        }).change();

        if($visualContainer.find('> .visualBuilderBlockEditModal .modal-body .colorpicker-component').length) {
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .colorpicker-component').colorpicker().on('changeColor', function(event) {
                $(this).find('input').change();
            });
        }

        if($visualContainer.find('> .visualBuilderBlockEditModal .modal-body .colorpicker').length) {
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .colorpicker').colorpicker();
        }

        if($visualContainer.find('> .visualBuilderBlockEditModal .modal-body .datetimepicker').length) {
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .datetimepicker').datetimepicker({
                format:'dd/mm/yyyy hh:ii',
                startDate: '0d',
                minDate: '0d',
            });
        }

        if($visualContainer.find('> .visualBuilderBlockEditModal .modal-body .icon-picker').length) {
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .icon-picker').iconpicker();
        }

        if($visualContainer.find('> .visualBuilderBlockEditModal .modal-body select[multiple]').length) {
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-body select[multiple]').each(function () {
                var data = $(this).data();
                $(this).multiselect({
                    columns: (typeof (data.cols) !== 'undefined' ? data.cols : 3),
                    texts: {
                        placeholder: (typeof (data.placeholder) !== 'undefined' ? data.placeholder : 'Seleziona elementi')
                    }
                });
            });
        }

        //initMSTooltip();
        initIChecks();
        prepareFormLabels($visualContainer.find('> .visualBuilderBlockEditModal .modal-body'));

        /* Inizializzo la modifica del design */
        $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .designModeChange').on('change', function () {
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .design-group').hide();
            $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .design-group[data-name="' + $(this).val() + '"]').show();
        });

        $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .editInAdvancedMode').on('change', function () {
            var $widgetGroup =  $(this).closest('.editModeContainer');
            $widgetGroup.find('.editTypeBox').hide();
            if($(this).is(':checked')) {
                $widgetGroup.find('.editTypeBox[data-type="advanced"]').show().find('input').change();
                $widgetGroup.find('.editTypeBox[data-type="simple"]').val('');
            } else {
                $widgetGroup.find('.editTypeBox[data-type="simple"]').show().find('input').change();
                $widgetGroup.find('.editTypeBox[data-type="advanced"]').val('');
            }
        }).change();

        $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .updateBoxPreview').on('change', function () {
            if(!$(this)[0].hasAttribute("name")) {
                $(this).closest('.widget-group').find('.updateBoxPreview[name]').val($(this).val()).change();
            }
        });

        $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .updateBoxPreview').filter(function() {
            return this.value.length !== 0;
        }).change();

        $visualContainer.find('> .visualBuilderBlockEditModal .modal-body .border-radius-suffix').on('change', function () {
            $(this).closest('.ibox').find('.updateBoxPreview[name]').change();
        });

        $visualContainer.find('> .visualBuilderBlockEditModal .uploader').each(function () {
            var id = $(this).data('id');
            var limit = (typeof($(this).data('limit')) !== 'undefined' ? $(this).data('limit') : 1);
            initOrak(id, limit, 15, 15, 200, getOrakImagesToPreattach(id), false, ['image/jpeg', 'image/png']);
        });

        $visualContainer.find('> .visualBuilderBlockEditModal .size-input').on('change', function () {
            var value = $(this).val();
            value = value.replace(/([^[0-9(px)%])+/gm, '');

            if(value.length && value.indexOf('%') < 0 && value.indexOf('px') < 0) {
                value += 'px';
            }

            $(this).val(value);
        });

        visualBuilderPopupJS.forEach(function (fn) {
            if (typeof (fn) === 'function') {
                fn();
            }
        });

        if($visualContainer.find('> .visualBuilderBlockEditModal [data-name="onclick"]').length) {

            $visualContainer.find('> .visualBuilderBlockEditModal [data-name="onclick"] [name="event"]').on('change', function () {
                $visualContainer.find('> .visualBuilderBlockEditModal .onClickEventSettings').hide();
                $visualContainer.find('> .visualBuilderBlockEditModal .onClickEventSettings[data-name="' + $(this).val() + '"]').show();
            }).change();

            $('#creatNewPopup').on('click', function () {
                showFastEditor('contenuti_popup', '', function (result) {
                    if(result.id > 0) {
                        $('#id_popup').append('<option value="' + result.id + '">Popup creato alle ore ' + new Date().toLocaleTimeString('it-IT', {timeStyle: 'short'}) + '</a>').val(result.id).change();
                        $('#fastModuleEditor').modal('toggle');
                        toastr['success']('Popup creato correttamente');
                    }
                });
            });

            $('#id_popup').on('change', function (e) {
                if($(this).val() !== "" && $(this).val().indexOf('page') < 0 && $(this).val() !== "relative") {
                    $('#editCurrentPopup').css('display', 'inline-block');
                    $('[data-name="appearance"]').hide();
                } else {
                    $('#editCurrentPopup').hide();
                    $('[data-name="appearance"]').show();
                }
            });

            $('#editCurrentPopup').on('click', function () {
                showFastEditor('contenuti_popup', $('#id_popup').val(), function (result) {
                    $('#fastModuleEditor').modal('toggle');
                    toastr['success']('Popup aggiornato correttamente');
                });
            });

            $('#createNewPage').on('click', function () {
                showFastEditor('contenuti_pagine', '', function (result) {
                    if(result.id > 0) {
                        $('#id_page').append('<option value="' + result.id + '">Pagina creata alle ore ' + new Date().toLocaleTimeString('it-IT', {timeStyle: 'short'}) + '</a>').val(result.id).change();
                        $('#fastModuleEditor').modal('toggle');
                        toastr['success']('Pagina creata correttamente');
                    }
                });
            });

            $('#id_page').on('change', function (e) {
                if($(this).val() !== "" && $(this).val() !== "relative") {
                    $('#editCurrentPopup').css('display', 'inline-block');
                } else {
                    $('#editCurrentPopup').hide();
                }
            });

            $('#editCurrentPage').on('click', function () {
                showFastEditor('contenuti_popup', $('#id_page').val(), function (result) {
                    $('#fastModuleEditor').modal('toggle');
                    toastr['success']('Pagina aggiornata correttamente');
                });
            });

            $visualContainer.find('> .visualBuilderBlockEditModal [data-name="onclick"] .onClickEventSettings select.form-control').chosen();
        }

        if($visualContainer.find('> .visualBuilderBlockEditModal [data-name="structure"]').length) {
            var options = {
                structure_mode: $visualContainer.find('> .visualBuilderBlockEditModal [data-name="structure"] .visualBlockStructure').data('type'),
                shortcodes: $visualContainer.find('> .visualBuilderBlockEditModal [data-name="structure"] .visualBlockStructure').data('shortcodes'),
                use_relative_page: $visualContainer.find('> .visualBuilderBlockEditModal [data-name="structure"] .visualBlockStructure').data('use-relative-page')
            };
            $visualContainer.find('> .visualBuilderBlockEditModal [data-name="structure"] .visualBlockStructure').visualBuilder(options);
        }

        if($visualContainer.find('> .visualBuilderBlockEditModal .attachVisualBuilder').length) {
            $visualContainer.find('> .visualBuilderBlockEditModal .attachVisualBuilder').each(function () {
                $(this).visualBuilder(element.settings);
            });
        }
    };

    this.getWidgetData = function () {
        var getChildren = function ($parent) {
            var tmpData = {};

            $parent.find('.widget-group').not($parent.find('.widget-group .widget-group')).not($parent.find('.visualBuilderBlockEditModal .widget-group')).each(function () {

                var group = $(this).data('name');
                if(typeof(tmpData[group]) === 'undefined') {
                    tmpData[group] = {};
                }

                $(this).find('.widget-data[name]').not($(this).find('.widget-group .widget-data[name]')).each(function () {
                    var name = $(this).attr('name');
                    var value = '';

                    var skip = false;

                    if($(this).hasClass('tinymce')) {
                        value = tinymce.get($(this).attr('id')).getContent();
                    } else if($(this).attr('type') === 'checkbox' || $(this).attr('type') === 'radio') {
                        if($(this).is(':checked')) {
                            value = $(this).val();
                        } else {
                            skip = true;
                        }
                    } else if($(this).hasClass('uploader')) {
                        value = {path: element.settings.uploads, is_common: element.settings.uploads_common, files: composeOrakImagesToSave($(this).data('id'))};
                    } else {
                        value = $(this).val();
                    }

                    if(!skip) tmpData[group][name] = value;
                });

                if($(this).find('.widget-group').length) {
                    tmpData[group] = Object.assign(tmpData[group], getChildren($(this)));
                }

            });

            return tmpData;
        };

        return getChildren($visualContainer.find('> .visualBuilderBlockEditModal').not(($visualContainer.find('> .visualBuilderBlockEditModal .visualBuilderBlockEditModal'))));
    };

    this.saveWidgetData = function () {
        var widgetData = element.getWidgetData();

        if(element.currentSelection.target.hasClass('visualBuilderRow')) {
            element.currentSelection.target.find('.row-data').html(JSON.stringify(widgetData));
            element.saveData();
            element.composeDesignPreview(element.currentSelection.target.find('.visualBuilderDesignPreview.rowPreview'), widgetData);
        } else if(element.currentSelection.target.hasClass('visualBuilderColumn')) {
            element.currentSelection.target.find('.col-data').html(JSON.stringify(widgetData));
            element.currentSelection.target.attr('data-col', widgetData.responsive.desktop.col);
            element.saveData();
            element.composeDesignPreview(element.currentSelection.target.find('.visualBuilderDesignPreview.columnPreview'), widgetData);
        } else {

            $('#editSaveBtn').attr('disabled', 'disabled');
            $.ajax({
                url: element.settings.endPoint,
                type: "POST",
                data: {
                    "action": 'drawWidgetPreview',
                    "data": widgetData
                },
                async: true,
                dataType: "text",
                success: function (preview) {

                    $('#editSaveBtn').attr('disabled', false);

                    var $preview = $(preview);
                    element.composeDesignPreview($preview.find('.visualBuilderDesignPreview.actionPreview'), widgetData);

                    if (element.currentSelection.target.hasClass('visualBuilderWidgetContent')) {
                        element.currentSelection.target.replaceWith($preview);
                    } else {
                        if (element.currentSelection.target_position === 'top') {
                            element.currentSelection.target.prepend($preview);
                        } else {
                            element.currentSelection.target.append($preview);
                        }
                    }

                    element.onContentDraw();
                }
            });
        }
    };

    this.saveData = function () {
        var data = element.getData();
        element.$textArea.val('<!--VISUAL-BUILDER-START-->' + JSON.stringify(data).replace(/<!--VISUAL-BUILDER-START-->/gm, '').replace(/<!--VISUAL-BUILDER-END-->/gm, '') + '<!--VISUAL-BUILDER-END-->');
    };

    this.getData = function () {
        var getChildren = function ($parent) {

            var rowsData = [];

            $parent.find('.visualBuilderRow').not($parent.find('.visualBuilderRow .visualBuilderRow')).each(function () {

                var rowData = [];

                $(this).find('.visualBuilderColumn[data-col]').not($(this).find('.visualBuilderRow .visualBuilderColumn[data-col]')).each(function () {
                    var col = $(this).data('col');

                    var tmpActions = [];
                    $(this).find('.visualBuilderColumnContent > *').not($(this).find('.visualBuilderColumnContent > * .visualBuilderColumnContent > *')).each(function () {

                        if($(this).hasClass('visualBuilderWidgetContent')) {

                            var action_json = JSON.parse($(this).find('.widget-data').html());

                            tmpActions.push({
                                type: 'block',
                                block_type: action_json.data.type,
                                json: JSON.parse($(this).find('.widget-data').html())
                            });
                        } else if($(this).hasClass('visualBuilderRow')) {
                            tmpActions.push({
                                type: 'row',
                                children: getChildren($(this).parent()),
                                json: ($(this).find('> .row-data').length ? JSON.parse($(this).find('> .row-data').html()) : {})
                            });
                        }

                    });

                    var json = ($(this).find('> .col-data').length ? JSON.parse($(this).find('> .col-data').html()) : {});
                    if(typeof(json.responsive) === 'undefined') json.responsive = {desktop: {}, tablet: {}, mobile: {}};
                    json.responsive.desktop.col = col;

                    rowData.push({
                        type: 'col',
                        col: col,
                        children: tmpActions,
                        json: json
                    });
                });

                rowsData.push({
                    type: 'row',
                    children: rowData,
                    json: ($(this).find('> .row-data').length ? JSON.parse($(this).find('> .row-data').html()) : {})
                });

            });

            return rowsData;
        };

        return getChildren($visualContainer.find('> .visualBuilderContent'));
    };

    this.composeDesignPreview = function ($element, data) {
        try {
            var html = [];

            var boxStyle = [];
            if (data.design.background.color.length) boxStyle.push('background-color: ' + data.design.background.color);
            if (data.design.background.opacity.length) boxStyle.push('opacity: ' + data.design.background.opacity / 100);
            if (data.design.border.color.length) boxStyle.push('border-color: ' + data.design.border.color);
            if (data.design.border.style.length) boxStyle.push('border-style: ' + data.design.border.style);

            var borderRadiusSuffix = data.design['border-radius'].suffix;
            data.design['border-radius'].suffix = '';
            Object.keys(data.design['border-radius']).forEach(function (key) {
                if (data.design['border-radius'][key].length) {
                    boxStyle.push('border-' + key + '-radius: ' + data.design['border-radius'][key] + borderRadiusSuffix);
                }
            });

            if (boxStyle.length) {
                html.push('<div class="bgPreview" style="' + boxStyle.join(';') + '"></div>');
            }

            if (typeof (data.display) !== 'undefined' && typeof (data.display.dates) !== 'undefined') {
                if (data.display.dates['hide-before'].length || data.display.dates['hide-after'].length) {
                    html.push('<div class="elemAttrIcon text-warning" title="Visualizzazione limitata nel tempo"><i class="far fa-clock"></i></div>');
                }
            }

            if (typeof (data.responsive) !== 'undefined') {
                var have_view_limitation = false;
                Object.keys(data.responsive).forEach(function (device) {
                    if(data.responsive[device]['hide'] == 1) have_view_limitation = true;
                });

                if (have_view_limitation) {
                    html.push('<div class="elemAttrIcon text-warning" title="La visualizzazione è limitata su alcuni dispositivi"><i class="far fa-eye-slash"></i></div>');
                }
            }

            $element.html(html.join(''));
        } catch(e) {}

        return $element;

    };

    /* MISC */
    this.getUniqueID = function () {
        return Math.random().toString(36).substr(2, 9);
    };

    /* Inizializzo il Visual Builder */
    this.init();

    return this;
};

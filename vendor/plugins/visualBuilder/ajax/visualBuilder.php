<?php
include('../../../../sw-config.php');

if($_POST['action'] === 'drawBuilder') {

    $data = (json_decode($_POST['data']) ? json_decode($_POST['data'], true) : array());

    if(is_array($data) && isset($data['tpl'])) {
        $data = $MSFrameworkVisualBuilder->getStandardTpl($data['tpl'], $data['content']);
    }

    if ($_POST['firstInit'] === "1") {
        echo '<div class="visualBuilderContainer">';
        include('../templates/backend/welcome.php');
    }

    include('../templates/backend/content.php');

    if ($_POST['firstInit'] === "1") {
        echo '<textarea class="visualBuilderTemplates emptyRow" style="display: none;">';
        include('../templates/backend/parts/row.php');
        echo '</textarea>';

        echo '<textarea class="visualBuilderTemplates emptyColumn" style="display: none;">';
        include('../templates/backend/parts/column.php');
        echo '</textarea>';

        include('../templates/backend/popup-list.php');
        include('../templates/backend/popup-edit.php');

        echo '</div>';
    }

} else if($_POST['action'] === 'getPopupContent') {
    $widgetDetails = $MSFrameworkVisualBuilder->getBlocksDetails($_POST['popup']);
    $widgetData = $_POST['data'];

    $haveMain = file_exists($widgetDetails['path'] . '/popup.php');
    $haveThemes = file_exists($widgetDetails['path'] . '/styles');
    $haveStructure = file_exists($widgetDetails['path'] . '/structure');

    $isActive = false;

    $uniqueID = uniqid('tab_');
    ?>
    <div class="tabs-container tabs-container-header">
        <ul class="nav nav-tabs" role="tablist">
            <?php if($haveMain) { ?>
                <li class="<?= (!$isActive ? 'active' : ''); ?>"><a class="nav-link" data-toggle="tab" href="#tab-specific-<?= $uniqueID; ?>">Generale</a></li>
                <?php  $isActive = true; ?>
            <?php } ?>
            <?php if($haveThemes) { ?>
                <li class="<?= (!$isActive ? 'active' : ''); ?>"><a class="nav-link" data-toggle="tab" href="#tab-themes-<?= $uniqueID; ?>">Temi</a></li>
                <?php  $isActive = true; ?>
            <?php } ?>
            <?php if($haveStructure) { ?>
                <li class="<?= (!$isActive ? 'active' : ''); ?>"><a class="nav-link" data-toggle="tab" href="#tab-structure-<?= $uniqueID; ?>">Struttura</a></li>
                <?php  $isActive = true; ?>
            <?php } ?>
            <li class="<?= (!$isActive ? 'active' : ''); ?>"><a class="nav-link" data-toggle="tab" href="#tab-global-<?= $uniqueID; ?>">Design</a></li>
            <?php  $isActive = true; ?>
            <li class=""><a class="nav-link" data-toggle="tab" href="#tab-display-<?= $uniqueID; ?>">Visualizzazione</a></li>
        </ul>
        <?php  $isActive = false; ?>
        <div class="tab-content">

            <?php if($haveMain) { ?>
                <div role="tabpanel" id="tab-specific-<?= $uniqueID; ?>" class="tab-pane <?= (!$isActive ? 'active' : ''); ?>">
                    <div class="panel-body">
                        <div class="widget-group" data-name="data">
                            <input type="hidden" class="widget-data" name="type" value="<?= $widgetDetails['id']; ?>">
                            <?php
                            include($widgetDetails['path'] . '/popup.php');

                            if($widgetDetails['include-settings']) {
                                foreach($widgetDetails['include-settings'] as $settingFile) include('../templates/backend/parts/popup-elements/' . $settingFile . '.php');
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php  $isActive = true; ?>
            <?php } ?>

            <?php if($haveThemes) { ?>
                <div role="tabpanel" id="tab-themes-<?= $uniqueID; ?>" class="tab-pane <?= (!$isActive ? 'active' : ''); ?>">
                    <div class="panel-body">
                        <div class="widget-group" data-name="theme">
                            <?php include('../templates/backend/popup-themes.php'); ?>
                        </div>
                    </div>
                </div>
                <?php  $isActive = true; ?>
            <?php } ?>

            <?php if($haveStructure) { ?>
                <div role="tabpanel" id="tab-structure-<?= $uniqueID; ?>" class="tab-pane <?= (!$isActive ? 'active' : ''); ?>">
                    <div class="panel-body">
                        <div class="widget-group" data-name="structure">
                            <?php include('../templates/backend/popup-structure.php'); ?>
                        </div>
                    </div>
                </div>
                <?php  $isActive = true; ?>
            <?php } ?>

            <div role="tabpanel" id="tab-global-<?= $uniqueID; ?>" class="tab-pane <?= (!$isActive ? 'active' : ''); ?>">
                <div class="panel-body">
                    <div class="widget-group" data-name="design">
                        <?php include('../templates/backend/popup-settings.php'); ?>
                    </div>
                </div>
            </div>

            <?php  $isActive = true; ?>

            <div role="tabpanel" id="tab-display-<?= $uniqueID; ?>" class="tab-pane <?= (!$isActive ? 'active' : ''); ?>">
                <div class="panel-body">
                    <?php include('../templates/backend/popup-display.php'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
} else if($_POST['action'] === 'drawWidgetPreview') {
    $widgetData = $_POST['data'];
    $blockDetails = $MSFrameworkVisualBuilder->getBlocksDetails($widgetData['data']['type']);

    ob_start();
    include('../templates/backend/parts/action.php');
    $action_html = ob_get_clean();

    ob_start();
    include($blockDetails['path'] . '/preview.php');
    $widget_block = ob_get_clean();

    $return_html = str_replace(
        array(
            '[action-title]',
            '[action-icon]',
            '[action-content]',
            '[action-data]'
        ),
        array(
            htmlentities($blockDetails['title']),
            htmlentities($blockDetails['url'] . '/icon.png'),
            $widget_block,
            json_encode($widgetData),
        ),
        $action_html
    );

    echo $return_html;

}
?>
<?php
include("../../../sw-config.php");
$producer_config = (new \MSFramework\cms())->getCMSData('producer_config');
$email_colors = json_decode($producer_config['templatemail_colors']);
?>
<table class="main" width="100%" style="background: <?= $email_colors[3]; ?>" data-css_replace="[background|{color_4}]" cellspacing="0" cellpadding="0" border="0" data-types="background,padding" data-last-type="padding">
    <tbody>
        <tr>
            <td class="element-content" style="padding: 10px 15px;font-family:Arial;font-size:13px;color:#000000;line-height:1.4">
                <h1 style="font-weight: normal;text-align:center;">Benvenuto nel nostro Editor</h1>
                <p style="text-align: center;">Trascina un elemento dalla scheda a sinistra per iniziare.</p>
            </td>
        </tr>
    </tbody>
</table>

    var returnVal;

    (function(factory) {
        "use strict";

        if (typeof define === 'function' && define.amd) { // AMD
            define(['jquery'], factory);
        } else if (typeof exports == "object" && typeof module == "object") { // CommonJS
            module.exports = factory(require('jquery'));
        } else { // Browser
            factory(jQuery);
        }
    })(function($, undefined) {
        "use strict";

        // -------------------------- variables -------------------------- //
        //left menu all items
        var _tabMenuItems = {
            //elements tab
            'typography': {
                itemSelector: 'typography',
                parentSelector: 'tab-elements'
            },
            'media': {
                itemSelector: 'media',
                parentSelector: 'tab-elements'
            },
            'layout': {
                itemSelector: 'layout',
                parentSelector: 'tab-elements'
            },
            'button': {
                itemSelector: 'button',
                parentSelector: 'tab-property'
            },
            'social': {
                itemSelector: 'social',
                parentSelector: 'tab-elements'
            },
            //property tab
            'background': {
                itemSelector: 'background',
                parentSelector: 'tab-property'
            },
            'border-radius': {
                itemSelector: 'border-radius',
                parentSelector: 'tab-property'
            },
            'text-style': {
                itemSelector: 'text-style',
                parentSelector: 'tab-property'
            },
            'padding': {
                itemSelector: 'padding',
                parentSelector: 'tab-property'
            },
            'youtube-frame': {
                itemSelector: 'youtube-frame',
                parentSelector: 'tab-property'
            },
            'hyperlink': {
                itemSelector: 'hyperlink',
                parentSelector: 'tab-property'
            },
            //'button': {
            //    itemSelector: 'button',
             //   parentSelector: 'tab-property'
           // },
            'image-settings': {
                itemSelector: 'image-settings',
                parentSelector: 'tab-property'
            },
            'social-content': {
                itemSelector: 'social-content',
                parentSelector: 'tab-property'
            }
        };


        /**
         * Using all variables in plugin
         */
        var _language = [];
        var _aceEditor, _blankPageHtml;
        var  _nav;

        var EmailBuilder = function(elem, options) {
            //Private variables
            this.elem = elem;
            this.$elem = $(elem);
            this.options = options;
            this.langArr;
        };

        EmailBuilder.prototype = {
            defaults: {
                //global settings
                elementJsonUrl: 'elements.json',
                langJsonUrl: 'lang.json',
                lang: 'en',
                loading_color1: '#3B7694',
                loading_color2: '#09181F',
                showLoading: true,

                blankPageHtmlUrl: 'template-blank-page.html',
                // when page load showing this html
                loadPageHtmlUrl: 'template-load-page.html',

                //show context menu
                showContextMenu: true,
                showContextMenu_FontFamily: true,
                showContextMenu_FontSize: true,
                showContextMenu_Bold: true,
                showContextMenu_Italic: true,
                showContextMenu_Underline: true,
                showContextMenu_Strikethrough: true,
                showContextMenu_Hyperlink: true,

                //left menu
                showElementsTab: true,
                showPropertyTab: true,
                showCollapseMenu: true,
                showBlankPageButton: true,
                showCollapseMenuinBottom: true, //btn-collapse-bottom
                showMobileView: true,

                //setting items
                showSettingsBar: true,
                showSettingsPreview: true,
                showSettingsExport: true,
                showSettingsSendMail: true,
                showSettingsSave: true,
                showSettingsLoadTemplate: true,

                //show or hide elements actions
                showRowMoveButton: true,
                showRowRemoveButton: true,
                showRowDuplicateButton: true,
                showRowCodeEditorButton: true,

                shortcodesList: [],

                //events of settings
                onSettingsPreviewButtonClick: function(e) {},
                onSettingsExportButtonClick: function(e) {},
                onBeforeSettingsSaveButtonClick: function(e) {},
                onSettingsSaveButtonClick: function(e) {},
                onBeforeSettingsLoadTemplateButtonClick: function(e) {},
                onSettingsSendMailButtonClick: function(e) {},

                //events in modal
                onBeforeChangeImageClick: function(e) {},
                onBeforePopupSelectImageButtonClick: function(e) {},
                onBeforePopupSelectTemplateButtonClick: function(e) {},
                onPopupSaveButtonClick: function(e) {},
                onPopupSendMailButtonClick: function(e) {},
                onPopupUploadImageButtonClick: function(e) {},
                onTemplateDeleteButtonClick: function(e) {},

                //selected element events
                onBeforeRowRemoveButtonClick: function(e) {},
                onAfterRowRemoveButtonClick: function(e) {},

                onBeforeRowDuplicateButtonClick: function(e) {},
                onAfterRowDuplicateButtonClick: function(e) {},

                onBeforeRowEditorButtonClick: function(e) {},
                onAfterRowEditorButtonClick: function(e) {},

                onBeforeShowingEditorPopup: function(e) {},
                onAfterLoad: function(e) {},

                onElementDragStart: function(e) {},
                onElementDragFinished: function(e, contentHtml) {},

                onUpdateButtonClick: function(e) {}
            },
            loadPageHtmlUrl: 'asd',
            /**
             * Init Plugin
             */
            init: function() {
                var _this = this;
                _this.config = $.extend({}, this.defaults, this.options);

                //show loading
                _this.show_loading();

                $.ajax({
                    url: _this.config.loadPageHtmlUrl,
                    data: '',
                    success: function(data) {
                        _this._loadPageHtml = data;
                        _this.getLangs();
                        _this.getBlankPageHtml();
                    },
                    error: function() {
                        console.error('Has error');
                    }
                });

                return this;
            },
            /**
             * Show loading
             */
            show_loading: function() {
                var _this = this;
                if (_this.config.showLoading == true) {
                    // _this.$elem.css({
                    //     'position': 'relative'
                    // });
                    _this.display('<div class="bal-loading-container"><div class="bal-loading"><div class="bal-loading-bounce-1" style="background-color:' + _this.config.loading_color1 + '"></div><div class="bal-loading-bounce-2" style="background-color:' + _this.config.loading_color2 + '"></div></div></div>');
                }
            },
            /**
             * Generate output information
             */
            generate: function(elementsHtml) {
                var _this = this;

                _nav = '<div class="bal-nav">' +
                    '<ul class="bal-left-menu">';
                if (_this.config.showElementsTab == true) {
                    _nav += '<li class="bal-menu-item tab-selector active" data-tab-selector="tab-elements">' +
                        '<i class="fa fa-puzzle-piece"></i>' +
                        '<span class="bal-menu-name">' + _this.langArr.elementsTab + '</span>' +
                        '</li>';
                }

                if (_this.config.showPropertyTab == true) {
                    _nav += '<li class="bal-menu-item tab-selector" data-tab-selector="tab-property">' +
                        '<i class="fa fa-pencil"></i>' +
                        '<span class="bal-menu-name">' + _this.langArr.propertyTab + '</span>' +
                        '</li>';
                }

                if (_this.config.showBlankPageButton == true) {
                    _nav += '<li class="bal-menu-item blank-page">' +
                        '<i class="fa fa-file"></i>' +
                        '<span class="bal-menu-name">' + _this.langArr.blankPage + '</span>' +
                        '</li>';
                }
                // _nav += '<li class="bal-menu-item bal-menu-line " ></li>';
                //
                // _nav += '<li class="bal-menu-item command-undo " >' +
                //     '<i class="fa fa-reply"></i>' +
                //     '<span class="bal-menu-name">' + _this.langArr.commandUndo + '</span>' +
                //     '</li>';
                //
                // _nav += '<li class="bal-menu-item command-redo">' +
                //     '<i class="fa fa-share"></i>' +
                //     '<span class="bal-menu-name">' + _this.langArr.commandRedo + '</span>' +
                //     '</li>';

                if (_this.config.showCollapseMenu == true) {
                    var _class = '';
                    if (_this.config.showCollapseMenuinBottom == true) {
                        _class = 'btn-collapse-bottom ';
                    }

                    _nav += '<li class="bal-menu-item bal-collapse ' + _class + '">' +
                        '<i class="fa fa-chevron-circle-left"></i>' +
                        '<span class="bal-menu-name">' + _this.langArr.collapseMenu + '</span>' +
                        '</li>';
                }
                _nav += '</ul></div>';

                var _settings = '';
                if (_this.config.showSettingsBar == true) {


                    // <div class="bal-setting-content">
                    //   <div class="bal-setting-content-item other-devices">
                    //     <ul>
                    //     <li class="bal-setting-device-tab mobile " data-tab="mobile-content">
                    //     <i class="fa fa-mobile"></i>
                    //     </li>
                    //     <li class="bal-setting-device-tab desktop active" data-tab="desktop-content">
                    //     <i class="fa fa-desktop"></i>
                    //     </li>
                    //     </ul>
                    //     <div>
                    //       <div class="mobile-content bal-setting-device-content ">mobile-content </div>
                    //       <div class="desktop-content bal-setting-device-content active">desktop-content </div>
                    //     </div>
                    //   </div>
                    // </div>

                    _settings = '<div class="bal-settings">' +
                        '<ul>';
                    if (_this.config.showSettingsPreview == true) {
                        _settings += '<li class="bal-setting-item preview" data-toggle="tooltip" title="' + _this.langArr.settingsPreview + '">' +
                            '<i class="fa fa-eye"></i>' +
                            '</li>';
                    }

                    if (_this.config.showSettingsExport == true) {
                        _settings += '<li class="bal-setting-item export" data-toggle="tooltip" title="' + _this.langArr.settingsExport + '">' +
                            '<i class="fa fa-share"></i>' +
                            '</li>';
                    }

                    if (_this.config.showSettingsSave == true) {
                        _settings += '<li class="bal-setting-item save-template" data-toggle="tooltip" title="' + _this.langArr.settingsSaveTemplate + '">' +
                            '<i class="fa fa-floppy-o"></i>' +
                            '</li>';
                    }

                    if (_this.config.showSettingsLoadTemplate == true) {
                        _settings += '<li class="bal-setting-item load-templates" data-toggle="tooltip" title="' + _this.langArr.settingsLoadTemplate + '">' +
                            '<i class="fa fa-file-text"></i>' +
                            '</li>';
                    }

                    if (_this.config.showSettingsSendMail == true) {
                        _settings += '<li class="bal-setting-item send-email" data-toggle="tooltip" title="' + _this.langArr.settingsSendMail + '">' +
                            '<i class="fa fa-envelope"></i>' +
                            '</li>';
                    }
                    if (_this.config.showMobileView == true) {
                        _settings += '<li class="bal-setting-item other-devices" data-toggle="tooltip" title="" data-original-title="' + _this.langArr.setttingsMobileViewTitle + '">' +
                            '<i class="fa fa-mobile"></i>' +
                            '</li>' +
                            '<div class="bal-setting-content">' +
                            '  <div class="bal-setting-content-item other-devices">' +
                            '    <ul>' +
                            '    <li class="bal-setting-device-tab mobile " data-tab="mobile-content">' +
                            '    <i class="fa fa-mobile"></i>' +
                            '    </li>' +
                            '    <li class="bal-setting-device-tab desktop active" data-tab="desktop-content">' +
                            '    <i class="fa fa-desktop"></i>' +
                            '    </li>' +
                            '    </ul>' +
                            '    <div>' +
                            '      <div class="mobile-content bal-setting-device-content ">' + _this.langArr.setttingsMobileViewMobileDesc + '</div>' +
                            '      <div class="desktop-content bal-setting-device-content active">' + _this.langArr.setttingsMobileViewDesktopDesc + '</div>' +
                            '    </div>' +
                            '  </div>  ' +
                            '</div>';
                    }
                    /*
                     */
                    _settings += '</ul></div>';
                }

                var _tabElements = '<div class="tab-elements bal-element-tab active"><ul class="bal-elements-accordion">' + elementsHtml + '</ul></div>';

                var colorSelect = '<select class="defaultColorsSelect"></select>';

                //  _tabProperty='<div class="tab-property bal-element-tab"><ul class="bal-elements-accordion"><li class="bal-elements-accordion-item" data-type="background"><a class="bal-elements-accordion-item-title">Background</a><div class="bal-elements-accordion-item-content clearfix"><div id="bg-color" class="bg-color bg-item" setting-type="background-color"><i class="fa fa-adjust"></i></div><!-- <div class="bg-item bg-image" setting-type="background-image"><i class="fa fa-image"></i></div>--></div></li><li class="bal-elements-accordion-item" data-type="padding"><a class="bal-elements-accordion-item-title">Padding</a><div class="bal-elements-accordion-item-content"><div class=" bal-element-boxs clearfix "><div class="big-box col-sm-6 "><input type="text" class="form-control padding all" setting-type="padding"></div><div class="small-boxs col-sm-6"><div class="row"><input type="text" class="form-control padding number" setting-type="padding-top"></div><div class="row clearfix"><div class="col-sm-6"><input type="text" class="form-control padding number" setting-type="padding-left"></div><div class="col-sm-6"><input type="text" class="form-control padding number" setting-type="padding-right"></div></div><div class="row"><input type="text" class="form-control padding number" setting-type="padding-bottom"></div></div></div></div></li><li class="bal-elements-accordion-item" data-type="border-radius"><a class="bal-elements-accordion-item-title">Border Radius</a><div class="bal-elements-accordion-item-content"><div class=" bal-element-boxs bal-border-radius-box clearfix "><div class="big-box col-sm-6 "><input type="text" class="form-control border-radius all" setting-type="border-radius"></div><div class="small-boxs col-sm-6"><div class="row clearfix"><div class="col-sm-6"><input type="text" class="form-control border-radius" setting-type="border-top-left-radius"></div><div class="col-sm-6"><input type="text" class="form-control border-radius" setting-type="border-top-right-radius"></div></div><div class="row clearfix margin"><div class="col-sm-6"><input type="text" class="form-control border-radius" setting-type="border-bottom-left-radius"></div><div class="col-sm-6"><input type="text" class="form-control border-radius" setting-type="border-bottom-right-radius"></div></div></div></div></div></li><li class="bal-elements-accordion-item" data-type="text-style"><a class="bal-elements-accordion-item-title">Text Style</a><div class="bal-elements-accordion-item-content"><div class="bal-element-boxs bal-text-style-box clearfix "><div class="bal-element-font-family col-sm-8"><select class="form-control font-family" setting-type="font-family"><option value="Arial">Arial</option><option value="Helvetica">Helvetica</option><option value="Georgia">Georgia</option><option value="Times New Roman">Times New Roman</option><option value="Verdana">Verdana</option><option value="Tahoma">Tahoma</option><option value="Calibri">Calibri</option></select></div><div class="bal-element-font-size col-sm-4"><input type="text" name="name" class="form-control number" value="14" setting-type="font-size" /></div><div class="bal-icon-boxs bal-text-icons clearfix"><div class="bal-icon-box-item fontStyle" setting-type="font-style" setting-value="italic"><i class="fa fa-italic"></i></div><div class="bal-icon-box-item active underline " setting-type="text-decoration" setting-value="underline"><i class="fa fa-underline"></i></div><div class="bal-icon-box-item line " setting-type="text-decoration" setting-value="line-through"><i class="fa fa-strikethrough"></i></div></div><div class="bal-icon-boxs bal-align-icons clearfix"><div class="bal-icon-box-item left active"><i class="fa fa-align-left"></i></div><div class="bal-icon-box-item center "><i class="fa fa-align-center"></i></div><div class="bal-icon-box-item right"><i class="fa fa-align-right"></i></div></div><div class="clearfix"></div><div class="bal-icon-boxs bal-text-icons "><div id="text-color" class="bal-icon-box-item text-color" setting-type="color"></div>Text Color </div><div class="bal-icon-boxs bal-font-icons clearfix"><div class="bal-icon-box-item" setting-type="bold"><i class="fa fa-bold"></i></div></div></div></div></li><li class="bal-elements-accordion-item" data-type="social-content"><a class="bal-elements-accordion-item-title">Social content</a><div class="bal-elements-accordion-item-content"><div class="col-sm-12 bal-social-content-box"><div class="row" data-social-type="instagram"><label class="small-title">Instagram</label><input type="text" name="name" value="#" class="social-input" /><label class="checkbox-title"><input type="checkbox" name="name" />Show </label></div><div class="row" data-social-type="pinterest"><label class="small-title">Pinterest</label><input type="text" name="name" value="#" class="social-input" /><label class="checkbox-title"><input type="checkbox" name="name" />Show </label></div><div class="row" data-social-type="google-plus"><label class="small-title">Google+</label><input type="text" name="name" value="#" class="social-input" /><label class="checkbox-title"><input type="checkbox" name="name" checked />Show </label></div><div class="row" data-social-type="facebook"><label class="small-title">Facebook</label><input type="text" name="name" value="#" class="social-input" /><label class="checkbox-title"><input type="checkbox" name="name" checked />Show </label></div><div class="row" data-social-type="twitter"><label class="small-title">Twitter</label><input type="text" name="name" value="#" class="social-input" /><label class="checkbox-title"><input type="checkbox" name="name" checked />Show </label></div><div class="row" data-social-type="linkedin"><label class="small-title">Linkedin</label><input type="text" name="name" value="#" class="social-input" /><label class="checkbox-title"><input type="checkbox" name="name" checked />Show </label></div><div class="row" data-social-type="youtube"><label class="small-title">Youtube</label><input type="text" name="name" value="#" class="social-input" /><label class="checkbox-title"><input type="checkbox" name="name" checked />Show </label></div><div class="row" data-social-type="skype"><label class="small-title">Skype</label><input type="text" name="name" value="#" class="social-input" /><label class="checkbox-title"><input type="checkbox" name="name" checked />Show </label></div></div></div></li><li class="bal-elements-accordion-item" data-type="youtube-frame"><a class="bal-elements-accordion-item-title">Youtube</a><div class="bal-elements-accordion-item-content"><div class="bal-social-content-box "><label>Youtube Video ID</label><input type="text" class=" youtube" setting-type=""></div></div></li><li class="bal-elements-accordion-item" data-type="hyperlink"><a class="bal-elements-accordion-item-title">Hyperlink</a><div class="bal-elements-accordion-item-content"><div class="bal-social-content-box "><label>Url</label><input type="text" class="hyperlink-url" setting-type=""></div></div></li></ul></div>';
                var _tabProperty = '<div class="tab-property bal-element-tab">' +
                    ' <ul class="bal-elements-accordion">' +
                    ' <li class="bal-elements-accordion-item" data-type="background">' +
                    '   <a class="bal-elements-accordion-item-title">' + _this.langArr.propertyBG + '</a>' +
                    '   <div class="bal-elements-accordion-item-content clearfix">' +
                    colorSelect +
                    '   <div id="bg-color" class="bg-color bg-item" setting-type="background-color">' +
                    '   <i class="fa fa-adjust"></i>' +
                    ' </div>' +
                    ' </div>' +
                    ' </li>' +
                    '   <li class="bal-elements-accordion-item" data-type="padding">' +
                    '     <a class="bal-elements-accordion-item-title">' + _this.langArr.propertyPadding + '</a>' +
                    '   <div class="bal-elements-accordion-item-content">' +
                    '     <div class=" bal-element-boxs clearfix ">' +
                    '       <div class="big-box col-sm-6 ">' +
                    '         <input type="text" class="form-control padding all" setting-type="padding">' +
                    '   </div>' +
                    ' <div class="small-boxs col-sm-6">' +
                    ' <div class="row">' +
                    '   <input type="text" class="form-control padding number" setting-type="padding-top">' +
                    '   </div>' +
                    '   <div class="row clearfix">' +
                    '     <div class="col-sm-6">' +
                    '         <input type="text" class="form-control padding number" setting-type="padding-left">' +
                    '   </div>' +
                    '   <div class="col-sm-6">' +
                    ' <input type="text" class="form-control padding number" setting-type="padding-right">' +
                    ' </div>' +
                    ' </div>' +
                    '   <div class="row">' +
                    '   <input type="text" class="form-control padding number" setting-type="padding-bottom">' +
                    '   </div>' +
                    '   </div>' +
                    ' </div>' +
                    '   </div>' +
                    '   </li>' +
                    '   <li class="bal-elements-accordion-item" data-type="border-radius">' +
                    '   <a class="bal-elements-accordion-item-title">' + _this.langArr.propertyBorderRadius + '</a>' +
                    '   <div class="bal-elements-accordion-item-content">' +
                    '   <div class=" bal-element-boxs bal-border-radius-box clearfix ">' +
                    '   <div class="big-box col-sm-6 ">' +
                    '     <input type="text" class="form-control border-radius all" setting-type="border-radius">' +
                    '     </div>' +
                    '   <div class="small-boxs col-sm-6">' +
                    '     <div class="row clearfix">' +
                    '   <div class="col-sm-6">' +
                    '     <input type="text" class="form-control border-radius" setting-type="border-top-left-radius">' +
                    '     </div>' +
                    ' <div class="col-sm-6">' +
                    '   <input type="text" class="form-control border-radius" setting-type="border-top-right-radius">' +
                    '   </div>' +
                    ' </div>' +
                    '   <div class="row clearfix margin">' +
                    '   <div class="col-sm-6">' +
                    '   <input type="text" class="form-control border-radius" setting-type="border-bottom-left-radius">' +
                    '   </div>' +
                    ' <div class="col-sm-6">' +
                    ' <input type="text" class="form-control border-radius" setting-type="border-bottom-right-radius">' +
                    ' </div>' +
                    ' </div>' +
                    ' </div>' +
                    ' </div>' +
                    ' </div>' +
                    ' </li>' +
                    ' <li class="bal-elements-accordion-item" data-type="text-style">' +
                    '   <a class="bal-elements-accordion-item-title">' + _this.langArr.propertyTextStyle + '</a>' +
                    '   <div class="bal-elements-accordion-item-content">' +
                    '   <div class="bal-element-boxs bal-text-style-box clearfix ">' +
                    '   <div class="bal-element-font-family col-sm-8">' +
                    '   <select class="form-control font-family" setting-type="font-family">' +
                    '     <option value="Arial">Arial</option>' +
                    '   <option value="Helvetica">Helvetica</option>' +
                    ' <option value="Georgia">Georgia</option>' +
                    '<option value="Times New Roman">Times New Roman</option>' +
                    '<option value="Verdana">Verdana</option>' +
                    '<option value="Tahoma">Tahoma</option>' +
                    '<option value="Calibri">Calibri</option>' +
                    '</select>' +
                    '</div>' +
                    '<div class="bal-element-font-size col-sm-4">' +
                    '  <input type="text" name="name" class="form-control number" value="14" setting-type="font-size" />' +
                    '</div>' +
                    '<div class="bal-icon-boxs bal-text-icons clearfix">' +
                    '<div class="bal-icon-box-item fontStyle" setting-type="font-style" setting-value="italic">' +
                    '<i class="fa fa-italic"></i>' +
                    '</div>' +
                    '<div class="bal-icon-box-item active underline " setting-type="text-decoration" setting-value="underline">' +
                    '<i class="fa fa-underline"></i>' +
                    '</div>' +
                    '<div class="bal-icon-box-item line " setting-type="text-decoration" setting-value="line-through">' +
                    '  <i class="fa fa-strikethrough"></i>' +
                    '</div>' +
                    '</div>' +
                    '<div class="bal-icon-boxs bal-align-icons clearfix">' +
                    '<div class="bal-icon-box-item left active">' +
                    '<i class="fa fa-align-left"></i>' +
                    '</div>' +
                    '<div class="bal-icon-box-item center ">' +
                    '  <i class="fa fa-align-center"></i>' +
                    '</div>' +
                    '<div class="bal-icon-box-item right">' +
                    '  <i class="fa fa-align-right"></i>' +
                    '  </div>' +
                    '</div>' +
                    '  <div class="clearfix"></div>' +
                    '  <div class="bal-icon-boxs bal-text-icons ">' +
                    '  <div id="text-color" class="bal-icon-box-item text-color" setting-type="color">' +
                    '  </div>' +
                    '  Text Color' +
                    '  </div>' +
                    '<div class="bal-icon-boxs bal-font-icons clearfix">' +
                    '<div class="bal-icon-box-item" setting-type="bold">' +
                    '<i class="fa fa-bold"></i>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '  </li>' +
                    '<li class="bal-elements-accordion-item" data-type="social-content">' +
                    '  <a class="bal-elements-accordion-item-title">' + _this.langArr.propertySocialContent + '</a>' +
                    '<div class="bal-elements-accordion-item-content">' +
                    '  <div class="col-sm-12 bal-social-content-box">' +
                    '  <div class="row" data-social-type="instagram">' +
                    '<div class="col-md-12">'+
                    '<label class="small-title">Instagram</label>' +
                    '<input type="text" name="name" value="#" class="social-input" />' +
                    '<label class="checkbox-title">' +
                    '<input type="checkbox" name="name" /> Mostra' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '  <div class="row" data-social-type="facebook">' +
                    '<div class="col-md-12">'+
                    '  <label class="small-title">Facebook</label>' +
                    '  <input type="text" name="name" value="#" class="social-input" />' +
                    '<label class="checkbox-title">' +
                    '<input type="checkbox" name="name" checked /> Mostra' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '  <div class="row" data-social-type="twitter">' +
                    '<div class="col-md-12">'+
                    '<label class="small-title">Twitter</label>' +
                    '<input type="text" name="name" value="#" class="social-input" />' +
                    '<label class="checkbox-title">' +
                    '  <input type="checkbox" name="name" checked /> Mostra' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row" data-social-type="linkedin">' +
                    '<div class="col-md-12">'+
                    '<label class="small-title">Linkedin</label>' +
                    '<input type="text" name="name" value="#" class="social-input" />' +
                    '<label class="checkbox-title">' +
                    '<input type="checkbox" name="name" checked /> Mostra' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row" data-social-type="youtube">' +
                    '<div class="col-md-12">'+
                    '  <label class="small-title">Youtube</label>' +
                    '  <input type="text" name="name" value="#" class="social-input" />' +
                    '<label class="checkbox-title">' +
                    '  <input type="checkbox" name="name" checked /> Mostra' +
                    '  </label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row" data-social-type="skype">' +
                    '<div class="col-md-12">'+
                    '<label class="small-title">Skype</label>' +
                    '  <input type="text" name="name" value="#" class="social-input" />' +
                    '<label class="checkbox-title">' +
                    '  <input type="checkbox" name="name" checked /> Mostra' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '  </div>' +
                    '</li>' +
                    '<li class="bal-elements-accordion-item" data-type="youtube-frame">' +
                    '<a class="bal-elements-accordion-item-title">Youtube</a>' +
                    '<div class="bal-elements-accordion-item-content">' +
                    '<div class="bal-social-content-box ">' +
                    '<label>Youtube Url</label>' +
                    '<input type="text" class=" youtube" setting-type="">' +
                    '</div>' +
                    '</div>' +
                    '</li>' +
                    '  <li class="bal-elements-accordion-item" data-type="width">' +
                    '<a class="bal-elements-accordion-item-title">' + _this.langArr.propertyEmailWidth + '</a>' +
                    '<div class="bal-elements-accordion-item-content">' +
                    '  <div class="bal-social-content-box ">' +
                    '<label>Width</label>' +
                    '<input type="text" class="email-width number" setting-type="">' +
                    '<span class="help">' + _this.langArr.propertyEmailWidthHelp + '</span>' +
                    '  </div>' +
                    '</div>' +
                    '</li>' +
                    '<li class="bal-elements-accordion-item" data-type="image-settings">' +
                    '<a class="bal-elements-accordion-item-title">' + _this.langArr.propertyImageSettings + '</a>' +
                    '<div class="bal-elements-accordion-item-content">' +
                    '<div class="bal-social-content-box ">' +
                    '<div class="change-image">' + _this.langArr.propertyChangeImage + '</div>' +
                    '<label>Larghezza immagine</label>' +
                    '<input type="text" class="image-width  image-size " setting-type="" >' +
                    '<label>Altezza immagine</label>' +
                    '<input type="text" class="image-height  image-size" setting-type="">' +
                    '</div>' +
                    '</div>' +
                    '</li>' +
                    '<li class="bal-elements-accordion-item" data-type="hyperlink">' +
                    '<a class="bal-elements-accordion-item-title">' + _this.langArr.propertyHyperlink + '</a>' +
                    '<div class="bal-elements-accordion-item-content">' +
                    '<div class="bal-social-content-box ">' +
                    '<label>Url</label>' +
                    '<input type="text" class="hyperlink-url" setting-type="">' +
                    '</div>' +
                    '</div>' +
                    '  </li>' +
                    '<li class="bal-elements-accordion-item" data-type="button">' +
                    '<a class="bal-elements-accordion-item-title">' + _this.langArr.propertyButton + '</a>' +
                    '<div class="bal-elements-accordion-item-content">' +
                    '<div class="bal-social-content-box ">' +
                    '<label>' + _this.langArr.propertyButtonText + '</label>' +
                    '<input type="text" class="button-text" setting-type="">' +
                    '</div>' +
                    '<div class="bal-social-content-box ">' +
                    '<label>' + _this.langArr.propertyButtonLink + '</label>' +
                    '<input type="text" class="button-hyperlink" setting-type="">' +
                    '</div>' +
                    '<div class="bal-social-content-box ">' +
                    '<label>' + _this.langArr.propertyButtonTextColor + '</label><br>' +
                    ' <div id="button-text-color" class="bg-color bg-item" setting-type="">' +
                    '   <i class="fa fa-adjust"></i>' +
                    ' </div>' +
                    '</div><br>' +
                    '<div class="bal-social-content-box ">' +
                    '<label>' + _this.langArr.propertyButtonBackgroundColor + '</label>' +
                    colorSelect +
                    ' <div id="button-bg-color" class="bg-color bg-item" setting-type="">' +
                    '   <i class="fa fa-adjust"></i>' +
                    ' </div>' +
                    '</div>' +
                    '<div class="bal-social-content-box ">' +
                    '<label class="checkbox-title"><input type="checkbox" name="button-full-width" class="button-full-width"> ' + _this.langArr.propertyButtonFullSize + '</label>' +
                    '</div>' +


                    '</div>' +
                    '  </li>' +
                    '</ul>' +
                    '</div>';
                var _elementsContainer = '<div class="bal-elements-container">' + _tabElements + _tabProperty + '</div>';
                var _elements = '<div class="bal-elements">' + _elementsContainer + _settings + '</div>';

                var _outputSideBar = '<aside class="bal-left-menu-container clearfix">' + _nav + _elements + '</aside>';

                    var _outputContent = '<div class="bal-content">' +
                                        '<div data-css_replace="' + _this.getMainBackground(_this._loadPageHtml)[1] + '" style="background: ' + _this.getMainBackground(_this._loadPageHtml)[0] + ';" class="bal-content-wrapper" data-types="width,background">' +
                                          '<div class="bal-content-main lg-width">' +
                                            '<div class="email-editor-elements-sortable">' +
                                                _this.helperForGenerateHTMl(_this._loadPageHtml) +
                                            '</div>' +
                                          '</div>' +
                                        '</div>' +
                                      '</div>';
                var _outputHtml = '<div class="bal-editor-container clearfix"> ' + _outputSideBar + _outputContent + '</div>';
                _this.generatePopups();
                _this.display(_outputHtml);


                _this.default_func();
                _this.events();

            },
            /**
             * Generate popups html
             */
            generatePopups: function() {
                var _this = this;

                // Dato che basa 1 solo modal per tipo, se è stato già creato evito di ricrearlo.
                if($('body .email_editor_element').length) {
                    return true;
                }

                var _popupImagesContent = '<div class="modal fade popup_images email_editor_element" id="popup_images" role="dialog">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                    '<h4 class="modal-title">' + _this.langArr.popupImageTitle + '</h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    '<div class="row">' +
                    '<div class="col-sm-6">' +
                    '<input type="file" class="input-file" accept="image/*" >' +
                    '</div>' +
                    '<div class="col-sm-6">' +
                    '<button class="btn-upload">' + _this.langArr.popupImageUpload + '</button>' +
                    '</div>' +
                    '</div>' +
                    '<div class="upload-images">' +
                    ' <div class="row">     ' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-success btn-select" >' + _this.langArr.popupImageOk + '</button>' +
                    '<button type="button" class="btn btn-default" data-dismiss="modal">' + _this.langArr.popupImageClose + '</button> ' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';


                jQuery(_popupImagesContent).appendTo('body');

                var _popup_save_template = '<div class="modal fade  email_editor_element" id="popup_save_template" role="dialog">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                    '<h4 class="modal-title">' + _this.langArr.popupSaveTemplateTitle + '<br>' +
                    '<small>' + _this.langArr.popupSaveTemplateSubTitle + '</small></h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    '<div class="row">' +
                    '<div class="col-sm-12">' +
                    '<input type="text" class="form-control template-name" placeholder="' + _this.langArr.popupSaveTemplatePLaceHolder + '"  >' +
                    '<br>' +
                    '<label class="input-error" style="color:red"></label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-success btn-save-template" >' + _this.langArr.popupSaveTemplateOk + '</button>' +
                    '<button type="button" class="btn btn-default" data-dismiss="modal">' + _this.langArr.popupSaveTemplateClose + '</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                jQuery(_popup_save_template).appendTo('body');

                var _popup_editor = '<div class="modal fade modal-wide email_editor_element" id="popup_editor" role="dialog">' +
                    '<div class="modal-dialog modal-lg">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                    '<h4 class="modal-title">' + _this.langArr.popupEditorTitle + '<br/>' +
                    '  <small></small></h4>' +
                    '  </div>' +
                    '<div class="modal-body">' +
                    '<div id="editorHtml" class="">' +
                    '</div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-success btn-save-editor" >' + _this.langArr.popupEditorOk + '</button>' +
                    '<button type="button" class="btn btn-default" data-dismiss="modal">' + _this.langArr.popupEditorClose + '</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

                jQuery(_popup_editor).appendTo('body');


                var _popup_send_email = '<div class="modal fade email_editor_element" id="popup_send_email" role="dialog">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                    '<h4 class="modal-title">' + _this.langArr.popupSendEmailTitle + '<br>' +
                    '<small>' + _this.langArr.popupSendEmailSubTitle + '</small></h4>' +
                    '  </div>' +
                    '<div class="modal-body">' +
                    '  <div class="row">' +
                    '<div class="col-sm-12">' +
                    '  <input type="email" class="form-control recipient-email" placeholder="' + _this.langArr.popupSendEmailPlaceHolder + '"  >' +

                    ' <br> <label>Select attachment</label><input type="file" id="send_attachments" multiple=""  />' +
                    '<br>' +
                    '<label class="popup_send_email_output" style="color:red"></label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-success btn-send-email-template" >' + _this.langArr.popupSendEmailOk + '</button>' +
                    '<button type="button" class="btn btn-default" data-dismiss="modal">' + _this.langArr.popupSendEmailClose + '</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

                jQuery(_popup_send_email).appendTo('body');

                var _popup_demo = '<div class="modal fade email_editor_element" id="popup_demo" role="dialog">' +
                    '<div class="modal-dialog">' +
                    '  <div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                    '<h4 class="modal-title">Demo<br>' +
                    '</h4>' +
                    '  </div>' +
                    '<div class="modal-body">' +
                    '<label  style="color:red">This is demo version. There is not access to use more.' +
                    'If you want to more please buy plugin.<a href="https://codecanyon.net/item/bal-email-newsletter-builder-php-version/18060733" title="Buy">Buy Plugin</a></label>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                    '  </div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

                jQuery(_popup_demo).appendTo('body');

                var _popup_load_template = '<div class="modal fade email_editor_element" id="popup_load_template" role="dialog">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content modal-md">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                    '<h4 class="modal-title">' + _this.langArr.popupLoadTemplateTitle + '<br>' +
                    '<small>' + _this.langArr.popupLoadTemplateSubtitle + '</small></h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    '<div class="template-list">' +
                    '</div>' +
                    '<label class="template-load-error" style="color:red"></label>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-success btn-load-template" >' + _this.langArr.popupLoadTemplateOk + '</button>' +
                    '<button type="button" class="btn btn-default" data-dismiss="modal">' + _this.langArr.popupLoadTemplateClose + '</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

                jQuery(_popup_load_template).appendTo('body');

            },
            /**
             * show output in page
             */
            display: function(data) {
                var _this = this;
                _this.$elem.html(data);
            },
            /**
             * this functions must be work after generate source code
             */
            default_func: function() {
                var _this = this;
                //Nicescroll
                /*
                _this.$elem.find(".bal-elements-container").niceScroll({
                    cursorcolor: "#5D8397",
                    cursorwidth: "10px",
                    background: "#253843"
                });
                */
                //make bootstrap tooltip
                jQuery('[data-toggle="tooltip"]').tooltip();

                // set colorpicker  default values and chnaged events
                _this.$elem.find('.defaultColorsSelect').on('change', function() {
                    var value = $(this).val();
                    var $bg_color = $(this).parent().find('.bg-color');

                    if($(this).val() == 'custom') {
                        $(this).addClass('custom');
                        if($bg_color.attr('id') == 'bg-color') {
                            _this.getActiveElementContent().removeAttr('data-css_replace');
                            _this.getActiveElementContent().css('background-color', 'white');
                        }
                        else {
                            _this.getActiveElementContent('a').removeAttr('data-css_replace');
                            _this.getActiveElementContent('a').css('background-color', 'white');
                        }
                    }
                    else {

                        value = parseInt(value)+1;
                        $(this).removeClass('custom');

                        var color = $(this).find('option:selected').data('color');

                        if($bg_color.attr('id') == 'bg-color') {
                            _this.changeSettings($('#bg-color').attr('setting-type'), color);
                            _this.getActiveElementContent().attr('data-css_replace', '[background|{color_' + value + '}]');
                            $(this).parent().find('.bg-color').css('background-color', color);
                        }
                        else {
                            _this.getActiveElementContent('a').css('background-color', color);
                            _this.getActiveElementContent('a').attr('data-css_replace', '[background|{color_' + value + '}]');
                        }

                    }

                });

                _this.$elem.find('#bg-color').ColorPicker({
                    color: '#fff',
                    onChange: function(hsb, hex, rgb) {
                        $('#bg-color').css('background-color', '#' + hex);
                        _this.changeSettings($('#bg-color').attr('setting-type'), '#' + hex);
                    }
                });
                _this.$elem.find('#button-bg-color').ColorPicker({
                    color: '#3498DB',
                    onChange: function(hsb, hex, rgb) {
                        $('#button-bg-color').css('background-color', '#' + hex);
                        _this.getActiveElementContent('a').css('background-color', '#' + hex);
                        //_this.changeSettings($('#bg-color').attr('setting-type'), '#' + hex);
                    }
                });
                _this.$elem.find('#button-text-color').ColorPicker({
                    color: '#fff',
                    onChange: function(hsb, hex, rgb) {
                        $('#button-text-color').css('background-color', '#' + hex);
                        _this.getActiveElementContent('a').css('color', '#' + hex);
                        //_this.changeSettings($('#bg-color').attr('setting-type'), '#' + hex);
                    }
                });
                //button-bg-color
                _this.$elem.find('#text-color').ColorPicker({
                    color: '#000',
                    onChange: function(hsb, hex, rgb) {
                        $('#text-color').css('background-color', '#' + hex);
                        _this.changeSettings($('#text-color').attr('setting-type'), '#' + hex);
                    }
                });

                //show content edit on page load
                setTimeout(function() {
                    jQuery('.bal-content-wrapper').click();
                    _this.tabMenu('typography');
                }, 100);
                _this.makeSortable();
                //_this.tabMenu('typography');

                _aceEditor = ace.edit("editorHtml");
                _aceEditor.setTheme("ace/theme/monokai");
                _aceEditor.getSession().setMode("ace/mode/html");

                _this.remove_row_elements();

                $('.bal-content-wrapper').removeAttr('contenteditable');
                $('.bal-content-wrapper .element-contenteditable').removeClass('element-contenteditable');
                $('.bal-content-wrapper .active').removeClass('active');

                _this.commandsUndoManager();
            },
            getMainBackground:function (html) {
                var _this = this;
                var _loadTempLoad= $('<div/>');
                _loadTempLoad.html(html);
                return [_loadTempLoad.find(' > table').css('background'), _loadTempLoad.find(' > table').data('css_replace')];
            },
            helperForGenerateHTMl:function (html) {
                var _this = this;
                var _loadTempLoad = $('<div/>');
                _loadTempLoad.html(html);
                var _contentOfLoad = '';
                _loadTempLoad.find('.main').each(function (index, item) {
                    _contentOfLoad += '<div class="sortable-row">' +
                          '<div class="sortable-row-container">' +
                          ' <div class="sortable-row-actions">';

                  _contentOfLoad += '<div class="row-move row-action fa fa-arrows-alt"></div>';


                  _contentOfLoad += '<div class="row-remove row-action fa fa-remove"></div>';


                  _contentOfLoad += '<div class="row-duplicate row-action fa fa-files-o"></div>';


                  _contentOfLoad += '<div class="row-code row-action fa fa-code"></div>';

                  _contentOfLoad += '</div>' +

                  '<div class="sortable-row-content" >' +$('<div/>').html(item).html()+

                  '</div></div></div>';

              });

              if(_loadTempLoad.children().length && !_contentOfLoad.length) {
                  bootbox.error("È stato caricato un template obsoleto. Pertanto prima di salvare le modifiche assicurati di crearne uno nuovo o andrà perso.")
              }

              return _contentOfLoad;
            },
            /**
             * Remove row buttons
             */
            remove_row_elements: function() {
                var _this = this;

                jQuery('.sortable-row-actions').html('');
                if (_this.config.showRowMoveButton == false) {
                    jQuery('.row-move').remove();
                } else {
                    jQuery('.sortable-row-actions').append('<div class="row-move row-action fa fa-arrows-alt"></div>');
                }

                if (_this.config.showRowRemoveButton == false) {
                    jQuery('.row-remove').remove();
                } else {
                    jQuery('.sortable-row-actions').append('<div class="row-remove row-action fa fa-remove"></div>');
                }

                if (_this.config.showRowDuplicateButton == false) {
                    jQuery('.row-duplicate').remove();
                } else {
                    jQuery('.sortable-row-actions').append('<div class="row-duplicate row-action fa fa-files-o"></div>');
                }

                if (_this.config.showRowCodeEditorButton == false) {
                    jQuery('.row-code').remove();
                } else {
                    jQuery('.sortable-row-actions').append('<div class="row-code row-action fa fa-code"></div>');
                }
            },
            /**
             *  Get content active element for change setting
             */
            getActiveElementContent: function(child) {
                if(typeof(child) === 'undefined') {
                    child = '';
                }

                var _this = this;
                var _element = _this.$elem.find('.sortable-row.active .sortable-row-content .element-content.active');

                if(child !== '') {
                    if(tinymce["activeEditor"]) {
                        tinymce["activeEditor"]["tmp_observer"].observe(tinymce["activeEditor"]["tmp"][0], { attributes: true, childList: true, subtree: true });
                        return tinymce["activeEditor"]["tmp"].find(child);
                    } else {
                        return _element.find(child);
                    }
                }

                //element-contenteditable active
                if (_element.parent().find('[contenteditable="true"]').hasClass('element-contenteditable')) {
                    _element = _element.parent().find('.element-contenteditable.active');
                }

                if (_this.$elem.find('.bal-content-wrapper').hasClass('active')) {
                    _element = _this.$elem.find('.bal-content-wrapper');
                }

                return _element;
            },
            /**
             *  Make content elements sortable
             */
            makeSortable: function() {
                var _this = this;

                _this.$elem.find(".email-editor-elements-sortable").sortable({
                    placeholder: "editor-elements-placeholder",
                    forcePlaceholderSize: true,
                    //group: 'no-drop',
                    handle: '.row-move',
                    revert: false
                });
            },

            composeShortcodesView:  function() {
                var _this = this;


                if(_this.config.shortcodesList.length > 0 && typeof(_this.config.shortcodesList['custom']) == 'undefined') {
                    _this.config.shortcodesList = {global: [], custom: _this.config.shortcodesList};
                }

                if(!_this.config.shortcodesList['global'].length && !_this.config.shortcodesList['custom'].length) {
                    return false;
                }

                $(_this.$elem).find('.element-content mark').each(function () {
                   $(this).replaceWith($(this).html());
                });

                $(_this.$elem).find('.element-content > *').each(function() {

                    var $table = $(this);
                    var src_str = $table.html();

                    var initial_html = src_str;

                    if(typeof(_this.config.shortcodesList['custom']) !== 'undefined' && _this.config.shortcodesList['custom'].length) {
                        var pattern = new RegExp("([^\[\|\"]|^)(?!(" + _this.config.shortcodesList['custom'].concat(_this.config.shortcodesList['global']).join('|').replace(/{/g, '{\\b') + ")){(?!\\[|\\])(.+?)}([^\[\|\"]|$)", "gi");
                        src_str = src_str.replace(pattern, "$1<mark class=\"no_avaialable\">{$3}</mark>$4");
                    }

                    if(typeof(_this.config.shortcodesList['global']) !== 'undefined' && _this.config.shortcodesList['global'].length) {
                        if (_this.config.shortcodesList['global'].length) {
                            pattern = new RegExp("([^\[\|\"]|^)(" + _this.config.shortcodesList['global'].join('|').replace(/{/g, '{\\b') + ")([^\[\|\"]|$)", "gi");
                            src_str = src_str.replace(pattern, "$1<mark class=\"global_shortcode\">$2</mark>$3");
                        }
                    }

                    if(typeof(_this.config.shortcodesList['custom']) !== 'undefined' && _this.config.shortcodesList['custom'].length) {
                        pattern = new RegExp("([^\[\|\"]|^)(" + _this.config.shortcodesList['custom'].join('|').replace(/{/g, '{\\b') + ")([^\[\|\"]|$)", "gi");
                        src_str = src_str.replace(pattern, "$1<mark class=\"avaialable\">$2</mark>$3");
                    }

                    pattern = new RegExp("{[[|\\]](.+?)[[|\\]]}", "gi");
                    src_str = src_str.replace(pattern, "<mark class=\"array\">{[$1]}</mark>");

                    if(initial_html != src_str) {
                        $table.html(src_str);
                    }

                });
            },

            /**
             *  All events
             */
            events: function() {
                var _this = this;

                jQuery(function() {
                    if (_this.config.onAfterLoad !== undefined) {
                        _this.config.onAfterLoad();
                    }
                    _this.makeSortable();
                    // setTimeout(function() {
                    //     _this.makeSortable();
                    // }, 2000);
                    jQuery('.bal-content-main').attr('data-width', '600px');
                    jQuery('.main').css('width', '600px');
                });

                //left menu tab click
                _this.$elem.find('.tab-selector').on('click', function() {
                    var _element = $(this);
                    _this.tabMenuItemClick(_element, true);
                });
                //menu accordion click
                _this.$elem.find('.bal-elements-accordion .bal-elements-accordion-item-title').on('click', function(j) {
                    var _element = $(this);
                    _this.menuAccordionClick(_element, true);
                });

                _this.$elem.find('.bal-collapse').on('click', function() {
                    var _element = $(this);
                    var _dataValue = _element.attr('data-value');
                    //console.log(_dataValue);
                    if (_dataValue === 'closed') {
                        _this.$elem.find('.bal-elements').show();
                        _this.$elem.find('.bal-left-menu-container').animate({
                            width: 380
                        }, 300, function() {
                            _this.$elem.find('.bal-content').css({
                                'padding-left': '380px'
                            });
                            _this.$elem.find('.bal-editor-container .bal-content .bal-content-wrapper').css({
                                'width': 'calc(100% - 380px)'
                            });
                            _this.$elem.find('.bal-left-menu-container').find('.bal-menu-item:eq(0)').addClass('active');
                        });
                        _element.find('.bal-menu-name').show();
                        _element.find('.fa').removeClass().addClass('fa fa-chevron-circle-left');
                        _element.attr('data-value', 'opened');
                    } else {
                        _this.$elem.find('.bal-left-menu-container').animate({
                            width: 70
                        }, 300, function() {
                            _this.$elem.find('.bal-elements').hide();
                            _this.$elem.find('.bal-left-menu-container').find('.bal-menu-item.active').removeClass('active');
                        });
                        _this.$elem.find('.bal-content').css({
                            'padding-left': '70px'
                        });
                        _this.$elem.find('.bal-editor-container .bal-content .bal-content-wrapper').css({
                            'width': 'calc(100% - 70px)'
                        });
                        _element.find('.bal-menu-name').hide();
                        _element.find('.fa').removeClass().addClass('fa fa-chevron-circle-right');
                        _element.attr('data-value', 'closed');
                    }
                });

                _this.$elem.find('.blank-page').on('click', function() {
                    var _element = $(this);
                    //console.log(_this.langArr.modalDeleteTitle);

                    bootbox.confirm(_this.langArr.modalDeleteText, function (result) {
                        if (result) {
                            _this.resetHtmlContent();
                        }
                    });
                });

                _this.$elem.find('.bal-elements-container .sortable-row-content').each(function(i) {
                    var _element = $(this);
                    (function(_element) {
                        /*
                        $.get(_element.attr('data-url'), function(responseText) {
                            _element.html(responseText.split('[site-url]').join(_this.getAbsolutePath()));
                        });
                        */
                    }(_element));
                });

                _this.$elem.find(".bal-elements-list .bal-elements-list-item").draggable({
                    connectToSortable: ".email-editor-elements-sortable",
                    helper: "clone",
                    //revert: "invalid",
                    create: function(event, ui) {
                        //console.log(event.target);

                    },
                    drag: function(event, ui) {
                    },
                    start: function(event, ui) {
                        _this.$elem.find(".bal-elements-container").css({
                            'overflow': ''
                        });
                        ui.helper.find('.bal-preview').hide();
                        ui.helper.find('.bal-view').show()
                        //$(this).find('.demo').show();

                        var $block = $(ui.helper);
                        if(!$(ui.helper).hasClass('loaded'))
                        {
                            $.get($block.find('[data-url]').attr('data-url'), function(responseText) {
                                $block.find('[data-url]').html(responseText.split('[site-url]').join(_this.getAbsolutePath()));
                                $block.addClass('loaded');
                            });
                        }

                        if (_this.config.onElementDragStart !== undefined) {
                            _this.config.onElementDragStart(event);
                        }

                        if(!_this.$elem.find('.bal-content-main .sortable-row').length) {
                            _this.resetHtmlContent();
                        }
                    },
                    stop: function(event, ui) {

                        _this.$elem.find(".bal-elements-container").css({
                            'overflow': 'hidden'
                        });

                        ui.helper.html(ui.helper.find('.bal-view').html());
                        //ui.helper.remove();
                        //_this.$elem.find('.email-editor-elements-sortable').append(ui.helper.find('.bal-view').html());
                        _this.$elem.find('.email-editor-elements-sortable .bal-elements-list-item').css({
                            'width': '',
                            'height': ''
                        });
                        //
                        //$(ui.helper).remove();
                        var contentHtml = _this.getContentHtml();
                        if (_this.config.onElementDragFinished !== undefined) {
                            _this.config.onElementDragFinished(event, contentHtml);
                        }


                    }
                });

                _this.$elem.on('click', '.bal-content-wrapper', function(event) {

                    _this.removeTinyMCE();

                    _this.$elem.find('.element-content').show();

                    _this.$elem.find('.sortable-row.active').removeClass('active');
                    _this.$elem.find('.element-content.active').removeClass('active');
                    _this.$elem.find('.divider-simple.active').removeClass('active');
                    _this.$elem.find('.sortable-row .element-contenteditable.active').removeClass('element-contenteditable').removeClass('active').removeAttr('contenteditable');
                    jQuery(this).addClass('active');

                    var _dataTypes = jQuery(this).attr('data-types');
                    if (_dataTypes.length < 1) {
                        return;
                    }
                    var _typeArr = _dataTypes.toString().split(',');
                    var _arrSize = _this.$elem.find('.tab-property .bal-elements-accordion-item').length;
                    var _accordionMenuItem = '';
                    for (var i = 0; i < _arrSize; i++) {
                        _accordionMenuItem = _this.$elem.find('.tab-property .bal-elements-accordion-item').eq(i);
                        //console.log(_accordionMenuItem.attr('data-type'))
                        if (_dataTypes.indexOf(_accordionMenuItem.attr('data-type')) > -1) {
                            _accordionMenuItem.show();
                        } else {
                            _accordionMenuItem.hide();
                        }
                    }


                    _this.$elem.find('[setting-type="padding-top"]').val(jQuery(this).css('padding-top').replace('px', ''));
                    _this.$elem.find('[setting-type="padding-bottom"]').val(jQuery(this).css('padding-bottom').replace('px', ''));
                    _this.$elem.find('[setting-type="padding-left"]').val(jQuery(this).css('padding-left').replace('px', ''));
                    _this.$elem.find('[setting-type="padding-right"]').val(jQuery(this).css('padding-right').replace('px', ''));

                    //  _this.$elem.find('.email-width').val(jQuery('.bal-content-main').width());
                    _this.$elem.find('.email-width').val(jQuery('.main').width() == '100' ? '600' : jQuery('.main').width());

                    _this.tabMenu(_typeArr[0]);
                    _this.getSettings();
                    _this.composeShortcodesView();
                });

                _this.$elem.on('click', 'a', function(e) {
                    e.preventDefault();
                });

                _this.events_of_row();

                _this.events_of_property();

                _this.events_of_popup();

                _this.events_of_setting();

                _this.remove_row_elements();
            },
            /**
             *  Events of row
             */
            events_of_row: function() {
                var _this = this;
                //remove button
                _this.$elem.on('click', '.sortable-row .row-remove', function(e) {

                    if (_this.config.onBeforeRemoveButtonClick !== undefined) {
                        _this.config.onBeforeRemoveButtonClick(e);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (e.isDefaultPrevented() == true) {
                        return false;
                    }

                    if (_this.$elem.find('.bal-content .sortable-row').length == 1) {
                        alert('Deve essere presente almeno 1 elemento');
                        return;
                    }
                    jQuery(this).parents('.sortable-row').remove();

                    if (_this.config.onAfterRemoveButtonClick !== undefined) {
                        _this.config.onAfterRemoveButtonClick(e);
                    }
                });

                //duplicate button
                _this.$elem.on('click', '.sortable-row .row-duplicate', function(e) {
                    if (_this.config.onBeforeRowDuplicateButtonClick !== undefined) {
                        _this.config.onBeforeRowDuplicateButtonClick(e);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (e.isDefaultPrevented() == true) {
                        return false;
                    }

                    _this.removeTinyMCE();

                    if (jQuery(this).hasParent('.bal-elements-list-item')) {
                        var _parentSelector = '.bal-elements-list-item';
                    } else {
                        var _parentSelector = '.sortable-row';
                    }
                    var _parent = jQuery(this).parents(_parentSelector);
                    jQuery('.sortable-row').removeClass('active');
                    jQuery('.bal-elements-list-item').removeClass('active');
                    _parent.addClass('active');
                    //_parent.after('<div class="sortable-row">'+ _parent.html()+"</div>");
                    var $clonedElement = _parent.clone();
                    $clonedElement.find('input').remove();
                    $clonedElement.insertAfter(_parentSelector + '.active');

                    if (_this.config.onAfterRowDuplicateButtonClick !== undefined) {
                        _this.config.onAfterRowDuplicateButtonClick(e);
                    }
                });

                //code button . for showing code editor popup
                _this.$elem.on('click', '.sortable-row .row-code', function(e) {
                    if (_this.config.onBeforeRowEditorButtonClick !== undefined) {
                        _this.config.onBeforeRowEditorButtonClick(e);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (e.isDefaultPrevented() == true) {
                        return false;
                    }
                    jQuery(this).parents('.sortable-row').addClass('code-editor');
                    var _html = jQuery(this).parents('.sortable-row').find('.sortable-row-content').html();
                    _aceEditor.session.setValue(_html);

                    if (_this.config.onAfterRowEditorButtonClick !== undefined) {
                        _this.config.onAfterRowEditorButtonClick(e);
                    }

                    if (_this.config.onBeforeShowingEditorPopup !== undefined) {
                        _this.config.onBeforeShowingEditorPopup(e);
                    }
                    if (e.isDefaultPrevented() == true) {
                        return false;
                    }
                    jQuery('#popup_editor').modal('show');
                });

                _this.$elem.on('click', '.main td', function(event) {

                    if(!$(this).hasClass('active')) {
                        _this.sortableClick(jQuery(this));
                    }
                    event.stopPropagation();
                });

                _this.$elem.on('click', '[contenteditable="true"]', function(event) {
                    event.stopPropagation();
                });
            },
            /**
             *  Events for Property
             */
            events_of_property: function() {
                var _this = this;
                //email width of template
                _this.$elem.on('keyup', '.email-width', function(event) {
                    var _element = jQuery(this);
                    var _val = jQuery('.email-width').val();
                    if (parseInt(_val) < 300 || parseInt(_val) > 1000) {
                        return false;
                    }
                    //jQuery('.bal-content-main').css('width', _val + 'px');
                    jQuery('.bal-content-main').attr('data-width', _val);
                    jQuery('.main').css('width', _val + 'px');
                });

                //hyperlink
                _this.$elem.on('click', 'a.hyperlink', function(event) {
                    var _element = jQuery(this);

                    var _href = _element.attr('href');
                    var _linkElement = jQuery('.bal-elements-accordion-item-content .hyperlink-url');

                    _linkElement.val(_href);
                });
                _this.$elem.on('keyup', '.bal-elements-accordion-item-content .hyperlink-url', function(event) {
                    var _element = jQuery(this);
                    var _val = _element.val();
                    var _activeElement = _this.getActiveElementContent('a');
                    _activeElement.attr('href', _val);
                });
                _this.$elem.on('keyup', '.bal-elements-accordion-item-content .button-text', function(event) {
                    var _element = jQuery(this);
                    var _val = _element.val();
                    var _activeElement = _this.getActiveElementContent('a');
                    _activeElement.text(_val);
                });
                _this.$elem.on('click', '[data-types*="button"] .element-content', function(event) {
                    var _element = jQuery(this).find('a.button-1');

                    if(!_element.length) {
                        return false;
                    }

                    var _val = jQuery.trim(_element.text());
                    var _href = _element.attr('href');
                    var _textElement = jQuery('.bal-elements-accordion-item-content .button-text');
                    var _linkElement = jQuery('.bal-elements-accordion-item-content .button-hyperlink');

                    _textElement.val(_val);
                    _linkElement.val(_href);
                });
                _this.$elem.on('keyup', '.bal-elements-accordion-item-content .button-hyperlink', function(event) {
                    var _element = jQuery(this);
                    var _val = _element.val();
                    var _activeElement = _this.getActiveElementContent('a');
                    _activeElement.attr('href', _val);
                });
                _this.$elem.on('change', '.bal-elements-accordion-item-content .button-full-width', function(event) {
                    var _activeElement = _this.getActiveElementContent('a');
                    if ($('.button-full-width').is(':checked') == true) {
                        _activeElement.css('display', 'block');
                    } else {
                        _activeElement.css('display', 'inline-block');
                    }
                });

                //youtube
                _this.$elem.on('keyup', '.bal-elements-accordion-item-content .youtube', function(event) {
                    var _element = jQuery(this);
                    var _val = _element.val();
                    var _activeElement = _this.getActiveElementContent('a');
                    _activeElement.attr('href', _val);
                    _activeElement.find('table').css('background-image', "url('https://img.youtube.com/vi/" + _this.getYoutubeVideoId(_val) + "/sddefault.jpg')");
                });

                //text style
                _this.$elem.on('click', '.bal-text-icons .bal-icon-box-item', function(event) {
                    var _element = jQuery(this);
                    if (_element.hasClass('active')) {
                        _element.removeClass('active');
                    } else {
                        _element.addClass('active');
                    }
                    if (_this.$elem.find('.bal-text-icons .bal-icon-box-item.fontStyle').hasClass('active')) {
                        _this.changeSettings('font-style', 'italic');
                    } else {
                        _this.changeSettings('font-style', '');
                    }
                    var _value = '';
                    if (_this.$elem.find('.bal-text-icons .bal-icon-box-item.underline').hasClass('active')) {
                        _value += 'underline ';
                    }
                    if (_this.$elem.find('.bal-text-icons .bal-icon-box-item.line').hasClass('active')) {
                        _value += ' line-through';
                    }
                    _this.changeSettings('text-decoration', _value);
                });


                //font
                _this.$elem.on('click', '.bal-font-icons .bal-icon-box-item', function(event) {
                    var _element = jQuery(this);
                    if (_element.hasClass('active')) {
                        _element.removeClass('active');
                    } else {
                        _element.addClass('active');
                    }
                    if (_this.$elem.find('.bal-font-icons .bal-icon-box-item').hasClass('active')) {
                        _this.changeSettings('font-weight', 'bold');
                    } else {
                        _this.changeSettings('font-weight', '');
                    }
                });

                //align
                _this.$elem.on('click', '.bal-align-icons .bal-icon-box-item', function(event) {
                    var _element = jQuery(this);
                    _this.$elem.find('.bal-align-icons .bal-icon-box-item').removeClass('active');
                    _element.addClass('active');
                    var _value = 'left';
                    if (_this.$elem.find('.bal-align-icons .bal-icon-box-item.center').hasClass('active')) {
                        _value = 'center';
                    }
                    if (_this.$elem.find('.bal-align-icons .bal-icon-box-item.right').hasClass('active')) {
                        _value = 'right';
                    }
                    _this.changeSettings('text-align', _value);
                });


                //chnage form cpntrol value for select
                _this.$elem.on('change', '.bal-left-menu-container  .form-control', function(event) {
                    var _element = jQuery(this);
                    _this.changeSettings(_element.attr('setting-type'), _element.val());
                });

                //chnage form cpntrol value for input
                _this.$elem.on('keyup', '.bal-left-menu-container .form-control', function(event) {
                    var _element = jQuery(this);
                    if (_element.hasClass('all') && _element.hasClass('padding')) {
                        _this.$elem.find('.padding:not(".all")').val(_element.val());
                    }
                    if (_element.hasClass('all') && _element.hasClass('border-radius')) {
                        _this.$elem.find('.border-radius:not(".all")').val(_element.val());
                    }
                    _this.changeSettings(_element.attr('setting-type'), _element.val() + 'px');
                });

                //social
                _this.$elem.on('keyup', '.bal-social-content-box .social-input', function(event) {
                    var _element = jQuery(this);
                    var _socialType = _element.parents('.row').attr('data-social-type');
                    var _val = _element.val();
                    var _activeElement = _this.getActiveElementContent();
                    if (_activeElement.hasClass('social-content')) {
                        _this.getActiveElementContent('a.' + _socialType).attr('href', _val);
                    }
                });

                //image-size
                _this.$elem.on('keyup', '.image-size', function(event) {
                    var _activeElement = _this.getActiveElementContent('.content-image');

                    if(_activeElement.find('.content-image').length) {
                        _activeElement = _activeElement.find('.content-image');
                    }

                    if (jQuery(this).hasClass('image-height')) {
                        _activeElement.css('height', jQuery(this).val());
                    } else if (jQuery(this).hasClass('image-width')) {
                        _activeElement.css('width', jQuery(this).val());
                    }

                });

                //number
                _this.$elem.on('keydown', '.number', function(e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1
                        //// Allow: Ctrl+A
                        //(e.keyCode == 65 && e.ctrlKey === true) ||
                        //// Allow: Ctrl+C
                        //(e.keyCode == 67 && e.ctrlKey === true) ||
                        //// Allow: Ctrl+X
                        //(e.keyCode == 88 && e.ctrlKey === true) ||
                        //// Allow: home, end, left, right
                        //(e.keyCode >= 35 && e.keyCode <= 39)
                    ) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });

                //example
                _this.$elem.on('change', '.bal-social-content-box .checkbox-title input', function(event) {
                    var _socialType = jQuery(this).parents('.row').attr('data-social-type');
                    var _activeElement = _this.getActiveElementContent('a.' + _socialType);
                    if (jQuery(this).is(":checked")) {
                        _activeElement.show();
                    } else {
                        _activeElement.hide();
                    }
                });


            },
            /**
             * Get video id from youtube url
             */
            getYoutubeVideoId: function(url) {
                var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
                if (videoid != null) {
                    return videoid[1];
                } else {
                    return '';
                }

            },
            /**
             *  Events for Settings
             */
            events_of_setting: function() {
                var _this = this;

                //other-devices
                _this.$elem.on('click', '.bal-setting-item.other-devices', function(event) {
                    var _element = jQuery(this);
                    var _parent = _element.parents('.bal-settings');
                    if (_element.hasClass('active')) {
                        _parent.find('.bal-setting-content .bal-setting-content-item.other-devices').hide();
                        _element.removeClass('active');
                    } else {
                        _parent.find('.bal-setting-content .bal-setting-content-item.other-devices').show();
                        _element.addClass('active');
                    }
                });

                //other devices content
                _this.$elem.on('click', '.bal-setting-content .bal-setting-device-tab', function(event) {
                    var _element = jQuery(this);

                    var _parent = _element.parents('.bal-setting-content');
                    _parent.find('.bal-setting-device-tab').removeClass('active');
                    _element.addClass('active');
                    _parent.find('.bal-setting-device-content').removeClass('active');
                    _parent.find('.bal-setting-device-content.' + _element.attr('data-tab')).addClass('active');
                    var _removeClass = 'sm-width lg-width';
                    var _addClass = '';
                    switch (_element.attr('data-tab')) {
                        case 'mobile-content':
                            _addClass = 'sm-width';
                            _this.$elem.find('.email-editor-elements-sortable').css({
                                'margin-left': '-150px'
                            });
                            break;
                        case 'desktop-content':
                            _addClass = 'lg-width';
                            _this.$elem.find('.email-editor-elements-sortable').css({
                                'margin-left': '0'
                            });
                            break;
                    }
                    _this.$elem.find('.bal-content-main').removeClass(_removeClass);
                    _this.$elem.find('.bal-content-main').addClass(_addClass);
                });

                //laod templates button
                _this.$elem.on('click', '.bal-setting-item.load-templates', function(event) {

                    if (_this.config.onBeforeSettingsLoadTemplateButtonClick !== undefined) {
                        _this.config.onBeforeSettingsLoadTemplateButtonClick(event);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }
                    jQuery('.btn-load-template').hide();
                    $('#popup_load_template').modal('show');
                });

                //export click
                _this.$elem.on('click', '.bal-setting-item.export', function(event) {
                    var _getHtml = _this.getContentHtml();
                    if (_this.config.onSettingsExportButtonClick !== undefined) {
                        _this.config.onSettingsExportButtonClick(event, _getHtml);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }
                });

                //preview click
                _this.$elem.on('click', '.bal-setting-item.preview', function(event) {
                    var _getHtml = _this.getContentHtml();
                    if (_this.config.onSettingsPreviewButtonClick !== undefined) {
                        _this.config.onSettingsPreviewButtonClick(event, _getHtml);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }
                });

                //save-template click
                _this.$elem.on('click', '.bal-setting-item.save-template', function(event) {
                    if (_this.config.onBeforeSettingsSaveButtonClick !== undefined) {
                        _this.config.onBeforeSettingsSaveButtonClick(event);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }


                    jQuery('.input-error').text('');
                    jQuery('.template-name').val('');
                    jQuery('#popup_save_template').modal('show');
                });


                //btn-save-template
                jQuery('#popup_save_template').on('click', '.btn-save-template', function(event) {

                    jQuery('.input-error').text('');
                    if (jQuery('.template-name').val().length < 1) {
                        jQuery('.input-error').text(_this.langArr.popupSaveTemplateError);
                        return false;
                    }

                    if (_this.config.onPopupSaveButtonClick !== undefined) {
                        _this.config.onPopupSaveButtonClick(event);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }

                });

                //btn-save-template
                jQuery(document).on('click', '.bal-btn-save', function(event) {


                    if (_this.config.onUpdateButtonClick !== undefined) {
                        _this.config.onUpdateButtonClick(event);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }

                });
                //send-email
                _this.$elem.on('click', '.bal-setting-item.send-email', function(event) {
                    event.preventDefault();

                    if (_this.config.onSettingsSendMailButtonClick !== undefined) {
                        _this.config.onSettingsSendMailButtonClick(event);
                    }

                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }
                    jQuery('.recipient-email').val('');
                    jQuery('#send_attachments').val('');
                    jQuery('.popup_send_email_output').text('');
                    jQuery('#popup_send_email').modal('show');

                });

                jQuery('#popup_send_email').on('click', '.btn-send-email-template', function(event) {
                    var _element = $(this);
                    if ($(this).hasClass('has-loading')) {
                        return;
                    }
                    _element.addClass('has-loading');
                    _element.text(_this.langArr.loading);

                    var _getHtml = _this.getContentHtml();

                    if (_this.config.onPopupSendMailButtonClick !== undefined) {
                        _this.config.onPopupSendMailButtonClick(event, _getHtml);
                    }
                    //if user want stop this action : e.preventDefault();
                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }

                });
                //open modal for change image
                _this.$elem.on('click', '.change-image', function(event) {
                  $('.mce-resizehandle').remove();
                    if (_this.config.onBeforeChangeImageClick !== undefined) {
                        _this.config.onBeforeChangeImageClick(event);
                    }

                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }
                    jQuery('#popup_images').modal('show');
                    jQuery('#popup_images').css('z-index','999999')

                });
                //select image
                jQuery('#popup_images').on('click', '.upload-image-item', function(event) {
                    jQuery('.modal .upload-image-item').removeClass('active');
                    jQuery(this).addClass('active');
                });

                //change select image button click
                jQuery('#popup_images').on('click', '.btn-select', function(event) {

                    if (_this.config.onBeforePopupSelectImageButtonClick !== undefined) {
                        _this.config.onBeforePopupSelectImageButtonClick(event);
                    }

                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }


                    var _url = jQuery('.modal').find('.upload-image-item.active').attr('src');
                    _this.getActiveElementContent('.content-image').attr('src', _url);
                    jQuery('#popup_images').modal('hide');
                });

                jQuery('#popup_load_template').on('click', '.template-list .template-item', function(event) {

                    jQuery('.template-list .template-item').removeClass('active');
                    jQuery(this).addClass('active');
                    jQuery('.btn-load-template').show();
                });
                jQuery('#popup_load_template').on('click', '.template-item-delete', function(event) {

                    var _dataId = jQuery(this).parents('.template-item').attr('data-id');
                    event.stopPropagation();
                    if (_this.config.onTemplateDeleteButtonClick !== undefined) {
                        _this.config.onTemplateDeleteButtonClick(event, _dataId, jQuery(this).parents('.template-item'));
                    }

                });
                jQuery('#popup_load_template').on('click', '.btn-load-template', function(event) {

                    if (_this.config.onBeforePopupSelectTemplateButtonClick !== undefined) {
                        _this.config.onBeforePopupSelectTemplateButtonClick(event);
                    }

                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }

                    var _dataId = jQuery('.template-list .template-item.active').attr('data-id');
                    //search template in array
                    var result = jQuery.grep(_templateListItems, function(e) {
                        return e.id == _dataId;
                    });
                    if (result.length == 0) {
                        //show error
                        jQuery('.template-load-error').text('An error has occurred');
                    }
                    var _contentText = jQuery('<div/>').html(result[0].content).text();


                    jQuery('.bal-header .bal-project-name').html(result[0].name);
                    jQuery('.bal-header .bal-project-name').attr('data-id', _dataId);
                    jQuery('.bal-project-container').show();

                    jQuery('.bal-content-wrapper').html(_contentText);
                    jQuery('#popup_load_template').modal('hide');
                    _this.makeSortable();
                    event.stopPropagation();
                });

                jQuery('body').on('click', '.btn-upload', function(event) {

                    if (_this.config.onPopupUploadImageButtonClick !== undefined) {
                        _this.config.onPopupUploadImageButtonClick(event);
                    }

                    if (event.isDefaultPrevented() == true) {
                        return false;
                    }


                });

            },
            /**
             *  Events of popup save
             */
            events_of_popup: function() {
                var _this = this;

                //save code editor
                jQuery('#popup_editor').on('click', '.btn-save-editor', function() {
                    jQuery('.sortable-row.code-editor .sortable-row-content').html(_aceEditor.getSession().getValue());
                    jQuery('#popup_editor').modal('hide');
                    jQuery('.sortable-row.code-editor').removeClass('code-editor');
                });

            },
            /**
             *  Left menu tab click event
             */
            tabMenuItemClick: function(_element, handle) {
                var _this = this;
                var _tabSelector = _element.data('tab-selector');
                if (_element.hasClass('bal-collapse')) {
                    return false;
                }
                _this.$elem.find('.bal-menu-item.active').removeClass('active');
                _this.$elem.find('.bal-element-tab.active').removeClass('active');
                //show tab content
                _this.$elem.find('.' + _tabSelector).addClass('active');
                //select new tab
                _element.addClass('active');
                if (!handle) {
                    _this.$elem.find('.sortable-row.active').removeClass('active');
                }
            },
            /**
             *  menu accordion
             */
            menuAccordionClick: function(_element, toggle) {
                var _this = this;
                var dropDown = _element.closest('.bal-elements-accordion-item').find('.bal-elements-accordion-item-content');
                _element.closest('.bal-elements-accordion').find('.bal-elements-accordion-item-content').not(dropDown).slideUp();
                if ($('.tab-property').hasClass('active')) {
                    _this.$elem.find('.sortable-row.active .main').attr('data-last-type', _element.closest('.bal-elements-accordion-item').attr('data-type'));
                }
                if (!toggle) {
                    _element.closest('.bal-elements-accordion').find('.bal-elements-accordion-item-title.active').removeClass('active');
                    _element.addClass('active');
                    dropDown.stop(false, true).slideDown();
                } else {
                    if (_element.hasClass('active')) {
                        _element.removeClass('active');
                    } else {
                        _element.closest('.bal-elements-accordion').find('.bal-elements-accordion-item-title.active').removeClass('active');
                        _element.addClass('active');
                    }
                    dropDown.stop(false, true).slideToggle();
                }
            },
            /**
             * Open/close left menu tab and it's child
             */
            tabMenu: function(tab) {
                var _this = this;

                if(typeof(tab) == 'undefined' || typeof(_tabMenuItems[tab]) == 'undefined') return false;

                var _menuItem = _tabMenuItems[tab];


                var _tabMenuItem = _this.$elem.find('.bal-left-menu-container .bal-menu-item[data-tab-selector="' + _menuItem.parentSelector + '"]');
                var _accordionMenuItem = _this.$elem.find('.bal-elements-accordion .bal-elements-accordion-item[data-type="' + _menuItem.itemSelector + '"] .bal-elements-accordion-item-title');
                _this.tabMenuItemClick(_tabMenuItem, true);
                _this.menuAccordionClick(_accordionMenuItem, false);
            },
            /**
             * Get created email template
             */
            getContentHtml: function() {
                var _this = this;

                var accepted_tags = [
                    "a",
                    "abbr",
                    "address",
                    "area",
                    "article",
                    "aside",
                    "audio",
                    "b",
                    "base",
                    "blockquote",
                    "body",
                    "br",
                    "button",
                    "caption",
                    "cite",
                    "code",
                    "col",
                    "data",
                    "dd",
                    "del",
                    "dfn",
                    "div",
                    "dl",
                    "dt",
                    "em",
                    "fieldset",
                    "figcaption",
                    "figure",
                    "footer",
                    "h1",
                    "h2",
                    "h3",
                    "h4",
                    "h5",
                    "h6",
                    "head",
                    "header",
                    "hgroup",
                    "hr",
                    "html",
                    "i",
                    "iframe",
                    "img",
                    "input",
                    "ins",
                    "kbd",
                    "label",
                    "legend",
                    "li",
                    "link",
                    "main",
                    "map",
                    "mark",
                    "math",
                    "menu",
                    "menuitem",
                    "meta",
                    "meter",
                    "nav",
                    "ol",
                    "output",
                    "p",
                    "param",
                    "picture",
                    "pre",
                    "progress",
                    "q",
                    "rb",
                    "rp",
                    "rt",
                    "rtc",
                    "ruby",
                    "s",
                    "section",
                    "select",
                    "small",
                    "span",
                    "strong",
                    "style",
                    "sub",
                    "summary",
                    "sup",
                    "svg",
                    "table",
                    "tbody",
                    "td",
                    "tfoot",
                    "th",
                    "thead",
                    "tr",
                    "u",
                    "ul",
                ];

                _this.removeTinyMCE();

                _this.$elem.find('.element-content').show();

                var _balContentWrapper = _this.$elem.find('.bal-content-wrapper').clone().attr('id', 'generatedTemplateOutput').appendTo('body');

                _balContentWrapper.find('*').each(function () {
                   if(accepted_tags.indexOf($(this).prop('tagName').toLowerCase()) === -1) {
                        $(this).remove();
                    }
                });

                _balContentWrapper.find('input, canvas, object').remove();

                _balContentWrapper.find('mark').each(function() {
                    $(this).replaceWith($(this).html());
                });

                _balContentWrapper.find('a').css({
                    'word-wrap': 'break-word'
                });
                _balContentWrapper.find('table').css({
                    'border-spacing': '0',
                    'border-collapse': 'collapse',
                    'mso-table-lspace': 'collapse',
                    'mso-table-rspace': 'collapse',
                    'width': '100%'
                });
                _balContentWrapper.find('table td').css({
                    'border-collapse': 'collapse'
                });
                _balContentWrapper.find('table, td, p, a, li').css({
                    '-webkit-text-size-adjust': '100%',
                    '-ms-text-size-adjust': '100%'
                });

                _balContentWrapper.find('[data-mce-style]').removeAttr('data-mce-style');
                _balContentWrapper.find('[data-mce-href]').removeAttr('data-mce-href');
                _balContentWrapper.find('[data-default]').removeAttr('data-default');

                var _html = '';
                _balContentWrapper.find('.sortable-row').each(function() {
                    _html += jQuery(this).find('.sortable-row-content').html().split('contenteditable="true"').join('');
                });
                var _width = _balContentWrapper.find('.bal-content-main').attr('data-width') == '100' ? '600' : jQuery('.bal-content-main').attr('data-width'); //$('.bal-content-main').css('width');

                if (typeof _width == 'undefined') {
                    _width = '600px';
                }
                var _style = '';
                _style += 'background:' + _balContentWrapper.css('background') + ';';
                var _padding = 'padding:' + _balContentWrapper.css('padding');

                var background_replace = '';
                if(_balContentWrapper.attr('data-css_replace')) {
                    background_replace = 'data-css_replace="' + _balContentWrapper.attr('data-css_replace') + '"';
                }

                var _result = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="' + _style + '"' + background_replace + '><tbody><tr><td><div class="emailContainer" style="margin:0 auto;max-width: ' + _width.replace('px', '') + 'px;width: 100%;' + _padding + '">' + _html + '</div></td></tr></table>';

               _balContentWrapper.remove();

                return _result;
            },
            /**
             * Get created email template
             */
            resetHtmlContent: function() {
                var _this = this;

                var html = '<div class="bal-content-main lg-width">' +
                    '<div class="email-editor-elements-sortable">' +
                    _this.helperForGenerateHTMl(_blankPageHtml) +
                    '</div>' +
                    '</div>';

                _this.$elem.find('.bal-content-wrapper').html(html);
                _this.makeSortable();
                _this.remove_row_elements();

                jQuery('.bal-project-container').hide();
                jQuery('.bal-project-name').attr('data-id', '');
            },
            /**
             * Cambia il template
             */
            setHtmlContent: function(html) {
                var _this = this;

                _this.$elem.find('.bal-content-wrapper').attr('data-css_replace', _this.getMainBackground(html)[1]).css("background",_this.getMainBackground(html)[0]);

                html = '<div class="bal-content-main lg-width">' +
                    '<div class="email-editor-elements-sortable">' +
                    _this.helperForGenerateHTMl(html) +
                    '</div>' +
                    '</div>';

                _this.$elem.find('.bal-content-wrapper').html(html);

                _this.makeSortable();
                _this.remove_row_elements();

                jQuery('.bal-project-container').hide();
                jQuery('.bal-project-name').attr('data-id', '');

                jQuery('.bal-content-main').attr('data-width', '600px');
                jQuery('.main').css('width', '600px');
            },
            /**
             * Generate left menu elements tab
             */
            generateElements: function() {
                var _this = this;
                var _outputHtml = '';

                $.ajax({
                    url: _this.config.elementJsonUrl,
                    data: '',
                    success: function(data) {
                        data = data.elements;
                        for (var i = 0; i < data.length; i++) {

                            _outputHtml += '<li class="bal-elements-accordion-item" data-type="' + data[i].name.toLowerCase() + '"><a class="bal-elements-accordion-item-title">' + data[i].name + '</a>';

                            _outputHtml += '<div class="bal-elements-accordion-item-content"><ul class="bal-elements-list">';

                            var _items = data[i].items;

                            for (var j = 0; j < _items.length; j++) {
                                _outputHtml += '<li>' +
                                    '<div class="bal-elements-list-item">' +
                                    '<div class="bal-preview">' +
                                    '<div class="bal-elements-item-icon">' +
                                    ' <i class="' + _items[j].icon + '"></i>' +
                                    '</div>' +
                                    '<div class="bal-elements-item-name">' +
                                    _items[j].name +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="bal-view">' +
                                    '<div class="sortable-row">' +
                                    '<div class="sortable-row-container">' +
                                    ' <div class="sortable-row-actions">';

                                if (_this.config.showRowMoveButton == true) {
                                    _outputHtml += '<div class="row-move row-action fa fa-arrows-alt"></div>';
                                }

                                if (_this.config.showRowRemoveButton == true) {
                                    _outputHtml += '<div class="row-remove row-action fa fa-remove"></div>';
                                }

                                if (_this.config.showRowDuplicateButton == true) {
                                    _outputHtml += '<div class="row-duplicate row-action fa fa-files-o"></div>';
                                }

                                if (_this.config.showRowCodeEditorButton == true) {
                                    _outputHtml += '<div class="row-code row-action fa fa-code"></div>';
                                }
                                _outputHtml += '</div>' +
                                    '<div class="sortable-row-content" data-url="' + _this.getAbsolutePath() + _items[j].content + '">' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    ' </div>' +
                                    '</div>' +
                                    '</li>';
                            }


                            _outputHtml += '</ul></div>';
                            _outputHtml += '</li>';
                        }



                        _this.generate(_outputHtml);
                    },
                    error: function() {
                        console.error('Has error');
                    },
                    dataType: 'json'
                });
            },
            /**
             * Get Site Url
             */
            getAbsolutePath: function() {
                return $('#baseElementPathAdmin').val();
            },
            /**
             * Content row click event
             */
            sortableClick: function(_thisElement) {
                var _this = this;
                var _element = _thisElement.parents('.sortable-row');

                _this.removeTinyMCE();

                jQuery('.bal-content-wrapper').removeClass('active');

                _this.$elem.find('.main .active').removeClass('active');
                _this.$elem.find('.element-contenteditable').removeClass('element-contenteditable').removeAttr('contenteditable');

                if(_thisElement.hasClass('element-content') && !_thisElement.hasClass('image') && !_thisElement.find('.button-1').length && !_thisElement.hasClass('no_edit'))
                {
                    _thisElement.addClass('element-contenteditable').attr('contenteditable', true);
                }
                _thisElement.addClass('active');

                //select current item
                _this.$elem.find('.bal-content .sortable-row').removeClass('active');
                _element.addClass('active');
                var _dataTypes = _element.find('.sortable-row-content .main').attr('data-types');
                if (typeof _dataTypes == 'undefined') {
                    return;
                }

                if (_dataTypes.length < 1) {
                    return;
                }

                var _typeArr = _dataTypes.toString().split(',');
                var _arrSize = _this.$elem.find('.tab-property .bal-elements-accordion-item').length;
                for (var i = 0; i < _arrSize; i++) {
                    var _accordionMenuItem = _this.$elem.find('.tab-property .bal-elements-accordion-item').eq(i);
                    //console.log(_accordionMenuItem.attr('data-type'))
                    if (_dataTypes.indexOf(_accordionMenuItem.attr('data-type')) > -1) {
                        _accordionMenuItem.show();
                    } else {
                        _accordionMenuItem.hide();
                    }
                }

                _this.tabMenu(_element.find('.sortable-row-content .main').attr('data-last-type'));
                _this.getSettings();
                _this.composeShortcodesView();
                if(_thisElement.hasClass('element-contenteditable')) {
                    _this.tinymceContextMenu();
                }
            },
            /**
             * Get active element settings
             */
            getSettings: function() {
                var _this = this;
                var _element = _this.getActiveElementContent();
                var _style = _element.attr('style');

                if (typeof _style === "undefined" || _style.length < 1) {
                    return;
                }

                //background
                var elementsToCheck = {
                    '[setting-type="background-color"]': _element,
                    '#button-bg-color': _element.find('a')
                };

                Object.keys(elementsToCheck).forEach(function (obj_id) {

                    _this.$elem.find('.tab-property .bal-elements-accordion-item ' + obj_id).css('background', elementsToCheck[obj_id].css('background'));

                    var background_id = '';
                    if (elementsToCheck[obj_id].attr('data-css_replace') && elementsToCheck[obj_id].attr('data-css_replace').includes('background')) {

                        if (elementsToCheck[obj_id].attr('data-css_replace').includes('color_1')) {
                            background_id = "0";
                        } else if (elementsToCheck[obj_id].attr('data-css_replace').includes('color_2')) {
                            background_id = "1";
                        } else if (elementsToCheck[obj_id].attr('data-css_replace').includes('color_3')) {
                            background_id = "2";
                        } else if (elementsToCheck[obj_id].attr('data-css_replace').includes('color_4')) {
                            background_id = "3";
                        }
                    }

                    if (background_id != '') {
                        _this.$elem.find('.tab-property .bal-elements-accordion-item ' + obj_id).parent().find('.defaultColorsSelect').val(background_id).removeClass('custom');
                    } else {
                        _this.$elem.find('.tab-property .bal-elements-accordion-item ' + obj_id).parent().find('.defaultColorsSelect').val('custom').addClass('custom');

                    }
                });
            },
            /**
             * Change active element settings
             */
            changeSettings: function(type, value) {
                var _this = this;
                var _activeElement = _this.getActiveElementContent();


                if (type == 'font-size') {
                    _activeElement.find('>h1,>h4').css(type, value);
                } else if (type == 'background-image') {
                    _activeElement.css(type, 'url("' + value + '")');
                    _activeElement.css({
                        'background-size': 'cover',
                        'background-repeat': 'no-repeat'
                    });
                }

                if (_activeElement.hasClass('button-1') && type == 'border-radius') {
                    _this.getActiveElementContent('a').css(type, value);
                } else {
                    _activeElement.css(type, value);
                }

                if (type == 'background-color' && _activeElement.hasClass('bal-content-wrapper')) {
                    _this.$elem.find('.bal-content-main').css('background-color', value);
                }
            },
            /**
             * Get selected html of the window
             */
            getSelectedHtml: function() {
                var _this = this;
                var html = "";
                if (typeof window.getSelection != "undefined") {
                    var sel = window.getSelection();
                    if (sel.rangeCount) {
                        var container = document.createElement("div");
                        for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                            container.appendChild(sel.getRangeAt(i).cloneContents());
                        }
                        html = container.innerHTML;
                    }
                } else if (typeof document.selection != "undefined") {
                    if (document.selection.type == "Text") {
                        html = document.selection.createRange().htmlText;
                    }
                }
                return html;
            },
            /**
             * tinymce Context Menu
             */
            tinymceContextMenu: function() {
                var _this = this;

                _this.removeTinyMCE();

                $('.bal-content-wrapper .active').find('mark').each(function () {
                    $(this).replaceWith($(this).html());
                });

                $('.bal-content-wrapper .element-content').show();

                var custom_params = {menubar: false, custom_ui_selector: '.bal-editor-container', 'height': $('.bal-content-wrapper [contenteditable]').height()};

                // Se il contenitore non è abbastanza grande mostro una versione minimale della toolbar
                if($('.bal-content-wrapper [contenteditable]').width() < 500) {
                    custom_params['toolbar'] = 'removeformat | styleselect | forecolor backcolor';
                }

                custom_params['setup'] = function (editor) {
                    editor.on('change', function () {
                        tinymce["activeEditor"]["tmp"].html(tinymce["activeEditor"].getContent());
                    });

                    editor.on('init', function() {
                        tinymce["activeEditor"]["tmp"] = $('<div>' + tinymce["activeEditor"].getContent() + '</div>');
                        tinymce["activeEditor"]["tmp_observer"] = new MutationObserver(function (mutationsList, observer) {
                            tinymce["activeEditor"].setContent(tinymce["activeEditor"]["tmp"][0].outerHTML);
                            tinymce["activeEditor"]["tmp_observer"].disconnect();
                        });
                    });
                };

                initTinyMCE('.bal-content-wrapper [contenteditable]', {small: true, shortcodes: (_this.config.shortcodesList ? _this.config.shortcodesList : {})}, custom_params, 'NEWSLETTER');

            },
            /**
             * Get languages
             */
            getLangs: function() {
                var _this = this;
                $.ajax({
                    url: _this.config.langJsonUrl,
                    data: '',
                    success: function(data) {
                        jQuery.each(data, function(i, val) {
                            _language[i] = val[0];
                        });

                        //set language data to private variable
                        _this.langArr = _language[_this.config.lang];
                        _this.generateElements();
                    },
                    error: function() {
                        console.error('Has error');
                    },
                    dataType: 'json'
                });
            },
            /**
             * Get blank page html code
             */
            getBlankPageHtml: function() {
                var _this = this;
                $.ajax({
                    url: _this.config.blankPageHtmlUrl,
                    success: function(data) {
                        _blankPageHtml = data;
                    },
                    error: function() {
                        console.error('Has error getBlankPageHtml');
                    }
                });
            },

            /**
             * Rimuove tutti i tinyMCE
             */
            removeTinyMCE: function() {
                if(typeof(tinyMCE) !== 'undefined') {
                    var length = tinyMCE.editors.length;
                    for (var i=length; i>0; i--) {
                        tinyMCE.editors[i-1].remove();
                    };
                }
            },

            /**
             * Undo /redo
             */
            commandsUndoManager: function() {
            }
        };

        $.fn.emailBuilder = function(options) {

            var _emailBuilder;

            /**
             * Set elements json file url, include which elements want to show in email builder
             */
            this.setElementJsonUrl = function(elementJsonUrl) {
                _emailBuilder.config.elementJsonUrl = elementJsonUrl;
            }
            /**
             * Chnage language builder  (en | fr | de | ru | tr ).
             */
            this.setLang = function(lang) {
                _emailBuilder.config.lang = lang;
            }
            /**
             *  Set json file url  include which supported languages .
             *  If you want ,you can add any language very easily.
             */
            this.setLangJsonUrl = function(value) {
                _emailBuilder.config.langJsonUrl = value;
            }
            /**
             * Set blank page html source. when users want to create blank page,they see this html
             */
            this.setBlankPageHtmlUrl = function(blankPageHtmlUrl) {
                _emailBuilder.config.blankPageHtmlUrl = blankPageHtmlUrl;
            }
            /**
             * Set html when page loading you can load your template from database or you can show any html into editor
             */
            this.setLoadPageHtmlUrl = function(loadPageHtmlUrl) {
                _emailBuilder.config.loadPageHtmlUrl = loadPageHtmlUrl;
            }

            /**
             * Show or hide context menu in editor
             */
            this.setShowContextMenu = function(showContextMenu) {
                _emailBuilder.config.showContextMenu = showContextMenu;
            }
            /**
             * Show or hide font family option context menu in editor
             */
            this.setShowContextMenu_FontFamily = function(showContextMenu_FontFamily) {
                _emailBuilder.config.showContextMenu_FontFamily = showContextMenu_FontFamily;
            }

            /**
             * Show or hide font size option context menu in editor
             */
            this.setShowContextMenu_FontSize = function(showContextMenu_FontSize) {
                _emailBuilder.config.showContextMenu_FontSize = showContextMenu_FontSize;
            }
            /**
             * Show or hide bold option context menu in editor
             */
            this.setShowContextMenu_Bold = function(showContextMenu_Bold) {
                _emailBuilder.config.showContextMenu_Bold = showContextMenu_Bold;
            }
            /**
             * Show or hide italic option context menu in editor
             */
            this.setShowContextMenu_Italic = function(showContextMenu_Italic) {
                _emailBuilder.config.showContextMenu_Italic = showContextMenu_Italic;
            }
            /**
             * Show or hide underline option context menu in editor
             */
            this.setShowContextMenu_Underline = function(showContextMenu_Underline) {
                _emailBuilder.config.showContextMenu_Underline = showContextMenu_Underline;
            }
            /**
             * Show or hide strikethrough option context menu in editor
             */
            this.setShowContextMenu_Strikethrough = function(showContextMenu_Strikethrough) {
                _emailBuilder.config.showContextMenu_Strikethrough = showContextMenu_Strikethrough;
            }
            /**
             * Show or hide hyperlink option context menu in editor
             */
            this.setShowContextMenu_Hyperlink = function(showContextMenu_Hyperlink) {
                _emailBuilder.config.showContextMenu_Hyperlink = showContextMenu_Hyperlink;
            }




            /**
             * Show or hide elements tab in left menu
             */
            this.setShowElementsTab = function(showElementsTab) {
                _emailBuilder.config.showElementsTab = showElementsTab;
            }
            /**
             * Show or hide property tab in left menu
             */
            this.setShowPropertyTab = function(showPropertyTab) {
                _emailBuilder.config.showPropertyTab = showPropertyTab;
            }
            /**
             * Show or hide 'collapse menu' button in left menu
             */
            this.setShowCollapseMenu = function(showCollapseMenu) {
                _emailBuilder.config.showCollapseMenu = showCollapseMenu;
            }
            /**
             * Show or hide 'blank page' button in left menu
             */
            this.setShowBlankPageButton = function(showBlankPageButton) {
                _emailBuilder.config.showBlankPageButton = showBlankPageButton;
            }
            /**
             * Show or hide 'collapse menu' button bottom or above
             */
            this.setShowCollapseMenuinBottom = function(showCollapseMenuinBottom) {
                _emailBuilder.config.showCollapseMenuinBottom = showCollapseMenuinBottom;
            }


            /**
             * Set value show or hide settings bar
             */
            this.setShowSettingsBar = function(showSettingsBar) {
                _emailBuilder.config.showSettingsBar = showSettingsBar;
            }
            /**
             * Set value  show or hide 'Preview' button in settings bar
             */
            this.setShowSettingsPreview = function(showSettingsPreview) {
                _emailBuilder.config.showSettingsPreview = showSettingsPreview;
            }
            /**
             * Set value show or hide 'Export' button in settings bar
             */
            this.setShowSettingsExport = function(showSettingsExport) {
                _emailBuilder.config.showSettingsExport = showSettingsExport;
            }
            /**
             * Set value show or hide 'Send Mail' button in settings bar
             */
            this.setShowSettingsSendMail = function(showSettingsSendMail) {
                _emailBuilder.config.showSettingsSendMail = showSettingsSendMail;
            }
            /**
             * Set value show or hide 'Save' button in settings bar
             */
            this.setShowSettingsSave = function(showSettingsSave) {
                _emailBuilder.config.showSettingsSave = showSettingsSave;
            }
            /**
             * Set value show or hide 'Load Template' button in settings bar
             */
            this.setShowSettingsLoadTemplate = function(showSettingsLoadTemplate) {
                _emailBuilder.config.showSettingsLoadTemplate = showSettingsLoadTemplate;
            }


            /**
             * Set value show or hide 'Move' button in actions row item
             */
            this.setShowRowMoveButton = function(showRowMoveButton) {
                _emailBuilder.config.showRowMoveButton = showRowMoveButton;
            }
            /**
             * Set value show or hide 'Remove' button in actions row item
             */
            this.setShowRowRemoveButton = function(showRowRemoveButton) {
                _emailBuilder.config.showRowRemoveButton = showRowRemoveButton;
            }
            /**
             * Set value show or hide 'Duplicate' button in actions row item
             */
            this.setShowRowDuplicateButton = function(showRowDuplicateButton) {
                _emailBuilder.config.showRowDuplicateButton = showRowDuplicateButton;
            }
            /**
             * Set value show or hide 'Code Editor' button in actions row item
             */
            this.setShowRowCodeEditorButton = function(showRowCodeEditorButton) {
                _emailBuilder.config.showRowCodeEditorButton = showRowCodeEditorButton;
            }

            /**
             * Aggiorna gli shortcodes
             */
            this.setShortcodesList = function(shortcodesList) {
                _emailBuilder.config.shortcodesList = shortcodesList;
            }

            /**
             * Aggiorna gli shortcodes
             */
            this.composeShortcodesView = function() {
                _emailBuilder.composeShortcodesView();
            }

            /**
             * Init email builder any time
             */
            this.init = function() {
                _emailBuilder.init();
            }


            /**
             * Set settings preview button click event
             */
            this.setSettingsPreviewButtonClick = function(func) {
                _emailBuilder.config.onSettingsPreviewButtonClick = func;
            }
            /**
             * Set Settings export button click event
             */
            this.setSettingsExportButtonClick = function(func) {
                _emailBuilder.config.onSettingsExportButtonClick = func;
            }
            /**
             * Set Settings before save button click event
             */
            this.setBeforeSettingsSaveButtonClick = function(func) {
                _emailBuilder.config.onBeforeSettingsSaveButtonClick = func;
            }
            /**
             * Set Settings save button click event
             */
            this.setSettingsSaveButtonClick = function(func) {
                _emailBuilder.config.onSettingsSaveButtonClick = func;
            }
            /**
             * Set Settings before load template button click event
             */
            this.setBeforeSettingsLoadTemplateButtonClick = function(func) {
                _emailBuilder.config.onBeforeSettingsLoadTemplateButtonClick = func;
            }
            /**
             * Set Settings send mail button click event
             */
            this.setSettingsSendMailButtonClick = function(func) {
                _emailBuilder.config.onSettingsSendMailButtonClick = func;
            }

            /**
             * Set Before 'change image' click event
             */
            this.setBeforeChangeImageClick = function(func) {
                _emailBuilder.config.onBeforeChangeImageClick = func;
            }
            /**
             * Set Before save button click event in 'select image' popup
             */
            this.setBeforePopupSelectImageButtonClick = function(func) {
                _emailBuilder.config.onBeforePopupSelectImageButtonClick = func;
            }
            /**
             * Set xxxxxxxxxxxx
             */
            this.setBeforePopupSelectTemplateButtonClick = function(func) {
                _emailBuilder.config.onBeforePopupSelectTemplateButtonClick = func;
            }
            /**
             * Set Save button click event in 'Save template' popup
             */
            this.setPopupSaveButtonClick = function(func) {
                _emailBuilder.config.onPopupSaveButtonClick = func;
            }
            /**
             * Set Before select template button click event in 'load template' popup
             */
            this.setPopupSendMailButtottonClick = function(func) {
                _emailBuilder.config.onPopupSendMailButtonClick = func;
            }
            /**
             * Set 'Upload' button click for upload image in 'select image' popup
             */
            this.setPopupUploadImageButtonClick = function(func) {
                _emailBuilder.config.onPopupUploadImageButtonClick = func;
            }

            /**
             * Set Before clicking 'Remove' button in element settings
             */
            this.setBeforeRowRemoveButtonClick = function(func) {
                _emailBuilder.config.onBeforeRowRemoveButtonClick = func;
            }
            /**
             * Set After clicking 'Remove' button in element settings
             */
            this.setAfterRowRemoveButtonClick = function(func) {
                _emailBuilder.config.onAfterRowRemoveButtonClick = func;
            }
            /**
             * Set Before clicking 'Duplicate' button in element settings
             */
            this.setBeforeRowDuplicateButtonClick = function(func) {
                _emailBuilder.config.onBeforeRowDuplicateButtonClick = func;
            }
            /**
             * Set After clicking 'Duplicate' button in element settings
             */
            this.setAfterRowDuplicateButtonClick = function(func) {
                _emailBuilder.config.onAfterRowDuplicateButtonClick = func;
            }
            /**
             * Set Before clicking 'Code editor' button in element settings
             */
            this.setBeforeRowEditorButtonClick = function(func) {
                _emai_emailBuilder.config.onBeforeRowEditorButtonClick = func;
            }
            /**
             * Set After clicking 'Code editor' button in element settings
             */
            this.setAfterRowEditorButtonClick = function(func) {
                _emailBuilder.config.onAfterRowEditorButtonClick = func;
            }
            /**
             * Set Before, show code editor for edit source any elemnt of template
             */
            this.setBeforeShowingEditorPopup = function(func) {
                _emailBuilder.config.onBeforeShowingEditorPopup = func;
            }
            /**
             * Set After page loading event
             */
            this.setAfterLoad = function(func) {
                _emailBuilder.config.onAfterLoad = func;
                _emailBuilder.composeShortcodesView();
            }
            /**
             * Get created email template
             */
            this.getContentHtml = function() {
                return _emailBuilder.getContentHtml();
            }

            this.makeSortable = function () {
                _emailBuilder.makeSortable();
            }

            this.makeRowElements = function () {
                _emailBuilder.remove_row_elements();
            }

            this.removeTinyMCE = function () {
               _emailBuilder.removeTinyMCE();
            }

            /**
             * CUSTOM PER FW
             */
            this.resetHtmlContent = function() {
                return _emailBuilder.resetHtmlContent();
            }

            this.setHtmlContent = function(html) {
                return _emailBuilder.setHtmlContent(html);
            }

            this.composeShortcodesView = function() {
                return _emailBuilder.composeShortcodesView();
            }

            return this.each(function() {
                _emailBuilder = new EmailBuilder(this, options);
                _emailBuilder.init();
            });

        };
    });

    jQuery.fn.hasParent = function(e) {
        return (jQuery(this).parents(e).length == 1 ? true : false);
    }

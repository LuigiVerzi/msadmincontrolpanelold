<table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" data-types="background,padding,image-settings" align="center" data-last-type="background">
    <tbody>
        <tr>
            <td align="left">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="element-content" style="text-align:center;padding:5px;background-color:#FFFFFF;">
                            <img border="0" class="content-image" src="[site-url]vendor/plugins/email-editor/images/service-list/lamp.png" style="display: inline-block;margin: 0px;width:64px;height:64px">
                        </td>
                    </tr>
                    <tr>
                        <td class="element-content" style="padding:5px;background-color:#FFFFFF;">
                            <h4 style="font-weight: normal;text-align:center">Generating ideas</h4>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="element-content" style="padding: 5px; font-family: Arial;font-size: 13px;color: #000000;line-height: 22px;">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="element-content" style="text-align:center;padding:5px;background-color:#FFFFFF;">
                            <img border="0" class="content-image" src="[site-url]vendor/plugins/email-editor/images/service-list/comp.png" style="display: inline-block;margin: 0px;width:64px;height:64px">
                        </td>
                    </tr>
                    <tr>
                        <td class="element-content" style="padding:5px;background-color:#FFFFFF;">
                            <h4 style="font-weight: normal;text-align:center">Web Design</h4>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="element-content" style="padding: 5px; font-family: Arial;font-size: 13px;color: #000000;line-height: 22px;">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="element-content" style="text-align:center;padding:5px;background-color:#FFFFFF;">
                            <img border="0" class="content-image" src="[site-url]vendor/plugins/email-editor/images/service-list/code.png" style="display: inline-block;margin: 0px;width:64px;height:64px">
                        </td>
                    </tr>
                    <tr>
                        <td class="element-content" style="padding:5px;background-color:#FFFFFF;">
                            <h4 style="font-weight: normal;text-align:center">Web Development</h4>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="element-content" style="padding: 5px; font-family: Arial;font-size: 13px;color: #000000;line-height: 22px;">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>

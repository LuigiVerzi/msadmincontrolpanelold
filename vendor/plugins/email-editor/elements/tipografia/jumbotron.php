<table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" data-types="background,padding,button" align="center" data-last-type="background">
    <tbody>
        <tr>
            <td align="left" class="element-content" style="padding: 10px 15px;background-color:#FFFFFF;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td>
                            <h1 style="font-weight: normal;text-align:center">Welcome to Builder</h1>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding: 10px 15px;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">
                            <a style="background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                                  Click me
                              </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>

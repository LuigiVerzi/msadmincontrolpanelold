<table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" data-types="social-content,background,padding" data-last-type="social-content" align="center" style="background-color:#FFFFFF;">
    <tbody>
        <tr>
            <td class="element-content social-content" style="padding:20px 50px;text-align:center">
                <a data-remove_if_empty="href" href="{social-instagram}" style="border: none;display: inline-block;" class="instagram">
                    <img border="0" src="[site-url]vendor/plugins/email-editor/images/social-icons/insta-01.png" width="32">
                </a>
                <a data-remove_if_empty="href" href="{social-facebook}" style="border: none;display: inline-block;" class="facebook">
                    <img border="0" src="[site-url]vendor/plugins/email-editor/images/social-icons/fb-01.png" width="32">
                </a>
                <a data-remove_if_empty="href" href="{social-twitter}" style="border: none;display: inline-block;" class="twitter">
                    <img border="0" src="[site-url]vendor/plugins/email-editor/images/social-icons/twt-01.png" width="32">
                </a>
                <a data-remove_if_empty="href" href="{social-linkedin}" style="border: none;display: inline-block;" class="linkedin">
                    <img border="0" src="[site-url]vendor/plugins/email-editor/images/social-icons/in-01.png" width="32">
                </a>
                <a data-remove_if_empty="href" href="{social-youtube}" style="border: none;display: inline-block;" class="youtube">
                    <img border="0" src="[site-url]vendor/plugins/email-editor/images/social-icons/ytb-01.png" width="32">
                </a>
                <a data-remove_if_empty="href" href="{social-skype}" style="border: none;display: inline-block;" class="skype">
                    <img border="0" src="[site-url]vendor/plugins/email-editor/images/social-icons/skype-01.png" width="32">
                </a>
            </td>
        </tr>
    </tbody>
</table>

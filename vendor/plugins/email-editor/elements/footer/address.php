<?php
include("../../../../../sw-config.php");
$producer_config = $MSFrameworkCMS->getCMSData('producer_config');
$email_colors = json_decode($producer_config['templatemail_colors']);
?>

<table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" data-types="background,padding" align="center" data-last-type="background">
    <tbody>
        <tr>
            <td data-css_replace="[background|{color_3}]" align="left" class="page-header element-content" style="padding: 10px 15px;text-align:center;background-color: <?= $email_colors[2]; ?>!important;">
                <div>
                    {geo-address}
                </div>
            </td>
        </tr>
    </tbody>
</table>

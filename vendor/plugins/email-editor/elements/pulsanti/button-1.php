<?php
include("../../../../../sw-config.php");
$producer_config = $MSFrameworkCMS->getCMSData('producer_config');
$email_colors = json_decode($producer_config['templatemail_colors']);
?>

<table  class="main" width="100%" cellspacing="0" cellpadding="0" border="0"
        data-types="background,border-radius,padding,button"
        data-last-type="button"
        style="background-color:#FFFFFF" align="center">
    <tbody>
    <tr>
        <td class="element-content button-1" align="left" style="padding: 10px 15px;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">

            <a data-css_replace="[background|{color_2}]" style="background-color: <?= $email_colors[1]; ?>!important;font-family: Arial;color: #FFFFFF;display: inline-block;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                Call to action
            </a>
        </td>
    </tr>
    </tbody>
</table>

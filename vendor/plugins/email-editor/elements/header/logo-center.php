<?php
include("../../../../../sw-config.php");
$producer_config = $MSFrameworkCMS->getCMSData('producer_config');

$siteCMSData = $MSFrameworkCMS->getCMSData('site');
$site_logos = json_decode($siteCMSData['logos'], true);

$email_colors = json_decode($producer_config['templatemail_colors']);
?>

<table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" data-types="padding" align="center" data-last-type="background">
    <tbody>
    <tr>
        <td data-css_replace="[background|{color_3}]" align="left" class="element-content image" style="background-color: <?= $email_colors[2]; ?>!important; padding: 10px 15px;text-align:center">
            <img border="0" class="content-image" data-css_replace="[width|{logo_size}][height|auto]" data-attr_replace="[src|{logo}]" src="<?= UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo']; ?>" style="display: inline-block;margin:0px;width:<?=  $producer_config['templatemail_logosize']; ?>px;height:auto;">
        </td>
    </tr>
    </tbody>
</table>

var ERD_ADD_ROW_TXT = "Add Row";
var ERD_DEL_ROW_TXT = "Delete Row";
var ERD_MAXROW_INSERT = "You can not insert more than %d rows!";
var ERD_CANNOT_DEL_LAST_ROW = "You can not delete the last row!";
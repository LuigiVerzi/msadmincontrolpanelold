var ERD_ADD_ROW_TXT = "Aggiungi riga";
var ERD_DEL_ROW_TXT = "Elimina riga";
var ERD_MAXROW_INSERT = "Non puoi inserire più di %d righe!";
var ERD_CANNOT_DEL_LAST_ROW = "Non puoi cancellare l'ultima riga!";
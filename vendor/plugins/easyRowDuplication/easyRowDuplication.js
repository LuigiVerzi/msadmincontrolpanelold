/*
	Copyright 2015 - Vincenzo Mennella
	All rights reserved
	
	This software is distribuited under the Envanto Licence you've choosen.
	Please take a look at http://codecanyon.net/licenses for licences details.

	Version 1.1.0 (16-03-2015)
	
	Changelog:
		ver. 1.0.0 (13-03-2015)
			- First relase
			
		ver. 1.1.0 (16-03-2015)
			- Added support for drag-and-drop. Rows can now be moved up and/or down. *Requires jQueryUI!*
			- Small improvements
*/

;(function ($) {
	$.fn.easyRowDuplication = function(options) {
	
    return this.each(function() {
		$.fn['easyRowDuplication'].defaults = {
			animation: "none", //il tipo da animazione da utilizzare: disponibili "none", "slidedown", "fadein"
			animationSpeed: 600, //la velocità dell'animazione
			maxDuplications: 0, //il numero massimo di duplicazioni possibili (0 disabilitato)
			addButtonPosition: "bottom-right", //la posizione di partenza del pulsante che permette di aggiungere nuove righe
			addButtonText: ERD_ADD_ROW_TXT, //il testo per il pulsante di aggiunta
			addButtonClasses: "", //classi da aggiungere al pulsante di aggiunta
			deleteButtonText: ERD_DEL_ROW_TXT, //il testo per il pulsante di eliminazione
			deleteButtonClasses: "", //classi da aggiungere al pulsante di eliminazione
			rowMargin: 15, //i pixel di margine tra una riga e l'altra
			useCustomDelete: false, //sceglie se usare un elemento personalizzato per eliminare gli elementi con classe easyRowDuplicationDelBtn 
			useCustomAdd: false, //sceglie se usare un elemento personalizzato per aggiungere gli elementi con classe easyRowDuplicationAddBtn 
			sortable: false, //consente alle righe di essere riordinate. Richiede jQuery UI.
			sortableOptions: {
                "sortHolderClass": "easyRowDuplicationRowElement"
			}, //un oggetto contenente le opzioni da passare al plugin sortable
			afterAddCallback: "", //callback dopo aggiunta
			afterDeleteCallback: "", //callback dopo rimozione
			beforeAddCallback: "", //callback prima di aggiunta
			beforeDeleteCallback: "" //callback prima di rimozione
		};
		
		new easyRowDuplication(this, options);
    });
}

function easyRowDuplication(element, options) {
    this.el = element;
    this.$el = $(element);
    
    this.options = $.extend({}, $.fn['easyRowDuplication'].defaults, options);

    this.init(this.options);
}

easyRowDuplication.prototype = {
    init: function(options) {	
	    plugin_this = this;
        duplicating_element = this.el;
	        
        $(duplicating_element).addClass('easyRowDuplicationRowElement');
				
		if(options.useCustomAdd == false) {
			html_addbutton = "<div class='easyRowDuplicationFooter'><input type='button' value='" + options.addButtonText + "' class='easyRowDuplicationAddBtn " + options.addButtonClasses + "'><div style='clear:both;'></div></div>";		
			if(options.addButtonPosition == "bottom-left") {
				$(duplicating_element).parent('.easyRowDuplicationContainer').append(html_addbutton);
				$(duplicating_element).parent('.easyRowDuplicationContainer').find('.easyRowDuplicationAddBtn').css('float', 'left');
			} else if(options.addButtonPosition == "bottom-right") {
				$(duplicating_element).parent('.easyRowDuplicationContainer').append(html_addbutton);
				$(duplicating_element).parent('.easyRowDuplicationContainer').find('.easyRowDuplicationAddBtn').css('float', 'right');
			} else if(options.addButtonPosition == "top-left") {
				$(duplicating_element).parent('.easyRowDuplicationContainer').prepend(html_addbutton);
				$(duplicating_element).parent('.easyRowDuplicationContainer').find('.easyRowDuplicationAddBtn').css('float', 'left');
			} else if(options.addButtonPosition == "top-right") {
				$(duplicating_element).parent('.easyRowDuplicationContainer').prepend(html_addbutton);
				$(duplicating_element).parent('.easyRowDuplicationContainer').find('.easyRowDuplicationAddBtn').css('float', 'right');
			}
		}
		
		if(options.useCustomDelete == false) {
			html_deletebutton = "<div class='customDelete' style='margin-top: 10px;'><input type='button' value='" + options.deleteButtonText + "' class='easyRowDuplicationDelBtn " + options.deleteButtonClasses + " ' style='display: none;'></div>";
			$(duplicating_element).append(html_deletebutton);
		}
			
		$(duplicating_element).parent('.easyRowDuplicationContainer').find('.easyRowDuplicationAddBtn').unbind('click');
		$(duplicating_element).parent('.easyRowDuplicationContainer').find('.easyRowDuplicationAddBtn').on('click', function() {			
			easyRowDuplication.prototype._duplicateRow(options, this);
		})
		
		$(duplicating_element).parent('.easyRowDuplicationContainer').find('.easyRowDuplicationDelBtn').unbind('click');
		$(duplicating_element).parent('.easyRowDuplicationContainer').find('.easyRowDuplicationDelBtn').on('click', function() {
			easyRowDuplication.prototype._deleteRow(options, this);
		})

		//fix per righe aggiunte da DB
		if($(duplicating_element).parent('.easyRowDuplicationContainer').find('> .easyRowDuplicationFooter').length > 1) {
            $(duplicating_element).parent('.easyRowDuplicationContainer').find('> .easyRowDuplicationFooter').not(":first").remove();
		}

		if($(duplicating_element).parent('.easyRowDuplicationContainer').find('> .easyRowDuplicationRowElement').length > 1) {
            $(duplicating_element).parent('.easyRowDuplicationContainer').find('.easyRowDuplicationDelBtn').not(":first").show();
            $(duplicating_element).parent('.easyRowDuplicationContainer').find('> .easyRowDuplicationRowElement').not(":first").css('margin-top', options.rowMargin + "px");
		}

        if(options.sortable) {
            options.sortableOptions.handle = '.' + options.sortableOptions.sortHolderClass;

            $(duplicating_element).parents('.easyRowDuplicationContainer:first').sortable(options.sortableOptions);
            $(duplicating_element).parents('.easyRowDuplicationContainer:first').on("sortupdate", function( event, ui ) {
                $(ui.item).parents('.easyRowDuplicationContainer:first').find(options.sortableOptions.items + " > .customDelete").find('.easyRowDuplicationDelBtn').show();
                $(ui.item).parents('.easyRowDuplicationContainer:first').find(options.sortableOptions.items + ":first > .customDelete").find('.easyRowDuplicationDelBtn').hide();
            });
        }
    },
    
    _duplicateRow: function(options, clicked_btn) {	
        duplicating_element = $(clicked_btn).parents('.easyRowDuplicationContainer:first').find('.easyRowDuplicationRowElement:first');

        if(options.maxDuplications != 0) {
	        if($(duplicating_element).length >= options.maxDuplications) {
		        alert(ERD_MAXROW_INSERT.replace("%d", options.maxDuplications));
		        return false;
	        }
        }	
        
        if(typeof(options.beforeAddCallback) == "function") {
			options.beforeAddCallback.call(this);
		}

		
		last_rowlement = $(duplicating_element);
		if($(duplicating_element).nextAll('.easyRowDuplicationRowElement').last().length != 0) {
			last_rowlement = $(duplicating_element).nextAll('.easyRowDuplicationRowElement').last();
		}
		
		new_element = $(duplicating_element).last().clone().hide().insertAfter(last_rowlement);
		if(options.animation == "none") {
			$(new_element).css('margin-top', options.rowMargin + "px").show();
		} else if(options.animation == "slidedown") {
			$(new_element).css('margin-top', options.rowMargin + "px").slideDown(options.animationSpeed);
		} else if(options.animation == "fadein") {
			$(new_element).css('margin-top', options.rowMargin + "px").fadeIn(options.animationSpeed);
		}
					
		if(options.useCustomDelete == false) {
			$(duplicating_element).parents('.easyRowDuplicationContainer:first').find('> .easyRowDuplicationRowElement').find('.customDelete:last').find('.easyRowDuplicationDelBtn').not(':first').show();
		}

		$(duplicating_element).parents('.easyRowDuplicationContainer:first').find('.easyRowDuplicationDelBtn').unbind('click');
		$(duplicating_element).parents('.easyRowDuplicationContainer:first').find('.easyRowDuplicationDelBtn').on('click', function() {
			easyRowDuplication.prototype._deleteRow(options, this);
		})
				
		if(options.sortable) {
            options.sortableOptions.items = '.' + options.sortableOptions.sortHolderClass;

            $(duplicating_element).parents('.easyRowDuplicationContainer:first').sortable(options.sortableOptions);
            $(duplicating_element).parents('.easyRowDuplicationContainer:first').on("sortupdate", function( event, ui ) {
                $(ui.item).parents('.easyRowDuplicationContainer:first').find(options.sortableOptions.items + " > .customDelete").find('.easyRowDuplicationDelBtn').show();
                $(ui.item).parents('.easyRowDuplicationContainer:first').find(options.sortableOptions.items + ":first > .customDelete").find('.easyRowDuplicationDelBtn').hide();
            });
		}
			
		if(typeof(options.afterAddCallback) == "function") {
			options.afterAddCallback.call(this, duplicating_element);
		}
    },
    
    _deleteRow: function(options, deleting_element) {		    		
		if($(deleting_element).parents('.easyRowDuplicationContainer:first').find('.easyRowDuplicationRowElement').length == 1) {
			alert(ERD_CANNOT_DEL_LAST_ROW);
			return false;
		}
		
        if(typeof(options.beforeDeleteCallback) == "function") {
			options.beforeDeleteCallback.call(this, deleting_element);
		}
  	
        $(deleting_element).closest('.easyRowDuplicationRowElement').remove();
        
        if(typeof(options.afterDeleteCallback) == "function") {
			options.afterDeleteCallback.call(this);
		}
    }

}
}( jQuery ));
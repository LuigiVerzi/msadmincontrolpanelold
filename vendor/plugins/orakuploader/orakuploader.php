<?php
error_reporting(0);
require_once('../../../sw-config.php');

// La lista totale dei formati ammessi per evitare che ne vengano aggiunti altri tramite JS
$total_allowed_ext = array(
    'image/*',
    'image/jpeg',
    'image/png',
    'image/gif',
    'image/svg+xml',
    'application/pdf',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'text/plain',
    'application/zip',
    'application/x-rar',
    'text/xml',
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
);

// Ottiene i formati ammessi per l'upload in questione
$ary_allowed_ext = $_REQUEST['orakuploader_allowed_ext'];
if(!is_array($ary_allowed_ext)) {
    $ary_allowed_ext = explode(",", $_REQUEST['orakuploader_allowed_ext']);
}

// Rimuove eventuali estensioni non ammesse
$ary_allowed_ext = array_intersect($total_allowed_ext, $ary_allowed_ext);

if(!$ary_allowed_ext) {
    $ary_allowed_ext = $total_allowed_ext;
}

if(!isset($_REQUEST['path'])) exit;

$dirback_calc = str_replace('/', '../', preg_replace("#[^\/]#", '', trim($_REQUEST['path'], '/')));
$dirback = $dirback_calc != '' ? '..'.$dirback_calc : "../";

$main_path = UPLOAD_TMP_FOR_DOMAIN;
$thumbnail_path = UPLOAD_TMP_FOR_DOMAIN . "tn";

mkdir($main_path, 0777, true);
mkdir($thumbnail_path, 0777, true);

if(isset($_GET['delete']))
{
	unlink($main_path."/".$_GET['delete']);
	unlink($thumbnail_path."/".$_GET['delete']);
	if(file_exists($main_path."/cache/".$_GET['delete'])) unlink($main_path."/cache/".$_GET['delete']);
	if(file_exists($thumbnail_path."/cache/".$_GET['delete'])) unlink($thumbnail_path."/cache/".$_GET['delete']);
	exit;
}

elseif(isset($_GET['rotate']))
{
	rotateImage($_GET['rotate'], $main_path, $_GET['degree_lvl']);
	rotateImage($_GET['rotate'], $thumbnail_path, $_GET['degree_lvl']);
	echo $_GET['rotate'];
	exit;
}

if($_GET['get_from_url'] == "1") {
    $extra_module_settings = (new \MSFramework\modules())->getExtraModuleSettings('scraper');
    if($extra_module_settings['type'] == "remax") {
        $http_scraper = "http://www.remax.it";
    }

    if (!strstr($_GET['url'], "http")) {
        $_GET['url'] = $http_scraper . $_GET['url'];
        $_GET['filename'] = "scr_down_" . rand(0, 100) . ".jpg";
    } else {
        $expl_url = explode("/", $_GET['url']);
        $_GET['filename'] = end($expl_url);
    }
}

$filename = formatFileName($_GET['filename']);

if($_GET['get_from_url'] == "1") {
    $bytes = file_put_contents(
        $main_path.'/'.($filename),
        file_get_contents($_GET['url'])
    );
} else {
    $bytes = file_put_contents(
        $main_path.'/'.($filename),
        file_get_contents('php://input')
    );
}

$mime = mime_content_type($main_path.'/'.$filename);
$imgsize = @getimagesize($main_path.'/'.$filename);
if(!$imgsize && in_array($mime, $ary_allowed_ext)) {
    $imgsize['mime'] = $mime;
}

if(!isset($imgsize) || !isset($imgsize['mime']))
{
	unlink($main_path.'/'.($filename));
    die('customerr_ext_not_allowed');
}

/*
 * CUSTOM PER SW!
 */

$crop_to_width = isset($_REQUEST['orakuploader_crop_to_width']) ? (int)$_REQUEST['orakuploader_crop_to_width'] : 0;
$crop_to_height = isset($_REQUEST['orakuploader_crop_to_height']) ? (int)$_REQUEST['orakuploader_crop_to_height'] : 0;

$crop_thumb_to_width = isset($_REQUEST['orakuploader_crop_thumb_to_width']) ? (int)$_REQUEST['orakuploader_crop_thumb_to_width'] : 0;
$crop_thumb_to_height = isset($_REQUEST['orakuploader_crop_thumb_to_height']) ? (int)$_REQUEST['orakuploader_crop_thumb_to_height'] : 0;

if(!in_array($imgsize['mime'], $ary_allowed_ext)) {
    unlink($main_path.'/'.($filename));
    die('customerr_ext_not_allowed');
}

$max_bytes = (strpos($mime, 'image') !== false ? 30000000 : 40000000);

if(($bytes >= $max_bytes) || ($bytes <= 8)) {
    unlink($main_path.'/'.($filename));
    die('customerr_heavy_file');
}

if(strpos($mime, 'image') !== false && strpos($mime, 'svg') === false && strpos($mime, 'gif') === false)
{

    if (($_REQUEST['orakuploader_min_width'] != 0 && $imgsize[0] < $_REQUEST['orakuploader_min_width']) || ($_REQUEST['orakuploader_min_height'] != 0 && $imgsize[1] < $_REQUEST['orakuploader_min_height'])) {
        die('customerr_min_size');
    }

    if (($crop_to_width != 0 && $crop_to_width != "" && $crop_to_width < $_REQUEST['orakuploader_min_width']) || ($crop_to_height != 0 && $crop_to_height != "" && $crop_to_height < $_REQUEST['orakuploader_min_height'])) {
        die('customerr_min_size_crop');
    }


    $compression_quality = isset($_REQUEST['orakuploader_compression_quality']) ? (int)$_REQUEST['orakuploader_compression_quality'] : 100;

    $imageCompressor = new ImageCompressor\Compressor($main_path . '/' . $filename, $filename, $compression_quality, $main_path);
    $compression_return = $imageCompressor->compress_image();

    clearstatcache();
    $bytes = filesize($main_path . '/' . $filename);

    // Se non è previsto un ridimensionamento e la dimensione supera già il limite allora interrompo
    if ($crop_to_width == 0 && $crop_to_height == 0 && $bytes >= 3097152) {
        unlink($main_path . '/' . ($filename));
        die('customerr_heavy_file');
    }

    if ( ($crop_to_width > 0 && $crop_to_width < $imgsize[0]) || ($crop_to_height > 0 && $crop_to_height < $imgsize[1])) {
        crop($crop_to_width, $crop_to_height, $main_path, $main_path, $thumbnail_path, $filename);

        clearstatcache();
        $bytes = filesize($main_path . '/' . $filename);

        if ($bytes >= 2097152) {
            unlink($main_path . '/' . ($filename));
            die('customerr_heavy_file');
        }
    }
}

if ((int)$_REQUEST["thumbnail_size"] > 0 || $crop_thumb_to_width > 0 || $crop_thumb_to_height > 0) {

    if ($crop_thumb_to_width > 0 || $crop_thumb_to_height > 0) {
        $thumbnail_width = $crop_thumb_to_width;
        $thumbnail_height = $crop_thumb_to_height;
    } else {
        $thumbnail_width = $_REQUEST["thumbnail_size"];
        $thumbnail_height = 0;
    }
    crop($thumbnail_width, $thumbnail_height, $main_path, $thumbnail_path, $thumbnail_path, $filename);
}

echo $filename;

/*
 * INIZIO FUNZIONI
 */

function formatFileName($s) {

    $s = time().'_'.strtolower($s);
    $s = preg_replace("#\\s+#", "_", $s);

    $cyr = array(
        'ж',  'ч',  'щ',   'ш',  'ю',  'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я',
        'Ж',  'Ч',  'Щ',   'Ш',  'Ю',  'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я');
    $lat = array(
        "l", "s",
        'zh', 'ch', 'sht', 'sh', 'yu', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'y', 'x', 'q',
        'Zh', 'Ch', 'Sht', 'Sh', 'Yu', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', 'Y', 'X', 'Q');

    $s = str_replace($cyr, $lat, $s);

    $replace = array(
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'Ae', 'Å'=>'A', 'Æ'=>'A', 'Ă'=>'A', 'Ą' => 'A', 'ą' => 'a',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'ae', 'å'=>'a', 'ă'=>'a', 'æ'=>'ae',
        'þ'=>'b', 'Þ'=>'B',
        'Ç'=>'C', 'ç'=>'c', 'Ć' => 'C', 'ć' => 'c',
        'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ę' => 'E', 'ę' => 'e',
        'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e',
        'Ğ'=>'G', 'ğ'=>'g',
        'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'İ'=>'I', 'ı'=>'i', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i',
        'Ł' => 'L', 'ł' => 'l',
        'Ñ'=>'N', 'Ń' => 'N', 'ń' => 'n',
        'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe', 'Ø'=>'O', 'ö'=>'oe', 'ø'=>'o',
        'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
        'Š'=>'S', 'š'=>'s', 'Ş'=>'S', 'ș'=>'s', 'Ș'=>'S', 'ş'=>'s', 'ß'=>'ss', 'Ś' => 'S', 'ś' => 's',
        'ț'=>'t', 'Ț'=>'T',
        'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'Ue',
        'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'ue',
        'Ý'=>'Y',
        'ý'=>'y', 'ý'=>'y', 'ÿ'=>'y',
        'Ž'=>'Z', 'ž'=>'z', 'Ż' => 'Z', 'ż' => 'z', 'Ź' => 'Z', 'ź' => 'z',
        '\'' => '', '[' => '-', ']' => '-', '%' => ''
    );
    return strtr($s, $replace);
}

function rotateImage($image_name, $path, $degree_lvl)
{

    $image_extension = @end(explode(".", $image_name));
    if(!in_array($image_extension, array('png', 'jpeg', 'jpg'))) {
        return $image_name;
    }

    if($degree_lvl == 4)
    {
        unlink($path."/".$image_name);
        rename($path."/cache/".$image_name, $path."/".$image_name);
        return $image_name;
    }

    if(!file_exists($path."/cache/".$image_name)) {
        @mkdir($path."/cache", 0777);
        copy($path."/".$image_name, $path."/cache/".$image_name);
        unlink($path."/".$image_name);
    }

    switch($image_extension)
    {
        case "jpg":
            @$image = imagecreatefromjpeg($path."/cache/".$image_name);
            break;
        case "jpeg":
            @$image = imagecreatefromjpeg($path."/cache/".$image_name);
            break;
        case "png":
            $image = imagecreatefrompng($path."/cache/".$image_name);
            break;
    }

    $transColor = imagecolorallocatealpha($image, 255, 255, 255, 270);
    $rotated_image = imagerotate($image, -90*$degree_lvl, $transColor);


    switch($image_extension)
    {
        case "jpg":
            header('Content-type: image/jpeg');
            imagejpeg($rotated_image, "$path/$image_name", 100);
            break;
        case "jpeg":
            header('Content-type: image/jpeg');
            imagejpeg($rotated_image, "$path/$image_name", 100);
            break;
        case "png":
            header('Content-type: image/png');
            imagepng($rotated_image, "$path/$image_name");
            break;
    }
    return $image_name;
}

function crop($max_width, $max_height, $source, $dst, $thumbDirectory, $imageName)
{
    $source_file = $source . "/" . $imageName;
    $dst_dir = $dst . "/" . $imageName;

    if(isset($_REQUEST["watermark"]) && $_REQUEST["watermark"] != '' && $_REQUEST["watermark"] == "true")
    {
        /*
         * CUSTOM!
        */
        $r_agency = (new \MSFramework\cms())->getCMSData('site');
        $loghi = json_decode($r_agency['logos'], true);
        $watermark_to_use = UPLOAD_LOGOS_FOR_DOMAIN . $loghi['watermark'];
        if(is_file($watermark_to_use)) {
            addWatermark($watermark_to_use, $source, $imageName);
            addWatermark($watermark_to_use, $thumbDirectory, $imageName);
        }
        /*
        * FINE CUSTOM!
        */
    }

    clearstatcache();
    $imgsize = getimagesize($source_file);
    $width = $imgsize[0];
    $height = $imgsize[1];
    $mime = $imgsize['mime'];

    // Se l'immagine è già più grande delle dimensioni inserite allora evito il ridimensionamento
    /*if((int)$max_width > $width || (int)$max_height > $height) {
       $max_width = $width;
       $max_height = $height;
    }*/

    $image_extension = @end(explode(".", $source_file));
    if(!in_array($image_extension, array('png', 'jpeg', 'jpg'))) {
        copy("$source/$imageName", "$thumbDirectory/$imageName");
        return true;
    }

    switch($mime){
        case 'image/png':
            $image_create = "imagecreatefrompng";
            $image = "imagepng";
            break;

        case 'image/jpeg':
            $image_create = "imagecreatefromjpeg";
            $image = "imagejpeg";
            break;

        default:
            return false;
            break;
    }


    $src_img = $image_create($source_file);

    if($max_width > 0 && $max_height > 0) {
        if ($max_width == $max_height) {
            if($width < $height) {
                $width_new = $max_width;
                $height_new = floor(($height/$width)*$max_width);
            }
            else {
                $width_new  = floor(($width/$height)*$max_height);
                $height_new = $max_height;
            }
        }
        else if ($max_width > $max_height) {
            $width_new = $max_width;
            $height_new = intval($height * $width_new / $width);
            if($height_new < $max_height) {
                $ratio = $width/$height;
                $height_diff = $max_height-$height_new;
                $height_new = $max_height;
                $width_new += ($height_diff*$ratio);
            }
        }
        else {
            $height_new = $max_height;
            $width_new = intval($width * $height_new / $height);
            if($width_new < $max_width) {
                $ratio = $width/$height;
                $width_diff = $max_width-$width_new;
                $width_new = $max_width;
                $height_new += ($width_diff*$ratio);
            }
        }
        $x_point = abs(intval(($max_width - $width_new) / 2));
        $y_point = abs(intval(($max_height - $height_new) / 2));
        $dst_img = imagecreatetruecolor($max_width, $max_height);
    }
    else {
        if($max_width > $max_height) {
            $width_new = $max_width;
            $height_new = floor(($height/$width)*$max_width);
        }
        else {
            $width_new  = floor(($width/$height)*$max_height);
            $height_new = $max_height;
        }
        $x_point = 0;
        $y_point = 0;
        $dst_img = imagecreatetruecolor($width_new, $height_new);
    }

    if($mime == 'image/png')
    {
        $background = imagecolorallocate($dst_img, 0, 0, 0);
        imagecolortransparent($dst_img, $background);
        imagealphablending($dst_img, false);
        imagesavealpha($dst_img, true);
    }

    imagecopyresampled($dst_img, $src_img, 0, 0, $x_point, $y_point, $width_new, $height_new, $width, $height);

    Global $compression_quality;
    if($mime == 'image/jpeg') {
        $image($dst_img, $dst_dir, $compression_quality);
    } else {
        $image($dst_img, $dst_dir);
    }

    if($dst_img)imagedestroy($dst_img);
    if($src_img)imagedestroy($src_img);
}

function addWatermark($watermark, $imageDirectory, $imageName, $x = 0, $y = 0)
{
    $image_extension = @end(explode(".", $imageName));
    if($image_extension == "svg") {
        //se l'immagine in questione è un SVG non aggiungo il watermark. per il momento non serve.
        return false;
    }

    if(file_exists($watermark))
    {
        $marge_right  = 0;
        $marge_bottom = 0;

        /*
        * CUSTOM!
        */

        $stamp_extension = @end(explode(".", $watermark));
        switch($stamp_extension)
        {
            case "jpg":
                $stamp = imagecreatefromjpeg($watermark);
                break;
            case "jpeg":
                $stamp = imagecreatefromjpeg($watermark);
                break;
            case "png":
                $stamp = imagecreatefrompng($watermark);
                break;
        }

        $opacity = 0.2;
        imagealphablending($stamp, false); // imagesavealpha can only be used by doing this for some reason
        imagesavealpha($stamp, true); // this one helps you keep the alpha.
        $transparency = 1 - $opacity;
        imagefilter($stamp, IMG_FILTER_COLORIZE, 0,0,0,127*$transparency); // the fourth parameter is alpha
        /*
        * FINE CUSTOM!
        */


        switch($image_extension)
        {
            case "jpg":
                $im = imagecreatefromjpeg("$imageDirectory/$imageName");
                break;
            case "jpeg":
                $im = imagecreatefromjpeg("$imageDirectory/$imageName");
                break;
            case "png":
                $im = imagecreatefrompng("$imageDirectory/$imageName");
                break;
        }

        $imageSize = getimagesize("$imageDirectory/$imageName");
        $watermark_o_width = imagesx($stamp);
        $watermark_o_height = imagesy($stamp);

        $newWatermarkWidth = $imageSize[0]/2;
        $newWatermarkHeight = ($watermark_o_height * $newWatermarkWidth / $watermark_o_width)/1.2;


        if((int)$x <= 0)
            $x = $imageSize[0]/2 - $newWatermarkWidth/2;
        if((int)$y <= 0)
            $y = $imageSize[1]/2 - $newWatermarkHeight/2;

        imagecopyresized($im, $stamp, $x, $y, 0, 0, $newWatermarkWidth, $newWatermarkHeight, imagesx($stamp), imagesy($stamp));

        switch($image_extension)
        {
            case "jpg":
                header('Content-type: image/jpeg');
                imagejpeg($im, "$imageDirectory/$imageName", 100);
                break;
            case "jpeg":
                header('Content-type: image/jpeg');
                imagejpeg($im, "$imageDirectory/$imageName", 100);
                break;
            case "png":
                header('Content-type: image/png');
                imagepng($im, "$imageDirectory/$imageName");
                break;
        }
    }
}
?>
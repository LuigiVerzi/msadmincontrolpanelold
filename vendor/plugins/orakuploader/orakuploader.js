(function ( $ ) {

    $.fn.orakuploader = function( options ) {

        var settings = $.extend({

            orakuploader       		    	 : true,
            orakuploader_use_main          	 : false,
            orakuploader_use_sortable      	 : false,
            orakuploader_use_dragndrop       : false,
            orakuploader_use_rotation        : false,
            orakuploader_hide_on_exceed      : false,
            orakuploader_hide_in_progress    : false,
            orakuploader_attach_images       : [],
            orakuploader_path  		         : 'orakuploader/',
            orakuploader_main_path           : 'files',
            orakuploader_thumbnail_path      : 'files/tn',
            orakuploader_real_path          : 'files/tn',
            orakuploader_resize_to           : 0,
            orakuploader_thumbnail_size      : 0,
            orakuploader_file_delete_label 	 : "",
            orakuploader_file_rotation_label : "",
            orakuploader_field_name          : $(this).attr('id'),
            orakuploader_add_image           : '',
            orakuploader_add_label           : ['Browser for images', 'Browser for files'],
            orakuploader_main_changed        : "",
            orakuploader_finished		     : "",
            orakuploader_picture_deleted     : "",
            orakuploader_maximum_uploads     : 100,
            orakuploader_crop_to_width       : 0,
            orakuploader_crop_to_height      : 0,
            orakuploader_crop_thumb_to_width : 0,
            orakuploader_crop_thumb_to_height: 0,
            orakuploader_compression_quality: 100, //custom per SW
            orakuploader_max_exceeded 	     : "",
            orakuploader_watermark           : "",
            orakuploader_min_width 			 : 0, //custom per SW
            orakuploader_min_height			 : 0, //custom per SW
            orakuploader_allowed_ext		 : [], //custom per SW

        }, options);

        var holdername = this;

        if($(this).attr('orakuploader') == 'on') {
            var imageHolderId = '#'+$(this).attr('id');
            holdername = $(this).replaceWith(getHtml($(this).attr('id'), settings));
            holdername = $("body").find(imageHolderId);
        }

        jQuery.data(holdername, 'already_uploaded', 1);
        jQuery.data(holdername, 'count', 0);
        jQuery.data(holdername, 'counter', 0);

        if(settings.orakuploader_use_sortable)
        {
            $(holdername).sortable({
                update: function(event, ui) {

                    if (typeof settings.orakuploader_rearranged == 'function') {
                        settings.orakuploader_rearranged();
                    }
                    changeMain(holdername, settings);
                }
            });

            $("#"+holdername.attr("id")+"_to_clone").find(".file").css("cursor", "move");
            $("#"+holdername.attr("id")+"_to_clone").find(".multibox .file").css("cursor", "move");
        }
        else
        {
            $("#"+holdername.attr("id")+"_to_clone").find(".file").css("cursor", "auto");
            $("#"+holdername.attr("id")+"_to_clone").find(".multibox .file").css("cursor", "auto");
        }
        $(holdername).disableSelection();

        $(document).on("change", "."+$(holdername).attr("id")+"Input", function() {
            if(settings.orakuploader_hide_in_progress == true) {
                if(parseInt(jQuery.data(holdername, 'currently_uploading')) == 1) return false;
                jQuery.data(holdername, 'currently_uploading', 1);
                $(holdername).parent().find('.uploadButton').hide();
            }
            jQuery.data(holdername, 'count', 0);
            orakuploaderHandle(this.files,holdername,settings);
        });

        for (i = 0; i < settings.orakuploader_attach_images.length; i++) {
            var image = settings.orakuploader_attach_images[i];

            var clone = $("#"+$(holdername).attr("id")+"_to_clone").find(".multibox").clone();
            $(holdername).append($(clone));
            //custom per SW

            /*
            In alcuni casi l'uploader viene ricaricato "al volo" quindi ho bisogno di recuperare le immagini nuovamente dalla TMP.
            Con questa funzione testo la presenza dell'immagine nella cartella REALE, se non la trovo, provo a caricare l'immagine dalla TMP.
            Se non la trovo nemmeno nella TMP, mostro l'immagine di errore (funzione onerror sel tag img)
            */

            var re = /(?:\.([^.]+))?$/;
            var ext = re.exec(image)[1].toLowerCase();

            html_to_push = "<div class='picture_delete'>"+settings.orakuploader_file_delete_label+"</div>";

            if(ext == "mp4") {
                html_to_push += "<video style='height: 100% !important' autoplay loop muted><source src='" + $('#' + $(holdername).attr("id") + '_realPath').val() + "/tn/"+image+"' type='video/" + ext + "'></video>";
            } else if(ext == "pdf") {
                html_to_push +=
                    '<object style="width:100%;height:100%;" data="' + $('#' + $(holdername).attr("id") + '_realPath').val() + "/tn/"+image+ '?#zoom=50&scrollbar=0&toolbar=0&navpanes=0" type="application/pdf">' +
                    '<embed style="width:100%;height:100%;" src="' + $('#' + $(holdername).attr("id") + '_realPath').val() + "/tn/"+image+ '?#zoom=50&scrollbar=0&toolbar=0&navpanes=0" type="application/pdf">' +
                    '</object>';
            } else {
                html_to_push += '<img src="' + $('#' + $(holdername).attr("id") + '_realPath').val() + "/tn/" + image + '" alt="" onerror="loadFileIcon(this, \'' + ext + '\', \'' + settings.orakuploader_path + '\')" class="picture_uploaded"/>';
            }

            html_to_push += "<input type='hidden' value='"+image+"' name='"+settings.orakuploader_field_name+"[]' />";

            $(clone).html(html_to_push);

            $(clone).attr('id', image);
            $(clone).attr('filename', image);
        }

        if(settings.orakuploader_attach_images.length > 0) {
            //custom per SW
            jQuery.data(holdername, "already_uploaded", settings.orakuploader_attach_images.length+1);
            if(settings.orakuploader_hide_on_exceed == true && settings.orakuploader_attach_images.length == parseInt(settings.orakuploader_maximum_uploads)) {
                $(holdername).parent().find('.uploadButton').hide();
            }

            changeMain(holdername, settings);
        }

        $(holdername).on("click", ".picture_delete", function() {

            jQuery.data(holdername, "already_uploaded", jQuery.data(holdername, "already_uploaded")-1);

            $.ajax({
                url: settings.orakuploader_path+"orakuploader.php?delete="+encodeURIComponent($(this).parent().attr('filename'))+"&path="+settings.orakuploader_path+"&resize_to="+settings.orakuploader_resize_to+"&thumbnail_size="+settings.orakuploader_thumbnail_size+"&main_path="+settings.orakuploader_main_path+"&thumbnail_path="+settings.orakuploader_thumbnail_path
            });

            $(this).parent().fadeOut("slow", function() {
                $(this).remove();

                if(jQuery.data(holdername, "already_uploaded")-1 > 0) {
                    changeMain(holdername, settings);
                }

                if(settings.orakuploader_hide_on_exceed == true) {
                    $(holdername).parent().find('.uploadButton').show();
                }
            });

            if (typeof settings.orakuploader_picture_deleted == 'function') {
                settings.orakuploader_picture_deleted($(this).parent().attr('filename'));
            }
        });

        $(holdername).on("click", ".rotate_picture", function() {
            var context = this;

            $.ajax({
                url: settings.orakuploader_path+"orakuploader.php?rotate="+encodeURIComponent($(this).parent().attr('filename'))+"&degree_lvl="+$(this).closest('.rotate_picture').attr('degree-lvl')+"&path="+settings.orakuploader_path+"&resize_to="+settings.orakuploader_resize_to+"&thumbnail_size="+settings.orakuploader_thumbnail_size+"&main_path="+settings.orakuploader_main_path+"&thumbnail_path="+settings.orakuploader_thumbnail_path
            }).done(function(file_name) {
                $img = $('html,body').find("input[value^='"+file_name+"']").prev('img');

                if(parseInt($(context).closest('.rotate_picture').attr('degree-lvl')) > 3)  {
                    $(context).closest('.rotate_picture').attr('degree-lvl', 1)
                } else {
                    $(context).closest('.rotate_picture').attr('degree-lvl', parseInt($(context).closest('.rotate_picture').attr('degree-lvl'))+1)
                }

                $img.attr('src', $img.attr('src') +"?"+ new Date().getTime());
            });
        });

        if(settings.orakuploader_use_dragndrop)
        {
            var holder = document.getElementById($(holdername).attr("id")+"DDArea");
            holder.ondragover = function () { $(".uploadButton").addClass("DragAndDropHover"); return false; };
            holder.ondragend  = function () { $(".uploadButton").removeClass("DragAndDropHover"); return false; };

            holder.ondrop = function (e) {
                $(".uploadButton").removeClass("DragAndDropHover");
                e.preventDefault();
                orakuploaderHandle(e.dataTransfer.files,holdername,settings);
            }
        }

    };

    function changeMain(holder, settings) {
        if(settings.orakuploader_use_main)
        {
            $(holder).find(".multibox").removeClass("main");
            $(holder).find(".multibox").first().addClass("main");

            if (typeof settings.orakuploader_main_changed == 'function') {
                settings.orakuploader_main_changed($(holder).find(".multibox").first().attr('filename'));
            }
        }
    }

    function orakuploaderHandle(files,holder,settings) {
        var i = 0;
        var msg_alert = false;
        for(i=0;i<files.length;i++)
        {
            if(jQuery.data(holder, "already_uploaded") > settings.orakuploader_maximum_uploads && (typeof settings.orakuploader_max_exceeded == 'function')) {
                if(msg_alert == false) settings.orakuploader_max_exceeded();
                msg_alert = true;
                if(settings.orakuploader_hide_on_exceed == true) $(holder).closest('.uploadButton').hide();
            }
            var re = /(?:\.([^.]+))?$/;
            var ext = re.exec(files[i].name)[1].toLowerCase();

            if(jQuery.data(holder, "already_uploaded") <= settings.orakuploader_maximum_uploads)
            //if((ext == 'jpg' || ext == 'jpeg' || ext == 'png') &&  jQuery.data(holder, "already_uploaded") <= settings.orakuploader_maximum_uploads)
            {
                var clone = $("#"+$(holder).attr("id")+"_to_clone").find(".multibox").clone();

                $(holder).append($(clone));
                upload(files[i], clone, i, holder, settings);
                jQuery.data(holder, "already_uploaded", jQuery.data(holder, "already_uploaded")+1);
                jQuery.data(holder, "count", jQuery.data(holder, "count")+1);
            }
        }
    }

    window.counter = 0;
    function upload(file, clone, place, holder, settings)
    {

        if(settings.orakuploader_hide_on_exceed == true && parseInt(jQuery.data(holder, 'already_uploaded')) == parseInt(settings.orakuploader_maximum_uploads)) {
            $(holder).parent().find('.uploadButton').hide();
        }
        var xhr = new XMLHttpRequest();

        //custom!
        finalval_crop_width = settings.orakuploader_crop_to_width;
        finalval_crop_height = settings.orakuploader_crop_to_height;
        finalval_crop_thumb_width = settings.orakuploader_crop_thumb_to_width;
        finalval_crop_thumb_height = settings.orakuploader_crop_thumb_to_height;
        finalval_compression_quality = settings.orakuploader_compression_quality;
        finalval_add_watermark = settings.orakuploader_watermark;

        if($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_width').val() != "" && !isNaN(parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_width').val())) && isFinite($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_width').val())) {
            finalval_crop_width = parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_width').val());
        }

        if($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_height').val() != "" && !isNaN(parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_height').val())) && isFinite($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_height').val())) {
            finalval_crop_height = parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_height').val());
        }

        if($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_width_thumb').val() != "" && !isNaN(parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_width_thumb').val())) && isFinite($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_width_thumb').val())) {
            finalval_crop_thumb_width = parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_width_thumb').val());
        }

        if($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_height_thumb').val() != "" && !isNaN(parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_height_thumb').val())) && isFinite($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_height_thumb').val())) {
            finalval_crop_thumb_height = parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_crop_height_thumb').val());
        }

        if($('#' + settings.orakuploader_field_name + '_modalUplSettings_compression_quality').data('current') > 0 && !isNaN(parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_compression_quality').data('current'))) && isFinite($('#' + settings.orakuploader_field_name + '_modalUplSettings_compression_quality').data('current'))) {
            finalval_compression_quality = parseFloat($('#' + settings.orakuploader_field_name + '_modalUplSettings_compression_quality').data('current'));
        }

        if($('#' + settings.orakuploader_field_name + '_modalUplSettings_apply_watermark:checked').length != 0) {
            finalval_add_watermark = true;
        }
        //fine custom!

        xhr.open("POST", settings.orakuploader_path+"orakuploader.php?filename="+encodeURIComponent(file.name)+"&path="+settings.orakuploader_path+"&resize_to="+settings.orakuploader_resize_to+"&thumbnail_size="+settings.orakuploader_thumbnail_size+"&main_path="+settings.orakuploader_main_path+"&thumbnail_path="+settings.orakuploader_thumbnail_path+"&watermark="+finalval_add_watermark+"&orakuploader_crop_to_width="+finalval_crop_width+"&orakuploader_crop_to_height="+finalval_crop_height+"&orakuploader_crop_thumb_to_width="+finalval_crop_thumb_width+"&orakuploader_crop_thumb_to_height="+finalval_crop_thumb_height+"&orakuploader_compression_quality="+finalval_compression_quality+"&orakuploader_min_width=" + settings.orakuploader_min_width+"&orakuploader_min_height=" + settings.orakuploader_min_height+"&orakuploader_allowed_ext=" + encodeURIComponent(settings.orakuploader_allowed_ext), true);
        xhr.send(file);
        xhr.onreadystatechange = function()
        {

            var rotation_html = "";
            if (xhr.readyState == 4)
            {
                if(settings.orakuploader_use_rotation == true) {
                    rotation_html = "<div class='rotate_picture' degree-lvl='1'>"+settings.orakuploader_file_rotation_label+"</div>";
                }

                if(xhr.responseText.substr(0, 9) != "customerr") {

                    //nuova gestione
                    var re = /(?:\.([^.]+))?$/;
                    var ext = re.exec(xhr.responseText)[1].toLowerCase();
                    html_to_push = "<div class='picture_delete'>" + settings.orakuploader_file_delete_label + "</div>" + rotation_html;

                    if (ext == "mp4") {
                        html_to_push += "<video style='height: 100% !important' autoplay loop muted><source src='" + settings.orakuploader_thumbnail_path + "/" + xhr.responseText + "' type='video/" + ext + "'></video>";
                    } else if(ext == "pdf") {
                        html_to_push +=
                            '<object style="height: 100% !important; width: 100% !important;" data="' + settings.orakuploader_thumbnail_path + "/" + xhr.responseText + '?#zoom=50&scrollbar=0&toolbar=0&navpanes=0" type="application/pdf">' +
                            '<embed style="height: 100% !important; width: 100% !important;" src="' + settings.orakuploader_thumbnail_path + "/" + xhr.responseText + '?#zoom=50&scrollbar=0&toolbar=0&navpanes=0" type="application/pdf">' +
                            '</object>';
                    } else {
                        html_to_push += '<img src="' + settings.orakuploader_thumbnail_path + '/' + xhr.responseText + '" alt="" onerror="loadFileIcon(this, \'' + ext + '\', \'' + settings.orakuploader_path + '\')" class="picture_uploaded"/>';
                    }

                    html_to_push += " <input type='hidden' value='" + xhr.responseText + "' name='" + settings.orakuploader_field_name + "[]' />";
                } else {
                    html_to_push = "<div class='picture_delete'>" + settings.orakuploader_file_delete_label + "</div>";
                }

                $(clone).html(html_to_push);

                //vecchia gestione
                //$(clone).html("<div class='picture_delete'>"+settings.orakuploader_file_delete_label+"</div>"+rotation_html+"<img src='"+settings.orakuploader_thumbnail_path+"/"+xhr.responseText+"' alt='' onerror=this.src='"+settings.orakuploader_path+"/images/no-image.jpg' class='picture_uploaded'/> <input type='hidden' value='"+xhr.responseText+"' name='"+settings.orakuploader_field_name+"[]' />");

                if (typeof settings.orakuploader_finished == 'function') {
                    settings.orakuploader_finished(xhr.responseText);
                }

                $(clone).attr('id', xhr.responseText);
                $(clone).attr('filename', xhr.responseText);
                jQuery.data(holder, "counter", jQuery.data(holder, "counter")+1);
                if(jQuery.data(holder, "count") == jQuery.data(holder, "counter"))
                {
                    changeMain(holder, settings);
                    jQuery.data(holder, "counter", 0);
                    if(settings.orakuploader_hide_in_progress == true) {
                        jQuery.data(holder, 'currently_uploading', 0);
                        $(holder).parent().find('.uploadButton').show();
                    }
                }

                if(xhr.responseText.substr(0, 9) == "customerr") {
                    $(clone).find('.picture_delete').trigger('click');
                }
            }
        }
    }

}( jQuery ));

window.initialized = 0;

function orakuploaderLoad(name) {
    $('.'+(name)+'Input').click();
    window.initialized++;
}

function getHtml(name, settings)
{

    var accept = '';
    var add_label = settings.orakuploader_add_label[0];

    var only_images = true;
    if(settings.orakuploader_allowed_ext.length) {
        settings.orakuploader_allowed_ext.forEach(function (ext) {
           if(ext.indexOf('image/') < 0) {
               only_images = false;
           }
        });
    }

    if(only_images) {
        accept += 'image/*';
    } else {
        accept = settings.orakuploader_allowed_ext.join(',');
        add_label = settings.orakuploader_add_label[1];
    }

    return '<div id="'+name+'_to_clone" class="clone_item"><div class="multibox file"><div class="loading"><img src="'+settings.orakuploader_path+'/images/loader.gif" alt="loader"/></div></div></div>\
		<div id="'+name+'DDArea">\
			<div id="'+name+'">\
			</div>\
			<div class="multibox uploadButton" onclick="javascript:orakuploaderLoad(\''+name+'\');">\
			<img src="'+ settings.orakuploader_add_image+'"/>\
			'+ add_label +'\
			</div>\
			<input type="file" class="'+name+'Input orakuploaderFileInput" accept="' + accept + '" multiple/>\
			<div class="clear"> </div>\
		</div>';

}

function loadFileIcon(element, ext, basepath) {
    var image = new Image();

    var full_name = $(element).closest('.multibox').attr('id');

    var file_name = full_name.split('_');
    file_name.shift();
    file_name = file_name.join('_');

    if(!$(element).parent().find('.name').length) {
        $('<span class="name">' + file_name + '</span>').insertAfter(element);
    }

    image.onload = function() {
        element.src = this.src;
    };

    if($(element).attr('src').indexOf('images/ext_icons')) {
        image.onerror = function () {
            element.src = basepath + '/images/no-image.jpg';
        };
    } else {
        image.onerror = function () {
            element.src = basepath + '/images/ext_icons/' + ext + '.png';
        };
    }

    if($(element).attr('src').indexOf($('#baseElementPathTmpFolder').val()) < 0) {
        image.src = $('#baseElementPathTmpFolder').val() + 'tn/' + full_name
    } else {
        image.src = basepath + '/images/ext_icons/' + ext + '.png';
    }
}
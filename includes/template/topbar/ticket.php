<?php
$have_message = 0;

$global_active_ids = array();
$global_active_array = array();
if((string)$userDetails['ruolo'] === "0") {
    $global_active_ids = $MSFrameworkTicket->getActiveTicketQuestionsIds(10, true);
    $global_active_array = ($global_active_ids ? $MSFrameworkTicket->getTicketQuestionDetails($global_active_ids, false) : array());
}

$active_tickets_ids = $MSFrameworkTicket->getActiveTicketQuestionsIds();
$active_tickets_array = ($active_tickets_ids ? $MSFrameworkTicket->getTicketQuestionDetails($active_tickets_ids, false) : array());

$ticket_count = 0;
foreach($active_tickets_array as $tinfo) {
    if($tinfo['have_unread'] > 0) $ticket_count += $tinfo['have_unread'];
}

$admin_count = 0;
foreach($global_active_array as $tinfo) {
    if($tinfo['admin_unread'] > 0) $admin_count += $tinfo['admin_unread'];
}
?>

<li class="dropdown">
    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
        <i class="fa fa-life-ring" aria-hidden="true"></i>
        <?php
        if((string)$userDetails['ruolo'] === "0") {
            if ($admin_count > 0) {
                echo '<span class="label label-danger">' . $admin_count . '</span>';
            }
        } else {
            if ($ticket_count > 0) {
                echo '<span class="label label-warning">' . $ticket_count . '</span>';
            }
        }
        ?>
        <label>Assistenza</label>
    </a>
    <ul class="dropdown-menu dropdown-alerts">
        <?php if((string)$userDetails['ruolo'] === "0" && $global_active_array) { ?>
            <li><h4 style="margin: 15px 0px 10px;">Richieste aperte (SuperAdmin)</h4></li>
            <li class="dropdown-divider"></li>
            <?php foreach($MSFrameworkTicket->getTicketQuestionDetails($global_active_ids, false) as $tinfo) { ?>
                <li>
                    <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>framework360/assistenza/ticket/edit.php?id=<?= $tinfo['id']; ?>" class="dropdown-item">
                        <div style="position:relative;">
                            <i class="fa fa-envelope fa-fw"></i> <?= $tinfo['title']; ?>
                            <?php if($tinfo['admin_unread'] > 0) { ?>
                                <span class="pull-right"><span class="label label-danger"><?= $tinfo['admin_unread']; ?></span></span>
                            <?php } ?>
                        </div>
                    </a>
                </li>
            <?php } ?>
        <?php } ?>

        <?php if(count($active_tickets_ids) > 0) { ?>
            <li><h4 style="margin: 15px 0px 10px;"><?= ((string)$userDetails['ruolo'] === "0" ? 'In corso su ' . SW_NAME : 'Richieste in corso'); ?></h4></li>
            <li class="dropdown-divider"></li>
            <?php foreach($MSFrameworkTicket->getTicketQuestionDetails($active_tickets_ids, false) as $tinfo) { ?>
                <li>
                    <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>assistenza/ticket/view.php?id=<?= $tinfo['id']; ?>" class="dropdown-item">
                        <div style="position:relative;">
                            <i class="fa fa-envelope fa-fw"></i> <?= $tinfo['title']; ?>
                            <?php if($tinfo['have_unread'] > 0 && (string)$userDetails['ruolo'] !== "0") { ?>
                                <span class="pull-right"><span class="label label-warning"><?= $tinfo['have_unread']; ?></span></span>
                            <?php } ?>
                        </div>
                    </a>
                </li>
            <?php } ?>
        <?php } ?>

        <?php if(!$active_tickets_ids && !$global_active_ids) { ?>
            <li>
                <a class="dropdown-item">
                    <div class="text-muted text-center">
                        Nessuna richiesta di assistenza in corso.
                    </div>
                </a>
            </li>
        <?php } ?>

        <li class="dropdown-divider"></li>
        <li>
            <?php if((string)$userDetails['ruolo'] === "0") { ?>
                <div class="text-center link-block">
                    <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>framework360/assistenza/ticket/" class="dropdown-item text-left pull-left btn btn-info btn-outline" style="margin: 0;">
                        <strong>Lista Globale</strong>
                    </a>

                    <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>assistenza/ticket/" class="dropdown-item text-right pull-right" style="margin: 0;">
                        <strong>Ticket sul sito</strong>
                    </a>
                </div>
            <?php } else { ?>
                <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>assistenza/ticket/" class="dropdown-item text-left pull-left" style="margin: 0;">
                    <strong>Lista ticket</strong>
                </a>

                <a href="<?= ABSOLUTE_ADMIN_MODULES_PATH_HTML; ?>assistenza/ticket/edit.php" class="dropdown-item text-right pull-right btn btn-info btn-outline" style="margin: 0;">
                    <strong>Apri nuovo Ticket</strong>
                </a>
            <?php } ?>
        </li>
    </ul>
</li>
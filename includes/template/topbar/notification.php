<li class="dropdown push_notification">
    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
        <i class="fa fa-bell" aria-hidden="true"></i>
        <label>Notifiche</label>
    </a>
    <ul class="dropdown-menu dropdown-alerts">

        <li>
            <h4 style="margin: 15px 0px 10px;">
                Notifiche Push
                <span id="pushNotificationStatus" style="float: right;"></span>
            </h4>
        </li>
        <li class="dropdown-divider"></li>
        <li>
            <span style="display: block; margin: 10px 0;">Il sistema di notifiche push ti permette di ricevere delle notifiche ogni qualvolta viene effettuata un'azione importate sul sito web.</span>
        </li>
        <li class="dropdown-divider"></li>
        <li>
            <div class="text-center link-block">
                <a href="#" class="dropdown-item" id="enablePushNotification">
                    <strong>Attiva notifiche Push</strong>
                </a>
            </div>
        </li>
    </ul>
</li>
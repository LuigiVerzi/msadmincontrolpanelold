<?php
$creditStatusLabel = array(
    'class' => 'danger',
    'message' => 'I tuoi crediti stanno per esaurirsi, acquista un nuovo pacchetto per usufruire di tutte le funzionalità.',
);

if(CUSTOMER_DOMAIN_INFO['credits'] > 200) {
    $creditStatusLabel = array(
        'class' => 'success',
        'message' => 'Hai crediti sufficienti per usufruire di tutte le funzionalità.'
    );
} else if(CUSTOMER_DOMAIN_INFO['credits'] > 50) {
    $creditStatusLabel = array(
        'class' => 'warning',
        'message' => 'I tuoi crediti stanno per esaurirsi, acquista un nuovo pacchetto per usufruire di tutte le funzionalità.',
    );
}
?>

<li class="dropdown push_notification">
    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
        <i class="fa fa-coins" aria-hidden="true"></i>
        <label>Crediti</label>
    </a>
    <ul class="dropdown-menu dropdown-alerts">

        <li>
            <h4 style="margin: 15px 0px 10px;">
                I tuoi crediti attuali
                <span id="creditsStatus" style="float: right;" class="label label-<?= $creditStatusLabel['class']; ?>"><?= CUSTOMER_DOMAIN_INFO['credits']; ?></span>
            </h4>
        </li>
        <li class="dropdown-divider"></li>
        <li>
            <span style="display: block; margin: 10px 0;"><?= $creditStatusLabel['message']; ?></span>
        </li>
        <li class="dropdown-divider"></li>
        <li>
            <div class="text-center link-block">
                <a href="<?= (new \MSSoftware\modules())->getModuleUrlByID('ms_agency_servizi'); ?>#8" class="dropdown-item btn btn-primary btn-block" style="color: white;" id="buyCreditsBtn">
                    <strong>Acquista crediti</strong>
                </a>
            </div>
        </li>
    </ul>
</li>
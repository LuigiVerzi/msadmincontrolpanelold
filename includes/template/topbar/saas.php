<?php
$envData = $MSFrameworkSaaSEnvironments->getEnvironmentDataFromDB($_SESSION['SaaS']['saas__environments']['id'], "user_email, expiration_date")[$_SESSION['SaaS']['saas__environments']['id']];
$data_scadenza_abbonamento_saas = (new \DateTime($envData['expiration_date']))->format("d/m/Y");
$saas_expired = $MSFrameworkSaaSEnvironments->isExpired();
if ($_SESSION['userData']['userlevel'] == "0") { ?>
    <li>
        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
            <i class="fa fa-cloud" aria-hidden="true"></i>
            <label>SaaS</label>
        </a>
        <ul class="dropdown-menu dropdown-alerts">
            <li>
                <h4 style="margin: 15px 0px 10px;">
                    Gestione Ambiente Saas
                </h4>
            </li>
            <li class="dropdown-divider"></li>
            <li>
                <span style="display: block; margin: 10px 0;">Sei un SuperAdmin e stai gestendo l'ambiente di un cliente. Tutte le modifiche si rifletteranno sull'account del cliente.</span>
            </li>
            <li>
                <span style="display: block; margin: 10px 0;">Proprietario: <strong><?= $envData['user_email'] ?></strong></span>
            </li>
            <li>
                <span style="display: block; margin: 10px 0;">Database: <strong><?= $_SESSION['SaaS']['saas__environments']['database'] ?></strong></span>
            </li>
            <li>
                <span style="display: block; margin: 10px 0;">Scadenza abbonamento: <strong><?= $data_scadenza_abbonamento_saas ?></strong></span>
            </li>
            <li>
                <span style="display: block; margin: 10px 0;">Nome SaaS: <strong><?= SW_NAME ?></strong></span>
            </li>
            <li class="dropdown-divider"></li>
            <li>
                <div class="row">
                    <div class="col-md-<?= ($_SESSION['SaaS']['saas__environments']['id'] != "front" ? "6" : "12") ?>">
                        <div class="text-center link-block">
                            <a href="<?php echo ABSOLUTE_SW_PATH_HTML ?>login/saasSelector.php"
                               class="dropdown-item" style="padding: 0">
                                <strong>Gestisci un altro ambiente</strong>
                            </a>
                        </div>
                    </div>

                    <?php
                    if($_SESSION['SaaS']['saas__environments']['id'] != "front") {
                        ?>
                        <div class="col-md-6">
                            <div class="text-center link-block">
                                <a href="<?php echo ABSOLUTE_SW_PATH_HTML ?>modules/framework360/saas/environments/edit.php?id=<?= $_SESSION['SaaS']['saas__environments']['id'] ?>"
                                   class="dropdown-item" style="padding: 0">
                                    <strong>Amministra ambiente</strong>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </li>
        </ul>
    </li>
<?php } else if ($MSFrameworkSaaSEnvironments->getOwnerID() == $_SESSION['userData']['user_id']) { ?>
    <li>
        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
            <i class="fa fa-cubes <?= ($saas_expired ? 'text-danger fa-beat' : '') ?>" aria-hidden="true"></i>
            <label class="<?= ($saas_expired ? 'text-danger' : '') ?>">Abbonamento</label>
        </a>
        <ul class="dropdown-menu dropdown-alerts">
            <li>
                <h4 style="margin: 15px 0px 10px;">
                    Gestione Abbonamento
                </h4>
            </li>
            <li class="dropdown-divider"></li>
            <li>
                <span style="display: block; margin: 10px 0;">Il tuo abbonamento <?= ($saas_expired ? "è scaduto" : "scadrà") ?> il <?= $data_scadenza_abbonamento_saas ?></span>
            </li>

            <li class="dropdown-divider"></li>
            <li>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center link-block">
                            <a href="<?php echo ABSOLUTE_SW_PATH_HTML ?>modules/saas/abbonamento/"
                               class="dropdown-item" style="padding: 0">
                                <strong>Gestisci abbonamento</strong>
                            </a>
                        </div>
                    </div>
                </div>

            </li>
        </ul>
    </li>
<?php } ?>
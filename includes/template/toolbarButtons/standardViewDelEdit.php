<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */ ?>

<div class="title-action">

    <?php
    if(isset($customToolbarButton)) {
        echo $customToolbarButton;
    }
    ?>

    <a class="btn btn-danger btn-outline" id="editDeleteBtn">Elimina</a>
    <a class="btn btn-danger no_check" id="editUndoBtn">Esci</a>
</div>
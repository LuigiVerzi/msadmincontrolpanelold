<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */ ?>

<div class="title-action">
    <?php
    if(isset($customToolbarButton)) {
        echo $customToolbarButton;
    }
    if(strstr($module_config['id'], 'errors_log_')) {
    ?>
    <a class="btn btn-success" id="dataTableBugSolved">Contrassegna come risolto</a>
    <?php } ?>

    <a class="btn btn-warning" id="dataTableEdit">Visualizza</a>
</div>
<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */ ?>

<div class="title-action">
    <?php
    if(isset($customToolbarButton)) {
        echo $customToolbarButton;
    }
    ?>

    <?php if($module_config['id'] == "newsletter__emails_csv" || $module_config['id'] == "importazione_csv_clienti" || $module_config['id'] == "importazione_csv_prodotti") { ?>
        <a class="btn btn-primary" id="start_import_csv">Avvia Importazione</a>
        <a class="btn btn-warning" id="start_export_csv">Avvia Esportazione</a>
    <?php } ?>
    <a class="btn btn-danger btn-outline no_check" id="editUndoBtn">Esci</a>
</div>
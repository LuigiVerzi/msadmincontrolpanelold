<?php
/**
 * MSAdminControlPanel
 * Date: 03/06/18
 */ ?>

<div class="title-action">
    <a class="btn btn-primary" id="editSaveAsNewBtn" saving_txt="Salvo i dati...">Salva come nuovo</a>
    <a class="btn btn-primary" id="editSaveBtn" saving_txt="Salvo i dati...">Salva</a>
    <a class="btn btn-danger" id="editUndoBtn">Indietro</a>
</div>
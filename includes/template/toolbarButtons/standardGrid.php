<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */ ?>

<div class="title-action">
    <?php
    if(isset($customToolbarButton)) {
        echo $customToolbarButton;
    }
    ?>

    <?php if((new \MSFramework\Sharer\sharer())->canShare($module_config['id'])) { ?>
        <a class="btn btn-info" id="dataTableSocialShare"><i class="fa fa-share-alt"></i> Condividi</a>
    <?php } ?>

    <a class="btn btn-primary" id="dataTableNew">Aggiungi</a>
    <a class="btn btn-warning" id="dataTableEdit">Modifica</a>
    <a class="btn btn-danger" id="dataTableDelete" deleting_txt="Elimino i dati...">Elimina</a>
</div>
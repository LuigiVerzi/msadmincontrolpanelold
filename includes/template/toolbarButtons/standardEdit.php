<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */ ?>

<div class="title-action">
    <?php
    if(isset($customToolbarButton)) {
        echo $customToolbarButton;
    }
    ?>

    <a class="btn btn-primary" id="editSaveBtn" saving_txt="Salvo i dati...">Salva</a>
    <a class="btn btn-danger" id="editUndoBtn">Indietro</a>

    <?php
    if($module_config['id'] == "notifications_list" && is_array($r)) {
    ?>
        <a class="btn btn-danger" id="deleteNotification" checking_txt="Elimino la notifica...">Elimina notifica</a>
    <?php
    }
    ?>
</div>
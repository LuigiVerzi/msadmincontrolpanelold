<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

$r_producer_config = $MSFrameworkCMS->getCMSData('producer_config');
$user_level = $MSFrameworkUsers->getUserDataFromSession('userlevel');
$is_owner = $MSFrameworkUsers->getUserDataFromSession('is_owner');
$MSSoftwareModules = new \MSSoftware\modules();
?>

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">

            <?php
            $avatar_html = '';
            if($userDetails['gallery_friendly']['propic']) {
                $background_image = $userDetails['gallery_friendly']['propic'][0]['html']['thumb'];
                $avatar_html = '<span><img alt="image" class="img-circle" src="' . $userDetails['gallery_friendly']['propic'][0]['html']['thumb'] .'" onerror="this.onerror=null;this.src=\'' . ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png\';" width="48"  height="48" /></span>';
            } else {
                $logo = json_decode($MSFrameworkCMS->getCMSData('site')["logos"], true)['logo'];
                if ($logo) {
                    $background_image = UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logo;
                    $avatar_html = '<span><img alt="image" src="' . UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logo . '" style="max-height: 50px; width: auto; max-width: 100%;" onerror="this.onerror=null;this.src=\'' . ABSOLUTE_SW_PATH_HTML . 'assets/img/user.png\';" /></span>';
                }
            }
            ?>

            <ul class="nav">
                <li class="nav-header">
                    <?= (!empty($background_image) ? "<style>.skin-1 .nav-header:before {background-image: url('" . $background_image . "');}</style>" : ""); ?>
                    <div class="dropdown profile-element">

                        <?php echo $avatar_html; ?>
                        <span class="block m-t-sm"><?= SW_NAME; ?></span>

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"><span class="text-xs block"><?= $userDetails['nome'] . ' ' . $userDetails['cognome'] ?> <b class="caret"></b></span> </span>
                        </a>

                        <ul class="dropdown-menu animated m-t-xs">
                            <li><a href="<?php echo ABSOLUTE_SW_PATH_HTML ?>modules/manager/utenti/profile/">Profilo</a></li>
                            <li><a href="<?php echo $MSFrameworkCMS->getUrlToSite(); ?>" target="_blank">Visualizza Sito</a></li>
                            <li class="divider"></li>
                            <?php
                            if($_SESSION['userData']['userlevel'] == "0" && $isSaaS) {
                            ?>
                                <li><a href="<?php echo ABSOLUTE_SW_PATH_HTML ?>login/saasSelector.php">Gestisci un altro ambiente</a></li>
                            <?php } ?>

                            <li><a href="<?php echo ABSOLUTE_SW_PATH_HTML ?>login.php?do-logout">Logout</a></li>
                        </ul>

                    </div>
                    <div class="logo-element">
                        MS
                    </div>
                </li>
            </ul>

            <?php
            $framework_menu_view = 'website';
            if($user_level == "0") {

                $framework_menu_view = (isset($_COOKIE['framework_menu_view']) ? $_COOKIE['framework_menu_view'] : "website");

                echo '<div class="fw360_menu_switches"><a data-id="fw360" class="fw360_menu_switch fw360 ' . ($framework_menu_view == 'fw360' ? 'active' : '') . '">FW360</a><a data-id="website" class="fw360_menu_switch website ' . ($framework_menu_view == 'fw360' ? '' : 'active') . '">Sito Web</a></div>';
                echo '<ul data-id="fw360" class="nav side-menu fw360_menu" style="display: ' . ($framework_menu_view == 'fw360' ? 'block' : 'none') . ';">';
                echo $MSSoftwareModules->printSidebarStructure($MSSoftwareModules->getModules(true));
                echo '</ul>';
            }
            ?>

            <ul data-id="website" class="nav side-menu website_menu" style="display: <?= ($framework_menu_view == 'fw360' ? 'none' : 'block'); ?>">
                <?php
                $menu_order = $r_producer_config['modules_order'];
                if($user_level == "0" && $menu_order != '') {
                    $menu_view = (isset($_COOKIE['menu_view_type']) ? $_COOKIE['menu_view_type'] : "0");
                    $users_roles = $MSFrameworkUsers->getUserLevels('', true);

                    $select_html = '<select id="change_menu_view">';

                    $select_html .= '<option value="" selected>Cambia Menù</option>';
                    $select_html .= '<optgroup label="Ruoli Standard">';
                    if ($menu_view !== "0") $select_html .= '<option value="0">Passa a Super Admin</option>';
                    foreach ($users_roles as $levID => $levName) {
                        if ($menu_view == $levID || strstr($levID, 'custom-')) continue;
                        $select_html .= '<option value="' . $levID . '">Passa a ' . $levName . '</option>';
                    }

                    $select_html .= '</optgroup>';

                    $have_custom_select = false;
                    $custom_select_html .= '<optgroup label="Ruoli Personalizzati">';
                    foreach ($users_roles as $levID => $levName) {
                        if ($menu_view == $levID || !strstr($levID, 'custom-')) continue;
                        $custom_select_html .= '<option value="' . $levID . '">Passa a ' . $levName . '</option>';
                        $have_custom_select = true;
                    }
                    $custom_select_html .= '</optgroup>';

                    if ($have_custom_select) {
                        $select_html .= $custom_select_html;
                    }

                    $select_html .= '</select>';

                    echo '<li class="menu_superadmin_message role_' . $menu_view . '">Menù <b>' . ($menu_view === "0" ? 'Super Admin' : $users_roles[$menu_view]) . '</b>' . $select_html . '</li>';
                }

                echo $MSSoftwareModules->printSidebarStructure(null, null, $menu_view);
                ?>
            </ul>

        </div>
    </nav>

<?php
unset($r_producer_config);
?>
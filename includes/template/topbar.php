<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */
?>

<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-expand"></i> </a>
    </div>

    <ul class="nav navbar-top-links navbar-right">
        <?php
        include('topbar/credits.php');

        if($isSaaS) {
            include('topbar/saas.php');
        }
        if(!$isSaaS) {
            include('topbar/notification.php');
        }

        include('topbar/ticket.php');
        ?>


        <?php if(count($active_languages_details) > 1) { ?>
            <li style="vertical-align: middle;">
                <select id="langs-htmlselect">
                    <?php foreach($active_languages_details as $langK => $langV) { ?>
                        <option value="<?php echo $langV['id'] ?>" data-imagesrc="<?php echo FRAMEWORK_COMMON_CDN ?>img/languages/<?php echo $langV['flag_code'] ?>.png" <?php if(USING_LANGUAGE_CODE == $langV['id']) { echo "selected"; } ?>><?php echo $langV['italian_name'] ?></option>
                    <?php } ?>
                </select>
            </li>
        <?php } ?>

        <li class="logo">
            <img src="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/logo.svg" alt="Marketing Studio">
        </li>
        <li class="logo-mobile">
            <img src="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/360.svg" alt="Marketing Studio">
        </li>
    </ul>
</nav>

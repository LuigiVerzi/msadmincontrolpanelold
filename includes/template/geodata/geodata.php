<?php
/**
 * MSAdminControlPanel
 * Date: 2019-03-26
 */
$MSFrameworkGeonames = (new \MSFramework\geonames());
$geodata = json_decode($r['geo_data'], true);
?>

<div class="row">
    <div class="col-sm-4">
        <label>Regione<?= ($module_config['mandatory_geonames'] ? "*" : "") ?></label>
        <select class="form-control regione" id="regione_sel" name="regione_sel" style="<?php echo $hide_selects ?>">
            <option value=""></option>
            <?php
            foreach($MSFrameworkGeonames->getRegionsList('3175395', "geonameid, name") as $v) { //solo ITALIA
                ?>
                <option value="<?php echo $v['geonameid'] ?>" <?php if($geodata['regione'] == $v['geonameid']) { echo "selected"; } ?>><?php echo $v['name'] ?></option>
                <?php
            }
            ?>
        </select>
    </div>

    <div class="col-sm-4">
        <label>Provincia<?= ($module_config['mandatory_geonames'] ? "*" : "") ?></label>
        <select class="form-control provincia" id="provincia_sel" name="provincia_sel" style="<?php echo $hide_selects ?>">
            <option value=""></option>
            <?php
            foreach($MSFrameworkGeonames->getProvinceList($geodata['regione'], "geonameid, name") as $v) {
                ?>
                <option value="<?php echo $v['geonameid'] ?>" <?php if($geodata['provincia'] == $v['geonameid']) { echo "selected"; } ?>><?php echo $v['name'] ?></option>
                <?php
            }
            ?>
        </select>
    </div>

    <div class="col-sm-4">
        <label>Comune<?= ($module_config['mandatory_geonames'] ? "*" : "") ?></label>
        <select class="form-control comune chosen-select" id="comune_sel" name="comune_sel" data-placeholder=" " style="<?php echo $hide_selects ?>;">
            <option value=""></option>
            <?php
            foreach($MSFrameworkGeonames->getComuniList($geodata['provincia'], "geonameid, name") as $v) {
                ?>
                <option value="<?php echo $v['geonameid'] ?>" <?php if($geodata['comune'] == $v['geonameid']) { echo "selected"; } ?>><?php echo $v['name'] ?></option>
                <?php
            }
            ?>
        </select>
    </div>
</div>

<div class="hr-line-dashed"></div>

<div class="row">
    <div class="col-sm-6">
        <label>Indirizzo<?= ($module_config['mandatory_geonames'] ? "*" : "") ?></label>
        <?php
        $addr_disabled = "";
        if($geodata['indirizzo'] == "" && $geodata['comune'] == "") {
            $addr_disabled = "disabled";
        }

        $show_comune_string = "";
        if($geodata['comune'] != "") {
            $show_comune_string = $MSFrameworkGeonames->getDettagliComune($geodata['comune'], "name")['name'] . ", ";
        }
        ?>
        <input type="text" id="indirizzo" class="form-control" value="<?php echo $show_comune_string ?> <?php echo $geodata['indirizzo'] ?>" <?php echo $addr_disabled ?> />
        <input type="hidden" id="indirizzo_hidden" class="form-control" value="<?php echo $geodata['indirizzo'] ?>" />
        <input type="hidden" id="street_number_hidden" class="form-control" value="<?php echo $geodata['street_number'] ?>" />
    </div>

    <div class="col-sm-2">
        <label>CAP<?= ($module_config['mandatory_geonames'] ? "*" : "") ?></label>
        <input type="text" id="cap" class="form-control" value="<?php echo $geodata['cap'] ?>" />
    </div>

    <div class="col-sm-2">
        <label>Latitudine</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Questo dato verrà utilizzato per localizzare l'elemento sulla mappa"></i></span>
        <input type="text" id="indirizzo_lat" class="form-control" value="<?php echo $geodata['lat'] ?>" />
    </div>

    <div class="col-sm-2">
        <label>Longitudine</label> <span class="ms-label-tooltip m-l-sm"><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Questo dato verrà utilizzato per localizzare l'elemento sulla mappa"></i></span>
        <input type="text" id="indirizzo_lng" class="form-control" value="<?php echo $geodata['lng'] ?>" />
    </div>
</div>

<div class="hr-line-dashed"></div>
<div class="row">
    <div class="col-sm-3">
        <label>&nbsp;</label>
        <div class="styled-checkbox form-control">
            <label style="width: 100%;">
                <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                    <input type="checkbox" id="indirizzo_custom" <?php echo (isset($geodata['indirizzo_visibile']) && !empty($geodata['indirizzo_visibile']) ? 'checked' : ''); ?>>
                    <i></i>
                </div>
                <span style="font-weight: normal;">Personalizza visualizzazione</span>
            </label>
        </div>
    </div>
    <div class="col-sm-6" id="showCustomAddress">
        <label>Indirizzo visualizzato</label>
        <input type="text" id="indirizzo_visibile" class="form-control" value="<?php echo $geodata['indirizzo_visibile'] ?>"/>
    </div>
</div>
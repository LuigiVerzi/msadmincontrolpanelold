<?php
/**
 * MSAdminControlPanel
 * Date: 11/07/18
 */

$producer_config = (new \MSFramework\cms())->getCMSData('producer_config');

$uploads_settings = $producer_config['uploadSettings'];
if(isset($uploads_settings['single']) && isset($uploads_settings['single'][$uploader_type]) && $uploads_settings['single'][$uploader_type]['enabled'] == "1") {
    $uploads_settings = $uploads_settings['single'][$uploader_type];
}
?>


<div class="orakUploaderContainer">

    <div class="row orakSettings" style="margin-bottom: 25px; display: <?= ($showSettings ? 'block' : 'none'); ?>;">
        <div class="col-sm-12">
            <button type="button" class="btn btn-default" id="<?= $id ?>_btnUplSettings" data-toggle="modal" data-target="#<?= $id ?>_modalUplSettings"><i class="fa fa-gear"></i></button> <br />

            <div id="<?= $id ?>_modalUplSettings" class="modal fade">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Imposta le preferenze per l'upload</h3>
                        </div>

                        <div class="modal-body">
                            <h4>Cropping (in px) (solo valori numerici)</h4>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Width</label>
                                            <input id="<?= $id ?>_modalUplSettings_crop_width" type="text" class="form-control" value="<?= $uploads_settings['crop_width'] ?>">
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Height</label>
                                            <input id="<?= $id ?>_modalUplSettings_crop_height" type="text" class="form-control" value="<?= $uploads_settings['crop_height'] ?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Width Thumb</label>
                                            <input id="<?= $id ?>_modalUplSettings_crop_width_thumb" type="text" class="form-control" value="<?= $uploads_settings['crop_width_thumb'] ?>">
                                        </div>

                                        <div class="col-sm-6">
                                            <label>Height Thumb</label>
                                            <input id="<?= $id ?>_modalUplSettings_crop_height_thumb" type="text" class="form-control" value="<?= $uploads_settings['crop_height_thumb'] ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Qualità Compressione Immagini</label>
                                            <div id="<?= $id ?>_modalUplSettings_compression_quality" class="orakUploaderCompressionRange" data-current="<?php echo htmlentities(($uploads_settings['compression_quality'] ? $uploads_settings['compression_quality'] : 100)) ?>"></div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-top: 8px;">
                                        <div class="col-sm-12">
                                            <label> &nbsp; </label>
                                            <div class="styled-checkbox form-control">
                                                <label style="width: 100%;">
                                                    <div class="checkbox i-checks pull-right" style="padding-top: 0px;">
                                                        <input type="checkbox" id="<?= $id ?>_modalUplSettings_apply_watermark" <?php if($uploads_settings['apply_watermark'] == "1") { echo "checked"; } ?>>
                                                        <i></i>
                                                    </div>
                                                    <span style="font-weight: normal;">Applica Watermark</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Salva</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    foreach($aryDataAttr as $group => $dataAry) {
        $cur_var = "data_" . $group;
        foreach($dataAry as $dataK => $dataV) {
            $$cur_var .= "data-" . $dataK . "='" . $dataV . "' ";
        }
    }
    ?>
    <div class="row text-center">
        <div class="col-sm-12">
            <div class="<?= $id ?>_container <?= $aryClasses['container'] ?>" data-name="<?= $aryClasses['name'] ?>">
                <textarea style="display: none;" id="prev_upl_<?= $id ?>" class="prev_upl_<?= $id ?> <?= $aryClasses['prevUploaded'] ?>"><?= $preload ?></textarea>
                <input type="hidden" id="<?= $id ?>_realPath" class="<?= $id ?>_realPath <?= $aryClasses['realPath'] ?>" value="<?= $path ?>" />
                <input type="hidden" id="<?= $id ?>_absPath" class="<?= $id ?>_absPath image_absPath" value="<?= $abs_path ?>" />

                <div id="<?= $id ?>" class="<?= $id ?> <?= $aryClasses['element'] ?>" <?= $data_element ?> orakuploader="on"></div>
            </div>
        </div>
    </div>
</div>
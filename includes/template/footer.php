<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */
?>

<div class="modal inmodal fade" id="inputShortcodesModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-code modal-icon"></i>
                <h4 class="modal-title" style="float: none;">Lista Shortcodes</h4>
            </div>
            <div class="modal-body">
                <?php $sI = 0; foreach($MSFrameworkPages->getCommonShortcodes() as $shortcode => $shortcode_values) { $sI++; ?>
                    <?php if($sI > 1) { ?>
                        <div class="hr-line-dashed"></div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-9">
                            <h3 style="margin: 0 0 5px;"><?= $shortcode_values['title']; ?> <small><a title="Clicca per copiare" class="shortCodeCopy"><u><?= $shortcode; ?></u></a></small></h3>
                            <small style="word-break: break-all; display: block;"><?= (!empty($shortcode_values['value']) ? $shortcode_values['value'] : 'N/A'); ?></small>
                            <input type="text" style="display: none;" value="<?= $shortcode; ?>">
                        </div>
                        <div class="col-sm-3">
                            <a href="#" data-shortcode="<?= $shortcode; ?>" class="msInsertBtn btn btn-outline btn-info btn-block">Inserisci</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<?php if(isset($module_config) && isset($module_config['frontend_shortcode'])) { ?>
    <div class="modal inmodal fade" id="moduleParserTag" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="fa fa-code modal-icon"></i>
                    <h4 class="modal-title" style="float: none;">Integrazione <?= array_values($module_config['frontend_shortcode'])[0]['name']; ?></h4>
                    <small>Inserisci il seguente codice sul tuo sito per includere l'elemento <i><?= array_values($module_config['frontend_shortcode'])[0]['name']; ?></i>.</small>
                </div>
                <div class="modal-body">
                    <?php
                    $html_to_return = '';
                    foreach($module_config['frontend_shortcode'] as $sh) {
                        $html_to_return .= '<table class="table table-bordered white-bg">';
                        $html_to_return .= (new \MSFramework\Frontend\Tags\tags())->generateSingleTagDocumentation($sh);
                        $html_to_return .= '</table>';

                        $html_to_return .= '<h2 class="title-with-button">Esempio</h2>';

                        $html_to_return .= '<textarea class="frontendShortcodeMirror" disabled>';
                        $html_to_return .= (new \MSFramework\Frontend\Tags\tags())->generateSingleTagExample($sh);
                        $html_to_return .= '</textarea>';
                    }
                    echo $html_to_return;
                    ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Chiudi</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<div class="footer">
    <div class="pull-right">
        v. <?php echo SW_VERSION ?>
    </div>
    <div>
        Copyright <?php echo date('Y'); ?> &copy; Tutti i diritti riservati
    </div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/js/jquery-2.1.1.js?v=<?= SW_VERSION; ?>"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/jquery-ui/jquery-ui.min.js?v=<?= SW_VERSION; ?>"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/js/bootstrap.min.js?v=<?= SW_VERSION; ?>"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/metisMenu/jquery.metisMenu.js?v=<?= SW_VERSION; ?>"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/slimscroll/jquery.slimscroll.min.js?v=<?= SW_VERSION; ?>"></script>

<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/multiselect/jquery.multiselect.js?v=<?= SW_VERSION; ?>"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/js/inspinia.js?v=<?= SW_VERSION; ?>"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/pace/pace.min.js?v=<?= SW_VERSION; ?>"></script>

<!-- GITTER -->
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/gritter/jquery.gritter.min.js?v=<?= SW_VERSION; ?>"></script>

<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/fullcalendar/moment.min.js?v=<?= SW_VERSION; ?>"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/chartJs/Chart.min.js?v=<?= SW_VERSION; ?>"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/peity/jquery.peity.min.js?v=<?= SW_VERSION; ?>"></script>

<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/iCheck/icheck.min.js?v=<?= SW_VERSION; ?>"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/ddslick/jquery.ddslick.min.js?v=<?= SW_VERSION; ?>"></script>

<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/js/main.js?v=<?= SW_VERSION; ?>"></script>

<?php if($_SESSION['userData']['userlevel'] == "0") { ?>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/js/moduleFieldsToggler.js?v=<?= SW_VERSION; ?>"></script>
<?php } ?>

<!-- Toastr -->
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/toastr/toastr.min.js?v=<?= SW_VERSION; ?>"></script>
<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/bootbox/bootbox.js?v=<?= SW_VERSION; ?>"></script>

<script src="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/js/pushNotification.js?v=<?= SW_VERSION; ?>"></script>

<?php
$has_worldDataUtils = false;
foreach($module_config['additional_js'] as $js_path) {
    $ary_path_js = explode("/", $js_path);
    $cur_filename = end($ary_path_js);

    if($cur_filename == "worldDataUtils.js") {
        $has_worldDataUtils = true;
    }
?>
    <script src="<?php echo $js_path ?><?= (strpos($js_path, '?') !== false ? '&' : '?');?>fwv=<?= SW_VERSION; ?>"></script>
<?php
}
?>

<?php
foreach($MSFrameworkModules->getModulesFiles('admin_footer') as $type => $files) {
    if($type == 'css') {
        foreach($files as $url) echo '<link href="' . $url . '" rel="stylesheet">';
    }
    if($type == 'js') {
        foreach($files as $url) echo '<script src="' . $url . '"></script>';
    }
    if($type == 'html') {
        foreach($files as $url) include($url);
    }
}
?>

<?php if($has_worldDataUtils) { ?>
<script src="//maps.googleapis.com/maps/api/js?key=<?php echo GPLACES_API_KEY ?>&libraries=places&callback=initGooglePlaces" async="false"></script>
<?php } ?>

<script>
    <?= $MSFrameworkCMS->getCMSData('producer_config')['additional_js_acp'] ?>
</script>
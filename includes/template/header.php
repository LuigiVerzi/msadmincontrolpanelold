<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */

$active_languages_details = $MSFrameworki18n->getActiveLanguagesDetails();
$primary_lang_details = $MSFrameworki18n->getLanguagesDetails($MSFrameworki18n->getPrimaryLangId())[$MSFrameworki18n->getPrimaryLangId()];

if($_SESSION['userData']['userlevel'] == "" && !$module_config['guest_mode']) {
    $append_logout_par = "";
    if(isset($_SESSION['userData']['userlevel'])) {
        $append_logout_par = "?not-logged";
    }

    header("location: " . ABSOLUTE_SW_PATH_HTML . "login.php" . $append_logout_par);
    die();
} else {
    if($isSaaS && $_SESSION['SaaS']['saas__environments']['id'] == "") {
        session_destroy();
        header("location: " . ABSOLUTE_SW_PATH_HTML . "login.php?empty-saas-info");
        die();
    }

    if($_SESSION['userData']['userlevel'] != "0") {
        if($module_config['id'] != "userprofile" && !in_array($module_config['id'], (new \MSSoftware\modules())->getEnabledModuleIDs())) {

            if(!$module_config['guest_mode']) {
                $MSFrameworkUsers->endUserSession();
                header("location: " . ABSOLUTE_SW_PATH_HTML . "login.php?not-logged");
                die();
            }

        }
    }
}

$r_cms_site = $MSFrameworkCMS->getCMSData('site');
$acp_settings = json_decode($r_cms_site['acp'], true);


/* INCREMENTA LA PRIORITA' DEL MODULO */
$most_used_modules = $MSFrameworkCMS->getCMSData('modules_most_used_' . $_SESSION['userData']['user_id']);
arsort($most_used_modules);
if($module_config['id'] !== 'dashboard') {
    if (!$most_used_modules) {
        $most_used_modules = array();
    }
    $most_used_modules[$module_config['id']] = ($most_used_modules[$module_config['id']] ? (int)$most_used_modules[$module_config['id']] : 0) + 1;
    $MSFrameworkCMS->setCMSData('modules_most_used_' . $_SESSION['userData']['user_id'], $most_used_modules);
}
?>

<!DOCTYPE html>
<html class="<?= (isset($_GET['fromFastEditor']) ? 'fromFastEditor' . ($_GET['fromFastPicker'] ? ' fromFastPicker' : '') : ''); ?>">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo SW_NAME ?> | <?php echo $module_config['name'] ?></title>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/img/favicon.png" />

    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/bootstrap.min.css?v=<?= SW_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/fontawesome5/css/all.css?v=<?= SW_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/fontawesome5/css/v4-shims.css?v=<?= SW_VERSION; ?>" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/toastr/toastr.min.css?v=<?= SW_VERSION; ?>" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/gritter/jquery.gritter.css?v=<?= SW_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/multiselect/jquery.multiselect.css?v=<?= SW_VERSION; ?>" rel="stylesheet">

    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/plugins/iCheck/custom.css?v=<?= SW_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/animate.css?v=<?= SW_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>vendor/css/style.css?v=<?= SW_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/custom.css?v=<?= SW_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo ABSOLUTE_SW_PATH_HTML ?>assets/css/colors-aquamarine.css?v=<?= SW_VERSION; ?>" rel="stylesheet">

    <input type="hidden" id='baseCurDomain' value="http://<?php echo $_SERVER['HTTP_HOST'] ?>/" />
    <input type="hidden" id='baseElementPathAdmin' value="<?php echo ABSOLUTE_SW_PATH_HTML ?>" />
    <input type="hidden" id='baseElementPathTmpFolder' value="<?php echo UPLOAD_TMP_FOR_DOMAIN_HTML ?>" />
    <input type="hidden" id='baseElementPathSW' value="<?php echo $MSFrameworkCMS->getURLToSite(); ?>" />
    <input type="hidden" id='loadedModuleID' value="<?php echo $module_id; ?>" />
    <input type="hidden" id='loadedModuleFieldsStatus' value="<?php echo ($module_config['module_fields_status'] ? htmlentities(json_encode($module_config['module_fields_status'])) : '{}'); ?>" />
    <input type="hidden" id='loadedModuleMultilangContent' value="<?php echo (isset($module_config['multilang_content']) && $module_config['multilang_content'] ? '1' : '0'); ?>" />
    <input type="hidden" id='userLevel' value="<?php echo $MSFrameworkUsers->getUserDataFromSession('userlevel'); ?>" />
    <input type="hidden" id='usingPrimaryLang' value="<?php echo ($using_primary_language || (isset($module_config['multilang_content']) && $module_config['multilang_content']) ? '1' : '0') ?>" />
    <input type="hidden" id='primaryLangFullName' value="<?php echo $primary_lang_details['native_name']; ?>" />
    <input type="hidden" id='primaryLangCode' value="<?php echo $primary_lang_details['long_code']; ?>" />
    <input type="hidden" id='currentLangCode' value="<?php echo $MSFrameworki18n->getCurrentLanguageDetails()['long_code']; ?>" />
    <input type="hidden" id='currentLangUrlCode' value="<?php echo (!$using_primary_language ? $MSFrameworki18n->getCurrentLanguageDetails()['url_code'] : ''); ?>" />
    <input type="hidden" id='currentCurrencyName' value="<?= $MSFrameworkCurrencies->getCurrentCurrencyDetails()['name']; ?>" />
    <input type="hidden" id='currentCurrencySymbol' value="<?= $MSFrameworkCurrencies->getCurrentCurrencyDetails()['symbol']; ?>" />
    <input type="hidden" id='sessionMaxLifetime' value="<?= ini_get('session.gc_maxlifetime'); ?>" />
    <input type="hidden" id='sessionStartTime' value="<?= $_SESSION['start_time']; ?>" />
    <input type="hidden" id='MSCommonShortcodes' value="<?= htmlentities(json_encode($MSFrameworkPages->getCommonShortcodes())); ?>" />

    <?php if(isset($module_config['page_relation'])) { ?>
    <input type="hidden" id='loadedModuleRelatedPage' value="<?php echo (isset($module_config['page_relation']) ? $module_config['page_relation'] : ''); ?>" />
    <input type="hidden" id='loadedModuleRelatedPageID' value="<?php echo ($_GET['id'] > 0 ? array_values($MSFrameworkPages->getPageIDsByRelation($module_config['page_relation'], false, array($_GET['id'])))[0] : ''); ?>" />
    <?php } ?>

    <input type="hidden" id='exitAfterSave' value="<?= $acp_settings['exit_after_save'] ?>" />

    <?php foreach($module_config['additional_css'] as $css_path) { ?>
        <link href="<?php echo $css_path ?>?v=<?= SW_VERSION; ?>" rel="stylesheet">
    <?php } ?>

    <?php
    foreach($MSFrameworkModules->getModulesFiles('admin_header') as $type => $files) {
        if($type == 'css') {
            foreach($files as $url) echo '<link href="' . $url . '?v=' . SW_VERSION . '" rel="stylesheet">';
        }
        if($type == 'js') {
            foreach($files as $url) echo '<script src="' . $url . '?v=' . SW_VERSION . '"></script>';
        }
        if($type == 'html') {
            foreach($files as $url) include($url);
        }
    }
    ?>

    <style>
        <?= $MSFrameworkCMS->getCMSData('producer_config')['additional_css_acp'] ?>
    </style>
</head>
<?php
/**
 * MSAdminControlPanel
 * Date: 17/02/18
 */
?>

<h2><?php echo $module_config['name']; ?></h2>
<ol class="breadcrumb">
    <?php
    foreach($module_config['breadcrumbs'] as $breadcrumb) {
        ?>
        <li>
            <?php echo $breadcrumb ?>
        </li>
        <?php
    }
    ?>
</ol>
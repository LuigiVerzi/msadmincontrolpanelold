<?php
/**
 * MSAdminControlPanel
 * Date: 08/09/18
 */

namespace MSFramework\Updater;

/**
 * Questa classe si occupa di verificare la necessità di avviare eventuali procedure di aggiornamento sui DB.
 *
 * Class dbUpdater
 * @package MSFramework
 */
class dbUpdater {
    private $isSuperAdmin;

    public function __construct() {
        global $firephp, $MSFrameworkDatabase, $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSSaaSEnvironments = new \MSFramework\SaaS\environments();

        $this->isSuperAdmin = ((new \MSFramework\users())->getUserDataFromSession('userlevel') == "0") ? true : false;
        $this->simulateProduction = false; //molto pericoloso: fa partire i controlli su tutti i domini anche se ci si trova in sviluppo/staging
    }

    /**
     * Restituisce un array contenente tutte le informazioni relative all'aggiornamento corrente, divise per database e versione.
     * L'array restituito tiene conto di eventuali limitazioni imposte nel file config.json delle singole procedure.
     *
     * @return array
     */
    public function getEnvolvedProcedures() {
        global $firephp;
        $array = array();
        $array_error = array();
        $array_config_info = array();

        if($this->isSuperAdmin && ($this->simulateProduction || (!strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/") && !$this->MSFrameworkCMS->isStaging()))) {
            $r_customers = $this->MSFrameworkDatabase->getAssoc("SELECT customer_database FROM `" . FRAMEWORK_DB_NAME . "`.`customers`");
            if(!is_array($r_customers)) {
                $r_customers = array();
            }

            $r_saas = array();
            foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments`") as $rsaas) {
                $MSFrameworkEnvironments = new \MSFramework\SaaS\environments($rsaas['id']);
                $r_saas[]['customer_database'] = $MSFrameworkEnvironments->getDBName();
            }

            foreach(array_merge($r_saas, $r_customers) as $customer) {
                $cur_db_version = $this->getDBVersion($customer['customer_database']);
                if($cur_db_version == "" || version_compare($cur_db_version, '0.0.1', '<')) {
                    $array_error[] = $customer['customer_database'];
                    continue;
                }

                foreach (scandir(PATH_TO_FRAMEWORK . "frameworkUpdates/updates") as $folder) {
                    if (is_dir(PATH_TO_FRAMEWORK . "frameworkUpdates/updates/" . $folder) && (version_compare($folder, $cur_db_version, '>') && version_compare($folder, $this->getSWVersion(), '<='))) {
                        foreach((new \DirectoryIterator(PATH_TO_FRAMEWORK . "frameworkUpdates/updates/" . $folder . "/")) as $procFolder) {
                            if(!$procFolder->isDot() && $this->checkConfig($folder, $procFolder->getBasename(), $customer['customer_database'])) {
                                $array[$customer['customer_database']][$folder][] = $procFolder->getBasename();

                                $config = $this->getConfigFile($folder, $procFolder->getBasename());
                                if(is_array($config)) {
                                    if(array_key_exists('info', $config)) {
                                        $array_config_info[$folder][$procFolder->getBasename()] = $config['info'];
                                    }
                                }
                            }
                        }

                        if(is_array($array[$customer['customer_database']][$folder])) {
                            sort($array[$customer['customer_database']][$folder]);
                        }
                    }
                }
            }
        } else {
            if($this->getDBVersion() == "" || version_compare($this->getDBVersion(), '0.0.1', '<')) {
                $array_error[] = $_SESSION['db'];
            } else {
                foreach (scandir(PATH_TO_FRAMEWORK . "frameworkUpdates/updates") as $folder) {
                    if (is_dir(PATH_TO_FRAMEWORK . "frameworkUpdates/updates/" . $folder) && (version_compare($folder, $this->getDBVersion(), '>') && version_compare($folder, $this->getSWVersion(), '<='))) {
                        foreach((new \DirectoryIterator(PATH_TO_FRAMEWORK . "frameworkUpdates/updates/" . $folder . "/")) as $procFolder) {
                            if(!$procFolder->isDot() && $this->checkConfig($folder, $procFolder->getBasename())) {
                                $array[$_SESSION['db']][$folder][] = $procFolder->getBasename();

                                $config = $this->getConfigFile($folder, $procFolder->getBasename());
                                if(is_array($config)) {
                                    if(array_key_exists('info', $config)) {
                                        $array_config_info[$folder][$procFolder->getBasename()] = $config['info'];
                                    }
                                }
                            }
                        }

                        if(is_array($array[$_SESSION['db']][$folder])) {
                            sort($array[$_SESSION['db']][$folder]);
                        }
                    }
                }
            }
        }

        return array("ready" => $array, "error" => $array_error, "info" => $array_config_info);
    }

    /**
     * Ottiene il file di configurazione di una determinata procedura
     *
     * @param $version La versione della procedura
     * @param $folder Il nome della cartella della procedura
     *
     * @return mixed|string
     */
    private function getConfigFile($version, $folder) {
        $config_file = PATH_TO_FRAMEWORK . "frameworkUpdates/updates/" . $version . "/" . $folder . "/config.json";
        if(file_exists($config_file)) {
            $config = json_decode(file_get_contents($config_file), true);
            if(json_last_error() != 0) {
                return "json_error";
            }

            return $config;
        } else {
            return "not_exists";
        }
    }

    /**
     * Verifica la presenza del file di configurazione (config.json) per una determinata procedura e determina se tale procedura sia eseguibile sul DB indicato
     *
     * @param $version La versione alla quale aggiornare
     * @param $folder Il nome della cartella della procedura
     * @param $db_name Il nome del DB
     *
     * @return bool
     */
    public function checkConfig($version, $folder, $dbName = "") {
        global $firephp;
        $dbName = $this->dbProtection($dbName);

        $config = $this->getConfigFile($version, $folder);
        if($config == "json_error") {
            return false;
        }

        if($dbName == DUMMY_DB_NAME && (!is_array($config['exclude_dbs']) || (is_array($config['exclude_dbs']) && !in_array(DUMMY_DB_NAME, $config['exclude_dbs'])))) { //il DB dummy viene sempre aggiornato a meno che non sia esplicitamente escluso (utile, ad esempio, quando si creano delle procedure per importare dei dati)
            return true;
        }

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT value FROM `" . $dbName . "`.`cms` WHERE type = 'producer_config'", array(), true);
        $r_producer_config = json_decode($r['value'], true);

        if(is_array($config)) {
            if(is_array($config['limit_to_dbs']) && !in_array($dbName, $config['limit_to_dbs'])) {
                return false;
            }

            if(is_array($config['exclude_dbs']) && in_array($dbName, $config['exclude_dbs'])) {
                return false;
            }

            if(is_array($config['limit_to_functions']) && count(array_intersect($config['limit_to_functions'], json_decode($r_producer_config['extra_functions'], true))) == 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Ottiene la versione del database.
     *
     * @param string $dbName Se l'utente loggato è un SuperAdmin, può impostare manualmente il parametro $dbName per selezionare un DB diverso da quello corrente (solo in produzione).
     *
     * @return string
     */
    public function getDBVersion($dbName = "") {
        $dbName = $this->dbProtection($dbName);

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT valore FROM `" . $dbName . "`.`_db_info` WHERE nome = 'versione'", array(), true);

        return $r['valore'];
    }

    /**
     * Imposta la versione di del database.
     *
     * @param $dbName Se l'utente loggato è un SuperAdmin, può impostare manualmente il parametro $dbName per selezionare un DB diverso da quello corrente (solo in produzione).
     * @param $version La versione da impostare
     *
     * @return bool
     */
    public function setDBVersion($version, $dbName) {
        $dbName = $this->dbProtection($dbName);

        $this->MSFrameworkDatabase->pushToDB("UPDATE `" . $dbName . "`.`_db_info` SET valore = :valore WHERE nome = 'versione'", array(':valore' => $version));
        return true;
    }

    /**
     * Ottiene la versione del software.
     *
     * @return string
     */
    private function getSWVersion() {
        return SW_VERSION;
    }

    /**
     * Si assicura di consentire l'accesso al DB specificato solo se l'utente ne ha il diritto (è un SuperAdmin, il nome del DB non è vuoto, l'utente non sta operando in sviuppo, l'utente non sta operando in staging).
     *
     * @param $dbName Il nome del DB da verificare.
     *
     * @return mixed
     */
    private function dbProtection($dbName) {
        if($this->isSuperAdmin && $this->simulateProduction) {
            return $dbName;
        }

        if(!$this->isSuperAdmin || $dbName == "" || strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/") || $this->MSFrameworkCMS->isStaging()) {
            return $_SESSION['db'];
        } else {
            return $dbName;
        }
    }

    /**
     * Crea una nuova connessione al DB configurandola con i parametri richiesti per una corretta esecuzione dell'updater
     *
     * @param $dbName Il nome del DB al quale connettersi
     *
     * @return \MSFramework\database
     */
    public function initNewDBConnection($dbName) {
        $MSFrameworkDatabase = new \MSFramework\database(FRAMEWORK_DB_HOST, $dbName, FRAMEWORK_DB_USER, FRAMEWORK_DB_PASS, "mysql", "utf8", array('disable_cache' => true));
        $MSFrameworkDatabase->detailed_error_info = true;
        $this->currentDB = $dbName;
        $this->isDevelopDB = $MSFrameworkDatabase->isDevelopDB;

        return $MSFrameworkDatabase;
    }

    /**
     * Verifica la presenza di errori all'interno della query eseguita e, nel caso, salva i dettagli all'interno di un file di log.
     *
     * @param $status Il risultato della query eseguita
     *
     * @return bool
     */
    public function checkQueryError($status) {
        if(array_key_exists('error_info', $status)) {
            $this->writeLogs('error');
            if(!$this->isDevelopDB && !$this->MSFrameworkCMS->isStaging()) { //solo se non mi trovo nel DB di sviluppo o di staging, segnalo l'errore nella query come un errore importante. Altrimenti lo gestisco come un semplice warning
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Ottiene il percorso in cui vengono salvati i log per la versione corrente
     * @return string
     */
    public function getLogPath() {
        return PATH_TO_LOGS . 'db_updater/' . $this->getSWVersion() . "/";
    }

    /**
     * Scrive i log della procedura all'interno del file
     *
     * @param $type Il tipo di log da creare (error/info)
     * @param array $array_rows Se il tipo di log è "info", all'interno di questo array è possibile passare le righe da scrivere nel file (una per chiave)
     *
     * @return bool
     */
    public function writeLogs($type, $array_rows = array()) {
        $log_path = $this->getLogPath();
        mkdir($log_path, 0777, true);

        if($type == "error") {
            if($this->isDevelopDB || $this->MSFrameworkCMS->isStaging()) {
                $myfile = fopen($log_path . "sqlWarnings.txt", "a+");
            } else {
                $myfile = fopen($log_path . "sqlErrors.txt", "a+");
            }

            fwrite($myfile, PHP_EOL . PHP_EOL . '--- checkQueryError ha scritto queste informazioni il ' . date("d/m/Y") . ' alle ' . date("H:i:s") . ' --- ' . PHP_EOL);

            $error_data = array();
            foreach(debug_backtrace() as $btK => $bt) {
                if(strstr($bt['file'], "frameworkUpdates/updates")) { //prelevo solo i dati del file dell'updater che ha restituito l'errore, e non l'intero backtrace
                    $error_data = $bt;
                    unset($error_data['class']);
                    unset($error_data['function']);
                    unset($error_data['object']);
                    unset($error_data['type']);
                    $error_data['errors'] = $bt['args'];
                    unset($error_data['args']);
                    $error_data['database'] = $this->currentDB;

                    break;
                }
            }

            fwrite($myfile, print_r($error_data, true));
        } else if($type == "info") {
            $myfile = fopen($log_path . "information.txt", "a+");
            fwrite($myfile, PHP_EOL . PHP_EOL . '--- writeLogs (info) ha scritto queste informazioni il ' . date("d/m/Y") . ' alle ' . date("H:i:s") . ' --- ' . PHP_EOL);
            foreach($array_rows as $row) {
                fwrite($myfile, $row . PHP_EOL);
            }
        }

        fwrite($myfile, PHP_EOL);
        fclose($myfile);

        return true;
    }
}
?>

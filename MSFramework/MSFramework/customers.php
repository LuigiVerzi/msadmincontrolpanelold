<?php
/**
 * MSFramework
 * Date: 17/02/18
 */

namespace MSFramework;

class customers {
    public function __construct($force_db = false) {
        global $MSFrameworkDatabase;

        $this->forcingDB = $force_db; //todo: se viene forzato un DB diverso da quello in uso (es: per registrare un cliente del SaaS su MarketingStudio) uso questa variabile per evitare di eseguire azioni che opererebbero sul db attuale (o userebbero dati del dominio corrente, come ad esempio i path degli uploads) e non su quello forzato. Sarebbe utile trovare una soluzione per gestire questa casistica in maniera semplice e globale
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        if($force_db !== false) {
            $this->MSFrameworkDatabase = new \MSFramework\database(FRAMEWORK_DB_HOST, $force_db, FRAMEWORK_DB_USER, FRAMEWORK_DB_PASS, "mysql", "utf8");
        }
    }

    /**
     * Restituisce le possibili provenienze di un cliente
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array
     */
    public function getCustomerSource($key = "") {
        $levels = array(
            "1" => "Telefono",
            "2" => "Email",
            "3" => "Sito",
            "4" => "Facebook",
            "5" => "Visita Negozio",
            "6" => "Altro",
            "7" => "Newsletter"
        );

        if($_SESSION['db'] == MARKETINGSTUDIO_DB_NAME) {
            $levels["8"] = "SaaS";
        }

        if($key != "") {
            return $levels[$key];
        } else {
            return $levels;
        }
    }

    /**
     * Controlla se le credenziali immesse nel form di login sono valide. In tal caso, attiva la sessione dell'utente
     *
     * @param $user string Il nome utente
     * @param $pass string La password
     * @param $check_mail_auth boolean Indica se controllare solo gli utenti convalidati
     *
     * @return bool
     */
    public function checkCredentials($user, $pass, $check_mail_auth = false) {
        if($user == "" || $pass == "") {
            return false;
        }
        $check_mail_sql = "";
        if($check_mail_auth) {
            $check_mail_sql = "AND mail_auth = ''";
        }

        $can_login = false;
        //se non si sta loggando un superadmin, verifico se si sta loggando un utente normale...
        $r = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM customers WHERE email = :email AND active = 1 $check_mail_sql", array(":email" => $user), true);
        if($r['password'] != "") {
            if (password_verify($pass, $r['password'])) {
                $can_login = true;
            }
        }

        if($can_login) {
            $_SESSION['customerData']['username'] = $user;
            $_SESSION['customerData']['email'] = $r['email'];
            $_SESSION['customerData']['user_id'] = $r['id'];
            $_SESSION['customerData']['userlevel'] = $r['ruolo'];
            $_SESSION['customerData']['nome'] = $r['nome'];
            $_SESSION['customerData']['cognome'] = $r['cognome'];

            Global $MSHooks;
            $MSHooks->action('user', 'login')->run(array('user_id' => $r['id'], 'user_email' => $user));

            (new \MSFramework\tracking())->checkForUserCookie($r['id']);

            return true;
        } else {
            return false;
        }

    }

    /**
     * Distrugge la sessione dell'utente e lo reindirizza alla pagina di login
     *
     */
    public function endUserSession() {
        unset($_SESSION['customerData']);
        header("location: " . (new \MSFramework\cms())->getURLToSite());
    }

    /**
     * Recupera i dati dell'utente salvati in $_SESSION.
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return mixed
     */
    public function getUserDataFromSession($key = "") {

        if($key != "") {
            return $_SESSION['customerData'][$key];
        } else {
            return $_SESSION['customerData'];
        }
    }

    /**
     * Recupera i dati del cliente dal database
     *
     * @param string $id L'ID del cliente.
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     * @param bool $limit_to_owner Se impostato su true, recupera solo i clienti visualizzabili da un determinato proprietario
     *
     * @return mixed
     */
    public function getCustomerDataFromDB($id, $fields = "*", $limit_to_owner = false) {
        $append_where_str = "";
        $append_where_ary = array();
        if($limit_to_owner) {
            if(!$_SESSION['userData']['is_owner']) {
                $append_where_str .= " AND (owner_id = '' OR owner_id = :owner)";
                $append_where_ary[":owner"] = (new \MSFramework\users())->getUserDataFromSession('user_id');
            }
        }

        $customer_data = $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . " FROM customers WHERE id = :id " . $append_where_str, array_merge($append_where_ary, array(":id" => $id)), true);
        if($customer_data) $customer_data['html_preview'] = $this->getCustomerPreviewHTML($customer_data);

        return $customer_data;
    }

    /**
     * Recupera i dati del cliente dal database tramite email
     *
     * @param string $email L'ID del cliente.
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getCustomerDataByEmail($email, $fields = "*") {
        return $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . " FROM customers WHERE email = :email", array(":email" => $email), true);
    }

    /**
     * Restituisce un array associativo (ID Cliente => Dati Cliente) con i dati relativi al cliente.
     *
     * @param $id L'ID del cliente (stringa) o dei clienti (array) richiesti (se vuoto, vengono recuperati i dati di tutti i clienti)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCustomerDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM customers WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Attiva l'account dell'utente
     *
     * @param $auth La stringa auth code dell'utente da attivare
     *
     * @return bool
     */
    public function activateAccount($auth) {
        $user_info = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM customers WHERE mail_auth = :auth", array(":auth" => $auth), true);

        if($user_info) {
            $this->MSFrameworkDatabase->pushToDB("UPDATE customers SET mail_auth = '' WHERE mail_auth = :auth", array(":auth" => $auth));

            // Se l'account non è ancora attivo notifico il cliente
            if($user_info['active'] == 0) {
                (new \MSFramework\emails())->sendMail('customer-awaiting-activation', array(
                    'nome' => (!empty($user_info['nome']) ? $user_info['nome'] . ' ' . $user_info['cognome'] : $user_info['email']),
                    'email' => $user_info['email']
                ));
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Controlla se l'indirizzo email è presente nel DB
     *
     * @param $email string L'indirizzo da controllare
     * @param $only_active bool Controlla solo gli account di persone abilitati al login
     *
     * @return bool
     */
    public function checkIfMailExists($email, $only_active = true) {
        $active_sql = "";
        if($only_active) $active_sql = "AND active = 1";

        if($this->MSFrameworkDatabase->getCount("SELECT email FROM customers WHERE email = :email $active_sql", array(":email" => $email)) > 0) {
            return true;
        }

        return false;
    }

    /**
     * Genera (e salva nel DB) il token necessario per il reset della password dell'utente.
     *
     * @param $email L'email per la quale creare il token
     */
    public function generatePassResetToken($email) {
        $token = sha1($email . time() . rand(0, 100));
        return $this->MSFrameworkDatabase->pushToDB("UPDATE customers SET passreset_token = :token WHERE email = :mail", array(":token" => $token, ":mail" => $email));
    }

    /**
     * Verifica se la password è sufficientemente forte. Restituisce una stringa di errore se il test fallisce, o una stringa vuota in caso di test positivo
     *
     * @param $pwd La password da controllare
     *
     * @return string
     */
    public function checkPasswordStrength($pwd) {
        $errors = "";

        if (strlen($pwd) < 6) {
            $errors .= "è troppo corta, ";
        }

        if (!preg_match("#[0-9]+#", $pwd)) {
            $errors .= "deve contenere almeno un numero, ";
        }

        if (!preg_match("#[a-zA-Z]+#", $pwd)) {
            $errors .= "deve contenere almeno una lettera, ";
        }

        if($errors != "") {
            $errors = "La password " . substr($errors, 0, -2) . ".";
        }

        return $errors;
    }

    /**
     * Crea un nuovo account nel DB
     *
     * @param $nome string Il nome dell'utente
     * @param $cognome string Il cognome dell'utente
     * @param $email string La mail dell'utente
     * @param $password string La password dell'utente (Lasciare vuoto per disabilitare il login)
     * @param $cellulare string Il cellulare dell'utente
     * @param $provenienza string La provenienza del cliente
     * @param $more array Colonne secondarie da aggiungere
     *
     * @return bool
     */
    public function registerUser($nome, $cognome, $email, $password, $cellulare, $provenienza = '', $more = array()) {
        $email = str_replace(' ', '', $email);

        if($this->checkIfMailExists($email, false)) {
            $user_info = $this->getCustomerDataByEmail($email);
            $user_id = $user_info['id'];

            // Aggiorna i dati del cliente se ci sono info nuove
            $data_to_update = array();

            $more['nome'] = $nome;
            $more['cognome'] = $cognome;
            $more['telefono_cellulare'] = $cellulare;

            if($password && !empty($password)) {
                $more['password'] = password_hash($password, PASSWORD_DEFAULT);
                $more['active'] = 1;
                $more['mail_auth'] = sha1($email . time());
            }

            foreach($more as $key => $value) {
                if(in_array($key, array('dati_fatturazione'))) {

                    $subOld = (json_decode($user_info[$key]) ? json_decode($user_info[$key], true) : array());
                    foreach($value as $subK => $subV) {
                        if(!isset($subOld[$subK]) || empty($subOld[$subK])) {
                            $subOld[$subK] = $subV;
                        }
                    }

                    $data_to_update[$key] = json_encode($subOld);

                } else {
                    $data_to_update[$key] = $value;
                }
            }

            // Se ci sono nuove info aggiorno il profilo del cliente
            if($data_to_update) {
                $stringForDB = $this->MSFrameworkDatabase->createStringForDB($data_to_update, 'update');
                $this->MSFrameworkDatabase->pushToDB("UPDATE customers SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $user_id), $stringForDB[0]));
            }

            if(!$this->forcingDB) {
                (new \MSFramework\tracking())->checkForUserCookie($user_id);
            }

            return $user_info['id'];
        }

        $array_to_save = array(
            "nome" => $nome,
            "cognome" => $cognome,
            "email" => $email,
            "telefono_cellulare" => $cellulare,
            "provenienza" => $provenienza,
            "mail_auth" => sha1($email . time()),
        );

        if($password && !empty($password)) {
            $array_to_save['active'] = 1;
            $array_to_save['password'] = password_hash($password, PASSWORD_DEFAULT);
        }

        foreach($more as $key => $val) {
            $array_to_save[$key] = $val;
        }

        if(isset($array_to_save['dati_fatturazione'])) {
            $array_to_save['dati_fatturazione'] = (is_array($array_to_save['dati_fatturazione']) ? json_encode($array_to_save['dati_fatturazione']) : $array_to_save['dati_fatturazione']);
        }

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, (isset($user_id) ? 'update' : 'insert'));

        if(isset($user_id)) {
            $result = $this->MSFrameworkDatabase->pushToDB("UPDATE customers SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $user_id), $stringForDB[0]));
        } else {
            $result = $this->MSFrameworkDatabase->pushToDB("INSERT INTO customers ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            $user_id = $this->MSFrameworkDatabase->lastInsertId();
        }

        if($password && !empty($password)) {
            Global $MSHooks;
            $MSHooks->action('user', 'registration')->run(array('user_id' => $user_id, 'user_email' => $email));
        }

        if($result) {
            if(!$this->forcingDB) {
                global $MSFrameworkCMS;

                $MSFrameworkEmails = new \MSFramework\emails();

                $site_settings = $MSFrameworkCMS->getCMSData('site');
                $notify_customer = ($site_settings['customers'] && json_decode($site_settings['customers']) ? json_decode($site_settings['customers'], true) : array());

                if($notify_customer['notify_via_mail'] && $notify_customer['notify_via_mail'] == "1") {
                    $MSFrameworkEmails->sendMail('admin-new-customer-registration', array(
                        'nome' => (!empty($array_to_save['nome']) ? $array_to_save['nome'] . ' ' . $array_to_save['cognome'] : $array_to_save['email']),
                        'email' => $array_to_save['email'],
                        'id' => $user_id
                    ));
                }

                // Se l'account non è ancora attivo notifico gli admin
                if(isset($more['active']) && $more['active'] == 0 && !empty($password)) {
                    $MSFrameworkEmails->sendMail('admin-customer-awaiting-activation', array(
                        'nome' => (!empty($array_to_save['nome']) ? $array_to_save['nome'] . ' ' . $array_to_save['cognome'] : $array_to_save['email']),
                        'email' => $array_to_save['email'],
                        'id' => $user_id
                    ));
                }

                (new \MSFramework\tracking())->checkForUserCookie($user_id);

                /* CERCA DI OTTENERE IL GRAVATAR DEL CLIENTE E NEL CASO IMPOSTARLO */
                $this->setGravatarIfExist($user_id);
            }

            return $user_id;
        }

        return false;

    }

    /**
     * Controlla l'esistenza del gravatar e nel caso lo imposta come avatar dell'utente
     *
     * @param $user_ref mixed L'indirizzo email o l'id dell'utente
     *
     * @return mixed Ritorna false se l'avatar non è stato trovato oppure l'indirizzo dell'immagine
     */
    public function setGravatarIfExist($user_ref)
    {
        $user_info = false;
        if (is_numeric($user_ref)) {
            $user_info = $this->getCustomerDataFromDB($user_ref);
        } else if((filter_var($user_ref, FILTER_VALIDATE_EMAIL)) ) {
            $user_info = $this->getCustomerDataByEmail($user_ref);
        }

        if($user_info) {
            $gravemail = md5(strtolower(trim($user_info['email'])));
            if (strpos(get_headers("http://www.gravatar.com/avatar/" . $gravemail . "?d=404")[0], '404') === false) {
                $avatar_name = 'gravatar_' . $user_info['id'] . '.jpeg';
                copy("http://www.gravatar.com/avatar/" . $gravemail . '?s=500', UPLOAD_TMP_FOR_DOMAIN . $avatar_name);
                copy("http://www.gravatar.com/avatar/" . $gravemail . '?s=150', UPLOAD_TMP_FOR_DOMAIN . 'tn/' . $avatar_name);
                $uploader = new \MSFramework\uploads('CUSTOMER_AVATAR');
                $ary_files = $uploader->prepareForSave(array($avatar_name));
                if($ary_files !== false) {
                    $this->MSFrameworkDatabase->pushToDB("UPDATE customers SET avatar = :avatar WHERE id = :id", array(':avatar' => json_encode(array($avatar_name)), ':id' => $user_info['id']));
                    return UPLOAD_TMP_FOR_DOMAIN_HTML . $avatar_name;
                }
            }
        }
        return false;
    }

    /**
     * Conta il numero totale degli utenti iscritti
     *
     * @return mixed
     */
    public function countRegisteredUsers() {
        return $this->MSFrameworkDatabase->getCount("SELECT * FROM customers WHERE active = 1");
    }

    /**
     * Recupera il lo storico dei crediti/debiti del cliente, restituendo sia un array con i singoli movimenti che il saldo finale
     *
     * @param string $id L'ID del cliente.
     *
     * @return mixed
     */
    public function getCustomerBalanceHistory($id) {
        $bilancio = 0.0;

        $ary_log = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM customers_balance WHERE cliente = :id ORDER BY data desc", array(":id" => $id)) as $operazione) {
            $operazione['note'] = $this->parseBalanceNotes($operazione['note']);
            $ary_log[] = $operazione;

            if($operazione['tipo'] == 'debito') {
                $bilancio -= (float)$operazione['valore'];
            }
            else {
                $bilancio += (float)$operazione['valore'];
            }
        }

        return array("log" => $ary_log, "balance" => $bilancio);
    }

    /**
     * Recupera il bilancio attuale del cliente
     *
     * @param string $id L'ID del cliente.
     *
     * @return mixed
     */
    public function getCustomerBalance($id) {
        $bilancio = 0.0;

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT tipo, valore FROM customers_balance WHERE cliente = :id", array(":id" => $id)) as $operazione) {
            if($operazione['tipo'] == 'debito') {
                $bilancio -= (float)$operazione['valore'];
            }
            else {
                $bilancio += (float)$operazione['valore'];
            }
        }

        return $bilancio;
    }

    /**
     * Effettua il parsing delle note dei crediti/debiti alla ricerca di tag speciali da convertire. Restituisce la nota convertita pronta per l'output
     *
     * @param $note La nota da analizzare
     *
     * @return mixed
     */
    public function parseBalanceNotes($note) {
        global $firephp;

        //Riferimento operazione
        $rif_op_tag = "{RIF_OP=";
        $pos_rif_op_start = strpos($note, $rif_op_tag);
        if($pos_rif_op_start !== false) {
            $pos_rif_op_end = strpos($note, '}', $pos_rif_op_start);
            $link_to_op_value = substr($note, $pos_rif_op_start+strlen($rif_op_tag), $pos_rif_op_end-($pos_rif_op_start+strlen($rif_op_tag)));

            $note = substr_replace($note, '<a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'fatturazione/operations/view.php?id=' . $link_to_op_value . '" target="_blank">Operazione #' . $link_to_op_value . '</a>', $pos_rif_op_start, $pos_rif_op_end-$pos_rif_op_start+1);
        }

        //Riferimento Cliente
        $rif_op_tag = "{RIF_CLIENTE=";
        $pos_rif_op_start = strpos($note, $rif_op_tag);
        if($pos_rif_op_start !== false) {
            $pos_rif_op_end = strpos($note, '}', $pos_rif_op_start);
            $link_to_op_value = substr($note, $pos_rif_op_start+strlen($rif_op_tag), $pos_rif_op_end-($pos_rif_op_start+strlen($rif_op_tag)));

            $note = substr_replace($note, '<a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'customers/list/edit.php?id=' . $link_to_op_value . '" target="_blank">Cliente #' . $link_to_op_value . '</a>', $pos_rif_op_start, $pos_rif_op_end-$pos_rif_op_start+1);
        }

        //Riferimento Cliente
        $rif_op_tag = "{RIF_SUBSCRIPTION_PLAN=";
        $pos_rif_op_start = strpos($note, $rif_op_tag);
        if($pos_rif_op_start !== false) {
            $pos_rif_op_end = strpos($note, '}', $pos_rif_op_start);
            $link_to_op_value = substr($note, $pos_rif_op_start+strlen($rif_op_tag), $pos_rif_op_end-($pos_rif_op_start+strlen($rif_op_tag)));

            $note = substr_replace($note, '<a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'subscriptions/plans/edit.php?id=' . $link_to_op_value . '" target="_blank">Piano Abbonamento #' . $link_to_op_value . '</a>', $pos_rif_op_start, $pos_rif_op_end-$pos_rif_op_start+1);
        }


        //Riferimento Cliente
        $rif_op_tag = "{RIF_TRAVELAGENCY_PACKAGE=";
        $pos_rif_op_start = strpos($note, $rif_op_tag);
        if($pos_rif_op_start !== false) {
            $pos_rif_op_end = strpos($note, '}', $pos_rif_op_start);
            $link_to_op_value = substr($note, $pos_rif_op_start+strlen($rif_op_tag), $pos_rif_op_end-($pos_rif_op_start+strlen($rif_op_tag)));

            $note = substr_replace($note, '<a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'travelagency/packages/edit.php?id=' . $link_to_op_value . '" target="_blank">Pacchetto Viaggio #' . $link_to_op_value . '</a>', $pos_rif_op_start, $pos_rif_op_end-$pos_rif_op_start+1);
        }

        //Riferimento Ordine Agenzia
        $rif_op_tag = "{RIF_TRAVELAGENCY_ORDER=";
        $pos_rif_op_start = strpos($note, $rif_op_tag);
        if($pos_rif_op_start !== false) {
            $pos_rif_op_end = strpos($note, '}', $pos_rif_op_start);
            $link_to_op_value = substr($note, $pos_rif_op_start+strlen($rif_op_tag), $pos_rif_op_end-($pos_rif_op_start+strlen($rif_op_tag)));

            $note = substr_replace($note, '<a href="' . ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'travelagency/ordini/edit.php?id=' . $link_to_op_value . '" target="_blank">Ordine Pacchetto Viaggio #' . $link_to_op_value . '</a>', $pos_rif_op_start, $pos_rif_op_end-$pos_rif_op_start+1);
        }

        return $note;
    }

    /**
     * Aggiunge o rimuove dei crediti al cliente
     *
     * @param string $id L'ID del cliente.
     * @param float $value Il valore dell'accredito/addebito (Può essere sia positivo che negativo)
     * @param string $note Eventuali note da allegare
     *
     * @return mixed
     */
    public function addNewOperation($id, $value, $note = "") {

        if($value > 0) {
            $tipo = 'credito';
        }
        else {
            $tipo = 'debito';
        }

        $array_to_save = array(
            "cliente" => $id,
            "tipo" => $tipo,
            "valore" => abs($value),
            "note" => $note
        );

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $result = $this->MSFrameworkDatabase->pushToDB("INSERT INTO customers_balance ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

        return $result;
    }

    /**
     * Aggiunge o rimuove dei regali al cliente
     *
     * @param int $id L'ID del cliente che riceve/scarica il regalo.
     * @param int $id_from L'ID del cliente che effettua il regalo.
     * @param string $gift_id L'ID del regalo (nel formato <tipologia>_<id>)
     * @param int $qty La quantità acquistata/scalata per il regalo (Può essere sia positivo che negativo)
     * @param array $ref L'array dell'oggetto prodotto/servizio/offerta selezionata
     * @param string $note Eventuali note da allegare
     *
     * @return mixed
     */
    public function addNewGiftOperation($id, $id_from, $gift_id, $qty, $ref, $note) {
        global $firephp;

        $array_to_save = array(
            "cliente" => $id,
            "cliente_from" => $id_from,
            "note" => $note,
            "qty" => $qty,
            "ref" => json_encode($ref),
            "gift_id" => $gift_id
        );

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $result = $this->MSFrameworkDatabase->pushToDB("INSERT INTO customers_gift ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
        return $result;
    }

    /**
     * Ottiene il numero di regali rimanenti per un determinato utente ed un determinato regalo
     *
     * @param $user_id L'id dell'utente
     * @param $gift_id L'ID del regalo (nel formato <tipologia>_<id>)
     *
     * @return int
     */
    public function giftsLeft($user_id, $gift_id) {
        $qty = 0;
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT qty FROM customers_gift WHERE cliente = :customer AND gift_id = :gift_id", array(":customer" => $user_id, ":gift_id" => $gift_id)) as $r) {
            $qty += $r['qty'];
        }

        return $qty;
    }

    /**
     * Ottiene la cronologia di tutti gli eventi relativi al cliente
     *
     * @param int $id L'ID del cliente.
     * @param int $page La pagina da mostrare (se 0 mostra tutto)
     * @param array $requested_info Un array con le info richieste [general|messages|booking|shopping] (vuoto per ottenere tutto)
     * @param int $s_p_p Il numero di eventi da mostrare su ogni pagina
     *
     * @return array
     */
    public function getCustomerHistories($id, $page = 0, $s_p_p = 10) {

        if($id > 0) {
            $user = $this->getCustomerDataFromDB($id);

            if (!$user) {
                return array();
            }
        } else {
            $user = '*';
        }

        // Creo le SQL necessarie
        $sql = $this->getCustomerHistorySQL($user);

        $start = 0;
        if($page > 0) {
            $start = ($page*$s_p_p)-$s_p_p;
        }

        $sql .= 'ORDER by date DESC LIMIT ' . $start . ', ' . $s_p_p;

        // Creo l'array leggibile dalla timeline / report
        $histories = array();

        foreach($this->MSFrameworkDatabase->getAssoc($sql) as $v) {

            $history = $this->formatCustomerHistories($v);

            $histories[] = $history;
        }

        return $histories;
    }

    public function formatCustomerHistories($v) {
        $history = array(
            'name' => '',
            'description' => $v['message'],
            'date' => $v['date'],
            'timeline' => array('icon' => 'star', 'bg' => 'blue', 'hex' => '#1c84c6'),
            'user' => array(
                'id' => $v['user_id'],
                'email' => $v['user_email']
            )
        );

        if($v['type'] == 'registration') {
            $history['name'] = 'Si è registrato sul sito';
            $history['description'] = 'Il cliente è stato registrato nel sistema.';
            $history['timeline'] = array('icon' => 'calendar', 'bg' => 'yellow', 'hex' => '#f8ac59');
        }

        if($v['type'] == 'appointment') {
            $history['name'] = 'Ha avuto un appuntamento';

            $extras = explode('|', $v['extra']);

            $services = array();
            if(!empty($extras[2])) {
                foreach((new \MSFramework\services())->getServiceDetails(explode(',', $extras[2])) as $service) {
                    $services[] = '<span class="badge">' . (new \MSFramework\i18n())->getFieldValue($service['nome']) . '</span> ';
                }
            }

            $op_data = ($extras[3] != "-1" ? (new \MSFramework\users())->getUserDataFromDB($extras[3], "cognome, nome") : array("cognome" => "Operatore", "nome" => "Generico"));

            $history['description'] = 'Dalle <i class="text-navy">' . date('H:i', $extras[0]) . '</i> alle <i class="text-navy">' . date('H:i', $extras[1]) . '</i> con <i class="text-navy">' . $op_data['cognome'] . ' ' . $op_data['nome'] . '</i>';
            if($services) $history['description'] .= '<br>' . implode($services, ' ');

            $history['timeline'] = array('icon' => 'calendar', 'bg' => 'red', 'hex' => '#FF0000');
        }

        if($v['type'] == 'contact') {
            $history['name'] = 'Richiesta di Contatto';
            $history['description'] = '<b>Messaggio Inviato:</b><br>' . $history['description'];
            $history['timeline'] = array('icon' => 'envelope', 'bg' => 'blue', 'hex' => '#1c84c6');
            $history['button'] = array('text' => 'Vedi di più', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'richieste/generiche/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'tracking_event') {
            $history['name'] = 'Tracciato evento <b>' . $v['message'] . '</b>';
            $history['description'] =  $v['message'] . ' (' . ($v['ref'] !== "" ? $v['ref'] : "<small>Nessuna info aggiuntiva</small>") . ')</b><br>';
            $history['timeline'] = array('icon' => 'dot-circle-o', 'bg' => 'yellow', 'hex' => '#f8ac59');
            $history['button'] = array('text' => 'Vedi di più', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'tracking/edit.php?id=' . $v['extra']);
        }

        if($v['type'] == 'form_submit') {

            $form_detail = (new \MSFramework\forms())->getFormDetails($v['ref'])[$v['ref']];

            $description = array();
            foreach(json_decode($v['extra'], true) as $nome_campo => $valore_campo) {
                $description[] = '<b>' . $nome_campo . '</b>: ' . $valore_campo;
            }

            $history['name'] = 'Ha inviato il form <b>' . $form_detail['nome'] . '</b>';
            $history['description'] =  implode('<br>', $description);
            $history['timeline'] = array('icon' => 'arrow-circle-right', 'bg' => 'blue', 'hex' => '#1c84c6');
            $history['button'] = array('text' => 'Vedi di più', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'pages/form/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'newsletter') {
            $history['name'] = 'Si è iscritto alla newsletter';
            $history['description'] = 'L\'iscrizione è attualmente: ' . ($v['extra'] == '1' ? '<span class="badge badge-primary">Attiva</span>' : '<span class="badge badge-danger">Inattiva</span>');
            $history['timeline'] = array('icon' => 'envelope', 'bg' => 'yellow', 'hex' => '#f8ac59');
            $history['button'] = array('text' => 'Gestisci Iscrizione', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'newsletter/destinatari/lista/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'newsletter__read') {
            $history['name'] = 'Ha letto la newsletter <b>' . $v['message'] . '</b>' . ($v['extra'] == "1" ? ' (+ click)' : '');
            $history['description'] = '<b>Campagna:</b><br>' . $v['message'];
            $history['timeline'] = array('icon' => 'envelope', 'bg' => 'navy', 'hex' => '#06994d');
            $history['button'] = array('text' => 'Vedi Campagna', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'newsletter/campaigns/status/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'newsletter__unsubscribe') {
            $history['name'] = 'Si è disiscritto dalla newsletter <b>' .  $v['message'] . '</b>';
            $history['description'] = '<b>Campagna:</b><br>' . $v['message'];
            $history['timeline'] = array('icon' => 'ban', 'bg' => 'red', 'hex' => '#FF0000');
            $history['button'] = array('text' => 'Vedi Campagna', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'newsletter/campaigns/status/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'booking') {
            $history['name'] = 'Ha prenotato la struttura';

            $extra = explode('|', $v['extra']);
            $ospiti = json_decode($extra[2]);

            $history['description'] = '<b>Ospiti:</b><br>' . $ospiti[0] . ' Adulti, ' . $ospiti[1] . ' Bambini';
            $history['description'] .= '<br><br><b>Date:</b><br>Dal <i>' . date('d/m/Y', strtotime($extra[0])). '</i> al <i>' . date('d/m/Y', strtotime($extra[1])) . '</i>';

            $history['timeline'] = array('icon' => 'hotel', 'bg' => 'blue', 'hex' => '#1c84c6');
            $history['button'] = array('text' => 'Vedi Prenotazione', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'richieste/prenotazioni/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'buy') {

            $info_operazione = (new \MSFramework\Fatturazione\vendite())->ottieniDettagliVendita($v['ref'])[$v['ref']];
            $info_operatore = (new \MSFramework\users())->getUserDataFromDB($info_operazione['operatore']);

            $totale_carrello = 0.0;
            foreach($info_operazione['carrello'] as $articolo) {
                $totale_carrello += $articolo['subtotal'];
            }


            $history['name'] = 'Ha effettuato un\'operazione';

            if(!empty($info_operazione['tipo_documento'])) {
                $document_names = array(
                    'preventivo' => 'Preventivo',
                    'ricevuta' => 'Ricevuta',
                    'fattura' => 'Fattura',
                    'scontrino' => 'Scontrino',
                    'scontrino_non_fiscale' => 'Scontrino non fiscale'
                );

                $history['name'] .= ' (' . $document_names[$info_operazione['tipo_documento']] . ')';
            }

            $history['description'] = '<b>Articoli:</b><br>' . count($info_operazione['carrello']);
            $history['description'] .= '<br><br><b>Totale:</b><br>' . number_format($totale_carrello, 2, ',', '.');
            $history['description'] .= '<br><br><b>Operatore:</b><br>' . $info_operatore['nome'] . ' ' . $info_operatore['cognome'];

            $history['timeline'] = array('icon' => 'calculator', 'bg' => 'blue', 'hex' => '#1c84c6');
            $history['button'] = array('text' => 'Vedi Operazione', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'fatturazione/operations/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'customers_gift') {
            $extras = explode('|', $v['extra']);

            $history['name'] = ($extras[0] == "0" && $extras[1] < 0 ? 'Ha utilizzato un Regalo' : 'Ha ricevuto un Regalo');
            $history['description'] = $v['message'];
            $history['date'] = $v['date'];
            $history['timeline'] = array('icon' => 'star', 'bg' => 'blue', 'hex' => '#1c84c6');

            $history['button'] = array('text' => 'Vedi Operazione', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'fatturazione/operations/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'customers_gift_donated') {
            $history['name'] = 'Ha effettuato un Regalo';

            $history['description'] = str_replace("Ricevuto regalo da", "Effettuato regalo a", $v['message']);
            $history['date'] = $v['date'];
            $history['timeline'] = array('icon' => 'star', 'bg' => 'blue', 'hex' => '#1c84c6');

            $history['button'] = array('text' => 'Vedi Operazione', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'fatturazione/operations/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'order') {

            $info_operazione = (new \MSFramework\Ecommerce\orders())->getOrderDetails($v['ref'])[$v['ref']];

            $totale_carrello = $info_operazione['cart']['coupon_price'];

            $history['name'] = 'Ha effettuato Ordine di ' . number_format($totale_carrello, 2, ',', '.') . CURRENCY_SYMBOL;

            $articoli = array();
            foreach($info_operazione['cart']['products'] as $product) {
                $articoli[] = '<small class="text-navy">' . $product['quantity'] . '* </small>' . $product['nome'];
            }

            $history['description'] = '<b>Articoli:</b><br>' . implode('<br>', $articoli);
            $history['description'] .= '<br><br><b>Totale:</b><br>' . number_format($totale_carrello, 2, ',', '.') . CURRENCY_SYMBOL;

            $history['timeline'] = array('icon' => 'calculator', 'bg' => 'blue', 'hex' => '#1c84c6');
            $history['button'] = array('text' => 'Vedi Ordine', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'ecommerce/ordini/edit.php?id=' . $v['ref']);
        }

        if($v['type'] == 'credito') {
            $history['name'] = 'Ha ricevuto un credito di ' . number_format($v['extra'], 2, ',', '.') . CURRENCY_SYMBOL;
            $history['description'] = (!empty($history['description']) ? '<br><br><b>Note:</b><br>' . $this->parseBalanceNotes($history['description']) : '');
            $history['description'] = '<b>Valore:</b><br>' . number_format($v['extra'], 2, ',', '.') . CURRENCY_SYMBOL . $history['description'];
            $history['timeline'] = array('icon' => 'money', 'bg' => 'navy', 'hex' => '#06994d');
            $history['button'] = array('text' => 'Gestisci Credito', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'customers/balance/edit.php?id=' . $v['id']);
        }

        if($v['type'] == 'debito') {
            $history['name'] = 'Ha ricevuto un debito di ' . number_format($v['extra'], 2, ',', '.') . CURRENCY_SYMBOL;
            $history['description'] = (!empty($history['description']) ? '<br><br><b>Note:</b><br>' . $this->parseBalanceNotes($history['description']) : '');
            $history['description'] = '<b>Valore:</b><br>' . number_format($v['extra'], 2, ',', '.') . CURRENCY_SYMBOL . $history['description'];

            $history['timeline'] = array('icon' => 'money', 'bg' => 'red', 'hex' => '#FF0000');
            $history['button'] = array('text' => 'Gestisci Credito', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'customers/balance/edit.php?id=' . $v['id']);
        }

        if($v['type'] == 'points') {
            if($v['extra'] < 0) {
                $history['name'] = 'Ha speso ' . abs($v['extra']) . ' punti';
                $history['description'] = (!empty($history['description']) ? '<br><br><b>Note:</b><br>' . $this->parseBalanceNotes($history['description']) : '');
                $history['description'] = '<b>Punti:</b><br>' . number_format($v['extra'], 2, ',', '.') . '' . $history['description'];

                $history['timeline'] = array('icon' => 'ticket', 'bg' => 'red', 'hex' => '#FF0000');
                $history['button'] = array('text' => 'Gestisci Punti', 'class' => 'danger', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'customers/points/edit.php?id=' . $v['id']);

            } else {
                $history['name'] = 'Ha guadagnato ' . abs($v['extra']) . ' punti';
                $history['description'] = (!empty($history['description']) ? '<br><br><b>Note:</b><br>' . $this->parseBalanceNotes($history['description']) : '');
                $history['description'] = '<b>Punti:</b><br>' . number_format($v['extra'], 2, ',', '.') . $history['description'];

                $history['timeline'] = array('icon' => 'ticket', 'bg' => 'navy', 'hex' => '#06994d');
                $history['button'] = array('text' => 'Gestisci Punti', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'customers/points/edit.php?id=' . $v['id']);
            }
        }

        if($v['type'] == 'seduta') {

            $seduta = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM beautycenter_sedute WHERE id = :id", array(':id' => $v['ref']), true);

            $ref = array();
            if(json_decode($seduta['ref'])) {
                $ref = json_decode($seduta['ref'], true);
            }

            if($v['extra'] > 0) {
                $history['name'] = 'Aggiunte ' . abs($v['extra']) . ' sedute';
            } else {
                $history['name'] = 'Rimosse ' . abs($v['extra']) . ' sedute';
            }

            $history['description'] = (!empty($history['description']) ? '<br><br><b>Note:</b><br>' . $history['description'] : '');
            $history['description'] = '<b>Sedute ' . ($v['extra'] > 0 ? 'Aggiunte' : 'Rimosse') . ':</b><br>' . abs($v['extra']) . $history['description'];

            if($ref) {
                $history['description'] = '<b>Pacchetto:</b><br>' . (new \MSFramework\i18n())->getFieldValue($ref['nome']) . '<br><br>' .  $history['description'];
            }

            $history['timeline'] = array('icon' => 'heart', 'bg' => 'yellow', 'hex' => '#f8ac59');
            $history['button'] = array('text' => 'Gestisci Sedute', 'class' => 'success', 'href' => ABSOLUTE_ADMIN_MODULES_PATH_HTML . 'beautycenter/sedute/edit.php?id=' . $v['id']);
        }

        return $history;
    }

    /**
     * Conta il numero di eventi relativi al cliente
     *
     * @param int $id L'ID del cliente.
     * @param array $requested_info Un array con le info richieste [general|messages|booking|shopping] (vuoto per ottenere tutto)
     *
     * @return mixed
     */
    public function countCustomerHistories($id) {
        $user = $this->getCustomerDataFromDB($id);

        if(!$user) {
            return 0;
        }

        $sql = $this->getCustomerHistorySQL($user);

        return $this->MSFrameworkDatabase->getCount($sql);
    }

    public function getCustomerHistorySQL($user = "") {
        Global $MSFrameworkCMS, $MSFrameworkDatabase;

        // Creo le SQL necessarie
        $sql = "SELECT * FROM (";

        $sql_split = array();

        $sql_split[] = "(SELECT 'registration' as type, UNIX_TIMESTAMP(c.registration_date) as date, '' as message, '' as ref, '' as extra, c.id as user_id, c.email as user_email FROM customers c)";

        if($MSFrameworkCMS->checkExtraFunctionsStatus('appointments')) {
            $sql_split[] = "(SELECT 'appointment' as type, UNIX_TIMESTAMP(appointments_list.start_date) as date, '' as message, appointments_list.id as ref, CONCAT(appointments_list.start_date, '|', appointments_list.end_date, '|', appointments_list.services, '|', appointments_list.user) as extra, appointments_list.user as user_id, '' as user_email FROM appointments_list)";
        }

        /* NEWSLETTER */
        $sql_split[] = "(SELECT 'newsletter' as type, UNIX_TIMESTAMP(newsletter__destinatari.data_inserimento) as date, newsletter__destinatari.commenti as message, newsletter__destinatari.id as ref, newsletter__destinatari.active as extra, newsletter__destinatari.id as user_id, '' as user_email FROM newsletter__destinatari)";
        $sql_split[] = "(SELECT 'newsletter__unsubscribe' as type, UNIX_TIMESTAMP(newsletter__emails_destinatari_status.date_open) as date, (SELECT nome FROM newsletter__emails WHERE newsletter__emails_destinatari_status.id_email = newsletter__emails.id) as message, newsletter__emails_destinatari_status.id_email as ref, newsletter__emails_destinatari_status.clicked as extra, newsletter__emails_destinatari_status.id_recipient as user_id, '' as user_email FROM newsletter__emails_destinatari_status WHERE newsletter__emails_destinatari_status.status = 3)";
        $sql_split[] = "(SELECT 'newsletter__read' as type, UNIX_TIMESTAMP(newsletter__emails_destinatari_status.date_open) as date, (SELECT nome FROM newsletter__emails WHERE newsletter__emails_destinatari_status.id_email = newsletter__emails.id) as message, newsletter__emails_destinatari_status.id_email as ref, newsletter__emails_destinatari_status.clicked as extra,  newsletter__emails_destinatari_status.id_recipient as user_id, '' as user_email FROM newsletter__emails_destinatari_status WHERE newsletter__emails_destinatari_status.status > 1)";

        $sql_split[] = "(SELECT 'tracking_event' as type, UNIX_TIMESTAMP(tracking__stats.track_date) as date, tracking__stats.track_event as message, tracking__stats.track_ref as ref, tracking__stats.project_id as extra, tracking__stats.user_ref as user_id, '' as user_email FROM tracking__stats)";

        $sql_split[] = "(SELECT 'form_submit' as type, UNIX_TIMESTAMP(forms_histories.submit_date) as date, '' as message, forms_histories.form_id as ref, forms_histories.form_data as extra, forms_histories.user_ref as user_id, forms_histories.user_ref as user_email FROM forms_histories)";

        $sql_split[] = "(SELECT 'contact' as type, richieste.creation_time as date, richieste.messaggio as message, richieste.id as ref, '' as extra, '' as user_id, richieste.contatto as user_email FROM richieste WHERE type LIKE 'generic')";

        if($MSFrameworkCMS->checkExtraFunctionsStatus('beautycenter') || $MSFrameworkCMS->checkExtraFunctionsStatus('hotel') || $MSFrameworkCMS->checkExtraFunctionsStatus('camping')) {
            $sql_split[] = "(SELECT 'booking' as type, UNIX_TIMESTAMP(prenotazioni.creation_time) as date, prenotazioni.info_aggiuntive as message, prenotazioni.id as ref, CONCAT(prenotazioni.arrivo, '|', prenotazioni.partenza, '|', prenotazioni.ospiti) as extra, '' as user_id, prenotazioni.email as user_email FROM prenotazioni)";
        }

        $sql_split[] = "(SELECT 'buy' as type, UNIX_TIMESTAMP(fatturazione_vendite.data) as date, '' as message, fatturazione_vendite.id as ref, fatturazione_vendite.carrello as extra, fatturazione_vendite.cliente as user_id, '' as user_email FROM fatturazione_vendite)";
        $sql_split[] = "(SELECT customers_balance.tipo as type, UNIX_TIMESTAMP(customers_balance.data) as date, customers_balance.note as message, customers_balance.id as ref, customers_balance.valore as extra, customers_balance.cliente as user_id, '' as user_email FROM customers_balance)";
        $sql_split[] = "(SELECT 'customers_gift' as type, UNIX_TIMESTAMP(customers_gift.data) as date, customers_gift.note as message, customers_gift.id as ref, CONCAT(customers_gift.cliente_from, '|', customers_gift.qty) as extra, customers_gift.cliente as user_id, '' as user_email FROM customers_gift)";
        $sql_split[] = "(SELECT 'customers_gift_donated' as type, UNIX_TIMESTAMP(customers_gift.data) as date, customers_gift.note as message, customers_gift.id as ref, customers_gift.cliente_from as extra, customers_gift.cliente_from as user_id, '' as user_email FROM customers_gift)";

        if ($MSFrameworkCMS->checkExtraFunctionsStatus('beautycenter')) {
            $sql_split[] = "(SELECT 'seduta' as type, UNIX_TIMESTAMP(beautycenter_sedute.data) as date, beautycenter_sedute.note as message, beautycenter_sedute.id as ref, beautycenter_sedute.valore as extra, beautycenter_sedute.cliente as user_id, '' as user_email FROM beautycenter_sedute)";
        }

        if ($MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) {
            $sql_split[] = "(SELECT 'order' as type, UNIX_TIMESTAMP(ecommerce_orders.order_date) as date, '' as message, ecommerce_orders.id as ref, ecommerce_orders.cart as extra, ecommerce_orders.user_id as user_id, ecommerce_orders.guest_email as user_email FROM ecommerce_orders)";
        }

        if($MSFrameworkCMS->checkExtraFunctionsStatus('points')) {
            $sql_split[] = "(SELECT 'points' as type, UNIX_TIMESTAMP(points_histories.data) as date, points_histories.note as message, points_histories.id as ref, points_histories.valore as extra, points_histories.cliente as user_id, '' as user_email FROM points_histories)";
        }

        $sql .= implode(" UNION ALL ", $sql_split);
        $sql .= ") results";

        if(is_array($user)) {
            $sql .= " WHERE (results.user_id = " . $user['id'];
            if (isset($user['email'])) $sql .= " OR results.user_email LIKE " . (strpos($user['email'], 'customers.') !== false ? $user['email'] : $MSFrameworkDatabase->quote($user['email']));
            $sql .= ")";
        } else {
            $sql .= " WHERE ((results.user_id != '' AND results.user_id IN (SELECT id FROM customers)) OR results.user_email != '')";
        }

        return $sql;

    }

    /**
     * Ottiene l'anteprima del cliente in HTML
     * @param mixed L'ID del cliente o l'array contenente le info
     * @return string
     */
    public function getCustomerPreviewHTML($data) {
        if(!is_array($data)) {
            $data = $this->getCustomerDataFromDB($data);
        }

        $return_html = '';

        if($data) {

            $info = array();
            if (!empty($data['email'])) {
                $info[] = $data['email'];
            }
            if (!empty($data['telefono_casa'])) {
                $info[] = $data['telefono_casa'];
            }
            if (!empty($data['telefono_cellulare'])) {
                $info[] = $data['telefono_cellulare'];
            }

            $return_html = '';


            $avatar_url = CUSTOMER_DOMAIN_INFO['backend_url'] . 'assets/img/user.png';
            if(json_decode($data['avatar'])) {

                $avatarUploads = new \MSFramework\uploads('CUSTOMER_AVATAR');

                if($this->forcingDB) {
                    Global $MSFrameworkFW;
                    $customerSiteData = $MSFrameworkFW->getWebsitePathsBy('customer_database', $this->forcingDB);

                    $avatarUploads->path = str_replace(CUSTOMER_DOMAIN_INFO['path'], $customerSiteData['path'], $avatarUploads->path);
                    $avatarUploads->path_html = str_replace(CUSTOMER_DOMAIN_INFO['url'], $customerSiteData['url'] , $avatarUploads->path_html);

                }

                $avatar_url = $avatarUploads->path_html . json_decode($data['avatar'], true)[0];
            }

            $return_html .= '<div style="position: relative; padding-left: 65px;">';
            $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px">';
            $return_html .= '<h2 style="margin: 0;">' . htmlentities($data['nome'] . ' ' . $data['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
            $return_html .= '</div>';

        }

        return $return_html;

    }

}
<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

namespace MSFramework\Points;

class emails extends \MSFramework\emails {
    /**
     * Ottiene la lista di template per l'ecommerce
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {
        $default_address = $this->MSFrameworkCMS->getCMSData("settings")['destination_address'];

        $templates = array(
            'points' => array(
                'points_earned' => array(
                    'nome' => 'Notifica guadagno punti',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Hai guadagnato {points} punti - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{name}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{points}" => "I punti guadagnati",
                            "{note}" => "Le note allegate"
                        ),
                        array(
                            "{[name]}",
                            "{[email]}",
                            "{[points]}",
                            "{[note]}"
                        )
                    )
                )
            )
        );

        return $templates;
    }
}
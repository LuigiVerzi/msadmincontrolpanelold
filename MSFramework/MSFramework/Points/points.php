<?php
/**
 * MSFramework
 * Date: 17/02/18
 */

namespace MSFramework\Points;


class points
{
    public function __construct()
    {
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Recupera il lo storico dei crediti/debiti del cliente, restituendo sia un array con i singoli movimenti che il saldo finale
     *
     * @param string $id L'ID del cliente.
     *
     * @return mixed
     */
    public function getCustomerBalanceHistory($id)
    {
        $bilancio = 0.0;

        $MSFrameworkCustomers = new \MSFramework\customers();

        $ary_log = array();
        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT * FROM points_histories WHERE cliente = :id ORDER BY data desc", array(":id" => $id)) as $operazione) {
            $operazione['note'] = $MSFrameworkCustomers->parseBalanceNotes($operazione['note']);
            $ary_log[] = $operazione;

            $bilancio += (float)$operazione['valore'];
        }

        return array("log" => $ary_log, "balance" => $bilancio);
    }

    /**
     * Recupera il bilancio attuale del cliente
     *
     * @param string $id L'ID del cliente.
     *
     * @return mixed
     */
    public function getCustomerBalance($id)
    {
        $bilancio = 0.0;

        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT valore FROM points_histories WHERE cliente = :id", array(":id" => $id)) as $operazione) {
            $bilancio += (float)$operazione['valore'];
        }

        return $bilancio;
    }

    /**
     * Aggiunge o rimuove dei crediti al cliente
     *
     * @param string $id L'ID del cliente.
     * @param float $value Il valore dell'accredito/addebito (Può essere sia positivo che negativo)
     * @param string $note Eventuali note da allegare
     *
     * @return mixed
     */
    public function addNewOperation($id, $value, $note = "")
    {

        $array_to_save = array(
            "cliente" => $id,
            "valore" => $value,
            "note" => $note
        );

        $operation_hash = '';
        if(count(explode('#', $note)) === 2) {
            $operation_hash = '#' . trim(explode('#', $note)[1]);
        }

        // Se il cliente ha già incassato dei punti per il medesimo motivo non li riassegno
        if($operation_hash && $this->MSFrameworkDatabase->getCount("SELECT * FROM points_histories WHERE cliente = :cliente AND note LIKE '%$operation_hash%'", array(':cliente' => $id)) > 0) {
            return false;
        }

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $result = $this->MSFrameworkDatabase->pushToDB("INSERT INTO points_histories ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

        // Se il cliente ha guadagnato dei punti lo notifico via email
        if($value > 0) {
            $customerData = (new \MSFramework\customers())->getCustomerDataFromDB($id);
            (new \MSFramework\Points\emails())->sendMail('points_earned', array(
                'name' => trim($customerData['nome'] . ' ' . $customerData['cognome']),
                'email' => $customerData['email'],
                'points' => $value,
                'note' => $note
            ));
        }

        return $result;
    }

    /**
     * Ritorna il numero di punti da assegnare in base alla spesa
     * @param $checkout_value float Il valore economico
     * @return int
     */
    public function getPointsToAssign($checkout_value) {
        Global $MSFrameworkCMS;

        $pointSettings = $MSFrameworkCMS->getCMSData('ecommerce_points');
        $ratio = $pointSettings['assignation']['checkout_points']/$pointSettings['assignation']['checkout_value'];

        return (int)($checkout_value * $ratio);
    }

    /**
     * Ritorna il valore dello sconto da applicare all'ordine
     * @param $user_id float Il numero di punti da utilizzare
     * @param $checkout_value float Il valore economico
     * @return array (Sconto da applicare, Punti da utilizzare)
     */
    public function getSaleValue($user_id, $checkout_value) {
        Global $MSFrameworkCMS;

        if(!$user_id) return array(0, 0);

        $pointSettings = $MSFrameworkCMS->getCMSData('ecommerce_points');
        $customerBalance = $this->getCustomerBalance($user_id);

        // Se il cliente non ha raggiunto il numero minimo di punti utilizzabile
        if((int)$pointSettings['conversion']['min_points_value'] > 0 && $pointSettings['conversion']['min_points_value'] > $customerBalance) {
            return array(0, 0);
        }

        $sale_to_return = 0;
        $points_to_use = 0;

        $max_sale_to_return = $pointSettings['conversion']['max_sale_value'];

        if($max_sale_to_return && $pointSettings['conversion']['max_sale_type'] === '%') {
            $max_sale_to_return = ($checkout_value*$max_sale_to_return)/100;
        }

        if(!(int)$max_sale_to_return || $max_sale_to_return > $checkout_value) {
            $max_sale_to_return = $checkout_value;
        }

        if($customerBalance > 0 && $pointSettings['conversion']['min_checkout_value'] <= $checkout_value) {
            $ratio = $pointSettings['conversion']['value']/$pointSettings['conversion']['points'];

            $sale_to_return = ($customerBalance * $ratio);
            $points_to_use = ($sale_to_return/$ratio);

            if($sale_to_return > $max_sale_to_return) {
                return array($max_sale_to_return, ($max_sale_to_return/$ratio));
            }
        }

        return array($sale_to_return, $points_to_use);

    }

}
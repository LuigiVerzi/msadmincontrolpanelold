<?php
/**
 * MSFramework
 * Date: 23/06/18
 */

namespace MSFramework\Modules;

class gdpr {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase, $MSFrameworkCMS, $MSFrameworkModules;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->modules = $MSFrameworkModules;

        $this->extra_module_settings = $this->modules->getExtraModuleSettings('gdpr');

    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude() {
        global $firephp;

        $files_to_include = array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'modules/gdpr/js/gdpr.js'),
            'css' => array(FRAMEWORK_COMMON_CDN . 'modules/gdpr/css/gdpr.css'),
            'html' => array(FRAMEWORK_ABSOLUTE_PATH . "common/modules/gdpr/php/gdpr.php")
        );

        return $files_to_include;
    }

    /**
     * Verifica se un tipo di cookie è stato esplicitamente consentito dall'utente o meno (se il supporto GDPR è disabilitato, tutti i cookie sono ammessi)
     *
     * @param $type Il tipo di cookie da verificare
     *
     * @return bool
     */
    public function checkCookieTypeAllowed($type) {

        if(!$this->modules->checkIfIsActive('gdpr') || $_COOKIE['GDPR_' . $type] == "1" || in_array($type, $this->extra_module_settings['auto_enabled_modules'])) {
            return true;
        }

        return false;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {
        if($params['action'] == 'getModal') {
            ob_start();
            include FRAMEWORK_ABSOLUTE_PATH . "common/modules/gdpr/php/modal.php";
            return  ob_get_clean();
        }
    }


    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $array_settings = array(
            "accept_all_label" => array(
                "name" => "Testo pulsante accettazione",
                "input_type" => "text",
                "input_attributes" => array(),
                "helper" => "Il testo del pulsante di accettazione dei Cookie",
                "mandatory" => false,
                "translations" => false,
                "default" => 'Abilita Tutto',
            ),
            "hide_settings" => array(
                "name" => "Nascondi icona impostazioni",
                "input_type" => "checkbox",
                "input_attributes" => array(),
                "helper" => "Nasconde l'icona delle impostazioni",
                "mandatory" => false,
                "translations" => false,
                "default" => 0,
            )
        );

        return $array_settings;
    }

    /**
     * Contiene tutte le risorse includibili nei siti che sono soggette al filtro GDPR, prelevate dal modulo "Risorse GDPR"
     *
     * La chiave dell'array (lo slug prelevato dal modulo) è l'ID della risorsa (da richiamare attraverso la funzione injectJS()). Se vuota, verranno prelevati i dati di tutti gli elementi
     *
     * La chiave "type" del sotto-array rappresenta il tipo di cookie che l'utente deve accettare per abilitare la risorsa
     * La chiave "load" del sotto-array contiene (in un array) tutte le risorse da caricare. Le impostazioni di tali risorse si trovano di seguito. Questo array viene utilizzato insieme alla chiave "keywords" per effettuare la ricerca dei plugin attivi.
     * La chiave "keywords" un array che contiene le keywords da cercare nella pagina affinchè l'elemento risulti attivo (sia nelle impostazioni del framework che nella descrizione del tag delle pagine [GDPR_DETAILS])
     *
     * La chiave "inject" del sotto-array è il codice da iniettare nella pagina
     * La chiave "in-tag" consente di scegliere il tipo di iniezione da effettuare. Se impostata su "true" inietta il codice all'interno del tag <script>. In caso contrario (default) imposta il valore di "inject" come attributo "src" del tag <script>
     * La chiave "tag-type" è utilizzata solo se in-tag è true. Di default assume il valore "script" ma è possibile impostare un valore diverso (ad esempio "noscript") per wrappare il codice all'interno di un tag differente
     * La chiave "async" consente di scegliere se impostare l'attributo "async" del tag <script>
     *
     * @param $what
     *
     * @return mixed
     */
    private function getElementInfo($what = "") {
        global $firephp;

        $append_where = "";
        $append_where_ary = array();
        if($what != "") {
            $append_where = " AND slug = :slug";
            $append_where_ary[':slug'] = $what;
        }

        $array = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`gdpr_scripts` WHERE id != '' $append_where", $append_where_ary) as $r) {
            $load_ary = array();
            foreach(json_decode($r['scripts'], true) as $script) {
                $load_ary[] = array(
                    "inject" => $script[0],
                    "tag-type" => $script[1],
                    "in-tag" => ($script[2] == "1"),
                    "async" => ($script[3] == "1"),
                );
            }

            $array[$r['slug']] = array(
                "keywords" => explode(",", $r['keywords']),
                "load" => $load_ary,
                "details" => array(
                    "nome" => $r['nome'],
                    "descr" => $r['descrizione'],
                    "dati_raccolti" => $r['dati_raccolti'],
                    "luogo_trattamento" => $r['luogo_trattamento'],
                    "privacy_policy_url" => $r['privacy_policy_url'],
                    "opt_out_url" => $r['opt_out_url'],
                )
            );

            unset($r['keywords']);
            unset($r['nome']);
            unset($r['descrizione']);
            unset($r['dati_raccolti']);
            unset($r['luogo_trattamento']);
            unset($r['privacy_policy_url']);
            unset($r['opt_out_url']);

            $array[$r['slug']] = array_merge($array[$r['slug']], $r);
        }

        return $array;
    }

    /**
     * Determina se includere direttamente il JS nel sito (in base alle preferenze del GDPR) o se tenerlo temporaneamente da parte in attesa di un'eventuale abilitazione da parte dell'utente.
     *
     * @param $what array Il tipo di risorsa da includere. L'elenco delle risorse si trova nella funzione getElementInfo();
     * In alternativa, è possibile passare un array dello stesso formato di quelli presenti nella funzione getElementInfo() per caricare una risorsa personalizzata
     *
     * @return string
     */
    public function injectJS($what) {
        $to_return = "";

        if(is_array($what)) {
            $info = $what;
        } else {
            $info = $this->getElementInfo($what);
        }

        if(is_array($info['mandatory']) && in_array("", $info['mandatory'], true)) {
            return $to_return;
        }

        $cookie_allowed = $this->checkCookieTypeAllowed($info['type']);

        foreach($info['load'] as $to_load) {
            if(is_array($to_load['mandatory']) && in_array("", $to_load['mandatory'], true)) {
                continue;
            }

            $in_tag = ($to_load['in-tag']) ? "1" : "0";
            $async = ($to_load['async']) ? "async" : "";
            $in_tag_type = ($to_load['tag-type'] != "") ? $to_load['tag-type'] : "script";
            if($cookie_allowed) {
                if($in_tag == "1") {
                    $to_return .= '<' . $in_tag_type . '>' . $to_load['inject'] . '</' . $in_tag_type . '>';
                } else {
                    $to_return .= '<script ' . $async . ' type="text/javascript" src="' . $to_load['inject'] . '" /></script>';
                }
            } else {
                $to_return .= '<textarea class="gdpr_waiting" data-type="' . $info['type'] . '" data-in-tag="' . $in_tag . '" data-in-tag-type="' . $in_tag_type . '" data-async="' . $async . '" style="display: none;">' . $to_load['inject'] . '</textarea>';
            }
        }

        return $to_return;
    }

    /**
     * Restituisce alcuni dettagli relativi alle categorie (raggruppamenti di script che l'utente abilita/disabilita) utilizzabili all'interno del modulo GDPR
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getCategorieGDPR($key = "") {
        $array = array(
            'preferenze' => array(
                'titolo' => 'Preferenze & Tecnici',
                'descrizione' => 'I Cookie tecnici hanno la funzione di permettere lo svolgimento di attività strettamente legate al funzionamento di questo spazio online. In questi sono registrate informazioni relative alle Sue preferenze e altri dati tecnici che permettono una navigazione più agevole.',
                'principale' => true, // Senza di esso non è possibile attivare gli altri
                'icona' => '<img src="' . FRAMEWORK_COMMON_CDN . 'modules/gdpr/images/icons/check-circle-solid.svg" class="gdpr_svg_ico">',
                'checkbox' => array(
                    'unchecked_message' => 'Se disabiliti questo cookie, non saremo in grado di salvare le tue preferenze. Ciò significa che ogni volta che visiti questo sito web dovrai abilitare o disabilitare nuovamente i cookie.'
                )
            ),
            'statistiche' => array(
                'titolo' => 'Statistiche',
                'descrizione' => 'Questa tipologia di cookie consente di ottenere informazioni statistiche riguardanti l\'utilizzo del sito da parte dell\'utente',
                'icona' => '<img src="' . FRAMEWORK_COMMON_CDN . 'modules/gdpr/images/icons/chart-bar-regular.svg" class="gdpr_svg_ico">',
                'checkbox' => array(
                    'required' => 'preferenze',
                    'required_message' => 'Per favore abilita i Cookie generali prima, altrimenti non sarà possibile salvare le tue preferenze.'
                )
            ),
            'marketing' => array(
                'titolo' => 'Marketing',
                'descrizione' => 'Questo tipo di servizi consente a questa Applicazione ed ai suoi partner di comunicare, ottimizzare e servire annunci pubblicitari basati sull’utilizzo passato di questa Applicazione da parte dell’Utente. Questa attività viene effettuata tramite il tracciamento dei Dati di Utilizzo e l’uso di Cookie, informazioni che vengono trasferite ai partner a cui l’attività di remarketing e behavioral targeting è collegata.',
                'icona' => '<img src="' . FRAMEWORK_COMMON_CDN . 'modules/gdpr/images/icons/cog-solid.svg" class="gdpr_svg_ico">',
                'checkbox' => array(
                    'required' => 'preferenze',
                    'required_message' => 'Per favore abilita i Cookie generali prima, altrimenti non sarà possibile salvare le tue preferenze.'
                )
            )
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Cerca e restituisce tutti gli elementi all'interno di una pagina (HTML) che corrispondono a quelli da segnalare nel GDPR
     *
     * @param $pageContent Il contenuto HTML della pagina da analizzare. Se vuoto, restituisce i dati prelevandoli dalla index del sito
     *
     * @return array
     */
    public function getGDPRElementsFromHTML($pageContent = "") {
        if($pageContent == "") {
            $pageContent = file_get_contents($this->MSFrameworkCMS->getURLToSite());
        }

        $trackingClass = new \MSFramework\tracking();
        $tracking_id = $trackingClass->getTrackingProjectIDsByWebsite();

        //compongo l'array contenente tutti gli elementi che devo cercare (definiti nella funzione getElementInfo())
        $look_for = array();
        foreach($this->getElementInfo() as $elementK => $elementV) {
            foreach($elementV['load'] as $what_loads) {
                if($what_loads['inject'] == "") {
                    continue;
                }

                $look_for[$elementK][] = $what_loads['inject'];
            }

            foreach($elementV['keywords'] as $kw) {
                $look_for[$elementK][] = $kw;
            }

            if($tracking_id) {
                $look_for[$elementK][] = $trackingClass->getJSCodeForTracking($tracking_id[0], $elementV['tracking_suite_id']);
            }
        }

        //cerco nel dom tutti gli elementi che POTREBBERO corrispondere a quello che cerco (tag script, tag con classe gdpr_waiting, etc)
        $doc = new \DOMDocument();
        $doc->loadHTML($pageContent);
        $finder = new \DomXPath($doc);

        $gotTags = array();
        foreach($doc->getElementsByTagName('script') as $script_tag) {
            $gotTags[] = $script_tag->nodeValue;
            $gotTags[] = $script_tag->getAttribute('src');
        }

        foreach($finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' gdpr_waiting ')]") as $script_tag) {
            $gotTags[] = $script_tag->nodeValue;
            $gotTags[] = $script_tag->getAttribute('src');
        }

        $gotTags = array_filter($gotTags);

        //verifico quanti degli elementi della pagina in analisi sono contenuti nell'array "$look_for" e restituisco le chiavi di quelli corrispondenti
        $found_matches = array();
        foreach($look_for as $k => $v) {
            foreach($v as $looking_for) {
                if($looking_for == "") continue;

                foreach($gotTags as $cur_tag) {
                    if($cur_tag == "") continue;

                    if(stristr($cur_tag, $looking_for) || stristr($looking_for, $cur_tag)) {
                        $found_matches[] = $k;
                    }
                }

            }
        }

        return $found_matches;
    }

    /**
     * Restituisce un testo HTML contenente le descrizioni/dettagli di tutti gli elementi GDPR passati nell'array $elements
     *
     * @param array $elements Un array di elementi GDPR per i quali creare la descrizione. Se vuoto, verrà creata una descrizione per tutti gli elementi disponibili nella funzione getElementInfo().
     * @param bool $check_user_settings Se impostato su false, non tiene conto delle eventuali personalizzazioni (abilita/disabilita manuale) dell'utente
     */
    public function composeGDPRText($elements = array(), $check_user_settings = true) {
        global $MSFrameworki18n;

        $return = "";
        if(count($elements) == "") {
            $elements = array_keys($this->getElementInfo());
        }

        if($check_user_settings) {
            $extra_module_settings = $this->modules->getExtraModuleSettings('gdpr');

            if(is_array($extra_module_settings['cookie_policy']['force_show'])) {
                $elements = array_unique(array_merge($elements, $extra_module_settings['cookie_policy']['force_show']));
            }

            if(is_array($extra_module_settings['cookie_policy']['force_hide'])) {
                $elements = array_unique(array_diff($elements, $extra_module_settings['cookie_policy']['force_hide']));
            }
        }

        if(count($elements) == 0) {
            return "";
        }

        foreach($elements as $cur_element) {
            $elements_info[$cur_element] = $this->getElementInfo($cur_element)[$cur_element];
        }

        $type_grouped = $this->groupElementsByType($elements_info);

        $old_type = "";

        foreach($type_grouped as $element_type => $elements) {
            if($old_type != $element_type) {
                $section_data = $this->getCategorieGDPR($element_type);

                $return .= "<div style='margin-bottom: 15px;'>";
                    $return .= "<h2>" . $section_data['titolo'] . "</h2>";
                    $return .= "<div>" . $section_data['descrizione'] . "</div>";
                $return .= "</div>";
            }

            foreach($elements as $element) {
                $gdpr_info = $element['details'];

                $return .= "<div style='margin-top: 20px; margin-bottom: 20px;'>";
                    $return .= "<h4>" . $MSFrameworki18n->getFieldValue($gdpr_info['nome']) . "</h4>";
                    $return .= "<div>" . $MSFrameworki18n->getFieldValue($gdpr_info['descr']) . "</div>";
                    $return .= "<div>Dati personali raccolti: " . $MSFrameworki18n->getFieldValue($gdpr_info['dati_raccolti']) . "</div>";
                    $return .= "<div>Luogo del trattamento: " . $MSFrameworki18n->getFieldValue($gdpr_info['luogo_trattamento']) . "</div>";
                    $return .= "<div>" . ($gdpr_info['privacy_policy_url'] != "" ? ' <a href="' . $gdpr_info['privacy_policy_url'] . '">Privacy Policy</a>' : '') . ($gdpr_info['opt_out_url'] != "" ? ' <a href="' . $gdpr_info['opt_out_url'] . '">Opt Out</a>' : '') . "</div>";
                $return .= "</div>";
            }

            $old_type = $element_type;
        }

        return $return;
    }

    /**
     * Raggruppa per tipo gli elementi provenienti dalla funzione getElementInfo()
     *
     * @param $elements_info Gli elementi prelevati dalla funzione getElementInfo(). Se vuoto, verranno raggruppati tutti gli elementi.
     *
     * @return array
     */
    public function groupElementsByType($elements_info = array()) {
        if(count($elements_info) == 0) {
            $elements_info = $this->getElementInfo();
        }

        $type_grouped = array();
        foreach($elements_info as $element_infoK => $element_info) {
            $type_grouped[$element_info['type']][$element_infoK] = $element_info;
        }

        return $type_grouped;
    }
}
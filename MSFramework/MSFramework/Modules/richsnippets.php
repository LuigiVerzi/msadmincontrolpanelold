<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class richsnippets {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules, $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
        $this->extra_functions = json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true);
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {

        $files_to_include = array('html' => array());

        if(isset($_GET['action'])) {

            if(in_array('ecommerce', $this->extra_functions)) {
                if ($_GET['action'] == 'getEcommerceProduct') {
                    if ($this->modules->getSettingValue('richsnippets', 'ecommerce_prodotto')) {
                        $files_to_include['html'][] = FRAMEWORK_ABSOLUTE_PATH . 'common/modules/richsnippets/php/ecommerce/product.php';
                    }
                }
            }

            if(in_array('blog', $this->extra_functions)) {
                if ($_GET['action'] == 'getBlogPost') {
                    if ($this->modules->getSettingValue('richsnippets', 'blog_articolo')) {
                        $files_to_include['html'][] = FRAMEWORK_ABSOLUTE_PATH . 'common/modules/richsnippets/php/blog/post.php';
                    }
                }
            }

        }

        if(!isset($_GET['slug'])) {
            $files_to_include['html'][] = FRAMEWORK_ABSOLUTE_PATH . 'common/modules/richsnippets/php/home.php';
        }

        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {

        return true;

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {

        $array_settings = array(
            'generale_titolo' => array(
                "name" => "Rich Snippets Generali",
                "input_type" => "title"
            ),
            'generale_social' => array(
                "name" => "Rich Snippet Social",
                "input_type" => "checkbox",
                "input_attributes" => array(),
                "helper" => "Inserisce lo snippet con i link dei social.",
                "mandatory" => false,
                "translations" => false,
                "default" => true,
            ),
            'generale_logo' => array(
                "name" => "Rich Snippet Logo",
                "input_type" => "checkbox",
                "input_attributes" => array(),
                "helper" => "Mostra il logo dell'azienda su Google",
                "mandatory" => false,
                "translations" => false,
                "default" => false,
            ),
            'generale_ricerca_active' => array(
                "name" => "Rich Snippet Query Ricerca",
                "input_type" => "checkbox",
                "input_attributes" => array(),
                "helper" => "Aggiunge la barra delle ricerche su google",
                "mandatory" => false,
                "translations" => false,
                "default" => true,
            ),
            'generale_ricerca_url' => array(
                "name" => "URL destinazione Ricerca",
                "input_type" => "text",
                "input_attributes" => array(),
                "check_to_show" => array('generale_ricerca_active'),
                "helper" => "Inserisci l'URL della pagina di ricerca. (Non eliminare {search_term})",
                "mandatory" => false,
                "translations" => false,
                "default" => (new \MSFramework\cms())->getURLToSite() . 's/{search_term}',
            )
        );

        if(in_array('ecommerce', $this->extra_functions)) {
            $array_settings['ecommerce_titolo'] = array(
                "name" => "RichSnippets Ecommerce",
                "input_type" => "title"
            );
            $array_settings['ecommerce_prodotto'] = array(
                "name" => "Rich Snippet Prodotto",
                "input_type" => "checkbox",
                "input_attributes" => array(),
                "helper" => "Inserisce lo snippet del prodotto.",
                "mandatory" => false,
                "translations" => false,
                "default" => true,
            );
        }

        if(in_array('blog', $this->extra_functions)) {
            $array_settings['blog_titolo'] = array(
                "name" => "RichSnippets Blog",
                "input_type" => "title"
            );
            $array_settings['blog_articolo'] = array(
                "name" => "Rich Snippet Articolo",
                "input_type" => "checkbox",
                "input_attributes" => array(),
                "helper" => "Inserisce lo snippet nella pagina dell'articolo.",
                "mandatory" => false,
                "translations" => false,
                "default" => true,
            );
        }

        return $array_settings;
    }
}
<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class onexitpopup {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'modules/onexitpopup/js/onexitpopup.js'),
            'css' => array(FRAMEWORK_COMMON_CDN . 'modules/onexitpopup/css/onexitpopup.css')
        );

        return $files_to_include;
    }

    /**
     * Ottiene la lista dei template disponibili
     *
     * @return array
     */
    public function getAvaialableTemplates()
    {
        $templates = array(
            'template_newsletter_1' => '[Generale] Newsletter 1',
            'ecommerce_salva_carrello' => '[Ecommerce] Salva Carrello',
            'realestate_richiedi_consulenza' => '[Realestate] Richiedi Consulenza'
        );

        return $templates;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {

        $extra_module_settings = $this->modules->getExtraModuleSettings('onexitpopup');

        if($params['action'] == 'getTemplate') {

            // Controlla le varie condizioni per stabilire se il popup deve essere mostrato
            if(isset($_COOKIE['onexitpopup_closed']) && !$this->modules->getSettingValue('onexitpopup', 'force')) return '{}';

            if($extra_module_settings['template'] == 'template_newsletter_1') {

            } else if($extra_module_settings['template'] == 'ecommerce_salva_carrello') {
                // Mostriamo solo se l'utente non è loggato e ha prodotti nel carrello
                $products_into_cart = (new \MSFramework\Ecommerce\cart())->getCart();

                if( (new \MSFramework\Ecommerce\customers())->getUserDataFromSession() || !$products_into_cart['products']) return '{}';
            }

            // Ottengo il codice del template
            $template_content = file_get_contents(FRAMEWORK_ABSOLUTE_PATH . 'common/modules/onexitpopup/templates/' . $extra_module_settings['template'] . '.html');

            // Sostituisco i vari shortcodes con le impostazioni del template
            $template_content = preg_replace_callback(array(
                "/{([a-z|A-Z|_|0-9]+)}/i"
            ), function (array $matches) use ($params) {
                return $this->modules->getSettingValue('onexitpopup',  $matches[1]);

            }, $template_content);

            // Sostituisco gli shortcode particolari in base al template
            if($extra_module_settings['template'] == 'ecommerce_salva_carrello') {

                $products_images = array();
                foreach($products_into_cart['products'] as $product) {
                    $products_images[] = '<img src="' . UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML . $product['image'] . '">';
                }

                $template_content = str_replace(array('[carrello]'), array(implode('', $products_images)), $template_content);

            }
            else
            {
                $template_content = str_replace(array('[bg_image]'), array(UPLOAD_MODULES_FOR_DOMAIN_HTML . $this->modules->getSettingValue('onexitpopup', 'bg_image')[0]), $template_content);
                $template_content = str_replace(array('[form]'), (new \MSFramework\forms())->composeHTMLForm($this->modules->getSettingValue('onexitpopup', 'form'),
                    array(
                        'templates' => array(
                            'form' => '<div class="row">{form}</div>',
                            'input' => '<div class="{classes}"><label>{label}:</label>{input}</div>',
                            'checkbox' => '<div class="{classes}"><label>{label}:</label>{input}</div>',
                            'radio' => array(
                                'group' => '<div class="{classes}"><label>{label}:</label>{input}</div>',
                                'single' =>  '{radio} {label}'
                            ),
                            'textarea' => '<div class="{classes}"><label>{label}:</label>{input}</div>',
                            'select' => '<div class="{classes}"><label>{label}:</label>{input}</div>',
                            'button' => '<div class="col-sm-12">{button}</div>'
                        ),
                        'classes' => array(
                            'input' => 'input-text',
                            'textarea' => 'input-textarea',
                            'button' => 'awe-btn awe-btn-13'
                        )
                    )
                ), $template_content);
            }

            $array_to_return = array(
                'delay' => $this->modules->getSettingValue('onexitpopup', 'delay'),
                'html_popup' => $template_content
            );

            return json_encode($array_to_return);

        }
        elseif($params['action'] == 'newsletterSubscribe') {

            $newsletter = new \MSFramework\Newsletter\subscribers();

            $email = $params['email'];
            $name = $params['name'];

            $is_subscribed = $newsletter->checkIfAlreadySubscribed($email);
            $newsletter__result = array('status' => false);

            if (!$is_subscribed) {
                $newsletter__result['status'] = $newsletter->subscribe($email, $name);
                if ($newsletter__result['status']) {
                    $newsletter__result['titolo'] = 'Iscrizione effettuata';
                    $newsletter__result['return_message'] = 'Adesso devi confermare la tua email, ti abbiamo inviato un\'email all\'indirizzo indicato da te.';
                } else {
                    $newsletter__result['titolo'] = 'Errore inaspettato';
                    $newsletter__result['return_message'] = 'Si è verificato un errore inaspettato durante l\'iscrizione alla newsletter';
                }
            } else {
                $newsletter__result = array('status' => false, 'titolo' => 'Email già iscritta', 'return_message' => 'L\'indirizzo email digitato risulta già iscritto alla nostra newsletter');
            }

            return json_encode($newsletter__result);
        }

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $extra_module_settings = $this->modules->getExtraModuleSettings('onexitpopup');

        $array_settings = array(
            "delay" => array(
                "name" => "Attesa Necessaria (ms)",
                "input_type" => "number",
                "input_attributes" => array(),
                "helper" => "Il tempo minimo necessario dopo il caricamento della pagina affinchè il popup possa essere mostrato",
                "mandatory" => false,
                "translations" => false,
                "default" => '5000',
            ),
            "force" => array(
                "name" => "Mostra insistentemente",
                "input_type" => "checkbox",
                "input_attributes" => array(),
                "helper" => "Continua a mostrare il popup nonostante l'utente l'abbia già chiuso (Sconsigliato)",
                "mandatory" => false,
                "translations" => false,
                "default" => false
            )
        );

        if($extra_module_settings['template'] == "template_newsletter_1") {
            $array_settings = array_merge($array_settings,
                array(
                    "titolo" => array(
                        "name" => "Titolo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => true,
                        "default" => 'Iscriviti alla nostra newsletter',
                    ),
                    "testo" => array(
                        "name" => "Testo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => true,
                        "default" => 'Riceverai le nostre migliori offerte',
                    ),
                    "sfondo" => array(
                        "name" => "Sfondo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '',
                    ),
                    "sfondo_pulsante" => array(
                        "name" => "Colore sfondo pulsante",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '#fe3544',
                    ),
                    "colore_pulsante" => array(
                        "name" => "Colore testo pulsante",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '#ffffff',
                    ),
                    "sfondo_popup" => array(
                        "name" => "Colore di sfondo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => 'rgba(0,123,255,.5)',
                    ),
                    "bg_image" => array(
                        "name" => "Immagine di Sfondo",
                        "input_type" => "image",
                        "uploader_settings" => array("max_uploads" => "1", "min_width" => "250", "min_height" => "250", "tn_width" => "100"),
                        "input_attributes" => array(),
                        "mandatory" => false,
                        "translations" => false,
                        "default" => false
                    )
                )
            );
        } else if($extra_module_settings['template'] == "ecommerce_salva_carrello") {
            $array_settings = $array_settings = array_merge($array_settings,
                array(
                    "titolo" => array(
                        "name" => "Titolo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => true,
                        "default" => 'Vuoi abbandonare il tuo carrello?',
                    ),
                    "testo" => array(
                        "name" => "Testo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => true,
                        "default" => 'Abbandonando il sito perderai il tuo carrello!<br>Perchè non registri il tuo account?<br>Ritroverai il tuo carrello in un secondo momento.',
                    ),
                    "pagina_checkout" => array(
                        "name" => "URL Pagina Checkouut",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '/checkout/cassa',
                    ),
                    "pagina_login" => array(
                        "name" => "URL Pagina Login",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '/profilo/login',
                    ),
                    "pagina_registrazione" => array(
                        "name" => "URL Pagina Registrazione",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '/profilo/registrazione',
                    ),
                    "colore_principale" => array(
                        "name" => "Colore Principale",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '#03A9F4',
                    ),
                    "pulsante_login" => array(
                        "name" => "Colore Pulsante Login",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '#03A9F4',
                    ),
                    "pulsante_registrati" => array(
                        "name" => "Colore Pulsante Registrati",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '#8BC34A',
                    )
                )
            );
        } else if($extra_module_settings['template'] == "realestate_richiedi_consulenza") {
            $array_settings = array_merge($array_settings,
                array(
                    "titolo" => array(
                        "name" => "Titolo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => true,
                        "default" => 'Aspetta! Prima di uscire chiedi una consulenza gratuita al nostro Property Finder!',
                    ),
                    "testo" => array(
                        "name" => "Testo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => true,
                        "default" => 'Riceverai le nostre migliori offerte',
                    ),
                    "sfondo" => array(
                        "name" => "Sfondo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '',
                    ),
                    "sfondo_pulsante" => array(
                        "name" => "Colore sfondo pulsante",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '#fe3544',
                    ),
                    "colore_pulsante" => array(
                        "name" => "Colore testo pulsante",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '#ffffff',
                    ),
                    "sfondo_popup" => array(
                        "name" => "Colore di sfondo Popup",
                        "input_type" => "text",
                        "input_attributes" => array(),
                        "helper" => "",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '#ffffff',
                    ),
                    "bg_image" => array(
                        "name" => "Immagine di Sfondo",
                        "input_type" => "image",
                        "uploader_settings" => array("max_uploads" => "1", "min_width" => "250", "min_height" => "250", "tn_width" => "100"),
                        "input_attributes" => array(),
                        "mandatory" => false,
                        "translations" => false,
                        "default" => false
                    ),
                    "form" => array(
                        "name" => "ID del Form",
                        "input_type" => "number",
                        "input_attributes" => array(),
                        "helper" => "L'ID del form da precaricare nel popup (se previsto)",
                        "mandatory" => false,
                        "translations" => false,
                        "default" => '',
                    ),
                )
            );
        }
        
        return $array_settings;
    }
}
<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class errorTracker {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase, $MSFrameworkModules, $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        // Nello sviluppo o ai bot non includo i file
        if(strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/") || (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT']))) {
           return array();
        }

        $files_to_include = array(
            'header' => array(
                'js' => FRAMEWORK_COMMON_CDN . 'modules/error_tracker/js/error_tracker.js'
            ),
            'admin_header' => array(
                'js' => FRAMEWORK_COMMON_CDN . 'modules/error_tracker/js/admin_error_tracker.js'
            )
        );

        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {
        if($params['type'] == 'js') {

            header("Access-Control-Allow-Origin: *");

            if(empty($params['error']) || empty($params['url']) || $params['error'] === 'Script error.' || $params['error'] === 'Script error') {
                return true;
            }

            $is_important_error = false;
            foreach(array('checkout', 'profilo', 'cart', 'profile', 'hooks', 'forms', 'product') as $important_file) {
                if(strpos($params['url'], $important_file) !== false || strpos($_SERVER['HTTP_REFERER'], $important_file) !== false) {
                    $is_important_error = true;
                }
            }

            $array_to_save = array(
                'fw_version' => SW_VERSION,
                'solved' => '0',
                'client_info' => json_encode(
                    array(
                        'user_agent'    => $_SERVER['HTTP_USER_AGENT'],
                        'user_ip'       => $_SERVER['REMOTE_ADDR']
                    )
                ),
                'website' => $this->MSFrameworkCMS->getCleanHost(),
                'error_url' => $params['url'],
                'error_referer' => $_SERVER['HTTP_REFERER'],
                'error_log' => json_encode(
                    array(
                        'error' => $params['error'],
                        'line'  => $params['line'],
                        'col'   => $params['col']
                    )
                )
            );

            // Controllo se lo stesso errore già esiste e nel caso incremento solo la priorità
            $same_error_exist = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.errors__javascript WHERE website = :website AND error_url = :error_url AND error_log = :error_log", array(':website' => $array_to_save['website'], ':error_url' => $array_to_save['error_url'], ':error_log' => $array_to_save['error_log']), true);

            $priority = 1;

            $db_action = 'insert';
            if($same_error_exist) {
                if($same_error_exist['solved'] == "1" && version_compare(SW_VERSION, $same_error_exist['fw_version'], '<=')) { //se la versione che ha generato l'errore è inferiore o uguale a quella che ha restituito l'errore inizialmente (e l'errore è marcato come "risolto") allora ignoro l'aggiornamento della priorità in attesa che venga effettuato l'aggiornamento del FW
                    return true;
                }

                $db_action = 'update';
                $array_to_save['last_error_date'] = date('Y-m-d H:i:s');

                // Se già l'errore è stato segnalato allora incremento la priorità solamente di 0.1
                $increment_value = (isset($_SESSION['error_logs_js']) && in_array($same_error_exist['id'], $_SESSION['error_logs_js']) ? 0.1 : 1);
                $priority = ($same_error_exist['priority']+$increment_value);
                $array_to_save['priority'] = $priority;

            } else if($is_important_error) {
                $array_to_save['priority'] = 100;
                $priority = 100;
            }
            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

            if($db_action == "insert") {
                $this->MSFrameworkDatabase->pushToDB("INSERT INTO marke833_framework.errors__javascript ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
                $error_id = $this->MSFrameworkDatabase->lastInsertId();
            } else {
                $this->MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.errors__javascript SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $same_error_exist['id']), $stringForDB[0]));
                $error_id = $same_error_exist['id'];
            }

            // Salvo l'errore nella sessione utente
            if(!isset($_SESSION['error_logs_js'])) $_SESSION['error_logs_js'] = array();
            $_SESSION['error_logs_js'][] = $error_id;

            // Invio un'email solo per gli errori di priorità 100 oppure se il problema era stato contrassegnato come risolto in precedenza
            if(!FRAMEWORK_DEBUG_MODE && ($priority == 100 || $same_error_exist['solved'] == "1")) {
                (new \MSFramework\emails(true))->sendMail(
                    'error-notification-javascript',
                    array(
                        'id' => $error_id,
                        'priority' => $priority,
                        'website' => $array_to_save['website'],
                        'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                        'error_url' => $array_to_save['error_url'],
                        'error_referer' => $array_to_save['error_referer'],
                        'error_log' => '<b>Errore:</b><br>' . $params['error'] . '<hr><b>Linea:</b><br>' . $params['line'] . '<hr><b>Colonna:</b><br>' . $params['col'] . '<hr><b>File:</b><br>' . $array_to_save['error_url'],
                    )
                );
            }

            /* Se è un errore importante invio una notifica PUSH ai SuperAdmin */
            if($priority == 100) {

                // Invio una notifica push ai superadmin per notificarli dell'apertura del ticket
                (new \MSFramework\pushNotification())
                    ->setAllSuperAdmins()
                    ->sendNotification(
                        '[' . SW_NAME . '] Errore JavaScript Importante.',
                        'Si è verificato un errore JavaScript di priorità ' . $priority . ' che potrebbe compromettere l\'usabilità del sito web.',
                        $this->MSFrameworkCMS->getURLToSite(1) . 'modules/framework360/errors_log/javascript/edit.php?id=' . $error_id
                    );
            }
        }
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function executeFunctions_header()
    {

    }

    /**
     * Salva l'errore relativo all'invio delle email
     *
     * @param $template_id string L'ID del template utilizzato (custom nel caso in cui non utilizza un template)
     * @param $email_subject string L'oggetto dell'email
     * @param $email_sender string L'indirizzo email del mittente
     * @param $email_recipients array Un array con la lista degli indirizzi dei destinatari
     * @param $error_log string I log relativi all'errore
     *
     * @return string
     */
    public function saveEmailErrorLog($template_id, $email_subject, $email_sender, $email_recipients, $error_log)
    {

        if(stristr($_SERVER['REQUEST_URI'], 'sendNewsletter') !== false) {
            return true;
        }

        $array_to_save = array(
            'fw_version' => SW_VERSION,
            'solved' => '0',
            'website' => $this->MSFrameworkCMS->getCleanHost(),
            'email_template_id' => $template_id,
            'email_subject' => $email_subject,
            'email_sender' => $email_sender,
            'email_recipient' => json_encode($email_recipients),
            'error_url' => $_SERVER['REQUEST_URI'],
            'error_log' => $error_log
        );

        // Nello sviluppo non salvo i log
        if(strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/")) {
            \ChromePhp::log($array_to_save);
            return true;
        }

        // Controllo se lo stesso errore già esiste e nel caso incremento solo la priorità
        $same_error_exist = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.errors__emails WHERE website = :website AND error_url = :error_url AND error_log = :error_log", array(':website' => $array_to_save['website'], ':error_url' => $array_to_save['error_url'], ':error_log' => $array_to_save['error_log']), true);

        $db_action = 'insert';
        if($same_error_exist) {
            if($same_error_exist['solved'] == "1" && version_compare(SW_VERSION, $same_error_exist['fw_version'], '<=')) { //se la versione che ha generato l'errore è inferiore o uguale a quella che ha restituito l'errore inizialmente (e l'errore è marcato come "risolto") allora ignoro l'aggiornamento della priorità in attesa che venga effettuato l'aggiornamento del FW
                return true;
            }

            $db_action = 'update';
            $array_to_save['last_error_date'] = date('Y-m-d H:i:s');
        }
        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

        if($db_action == "insert") {
            $this->MSFrameworkDatabase->pushToDB("INSERT INTO marke833_framework.errors__emails ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            $error_id = $this->MSFrameworkDatabase->lastInsertId();
            $priority = 1;
        } else {
            $increment_value = (isset($_SESSION['error_logs_emails']) && in_array($same_error_exist['id'], $_SESSION['error_logs_emails']) ? 0.1 : 1);
            if(!isset($_SESSION['error_logs_emails'])) $_SESSION['error_logs_emails'] = array();
            $_SESSION['error_logs_emails'][] = $same_error_exist['id'];
            $this->MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.errors__emails SET $stringForDB[1], priority = (priority+$increment_value) WHERE id = :id", array_merge(array(":id" => $same_error_exist['id']), $stringForDB[0]));
            $error_id = $same_error_exist['id'];
            $priority = ($same_error_exist['priority']+$increment_value);
        }

        if(!FRAMEWORK_DEBUG_MODE) {
            (new \MSFramework\emails(true))->sendMail(
                'error-notification-email',
                array(
                    'id' => $error_id,
                    'priority' => $priority,
                    'website' => $array_to_save['website'],
                    'error_email_subject' => $array_to_save['email_subject'],
                    'error_email_sender' => $array_to_save['email_sender'],
                    'error_email_recipients' => implode(', ', (json_decode($array_to_save['email_recipient']) ? json_decode($array_to_save['email_recipient'], true) : array('<b class="text-danger">Nessuno!</b>'))),
                    'error_url' => $array_to_save['error_url'],
                    'error_log' => $array_to_save['error_log'],
                )
            );
        }
    }

    /**
     * Salva l'errore relativo al PHP
     *
     * @param $errno integer Contiene il livello dell'errore
     * @param $errstr string La stringa contenente l'errore
     * @param $errfile string Il file contenente l'errore
     * @param $errline string La linea relativa all'errore
     *
     * @return string
     */
    public function savePHPErrorLog($errno, $errstr, $errfile, $errline)
    {

        $errno_strings = array(
            E_ERROR => 'Fatal Error',
            E_USER_ERROR => 'Fatal Error',
            E_DEPRECATED => 'Deprecated',
            //E_WARNING => 'Warning',
            // E_NOTICE  => 'Notice'
        );

        if(!in_array($errno, array_keys($errno_strings))) {
            return true;
        }

        $error_log = array(
            'error' => '[' . $errno_strings[$errno] . '] ' . $errstr,
            'line' => $errline,
            'file' => str_replace($_SERVER['DOCUMENT_ROOT'], '', $errfile),
            'version' => PHP_VERSION
        );

        // Nello sviluppo non salvo i log
        if(strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/")) {
            \ChromePhp::log($error_log);
            return true;
        }

        $array_to_save = array(
            'fw_version' => SW_VERSION,
            'solved' => '0',
            'website' => $this->MSFrameworkCMS->getCleanHost(),
            'error_url' => $errfile,
            'error_referer' =>  (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
            'error_log' => json_encode(
                $error_log
            )
        );

        // Controllo se lo stesso errore già esiste e nel caso incremento solo la priorità
        $same_error_exist = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.errors__php WHERE website = :website AND error_url = :error_url AND error_log = :error_log", array(':website' => $array_to_save['website'], ':error_url' => $array_to_save['error_url'], ':error_log' => $array_to_save['error_log']), true);

        $db_action = 'insert';
        if($same_error_exist) {
            if($same_error_exist['solved'] == "1" && version_compare(SW_VERSION, $same_error_exist['fw_version'], '<=')) { //se la versione che ha generato l'errore è inferiore o uguale a quella che ha restituito l'errore inizialmente (e l'errore è marcato come "risolto") allora ignoro l'aggiornamento della priorità in attesa che venga effettuato l'aggiornamento del FW
                return true;
            }

            $db_action = 'update';
            $array_to_save['last_error_date'] = date('Y-m-d H:i:s');
        }
        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

        if($db_action == "insert") {
            $this->MSFrameworkDatabase->pushToDB("INSERT INTO marke833_framework.errors__php ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            $error_id = $this->MSFrameworkDatabase->lastInsertId();
            $priority = 1;
        } else {
            $increment_value = (isset($_SESSION['error_logs_php']) && in_array($same_error_exist['id'], $_SESSION['error_logs_php']) ? 0.1 : 1);
            if(!isset($_SESSION['error_logs_php'])) $_SESSION['error_logs_php'] = array();
            $_SESSION['error_logs_php'][] = $same_error_exist['id'];
            $this->MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.errors__php SET $stringForDB[1], priority = (priority+$increment_value) WHERE id = :id", array_merge(array(":id" => $same_error_exist['id']), $stringForDB[0]));
            $error_id = $same_error_exist['id'];
            $priority = ($same_error_exist['priority']+$increment_value);
        }

        // Invio l'email solo se è avvenuto un errore oppure se il warning ha priorità maggiore di 10 oppure se il problema era stato contrassegnato come risolto in precedenza
        if(!FRAMEWORK_DEBUG_MODE && ($errno == E_USER_ERROR || $priority > 10 || $same_error_exist['solved'] == "1")) {
            (new \MSFramework\emails(true))->sendMail(
                'error-notification-php',
                array(
                    'id' => $error_id,
                    'priority' => $priority,
                    'website' => $array_to_save['website'],
                    'error_url' => $array_to_save['error_url'],
                    'error_referer' => $array_to_save['error_referer'],
                    'error_type' => $errno_strings[$errno],
                    'error_log' => '<b>Errore:</b><br>' . $error_log['error'] . '<hr><b>Linea:</b><br>' . $error_log['line'] . '<hr><b>File:</b><br>' . $array_to_save['error_url'],
                )
            );
        }

        return true;
    }

    /**
     * Salva l'errore relativo alla SQL
     *
     * @param $exception_details array Contiene i dettagli dell'errore
     *
     * @return string
     */
    public function saveSQLErrorLog($exception_details)
    {

        // Nello sviluppo non salvo i log
        if(strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/") || strstr($exception_details['exception_message'], '1146 Table')) {
            \ChromePhp::log($exception_details);
            return true;
        }

        $error_log = array(
            'error' => $exception_details['error_info'][2],
            'exception' => $exception_details['exception_message'],
            'query' => $exception_details['query']
        );

        //Se l'errore proviene da un aggiornamento dell'updater, non lo segnalo. Questi errori hanno log separati e verificati immediatamente.
        if(stristr($_SERVER["SCRIPT_FILENAME"], "frameworkUpdates")) {
            return true;
        }

        $array_to_save = array(
            'fw_version' => SW_VERSION,
            'solved' => '0',
            'website' => $this->MSFrameworkCMS->getCleanHost(),
            'error_url' => $_SERVER["SCRIPT_FILENAME"],
            'error_referer' =>  (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
            'error_log' => json_encode(
                $error_log
            )
        );

        // Controllo se lo stesso errore già esiste e nel caso incremento solo la priorità
        $same_error_exist = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.errors__sql WHERE website = :website AND error_log LIKE :error_log", array(':website' => $array_to_save['website'], ':error_log' => '{"error":"' . $error_log['error'] . '%'), true);

        $db_action = 'insert';
        if($same_error_exist) {
            if($same_error_exist['solved'] == "1" && version_compare(SW_VERSION, $same_error_exist['fw_version'], '<=')) { //se la versione che ha generato l'errore è inferiore o uguale a quella che ha restituito l'errore inizialmente (e l'errore è marcato come "risolto") allora ignoro l'aggiornamento della priorità in attesa che venga effettuato l'aggiornamento del FW
                return true;
            }

            $db_action = 'update';
            $array_to_save['last_error_date'] = date('Y-m-d H:i:s');
        }
        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

        if($db_action == "insert") {
            $this->MSFrameworkDatabase->pushToDB("INSERT INTO marke833_framework.errors__sql ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            $error_id = $this->MSFrameworkDatabase->lastInsertId();
            $priority = 1;
        } else {
            $increment_value = (isset($_SESSION['error_logs_sql']) && in_array($same_error_exist['id'], $_SESSION['error_logs_sql']) ? 0.1 : 1);
            if(!isset($_SESSION['error_logs_sql'])) $_SESSION['error_logs_sql'] = array();
            $_SESSION['error_logs_sql'][] = $same_error_exist['id'];
            $this->MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.errors__sql SET $stringForDB[1], priority = (priority+$increment_value) WHERE id = :id", array_merge(array(":id" => $same_error_exist['id']), $stringForDB[0]));
            $error_id = $same_error_exist['id'];
            $priority = ($same_error_exist['priority']+$increment_value);
        }

        if(!FRAMEWORK_DEBUG_MODE && (($same_error_exist['priority'] < 10 && (int)$priority == 5) || $same_error_exist['solved'] == "1")) {
            (new \MSFramework\emails(true))->sendMail(
                'error-notification-sql',
                array(
                    'id' => $error_id,
                    'priority' => $priority,
                    'website' => $array_to_save['website'],
                    'error_url' => $array_to_save['error_url'],
                    'error_referer' => $array_to_save['error_referer'],
                    'error_log' => '<b>SQL:</b><br>' . $error_log['query'] . '<hr><b>Errore:</b><br>' . $error_log['error'] . '<hr><b>Exception:</b><br>' . $error_log['exception'] . '<hr><b>File:</b><br>' . $array_to_save['error_url'],
                )
            );
        }

        return true;
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $array_settings = array();
        return $array_settings;
    }

    /**
     * Contrassegna uno o più ID di errori come risolti
     *
     * @param $ids Un array di ID di errori
     * @param $type La tipologia di errori (php|sql|javascript|emails)
     *
     * @return bool
     */
    public function markAsSolved($ids, $type) {
        if($ids == "" || $type == "") {
            return false;
        }

        if(!is_array($ids)) {
            $ids = array($ids);
        }

        $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($ids, "OR", "id");

        return $this->MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.errors__" . $type . " SET solved = '1' WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);
    }
}
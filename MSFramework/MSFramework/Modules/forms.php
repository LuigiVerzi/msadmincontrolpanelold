<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class forms
{
    public function __construct()
    {
        global $firephp, $MSFrameworkDatabase, $MSFrameworki18n, $MSFrameworkCMS, $MSFrameworkModules;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworki18n = $MSFrameworki18n;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkUsers = new \MSFramework\users();

        $this->producer_config = $this->MSFrameworkCMS->getCMSData('producer_config');

        $this->modules = $MSFrameworkModules;
        $this->forms = new \MSFramework\forms();
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'modules/forms/js/forms.js')
        );

        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     */
    public function ajaxCall($params, $slug = array())
    {
        global $firephp, $MSFrameworki18n;

        if ($params['action'] == 'formSubmit') {

            $form_id = $params['id'];
            $submit_data = $params['form_data'];
            $form_settings = $params['form_settings'];

            if(isset($params['lang_id']) && $params['lang_id']) {
                $this->MSFrameworki18n->user_selected_lang = $params['lang_id'];
            }

            // Faccio un escape di tutti i dati inviati dal cliente
            // foreach($submit_data as $data_k => $data_v) $submit_data[$data_k] = htmlentities($data_v);

            $form = $this->forms->getFormDetails($form_id)[$form_id];
            $fields = json_decode($form['fields'], true);
            $actions = json_decode($form['actions'], true);
            $actions_value = json_decode($form['actions_values'], true);

            $default_actions_info = $this->forms->getFormActionsInfo();

            // Formatto le date nel formato italiano
            foreach($submit_data as $k => $d) {
                foreach ($fields as $field) {
                    if ($field['field_id'] == $k && $field['input_type'] == 'date') {
                        $submit_data[$k] = date('d/m/Y', strtotime($d));
                        $submit_data['original_' . $k] = $d;
                    }
                }
            }

            // La variabile che conterrà l'indirizzo email dell'utente
            // Cercherò di ottenerlo tramite le eventuali azioni settate di seguito
            $user_store_email = '';

            $results = array();
            $implicit_messages = array();
            foreach ($actions as $action) {

                // Se si è verificato un errore blocco l'esecuzione degli altri eventi
                foreach($implicit_messages as $im) {
                    if(!$im['status']) {
                        continue;
                    }
                }

                $result = false;

                $action_value = $actions_value[$action];

                if ($action == 'anagrafica') {
                    $customers = new \MSFramework\customers();

                    /* DATI GENERALI */
                    $nome = $submit_data[$action_value['nome']];
                    $cognome = '';

                    if(!empty($action_value['cognome'])) {
                        $cognome = $submit_data[$action_value['cognome']];
                    } else {
                        // Provo a dividere il nome se è stato passato un solo campo
                        $name_exploded = explode(' ', $submit_data[$action_value['nome']]);

                        if(count($name_exploded) > 1) {
                            $cognome = end($name_exploded);
                            $nome = str_replace(' ' . $cognome, '', $nome);
                        }
                    }

                    $email = $submit_data[$action_value['email']];
                    $cellulare = $submit_data[$action_value['telefono_cellulare']];

                   /* DATI SECONDARI */
                    $secondary_info = array(
                        'indirizzo' => (!empty($action_value['indirizzo']) ? $submit_data[$action_value['indirizzo']] : ''),
                        'sito_web' => (!empty($action_value['sito_web']) ? $submit_data[$action_value['sito_web']] : ''),
                        'data_nascita' => (!empty($action_value['data_nascita']) ? strtotime($submit_data['original_' . $action_value['data_nascita']]) : ''),
                        'stato' => (!empty($action_value['stato']) ? $submit_data[$action_value['stato']] : ''),
                        'regione' => (!empty($action_value['regione']) ? $submit_data[$action_value['regione']] : ''),
                        'citta' => (!empty($action_value['citta']) ? $submit_data[$action_value['citta']] : ''),
                        'provincia' => (!empty($action_value['provincia']) ? $submit_data[$action_value['provincia']] : ''),
                        'cap' => (!empty($action_value['cap']) ? $submit_data[$action_value['cap']] : ''),
                    );

                    $dati_fatturazione = array();
                    if(!empty($action_value['ragione_sociale'])) $dati_fatturazione['ragione_sociale'] = $submit_data[$action_value['ragione_sociale']];
                    if(!empty($action_value['piva'])) $dati_fatturazione['piva'] = $submit_data[$action_value['piva']];
                    if(!empty($action_value['cf'])) $dati_fatturazione['cf'] = $submit_data[$action_value['cf']];

                    if(!empty($action_value['indirizzo_azienda'])) $dati_fatturazione['indirizzo'] = $submit_data[$action_value['indirizzo_azienda']];
                    if(!empty($action_value['stato_azienda'])) $dati_fatturazione['stato'] = $submit_data[$action_value['stato_azienda']];
                    if(!empty($action_value['regione_azienda'])) $dati_fatturazione['regione'] = $submit_data[$action_value['regione_azienda']];
                    if(!empty($action_value['citta_azienda'])) $dati_fatturazione['citta'] = $submit_data[$action_value['citta_azienda']];
                    if(!empty($action_value['provincia_azienda'])) $dati_fatturazione['provincia'] = $submit_data[$action_value['provincia_azienda']];
                    if(!empty($action_value['cap_azienda'])) $dati_fatturazione['cap'] = $submit_data[$action_value['cap_azienda']];

                    if($dati_fatturazione) {
                        $secondary_info['dati_fatturazione'] = $dati_fatturazione;
                    }

                    $result = array('status' => 1);

                    /* DATI DI ACCESSO */
                    if (isset($action_value['enable_login']) && $action_value['enable_login'] == "1") {
                        $password = $submit_data[$action_value['password']];
                        if($customers->checkPasswordStrength($password) != '') {
                            $result['status'] = array(0, 'password_strength');
                        } else if(!empty($action_value['ripeti_password']) && $submit_data[$action_value['ripeti_password']] !== $password) {
                            $result['status'] = array(0, 'password_match');
                        } else if($customers->checkIfMailExists($email)) {
                            $result['status'] = array(0, 'email_exists');
                        }
                    } else {
                        $password = '';
                    }

                    // Indico l'email del cliente che verrà successivamente utilizzata per salvare la richiesta nella cronologia
                    $user_store_email = $email;
                    if(!is_array($result['status'])) {
                        $user_id = $customers->registerUser($nome, $cognome, $email, $password, $cellulare, 3, $secondary_info);
                        $result['status'] = ($user_id > 0 ? 1 : 0);
                    }

                    if($result['status'] && !empty($password)) {
                        // Invia l'email di convalida account
                        $user = $customers->getCustomerDataFromDB($user_id);

                        $activation_code = $user['mail_auth'];
                        $email_address = $user['email'];

                        $return_array['status'] = (new \MSFramework\emails())->sendMail('account-activation', array(
                            'nome' => (!empty($user['nome']) ? $user['nome'] . ' ' . $user['cognome'] : $user['email']),
                            'email' => $email_address,
                            'code' => $activation_code
                        ));

                        if($action_value['auto_login'] == '1') {
                            $customers->checkCredentials($email, $password);
                        }
                    }

                }

                if ($action == 'newsletter_subscribe' && (!$action_value['checkbox_allow_subscribe'] || isset($submit_data[$action_value['checkbox_allow_subscribe']]))) {
                    $newsletter = new \MSFramework\Newsletter\subscribers();

                    $nome = $submit_data[$action_value['name']];

                    // Se il campo cognome è linkato aggiungo il cognome al nome
                    if(isset($action_value['surname']) && !empty($action_value['surname'])) {
                        $nome .= ' ' . $submit_data[$action_value['surname']];
                    }

                    $email = $submit_data[$action_value['email']];
                    $tag = ($action_value['tag'] ? $action_value['tag'] : array());

                    // Indico l'email del cliente che verrà successivamente utilizzata per salvare la richiesta nella cronologia
                    $user_store_email = $email;

                    // Se ci sono tag aggiuntivi li aggiunge
                    if ($action_value['additional_tag']) {
                        foreach ($action_value['additional_tag'] as $c_tag) {
                            array_push($tag, $submit_data[$c_tag]);
                        }
                    }

                    $already_subscribed = $newsletter->checkIfAlreadySubscribed($email);

                    $result = array(
                        'status' => $newsletter->subscribeNewEmail($nome, $email, $tag)
                    );

                    if ($already_subscribed) {
                        $result = array(
                            'status' => 2
                        );
                    }
                }

                if ($action == 'reply_to_mail') {
                    $email = $submit_data[$action_value['email']];

                    // Indico l'email del cliente che verrà successivamente utilizzata per salvare la richiesta nella cronologia
                    $user_store_email = $email;
                    
                    $oggetto = $this->MSFrameworki18n->getFieldValue($action_value['subject']);
                    $reply_to_address = $action_value['reply_to_addr'];
                    $reply_to_name = $action_value['reply_to_name'];

                    $custom_txt = $this->MSFrameworki18n->getFieldValue($action_value['custom_txt']);
                    $custom_html = $this->MSFrameworki18n->getFieldValue($action_value['custom_html']);

                    /* Sostituisco gli shortcodes nel contenuto della email */
                    foreach($submit_data as $shortcode => $value) {
                        $custom_txt = str_replace("{" . $shortcode . "}", $value, $custom_txt);
                        $custom_html = str_replace( "{" . $shortcode . "}",  htmlentities($value), $custom_html);
                    }

                    $attachments = json_decode($action_value['attachments'], true);

                    foreach ($attachments as $k => $attachment) {
                        $attachments[$k] = UPLOAD_FORMS_FOR_DOMAIN . $attachment;
                    }

                    $reply_to_array = array();
                    if (!empty($reply_to_address) && !empty($reply_to_name)) {
                        $reply_to_array = array('email' => $reply_to_address, 'name' => $reply_to_name);
                    }

                    $result = array(
                        'status' => (new \MSFramework\emails())->sendCustomMail($oggetto, $custom_html, $email, $reply_to_array, $custom_txt, $attachments)
                    );
                }

                if ($action == 'reservation') {
                    $extra_functions_db = json_decode($this->MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true);

                    $additional_array = array();
                    foreach ($action_value['body'] as $body_input) {
                        if(!empty($submit_data[$body_input])) {
                            foreach ($fields as $field) {
                                if ($field['field_id'] == $body_input) {
                                    $additional_array[$this->MSFrameworki18n->getFieldValue($field['name'])] = $submit_data[$body_input];
                                }
                            }
                        }
                    }

                    $use_default_mail = $action_value['default_mail'];
                    $use_custom_mail = ($use_default_mail && isset($action_value['use_custom_mail']) && $action_value['use_custom_mail'] == 1);

                    $params = array(
                        "date" => array(
                            "arrival" => $submit_data[$action_value['date_from']],
                            "departure" => $submit_data[$action_value['date_to']],
                        ),
                        "detail" => array(
                            "name" => $submit_data[$action_value['name']],
                            "surname" => $submit_data[$action_value['surname']],
                            "email" => $submit_data[$action_value['email']],
                            "phone" => $submit_data[$action_value['cell_phone']],
                            "additional" => $additional_array,
                        ),
                        "roommate" => array(
                            "adults" => $submit_data[$action_value['adults']],
                            "children" => $submit_data[$action_value['children']]
                        ),
                        "origin" => (json_decode($submit_data[$action_value['origin']]) ? json_decode($submit_data[$action_value['origin']], true) : array('origin' => 'home'))
                    );

                    $check_booking_field = $this->checkBookingFields($params["origin"], $params["date"], $params["roommate"], $params["detail"]);

                    if ($check_booking_field) {
                        // Formatta i dati da inviare al database
                        $arrival_db = (new \DateTime)::createFromFormat('!d/m/Y', $params["date"]["arrival"])->format('Y-m-d');
                        $departure_db = (new \DateTime)::createFromFormat('!d/m/Y', $params["date"]["departure"])->format('Y-m-d');

                        $array_to_save = array(
                            "nome" => $params["detail"]["name"],
                            "cognome" => $params["detail"]["surname"],
                            "email" => $params["detail"]["email"],
                            "telefono" => $params["detail"]["phone"],
                            "info_aggiuntive" => json_encode($params["detail"]["additional"]),
                            "arrivo" => $arrival_db,
                            "partenza" => $departure_db,
                            "ospiti" => json_encode(array($params["roommate"]["adults"], $params["roommate"]["children"])),
                            "referenza" => json_encode($params["origin"]),
                            "lang" => USING_LANGUAGE_CODE
                        );

                        // Indico l'email del cliente che verrà successivamente utilizzata per salvare la richiesta nella cronologia
                        $user_store_email = $params["detail"]["email"];

                        // Invia i dati ricevuti al database
                        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
                        $status_db = $this->MSFrameworkDatabase->pushToDB("INSERT INTO prenotazioni ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
                        $params['request_id'] = $this->MSFrameworkDatabase->lastInsertId();

                        if ($status_db) {
                            $result = array(
                                "status" => 1
                            );

                            if($use_default_mail) {

                                if($use_custom_mail)
                                {
                                    $custom_html = $this->MSFrameworki18n->getFieldValue($action_value['custom_mail']['custom_html']);
                                    $custom_txt = $this->MSFrameworki18n->getFieldValue($action_value['custom_mail']['custom_txt']);
                                    $custom_subject = $this->MSFrameworki18n->getFieldValue($action_value['custom_mail']['custom_subject']);

                                    $submit_data['request_url'] = $this->MSFrameworkCMS->getURLToSite(1) . 'modules/richieste/prenotazioni/edit.php?id=' . $params['request_id'];

                                    /* Sostituisco gli shortcodes nel contenuto della email */
                                    foreach($submit_data as $shortcode => $value) {
                                        $custom_txt = str_replace("{" . $shortcode . "}", $value, $custom_txt);
                                        $custom_html = str_replace( "{" . $shortcode . "}", $value, $custom_html);
                                        $custom_subject = str_replace( "{" . $shortcode . "}", $value, $custom_subject);
                                    }

                                    (new \MSFramework\emails())->sendCustomMail($custom_subject, $custom_html, $this->MSFrameworkCMS->getCMSData('settings')['destination_address'], array('email' => $array_to_save['email'], 'name' => $array_to_save['nome'] . ' ' . $array_to_save['cognome']), $custom_txt);

                                }
                                else
                                {

                                    if (in_array("camping", $extra_functions_db)) {
                                        (new \MSFramework\emails())->sendMail('camping_booking_request', $params);
                                    }
                                    if (in_array("hotel", $extra_functions_db)) {
                                        (new \MSFramework\emails())->sendMail('hotel_booking_request', $params);
                                    }
                                }

                            }

                        } else {
                            $result = array(
                                "status" => 0
                            );
                        }
                    } else {
                        $result = array(
                            "status" => 0
                        );
                    }
                }

                if ($action == 'generic_request' || $action == 'property_info') {
                    $form_type = "contact-form";
                    if($action == "property_info") {
                        $form_type = "properties-questions/new-question";
                    }

                    $full_name = $submit_data[$action_value['name']] . (!empty($action_value['surname']) ? ' ' . $submit_data[$action_value['surname']] : '');
                    $email = $submit_data[$action_value['email']];
                    $subject = $submit_data[$action_value['subject']];
                    $body = $submit_data[$action_value['body']];

                    // Indico l'email del cliente che verrà successivamente utilizzata per salvare la richiesta nella cronologia
                    $user_store_email = $email;

                    $additional_info_string = "";
                    $additional_info_array = array();
                    $k = 0;
                    foreach ($action_value['additional_info'] as $extra_input) {
                        $field_name = "";
                        foreach ($fields as $field) {
                            if ($field['field_id'] == $extra_input) {
                                $field_name = $this->MSFrameworki18n->getFieldValue($field['name']);
                            }
                        }
                        $additional_info_array[$field_name] = $submit_data[$extra_input];
                        $additional_info_string .= '<br><br><b>' . $field_name .  '</b><br>' . htmlentities($submit_data[$extra_input]);
                        $k++;
                    }

                    $use_default_mail = $action_value['default_mail'];
                    $use_custom_mail = ($use_default_mail && isset($action_value['use_custom_mail']) && $action_value['use_custom_mail'] == 1);

                    $dest_mail = "";
                    if(isset($action_value['custom_recipient']) && $action_value['custom_recipient']) {
                        $dest_mail = $action_value['custom_recipient'];
                    } else if(isset($form_settings['send_to_user_id']) && $form_settings['send_to_user_id'] != "") {
                        $dest_mail = $this->MSFrameworkUsers->getUserDataFromDB($form_settings['send_to_user_id'], 'email')['email'];
                    } else if(isset($form_settings['send_to_user_mail']) && $form_settings['send_to_user_mail'] != "") {
                        $dest_mail = $form_settings['send_to_user_mail'];
                    }

                    if(count(explode(',', $dest_mail)) > 1) {
                        $dest_mail = explode(',', $dest_mail);
                    }

                    if(!is_array($dest_mail)) $dest_mail = array($dest_mail);

                    foreach($dest_mail as $ek => $em) {
                        if(!filter_var($em, FILTER_VALIDATE_EMAIL)) unset($dest_mail[$ek]);
                    }

                    if(!$dest_mail) {
                        $dest_mail = $this->MSFrameworkCMS->getCMSData('settings')['destination_address'];
                    } else {
                        $dest_mail = implode(',', $dest_mail);
                    }


                    $email_status = true;

                    if($use_default_mail) {

                        if($use_custom_mail)
                        {

                            $custom_html = $this->MSFrameworki18n->getFieldValue($action_value['custom_mail']['custom_html']);
                            $custom_txt = $this->MSFrameworki18n->getFieldValue($action_value['custom_mail']['custom_txt']);
                            $custom_subject = $this->MSFrameworki18n->getFieldValue($action_value['custom_mail']['custom_subject']);

                            /* Sostituisco gli shortcodes nel contenuto della email */
                            foreach($submit_data as $shortcode => $value) {
                                $custom_txt = str_replace("{" . $shortcode . "}", htmlentities($value), $custom_txt);
                                $custom_html = str_replace( "{" . $shortcode . "}", htmlentities($value), $custom_html);
                                $custom_subject = str_replace( "{" . $shortcode . "}", htmlentities($value), $custom_subject);
                            }

                            $mailClass = new \MSFramework\emails();
                            if(strlen($action_value['custom_sender_name']) > 2) $mailClass->mail->setFrom($mailClass->noReplyInfo['mail'], $action_value['custom_sender_name']);
                            else $mailClass->mail->setFrom($mailClass->noReplyInfo['mail'], $full_name . ' tramite ' . SW_NAME);

                            $email_status = $mailClass->sendCustomMail($custom_subject, $custom_html, $dest_mail, array('email' => $email, 'name' => $full_name), $custom_txt);

                        }
                        else
                        {
                            $ary_params = array(
                                'name' => htmlentities($full_name),
                                'email' => htmlentities($email),
                                'message' => htmlentities($body) . (!empty($additional_info_string) ? $additional_info_string : ''),
                                'subject' => $subject,
                                'dest' => $dest_mail
                            );

                            if($action == "property_info") {
                                $MSFrameworkImmobili = new \MSFramework\RealEstate\immobili();
                                $ary_params['immobile'] = $MSFrameworki18n->getFieldValue($MSFrameworkImmobili->getImmobileDataFromDB($form_settings['property_id'], "titolo")['titolo']) . " ({site-url}" . $MSFrameworkImmobili->dettaglioImmobileURL($form_settings['property_id']) . ")";
                            }

                            $email_status = (new \MSFramework\emails())->sendMail($form_type, $ary_params);
                        }
                    }

                    if ($email_status) {
                        $request_type = "generic";
                        $request_owner = "";

                        if($action == 'property_info') {
                            $request_type = "property";
                        }

                        $this->forms->addGenericRequest($full_name, $email, $body, $additional_info_array, $this->MSFrameworki18n->user_selected_lang, $request_type, $form_settings['send_to_user_id'], $form_settings);

                        $result = array(
                            "status" => 1
                        );
                    } else {
                        $result = array(
                            "status" => 0
                        );
                    }
                }

                if ($action == 'login') {
                    $customers = new \MSFramework\customers();

                    /* DATI GENERALI */
                    $email = $submit_data[$action_value['email']];
                    $password = $submit_data[$action_value['password']];
                    $rimani_connesso = $submit_data[$action_value['rimani_connesso']];

                    // Indico l'email del cliente che verrà successivamente utilizzata per salvare la richiesta nella cronologia
                    $user_store_email = $email;

                    $result = array('status' => 1);
                    if(strlen($password) < 6) {
                        $result = array('status' => 0);
                    } else {
                        $result['status'] = $customers->checkCredentials($email, $password);
                    }
                }

                if ($action == 'password_recovery') {
                    $customers = new \MSFramework\customers();

                    /* DATI GENERALI */
                    $email = $submit_data[$action_value['email']];

                    // Indico l'email del cliente che verrà successivamente utilizzata per salvare la richiesta nella cronologia
                    $user_store_email = $email;

                    $result = array('status' => 1);
                    if($customers->checkIfMailExists($email)) {
                        $customers->generatePassResetToken($email);
                        if(!(new \MSFramework\emails())->sendMail('pass-reset', array("email" => $email, 'origin' => 'customer'))) {
                            $result = array('status' => 0);
                        }
                    } else {
                        $result = array('status' => 0);
                    }
                }

                if(is_array($result)) {
                    if(is_array($result['status'])) {
                        $status_text_id = (int)$result['status'][0];
                        $status_text_specific = $result['status'][1];
                        $result['status'] = $status_text_id;
                    } else {
                        $result['status'] = (int)$result['status'];
                        $status_text_id = (int)$result['status'];
                        $status_text_specific = '';
                    }

                    if(is_numeric($status_text_id)) {
                        $status_text_id = 'success';
                        if ((int)$result['status'] == 2) {
                            $status_text_id = 'warning';
                        } else if ((int)$result['status'] == 0) {
                            $status_text_id = 'danger';
                        }
                    }

                    $template = $default_actions_info[$action]['return_message']['template'];

                    $implicit = false;

                    if (isset($action_value['messages']) && is_array($action_value['messages'])) {

                        $full_status_id = $status_text_id . ($status_text_specific ? '_' . $status_text_specific : '');

                        $result['titolo'] = $this->MSFrameworki18n->getFieldValue($action_value['messages'][$full_status_id]['titolo']);
                        $result['message'] = $this->MSFrameworki18n->getFieldValue($action_value['messages'][$full_status_id]['message']);

                        if (isset($action_value['messages'][$full_status_id]['template']) && is_array($action_value['messages'][$full_status_id]['template'])) {
                            $template = $action_value['messages'][$full_status_id]['template'];
                        }

                        $implicit = true;
                    } else if (isset($default_actions_info[$action]['return_message']) && $default_actions_info[$action]['return_message']['show']) {

                        $message_array = $default_actions_info[$action]['return_message']['strings'][$status_text_id];

                        if(!isset($message_array['label'])) {
                            if($status_text_specific) {
                                $message_array =  $message_array[$status_text_specific];
                            } else {
                                $message_array = array_values($message_array)[0];
                            }
                        }

                        $result['titolo'] = $message_array['titolo'];
                        $result['message'] = $message_array['message'];
                    }

                    $result['html'] = str_replace(
                        array('[warning|danger|success]', '[titolo]', '[messaggio]'),
                        array($status_text_id, $result['titolo'], $result['message']),
                        implode('', $template)
                    );

                    if ($implicit) {
                        $implicit_messages[$action] = $result;
                    }

                    $results[$action] = $result;
                }

            }

            /* Se esiste la classe /MSSoftware/forms() nel front-end allora vado a cercare un'eventuale evento onSubmit */
            if(class_exists("MSSoftware\\forms")) {
                $MSSoftwareForms = new \MSSoftware\forms();

                if(method_exists($MSSoftwareForms,'onFormSubmit')) {
                    $MSSoftwareForms->onFormSubmit($form, $submit_data);
                }

            }

            /* Se è stato specificato almeno un messaggio di ritorno in modo manuale
            allora non mostro quelli predefiniti perchè non esplicitamente voluti */
            if(count($implicit_messages) > 0) {
                $results = $implicit_messages;
            }

            if(empty($user_store_email)) {
                $user_store_email = (new \MSFramework\customers())->getUserDataFromSession();
                if(isset($user_store_email['user_id'])) $user_store_email = $user_store_email['user_id'];
            }


            $this->forms->storeFormSubmission($form_id, $user_store_email, $fields, $submit_data, $results,  $this->MSFrameworki18n->user_selected_lang);

            $json = json_encode($results);
            die($json);
        }
        if ($params['action'] == 'generateQuote') {
            $quote = $this->forms->calculateQuote($params['id'], $params['form_data'], $params['lang']);
            die((string)$quote);
        }
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings()
    {
        $array_settings = array();
        return $array_settings;
    }

    /**
     * Controlla la validità dei dati ricevuti
     *
     * @param array $origin Contiene l'origine della richiesta.
     * @param array $date Contiene la data di arrivo e quella di partenza
     * @param array $roommate Contiene il numero di inquilini richiedenti (Bambini & Adulti separati)
     * @param array $detail Contiene i dettagli del cliente che vuole effettuare la prenotazione
     *
     * @return boolean
     */
    public function checkBookingFields($origin, $date, $roommate, $detail = array())
    {
        $status = true;

        // Controlla l'esistenza dei campi base necessari
        if (!isset($origin['origin']) || !isset($date['arrival']) || !isset($date['departure']) || !isset($roommate['children']) || !isset($roommate['adults'])) return false;

        // Controlla alcuni parametri logici necessari (Se ci sia almeno un adulto e se la data di arrivo sia antecedente alla partenza)
        if ($roommate['adults'] < 0 || !(new \DateTime)::createFromFormat('!d/m/Y', $date['arrival']) || !(new \DateTime)::createFromFormat('!d/m/Y', $date['departure']) || (new \DateTime)::createFromFormat('!d/m/Y', $date['arrival'])->getTimestamp() >= (new \DateTime)::createFromFormat('!d/m/Y', $date['departure'])->getTimestamp()) return false;

        // Se ci sono i campi finali relativi all'utente controlla anche quelli
        if ($detail) {
            //$phone_regex = '/^(\+|\d)[0-9]{7,16}$/';
            //if (!preg_match($phone_regex, $detail['phone']) || !filter_var($detail['email'], FILTER_VALIDATE_EMAIL)) return false;
        }

        return $status;
    }
}
<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class trackingSuite {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules, $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->modules = $MSFrameworkModules;
        $this->trackingSuite = new \MSFramework\tracking();
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {

        $files_to_include = array();
        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params, $slug = array())
    {

        header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);

        if($params)
        {
            if(isset($params['project_id'])) {
                $this->trackingSuite->saveTrackDetails($params);
            }
        }
        else if(count($slug) && strpos($slug[0], '.js') !== false) {

            $tracking_id = str_replace('.js', '', $slug[0]);

            if(is_numeric($tracking_id)) {
                $current_website_project_ids = $this->trackingSuite->getTrackingProjectIDsByWebsite($_SERVER['HTTP_REFERER']);

                if(in_array($tracking_id, $current_website_project_ids)) {
                    echo $this->trackingSuite->generateTrackingJavascript($tracking_id);
                }
            }

        }
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function executeFunctions_header()
    {

        if(isset($_GET['tsuite'])) {

           echo "<script>document.domain = '" . $this->MSFrameworkCMS->getCleanHost() . "';</script>";

        }

    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function executeFunctions_footer()
    {
        // Carico il codice di tracking se trovo un progetto attivo
        $tracking_id = $this->trackingSuite->getTrackingProjectIDsByWebsite();

        if($tracking_id) {

            echo (new \MSFramework\Modules\gdpr())->injectJS(array(
                "type" => "statistiche",
                "load" => array(
                    array(
                        "inject" => ABSOLUTE_SW_PATH_HTML . $this->modules->getShortPrefix() . '/trackingsuite/' . $tracking_id[0] . '.js',
                        "in-tag" => false
                    )
                )
            ));

        }

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $array_settings = array();
        return $array_settings;
    }
}
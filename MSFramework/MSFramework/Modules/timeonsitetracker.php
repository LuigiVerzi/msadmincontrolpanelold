<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class timeonsitetracker {
    public function __construct() {
        global $firephp;

        $this->MSFrameworkStats = new \MSFramework\stats();
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'modules/timeonsitetracker/js/timeonsitetracker.js')
        );

        return $files_to_include;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function executeFunctions_footer()
    {
        Global $MSFrameworkUrl;

        $ref = array();

        if($MSFrameworkUrl->currentSlugDetails && (new \MSFramework\stats())->actionTables[$MSFrameworkUrl->currentSlugDetails['action']]) {
            $ref = array(
                'type' => $MSFrameworkUrl->currentSlugDetails['action'],
                'id' => $MSFrameworkUrl->currentSlugDetails['id']
            );
        }

        return $this->MSFrameworkStats->saveVisitorVisit($ref);
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {

        if($params['action'] == 'saveTimeSpent') {

            $time = $params['time'];
            if($time < 600) {
                $this->MSFrameworkStats->saveVisitorNavigationTime($time, $params['reference']);
            }

        }

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $array_settings = array();
        return $array_settings;
    }
}
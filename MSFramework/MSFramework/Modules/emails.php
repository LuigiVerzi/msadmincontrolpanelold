<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class emails {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules, $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;

        
        $this->producer_config = $MSFrameworkCMS->getCMSData("producer_config");
        
        $this->recaptcha_is_active = (trim($this->producer_config['recaptcha_public']) != "" && trim($this->producer_config['recaptcha_private']) != "");
        $this->recaptcha_is_invisible = ($this->producer_config['recaptcha_invisible'] == 1);
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array(
            'js' => array(
                FRAMEWORK_COMMON_CDN . 'modules/emails/js/emails.js',
                FRAMEWORK_COMMON_CDN . 'js/lib/polyfill.js',
                FRAMEWORK_COMMON_CDN . 'js/lib/core-js.js',
                FRAMEWORK_COMMON_CDN . 'js/lib/sweetalert.min.js'
            )
        );

        if($this->recaptcha_is_active) {
            if($this->recaptcha_is_invisible) {
                $files_to_include['js'][] = 'https://www.google.com/recaptcha/api.js?onload=onloadInvisibleRecaptcha';
                $files_to_include['js'][] = FRAMEWORK_COMMON_CDN . 'modules/emails/js/invisible_recaptcha.js';
            }
            else {
                $files_to_include['js'][] = 'https://www.google.com/recaptcha/api.js';
            }
        }

        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {

        if($params['action'] == 'contact-form') {

            if($params['subject'] == "") {
                $params['subject'] = (new \MSFramework\i18n())->gettext("Contatto tramite il form del sito");
            }

            if($params['name'] == "" || $params['email'] == "" || (!is_array($params['message']) && $params['message'] == "") ) {
                return json_encode(array(
                    "result" => "mandatory_data_missing",
                    "return_message" => (new \MSFramework\i18n())->gettext("Compila correttamente tutti i campi!")
                ));
            }

            if(is_array($params['message'])) {
                $message_array = array();
                foreach($params['message'] as $k=>$extra_message) {
                    $message_array[] = $k . ': ' . $extra_message;
                }
                $params['message'] = implode('<br><br>', $message_array);
            }

            if(trim($params['email']) != "") {
                if(!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
                    return json_encode(array(
                        "result" => "mail_not_ok",
                        "return_message" => (new \MSFramework\i18n())->gettext("Il formato dell'indirizzo email non è valido. Verificare e riprovare!")
                    ));
                }
            }

            $subscribe_to_newsletter = false;
            if($params['newsletter'] == "on") {
                if(trim($params['email']) != "") {
                    $subscribe_to_newsletter = true;
                } else {
                    return json_encode(array(
                        "result" => "newsletter__mail_empty",
                        "return_message" => (new \MSFramework\i18n())->gettext("Devi necessariamente inserire un indirizzo email, per poterti iscrivere alla newsletter!")
                    ));
                }
            }

            $check_captcha = false;
            if($this->recaptcha_is_active) {
                $check_captcha = true;
            }

            if($check_captcha) {
                $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".trim($this->producer_config['recaptcha_private'])."&response=".$params["g-recaptcha-response"]."&remoteip=".$_SERVER["REMOTE_ADDR"]), true);
            }

            if ($response["success"] != false || !$check_captcha) {
                $MSFrameworkEmails = new \MSFramework\emails();

                if($subscribe_to_newsletter) {
                    (new \MSFramework\Newsletter\subscribers())->subscribe($params['email'], $params['name']);
                }

                if ($MSFrameworkEmails->sendMail('contact-form', $params)) {

                    (new \MSFramework\forms())->addGenericRequest($params['name'], $params['email'], $params['message'], array(
                        "Oggetto" => $params['subject'],
                        "Iscrizione alla Newsletter" => ($params['newsletter'] == "on") ? "Si" : "No",
                    ));

                    return json_encode(array(
                        "result" => "ok",
                        "return_message" => (new \MSFramework\i18n())->gettext("Email inviata correttamente! La ricontatteremo nel minor tempo possibile!")
                    ));
                } else {
                    return json_encode(array(
                        "result" => "err",
                        "return_message" => (new \MSFramework\i18n())->gettext("Si è verificato un errore durante l'invio della mail. Se il problema persiste, la preghiamo di ricontattarci telefonicamente.")
                    ));
                }
            } else {
                if($response["success"] == false) {
                    return json_encode(array(
                        "result" => "captcha_failed",
                        "return_message" => (new \MSFramework\i18n())->gettext("Impossibile validare il captcha con Google!")
                    ));
                }
            }

        } //TODO Da collegare con i siti
        else if($params['action'] == 'newsletter') {

            $newsletter = new \MSFramework\Newsletter\subscribers();

            $email = $params['email'];
            $name = $params['name'];

            $is_subscribed = $newsletter->checkIfAlreadySubscribed($email);
            $newsletter__result = array('status' => false);

            if (!$is_subscribed)
            {
                $newsletter__result['status'] = $newsletter->subscribe($email, $name);
                if($newsletter__result['status']) {
                    $newsletter__result['titolo'] = 'Iscrizione effettuata';
                    $newsletter__result['return_message'] = 'Adesso devi confermare la tua email, ti abbiamo inviato un\'email all\'indirizzo indicato da te.';
                } else {
                    $newsletter__result['titolo'] = 'Errore inaspettato';
                    $newsletter__result['return_message'] = 'Si è verificato un errore inaspettato durante l\'iscrizione alla newsletter';
                }
            }
            else
            {
                $newsletter__result = array('status' => false, 'titolo' => 'Email già iscritta', 'return_message' => 'L\'indirizzo email digitato risulta già iscritto alla nostra newsletter');
            }

            return(json_encode($newsletter__result));

        }

        return true;

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $array_settings = array();
        return $array_settings;
    }
}
<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class bridge {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules, $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array();
        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     */
    public function ajaxCall($params, $slug = array())
    {
        Global $MSFrameworkSaaSBase;

        if(count($slug)) {
            if(count($slug) === 1 && $slug[0] === 'embed.js') {
                // Includiamo il codice JS per il sito in questione
                echo $this->generateEmbedJS();

            } else if(count($slug) === 2 && is_numeric($slug[0]) && $slug[1] === 'embed.js') {
                // Includiamo il codice JS per un SaaS
                if($MSFrameworkSaaSBase->isSaaSDomain()) {
                    $this->MSFrameworkDatabase->exec('USE marke833__SaaS__id_' . $MSFrameworkSaaSBase->getID() . '__env_' . $slug[0]);
                    echo $this->generateEmbedJS($slug[0]);
                }
            }
        }
    }

    private function generateEmbedJS($ambiente = 0) {
        Global $MSFrameworkCMS, $MSFrameworki18n, $MSFrameworkModules, $MSFrameworkPages, $MSFrameworkVisualBuilder;

        $js = array();

        $js[] = "window.FW360_SaaS_Url = '" . $this->MSFrameworkCMS->getURLToSite() . "';";
        $js[] = "window.FW360_SaaS_Environment = '" . (int)$ambiente . "';";

        $js[] = "if(typeof jQuery == 'undefined') {";
        $js[] = file_get_contents(FRAMEWORK_ABSOLUTE_PATH . 'common/js/lib/jquery.min.js');
        $js[] = '}';

        $js[] = 'const $ = jQuery;';
        $js[] = 'window.$ = jQuery;';

        /* INCLUDO I CODICI DI TRACKING SE PRESENTI */
        $current_website_project_ids = (new \MSFramework\tracking())->getTrackingProjectIDsByWebsite($_SERVER['HTTP_REFERER']);
        if($current_website_project_ids) {
            $js[] = (new \MSFramework\tracking())->generateTrackingJavascript($current_website_project_ids[0]);
        }

        /* INCLUDO L'HEADER COMMON */
        ob_start();
        require(FRAMEWORK_ABSOLUTE_PATH . 'common/includes/header.php');
        $js[] = "jQuery('head').append(atob('" . base64_encode(ob_get_clean()) . "'));";

        /* INCLUDO IL FOOTER COMMON */
        ob_start();
        require(FRAMEWORK_ABSOLUTE_PATH . 'common/includes/footer.php');
        $js[] = "jQuery('body').prepend(atob('" . base64_encode(ob_get_clean()) . "'));";

        // Se il cliente fa parte di un SaaS allora faccio aggiungere un header personalizzato su tutte le chiamate ajax per indicare al framework l'ID del cliente
        if($ambiente > 0) {
            $js[] = "$.ajaxSetup({
                headers: { 'x-FW360-Environment': '" . (int)$ambiente . "' },
                xhrFields: { withCredentials: true }
            });";
        }

        // Includo lo script che permette l'integrazione dei tag MS
        $js[] = file_get_contents(FRAMEWORK_ABSOLUTE_PATH . 'common/modules/frontendParser/js/externalEmbed.js');

        return implode(' ', $js);
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function executeFunctions_header() {}

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function executeFunctions_footer() {}

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $array_settings = array();
        return $array_settings;
    }
}
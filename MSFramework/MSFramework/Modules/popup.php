<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class popup {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules, $MSFrameworki18n;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
        $this->MSFrameworki18n = $MSFrameworki18n;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude() {
        Global $MSFrameworkCMS, $MSFrameworkUrl, $MSFrameworkParser, $MSFrameworkVisualBuilder;

        $current_clean_url = $MSFrameworkCMS->getCleanHost($MSFrameworkUrl->current_url);

        $popupHtml = array();
        foreach((new \MSFramework\popup())->getPopupDetails() as $popup) {

            $to_show = true;

            if(!(int)$popup['active']) {
                $to_show = false;
            }

            if(!(int)$popup['options']['behavior']['force_showing'] && isset($_COOKIE['ms_onExitPopup_' . $popup['id'] . '_closed'])) {
                $to_show = false;
            }

            if($to_show && $popup['options']['pages_to_include']) {
                $to_show = false;
                foreach(explode(',', $popup['options']['pages_to_include']) as $url) {
                    if ($current_clean_url === rtrim($MSFrameworkCMS->getCleanHost($url), '/')) {
                        $to_show = true;
                    }
                }
            }

            if($to_show && $popup['options']['pages_to_exclude']) {
                foreach(explode(',', $popup['options']['pages_to_exclude']) as $url) {
                    if ($current_clean_url === rtrim($MSFrameworkCMS->getCleanHost($url), '/')) {
                        $to_show = false;
                    }
                }
            }

            if($to_show && $popup['options']['conditions']) {

                $conditionClass = new \MSFramework\Newsletter\conditions();
                $conditionClass->ignoreMandatoryCustomers = true;
                $conditionResult = $conditionClass->getPassingConditionUsers($popup['options']['conditions']);

                if(!$conditionResult) $to_show = false;

                if($conditionClass->conditionNeedUserLogin) {
                    $user_data = (new \MSFramework\customers())->getUserDataFromSession();
                    $user_id = 0;

                    if($user_data) {
                        $user_id = $user_data['user_id'];
                    } else if($_SESSION['trackinSuite_UserID']) {
                        $user_id = $_SESSION['trackinSuite_UserID'];
                    } else if($_COOKIE['trackinSuite_UserID']) {
                        $user_id = $_COOKIE['trackinSuite_UserID'];
                    }

                    if(!in_array($user_id, $conditionResult) && !in_array($_SERVER['REMOTE_ADDR'], $conditionResult)) $to_show = false;
                }

            }

            $popupStyles = array();
            if(!empty($popup['options']['appearance']['max-width'])) {
                $popupStyles[] = 'max-width: ' . $popup['options']['appearance']['max-width'] . 'px';
            }

            $popupClasses = array();
            if(!empty($popup['options']['appearance']['overlay'])) {
                $popupClasses[] = $popup['options']['appearance']['overlay'] . 'Overlay';
            }

            $popupHtml[] = (new \MSFramework\popup())->drawPopupHtml($popup['id'], $popup['content'], $popupClasses, $popupStyles, $popup['options']['appearance']);

            $popupHtml[] = '<script>';

            if($to_show) {

                $popupHtml[] = 'jQuery(window).on(\'load\', function(){';
                if ((int)$popup['options']['behavior']['wait'] > 0) {
                    $popupHtml[] = 'setTimeout(function() {';
                }

                if ($popup['options']['behavior']['mode'] === 'immediately') {
                    $popupHtml[] = 'initMSPopup(' . $popup['id'] . ');';
                } else {
                    $popupHtml[] = 'initMSPopupOnExit(' . $popup['id'] . ');';
                }

                if ((int)$popup['options']['behavior']['wait'] > 0) {
                    $popupHtml[] = '}, ' . $popup['options']['behavior']['wait'] * 1000 . ');';
                }
                $popupHtml[] = '});';

            }

            $popupHtml[] = '</script>';

        }

        $MSFrameworkParser->loadLibrary('animate');

        $files_to_include = array(
            'footer' => array(
                'js' => array(FRAMEWORK_COMMON_CDN . 'modules/popup/js/popup.js'),
                'css' => array(FRAMEWORK_COMMON_CDN . 'modules/popup/css/popup.css'),
                'html' => implode('', $popupHtml)
            )
        );

        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params) {
        return false;
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        return array();
    }
}
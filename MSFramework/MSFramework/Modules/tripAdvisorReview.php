<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Modules;

class tripAdvisorReview {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array(
            'js' => array(),
            'css' => array()
        );

        return $files_to_include;
    }
    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {

        $id_tripadvisor = $this->modules->getSettingValue('tripadvisor_autofill', 'tripadvisor_id');


        $post_inputs = array(
            'detail' => $id_tripadvisor, // ID Attrazione

            'ReviewTitle' => '',
            'ReviewText' => '',

            'trip_date_month_year' => date('m,Y'), // Mese,Anno di Viaggio

            /* VOTI */
            'qid12' => 5,
            'qid47' => 5,
            'qid13' => 5,
            'qid203' => 5,
            'qid10' => 5,
            'qid14' => 5,
            'qid11' => 5,
            'qid190' => 5,

            'reviewTagIDs' => '' // Questi variano da tipologia a tipologia, il valore viene settato tipo questo (-112731,-91641,-91791,-96651,-96631,-91561,)

        );

        $return_html = '<form id="myForm" action="https://www.tripadvisor.it/UserReviewEdit" method="post">';
        foreach ($post_inputs as $a => $b) {
            $return_html .= '<input type="hidden" name="'.htmlentities($a).'" value="'.htmlentities($b).'">';
        }

        $return_html .= '</form>';
        $return_html .= '<script type="text/javascript">';
        $return_html .= 'document.getElementById(\'myForm\').submit();';
        $return_html .= '</script>';

        return $return_html;
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {

        $array_settings = array(
            "tripadvisor_id" => array(
                "name" => "ID Attività TripAdvisor",
                "input_type" => "text",
                "input_attributes" => array(),
                "helper" => "L'ID dell'attività su TripAdvisor",
                "mandatory" => false,
                "translations" => false,
                "default" => '',
            ),
        );

        return $array_settings;
    }
}
<?php
/**
 * MSFramework
 * Date: 11/07/18
 */

namespace MSFramework;

class framework {

    function __construct() {
        global $MSFrameworkDatabase, $MSFrameworkCMS;
        
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
    }

    public function getWebsitePathsBy($column = 'customer_domain', $value = '', $from_saas = false) {
        global $MSFrameworkSaaSBase, $MSFrameworki18n, $MSFrameworkCMS;

        if($value == '') $value = $this->MSFrameworkCMS->getCleanHost();

        if($column === 'customer_database' && stristr($value, 'marke833__SaaS__id_')) {
            $saas_db = explode('__env_', str_replace('marke833__SaaS__id_', '', $value));
            $column = 'id';
            $value = $saas_db[1];
        }

        if($from_saas) {
            $website_info = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.saas__environments WHERE $column = :value", array(':value' => $value), true);

            $MSFrameworkEnvironments = new \MSFramework\SaaS\environments($website_info['id']);
            $saas_id = $MSFrameworkEnvironments->getSaaSIDByEnvironment();
            $r_saas_data = $MSFrameworkSaaSBase->getSaaSDataFromDB($saas_id, 'domain')[$saas_id];

            $website_info['customer_domain'] = $r_saas_data['domain'];
            $website_info['id'] = "saas_" . $website_info['id'];
            $website_info['customer_database'] = $MSFrameworkEnvironments->getDBName();
        } else {
            $website_info = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.customers WHERE $column = :value", array(':value' => $value), true);
        }

        $http_ext = 'https:';
        $backend_http_ext = 'http:';

        if(!empty($website_info['virtual_server_name'])) {
            $website_path = SERVER_HOME_FOLDER . DEV_FOLDER_NAME . "/" . $website_info['customer_domain'] . "/" . $website_info['virtual_server_name'] . "/";
            $website_url = $website_info['customer_domain'] . "/" . $website_info['virtual_server_name'];
            $backend_url = $website_info['customer_domain'] . "/MSAdminControlPanel/v1/";
        } else {
            $website_path = SERVER_HOME_FOLDER . PROD_FOLDER_NAME . "/" . $website_info['customer_domain'] . "/";
            $website_url = $website_info['customer_domain'];
            $backend_url = ADMIN_URL . $website_info['customer_domain'] . '/';
        }

        $serverProtocols = $MSFrameworkCMS->getCMSData("server_protocols", false, null, ($from_saas ? str_replace("saas_", "", $website_info['id']) : $website_info['customer_database']), ($from_saas ? "saas" : "site"));

        if ($serverProtocols['front']) {
            $http_ext = $serverProtocols['front'];
        }

        if ($serverProtocols['back']) {
            $backend_http_ext = 'http:';
        }


        $backend_url = $backend_http_ext . '//' . $backend_url;

        $siteData = $MSFrameworkCMS->getCMSData("site", false, null, ($from_saas ? str_replace("saas_", "", $website_info['id']) : $website_info['customer_database']), ($from_saas ? "saas" : "site"));
        $site_logo = '';
        $site_favicon = '';
        if(json_decode($siteData['logos'])) {

            if(json_decode($siteData['logos'], true)['logo']) {
                $site_logo = UPLOAD_LOGOS_FOR_DOMAIN_HTML . json_decode($siteData['logos'], true)['logo'];
            }

            if(json_decode($siteData['logos'], true)['favicon']) {
                $site_favicon = UPLOAD_LOGOS_FOR_DOMAIN_HTML . json_decode($siteData['logos'], true)['favicon'];
            }

            if($value !== '') {
                $site_logo = str_replace(CUSTOMER_DOMAIN_INFO['path'], $website_path, $site_logo);
                $site_favicon = str_replace(CUSTOMER_DOMAIN_INFO['path'], $website_path, $site_favicon);
            }
        }

        $return_array = array(
            'id' => $website_info['id'],
            'database' => $website_info['customer_database'],
            'path' => $website_path,
            'url' => $website_url,
            'backend_url' => $backend_url . ($from_saas ? '?env=' . str_replace("saas_", '', $website_info['id']) : ''),
            'http_ext' => $http_ext,
            'virtual_server' => $website_info['virtual_server_name'],
            'site_name' => $MSFrameworki18n->getFieldValue($siteData['nome']),
            'assets' => array(
                'logo' => $site_logo,
                'favicon' => $site_favicon
            ),
            'owner' => $website_info['owner'],
            'credits' => $website_info['credits'],
            'from_saas' => ($from_saas !== false ? true : false)
        );

        return $return_array;

    }

    /**
     * Restituisce i dati impostati all'interno dei moduli "CMS" del Framework
     *
     * @param string $what Impostare il tipo di dato desiderato (la riga della tabella "cms")
     * @param bool $cache Se impostato su true salva il risultato della query in memcached (o lo recupera dalla cache, se già presente)
     *
     * @return mixed
     */
    public function getCMSData($what, $cache = null) {
        $cache_key = base64_encode((string)$what);
        if(isset($this->cms_data[$cache_key])) {
            return $this->cms_data[$cache_key];
        }

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT value FROM `" . FRAMEWORK_DB_NAME . "`.`cms` WHERE type = :type", array(":type" => $what), true, $cache);
        $ary_data = json_decode($r['value'], true);

        $this->cms_data[$cache_key] = $ary_data;

        return $ary_data;
    }
}
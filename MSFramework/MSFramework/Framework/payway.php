<?php
/**
 * MSFramework
 * Date: 07/03/18
 */
 
namespace MSFramework\Framework;

class payway {
    private $state;

    private $avail_types = array('ms_service', 'ticket_quotation', 'saas');
    private $avail_providers = array("paypal", "stripe");

    public $transactionTables = array('ticket__transactions', 'ms_agency__transactions', 'saas__transactions');

    /**
     * Verifica se un determinato tipo è disponibile per il payway (i tipi sono definiti in $avail_types)
     *
     * @param $type Il tipo da verificare
     * @param bool $force_logout Se impostato su true ed il tipo non è tra quelli disponibili, viene effettuato il logout dell'utente
     *
     * @return bool
     */
    public function checkTypeExists($type, $force_logout = false) {
        global $MSFrameworkUsers;

        if(!in_array($type, $this->avail_types)) {
            if($force_logout) {
                $MSFrameworkUsers->endUserSession('not-logged');
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Verifica se un determinato provider è disponibile per il payway (i providers sono definiti in $avail_providers)
     *
     * @param $type Il provider da verificare
     * @param bool $force_logout Se impostato su true ed il provider non è tra quelli disponibili, viene effettuato il logout dell'utente
     *
     * @return bool
     */
    public function checkProviderExists($provider, $force_logout = false) {
        global $MSFrameworkUsers;

        if(!in_array($provider, $this->avail_providers)) {
            if($force_logout) {
                $MSFrameworkUsers->endUserSession('not-logged');
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Verifica se un determinato provider è abilitato a livello globale del FW ed a livello di abbonamento/preventivo/servizio
     *
     * @param $provider Il provider da verificare
     * @param $type Il tipo di pagamento per il quale effettuare la verifica (i tipi sono definiti in $avail_types)
     * @param array $additional_data Eventuali dati addizionali
     * @param bool $force_logout Se impostato su true ed il provider non è tra quelli disponibili, viene effettuato il logout dell'utente
     *
     * @return bool
     */
    public function checkProviderEnabled($provider, $type, $additional_data = array(), $force_logout = false) {
        global $MSFrameworkFW, $MSFrameworkUsers;

        if($this->checkProviderExists($provider, $force_logout)) {
            $payPalData = $MSFrameworkFW->getCMSData('settings')['payMethods'][$provider];
            if($payPalData['enable'] != "1") {
                if($force_logout) {
                    $MSFrameworkUsers->endUserSession('not-logged');
                } else {
                    return false;
                }
            }

            $avail = array();
            if($type == "ticket_quotation") {
                $class = new \MSFramework\Framework\Ticket\quotations();
                $avail = $class->getAvailPayMethods($additional_data['id']);
            } else if($type == "saas") {
                $class = new \MSFramework\SaaS\subscriptions();
                $avail = $class->getAvailPayMethods($additional_data['id']);
            } else if($type == "ms_service") {
                $class = new \MSFramework\MSAgency\orders();
                $avail = $class->getAvailPayMethods($additional_data['id']);
            }

            if(!array_key_exists($provider, $avail)) {
                if($force_logout) {
                    $MSFrameworkUsers->endUserSession('not-logged');
                } else {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Ottiene i dati di configurazione di un determinato provider
     *
     * @param $provider Il provider per il quale ottenere i dati
     *
     * @return mixed
     */
    public function getProviderData($provider) {
        global $MSFrameworkFW;

        return $MSFrameworkFW->getCMSData('settings')['payMethods'][$provider];
    }

    /**
     * Restituisce un array contenente le informazioni relative al prezzo del preventivo (base imponibile, iva, totale)
     *
     * @param string $price Il prezzo del preventivo (iva inclusa)
     *
     * @return array
     */
    public function formatPrice($price) {
        $prezzo_iva_inclusa = number_format($price, 2, ".", "");
        $prezzo_iva_esclusa = number_format($prezzo_iva_inclusa/1.22, 2, ".", "");
        $iva = number_format($prezzo_iva_inclusa-$prezzo_iva_esclusa, 2, ".", "");

        return array("iva_inclusa" => $prezzo_iva_inclusa, "iva_esclusa" => $prezzo_iva_esclusa, "iva" => $iva);
    }

    /**
     * Crea un preordine nel momento in cui l'utente avvia il checkout vero e proprio (pagamento con il provider)
     *
     * @param string $type Il tipo di ordine in fase di checkout
     * @param string $provider Il provider
     * @param array $framework_data_ary Un array da salvare nella colonna "framework_data"
     * @param array $transaction_id Un array nel formato array("colname" => "nome_colonna_db", "value" => "id_operazione") per la colonna che identifica l'ID dell'operazione in base all'ordine
     *
     * @return array
     */
    public function createPreOrder($type, $provider, $framework_data_ary, $transaction_id) {
        global $MSFrameworkDatabase;

        if($type == "ticket_quotation") {
            $table = "ticket__transactions";
        } else if($type == "ms_service") {
            $table = "ms_agency__transactions";
        } else if($type == "saas") {
            $table = "saas__transactions";
        }

        $id_col = $transaction_id['colname'];
        $id_value = $transaction_id['value'];

        //elimino eventuali transazioni pending per questo ordine (a parità di ID)
        $MSFrameworkDatabase->query("DELETE FROM `" . FRAMEWORK_DB_NAME . "`.`$table` WHERE (ipn_data = '' OR ipn_data is null) AND `$id_col` = :cur_id AND framework_data = :framework_data", array(":cur_id" => $id_value, ":framework_data" => json_encode($framework_data_ary)));

        $MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`$table` (`$id_col`, insert_date, framework_data, provider) VALUES (:cur_id, :insert_date, :framework_data, :provider)", array(":cur_id" => $id_value, ":insert_date" => date("Y-m-d H:i:s"), ":framework_data" => json_encode($framework_data_ary), ":provider" => $provider));

        return array($table, $MSFrameworkDatabase->lastInsertId());
    }

    /**
     * Ottiene i dati di un preordine
     *
     * @param $preorder_reference L'array di riferimento del preordine
     *
     * @return mixed
     */
    public function getPreorderData($preorder_reference) {
        global $MSFrameworkDatabase;

        return $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`" . $preorder_reference[0] . "` WHERE id = :id", array(":id" => $preorder_reference[1]), true);
    }

    /**
     * Crea un preordine nel momento in cui l'utente avvia il checkout vero e proprio (pagamento con il provider)
     *
     * @param array $preorder_data Il preorder
     * @param string $agreement_id L'ID dell'abbonamento attivato
     * @return bool
     */
    public function createSubscriptionAgreement($preorder_data, $agreement_id) {
        global $MSFrameworkDatabase;

        if($MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`subscription__agreements` (agreement_id, transaction_table, transaction_id) VALUES (:agreement_id, :transaction_table, :transaction_id)", array(":agreement_id" => $agreement_id, ":transaction_table" => $preorder_data[0], ":transaction_id" => $preorder_data[1]))) {
            return $MSFrameworkDatabase->lastInsertId();
        }

        return false;
    }

    /**
     * Ottiene il PreorderData tramite l'agreement_id di un abbonamento
     *
     * @param string $agreement_id L'ID dell'abbonamento attivato
     * @return array Ritorna il preorder reference
     */
    public function getSubscriptionAgreement($agreement_id) {
        global $MSFrameworkDatabase;

        $agreement_data = $MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`subscription__agreements` WHERE agreement_id = :agreement_id", array(':agreement_id' => $agreement_id), true);
        if($agreement_data) {
            return array($agreement_data['transaction_table'], $agreement_data['transaction_id']);
        }

        return array();

    }

}
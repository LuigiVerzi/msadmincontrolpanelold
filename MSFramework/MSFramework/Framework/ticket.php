<?php
/**
 * MSFramework
 * Date: 07/03/18
 */
 
namespace MSFramework\Framework;

class ticket {

    public $statusFixHashtags = array(
        '#risolto', '#completo', '#completato', '#chiuso', '#chiudi', '#fatto', '#finito', '#fix', '#closed'
    );

    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Risposta => Dati Risposta) con i dati relativi al post
     *
     * @param string $id L'ID della risposta (stringa) o delle risposte (array) richieste (se vuoto, vengono recuperati i dati di tutte le risposte)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getTicketResponseDetails($id = "", $fields = "*") {
        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary)) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`ticket__risposte` WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Domanda => Dati Domanda) con i dati relativi alla domanda.
     *
     * @param $id L'ID della domanda (stringa) o delle domande (array) richiesti (se vuoto, vengono recuperati i dati di tutte le domande)
     * @param bool $include_messages Indica se formattare anche i messaggi oppure se ottenere solo le info principali
     * @param integer $limit Il limite di discussioni da mostrare
     *
     * @return array
     */
    public function getTicketQuestionDetails($id = "", $include_messages = true, $limit = 0) {
        Global $MSFrameworkFW;

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }


        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.ticket__domande WHERE id != '' AND (" . $same_db_string_ary[0] . ") ORDER BY id DESC" . ($limit > 0 ? " LIMIT 0, $limit" : ""), $same_db_string_ary[1]) as $r) {
            $r['domain_info'] = $MSFrameworkFW->getWebsitePathsBy('id', str_replace("saas_", "", $r['domain']), strstr($r['domain'], "saas_"));
            if($include_messages) $r['messages'] = $this->getTicketDiscussion($r);
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Risposta => Dati Risposta) con i dati relativi alle risposte.
     *
     * @param $domanda L'array della domanda
     *
     * @return array
     */
    public function getTicketDiscussion($domanda) {
        $ary_to_return = array();
        if($domanda) {
            foreach ($this->MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.ticket__risposte WHERE question = :id", array(':id' => $domanda['id'])) as $r) {
                $ary_to_return[$r['id']] = $this->formatTicketMessage($r, $domanda);
            }
        }
        return $ary_to_return;
    }

    /**
     * Formatta l'array della domanda
     *
     * @param $messaggi array L'array con tutte le info relative al messaggio
     * @param $domanda array L'array con tutte le info relative alla domanda
     *
     * @return array
     */
    public function formatTicketMessage($messaggi, $domanda) {
        Global $MSFrameworkUsers, $MSFrameworkCMS, $MSFrameworkFW, $MSFrameworkSimulator;

        $current_website_paths = CUSTOMER_DOMAIN_INFO;
        $custom_website_paths = $domanda['domain_info'];

        if($messaggi['is_staff']) {

            $author_info = $MSFrameworkUsers->getUserDetails($messaggi['user_id'])[$messaggi['user_id']];

            if($author_info) {
                $messaggi['author_info'] = array(
                    'is_staff' => true,
                    'id' => $messaggi['user_id'],
                    'name' => $author_info['nome'] . ' ' . $author_info['cognome'],
                    'email' => $author_info['email'],
                    'avatar' => ($author_info['gallery_friendly'] && $author_info['gallery_friendly']['propic'] ? $author_info['gallery_friendly']['propic'][0]['html']['thumb'] : ABSOLUTE_SW_PATH_HTML . 'assets/img/ms.svg')
                );
            } else {
                // Se l'utente staff non esiste allora mostro un utente standard
                $messaggi['author_info'] = array(
                    'is_staff' => true,
                    'id' => 0,
                    'name' => 'Assistenza MarketingStudio',
                    'email' => 'info@marketingstudio.it',
                    'avatar' => ABSOLUTE_SW_PATH_HTML . 'assets/img/ms.svg'
                );
            }

        } else {
            $ticket_author_propic = ABSOLUTE_SW_PATH_HTML . "assets/img/user.png";

            if(strstr($domanda['domain'], "saas_")) {
                $MSFrameworkEnvironments = new \MSFramework\SaaS\environments(str_replace("saas_", "", $domanda['domain']));

                $db_name = $MSFrameworkEnvironments->getDBName();
            } else {
                $db_name = $this->MSFrameworkDatabase->getAssoc("SELECT customer_database FROM marke833_framework.customers WHERE id = :id", array(":id" => $domanda['domain']), true)['customer_database'];
            }

            if($messaggi['user_id']) {

                $author_info = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `" . $db_name . "`.`users` WHERE id = :id", array(":id" => $messaggi['user_id']), true);
                $ticket_author_propic = json_decode($author_info['propic'], true);

                if ($ticket_author_propic && $ticket_author_propic[0]) {
                    $ticket_author_propic = $custom_website_paths['http_ext'] . str_replace($current_website_paths['url'], $custom_website_paths['url'], UPLOAD_PROPIC_FOR_DOMAIN_HTML) . "tn/" . $ticket_author_propic[0];
                } else {
                    $ticket_author_propic = ABSOLUTE_SW_PATH_HTML . "assets/img/user.png";
                }

                $messaggi['author_info'] = array(
                    'is_staff' => false,
                    'id' => $messaggi['user_id'],
                    'name' => (!empty($author_info['nome']) ? $author_info['nome'] . ' ' . $author_info['cognome'] : $author_info['email']),
                    'email' => $author_info['email'],
                    'avatar' => $ticket_author_propic
                );

            } else {
                // Se l'utente staff non esiste allora mostro un utente standard
                $messaggi['author_info'] = array(
                    'is_staff' => false,
                    'id' => 0,
                    'name' => $domanda['notification_email'],
                    'email' => $domanda['notification_email'],
                    'avatar' => $ticket_author_propic
                );
            }
        }

        $files = array();
        if(json_decode($messaggi['files']) && json_decode($messaggi['files'], true) > 0) {

            $uploader = new \MSFramework\uploads('ASSISTENZA_TICKET');

            foreach(json_decode($messaggi['files'], true) as $file) {
                $files[] = array(
                    "html" => array(
                        'main' => $uploader->path_html . $file,
                        'thumb' => $uploader->path_html . 'tn/' . $file
                    ),
                    "absolute" => array(
                        "main" => $uploader->path . $file,
                        "thumb" => $uploader->path . "tn/" . $file
                    ),
                    "filename" => preg_replace('/([0-9]+)_/', '', $file)
                );
            }
        }

        $messaggi['files_json'] = $messaggi['files'];
        $messaggi['files'] = $files;
        $messaggi['quotation'] = json_decode($messaggi['quotation'], true);
        $messaggi['message'] = $this->prepareMessageString($messaggi['message']);

        return $messaggi;
    }

    public function prepareMessageString($message) {

        if(json_decode($message)) {
            $message_array = json_decode($message, true);
            if($message_array['type'] == 'status_update') {
                $message = 'Lo stato del ticket è stato aggiornato: <b>' . $message_array['value'] . '%</b>';
            }
        }

        return $message;
    }

    /**
     * Ottiene un array con la lista degli IDS di eventuali Ticket aperti
     *
     * @param $limit integer Il limite di ids da mostrare
     * @param $global bool Se impostato mostra i ticket di tutti i siti
     *
     * @return array
     */
    public function getActiveTicketQuestionsIds($limit = 10, $global = false) {
        $ary_to_return = array();

        $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString(CUSTOMER_DOMAIN_INFO['id'], "OR", "domain");
        if($global) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM marke833_framework.ticket__domande WHERE status != 100 AND (" . $same_db_string_ary[0] . ") ORDER BY id DESC LIMIT $limit", $same_db_string_ary[1]) as $r) {
            $ary_to_return[] = $r['id'];
        }
        return $ary_to_return;
    }

    /**
     * Ottiene un array con la lista degli IDS di eventuali Ticket chiusi
     *
     * @param $limit integer Il limite di ids da mostrare
     *
     * @return array
     */
    public function getInactiveTicketQuestionsIds($limit = 10) {
        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM marke833_framework.ticket__domande WHERE domain LIKE :domain AND status = 100 ORDER BY id DESC LIMIT $limit", array(':domain' => CUSTOMER_DOMAIN_INFO['id'])) as $r) {
            $ary_to_return[] = $r['id'];
        }
        return $ary_to_return;
    }

    public function getTicketCategories() {

        $ary_to_return = array(
            'richiesta_modifiche' => array(
                'title' => 'Richiesta di Modifiche',
                'disclaimer' => 'Hai bisogno di modifiche? Apri un ticket di richiesta modifiche e provvederemo nel minor tempo possibile.<br><b>N.B.</b> Alcuni interventi potrebbero essere soggetti a preventivo.'
            ),
            'segnalazione_bug' => array(
                'title' => 'Segnalazione di Bug',
                'disclaimer' => 'Hai trovato un errore nel software? Apri un ticket di segnalazione e lo risolveremo nel minor tempo possibile.'
            ),
            'assistenza_tecnica' => array(
                'title' => 'Richiesta di Informazioni',
                'disclaimer' => 'Stai riscontrando difficoltà nell\'utilizzo del software? Illustraci il tuo problema e risponderemo nel minor tempo possibile.'
            )
        );

        return $ary_to_return;
    }

    /**
     * Crea un nuovo ticket
     *
     * @param $domain_id integer L'ID del dominio attuale
     * @param $user_data array Le informazioni del cliente
     * @param $title string Il titolo del messaggio
     * @param $message string Il messaggio del ticket
     * @param $notification_email string L'email dove il cliente riceverà le notifiche
     * @param $category string La categoria del ticket
     * @param $files array Eventuali allegati
     * @param $from_mail boolean Indica se il ticket è stato inviato tramite email
     *
     * @return integer
     */
    public function createNewTicket($domain_id, $user_data, $title, $message, $notification_email, $category = 'assistenza_tecnica', $files = array(), $from_mail = false) {
        global $MSFrameworkDatabase, $MSFrameworkSaaSBase;

        $array_to_save = array(
            "domain" => $domain_id,
            "user_id" => $user_data['id'],
            "category" => $category,
            "title" => $title,
            "notification_email" => $notification_email,
            "admin_unread" => 1
        );

        $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $result = $MSFrameworkDatabase->pushToDB("INSERT INTO marke833_framework.ticket__domande ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

        $ticket_id = 0;
        if($result) {
            $ticket_id = $MSFrameworkDatabase->lastInsertId();

            $message_id = $this->addReplyToTicket($ticket_id, $user_data['id'], $message, $files, $from_mail);

            if (!$message_id) {
                $MSFrameworkDatabase->deleteRow('marke833_framework.ticket__domande', 'id', $ticket_id);
                return false;
            }

            // Invio un email agli admin per notificargli l'apertura del ticket
            $emailClass = new \MSFramework\emails();
            $emailClass->forceRealRecipients = true;

            $emailClass->sendMail('ticket-new-question', array(
                'id' => $ticket_id,
                'titolo' => $title,
                'messaggio' => $message,
                'categoria' => $this->getTicketCategories()[$category]['title'],
                'user_name' => (!empty($user_data['nome']) ? $user_data['nome'] . ' ' . $user_data['cognome'] : $user_data['email']),
                'user_email' => ($user_data['email'] ? $user_data['email'] : $user_data['username']),
                'attachments' => $files
            ));

            if (!empty($notification_email)) {
                // Invio un email al cliente con il resoconto del ticket se è stata indicata un email da notificare

                $emailClass = new \MSFramework\emails(); // Ridichiaro per resettare le variabili SMTP
                $emailClass->forceRealRecipients = true;

                $emailClass->sendMail('ticket-user-summary', array(
                    'id' => $ticket_id,
                    'titolo' => $title,
                    'messaggio' => $message,
                    'categoria' => $this->getTicketCategories()[$category]['title'],
                    'user_name' => (!empty($user_data['nome']) ? $user_data['nome'] . ' ' . $user_data['cognome'] : $user_data['email']),
                    'notification_email' => $notification_email
                ));
            }

            // Invio una notifica push ai superadmin per notificarli dell'apertura del ticket
            (new \MSFramework\pushNotification())
                ->setAllSuperAdmins()
                ->sendNotification(
                    '[Ticket #' . $ticket_id .'] Nuova ' . $this->getTicketCategories()[$category]['title'],
                    (!empty($user_data['nome']) ? $user_data['nome'] . ' ' . $user_data['cognome'] : $user_data['email']) . ' ha aperto un ticket di ' . $this->getTicketCategories()[$category]['title'],
                    'https://' . ADMIN_URL . 'marketingstudio.it/modules/framework360/assistenza/ticket/edit.php?id=' . $ticket_id
                );
        }

        return $ticket_id;

    }
    
    /**
     * Aggiunge la risposta sul ticket e notifica i clienti se necessario
     *
     * @param $ticket_id integer L'ID del ticket
     * @param $user_id integer L'ID del cliente
     * @param $message string Il messaggio inviato
     * @param $is_staff boolean Definisce il ruolo dell'utente
     * @param $files array Eventuali allegati
     * @param $from_mail boolean Indica se il messaggio è stato inviato tramite mail
     * @param $quotation array Eventuale preventivo
     *
     * @return integer
     */
    public function addReplyToTicket($ticket_id, $user_id, $message, $files = array(), $from_mail = false, $quotation = array()) {
        Global $MSFrameworkDatabase;

        $ticket_details = $this->getTicketQuestionDetails($ticket_id, true);
        if($ticket_details) {
            $ticket_details = $ticket_details[$ticket_id];
        } else {
            return false;
        }

        $uploader = new \MSFramework\uploads('ASSISTENZA_TICKET');

        $ary_files = $uploader->prepareForSave($files, array(
            "png",
            "jpeg",
            "jpg",
            "pdf",
            "rar",
            "zip",
            "doc",
            "docx",
            "ppt",
            "svg",
            "txt",
            "xls",
            "xlsx",
            "xml"
        ));

        if($ary_files === false && !$from_mail) {
            echo json_encode(array("status" => "mv_error"));
            die();
        }

        $is_staff = (stristr($user_id, 'global') ? 1 : 0);

        $array_to_save = array(
            "question" => $ticket_id,
            "is_staff" => $is_staff,
            "user_id" => $user_id,
            "message" => $message,
            "files" => json_encode($files),
            "quotation" => json_encode($quotation),
            "from_mail" => ($from_mail ? "1" : "0")
        );

        $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $result = $MSFrameworkDatabase->pushToDB("INSERT INTO marke833_framework.ticket__risposte ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

        if(!$result) {
            $uploader->unlink($ary_files);
            return false;
        }

        $message_id = $MSFrameworkDatabase->lastInsertId();

        if(count($ticket_details['messages']) > 0) {

            if($is_staff) {

                //$user_info = $MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.users WHERE id = :user_id", array(':user_id' => $user_id), true);

                // Inserendo un hashtag come il seguente '#50%' lo stato viene automaticamente aggiornato
                $update_status = false;
                preg_match_all('/#([0-9]{1,3})%/m', $message, $status_update, PREG_SET_ORDER, 0);
                if($status_update) {
                    $update_status = $status_update[0][1];
                }

                // Inserendo uno dei seguenti hashtag nell'email il ticket viene impostato come 'Completato'
                $fix_hashtags = $this->statusFixHashtags;

                foreach($fix_hashtags as $ht) {
                    if(stristr($message, $ht)) {
                        $update_status = 100;
                    }
                }

                // Rimuovo eventuali hashtag dal messaggio
                $message = preg_replace('/#([0-9]{1,3})%/m', '', $message);
                $message = str_replace($fix_hashtags, array(), $message);

                // Se lo stato del ticket è 0 lo imposto automaticamente al 25 per far capire che è stato preso in carico
                if($update_status == 0 && $ticket_details['status'] == 0) {
                    $update_status = 25;
                }

                if($update_status > 0) {
                    $MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.ticket__domande SET status = :status WHERE id = :id", array(':id' => $ticket_id, ':status' => $update_status));
                }

                if($update_status !== false) {
                    if(!strlen(strip_tags($message))) {
                        $message = json_encode(array(
                            'type' => 'status_update',
                            'value' => $update_status
                        ));
                    }
                    $MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.ticket__risposte SET message = :message WHERE id = :id", array(':id' => $message_id, ':message' => $message));
                }

                // Invio un email al cliente con l'aggiornamento dello stato
                if($ticket_details['notification_email'] !== "") {
                    $author_info = array_values($ticket_details['messages'])[0]['author_info'];


                    $emailClass = new \MSFramework\emails();
                    $emailClass->forceRealRecipients = true;

                    if(strlen(strip_tags($message)) > 1 || $quotation['enable'] === "1") {
                        // Se il messaggio contiene del testo, o se è stato aggiunto un preventivo, invio la notifica

                        $emailClass->sendMail('ticket-new-reply[user]', array(
                            'id' => $ticket_id,
                            'titolo' => $ticket_details['title'],
                            'messaggio' => $message,
                            'categoria' => $this->getTicketCategories()[$ticket_details['category']]['title'],
                            'user_name' => $author_info['name'],
                            'notification_email' => $ticket_details['notification_email'],
                            'attachments' => $files,
                            'quotation' => $quotation
                        ));

                    } else if((int)$update_status !== (int)$ticket_details['status']) {
                        // Se il messaggio contiene un aggiornamento di stato invio la notifica

                        $status_label = 'In attesa di aggiornamenti';
                        if ($update_status > 0) {
                            if ($update_status == 100) {
                                $status_label = 'Completato al 100%';
                            } else {
                                $status_label = 'Completato al ' . $update_status . '%';
                            }
                        }

                        $emailClass->sendMail('ticket-status-update', array(
                            'id' => $ticket_id,
                            'stato' => $status_label,
                            'categoria' => $this->getTicketCategories()[$ticket_details['category']]['title'],
                            'user_name' => $author_info['name'],
                            'notification_email' => $ticket_details['notification_email']
                        ));
                    }
                }

                $MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.ticket__domande SET have_unread = (have_unread+1) WHERE id = :id", array(':id' => $ticket_id));

            } else {

                $user_info = $MSFrameworkDatabase->getAssoc("SELECT * FROM " . $ticket_details['domain_info']['database'] . ".users WHERE id = :user_id", array(':user_id' => $user_id), true);

                // Invio un email agli admin per notificargli il messaggio
                $emailClass = new \MSFramework\emails();
                $emailClass->forceRealRecipients = true;
                $emailClass->sendMail('ticket-new-reply[admin]', array(
                    'id' => $ticket_id,
                    'titolo' => $ticket_details['title'],
                    'messaggio' => $message,
                    'categoria' => $this->getTicketCategories()[$ticket_details['category']]['title'],
                    'user_name' => $user_info['nome'] . ' ' . $user_info['cognome'],
                    'attachments' => $files
                ));

                $MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.ticket__domande SET admin_unread = (admin_unread+1) WHERE id = :id", array(':id' => $ticket_id));

                // Riapre il ticket se chiuso
                if($ticket_details['status'] == 100) {
                    $MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.ticket__domande SET status = :status WHERE id = :id", array(':id' => $ticket_id, ':status' => 50));
                }

                // Invio una notifica push ai superadmin per notificarli dell'apertura del ticket
                (new \MSFramework\pushNotification())
                    ->setAllSuperAdmins()
                    ->sendNotification(
                        '[Ticket #' . $ticket_details['id'] .'] Nuovo messaggio ricevuto',
                        (!empty($user_info['nome']) ? $user_info['nome'] . ' ' . $user_info['cognome'] : $user_info['email']) . ' ha risposto al ticket ' . $ticket_details['title'],
                        'https://' . ADMIN_URL . 'marketingstudio.it/modules/framework360/assistenza/ticket/edit.php?id=' . $ticket_id
                    );

            }

        }

        return $message_id;

    }
}
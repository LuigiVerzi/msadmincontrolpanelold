<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Framework\Ticket;

class quotations extends \MSFramework\Framework\ticket {
    function __construct() {
        global $MSFrameworkCMS;

        parent::__construct();

        $this->MSFrameworkCMS = $MSFrameworkCMS;
    }

    /**
     * Restituisce la versione testuale dei vari stati di pagamento di un preventivo
     *
     * @param $state Lo stato per il quale ottenere la versione testuale
     *
     * @return mixed
     */
    public function formatPaymentState($state) {
        $r = array(
            "unpaid" => "In attesa di pagamento",
            "paid" => "Pagato",
            "canceled" => "Annullato",
        );

        return $r[$state];
    }

    /**
     * Aggiorna lo stato di pagamento di un preventivo
     *
     * @param $id_or_preorder integer L'ID del preventivo o l'array con i riferimento alla riga del preordine
     * @param $status string Il nuovo stato
     * @param bool $ipn_data Se valorizzato come array, processa i dati registrando l'ordine ed emettendo fattura
     *
     * @return bool
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function changeStatus($id_or_preorder, $status, $ipn_data = false) {
        if(is_array($id_or_preorder)) {
            $preorder = (new \MSFramework\Framework\payway())->getPreorderData($id_or_preorder);
            $table = $id_or_preorder[0];
            $id = $preorder['quote_id'];
        } else {
            $table = "ticket__transactions";
            $id = $id_or_preorder;
        }

        $r = $this->getTicketResponseDetails($id, "quotation")[$id];
        $quotation = json_decode($r['quotation'], true);

        if($status == "canceled" && $quotation['state'] == "paid") {
            return false;
        }

        if(is_array($quotation) && count($quotation) > 0) {
            $quotation['state'] = $status;
            $quotation['state_change_date'] = date("d/m/Y H:i");

            $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`ticket__risposte` SET quotation = :quotation WHERE id = :id", array(':id' => $id, ':quotation' => json_encode($quotation)));

            if($status == "paid" && $ipn_data !== false && is_array($ipn_data)) {
                $fic_invoices = new \MSFramework\Fatturazione\Services\FattureInCloud\invoices();
                $fic_customers = new \MSFramework\Fatturazione\Services\FattureInCloud\customers();

                $preorder_framework_data = json_decode($preorder['framework_data'], true);

                $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`" . $table . "` SET ipn_data = :ipn_data WHERE id = :id", array(":ipn_data" => json_encode($ipn_data), ":id" => $id_or_preorder[1]));

                //emetto la fattura su Fatture in Cloud
                $fic_invoices->newInvoice($fic_customers->createCustomerDataArray(array_merge(array("email" => $preorder_framework_data['user']['email']), $preorder_framework_data['user']['dati_fatturazione'])), [["name" => $preorder_framework_data['item']['title'], "descr" => $preorder_framework_data['item']['descr'], "price" => $preorder_framework_data['price']['iva_inclusa']]], ["method" => $preorder['provider']]);
            }

            return true;
        }

        return false;
    }

    /**
     * Ottiene tutti i metodi di pagamento disponibili per un determinato preventivo
     *
     * @return array
     */
    public function getAvailPayMethods($quotation_id) {
        global $MSFrameworkFW;

        $r_response = $this->getTicketResponseDetails($quotation_id, "quotation")[$quotation_id];

        $quotation = json_decode($r_response['quotation'], true);
        $settings_data = $MSFrameworkFW->getCMSData('settings')['payMethods'];

        foreach($quotation['data']['pay_methods'] as $cur_id) {
            if($settings_data[$cur_id]['enable'] == "1") {
                $return[$cur_id] = $settings_data[$cur_id];
            }
        }

        return $return;
    }

    /**
     * Verifica che l'id del preventivo (della risposta) appartenga effettivamente al cliente che fa la richiesta
     * @param $id L'id del preventivo
     * @param bool $force_logout Se impostato su true ed il provider non è tra quelli disponibili, viene effettuato il logout dell'utente
     *
     * @return bool
     */
    public function checkQuotationOwner($id, $force_logout = false) {
        global $MSFrameworkFW, $MSFrameworkUsers;

        $r_response_details = $this->getTicketResponseDetails($id, 'question')[$id];
        if(isset($r_response_details['question'])) {
            $r = $this->getTicketQuestionDetails($r_response_details['question'])[$r_response_details['question']];
        }

        if(strstr($r['domain'], "saas_")) {
            $domain_info = $MSFrameworkFW->getWebsitePathsBy('id', str_replace("saas_", "", $r['domain']), true)['id'];
        } else {
            $domain_info = $MSFrameworkFW->getWebsitePathsBy('customer_domain', '', strstr($r['domain'], "saas_"))['id'];
        }

        if(!$r || $r['domain'] != $domain_info) {
            if($force_logout) {
                $MSFrameworkUsers->endUserSession('not-logged');
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Verifica che il preventivo sia effettivamente pagabile (appartiene al cliente che fa la richiesta e non è stato già pagato)
     *
     * @param $id L'id del preventivo
     * @param bool $force_logout Se impostato su true ed il provider non è tra quelli disponibili, viene effettuato il logout dell'utente
     *
     * @return bool
     */
    public function checkQuotationPayable($id, $force_logout = false) {
        global $MSFrameworkUsers;

        if($this->checkQuotationOwner($id, $force_logout)) {
            $r_response_details = $this->getTicketResponseDetails($id, 'quotation')[$id];
            $quotation = json_decode($r_response_details['quotation'], true);

            if(!is_array($quotation) || count($quotation) == 0 || $quotation['state'] != "unpaid") {
                if($force_logout) {
                    $MSFrameworkUsers->endUserSession('not-logged');
                } else {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Ottiene eventuali dati di pagamento digitali (paypal, stripe...) a fronte di un id di preventivo
     *
     * @param $id L'ID del preventivo
     *
     * @return mixed
     */
    public function getDigitalPaymentData($id) {
        return $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`ticket__transactions` WHERE quote_id = :quote_id", array(":quote_id" => $id), true);
    }
}
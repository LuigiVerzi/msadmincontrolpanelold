<?php
/**
 * MSFramework
 * Date: 07/03/18
 */
 
namespace MSFramework\Framework;

class simulator {
    private $state;
    
    public function __construct() {
        $this->state = null; //può assumere i valori di saas o site, ed in base a questo alcune funzioni potrebbero comportarsi in maniera diversa
    }

    public function set($type) {
        if(in_array($type, array("site", "saas"))) {
            $this->state = $type;
        }
    }

    public function unset() {
        $this->state = null;
    }

    public function get() {
        return $this->state;
    }
}
<?php
/**
 * MSFramework
 * Date: 07/03/18
 */

namespace MSFramework\Framework;

class credits {

    private $user_id = CUSTOMER_DOMAIN_INFO['database'];
    private $table = 'customers';

    public $creditsRequired = array();

    /**
     * credits constructor.
     * @param integer $user_id L'ID del cliente o dell'ambiente SaaS (da tabella customers o saas_environments)
     * @param bool $is_saas Il tipo di cliente
     */
    public function __construct($user_id = 0, $is_saas = false) {
        global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;

        $this->creditsRequired = array(
            'sms' => array(
                'title' => 'Invio SMS',
                'description' => "L'invio di SMS richiede l'utilizzo dei crediti.",
                'price' => 0.10,
                'price_suffix' => 'per sms'
            ),
            /* AZIONI TRACKING */
            'tracking' => array(
                'title' => 'Tracking',
                'description' => 'Qui la descriione del tracking.',
                'price' => 5.0,
                'price_suffix' => 'per progetto',
                'check' => function() {
                    Global $MSFrameworkDatabase;
                    return ($MSFrameworkDatabase->getCount("SELECT * FROM tracking__projects") > 0 ? true : false);
                },
            ),
            'emails' => array(
                'title' => 'Email',
                'description' => ''
            )
        );

        $this->user_id = str_replace('saas_', '', CUSTOMER_DOMAIN_INFO['id']);
        $this->table = (CUSTOMER_DOMAIN_INFO['from_saas'] ? 'saas__environments' : 'customers');

        if($user_id) $this->user_id = $user_id;
        if($is_saas) $this->table = 'saas__environments';
    }

    /**
     * Aggiunge dei crediti al cliente
     * @param $credits float IL numero di crediti da assegnare
     * @param $note string Eventuali note da allegare
     * @return integer
     */
    public function addCredits($credits = 0.0, $note = '') {
        if($this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`" . $this->table . "` SET credits = (credits + " . (float)$credits . ") WHERE id = :id", array(':id' => $this->user_id))) {

            $array_to_save = array(
                "value" => $credits,
                "note" => $note,
                "user_id" => $this->user_id,
                "is_saas" => ($this->table !== 'customer' ? 1 : 0),
            );

            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
            return $this->MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`credits__histories` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

        }

        return false;
    }

    /**
     * Imposta i crediti del cliente
     * @param $credits float IL numero di crediti da impostare
     * @return integer
     */
    public function setCredits($credits = 0.0) {
        return $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`" . $this->table . "` SET credits = '" . (float)$credits . "' WHERE id = :id", array(':id' => $this->user_id));
    }

    /**
     * Rimuove dei crediti al cliente
     * @param $credits float IL numero di crediti da rimuovere
     * @param $note string Eventuali note da allegare
     * @return integer
     */
    public function removeCredits($credits = 0.0, $note = '') {
        if($this->getCredits() > $credits && $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`" . $this->table . "` SET credits = (credits - " . (float)$credits . ") WHERE id = :id", array(':id' => $this->user_id))) {
            $array_to_save = array(
                "value" => -$credits,
                "note" => $note,
                "user_id" => $this->user_id,
                "is_saas" => ($this->table !== 'customer' ? 1 : 0),
            );

            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
            return $this->MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`credits__histories` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
        }

        return false;
    }

    public function getCredits() {
        return (float)$this->MSFrameworkDatabase->getAssoc("SELECT credits FROM `" . FRAMEWORK_DB_NAME . "`.`" . $this->table . "` WHERE id = :id", array(':id' => $this->user_id), true)['credits'];
    }

    public function checkCreditsForAction($actions) {

        if(!is_array($actions)) {
            $actions = array($actions);
        }

        $price_value = 0;

        $confirmation_html = array();
        foreach ($actions as $s) {
            if (in_array($s, array_keys($this->creditsRequired))) {
                if(!isset($this->creditsRequired[$s]['check']) || $this->creditsRequired[$s]['check']()) {

                    $confirmation_html[] = '<div>
                    ' . (isset($this->creditsRequired[$s]['price']) ? '<span style="text-align: right; font-size: 14px; padding: 5px;" class="label label-info pull-right"><span style="display: block; text-align: right; font-size: 14px;">' . number_format($this->creditsRequired[$s]['price'], 2, ',', '.') . '</span><small style="font-size: 10px;">' . $this->creditsRequired[$s]['price_suffix'] . '</small></span>' : '') .
                    '<h3 style="margin: 0 0 2px;">' . $this->creditsRequired[$s]['title'] . '</h3>
                    <p style="margin: 0;">' . $this->creditsRequired[$s]['description'] . '</p>
                    </div>';

                    if (isset($this->creditsRequired[$s]['price'])) {
                        $price_value += $this->creditsRequired[$s]['price'];
                    }
                    
                }
            }
        }

        if($confirmation_html) {

            $currentCredits = $this->getCredits();

            $confirmation_html[] = '<div class="text-right"><label style="margin: 0;">Crediti attuali:</label><p>' . number_format($currentCredits, 2, ',', '.') . '</p></div>';
            if ($price_value > $currentCredits) {

                die(json_encode(array(
                    'status' => 'credits_insufficient',
                    'title' => 'Credito non sufficiente',
                    'message' => implode(PHP_EOL, array(
                        implode('<div class="hr-line-dashed"></div>', $confirmation_html),
                        '<div class="hr-line-dashed"></div>',
                        '<h4>Crediti richiesti</h4>',
                        '<p>' . $price_value . '</p>',
                    )),
                    'cancel_label' => 'Annulla',
                    'confirm_label' => 'Acquista crediti',
                    'redirect' => (new \MSSoftware\modules())->getModuleUrlByID('ms_agency_servizi')
                )));

            } else if (!in_array('X-Credits-Confirmed', array_keys(getallheaders()))) {
                die(json_encode(array(
                    'title' => 'Conferma utilizzo dei crediti',
                    'status' => 'credits_confirmation',
                    'message' => implode(PHP_EOL, array(
                        implode('<div class="hr-line-dashed"></div>', $confirmation_html)
                    )),
                    'cancel_label' => 'Annulla',
                    'confirm_label' => 'Conferma'
                )));
            }
        }
    }

    /**
     * Rimuove dei crediti al cliente in base alle azioni
     * @param $actions string|array Le azioni che sta utilizzando il cliente
     * @return bool
     */
    public function removeCreditsForActions($actions) {

        if(!is_array($actions)) {
            $actions = array($actions);
        }

        $prices = array();
        foreach ($actions as $s) {
            if (in_array($s, array_keys($this->creditsRequired))) {
                if(!isset($this->creditsRequired[$s]['check']) || $this->creditsRequired[$s]['check']()) {
                    if (isset($this->creditsRequired[$s]['price'])) {
                        $prices[$this->creditsRequired[$s]['title']] = $this->creditsRequired[$s]['price'];
                    }
                }
            }
        }

        if($this->getCredits() > array_sum(array_values($prices))) {
            foreach($prices as $note => $price) $this->removeCredits($price, $note);
            return true;
        }

        // TODO: Inviare email crediti esauriti

        return false;
    }

}
<?php
/**
 * MSFramework
 * Date: 10/03/18
 */

namespace MSFramework;


class forms {
    public function __construct() {
        global $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Form => Dati Form) con i dati relativi al Form.
     *
     * @param $id L'ID del form (stringa) o dei form (array) richiesti (se vuoto, vengono recuperati i dati di tutti i form)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getFormDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }


        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM forms WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Genera il codice HTML del form selezionato passando dei parametri
     *
     * @param $id L'ID del form
     * @param $params I parametri del form da sovrascrivere
     * @param $default_value I parametri da passare come valore dei campi
     *
     * @return string
     */
    public function composeHTMLForm($id, $params = array(), $default_value = array()) {
        global $MSFrameworki18n;

        $default = array(
            'templates' => array(
                'form' => '{form}',
                'input' => '<div class="{classes}">{label} {input} {description}</div>',
                'checkbox' => '<div class="{classes}">{input} {label} {description}</div>',
                'radio' => array(
                    'group' => '<div class="{classes}">{label}{input} {description}</div>',
                    'single' =>  '{radio} {label}'
                ),
                'textarea' => '<div class="{classes}">{label}{input}{description}</div>',
                'select' => '<div class="{classes}">{label}{input}{description}</div>',
                'button' => '{button}'
            ),
            'classes' => array(
                'form' => '',
                'form_loading' => 'loading',
                'input_error' => 'error',
                'input' => '',
                'checkbox' => '',
                'radio' => '',
                'textarea' => '',
                'select' => '',
                'button' => '',
            ),
            'hooks' => array(
                'before_form_submit' => 'MSForms_beforeFormSubmit',
                'after_form_submit' => 'MSForms_afterFormSubmit',
                'before_quote_calculation' => 'MSForms_beforeQuoteCalculation',
                'after_quote_calculation' => 'MSForms_afterQuoteCalculation',
                'on_change' => 'MSForms_onInputChange',
                'on_error' => 'MSForms_onError',
                'show_message' => 'MSForms_showMessage',
            ),
            'messages' => array(
                'required_field' => 'Questo campo è obbligatorio'
            ),
            'settings' => array(
                //'send_to_user_id' => "Se impostato, invia la mail all'indirizzo dell'utente con ID <send_to_user_id> invece che alla mail di default (solo per l'azione: generic_request)",
                //'send_to_user_mail' => "Se impostato, invia la mail all'indirizzo <send_to_user_mail> invece che alla mail di default (solo per l'azione: generic_request)",
            )
        );
        $params = array_replace_recursive($default, $params);

        $html = '';
        $form = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM forms WHERE id = :id", array(":id" => $id), true);

        $form_actions = json_decode($form["actions"], true);

        $show_gdpr_check = false;
        if((new \MSFramework\modules())->checkIfIsActive('gdpr')) {
            if(count(array_intersect(array("newsletter_subscribe", "generic_request", "property_info", "anagrafica"), $form_actions)) > 0) {
                $show_gdpr_check = true;
            }
        }

        if(!in_array("quote", $form_actions)) {
            unset($params['hooks']['before_quote_calculation']);
            unset($params['hooks']['after_quote_calculation']);
        } else {
            $params['settings']['quote_force_manually'] = json_decode($form["actions_values"], true)['quote']['force_manually'];
        }

        if($form && json_decode($form['fields'])) {
            $fields = json_decode($form['fields'], true);

            foreach($default_value as $name => $def) {

                foreach($fields as $k=>$field) {
                    if($field['field_id'] == $name) unset($fields[$k]);
                }

                $fields[] = array(
                    'field_id' => $name,
                    'name' => $name,
                    'input_type' => 'hidden',
                    'type' => 0,
                    'default' => $def
                );

            }

            if($show_gdpr_check) {
                //alcuni form utilizzano un sistema a step utilizzando come nome della classe "stepX" (dove X è il numero dello step). Intercetto tale sistema per piazzare il check del GDPR alla fine degli step
                $found_step_method = false;
                $max_step = 0;
                $max_step_class = "";
                $bootstrap_cols_used = array('col-sm-12');
                foreach($fields as $field) {
                    $ary_cont_class = explode(" ", $field['add_container_class']);
                    $matches = preg_grep("/step/", $ary_cont_class);

                    if(stristr($field['add_container_class'], 'col-xs')) {
                        $bootstrap_cols_used[] = 'col-xs-12';
                    }

                    if($matches) {
                        $found_step_method = true;

                        foreach($matches as $match) {
                            $cur_step = str_replace("step", "", $match);
                            if($cur_step > $max_step) {
                                $max_step = $cur_step;
                            }
                        }
                    }
                }

                if($found_step_method) {
                    $max_step_class = "step" . $max_step;
                }
                //

                $bootstrap_cols_used = array_unique($bootstrap_cols_used);

                $fields[] = array("name" => json_encode(array("it_IT" => "Ho letto ed accetto <a href='" . (new \MSFramework\pages())->getURL('privacy-policy') . "' target='_blank'><u>l'informativa sulla privacy</u></a>", "en_GB" => "I have read and accept the <a href='" . (new \MSFramework\pages())->getURL('privacy-policy') . "' target='_blank'><u>privacy policy</u></a>")), "input_type" => "checkbox", "mandatory" => "1", "default_checked" => "0", "field_id" => "gdpr_form_checkbox", "add_container_class" => implode(' ', $bootstrap_cols_used) . " $max_step_class gdpr-container");
            }

            foreach($fields as $field) {

                $input = '';
                $attribute_to_attach = 'name="' . $field['field_id'] . '" data-label="' . htmlentities($MSFrameworki18n->getFieldValue($field['name'])) . '"';

                if($field['input_type'] != "hidden") $input_value = $MSFrameworki18n->getFieldValue($field['default']);
                else $input_value = $field['default'];

                // SE È STATO PASSATO UN VALORE PREDEFINITO ALLORA NASCONDO IL CAMPO IMPOSTANDOLO HIDDEN
                if(isset($default_value[$field['field_id']])) {
                    $input_value = $default_value[$field['field_id']];
                    $field['type'] = 0;
                    $field['input_type'] = 'hidden';
                }

                if($field['mandatory']) $attribute_to_attach .= ' required';

                if($field['type'] == 0) // INPUT
                {
                    $input_template = $params['templates']['input'];
                    $input_classes = $params['classes']['input'] . ' ' . $field['add_class'];

                    if($field['input_type'] == 'number') {
                        $attribute_to_attach .= ' min="' . $field['min'] . '" max="' . $field['max'] . '"';
                    }
                    else if($field['input_type'] == 'checkbox') {
                        $input_template = $params['templates']['checkbox'];
                        $input_classes = $params['classes']['checkbox'] . ' ' . $field['add_class'];
                        if($field['default_checked']) $attribute_to_attach .= ' checked';
                        $field['default'] = 'on';
                    }

                    // Aggiungo il placeholder se l'input è testuale
                    if(in_array($field['input_type'], array('text', 'email', 'password', 'number'))) {
                        $attribute_to_attach .= ' placeholder="' . htmlentities($MSFrameworki18n->getFieldValue($field['placeholder'])) . '"';
                    }

                    if($field['input_type'] == 'radio') {
                        $input_template = $params['templates']['radio']['group'];
                        $input_classes = $params['classes']['radio'] . ' ' . $field['add_class'];
                        foreach($field['data'] as $k=>$radio) {
                            $input .= str_replace(
                                array('{label}', '{radio}'),
                                array(
                                    $MSFrameworki18n->getFieldValue($radio['text']),
                                    '<input type="' . $field['input_type'] . '" ' . $attribute_to_attach . ' value="' . $MSFrameworki18n->getFieldValue($radio['value']) . '" ' . ($k == 0 ? 'checked' : '') . '>'),
                                $params['templates']['radio']['single']
                            );
                        }
                    }
                    else
                    {
                        if($field['input_type'] == 'hidden') {
                            $input_classes = '';
                            $input_template = '{input}';
                        }
                        $input = '<input class="' . $input_classes . '" type="' . $field['input_type'] . '" ' . $attribute_to_attach . ' value="' . htmlentities($input_value) . '">';
                    }
                }
                else if($field['type'] == 1) // TEXTAREA
                {
                    $input_template = $params['templates']['textarea'];
                    $input_classes = $params['classes']['textarea'] . ' ' . $field['add_class'];

                    $attribute_to_attach .= ' placeholder="' . htmlentities($MSFrameworki18n->getFieldValue($field['placeholder'])) . '"';
                    $input = '<textarea class="' . $input_classes . '" ' . $attribute_to_attach . '>' . htmlentities($input_value) . '</textarea>';
                }
                else if($field['type'] == 2) // SELECT
                {
                    $input_template = $params['templates']['select'];
                    $input_classes = $params['classes']['select'] . ' ' . $field['add_class'];
                    $options = '';
                    foreach($field['data'] as $opt) {
                        $options .= '<option value="' . htmlentities($MSFrameworki18n->getFieldValue($opt['value'])) . '">' . htmlentities($MSFrameworki18n->getFieldValue($opt['text'])) . '</option>';
                    }
                    $input = '<select ' . $attribute_to_attach . ' class="' . $input_classes . '">' . $options . '</select>';
                }

                $html .= str_replace(
                    array('{label}', '{input}', '{name}', '{classes}', '{col-size}', '{description}'),
                    array($MSFrameworki18n->getFieldValue($field['name']), $input, $field['field_id'], $field['add_container_class'], ($field['add_container_size'] ? implode(' ', $field['add_container_size']) : 'ms-col-12'), $MSFrameworki18n->getFieldValue($field['description'])),
                    $input_template
                );

            }

            $submit_text = $MSFrameworki18n->getFieldValue($form['submit_btn_text']);
            $html .= str_replace(
                array('{button}'),
                array('<button type="submit" class="' . $params['classes']['button'] . '">' . $submit_text . '</button>'),
                $params['templates']['button']
            );

            $html = str_replace(
                array('{form}'),
                array($html),
                $params['templates']['form']
            );

            $hooks_function = array();
            foreach($params['hooks'] as $hook_name => $hook_value) {
                $hooks_function[] = 'data-' . $hook_name . '="' . htmlentities($hook_value) . '"';
            }

            $form_settings = array();
            foreach($params['settings'] as $setting_name => $setting_value) {
                $form_settings[] = 'data-setting_' . $setting_name . '="' . htmlentities($setting_value) . '"';
            }

            $messages_data = array();
            foreach($params['messages'] as $message_name => $message_value) {
                $messages_data[] = 'data-message_' . $message_name . '="' . htmlentities($message_value) . '"';
            }

            $html = '<form id="form_' . $id . '" data-form_id="' . $id . '" data-loading_class="' . $params['classes']['form_loading'] . '" data-input_error_class="' . $params['classes']['input_error'] . '" class="MSForm ' . $params['classes']['form'] . '" ' . implode(' ', $form_settings) . '  ' . implode(' ', $hooks_function) . ' ' . implode(' ', $messages_data) . ' action="' . $this->getURL($id) . '">' . $html . '</form>';

        }

        if(json_decode($form['events'])) {
            $form_custom_events = json_decode($form['events'], true);
            $dynamic_hooks_info = $this->getDynamicsHooksInfo();

            $form_event_html = "<script>function MSForm" . $form['id'] . "_GetHooks() {";
            $form_event_html .= "var hooks = {};";

            $form_event_html .= "try {";
            foreach($form_custom_events as $event_name => $form_event) {
                if(!empty($form_event['js'])) {
                    $form_event_html .= "hooks['" . $event_name . "'] = function(" . implode(', ', array_keys($dynamic_hooks_info[$event_name]['passed_data'])) . ") {try { if(" . htmlentities('$form') . ".data('form_id') == " . $form['id'] . ") {  " . $form_event['js'] . " } } catch(err) {}};";
                }
            }
            $form_event_html .= "} catch(err) {}";

            $form_event_html .= "return hooks;";
            $form_event_html .= "}</script>";

            $html .= $form_event_html;
        }

        return $html;
    }

    /**
     * Ottiene l'URL del submit del form
     * L'URL punterà ad un file presente nella sottocartella "forms/actions/". Il file DEVE chiamarsi <$id>.php (dove <$id> sta per l'ID passato a questa funzione)
     *
     * @param $id L'ID del form
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n, $MSFrameworkCMS;

        return $MSFrameworkCMS->getURLToSite() . (new \MSFramework\modules())->getShortPrefix() . "/forms/";
    }

    /**
     * Ottiene la lista di Hooks dinamici
     *
     * @return array
     *
     */
    public function getDynamicsHooksInfo() {
        $form_events = array(
            'before_form_submit' => array(
                'nome' => 'Prima dell\'Invio',
                'descrizione' => 'Viene richiamato un istante prima dell\'invio vero e proprio dei dati.',
                'can_stop' => true,
                'can_format' => true,
                'passed_data' => array(
                    '$form' => 'L\'elemento del Form',
                    'form_data' => 'Un array (ID Campo => Valore) contenente tutti i valori del form passati dall\'utente'
                )
            ),
            'after_form_submit' => array(
                'nome' => 'Dopo l\'Invio',
                'descrizione' => 'Viene richiamato subito l\'invio dei dati.',
                'can_stop' => true,
                'can_format' => false,
                'passed_data' => array(
                    '$form' => 'L\'elemento del Form',
                    'form_data' => 'Un array (ID Campo => Valore) contenente tutti i valori del form passati dall\'utente',
                    'data' => 'Un array contenente lo stato delle varie azioni (Es: data.reservation.status, data.generic_request.status, data.newsletter_subscribe.status)'
                )
            ),
            'on_change' => array(
                'nome' => 'Cambio Valore Input',
                'descrizione' => 'Viene richiamato nel momento in cui il valore di un input cambia.',
                'can_stop' => false,
                'can_format' => false,
                'passed_data' => array(
                    '$form' => 'L\'elemento del Form',
                    '$input' => 'L\elemento dell\'input che ha subito la variazione',
                    'input_val' => 'Il nuovo valore dell\'input'
                )
            ),
            'show_message' => array(
                'nome' => 'Prima del messaggio di conferma',
                'descrizione' => 'Viene richiamato un attimo prima che venga mostrato il messaggio di successo o di errore.',
                'can_stop' => true,
                'can_format' => false,
                'passed_data' => array(
                    '$form' => 'L\'elemento del Form',
                    'message_type' => 'Il tipo di messaggio che sarà mostrato (reservation, generic_request, newsletter_subscribe)',
                    'data' => 'Un array contenente lo stato della richiesta e i testi da mostrare (data.status, data.titolo, data.message)'
                )
            )
        );

        return $form_events;
    }

    /**
     * Ritorna un array con tutte le info delle azioni utilizzate
     *
     * @return array
     *
     */
    public function getFormActionsInfo() {

        $default_template_html = array(
            '<p class="alert newsletter alert-',
            '[warning|danger|success]',
            '"><b>',
            '[titolo]',
            '</b><br>',
            '[messaggio]',
            '</p>'
        );

        $actions_checks = array(
            "anagrafica" => array(
                "id" => "anagrafica",
                "name" => "Registra Cliente",
                "descr" => "Salva i dati dell'utente nella lista Clienti",
                "fa-icon" => "fa-address-card-o",
                "return_message" => array(
                    'show' => false,
                    'template' => $default_template_html,
                    'strings' => array(
                        'success' => array(
                            'label' => 'Messaggio di Successo',
                            'titolo' => 'Account Registrato',
                            'message' => 'L\'account è stato registrato correttamente'
                        ),
                        'danger' => array(
                            'error' => array(
                                'label' => 'Errore Generico',
                                'titolo' => 'Errore durante la registrazione',
                                'message' => 'Si è verificato un errore inaspettato durante la registrazione dell\'account.'
                            ),
                            'password_match' => array(
                                'label' => 'Password non corrispondenti',
                                'titolo' => 'Le password non corrispondono',
                                'message' => 'Le due password inserite non corrispondono, assicurati di digitarle correttamente.'
                            ),
                            'email_exists' => array(
                                'label' => 'Email esistente',
                                'titolo' => 'Email già esistente',
                                'message' => 'L\'email inserita risulta già registrata!'
                            ),
                            'password_strength' => array(
                                'label' => 'Password troppo debole',
                                'titolo' => 'Password troppo debole',
                                'message' => 'La password inserita è troppo corta, inserisci una password di almeno 6 caratteri'
                            ),
                        )
                    )
                )
            ),
            "quote" => array(
                "id" => "quote",
                "name" => "Genera preventivo",
                "descr" => "Genera un preventivo basato sui valori immessi nel form.",
                "fa-icon" => "fa-money"
            ),
            "newsletter_subscribe" => array(
                "id" => "newsletter_subscribe",
                "name" => "Iscrizione Newsletter",
                "descr" => "Iscrive l'utente ad una lista newsletter.",
                "fa-icon" => "fa-envelope",
                "return_message" => array(
                    'show' => true,
                    'template' => $default_template_html,
                    'strings' => array(
                        'success' => array(
                            'label' => 'Messaggio di Successo',
                            'titolo' => 'Iscrizione alla newsletter',
                            'message' => 'La tua richiesta di registrazione alla newsletter è avvenuta con successo.'
                        ),
                        'danger' => array(
                            'label' => 'Messaggio di Errore',
                            'titolo' => 'Iscrizione alla newsletter',
                            'message' => 'Si è verificato un errore inaspettato durante la registrazione dell\'account.'
                        ),
                        'warning' => array(
                            'label' => 'Email Già iscritta',
                            'titolo' => 'Iscrizione alla newsletter',
                            'message' => 'Il tuo indirizzo email risulta già iscritto alla nostra newsletter.'
                        )
                    )
                )
            ),
            "reservation" => array(
                "id" => "reservation",
                "name" => "Effettua Prenotazione", "limit_to_extra_function" => array("hotel", "camping"),
                "descr" => "Effettua una prenotazione per Hotel/Campeggi.",
                "fa-icon" => "fa-bed",
                "return_message" => array(
                    'show' => true,
                    'template' => $default_template_html,
                    'strings' => array(
                        'success' => array(
                            'label' => 'Messaggio di Successo',
                            'titolo' => 'Prenotazione',
                            'message' => 'La tua richiesta di prenotazione è stata inviata con successo alla struttura, verrai contattato nel più breve tempo possibile per un\'eventuale conferma'
                        ),
                        'danger' => array(
                            'label' => 'Messaggio di Errore',
                            'titolo' => 'Prenotazione',
                            'message' => 'Si è verificato un errore tecnico durante l\'invio della richiesta. Prova a effettuare una nuova richiesta oppure contatta direttamente la struttura'
                        )
                    )
                )
            ),
            "property_info" => array(
                "id" => "property_info",
                "name" => "Richiesta per Immobile", "limit_to_extra_function" => array("realestate"),
                "descr" => "Effettua una richiesta di informazioni per un Immobile",
                "fa-icon" => "fa-building-o",
                "return_message" => array(
                    'show' => true,
                    'template' => $default_template_html,
                    'strings' => array(
                        'success' => array(
                            'label' => 'Messaggio di Successo',
                            'titolo' => 'Email inviata correttamente',
                            'message' => 'La tua richiesta di informazioni è stata inviata con successo al consulente, verrai contattato nel più breve tempo possibile'
                        ),
                        'danger' => array(
                            'label' => 'Messaggio di Errore',
                            'titolo' => 'Errore durante l\'invio dell\'email',
                            'message' => 'Si è verificato un errore tecnico durante l\'invio della richiesta. Prova a effettuare una nuova richiesta oppure contatta direttamente l\'agenzia'
                        )
                    )
                )
            ),
            "generic_request" => array(
                "id" => "generic_request",
                "name" => "Richiesta Generica",
                "descr" => "Invia una richiesta generica di contatto.",
                "fa-icon" => "fa-book",
                "return_message" => array(
                    'show' => true,
                    'template' => $default_template_html,
                    'strings' => array(
                        'success' => array(
                            'label' => 'Messaggio di Successo',
                            'titolo' => 'Email inviata correttamente',
                            'message' => 'Email inviata correttamente! La ricontatteremo nel minor tempo possibile!'
                        ),
                        'danger' => array(
                            'label' => 'Messaggio di Errore',
                            'titolo' => 'Errore durante l\'invio dell\'email',
                            'message' => 'Si è verificato un errore durante l\'invio della mail. Se il problema persiste, la preghiamo di ricontattarci telefonicamente.'
                        )
                    )
                )
            ),
            "reply_to_mail" => array(
                "id" => "reply_to_mail",
                "name" => "Risposta Automatica",
                "descr" => "Permette di creare un email di risposta personalizzata.",
                "fa-icon" => "fa-reply",
                "return_message" => array(
                    'show' => false,
                    'template' => $default_template_html,
                    'strings' => array(
                        'success' => array(
                            'label' => 'Messaggio di Successo',
                            'titolo' => 'Email inviata correttamente',
                            'message' => 'Ti abbiamo appena inviato l\'email come da te richiesto.'
                        ),
                        'danger' => array(
                            'label' => 'Messaggio di Errore',
                            'titolo' => 'Errore durante l\'invio dell\'email',
                            'message' => 'Si è verificato un errore durante l\'invio della mail. Se il problema persiste, la preghiamo di ricontattarci telefonicamente.'
                        )
                    )
                )
            ),
            "login" => array(
                "id" => "login",
                "name" => "Login",
                "descr" => "Permette di far accedere l'utente.",
                "fa-icon" => "fa-user",
                "return_message" => array(
                    'show' => true,
                    'template' => $default_template_html,
                    'strings' => array(
                        'success' => array(
                            'label' => 'Messaggio di Successo',
                            'titolo' => 'Login effettuato',
                            'message' => 'Hai effettuato l\'accesso con successo, stai per essere reindirizzato...'
                        ),
                        'danger' => array(
                            'label' => 'Messaggio di Errore',
                            'titolo' => 'Errore durante il login',
                            'message' => 'La password o l\'email inserita non sono validi.'
                        )
                    )
                )
            ),
            "password_recovery" => array(
                "id" => "password_recovery",
                "name" => "Recupero Password",
                "descr" => "Invia un email di recupero password tramite l'email del cliente.",
                "fa-icon" => "fa-key",
                "return_message" => array(
                    'show' => true,
                    'template' => $default_template_html,
                    'strings' => array(
                        'success' => array(
                            'label' => 'Messaggio di Successo',
                            'titolo' => 'Email inviata',
                            'message' => 'Ti abbiamo inviato un email con il link per reimpostare la password'
                        ),
                        'danger' => array(
                            'label' => 'Messaggio di Errore',
                            'titolo' => 'Email non trovata',
                            'message' => 'L\'indirizzo email digitato non è stato trovato'
                        )
                    )
                )
            )
        );

        return $actions_checks;
    }

    /**
     * Salva l'invio del form nella cronologia
     *
     * @param $form_id integer L'ID del form
     * @param $user_ref string L'eventuale indirizzo email dell'utente
     * @param $fields array I campi del form
     * @param $form_data array I parametri inviati
     * @param $form_results array L'array con i risultati del submit
     * @param $lang_id L'ID della lingua utilizzata
     *
     ** @return array|mixed
     */
    public function storeFormSubmission($form_id, $user_ref, $fields, $form_data, $form_results, $lang_id) {

        foreach($form_data as $nome_campo => $data) {
            $input_type = '';
            foreach ($fields as $input_info) {
                if ($input_info['field_id'] == $nome_campo) {
                    $input_type = $input_info['input_type'];
                }
            }

            if($input_type == 'password') {
                $form_data[$nome_campo] = '***';
            } else if($input_type == 'checkbox' || ($input_type == '' && $data == '')) {
                $form_data[$nome_campo] = 'Selezionato';
            }
        }

        $form_results = json_encode($form_results);
        $form_data = json_encode($form_data);

        return $this->MSFrameworkDatabase->pushToDB("INSERT INTO forms_histories (form_id, user_ref, form_data, form_results, lang) VALUES (:form_id, :user_ref, :form_data, :form_results, :lang_id)", array(":form_id" => $form_id, ":user_ref" => $user_ref, ":form_data" => $form_data, ":form_results" => $form_results, ":lang_id" => $lang_id));
    }

    /**
     * Aggiunge una nuova richiesta all'interno del database.
     *
     * @param $nome string Il nome di chi ha effettuato la richiesta
     * @param $contatto string Un metodo di contatto (email/telefono)
     * @param $messaggio string Il messaggio della richiesta
     * @param $form_data array L'array completo dei dati gestiti dalla richiesta (es: tutti i dati del form) ES: array("Oggetto" => "Il mio oggetto", "Città Mittente" => "Madrid"). Tali dati verranno visualizzati sottoforma di "Informazioni addizionali" nel backoffice
     * @param $lang integer La lingua di origine
     * @param $type string Il tipo di richiesta (generic|property)
     * @param $owner string Se la richiesta deve essere visibile solo ad una persona in particolare, specificare qui l'ID
     * @param $owner string Un array contenente informazioni extra da aggiungere alla richiesta
     *
     ** @return array|mixed
     */
    public function addGenericRequest($nome, $contatto, $messaggio, $form_data, $lang = USING_LANGUAGE_CODE, $type = "generic", $owner = "", $extra_info = array()) {
        if($nome != "" && $contatto != "") {
            if($form_data != "" && !is_array($form_data)) {
                return false;
            }

            return $this->MSFrameworkDatabase->pushToDB("INSERT INTO richieste (type, nome, contatto, messaggio, form_data, creation_time, lang, owner_id, extra_info) VALUES (:type, :nome, :contatto, :messaggio, :form_data, :creation_time, :lang, :owner, :extra_info)", array(":type" => $type, ":nome" => $nome, ":contatto" => $contatto, ":messaggio" => $messaggio, ":form_data" => json_encode($form_data), ":creation_time" => time(), ":lang" => $lang, ":owner" => $owner, ":extra_info" => json_encode($extra_info)));
        }

        return false;
    }

    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente alle tipologie di campo del form
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getFieldTypes($key = "") {
        $ary = array(
            "0" => "Input",
            "1" => "Textarea",
            "2" => "Select",
        );

        if($key != "") {
            return $ary[$key];
        } else {
            return $ary;
        }
    }

    /**
     * Questa funzione si occupa di calcolare il prezzo del preventivo
     *
     * @param int $form_id L'ID del form per il quale si intende calcolare il preventivo
     * @param array $form_data I valori del form compilato dall'utente. E' possibile utilizzare anche la funzione serializeArray di jQuery per ottenere un array compatibile. es: array("name" => "id del campo del form", "value" => "il valore impostato dall'utente");
     * @param integer $lang L'ID della lingua selezionata dal cliente
     *
     * @return float|int|mixed
     */
    public function calculateQuote($form_id, $form_data, $lang_id) {
        global $firephp, $MSFrameworki18n;

        $form_details = $this->getFormDetails($form_id, "fields, actions_values, actions")[$form_id];
        $actions_decoded = json_decode($form_details['actions'], true);
        if(!in_array('quote', $actions_decoded)) {
            return false;
        }

        $form_data_indexed = array();
        foreach($form_data as $k => $cur_form_row) {
            if(is_array($cur_form_row)) $form_data_indexed[$cur_form_row['name']] = $cur_form_row['value'];
            else $form_data_indexed[$k] = $cur_form_row;
        }

        $form_fields = json_decode($form_details['fields'], true);
        $cur_actions_values = json_decode($form_details['actions_values'], true);
        $form_quote = $cur_actions_values['quote'];

        $base_price = $form_quote['base_price'];
        $conditions = $form_quote['conditions'];

        $summary_list = array();

        foreach($conditions as $condition) { //questi sono i box principali (.actionDuplication), che contengono le varie condizioni principali (che a loro volta contengono le varie sotto-condizioni)
            $do_action = false;
            $condition_action = $condition['action'];
            $rule_concat = "";
            foreach($condition as $sub_conditionK => $sub_conditionV) { //questi sono i box delle condizioni principali (.quoteDuplication), che  contengono le varie sotto-condizioni
                if($sub_conditionK === "action") {
                    continue;
                }

                $cur_cond_elements = count($sub_conditionV)-1;


                foreach($sub_conditionV as $ruleK => $ruleV) { //questi sono i box delle sotto-condizioni (.subconditionDuplication)
                    if($ruleK === "concat") {
                        continue;
                    }

                    if($ruleK+1 == $cur_cond_elements) { //è l'ultimo giro per questo gruppo di condizioni!
                        if(($rule_concat == "and" && $do_action == false) || ($rule_concat == "or" && $do_action == true)) {
                            continue 2;
                        }
                    }

                    switch($ruleV['operator']) {
                        case "equal":
                            $do_action = ($form_data_indexed[$ruleV['field']] == $ruleV['value']);
                        break;

                        case "not_equal":
                            $do_action = ($form_data_indexed[$ruleV['field']] != $ruleV['value']);
                        break;

                        case "is_more_than":
                            $do_action = ($form_data_indexed[$ruleV['field']] > $ruleV['value']);
                        break;

                        case "is_less_than":
                            $do_action = ($form_data_indexed[$ruleV['field']] < $ruleV['value']);
                        break;

                        case "is_checked":
                            $do_action = ($form_data_indexed[$ruleV['field']] == "on");
                        break;

                        case "is_not_checked":
                            $do_action = ($form_data_indexed[$ruleV['field']] == "");
                        break;
                    }

                    $rule_concat = $sub_conditionV['concat'];

                    if(($ruleV['concat'] == "and" && $do_action == false) || ($ruleV['concat'] == "or" && $do_action == true)) {
                        continue 2;
                    }
                }
            }

            if($do_action) {
                switch($condition_action['type']) {
                    case "discount_percentage":
                        $base_price -= ($base_price*$condition_action['value'])/100;
                        $single_price = -abs(($base_price*$condition_action['value'])/100);
                    break;

                    case "discount_eur":
                        $base_price -= $condition_action['value'];
                        $single_price = -abs($condition_action['value']);
                    break;

                    case "supplement_percentage":
                        $base_price += ($base_price*$condition_action['value'])/100;
                        $single_price = ($base_price*$condition_action['value'])/100;
                    break;

                    case "supplement_eur":
                        $base_price += $condition_action['value'];
                        $single_price = $condition_action['value'];
                    break;

                    case "formula":
                        $action_value = str_replace("[BASE]", $base_price, $condition_action['value']);

                        foreach($form_data as $id => $value) {
                            $action_value = str_replace("[" . $id . "]", $value, $action_value);
                        }

                        $action_value = preg_replace('/\s+/', '', $action_value);
                        $number = '(?:\d+(?:[,.]\d+)?|pi|π)'; // What is a number
                        $functions = '(?:sinh?|cosh?|tanh?|abs|acosh?|asinh?|atanh?|exp|log10|deg2rad|rad2deg|sqrt|ceil|floor|round)'; // Allowed PHP functions
                        $operators = '[+\/*\^%-]'; // Allowed math operators
                        $regexp = '/^(('.$number.'|'.$functions.'\s*\((?1)+\)|\((?1)+\))(?:'.$operators.'(?2))?)+$/'; // Final regexp, heavily using recursive patterns

                        if(preg_match($regexp, $action_value) || 1) { //Todo: Con la regExp viene bloccato l'IF, altrimenti funziona.
                            $new_price = eval('return '.$action_value.';');
                            $single_price = eval('return '.$action_value.';') - $base_price;
                            if($new_price) $base_price = eval('return '.$action_value.';');
                        }
                    break;
                }

                $MSFrameworki18n->user_selected_lang = $lang_id;

                $summary_list[] = array(
                    'ref' => $MSFrameworki18n->getFieldValue($condition_action['ref']),
                    'price' => $single_price
                );
            }
        }

        return json_encode(array('total' => $base_price, 'summary' => $summary_list));
    }
}
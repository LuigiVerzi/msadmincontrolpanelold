<?php
/**
 * MSFramework
 * Date: 17/02/18
 */

namespace MSFramework;

class mailBox {

    public $lastImapPath = '';

    /**
     *
     * @throws \phpmailerException
     */
    public function __construct() {
        global $MSFrameworkDatabase, $MSFrameworkCMS, $MSFrameworkUsers, $MSFrameworki18n;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
    }

    public function getFolders() {
        $folders = array(
            'Inbox' => array(
                'name' => 'Posta in arrivo',
                'icon' => 'fa fa-inbox'
            ),
            'Sent' => array(
                'name' => 'Posta inviata',
                'icon' => 'fa fa-envelope-o'
            ),
            'Trash' => array(
                'name' => 'Cestino',
                'icon' => 'fa fa-trash'
            )
        );

        return $folders;

    }

    /**
     * Restituisce un array associativo (ID Template => Dati Template) con i dati relativi ai template.
     *
     * @param $id L'ID del template (stringa) o dei template (array) richiesti (se vuoto, vengono recuperati i dati di tutti i template)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getTemplateDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = "";
        $append_where_ary = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM mailbox__templates WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ")", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Verifica se un particolare file è condiviso da più moduli (ad esempio, il modulo template, il modulo lista campagne ed il modulo campagne inviate) escudendo dalla ricerca il modulo/record corrente
     *
     * @param $filename string Il nome del file da controllare
     * @param $exclude_table string Il nome della tabella da escludere dalla ricerca
     * @param $exclude_id integer L'ID del record da escludere dalla ricerca
     *
     * @return bool
     */
    public function isSharedAttachment($filename, $exclude_table, $exclude_id) {
        if($filename == "" || $exclude_table == "" || $exclude_id == "") {
            return null;
        }

        $tables_to_check = array("mailbox__templates");
        foreach($tables_to_check as $table) {
            $exclude_id_str = "";
            $exclude_id_ary = array();

            if($exclude_table == $table) {
                $exclude_id_str = " AND id != :id ";
                $exclude_id_ary[':id'] = $exclude_id;
            }

            if($this->MSFrameworkDatabase->getCount("SELECT id FROM $table WHERE attachments LIKE :filename " . $exclude_id_str, array_merge($exclude_id_ary, array(":filename" => "%" . $filename . "%"))) != 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Sincronizza le email in entrata
     */
    public function sync() {

        foreach($this->getMailBoxServers() as $server) {
            $serverDetails = $server['data'];

            if ($serverDetails['username'] == "" || $serverDetails['bmh_password'] == "" || $serverDetails['bmh_server'] == "") {
                continue;
            }

            $con = $this->getImapConnection($serverDetails, '');
            $imapDirs = imap_list($con, $this->lastImapPath, "*");
            imap_close($con);

            $replacePath = $this->lastImapPath;

            foreach($imapDirs as $folder) {

                $folderSlug = str_replace($replacePath, '', str_replace($replacePath . '.', '', $folder));

                $con = $this->getImapConnection($serverDetails, $folderSlug);

                if(in_array($folderSlug, array('INBOX.[FW360] Inviate'))) {
                    $folderSlug = 'Sent';
                }

                if(!$con) continue;

                $range = imap_sort($con, SORTDATE, 1);
                $range = array_slice($range, 0, 500);

                foreach ($range as $i) {

                    $msg = array_values(imap_fetch_overview($con, $i))[0];
                    $raw = imap_fetchbody($con, $i, "");

                    if (!$raw) continue;

                    $email = new \EmailUtils\PlancakeEmailParser($raw);

                    $sender_name = '';
                    if(preg_match_all('/"{0,1}([^<"]+)"{0,1}\s<([^<]+)>/m', $email->getHeader('from'), $matches)) {
                        $sender_name = trim($matches[1][0], '"');
                        $sender_email = trim($matches[2][0], '"<');
                    } else {
                        $sender_email = $email->getHeader('from');
                    }

                    $receiver_name = '';
                    if(preg_match_all('/"{0,1}([^<"]+)"{0,1}\s<([^<]+)>/m', $email->getHeader('to'), $matches)) {
                        $receiver_name = trim($matches[1][0], '"');
                        $receiver_email = trim($matches[2][0], '"<');
                    } else {
                        $receiver_email = $email->getHeader('to');
                    }

                    $ary_to_insert = array(
                        'message_id' => trim(trim($msg->message_id), '<>'),
                        'server_id' => $server['id'],
                        "body" => $email->getHTMLBody(),
                        "sender_email" => $sender_email,
                        "sender_info" => $sender_name,
                        "receiver_email" => $receiver_email,
                        "subject" => $email->getSubject(),
                        "read_status" => ($msg->seen ? '1' : '0'),
                        "folder" => ($folderSlug === 'INBOX' ? 'Inbox' : str_replace('INBOX.', '', $folderSlug)),
                        "reply_to_info" => $email->getHeader('Reply-to'),
                        "mail_date" => date('Y-m-d H:i:s', strtotime($msg->date))
                    );

                    if(!$this->MSFrameworkDatabase->getCount("SELECT message_id FROM mailbox__messages WHERE message_id = :message_id", array(':message_id' => trim(trim($msg->message_id), '<>')))) {
                        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($ary_to_insert, 'insert');
                        $this->MSFrameworkDatabase->pushToDB("INSERT INTO mailbox__messages ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

                        $email_id = $this->MSFrameworkDatabase->lastInsertId();

                        $attachments = $email->getAttachments();

                        if($attachments) {
                            foreach($attachments as $attachment) {

                                $file_data = base64_decode($attachment['file']);
                                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                                $mime_type = finfo_buffer($finfo, $file_data);

                                $ary_to_insert = array(
                                    'file_name' => $attachment['filename'],
                                    'data' => $attachment['file'],
                                    'email_id' => $email_id,
                                    'mime' => $mime_type
                                );

                                $stringForDB = $this->MSFrameworkDatabase->createStringForDB($ary_to_insert, 'insert');
                                $this->MSFrameworkDatabase->pushToDB("INSERT INTO mailbox__attachments ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
                            }
                        }

                    }

                }
            }
        }

        imap_close($con);
    }

    public function getMailBoxServers() {
        $ary_to_return = array();

        foreach((new \MSFramework\emails())->getSMTPDetails() as $server) {
            if($server['enable_mailbox'] == "1") {
                $ary_to_return[] = $server;
            }
        }

        return $ary_to_return;
    }

    public function getImapConnection($serverDetails, $clientDir = 'INBOX') {
        $host = '{' . $serverDetails['bmh_server'] . ':' . $serverDetails['bmh_port'] . '/' . $serverDetails['bmh_service'] . (!empty($serverDetails['bmh_soption']) ? '/' . $serverDetails['bmh_soption'] : '') . '}' . $clientDir;
        $this->lastImapPath = $host;
        $con = imap_open($host, $serverDetails['username'], $serverDetails['bmh_password']);
        return $con;
    }
}
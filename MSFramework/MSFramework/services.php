<?php
/**
 * MSFramework
 * Date: 07/03/18
 */

namespace MSFramework;


class services {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Servizio => Dati Servizio) con i dati relativi al servizio.
     *
     * @param $id L'ID del servizio (stringa) o dei servizi (array) richiesti (se vuoto, vengono recuperati i dati di tutti i servizi)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getServiceDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM servizi WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {

            if(json_decode($r['extra_fields'])) {
                $r['extra_fields'] = (new \MSFramework\utils())->formatExtraFieldsValues(json_decode($r['extra_fields'], true));
            }
            else $r['extra_fields'] = array();

            if(isset($r['extra_fields']['imposta'])) {
                $r['prezzo_formattato']  = (new \MSFramework\Fatturazione\imposte())->getPriceToShow($r['prezzo'], $r['extra_fields']['imposta']);
            }
            else {
                $r['prezzo_formattato']  = (new \MSFramework\Fatturazione\imposte())->getPriceToShow($r['prezzo'], 0);
            }

            $r['gallery_friendly'] = array();
            $service_gallery = json_decode($r['images'], true);
            if(is_array($service_gallery)) {
                foreach ($service_gallery as $file) {
                    $r['gallery_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SERVICES_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SERVICES_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_SERVICES_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }
            }

            //per mantenere la retrocompatibilità non posso creare un array tipo $r['gallery_friendly']['banner'] ma devo creare un array completamente diverso
            $r['gallery_friendly_banner'] = array();
            $service_gallery = json_decode($r['banner'], true);
            if(is_array($service_gallery)) {
                foreach ($service_gallery as $file) {
                    $r['gallery_friendly_banner'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SERVICES_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SERVICES_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_SERVICES_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }
            }

            $ary_to_return[$r['id']] = $r;

        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Servizio => Dati Servizio) con i dati relativi al servizio.
     *
     * @param $category L'ID della categoria
     *
     * @return array
     */
    public function getServicesIDsByCategory($category) {
        if($category != "") {
            if(!is_array($category)) {
                $category = array($category);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($category, "OR", "category");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" category != '' ", array());
        }
        $ary_to_return = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM servizi WHERE category != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[] = $r['id'];
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Servizio => Dati Servizio) con i dati relativi al servizio.
     *
     * @param $sector int L'ID del settore
     *
     * @return array
     */
    public function getServicesIDsBySector($sector) {
        if($sector != "") {
            if(!is_array($sector)) {
                $sector = array($sector);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($sector, "OR", "sector", 'LIKE', 'both', '', '"', '"');
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" sector != '' ", array());
        }
        $ary_to_return = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM servizi WHERE sector != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[] = $r['id'];
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria.
     *
     * @param $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM servizi_categorie WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {


            $r['gallery_friendly'] = array();
            $service_gallery = json_decode($r['images'], true);
            if(is_array($service_gallery)) {
                foreach ($service_gallery as $file) {
                    $r['gallery_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SERVICES_CATEGORY_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_CATEGORY_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SERVICES_CATEGORY_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_SERVICES_CATEGORY_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Settore => Dati Settore) con i dati relativi al settore.
     *
     * @param $id L'ID del settore (stringa) o dei settori (array) richiesti (se vuoto, vengono recuperati i dati di tutti i settori)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getSectorDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM servizi_settori WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {


            $r['gallery_friendly'] = array();
            $service_gallery = json_decode($r['images'], true);
            if(is_array($service_gallery)) {
                foreach ($service_gallery as $file) {
                    $r['gallery_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SERVICES_SECTOR_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_SECTOR_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SERVICES_SECTOR_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_SERVICES_SECTOR_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Portfolio => Dati Portfolio) con i dati relativi al portfolio.
     *
     * @param $id L'ID del portfolio (stringa) o dei portfolio (array) richiesti (se vuoto, vengono recuperati i dati di tutti i portfolio)
     * @param string $fields I campi da prelevare dal DB
     * @param int $limit_records Se impostato su un valore > 0, limita il numero di record prelevati dal DB
     * @param mixed $limit_by_service Se impostato, preleva solo i record appartenenti ad uno o più servizi indicati
     * @param mixed $limit_by_sector Se impostato, preleva solo i record appartenenti ad uno o più settori indicati
     * @param $just_active Se impostato su true, recupera solo le voci del portfolio attive
     *
     * @return array
     */
    public function getPortfolioDetails($id = "", $fields = "*", $limit_records = 0, $limit_by_service = false, $limit_by_sector = false, $just_active = true) {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = array("", array());
        if($limit_by_service !== false && $limit_by_service != "") {
            if(!is_array($limit_by_service)) {
                $limit_by_service = array($limit_by_service);
            }

            if(count($limit_by_service) > 0) {
                $same_db_string_ary_service = $this->MSFrameworkDatabase->composeSameDBFieldString($limit_by_service, "OR", "service");

                $append_where[0] .= "AND (" . $same_db_string_ary_service[0] . ")";
                $append_where[1] = array_merge($append_where[1], $same_db_string_ary_service[1]);
            }
        }

        if($limit_by_sector !== false && $limit_by_sector != "") {
            if(!is_array($limit_by_sector)) {
                $limit_by_sector = array($limit_by_sector);
            }

            if(count($limit_by_sector) > 0) {
                $same_db_string_ary_service = $this->MSFrameworkDatabase->composeSameDBFieldString($limit_by_sector, "OR", "sector");

                $append_where[0] .= "AND (" . $same_db_string_ary_service[0] . ")";
                $append_where[1] = array_merge($append_where[1], $same_db_string_ary_service[1]);
            }
        }

        if($just_active) {
            $append_where[0] .= " AND is_active = '1' ";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM servizi_portfolio WHERE id != '' AND (" . $same_db_string_ary[0] . ") $append_where[0] ORDER BY id DESC " . ($limit_records > 0 ? "LIMIT $limit_records" : ""), array_merge($same_db_string_ary[1], $append_where[1])) as $r) {
            $r['gallery_friendly'] = array();

            $service_gallery = json_decode($r['images'], true);
            if(is_array($service_gallery)) {
                foreach ($service_gallery as $file) {
                    $r['gallery_friendly']['images'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                    );
                }
            }

            $service_gallery = json_decode($r['web_images'], true);
            if(is_array($service_gallery)) {
                foreach ($service_gallery as $file) {
                    $r['gallery_friendly']['web_images'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                    );
                }
            }

            $service_gallery = json_decode($r['mob_images'], true);
            if(is_array($service_gallery)) {
                foreach ($service_gallery as $file) {
                    $r['gallery_friendly']['mob_images'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SERVICES_PORTFOLIO_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                    );
                }
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce l'ID della categoria associata al portfolio. L'ID è ricavato dalla tipologia del servizio associato al portfolio
     *
     * @param $id l'ID del portfolio
     *
     * @return bool
     */
    public function getPortfolioCategory($id) {
        if($id == "") return false;

        $service_id = $this->getPortfolioDetails($id, "service")[$id]['service'];

        return $this->getServiceDetails($service_id, "category")[$service_id]['category'];
    }

    /**
     * Ottiene l'URL della categoria
     *
     * @param $id L'ID della categoria per la quale si desidera l'URL
     *
     * @return string
     */
    public function getCategoryURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl;

        if(is_array($id) && isset($id['slug'])) $page_det = $id;
        else $page_det = $this->getCategoryDetails($id, "slug")[$id];

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";
    }

    /**
     * Ottiene l'URL del servizio
     *
     * @param $id L'ID del servizio per il quale si desidera l'URL
     *
     * @return string
     */
    public function getServiceURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl;

        if(is_array($id) && isset($id['slug'])) $page_det = $id;
        else $page_det = $this->getServiceDetails($id, "slug")[$id];

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";
    }

    /**
     * Ottiene l'URL del portfolio
     *
     * @param $id L'ID del portfolio per il quale si desidera l'URL
     *
     * @return string
     */
    public function getPortfolioURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl;

        if(is_array($id) && isset($id['slug'])) $page_det = $id;
        else $page_det = $this->getPortfolioDetails($id, "slug")[$id];

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";
    }
}
<?php
/**
 * MSFramework
 * Date: 24/02/18
 */

namespace MSFramework;

class pages {

    public $privatePages = array();

    public function __construct() {
        global $firephp, $MSFrameworkDatabase, $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;

        $this->url_cache = array();

        $this->privatePages = array(
            'main' => array(
                'title' => 'Generali',
                'icon' => 'fa-file',
                'background' => '#001921',
                'color' => 'white',
                'pages' => array(
                    'home-page' => array(
                        'title' => 'Home page',
                        'action' => 'getHomePage',
                        'description' => 'La pagina principale del sito web',
                        'blocks' => array(),
                        'exec' => function($id) {},
                    ),
                    'login' => array(
                        'title' => 'Login cliente',
                        'action' => 'getCustomerSignIn',
                        'description' => 'La pagina di login del cliente',
                        'blocks' => array(),
                        'exec' => function() {},
                        'slug' => array(
                            'default' => 'login'
                        )
                    ),
                    'registration' => array(
                        'title' => 'Registrazione cliente',
                        'action' => 'getCustomerSignUp',
                        'description' => 'La pagina di registrazione del cliente',
                        'blocks' => array(),
                        'exec' => function() {},
                        'slug' => array(
                            'default' => 'registration',
                        )
                    )
                )
            ),
            'beautycenter' => array(
                'title' => 'Centro Estetico',
                'icon' => 'fa-heart',
                'background' => '#bf436d',
                'color' => 'white',
                'limit_to_extra_function' => 'beautycenter',
                'pages' => array(
                    'beautycenter-offer' => array(
                        'title' => 'Pagina Offerta',
                        'action' => 'getOffer',
                        'description' => 'La pagina con i dettagli dell\'offerta',
                        'shortcodes' => array(
                            "{offer-title}" => array(
                                'description' => "Il titolo dell'offerta",
                                'value' => function($currentPageDetails) {
                                    Global $MSFrameworki18n;
                                    return $MSFrameworki18n->getFieldValue($currentPageDetails['nome']);
                                }
                            ),
                            "{offer-description}" => array(
                                'description' => "La descrizione dell'offerta",
                                'value' => function($currentPageDetails) {
                                    Global $MSFrameworki18n;
                                    return $MSFrameworki18n->getFieldValue($currentPageDetails['short_content']);
                                }
                            ),
                            "{offer-details}" => array(
                                'description' => "I dettagli dell'offerta",
                                'value' => function($currentPageDetails) {
                                    Global $MSFrameworki18n;
                                    return $MSFrameworki18n->getFieldValue($currentPageDetails['content']);
                                }
                            ),
                            "{offer-price}" => array(
                                'description' => "Il prezzo dell'offerta",
                                'value' => function($currentPageDetails) {
                                    return number_format($currentPageDetails['prezzo'], 2, ',', '.');
                                }
                            ),
                            "{offer-expiration}" => array(
                                'description' => "Data di scadenza dell'offerta",
                                'value' => function($currentPageDetails) {
                                    return ($currentPageDetails['scadenza'] ? date($currentPageDetails['scadenza']) : '');
                                }
                            ),
                            "{offer-image}" => array(
                                'description' => "L'URL dell'immagine dell'offerta",
                                'value' => function($currentPageDetails) {
                                    return $currentPageDetails['gallery_friendly'][0]['html']['main'];
                                }
                            ),
                            "{offer-images}" => array(
                                'description' => "L'URL dell'immagine dell'offerta",
                                'value' => function($currentPageDetails) {
                                    $images_list = array();
                                    foreach($currentPageDetails['gallery_friendly'] as $image) {
                                        $images_list[] = $image['html']['main'];
                                    }
                                    return implode(',', $images_list);
                                }
                            ),
                        ),
                        'exec' => function($id) {
                            Global $MSFrameworki18n;
                            $pageDetails = (new \MSFramework\BeautyCenter\offers())->getOfferDetails($id)[$id];
                            if($pageDetails) {
                                return $pageDetails;
                            }
                            return array();
                        },
                        'blocks'  => array(
                            array('beautycenter', 'offer')
                        )
                    )
                )
            ),
            'ecommerce' => array(
                'title' => 'Ecommerce',
                'icon' => 'fa-shopping-cart',
                'background' => '#9c27b0',
                'color' => 'white',
                'limit_to_extra_function' => 'ecommerce',
                'pages' => array(
                    'product_detail' => array(
                        'title' => 'Dettaglio prodotto',
                        'description' => 'La pagina con i dettagli del prodotto',
                        'action' => 'getEcommerceProduct',
                        'have_seo' => true,
                        'shortcodes' => array(
                            "{product-id}" => array(
                                'description' => "L'ID del prodotto",
                                'value' => function($currentPageDetails) {
                                    return $currentPageDetails['id'];
                                }
                            ),
                            "{product-type}" => array(
                                'description' => "La tipologia del prodotto [standard|variable]",
                                'value' => function($currentPageDetails) {
                                    return ($currentPageDetails["formattedAttributes"]["variations"] ? 'variable' : 'standard');
                                }
                            ),
                            "{product-title}" => array(
                                'description' => "Il nome del prodotto",
                                'value' => function($currentPageDetails) {
                                    Global $MSFrameworki18n;
                                    return $MSFrameworki18n->getFieldValue($currentPageDetails['nome']);
                                }
                            ),
                            "{product-description}" => array(
                                'description' => "La descrizione del prodotto",
                                'value' => function($currentPageDetails) {
                                    Global $MSFrameworki18n;
                                    return $MSFrameworki18n->getFieldValue($currentPageDetails['short_descr']);
                                }
                            ),
                            "{product-details}" => array(
                                'description' => "I dettagli del prodotto",
                                'value' => function($currentPageDetails) {
                                    Global $MSFrameworki18n;
                                    return $MSFrameworki18n->getFieldValue($currentPageDetails['long_descr']);
                                }
                            ),
                            "{product-price}" => array(
                                'description' => "Il prezzo del prodotto",
                                'value' => function($currentPageDetails) {
                                    return implode('.', $currentPageDetails['prezzo']['shop_price']);
                                }
                            ),
                            "{product-offer-price}" => array(
                                'description' => "Il prezzo scontato del prodotto",
                                'value' => function($currentPageDetails) {
                                    return implode('.', $currentPageDetails['prezzo_promo']['shop_price']);
                                }
                            ),
                            "{product-offer-expiration}" => array(
                                'description' => "Data di scadenza dell'offerta",
                                'value' => function($currentPageDetails) {
                                    return ($currentPageDetails['prezzo_promo_expire'] ? date('Y-m-d H:i', $currentPageDetails['prezzo_promo_expire']) : '');
                                }
                            ),
                            "{product-image}" => array(
                                'description' => "L'URL dell'immagine principale del prodotto",
                                'value' => function($currentPageDetails) {
                                    return $currentPageDetails['gallery_friendly'][0]['html']['main'];
                                }
                            ),
                            "{product-images}" => array(
                                'description' => "Gli URL (separati da una virgola) delle immagini del prodotto",
                                'value' => function($currentPageDetails) {
                                    $images_list = array();
                                    foreach($currentPageDetails['gallery_friendly'] as $image) {
                                        $images_list[] = $image['html']['main'];
                                    }
                                    return implode(',', $images_list);
                                }
                            ),
                        ),
                        'exec' => function($id) {
                            Global $MSFrameworki18n;
                            $pageDetails = (new \MSFramework\Ecommerce\products())->getProductDetails($id)[$id];
                            if($pageDetails) {
                                return $pageDetails;
                            }
                            return array();
                        },
                        'blocks'  => array(
                            array('ecommerce', 'product')
                        )
                    ),
                    'product_category' => array(
                        'title' => 'Categoria prodotto',
                        'description' => 'La pagina categoria dei prodotti',
                        'action' => 'getEcommerceCategory',
                        'shortcodes' => array(
                            "{category-id}" => array(
                                'description' => "L'ID della categoria",
                                'value' => function($currentPageDetails) {
                                    return $currentPageDetails['id'];
                                }
                            ),
                            "{category-title}" => array(
                                'description' => "Il nome della categoria",
                                'value' => function($currentPageDetails) {
                                    Global $MSFrameworki18n;
                                    return $MSFrameworki18n->getFieldValue($currentPageDetails['nome']);
                                }
                            ),
                            "{category-description}" => array(
                                'description' => "La descrizione della categoria",
                                'value' => function($currentPageDetails) {
                                    Global $MSFrameworki18n;
                                    return $MSFrameworki18n->getFieldValue($currentPageDetails['descr']);
                                }
                            ),
                            "{category-details}" => array(
                                'description' => "I dettagli della categoria",
                                'value' => function($currentPageDetails) {
                                    Global $MSFrameworki18n;
                                    return $MSFrameworki18n->getFieldValue($currentPageDetails['long_descr']);
                                }
                            ),
                            "{category-image}" => array(
                                'description' => "L'URL dell'immagine principale della categoria",
                                'value' => function($currentPageDetails) {
                                    return $currentPageDetails['gallery_friendly']['images'][0]['html']['main'];
                                }
                            ),
                            "{category-banner}" => array(
                                'description' => "L'URL del banner della categoria",
                                'value' => function($currentPageDetails) {
                                    return $currentPageDetails['gallery_friendly']['banner'][0]['html']['main'];
                                }
                            ),
                            "{category-products-array}" => array(
                                'description' => "",
                                'value' => function($currentPageDetails) {
                                    return $currentPageDetails['currentSearch'];
                                }
                            ),
                        ),
                        'exec' => function($id) {
                            Global $MSFrameworki18n;
                            $pageDetails = (new \MSFramework\Ecommerce\categories())->getCategoryDetails($id)[$id];
                            if($pageDetails) {

                                $pageDetails['currentSearch'] = (new \MSFramework\Ecommerce\products())->getProductsList(array(
                                    'category' => $id,
                                    'limit' => 12,
                                    'page' => (isset($_GET['page']) ? $_GET['page'] : 1)
                                ));

                                return $pageDetails;
                            }
                            return array();
                        },
                        'blocks' => array(
                            array('ecommerce', 'category')
                        )
                    )
                )
            )
        );
    }

    /**
     * Ottiene i dati di una pagina privata
     * @param integer $id L'id della pagina (vedere $privatePages sopra)
     * @return array
     */
    public function getPrivatePageData($id) {
        $ary_to_return = array(
            'id' => '',
            'content' => '',
            'origin' => '',
            'last_update' => '',
            'details' => array()
        );

        $page_details = array();
        foreach($this->privatePages as $cat) {
            if(isset($cat['pages'][$id])) $page_details = $cat['pages'][$id];
        }

        if($page_details) {

            $ary_to_return['id'] = $id;

            $r = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM pagine_private WHERE id = :id", array(':id' => $id), true);

            if ($r) {
                $r['seo'] = (isset($r['seo']) && json_decode($r['seo']) ? json_decode($r['seo'], true) : array());
                $ary_to_return = $r;
            }

            $ary_to_return['details'] = $page_details;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene l'URL della pagina privata
     *
     * @param $id mixed L'ID della pagina privata per la quale si desidera ricevere lo slug
     *
     * @return string
     */
    public function getPrivatePageURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl, $MSFrameworkCMS;

        $page_url = '';

        $page_det = $this->getPrivatePageData($id);
        if($page_det && $page_det['slug']) {
            $page_url = $MSFrameworkCMS->getURLToSite() . $MSFrameworkUrl->getLangPrefix($MSFrameworki18n->user_selected_lang) . $MSFrameworki18n->getFieldValue($page_det['slug']);
        }

        return $page_url;
    }

    /**
     * Restituisce l'ID della pagina che corrisponde ad uno slug
     *
     * @param string $slug Lo slug della pagina
     *
     * @return integer
     */
    public function getPageIDBySlug($slug = "") {

        $page = $this->MSFrameworkDatabase->getAssoc("SELECT id FROM pagine WHERE id != '' AND slug LIKE :slug", array(':slug' => '%"' . $slug . '"%'), true);

        if($page) return $page['id'];
        else return false;
    }

    /**
     * Restituisce gli ID della pagina che corrispondono ad una tipologia
     *
     * @param string $type La tipologia della pagina
     *
     * @return array
     */
    public function getPageIDsByType($type = "") {

        $page_ids = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM pagine WHERE id != '' AND type LIKE :type", array(':type' => $type)) as $page) {
            $page_ids[] = $page['id'];
        }
        return $page_ids;
    }

    /**
     * Restituisce un array associativo (ID Pagina => Dati Pagina) con i dati relativi alla pagina
     *
     * @param string $id L'ID della pagina (stringa) o delle pagine (array) richieste (se vuoto, vengono recuperati i dati di tutte le pagine del sito). Aggiungendo "global-" prima dell'ID, i dati verranno prelevati dalla tabella "pagine" del FW
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getPageDetails($id = "", $fields = "*") {
        global $firephp;

        $get_from_global = array();

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $tmp_id = $id;
            foreach($id as $cur_idK => $cur_id) {
                if(strstr($cur_id, "global-")) {
                    unset($tmp_id[$cur_idK]);
                    $get_from_global[] = str_replace("global-", "", $cur_id);
                }
            }

            $id = $tmp_id;
        }

        $ary_to_return = array();
        if ($fields != "*" && !in_array('id', explode(',', str_replace(' ', '', $fields)))) {
            $fields .= ", id";
        }

        $page_types = $this->getTypes();

        foreach(array("pagine", "`" . FRAMEWORK_DB_NAME . "`.`pagine`") as $table) {

            $prepend_id = "";

            $ary_to_search = $id;
            if($table == "`" . FRAMEWORK_DB_NAME . "`.`pagine`") {
                $prepend_id = 'global-';
                $ary_to_search = $get_from_global;

                if($id && !$ary_to_search) continue;
            } else {
                if(!$id && $get_from_global) continue;
            }

            if($ary_to_search) {
                $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($ary_to_search, "OR", "id");
            } else {
                $same_db_string_ary = array(" id != '' ", array());
            }

            foreach ($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM $table WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
                $r['gallery_friendly'] = $this->getGallery($r['id']);
                $r['info_aggiuntive'] = (isset($r['info_aggiuntive']) && json_decode($r['info_aggiuntive']) ? json_decode($r['info_aggiuntive'], true) : array());

                $r['seo'] = (isset($r['seo']) && json_decode($r['seo']) ? json_decode($r['seo'], true) : array());

                $r['page_relation'] = ($r['info_aggiuntive']['page_relation'] ? $r['info_aggiuntive']['page_relation']['id'] : '');
                $r['page_relation_type'] = '';
                $r['page_relation_content'] = array();

                if (isset($page_types[$r['type']]) && isset($page_types[$r['type']]['related_to'])) {

                    if($r['page_relation'] === '' && $r['parent'] > 0) {
                        $parent_info = $this->getPageDetails($r['parent'], 'info_aggiuntive')[$r['parent']];
                        if($parent_info['page_relation'] &&  (int)$parent_info['page_relation'] > 0) {
                            $r['page_relation'] = $parent_info['page_relation'];
                        }
                    }

                    if ($r['page_relation'] !== '') {
                        $r['page_relation_type'] = $page_types[$r['type']]['related_to'];
                        $r['page_relation_content'] = $this->getTypesRelations($page_types[$r['type']]['related_to'])['get']($r['page_relation']);

                        // Se indicato sovrascrivo i valori della pagina con quelli dell'elemento collegato
                        if ($page_types[$r['type']]['hide_content'] || ($r['info_aggiuntive']['page_relation']['overwrite_content'] == "on" || $r['info_aggiuntive']['page_relation']['overwrite_content'] == "on")) {
                            $r = $this->getTypesRelations($page_types[$r['type']]['related_to'])['overwrite']($r);
                        }
                    }
                }

                $ary_to_return[$prepend_id . $r['id']] = $r;
            }
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Pagina => Dati Pagina) con i dati relativi alla pagina
     *
     * @param string $parent L'ID della pagina parent
     *
     * @return array
     */
    public function getPagesHierarchy($parent = '') {
        global $firephp;

        if($parent != "") {
            if (!is_array($parent)) {
                $parent = array($parent);
            }
        }

        $ary_to_return = array();

        if($parent) {
            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($parent, "OR", "parent");
        } else {
            $same_db_string_ary = array(" parent = '' ", array());
        }

        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT id, parent, titolo, slug, slug_divider FROM pagine WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $r['childrens'] = $this->getPagesHierarchy($r['id']);
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene la gallery della pagina, determinando se la gallery è "personalizzata" o "importata dal modulo gallery" e restituendo i valori e i percorsi pronti all'uso
     *
     * @param $id L'ID della pagina
     *
     * @return bool|array
     */
    public function getGallery($id) {
        if($id == "") {
            return false;
        }

        $page_details = $this->MSFrameworkDatabase->getAssoc("SELECT id, gallery FROM pagine WHERE id = :id", array(":id" => $id), true);
        $gallery_global = json_decode($page_details['gallery'], true);

        if($gallery_global['type'] == "0") {

            $imp_gallery = (new \MSFramework\gallery())->getGalleryDetails($gallery_global['gallery'], 'id, gallery')[$gallery_global['gallery']];
            return $imp_gallery['gallery_friendly'];

        } else if($gallery_global['type'] == "1") {

            $to_return = array();
            foreach($gallery_global['gallery'] as $file) {
                $to_return[] = array(
                    "html" => array(
                        "main" => UPLOAD_PAGEGALLERY_FOR_DOMAIN_HTML . $file,
                        "thumb" => UPLOAD_PAGEGALLERY_FOR_DOMAIN_HTML . "tn/" . $file
                    ),
                    "absolute" => array(
                        "main" => UPLOAD_PAGEGALLERY_FOR_DOMAIN . $file,
                        "thumb" => UPLOAD_PAGEGALLERY_FOR_DOMAIN . "tn/" . $file
                    ),
                );
            }

            return $to_return;
        }

        return false;
    }

    /**
     * Restituisce tutte le tipologie di pagina disponibili
     * Attraverso questa selezione, sarà scelto anche il modello HTML da applicare all'interno del frontoffice
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getTypes($key = "") {
        $types = array();

        // Controllo se trovo dei template vecchio stile
        if(file_exists(MAIN_SITE_FOLDERPATH . "models/pages/public/info.json")) {

            $contents = file_get_contents(MAIN_SITE_FOLDERPATH . "models/pages/public/info.json");
            $types = json_decode($contents, true);

            /* CONTROLLO SE VIENE USATO LO SHORT CONTENT */
            foreach ($types as $page_id => $page_type) {
                if ($page_id === '*') continue;

                if (file_exists(MAIN_SITE_FOLDERPATH . "models/pages/public/" . $page_id . ".php") && strpos(file_get_contents(MAIN_SITE_FOLDERPATH . "models/pages/public/" . $page_id . ".php"), 'short_content') !== false) {
                    $types[$page_id]['use_short_content'] = true;
                } else {
                    $types[$page_id]['use_short_content'] = false;
                }
            }

            $common_fields = array();
            if (isset($types["*"]) && isset($types["*"]["extra_fields"])) {
                $common_fields = $types["*"]["extra_fields"];
                unset($types["*"]);
            }

            if ($common_fields) {
                foreach ($types as $page_id => $page_type) {
                    if (!isset($page_type["extra_fields"])) $types[$page_id]["extra_fields"] = array();
                    $types[$page_id]["extra_fields"] = array_merge($types[$page_id]["extra_fields"], $common_fields);
                }
            }

        } else {

            // Cerco tra i template nuovo stile
            foreach(glob(MAIN_SITE_FOLDERPATH . "www/templates/*.php") as $tpl) {

                $tpl_content = file_get_contents($tpl);

                $tpl_id = end(str_replace(array('.php'), array(), explode('/', $tpl)));

                $tpl_name = $tpl_id;
                if(preg_match_all('/Name: ([^\n]+[a-z|A-Z|0-9]+)/m', $tpl_content, $match)) {
                    $tpl_name = trim($match[1][0]);
                }

                $related_to = '';
                if(preg_match_all('/Related To: ([^\n]+[a-z|A-Z|0-9|_]+)/m', $tpl_content, $match)) {
                    $related_to = trim($match[1][0]);
                }

                $types[$tpl_id] = array(
                    'nome' => $tpl_name,
                    'related_to' => $related_to,
                    'order' => ($tpl_id === 'standard' ? 0 : 1)
                );

            }

        }

        uasort($types, function ($item1, $item2) {
            return ($item1['order'] ? $item1['order'] > $item2['price'] : 0);
        });

        if(!$types) {
            $types['standard'] = array(
                'nome' => 'Standard'
            );

            $types['service'] = array(
                'nome' => 'Dettagli Servizio',
                'related_to' => "services"
            );

            $types['service-category'] = array(
                'nome' => 'Categoria Servizio',
                'related_to' => "service_category"
            );
        }

        if($key != "") {
            return $types[$key];
        } else {
            return $types;
        }
    }

    /**
     * Restituisce i valori collegabili alle pagine in base alla tipologia
     *
     * @param string $type Il modulo nella quale cercare i valori
     *
     * @return array|mixed
     */
    public function getTypesRelations($type = "") {

        $relations_array = array(
            "service_sector" => array(
                "nome" => 'Settore',
                "label" => 'A quale settore desideri collegare la pagina?',
                "module_id" => 'servizi_settori',
                "get" => function($current_id = "") {
                    $array_details = (new \MSFramework\services())->getSectorDetails($current_id, ($current_id == "" ? "id, nome" : "*"));

                    if($current_id === "") return $array_details;
                    else return $array_details[$current_id];
                },
                "overwrite" => function($currentPageDetails) {

                    $newPageDetails = $currentPageDetails;

                    $newPageDetails['gallery_friendly'] = $currentPageDetails['page_relation_content']['gallery_friendly'];
                    $newPageDetails['sottotitolo'] = $currentPageDetails['page_relation_content']['descr'];
                    //$newPageDetails['short_content'] = $currentPageDetails['page_relation_content']['descr_title'];
                    $newPageDetails['content'] = $currentPageDetails['page_relation_content']['long_descr'];

                    return $newPageDetails;
                }
            ),
            "service_category" => array(
                "nome" => 'Categoria Servizio',
                "label" => 'A quale categoria del servizio desideri collegare la pagina?',
                "module_id" => 'servizi_categorie',
                "get" => function($current_id = "") {
                    $array_details = (new \MSFramework\services())->getCategoryDetails($current_id, ($current_id == "" ? "id, nome" : "*"));

                    if($current_id === "") return $array_details;
                    else return $array_details[$current_id];
                },
                "overwrite" => function($currentPageDetails) {

                    $newPageDetails = $currentPageDetails;

                    $newPageDetails['gallery_friendly'] = $currentPageDetails['page_relation_content']['gallery_friendly'];
                    $newPageDetails['sottotitolo'] = $currentPageDetails['page_relation_content']['descr'];
                    //$newPageDetails['short_content'] = $currentPageDetails['page_relation_content']['descr_title'];
                    $newPageDetails['content'] = $currentPageDetails['page_relation_content']['long_descr'];

                    return $newPageDetails;
                }
            ),
            "services" => array(
                "nome" => 'Servizio',
                "label" => 'A quale servizio desideri collegare la pagina?',
                "module_id" => 'servizi',
                "get" => function($current_id = "") {
                    $array_details = (new \MSFramework\services())->getServiceDetails($current_id, ($current_id == "" ? "id, nome" : "*"));

                    if($current_id === "") return $array_details;
                    else return $array_details[$current_id];
                },
                "overwrite" => function($currentPageDetails) {

                    $newPageDetails = $currentPageDetails;

                    $newPageDetails['gallery_friendly'] = $currentPageDetails['page_relation_content']['gallery_friendly'];
                    $newPageDetails['sottotitolo'] = $currentPageDetails['page_relation_content']['descr'];
                    $newPageDetails['short_content'] = $currentPageDetails['page_relation_content']['descr_title'];
                    $newPageDetails['content'] = $currentPageDetails['page_relation_content']['long_descr'];

                    return $newPageDetails;
                }
            )
        );

        if($type !== "") {
            return $relations_array[$type];
        } else {
            return $relations_array;
        }
    }

    public function generateAddToPageButton() {

    }

    /**
     * Restituisce tutti gli IDS delle pagine relative alla relazione indicata
     *
     * @param string $relation Il modulo relativo
     * @param bool $only_parent Se impostato ritorna solo le pagine genitori
     * @param mixed $relation_ids Se impostati preleverà solamente le pagine che puntano a quei determinati ID
     *
     * @return array|mixed
     */
    public function getPageIDsByRelation($relation, $only_parent = false, $relation_ids = false) {

        $page_type = '';

        foreach($this->getTypes() as $type_id => $type_value) {
            if($type_value['related_to'] && $type_value['related_to'] == $relation) {
                $page_type = $type_id;
            }
        }

        $page_ids = array();

        $only_parent_sql = ($only_parent ? 'AND parent = 0' : '');
        $relation_ids_sql = '';

        if($relation_ids !== false) {

            if(!is_array($relation_ids)) $relation_ids = array($relation_ids);

            $relation_ids_sql = 'AND info_aggiuntive RLIKE \'{"page_relation":{"id":"(' . implode('|', $relation_ids) . ')"\'';
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM pagine WHERE type = :page_type $only_parent_sql $relation_ids_sql", array(':page_type' => $page_type)) as $page) {
            $page_ids[] = $page['id'];
        }

        return $page_ids;

    }

    /**
     * Recupera il modello di pagina da utilizzare come "contenitore" della pagina in questione.
     * Se il file richiesto non è presente nella cartella "models" verrà utilizzata la pagina "standard". Se l'ID non è impostato, verrà visualizzata l'index.
     *
     * @param $page_id integer L'ID della pagina in analisi
     * @param $page_type string La tipologia della pagina in analisi
     * @param $path string Il path default della index
     *
     * @return string
     */
    public function getModelPageURL($page_id, $page_type, $path = 'models/pages/private/index.php') {
        if($page_id) {

            if(file_exists(MAIN_SITE_FOLDERPATH . "models/pages/public/info.json")) {

                if (is_file(ABSOLUTE_SW_PATH . "models/pages/public/" . $page_type . ".php")) {
                    $path = 'models/pages/public/' . $page_type . '.php';
                } else {
                    $path = 'models/pages/public/standard.php';
                }

            } else if(file_exists(MAIN_SITE_FOLDERPATH . "www/templates/")) {

                if (is_file(ABSOLUTE_SW_PATH . "www/templates/" . $page_type . ".php")) {
                    $path = 'templates/' . $page_type . '.php';
                } else {
                    $path = 'templates/standard.php';
                }

            }

        }

        return ABSOLUTE_SW_PATH . $path;
    }

    /**
     * Effettua il parsing del testo della pagina alla ricerca di eventuali tag da convertire/modificare dinamicamente
     *
     * @param $content Il contenuto della pagina
     *
     * @return mixed
     */
    public function parsePageContent($content) {
        global $firephp, $MSFrameworki18n, $MSFrameworkParser, $MSFrameworkVisualBuilder;

        $content = $MSFrameworkVisualBuilder->draw($content);

        $clean_shortcodes = array (
            '<p>\s*\[' => '[',
            '<p dir=".">\s*\[' => '[',
            '\]<\/p>' => ']',
            '\]<br \/>' => ']',
            '<div>\s*\[' => '[',
            '<div dir=".">\s*\[' => '[',
            '\]\s*<\/div>' => ']'
        );

        foreach($clean_shortcodes as $regex=>$replace) {
            $content = preg_replace('/' . $regex . '/msU', $replace, $content);
        }

        $tag_attributes = array("id", "value", "path", "extra");

        $context = stream_context_create(array('http' => array('header'=> 'Cookie: ' . $_SERVER['HTTP_COOKIE']."\r\n")));

        foreach($this->getTagList() as $tK => $tV) {
            $start = 0;

            while(($inizio_tag=strpos(($content),"[" . $tK, $start)) !== false) {
                //alla fine di questo ciclo ci saranno "n" variabili $valore_<nome_attributo> contenenti il valore dell'attributo del tag
                //es: $valore_id, $valore_value ecc
                foreach($tag_attributes as $attr_name) {

                    $pos_inizio = "inizio_" . $attr_name;
                    $pos_fine = "fine_" . $attr_name;
                    $valore_tag = "valore_" . $attr_name;

                    if(strpos(strtoupper($content), strtoupper($attr_name)) !== false) {
                        $$pos_inizio = strpos(($content), strtoupper($attr_name) . '="', $inizio_tag) + strlen($attr_name) + 1;
                        $$pos_fine = strpos(($content), '"', $$pos_inizio + 1);
                        $$valore_tag = substr($content, $$pos_inizio + 1, ($$pos_fine - $$pos_inizio - 1));
                    }
                }

                $fine_tag = strpos(($content),']', $inizio_tag);
                $start = $fine_tag+1;

                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

                if($tK == "INCLUDE_PAGE") {
                    $currentPageDetails = $this->getPageDetails($valore_id, "id, type")[$valore_id];
                    if($currentPageDetails['type'] == "") {
                        continue;
                    }

                    $model_page_url = str_replace(ABSOLUTE_SW_PATH, "", $this->getModelPageURL($currentPageDetails['id'], $currentPageDetails['type']));
                    session_write_close();
                    $new_value = file_get_contents($protocol . $_SERVER['HTTP_HOST'] . ABSOLUTE_SW_PATH_HTML . $model_page_url . "?id=" . $valore_id . "&from_get_contents=1" . $MSFrameworki18n->getURLCodeParam(), false, $context);
                    session_start();
                } else if($tK == "LINK_TO_PAGE") {
                    $new_value = '<a href="' . $this->getURL($valore_id) . (isset($valore_extra) ? $valore_extra : '') .'">' . $valore_value . '</a>';
                } else if($tK == "PAGE_URL") {
                    $new_value = $this->getURL($valore_id);
                } else if($tK == "INCLUDE_BLOCK") {
                    if(!is_file(ABSOLUTE_SW_PATH . "models/blocks/" . $valore_path . ".php")) {
                        continue;
                    }

                    session_write_close();
                    $new_value = file_get_contents($protocol . $_SERVER['HTTP_HOST'] . ABSOLUTE_SW_PATH_HTML . "models/blocks/" . $valore_path . ".php?from_get_contents=1&id=" . $valore_id . (isset($valore_extra) && !empty($valore_extra) ? "&extra=" . $valore_extra : '') . $MSFrameworki18n->getURLCodeParam(), false, $context);
                    session_start();
                } else if($tK == "GDPR_DETAILS") {
                    if((new \MSFramework\modules())->checkIfIsActive('gdpr')) {
                        session_write_close();
                        $site_index = file_get_contents($protocol . $_SERVER['HTTP_HOST'] . ABSOLUTE_SW_PATH_HTML, false, $context);
                        session_start();

                        $gdpr_elements = (new \MSFramework\Modules\gdpr())->getGDPRElementsFromHTML($site_index);
                        $new_value = (new \MSFramework\Modules\gdpr())->composeGDPRText($gdpr_elements);
                    }
                }

                $content = substr_replace($content, $new_value, $inizio_tag, ($fine_tag-$inizio_tag+1));
            }
        }

        foreach($this->getShortcodesList() as $tK => $tV) {

            if(is_array($tV) && isset($tV['regex'])) {
                $regex = '/' . $tV['regex'] . '/msU';
                $content = preg_replace($regex, $tV['replace'], $content);
            }
        }

        return $content;
    }

    /**
     * Definisce e restituisce l'elenco dei tag disponibili ed utilizzabili nelle pagine
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     * @param string $format (array/html) il tipo di dato che deve restituire la funzione
     *
     * @return array|mixed|string
     */
    public function getTagList($key = "", $format = 'array') {
        $tags = array(
            "INCLUDE_PAGE" => array(
                "name" => "Includi altra pagina",
                "tag_ph" => '[INCLUDE_PAGE ID=""]',
            ),
            "INCLUDE_BLOCK" => array(
                "name" => "Includi blocco",
                "tag_ph" => '[INCLUDE_BLOCK PATH="" ID=""]',
            ),
            "divider" => 'divider',
            "LINK_TO_PAGE" => array(
                "name" => "Link ad altra pagina",
                "tag_ph" => '[LINK_TO_PAGE ID="" VALUE=""]',
            ),
            "PAGE_URL" => array(
                "name" => "URL di una pagina",
                "tag_ph" => '[PAGE_URL ID=""]',
            ),
            "divider" => 'divider',
            "GDPR_DETAILS" => array(
                "name" => "Dettagli GDPR",
                "tag_ph" => '[GDPR_DETAILS]',
            ),
        );

        if($key != "") {
            $to_return = $tags[$key];
        } else {
            $to_return = $tags;
        }

        if($format == "array") {
            return $to_return;
        } else if($format == "html") {
            if($key == "") {
                $html = "";
                foreach($to_return as $tK => $tV) {
                    $html .= "<li tag='" . $tV['tag_ph'] . "']><a>" . $tV['name'] . "</a></li>";
                }

                return $html;
            } else {
                return "<li tag='" . $to_return['tag_ph'] . "']><a>" . $to_return['name'] . "</a></li>";
            }
        }
    }

    /**
     * Definisce e restituisce l'elenco degli shortcodes personalizzati se disponibili
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     * @param string $format (array/html) il tipo di dato che deve restituire la funzione
     *
     * @return array|mixed|string
     */
    public function getShortcodesList($key = "", $format = 'array') {

        $filename = MAIN_SITE_FOLDERPATH . "models/pages/public/shortcodes.json";

        if(!file_exists($filename)) return false;

        $handle = fopen($filename, "r");
        $contents = fread($handle, filesize($filename));
        fclose($handle);

        $tags = json_decode($contents, true);

        if(!$tags) {
            return false;
        }

        if($key != "") {
            $to_return = $tags[$key];
        } else {
            $to_return = $tags;
        }

        if($format == "array") {
            return $to_return;
        } else if($format == "html") {
            if($key == "") {
                $html = "";
                foreach($to_return as $tK => $tV) {
                    $html .= "<li tag='" . $tV['tag_ph'] . "']><a>" . $tV['name'] . "</a></li>";
                }
                return $html;
            } else {
                return "<li tag='" . $to_return['tag_ph'] . "']><a>" . $to_return['name'] . "</a></li>";
            }
        }
    }

    /**
     * Ottiene l'URL della pagina
     *
     * @param $id mixed L'ID della pagina per la quale si desidera l'URL. Se viene passata una stringa, la ricerca viene effettuata per il "global_id" e non per "id".
     * @param $langClass mixed Se passato viene utilizzata a posto della classe MSFrameworki18n con lingua principale
     * @param $only_slug boolean Se passato ritorna solamente lo slug senza dominio
     *
     * @return string
     */
    public function getURL($id, $langClass = false, $only_slug = false) {
        global $MSFrameworki18n, $MSFrameworkUrl, $MSFrameworkCMS;

        if(!$langClass) {
            $langClass = $MSFrameworki18n;
        }

        $page_det = array();

        // Cerco se l'URL è già stato ottenuto in precedenza per evitare richieste superflue
        $cache_key = base64_encode((is_array($id) ? json_encode($id) : $id) . '_' . ($langClass ? $langClass->user_selected_lang : '_g'));
        if(isset($this->url_cache[$cache_key])) {
            return $this->url_cache[$cache_key];
        }

        if(is_array($id) && isset($id['slug'])) {
            $page_det = $id;
        } else if(!ctype_digit($id) && !is_numeric($id)) {
            foreach(array("pagine", "`" . FRAMEWORK_DB_NAME . "`.`pagine`") as $table) {
                $prepend = "";
                if($table == "`" . FRAMEWORK_DB_NAME . "`.`pagine`") {
                    $prepend = "global-";
                }

                $r = $this->MSFrameworkDatabase->getAssoc("SELECT id FROM $table WHERE global_id = :global_id", array(":global_id" => $id), true);

                if($r['id'] != "") {
                    $page_det = $this->getPageDetails($prepend . $r['id'], "slug, parent, slug_divider")[$prepend . $r['id']];
                    break;
                }
            }
        } else {
            $page_det = $this->getPageDetails($id, "slug, parent, slug_divider")[$id];
        }

        // Cerca eventuali pagine genitore
        $parent_prepend = '';
        if((int)$page_det['parent'] > 0) {

            foreach($this->getPageParents($page_det) as $parent_det) {

                // Controllo se è stato impostato un divisore slug nella pagina corrente o genitore
                $slug_divider = (!empty($page_det['slug_divider']) ? $page_det['slug_divider'] : $parent_det['slug_divider']);
                if(empty($slug_divider)) {
                    $slug_divider = '/';
                }

                $parent_prepend = $MSFrameworkUrl->cleanString($langClass->getFieldValue($parent_det['slug'])) . $slug_divider . $parent_prepend;
            }

        }

        $page_url = $parent_prepend . $MSFrameworkUrl->cleanString($langClass->getFieldValue($page_det['slug'])) . "/";

        if(!$only_slug) {
            $page_url = $MSFrameworkCMS->getURLToSite() . $MSFrameworkUrl->getLangPrefix($langClass->user_selected_lang) . $page_url;
        }

        // Salvo il link nella variabile cache per evitare future richieste
        $this->url_cache[$cache_key] = $page_url;

        return $page_url;
    }

    /**
     * Ottiene un array con la lista di pagine genitore
     *
     * @param $page_det array Un array con le info della pagina ottenute dal DB
     *
     * @return array
     */
    public function getPageParents($page_det) {

        $parents = array();

        for($parent_id = $page_det['parent']; (int)$parent_id > 0; ) {
            $parent_det = $this->getPageDetails($parent_id, "titolo, slug, parent, slug_divider")[$parent_id];
            $parents[$parent_id] = $parent_det;

            // Se la pagina genitore è figlia di qualche altra pagina allora continuo a cercare
            if((int)$parent_det['parent'] > 0) {
                $parent_id = $parent_det['parent'];
            } else {
                $parent_id = 0;
            }
        }

        return array_reverse($parents);

    }

    /**
     * Ottiene la lista di shortcodes
     * @param boolean $only_value Indica se ritornare solo il valore o se tutte le info
     *
     * @return array
     */
    public function getCommonShortcodes($only_value = false) {
        Global $MSFrameworki18n;
        $siteCMSData = $this->MSFrameworkCMS->getCMSData('site');
        $site_logos = json_decode($siteCMSData['logos'], true);

        $shortcodesList = array(

            /* LOGHI */
            "ms-site-logo" => array(
                'title' => 'Logo del sito',
                'value' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https:' : 'http:') . UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo']
            ),
            "ms-site-logo2" => array(
                'title' => 'Logo secondario',
                'value' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https:' : 'http:') . UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo_footer']
            ),

            /* INFO SITO */
            "ms-site-name" => array(
                'title' => 'Titolo del sito',
                'value' => $MSFrameworki18n->getFieldValue($siteCMSData['nome'])
            ),
            "ms-site-slogan" => array(
                'title' => 'Motto del sito',
                'value' => $MSFrameworki18n->getFieldValue($siteCMSData['motto'])
            ),
            "ms-site-description" => array(
                'title' => 'Descrizione lunga del sito',
                'value' => strip_tags($MSFrameworki18n->getFieldValue($siteCMSData['long_descr']))
            ),
            "ms-site-url" => array(
                'title' => 'Url sito web',
                'value' => rtrim($this->MSFrameworkCMS->getURLToSite(false, true), '/')
            ),

            /* CONTATTI SITO */
            "ms-geo-address" => array(
                'title' => 'Indirizzo azienda',
                'value' => $this->MSFrameworkCMS->getFullAddress()
            ),
            "ms-site-phone" => array(
                'title' => 'Telefono azienda',
                'value' => $siteCMSData['telefono']
            ),
            "ms-site-mobile" => array(
                'title' => 'Cellulare azienda',
                'value' => $siteCMSData['cellulare']
            ),
            "ms-site-email" => array(
                'title' => 'Email azienda',
                'value' => $siteCMSData['email']
            ),
            "ms-site-pec" => array(
                'title' => 'PEC azienda',
                'value' => $siteCMSData['pec']
            ),

            /* SOCIAL */
            "ms-social-instagram" => array(
                'title' => 'Profilo Instagram',
                'value' => $siteCMSData['instagram_profile']
            ),
            "ms-social-twitter" => array(
                'title' => 'Profilo Twitter',
                'value' => $siteCMSData['twitter_profile']
            ),
            "ms-social-facebook" => array(
                'title' => 'Profilo Facebook',
                'value' => $siteCMSData['fb_profile']
            ),
            "ms-social-linkedin" => array(
                'title' => 'Profilo Linkedin',
                'value' => $siteCMSData['linkedin_profile']
            ),
            "ms-social-youtube" => array(
                'title' => 'Canale YouTube',
                'value' => $siteCMSData['youtube_profile']
            ),
            "ms-social-skype" => array(
                'title' => 'ID Skype',
                'value' => $siteCMSData['skype_profile']
            )
        );

        $return_array = array();
        if($only_value) {
            foreach($shortcodesList as $shortcode => $s_v) {
                $return_array['{' . $shortcode . '}'] = $s_v['value'];
            }
        } else {
            foreach($shortcodesList as $shortcode => $s_v) {
                $return_array['{' . $shortcode . '}'] = $s_v;
            }
        }

        return $return_array;

    }
}
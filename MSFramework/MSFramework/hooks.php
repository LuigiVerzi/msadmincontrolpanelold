<?php
/**
 * MSAdminControlPanel
 * Date: 04/08/2019
 */

namespace MSFramework;

class hooks
{

    private $hooks = array(
        'user' => array(
            'registration' => array(
                'params' => array(
                    'user_id' => 'L\'ID del cliente registrato',
                    'user_email' => 'L\'indirizzo email del cliente registrato'
                ),
                'fn' => array()
            ),
            'login' => array(
                'params' => array(
                    'user_id' => 'L\'ID del cliente registrato',
                    'user_email' => 'L\'indirizzo email del cliente registrato'
                ),
                'fn' => array()
            )
        ),
        'newsletter' => array(
            'subscribe' => array(
                'params' => array(
                    'user_id' => 'L\'ID del cliente registrato',
                    'user_email' => 'L\'indirizzo email del cliente registrato'
                ),
                'fn' => array()
            )
        ),
        'ecommerce' => array(
            'checkout' => array(
                'before_CartSummary' => array(
                    'params' => array(
                        'cart_total' => 'Il totale del carrello'
                    ),
                    'fn' => array()
                ),
                'after_Payment' => array(
                    'params' => array(
                        'cart_total' => 'Il totale del carrello',
                        "user_id" => "L'ID dell'acquirente",
                        "order_id" => "L'ID dell'ordine"
                    ),
                    'fn' => array()
                )
            ),
            'buttons' => array(
                'before_AddToCart' => array(
                    'params' => array(
                        'product_id' => 'L\'ID del prodotto'
                    ),
                    'fn' => array()
                ),
                'after_AddToCart' => array(
                    'params' => array(
                        'product_id' => 'L\'ID del prodotto'
                    ),
                    'fn' => array()
                )
            )
        )
    );
    private $filters = array(
        'ecommerce' => array(
            'getCart' => array(
                'params' => array(
                    'cart_total' => 'Il totale del carrello'
                ),
                'fn' => array()
            )
        )
    );

    private $currentHooks = array();
    private $currentPath = '';
    private $currentMethod = 'hooks';

    public function __construct() {

    }

    /**
     * Imposta la path dell'hook da gestire.
     * Successivamente sarà possibile richiamare la funzione run o add.
     * @return $this
     */
    public function action() {

        $this->currentMethod = 'hooks';
        $this->currentPath = '$this->hooks';
        $this->currentHooks = $this->hooks;

        foreach(func_get_args() as $arg) {
            $this->currentPath .= "['" . $arg . "']";
            $this->currentHooks = $this->currentHooks[$arg];
        };

        return $this;
    }

    /**
     * Imposta la path del filtro da gestire.
     * Successivamente sarà possibile richiamare la funzione run o add.
     * @return $this
     */
    public function filter() {

        $this->currentMethod = 'filters';
        $this->currentPath = '$this->filters';
        $this->currentHooks = $this->filters;

        foreach(func_get_args() as $arg) {
            $this->currentPath .= "['" . $arg . "']";
            $this->currentHooks = $this->currentHooks[$arg];
        };

        return $this;
    }

    /**
     * Esegue la lista di Hook disponibili nella path precedentemente impostata
     * @param array $data Eventuali parametri da passare alla funzione
     * @return array I dati passati ed eventualmente formattati dai filtri
     */
    public function run($data = array()) {
        foreach($this->currentHooks['fn'] as $hook) {
            if($this->currentMethod === 'hooks') {
                $hook($data);
            } else {
                $data = $hook($data);
            }
        }
        return $data;
    }

    /**
     * Aggiunge una funzione da eseguire
     * @param object $function La funzione da far eseguire
     */
    public function add($function) {
        eval($this->currentPath . '[\'fn\'][] = $function;');
    }

}
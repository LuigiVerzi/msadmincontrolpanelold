<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-29
 */

namespace MSFramework\SaaS;

class subscriptions extends base {
    private $saas_id;

    function __construct($saas_id = "") {
        global $MSFrameworkSaaSEnvironments, $MSFrameworkSaaSBase;
        parent::__construct();

        $this->saas_id = ($saas_id == '' ? parent::getID() : $saas_id);

        $this->MSFrameworkSaaSEnvironments = $MSFrameworkSaaSEnvironments;
        $this->MSFrameworkSaaSBase = $MSFrameworkSaaSBase;
    }

    /**
     * Ottiene tutti gli abbonamenti disponibili per un determinato SaaS
     *
     * @param bool $exclude_hidden Se impostato su true, non recupera i dati degli abbonamenti nascosti
     *
     * @return mixed
     */
    public function getAvailSubscriptions($exclude_hidden = false) {
        $r = json_decode($this->MSFrameworkSaaSBase->getSaaSDataFromDB($this->saas_id, "subscriptions")[$this->saas_id]["subscriptions"], true);
        if(!$exclude_hidden) {
            return $r;
        } else {
            $final_ary = array();
            foreach($r as $subscriptionK => $subscription) {
                if ($subscription['hidden'] == "1") {
                    continue;
                }

                $final_ary[$subscription['id']] = $subscription;
            }

            return $final_ary;
        }

    }

    /**
     * Ottiene tutti i metodi di pagamento disponibili per un determinato SaaS
     *
     * @return array
     */
    public function getAvailPayMethods() {
        global $MSFrameworkFW;

        $r = json_decode($this->MSFrameworkSaaSBase->getSaaSDataFromDB($this->saas_id, "pay_methods")[$this->saas_id]["pay_methods"], true);
        $settings_data = $MSFrameworkFW->getCMSData('settings')['payMethods'];

        $return = array();
        foreach($r as $cur_pay) {
            $cur_id = $cur_pay['id'];
            if($cur_pay['enable'] == "1" && $settings_data[$cur_id]['enable'] == "1") {
                $return[$cur_id] = $settings_data[$cur_id];
            }
        }

        return $return;
    }

    /**
     * Ottiene i dettagli (prezzo, descrizione azioni rinnovo, testo pulsanti) di un abbonamento in base al contesto in cui ci si trova. Calcola anche le differenze di prezzo in base al tipo di abbonamento di partenza->destinazione
     *
     * @param $abb_id L'abbonamento per il quale ottenere il prezzo
     *
     * @return array
     * @throws \Exception
     */
    public function getDetailsInContext($abb_id) {
        $active_subs = $this->getAvailSubscriptions();
        $current_abb = $this->MSFrameworkSaaSEnvironments->getCurrentSubscription();
        $expiration = $this->MSFrameworkSaaSEnvironments->getExpirationDate();

        $user_abb_position = array_search($current_abb, array_keys($active_subs));
        $cur_abb_position = array_search($abb_id, array_keys($active_subs));
        $days_to_exp = $expiration->diff((new \DateTime()))->format("%a");

        $expiration_modifier = "+" . $active_subs[$abb_id]['durata']['value'] . " " . $active_subs[$abb_id]['durata']['type'];
        $expiration_str = $active_subs[$abb_id]['durata']['value'] . " " . str_replace(array("days", "months", "years"), array("giorn%", "mes?", "ann%"), $active_subs[$abb_id]['durata']['type']);
        $expiration_str = ($active_subs[$abb_id]['durata']['value'] == "1" ? str_replace(array("%", "?"), array("o", "e"), $expiration_str) : str_replace(array("%", "?"), array("i", "i"), $expiration_str));

        $composizione_prezzo = array();
        if(!$this->MSFrameworkSaaSEnvironments->isExpired()) {
            if($active_subs[$current_abb]['price'] == "0") { //l'abbonamento corrente è un abbonamento gratuito, non effettuo calcoli di maggiorazioni/sconti sul prezzo
                $button_text = "Effettua Upgrade";
                $price = $active_subs[$abb_id]['price'];
                $composizione_prezzo = array("base" => $active_subs[$abb_id]['price']);
                $will_expire = $expiration->modify($expiration_modifier)->format("d/m/Y");
                $explain_text = "Effettuando l'upgrade, pagherai il costo di rinnovo per " . $expiration_str . ". La data di scadenza sarà estesa di " . $expiration_str . " rispetto alla data di scadenza del tuo abbonamento corrente.";
            } else {
                if ($cur_abb_position > $user_abb_position) {
                    $button_text = "Effettua Upgrade";
                    $differenza_prezzo_giorni_residui = (($active_subs[$abb_id]['price']-$active_subs[$current_abb]['price'])/365)*($days_to_exp);
                    if($days_to_exp > 30) {
                        $price = $differenza_prezzo_giorni_residui;
                        $composizione_prezzo = array("differenza_giorni_residui" => $price);
                        $will_expire = $expiration->format("d/m/Y");
                        $explain_text = "Effettuando l'upgrade, pagherai solo la differenza per i giorni di abbonamento residui ed il tuo piano passerà alla classe superiore fino alla data di scadenza del tuo abbonamento attuale.";
                    } else {
                        $price = $differenza_prezzo_giorni_residui+$active_subs[$abb_id]['price'];
                        $composizione_prezzo = array("base" => $active_subs[$abb_id]['price'], "differenza_giorni_residui" => $differenza_prezzo_giorni_residui);
                        $explain_text = "Effettuando l'upgrade, pagherai la differenza per i giorni di abbonamento residui ed il costo di rinnovo per " . $expiration_str . ". La data di scadenza sarà estesa di " . $expiration_str . " rispetto alla data di scadenza del tuo abbonamento corrente.";
                        $will_expire = $expiration->modify($expiration_modifier)->format("d/m/Y");
                    }
                } else if ($cur_abb_position == $user_abb_position) {
                    $button_text = "Effettua Rinnovo";
                    $explain_text = "Effettuando il rinnovo non perderai i giorni di abbonamento residui. La data di scadenza sarà estesa di " . $expiration_str . " rispetto alla data di scadenza del tuo abbonamento corrente.";
                    $price = $active_subs[$abb_id]['price'];
                    $composizione_prezzo = array("base" => $active_subs[$abb_id]['price']);
                    $will_expire = $expiration->modify($expiration_modifier)->format("d/m/Y");
                } else if ($cur_abb_position < $user_abb_position) {
                    $button_text = "Effettua Downgrade";
                    $explain_text = "";
                    $price = $active_subs[$abb_id]['price'];
                    $composizione_prezzo = array("base" => $active_subs[$abb_id]['price']);
                    $will_expire = $expiration->format("d/m/Y");
                }
            }
        } else {
            $button_text = "Abbonati";
            $price = $active_subs[$abb_id]['price'];
            $composizione_prezzo = array("base" => $active_subs[$abb_id]['price']);
            $will_expire = (new \DateTime())->modify($expiration_modifier)->format("d/m/Y");
            $explain_text = "Il tuo abbonamento verrà rinnovato per " . $expiration_str . " a partire dalla data del pagamento.";
        }

        return array((new \MSFramework\Framework\payway())->formatPrice($price), $will_expire, $explain_text, $button_text, $composizione_prezzo);
    }
}
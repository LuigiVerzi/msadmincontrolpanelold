<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-29
 */

namespace MSFramework\SaaS;

/**
 * Questa classe contiene gli elementi di base per avviare il SaaS. Viene utilizzata ancora prima di istanziare una connessione al DB, quindi conterrà solo le funzioni elementari per i check preliminari
 *
 * Class SaaSBase
 * @package MSFramework\SaaS
 */
class base {
    private $domain;

    public function __construct($domain = '') {
        $this->domain = ($domain == '' ? $this->getCleanHost() : $domain);
    }

    /**
     * Ottiene i dati configurativi dei SaaS dal DB
     *
     * @param string $id L'ID del SaaS
     * @param string $fields I campi da prelevare
     *
     * @return array
     */
    public function getSaaSDataFromDB($id = '', $fields = '*') {
        if ($fields != "*" && !in_array('id', explode(',', str_replace(' ', '', $fields)))) {
            $fields .= ", id";
        }

        $MSFrameworkDatabase = new \PDO('mysql:host=localhost;dbname=' . FRAMEWORK_DB_NAME . ';charset=utf8', FRAMEWORK_DB_USER, FRAMEWORK_DB_PASS);

        $where = array('', array());
        if($id != "") {
            $where[0] = " AND id = :id ";
            $where[1] = array_merge($where[1], array(":id" => $id));
        }

        $domains = array();
        $ex = $MSFrameworkDatabase->prepare("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`saas__config` WHERE id != '' " . $where[0]);
        $ex->execute($where[1]);
        foreach($ex->fetchAll(\PDO::FETCH_ASSOC) as $r) {
            $domains[$r['id']] = $r;
        }

        return $domains;
    }

    /**
     * Restituisce l'elenco dei domini considerati SaaS
     *
     * @return array
     */
    public function getSaaSDomains() {
        $domains = array();

        foreach($this->getSaaSDataFromDB() as $r) {
            $domains[] = $r['domain'];
        }

        return $domains;
    }

    /**
     * Restituisce true se il dominio è considerato SaaS, false se non lo è
     *
     * @return bool
     */
    public function isSaaSDomain() {
        global $MSFrameworkSimulator;
        
        if($MSFrameworkSimulator->get() != null) {
            return ($MSFrameworkSimulator->get() == "saas");
        }

        return in_array($this->domain, $this->getSaaSDomains());
    }

    /**
     * Restituisce true se il database è considerato SaaS, false se non lo è
     *
     * @param $database Il database da controllare.
     *
     * @return bool
     */
    public function isSaaSDatabase($database) {
        if($database == "") {
            return false;
        }

        return stristr($database, "marke833__SaaS__");
    }

    /**
     * Ottiene l'ID del SaaS
     *
     * @return mixed
     */
    public function getID() {
        $data = $this->getSaaSDataFromDB('', 'id, domain');

        foreach($data as $current) {
            if($current['domain'] == $this->domain) {
                return $current['id'];
            }
        }
    }

    /**
     * Ottiene il valore dell'host "pulito"
     *
     * @param $url string L'URL da pulire (Lasciando vuoto ottiene quello del server)
     *
     * @return mixed
     */
    public function getCleanHost($url = '') {
        if($url === '') $url = $_SERVER['HTTP_HOST'];

        return str_replace(
            array("http://", "https://", "www.", ADMIN_URL),
            array("", "", "", ""),
            $url
        );
    }
}
<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-29
 */

namespace MSFramework\SaaS;

class environments extends base {
    private $env_id;

    /**
     * environments constructor.
     *
     * @param string $env_id L'ID dell'ambiente
     * @param string $domain Il dominio dell'abiente (utile, ad esempio, quando si effettuano operazioni sul front)
     */
    function __construct($env_id = "", $domain = "") {
        global $MSFrameworkCMS, $MSFrameworkDatabase, $MSFrameworkUsers;

        parent::__construct($domain);

        $this->env_id = ($env_id == '' ? $this->getCurrentEnvironmentID() : $env_id);

        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkUsers = $MSFrameworkUsers;
    }

    /**
     * Restituisce un array associativo (ID Environment => Dati Environment) con i dati relativi all'environment
     *
     * @param string $id L'ID dell'environment (stringa) o degli environment (array) richiesti (se vuoto, vengono recuperati i dati di tutti gli environment)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getEnvironmentDataFromDB($id = '', $fields = '*') {
        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*") {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Collega un indirizzo email all'ambiente del SaaS in modo da potergli associare il giusto db in fase di login
     *
     * @param $user_id L'ID dell'utente (verrà ricavata la mail)
     *
     * @return bool|mixed
     */
    public function linkUserToEnvironment($user_id) {
        $r_user = $this->MSFrameworkUsers->getUserDetails($user_id, 'email')[$user_id];
        $user_mail = $r_user['email'];

        if(!$this->isAlreadyRegistered($user_mail)) {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT child_accounts FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE id = :id", array(":id" => $this->env_id), true);
            $child_array = explode(',', $r['child_accounts']);
            if(array_search($user_mail, $child_array) === false) {
                $child_array[] = $user_mail;
            }

            return $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`saas__environments` SET child_accounts = :new_child WHERE id = :id", array(":new_child" => implode(',', $child_array), ':id' => $this->env_id));
        }

        return false;
    }

    /**
     * Scollega un indirizzo email dall'ambiente del SaaS.
     *
     * @param $user_id L'ID dell'utente (verrà ricavata la mail)
     *
     * @return bool|mixed
     */
    public function unlinkUserFromEnvironment($user_id) {
        $r_user = $this->MSFrameworkUsers->getUserDetails($user_id, 'email')[$user_id];
        $user_mail = $r_user['email'];

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT child_accounts FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE id = :id", array(":id" => $this->env_id), true);
        $child_array = explode(',', $r['child_accounts']);

        $k_to_unset = array_search($user_mail, $child_array);
        unset($child_array[$k_to_unset]);

        return $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`saas__environments` SET child_accounts = :new_child WHERE id = :id", array(":new_child" => implode(',', $child_array), ':id' => $this->env_id));
    }

    /**
     * Verifica se un determinato indirizzo email risulta già registrato come child o come owner per la stessa tipologia di SaaS che sta effettuando il controllo.
     *
     * @param $user_email L'indirizzo email da controllare
     * @param string $saas_id Il SaaS per il quale limitare la ricerca. Se vuoto, verrà limitata per il SaaS corrente.
     * @param string $exclude_environment_id L'ambiente da escludere dalla ricerca. Se vuoto, verrà escluso l'ambiente corrente.
     *
     * @return bool
     */
    public function isAlreadyRegistered($user_email, $saas_id = "", $exclude_environment_id = "") {
        $current_env = $this->getCurrentEnvironmentID();
        if($exclude_environment_id == "") $exclude_environment_id = $current_env;
        if($saas_id == "") $saas_id = $this->getID();

        $append_where[0] = " AND saas_id = :saas_id ";
        $append_where[1][':saas_id'] = $saas_id;

        return ($this->MSFrameworkDatabase->getCount("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE id != :id " . $append_where[0] . " AND (FIND_IN_SET(:email, child_accounts) OR user_email = :usermail)", array_merge($append_where[1], array(":id" => $exclude_environment_id, ":email" => $user_email, ":usermail" => $user_email))) == 0 ? false : true);
    }

    /**
     * Ottiene l'ID dell'ambiente per un determinato indirizzo email ed un determinato SaaS
     *
     * @param $user_email L'indirizzo email da controllare
     * @param string $saas_id Il SaaS per il quale limitare la ricerca. Se vuoto, verrà limitata per il SaaS corrente.
     *
     * @return int
     */
    public function getEnvIDByMail($user_email, $saas_id = "") {
        if($saas_id == "") $saas_id = $this->getID();

        $append_where[0] = " AND saas_id = :saas_id ";
        $append_where[1][':saas_id'] = $saas_id;

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE id != '' " . $append_where[0] . " AND (FIND_IN_SET(:email, child_accounts) OR user_email = :usermail)", array_merge($append_where[1], array(":email" => $user_email, ":usermail" => $user_email)), true);
        if($r['id'] == "") {
            return false;
        }

        return $r['id'];
    }

    /**
     * Ottiene l'ID del proprietario dell'environment
     *
     * @return bool
     */
    public function getOwnerID() {
        global $MSFrameworkUsers; //in fase di login, quando viene effettuato il controllo per l'abbonamento scaduto, non riesce a recuperare la variabile $this->MSFrameworkUsers

        return $MSFrameworkUsers->getIDFromEmail($this->getOwnerEmail());
    }

    /**
     * Ottiene la mail del proprietario dell'environment
     *
     * @return string
     */
    public function getOwnerEmail() {
        global $MSFrameworkUsers; //in fase di login, quando viene effettuato il controllo per l'abbonamento scaduto, non riesce a recuperare la variabile $this->MSFrameworkUsers

        return $this->getEnvironmentDataFromDB($this->env_id, "user_email")[$this->env_id]['user_email'];
    }

    /**
     * Restituisce l'ID dell'ambiente SaaS corrente
     *
     * @return mixed
     */
    public function getCurrentEnvironmentID() {
        $session_id = (isset($_SESSION['SaaS']['saas__environments']['id']) ? $_SESSION['SaaS']['saas__environments']['id'] : "");

        return $session_id;
    }

    /**
     * Ottiene gli ID (o i dettagli) di tutti gli ambienti appartenenti ad una determinata tipologia di SaaS
     *
     * @param $id L'ID del SaaS per il quale recuperare gli ambienti. Se vuoto, ottiene l'ID del tipo di SaaS corrente.
     * @param null $get_data Se diverso da null, recupera anche le colonne richieste dalla tabella saas__environments
     *
     * @return array
     */
    public function getEnvironmentsBySaaSID($id = "", $get_data = null) {
        if($id == "") $id = parent::getID();

        $return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE saas_id = :id", array(":id" => $id)) as $r) {
            $return[$r['id']] = $r;
            if($get_data != null) {
                $return[$r['id']] = $this->getEnvironmentDataFromDB($r['id'], $get_data)[$r['id']];
            }
        }

        return $return;
    }

    /**
     * Ottiene L'ID della tipologia di SaaS a cui appartiene l'ambiente
     *
     * @param $id L'ID dell'ambiente SaaS per il quale recuperare la tipologia di SaaS. Se vuoto, ottiene l'ID dell'ambiente corrente.
     *
     * @return array
     */
    public function getSaaSIDByEnvironment($id = "") {
        if($id == "") $id = $this->env_id;

        return $this->MSFrameworkDatabase->getAssoc("SELECT saas_id FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE id = :id", array(":id" => $id), true)['saas_id'];
    }

    /**
     * Crea un nuovo ambiente SaaS per il frontoffice.
     *
     * @param $front_domain Il dominio del front per il quale creare l'ambiente
     *
     * @return bool
     */
    public function createFrontEnvironment($front_domain) {
        $MSFrameworkEnvironments = new \MSFramework\SaaS\environments('front', $front_domain);
        $db_name = $MSFrameworkEnvironments->getDBName();

        $this->MSFrameworkDatabase->copyDB(DUMMY_DB_NAME, $db_name, false, true);

        //elimino dal database tutte le funzionalità extra (verranno configurate in seguito dall'admin)
        foreach (array_keys($this->MSFrameworkCMS->getExtraFunctionsDetails()) as $module) {
            $module_db_prefix = $this->MSFrameworkCMS->getExtraFunctionsDetails($module)['db_prefix'];
            if ($module_db_prefix != "") {
                foreach ($this->MSFrameworkDatabase->getFromDummyByPrefix($module_db_prefix) as $table) {
                    $this->MSFrameworkDatabase->query("DROP TABLE `" . $db_name . "`.`" . $table['TABLE_NAME'] . "`");
                }
            }
        }
    }

    /**
     * Crea un nuovo ambiente SaaS per un determinato utente. L'utente in questione acquisirà il livello di default selezionato nelle impostazioni del framework, che in teoria dovrebbe essere sempre "Admin".
     *
     * @param $nome Il nome dell'utente
     * @param $cognome Il cognome dell'utente
     * @param $email L'indirizzo email dell'utente
     * @param $password La password dell'utente
     * @param $livello Il livello dell'utente
     * @param string $cellulare Il numero di cellulare dell'utente
     * @param string $saas_id L'ID del SaaS per il quale creare l'ambiente. Se vuoto, viene creato per l'ID corrente
     *
     * @return bool
     */
    public function createNewEnvironment($nome, $cognome, $email, $password, $livello, $cellulare = '', $saas_id = '') {
        if(!$this->isAlreadyRegistered($email)) {
            if($saas_id == "") $saas_id = parent::getID();

            $MSFrameworkSubscriptions = new \MSFramework\SaaS\subscriptions($saas_id);
            $avail_subs = $MSFrameworkSubscriptions->getAvailSubscriptions();

            $cur_subscription = null;
            foreach($avail_subs as $sub) {
                if($sub['default_on_reg'] == "1") {
                    $cur_subscription = $sub['id'];
                    $durata = $sub['durata'];

                    $expiry_date = new \DateTime();
                    $expiry_date->modify("+" . $durata['value'] . " " . $durata['type']);

                    break;
                }
            }

            if($cur_subscription == null) {
                $cur_subscription = -1;
                $expiry_date = new \DateTime('now');
            }

            if($cur_subscription !== null) {
                $this->MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`saas__environments` (user_email, saas_id, expiration_date, registration_date, subscription) VALUES (:mail, :saas_id, :exp_date, :reg_date, :subscription)", array(":mail" => $email, ":saas_id" => $saas_id, ":subscription" => $cur_subscription, ":exp_date" => $expiry_date->format("Y-m-d"), ":reg_date" => (new \DateTime())->format("Y-m-d")));
                $got_env_id = $this->MSFrameworkDatabase->lastInsertId();

                $MSFrameworkEnvironments = new \MSFramework\SaaS\environments($got_env_id);
                $db_name = $MSFrameworkEnvironments->getDBName();

                $this->MSFrameworkDatabase->copyDB(DUMMY_DB_NAME, $db_name, false, true);

                //elimino dal database le funzionalità extra non richieste per questa tipologia di saas
                $producer_config = $this->MSFrameworkCMS->getCMSData('producer_config');
                $extra_functions_diffs = array_diff(array_keys($this->MSFrameworkCMS->getExtraFunctionsDetails()), json_decode($producer_config['extra_functions'], true));

                if (count($extra_functions_diffs) > 0) {
                    foreach ($extra_functions_diffs as $module) {
                        $module_db_prefix = $this->MSFrameworkCMS->getExtraFunctionsDetails($module)['db_prefix'];
                        if ($module_db_prefix != "") {
                            foreach ($this->MSFrameworkDatabase->getFromDummyByPrefix($module_db_prefix) as $table) {
                                $this->MSFrameworkDatabase->query("DROP TABLE `" . $db_name . "`.`" . $table['TABLE_NAME'] . "`");
                            }
                        }
                    }
                }

                if($this->MSFrameworkUsers->registerUser($nome, $cognome, $email, $password, $cellulare, $livello, array(), array("env_id" => $got_env_id), true, $db_name)) {
                    (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))->registerUser($nome, $cognome, $email, $password, $cellulare, "8", array("active" => "0"));
                    return true;
                }

                $MSFrameworkEnvironments->destroy();
                return false;
            }
        }

        return false;
    }

    /**
     * Ottiene il nome del DB dell'ambiente
     *
     * @return string
     */
    public function getDBName() {
        $front = ($this->env_id == "front");

        $saas_id = ($front ? parent::getID() : $this->getEnvironmentDataFromDB($this->env_id, "id, saas_id")[$this->env_id]['saas_id']);
        return "marke833__SaaS__id_" . $saas_id . "__env_" . ($front ? "front" : $this->env_id);
    }

    /**
     * Controlla se l'abbonamento di un determinato ambiente SaaS è scaduto o meno
     *
     * @return bool
     * @throws \Exception
     */
    public function isExpired() {
        return $this->getExpirationDate() < (new \DateTime("now"));
    }

    /**
     * Ottiene la data di scadenza di un determinato ambiente
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getExpirationDate() {
        $exp_date = $this->getEnvironmentDataFromDB($this->env_id, "expiration_date")[$this->env_id]['expiration_date'];

        return new \DateTime($exp_date);
    }

    /**
     * Ottiene l'ID dell'abbonamento attualmente attivo per un determinato ambiente
     *
     * @return mixed
     */
    public function getCurrentSubscription() {
        return $this->getEnvironmentDataFromDB($this->env_id, "subscription")[$this->env_id]['subscription'];
    }

    /**
     * Effettua il rinnovo di un ambiente SaaS
     *
     * @param $preorder_reference L'array con i riferimento alla riga del preordine
     * @param array $ipn_data Un array di dati restituiti dall'IPN da salvare insieme alla transazione
     *
     * @return bool
     */
    public function renew($preorder_reference, $ipn_data = array()) {
        $MSFrameworkSubscriptions = new \MSFramework\SaaS\subscriptions();
        $fic_invoices = new \MSFramework\Fatturazione\Services\FattureInCloud\invoices();
        $fic_customers = new \MSFramework\Fatturazione\Services\FattureInCloud\customers();

        $preorder = (new \MSFramework\Framework\payway())->getPreorderData($preorder_reference);
        $preorder_framework_data = json_decode($preorder['framework_data'], true);

        $exp_date = $preorder_framework_data['subscription']['exp_date'];
        $subscription = $preorder_framework_data['subscription']['id'];
        $subscriptionDetails = $preorder_framework_data['subscription']['details'];
        $provider = $preorder['provider'];

        //aggiorno i dati dell'abbonamento
        $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`saas__environments` SET expiration_date = :exp, subscription = :sub WHERE id = :id", array(":exp" => $exp_date, ":sub" => $subscription, ":id" => $this->env_id));

        //aggiungo il log della transazione
        $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`" . $preorder_reference[0] . "` SET ipn_data = :ipn_data WHERE id = :id", array(":ipn_data" => json_encode($ipn_data), ":id" => $preorder_reference[1]));

        //emetto la fattura su Fatture in Cloud
        $fic_invoices->newInvoice($fic_customers->createCustomerDataArray($preorder_framework_data['user']['dati_fatturazione']), [["name" => $subscriptionDetails['name'], "descr" => $subscriptionDetails['short_descr'], "price" => $preorder_framework_data['price']['iva_inclusa']]], ["method" => $provider]);

        return true;
    }

    /**
     * Disabilita un SaaS
     *
     * @param $preorder_reference L'array con i riferimento alla riga del preordine
     *
     * @return bool
     */
    public function disable($preorder_reference) {
        $preorder = (new \MSFramework\Framework\payway())->getPreorderData($preorder_reference);

        //aggiorno i dati dell'abbonamento
        $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`saas__environments` SET expiration_date = NOW() WHERE id = :id", array(":id" => $preorder['env_id']));

        return true;
    }

    /**
     * Distrugge completamente l'ambiente
     * ATTENZIONE! Operazione non reversibile, cancella TUTTI i dati relativi da un intero ambiente. Utilizzare con cautela.
     *
     * @return bool
     */
    public function destroy() {
        $dbname = $this->getDBName();

        (new \cPanel\Database\database())->delete($dbname);
        shell_exec("rm -rf " . PATH_TO_COMMON_STORAGE . "SaaS/" . $dbname . "/");
        $this->MSFrameworkDatabase->deleteRow("`" . FRAMEWORK_DB_NAME . "`.`saas__environments`", "id", $this->env_id);

        return true;
    }

    /**
     * Restituisce una stringa con "ragione sociale - nome proprietario"
     *
     * @return string
     */
    public function getOwnerReadableString() {
        $dbname = $this->getDBName();

        $settings_r = $this->MSFrameworkDatabase->getAssoc("SELECT value FROM `$dbname`.`cms` WHERE type = 'settings'", array(), true);
        $settings = json_decode($settings_r['value'], true);

        $email = $this->getOwnerEmail();
        $profile = $this->MSFrameworkDatabase->getAssoc("SELECT nome, cognome FROM `$dbname`.`users` WHERE email = :mail", array(":mail" => $email), true);

        $str = "";
        if($settings['datiFatturazione']['rag_soc'] != "") {
            $str .= json_decode($settings['datiFatturazione'], true)['rag_soc'];
        }

        return ($str != "" ? $str . " di " : "") . $profile['cognome'] . " " . $profile['nome'];
    }
}
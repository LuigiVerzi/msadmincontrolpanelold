<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-29
 */

namespace MSFramework\SaaS;

class transactions extends base {
    private $saas_id;

    function __construct($saas_id = "") {
        global $MSFrameworkSaaSEnvironments, $MSFrameworkSaaSBase, $MSFrameworkDatabase;
        parent::__construct();

        $this->saas_id = ($saas_id == '' ? parent::getID() : $saas_id);

        $this->MSFrameworkSaaSEnvironments = $MSFrameworkSaaSEnvironments;
        $this->MSFrameworkSaaSBase = $MSFrameworkSaaSBase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Transazione => Dati Transazione) con i dati relativi alla transazione
     *
     * @param $id L'ID della transazione (stringa) o delle transazioni (array) richieste (se vuoto, vengono recuperati i dati di tutte le transazioni)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getTransactionsDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`saas__transactions` WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene tutte le transazioni per un determinato ambiente
     *
     * @param $env_id L'ID dell'ambiente
     * @param string $fields I campi da prelevare dal DB
     *
     * @return mixed
     */
    public function getTransactionForEnvirorment($env_id, $fields = "*") {
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`saas__transactions` WHERE env_id = :env_id", array(":env_id" => $env_id)) as $r) {
            $ary_to_return[$r['id']] = $this->getTransactionsDetails($r['id'], $fields)[$r['id']];
        }

        return $ary_to_return;
    }
}
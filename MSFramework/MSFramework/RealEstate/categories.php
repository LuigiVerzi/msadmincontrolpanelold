<?php
/**
 * MSFramework
 * Date: 17/04/18
 */

namespace MSFramework\RealEstate;


class categories
{
    public function __construct()
    {
        global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria
     *
     * @param string $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryDetails($id = "", $fields = "*")
    {
        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }
        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        $category = $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_categories WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);
        foreach ((array)$category as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene solo le categorie principali
     *
     * @param bool $just_used Se impostato su true, ottiene solo le categorie realmente usate
     *
     * @return array
     */
    public function getOnlyParents($just_used = false) {
        $immobiliObj = new \MSFramework\RealEstate\immobili();

        $to_return = array();

        foreach($this->getCategoryDetails() as $catID => $catV) {
            if($this->isCategoryParent($catV['id']) && (!$just_used || ($just_used && $immobiliObj->getImmobiliForCat($catV['id'], 'count', 'id') != 0))) {
                $to_return[$catID] = $catV;
            }

        }

        return $to_return;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria
     *
     * @param array $list La lista della ricerca attuale o l'ID della categoria
     * @param boolean $just_used Se impostato su true, ottiene solo le categorie realmente usate
     *
     * @return array
     */
    public function getCategoryChildrens($parent_id, $just_used = false) {
        $immobiliObj = new \MSFramework\RealEstate\immobili();

        $category_order = (new \MSFramework\cms())->getCMSData('realestate_cats_order')['list'];

        $children_ids = array();
        foreach ($category_order as $parent) {
            if ($parent_id == 0 || $parent['id'] == $parent_id) {
                foreach ($parent['children'] as $cat_child) {
                    if(!$just_used || ($just_used && $immobiliObj->getImmobiliForCat($cat_child['id'], 'count', 'id') != 0)) {
                        $children_ids[] = $cat_child['id'];
                    }
                }
            }
        }

        $children_categories = array();
        if($children_ids) {
            $children_categories = $this->getCategoryDetails($children_ids);
        }

        return $children_categories;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria genitore
     *
     * @param string $id L'ID della categoria figlia
     * @param bool $top_parent Se impostato su true, restituisce solo i dati del primo genitore della categoria
     *
     * @return array
     */
    public function getCategoryParent($children_id, $top_parent = false)
    {

        $category_order = (new \MSFramework\cms())->getCMSData('realestate_cats_order')['list'];

        $parent_id = array();
        foreach ($category_order as $parent) {
            foreach($parent['children'] as $children) {
                if ($children['id'] == $children_id) {
                   $parent_id = $parent['id'];
                }
                else if(isset($children['children']))
                {
                    foreach($children['children'] as $subchildren) {
                        if ($subchildren['id'] == $children_id) {
                            $parent_id = array($parent['id'], $children['id']);
                        }
                    }
                }
            }
        }

        $category = ($parent_id ? $this->getCategoryDetails($parent_id) : array());

        if($top_parent) {
            return array_values($category)[0];
        }

        return $category;
    }

    /**
     * Restituisce (boolean) se la categoria è genitore o no
     *
     * @param string $id L'ID della categoria da controllare
     *
     * @return boolean
     */
    public function isCategoryParent($id)
    {

        $category_order = (new \MSFramework\cms())->getCMSData('realestate_cats_order')['list'];
        foreach ($category_order as $parent) {
            if ($parent['id'] == $id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Restituisce la struttura padre/figlio del menu in formato HTML.
     *
     * @param string $menu_type Il tipo di HTML desiderato (accetta: select)
     * @param string $active_id L'ID della categoria attualmente in uso (o da impostare come attiva/selezionata)
     * @param string $ary_menu L'array contenente le voci del menu per il quale effettuare il parsing. Se vuoto, viene recuperato tutto il menu
     * @param string $html_struct L'array contenente i valori per la struttura HTML
     * @param boolean $hide_empty Se true nasconde le categorie che non contengono immobili
     *
     * @return string
     */
    public function composeHTMLMenu($menu_type = "select", $active_id = "", $ary_menu = "", $html_struct = "", $hide_empty = false)
    {
        global $MSFrameworki18n, $ind_level, $firephp;

        $immobiliObj = new \MSFramework\RealEstate\immobili();

        if (!is_array($ary_menu)) {
            $ary_menu = (new \MSFramework\cms())->getCMSData('realestate_cats_order')['list'];
        }

        $html = "";

        if ($menu_type == "select") {
            if ($ind_level == "") {
                $ind_level = 1;
            }

            if (!is_array($active_id)) {
                $active_id = array($active_id);
            }

            foreach ($ary_menu as $cur_menu) {
                if($hide_empty) {
                    if($immobiliObj->getImmobiliForCat($cur_menu['id'], 'count', 'id') == 0) continue;
                }

                $nbsp = "";
                for ($x = 1; $x < $ind_level; $x++) {
                    $nbsp .= "&nbsp;";
                }

                $set_selected = (in_array($cur_menu['id'], $active_id) ? "selected" : "");

                if(is_array($cur_menu['children'])) {
                    $html .= '<optgroup label="' . $MSFrameworki18n->getFieldValue($this->getCategoryDetails($cur_menu['id'], "nome")[$cur_menu['id']]['nome']) . '">';
                } else {
                    $html .= '<option data-macro="' . $this->getMacroForCategory($cur_menu['id']) . '" value="' . $cur_menu['id'] . '"' . $set_selected . (is_array($cur_menu['children']) ? ' disabled' : '') . '>' . $nbsp . " - " . $MSFrameworki18n->getFieldValue($this->getCategoryDetails($cur_menu['id'], "nome")[$cur_menu['id']]['nome'], true) . '</option>';
                }

                if (is_array($cur_menu['children'])) {
                    $ind_level = $ind_level * 3;
                    $html .= $this->composeHTMLMenu($menu_type, $active_id, $cur_menu['children'], "", $hide_empty);
                }
            }

            if ($ind_level > 0) {
                $html .= '</optgroup>';
                $ind_level = $ind_level / 3;
            }
        } else if($menu_type = "navigation") {

            if(!is_array($html_struct)) { //imposto un default nel caso in cui $html_struct non è un array
                $html_struct = array(
                    "main_element" => "<li>",
                    "main_element_append_class" => "",
                    "main_element_active_class" => "current-menu-item",
                    "sub_element" => "<ul>",
                    "sub_element_append_class" => "dropdown-menu",
                    "arrow_down" => '<span class="fa fa-caret-down"></span>',
                    "arrow_right" => '<span class="fa fa-caret-right"></span>',
                );
            }
            foreach ($ary_menu as $cur_menu) {

                $main_element_append_class = $html_struct['main_element_append_class'];
                if($hide_empty) {
                    if($immobiliObj->getImmobiliForCat($cur_menu['id'], 'count', 'id') == 0) continue;
                }

                $caret = "";
                if (is_array($cur_menu['children'])) {
                    $caret = $html_struct['arrow_down'];
                }

                $category_info = $this->getCategoryDetails($cur_menu['id'])[$cur_menu['id']];
                $menu_colors = json_decode($category_info['menu_colors'], true);

                $menu_bgcolor = ($menu_colors['bgColor'] != '') ? " background-color: " . $menu_colors['bgColor'] . "; " : "";
                $menu_color = ($menu_colors['color'] != '') ? " color: " . $menu_colors['color'] . "; " : "";
                $menu_mouseover = ($menu_colors['mouseover'] != '') ? ' onmouseover="this.style.backgroundColor=\'' . $menu_colors['mouseover'] . '\';" onmouseout="this.style.backgroundColor=\'' . $menu_colors['bgColor'] . '\';"' : "";

                $html_childrens = "";
                $childrens_data = array();
                if(is_array($cur_menu['children'])) {
                    $childrens_data = $this->_getChildren($cur_menu['children'], $html_struct, $active_id, false, $category_info, $hide_empty);
                    $html_childrens = $childrens_data[0];
                }

                $set_page_active = "";
                if($active_id == $cur_menu['id'] || $childrens_data[1] === true) {
                    $set_page_active = " " . $html_struct['main_element_active_class'];
                }

                if($html_childrens != "") {
                    $main_element_append_class  .= ' dropdown';
                }

                $main_element = str_replace(">", " class='" . $main_element_append_class . $set_page_active . "'>", $html_struct['main_element']);

                $html .= $main_element . '<a href="' . $this->getURL($category_info['id']) . '" style="' . $menu_bgcolor . $menu_color . '" ' . $menu_mouseover . '>' . $MSFrameworki18n->getFieldValue($category_info['nome']) . ' ' . $caret . '</a>';

                if($html_childrens != "") {
                    $html .= $html_childrens;
                }

                $html .= str_replace("<", "</", $html_struct['main_element']);

            }

        }

        return $html;
    }

    /**
     * Restituisce la macrocategoria per una data categoria
     *
     * @param $cat_id L'ID della categoria
     *
     * @return mixed
     */
    public function getMacroForCategory($cat_id) {
        return $this->MSFrameworkDatabase->getAssoc("SELECT macro FROM realestate_categories WHERE id = :id", array(":id" => $cat_id), true)['macro'];
    }

    private function _getChildren($children, $html_struct, $current_active_page, $set_parent_active = false, $parent_info = array(), $hide_empty = false) {
        global $firephp, $MSFrameworki18n;

        $immobiliObj = new \MSFramework\RealEstate\immobili();

        $sub_element = $html_struct['sub_element'];
        if($html_struct['sub_element_append_class'] != "") {
            $sub_element = str_replace(">", " class='" . $html_struct['sub_element_append_class'] . "'>", $html_struct['sub_element']);
        }
        $html = $sub_element;

        foreach($children as $cur_children) {
            if($hide_empty) {
                if($immobiliObj->getImmobiliForCat($cur_children['id'], 'count', 'id') == 0) continue;
            }

            $caret = "";
            if(is_array($cur_children['children'])) {
                $caret = $html_struct['arrow_right'];
            }

            if($set_parent_active !== true) {
                if($current_active_page == $cur_children['id']) {
                    $set_parent_active = true;
                }
            }

            $category_info = $this->getCategoryDetails($cur_children['id'])[$cur_children['id']];
            $menu_colors = json_decode($category_info['menu_colors'], true);
            $menu_bgcolor = ($menu_colors['bgColor'] != '') ? " background-color: " . $menu_colors['bgColor'] . "; " : "";
            $menu_color = ($menu_colors['color'] != '') ? " color: " . $menu_colors['color'] . "; " : "";
            $menu_mouseover = ($menu_colors['mouseover'] != '') ? ' onmouseover="this.style.backgroundColor=\'' . $menu_colors['mouseover'] . '\';" onmouseout="this.style.backgroundColor=\'' . $menu_colors['bgColor'] . '\';"' : "";

            $html .= $html_struct['main_element'] . '<a href="' . $this->getURL($category_info['id']) . '" style="' . $menu_bgcolor . $menu_color . '" ' . $menu_mouseover . '>' . $MSFrameworki18n->getFieldValue($category_info['nome']) . ' ' . $caret . '</a>';

            if(is_array($cur_children['children'])) {
                $html .= $this->_getChildren($cur_children['children'], $html_struct, $current_active_page, $set_parent_active, $parent_info, $hide_empty)[0];
            }

            $html .= str_replace("<", "</", $html_struct['main_element']);
        }

        $html .= str_replace("<", "</", $html_struct['sub_element']);

        return array($html, $set_parent_active);
    }


    /**
     * Ottiene l'URL della categoria
     *
     * @param $id L'ID (o Array) della categoria per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n;

        if(is_array($id)) $page_det = $id;
        else $page_det = $this->getCategoryDetails($id, "slug")[$id];

        $slug = '';
        $have_parents = $this->getCategoryParent($id);
        if($have_parents) {
            foreach($have_parents as $parent) {
                $slug .= (new \MSFramework\url())->cleanString($MSFrameworki18n->getFieldValue($parent['slug'])) . "/";
            }
        }

        $slug .= (new \MSFramework\url())->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";

        return ABSOLUTE_SW_PATH_HTML . (new \MSFramework\url())->getLangPrefix() . $slug;
    }

    /**
     * Ottiene l'ID per effettuare il link con la gestione multiportale
     *
     * @param $cat_id L'ID della categoria per il quale cercare il link
     * @param $portal Il nome del portale per il quale si desidera cercare il link
     *
     * @return mixed
     */
    public function getMultiportalLink($cat_id, $portal) {
        $r = $this->getCategoryDetails($cat_id, 'multiportal_links')[$cat_id];
        $link = json_decode($r['multiportal_links'], true);

        return $link[$portal];
    }
}
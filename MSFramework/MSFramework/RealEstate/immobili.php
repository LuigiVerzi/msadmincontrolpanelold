<?php
/**
 * MSFramework
 * Date: 12/02/19
 */

namespace MSFramework\RealEstate;

class immobili {
    private $pagination;
    private $search_in_address;

    public function __construct() {
        $this->search_in_address = false; //avevo fatto una ricerca per indirizzo, che ora non serve più. la disabilito, ma mantengo il codice. Se dovesse servire di nuovo, basta impostare questa variabile a true

        $this->pagination = array(
            "recordsPerPage" => 10,
            "currentPage" => ($_GET['page'] == "") ? 1 : $_GET['page'],
        );
    }

    /**
     * Questa funzione restituisce tutti gli immobili di una data categoria
     *
     * @param $mainCat L'ID della categoria
     * @param string $return Il tipo di dato da restituire: "all" restituisce tutti i record dal DB, "count" restituisce il conteggio dei record, "pagination" restituisce i record applicando le regole della paginazione
     * @param string $fields Le colonne da selezionare nel DB
     */
    public function getImmobiliForCat($mainCat, $return = "all", $fields = "*") {
        Global $MSFrameworkDatabase;
        $MSFrameworkRealEstateCategories = new \MSFramework\RealEstate\categories();

        $childs_id = array($mainCat);
        foreach($MSFrameworkRealEstateCategories->getCategoryChildrens($mainCat) as $child) {
            $childs_id[] = $child['id'];
        }

        $same_db_string_ary = $MSFrameworkDatabase->composeSameDBFieldString($childs_id, "OR", "category");

        if($return == "all") {
            return $MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE stato_vendita = '0' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);
        } else if($return == "count") {
            return $MSFrameworkDatabase->getCount("SELECT $fields FROM realestate_immobili WHERE stato_vendita = '0'  AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);
        } else if($return == "pagination") {
            $limit_start = ($this->pagination['currentPage']-1)*$this->pagination['recordsPerPage'];
            $limit_stop = $this->pagination['recordsPerPage'];
            return $MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE stato_vendita = '0'  AND (" . $same_db_string_ary[0] . ") LIMIT $limit_start, $limit_stop", $same_db_string_ary[1]);
        }
    }

    /**
     * Questa funzione restituisce tutti gli immobili di un dato agente
     *
     * @param $id L'ID dell'agente
     * @param string $return Il tipo di dato da restituire: "all" restituisce tutti i record dal DB, "count" restituisce il conteggio dei record, "pagination" restituisce i record applicando le regole della paginazione
     * @param string $fields Le colonne da selezionare nel DB
     */
    public function getImmobiliForUser($id, $return = "all", $fields = "*") {
        Global $MSFrameworkDatabase;
        
        if($return == "all") {
            return $MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE owner_id = :owner_id AND stato_vendita = '0'", array(":owner_id" => $id));
        } else if($return == "count") {
            return $MSFrameworkDatabase->getCount("SELECT $fields FROM realestate_immobili WHERE owner_id = :owner_id AND stato_vendita = '0'", array(":owner_id" => $id));
        } else if($return == "pagination") {
            $limit_start = ($this->pagination['currentPage']-1)*$this->pagination['recordsPerPage'];
            $limit_stop = $this->pagination['recordsPerPage'];
            return $MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE owner_id = :owner_id AND stato_vendita = '0' LIMIT $limit_start, $limit_stop", array(":owner_id" => $id));
        }
    }

    /**
     * Questa funzione restituisce tutti gli immobili di una data agenzia
     *
     * @param $id L'ID dell'agenzia
     * @param string $return Il tipo di dato da restituire: "all" restituisce tutti i record dal DB, "count" restituisce il conteggio dei record, "pagination" restituisce i record applicando le regole della paginazione
     * @param string $fields Le colonne da selezionare nel DB
     * @param int $limit Se impostato, seleziona $limit immobili a caso (solo se $return == "all")
     */
    public function getImmobiliForAgency($id, $return = "all", $fields = "*", $limit = "") {
        Global $MSFrameworkDatabase;

        $limit_str = ($return == "all" && $limit != "" ? " ORDER BY RAND() LIMIT " . $limit : "");

        if($return == "all") {
            return $MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE agency = :owner_id AND stato_vendita = '0' $limit_str", array(":owner_id" => $id));
        } else if($return == "count") {
            return $MSFrameworkDatabase->getCount("SELECT $fields FROM realestate_immobili WHERE agency = :owner_id AND stato_vendita = '0'", array(":owner_id" => $id));
        } else if($return == "pagination") {
            $limit_start = ($this->pagination['currentPage']-1)*$this->pagination['recordsPerPage'];
            $limit_stop = $this->pagination['recordsPerPage'];
            return $MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE agency = :owner_id AND stato_vendita = '0' LIMIT $limit_start, $limit_stop", array(":owner_id" => $id));
        }
    }

    /**
     * Questa funzione contiene tutti gli stati di vendita possibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getStatoVendita($key = "") {
        $array = array(
            "0" => "In vendita",
            "1" => "Venduto",
            "2" => "Non più in vendita"
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione contiene tutti gli stati possibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getStatiImmobili($key = "") {
        $array = array(
            "0" => "Libero",
            "1" => "Da costruire",
            "2" => "Occupato da proprietario",
            "3" => "Occupato da inquilino",
            "4" => "Parzialmente Rifinito",
            "5" => "Rifiniture Standard",
            "6" => "Rifiniture di Lusso",
            "7" => "Pre Costruzione",
            "8" => "Con Permessi",
            "9" => "Convertito",
            "10" => "Convertito Parzialmente",
            "11" => "Non Convertito",
            "12" => "Nuovo / In costruzione",
            "13" => "Ottimo / Ristrutturato",
            "14" => "Buono / Abitabile",
            "15" => "Da Ristrutturare",
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione contiene tutti i tipi di arredamento possibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getStatiArredamento($key = "") {
        $array = array(
            "0" => "Arredato",
            "1" => "Parzialmente arredato",
            "2" => "Non arredato"
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione contiene tutte le classi energetiche possibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getClassiEnergetiche($key = "") {
        $array = array(
            "0" => "A+",
            "1" => "A",
            "2" => "B",
            "3" => "C",
            "4" => "D",
            "5" => "E",
            "6" => "F",
            "7" => "G",
            "8" => "In fase di rilascio",
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione contiene tutti i piani possibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array
     */
    public function getElencoPiani($key = "") {
        $array = array(
            "interrato" => "Interrato",
            "seminterrato" => "Seminterrato",
            "terra" => "Piano terra",
            "rialzato" => "Piano rialzato",
        );

        for($x=1; $x<=10; $x++) {
            $array['piano_' . $x] = $x;
        }

        $array = array_merge($array, array(
            "10+" => ">10",
            "ultimo" => ">Ultimo",
            "piu_livelli" => ">Su più livelli",
        ));

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione contiene tutte le tipologie di riscaldamento possibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getTipiRiscaldamento($key = "") {
        $array = array(
            "0" => "Assente",
            "1" => "Autonomo",
            "2" => "Centralizzato"
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione contiene tutte le tipologie di cucina possibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getTipiCucina($key = "") {
        $array = array(
            "0" => "Abitabile",
            "1" => "Angolo cottura",
            "2" => "Cucinotto",
            "3" => "Semi abitabile",
            "4" => "A vista",
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione contiene tutti i tipi di garage possibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getTipiGarage($key = "") {
        $array = array(
            "0" => "Si, singolo",
            "1" => "Si, doppio",
            "2" => "Posto auto",
            "3" => "No"
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione contiene tutti i tipi di giardino possibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getTipiGiardino($key = "") {
        $array = array(
            "0" => "Privato",
            "1" => "Comune",
            "2" => "Nessuno"
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }


    /**
     * Questa funzione contiene tutti i servizi disponibili per un immobile
     *
     * @param $mainCat L'ID della categoria principale
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getElencoServizi($mainCat = "", $key = "") {
        $array = array(
            "0" => array(
                "service_groupname" => "Caratteristiche Immobile",
                "services" => array(
                    "0" => "Palazzo Celebre",
                    "1" => "Ammobiliato",
                    "2" => "Aria condizionata",
                    "3" => "Struttura in costruzione",
                    "4" => "Da ristrutturare",
                    "5" => "Ristrutturato",
                    "6" => "Portineria",
                    "7" => "Riscaldamento Autonomo",
                    "8" => "Riscaldamento Centralizzato",
                    "9" => "Riscaldamento a Pavimento",
                    "10" => "Antifurto",
                    "11" => "Piscina",
                    "12" => "Camino/Caminetto",
                    "13" => "Linea Internet",
                )
            ),
            "1" => array(
                "service_groupname" => "Caratteristiche Esterne",
                "services" => array(
                    "0" => "Accessibile PMR",
                    "1" => "Portone automatico",
                    "2" => "Parcheggio",
                    "3" => "Posto Auto Riservato",
                    "4" => "Vista Mare",
                    "5" => "Vista Montagne",
                    "6" => "Vista Lago",
                    "7" => "Vista panoramica città",
                    "8" => "Vista panoramica natura",
                )
            ),
            "2" => array(
                "service_groupname" => "Punti di interesse",
                "services" => array(
                    "0" => "Vicinanze Centro città",
                    "1" => "Vicinanze Negozi",
                    "2" => "Vicinanze Aereoporto",
                    "3" => "Vicinanze Fermata Autobus",
                    "4" => "Vicinanze Metropolitana",
                    "5" => "Vicinanze Ospedale"
                )
            )
        );

        if($mainCat == "") {
            return $array;
        } else {
            $workingOn = $array[$mainCat];
            if($key != "") {
                return $workingOn['services'][$key];
            } else {
                return $workingOn;
            }
        }
    }

    /**
     * Questa funzione contiene tutti i tipi di mutuo disponibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getTipiMutui($key = "") {
        $array = array(
            "0" => "Fisso",
            "1" => "Misto",
            "2" => "Variabile",
            "3" => "Variabile con opzione",
            "4" => "Variabile con rata costante",
            "5" => "Variabile con rimborso flessibile",
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione contiene tutte le periodicità di mutuo disponibili per un immobile
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getPeriodicitaRataMutui($key = "") {
        $array = array(
            "0" => "Annuale",
            "1" => "Mensile",
            "2" => "Semestrale",
            "3" => "Settimanale"
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione ottiene i dati dell'immobile dal DB
     *
     * @param $id L'id dell'immobile
     * @param string $fields Le colonne da selezionare nel DB
     */
    public function getImmobiliDetails($id, $fields = "*") {
        Global $MSFrameworkDatabase;

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;

    }

    /**
     * Questa funzione ottiene i dati dell'immobile dal DB
     *
     * @param $id L'id dell'immobile
     * @param string $fields Le colonne da selezionare nel DB
     */
    public function getImmobileDataFromDB($id, $fields = "*") {
        Global $MSFrameworkDatabase;

        return $MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE id = :id", array(":id" => $id), true);
    }

    /**
     * Questa funzione permette di generare l'HTML necessario per visualizzare delle piccole info al di sotto della scheda dell'immobile (quando ci si trova nell'elenco di tutti gli immobili o della ricerca)
     *
     * @param $id L'ID dell'immobile
     * @param int $limit Il limite massimo di elementi da mostrare. Gli elementi vengono visualizzati secondo l'ordine di priorità impostato all'interno dell'array $priority_order. -1 = nessun limite
     *
     * @return string
     */
    public function getHTMLInfoRecap($id, $limit = 5) {
        global $firephp;

        $r = $this->getImmobileDataFromDB($id, "caratteristiche, superficie, anno_costruzione");
        $caratteristiche = json_decode($r['caratteristiche'], true);
        $str_html = "";
        $done = 0;

        //Con questo array è possibile impostare la priorità di visualizzazione dei dati. Una volta raggiunto il limite ($limit) i dati con meno priorità non saranno visualizzati.
        $priority_order = array(
            array("Superficie", $r['superficie']." mq"),
            array("Locali", $caratteristiche['totale_locali']),
            array("Bagni", $caratteristiche['bagni']),
            array("Classe Energetica", ($caratteristiche['classe_energetica'] != "") ? $this->getClassiEnergetiche($caratteristiche['classe_energetica']) : ""),
            array("Arredamento", ($caratteristiche['stato_arredamento'] != "") ? $this->getStatiArredamento($caratteristiche['stato_arredamento']) : ""),
            array("Posti Auto", $caratteristiche['posti_auto']),
            array("Box/Garage", ($caratteristiche['box_garage'] != "") ? $this->getTipiGarage($caratteristiche['box_garage']) . " " . $caratteristiche['box_garage_mq'] . " mq" : ""),
            array("Posti Auto Garage", $caratteristiche['posti_auto_garage']),
            array("Piano", ($caratteristiche['piano'] != "") ? $this->getElencoPiani($caratteristiche['piano']) : ""),
            array("Piani Totali", $caratteristiche['piani_totali']),
            array("Anno di Costruzione", $r['anno_costruzione']),
            array("Riscaldamento", ($caratteristiche['riscaldamento'] != "") ? $this->getTipiRiscaldamento($caratteristiche['riscaldamento']) : ""),
            array("Cucina", ($caratteristiche['cucina'] != "") ? $this->getTipiCucina($caratteristiche['cucina']) : ""),
            array("Camere da letto", $caratteristiche['camere_da_letto']),
            array("Giardino", ($caratteristiche['giardino'] != "") ? $this->getTipiGiardino($caratteristiche['giardino']) . " " . $caratteristiche['giardino_mq'] . " mq" : ""),
            array("Stato Immobile", ($caratteristiche['stato_immobile'] != "") ? $this->getStatiImmobili($caratteristiche['stato_immobile']) : ""),
            array("A reddito", ($caratteristiche['check_reddito'] == "1") ? "Si" : ""),
            array("Reception", ($caratteristiche['check_reception'] == "1") ? "Si" : ""),
            array("Allarme", ($caratteristiche['check_allarme'] == "1") ? "Si" : ""),
            array("Cablato", ($caratteristiche['check_cablato'] == "1") ? "Si" : ""),
            array("Ascensore", ($caratteristiche['check_ascensore'] == "1") ? "Si " . $caratteristiche['liftMq'] . " mq" : ""),
            array("Terrazzo", ($caratteristiche['check_terrazzo'] == "1") ? "Si " . $caratteristiche['terrazzo_mq'] . " mq" : ""),
            array("Balcone", ($caratteristiche['check_balcone'] == "1") ? "Si " . $caratteristiche['balcone_mq'] . " mq" : ""),
            array("Cantina", ($caratteristiche['check_cantina'] == "1") ? "Si " . $caratteristiche['cantina_mq'] . " mq" : ""),
            array("Condizionatore", ($caratteristiche['check_condizionatore'] == "1") ? "Si" : ""),
        );

        foreach($priority_order as $cur_priority) {
            $value = $cur_priority[1];

            if($value != "") {
                $str_html .= "<li>" . $cur_priority[0] . ": <span>" . $value . "</span></li>";

                $done++;
                if($done == $limit && $limit != -1) {
                    break;
                }

            }
        }

        if($str_html == "") {
            return "<li>&nbsp;<span>&nbsp;</span></li>";
        } else {
            return $str_html;
        }

    }

    /**
     * Questa funzione permette di generare l'HTML necessario per visualizzare l'elenco dei servizi all'interno della scheda dell'immobile
     *
     * @param $id L'ID dell'immobile
     * @param int $limit Il limite massimo di elementi da mostrare. -1 = nessun limite
     *
     * @return string
     */
    public function getHTMLServizi($id, $limit = 10) {
        $r = $this->getImmobileDataFromDB($id, "servizi");
        $servizi = json_decode($r['servizi'], true);
        $str_html = "";
        $done = 0;

        foreach($servizi as $servizioK => $servizioV) {
            if($servizioV != "1") {
                continue;
            }

            $expl_servizio = explode("_", $servizioK);
            $str_html .= '<li>' . $this->getElencoServizi($expl_servizio[0], $expl_servizio[1]) . '</li>';

            $done++;
            if($done == $limit && $limit != -1) {
                break;
            }
        }

        return $str_html;
    }

    /**
     * Questa funzione consente di ottenere l'HTML con i dati del mutuo da inserire all'interno del dettaglio immobile. Se non ci sono dati, restituisce una stringa vuota
     * @param $id L'ID dell'immobile
     *
     * @return string
     */
    public function getHTMLDatiMutuo($id) {
        $r = $this->getImmobileDataFromDB($id, "dati_mutuo, riferimento, prezzo, trattativa_riservata, tipo_vendita");
        $dati = json_decode($r['dati_mutuo'], true);

        $show_credipass = false;
        $cols_sit_fin = "12";
        if($r['trattativa_riservata'] != "1" && $r['tipo_vendita'] != "1") {
            $show_credipass = true;
            $cols_sit_fin = "7";
        }

        $str_html = "<div class='col-md-" . $cols_sit_fin . "'>";

        $no_financial_data = true;
        foreach ($dati as $k => $v) {
            if ($v == "") {
                continue;
            }

            $no_financial_data = false;

            if ($k == "tipo") {
                $dato = $this->getTipiMutui($v);
            } else if ($k == "period") {
                $dato = $this->getPeriodicitaRataMutui($v);
            } else {
                $dato = $v;
            }

            $str_html .= '<div class="col-md-6 mb-10">
                        <div class="bold">' . $this->getDatiMutuoFields($k) . '</div>
                        <div>' . $dato . '</div>
                    </div>';
        }

        if(!$show_credipass && $no_financial_data) {
            return "";
        }

        if($no_financial_data) {
            $str_html .= "<div class='loan'>Questo immobile non presenta situazione finanziaria</div>";
        }

        $str_html .= "</div>";

        if($show_credipass) {
            $str_html .= '<div class="col-md-5 col-sm-5">
                        <iframe src="http://tool.credipass.it/boxrata?Canale=REMAX&amp;CostoImmobile=' . $r['prezzo'] . '&amp;IdConv=0&amp;CodiceImmobile=' . $r['riferimento'] . '&amp;Width=430&amp;HideConv=1" id="ctl07_ctl00_iFrameMortgageCalc" frameborder="0" name="iFrameMortgageCalc" width="100%" height="260"> </iframe>
                    </div>';
        }

        return $str_html;
    }

    /**
     * Questa funzione restituisce il valore "human readable" di uno o più campi "mutuo" relativo alla chiave salvata all'interno del DB
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getDatiMutuoFields($key = "") {
        $array = array(
            "attuale" => "Attuale",
            "differenza_saldo" => "Differenza a Saldo",
            "tipo" => "Tipo",
            "residuo_mutuo" => "Residuo mutuo",
            "residuo_anni" => "Residuo anni",
            "period" => "Periodicità rata",
            "pagamento" => "Pagamento",
            "banca" => "Banca",
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Questa funzione restituisce i record applicando le regole della ricerca impostate
     *
     * @param string $return Il tipo di dato da restituire: "all" restituisce tutti i record dal DB, "count" restituisce il conteggio dei record, "pagination" restituisce i record applicando le regole della paginazione
     */
    public function doSearch($return = "all") {
        global $firephp;

        Global $MSFrameworkDatabase;
        $MSFrameworkAnalytics = new \MSFramework\RealEstate\analytics();
        $MSFrameworkRealEstateCategories = new \MSFramework\RealEstate\categories();

        $order_by = "";
        $order_city_priority = "";
        $oth_filters = "";
        $oth_filters_ary = array();
        $select_string = "*, ";
        $user_set_param = false;

        //GRUPPO AND
        if($_GET['stype'] != '') {
            $user_set_param = true;
            $oth_filters .= " tipo_vendita = :tipo_vendita AND ";
            $oth_filters_ary[":tipo_vendita"] = $_GET['stype'];
        }

        if($_GET['icat'] != '') {
            $user_set_param = true;
            $oth_filters .= " category = :category AND ";
            $oth_filters_ary[":category"] = $_GET['icat'];
        } else if($_GET['itype'] != '') {
            $user_set_param = true;

            $childs_id = array($_GET['itype']);
            foreach($MSFrameworkRealEstateCategories->getCategoryChildrens($_GET['itype']) as $child) {
                $childs_id[] = $child['id'];
            }

            $same_db_string_ary = $MSFrameworkDatabase->composeSameDBFieldString($childs_id, "OR", "category");
            if($same_db_string_ary[0] != "") {
                $oth_filters .= "(" . $same_db_string_ary[0] . ") AND ";
                $oth_filters_ary = array_merge($oth_filters_ary, $same_db_string_ary[1]);
            }
        }

        if($_GET['address'] != '') {
            $user_set_param = true;
            $found_city_filter = false;
            $exploded_addr = $this->cleanCityAddressString($_GET['address']);
            $exploded_addr_min3 = $this->cleanCityAddressString($_GET['address'], 3);

            $close_query_parent = "";
            if(is_array($exploded_addr_min3) && count($exploded_addr_min3) > 0) {
                //ricerca città
                $where_ary_city = $MSFrameworkDatabase->composeSameDBFieldString($exploded_addr_min3, "OR", "name", "LIKE");
                $geoname_or = "";

                foreach($MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname` WHERE " . $where_ary_city[0], $where_ary_city[1]) as $geonameInfos) {
                    $geoname_or .= " geo_data LIKE '%\"comune\":\"" . $geonameInfos['geonameid'] . "\"%' OR ";
                    $order_city_priority .= " geo_data LIKE '%\"comune\":\"" . $geonameInfos['geonameid'] . "\"%' DESC, ";
                }

                //controllo se qualcuno dei termini inseriti potrebbe corrispondere ad una provincia (Issue #85)
                foreach($MSFrameworkDatabase->getAssoc("SELECT code FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin2Codes` WHERE " . $where_ary_city[0], $where_ary_city[1]) as $provInfos) {
                    $prov_code = end(explode(".", $provInfos['code']));

                    foreach($MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname` WHERE admin2 = :prov", array(":prov" => $prov_code)) as $geonameInfos) {
                        $geoname_or .= " geo_data LIKE '%\"comune\":\"" . $geonameInfos['geonameid'] . "\"%' OR ";
                    }
                }

                if ($geoname_or != "") {
                    $geoname_or = substr($geoname_or, 0, -4);
                    if($this->search_in_address) {
                        $oth_filters .= "((" . $geoname_or . ") OR ";
                        $close_query_parent = ")";
                    } else {
                        $oth_filters .= "(" . $geoname_or . ") AND ";
                    }

                    $found_city_filter = true;
                }
            }

            if(is_array($exploded_addr) && count($exploded_addr) > 0 && $this->search_in_address) {
                //ricerca indirizzo (via)
                foreach($exploded_addr as $piece) {
                    $select_string .= "(LENGTH(`geo_data`) - LENGTH(REPLACE(`geo_data`, '$piece', ''))) / LENGTH('$piece') + ";
                }

                if($select_string != "") {
                    $select_string = substr($select_string, 0, -3) . " `occurences`, ";
                }
                $where_ary = $MSFrameworkDatabase->composeSameDBFieldString($exploded_addr, "OR", "geo_data", "LIKE");
                $oth_filters .= "(" . $where_ary[0] . ")" . $close_query_parent . " AND ";
                $oth_filters_ary = array_merge($oth_filters_ary, $where_ary[1]);
                $order_by .= " `occurences`  DESC, ";
            }

            if(!$found_city_filter) {
                $oth_filters .= " geo_data = '-1' AND ";
            }
        }

        if($_GET['bagni'] != '') {
            $user_set_param = true;
            $oth_filters .= " caratteristiche LIKE :bagni AND ";
            $oth_filters_ary[":bagni"] = '%"bagni":"' . $_GET['bagni'] . '"%';
        }

        if($_GET['energy'] != '') {
            $user_set_param = true;
            $oth_filters .= " caratteristiche LIKE :classe_energetica AND ";
            $oth_filters_ary[":classe_energetica"] = '%"classe_energetica":"' . $_GET['energy'] . '"%';
        }

        if($_GET['istate'] != '') {
            $user_set_param = true;
            $oth_filters .= " caratteristiche LIKE :stato_immobile AND ";
            $oth_filters_ary[":stato_immobile"] = '%"stato_immobile":"' . $_GET['istate'] . '"%';
        }

        if($_GET['superficie_min'] != '') {
            $user_set_param = true;
            $oth_filters .= " superficie >= :superficie_min AND ";
            $oth_filters_ary[":superficie_min"] = $_GET['superficie_min'];
        }

        if($_GET['superficie_max'] != '' && $_GET['superficie_max'] > $_GET['superficie_min']) {
            $user_set_param = true;
            $oth_filters .= " superficie <= :superficie_max AND ";
            $oth_filters_ary[":superficie_max"] = $_GET['superficie_max'];
        }

        if($_GET['price_min'] != '') {
            $user_set_param = true;
            $oth_filters .= " prezzo >= :price_min AND ";
            $oth_filters_ary[":price_min"] = $_GET['price_min'];
        }

        if($_GET['price_max'] != '' && $_GET['price_max'] > $_GET['price_min']) {
            $user_set_param = true;
            $oth_filters .= " prezzo <= :price_max AND ";
            $oth_filters_ary[":price_max"] = $_GET['price_max'];
        }

        if($_GET['kw'] != "") {
            $user_set_param = true;
            $oth_filters .= " MATCH (descrizione,titolo) AGAINST (:kw1) AND ";
            $oth_filters_ary[":kw1"] = $_GET['kw'];
            $select_string .= " MATCH (descrizione,titolo) AGAINST (:kw2) as punteggio, ";
            $oth_filters_ary[":kw2"] = $_GET['kw'];

            $order_by .= " `punteggio`, ";
        }

        if($oth_filters != "") {
            $oth_filters = " AND (" . substr($oth_filters, 0, -5) . ")";
        }

        //GRUPPO OR
        if($_GET['code'] != '') {
            $user_set_param = true;
            ($oth_filters == "") ? $op = "AND" : $op = "OR";
            $oth_filters .= " $op riferimento = :rif ";
            $oth_filters_ary[":rif"] = $_GET['code'];
        }

        $order_by .= " `id`  DESC";
        if($order_city_priority != "") {
            $order_by = $order_city_priority . $order_by;
        }

        $select_string = substr($select_string, 0, -2);

        //salvo la ricerca dell'utente nel DB
        if($return != "count" && $user_set_param) {
            $MSFrameworkAnalytics->saveSearch(
                array(
                    "keywords" => $_GET['kw'],
                    "riferimento" => $_GET['code'],
                    "tipo_vendita" => $_GET['stype'],
                    "tipo_immobile" => $_GET['itype'],
                    "categoria" => $_GET['icat'],
                    "comune" => $_GET['address'],
                    "price_from" => $_GET['price_min'],
                    "price_to" => $_GET['price_max'],
                    "mq_from" => $_GET['superficie_min'],
                    "mq_to" => $_GET['superficie_max'],
                    "oth_filters" => json_encode( //potenzialmente, in un futuro, potremmo consentire la ricerca per ogni singola caratteristica/servizio dell'immobile. non mi piace creare n colonne nel DB per ogni filtro minore, quindi li raggruppo qui dentro.
                        array(
                            "bagni" => $_GET['bagni'],
                            "energy" => $_GET['energy'],
                            "istate" => $_GET['istate'],
                        )
                    ),
                    "results" => $this->doSearch("count"),
                )
            );
        }

        if($return == "all") {
            $results = $MSFrameworkDatabase->getAssoc("SELECT $select_string FROM realestate_immobili WHERE id != '' AND stato_vendita = '0' $oth_filters ORDER BY $order_by", $oth_filters_ary);
            return $results;
        } else if($return == "count") {
            return $MSFrameworkDatabase->getCount("SELECT $select_string FROM realestate_immobili WHERE id != '' AND stato_vendita = '0' $oth_filters ORDER BY $order_by", $oth_filters_ary);
        } else if($return == "pagination") {
            $limit_start = ($this->pagination['currentPage']-1)*$this->pagination['recordsPerPage'];
            $limit_stop = $this->pagination['recordsPerPage'];

            return $MSFrameworkDatabase->getAssoc("SELECT $select_string FROM realestate_immobili WHERE id != '' AND stato_vendita = '0' $oth_filters ORDER BY $order_by LIMIT $limit_start, $limit_stop", $oth_filters_ary);
        }

    }


    /**
     * Questa funzione permette di recuperare il valore minimo/massimo presente in una colonna nella tabella immobili
     *
     * @param $type Il tipo di dato da recuperare (lowest/highest)
     * @param $col la colonna in analisi
     *
     * @return bool
     */
    public function getLowHighValue($type, $col) {
        Global $MSFrameworkDatabase;
        
        if($type == "lowest") {
            $mysql_func = "MIN($col)";
        } else if($type == "highest") {
            $mysql_func = "MAX($col)";
        }

        $append_where = "";
        if($mysql_func != "") {
            if($col == "prezzo") {
                $append_where = " AND trattativa_riservata = 0";
            }

            $r = $MSFrameworkDatabase->getAssoc("SELECT $mysql_func as value FROM realestate_immobili WHERE stato_vendita = '0' $append_where", array(), true);
            return $r['value'];
        }

        return false;
    }

    /**
     * Questa funzione crea l'html che permette all'utente di navigare tra le pagine (sia dell'elenco per categoria, sia all'interno della ricerca)
     *
     * @param $total_records Il numero totale di record da gestire
     *
     * @return string
     */
    public function createPagination($total_records) {
        global $firephp;
        $MSFrameworkURL = new \MSFramework\url();
        
        $pages_needed = ceil($total_records/$this->pagination['recordsPerPage']);

        $str = "<ul class=\"pagination\">";

        if($this->pagination['currentPage'] != 1) {
            $str .= "<li class=\"page-item\">
                <a class=\"page-link\" href=\"" . $MSFrameworkURL->regenerateGETParams(array("page", "mainCat", "listType"), array("page" => $this->pagination['currentPage']-1)) . "\" aria-label=\"Precedente\">
                    <span class=\"fa fa-caret-left\"> </span>
                </a>
            </li>";
        }

        $pages_done = array();

        $giri_first = 0;
        for($x = 1; $x <= $pages_needed; $x++) {
            $active = "";
            if($x == $this->pagination['currentPage']) {
                $active = "active";
            }

            $middle_group = array($this->pagination['currentPage']-2, $this->pagination['currentPage']-1, $this->pagination['currentPage'], $this->pagination['currentPage']+1);
            $last_group = array($pages_needed-3, $pages_needed-2, $pages_needed-1, $pages_needed);

            if($x <= 4) {
                $str .= "<li class=\"page-item $active\"><a class=\"page-link\" href=\"" . $MSFrameworkURL->regenerateGETParams(array("page", "mainCat", "listType"), array("page" => $x)) . "\">$x</a></li>";
                $pages_done[] = $x;

                if($x == 4 && $pages_needed > 4 && $x+1 < $middle_group[$giri_first]) {
                    $str .= "<li class=\"page-item\"><a class=\"page-link\" href=\"\">...</a></li>";
                }
            }

            if($x >= $middle_group[$giri_first] && $giri_first < count($middle_group) && !in_array($x, $pages_done)) {
                $str .= "<li class=\"page-item $active\"><a class=\"page-link\" href=\"" . $MSFrameworkURL->regenerateGETParams(array("page", "mainCat", "listType"), array("page" => $x)) . "\">$x</a></li>";
                $pages_done[] = $x;
                $giri_first++;

                if($giri_first == count($middle_group) && $pages_needed-4 > $x) {
                    $str .= "<li class=\"page-item\"><a class=\"page-link\" href=\"\">...</a></li>";
                    $giri_first++;
                }
            }

            if($x >= min($last_group) && $x <= max($last_group) && !in_array($x, $pages_done)) {
                $str .= "<li class=\"page-item $active\"><a class=\"page-link\" href=\"" . $MSFrameworkURL->regenerateGETParams(array("page", "mainCat", "listType"), array("page" => $x)) . "\"> $x</a></li>";
            }
        }

        if($pages_needed != $this->pagination['currentPage'] && $pages_needed != 0) {
            $str .= "<li class=\"page-item\">
                <a class=\"page-link\" href=\"" . $MSFrameworkURL->regenerateGETParams(array("page", "mainCat", "listType"), array("page" => $this->pagination['currentPage']+1)) . "\" aria-label=\"Successivo\">
                    <span class=\"fa fa-caret-right\"> </span>
                </a>
            </li>";
        }

        $str .= "</ul>";

        return $str;
    }

    /**
     * Questa funzione restituisce il prezzo dell'immobile o la scritta "Trattativa Riservata"
     *
     * @param $id L'ID dell'immobile
     *
     * @return string
     */
    public function getPrice($id) {
        $r = $this->getImmobileDataFromDB($id, "prezzo, trattativa_riservata, category, tipo_vendita");
        if($r['trattativa_riservata'] == "1") {
            return "Tratt. Riservata";
        }

        $prepend_str = "";
        if($r['categoria'] == "2") { //TODO: questo non funzionerà più a causa del cambio di gestione delle categorie
            $prepend_str .= "A partire da<br>";
        }

        $append_str = "";
        if($r['tipo_vendita'] == "1") {
            $append_str .= " al mese";
        }

        return $prepend_str . CURRENCY_SYMBOL . ' ' . $r['prezzo'] . $append_str;
    }

    /**
     * Questa funzione restituisce un elenco di immobili "in evidenza"
     *
     * @param string $fields Le colonne da selezionare nel DB
     * @param int $limit Il limite massimo di elementi da mostrare. -1 = nessun limite
     * @param string $order_by L'ordinamento dei risultati
     *
     */
    public function getImmobiliInEvidenza($fields = "*", $limit = -1, $order_by = "RAND()") {
        global $firephp;
        Global $MSFrameworkDatabase;
        
        if($limit != -1) {
            $str_limit = " LIMIT " . $limit;
        }

        return $MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE in_evidenza = '1' AND stato_vendita = '0' ORDER BY $order_by $str_limit");
    }

    /**
     * Questa funzione restituisce una serie di immobili simili alle caratteristiche della ricerca dell'utente o dell'immobile che sta visualizzando
     *
     * @param $limit Il numero di risultati da restituire
     * @param string $keywords Se l'utente ha impostato delle parole chiave come preferenza, passarle in questo parametro
     * @param string $tipo_immobile Se l'utente ha impostato una tipologia di immobile come preferenza, passarla in questo parametro
     * @param string $citta Se l'utente ha impostato una città/via come preferenza, passarla in questo parametro
     * @param string $current_immobile L'ID dell'immobile corrente che sta visualizzando l'utente. Sarà escluso dai suggerimento
     * @param array $current_try_ary NON SOVRASCRIVERE QUESTO PARAMETRO! SERVE PER LA RICORSIVITA' DELLA FUNZIONE
     *
     * @return array|void
     */
    public function mightBeInterestedIn($limit, $keywords = "", $tipo_immobile = "", $citta = "", $current_immobile = "", $current_try_ary = array(0, array())) {
        global $firephp;
        Global $MSFrameworkDatabase;
        
        $current_try = $current_try_ary[0];
        $res_to_keep = $current_try_ary[1];

        $order_by = "";
        $order_city_priority = "";
        $oth_filters = "";
        $oth_filters_ary = array();
        $select_string = "id, titolo, geo_data, images, tipo_vendita, ";

        if($keywords != "" || $citta != "") {
            $oth_filters .= " MATCH (descrizione,titolo) AGAINST (:kw1) AND ";
            $oth_filters_ary[":kw1"] = $keywords . " " . $citta;
            $select_string .= " MATCH (descrizione,titolo) AGAINST (:kw2) as punteggio, ";
            $oth_filters_ary[":kw2"] = $keywords . " " . $citta;

            $order_by .= " `punteggio`, ";
        }

        if($tipo_immobile != "") {
            $oth_filters .= " category = :category AND ";
            $oth_filters_ary[":category"] = $tipo_immobile;
        }

        if($current_immobile != "") {
            $oth_filters .= " id != :cur_id AND ";
            $oth_filters_ary[":cur_id"] = $current_immobile;

            $cur_owner = $MSFrameworkDatabase->getAssoc("SELECT owner_id FROM realestate_immobili WHERE id = :id", array(":id" => $current_immobile), true);
            $oth_filters .= " owner_id = :cur_owner_id AND ";
            $oth_filters_ary[":cur_owner_id"] = $cur_owner['owner_id'];
        }

        if($citta != '') {
            $exploded_addr = $this->cleanCityAddressString($citta);
            $exploded_addr_min3 = $this->cleanCityAddressString($citta, 3);

            $close_query_parent = "";
            if(is_array($exploded_addr_min3) && count($exploded_addr_min3) > 0) {
                //ricerca città
                $where_ary_city = $MSFrameworkDatabase->composeSameDBFieldString($exploded_addr_min3, "OR", "name", "LIKE");
                $geoname_or = "";

                foreach ($MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname` WHERE " . $where_ary_city[0], $where_ary_city[1]) as $geonameInfos) {
                    $geoname_or .= " geo_data LIKE '%\"comune\":\"" . $geonameInfos['geonameid'] . "\"%' OR ";
                    $order_city_priority .= " geo_data LIKE '%\"comune\":\"" . $geonameInfos['geonameid'] . "\"%' DESC, ";
                }

                //controllo se ci sono immobili all'interno della stessa provincia della città ricercata (Issue #72)
                foreach($MSFrameworkDatabase->getAssoc("SELECT code FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin2Codes` WHERE " . $where_ary_city[0], $where_ary_city[1]) as $provInfos) {
                    $prov_code = end(explode(".", $provInfos['code']));

                    foreach($MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname` WHERE admin2 = :prov", array(":prov" => $prov_code)) as $geonameInfos) {
                        $geoname_or .= " geo_data LIKE '%\"comune\":\"" . $geonameInfos['geonameid'] . "\"%' OR ";
                    }
                }

                if ($geoname_or != "") {
                    $geoname_or = substr($geoname_or, 0, -4);

                    if($this->search_in_address) {
                        $oth_filters .= "((" . $geoname_or . ") OR ";
                        $close_query_parent = ")";
                    } else {
                        $oth_filters .= "(" . $geoname_or . ") AND ";
                    }
                }
            }

            if(is_array($exploded_addr) && count($exploded_addr) > 0 && $this->search_in_address) {
                //ricerca indirizzo (via)
                foreach($exploded_addr as $piece) {
                    $select_string .= "(LENGTH(`geo_data`) - LENGTH(REPLACE(`geo_data`, '$piece', ''))) / LENGTH('$piece') + ";
                }

                if($select_string != "") {
                    $select_string = substr($select_string, 0, -3) . " `occurences`, ";
                }
                $where_ary = $MSFrameworkDatabase->composeSameDBFieldString($exploded_addr, "OR", "geo_data", "LIKE");
                $oth_filters .= "(" . $where_ary[0] . ")" . $close_query_parent . " AND ";
                $oth_filters_ary = array_merge($oth_filters_ary, $where_ary[1]);
                $order_by .= " `occurences`  DESC, ";
            }
        }

        if($oth_filters != "") {
            $oth_filters = " AND (" . substr($oth_filters, 0, -5) . ")";
        }

        $order_by .= " RAND()";
        if($order_city_priority != "") {
            $order_by = $order_city_priority . $order_by;
        }

        $exclude_old_results = "";
        foreach($res_to_keep as $old_results) {
            $exclude_old_results .= " AND id != '" . $old_results['id'] . "' ";
        }

        $select_string = substr($select_string, 0, -2);

        $r = $MSFrameworkDatabase->getAssoc("SELECT $select_string FROM realestate_immobili WHERE id != '' $exclude_old_results AND stato_vendita = '0' $oth_filters ORDER BY $order_by LIMIT $limit", $oth_filters_ary);

        /*
         * Incremento $new_current di 1 e provo ad eseguire una ricerca un po' più generica fin quando non raggiungo il valore impostato in $limit
         */
        $new_current = $current_try+1;
        $r = array_merge($res_to_keep, $r);

        if(count($r) < $limit) {
            if($new_current == 1) {
                return $this->mightBeInterestedIn($limit, "", $tipo_immobile, $citta, $current_immobile, array($new_current, $r));
            } else if($new_current == 2) {
                return $this->mightBeInterestedIn($limit, "", $tipo_immobile, "", $current_immobile, array($new_current, $r));
            } else {
                return array_slice($r, 0, $limit);
            }
        } else {
            return array_slice($r, 0, $limit);
        }
    }

    private function cleanCityAddressString($str, $use_min = 0) {
        global $firephp;
        $exploded_addr = explode(" ", strtolower($str));

        $exploded_addr = array_filter($exploded_addr, function($v) {
            //rimuovo le parole di uso comune
            return !in_array($v, array("via", "piazza", "viale", "largo", "piazzale", "strada", "piazzetta"));
        });

        if($use_min == 0) {
            return $exploded_addr;
        } else {
            foreach($exploded_addr as $v) {
                if(strlen($v) >= $use_min) {
                    $exploded_addr_min3[] = $v;
                }
            }

            return $exploded_addr_min3;
        }
    }

    /**
     * Questa funzione restituisce i valori che comporranno le select dei prezzi poste all'interno dei box di ricerca
     * @param $type 0|1 Il tipo di range desiderato (vendita/affitto)
     *
     * @return array
     */
    public function getPricesForSearchSelects($type) {
        if($type == "0") {
            return array("5000", "10000", "20000", "40000", "50000", "60000", "70000", "80000", "90000", "100000", "110000", "120000", "130000", "140000", "150000", "160000", "175000", "180000", "200000", "230000", "250000", "280000", "300000", "350000", "400000", "450000", "500000", "600000", "700000", "800000", "900000", "1000000", "1500000", "2000000", "2500000", "3000000", "5000000", "15000000");
        } else if($type == "1") {
            return array("25", "50", "100", "150", "200", "250", "300", "350", "400", "500", "600", "700", "800", "900", "1000", "1250", "1500", "2000", "2500", "3000", "4000", "5000", "7500", "10000", "225000", "275000", "325000", "1500000", "2500000");
        }
    }

    /**
     * Questa funzione restituisce i valori che comporranno le select dei metri quadtrati poste all'interno dei box di ricerca
     *
     * @return array
     */
    public function getMQForSearchSelects() {
        return array("30", "40", "50", "70", "100", "150", "200", "300", "400", "500", "1000", "2000", "5000", "10000", "20000", "50000", "100000");
    }


    /**
     * Questa funzione restituisce l'ID dell'immobile a fronte di un "codice immobile" passato
     *
     * @param $code Il codice immobile
     */
    public function getIDFromCode($code) {
        Global $MSFrameworkDatabase;
        
        $r = $MSFrameworkDatabase->getAssoc("SELECT id FROM realestate_immobili WHERE riferimento = :rife", array(":rife" => $code), true);
        return $r['id'];
    }

    /**
     * Ottiene l'URL per la pagina di dettaglio immobile
     *
     * @param $id L'ID dell'immobile
     * @param bool $landing Se impostato su true, ottiene l'URL per la pagina in versione "landing"
     *
     * @return string
     */
    public function dettaglioImmobileURL($id, $landing = false) {
        global $MSFrameworki18n, $MSFrameworkUrl, $MSFrameworkCMS;

        if(is_array($id) && isset($id['slug'])) $page_det = $id;
        else $page_det = $this->getImmobileDataFromDB($id, "slug");

        $append_landing = ($landing ? $this->getLandingPageSuffix() : "");

        if($MSFrameworkCMS->checkRequestOrigin() == "front") {
            return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . $append_landing . "/";
        } else {
            return $MSFrameworkCMS->getUrlToSite() . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . $append_landing . "/";
        }
    }


    /**
     * Restituisce il suffisso da aggiungere all'URL per richiedere la landing page di un immobile
     *
     * @return string
     */
    public function getLandingPageSuffix() {
        return "-landing";
    }

    public function searchImmobiliURL() {
        global $MSFrameworkCMS;

        return $MSFrameworkCMS->getUrlToSite() . "ricerca-immobile/";
    }
}
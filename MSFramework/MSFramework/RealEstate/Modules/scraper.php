<?php
/**
 * MSFramework
 * Date: 13/02/19
 */

namespace MSFramework\RealEstate\Modules;

class scraper {
    public function __construct() {
        $this->MSFrameworkDatabase = new \MSFramework\database();
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array();

        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {
        return true;
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $array_settings = array();
        return $array_settings;
    }

}
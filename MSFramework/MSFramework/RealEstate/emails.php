<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

namespace MSFramework\RealEstate;

use MSFramework\cms;

class emails extends \MSFramework\emails {
    /**
     * Ottiene la lista di template per le agenzie immobiliari
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {
        $default_address = $this->MSFrameworkCMS->getCMSData("settings")['destination_address'];

        $templates = array(
            'realestate' => array(
                /* TEMPLATE RICHIESTA INFORMAZIONI IMMOBILI */
                'properties-questions/new-question' => array(
                    'nome' => 'Richiesta informazioni immobile',
                    'reply_to' => array("{[email]}", "{[name]}"),
                    'address' => "{[dest]}",
                    'subject' => "Hai ricevuto una nuova richiesta di informazioni - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{name}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{subject}" => "L'oggetto della mail",
                            "{immobile}" => "Il nome dell'immobile",
                            "{message}" => "Il testo del messaggio"
                        ),
                        array(
                            "{[name]}",
                            "{[email]}",
                            "{[subject]}",
                            "{[immobile]}",
                            "{[message]}"
                        )
                    )
                )
            )
        );

        return $templates;
    }

}
<?php
/**
 * MSAdminControlPanel
 * Date: 2019-02-12
 */

namespace MSFramework\RealEstate;


class agents extends \MSFramework\users {
    /**
     * Questa funzione consente di determinare se l'utente connesso ha la possibilità di modificare l'owner_id di una richiesta, immobile, cliente o qualunque altro caso applicabile
     * @return bool
     */
    public function canChangeOwnerId() {
        $allowed = array("0", "realestate-1", "realestate-3", "realestate-4");
        return in_array($this->getUserDataFromSession('userlevel'), $allowed);
    }
}
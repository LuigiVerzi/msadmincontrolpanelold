<?php
/**
 * MSAdminControlPanel
 * Date: 2019-02-12
 */

namespace MSFramework\RealEstate;

class analytics {
    public function __construct() {
        $this->MSFrameworkDatabase = new \MSFramework\database();
        $this->MSFrameworkUtils = new \MSFramework\utils();

        $this->onePerSession = true; //se impostato su true, le statistiche vengono conteggiate univocamente per ogni sessione (10 visite allo stesso immobile nella stessa sessione, contano come 1)
    }

    /**
     * Questa funzione incrementa il numero di visite totali ricevute da un immobile
     *
     * @param $id L'ID dell'immobile
     *
     * @return bool
     */
    public function setVisitToImmobile($id) {
        if($this->onePerSession && in_array($id, $_SESSION['analyticsData']['immobili']['visite'])) {
            return false;
        }

        if(!in_array($id, $_SESSION['analyticsData']['immobili']['visite'])) {
            $_SESSION['analyticsData']['immobili']['visite'][] = $id;
        }

        $this->MSFrameworkDatabase->pushToDB("UPDATE realestate_immobili SET visite = visite + 1 WHERE id = :id", array(":id" => $id));

        return true;
    }

    /**
     * Questa funzione incrementa il numero di contatti totali ricevuti da un immobile
     *
     * @param $id L'ID dell'immobile
     *
     * @return bool
     */
    public function setContactToImmobile($id) {
        if($this->onePerSession && in_array($id, $_SESSION['analyticsData']['immobili']['contatti'])) {
            return false;
        }

        if(!in_array($id, $_SESSION['analyticsData']['immobili']['contatti'])) {
            $_SESSION['analyticsData']['immobili']['contatti'][] = $id;
        }

        $this->MSFrameworkDatabase->pushToDB("UPDATE realestate_immobili SET contatti = contatti + 1 WHERE id = :id", array(":id" => $id));

        return true;
    }

    /**
     * Questa funzione restituisce il numero di visite per un dato immobile
     *
     * @param $id L'ID dell'immobile
     *
     * @return mixed
     */
    public function getVisitsForImmobile($id) {
        $r = $this->MSFrameworkDatabase->getAssoc("SELECT visite FROM realestate_immobili WHERE id = :id", array(":id" => $id), true);
        return $r['visite'];
    }

    /**
     * Questa funzione restituisce il numero di contatti per un dato immobile
     *
     * @param $id L'ID dell'immobile
     *
     * @return mixed
     */
    public function getContactsForImmobile($id) {
        $r = $this->MSFrameworkDatabase->getAssoc("SELECT contatti FROM realestate_immobili WHERE id = :id", array(":id" => $id), true);
        return $r['contatti'];
    }

    /**
     * Questa funzione restituisce l'elenco degli immobili più visti
     *
     * @param int $limit Il numero massimo degli immobili da restituire
     * @param string $fields I campi da prelevare dalla tabella "immobili"
     * @param string $owner Se impostato, limita la ricerca solo a quel particolare agente
     */
    public function getTopViewedImmobili($limit = 5, $fields = "id", $owner = "") {
        $str_where = "";
        $ary_where = array();
        if($owner != "") {
            $str_where = " AND owner_id = :owner ";
            $ary_where[':owner'] = $owner;
        }

        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE visite != '0' $str_where ORDER BY visite DESC LIMIT $limit", $ary_where);
    }

    /**
     * Questa funzione restituisce l'elenco degli immobili più contattati
     *
     * @param int $limit Il numero massimo degli immobili da restituire
     * @param string $fields I campi da prelevare dalla tabella "immobili"
     * @param string $owner Se impostato, limita la ricerca solo a quel particolare agente
     */
    public function getTopContactedImmobili($limit = 5, $fields = "id", $owner = "") {
        $str_where = "";
        $ary_where = array();
        if($owner != "") {
            $str_where = " AND owner_id = :owner ";
            $ary_where[':owner'] = $owner;
        }

        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_immobili WHERE contatti != '0' $str_where ORDER BY contatti DESC LIMIT $limit", $ary_where);
    }

    /**
     * Questa funzione permette di salvare le ricerche nel DB
     *
     * @param array $search_data L'array di dati da salvare. La chiave corrisponde alla colonna all'interno del DB (tabella ricerche)
     *
     * @return bool
     */
    public function saveSearch($search_data = array()) {
        global $firephp;

        if($search_data['categoria'] != "") {
            $search_data['category'] = $search_data['categoria'];
        } else {
            $search_data['category'] = $search_data['tipo_immobile'];
        }

        unset($search_data['categoria']);
        unset($search_data['tipo_immobile']);

        if(count($search_data) == 0) {
            return false;
        }

        //verifico se c'è almeno un filtro impostato. se non ce ne sono, non salvo la ricerca
        $found_filter = false;
        foreach($search_data as $sdk => $sdv) {
            if($sdk == "results") {
                continue;
            }

            if($sdk == "oth_filters") {
                $tmp_sdv = json_decode($sdv, true);
                foreach($tmp_sdv as $tmp_sdvk => $tmp_sdvv) {
                    if($tmp_sdvv != "") {
                        $found_filter = true;
                        break 2;
                    }
                }
            }

            if($sdv != "") {
                $found_filter = true;
                break;
            }
        }

        if(!$found_filter) {
            return false;
        }
        //

        /**
         * TODO: Forse per rendere una ricerca DAVVERO univoca, non c'è bisogno di tener conto dei filtri "minori". Verificare.
         *  Magari è anche giusto salvare le varianti della stessa ricerca, ma forse è necessario raggruppare le ricerche simili quando si creano le statistiche (tramite IP?)
         */
        $id = json_encode($search_data);
        if($this->onePerSession && in_array($id, $_SESSION['analyticsData']['ricerche'])) {
            return false;
        }

        if(!in_array($id, $_SESSION['analyticsData']['ricerche'])) {
            $_SESSION['analyticsData']['ricerche'][] = $id;
        }
        $search_data['user_ip'] = json_encode($this->MSFrameworkUtils->getUserIPAddr());
        $search_data['creation_time'] = time();

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($search_data, 'insert');
        $this->MSFrameworkDatabase->pushToDB("INSERT INTO realestate_ricerche ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

        return true;
    }

    /**
     * Questa funzione consente di ottenere le ricerche effettuate dagli utenti
     *
     * @param int $days_before_limit. Se impostato, limita la ricerca agli ultimi "n" giorni (partendo dalla data odierna). -1 senza limiti.
     * @param string $fields I campi da estrarre dalla tabella "ricerche"
     * @param string $append_where Eventuali condizioni da applicare alla query, nel formato " AND ( ... ) "
     * @param bool $group_by_similar Se impostato su true, raggruppa ricerche simili effettuate dallo stesso IP (rende le ricerche più "univoche" non tenendo conto dei dettagli quali bagni, numero di stanze, ecc)
     */
    public function getSearches($days_before_limit = -1, $fields = "*", $append_where = "", $group_by_similar = true) {
        global $firephp;

        $time_before = 0;
        if($days_before_limit != -1) {
            if($days_before_limit == 1) {
                $time_before = strtotime("-1 day");
            } else if($days_before_limit > 1) {
                $time_before = strtotime("-" . $days_before_limit . " days");
            }
        }

        $group_by_str = "";
        if($group_by_similar) {
            $group_by_str = " GROUP BY user_ip, keywords, riferimento, tipo_vendita, category, comune, price_from, price_to, mq_from, mq_to, results ";
        }

        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM realestate_ricerche WHERE creation_time >= :time $append_where $group_by_str", array(":time" => $time_before));
    }
}
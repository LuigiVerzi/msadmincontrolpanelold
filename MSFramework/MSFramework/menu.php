<?php
/**
 * MSFramework
 * Date: 23/02/18
 */

namespace MSFramework;

class menu {
    private $menuData;

    public function __construct($menu_id = "") {
        global $firephp, $MSFrameworkDatabase, $MSFrameworki18n, $MSFrameworkCMS, $MSFrameworkPages;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->current_language_flag = $MSFrameworki18n->getLanguagesDetails(USING_LANGUAGE_CODE)[USING_LANGUAGE_CODE]['long_code'];

        if($menu_id == "") {
            //per retrocompatibilità (prima c'era un solo menu, ora possono essercene "n") prelevo dal database il menu con ID più piccolo
            $this->menuData = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM menu ORDER BY id asc LIMIT 1", array(), true);
            $firephp->info('Questa istanza della classe menu è stata avviata senza un ID di riferimento. Per retrocompatibilità, è stato utilizzato l\'ID più basso della tabella menu. Si consiglia di specificare un ID');
        } else {
            $this->menuData = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM menu WHERE id = :id", array(":id" => $menu_id), true);
        }

        /* INIZIALIZZO LE VARIABILI DELLE CLASSI */
        $this->MSFrameworkRealEstateCategories = new \MSFramework\RealEstate\categories();
        $this->MSFrameworkRealEstateImmobili = new \MSFramework\RealEstate\immobili();
        $this->MSFrameworkPages = $MSFrameworkPages;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkBlogCategories = new \MSFramework\Blog\categories();
        $this->MSFrameworkEcommerceCategories = new \MSFramework\Ecommerce\categories();
        $this->MSFrameworkServices = new \MSFramework\services();

    }

    /**
     * Restituisce un array associativo (ID Menu => Dati Menu) con i dati relativi al menu
     *
     * @param string $id L'ID del menu (stringa) o dei menu (array) richiesti (se vuoto, vengono recuperati i dati di tutti i menu)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getMenuDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM menu WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array unidimensionale contenente gli ID di tutte le pagine utilizzate nel menu
     *
     * @param array $ary_menu Se passato processa solo il menù indicato
     *
     * @return array L'array degli ID usati nel menu
     */
    public function getPageIDsUsed($ary_menu = array()) {
        $getUniquePageIDUsedGotAry = array();

        if(!$ary_menu) {
            $ary_menu = json_decode($this->menuData['list'], true);
        }

        if(!is_array($ary_menu)) {
            $ary_menu = array();
        }

        foreach($ary_menu as $key => $v) {
            if(!isset($v['type'])) $v['type'] = 'pagina';

            if(!isset($getUniquePageIDUsedGotAry[$v['type']])) {
                $getUniquePageIDUsedGotAry[$v['type']] = array();
            }

            if(isset($v['id'])) {
                $getUniquePageIDUsedGotAry[$v['type']][] = $v['id'];
            }

            if($v['children']) {
                foreach($this->getPageIDsUsed($v['children']) as $ck => $cv) {
                    if(!isset($getUniquePageIDUsedGotAry[$ck])) $getUniquePageIDUsedGotAry[$ck] = array();
                    $getUniquePageIDUsedGotAry[$ck] = array_merge($getUniquePageIDUsedGotAry[$ck], $cv);
                }
            }

        }

        return $getUniquePageIDUsedGotAry;
    }

    /**
     * Compone la struttura HTML del menu
     *
     * @param string $current_active_url L'url della pagina da segnare come attiva
     * @param $html_struct array è un array che consente di indicare i vari elementi che compongono la struttura: di seguito gli elementi dell'array
     *                     main_element: l'elemento html (tag) che contiene le voci principali del menu (elemento padre) - obbligatorio
     *                     main_element_append_class: l'elenco delle classi da aggiungere all'elemento padre - opzionale
     *                     sub_element: l'elemento html (tag) che contiene le voci secondarie del menu (elementi figli) - obbligatorio
     *                     sub_element_append_class: l'elenco delle classi da aggiungere all'elemento figlio - opzionale
     *                     arrow_down: l'elemento HTML che rappresenta la freccia puntata verso il basso - opzionale
     *                     arrow_right: l'elemento HTML che rappresenta la freccia puntata verso il destra - opzionale
     * @param $exclude_page array un array che consente di indicare le pagine da nascondere
     * @return string
     */
    public function composeHTMLStructure($current_active_url = "", $html_struct = array(), $exclude_page = array()) {

        $def_struct = array(
            "main_element" => "<li>",
            "main_element_append_class" => "",
            "main_element_active_class" => "current-menu-item",
            "main_link_append_class" => "",
            "main_link_active_class" => "",
            "dropdown_class" => "dropdown",
            "sub_element" => "<ul>",
            "sub_element_append_class" => "sub-menu",
            "arrow_down" => '<span class="fa fa-caret-down"></span>',
            "arrow_right" => '<span class="fa fa-caret-right"></span>',
        );

        if(!is_array($html_struct)) {
            $html_struct = array();
        }

        foreach($def_struct as $k => $v) {
            if(!isset($html_struct[$k])) {
                $html_struct[$k] = $v;
            }
        }

        $html = "";
        foreach(json_decode($this->menuData['list'], true) as $cur_menu) {

            $main_element_append_class = $html_struct['main_element_append_class'];

            if(isset($exclude_page[$cur_menu['type']]) && in_array($cur_menu['id'], $exclude_page[$cur_menu['type']])) continue;

            $caret = "";
            if(is_array($cur_menu['children'])) {
                $caret = $html_struct['arrow_down'];
            }

            $html_childrens = "";
            $childrens_data = array();
            if(is_array($cur_menu['children'])) {
                $childrens_data = $this->_getChildren($cur_menu['children'], $html_struct, $exclude_page);
                $html_childrens = $childrens_data[0];
            }

            $menu_info = $this->getMenuVoiceDetails($cur_menu);

            if(!$menu_info) continue;

            $menu_settings = array();
            if(isset($cur_menu['settings']) && is_array($cur_menu['settings'])) {
                $menu_settings = $cur_menu['settings'];
            }

            if($menu_info['untranslated_slug'] && !isset(json_decode($menu_info['untranslated_slug'], true)[$this->current_language_flag])) {
                continue;
            }

            if($cur_menu['type'] == 'custom' && count($cur_menu['children']) > 0 && strpos($html_childrens, '<li') === false) {
                continue;
            }

            $set_page_active = "";


            $current_clean_url = trim($_SERVER["REQUEST_URI"], '/');
            if(!empty($current_active_url)) $current_clean_url = trim($current_active_url, '/');
            $current_menu_clean_url = trim(str_replace($this->MSFrameworkCMS->getCleanHost($_SERVER['HTTP_HOST']), '', $this->MSFrameworkCMS->getCleanHost($menu_info['url'])), '/');

            if($current_menu_clean_url == $current_clean_url || $childrens_data[1] === true) {
                $set_page_active = " " . $html_struct['main_element_active_class'];
            }

            if($html_childrens != "") {
                $main_element_append_class .= ' ' . $html_struct['dropdown_class'];
            }

            $css_to_append = '';
            if(isset($menu_settings['css']) && !empty($menu_settings['css'])) {
                $css_to_append .= ' ' . htmlentities($menu_settings['css']);
            }

            $main_element = str_replace(">", " class='" . $main_element_append_class . $set_page_active . "'>", $html_struct['main_element']);

            $html .= $main_element . '<a class="' . $html_struct['main_link_append_class'] .  $css_to_append . '" href="' . $menu_info['url'] . '" ' . (isset($menu_settings['new_tab']) && $menu_settings['new_tab'] === "1" ? 'target="_blank"' : '') . '>' . $menu_info['title'] . ' ' . $caret . '</a>';

            if($html_childrens != "") {
                $html .= $html_childrens;
            }

            $html .= str_replace("<", "</", $html_struct['main_element']);
        }

        return $html;
    }

    private function _getChildren($children, $html_struct, $set_parent_active = false, $exclude_page = "") {

        $sub_element = $html_struct['sub_element'];
        if($html_struct['sub_element_append_class'] != "") {
            $sub_element = str_replace(">", " class='" . $html_struct['sub_element_append_class'] . "'>", $html_struct['sub_element']);
        }
        $html = $sub_element;

        $set_parent_active = false;
        foreach($children as $cur_children) {

            if(isset($exclude_page[$cur_children['type']]) && in_array($cur_children['id'], $exclude_page[$cur_children['type']])) continue;

            $caret = "";
            if(is_array($cur_children['children'])) {
                $caret = $html_struct['arrow_right'];
            }

            $menu_info = $this->getMenuVoiceDetails($cur_children);

            if(!$menu_info) continue;

            $menu_settings = array();
            if(isset($cur_menu['settings']) && is_array($cur_children['settings'])) {
                $menu_settings = $cur_children['settings'];
            }

            if(isset($menu_info['untranslated_slug']) && !isset(json_decode($menu_info['untranslated_slug'], true)[$this->current_language_flag])) continue;

            $html_childrens = "";
            $childrens_data = array();
            if(is_array($cur_children['children'])) {
                $childrens_data = $this->_getChildren($cur_children['children'], $html_struct, $exclude_page);
                $html_childrens = $childrens_data[0];
            }

            $current_clean_url = trim($_SERVER["REQUEST_URI"], '/');
            $current_menu_clean_url = trim(str_replace($this->MSFrameworkCMS->getCleanHost($_SERVER['HTTP_HOST']), '', $this->MSFrameworkCMS->getCleanHost($menu_info['url'])), '/');

            $set_page_active = "";
            if($current_menu_clean_url == $current_clean_url || $childrens_data[1] === true) {
                $set_page_active = " " . $html_struct['main_element_active_class'];
                $set_parent_active = true;
            }

            $css_to_append = '';
            if(isset($menu_settings['css']) && !empty($menu_settings['css'])) {
                $css_to_append .= ' ' . htmlentities($menu_settings['css']);
            }

            $main_element_append_class = $html_struct['main_element_append_class'];
            if($html_childrens != "") {
                $main_element_append_class .= ' ' . $html_struct['dropdown_class'];
            }
            $main_element = str_replace(">", " class='" . $main_element_append_class . $set_page_active . "'>", $html_struct['main_element']);

            $html .= $main_element . '<a class="' . $html_struct['main_link_append_class'] . $css_to_append . '" href="' . $menu_info['url'] . '" ' . (isset($menu_settings['new_tab']) && $menu_settings['new_tab'] === "1" ? 'target="_blank"' : '') . '>' . $menu_info['title'] . ' ' . $caret . '</a>';

            if($html_childrens !== "") {
                $html .= $html_childrens;
            }

            $html .= str_replace("<", "</", $html_struct['main_element']);
        }

        $html .= str_replace("<", "</", $html_struct['sub_element']);

        return array($html, $set_parent_active);
    }

    public function getMenuVoices() {
        Global $MSFrameworkCMS;

        $ary_to_return = array(
            'pagina' => array(
                'name' => 'Pagina',
                'list' => function($pages_used) {
                    Global $MSFrameworki18n;

                    $ary_to_return = array();

                    $where_hidden = "";
                    if($_SESSION['userData']['userlevel'] != "0") $where_hidden =  " AND hidden = 0 ";
                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, titolo, parent FROM pagine WHERE id != '' $where_hidden") as $pagina) {
                        if(!in_array($pagina['id'], $pages_used)) {
                            $ary_to_return[$pagina['id']] = $this->formatMenuInfoByType($pagina, 'pagina');
                        }
                    }

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, titolo FROM pagine_private WHERE slug != '' OR id = 'home-page'") as $pagina) {
                        if(!in_array($pagina['id'], $pages_used)) {
                            $ary_to_return[$pagina['id']] = $this->formatMenuInfoByType($pagina, 'pagina');
                        }
                    }

                    return $ary_to_return;
                },
                'is_active' => function($MSFrameworkCMS) {
                    return true;
                }
            ),
            'shop' => array(
                'name' => 'Ecommerce',
                'list' => function($pages_used) {
                    Global $MSFrameworki18n;
                    $ary_to_return = array();

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, nome FROM ecommerce_categories") as $categoria_ecommerce) {
                        $categoria_ecommerce['parent'] = (new \MSFramework\Ecommerce\categories())->getCategoryParent($categoria_ecommerce['id']);

                        if($categoria_ecommerce['parent']) {
                            $categoria_ecommerce['parent'] = end(array_keys($categoria_ecommerce['parent']));
                        } else {
                            $categoria_ecommerce['parent'] = 0;
                        }

                        if (!in_array($categoria_ecommerce['id'], $pages_used)) {
                            $ary_to_return[] = $this->formatMenuInfoByType($categoria_ecommerce, 'shop');
                        }
                    }

                    return $ary_to_return;
                },
                'is_active' => function($MSFrameworkCMS) {
                    return (in_array('ecommerce', json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true)));
                }
            ),
            'realestate' => array(
                'name' => 'Real Estate',
                'list' => function($pages_used) {
                    $ary_to_return = array();

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, nome FROM realestate_categories") as $categoria_realestate) {
                        if(!in_array($categoria_realestate['id'], $pages_used)) {
                            $ary_to_return[] = $this->formatMenuInfoByType($categoria_realestate, 'realestate');
                        }
                    }

                    return $ary_to_return;
                },
                'is_active' => function($MSFrameworkCMS) {
                    return (in_array('realestate', json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true)));
                }
            ),
            'blog' => array(
                'name' => 'Blog',
                'list' => function($pages_used) {
                    Global $MSFrameworkCMS, $MSFrameworki18n;
                    $ary_to_return = array();

                    if(!in_array(0, $pages_used)) {
                        $ary_to_return[] = $this->formatMenuInfoByType(array(), 'blog');
                    }

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, nome FROM blog_categories") as $categoria_blog) {
                        if(!in_array($categoria_blog['id'], $pages_used)) {
                            $ary_to_return[] = $this->formatMenuInfoByType($categoria_blog, 'blog');
                        }
                    }

                    return $ary_to_return;
                },
                'is_active' => function($MSFrameworkCMS) {
                    return (in_array('blog', json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true)));
                }
            ),
            'service_sector' => array(
                'name' => 'Settori Servizi',
                'list' => function($pages_used) {
                    $ary_to_return = array();

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, nome FROM servizi_settori") as $settore) {
                        if(!isset($pages_used['service_sector']) || !in_array($settore['id'], $pages_used['service_sector'])) {
                            $ary_to_return[] = $this->formatMenuInfoByType($settore, 'service_sector');
                        }
                    }

                    return $ary_to_return;
                },
                'is_active' => function($MSFrameworkCMS) {
                    return $this->MSFrameworkDatabase->getCount("SELECT id FROM servizi_settori");
                }
            ),
            'service_category' => array(
                'name' => 'Categoria Servizi',
                'list' => function($pages_used) {
                    $ary_to_return = array();

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, nome FROM servizi_categorie") as $categoria) {
                        if(!isset($pages_used['service_category']) || !in_array($categoria['id'], $pages_used['service_category'])) {
                            $ary_to_return[] = $this->formatMenuInfoByType($categoria, 'service_category');
                        }
                    }

                    return $ary_to_return;
                },
                'is_active' => function($MSFrameworkCMS) {
                    return $this->MSFrameworkDatabase->getCount("SELECT id FROM servizi_categorie");
                }
            ),
            'service' => array(
                'name' => 'Servizi',
                'list' => function($pages_used) {
                    $ary_to_return = array();

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, nome FROM servizi") as $servizio) {
                        if(!in_array($servizio['id'], $pages_used)) {
                            $ary_to_return[] = $this->formatMenuInfoByType($servizio, 'service');
                        }
                    }

                    return $ary_to_return;
                },
                'is_active' => function($MSFrameworkCMS) {
                    return $this->MSFrameworkDatabase->getCount("SELECT id FROM servizi");
                }
            ),
        );

        $pages_used = $this->getPageIDsUsed();
        foreach($ary_to_return as $k => $ary) {
            if($ary['is_active']($MSFrameworkCMS)) {
                $list = $this->formatMenuVoiceHierarchy($ary['list']((isset($pages_used[$k]) ? $pages_used[$k] : array())), 0);
                $ary_to_return[$k]['list'] = $list;
            } else {
                unset($ary_to_return[$k]);
            }
        }

        return $ary_to_return;
    }

    private function formatMenuVoiceHierarchy($elements, $parentId = 0) {
        $branch = array();

        foreach($elements as $k => $e) {
            if(!in_array($e['parent'], array_keys($elements))) {
                $elements[$k]['parent'] = 0;
            }
        }

        foreach ($elements as $element) {
            if(isset($element['parent']) && $element['parent'] == $parentId && $parentId !== 0) {
                    $children = $this->formatMenuVoiceHierarchy($elements, $element['id']);
                    if ($children) {
                        $element['children'] = $children;
                    }
                    $branch[] = $element;
            } else {
                $branch[] = $element;
            }
        }

        return $branch;
    }

    /**
     * Ottiene le informazioni reali (url, titolo etc) del menù passato
     *
     * @param array $menu L'array della voce attuale
     *
     * @return array L'array con la voce formattata del menù
     */
    public function getMenuVoiceDetails($menu) {
        Global $MSFrameworki18n, $MSFrameworkCMS, $MSFrameworkUrl;

        $ary_to_return = array();

        if($menu['type'] == 'blog') {
            if($menu['id']) {
                $page_detail = $this->MSFrameworkBlogCategories->getCategoryDetails($menu['id']);
                if($page_detail) {
                    $ary_to_return = array(
                        'title' => $MSFrameworki18n->getFieldValue($page_detail[$menu['id']]['nome']),
                        'url' => $this->MSFrameworkBlogCategories->getURL($menu['id']),
                        'untranslated_slug' => $page_detail[$menu['id']]['slug']
                    );
                }
            }
            else
            {
                $blog_preferenze = $MSFrameworkCMS->getCMSData('blog');
                $ary_to_return = array(
                    'title' => $MSFrameworki18n->getFieldValue($blog_preferenze['titolo']),
                    'url' => ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworki18n->getFieldValue($blog_preferenze['slug']),
                    'untranslated_slug' => $blog_preferenze['slug']
                );
            }
        }
        else if($menu['type'] == 'shop') {
            $page_detail = $this->MSFrameworkEcommerceCategories->getCategoryDetails($menu['id']);
            if($page_detail) {
                $ary_to_return = array(
                    'title' => $MSFrameworki18n->getFieldValue($page_detail[$menu['id']]['nome']),
                    'url' => $this->MSFrameworkEcommerceCategories->getURL($menu['id']),
                    'untranslated_slug' => $page_detail[$menu['id']]['slug']
                );
            }
        }
        else if($menu['type'] == 'realestate') {
            if($this->MSFrameworkRealEstateImmobili->getImmobiliForCat($menu['id'], 'count', 'id') == 0) return array();

            $page_detail = $this->MSFrameworkRealEstateCategories->getCategoryDetails($menu['id']);
            if($page_detail) {
                $ary_to_return = array(
                    'title' => $MSFrameworki18n->getFieldValue($page_detail[$menu['id']]['nome']),
                    'url' => $this->MSFrameworkRealEstateCategories->getURL($menu['id']),
                    'untranslated_slug' => $page_detail[$menu['id']]['slug']
                );
            }

        }
        else if($menu['type'] == 'service_sector') {

            $page_detail = $this->MSFrameworkServices->getSectorDetails($menu['id']);
            if($page_detail) {
                $ary_to_return = array(
                    'title' => $MSFrameworki18n->getFieldValue($page_detail[$menu['id']]['nome']),
                    'url' => $this->MSFrameworkServices->getServiceURL($page_detail[$menu['id']]),
                    'untranslated_slug' => $page_detail[$menu['id']]['slug']
                );
            }
        }
        else if($menu['type'] == 'service_category') {

            $page_detail = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM servizi_categorie WHERE id = :id", array(':id' => $menu['id']), true);
            if($page_detail) {
                $ary_to_return = array(
                    'title' => $MSFrameworki18n->getFieldValue($page_detail['nome']),
                    'url' => $this->MSFrameworkServices->getServiceURL($page_detail),
                    'untranslated_slug' => $page_detail['slug']
                );
            }
        }
        else if($menu['type'] == 'service') {

            $page_detail = $this->MSFrameworkServices->getServiceDetails($menu['id']);
            if($page_detail) {
                $ary_to_return = array(
                    'title' => $MSFrameworki18n->getFieldValue($page_detail[$menu['id']]['nome']),
                    'url' => $this->MSFrameworkServices->getServiceURL($page_detail[$menu['id']]),
                    'untranslated_slug' => $page_detail[$menu['id']]['slug']
                );
            }

        } else if($menu['type'] == 'custom') {

            $ary_to_return = array(
                'title' => $MSFrameworki18n->getFieldValue($menu['multilang_title']),
                'url' => $menu['link']
            );

        } else if($menu['type'] == 'pagina-standard') {


        } else {

            if(is_numeric($menu['id'])) {
                $page_detail = $this->MSFrameworkPages->getPageDetails($menu['id']);
                if ($page_detail) {
                    $ary_to_return = array(
                        'title' => $MSFrameworki18n->getFieldValue($page_detail[$menu['id']]['titolo']),
                        'url' => $this->MSFrameworkPages->getURL($page_detail[$menu['id']]),
                        'untranslated_slug' => $page_detail[$menu['id']]['slug']
                    );
                }
            } else {
                $page_detail = $this->MSFrameworkPages->getPrivatePageData($menu['id']);

                $ary_to_return = array(
                    'title' => $MSFrameworki18n->getFieldValue($page_detail['titolo']),
                    'url' => $this->MSFrameworkPages->getPrivatePageURL($menu['id'])
                );
            }

        }

        if($ary_to_return) {
            $ary_to_return['original_title'] = ($menu['type'] !== 'custom' ? $ary_to_return['title'] : '');
        }

        if(isset($menu['multilang_title']) && is_array($menu['multilang_title'])) {
            $ary_to_return['multilang_title'] = json_encode($menu['multilang_title']);
            $multilang_title =  $MSFrameworki18n->getFieldValue($ary_to_return['multilang_title']);
            if(!empty($multilang_title)) {
                $ary_to_return['title'] = $multilang_title;
            }
        }

        return $ary_to_return;
    }

    private function formatMenuInfoByType($menu, $type) {
        Global $MSFrameworki18n, $MSFrameworkCMS;

        $ary_to_return = array(
            'id' => ($menu ? $menu['id'] : '')
        );

        $ary_key_groups = array(
            'titolo' => array('pagina', 'pagina-standard'),
            'nome' => array('shop', 'realestate', 'blog', 'service_sector', 'service_category', 'service')
        );

        foreach($ary_key_groups as $key => $types) {
            if(in_array($type, $types)) {
                $ary_to_return['name'] = (isset($menu[$key]) ? $MSFrameworki18n->getFieldValue($menu[$key]) : '');
            }
        }

        if(!$menu) {
            if ($type == 'blog') {
                $blog_preferenze = $MSFrameworkCMS->getCMSData('blog');
                $ary_to_return['name'] = $MSFrameworki18n->getFieldValue($blog_preferenze['titolo']);
            }
        }

        if(isset($menu['parent'])) $ary_to_return['parent'] = $menu['parent'];

        return $ary_to_return;
    }


}
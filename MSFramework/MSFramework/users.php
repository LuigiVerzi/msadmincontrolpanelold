<?php
/**
 * MSFramework
 * Date: 17/02/18
 */

namespace MSFramework;


class users {
    public function __construct($force_db = false) {
        Global $MSFrameworkDatabase, $MSFrameworkCMS, $MSFrameworkSaaSBase;

        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkSaaS = $MSFrameworkSaaSBase;
        $this->MSFrameworkSaaSEnvironments = new \MSFramework\SaaS\environments();

        $this->forcingDB = $force_db; //todo: se viene forzato un DB diverso da quello in uso (es: per registrare un cliente del SaaS su MarketingStudio) uso questa variabile per evitare di eseguire azioni che opererebbero sul db attuale (o userebbero dati del dominio corrente, come ad esempio i path degli uploads) e non su quello forzato. Sarebbe utile trovare una soluzione per gestire questa casistica in maniera semplice e globale
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        if($force_db !== false) {
            $this->MSFrameworkDatabase = new \MSFramework\database(FRAMEWORK_DB_HOST, $force_db, FRAMEWORK_DB_USER, FRAMEWORK_DB_PASS, "mysql", "utf8");
        }

    }

    /**
     * Restituisce i livelli utente disponibili in MarketingStudio ACP
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     * @param string $include_custom Se impostato su true, include nell'array anche i livelli utenti personalizzati. E' automaticamente true se nella $key è presente il testo custom-
     *
     * @return array
     */
    public function getUserLevels($key = "", $include_custom = false) {
        $levels_ary['global'] = array( //Il livello "0" è riservato ai Super Admin
            "1" => "Admin"
        );

        $levels_ary['realestate'] = array(
            "3" => "Broker",
            "4" => "Office Manager",
            "5" => "Consulente", //questo livello ha varie limitazioni qua e la nel SW
            "6" => "Assistente", //questo livello ha varie limitazioni qua e la nel SW
        );

        if($this->MSFrameworkCMS->checkExtraFunctionsStatus('realestate')) {
            unset($levels_ary['global']);
        }

        foreach($levels_ary as $k => $v) {
            if($k == "global" || $this->MSFrameworkCMS->checkExtraFunctionsStatus($k)) {
                foreach($v as $levelK => $levelV) {
                    $prefix = ($k == "global" ? "" : $k . '-');
                    $levels[$prefix . $levelK] = $levelV;
                }
            }
        }

        if(strstr($key, 'custom-') || $include_custom) {
            foreach($this->getCustomUserLevels() as $customLevel) {
                $levels['custom-' . $customLevel['id']] = $customLevel['name'];
            }
        }

        if($key != "") {
            if($key == "0") {
                return "Super Admin";
            } else {
                return $levels[$key];
            }
        } else {
            return $levels;
        }
    }

    /**
     * Restituisce un array associativo (ID Livello Utente => Dati Livello Utente) con i dati relativi al livello utente.
     *
     * @param $id L'ID del livello utente (stringa) o dei livelli utenti (array) richiesti (se vuoto, vengono recuperati i dati di tutti i livelli)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCustomUserLevels($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = "";
        $append_where_ary = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM users_roles WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ")", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce i livelli utente disponibili in MarketingStudio Agency
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array
     */
    public function getMSAgencyLevels($key = "") {
        $levels = array(
            "0" => "SuperAdmin",
            "1" => "Consulente Marketing Studio",
            "2" => "Consulente Semplice"
        );


        if($key != "") {
            if($key == "0") {
                return "Super Admin";
            } else {
                return $levels[$key];
            }
        } else {
            return $levels;
        }
    }

    /**
     * Restituisce i "Super Admin" di MarketingStudio. Un Super Admin è colui che può gestire il SW ad un livello ancora più alto dell'admin del sito.
     * In pratica, è il proprietario del SW MarketingStudio. La password è criptata con la funzione password_hash()
     *
     * @param $email Se impostato, cerca un superadmin con l'indirizzo email specificato
     *
     * @return array
     */
    public function getSuperAdmins($email = "") {
        $append_where = "";
        $append_where_ary = array();

        if($email != "") {
            $append_where = " AND email = :email";
            $append_where_ary[':email'] = $email;
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`users` WHERE ms_agency = '0' $append_where", $append_where_ary) as $r) {
            $r['global'] = true;
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce gli utenti globali di MarketingStudio.
     *
     * @param $email Se impostato, cerca un superadmin con l'indirizzo email specificato
     *
     * @return array
     */
    public function getGlobalUsers($email = "") {
        $append_where = "";
        $append_where_ary = array();

        if($email != "") {
            $append_where = " AND email = :email";
            $append_where_ary[':email'] = $email;
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`users` WHERE id != '0' $append_where", $append_where_ary) as $r) {
            $r['global'] = true;
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene un elenco di utenti per un determinato livello
     *
     * @param $level Il livello da cercare
     * @param string $fields I campi da prelevare dal DB
     * @param string $just_active Se impostato su true, vengono restituiti solo gli utenti attivi
     *
     * @return bool
     */
    public function getUsersByLevel($level, $fields = "*", $just_active = true) {
        if($level == "") {
            return false;
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = "";
        if($just_active) {
            $append_where .= " AND active = '1' ";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM users WHERE ruolo = :ruolo $append_where", array(":ruolo" => $level)) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Controlla se le credenziali immesse nel form di login sono valide. In tal caso, attiva la sessione dell'utente
     *
     * @param $user Il nome utente
     * @param $pass La password
     *
     * @return bool
     */
    public function checkCredentials($user, $pass) {

        if($user == "" || $pass == "") {
            return array(false, "E' necessario fornire nome utente e password");
        }

        $globalusers = $this->getGlobalUsers($user);

        if($globalusers) {
            if(array_values($globalusers)[0]['ms_agency'] == 2) $globalusers = array();
        }

        if($this->MSFrameworkSaaS->isSaaSDomain() && count($globalusers) == 0) { //sono in ambiente SaaS e non si sta loggando un superadmin, effettuo un controllo preliminare per determinare il DB
            $user_db = $this->MSFrameworkDatabase->getAssoc("SELECT id, expiration_date FROM `" . FRAMEWORK_DB_NAME . "`.`saas__environments` WHERE (user_email = :user OR FIND_IN_SET(:user2, child_accounts)) AND set_offline != '1' AND saas_id = :saas_id", array(":user" => $user, ":user2" => $user, ":saas_id" => $this->MSFrameworkSaaS->getID()), true);
            if($user_db['id'] == "") {
                return array(false, "Non è stato possibile trovare l'utente o l'accesso alla piattaforma è stato disabilitato");
            } else {
                $MSSaaSEnvironments = new \MSFramework\SaaS\environments($user_db['id']);
                $_SESSION['SaaS']['saas__environments']['database'] = $MSSaaSEnvironments->getDBName();
                $_SESSION['SaaS']['saas__environments']['id'] = $user_db['id'];
                $this->MSFrameworkDatabase = new \MSFramework\database();
            }
        }

        $error_message = "";
        $can_login = false;

        $r_users = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM users WHERE email = :email", array(":email" => $user));
        $r_users = (is_array($r_users) ? $r_users : array());

        $r = array_merge($globalusers, $r_users)[0]; //dovrebbe esserci sempre una sola occorrenza, nel caso di più eventi (es: email corrisponde sia ad utente normale che a superadmin) do la priorità al superadmin

        if($r['id'] != "") {

            // Controllo se l'utente è un consulente e nel caso se ha il permesso di accedere al sito
            if(!FRAMEWORK_DEBUG_MODE && ($r['ms_agency'] == "2" && CUSTOMER_DOMAIN_INFO['id'] != 41 && CUSTOMER_DOMAIN_INFO['owner'] != $r['id'])) {
                $error_message = "Non hai i permessi necessari per visualizzare il sito";
            }

            if(($r['password'] == "" || !password_verify($pass, $r['password'])) && $error_message == "") {
                $error_message = "La password fornita non è valida";
            }

            if($r['active'] != "1" && $error_message == "") {
                $error_message = "L'account utente non è attivo";
            }

            if($r['mail_auth'] != "" && $error_message == "") {
                $error_message = "E' necessario verificare il proprio indirizzo email prima di potersi loggare. Controlla la tua casella di posta per poter verificare il tuo indirizzo email.";
            }

            if($this->MSFrameworkSaaS->isSaaSDomain() && count($globalusers) == 0 && $r['id'] != $MSSaaSEnvironments->getOwnerID() && $MSSaaSEnvironments->isExpired()) {
                $error_message = "L'abbonamento è scaduto. L'amministratore di questo abbonamento deve effettuare il rinnovo prima che tu possa effettuare il login.";
            }

            if($error_message == "") {
                $can_login = true;
            }
        } else {
            $error_message = "L'indirizzo email fornito non risulta iscritto al sito";
        }

        if($error_message == "") {
            $error_message = "Si è verificato un errore in fase di login";
        }

        if($can_login) {
            $_SESSION['userData']['id'] = (isset($r['global']) ? 'global-' : '') . $r['id'];
            $_SESSION['userData']['username'] = $user;
            $_SESSION['userData']['email'] = $user;
            $_SESSION['userData']['user_id'] = $_SESSION['userData']['id'];
            $_SESSION['userData']['userlevel'] = $r['ruolo'];
            $_SESSION['userData']['nome'] = $r['nome'];
            $_SESSION['userData']['cognome'] = $r['cognome'];
            $_SESSION['userData']['is_global'] = (isset($r['global']) ? true : false);
            $_SESSION['userData']['is_owner'] = ($_SESSION['userData']['is_global'] && $r['ms_agency'] == 0 ? true : false);
            $_SESSION['userData']['ms_agency'] = $r['ms_agency'];
            $_SESSION['userData']['global'] = $r['global'];
            $_SESSION['start_time'] = time();

            if($this->MSFrameworkSaaS->isSaaSDomain() && count($globalusers) != 0) { //si sta loggando un superadmin in ambiente SaaS, devo implementare un passaggio supplementare per verificare quale tipo di SaaS voglia usare
                return array(true, "saas_superadmin");
            } else {
                return array(true, '');
            }
        } else {
            return array(false, $error_message);
        }
    }

    /**
     * Distrugge la sessione dell'utente e lo reindirizza alla pagina di login
     *
     * @param string $append_par Passando questo valore viene aggiunto un parametro alla fine dell'URL
     */
    public function endUserSession($append_par = "") {
        session_destroy();
        header("location: " . ABSOLUTE_SW_PATH_HTML . "login.php" . ($append_par != "" ? "?" . $append_par : ""));
        die();
    }

    /**
     * Recupera i dati dell'utente salvati in $_SESSION.
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return mixed
     */
    public function getUserDataFromSession($key = "") {
        
        if($key != "") {
            return $_SESSION['userData'][$key];
        } else {
            return $_SESSION['userData'];
        }
    }

    /**
     * In base all'ID passato, restituisce il nome della tabella da utilizzare per l'utente in questione
     *
     * @param $id Se l'id contiene "global-" allora i dati vengono prelevati dalla tabella generale del framework
     *
     * @return string
     */
    public function getUsersTable($id) {
        return (!strstr($id, "global-") ? "users" : "`" . FRAMEWORK_DB_NAME . "`.`users`");
    }

    /**
     * Recupera i dati dell'utente dal database
     *
     * @param string $id L'ID dell'utente.
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getUserDataFromDB($id, $fields = "*") {
        $data = $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . " FROM " . $this->getUsersTable($id) . " WHERE id = :id", array(":id" => str_replace("global-", "", $id)), true);
        if($data) $data['id'] = $id;
        return $data;
    }

    /**
     * Attiva l'account dell'utente
     *
     * @param $auth La stringa auth code dell'utente da attivare
     * @param $env_id L'ID dell'ambiente SaaS per il quale si sta attivando l'utente (utile per effettuare un pre-link e capire il DB da utilizzare)
     *
     * @return bool
     */
    public function activateAccount($auth, $env_id = "") {
        $found_act = false;
        $isSaaS = $this->MSFrameworkSaaS->isSaaSDomain();
        $prepend_db = "";

        if($isSaaS) {
            $MSSaaSEnvironments = new \MSFramework\SaaS\environments($env_id);
            $prepend_db = "`" . $MSSaaSEnvironments->getDBName() . "`.";
        }

        $tables = array($prepend_db . "`users`", "`" . FRAMEWORK_DB_NAME . "`.`users`");

        foreach($tables as $table) {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT id, mail_auth FROM " . $table . " WHERE mail_auth = :auth", array(":auth" => $auth), true);
            if(count($r) != 0) {
                $this->MSFrameworkDatabase->pushToDB("UPDATE " . $table . " SET mail_auth = '' WHERE mail_auth = :auth", array(":auth" => $auth));

                $found_act = true;
                break;
            }
        }

        if($found_act && $isSaaS) {
            $MSFrameworkEnvironments = new \MSFramework\SaaS\environments($env_id);
            $MSFrameworkEnvironments->linkUserToEnvironment($r['id']);
        }

        return $found_act;
    }


    /**
     * Controlla se l'indirizzo email è presente nel DB
     *
     * @param $email L'indirizzo da controllare
     * @param bool $include_globalusers se impostato su true, include nel controllo anche gli indirizzi dei superadmin
     *
     * @return bool
     */
    public function checkIfMailExists($email, $include_globalusers = true) {
        global $MSFrameworkSaaSEnvironments;

        $isSaaS = $this->MSFrameworkSaaS->isSaaSDomain();

        if($isSaaS) {
            if($MSFrameworkSaaSEnvironments->getEnvIDByMail($email) !== false) {
                return true;
            }
        } else {
            if($this->MSFrameworkDatabase->getCount("SELECT email FROM users WHERE email = :email", array(":email" => $email), true) != 0) {
                return true;
            }
        }

        if($include_globalusers) {
            if(array_search($email, array_column($this->getGlobalUsers(), 'email')) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * Genera (e salva nel DB) il token necessario per il reset della password dell'utente.
     *
     * @param $email L'email per la quale creare il token
     */
    public function generatePassResetToken($email) {
        global $MSFrameworkSaaSEnvironments;

        $token = sha1($email . time() . rand(0, 100));
        if(count($this->getGlobalUsers($email)) > 0) {
            return $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`users` SET passreset_token = :token WHERE email = :mail", array(":token" => $token, ":mail" => $email));
        } else {
            $isSaaS = $this->MSFrameworkSaaS->isSaaSDomain();

            if($isSaaS) {
                $db_name = (new \MSFramework\SaaS\environments($MSFrameworkSaaSEnvironments->getEnvIDByMail($email)))->getDBName();
                $set_table = "`$db_name`.`users`";
            } else {
                $set_table = "`users`";
            }

            return $this->MSFrameworkDatabase->pushToDB("UPDATE $set_table SET passreset_token = :token WHERE email = :mail", array(":token" => $token, ":mail" => $email));
        }
    }

    /**
     * Verifica se la password è sufficientemente forte. Restituisce una stringa di errore se il test fallisce, o una stringa vuota in caso di test positivo
     *
     * @param $pwd La password da controllare
     *
     * @return string
     */
    public function checkPasswordStrength($pwd) {
        $errors = "";

        if (strlen($pwd) < 8) {
            $errors .= "è troppo corta, ";
        }

        if (!preg_match("#[0-9]+#", $pwd)) {
            $errors .= "deve contenere almeno un numero, ";
        }

        if (!preg_match("#[a-zA-Z]+#", $pwd)) {
            $errors .= "deve contenere almeno una lettera, ";
        }

        if($errors != "") {
            $errors = "La password " . substr($errors, 0, -2);
        }

        return $errors;
    }

    /**
     * Crea un nuovo account nel DB
     *
     * @param $nome Il nome dell'utente
     * @param $cognome Il cognome dell'utente
     * @param $email La mail dell'utente
     * @param $password La password dell'utente
     * @param $cellulare Il cellulare dell'utente
     * @param $livello Il livello dell'utente
     * @param $extra L'array da salvare nella colonna "extra_data"
     * @param $oth Un array di dati supplementari non obbligatori
     * @param $send_emails Se impostato su true, invia eventuali email (di conferma/di richiesta attivazione account/di notifica nuovo utente) in base alle configurazioni impostate
     * @param $force_db Se dievrso da false, forza la creazione dell'utente sul database specificato (utile nei SaaS)
     *
     * @return bool
     */
    public function registerUser($nome, $cognome, $email, $password, $cellulare, $livello, $extra = array(), $oth = array(), $send_emails = false, $force_db = false) {
        global $MSFrameworkUrl;

        $r_settings = $this->MSFrameworkCMS->getCMSData('site');
        $r_settings_reg = json_decode($r_settings['registration'], true);

        $mail_auth = sha1($email . time());
        $str_confirm_mail_confirm_template = "account-activation";
        $ary_params = array("code" => $mail_auth);

        if($r_settings_reg['request_mail_confirm'] === "0") {
            $mail_auth = "";
            $str_confirm_mail_confirm_template = "account-activation-no-confirm";
            $ary_params = array();
        }

        $slug = $MSFrameworkUrl->cleanString($nome . " " . $cognome);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug)) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $array_to_save = array(
            "nome" => $nome,
            "cognome" => $cognome,
            "ruolo" => $livello,
            "email" => $email,
            "cellulare" => $cellulare,
            "password" => password_hash($password, PASSWORD_DEFAULT),
            "active" => ($r_settings_reg['request_admin_confirm'] === "1" ? "0" : "1"),
            "mail_auth" => $mail_auth,
            "extra_data" => json_encode($extra),
            "slug" => $slug,
            "agency" => $oth['agency'],
        );

        $prepend_db_string = "";
        if($force_db !== false) {
            $prepend_db_string = "`" . $force_db . "`.";
        }

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "insert");
        if($this->MSFrameworkDatabase->pushToDB("INSERT INTO " . $prepend_db_string . "`users` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0])) {
            $new_user_id = $this->MSFrameworkDatabase->lastInsertId();

            if($send_emails) {
                (new \MSFramework\emails(false, $this->MSFrameworkSaaS->isSaaSDomain()))->sendMail($str_confirm_mail_confirm_template, array_merge(array("email" => $email), $ary_params, $oth));

                if($r_settings_reg['request_admin_confirm'] == "1") {
                    $admin_levels = array("1");
                    if($this->MSFrameworkCMS->checkExtraFunctionsStatus('realestate')) {
                        $admin_levels[] = "realestate-4";
                    }

                    foreach($admin_levels as $level) {
                        foreach($this->getUsersByLevel($level, "email, nome") as $admin) {
                            if($admin['email'] != "") {
                                (new \MSFramework\emails())->sendMail('notify-admin-activate-new-user', array("admin_email" => $admin['email'], "nome" => $admin['nome'], "email" => $email));
                            }
                        }
                    }
                }
            }

            /* CERCA DI OTTENERE IL GRAVATAR DEL CLIENTE E NEL CASO IMPOSTARLO */
            $this->setGravatarIfExist($new_user_id);

            return $new_user_id;
        } else {
            return false;
        }
    }

    /**
     * Controlla l'esistenza del gravatar e nel caso lo imposta come avatar dell'utente
     *
     * @param $user_ref mixed L'indirizzo email o l'id dell'utente
     *
     * @return mixed Ritorna false se l'avatar non è stato trovato oppure l'indirizzo dell'immagine
     */
    public function setGravatarIfExist($user_ref)
    {
        $user_info = false;
        if (is_numeric($user_ref)) {
            $user_info = $this->getUserDataFromDB($user_ref);
        } else if((filter_var($user_ref, FILTER_VALIDATE_EMAIL)) ) {
            $user_info = $this->getUserDataFromDB($user_ref);
        }

        if($user_info) {
            $gravemail = md5(strtolower(trim($user_info['email'])));
            if (strpos(get_headers("http://www.gravatar.com/avatar/" . $gravemail . "?d=404")[0], '404') === false) {
                $avatar_name = 'gravatar_' . $user_info['id'] . '.jpeg';
                copy("http://www.gravatar.com/avatar/" . $gravemail . '?s=500', UPLOAD_TMP_FOR_DOMAIN . $avatar_name);
                copy("http://www.gravatar.com/avatar/" . $gravemail . '?s=150', UPLOAD_TMP_FOR_DOMAIN . 'tn/' . $avatar_name);
                $uploader = new \MSFramework\uploads('PROPIC', stristr($user_ref, 'global') ? true : false);
                $ary_files = $uploader->prepareForSave(array($avatar_name));
                if($ary_files !== false) {
                    $this->MSFrameworkDatabase->pushToDB("UPDATE " . (stristr($user_ref, 'global') ? FRAMEWORK_DB_NAME . '.' : '') . "users SET propic = :avatar WHERE id = :id", array(':avatar' => json_encode(array($avatar_name)), ':id' => $user_info['id']));
                    return UPLOAD_TMP_FOR_DOMAIN_HTML . $avatar_name;
                }
            }
        }
        return false;
    }

    /**
     * Conta il numero totale degli utenti iscritti
     *
     * @return mixed
     */
    public function countRegisteredUsers() {
        return $this->MSFrameworkDatabase->getCount("SELECT id FROM users WHERE active = 1 AND ruolo != 0");
    }

    /**
     * Controlla se l'utente è un bot
     *
     * @return boolean
     */
    public function isAnBot() {

        // User lowercase string for comparison.
        $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);

        // A list of some common words used only for bots and crawlers.
        $bot_identifiers = array(
            'bot',
            'slurp',
            'patrol',
            'crawler',
            'crawl',
            'spider',
            'curl',
            'facebook',
            'fetch',
            'whatsapp',
            'mediapartners'
        );

        // See if one of the identifiers is in the UA string.
        foreach ($bot_identifiers as $identifier) {
            if (strpos($user_agent, $identifier) !== FALSE) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Ottiene l'URL dell'utente
     *
     * @param $id L'ID dell'utente per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworkUrl;
        if(is_array($id) && isset($id['slug'])) $page_det = $id;
        else $page_det = $this->getUserDataFromDB($id, "slug");

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($page_det['slug']) . "/";
    }

    /**
     * Restituisce un array associativo (ID Utente => Dati Utente) con i dati relativi all'utente.
     *
     * @param $id L'ID dell'utente (stringa) o degli utenti (array) richiesti (se vuoto, vengono recuperati i dati di tutti gli utenti)
     * @param string $fields I campi da prelevare dal DB
     * @param bool $just_show_on_website Se impostato su "true" recupera solo i dati degli utenti per il quale la spunta "Mostra sul Sito" è attiva
     *
     * @return array
     */
    public function getUserDetails($id = "",  $fields = "*", $just_show_on_website = false) {
        $get_from_global = array();

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $tmp_id = $id;
            foreach($id as $cur_idK => $cur_id) {
                if(strstr($cur_id, "global-")) {
                    unset($tmp_id[$cur_idK]);
                    $get_from_global[] = str_replace("global-", "", $cur_id);
                }
            }

            $id = $tmp_id;
        }

        $ary_to_return = array();
        if ($fields != "*" && !in_array('id', explode(',', str_replace(' ', '', $fields)))) {
            $fields .= ", id";
        }

        foreach(array("users", "`" . FRAMEWORK_DB_NAME . "`.`users`") as $table) {

            $prepend_id = "";

            $ary_to_search = $id;
            if($table == "`" . FRAMEWORK_DB_NAME . "`.`users`") {
                $prepend_id = 'global-';
                $ary_to_search = $get_from_global;

                if($id && !$ary_to_search) continue;
            } else {
                if(!$id && $get_from_global) continue;
            }

            if($ary_to_search) {
                $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($ary_to_search, "OR", "id");
            } else {
                $same_db_string_ary = array(" id != '' ", array());
            }

            $append_where_str = "";
            if($just_show_on_website) {
                $append_where_str .= " AND show_on_website = '1' ";
            }

            foreach ($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM $table WHERE id != '' $append_where_str AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
                $r['gallery_friendly'] = array();
                $r['gallery_friendly']['propic'] = (new \MSFramework\uploads('PROPIC', ($table == "`" . FRAMEWORK_DB_NAME . "`.`users`")))->composeGalleryFriendlyArray(json_decode($r['propic'], true));
                $ary_to_return[$prepend_id . $r['id']] = $r;
            }
        }

        return $ary_to_return;
    }

    /**
     * Ottiene l'ID dell'utente a partire dalla mail
     *
     * @param $email L'indirizzo email per il quale ottenere l'ID
     *
     * @return bool
     */
    public function getIDFromEmail($email) {
        if($email == "") return false;

        return $this->MSFrameworkDatabase->getAssoc("SELECT id FROM users WHERE email = :email", array(":email" => $email), true)['id'];
    }

    /**
     * Ottiene l'anteprima del cliente in HTML
     * @param mixed L'ID del cliente o l'array contenente le info
     * @param boolean $show_small Se impostato msotra una versione più piccola
     * @return string
     */
    public function getUserPreviewHTML($data, $show_small = false) {
        if(!is_array($data)) {
            $data = $this->getUserDataFromDB($data);
        }

        $return_html = '';

        if($data) {

            $info = array();
            if (!empty($data['email'])) {
                $info[] = $data['email'];
            }
            if (!empty($data['cellulare'])) {
                $info[] = $data['cellulare'];
            }

            $return_html = '';

            $avatar_url = CUSTOMER_DOMAIN_INFO['backend_url'] . 'assets/img/user.png';
            if(json_decode($data['propic'])) {

                $avatarUploads = new \MSFramework\uploads('PROPIC', stristr($data['id'], 'global'));
                if(!stristr($data['id'], 'global') && $this->forcingDB) {
                    Global $MSFrameworkFW;
                    $customerSiteData = $MSFrameworkFW->getWebsitePathsBy('customer_database', $this->forcingDB);

                    $avatarUploads->path = str_replace(CUSTOMER_DOMAIN_INFO['path'], $customerSiteData['path'], $avatarUploads->path);
                    $avatarUploads->path_html = str_replace(CUSTOMER_DOMAIN_INFO['url'], $customerSiteData['url'] , $avatarUploads->path_html);
                }

                $avatar_url = $avatarUploads->path_html . json_decode($data['propic'], true)[0];
            }

            if($show_small) {
                $return_html .= '<div style="position: relative; padding-left: 45px; min-height: 30px; line-height: 1;">';
                $return_html .= '<img src="' . $avatar_url . '" width="30" style="position: absolute; left: 0; top: 0; margin-right: 15px; object-fit: contain; height: 100%;">';
                $return_html .= '<h4 style="margin: 0;">' . htmlentities($data['nome'] . ' ' . $data['cognome']) . '</h4><small>' . implode(' - ', $info) . '</small>';
                $return_html .= '</div>';
            } else {
                $return_html .= '<div style="position: relative; padding-left: 65px; min-height: 45px;">';
                $return_html .= '<img src="' . $avatar_url . '" width="50" style="position: absolute; left: 0; top: 0; margin-right: 15px; object-fit: contain; height: 100%;">';
                $return_html .= '<h2 style="margin: 0;">' . htmlentities($data['nome'] . ' ' . $data['cognome']) . '</h2><small>' . implode(' - ', $info) . '</small>';
                $return_html .= '</div>';
            }

        }

        return $return_html;

    }

    /**
     * Permette agli utenti globali di passare da un sito all'altro senza dover effettuare il login tramite la generazione di un token temporaneo
     * @param string $target Il sito da raggiungere
     * @return string L'url da raggiungere una volta generato il codice
     */
    public function generateGlobalLoginRedirect($target) {

        $currentUserdata = $this->getUserDataFromSession();

        if($currentUserdata['is_global']) {

            $token = array(
                'expiration_date' => time() + 30,
                'token' => bin2hex(random_bytes(10))
            );

            $token_encoded = base64_encode(json_encode($token));

            $this->MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.users SET login_token = :login_token WHERE id = :id", array(':login_token' => $token_encoded, ':id' => str_replace('global-', '', $currentUserdata['id'])));

            return $target . (stristr($target, '?') ? '&' : '?') . 'globalLoginToken=' . $token_encoded;
        }

        return '';
    }

    /**
     * Permette agli utenti globali di passare da un sito all'altro senza dover effettuare il login tramite la generazione di un token temporaneo
     * @param $token string Il token da controllare
     */
    public function checkGlobalLoginToken($token) {
        $r = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.users WHERE login_token = :login_token", array(':login_token' => $token), true);
        if($r) {
            $decoded_token = json_decode(base64_decode($r['login_token']), true);

            if(!FRAMEWORK_DEBUG_MODE && ($r['ms_agency'] == "2" && CUSTOMER_DOMAIN_INFO['id'] != 41 && CUSTOMER_DOMAIN_INFO['owner'] != $r['id'])) {
                return false;
            }

            if($decoded_token['expiration_date'] > time()) {
                $_SESSION['userData']['id'] = 'global-' . $r['id'];
                $_SESSION['userData']['username'] = $r['email'];
                $_SESSION['userData']['email'] = $r['email'];
                $_SESSION['userData']['user_id'] = 'global-' . $r['id'];
                $_SESSION['userData']['userlevel'] = $r['ruolo'];
                $_SESSION['userData']['nome'] = $r['nome'];
                $_SESSION['userData']['cognome'] = $r['cognome'];
                $_SESSION['userData']['is_global'] = true;
                $_SESSION['userData']['is_owner'] = ($r['ms_agency'] == 0 ? true : false);
                $_SESSION['userData']['ms_agency'] = $r['ms_agency'];
                $_SESSION['userData']['global'] = true;
                $_SESSION['start_time'] = time();
            }
            return $_SESSION['userData']['id'];
        }
        return false;
    }
}
<?php
/**
 * MSFramework
 * Date: 10/12/18
 */

namespace MSFramework;


class pushNotification {
    public function __construct($origin = '') {

        Global $MSFrameworkDatabase, $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;

        $this->origin = (empty($origin) ? $this->MSFrameworkCMS->getCleanHost() : $origin);

        $this->recipients = array();

    }

    /* =================  LE FUNZIONI UTILI AD OTTENERE I DESTINATARI DELLA NOTIFICA =================  */

    /**
     * Imposta i filtri di destinazione dei SuperAdmin
     *
     * @return $this
     */
    public function setAllSuperAdmins() {
        $this->recipients = array(
            array("field" => "tag", "key" => 'role', "relation" => "=", "value" => "0")
        );

        return $this;
    }

    /**
     * Imposta i filtri di destinazione degli Admin
     *
     * @return $this
     */
    public function setAllAdmins() {
        $this->recipients = array(
            array("field" => "tag", "key" => 'role', "relation" => "=", "value" => "1"),
            array("field" => "tag", "key" => 'website', "relation" => "=", "value" => $this->origin),
        );

        return $this;
    }

    /**
     * Imposta i filtri di destinazione degli Utenti
     *
     * @return $this
     */
    public function setAllUsers() {
        $this->recipients = array(
            array("field" => "tag", "key" => 'role', "relation" => "=", "value" => "2"),
            array("field" => "tag", "key" => 'website', "relation" => "=", "value" => $this->origin),
        );

        return $this;
    }

    /**
     * Imposta i filtri di destinazione di un utente
     *
     * @param integer $id L'ID dell'utente destinatario
     *
     * @return $this
     */
    public function setUserByID($id) {
        $this->recipients = array(
            array("field" => "tag", "key" => 'user_id', "relation" => "=", "value" => $id),
            array("field" => "tag", "key" => 'website', "relation" => "=", "value" => $this->origin),
        );

        return $this;
    }

    /**
     * Imposta i filtri di destinazione di tutti i Clienti
     *
     * @return $this
     */
    public function setAllCustomers() {
        $this->recipients = array(
            array("field" => "tag", "key" => 'role', "relation" => "=", "value" => "customer"),
            array("field" => "tag", "key" => 'website', "relation" => "=", "value" => $this->origin),
        );

        return $this;
    }


    /**
     * Imposta i filtri di destinazione di un Cliente
     *
     * @param integer $id L'ID dell'utente destinatario
     *
     * @return $this
     */
    public function setCustomerByID($id) {
        $this->recipients = array(
            array("field" => "tag", "key" => 'user_id', "relation" => "=", "value" => $id),
            array("field" => "tag", "key" => 'website', "relation" => "=", "value" => $this->origin),
        );

        return $this;
    }


    /**
     * Imposta i tag dell'utente su OneSignal
     *
     * @param string $playerID L'ID dell'utente su OneSignal
     *
     * @return array Lo stato della chiamata
     */
    public function setUserTags($playerID) {

        $fields = array(
            'tags' => array(
                'role'=> $_SESSION['userData']['userlevel'],
                'user_id'=> $_SESSION['userData']['user_id'],
                'website' => $this->origin,
                'last_active' => time()
            )
        );
        $fields['app_id'] = ONESIGNAL_APP_ID;

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/players/" . $playerID);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /* ================= LE FUNZIONI DI INVIO DELLA NOTIFICA =================  */

    /**
     * Invia la notifica al cliente indicato
     *
     * @param string $title Il titolo della notifica
     * @param string $content Il testo della notifica
     * @param string $url L'eventuale URL da far aprire al click sulla notifica
     *
     * @return array
     */
    public function sendNotification($title, $content, $url = '') {

        if(!count($this->recipients)) return array(
            'status' => false,
            'error' => 'Nessun destinatario trovato'
        );

        $fields = array(
            'filters' => $this->recipients,
            'contents' => array("en" => $content),
            'headings' => array("en" => $title),
            'url' => $url
        );

        return $this->doCall($fields);
    }

    /**
     * Invia la notifica al server
     *
     * @param array $fields I campi con le info della notifica
     * @param string $destination L'URL da richiamare
     *
     * @return array La risposta del server
     */
    private function doCall($fields) {

        $fields['app_id'] = ONESIGNAL_APP_ID;
        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,  'https://onesignal.com/api/v1/notifications');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . ONESIGNAL_APP_KEY
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

}
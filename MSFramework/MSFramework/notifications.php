<?php
/**
 * MSFramework
 * Date: 10/12/18
 */

namespace MSFramework;


class notifications {
    public function __construct() {
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkEmails = new \MSFramework\emails();
        $this->MSFrameworkCustomers = new \MSFramework\customers();
    }

    /**
     * Restituisce un array associativo (ID Notifica => Dati Notifica) con i dati relativi alla notifica.
     *
     * @param mixed $id Il tipo della notifica (stringa) o delle notifiche (array) richieste (se vuoto, vengono recuperati i dati di tutte le notifiche)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getNotificationDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "type");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" type != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "type")) {
            $fields .= ", type";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM notifications WHERE type != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $r['count'] = $this->countSentNotifications($r['type']);
            $ary_to_return[$r['type']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce le notifiche disponibili
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     * @param bool $limit_to_type Impostando questo parametro su "true", vengono restituite solo le notifiche disponibili per la tipologia di sito corrente (default: true)
     *
     * @return array Array con le notifiche disponibili
     *
     */
    public function getAvailNotifications($key = "", $limit_to_type = true) {
        Global $MSFrameworkCMS;
        /* Struttura di esempio

            "id_notifica" => array(
                "name" => "Nome Notifica",
                "descr" => "Descrizione Notifica",
                "fa-icon" => "fa-comments",
                "limit_to_functions" => array("beautycenter"),
                "shortcodes" => array(
                    array()
                )
            ),

        */

        $array = array(
            "birthday" => array(
                "name" => "Compleanno",
                "descr" => "Invia gli auguri di buon compleanno agli utenti.",
                "fa-icon" => "fa-birthday-cake",
                "shortcodes" => array(
                    array()
                ),
                "send" => function($email_settings, $specific_settings) {
                    Global $MSFrameworki18n;

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM customers WHERE email != '' AND data_nascita >= :data1 AND data_nascita <= :data2", array(":data1" => strtotime('today midnight'), ":data2" => strtotime('tomorrow midnight'))) as $user) {
                        $this->MSFrameworkEmails->sendCustomMail(
                            $MSFrameworki18n->getFieldValue($email_settings['subject']),
                            $email_settings['custom_html'],
                            $user['email'],
                            $email_settings['reply_to_addr'],
                            $email_settings['custom_txt'],
                            array(),
                            $this->getShortcodesValues($user, "birthday")
                        );
                        $this->saveHistory('birthday', $user['id'], $this->MSFrameworkEmails);
                    }
                }
            ),
            "post_appointment_review" => array(
                "name" => "Recensione appuntamento",
                "descr" => "Richiedi agli utenti una recensione qualche giorno dopo il loro ultimo appuntamento.",
                "fa-icon" => "fa-comments",
                "limit_to_functions" => array("appointments"),
                "shortcodes" => array(
                    array()
                ),
                "send" => function($email_settings, $specific_settings) {
                    Global $MSFrameworki18n;

                    $day_diff = ($specific_settings['wait_days'] != "" ? $specific_settings['wait_days'] : "3");
                    $search_date_ts = time()-(86400*$day_diff);

                    $done_customers = array();

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT `customer` FROM `appointments_list` WHERE DATE(start_date) = :date", array(":date" => date("Y-m-d", $search_date_ts))) as $appointment) {
                        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT * FROM customers WHERE email != '' AND id = :id", array(":id" => $appointment['customer'])) as $user) {
                            if(in_array($user['id'], $done_customers)) {
                                continue;
                            }

                            $done_customers[] = $user['id'];

                            $this->MSFrameworkEmails->sendCustomMail(
                                $MSFrameworki18n->getFieldValue($email_settings['subject']),
                                $email_settings['custom_html'],
                                $user['email'],
                                $email_settings['reply_to_addr'],
                                $email_settings['custom_txt'],
                                array(),
                                $this->getShortcodesValues($user, "post_appointment_review")
                            );
                        }
                    }
                }
            ),
            "appointment_reminder" => array(
                "name" => "Promemoria appuntamento",
                "descr" => "Invia un promemoria ai clienti per ricordargli dell'appuntamento.",
                "fa-icon" => "fa-bell",
                "limit_to_functions" => array("appointments"),
                "shortcodes" => array(
                    array()
                ),
                "send" => function($email_settings, $specific_settings) {
                    Global $MSFrameworki18n;


                    $day_diff = ($specific_settings['days_before'] != "" ? $specific_settings['days_before'] : "1");
                    $search_date_ts = time()+(86400*$day_diff);

                    $done_customers = array();

                    foreach($this->MSFrameworkDatabase->getAssoc("SELECT `customer` FROM `appointments_list` WHERE DATE(start_date) = :date", array(":date" => date("Y-m-d", $search_date_ts))) as $appointment) {
                        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT * FROM customers WHERE email != '' AND id = :id", array(":id" => $appointment['customer'])) as $user) {
                            if(in_array($user['id'], $done_customers)) {
                                continue;
                            }

                            $done_customers[] = $user['id'];

                            $this->MSFrameworkEmails->sendCustomMail(
                                $MSFrameworki18n->getFieldValue($email_settings['subject']),
                                $email_settings['custom_html'],
                                $user['email'],
                                $email_settings['reply_to_addr'],
                                $email_settings['custom_txt'],
                                array(),
                                $this->getShortcodesValues($user, "appointment_reminder")
                            );
                            $this->saveHistory('appointment_reminder', $user['id'], $this->MSFrameworkEmails);
                        }
                    }
                }
            ),
            "abandoned_cart" => array(
                "name" => "Carrello abbandonato",
                "descr" => "Invia un promemoria ai clienti che hanno abbandonato il carrello.",
                "fa-icon" => "fa-shopping-cart",
                "limit_to_functions" => array("ecommerce"),
                "shortcodes" => array(
                    array(
                        "{tabella_prodotti}" => "Una tabella con la lista dei prodotti abbandonati"
                    ),
                    array(
                        function($user_info) {
                            $user = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($user_info['id']);
                            $carrello = (new \MSFramework\Ecommerce\cart())->getCart(json_decode($user["extra"]['carrello'], true));

                            $products_rows_template = '<tr><td style="text-align: left;"><small><b>{quantity}*</b></small> {product_name}</td><td style="text-align: right;">{product_price}</td></tr>';
                            $products_row_html = '';
                            foreach ($carrello['products'] as $product) {
                                $products_row_html .= str_replace(
                                    array(
                                        "{quantity}",
                                        "{product_name}",
                                        "{product_price}",
                                    ),
                                    array(
                                        $product['quantity'],
                                        $product['nome'],
                                        number_format((new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['prezzo']['tax']) * $product['quantity'], 2, ',', '.') . CURRENCY_SYMBOL
                                    ),
                                    $products_rows_template
                                );
                            }

                            return '<table style="width: 100%;">' . $products_row_html . '</table>';
                        }
                    )
                ),
                "send" => function($email_settings, $specific_settings) {
                    Global $MSFrameworki18n;

                    if(empty($specific_settings['wait_days'])) $specific_settings['wait_days'] = 7;

                    $days = (int)$specific_settings['wait_days'];
                    $users_extra = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_users_extra WHERE (carrello NOT LIKE '[]' AND carrello NOT LIKE '') AND DATE(edit_date) < NOW() - INTERVAL " . (int)$days . " DAY AND cart_notified = 0");

                    $ecommerceCartClass = new \MSFramework\Ecommerce\cart();

                    foreach($users_extra as $user_extra) {

                        $user = $this->MSFrameworkCustomers->getCustomerDataFromDB($user_extra['id']);
                        $carrello = $ecommerceCartClass->getCart(json_decode($users_extra['carrello'], true));

                        if(count($carrello['count']) > 0) {
                            $this->MSFrameworkEmails->sendCustomMail(
                                $MSFrameworki18n->getFieldValue($email_settings['subject']),
                                $email_settings['custom_html'],
                                $user['email'],
                                $email_settings['reply_to_addr'],
                                $email_settings['custom_txt'],
                                array(),
                                $this->getShortcodesValues($user, "abandoned_cart")
                            );
                            $this->saveHistory('abandoned_cart', $user_extra['id'], $this->MSFrameworkEmails);
                        }
                    }

                    $this->MSFrameworkDatabase->query('UPDATE ecommerce_users_extra SET cart_notified = 1 WHERE 1');
                }
            ),
            "order_pending_payment" => array(
                "name" => "Ordine in attesa di Pagamento",
                "descr" => "Invia un promemoria ai clienti che non hanno ancora pagato il loro ordine.",
                "fa-icon" => "fa-credit-card",
                "limit_to_functions" => array("ecommerce"),
                "shortcodes" => array(
                    array(
                        "{tabella_prodotti}" => "Una tabella con la lista dei prodotti acquistati",
                        "{paga_ordine}" => "Il link del pannello per completare l'ordine",
                    ),
                    array(
                        function($user_info) {

                            $last_awaiting_payment_order = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_orders` WHERE user_id = :user_id AND order_status = 0 ORDER BY id DESC LIMIT 1", array(':user_id' => $user_info['id']), true);

                            $products_rows_template = '<tr><td style="text-align: left;"><small><b>{quantity}*</b></small> {product_name}</td><td style="text-align: right;">{product_price}</td></tr>';
                            $products_row_html = '';
                            foreach (json_decode($last_awaiting_payment_order['cart'], true)['products'] as $product) {
                                $products_row_html .= str_replace(
                                    array(
                                        "{quantity}",
                                        "{product_name}",
                                        "{product_price}",
                                    ),
                                    array(
                                        $product['quantity'],
                                        $product['nome'],
                                        number_format((new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['prezzo']['tax']) * $product['quantity'], 2, ',', '.') . CURRENCY_SYMBOL
                                    ),
                                    $products_rows_template
                                );
                            }

                            return '<table style="width: 100%;">' . $products_row_html . '</table>';
                        },
                        function($user_info) {
                            Global $MSFrameworkCMS;

                            $last_awaiting_payment_order = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_orders` WHERE user_id = :user_id AND order_status = 0 ORDER BY id DESC LIMIT 1", array(':user_id' => $user_info['id']), true);
                            return $MSFrameworkCMS->getURLToSite() . 'redirect_to_order/' . $last_awaiting_payment_order['id'];
                        }
                    )
                ),
                "send" => function($email_settings, $specific_settings) {
                    Global $MSFrameworki18n;

                    if(empty($specific_settings['wait_days'])) $specific_settings['wait_days'] = 7;
                    $days = (int)$specific_settings['wait_days'];

                    $awaiting_payment_orders = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_orders` WHERE order_status = 0 AND DATE(order_edit_date) < NOW() - INTERVAL $days DAY AND DATE(order_edit_date) > NOW() - INTERVAL " . ($days+1) . " DAY AND order_edit_date = order_date");

                    $order_ids = array();
                    foreach($awaiting_payment_orders as $order_details) {
                        $order_ids[] = $order_details['id'];

                        $user = $this->MSFrameworkCustomers->getCustomerDataFromDB($order_details['user_id']);

                        $this->MSFrameworkEmails->sendCustomMail(
                            $MSFrameworki18n->getFieldValue($email_settings['subject']),
                            $email_settings['custom_html'],
                            $user['email'],
                            $email_settings['reply_to_addr'],
                            $email_settings['custom_txt'],
                            array(),
                            $this->getShortcodesValues($user, "order_pending_payment")
                        );
                        $this->saveHistory('order_pending_payment', $order_details['user_id'], $this->MSFrameworkEmails);
                    }

                    if($order_ids) {
                        $this->MSFrameworkDatabase->query('UPDATE ecommerce_orders SET order_edit_date = NOW() WHERE id IN (' . implode(',', $order_ids) . ')');
                    }
                }
            ),
            "order_completed" => array(
                "name" => "Ordine completato",
                "descr" => "Invia un promemoria ai clienti che hanno effettuato un ordine.",
                "fa-icon" => "fa-credit-card-alt",
                "limit_to_functions" => array("ecommerce"),
                "shortcodes" => array(
                    array(
                        "{tabella_prodotti}" => "Una tabella con la lista dei prodotti acquistati"
                    ),
                    array(
                        function($user_info) {

                            $last_awaiting_payment_order = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_orders` WHERE user_id = :user_id AND order_status = 0 ORDER BY id DESC LIMIT 1", array(':user_id' => $user_info['id']), true);

                            $products_rows_template = '<tr><td style="text-align: left;"><small><b>{quantity}*</b></small> {product_name}</td><td style="text-align: right;">{product_price}</td></tr>';
                            $products_row_html = '';
                            foreach (json_decode($last_awaiting_payment_order['cart'], true)['products'] as $product) {
                                $products_row_html .= str_replace(
                                    array(
                                        "{quantity}",
                                        "{product_name}",
                                        "{product_price}",
                                    ),
                                    array(
                                        $product['quantity'],
                                        $product['nome'],
                                        number_format((new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['prezzo']['tax']) * $product['quantity'], 2, ',', '.') . CURRENCY_SYMBOL
                                    ),
                                    $products_rows_template
                                );
                            }

                            return '<table style="width: 100%;">' . $products_row_html . '</table>';
                        }
                    )
                ),
                "send" => function($email_settings, $specific_settings) {
                    Global $MSFrameworki18n;

                    if(empty($specific_settings['wait_days'])) $specific_settings['wait_days'] = 7;
                    $days = (int)$specific_settings['wait_days'];

                    $awaiting_payment_orders = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_orders` WHERE order_status = 3 AND DATE(order_edit_date) < NOW() - INTERVAL $days DAY AND DATE(order_edit_date) > NOW() - INTERVAL " . ($days+1) . " DAY");

                    $order_ids = array();
                    foreach($awaiting_payment_orders as $order_details) {
                        $order_ids[] = $order_details['id'];

                        $user = $this->MSFrameworkCustomers->getCustomerDataFromDB($order_details['user_id']);

                        $this->MSFrameworkEmails->sendCustomMail(
                            $MSFrameworki18n->getFieldValue($email_settings['subject']),
                            $email_settings['custom_html'],
                            $user['email'],
                            $email_settings['reply_to_addr'],
                            $email_settings['custom_txt'],
                            array(),
                            $this->getShortcodesValues($user, "order_completed")
                        );
                        $this->saveHistory('order_completed', $order_details['user_id'], $this->MSFrameworkEmails);
                    }

                    if($order_ids) {
                        $this->MSFrameworkDatabase->query('UPDATE ecommerce_orders SET order_edit_date = NOW() WHERE id IN (' . implode(',', $order_ids) . ')');
                    }
                }
            ),
            "product_available" => array(
                "name" => "Prodotto disponibile",
                "descr" => "Invia un promemoria ai clienti quando un prodotto che seguono ritorna disponibile.",
                "fa-icon" => "fa-bell",
                "limit_to_functions" => array("ecommerce"),
                "shortcodes" => array(
                    array(
                        "{product_name}" => "Il nome del prodotto ritornato disponibile",
                        "{product_url}" => "L'URL del prodotto ritornato disponibile"
                    ),
                    array(
                        function($user_info) {
                            Global $MSFrameworki18n;

                            $MSFrameworkEcommerceProducts = new \MSFramework\Ecommerce\products();
                            $awaiting_availability = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_users_extra` WHERE id = :user_id", array(':user_id' => $user_info['id']));

                            foreach($awaiting_availability as $awaiting_user) {
                                $products = (json_decode($awaiting_user['awaiting_availability']) ? json_decode($awaiting_user['awaiting_availability'], true) : array());
                                if (!$products) continue;

                                foreach ($products as $product) {
                                    $product_info = $MSFrameworkEcommerceProducts->getProductVariationInfo($product['product_id'], $product['variations']);

                                    if ((int)$product_info['warehouse_qty'] > 0) {
                                        return $MSFrameworki18n->getFieldValue($product_info['nome']);
                                    }
                                }
                            }

                        },
                        function($user_info) {
                            Global $MSFrameworki18n;

                            $MSFrameworkEcommerceProducts = new \MSFramework\Ecommerce\products();
                            $awaiting_availability = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_users_extra` WHERE id = :user_id", array(':user_id' => $user_info['id']));

                            foreach($awaiting_availability as $awaiting_user) {
                                $products = (json_decode($awaiting_user['awaiting_availability']) ? json_decode($awaiting_user['awaiting_availability'], true) : array());
                                if (!$products) continue;

                                foreach ($products as $product) {
                                    $product_info = $MSFrameworkEcommerceProducts->getProductVariationInfo($product['product_id'], $product['variations']);
                                    if ((int)$product_info['warehouse_qty'] > 0) {
                                        return (new \MSFramework\Ecommerce\products())->getURL($product['product_id']);
                                    }
                                }
                            }
                        }
                    )
                ),
                "send" => function($email_settings, $specific_settings) {
                    Global $MSFrameworki18n;

                    $MSFrameworkEcommerceProducts = new \MSFramework\Ecommerce\products();

                    $awaiting_availability = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_users_extra` WHERE awaiting_availability != ''");

                    foreach($awaiting_availability as $awaiting_user) {

                        $products = (json_decode($awaiting_user['awaiting_availability']) ? json_decode($awaiting_user['awaiting_availability'], true) : array());

                        if(!$products) continue;

                        foreach($products as $k => $product) {
                            $product_info = $MSFrameworkEcommerceProducts->getProductVariationInfo($product['id'], $product['variations']);
                            if((int)$product_info['warehouse_qty'] > 0) {
                                $user = $this->MSFrameworkCustomers->getCustomerDataFromDB($awaiting_user['id']);

                                $this->MSFrameworkEmails->sendCustomMail(
                                    $MSFrameworki18n->getFieldValue($email_settings['subject']),
                                    $email_settings['custom_html'],
                                    $user['email'],
                                    $email_settings['reply_to_addr'],
                                    $email_settings['custom_txt'],
                                    array(),
                                    $this->getShortcodesValues($user, "product_available")
                                );

                                unset($products[$k]);
                                (new \MSFramework\Ecommerce\customers())->saveInfo('awaiting_availability', $products, $awaiting_user['id']);

                                $this->saveHistory('product_available', $awaiting_user['id'], $this->MSFrameworkEmails);
                            }
                        }
                    }
                }
            ),
            "points_reminder" => array(
                "name" => "Promemoria punti cliente",
                "descr" => "Invia un promemoria ai clienti quando raggiungono un certo numero di punti.",
                "fa-icon" => "fa-ticket",
                "limit_to_functions" => array("points"),
                "shortcodes" => array(
                    array(
                        "{points}" => "Il numero di punti raggiunti dal cliente"
                    ),
                    array(
                        function($user_info) {
                            return (new \MSFramework\Points\points())->getCustomerBalance($user_info['id']);
                        }
                    )
                ),
                "send" => function($email_settings, $specific_settings) {
                    Global $MSFrameworki18n;

                    $customers_to_notify = $this->MSFrameworkDatabase->getAssoc("SELECT SUM(valore) as total_points, cliente, (SELECT valore FROM points_histories h2 WHERE h2.cliente = points_histories.cliente ORDER BY id DESC LIMIT 1) as last_operation FROM points_histories WHERE cliente NOT IN(SELECT user_id FROM notifications__histories WHERE notification = 'points_reminder') OR (SELECT data FROM points_histories h3 WHERE h3.cliente = points_histories.cliente ORDER BY id DESC LIMIT 1) > (SELECT sent_date FROM notifications__histories WHERE notifications__histories.notification = 'points_reminder' AND notifications__histories.user_id = points_histories.cliente ORDER BY sent_date DESC LIMIT 1) GROUP BY cliente HAVING  total_points >= " . (int)$specific_settings['points'] . " AND total_points-last_operation < " . (int)$specific_settings['points']);

                    foreach($customers_to_notify as $customer_points) {

                        $user = (new \MSFramework\customers())->getCustomerDataFromDB($customer_points['cliente']);

                        $this->MSFrameworkEmails->sendCustomMail(
                            $MSFrameworki18n->getFieldValue($email_settings['subject']),
                            $email_settings['custom_html'],
                            $user['email'],
                            $email_settings['reply_to_addr'],
                            $email_settings['custom_txt'],
                            array(),
                            $this->getShortcodesValues($user, "points_reminder")
                        );

                        $this->saveHistory('points_reminder', $customer_points['cliente'], $this->MSFrameworkEmails);
                    }
                }
            ),
        );

        if($limit_to_type) {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT value FROM `cms` WHERE type = 'producer_config'", array(), true);
            $r_producer_config = json_decode($r['value'], true);
            $tmp_ary = array();
            foreach($array as $cur_notificationK => $cur_notification) {
                if(!is_array($cur_notification['limit_to_functions'])) {
                    $tmp_ary[$cur_notificationK] = $cur_notification;
                    continue;
                }

                if(count(array_intersect($cur_notification['limit_to_functions'], json_decode($r_producer_config['extra_functions'], true))) > 0) {
                    $tmp_ary[$cur_notificationK] = $cur_notification;
                }
            }

            $array = $tmp_ary;
        }


        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Ottiene la lista degli shortcodes utilizzabili
     *
     * @paray bool $get_description Se true ottiene anche la descrizione degli shortcodes
     *
     * @return array
     */
    public function getShortcodeList() {

        $tag_from =  array(
            array(
                "{nome}" => "Il nome del cliente",
                "{cognome}" => "Il cognome del cliente",
                "{email}" => "L'email del cliente",
            ),
            array(
                function($user_info) {
                    return $user_info['nome'];
                },
                function($user_info) {
                    return $user_info['cognome'];
                },
                function($user_info) {
                    return $user_info['email'];
                }
            )
        );

        return $tag_from;
    }

    /**
     * Sostituisce gli shortcodes delle notifiche
     *
     * @param $user_info array L'array con i dati del destinatario
     * @param $notification_id string L'ID della notifica
     *
     * @return array Un array con le sostituzioni degli shortcodes
     */
    public function getShortcodesValues($user_info, $notification_id) {

        $shortcodes_values = array();

        $shortcodes_set = array($this->getShortcodeList());
        if(isset($this->getAvailNotifications($notification_id)['shortcodes'])) {
            $shortcodes_set[] = $this->getAvailNotifications($notification_id)['shortcodes'];
        }

        foreach($shortcodes_set as $shortcodes) {
            $s_key = 0;
            foreach ($shortcodes[0] as $param_key => $param_value) {
                $shortcode = $param_key;
                if (is_numeric($param_key)) {
                    $shortcode = $param_value;
                }
                if (is_callable($shortcodes[1][$s_key])) {
                    $shortcodes_values[$shortcode] = $shortcodes[1][$s_key]($user_info);
                } else {
                    $shortcodes_values[$shortcode] = $shortcodes[1][$s_key];
                }
                $s_key++;
            }
        }

        return $shortcodes_values;
    }

    /**
     * Controlla se una determinata notifica esiste (è presente la riga nel DB)
     *
     * @param $id L'ID della notifica
     *
     * @return bool
     */
    public function checkExists($id) {
        if($id == "") {
            return false;
        }

        return ($this->MSFrameworkDatabase->getCount("SELECT id FROM `notifications` WHERE type = :type", array(":type" => $id)) == 0 ? false : true);
    }

    /**
     * Controlla se una determinata notifica è attiva (è presente la riga nel DB ed è spuntata come attiva)
     *
     * @param $id L'ID della notifica
     *
     * @return bool
     */
    public function checkIsActive($id) {
        if($id == "") {
            return false;
        }

        return ($this->MSFrameworkDatabase->getCount("SELECT id FROM `notifications` WHERE type = :type AND is_active = 1", array(":type" => $id)) == 0 ? false : true);
    }

    /**
     * Salva l'invio della notifica nella cronologia deegli invii
     *
     * @param $notification_type string La tipologia di notifica
     * @param $user_id int L'ID del cliente
     * @param $mail object I parametri della classe mail
     *
     * @return mixed
     */
    public function saveHistory($notification_type, $user_id, $mail) {

        $email_data = json_encode(
            array(
                'recipient' => array_keys($mail->mail->getAllRecipientAddresses())[0],
                'reply_to' => $mail->mail->getReplyToAddresses(),
                'subject' => $mail->mail->Subject,
                'body' => $mail->mail->Body,
                'txt_body' => $mail->mail->AltBody
            )
        );

        return $this->MSFrameworkDatabase->pushToDB("INSERT INTO notifications__histories (user_id, notification, email_data) VALUES (:user_id, :notification, :email_data)", array(":user_id" => $user_id, ":notification" => $notification_type, ":email_data" => $email_data));
    }

    /**
     * Conta il numero di invii di una determinata tipologia di notifiche
     *
     * @param $notification_type string La tipologia di notifica
     *
     * @return integer
     */
    public function countSentNotifications($notification_type) {
        return (int)$this->MSFrameworkDatabase->getCount("SELECT * FROM notifications__histories WHERE notification = :notification", array(":notification" => $notification_type));
    }
}
<?php
/**
 * MSFramework
 * Date: 05/03/18
 */

namespace MSFramework;


class faq {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID FAQ => Dati FAQ) con i dati relativi alle FAQ.
     *
     * @param $id L'ID della FAQ (stringa) o delle FAQ (array) richieste (se vuoto, vengono recuperati i dati di tutte le FAQ)
     * @param $just_active Se impostato su true, recupera solo le sezioni delle FAQ attive
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getFAQDetails($id = "", $just_active = true, $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = "";
        $append_where_ary = array();
        if($just_active) {
            $append_where .= " AND is_active = '1' ";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM faq WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ")", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {
            if(isset($r['questions'])) $r['questions'] = json_decode($r['questions'], true);
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }
}
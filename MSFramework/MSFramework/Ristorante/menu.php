<?php
/**
 * MSFramework
 * Date: 02/03/18
 */

namespace MSFramework\Ristorante;


class menu {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

    $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Menu => Dati Menu) con i dati relativi al menu.
     *
     * @param $id L'ID del menù (stringa) o dei menu (array) richiesti (se vuoto, vengono recuperati i dati di tutti menu)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getMenuDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM restaurant_menu WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $r['fields'] = json_decode($r['fields'], true);
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }
}
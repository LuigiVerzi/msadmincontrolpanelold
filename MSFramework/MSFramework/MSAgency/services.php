<?php
/**
 * MSFramework
 * Date: 07/03/18
 */

namespace MSFramework\MSAgency;


class services {

    public $userViewMode = 'customer';

    public function __construct() {
        global $firephp, $MSFrameworkDatabase, $MSFrameworkUsers;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;

        if(CUSTOMER_DOMAIN_INFO['id'] === 41) {
            if($MSFrameworkUsers->getUserDataFromSession('userlevel') === '0') {
                $this->userViewMode = 'consultant';
            } else if($MSFrameworkUsers->getUserDataFromSession('userlevel') === 'custom-2') {
                $this->userViewMode = 'seller';
            }
        }

        if(FRAMEWORK_DEBUG_MODE) $this->userViewMode = 'consultant';

    }

    /**
     * Restituisce un array associativo (ID Servizio => Dati Servizio) con i dati relativi al servizio.
     *
     * @param $id L'ID del servizio (stringa) o dei servizi (array) richiesti (se vuoto, vengono recuperati i dati di tutti i servizi)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getPackageDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $uploads = new \MSFramework\uploads('PACKAGES', true);

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM marke833_framework.ms_agency__service_packages WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {

            if(isset($r['duration'])) $r['duration'] = (json_decode($r['duration']) ? json_decode($r['duration'], true) : array('0', 'days'));
            if(isset($r['trial'])) $r['trial'] = (json_decode($r['trial']) ? json_decode($r['trial'], true) : array('0', 'days'));

            $r['type'] = 'package';

            if(isset($r['services'])) {
                $r['services'] = (json_decode($r['services']) ? json_decode($r['services'], true) : array());
            }

            $time_conversion = array(
                'days' => array('giorn', 'o', 'i'),
                'months' => array('mes', 'e', 'i'),
                'years' => array('ann', 'o', 'i')
            );

            $r['payment_recurrence_string'] =  ($r['una_tantum'] ? 'Una tantum' : $r['duration']['value'] . ' ' . $time_conversion[$r['duration']['type']][0] . $time_conversion[$r['duration']['type']][($r['duration']['value'] == 1 ? 1 : 2)]);

            $r['gallery_friendly'] = array();
            $service_gallery = json_decode($r['images'], true);
            if(is_array($service_gallery)) {
                $r['gallery_friendly'] = $uploads->composeGalleryFriendlyArray($service_gallery);
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Servizio => Dati Servizio) con i dati relativi al servizio.
     *
     * @param $id L'ID del servizio (stringa) o dei servizi (array) richiesti (se vuoto, vengono recuperati i dati di tutti i servizi)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getServiceDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $uploads = new \MSFramework\uploads('SERVICES', true);

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM marke833_framework.ms_agency__services WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {

            if(isset($r['duration'])) $r['duration'] = (json_decode($r['duration']) ? json_decode($r['duration'], true) : array('0', 'days'));
            if(isset($r['trial'])) $r['trial'] = (json_decode($r['trial']) ? json_decode($r['trial'], true) : array('0', 'days'));

            $r['type'] = 'service';

            $time_conversion = array(
                'days' => array('giorn', 'o', 'i'),
                'months' => array('mes', 'e', 'i'),
                'years' => array('ann', 'o', 'i')
            );

            $r['payment_recurrence_string'] =  ($r['una_tantum'] ? 'Una tantum' : $r['duration']['value'] . ' ' . $time_conversion[$r['duration']['type']][0] . $time_conversion[$r['duration']['type']][($r['duration']['value'] == 1 ? 1 : 2)]);

            $r['gallery_friendly'] = array();
            $service_gallery = json_decode($r['images'], true);
            if(is_array($service_gallery)) {
                $r['gallery_friendly'] = $uploads->composeGalleryFriendlyArray($service_gallery);
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Servizio => Dati Servizio) con i dati relativi al servizio.
     *
     * @param $category L'ID della categoria
     *
     * @return array
     */
    public function getServicesIDsByCategory($category) {
        if($category != "") {
            if(!is_array($category)) {
                $category = array($category);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($category, "OR", "category");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" category != '' ", array());
        }
        $ary_to_return = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM marke833_framework.ms_agency__services WHERE category != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[] = $r['id'];
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria.
     *
     * @param $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM marke833_framework.ms_agency__service_categories WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }
}
<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\MSAgency;

class earnings {

    public $sellerCommission = 15; // Percentuale
    public $consultantCommission = 60; // Percentuale

    public $minWithdrawalCredit = 200;

    public function __construct() {
        global $firephp;
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    public function assignCommission($seller_id, $order_id, $product_id, $type = 'seller') {
        $orderDetails = (new \MSFramework\MSAgency\orders())->getOrderDetails($order_id)[$order_id];

        if($seller_id && $orderDetails && $orderDetails['cart'][$product_id]) {
            $productDetails = $orderDetails['cart'][$product_id];

            if(((int)$productDetails['una_tantum'] === 1 && (int)$productDetails['payment_status'] === 2) || ((int)$productDetails['una_tantum'] === 0 && (int)$productDetails['payment_status'] === 1)) {

                $commission = (int)($type === 'seller' ? $orderDetails['seller_commission'] : $productDetails['consultant_commission']);

                if(!$this->getCommissionByOrder($order_id, $product_id, $type) && $commission > 0) {

                    $array_to_save = array(
                        "type" => $type,
                        "user_id" => $seller_id,
                        "order_id" => $order_id,
                        "product" => $product_id,
                        "earning" => ($productDetails['prezzo']*$commission)/100
                    );

                    $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
                    $this->MSFrameworkDatabase->pushToDB("INSERT INTO `" . FRAMEWORK_DB_NAME . "`.`ms_agency__earnings` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
                }
            }
        }

    }

    public function getCommissionByOrder($order_id, $product_id = '', $type = 'seller') {
        $product_sql = '';
        if(!empty($product_id)) {
            $product_sql = " AND product LIKE '" . $product_id . "'";
        }

        $ary_to_return = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM  `" . FRAMEWORK_DB_NAME . "`.`ms_agency__earnings` WHERE order_id = :order_id AND type = :type $product_sql", array(':order_id' => $order_id,':type' => $type)) as $commission) {
            $ary_to_return[$commission['id']] = $this->getCommissionDetails($commission['id'])[$commission['id']];
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Commissione => Dati commission) con i dati relativi
     *
     * @param mixed $id L'ID dela commissione da ottenere (o un array con più id)
     *
     * @return array
     */
    public function getCommissionDetails($id = "") {
        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))$same_db_string_ary = array(" id != '' ", array());

        $ary_to_return = array();
        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT * FROM  `" . FRAMEWORK_DB_NAME . "`.`ms_agency__earnings` WHERE id != '' AND (" . $same_db_string_ary[0] . ") ORDER BY id DESC", $same_db_string_ary[1]) as $r) {
            $r['user_data'] = (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserDataFromDB($r['user_id']);
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene i dati di pagamento di un determinato cliente
     *
     * @param mixed $user_id L'ID dell'utente
     *
     * @return array
     */
    public function getUserPaymentsData($user_id) {

        $ary_to_return = array();

        $payment_data = $this->MSFrameworkDatabase->getAssoc("SELECT payment_data FROM " . FRAMEWORK_DB_NAME . ".ms_agency__payments_info WHERE user_id = :user_id", array(':user_id' => $user_id), true);

        if($payment_data && json_decode($payment_data['payment_data'])) {
            $ary_to_return = json_decode($payment_data['payment_data'], true);
        }

        return $ary_to_return;
    }

    /* =============== FUNZIONI ACP ============= */

    /**
     * Questa funzione permette di ottenere il grafico dell'ultimo mese
     *
     * @param string $start La data YYYY-MM-DD di partenza
     * @param string $end La data YYYY-MM-DD di finale
     *
     * @return array
     */
    public function getDateRangeChart($start, $end) {
        Global $MSFrameworkUsers;

        $grafico_ordini = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT earning, date(date) as day FROM " . FRAMEWORK_DB_NAME . ".ms_agency__earnings WHERE date >= '$start' AND date <= '$end 23:59' AND user_id = '" . $MSFrameworkUsers->getUserDataFromSession('id') . "'") as $r) {

            if(!isset($grafico_ordini[$r['day']])) $grafico_ordini[$r['day']] = array('earn' => 0, 'orders' => 0);

            $grafico_ordini[$r['day']]['earn'] += $r['earning'];
            $grafico_ordini[$r['day']]['orders'] += 1;
        }

        $start =  new \DateTime($start);
        $end =  new \DateTime($end);

        $array_data = array();

        for($i = $start; $i <= $end; $i->modify('+1 day')) {
            if(isset($grafico_ordini[$i->format("Y-m-d")])) $array_data[] = $grafico_ordini[$i->format("Y-m-d")];
            else $array_data[] = array('earn' => 0, 'orders' => 0);
        }

        return $array_data;
    }

}
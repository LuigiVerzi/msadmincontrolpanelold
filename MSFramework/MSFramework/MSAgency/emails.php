<?php
/**
 * Sarah Cosmi Restart
 * Date: 1/08/18
 */

namespace MSFramework\MSAgency;

class emails extends \MSFramework\emails
{

    /**
     * Ottiene la lista di template per l'ecommerce
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {

        $beforeSend = function ($params, $template_settings) {

            // Se l'ordine non ha nessun venditore non invio l'email
            if(stristr($params['template'], 'seller') && ($params['order']['origin_reference'] !== (new \MSFramework\MSAgency\orders())->sellerOriginDB || !$params['order']['seller_data'])) {
                return false;
            }

            // Se le notifiche sono disattivate per il cliente non invio l'email
            if(stristr($params['template'], 'customer') && (int)$params['order']['customer_data']['send_notification'] !== 1) {
                return false;
            }

            return true;
        };

        /* SHORTCODES ORDINI */
        $globalShortcodeKeys = array(
            /* CLIENTE */
            "{cliente-nome}" => "Il nome del cliente",
            "{cliente-cognome}" => "L'email del cliente",
            "{cliente-email}" => "L'email del cliente",
            "{anteprima-cliente}" => "I dati principali del cliente",

            /* CLIENTE */
            "{venditore-nome}" => "Il nome del venditore",
            "{venditore-cognome}" => "L'email del venditore",
            "{venditore-email}" => "L'email del venditore",
            "{anteprima-venditore}" => "I dati principali del venditore",

            /* TABELLA PRODOTTI */
            "{tabella-prodotti}" => "La tabella dei prodotti",

            /* NOTE */
            "{note}" => "Eventuali note allegate",
        );
        $globalShortcodeValues = array(
            /* CLIENTE */
            "{[order][customer_data][nome]}",
            "{[order][customer_data][cognome]}",
            "{[order][customer_data][email]}",
            function ($params, $template_settings) {
                return preg_replace(
                    '/padding([^px%"]+)[px]+/m' ,
                    '',
                    (new \MSFramework\customers(MARKETINGSTUDIO_DB_NAME))->getCustomerPreviewHTML($params['order']['customer_reference'])
                );
            },

            /* COMMERCIALE */
            "{[order][seller_data][nome]}",
            "{[order][seller_data][cognome]}",
            "{[order][seller_data][email]}",
            function ($params, $template_settings) {
                $seller_preview = (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($params['order']['seller']);
                if($seller_preview) {
                    return preg_replace(
                        '/padding([^px%"]+)[px]+/m',
                        '',
                        (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($params['order']['seller'])
                    );
                } else {
                    return '<small>Nessuno</small>';
                }
            },

            /* TABELLA PRODOTTI */
            function ($params, $template_settings) {
                Global $MSFrameworki18n;

                $serviceUploads = new \MSFramework\uploads('SERVICES', true);
                $packageUploads = new \MSFramework\uploads('PACKAGES', true);

                $products_rows_template = '<tr>
                    <td style="text-align: center; padding: 5px 5px 0 0;" width="80px;"><img src="{product_icon}" width="80" /></td>
                    <td style="text-align: left;  padding: 5px 15px;">{product_name}</td>
                    <td style="text-align: right;  padding: 5px 15px;">{product_price}</td>
                </tr>';

                $products_row_html = '';
                foreach ($params['order']['cart'] as $full_id => $product) {

                    if (stristr($full_id, 'service')) $image_base = $serviceUploads->path_html;
                    else $image_base = $packageUploads->path_html;

                    $products_row_html .= str_replace(
                        array(
                            "{product_name}",
                            "{product_price}",
                            "{product_icon}",
                        ),
                        array(
                            $MSFrameworki18n->getFieldValue($product['nome']),
                            ($product['quote_needed'] ? '<span style="color: #FF9800;font-weight: bold;">Richiesto Preventivo</span>' : number_format($product['prezzo'], 2, ',', '.') . CURRENCY_SYMBOL),
                            $image_base . json_decode($product['images'], true)[0]
                        ),
                        $products_rows_template
                    );
                }

                return '<table style="width: 100%;">' . $products_row_html . '</table>';
            },

            /* NOTE */
            function ($params, $template_settings) {
                return (!empty(trim(strip_tags($params['order']['note']))) ? $params['order']['note'] : '<small>N/A</small>');
            }
        );

        /* SHORTCODES PRELIEVI */
        $withdrawalShortcodes = array(
            array(
                /* UTENTE */
                "{utente-nome}" => "Il nome dell'utente",
                "{utente-cognome}" => "L'email dell'utente",
                "{utente-email}" => "L'email dell'utente",
                "{anteprima-utente}" => "I dati principali dell'utente",

                "{valore}" => "Il valore richiesto",
            ),
            array(
                /* UTENTE */
                "{[user_data][nome]}",
                "{[user_data][cognome]}",
                "{[user_data][email]}",
                function ($params, $template_settings) {
                    return preg_replace(
                        '/padding([^px%"]+)[px]+/m' ,
                        '',
                        (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($params['user_data']['id'])
                    );
                },
                "{[value]}"
            )
        );

        $templates = array(
            'msagency' => array(
                '[msagency][admin]new-order' => array(
                    'nome' => '[ADMIN] Nuovo ordine ricevuto',
                    'reply_to' => array(),
                    'address' => $this->getSuperAdminEmails(),
                    'subject' => "Nuovo ordine ricevuto - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/framework360/ms/orders/edit.php?id=' . $params['order']['id'];
                            }
                        ))
                    )
                ),
                '[msagency][consultant]new-order' => array(
                    'nome' => '[CONSULENTE] Nuovo ordine ricevuto',
                    'reply_to' => array(),
                    'address' => '{[consultant_data][email]}',
                    'subject' => "Nuovo ordine ricevuto - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(

                            /* CONSULENTE */
                            "{consulente-nome}" => "Il nome del consulente",
                            "{consulente-cognome}" => "L'email del consulente",
                            "{consulente-email}" => "L'email del consulente",
                            "{anteprima-consulente}" => "I dati principali del consulente",

                            /* URL ORDINE */
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(

                            /* CONSULENTE */
                            "{[consultant_data][nome]}",
                            "{[consultant_data][cognome]}",
                            "{[consultant_data][email]}",
                            function ($params, $template_settings) {
                                return preg_replace(
                                    '/padding([^px%"]+)[px]+/m' ,
                                    '',
                                    (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserPreviewHTML($params['consultant_data']['id'])
                                );
                            },

                            /* URL ORDINE */
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?id=' . $params['order']['id'];
                            }
                        ))
                    )
                ),
                '[msagency][customer]new-quote' => array(
                    'nome' => '[CLIENTE] Preventivo aggiornato',
                    'reply_to' => array(),
                    'address' => '{[order][customer_data][email]}',
                    'subject' => "Il tuo preventivo - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?token=' . $params['order']['guest_token'];
                            }
                        ))
                    )
                ),
                '[msagency][seller]new-quote' => array(
                    'nome' => '[VENDITORE] Preventivo aggiornato',
                    'reply_to' => array(),
                    'address' => '{[order][seller_data][email]}',
                    'subject' => "Preventivo disponibile - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?token=' . $params['order']['guest_token'];
                            }
                        ))
                    )
                ),
                '[msagency][customer]order-summary' => array(
                    'nome' => '[CLIENTE] Resoconto ordine',
                    'reply_to' => array(),
                    'address' => '{[order][customer_data][email]}',
                    'subject' => "Resoconto dell'ordine - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?token=' . $params['order']['guest_token'];
                            }
                        ))
                    )
                ),
                '[msagency][seller]order-summary' => array(
                    'nome' => '[VENDITORE] Resoconto ordine',
                    'reply_to' => array(),
                    'address' => '{[order][seller_data][email]}',
                    'subject' => "Resoconto dell'ordine - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?id=' . $params['order']['id'];
                            }
                        ))
                    )
                ),
                '[msagency][customer]order-status[1]' => array(
                    'nome' => '[CLIENTE] Ordine pagato',
                    'reply_to' => array(),
                    'address' => '{[order][customer_data][email]}',
                    'subject' => "Ordine confermato - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?token=' . $params['order']['guest_token'];
                            }
                        ))
                    )
                ),
                '[msagency][seller]order-status[1]' => array(
                    'nome' => '[VENDITORE] Ordine pagato',
                    'reply_to' => array(),
                    'address' => '{[order][seller_data][email]}',
                    'subject' => "Ordine confermato - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?id=' . $params['order']['id'];
                            }
                        ))
                    )
                ),
                '[msagency][admin]order-status[1]' => array(
                    'nome' => '[ADMIN] Ordine pagato',
                    'reply_to' => array(),
                    'address' => $this->getSuperAdminEmails(),
                    'subject' => "Ordine confermato - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/framework360/ms/orders/edit.php?id=' . $params['order']['id'];
                            }
                        ))
                    )
                ),
                '[msagency][seller]order-status[2]' => array(
                    'nome' => '[VENDITORE] Ordine completato',
                    'reply_to' => array(),
                    'address' => '{[order][seller_data][email]}',
                    'subject' => "Ordine completato - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?token=' . $params['order']['id'];
                            }
                        ))
                    )
                ),
                '[msagency][customer]order-status[2]' => array(
                    'nome' => '[CLIENTE] Ordine completato',
                    'reply_to' => array(),
                    'address' => '{[order][customer_data][email]}',
                    'subject' => "Ordine completato - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?token=' . $params['order']['guest_token'];
                            }
                        ))
                    )
                ),
                '[msagency][admin]order-status[2]' => array(
                    'nome' => '[ADMIN] Ordine completato',
                    'reply_to' => array(),
                    'address' => $this->getSuperAdminEmails(),
                    'subject' => "Ordine completato - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/framework360/ms/orders/edit.php?id=' . $params['order']['id'];
                            }
                        ))
                    )
                ),
                '[msagency][customer]order-status[3]' => array(
                    'nome' => '[CLIENTE] Ordine annullato',
                    'reply_to' => array(),
                    'address' => '{[order][customer_data][email]}',
                    'subject' => "Ordine annullato - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?token=' . $params['order']['guest_token'];
                            }
                        ))
                    )
                ),
                '[msagency][customer]order-status[4]' => array(
                    'nome' => '[CLIENTE] Ordine rimborsato',
                    'reply_to' => array(),
                    'address' => '{[order][customer_data][email]}',
                    'subject' => "Ordine rimborsato - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => array(
                        array_merge($globalShortcodeKeys, array(
                            "{order-url}" => "URL dell'ordine",
                        )),
                        array_merge($globalShortcodeValues, array(
                            function ($params, $template_settings) {
                                return  (new \MSFramework\framework())->getWebsitePathsBy('id', 41)['backend_url'] . 'modules/ms_agency/ordini/edit.php?token=' . $params['order']['guest_token'];
                            }
                        ))
                    )
                ),
                '[msagency][admin]withdrawal-request' => array(
                    'nome' => '[ADMIN] Nuova richiesta di prelievo',
                    'reply_to' => '{[user_data][email]}',
                    'address' => $this->getSuperAdminEmails(),
                    'subject' => "Nuova richiesta di prelievo - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => $withdrawalShortcodes
                ),
                '[msagency][user]withdrawal-requested' => array(
                    'nome' => '[UTENTE] Richiesta di prelievo aperta',
                    'reply_to' => array(),
                    'address' => "{[user_data][email]}",
                    'subject' => "Richiesta di prelievo aperta - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => $withdrawalShortcodes
                ),
                '[msagency][user]withdrawal-accepted' => array(
                    'nome' => '[UTENTE] Richiesta di prelievo accettata',
                    'reply_to' => array(),
                    'address' => "{[user_data][email]}",
                    'subject' => "Richiesta di prelievo accettata - {site-name}",
                    'beforeSend' => $beforeSend,
                    'shortcodes' => $withdrawalShortcodes
                )
            )
        );

        return $templates;
    }
}
<?php
/**
 * MSFramework
 * Date: 07/05/18
 */

namespace MSFramework\MSAgency;

class orders {

    public $currentUserID = false;
    public $userViewMode = false;

    public $sellerOriginDB = 'marke833_marketingstudio';

    public function __construct() {

        Global $MSFrameworkDatabase, $MSFrameworkCMS, $MSFrameworkUsers;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;

        $this->currentUserID = $MSFrameworkUsers->getUserDataFromSession('id');
        $this->userViewMode = (new \MSFramework\MSAgency\services())->userViewMode;


        //if(FRAMEWORK_DEBUG_MODE) $this->sellerOriginDB = $_SESSION['db'];
    }

    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente allo stato degli ordini
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getStatus($key = "") {
        $ary = array(
            "0" => array(
                "id" => "awaiting_payment",
                "label" => "In attesa di pagamento",
                "icon" => '',
                "color" => "#F44336"
            ),
            "1" => array(
                "id" => "wip",
                "label" => "In corso",
                "icon" => '',
                "color" => "#2196F3"
            ),
            "2" => array(
                "id" => "complete",
                "label" => "Completato",
                "icon" => '',
                "color" => "#4CAF50"
            ),
            "3" => array(
                "id" => "canceled",
                "label" => "Annullato",
                "icon" => '',
                "color" => "#9E9E9E"
            ),
            "4" => array(
                "id" => "refunded",
                "label" => "Rimborsato",
                "icon" => '',
                "color" => "#000000"
            ),
        );

        if($key !== "") {
            return $ary[$key];
        } else {
            return $ary;
        }
    }

    /**
     * Restituisce lo stato di un singolo prodotto tenendo in considerazione tutte le variabili
     * @param array $product L'array con le info del prodotto
     * @return array
     */
    public function getSingleProductStatus($product) {

        $single_status = array(
            "0" => array(
                "id" => "to_assign",
                "label" => "Pagato e da assegnare",
                "icon" => '',
                "color" => "#f8ac59"
            ),
            "1" => array(
                "id" => "awaiting_seller",
                "label" => "In attesa di accettazione",
                "icon" => '',
                "color" => "#FFC107"
            ),
            "2" => array(
                "id" => "quote_needed",
                "label" => "Da preventivare",
                "icon" => '',
                "color" => "#f8ac59"
            ),
            "3" => array(
                "id" => "active_subscription",
                "label" => "Abbonamento attivo",
                "icon" => '',
                "color" => "#14588e"
            )
        );

        $current_product_status = array();

        if ((int)$product['payment_status'] >= 2) {

            if((int)$product['payment_status'] == 2 && (int)$product['una_tantum'] === 0) {
                $current_product_status = $single_status["3"];
            } else {
                // Se l'ordine è stato concluso ottengo direttamente lo stato impostato
                $current_product_status = $this->getStatus($product['payment_status']);
            }
        }
        else
        {
            if (!$product['quote_needed']) {
                if ((int)$product['payment_status'] === 1) {

                    if (!empty($product['consultant'])) {

                        if((int)$product['consultant_accepted'] === 1) {
                            // Se il consulente ha accettato imposto 'In Corso'
                            $current_product_status = $this->getStatus('1');
                        } else {
                            // Se il consulente non ha ancora accettato imposto 'In attesa di accettazione'
                            $current_product_status =  $single_status["1"];
                        }

                    } else {
                        //Se l'ordine non è stato assegnato imposto 'Da assegnare'
                        $current_product_status = $single_status["0"];
                    }

                } else if ((int)$product['payment_status'] === 0) {
                    // Se l'ordine non è stato ancora pagato imposto 'In attesa di pagamento'
                    $current_product_status = $this->getStatus('0');
                }
            } else {
                // Se l'ordine richiede ancora un preventivo imposto 'Da preventivare'
                $current_product_status = $single_status["2"];
            }
        }

        return $current_product_status;

    }

    /**
     * Restituisce lo stato di un ordine tenendo in considerazione lo stato dei singoli prodotti
     * @param mixed $order L'ID dell'ordine o direttamente l'array
     * @param string $for [admin|seller|consultant|customer]
     * @return array
     */
    public function getFullOrderStatus($order, $for = 'admin') {
        if(!is_array($order)) {
            $order = $this->getOrderDetails($order, true)[$order];
        }

        $single_statuses = array();
        foreach($order['cart'] as $product) {
            if($for === 'customer') {
                $single_statuses[] = json_encode($this->getStatus((int)$product['payment_status']));
            } else {
                $single_statuses[] = json_encode($this->getSingleProductStatus($product));
            }
        }

        $single_unique_statuses = array_unique($single_statuses);

        $current_product_status = array();

        if(count($single_unique_statuses) === 1) {
            // Se tutti i prodotti sono nello stesso stato lo imposto anche all'ordine
            $current_product_status[] = json_decode($single_unique_statuses[0], true);
        } else {

            foreach($single_unique_statuses as $single_unique_status_json) {
                $count_current_status = array_count_values($single_statuses);
                $single_unique_status_array = json_decode($single_unique_status_json, true);
                $single_unique_status_array['count'] = $count_current_status[$single_unique_status_json];
                $single_unique_status_array['label'] = '<b>' . $count_current_status[$single_unique_status_json] . ' - </b> ' . $single_unique_status_array['label'];
                $current_product_status[] = $single_unique_status_array;
            }

            usort($current_product_status, function ($a, $b) {
                return $b['count'] - $a['count'];
            });

        }

        return $current_product_status;

    }

    /**
     * Restituisce un array associativo (ID Ordine => Dati ordine) con i dati relativi all'ordine
     *
     * @param mixed $id L'ID dell'ordine da ottenere (o un array con più id)
     * @param mixed $limit_by_role Se passato un valore limita i risultati, se true limita i risultati in base al ruolo dell'utente
     *
     * @return array
     */
    public function getOrderDetails($id = "", $limit_by_role = false) {
        Global $MSFrameworkUsers;

        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))$same_db_string_ary = array(" id != '' ", array());

        $limit_where = '';
        if($limit_by_role) {
            $currentUserRole = $limit_by_role;
            if($limit_by_role === true) $currentUserRole = $this->userViewMode;

            if($currentUserRole === 'consultant') {
                $limit_where = "FIND_IN_SET('" . $this->currentUserID . "', consultant)";
            } else if ($currentUserRole === 'seller') {
                $limit_where = "seller = '" . $this->currentUserID . "'";
            }

            // I clienti vedono solo gli ordini assegnati a loro, e dal loro DB
            $limit_where = "AND ((seller = '" . $this->currentUserID . "' AND origin_reference = '" . $_SESSION['db'] . "')" . ($limit_where ? ' OR (' . $limit_where . ')' : '') . ")";
        }

        $ary_to_return = array();
        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT * FROM  `" . FRAMEWORK_DB_NAME . "`.`ms_agency__orders` WHERE id != '' $limit_where AND (" . $same_db_string_ary[0] . ") ORDER BY id DESC", $same_db_string_ary[1]) as $r) {
            $r['customer_data'] = json_decode($r['customer_data'], true);
            $r['cart'] = json_decode($r['cart'], true);

            // Se l'acquisto non è partito da MarketingStudio nascondo il seller
            if($r['origin_reference'] === $this->sellerOriginDB) {
                $r['seller_data'] = (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserDataFromDB($r['seller']);
            } else {

                $r['buyer'] = $r['seller'];
                $r['buyer_data'] = array();

                $r['seller'] = 0;
                $r['seller_data'] = array();
            }

            if($limit_by_role && $this->userViewMode === 'consultant') {
                if($r['seller'] != $this->currentUserID) {
                    foreach($r['cart'] as $full_id => $product) {
                        if ($product['consultant'] != $this->currentUserID) {
                            unset($r['cart'][$full_id]);
                        } else if ($product['payment_status'] > 0) {
                            // Mostro il prezzo del consulente (applico la percentuale)
                            $r['cart'][$full_id]['prezzo'] = ($r['cart'][$full_id]['prezzo'] * (new \MSFramework\MSAgency\earnings())->consultantCommission) / 100;
                        }
                    }
                }
            }

            $r['guest_token'] = base64_encode($r['order_date'] . '_' . $r['id']);

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Permette di ottenere i dettagli di un ordine utilizzando il token segreto
     * @param $token string Il token dell'ordine
     * @return array Le info dell'ordine
     */
    public function getOrderByToken($token) {

        $return_array = array();

        if(base64_decode($token)) {
            $decoded_token = explode('_', base64_decode($token));
            if(count($decoded_token) == 2) {
                if($this->MSFrameworkDatabase->getCount("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`ms_agency__orders` WHERE id = :id AND order_date = :order_date", array(':id' => $decoded_token[1], ':order_date' => $decoded_token[0]))) {
                    $return_array = $this->getOrderDetails($decoded_token[1])[$decoded_token[1]];
                }
            }
        }

        return $return_array;
    }

    /**
     * Restituisce una lista di tutti i consulenti collegati ad un ordine
     *
     * @param mixed $id L'ID dell'ordine
     *
     * @return array
     */
    public function getOrderConsultants($id) {

        $orderDetails = $this->getOrderDetails($id)[$id];

        $consultant_ids = array();
        foreach($orderDetails['cart'] as $product) {
            if((int)$product['consultant']) $consultant_ids[] = (int)$product['consultant'];
        }

        return array_unique($consultant_ids);
    }

    /**
     * Aggiorna un prodotto di un determinato ordine
     * @param int $order_id L'ID dell'ordine
     * @param int $product_id L'ID del prodotto
     * @param array $update_array Un array associativo con i valori da aggiornare
     *
     * @return bool
     */
    public function updateOrderProducts($order_id, $product_id, $update_array) {

        $order_data = $this->getOrderDetails($order_id)[$order_id];
        $product_data = $order_data['cart'][$product_id];

        if(!$product_data) false;

        $total_consultant = explode(',', $order_data['consultant']);

        foreach($update_array as $key => $value) {
            if($key === 'prezzo') {
                if((float)$value == 0) $product_data['quote_needed'] = 1;
                else $product_data['quote_needed'] = 0;

            } else if($key == 'consultant') {
                if($product_data['consultant']) $total_consultant = array_diff($total_consultant, [$product_data['consultant']]);
                $total_consultant[] = $value;
            }

            $product_data[$key] = $value;
        }

        // Assegno eventuali crediti
        if((int)$update_array['payment_status'] === 1 || (int)$update_array['payment_status'] === 2) {
            if ($product_data['credit_to_assign'] > 0 && $product_data['credit_assigned'] != 1 && $order_data['origin_reference'] !== $this->sellerOriginDB) {
                Global $MSFrameworkFW, $MSFrameworki18n;
                $customerData = $MSFrameworkFW->getWebsitePathsBy('customer_database', $order_data['origin_reference']);
                if ($customerData) {
                    (new \MSFramework\Framework\credits(str_replace('saas_', '', $customerData['id']), ($customerData['from_saas'])))
                        ->addCredits($product_data['credit_to_assign'], 'Acquisto ' . $MSFrameworki18n->getFieldValue($product_data['nome']));
                    $product_data['payment_status'] = 2;
                    $product_data['credit_assigned'] = 1;
                    $update_array['payment_status'] = 2;
                }
            }
        }

        $order_data['cart'][$product_id] = $product_data;

        $array_to_save = array(
            "cart" => json_encode($order_data['cart']),
            "consultant" => implode(',', array_filter(array_unique($total_consultant))),
            "last_update" => date('Y-m-d H:i:s')
        );

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'update');
        $this->MSFrameworkDatabase->pushToDB("UPDATE marke833_framework.ms_agency__orders SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $order_id), $stringForDB[0]));


        // Imposto solo il prodotto da mostrare nelle email
        $order_data['cart'] = array($product_id => $order_data['cart'][$product_id]);

        // Email a consulente (Nuovo lavoro)
        if(
            (in_array('consultant', array_keys($update_array)) && (int)$product_data['payment_status'] === 1)
            ||
            (in_array('payment_status', array_keys($update_array)) && (int)$update_array['payment_status'] === 1 && $product_data['consultant'])
        ) {
            (new \MSFramework\MSAgency\emails())->sendMail(
                '[msagency][consultant]new-order',
                array(
                    'order' => $order_data,
                    'product_id' => $product_id,
                    'consultant_data' => (new \MSFramework\users(MARKETINGSTUDIO_DB_NAME))->getUserDataFromDB($product_data['consultant'])
                )
            );
        }

        // Assegno la commissione al consulente
        if($product_data['consultant'] && ((int)$product_data['payment_status'] === 1 || (int)$product_data['payment_status'] === 2) && (int)$product_data['consultant_accepted'] == 1) {
            (new \MSFramework\MSAgency\earnings())->assignCommission($product_data['consultant'], $order_id, $product_id, 'consultant');
        }

        // Notifica cambio di stato
        if(in_array('payment_status', array_keys($update_array))) {

            // Invio la notifica al cliente
            (new \MSFramework\MSAgency\emails())->sendMail(
                '[msagency][customer]order-status[' . $update_array['payment_status'] . ']',
                array(
                    'order' => $order_data,
                    'product_id' => $product_id
                )
            );

            // Invio la notifica al venditore
            (new \MSFramework\MSAgency\emails())->sendMail(
                '[msagency][seller]order-status[' . $update_array['payment_status'] . ']',
                array(
                    'order' => $order_data,
                    'product_id' => $product_id
                )
            );

            // Invio la notifica agli admin
            (new \MSFramework\MSAgency\emails())->sendMail(
                '[msagency][admin]order-status[' . $update_array['payment_status'] . ']',
                array(
                    'order' => $order_data,
                    'product_id' => $product_id
                )
            );

            if((int)$update_array['payment_status'] === 1 && $order_data['seller']) {
                (new \MSFramework\MSAgency\earnings())->assignCommission($order_data['seller'], $order_id, $product_id, 'seller');
            }

        }

        return true;

    }

    /**
     * Verifica che il preventivo sia effettivamente pagabile (appartiene al cliente che fa la richiesta e non è stato già pagato)
     *
     * @param $id L'id del preventivo
     * @param $product_id L'ID del prodotto da pagare
     *
     * @return bool
     */
    public function checkOrderPayable($id, $product_id) {

        $r_response_details = $this->getOrderDetails($id)[$id];

        $is_payable = false;
        if($r_response_details && isset($r_response_details['cart'][$product_id])) {
            if((int)$r_response_details['cart'][$product_id]['payment_status'] === 0 && !(int)$r_response_details['cart'][$product_id]['quote_needed'] && $r_response_details['cart'][$product_id]['prezzo'] > 0) {
                $is_payable = true;
            }
        }

        return $is_payable;
    }

    /**
     * Aggiorna lo stato di pagamento di un preventivo
     *
     * @param $preorder_info integer L'ID del preventivo o l'array con i riferimento alla riga del preordine
     * @param $status integer Il nuovo stato
     * @param bool $ipn_data Se valorizzato come array, processa i dati registrando l'ordine ed emettendo fattura
     *
     * @return bool
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function changeStatus($preorder_info, $status, $ipn_data = false) {
        $preorder = (new \MSFramework\Framework\payway())->getPreorderData($preorder_info);

        $preorder_framework_data = json_decode($preorder['framework_data'], true);

        $order_id = $preorder['order_id'];
        $product_id = $preorder_framework_data['item']['id'];

        $r = $this->getOrderDetails($order_id)[$order_id];

        if($ipn_data) {

            $ipn_data = array($ipn_data);
            if (json_decode($preorder['ipn_data'])) {
                $ipn_data[] = json_decode($preorder['ipn_data'], true);
            }

            $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`ms_agency__transactions` SET ipn_data = :ipn_data WHERE id = :id", array(":ipn_data" => json_encode($ipn_data), ":id" => $preorder['id']));

            if ($status == 1) {

                $this->updateOrderProducts($order_id, $product_id, array(
                    'payment_status' => 1
                ));

                $customer_invoice_data = array(
                    "nome" => ($r['customer_data']['ragione_sociale'] != "" ? $r['customer_data']['ragione_sociale'] : $r['customer_data']['cognome'] . " " . $r['customer_data']['nome']),
                    "indirizzo_via" => $r['customer_data']['indirizzo'],
                    "indirizzo_cap" => $r['customer_data']['cap'],
                    "indirizzo_citta" => $r['customer_data']['comune'],
                    "indirizzo_provincia" => $r['customer_data']['provincia'],
                    "paese" => "Italia",
                    "paese_iso" => "IT",
                    "mail" => $r['customer_data']['email'],
                    "piva" => $r['customer_data']['piva'],
                    "cf" => $r['customer_data']['cf']
                );

                //emetto la fattura su Fatture in Cloud
                $fic_invoices = new \MSFramework\Fatturazione\Services\FattureInCloud\invoices();
                $fic_invoices->newInvoice($customer_invoice_data, [["name" => $preorder_framework_data['item']['title'], "descr" => $preorder_framework_data['item']['descr'], "price" => $preorder_framework_data['price']['iva_inclusa']]], ["method" => $preorder['provider']]);
            } else if($status == 0) {
                $this->updateOrderProducts($order_id, $product_id, array(
                    'payment_status' => 3
                ));
            }

        }

        return false;
    }

    /**
     * Aggiorna lo stato di pagamento di un preventivo
     *
     * @param $order_id integer L'ID dell'ordine
     * @param $product_id integer L'ID del prodotto
     *
     * @return bool
     */
    public function cancelSubscription($order_id, $product_id) {
        $orderDetails = $this->getOrderDetails($order_id, true)[$order_id];
        $product_details = $orderDetails['cart'][$product_id];

        if($product_details) {
            $transaction = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`ms_agency__transactions` WHERE order_id = :order_id AND framework_data LIKE '%\"" . $product_id . "\"%'", array(':order_id' => $order_id), true);
            $preorder_data = $this->MSFrameworkDatabase->getAssoc("SELECT agreement_id FROM `" . FRAMEWORK_DB_NAME . "`.`subscription__agreements` WHERE transaction_id = :id", array(':id' => $transaction['id']), true);

            if($transaction && $preorder_data) {
                $providerData = (new \MSFramework\Framework\payway())->getProviderData('paypal');
                $settings_data = $providerData['pay_data'][PAYMETHODS_USE_KEY];
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential($settings_data['client_id'], $settings_data['secret']));
                $apiContext->setConfig(
                    array(
                        'mode' => (PAYMETHODS_USE_KEY == "sandbox" ? PAYMETHODS_USE_KEY : "live")
                    )
                );

                $agreement = new \PayPal\Api\Agreement();

                $agreement->setId($preorder_data['agreement_id']);
                $agreementStateDescriptor = new \PayPal\Api\AgreementStateDescriptor();
                $agreementStateDescriptor->setNote("Abbonamento annullato");

                try {
                    $agreement->cancel($agreementStateDescriptor, $apiContext);
                    \PayPal\Api\Agreement::get($agreement->getId(), $apiContext);
                    $this->updateOrderProducts($order_id, $product_id, array('payment_status' => 3));

                } catch (PayPal\Exception\PayPalConnectionException $ex) {
                    return false;
                } catch (\Exception $ex) {
                    return false;
                }
            }

        }

        return false;
    }

    /**
     * Ottiene tutti i metodi di pagamento disponibili per un determinato SaaS
     *
     * @return array
     */
    public function getAvailPayMethods() {
        global $MSFrameworkFW;

        $settings_data = $MSFrameworkFW->getCMSData('settings')['payMethods'];
        return $settings_data;
    }

}
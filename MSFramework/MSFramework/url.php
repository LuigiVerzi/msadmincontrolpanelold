<?php
/**
 * MSFramework
 * Date: 24/02/18
 */

namespace MSFramework;

class url {

    // Verrà sostituito con il REFERER quando i clienti usano FW360 su un sito esterno tramite integrazione
    public $current_url = '';

    public $currentSlugDetails = array();
    public $currentSlugArray = array();

    public function __construct($custom_rewrite = array()) {
        global $firephp;

        Global $MSFrameworkDatabase;
        Global $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;

        $url_to_rewrite = array_merge($this->getUrlToRewrite(), $custom_rewrite);
        $this->url_to_rewrite = $url_to_rewrite;

        $this->current_url = $_SERVER['HTTP_HOST'] . rtrim($MSFrameworkCMS->getCleanHost($MSFrameworkCMS->getCleanHost($_SERVER['REQUEST_URI'])), '/');
    }

    /**
     * Ottiene l'indirizzo reale partendo dal parametro
     *
     * @param boolean $override Se impostato su TRUE sostituisce i parametri con gli eventuali dati specifici del singolo sito (file /slug.json)
     *
     * @return array
     */
    public function getParamsLocation($override = false) {

        $default_slugs = array(
            'redirect_to_dashboard' => 'profilo/dashboard',
            'redirect_to_order' => 'profilo/ordine/$1',
            'redirect_to_login' => 'profilo/login',
            'redirect_to_registration' => 'profilo/registrazione',
            'redirect_to_subscription' => 'dashboard/?tab=abbonamento',
        );

        if($override) {
            if(file_exists(ABSOLUTE_SW_PATH . 'slug.json')) {
                $slugs = file_get_contents(ABSOLUTE_SW_PATH . 'slug.json');
                if(json_decode($slugs)) {
                    foreach(json_decode($slugs, true) as $key => $new_slug) {
                        $default_slugs[$key] = $new_slug;
                    };

                }
            }
        }

        return $default_slugs;

    }

    /**
     * Pulisce ed uniforma la stringa che compone l'URL
     *
     * @param string $str La stringa da pulire
     *
     * @return string
     */
    public function cleanString($str) {
        $transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');

        $str = str_replace(array_keys($transliterationTable), array_values($transliterationTable), $str);

        $str = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $str = str_replace(array("/", "'"), " ", $str);
        $str = preg_replace('/[^a-z0-9-\s]/ui', '', $str);
        $str = preg_replace('/\s+/', '-', $str);
        $str = preg_replace('/-+/', '-', $str);
        $str = trim($str, '-');

        return strtolower($str);
    }

    /**
     * Restituisce l'elenco delle tabelle all'interno delle quali è presente il campo "slug", divise per livello (es domain.it/, domain.it/livello1, domain.it/livello1/livello2)
     * @param $level Se impostato, restituisce solo il livello richiesto
     *
     * @return array|mixed
     */
    public function slugCategories($level) {
        global $MSFrameworki18n;
        foreach($MSFrameworki18n->getActiveLanguagesDetails() as $lang) {
            $tables_to_search[$lang['url_code'] . "/"] = array(
                /* PAGINE AGGIUNTIVE*/
                "pagine" => array("action" => "getPage", "use_parents" => true),
                FRAMEWORK_DB_NAME . "`.`pagine" => array("action" => "getPage"),
                /* PAGINE STANDARD */
                "pagine_private" => array("action" => "getPrivatePage", "use_parents" => true),
                /* SCRIPT GDPR */
                FRAMEWORK_DB_NAME . "`.`gdpr_scripts" => array("action" => "getGDPRScript", "translated" => false),
                /* UTENTI */
                "users" => array("action" => "getUser", "translated" => false),
                /* SEDI NEGOZIO */
                "negozio_sedi" => array("action" => "getSede"),
                //"servizi" => array("action" => "getServizio"),
                //"servizi_categorie" => array("action" => "getCategoriaServizio"),
                //"servizi_settori" => array("action" => "getSettoreServizio"),
                //"servizi_portfolio" => array("action" => "getPortfolio"),
                /* SLUG HOTEL */
                "hotel_offerte" => array("action" => "getOffer", "limit_to_functions" => array("hotel")),
                "hotel_rooms" => array("action" => "getRoom", "limit_to_functions" => array("hotel")),
                /* SLUG HOTEL */
                "camping_offerte" => array("action" => "getOffer", "limit_to_functions" => array("camping")),
                "camping_piazzole" => array("action" => "getPiazzola", "limit_to_functions" => array("camping")),
                /* SLUG ECOMMERCE */
                "ecommerce_categories" => array("action" => "getEcommerceCategory", "limit_to_functions" => array("ecommerce")),
                "ecommerce_products" => array("action" => "getEcommerceProduct", "limit_to_functions" => array("ecommerce")),
                "ecommerce_brand" => array("action" => "getEcommerceBrand", "limit_to_functions" => array("ecommerce")),
                /* SLUG BLOG */
                "blog_categories" => array("action" => "getBlogCategory", "limit_to_functions" => array("blog")),
                "blog_posts" => array("action" => "getBlogPost", "limit_to_functions" => array("blog")),
                /* SLUG CENTRO ESTETICO */
                "beautycenter_offerte" => array("action" => "getOffer", "limit_to_functions" => array("beautycenter")),
                /* SLUG AGENZIA VIAGGI */
                "travelagency__packages" => array("action" => "getTravelAgencyPackage", "limit_to_functions" => array("travelagency")),
                "travelagency__packages_categories" => array("action" => "getTravelAgencyPackageCategory", "limit_to_functions" => array("travelagency")),
                /* SLUG AGENZIA IMMOBILIARE */
                "realestate_immobili" => array("action" => "getRealEstateImmobile", "limit_to_functions" => array("realestate")),
                "realestate_categories" => array("action" => "getRealEstateCategory", "limit_to_functions" => array("realestate")),
            );
        }

        if($level != "") {
            return $tables_to_search[$level];
        } else {
            return $tables_to_search;
        }
    }

    /**
     * Ottiene informazioni dettagliate riguardanti il livello, effettuando il parsing e la conversione di eventuali stringhe speciali inserite in $level (come ad esempio langID_x)
     *
     * @param $level Il livello da analizzare
     *
     * @return array
     */
    private function getLevelData($level) {
        global $MSFrameworki18n;
        $lang_det = $MSFrameworki18n->getLanguagesDetails(USING_LANGUAGE_CODE, "url_code, long_code")[USING_LANGUAGE_CODE];
        $level = $lang_det['url_code'] . "/" . $level;
        $lang_long_code = $lang_det['long_code'];

        $ary_to_return['level'] = $level;
        $ary_to_return['lang_long_code'] = $lang_long_code;

        return $ary_to_return;
    }

    /**
     * Controlla se lo slug è disponibile all'interno del livello scelto
     *
     * @param $slug mixed Lo slug da controllare
     * @param string $table_excluded_id Tabella in cui si trova il record da escludere. In caso di controllo "al volo" in fase di edit di un record (o in fase di salvataggio) passando questo parametro è possibile escludere il record corrente dalla ricerca
     * @param mixed $exclude_id Id da escludere. In caso di controllo "al volo" in fase di edit di un record (o in fase di salvataggio) passando questo parametro è possibile escludere il record corrente dalla ricerca
     * @param string $level Il livello in cui cercare (es domain.it/, domain.it/livello1, domain.it/livello1/livello2)
     *
     * @return bool
     */
    public function checkSlugAvailability($slug, $table_excluded_id = "", $exclude_id = "") {

        if(!is_array($slug)) {
            $slug = array($slug);
        }

        $result = true;

        $slug_check = $this->getDetailsFromSlug($slug);

        if($slug_check) {
            if((string)$slug_check['table'] != (string)$table_excluded_id || (int)$slug_check['id'] != (int)$exclude_id) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Ricerca il primo slug disponibile all'interno del livello selezionato
     *
     * @param $slug array Un array con lo slug da controllare
     * @param string $level Il livello in cui cercare (es domain.it/, domain.it/livello1, domain.it/livello1/livello2).
     * @param int $current_counter Serve alla funzione per ricordarsi il valore del counter quando richiama se stessa. NON MODIFICARE
     * @param string $single_slug Lo slug specifico (senza il path dietro)
     *
     * @return string
     */
    public function findNextAvailableSlug($slug, $level = "", $current_counter = 1, $single_slug = '') {

        if(!is_array($slug)) {
            $slug = array($slug);
        }

        $count = $current_counter;

        $new_slug = $slug;
        $new_slug[count($new_slug)-1] = $new_slug[count($new_slug)-1] . '-' . $count;

        if($this->checkSlugAvailability($new_slug, "", "", $level)) {
            if($single_slug != '') return $single_slug  . '-' . $count;
            else return end($new_slug);
        } else {
            $count++;
            return $this->findNextAvailableSlug($slug, $level, $count. $single_slug);
        }
    }

    /**
     * Ottiene i dettagli quali tipologia di pagina ed ID della pagina in base allo slug passato
     *
     * @param $slug array Lo slug da controllare
     * @param string $level Il livello in cui cercare (es domain.it/, domain.it/livello1, domain.it/livello1/livello2).
     *
     * @return array|mixed
     */
    public function getDetailsFromSlug($slug, $level = "") {

        // Aggiungo questo controllo per evitare che la ricerca venga effettuata più volte
        if($this->currentSlugArray && $this->currentSlugDetails && count($this->currentSlugDetails) > 0 && $this->currentSlugArray === $slug) {
            return $this->currentSlugDetails;
        }

        $this->currentSlugArray = $slug;

        $level_data = $this->getLevelData($level);
        $level = $level_data['level'];
        $lang_long_code = $level_data['lang_long_code'];

        $to_return = array();

        if(!is_array($slug)) {
            if(empty($slug)) return $to_return;
            $slug = array($slug);
        }

        if($slug) {
            // Se il seguente slug è presente negli URL riscritti ritorno subito il risultato
            $url_to_rewrite = $this->checkUrlToRewrite($slug);
            if($url_to_rewrite) {
                return $url_to_rewrite;
            }

            foreach ($this->slugCategories($level) as $table => $details) {
                $found_func = true;
                if(is_array($details['limit_to_functions'])) {
                    $found_func = false;
                    foreach($details['limit_to_functions'] as $cur_func) {
                        if($this->MSFrameworkCMS->checkExtraFunctionsStatus($cur_func)) {
                            $found_func = true;
                            break;
                        }
                    }
                }

                // Passo il nome della tabella (utile per la funzione slugAvailCheck)
                $details['table'] = $table;

                // Se la funzionalità extra della seguente tabelle non è attiva salto tipologia
                if(!$found_func) {
                    continue;
                }

                // Se stiamo ricercando tra le pagine allora utilizzo la funzione fatta ad Hoc che gestisce la gerarchia
                if($table == 'pagine' || $table == FRAMEWORK_DB_NAME . "`.`pagine") {

                    $page_found = $this->checkHierarchicalPageFromSlug($slug, $level);

                    if($page_found) {
                        $to_return = $details;
                        $to_return['id'] = $page_found['id'];
                        $to_return['i18n'] = json_decode($page_found['slug'], true);

                        $this->currentSlugDetails = $to_return;

                        return $to_return;
                    }

                } else {

                    $searchingSlug = end($slug);
                    if($table == "realestate_immobili") {
                        $landingSuffix = (new \MSFramework\RealEstate\immobili())->getLandingPageSuffix();
                        $originalSlug = $searchingSlug;
                        $searchingSlug = str_replace($landingSuffix, '', $searchingSlug);
                    }

                    foreach ($this->MSFrameworkDatabase->getAssoc("SELECT id, slug FROM `$table` WHERE slug LIKE :slug", array(":slug" => "%" . $searchingSlug . "%")) as $r) {

                        if (isset($details['translated']) && $details['translated'] === false) {
                            $slug_to_check = $r['slug'];
                        } else {
                            $slug_ary = json_decode($r['slug'], true);
                            $slug_to_check = $slug_ary[$lang_long_code];
                        }

                        if ($slug_to_check == $searchingSlug) {

                            // Controlla che gli URL genitori facciano parte della stessa categoria
                            $i = 0;
                            $ilen = count($slug);

                            foreach (array_slice(array_reverse($slug), 1) as $parent_slug) {
                                if (++$i == $ilen) break;
                                if (!$this->MSFrameworkDatabase->getCount("SELECT id, slug FROM `$table` WHERE slug LIKE :slug", array(":slug" => "%" . $parent_slug . "%"))) {
                                    return $to_return;
                                }
                            }

                            if ($details['action'] == "getRealEstateImmobile" && substr($originalSlug, strlen($landingSuffix) * -1) == $landingSuffix) {
                                $details['action'] = "getRealEstateImmobileLanding";
                            }

                            $to_return = $details;
                            $to_return['id'] = $r['id'];
                            $to_return['i18n'] = json_decode($r['slug'], true);

                            $this->currentSlugDetails = $to_return;

                            return $to_return;
                        }

                    }
                }
            }
        }

        $this->currentSlugDetails = $to_return;

        return $to_return;
    }

    /**
     * Controlla se lo slug punta ad una pagina
     *
     * @param $slug array L'array con le info relative all'URL corrente
     *
     * @return array
     */
    private function checkHierarchicalPageFromSlug($slug, $level) {
        Global $MSFrameworkPages, $MSFrameworkCMS;

        // Faccio controllare prima lo slug diviso dallo slash e successivamente quello tramite trattino
        $slugs_to_check = array($slug);
        $alternative_divider_slug = explode('-', end($slug));
        if(count($alternative_divider_slug) > 0) {
            $slugs_to_check[] = $alternative_divider_slug;
        }

        $page_info = array();
        foreach($slugs_to_check as $slug_to_check) {

            $last_slug = array_pop($slug_to_check);
            foreach(array('pagine', FRAMEWORK_DB_NAME . "`.`pagine") as $table) {

                foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, global_id, slug, slug_divider, parent FROM `$table` WHERE slug LIKE :slug", array(":slug" => "%" . $last_slug . "\"%")) as $r) {

                    $replace_array = array(
                        array($MSFrameworkCMS->getURLToSite(), $level),
                        array('', '')
                    );

                    $id_to_search = $r['id'];
                    if(!empty($r['global_id'])) {
                        $id_to_search = $r['global_id'];
                    }

                    $full_slug = trim(str_replace($replace_array[0], $replace_array[1], $MSFrameworkPages->getURL($id_to_search)), "/");
                    $current_slug = trim(implode('/', $slug), "/");

                    if ($full_slug == $current_slug) {
                        if($table == FRAMEWORK_DB_NAME . "`.`pagine") {
                            $r['id'] = "global-" . $r['id'];
                        }
                        $page_info = $r;
                    }
                }
            }
        }

        return $page_info;

    }

    /**
     * Controlla se lo slug punta ad un Url personalizzato
     *
     * @param $slug array L'array con le info relative all'URL corrente
     *
     * @return array
     */
    public function checkUrlToRewrite($slug) {

        $ary_to_return = array();

        foreach($this->url_to_rewrite as $rewrite)
        {
            $imploded_slug = implode('/', $slug);
            $pattern_url = implode('\/', $rewrite[1]);

            if(preg_match('/^' . $pattern_url . '$/i', $imploded_slug)) {
                $ary_to_return = $rewrite[0];
                foreach($ary_to_return as $k=>$r) {
                    $ary_to_return[$k] = preg_replace_callback(array(
                        '/\$([0-9])/',
                    ), function (array $matches) use ($slug) {
                        return $slug[$matches[1]];
                    }, $ary_to_return[$k]);
                }
                $ary_to_return['custom_rewrite'] = true;
                return $ary_to_return;
            }
        }

        return $ary_to_return;
    }

    /**
     * Crea un redirect 301 automatico nel caso in cui venga modificato lo slug della pagina o elimina un vecchi redirect se necessario
     *
     * @param $reference string La referenza del redirect (Esempio: page-10)
     * @param $new_url string Lo slug attuale della pagina
     * @param $old_url string Il vecchio slug della pagina (Opzionale)
     *
     */
    public function makeRedirectIfNeeded($reference, $new_url, $old_url = '') {
        Global $MSFrameworkUrl;

        $site_url = $this->MSFrameworkCMS->getURLToSite();

        if(stristr($new_url, $site_url) === false) {
            if(FRAMEWORK_DEBUG_MODE) $new_url = str_replace(ABSOLUTE_SW_PATH_HTML, '', $new_url);
            $new_url = $site_url . $new_url;
        }

        if(!empty(str_replace('/', '', $old_url)) && ($old_url !== ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . '/') ) {

            if (stristr($old_url, $site_url) === false) {
                if (FRAMEWORK_DEBUG_MODE && stristr($old_url, $site_url) === false) {
                    $old_url = str_replace(ABSOLUTE_SW_PATH_HTML, '', $old_url);
                }
                $old_url = $site_url . $old_url;
            }

            if ($new_url !== $old_url) {

                $new_slug = trim(str_replace($this->MSFrameworkCMS->getURLToSite(), '', $new_url), '/');
                $old_slug = trim(str_replace($this->MSFrameworkCMS->getURLToSite(), '', $old_url), '/');

                if(!empty($old_slug)) {
                    // Elimino eventuali redirect con la stessa origine del nuovo slug
                    $this->MSFrameworkDatabase->deleteRow("redirect", "url_from", array($new_slug, $new_slug . '/'));

                    // Aggiorno gli altri redirect facendoli puntare al nuovo link
                    $this->MSFrameworkDatabase->query("UPDATE redirect SET url_to = :url_to WHERE redirect_ref = :redirect_ref", array(':url_to' => $new_url, ':redirect_ref' => $reference));

                    $existing_redirect = $this->MSFrameworkDatabase->getAssoc("SELECT creation_date FROM redirect WHERE redirect_ref = :redirect_ref ORDER by creation_date DESC LIMIT 1", array(':redirect_ref' => $reference), true);

                    // Evito di creare un nuovo redirect se ne esistono altri inseriti meno di un giorno fa
                    if (!$existing_redirect || time() > (strtotime($existing_redirect['creation_date'] . ' + 1 days')) && $this->checkSlugAvailability(explode('/', $old_slug))) {
                        $array_to_save = array(
                            "url_from" => $old_slug,
                            "url_to" => $new_url,
                            "redirect_type" => 301,
                            "redirect_ref" => $reference,
                            "active" => 1
                        );

                        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
                        $this->MSFrameworkDatabase->pushToDB("INSERT INTO redirect ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
                    }
                }
            }
        }
    }

    /**
     * Crea un redirect 301 automatico nel caso in cui venga modificato lo slug della pagina o elimina un vecchi redirect se necessario
     *
     * @param $reference_type string La referenza del redirect (Esempio: page)
     * @param $reference_ids array La lista di IDS da eliminare
     *
     */
    public function deleteRedirectsByReferences($reference_type, $reference_ids) {
        $full_ref_ids = array();
        foreach($reference_ids as $ref_id) {
            $full_ref_ids[] = $reference_type . '-' . $ref_id;
        }
        $this->MSFrameworkDatabase->deleteRow("redirect", "redirect_ref", $full_ref_ids);
    }

    /**
     * Ottiene il prefisso da applicare agli URL in base alla lingua selezionata dall'utente o a quella passata al primo parametro
     *
     * @param $lang L'id della lingua per la quale ottenere il prefisso
     *
     * @return string
     */
    public function getLangPrefix($lang = "") {
        global $MSFrameworki18n;

        if($lang == "") {
            $using_lang = $MSFrameworki18n->user_selected_lang;
        } else {
            $using_lang = $lang;
        }

        if($MSFrameworki18n->getPrimaryLangId() == $using_lang) {
            return "";
        } else {
            return $MSFrameworki18n->getLanguagesDetails($using_lang)[$using_lang]['url_code'] . "/";
        }
    }

    /**
     * Verifica se un determinato URL è raggiungibile ed esistente
     *
     * @param $url L'URL da verificare
     *
     * @return bool
     */
    public function checkURLAvailability($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $response = curl_exec($ch);
        $response_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if($response_status == '404' || $response_status == '0') {
            return false;
        }

        return true;
    }

    /**
     * Genera lo short URL e lo salva nel DB, restituendo la stringa generata. Verifica anche se un determinato URL è già stato accorciato evitando di inserire duplicati.
     * @param $url L'URL da verificare
     *
     * @return bool|string
     */
    public function shortenURL($url) {
        if(!$this->checkURLAvailability($url)) {
            return false;
        }

        $r = $this->MSFrameworkDatabase->getAssoc('SELECT id FROM short_urls WHERE url= :url', array(":url" => $url), true);
        if($r['id'] != '') {
            return $this->getShortPrefix() . $this->getShortenedURLFromID($r['id']);
        } else {
            $result = $this->MSFrameworkDatabase->pushToDB('INSERT INTO short_urls (url, created) VALUES (:url, :time)', array(":url" => $url, ":time" => time()));
            if($result) {
                return $this->getShortPrefix() . $this->getShortenedURLFromID($this->MSFrameworkDatabase->lastInsertId());
            }
        }

        return false;
    }

    /**
     * Effettua il redirect all'URL reale partendo dallo short link
     *
     * @param $short Lo short link (solo il codice, senza prefissi) dal quale recuperare l'URL completo
     *
     * @return bool
     */
    public function redirectFromShort($short) {

        $shortened_id = $this->getIDFromShortenedURL($short);
        if($shortened_id == "") {
            return false;
        }

        $long_url = $this->MSFrameworkDatabase->getAssoc('SELECT url FROM short_urls WHERE id = :id', array(":id" => $shortened_id), true);
        $this->MSFrameworkDatabase->getAssoc('UPDATE short_urls SET hits = hits+1 WHERE id = :id', array(":id" => $shortened_id));

        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' .  $long_url['url']);
    }

    /**
     * Restituisce il prefisso che viene settato prima dello short code
     *
     * @return string
     */
    public function getShortPrefix() {
        return 't';
    }

    /**
     * Restituisce l'elenco di caratteri utilizzabili per composse lo short code
     *
     * @return string
     */
    private function getBaseForShort() {
        return '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }

    /**
     * Ottiene lo short code a partire dall'ID del record inserito nel DB
     *
     * @param $id L'ID del record inserito nel DB
     *
     * @return string
     */
    public function getShortenedURLFromID($id) {
        $id += 4294967295; //sommo questo valore (che corrisponde al numero massimo degli ID gestibili con un INT in mysql) all'id normale (quindi il valore massimo di $id può essere 4294967295*2). Faccio questo per fare in modo che il codice short restituito sia sempre di 6 caratteri e non di 1-6 come inizialmente previsto dallo script

        $base = $this->getBaseForShort();

        $length = strlen($base);
        while($id > $length - 1) {
            $out = $base[fmod($id, $length)] . $out;
            $id = floor( $id / $length );
        }

        return $base[$id] . $out;
    }

    /**
     * Ottiene l'ID del record inserito nel DB a partire dallo short code
     *
     * @param $string Lo short code
     *
     * @return bool|int
     */
    public function getIDFromShortenedURL($string) {
        $base = $this->getBaseForShort();

        $length = strlen($base);
        $size = strlen($string) - 1;
        $string = str_split($string);
        $out = strpos($base, array_pop($string));

        foreach($string as $i => $char) {
            $out += strpos($base, $char) * pow($length, $size - $i);
        }

        return $out-4294967295;
    }

    /**
     * Restituisce l'elenco degli URL da riscrivere
     *
     * @return array
     */
    private function getUrlToRewrite() {
        global $MSFrameworki18n;
        $custom_rewrite = array();

        if($this->MSFrameworkCMS->checkExtraFunctionsStatus('blog')) {
            $blog_preferenze = $this->MSFrameworkCMS->getCMSData('blog');
            $custom_rewrite[] = array(array('action' => 'getBlog'), array($MSFrameworki18n->getFieldValue($blog_preferenze['slug'])));
        }

        if($this->MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) {
            $custom_rewrite[] = array(array('action' => 'checkout', 'step' => '$1'), array('checkout', '([a-z0-9\-]+)'));
            $custom_rewrite[] = array(array('action' => 'search', 'query' => '$1'), array('s', '([^\/]+)'));

            $custom_rewrite[] = array(array('action' => 'profilo', 'section' => 'attivazione', 'code' => '$1'), array('profilo', 'attivazione', '([a-z0-9\-]+)'));
            $custom_rewrite[] = array(array('action' => 'profilo', 'section' => 'ordine', 'id' =>'$2'), array('profilo', 'ordine', '([a-z0-9\-]+)'));
            $custom_rewrite[] = array(array('action' => 'profilo', 'section' => '$1'), array('profilo', '([a-z0-9\-]+)'));
            $custom_rewrite[] = array(array('action' => 'profilo', 'section' => 'dashboard'), array('profilo'));
        }

        return $custom_rewrite;
    }

    /**
     * Questa funzione permette di ricreare la stringa da passare in get all'url (utile nel caso della ricerca unita al cambio di visualizzazione dei risultati, ad esempio)
     *
     * @param array $exclude I valori inseriti in questo array (che devono corrispondere al parametro GET) saranno esclusi dalla stringa
     * @param array $append I valori inseriti in questo array saranno aggiunti alla fine della stringa
     *
     * @return string
     */
    public function regenerateGETParams($exclude = array(), $append = array()) {
        $str = "";
        $first_cycle = true;

        $exclude = array_merge(array('bodyClass', 'lang', 'action', 'id', 'level', 'slug'), $exclude);

        foreach($_GET as $k => $v) {
            if(in_array($k, $exclude) || $v == "") {
                continue;
            }

            ($first_cycle) ? $concat = "?" : $concat = "&";

            $str .= $concat . $k . "=" . $v;
            $first_cycle = false;
        }

        if(count($append) != 0) {
            foreach($append as $k => $v) {
                if($v == "") {
                    continue;
                }

                ($first_cycle) ? $concat = "?" : $concat = "&";
                $str .= $concat . $k . "=" . $v;

                $first_cycle = false;
            }
        }

        return $str;
    }
}
?>
<?php
/**
 * MSFramework
 * Date: 01/02/19
 */

namespace MSFramework\Subscriptions;


class subscribers {
    public function __construct() {
        global $firephp;
        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }


    /**
     * Ottiene lo stato dell'abbonamento per un cliente
     *
     * @return mixed
     */
    public function getSubscriptionStatus($user_id) {

        $ary_to_return = array(
            'status' => false,
            'current' => array(),
            'awaiting' => array()
        );

        $subscription_status = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM subscriptions__subscribers WHERE cliente = :cliente AND data_fine > NOW()", array(':cliente' => $user_id), true);
        if($subscription_status) {
            if(strtotime($subscription_status['data_fine']) > time()) {
                $ary_to_return['status'] = true;
                $ary_to_return['current'] = array(
                    'subscription_id' => $subscription_status['id'],
                    'plan_id' => $subscription_status['piano'],
                    'plan_info' => (new \MSFramework\Subscriptions\plans())->getPlanDetails($subscription_status['piano'])[$subscription_status['piano']],
                    'start_date' => $subscription_status['data_inizio'],
                    'end_date' => $subscription_status['data_fine']
                );
            }
        }

        // Controllo se c'è qualche abbonamento in attesa di pagamento
        $awaiting_subscription = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM subscriptions__orders WHERE user_id = :cliente AND order_status = 0", array(':cliente' => $user_id), true);
        if($awaiting_subscription) {
            $ary_to_return['awaiting'] = (new \MSFramework\Subscriptions\orders())->getOrderDetails($awaiting_subscription['id'])[$awaiting_subscription['id']];
        }

        return $ary_to_return;

    }

    /**
     * Ottiene lo stato dell'abbonamento per un cliente
     *
     * @param $user_id int L'ID del beneficiario
     * @param $plan_id int L'ID del piano acquistato
     *
     * @return mixed
     */
    public function addSubscription($user_id, $plan_id) {

        $subscription_status = $this->getSubscriptionStatus($user_id);
        $plan_details = (new \MSFramework\Subscriptions\plans())->getPlanDetails($plan_id)[$plan_id];

        $end_date = date('Y-m-d H:i', strtotime('+1 year'));

        if($this->canUseTrial($user_id, $plan_id)) {
            $end_date = date('Y-m-d H:i', strtotime('+' . (int)$plan_details['trial_days'] . ' day'));
        } else if($subscription_status['current']) {
            if(strtotime($subscription_status['current']['end_date']) > time()) {
                $end_date = date('Y-m-d H:i', strtotime('+1 year', strtotime($subscription_status['current']['end_date'])));
            }
        }

        if($subscription_status['current']) {
            $status = $this->MSFrameworkDatabase->query("UPDATE subscriptions__subscribers SET piano = :piano, data_fine = :scadenza WHERE id = :id", array(':piano' => $plan_id, ':scadenza' => $end_date, ':id' => $subscription_status['current']['subscription_id']));
        }
        else {
            $status = $this->MSFrameworkDatabase->query("INSERT INTO subscriptions__subscribers (cliente, piano, data_inizio, data_fine) VALUES (:cliente, :piano, :inizio, :scadenza)", array(':cliente' => $user_id, ':piano' => $plan_id, ':inizio' => date('Y-m-d H:i'), ':scadenza' => $end_date));
        }

        return $status;

    }

    /**
     * Indica se l'utente può ottenere o no una prova gratuita
     *
     * @param $user_id int L'ID del beneficiario
     *
     * @return mixed
     */
    public function canUseTrial($user_id, $package_id) {

        $canUseTrial = false;

        $package_details = (new \MSFramework\Subscriptions\plans())->getPlanDetails($package_id);
        if($package_details) {
            $package_details = $package_details[$package_id];
            if ((int)$package_details['trial_days'] > 0 && !$this->MSFrameworkDatabase->getAssoc("SELECT * FROM subscriptions__subscribers WHERE cliente = :cliente", array(':cliente' => $user_id), true)) {
                $canUseTrial = true;
            }
        }

        return $canUseTrial;
    }


}
<?php
/**
 * MSFramework
 * Date: 07/05/18
 */

namespace MSFramework\Subscriptions;


use MSFramework\cms;

class orders {
    public function __construct() {
        Global $MSFrameworkDatabase, $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
    }

    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente allo stato degli ordini
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getStatus($key = "") {
        $ary = array(
            "0" => "In attesa di pagamento",
            "1" => "Completato",
            "4" => "Annullato",
        );
        
        if($key !== "") {
            return $ary[$key];
        } else {
            return $ary;
        }
    }

    /**
     * Invia l'ordine al database
     *
     * @param mixed $user_ref L'id o l'email (nel caso dell'acquisto ospite) del cliente che ha piazzato l'ordine
     * @param array $plan_id L'id del pacchetto acquistato
     * @param array $fatturazione l'array con i dati di fatturazione
     * @param array $spedizione l'array con i dati di spedizione
     * @param string $coupons_used Il coupon utilizzato per quest'ordine
     * @param int $order_status Lo stato dell'ordine (da 0 a 4)
     * @param string $payment_type Il pagamento utilizzato per quest'ordine
     * @param int $payment_id L'id del pagamento nel caso in cui l'ordine sia già stato pagato con sistemi automatici
     *
     * @return array|mixed
     */
    public function createNewOrder($user_ref, $plan_id, $fatturazione, $spedizione, $coupons_used, $order_status, $payment_type, $payment_id = 0) {

        $is_trial = (new \MSFramework\Subscriptions\subscribers())->canUseTrial($user_ref, $plan_id);

        $array_to_save = array(
            "user_id" => $user_ref,
            "plan_id" => $plan_id,
            "is_trial" => ($is_trial ? "1" : "0"),
            "plan_info" => json_encode((new \MSFramework\Subscriptions\plans())->getPlanDetails($plan_id)[$plan_id]),
            "info_fatturazione" => json_encode($fatturazione),
            "info_spedizione" => json_encode($spedizione),
            "coupons_used" => (is_array($coupons_used) ? json_encode($coupons_used) : $coupons_used),
            "order_date" => date("Y-m-d H:i:s"),
            "order_status" => $order_status,
            "payment_type" => $payment_type,
            "payment_id" => $payment_id
        );

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "insert");

        if($this->MSFrameworkDatabase->pushToDB("INSERT INTO subscriptions__orders ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0])) {
            return $this->MSFrameworkDatabase->lastInsertId();
        }
        else {
            return false;
        }
    }

    /**
     * Restituisce un array associativo (ID Ordine => Dati ordine) con i dati relativi all'ordine
     *
     * @param string $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     * @param string $fields I campi da prelevare dal DB
     * @param mixed $only_user Trova solo gli ordini di un determinato utente
     *
     * @return array
     */
    public function getOrderDetails($id = "", $fields = "*", $only_user = false)
    {
        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }
        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        if($only_user != false) {
            $user_where = "user_id = " . $only_user;
        } else {
            $user_where = "id != ''";
        }

        $ary_to_return = array();
        $category = $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM subscriptions__orders WHERE id != '' AND (" . $same_db_string_ary[0] . ") AND $user_where ORDER BY id DESC", $same_db_string_ary[1]);
        foreach ((array)$category as $r) {
            $ary_to_return[$r['id']] = $r;
            if(isset($r['plan_info'])) $ary_to_return[$r['id']]['plan_info'] = json_decode($r['plan_info'], true);
            if(isset($r['info_fatturazione'])) $ary_to_return[$r['id']]['info_fatturazione'] = json_decode($r['info_fatturazione'], true);
            if(isset($r['info_spedizione'])) $ary_to_return[$r['id']]['info_spedizione'] = json_decode($r['info_spedizione'], true);
            if(isset($r['coupons_used'])) $ary_to_return[$r['id']]['coupons_used'] = (json_decode($r['coupons_used']) ? json_decode($r['coupons_used'], true) : array());
        }

        return $ary_to_return;
    }

    /**
     * Aggiorna lo stato dell'ordine
     *
     * @param $order_id L'ID dell'ordine completato
     * @param $status_id L'ID dello stato attuale
     * @param $send_email bool Se impostato su true allora invia anche l'email di conferma all'utente
     *
     * @return bool
     */
    public function updateOrderStatus($order_id, $status_id, $send_email = true) {

        $this->MSFrameworkDatabase->query("UPDATE subscriptions__orders SET order_status = " . $status_id . " WHERE id = " . $order_id);

        $order_details = $this->getOrderDetails($order_id)[$order_id];

        if($status_id == 1) {
            (new \MSFramework\Subscriptions\subscribers())->addSubscription($order_details['user_id'], $order_details['plan_id']);
            (new \MSFramework\Fatturazione\vendite())->importFrom('subscriptions', $order_id);
        } else if($status_id == 4) {
            if((new \MSFramework\Subscriptions\subscribers())->getSubscriptionStatus($order_details['user_id'])['status']) {
                if($this->MSFrameworkDatabase->getAssoc("SELECT id FROM subscriptions__orders WHERE user_id = :user_id and id = (SELECT MAX(id) FROM subscriptions__orders WHERE user_id = :user_id2)", array(':user_id' => $order_details['user_id'], ':user_id2' => $order_details['user_id']), true)) {
                    $this->MSFrameworkDatabase->query("UPDATE subscriptions__subscribers SET data_fine = NOW() WHERE cliente = :id", array(':id' => $order_details['user_id']));
                }
            }
        }

        if($send_email) {
            $this->sendOrderStatusEmail($order_id);
        }

        return true;
    }

    /**
     * Invia l'email relativa allo stato dell'ordine attuale
     *
     * @param $order_id int L'ID dell'ordine completato
     * @param $origin int L'origine dell'invio dell'email (0 = FRONT, 1 = BACK)
     *
     * @return mixed
     */
    public function sendOrderStatusEmail($order_id, $origin = 0) {

        $subscriptionsEmail = new \MSFramework\Subscriptions\emails();

        if(strstr(REAL_SW_PATH, "MSAdminControlPanel")) {
            $ajax_url = $this->MSFrameworkCMS->getURLToSite() . $subscriptionsEmail->getACPBridgeShort() . '/' . $subscriptionsEmail->getACPBridgeToken() . '/send_subscription_status_email/' . $order_id;
            return file_get_contents($ajax_url);
        } else {

            $order_details = $this->getOrderDetails($order_id)[$order_id];
            $status_id = intval($order_details['order_status']);

            if($order_details['user_id']) {
                $customer = (new \MSFramework\customers())->getCustomerDataFromDB($order_details['user_id']);
                $email_address = $customer['email'];
            }
            else {
                $customer = array('nome' => $order_details['info_fatturazione']['nome'], 'cognome' => $order_details['info_fatturazione']['cognome'], 'username' => $order_details['guest_email']);
                $email_address = $order_details['guest_email'];
            }

            /*
            $invoice_settings = $this->MSFrameworkCMS->getCMSData('fatturazione_fatture');
            if(!empty($invoice_settings['when_to_send']) && $invoice_settings['when_to_send'] == $status_id) {
                (new \MSFramework\Subscriptions\invoices())->sendInvoiceToCustomer($order_id);
            }
            */

            /* SE L'INVIO AVVIENE DAL FRONTEND ALLORA NOTIFICO L'ADMIN */
            if($origin == 0) {
                $subscriptionsEmail->sendMail('subscription-status/admin/' . $status_id, array(
                    'to' => 'admin',
                    'order' => $order_details,
                    'user' => $customer,
                ));
            }

            return $subscriptionsEmail->sendMail('subscription-status/' . $status_id, array(
                'to' => 'customer',
                'order' => $order_details,
                'email' => $email_address,
                'user' => $customer
            ));
        }
    }

    /**
     * Conta il numero di ordini totali
     *
     * @return mixed
     */
    public function countTotalOrders() {
       return $this->MSFrameworkDatabase->getCount("SELECT * FROM subscriptions__orders");
    }

    /* =============== FUNZIONI ACP ============= */

    /**
     * Questa funzione permette di ottenere il grafico dell'ultimo mese
     *
     * @param $start string La data YYYY-MM-DD di partenza
     * @param $end string La data YYYY-MM-DD di finale
     * @param $order_status integer Lo stato dell'ordine
     *
     * @return array
     */
    public function getDateRangeChart($start, $end, $order_status = 1) {

        /* TODO
        $grafico_ordini = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT date(order_date) as day FROM subscriptions_order WHERE order_status = $order_status AND order_date >= '$start' AND order_date <= '$end 23:59'") as $r) {

            if(!isset($grafico_ordini[$r['day']])) $grafico_ordini[$r['day']] = array('earn' => 0, 'orders' => 0);

            $cart = json_decode($r['cart'], true);

            $grafico_ordini[$r['day']]['earn'] += $cart['coupon_price'];
            $grafico_ordini[$r['day']]['orders'] += 1;
        }

        $start = new \DateTime($start);
        $end =  new \DateTime($end);

        $array_data = array();

        for($i = $start; $i <= $end; $i->modify('+1 day')) {
            if(isset($grafico_ordini[$i->format("Y-m-d")])) $array_data[] = $grafico_ordini[$i->format("Y-m-d")];
            else $array_data[] = array('earn' => 0, 'orders' => 0);
        }

        return $array_data;
        */
    }

}
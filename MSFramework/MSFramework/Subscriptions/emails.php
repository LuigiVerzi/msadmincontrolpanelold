<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

namespace MSFramework\Subscriptions;

class emails extends \MSFramework\emails {
    /**
     * Ottiene la lista di template per l'ecommerce
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {
        $order_status_params = array(
            'prepareParams' => function ($params, $template_settings) {
                $params['order']['info_fatturazione']['stato'] = (new \MSFramework\geonames())->getCountryDetails($params['order']['info_fatturazione']['stato'])[$params['order']['info_fatturazione']['stato']]['name'];
                $params['order']['info_spedizione']['stato'] = (new \MSFramework\geonames())->getCountryDetails($params['order']['info_spedizione']['stato'])[$params['order']['info_spedizione']['stato']]['name'];
                return $params;
            },
            'shortcodes' => array(
                array(
                    "{nome}" => 'Il nome del cliente',
                    "{prezzo_totale}" => "Il prezzo totale",
                    "{tabella_prodotti}" => "La tabella con la lista dei prodotti",
                    "{order_url}" => 'L\'URL del pannello relativo all\'ordine'
                ),
                array(
                    function ($params) {
                        return (!empty($params['user']['nome']) ? $params['user']['nome'] . ' ' . $params['user']['cognome'] : $params['user']['username']);
                    },
                    function ($params) {
                        return ($params['order']['is_trial'] == "1" ? '0' . CURRENCY_SYMBOL . ' (Prova Gratuita)' : number_format((new \MSFramework\Fatturazione\imposte())->getPriceToShow($params['order']['plan_info']['prezzo_annuale'])['tax'],2,',','.') . CURRENCY_SYMBOL);
                    },
                    function ($params) {

                        Global $MSFrameworki18n;

                        $products_rows_template = '<tr><td style="text-align: left;">{product_name}</td><td style="text-align: right;">{product_price}</td></tr>';

                        $products_row_html = str_replace(
                            array(
                                "{product_name}",
                                "{product_price}",
                            ),
                            array(
                                $MSFrameworki18n->getFieldValue($params['order']['plan_info']['titolo']),
                                ($params['order']['is_trial'] == "1" ? '0' . CURRENCY_SYMBOL . ' (Prova Gratuita)' : number_format((new \MSFramework\Fatturazione\imposte())->getPriceToShow($params['order']['plan_info']['prezzo_annuale'])['tax'],2,',','.') . CURRENCY_SYMBOL)
                            ),
                            $products_rows_template
                        );


                        return '<table style="width: 100%;">' . $products_row_html . '</table>';

                    },
                    function ($params) {
                        if($params['to'] == 'admin') {
                            return $this->MSFrameworkCMS->getURLToSite(1) . 'modules/subscriptions/subscribers/edit.php?id=' . $params['order']['user_id'];
                        } else {
                            return $this->MSFrameworkCMS->getURLToSite() . 'redirect_to_subscription/' . $params['order']['id'];
                        }
                    }
                )
            )
        );

        $default_address = $this->MSFrameworkCMS->getCMSData("settings")['destination_address'];

        $templates = array(
            'subscriptions' => array(
                /* TEMPLATE STATO ABBONAMENTI */
                'subscription-status/0' => array(
                    'nome' => '[STATO ABBONAMENTO] In Attesa di Pagamento',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "In Attesa di Pagamento - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes']
                ),
                'subscription-status/1' => array(
                    'nome' => '[STATO ABBONAMENTO] Abbonamento Effettuato',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Abbonamento Attivato - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes']
                ),
                /* TEMPLATE STATO ABBONAMENTI ADMIN */
                'subscription-status/admin/0' => array(
                    'nome' => '[STATO ABBONAMENTO][ADMIN] Nuovo Abbonamento (In Attesa di Pagamento)',
                    'reply_to' => array(),
                    'address' => $default_address,
                    'subject' => "Nuovo Abbonamento - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes']
                ),
                'subscription-status/admin/1' => array(
                    'nome' => '[STATO ABBONAMENTO][ADMIN] Nuovo Abbonamento (Pagato)',
                    'reply_to' => array(),
                    'address' => $default_address,
                    'subject' => "Nuovo Pagamento Ricevuto - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes']
                )
            )
        );

        return $templates;
    }
}
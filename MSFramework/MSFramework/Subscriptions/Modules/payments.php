<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Subscriptions\Modules;

use MSFramework\Subscriptions\orders;

class payments {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {

        $files_to_include = array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'modules/subscriptions/payments/js/payments.js')
        );

        return $files_to_include;
    }

    /**
     * Ottiene le impostazioni del modulo
     *
     * @return array
     */
    public function getModuleSettings()
    {

        $module_settings = array(

        );

        return $module_settings;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {

        /* LE FUNZIONI IPN */
        if(isset($params["ipn_track_id"])) {
            $paypal = new \MSFramework\Subscriptions\Payments\paypal();
            $paypal->checkIPN($params);
        }

        /* FUNZIONI PER L'ELABORAZIONE DEL PAGAMENTO */
        if(isset($params['pay_with_stripe'])) {
            $nexi = new \MSFramework\Subscriptions\Payments\stripe();
            $nexi->payOrder($params);
        }
        return '';

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        return array();
    }

}
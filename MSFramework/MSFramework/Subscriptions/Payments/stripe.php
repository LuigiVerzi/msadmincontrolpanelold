<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

namespace MSFramework\Subscriptions\Payments;


class stripe {
    public function __construct() {
        Global $MSFrameworkDatabase;
        Global $MSFrameworki18n;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworki18n = $MSFrameworki18n;
    }

    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param array $orderDetails L'Array con le info relative all'ordine
     * @param bool $autoclick Se true clicca automaticamente il pulsante con il pagamento
     * @param string $ipn_url Un URL personalizzato per l'IPN
     *
     * @return string L'HTML con il pulsante
     *
     */
    public function getPaymentButton($orderDetails, $autoclick, $ipn_url = "") {
        Global $MSFrameworkCMS;

        if(!$ipn_url || empty($ipn_url)) {
            $ipn_url = $MSFrameworkCMS->getURLToSite() . 'm/payments';
        }

        $metodi_pagamento = $MSFrameworkCMS->getCMSData('fatturazione_fatture');
        $metodi_pagamento = json_decode($metodi_pagamento['webPayments'], true);

        if($metodi_pagamento['stripe_data']['logo'] && json_decode($metodi_pagamento['stripe_data']['logo'])) {
            $stripe_logo = (isset($_SERVER['HTTPS']) ? "https" : "http") . ":" . UPLOAD_ECOMMERCE_PAYMENTS_LOGOS_FOR_DOMAIN_HTML . json_decode($metodi_pagamento['stripe_data']['logo'], true)[0];
        }
        else {
            $stripe_logo = '';
        }

        $CHIAVEPUBBLICAZIONE = $metodi_pagamento['stripe_data']['publishable_key'];

        $prezzo_tassato = (new \MSFramework\Fatturazione\imposte())->getPriceToShow($orderDetails['plan_info']['prezzo_annuale'])['tax'];
        $importo = $prezzo_tassato*100;

        $html = '<script src="https://checkout.stripe.com/checkout.js"></script>';

        $html .= '<a id="stripe_payments" href="#" class="btn btn-success' . ($autoclick ? ' autoclick' : '') . '">' . $this->MSFrameworki18n->gettext("Procedi con il Pagamento") . '</a>';

        $html .= '<script>';
        $html .= 'document.onreadystatechange = function () {';
        $html .= 'if (document.readyState == "complete") {';
        $html .= 'attachStripePaymentsToButton("stripe_payments", ' . $orderDetails['id'] . ', ' . $importo . ', "' . SW_NAME . '" ,"' . $CHIAVEPUBBLICAZIONE . '", "' . $stripe_logo . '", "' . $ipn_url . '");';
        $html .= '}}';
        $html .= '</script>';


        return $html;

    }

    /**
     * Effettua il pagamento dell'ordine
     *
     * @param array $request
     * @param mixed $on_success È possibie passare una funzione
     *
     */
    public function payOrder($request, $on_success = false) {

        $MSFrameworkCMS = new \MSFramework\cms();

        $orders = new \MSFramework\Subscriptions\orders();
        $metodi_pagamento = (new \MSFramework\cms())->getCMSData('fatturazione_fatture');
        $metodi_pagamento = json_decode($metodi_pagamento['webPayments'], true);

        $order_id = $request['order_id'];
        $orderDetails = $orders->getOrderDetails($order_id)[$order_id];

        if(!$orderDetails) die(json_encode(array('status' => 'order_not_found', 'message' => 'Si è verificato un errore, prova a ricaricare la pagina ed a effettuare l\'ordine nuovamente.')));

        $CHIAVESEGRETA = $metodi_pagamento['stripe_data']['secret_key'];

        if($MSFrameworkCMS->getHTTPType() != 'https://') {
            \Stripe\Stripe::setVerifySslCerts(false);
        }

        \Stripe\Stripe::setApiKey($CHIAVESEGRETA);

        $importo = $orderDetails['plan_info']['prezzo_annuale']*100;

        $charge = \Stripe\Charge::create([
            'amount' => $importo,
            'currency' => 'EUR',
            'description' => 'Pagamento per Ordine #' . $order_id,
            'source' => $request['id'],
            'receipt_email' => $request['email'],
        ]);

        //retrieve charge details
        $chargeJson = $charge->jsonSerialize();

        //check whether the charge is successful
        if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){

            $this->MSFrameworkDatabase->query("UPDATE subscriptions__orders SET payment_log = :plog WHERE id = :id", array(":plog" => json_encode($request), ":id" => $order_id));
            $orders->updateOrderStatus($order_id, 1);

            if(is_callable($on_success)) {
                $on_success($order_id);
            }

            die(json_encode(array('status' => 'ok')));

        } else {
            die(json_encode(array('status' => 'transaction_error', 'message' => 'Si è verificato un errore durante il pagamento, se il problema persiste per favore contatta l\'assistenza')));
        }
    }

}
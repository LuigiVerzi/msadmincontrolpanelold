<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

namespace MSFramework\Subscriptions\Payments;


class paypal {
    public function __construct() {
        Global $MSFrameworkDatabase;
        Global $MSFrameworki18n;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworki18n = $MSFrameworki18n;
    }

    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param $orderDetails array L'Array con le info relative all'ordine
     * @param $return_url string L'url di ritorno
     *
     * @return string La stringa con l'URL che porta al pagamento
     *
     */
    public function getPaymentButton($orderDetails, $return_url) {
        Global $MSFrameworkCMS, $MSFrameworki18n;

        $siteCMSData = (new \MSFramework\cms())->getCMSData('site');
        $site_logos = json_decode($siteCMSData['logos'], true);

        $loggedUser = (new \MSFramework\Ecommerce\customers())->getUserDataFromSession();

        $metodi_pagamento = (new \MSFramework\cms())->getCMSData('fatturazione_fatture');
        $metodi_pagamento = json_decode($metodi_pagamento['webPayments'], true);

        $prezzo_tassato = (new \MSFramework\Fatturazione\imposte())->getPriceToShow($orderDetails['plan_info']['prezzo_annuale'])['tax'];

        $html = '<form method="post" name="paypal_form" id="paypal_form" action="https://www' . (($MSFrameworkCMS->isStaging() || $MSFrameworkCMS->isDevelopment()) ? '.sandbox' : '') . '.paypal.com/cgi-bin/webscr">';
        $html .= '<input type="hidden" name="business" value="' . $metodi_pagamento['paypal_data']['email'] . '" />';
        $html .= '<input type="hidden" name="cmd" value="_xclick-subscriptions" />';
        $html .= '<input type="hidden" name="upload" value="1">';
        
        $html .= '<input type="hidden" name="image_url" value="' . (isset($_SERVER['HTTPS']) ? "https" : "http") . ":" . UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo'] .'">';

        $html .= '<input type="hidden" name="return" value="' . $return_url . '&return" />';
        $html .= '<input type="hidden" name="cancel_return" value="' . $return_url . '" />';

        $html .= '<input type="hidden" name="notify_url" value="' . $MSFrameworkCMS->getURLToSite() . 'ipn/abbonamenti/paypal_ipn.php'. '" />';
        $html .= '<input type="hidden" name="rm" value="2" />';
        $html .= '<input type="hidden" name="currency_code" value="EUR" />';
        $html .= '<input type="hidden" name="lc" value="it_IT" />';
        $html .= '<input type="hidden" name="cbt" value="Vedi il tuo ordine su ' . SW_NAME. '" />';

        $html .= '<input type="hidden" name="item_name" value="' . htmlspecialchars($MSFrameworki18n->getFieldValue($orderDetails['plan_info']['titolo'])). '">';
        //$html .= '<input type="hidden" name="item_number" value="' . $orderDetails['plan_info']['id'] . '">';
        $html .= '<input type="hidden" name="no_shipping" value="1">';

        if((new \MSFramework\Subscriptions\subscribers())->canUseTrial($orderDetails['user_id'], $orderDetails['plan_info']['id'])) {
            $html .= '<input type="hidden" name="a1" value="0">';
            $html .= '<input type="hidden" name="p1" value="' . (int)$orderDetails['plan_info']['trial_days'] . '">';
            $html .= '<input type="hidden" name="t1" value="D">';
        }

        $html .= '<input type="hidden" name="a3" value="' . floatval($prezzo_tassato) . '">';
        $html .= '<input type="hidden" name="p3" value="1">';
        $html .= '<input type="hidden" name="t3" value="Y">';

        $html .= '<input type="hidden" name="src" value="1">';
        $html .= '<input type="hidden" name="sra" value="1">';

        $html .= '<input type="hidden" name="custom" value="' . $orderDetails['id'] . '" />';

        $html .= '<input type="hidden" name="first_name" value="' . $orderDetails['info_spedizione']['nome'] . '"/>';
        $html .= '<input type="hidden" name="last_name" value="' . $orderDetails['info_spedizione']['cognome'] . '"/>';
        $html .= '<input type="hidden" name="address1" value="' . $orderDetails['info_spedizione']['indirizzo'] . '"/>';
        $html .= '<input type="hidden" name="address2" value="' . (!empty($orderDetails['info_spedizione']['indirizzo_2']) ? $orderDetails['info_spedizione']['indirizzo_2'] : ''). '"/>';
        $html .= '<input type="hidden" name="city" value="' . $orderDetails['info_spedizione']['comune'] . '"/>';
        $html .= '<input type="hidden" name="state" value="' . (new \MSFramework\geonames())->getCountryDetails($orderDetails['info_spedizione']['stato'])[$orderDetails['info_spedizione']['stato']]['name'] . '"/>';
        $html .= '<input type="hidden" name="zip" value="' . $orderDetails['info_spedizione']['cap'] . '"/>';
        $html .= '<input type="hidden" name="email" value="' . $loggedUser['username'] . '"/>';

        $html .= '<input type="image" src="' . ABSOLUTE_SW_PATH_HTML. 'assets/images/paypal_logo.gif" border="0" name="submit" alt="' . $this->MSFrameworki18n->gettext('Paga subito tramite PayPal'). '" />';

        $html .= '</form>';

        return $html;

    }
    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param array $request
     * @param mixed $on_success È possibie passare una funzione
     *
     */
    public function checkIPN($request, $on_success = false) {
        Global $MSFrameworkCMS;

        $orders = new \MSFramework\Subscriptions\orders();
        $ipn = new \PayPal\PaypalIPN();

        // Use the sandbox endpoint during testing.
        if(($MSFrameworkCMS->isStaging() || $MSFrameworkCMS->isDevelopment())) {
            $ipn->useSandbox();
        }

        $verified = $ipn->verifyIPN();

        if ($verified) {

            $receiver_email = filter_var($request['receiver_email'], FILTER_SANITIZE_EMAIL);
            $order_id = filter_var($request['custom'], FILTER_SANITIZE_STRING);

            $metodi_pagamento = (new \MSFramework\cms())->getCMSData('fatturazione_fatture');
            $metodi_pagamento = json_decode($metodi_pagamento['webPayments'], true);

            if ($receiver_email == $metodi_pagamento['paypal_data']['email']) {

                if ($request['txn_type'] === 'subscr_cancel') {
                    $orders->updateOrderStatus($order_id, 4);
                } else if ($request['txn_type'] === 'subscr_signup' && isset($request['mc_amount1']) && (int)$request['mc_amount1'] == 0) {

                    $this->MSFrameworkDatabase->query("UPDATE subscriptions__orders SET payment_log = :plog WHERE id = :id", array(":plog" => json_encode($request), ":id" => $order_id));
                    $orders->updateOrderStatus($order_id, 1);

                    if (is_callable($on_success)) {
                        $on_success($order_id);
                    }

                } else if($request['txn_type'] === 'subscr_payment') {

                    $dettagli_ordine = $orders->getOrderDetails($order_id);
                    $log_ordine_precedente = array();
                    if($dettagli_ordine) {
                        $log_ordine_precedente = json_decode($dettagli_ordine['payment_log']) ? json_decode($dettagli_ordine['payment_log'], true) : array();
                    }

                    $this->MSFrameworkDatabase->query("UPDATE subscriptions__orders SET payment_log = :plog, is_trial = 0 WHERE id = :id", array(":plog" => json_encode($request), ":id" => $order_id));
                    $orders->updateOrderStatus($order_id, 1);

                    if (is_callable($on_success)) {
                        $on_success($order_id);
                    }

                    if($log_ordine_precedente && isset($log_ordine_precedente['transaction_subject'])) {
                        $orders->createNewOrder($dettagli_ordine['user_id'], $dettagli_ordine['plan_id'], $dettagli_ordine['info_fatturazione'], $dettagli_ordine['info_spedizione'], $dettagli_ordine['coupons_used'], 1, 'paypal');
                    }

                }
            }
        }

        header("HTTP/1.1 200 OK");
    }
}
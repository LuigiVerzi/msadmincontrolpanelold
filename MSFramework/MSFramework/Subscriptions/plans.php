<?php
/**
 * MSFramework
 * Date: 15/02/18
 */

namespace MSFramework\Subscriptions;


class plans {
    public function __construct() {
        global $firephp;
        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Piano => Dati Piano) con i dati relativi al piano d'abbonamento.
     *
     * @param $id L'ID del piano (stringa) o dei piani (array) richieste (se vuoto, vengono recuperati i dati di tutti i piani d'abbonamento)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getPlanDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM subscriptions__plans WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {

            $page_gallery = json_decode($r['gallery'], true);
            if(is_array($page_gallery)) {

                $r['gallery_friendly'] = array();
                foreach($page_gallery as $file) {
                    $r['gallery_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SUBSCRIPTION_PLANS_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SUBSCRIPTION_PLANS_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SUBSCRIPTION_PLANS_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_SUBSCRIPTION_PLANS_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }

            }

            if($r['imposta'] == '') {
                $r['imposta'] = 0;
            } else if($r['imposta'] == 'default') {
                $r['imposta'] = '0';
                foreach((new \MSFramework\Fatturazione\imposte())->getImposte() as $value => $imposta) {
                    if($imposta[1] == "1") {
                        $r['imposta'] = $value;
                    }
                }
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }
}
<?php
/**
 * MSFramework
 * Date: 05/03/18
 */

namespace MSFramework;


class blocks {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Blocco => Dati Blocco) con i dati relativi al blocco.
     *
     * @param mixed $id L'ID del blocco (stringa) o dei blocchi (array) richieste (se vuoto, vengono recuperati i dati di tutti i blocchi)
     * @return array
     */
    public function getBlockDetails($id = "") {
        global $firephp, $MSFrameworki18n;

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }
            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM blocks WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }
}
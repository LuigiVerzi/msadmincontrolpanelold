<?php
/**
 * MSFramework
 * Date: 15/02/18
 */

namespace MSFramework\Camping;


class stagioni {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Stagione => Dati Stagione) con i dati relativi alla stagione.
     *
     * @param $id L'ID della stagione (stringa) o delle stagioni (array) richieste (se vuoto, vengono recuperati i dati di tutte le stagioni)
     *
     * @return array
     */
    public function getSeasonDetails($id = array()) {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }
        }

        $ary_to_return = array();

        foreach((new \MSFramework\cms())->getCMSData('camping_season') as $stagione) {

            if(isset($ary_to_return[$stagione['id']])) {
                $ary_to_return[$stagione['id']]['date'][] = array($stagione['inizio'], $stagione['fine']);
            }
            else {
                $ary_to_return[$stagione['id']] = array(
                    'id' => $stagione['id'],
                    'nome' => $this->getSeasonName($stagione['id']),
                    'note_aggiuntive' => (new \MSFramework\i18n())->getFieldValue($stagione['note_aggiuntive']),
                    'date' => array(
                        array($stagione['inizio'], $stagione['fine'])
                    )
                );
            }

        }

        return $ary_to_return;
    }

    /**
     * Restituisce il nome della stagione tramite l'ID inserito
     *
     * @param $id L'ID della stagione
     *
     * @return string
     */
    public function getSeasonName($id = 0) {
        return $this->getSeasonTypes()[$id];
    }

    /**
     * Restituisce la lista di stagioni disponibili
     *
     * @param $id L'ID della stagione
     *
     * @return string
     */
    public function getSeasonTypes() {

        $seasons = array(
            0 => "Bassa Stagione",
            1 => "Media Stagione",
            2 => "Alta Stagione",
            3 => "Promo"
        );

        return $seasons;
    }

    /**
     * Restituisce un array con le date di apertura e chiusura del campeggio
     *
     * @return array
     */
    public function getOpeningDates() {

        $stagioni = $this->getSeasonDetails();

        $opening_date = '';
        $closing_date = '';

        foreach($stagioni as $stagione) {
            foreach($stagione['date'] as $date) {
                if($date[0] < $opening_date || $opening_date == '') $opening_date = $date[0];
                if($date[1] > $closing_date || $closing_date == '') $closing_date = $date[1];
            }
        }

        $tmp_opening = date(date('Y') . '-m-d', $opening_date);
        $tmp_closing = date(date('Y') . '-m-d', $closing_date);

        if(strtotime($tmp_closing) < time()) {
            $opening_date = strtotime(date((date('Y')+1) . '-m-d', strtotime($tmp_opening)));
        }
        else {
            $opening_date = strtotime($tmp_opening);
        }

        if(strtotime($tmp_closing) < $opening_date) {
            $closing_date = strtotime(date((date('Y')+1) . '-m-d', strtotime($tmp_closing)));
        }
        else {
            $closing_date = strtotime($tmp_closing);
        }

        return array($opening_date, $closing_date);

    }
}
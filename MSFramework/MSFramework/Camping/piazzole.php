<?php
/**
 * MSFramework
 * Date: 15/02/18
 */

namespace MSFramework\Camping;


class piazzole {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Stanza => Dati Stanza) con i dati relativi alla piazzola.
     * L'array contiene (in una chiave addizionale 'gallery_friendly' aggiunta al volo) sia le informazioni da utilizzare nel DOM (HTML) che il path assoluto delle immagini
     *
     * @param $id L'ID della piazzola (stringa) o delle piazzole (array) richieste (se vuoto, vengono recuperati i dati di tutte le piazzole)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getPiazzolaDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM camping_piazzole WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $page_gallery = json_decode($r['gallery'], true);
            if(is_array($page_gallery)) {

                $r['gallery_friendly'] = array();
                foreach($page_gallery as $file) {
                    $r['gallery_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_CAMPING_PIAZZOLE_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_CAMPING_PIAZZOLE_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_CAMPING_PIAZZOLE_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_CAMPING_PIAZZOLE_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }

            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene l'URL della stanza
     *
     * @param $id L'ID della stanza per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl;

        if(is_array($id) && isset($id['slug'])) $page_det = $id;
        else $page_det = $this->getPiazzolaDetails($id, "slug")[$id];

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) .  "/";
    }
}
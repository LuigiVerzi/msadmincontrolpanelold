<?php
/**
 * MSAdminControlPanel
 * Date: 22/12/2018
 */

namespace MSFramework\Report\Output\XLS;

use \PhpOffice\PhpSpreadsheet\Chart\Chart;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use \PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use \PhpOffice\PhpSpreadsheet\Chart\Layout;
use \PhpOffice\PhpSpreadsheet\Chart\Legend;
use \PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use \PhpOffice\PhpSpreadsheet\Chart\Title;

class xls extends \PhpOffice\PhpSpreadsheet\Spreadsheet {
    public function __construct($reportParent, $reportItem) {
        if($reportParent == "" || $reportItem == "") {
            throw new \Exception('Impossibile determinare il report corrente!');
        }

        parent::__construct();

        $cat_details = (new \MSFramework\Report\report())->getAvailCategories($reportParent);

        $this->reportName = $cat_details['name'] . " > " . $cat_details['childrens'][$reportItem]['name'];
        $this->defaultFont = 'calibri';
        $this->defaultFontSize = 12;

        $this->getProperties()->setCreator('MarketingStudio')->setTitle($this->reportName);

        $this->setActiveSheetIndex(0);
        $this->getDefaultStyle()->getFont()->setName($this->defaultFont);
        $this->getDefaultStyle()->getFont()->setSize($this->defaultFontSize);
        $this->getDefaultStyle()->getFont()->setBold(false);

        $this->removeSheetByIndex($this->getIndex(
            $this->getSheetByName('Worksheet')
        ));

        $this->addNewSheet(substr($this->reportName, 0, 30)); //I nomi dei fogli possono avere una lunghezza di massimo 30 caratteri

        $this->setActiveSheetIndex(0);
    }

    /**
     * Inizializza una serie di impostazioni/comportamenti finali da lanciare appena viene terminato il report. Si occupa anche di eseguire l'output del file.
     */
    public function closeReport() {
        $name = preg_replace('/[\s]+/', '_', $this->reportName);
        $name = preg_replace('/[^a-zA-Z0-9_\.-]/', '', $name);

        $this->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $name . '.xlsx"');
        header('Cache-Control: max-age=0');
        header('Set-Cookie: fileDownload=true; path=/'); //utilizzato dal plugin per capire che il download è pronto

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this, 'Xlsx');
        $writer->setIncludeCharts(true);
        $writer->save('php://output');
    }


    /**
     * Genera l'header del documento
     *
     * @param $columns L'array delle intestazioni peculiari del report (array(array("Nome colonna", "(int) larghezza colonna"))
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function Header($columns) {
        $this->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        $this->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->getActiveSheet()->setCellValue('A1', $this->reportName);

        $start_letter = "A";
        foreach($columns as $column) {
            $this->getActiveSheet()->setCellValue($start_letter . '4', $column[0]);
            $this->getActiveSheet()->getColumnDimension($start_letter)->setWidth($column[1]);
            $this->getActiveSheet()->getStyle($start_letter . '4')->getFont()->setSize(12);
            $this->getActiveSheet()->getStyle($start_letter . '4')->getFont()->setBold(true);
            $this->getActiveSheet()->getStyle($start_letter . '4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $start_letter++;
        }

        $this->getActiveSheet()->freezePane('A5');
    }

    /**
     * Aggiunge un nuovo foglio al file
     *
     * @param $name Il nome del foglio
     * @param bool $setActive Se impostato su true, rende il foglio appena aggiunto attivo
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function addNewSheet($name, $setActive = true) {
        $c = $this->getSheetCount();

        $this->addSheet(new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($this, $name), $c);

        if($setActive) {
            $this->setActiveSheetIndex($c);
        }
    }

    /**
     * Aggiunge un grafico a torta nel foglio attivo
     *
     * @param $graph_title Il titolo del grafico
     * @param $data_origin Un array contenente l'origine dei dati, composto come segue: array(
                'sheet_name' => 'Il nome del foglio in cui sono presenti i dati',
                'label' => array('$A$5', '$A$7'), //colonna/riga da - colonna/riga a (per la label)
                'data' => array('$B$5', '$B$7') //colonna/riga da - colonna/riga a (per i dati)
                ),
     * @param $position array(<posizione grafico top left>, <posizione grafico bottom right>)
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function addPieChart($graph_title, $data_origin, $position) {
        Global $MSFrameworkUrl;
        $dataSeriesValues = [
            new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, '\'' . $data_origin['sheet_name'] . '\'!' . $data_origin['data'][0] . ':' . $data_origin['data'][1]),
        ];

        $series = new DataSeries(
            DataSeries::TYPE_PIECHART, // plotType
            null, // plotGrouping (Pie charts don't have any grouping)
            range(0, count($dataSeriesValues) - 1), // plotOrder
            [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, '\'' . $data_origin['sheet_name'] . '\'!' . $data_origin['label'][0] . ':' . $data_origin['label'][1]),
            ],
            [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, '\'' . $data_origin['sheet_name'] . '\'!' . $data_origin['label'][0] . ':' . $data_origin['label'][1]),
            ],
            $dataSeriesValues
        );

        $layout = new Layout();
        $layout->setShowVal(true);
        $layout->setShowPercent(true);

        $chart1 = new Chart(
            $MSFrameworkUrl->cleanString($graph_title),
            new Title($graph_title),
            new Legend(Legend::POSITION_RIGHT, null, false),
            new PlotArea($layout, [$series]),
            true,
            0,
            null,
            null
        );

        $chart1->setTopLeftPosition($position[0]);
        $chart1->setBottomRightPosition($position[1]);

        $this->getActiveSheet()->addChart($chart1);
    }

}
<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

namespace MSFramework\Report\Output\PDF;

class pdf extends \Interpid\PdfLib\Pdf {
    public function __construct($reportParent, $reportItem, $orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false) {
        if($reportParent == "" || $reportItem == "") {
            throw new \Exception('Impossibile determinare il report corrente!');
        }

        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);

        $cat_details = (new \MSFramework\Report\report())->getAvailCategories($reportParent);

        $this->reportName = $cat_details['name'] . " > " . $cat_details['childrens'][$reportItem]['name'];
        $this->defaultFont = 'helvetica';
        $this->defaultFontSize = 8;

        $this->SetCreator('MarketingStudio');
        $this->SetAuthor(SW_NAME);
        $this->SetTitle($this->reportName);
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->SetHeaderMargin(15);
        $this->SetFooterMargin(5);
        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    }

    /**
     * Inizializza una serie di impostazioni/comportamenti preliminari da lanciare appena viene avviato il report
     */
    public function openReport() {
        $this->SetDefaultStyle();
        $this->AddPage();
    }

    /**
     * Inizializza una serie di impostazioni/comportamenti finali da lanciare appena viene terminato il report. Si occupa anche di eseguire l'output del file.
     *
     * @param string $dest Il tipo di output da eseguire
     */
    public function closeReport($dest = "D") {
        if($dest == "") {
            $dest = "D";
        }

        $this->lastPage();

        header('Set-Cookie: fileDownload=true; path=/'); //utilizzato dal plugin per capire che il download è pronto
        $this->Output($this->reportName . ".pdf", $dest);
    }

    /**
     * Genera l'header del documento
     */
    public function Header() {
        $this->SetFont($this->defaultFont, 'B', 10);
        $this->Cell(0, 15, $this->reportName, 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    /**
     * Genera il footer del documento
     */
    public function Footer() {
        $this->SetFont($this->defaultFont, '', 5);
        $this->Cell(0, 15, 'Report generato da MarketingStudio per ' . SW_NAME . '. Dati aggiornati al ' . date("d/m/Y") . ' ore ' . date("H:i"), 0, false, 'R', 0, '', 0, false, 'M', 'M');
    }

    /**
     * Calcola restituisce il valore numerico del width calcolandolo da un valore percentuale (es: il 25% della pagina, margini esclusi)
     *
     * @param $percent La percentuale da calcolare
     *
     * @return float|int
     */
    public function widthFromPercentage($percent) {
        $margins = $this->getMargins();

        return ($percent*($this->getPageWidth()-$margins['left']-$margins['right']))/100;
    }

    /**
     * Imposta i colori ed il font standard per l'header ed il footer delle tabelle
     */
    public function openTableHeaderFooter() {
        $this->SetFillColor(245, 245, 246);
        $this->SetFont($this->defaultFont, 'B', $this->defaultFontSize);
    }

    /**
     * Imposta i colori ed il font standard per il corpo della pagina
     */
    public function SetDefaultStyle() {
        $this->SetFillColor(255, 255, 255);
        $this->SetFont($this->defaultFont, '', $this->defaultFontSize);
    }

    /**
     * Aggiunge un grafico a torta nel PDF
     *
     * @param $data L'array contenente i dati del grafico. Es: array("data" => array(<dato1>, <dato2>))
     * @param null $x L'x di partenza del grafico (se null viene impostata la x attuale)
     * @param null $y L'y di partenza del grafico (se null viene impostata la y attuale)
     * @param int $radius Il raggio del grafico
     */
    public function addPieChart($data, $x = null, $y = null, $radius = 15) {
        if($x == null) {
            $x = $this->GetX();
        }

        if($y == null) {
            $y = $this->GetY();
        }

        $old_slices = 0;
        $slice_begin = 0;
        $sum = array_sum($data['data']);

        foreach($data['data'] as $typeK => $typeV) {
            $pie_slice = (360 * $typeV) / $sum;

            $this->SetFillColor(rand(0, 255), rand(0, 255), rand(0, 255));
            $this->PieSector($x, $y, $radius, $slice_begin, $pie_slice+$old_slices);

            $slice_begin = $old_slices-$pie_slice;
            $old_slices += $pie_slice;
        }
    }
}
<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

namespace MSFramework\Report;


class report {
    public function __construct() {
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce le categorie principali disponibili
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     * @param bool $limit_to_type Impostando questo parametro su "true", vengono restituite solo le categorie disponibili per la tipologia di sito corrente (default: true)
     */
    public function getAvailCategories($key = "", $limit_to_type = true) {
        /* Struttura di esempio

            "id_categoria" => array(
                "name" => "Nome Categoria",
                "descr" => "Descrizione Categoria",
                "fa-icon" => "fa-comments",
                "limit_to_functions" => array("beautycenter"),
                "childs" => array("")
            ),

        */

        $array = array(
            "fatturazione" => array(
                "name" => "Fatturazione",
                "descr" => "Categoria di report relativi alla fatturazione",
                "fa-icon" => "fa-calculator",
                "childrens" => array(
                    "fatturato" => array("name" => "Fatturato"),
                )
            ),
            "clienti" => array(
                "name" => "Clienti",
                "descr" => "Categoria di report relativi ai clienti",
                "fa-icon" => "fa-male",
                "childrens" => array(
                    "general" => array("name" => "Report clienti"),
                )
            ),
            "operatori" => array(
                "name" => "Sito & Utenti",
                "descr" => "Categoria di report relativi al sito ed agli utenti",
                "fa-icon" => "fa-male",
                "childrens" => array(
                    "general" => array("name" => "Report utenti"),
                    "work_hours" => array("name" => "Ore lavorate", "limit_to_functions" => array("working_hours")),
                )
            ),
            "ecommerce" => array(
                "name" => "Ecommerce",
                "descr" => "Categoria di report relativi agli ecommerce",
                "fa-icon" => "fa-money",
                "limit_to_functions" => array("ecommerce"),
                "childrens" => array(
                    "ordini" => array("name" => "Report ordini"),
                )
            ),
            "appointments" => array(
                "name" => "Appuntamenti",
                "descr" => "Categoria di report relativi agli appuntamenti",
                "fa-icon" => "fa-calendar",
                "limit_to_functions" => array("appointments"),
                "childrens" => array(
                    "general" => array("name" => "Report appuntamenti"),
                )
            )
        );

        if($limit_to_type) {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT value FROM `cms` WHERE type = 'producer_config'", array(), true);
            $r_producer_config = json_decode($r['value'], true);
            $tmp_ary = array();
            foreach($array as $cur_reportK => $cur_report) {
                if(!is_array($cur_report['limit_to_functions'])) {
                    $tmp_ary[$cur_reportK] = $cur_report;
                    continue;
                }

                if(count(array_intersect($cur_report['limit_to_functions'], json_decode($r_producer_config['extra_functions'], true))) > 0) {
                    $tmp_ary[$cur_reportK] = $cur_report;
                }
            }

            $array = $tmp_ary;
        }


        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    function getChildrenForCat($cat) {
        if($cat == "") { return false; }

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT value FROM `cms` WHERE type = 'producer_config'", array(), true);
        $r_producer_config = json_decode($r['value'], true);

        $to_return = array();
        foreach($this->getAvailCategories($cat)['childrens'] as $childK => $child) {
            if(!is_array($child['limit_to_functions'])) {
                $tmp_ary[$childK] = $child;
                continue;
            }

            if(count(array_intersect($child['limit_to_functions'], json_decode($r_producer_config['extra_functions'], true))) > 0) {
                $tmp_ary[$childK] = $child;
            }
        }

        return $tmp_ary;
    }
}
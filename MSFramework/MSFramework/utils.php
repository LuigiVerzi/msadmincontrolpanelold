<?php
/**
 * MSFramework
 * Date: 07/04/18
 */

namespace MSFramework;


class utils {

    /** Genera una tabella con gli shortcodes */
    public function generateShortcodesTable($specific_shortcodes, $include_global = false, $text = 'Questa pagina supporta l\'utilizzo di alcuni shortcodes') {

        $html = '<div id="shortcodesContainer" style="margin-bottom: 15px;">';
        $html .= '<div class="alert alert-info">' . $text . '<a href="#" class="pull-right btn btn-sm btn-default" onclick="$(\'.structureShortcodes\').toggle();" style="margin: -5px 0;"><i class="fa fa-code"></i> Mostra Shortcodes</a></div>';

        $html .= '<div class="structureShortcodes" style="margin-top: 15px; display: none;">
            <table class="table table-bordered small">
                <thead>
                    <tr>
                        <th style="background-color: #ececec;" colspan="2">Lista shortcodes Disponibili</th>
                    </tr>
                </thead>
                <tbody id="availableShortcodes">';

        foreach($specific_shortcodes as $shortcode_key => $shortcode_value) {
            $html .= '<tr><td width="210" style="background-color: #f4f4f4;">' . $shortcode_key . '</td><td>' . $shortcode_value . '</td></tr>';
        }

        if($include_global) {
            $shortcodes_globali = (new \MSFramework\emails())->getCommonShortcodes();
            $html .= '<tr><th colspan="2">Shortcodes Globali</th></tr>';
            foreach($shortcodes_globali[0] as $k => $shortcode) {
                $html .= '<tr><td width="210" style="background-color: #f4f4f4;">' . $shortcode . '</td><td>' . $shortcodes_globali[1][$k] . '</td></tr>';
            }
        }

        $html .= '</tbody>
            </table>
        </div>';
        $html .= '</div>';

        return $html;

    }

    /**
     * Formatta i valori Extra Fields dei vari moduli
     *
     * @param array/string $extra_fields L'array o il JSON contenente i valori degli extra fields
     *
     * @return array
     */
    public function formatExtraFieldsValues($extra_fields) {

        if(isset($extra_fields['imposta'])) {
            if($extra_fields['imposta'] == 'default') {
                $extra_fields['imposta'] = '0';
                foreach ((new \MSFramework\Fatturazione\imposte())->getImposte() as $value => $imposta) {
                    if ($imposta[1] == "1") {
                        $extra_fields['imposta'] = $value;
                    }
                }
            }
        }

        return $extra_fields;
    }

    /**
     * Restituisce un array contenente il path dei files trovati filtrandoli per l'estensione richiesta
     *
     * @param string $ext L'estensione (o le estensioni) da cercare (formato REGEX)
     * @param bool $backend_mode Se impostato su true cercherà le traduzioni nel Framework invece che sul front
     *
     * @return array
     */
    public function getFilesByExtension($ext = 'php|js|html', $backend_mode = false) {

        $files = array();

        $paths = ($backend_mode ?
            array(
                FRAMEWORK_ABSOLUTE_PATH . 'MSFramework/'
            ) :
            array(
                MAIN_SITE_FOLDERPATH
            )
        );

        foreach($paths as $path) {
            $rii = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
            $regexit = new \RegexIterator($rii, '/^.*\.(' . $ext . ')$/i');

            foreach ($regexit as $regexitV) {
                $files[] = $path . $regexit->getSubPathname();
            }
        }

        return $files;
    }

    /**
     * Ottiene la dimensione di una cartella
     * @param string $directory
     * @return integer
     */
    function dirSize($directory) {
        $size = 0;
        foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory)) as $file){
            $size+=$file->getSize();
        }
        return $size;
    }

    /**
     * Formatta una data restituendola in modo più bello da leggere
     *
     * @param string $timestamp Il timestamp della data da formattare
     *
     * @return string
     */
    public function smartdate($timestamp) {
        $diff = time() - $timestamp;

        if ($diff <= 0) {
            return 'Adesso';
        }
        else if ($diff < 60) {
            return $this->grammar_date(floor($diff), ' ' . (new \MSFramework\i18n())->gettext('second[o|i] fa'));
        }
        else if ($diff < 60*60) {
            return $this->grammar_date(floor($diff/60), ' ' . (new \MSFramework\i18n())->gettext('minut[o|i] fa'));
        }
        else if ($diff < 60*60*24) {
            return $this->grammar_date(floor($diff/(60*60)), ' ' . (new \MSFramework\i18n())->gettext('or[a|e] fa'));
        }
        else if ($diff < 60*60*24*30) {
            return $this->grammar_date(floor($diff/(60*60*24)), ' ' . (new \MSFramework\i18n())->gettext('giorn[o|i] fa'));
        }
        else if ($diff < 60*60*24*30*12) {
            return $this->grammar_date(floor($diff/(60*60*24*30)),  ' ' . (new \MSFramework\i18n())->gettext('mes[e|i] fa'));
        }
        else {
            return $this->grammar_date(floor($diff/(60*60*24*30*12)), ' ' . (new \MSFramework\i18n())->gettext('ann[o|i] fa'));
        }
    }

    public function grammar_date($val, $sentence) {

        $pattern = '/(\[(.)\|(.)]+)/m';

        if ($val > 1) {
            return $val.preg_replace($pattern, '$3', $sentence);
        } else {
            return $val.preg_replace($pattern, '$2', $sentence);
        }
    }

    /**
     * Specifica se i valori di un array sono in crescita, in diminuzione o in pareggio
     *
     * @param array $array L'array con i valori da contollare
     *
     * @return integer
     */
    public function getArrayValuesTrend($array) {

        $array_diviso = array_chunk($array, ceil(count($array) / 2));

        if(array_sum($array_diviso[1]) > array_sum($array_diviso[0])) return 1;
        else if(array_sum($array_diviso[1]) < array_sum($array_diviso[0])) return 0;
        else return 2;
    }

    /**
     * Converte un colore HEX a RGBA
     *
     * @param string $color Il colore in HEX
     * @param mixed $opacity Il valore di trasparenza
     *
     * @return integer
     */
    public function hex2rgba($color, $opacity = false) {

        $default = 'rgb(0,0,0)';

        //Return default if no color provided
        if(empty($color))
            return $default;

        //Sanitize $color if "#" is provided
        if ($color[0] == '#' ) {
            $color = substr( $color, 1 );
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
            $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
            $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
            return $default;
        }

        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if($opacity){
            if(abs($opacity) > 1) $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
    }

    /**
     * Ottiene la luminosità di un colore HEX
     *
     * @param string $hex Il colore in Hex
     *
     * @return integer
     */
    function getHexLightness($hex) {
        $hex       = str_replace('#', '', $hex);
        $r         = (hexdec(substr($hex, 0, 2)) / 255);
        $g         = (hexdec(substr($hex, 2, 2)) / 255);
        $b         = (hexdec(substr($hex, 4, 2)) / 255);
        $lightness = round((((max($r, $g, $b) + min($r, $g, $b)) / 2) * 100));
        return $lightness;
    }

    /**
     *
     * @get text between tags
     *
     * @param string $tag The tag name
     *
     * @param string $html The XML or XHTML string
     *
     * @param int $strict Whether to use strict mode
     *
     * @return array
     *
     */
    public function getTextBetweenTags($tag, $html, $strict=0)
    {
        /*** a new dom object ***/
        $dom = new \domDocument;

        /*** load the html into the object ***/
        if($strict==1)
        {
            $dom->loadXML($html);
        }
        else
        {
            $dom->loadHTML($html);
        }

        /*** discard white space ***/
        $dom->preserveWhiteSpace = false;

        /*** the tag by its tag name ***/
        $content = $dom->getElementsByTagname($tag);

        /*** the array to return ***/
        $out = array();
        foreach ($content as $item)
        {
            /*** add node value to the out array ***/
            $out[] = $item->nodeValue;
        }
        /*** return the results ***/
        return $out;
    }

    /**
     * Cerca di ottenere il link embed tramite l'URL del video. Ritorna una stringa vuota se il link non è valido
     *
     * @param string $url L'url del video (Youtube, Vimeo, etc...)
     * @param bool $autoplay Se impostato su TRUE aggiunge i parametri di autoplay
     *
     * @return string Il link del video embed
     */
    public function getEmbedVideoUrl($url, $autoplay = false) {

        $host_accettati = array(
            'www.youtube.com'
        );

        $parsed_url = parse_url($url);

        $url_embed = '';
        if(in_array($parsed_url['host'], $host_accettati)) {

            if(strpos($parsed_url['path'], 'embed') !== false) {
                $url_embed = $url;
            }
            else if($parsed_url['path'] == '/watch') {
                $url_embed = (SSL_ENABLED ? "https" : "http") . '://www.youtube.com/embed/' . str_replace('v=', '', $parsed_url['query']);
                if($autoplay) {
                    $origin = (SSL_ENABLED ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];
                    $url_embed .= '?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&mute=1&enablejsapi=1&origin=' . $origin . '&playlist=' . str_replace('v=', '', $parsed_url['query']);
                }
            }

        }

        return $url_embed;

    }

    public function getUserIPAddr() {
        if(isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if(isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        } else if(isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if(isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } else if(isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = '';
        }

        return array($ipaddress, $this->getIPAddrDetails($ipaddress));
    }

    private function getIPAddrDetails($ip) {
        if($ip == "") {
            return false;
        }

        //Ho deciso di ottenere i dati geografici degli IP senza appoggiarmi a servizi di terzi (gratuiti con limitazioni o a pagamento)
        //I DB gratuiti di GeoIP2 possono essere scaricati da http://dev.maxmind.com/geoip/geoip2/geolite2/
        //Docs delle API usate: https://github.com/maxmind/GeoIP2-php
        require(FRAMEWORK_ABSOLUTE_PATH . 'Phar/geoip2/geoip2.phar');
        $reader = new \GeoIp2\Database\Reader(FRAMEWORK_ABSOLUTE_PATH . 'Phar/geoip2/GeoLite2-City.mmdb');

        $record = $reader->city($ip);

        return array("name" => $record->city->name, "lat" => $record->location->latitude, "lng" => $record->location->longitude);
    }

    /** function array_filter_recursive
     *
     *      Exactly the same as array_filter except this function
     *      filters within multi-dimensional arrays
     *
     * @param array
     * @param string optional callback function name
     * @param bool optional flag removal of empty arrays after filtering
     * @return array merged array
     */
    public function array_filter_recursive($array, $callback = null, $remove_empty_arrays = false)
    {
        foreach ($array as $key => & $value) { // mind the reference
            if (is_array($value)) {
                $value = $this->array_filter_recursive($value, $callback, $remove_empty_arrays);
                if ($remove_empty_arrays && !(bool) $value) {
                    unset($array[$key]);
                }
            } else if (!(bool) $value) {
                unset($array[$key]);
            }
        }
        unset($value);
        return $array;
    }

}
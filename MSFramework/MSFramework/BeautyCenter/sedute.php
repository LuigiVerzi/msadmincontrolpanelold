<?php
/**
 * MSFramework
 * Date: 08/03/18
 */

namespace MSFramework\BeautyCenter;


class sedute {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Recupera il numero di sessioni rimanenti
     *
     * @param string $id L'ID del cliente.
     * @param string $type Tipo+ID del servizio da ottenere
     *
     * @return array
     */
    public function getCustomerSessionsBalance($id, $type = "") {
        $bilancio = array();

        $sedute = $this->MSFrameworkDatabase->getAssoc("SELECT SUM(valore) as sedute, ref, tipo FROM beautycenter_sedute WHERE cliente = :id GROUP by tipo", array(":id" => $id));
        if($sedute) {
            $bilancio = $sedute;
        }

        if($type != "") {
            foreach ($sedute as $seduta) {
                if ($seduta['tipo'] == $type) {
                    return $seduta['sedute'];
                }
            }

            return 0;
        } else {
            return $bilancio;
        }
    }

    /**
     * Aggiunge o rimuove sessioni al cliente
     *
     * @param string $id L'ID del cliente.
     * @param float $value Il valore del numero di sessioni da aggiungere/rimuovere (Può essere sia positivo che negativo)
     * @param string $type La tipologia della sessione (service|offer)
     * @param array $ref L'array dell'oggetto Servizio/Offerta selezionata
     * @param string $note Eventuali note da allegare
     *
     * @return mixed
     */
    public function updateSessionsQuantity($id, $value, $type = "service", $ref = array(), $note = "") {

        $array_to_save = array(
            "cliente" => $id,
            "valore" => $value,
            "tipo" => $type . '_' . $ref['id'],
            "ref" => json_encode($ref),
            "note" => $note
        );

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $result = $this->MSFrameworkDatabase->pushToDB("INSERT INTO beautycenter_sedute ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

        return $result;
    }
}
<?php
/**
 * MSFramework
 * Date: 08/03/18
 */

namespace MSFramework\BeautyCenter;


class offers {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce tutte le tipologie di offerte disponibili
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getTypes($key = "") {
        $types = array(
            "sconto" => "Sconto",
            "pacchetto" => "Pacchetto",
            "convenzione" => "Convenzione"
        );

        if($key != "") {
            return $types[$key];
        } else {
            return $types;
        }
    }

    /**
     * Restituisce un array associativo (ID Offerta => Dati Offerta) con i dati relativi all'offerta.
     * L'array contiene (in una chiave addizionale 'gallery_friendly' aggiunta al volo) sia le informazioni da utilizzare nel DOM (HTML) che il path assoluto delle immagini
     *
     * @param $id L'ID dell'offerta (stringa) o delle offerte (array) richieste (se vuoto, vengono recuperati i dati di tutte le offerte)
     * @param string $exclude_expired Se impostato su "true" esclude le offerte scadute
     * @param string $fields I campi da prelevare dal DB
     * @param string $exclude_unactive Se impostato su "true" esclude le offerte non attive
     *
     * @return array
     */
    public function getOfferDetails($id = "", $exclude_expired = true, $fields = "*", $exclude_unactive = true) {

        if($id != "") {

            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = "";
        $append_where_ary = array();
        if($exclude_expired) {
            $append_where .= " AND scadenza >= '" . time() . "' OR scadenza = '' OR auto_renew_promo > 0";
        }

        if($exclude_unactive) {
            $append_where .= " AND active = '1'";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM beautycenter_offerte WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ")", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {
            $page_gallery = json_decode($r['gallery'], true);
            if(is_array($page_gallery)) {

                $r['gallery_friendly'] = array();
                foreach($page_gallery as $file) {
                    $r['gallery_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_BEAUTYOFFERSGALLERY_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_BEAUTYOFFERSGALLERY_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_BEAUTYOFFERSGALLERY_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_BEAUTYOFFERSGALLERY_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }

            }

            if(isset($r['scadenza']) && $r['scadenza'] < time() && $r['auto_renew_promo'] > 0)
            {
                // Continua ad aggiungere giorni alla data di scadenza fino a quando la scadenza sarà posteriore.
                for($new_offer_date = $r['scadenza']; $new_offer_date < time(); ) {
                    $new_offer_date += (86400 * $r['auto_renew_promo']);
                }
                $r['scadenza'] = $new_offer_date;
            }

            if(json_decode($r['extra_fields'])) {
                $r['extra_fields'] = (new \MSFramework\utils())->formatExtraFieldsValues(json_decode($r['extra_fields'], true));
            }
            else $r['extra_fields'] = array();

            $replaced_val = str_replace(array('€', '%', CURRENCY_SYMBOL), array('', '', ''), $r['prezzo']);

            if($r['type'] != 'sconto') {
                if (isset($r['extra_fields']['imposta'])) {
                    $r['prezzo_formattato'] = (new \MSFramework\Fatturazione\imposte())->getPriceToShow($replaced_val, $r['extra_fields']['imposta']);
                } else {
                    $r['prezzo_formattato'] = (new \MSFramework\Fatturazione\imposte())->getPriceToShow($replaced_val, 0);
                }
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }


    /**
     * Ottiene l'URL dell'offerta
     *
     * @param $id L'ID dell'offerta per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl;

        if(is_array($id)) {
            $page_det = $id;
        } else {
            $page_det = $this->getOfferDetails($id, false, "slug")[$id];
        }

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";
    }
}
<?php
/**
 * MSFramework
 * Date: 23/09/18
 */

namespace MSFramework\Blog;


class categories {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria
     *
     * @param string $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryDetails($id = "", $fields = "*")
    {
        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }
        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        $category = $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM blog_categories WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);
        foreach ((array)$category as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria
     *
     * @param integer $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     *
     * @return array
     */
    public function getCategoryChildrens($parent_id)
    {

        $category_order = (new \MSFramework\cms())->getCMSData('blog_cats_order')['list'];

        $children_ids = array();
        foreach ($category_order as $parent) {
            if ($parent_id == 0 || $parent['id'] == $parent_id) {
                foreach ($parent['children'] as $cat_child) {
                    $children_ids[] = $cat_child['id'];
                }
            }
        }

        $children_categories = array();
        if($children_ids) {
            $children_categories = $this->getCategoryDetails($children_ids);
        }

        return $children_categories;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria genitore
     *
     * @param string $id L'ID della categoria figlia
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryParent($children_id, $fields = "*")
    {

        $category_order = (new \MSFramework\cms())->getCMSData('blog_cats_order')['list'];

        $parent_id = 0;
        foreach ($category_order as $parent) {
            foreach($parent['children'] as $children) {
                if ($children['id'] == $children_id) {
                   $parent_id = $parent['id'];
                }
            }
        }

        $category = ($parent_id != 0 ? $this->getCategoryDetails($parent_id, $fields)[$parent_id] : array());

        return $category;
    }

    /**
     * Restituisce (boolean) se la categoria è genitore o no
     *
     * @param string $id L'ID della categoria da controllare
     *
     * @return boolean
     */
    public function isCategoryParent($id)
    {

        $category_order = (new \MSFramework\cms())->getCMSData('blog_cats_order')['list'];

        foreach ($category_order as $parent) {
            if ($parent['id'] == $id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Restituisce la struttura padre/figlio del menu in formato HTML.
     *
     * @param string $menu_type Il tipo di HTML desiderato (accetta: select)
     * @param string $active_id L'ID della categoria attualmente in uso (o da impostare come attiva/selezionata)
     * @param string $ary_menu L'array contenente le voci del menu per il quale effettuare il parsing. Se vuoto, viene recuperato tutto il menu
     *
     * @return string
     */
    public function composeHTMLMenu($menu_type = "select", $active_id = "", $ary_menu = "", $html_struct = "")
    {
        global $MSFrameworki18n, $ind_level, $firephp;

        if (!is_array($ary_menu)) {
            $ary_menu = (new \MSFramework\cms())->getCMSData('blog_cats_order')['list'];
        }

        $html = "";

        if ($menu_type == "select") {
            if ($ind_level == "") {
                $ind_level = 1;
            }

            if (!is_array($active_id)) {
                $active_id = array($active_id);
            }

            foreach ($ary_menu as $cur_menu) {
                $nbsp = "";
                for ($x = 1; $x < $ind_level; $x++) {
                    $nbsp .= "&nbsp;";
                }

                $set_selected = (in_array($cur_menu['id'], $active_id) ? "selected" : "");

                $html .= '<option value="' . $cur_menu['id'] . '"' . $set_selected . (is_array($cur_menu['children']) ? ' disabled' : '') . '>' . $nbsp . " - " . $MSFrameworki18n->getFieldValue($this->getCategoryDetails($cur_menu['id'], "nome")[$cur_menu['id']]['nome'], true) . '</option>';

                if (is_array($cur_menu['children'])) {
                    $ind_level = $ind_level * 3;
                    $html .= $this->composeHTMLMenu($menu_type, $active_id, $cur_menu['children']);
                }

                if ($ind_level > 0) {
                    $ind_level = $ind_level / 3;
                }
            }
        } else if($menu_type = "navigation") {

            if(!is_array($html_struct)) { //imposto un default nel caso in cui $html_struct non è un array
                $html_struct = array(
                    "main_element" => "<li>",
                    "main_element_append_class" => "",
                    "main_element_active_class" => "current-menu-item",
                    "sub_element" => "<ul>",
                    "sub_element_append_class" => "dropdown-menu",
                    "arrow_down" => '<span class="fa fa-caret-down"></span>',
                    "arrow_right" => '<span class="fa fa-caret-right"></span>',
                );
            }
            foreach ($ary_menu as $cur_menu) {

                $caret = "";
                if (is_array($cur_menu['children'])) {
                    $caret = $html_struct['arrow_down'];
                }

                $category_info = $this->getCategoryDetails($cur_menu['id'])[$cur_menu['id']];
                $menu_colors = json_decode($category_info['menu_colors'], true);

                $menu_bgcolor = ($menu_colors['bgColor'] != '') ? " background-color: " . $menu_colors['bgColor'] . "; " : "";
                $menu_color = ($menu_colors['color'] != '') ? " color: " . $menu_colors['color'] . "; " : "";
                $menu_mouseover = ($menu_colors['mouseover'] != '') ? ' onmouseover="this.style.backgroundColor=\'' . $menu_colors['mouseover'] . '\';" onmouseout="this.style.backgroundColor=\'' . $menu_colors['bgColor'] . '\';"' : "";

                $html_childrens = "";
                $childrens_data = array();
                if(is_array($cur_menu['children'])) {
                    $childrens_data = $this->_getChildren($cur_menu['children'], $html_struct, $active_id, false, $category_info);
                    $html_childrens = $childrens_data[0];
                }

                $set_page_active = "";
                if($active_id == $cur_menu['id'] || $childrens_data[1] === true) {
                    $set_page_active = " " . $html_struct['main_element_active_class'];
                }

                if($html_childrens != "") {
                    $html_struct['main_element_append_class'] .= ' dropdown';
                }

                $main_element = str_replace(">", " class='" . $html_struct['main_element_append_class'] . $set_page_active . "'>", $html_struct['main_element']);

                $html .= $main_element . '<a href="' . ABSOLUTE_SW_PATH_HTML . $MSFrameworki18n->getFieldValue($category_info['slug']) . '" style="' . $menu_bgcolor . $menu_color . '" ' . $menu_mouseover . '>' . $MSFrameworki18n->getFieldValue($category_info['nome']) . ' ' . $caret . '</a>';

                if($html_childrens != "") {
                    $html .= $html_childrens;
                }

                $html .= str_replace("<", "</", $html_struct['main_element']);

            }

        }

        return $html;
    }

    private function _getChildren($children, $html_struct, $current_active_page, $set_parent_active = false, $parent_info = array()) {
        global $firephp, $MSFrameworki18n;

        $sub_element = $html_struct['sub_element'];
        if($html_struct['sub_element_append_class'] != "") {
            $sub_element = str_replace(">", " class='" . $html_struct['sub_element_append_class'] . "'>", $html_struct['sub_element']);
        }
        $html = $sub_element;

        foreach($children as $cur_children) {
            $caret = "";
            if(is_array($cur_children['children'])) {
                $caret = $html_struct['arrow_right'];
            }

            if($set_parent_active !== true) {
                if($current_active_page == $cur_children['id']) {
                    $set_parent_active = true;
                }
            }

            $category_info = $this->getCategoryDetails($cur_children['id'])[$cur_children['id']];
            $menu_colors = json_decode($category_info['menu_colors'], true);
            $menu_bgcolor = ($menu_colors['bgColor'] != '') ? " background-color: " . $menu_colors['bgColor'] . "; " : "";
            $menu_color = ($menu_colors['color'] != '') ? " color: " . $menu_colors['color'] . "; " : "";
            $menu_mouseover = ($menu_colors['mouseover'] != '') ? ' onmouseover="this.style.backgroundColor=\'' . $menu_colors['mouseover'] . '\';" onmouseout="this.style.backgroundColor=\'' . $menu_colors['bgColor'] . '\';"' : "";

            $html .= $html_struct['main_element'] . '<a href="' . ABSOLUTE_SW_PATH_HTML . $MSFrameworki18n->getFieldValue($parent_info['slug']) . '?cat=' . $cur_children['id'] . '" style="' . $menu_bgcolor . $menu_color . '" ' . $menu_mouseover . '>' . $MSFrameworki18n->getFieldValue($category_info['nome']) . ' ' . $caret . '</a>';

            if(is_array($cur_children['children'])) {
                $html .= $this->_getChildren($cur_children['children'], $html_struct, $current_active_page, $set_parent_active, $parent_info)[0];
            }

            $html .= str_replace("<", "</", $html_struct['main_element']);
        }

        $html .= str_replace("<", "</", $html_struct['sub_element']);

        return array($html, $set_parent_active);
    }

    /**
     * Restituisce la struttura della paginazione in HTML
     *
     * @param int $total_products Il numero di articoli totali ottenuti
     * @param int $current_page La pagina che si sta mostrando attualmente
     * @param int $ary_menu Il numero di articoli che vengono mostrati per pagina
     *
     * @return string
     */
    public function composeHTMLPagination($current_query, $total_products, $current_page = 1, $show_per_page = 12)
    {
        $links = 10;

        $last = ceil($total_products / $show_per_page);

        $start = (($current_page - $links) > 0) ? $current_page - $links : 1;
        $end = (($current_page + $links) < $last) ? $current_page + $links : $last;

        $html = '<div class="pagination">';

        $class = ($current_page == 1) ? "disabled" : "";
        $html .= '<a class="' . $class . '" href="?' . $this->composeURLFilters($current_query, 'page', $current_page - 1) . '">&laquo;</a>';

        if ($start > 1) {
            $html .= '<a href="?page=1">1</a>';
            $html .= '<a class="disabled"><span>...</span></a>';
        }

        for ($i = $start; $i <= $end; $i++) {
            $class = ($current_page == $i) ? "active" : "";
            $html .= '<a class="' . $class . '" href="?' . $this->composeURLFilters($current_query, 'page', $i) . '">' . $i . '</a>';
        }

        if ($end < $last) {
            $html .= '<a class="disabled"><span>...</span></a>';
            $html .= '<a href="?' . $this->composeURLFilters($current_query, 'page', $last) . '">' . $last . '</a>';
        }

        $class = ($current_page == $last) ? "disabled" : "";
        $html .= '<a class="' . $class . '" href="?' . $this->composeURLFilters($current_query, 'page', $current_page + 1) . '">&raquo;</a>';

        $html .= '</div>';

        return $html;

    }

    /**
     * Restituisce la struttura della paginazione in HTML
     *
     * @param string $key Il parametro da modificare/aggiungere
     * @param string $value Il valore del parametro da modificare/aggiungere
     *
     * @return string
     */
    public function composeURLFilters($query, $key, $value)
    {
        $query[$key] = $value;
        $query_result = http_build_query($query);

        return urldecode($query_result);

    }


    /**
     * Restituisce un Array con tutti i filtri attivi
     *
     * @return array
     */
    public function getActiveFilters($query)
    {

        $active_filters = array();

        foreach ($query as $param => $val) {
           if ($param == 'cat') {
                $category = $this->getCategoryDetails($val);
                if (isset($category[$val])) {
                    $custom_query = $query;
                    unset($custom_query['cat']);

                    $active_filters['cat'] = array(
                        'nome' => $category[$val]['nome'],
                        'href' => urldecode(http_build_query($custom_query))
                    );
                }
            }
        }
        return $active_filters;
    }

    /**
     * Ottiene l'URL della categoria
     *
     * @param $id L'ID della pagina per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl, $MSFrameworkCMS;

        if(is_array($id)) {
            $page_det = $id;
        }
        else {
            $page_det = $this->getCategoryDetails($id, "slug")[$id];
        }

        return$MSFrameworkCMS->getURLToSite() . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";
    }
}
<?php
/**
 * MSFramework
 * Date: 23/09/18
 */

namespace MSFramework\Blog;


class posts {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        
        $this->only_language = true;
        $this->current_language_flag = (new \MSFramework\i18n())->getLanguagesDetails(USING_LANGUAGE_CODE)[USING_LANGUAGE_CODE]['long_code'];
    }

    /**
     * Restituisce un array associativo (ID Post => Dati Post) con i dati relativi al post
     *
     * @param string $id L'ID del post (stringa) o dei post (array) richiesti (se vuoto, vengono recuperati i dati di tutti i post)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getPostDetails($id = "", $fields = "*")
    {
        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }
        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        $category = $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM blog_posts WHERE active = 1 AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);
        foreach ((array)$category as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Questa funzione permette di recuperare i dati dell'articolo dal database
     *
     * @param string $id L'ID dell'articolo
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getBlogArticleByID($id, $fields = "*") {
        return $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . " FROM blog_posts WHERE active = 1 AND id = :id", array(":id" => $id), true);
    }

    /**
     * Questa funzione permette di recuperare i dati dell'articolo dal database (Usando lo slug)
     *
     * @param string $slug Lo slug dell'articolo
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getBlogArticleBySlug($slug, $fields = "*") {
        return $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . " FROM blog_posts WHERE active = 1 AND slug = :slug", array(":slug" => $slug), true);
    }

    /**
     * Questa funzione permette di recuperare i dati degli articoli tramite l'ID della categoria
     *
     * @param string $id L'id della categoria
     * @param integer $start L'offset di partenza
     * @param integer $limit Il numero massimo di articoli da mostrare
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getBlogArticleByCategoryID($id, $start = 0, $limit = 3, $fields = "*") {
        if($this->only_language) $language_where = 'AND slug LIKE \'%"' . $this->current_language_flag . '"%\'';
        else $language_where = '';
        
        return $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . " FROM blog_posts WHERE active = 1 AND (category = :categoria OR FIND_IN_SET(:categoria2, category)) $language_where ORDER BY data_creazione desc LIMIT :start, :limit", array(":categoria" => $id, ":categoria2" => $id, ':start' => $start, ':limit' => $limit));
    }

    /**
     * Questa funzione permette di contare il numero di articoli presenti all'interno di una categoria
     *
     * @param string $id L'id della categoria
     *
     * @return mixed
     */
    public function countBlogArticleByCategoryID($id) {
        if($this->only_language) $language_where = 'AND slug LIKE \'%"' . $this->current_language_flag . '"%\'';
        else $language_where = '';
        return $this->MSFrameworkDatabase->getCount("SELECT * FROM blog_posts WHERE active = 1 AND (category = :categoria OR FIND_IN_SET(:categoria2, category)) $language_where", array(":categoria" => $id, ":categoria2" => $id));
    }

    /**
     * Questa funzione permette di recuperare gli ultimi articoli inseriti
     *
     * @param integer $limit Il numero massimo di articoli da mostrare
     * @param integer $start L'offset da cui partire
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getBlogLastArticles($limit = 3, $start = 0, $fields = "*") {
        if($this->only_language) $language_where = 'AND slug LIKE \'%"' . $this->current_language_flag . '"%\'';
        else $language_where = '';
        return $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . " FROM blog_posts WHERE active = 1 $language_where ORDER BY id DESC LIMIT :start, :limit", array(':start' => $start, ':limit' => $limit));
    }


    /**
     * Questa funzione permette di recuperare i dati degli articoli tramite un tag
     *
     * @param mixed $tags Un array o una stringa con la lista di tag da cercare
     * @param integer $limit Il numero massimo di articoli da mostrare
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getBlogSimilarArticleByTags($tags, $limit = 3, $fields = "*") {
        if($this->only_language) $language_where = 'AND slug LIKE \'%"' . $this->current_language_flag . '"%\'';
        else $language_where = '';
        
        if(is_array($tags)) $sql_tags = implode(' ', $tags);
        else $sql_tags = str_replace(',', ' ', $tags);

        return $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . " FROM blog_posts WHERE active = 1 AND MATCH(tag) AGAINST('" . $sql_tags . "' IN BOOLEAN MODE) ORDER BY MATCH(tag) AGAINST('" . $sql_tags . "' IN BOOLEAN MODE) DESC LIMIT :limit", array(':limit' => $limit));
    }

    /**
     * Questa funzione permette di recuperare gli articoli più letti
     *
     * @param integer $limit Il numero massimo di articoli da mostrare
     * @param integer $start L'offset da cui partire
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getMostReadArticles($limit = 5, $start = 0, $fields = "*") {
        if($this->only_language) $language_where = 'AND slug LIKE \'%"' . $this->current_language_flag . '"%\'';
        else $language_where = '';
        return $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . ", (SELECT SUM(n_visite) FROM `blog_stats` WHERE blog_stats.article_id = blog_posts.id) as n_visits, (SELECT AVG(tempo_medio) FROM `blog_stats` WHERE blog_stats.article_id = blog_posts.id AND blog_stats.tempo_medio > 0) as watch_time FROM blog_posts WHERE active = 1 $language_where HAVING watch_time IS NOT NULL ORDER BY n_visits DESC LIMIT :start, :limit", array(':start' => $start, ':limit' => $limit));
    }

    /**
     * Ottiene l'URL della categoria
     *
     * @param $id L'ID della pagina per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl, $MSFrameworkCMS;

        if(is_array($id)) {
            $page_det = $id;
        }
        else {
            $page_det = $this->getBlogArticleByID($id, "slug");
        }

        return $MSFrameworkCMS->getURLToSite() . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";
    }


}
<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

namespace MSFramework\Affiliations;

class emails extends \MSFramework\emails {
    /**
     * Ottiene la lista di template per l'ecommerce
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {

        $default_address = $this->MSFrameworkCMS->getCMSData("settings")['destination_address'];

        $withdrawals_shortcodes = array(
            array(
                "{nome}" => "Il nome del cliente",
                "{email}" => "L'email del cliente",
                "{valore}" => "Il valore della richiesta di prelievo",
                "{metodo_pagamento}" => "Il metodo di pagamento",
                "{note}" => "Le note della richiesta"
            ),
            array(
                "{[cliente][nome]}",
                "{[cliente][email]}",
                "{[valore]}" . CURRENCY_SYMBOL,
                "{[metodo_pagamento]}",
                "{[note]}"
            )
        );


        $templates = array(
            'affiliations' => array(
                /* TEMPLATE STATO ABBONAMENTI */
                'nuovo-utente-sponsorizzato' => array(
                    'nome' => 'Nuovo Utente Sponsorizzato',
                    'reply_to' => array(),
                    'address' => "{[cliente][email]}",
                    'subject' => "Nuovo utente sponsorizzato - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{codice-sponsor}" => "Il codice sponsor dell'utente",
                            "{nome-sponsorizzato}" => "Il nome dell'utente sponsorizzato",
                            "{email-sponsorizzato}" => "L'email del cliente sponsorizzato"
                        ),
                        array(
                            "{[cliente][nome]}",
                            "{[cliente][email]}",
                            "{[codice-sponsor]}",
                            "{[affiliato][nome]}",
                            "{[affiliato][email]}"
                        )
                    )
                ),

                /* TEMPLATE RICHIESTE DI PRELIEVO */
                'withdrawal-status-0' => array(
                    'nome' => 'Richiesta di prelievo - In Attesa',
                    'reply_to' => array(),
                    'address' => "{[cliente][email]}",
                    'subject' => "Richiesta di prelievo In Attesa - {site-name}",
                    'shortcodes' => $withdrawals_shortcodes
                ),
                'withdrawal-status-1' => array(
                    'nome' => 'Richiesta di prelievo - Elaborata',
                    'reply_to' => array(),
                    'address' => "{[cliente][email]}",
                    'subject' => "Richiesta di prelievo Accettata - {site-name}",
                    'shortcodes' => $withdrawals_shortcodes
                ),
                'withdrawal-status-2' => array(
                    'nome' => 'Richiesta di prelievo - Annullata',
                    'reply_to' => array(),
                    'address' => "{[cliente][email]}",
                    'subject' => "Richiesta di prelievo Annullata - {site-name}",
                    'shortcodes' => $withdrawals_shortcodes
                ),
                'admin/nuova-richiesta-di-prelievo' => array(
                    'nome' => '[ADMIN] Nuova Richiesta di prelievo',
                    'reply_to' => array(),
                    'address' => "{[cliente][email]}",
                    'subject' => "Nuova richiesta di prelievo - {site-name}",
                    'shortcodes' =>  array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{valore}" => "Il valore della richiesta di prelievo",
                            "{metodo_pagamento}" => "Il metodo di pagamento",
                            "{note}" => "Le note della richiesta",
                            '{admin_link}' => 'Il link per gestire la richiesta di prelievo'
                        ),
                        array(
                            "{[cliente][nome]}",
                            "{[cliente][email]}",
                            "{[valore]}",
                            "{[metodo_pagamento]}",
                            "{[note]}",
                            $this->MSFrameworkCMS->getURLToSite(1) . 'modules/affiliations/withdrawals/edit.php?id={[id]}'
                        )
                    )
                ),
            )
        );

        return $templates;
    }
}
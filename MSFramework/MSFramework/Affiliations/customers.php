<?php
/**
 * MSFramework
 * Date: 01/02/19
 */

namespace MSFramework\Affiliations;


class customers {
    public function __construct() {
        global $firephp;
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }


    /**
     * Crea i dati di affiliazione di un cliente
     *
     * @param $user_id int L'id del cliente da aggiungere
     * @param $sponsored_by mixed Il codice di sponsorizzazione o l'ID dello sponsor
     *
     * @return string il codice del cliente
     */
    public function registerUser($user_id, $sponsored_by = 0) {
        Global $MSFrameworkUrl;
        $user_info = (new \MSFramework\customers())->getCustomerDataFromDB($user_id);
        $custom_code = $MSFrameworkUrl->cleanString($user_info['nome'] . $user_info['cognome'] . $user_id);

        if(!is_numeric($sponsored_by)) {
            $sponsored_by = (int)$this->getCustomerIDByCode($sponsored_by);
        }

        $this->MSFrameworkDatabase->query("INSERT INTO affiliations__customers (id, affiliation_code, sponsored_by) VALUES (:id, :aff_code, :sponsored_by)", array(':id' => $user_id, ':aff_code' => $custom_code, ':sponsored_by' => $sponsored_by));

        if($sponsored_by > 0) {
            (new \MSFramework\Affiliations\emails())->sendMail('nuovo-utente-sponsorizzato', array(
                'cliente' => (new \MSFramework\customers())->getCustomerDataFromDB($sponsored_by),
                'affiliato' => $user_info,
                'codice-sponsor' => $this->getCustomerCode($sponsored_by)
            ));
        }

        return $custom_code;
    }

    /**
     * Ottiene la lista di tutti gli utenti affiliati di un determinato utente
     *
     * @param $user_id int L'id del cliente da aggiungere
     *
     * @return array Un array con la lista di tutti i clienti affiliati
     */
    public function getCustomerAffiliates($user_id) {

        $customers = new \MSFramework\customers();

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM affiliations__customers WHERE sponsored_by = :id ORDER BY id DESC", array(':id' => $user_id)) as $c) {
            $ary_to_return[$c['id']] = $customers->getCustomerDataFromDB($c['id']);
        }

        return $ary_to_return;
    }

    /**
     * Ottiene l'ID dello sponsor del cliente se impostato
     *
     * @param $user_id integer L'ID del cliente
     *
     * @return mixed Le info riguardo l'affiliazione del cliente
     */
    public function getCustomerInfo($user_id) {
        $check_customer = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM affiliations__customers WHERE id = :id", array(':id' => $user_id), true);
        if($check_customer) {
            return $check_customer;
        }
        return false;
    }


    /**
     * Modifica il codice sponsor del cliente
     *
     * @param $user_id integer L'ID del cliente
     * @param $affiliation_code string Il codice sponsor del cliente
     *
     * @return mixed Lo stato dell'inserimento
     */
    public function setCustomerCode($user_id, $affiliation_code) {

        // Se l'utente non esiste nella tabella affiliati lo aggiungo
        $check_customer = $this->MSFrameworkDatabase->getCount("SELECT id FROM affiliations__customers WHERE id = :id", array(':id' => $user_id));
        if(!$check_customer) {
            (new \MSFramework\Affiliations\customers())->registerUser($user_id);
        }

        return $this->MSFrameworkDatabase->query("UPDATE affiliations__customers SET affiliation_code = :affiliation_code WHERE id = :id", array(':affiliation_code' => $affiliation_code, ':id' => $user_id));
    }

    /**
     * Ottiene l'ID di un cliente tramite il codice affiliazione
     *
     * @param $sponsor_code string Il codice sponsor
     * @param $exclude_id mixed Eventuale cliente da escludere dalla ricerca
     *
     * @return integer L'ID del cliente
     */
    public function getCustomerIDByCode($sponsor_code, $exclude_id = false) {

        if(empty($sponsor_code)) return false;

        $exclude_sql = "";
        if($exclude_id) {
            $exclude_sql = " AND id != " . (int)$exclude_id;
        }

        $check_customer = $this->MSFrameworkDatabase->getAssoc("SELECT id FROM affiliations__customers WHERE affiliation_code = :aff_code $exclude_sql", array(':aff_code' => $sponsor_code), true);

        if($check_customer) {
            return $check_customer['id'];
        }
        else {
            return false;
        }
    }

    /**
     * Ottiene l'ID di un cliente tramite il codice affiliazione
     *
     * @param $user_id integer L'ID del cliente
     *
     * @return integer L'ID del cliente
     */
    public function getCustomerCode($user_id) {
        $check_customer = $this->MSFrameworkDatabase->getAssoc("SELECT affiliation_code FROM affiliations__customers WHERE id = :id", array(':id' => $user_id), true);

        if($check_customer) {
            return $check_customer['affiliation_code'];
        }
        else {
            return false;
        }
    }

    /**
     * Imposta lo sponsor di un utente tramite il codice
     *
     * @param $user_id integer L'ID del cliente
     * @param $sponsor_code string Il codice sponsor
     *
     * @return mixed Lo stato dell'operazione
     */
    public function setCustomerSponsorByCode($user_id, $sponsor_code) {

        $MSFrameworkCustomers = new \MSFramework\customers();

        $sponsor_id = $this->getCustomerIDByCode($sponsor_code);

        if($sponsor_id && $this->MSFrameworkDatabase->query("UPDATE affiliations__customers SET sponsored_by = :sponsor_id WHERE id = :id", array(':sponsor_id' => $sponsor_id, ':id' => $user_id))) {

            $user_info = $MSFrameworkCustomers->getCustomerDataFromDB($user_id);
            $sponsor_info = $MSFrameworkCustomers->getCustomerDataFromDB($sponsor_id);

            if($sponsor_info) {
                (new \MSFramework\Affiliations\emails())->sendMail('nuovo-utente-sponsorizzato', array(
                    'cliente' => $sponsor_info,
                    'affiliato' => $user_info,
                    'codice-sponsor' => $this->getCustomerCode($sponsor_id)
                ));
            }
        }

        return true;
    }


}
<?php
/**
 * MSFramework
 * Date: 01/02/19
 */

namespace MSFramework\Affiliations;


class commissions {
    public function __construct() {
        global $firephp;
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Ottiene tutte le proviggioni non ancora riscosse
     *
     * @param $id integer L'id del cliente che ha effettuato l'operazione
     * @param $type string La tipologia di guadagni da ottenere
     * @param $only_active bool Se impostato ottiene solamente le commissioni non ancora prelevate
     *
     * @return array Un lista in ordine cronologico (DESC) con tutti i guadagni di un cliente
     */
    public function getCommissionsList($id, $type = '', $only_active = true) {

        $bilancio = 0.0;

        $type_sql = '';
        if(!empty($type)) {
            $type_sql = " AND note LIKE '%{RIF_$type=%'";
        }

        $withdraw_info = $this->getCustomerWithdrawalInfo($id);

        $date_sql = '';
        if($only_active && $type != "" && $withdraw_info && $withdraw_info['status'] != 2) {
            $date_sql = " AND data > '" . $withdraw_info['data'] . "'";
        }

        $ary_log = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM customers_balance WHERE cliente = :id $type_sql $date_sql ORDER BY data desc", array(":id" => $id)) as $operazione) {

            $id_cliente = $this->getBalanceNotes($operazione['note'], "CLIENTE");

            if($id_cliente) {
                $operazione['cliente'] = (new \MSFramework\customers())->getCustomerDataFromDB($this->getBalanceNotes($operazione['note'], "CLIENTE"));
            }
            else {
                $operazione['cliente'] = false;
            }

            $oggetto = $this->parseCommissionSubject($operazione['note']);
            if($oggetto) {
                $operazione['oggetto'] = $oggetto;
            }
            else {
                $operazione['oggetto'] = $operazione['note'];
            }

            $ary_log[] = $operazione;

            if($operazione['tipo'] == 'debito') {
                $bilancio -= (float)$operazione['valore'];
            }
            else {
                $bilancio += (float)$operazione['valore'];
            }
        }

        return array("log" => $ary_log, "balance" => $bilancio);
    }

    /**
     * Ottiene l'oggetto della commissione
     *
     * @param $note string La nota da analizzare
     *
     * @return mixed
     */
    public function parseCommissionSubject($note) {
        global $firephp;
        global $MSFrameworki18n;

        $return_value = "";

        $subject_keys = array(
            'SUBSCRIPTION_PLAN' => 'Abbonamento',
            'TRAVELAGENCY_PACKAGE' => 'Viaggio'
        );

        foreach($subject_keys as $key => $type) {
            $note_val = $this->getBalanceNotes($note, $key);
            if($note_val) {
                $subject_name = '';

                if($key == 'SUBSCRIPTION_PLAN') {
                    $subject_name = $MSFrameworki18n->getFieldValue((new \MSFramework\Subscriptions\plans())->getPlanDetails($note_val)[$note_val]['titolo']);
                }
                else if($key == 'TRAVELAGENCY_PACKAGE') {
                    $subject_name = $MSFrameworki18n->getFieldValue((new \MSFramework\TravelAgency\packages())->getPackageDetails($note_val)[$note_val]['nome']);
                }

                $return_value = $type . ' ' . $subject_name;
            }
        }

        return $return_value;
    }

    /**
     * Effettua il parsing delle note dei crediti/debiti alla ricerca di tag speciali da convertire. Restituisce la nota convertita pronta per l'output
     *
     * @param $note string La nota da analizzare
     * @param $key string La chiave che si desidera estrapolare
     *
     * @return mixed
     */
    public function getBalanceNotes($note, $key) {
        global $firephp;

        $return_value = "";

        //Riferimento operazione
        $rif_op_tag = "{RIF_$key=";
        $pos_rif_op_start = strpos($note, $rif_op_tag);
        if($pos_rif_op_start !== false) {
            $pos_rif_op_end = strpos($note, '}', $pos_rif_op_start);
            $link_to_op_value = substr($note, $pos_rif_op_start+strlen($rif_op_tag), $pos_rif_op_end-($pos_rif_op_start+strlen($rif_op_tag)));

            $return_value = $link_to_op_value;
        }

        return $return_value;
    }

    /**
     * Ottiene tutte le proviggioni di un determinato cliente
     *
     * @param $customer_id integer L'id del cliente che ha effettuato l'operazione
     * @param $amount float Il valore del prelievo
     * @param $metodo_pagamento string Il metodo di pagamento usato (Esempio: paypal)
     * @param $referenza_pagamento mixed Le referenze utili in base al pagamento selezionato (per paypal ad esempio potremmo immagazinare l'email destinatario e il codice richiesta)
     * @param $note string Le note da allegare al prelievo
     *
     * @return mixed Ritorna l'ID del prelievo o false in caso di errore
     */
    public function createWithdraw($customer_id, $amount, $metodo_pagamento, $referenza_pagamento, $note = '') {

        if(is_array($referenza_pagamento)) {
            $referenza_pagamento = json_encode($referenza_pagamento);
        }

        if($this->MSFrameworkDatabase->query("INSERT INTO affiliations__withdrawals (cliente, metodo_pagamento, referenza_pagamento, valore, note, status) VALUES (:cliente, :metodo_pagamento, :referenza_pagamento, :valore, :note, :status)", array(':cliente' => $customer_id, ':metodo_pagamento' => $metodo_pagamento, ':referenza_pagamento' => $referenza_pagamento, ':valore' => $amount, ':note' => $note, ':status' => 0))) {

            $withdrawal_id = $this->MSFrameworkDatabase->lastInsertId();

            $email_params = array(
                'id' => $withdrawal_id,
                'cliente' => (new \MSFramework\customers())->getCustomerDataFromDB($customer_id),
                'valore' => $amount,
                'metodo_pagamento' => $metodo_pagamento,
                'note' => $note
            );

            (new \MSFramework\Affiliations\emails())->sendMail('admin/nuova-richiesta-di-prelievo', $email_params);
            (new \MSFramework\Affiliations\emails())->sendMail('withdrawal-status-0', $email_params);

            return $withdrawal_id;
        }
        else {
            return false;
        }
    }


    /**
     * Ottiene le informazioni dell'ultimo prelievo effettuato
     *
     * @param $customer_id integer L'id del cliente che ha effettuato l'operazione
     * @return array Se presente ritorna un array con le info riguardo l'ultimo prelievo effettuato altrimenti un array vuoto
     */
    public function getCustomerWithdrawalInfo($customer_id) {

        $result = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM affiliations__withdrawals WHERE cliente = $customer_id ORDER BY id DESC LIMIT 1", array(), true);
        if($result) {

            if(json_decode($result['referenza_pagamento'])) {
                $result['referenza_pagamento'] = json_decode($result['referenza_pagamento'], true);
            }

           return (array)$result;
        }
        else {
            return array();
        }
    }

    /**
     * Ottiene le informazioni dell'ultimo prelievo effettuato
     *
     * @param $customer_id integer L'id del cliente che ha effettuato l'operazione
     * @param $params array Un array con tutti i parametri relativi
     *
     * @return array Un array con la lista dei prelievi effettuati
     */
    public function getCustomerWithdrawalsList($customer_id, $params = array()) {

        $default_params = array(
            'page' => 1,
            'limit' => 12
        );

        foreach($default_params as $key => $v) {
            if(!isset($params[$key])) {
                $params[$key] = $v;
            }
        }

        /* IMPOSTA L'OFFSET IN BASE ALLA PAGINA DA MOSTRARE */
        $start = ($params['page'] * $params['limit']) - $params['limit'];

        $count = $this->MSFrameworkDatabase->getCount("SELECT * FROM affiliations__withdrawals WHERE cliente = :cliente", array(":cliente" => $customer_id));

        $ary_to_return = array('page' => $params['page'], 'showing_per_page' => $params['limit'], 'count' => $count, 'list' => array());

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM affiliations__withdrawals WHERE cliente = $customer_id ORDER BY id DESC LIMIT :start, :limit", array(":start" => $start, ":limit" => $params['limit'])) as $r) {

            if(json_decode($r['referenza_pagamento'])) {
                $r['referenza_pagamento'] = json_decode($r['referenza_pagamento'], true);
            }

            $ary_to_return['list'][$r['id']] = $r;
        }

        return $ary_to_return;

    }

    /**
     * Ottiene le informazioni dell'ultimo prelievo effettuato
     *
     * @param $id integer L'id del prelievo
     * @param $status integer Lo stato del prelievo (0 In corso; 1 Accettato; 2 Annullato)
     *
     * @return mixed
     */
    public function updateWithdrawalStatus($id, $status) {

        $withdrawal_info = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM  affiliations__withdrawals WHERE id = :id", array(':id' => $id), true);
        if(!$withdrawal_info) return false;

        if($status == 1) {
            (new \MSFramework\customers())->addNewOperation($withdrawal_info['cliente'], -$withdrawal_info['valore'], $withdrawal_info['note']);
        }

        (new \MSFramework\Affiliations\emails())->sendMail('withdrawal-status-' . $status, array(
            'cliente' => (new \MSFramework\customers())->getCustomerDataFromDB($withdrawal_info['cliente']),
            'valore' => $withdrawal_info['valore'],
            'metodo_pagamento' => $withdrawal_info['metodo_pagamento'],
            'note' => $withdrawal_info['note']
        ));

        return $this->MSFrameworkDatabase->query("UPDATE affiliations__withdrawals SET status = " . $status . ", data = NOW() WHERE id = " . $id);
    }


    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente allo stato dei prelievi
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getWithdrawalStatus($key = "") {
        $ary = array(
            "0" => "In Attesa",
            "1" => "Elaborato",
            "2" => "Annullato"
        );

        if($key !== "") {
            return $ary[$key];
        } else {
            return $ary;
        }
    }
}
<?php
/**
 * MSFramework
 * Date: 16/05/2019
 */

namespace MSFramework;


class cronjob {
    public function __construct() {

    }

    /**
     * Ottiene tutti i cronJob relativi al Framework
     *
     * La struttura dell'array è formata in questo modo:
     * 'nome cartella' => 'nome file'.
     * intervallo: L'intervallo del cron
     * check: Il controllo da effettuare prima di lanciare l'azione (se negativo non viene eseguita)
     *
     * @return array|mixed
     */
    public function getFrameworkCronJobs() {

        $cronJobs = array(

        );

        return $cronJobs;

    }

    /**
     * Ottiene tutti i cronJob standard da avviare
     *
     * La struttura dell'array è formata in questo modo:
     * 'nome cartella' => 'nome file'.
     * intervallo: L'intervallo del cron
     * check: Il controllo da effettuare prima di lanciare l'azione (se negativo non viene eseguita)
     *
     * @return array|mixed
     */
    public function getStandardCronJobs() {

        $cronJobs = array(
            /* CRON NEWSLETTER */
            'newsletter' => array(
                'sendNewsletter' => array(
                    'intervallo' => array('*', '*', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM newsletter__campagne WHERE status = 1") > 0) &&  ($DB->getCount("SELECT * FROM `newsletter__campagne_destinatari_status` WHERE status = 1") > 0);
                    }
                ),
                'syncLists' => array(
                    'intervallo' => array('*/15', '*', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM `newsletter__tag` WHERE sync_options != '[]'") > 0);
                    }
                ),
                'checkBounced' => array(
                    'intervallo' => array('0', '0,12', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE date_sent > NOW() - INTERVAL 1 DAY") > 0);
                    }
                )
            ),
            'cleaner' => array(
                'statsCleaner' => array(
                    'intervallo' => array('0', '0', '*', '*', '0,3'),
                    'check' => function($DB, $CMS) {
                        return true;
                    }
                ),
                'tmpCleaner' => array(
                    'intervallo' => array('0', '3', '*', '*', '6'),
                    'check' => function($DB, $CMS) {
                        return true;
                    }
                )
            ),
            'notifications' => array(
                'sendNotifications' => array(
                    'intervallo' => array('0', '12', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM notifications WHERE is_active = '1'") > 0);
                    }
                )
            ),
            'sitemap' => array(
                'generateSitemap' => array(
                    'intervallo' => array('0', '5', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        $sitemap_settings = json_decode($CMS->getCMSData('site')['sitemap'], true);
                        return ($sitemap_settings['auto_generate'] == "1");
                    }
                )
            ),
            'mailbox' => array(
                'sync' => array(
                    'intervallo' => array('*', '*', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM `email_config` WHERE enable_mailbox = '1'") > 0);
                    }
                )
            )
        );

        return $cronJobs;

    }

    /**
     * Ottiene tutti i cronJob da avviare in sviluppo
     *
     * La struttura dell'array è formata in questo modo:
     * 'nome cartella' => 'nome file'.
     * intervallo: L'intervallo del cron
     * check: Il controllo da effettuare prima di lanciare l'azione (se negativo non viene eseguita)
     *
     * @return array|mixed
     */
    public function getDevelopmentCronJobs() {

        $cronJobs = array(
            'cleaner' => array(
                'uploadsCleaner' => array(
                    'intervallo' => array('30', '3', '*', '*', '0,4'),
                    'check' => function($DB, $CMS) {
                        return true;
                    }
                )
            ),
            'notifications' => array(
                'sendNotifications' => array(
                    'intervallo' => array('0', '12', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM notifications WHERE is_active = '1'") > 0);
                    }
                )
            ),
            'newsletter' => array(
                'syncLists' => array(
                    'intervallo' => array('0', '9,14,19', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM `newsletter__tag` WHERE sync_options != '[]'") > 0);
                    }
                ),
                'sendNewsletter' => array(
                    'intervallo' => array('*', '*', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM newsletter__campagne WHERE status = 1") > 0) && ($DB->getCount("SELECT * FROM `newsletter__campagne_destinatari_status` WHERE status = 1") > 0);
                    }
                ),
                'checkBounced' => array(
                    'intervallo' => array('0', '0,12', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM `newsletter__emails_destinatari_status` WHERE date_sent > NOW() - INTERVAL 1 DAY") > 0);
                    }
                )
            ),
            'mailbox' => array(
                'sync' => array(
                    'intervallo' => array('*', '*', '*', '*', '*'),
                    'check' => function($DB, $CMS) {
                        return ($DB->getCount("SELECT * FROM `email_config` WHERE enable_mailbox = '1'") > 0);
                    }
                )
            )
        );

        return $cronJobs;

    }

    /**
     * Restituisce il prefisso che viene utilizzato per inviare i cron dal frontend
     *
     * @return string
     */
    public function getShortPrefix() {
        return 'fkdljk48548574389frhds324ds';
    }
}
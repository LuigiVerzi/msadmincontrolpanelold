<?php
/**
 * MSFramework
 * Date: 17/04/18
 */

namespace MSFramework\TravelAgency;


class categories
{
    public function __construct()
    {
        global $firephp;

        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria
     *
     * @param string $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryDetails($id = "", $fields = "*")
    {
        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }
            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary) || !$same_db_string_ary)
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        $category = $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM travelagency__packages_categories WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);
        foreach ((array)$category as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria genitore
     *
     * @param string $id L'ID della categoria figlia
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryParent($children_id, $fields = "*")
    {

        $category_order = (new \MSFramework\cms())->getCMSData('travelagency_cats_order')['list'];

        $parent_id = array();
        foreach ($category_order as $parent) {
            foreach($parent['children'] as $children) {
                if ($children['id'] == $children_id) {
                    $parent_id = $parent['id'];
                }
                else if(isset($children['children']))
                {
                    foreach($children['children'] as $subchildren) {
                        if ($subchildren['id'] == $children_id) {
                            $parent_id = array($parent['id'], $children['id']);
                        }
                    }
                }
            }
        }

        $category = ($parent_id ? $this->getCategoryDetails($parent_id) : array());

        return $category;
    }

    /**
     * Ottiene l'URL della categoria
     *
     * @param $id L'ID (o Array) della categoria per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl;

        if(is_array($id)) $page_det = $id;
        else $page_det = $this->getCategoryDetails($id, "slug")[$id];

        $slug = '';
        $have_parents = $this->getCategoryParent($id);
        if($have_parents) {
            foreach($have_parents as $parent) {
                $slug .= $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($parent['slug'])) . "/";
            }
        }

        $slug .= $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $slug;
    }
}
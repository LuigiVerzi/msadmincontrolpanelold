<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

namespace MSFramework\TravelAgency\Payments;


class paypal {
    public function __construct() {
        Global $MSFrameworkDatabase;
        Global $MSFrameworki18n;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworki18n = $MSFrameworki18n;
    }

    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param $orderDetails array L'Array con le info relative all'ordine
     * @param $return_url string L'url di ritorno
     *
     * @return string La stringa con l'URL che porta al pagamento
     *
     */
    public function getPaymentButton($orderDetails, $return_url) {
        Global $MSFrameworkCMS;

        $siteCMSData = $MSFrameworkCMS->getCMSData('site');
        $site_logos = json_decode($siteCMSData['logos'], true);

        $loggedUser = (new \MSFramework\Ecommerce\customers())->getUserDataFromSession();

        $metodi_pagamento = $MSFrameworkCMS->getCMSData('fatturazione_fatture');
        $metodi_pagamento = json_decode($metodi_pagamento['webPayments'], true);

        $imposte_class = new \MSFramework\Fatturazione\imposte();
        $formatted_price = $imposte_class->getPriceToShow( ($orderDetails['package_info']['prezzo_scontato'] > 0 ? $orderDetails['package_info']['prezzo_scontato'] : $orderDetails['package_info']['prezzo']) , $orderDetails['package_info']['extra_fields']['imposta']);

        if(!empty($orderDetails["coupons_used"]) && $orderDetails["coupons_used"]) {
            $prezzo_scontato = $formatted_price['no_tax'] - $orderDetails["coupons_used"]['discount'];
            $subtotale_ivato = $imposte_class->addTax($prezzo_scontato, $orderDetails['package_info']['extra_fields']['imposta']);
        }
        else
        {
            $subtotale_ivato = $formatted_price['tax'];
        }

        $html = '<form method="post" name="paypal_form" id="paypal_form" action="https://www' . (strstr($_SERVER['DOCUMENT_ROOT'], "SVILUPPO") ? '.sandbox' : '') . '.paypal.com/cgi-bin/webscr">';
        $html .= '<input type="hidden" name="business" value="' . $metodi_pagamento['paypal_data']['email'] . '" />';
        $html .= '<input type="hidden" name="cmd" value="_cart" />';
        $html .= '<input type="hidden" name="upload" value="1">';
        
        $html .= '<input type="hidden" name="image_url" value="' . (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo'] .'">';

        $html .= '<input type="hidden" name="return" value="' . $return_url . '" />';
        $html .= '<input type="hidden" name="cancel_return" value="' . $return_url . '" />';

        $html .= '<input type="hidden" name="notify_url" value="' . $MSFrameworkCMS->getURLToSite() . 'ipn/pacchetti/paypal_ipn.php'. '" />';
        $html .= '<input type="hidden" name="rm" value="2" />';
        $html .= '<input type="hidden" name="currency_code" value="EUR" />';
        $html .= '<input type="hidden" name="lc" value="IT" />';
        $html .= '<input type="hidden" name="cbt" value="Vedi il tuo ordine su ' . SW_NAME. '" />';

        $html .= '<input type="hidden" name="item_name_1" value="' . htmlspecialchars((new \MSFramework\i18n())->getFieldValue($orderDetails['package_info']['nome'])). '">';
        $html .= '<input type="hidden" name="amount_1" value="' . $subtotale_ivato . '">';
        $html .= '<input type="hidden" name="quantity_1" value="1">';

        $html .= '<input type="hidden" name="custom" value="' . $orderDetails['id'] . '" />';
        //$html .= '<input type="hidden" name="discount_amount_cart" value="' . ($orderDetails['cart']['total_price']['tax'] - $orderDetails['cart']['coupon_price']) . '" />'; Todo: Sconto

        $html .= '<input type="hidden" name="first_name" value="' . $orderDetails['info_spedizione']['nome'] . '"/>';
        $html .= '<input type="hidden" name="last_name" value="' . $orderDetails['info_spedizione']['cognome'] . '"/>';
        $html .= '<input type="hidden" name="address1" value="' . $orderDetails['info_spedizione']['indirizzo'] . '"/>';
        $html .= '<input type="hidden" name="address2" value="' . (!empty($orderDetails['info_spedizione']['indirizzo_2']) ? '<br>' . $orderDetails['info_spedizione']['indirizzo_2'] : ''). '"/>';
        $html .= '<input type="hidden" name="city" value="' . $orderDetails['info_spedizione']['comune'] . '"/>';
        $html .= '<input type="hidden" name="state" value="' . (new \MSFramework\geonames())->getCountryDetails($orderDetails['info_spedizione']['stato'])[$orderDetails['info_spedizione']['stato']]['name'] . '"/>';
        $html .= '<input type="hidden" name="zip" value="' . $orderDetails['info_spedizione']['cap'] . '"/>';
        $html .= '<input type="hidden" name="email" value="' . $loggedUser['username'] . '"/>';

        $html .= '<input type="image" src="' . ABSOLUTE_SW_PATH_HTML. 'assets/images/paypal_logo.gif" border="0" name="submit" alt="' . $this->MSFrameworki18n->gettext('Paga subito tramite PayPal'). '" />';

        $html .= '</form>';

        return $html;

    }
    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param array $request
     * @param mixed $on_success È possibie passare una funzione
     *
     */
    public function checkIPN($request, $on_success = false) {
        Global $MSFrameworkCMS;

        $orders = new \MSFramework\TravelAgency\orders();
        $ipn = new \PayPal\PaypalIPN();

        // Use the sandbox endpoint during testing.
        if(($MSFrameworkCMS->isStaging() || $MSFrameworkCMS->isDevelopment())) {
            $ipn->useSandbox();
        }
        $verified = $ipn->verifyIPN();

        if ($verified) {

            $receiver_email = filter_var($request['receiver_email'], FILTER_SANITIZE_EMAIL);
            $order_id = filter_var($request['custom'], FILTER_SANITIZE_STRING);

            $metodi_pagamento = $MSFrameworkCMS->getCMSData('fatturazione_fatture');
            $metodi_pagamento = json_decode($metodi_pagamento['webPayments'], true);

            if($receiver_email == $metodi_pagamento['paypal_data']['email']) {

                $this->MSFrameworkDatabase->query("UPDATE travelagency__orders SET payment_log = :plog WHERE id = :id", array(":plog" => json_encode($request), ":id" => $order_id));
                $orders->updateOrderStatus($order_id, 1);

                if(is_callable($on_success)) {
                    $on_success($order_id);
                }

            }
        }

        header("HTTP/1.1 200 OK");
    }
}
<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

namespace MSFramework\TravelAgency;

class emails extends \MSFramework\emails {
    /**
     * Ottiene la lista di template per l'ecommerce
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {
        $order_status_params = array(
            'prepareParams' => function ($params, $template_settings) {
                $params['order']['info_fatturazione']['stato'] = (new \MSFramework\geonames())->getCountryDetails($params['order']['info_fatturazione']['stato'])[$params['order']['info_fatturazione']['stato']]['name'];
                return $params;
            },
            'shortcodes' => array(
                array(
                    "{nome}" => 'Il nome del cliente',
                    "{prezzo_totale}" => "Il prezzo totale",
                    "{tabella_prodotti}" => "La tabella con la lista dei prodotti",
                    "{order_url}" => 'L\'URL del pannello relativo all\'ordine'
                ),
                array(
                    function ($params) {
                        return (!empty($params['user']['nome']) ? $params['user']['nome'] . ' ' . $params['user']['cognome'] : $params['user']['username']);
                    },
                    function ($params) {
                        return number_format((new \MSFramework\Fatturazione\imposte())->getPriceToShow(($params['order']['package_info']['prezzo_scontato'] > 0 ? $params['order']['package_info']['prezzo_scontato'] : $params['order']['package_info']['prezzo']))['tax'],2,',','.') . CURRENCY_SYMBOL;
                    },
                    function ($params) {

                        Global $MSFrameworki18n;

                        $products_rows_template = '<tr><td style="text-align: left;">{product_name}</td><td style="text-align: right;">{product_price}</td></tr>';

                        $products_row_html = str_replace(
                            array(
                                "{product_name}",
                                "{product_price}",
                            ),
                            array(
                                $MSFrameworki18n->getFieldValue($params['order']['package_info']['nome']),
                                number_format((new \MSFramework\Fatturazione\imposte())->getPriceToShow(($params['order']['package_info']['prezzo_scontato'] > 0 ? $params['order']['package_info']['prezzo_scontato'] : $params['order']['package_info']['prezzo']))['tax'],2,',','.') . CURRENCY_SYMBOL
                            ),
                            $products_rows_template
                        );


                        return '<table style="width: 100%;">' . $products_row_html . '</table>';

                    },
                    function ($params) {
                        if($params['to'] == 'admin') {
                            return $this->MSFrameworkCMS->getURLToSite(1) . 'modules/travelagency/ordini/edit.php?id=' . $params['order']['id'];
                        } else {
                            return $this->MSFrameworkCMS->getURLToSite() . 'redirect_to_order/' . $params['order']['id'];
                        }
                    }
                )
            )
        );

        $default_address = $this->MSFrameworkCMS->getCMSData("settings")['destination_address'];

        $templates = array(
            'travelagency' => array(
                /* TEMPLATE STATO ABBONAMENTI */
                'travelagency-status/0' => array(
                    'nome' => '[STATO ORDINE] In Attesa di Pagamento',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "In Attesa di Pagamento - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes']
                ),
                'travelagency-status/1' => array(
                    'nome' => '[STATO ORDINE] Pacchetto Acquistato',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Pacchetto Acquistato - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes']
                ),
                /* TEMPLATE STATO ABBONAMENTI ADMIN */
                'travelagency-status/admin/0' => array(
                    'nome' => '[STATO ORDINE][ADMIN] Nuovo Acquisto (In Attesa di Pagamento)',
                    'reply_to' => array(),
                    'address' => $default_address,
                    'subject' => "Nuovo Acquisto - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes']
                ),
                'travelagency-status/admin/1' => array(
                    'nome' => '[STATO ORDINE][ADMIN] Nuovo Acquisto (Pagato)',
                    'reply_to' => array(),
                    'address' => $default_address,
                    'subject' => "Nuovo Pagamento Ricevuto - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes']
                )
            )
        );

        return $templates;
    }
}
<?php
/**
 * MSFramework
 * Date: 15/02/18
 */

namespace MSFramework\TravelAgency;


class packages {
    public function __construct() {
        global $firephp;
        Global $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Pacchetto => Dati Pacchetto) con i dati relativi al pacchetto.
     * L'array contiene (in una chiave addizionale 'gallery_friendly' aggiunta al volo) sia le informazioni da utilizzare nel DOM (HTML) che il path assoluto delle immagini
     *
     * @param $id L'ID del pacchetto (stringa) o dei pacchetti (array) richieste (se vuoto, vengono recuperati i dati di tutti i pacchetti)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getPackageDetails($id = "", $fields = "*") {
        if($id !== "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM travelagency__packages WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $this->formatPackageDetails($r);;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Pacchetto => Dati Pacchetto) con i dati relativi ai pacchetti in evidenza.
     *
     * @param $limit integer Il numero di paccheti da mostrare
     *
     * @return array
     */
    public function getFeaturedPackages($limit = 5) {

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM travelagency__packages WHERE id != '' AND featured = 1 ORDER BY id DESC LIMIT :limit", array(":limit" => $limit)) as $r) {
            $ary_to_return[$r['id']] = $this->formatPackageDetails($r);
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Pacchetto => Dati Pacchetto) con i dati relativi ai pacchetti in evidenza.
     *
     * @param $limit integer Il numero di paccheti da mostrare
     *
     * @return array
     */
    public function getLastPackages($limit = 5) {

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM travelagency__packages WHERE id != '' ORDER BY id DESC LIMIT :limit", array(":limit" => $limit)) as $r) {
            $ary_to_return[$r['id']] = $this->formatPackageDetails($r);
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Pacchetto => Dati Pacchetto) con i dati relativi ai pacchetti in ordine cronologico.
     *
     * @param $limit integer Il numero di paccheti da mostrare
     *
     * @return array
     */
    public function getNextPackages($limit = 5) {

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM travelagency__packages WHERE id != '' ORDER BY data_partenza ASC LIMIT :limit", array(":limit" => $limit)) as $r) {
            $ary_to_return[$r['id']] = $this->formatPackageDetails($r);
        }

        return $ary_to_return;
    }

    /**
     * Ottiene l'URL del pacchetto
     *
     * @param $page_detail mixed L'ID o array del pacchetto per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($page_detail) {
        global $MSFrameworki18n;
        global $MSFrameworkUrl;

        if(!is_array($page_detail)) {
            $page_detail = $this->getPackageDetails($page_detail, "slug")[$page_detail];
        }

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_detail['slug'])) .  "/";
    }


    /**
     * Prepara i dettagli del pacchetto per una lettura più veloce
     *
     * @param $r array Un array con i dettagli del pacchetto
     *
     * @return array
     */
    public function formatPackageDetails($r) {

        $page_gallery = json_decode($r['gallery'], true);
        if(is_array($page_gallery)) {

            $r['gallery_friendly'] = array();
            foreach($page_gallery as $file) {
                $r['gallery_friendly'][] = array(
                    "html" => array(
                        "main" => UPLOAD_TRAVELAGENCY_PACKAGE_FOR_DOMAIN_HTML . $file,
                        "thumb" => UPLOAD_TRAVELAGENCY_PACKAGE_FOR_DOMAIN_HTML . "tn/" . $file
                    ),
                    "absolute" => array(
                        "main" => UPLOAD_TRAVELAGENCY_PACKAGE_FOR_DOMAIN . $file,
                        "thumb" => UPLOAD_TRAVELAGENCY_PACKAGE_FOR_DOMAIN . "tn/" . $file
                    ),
                );
            }

        }

        if(isset($r['prezzo'])) $r['prezzo_singolo'] = $r['prezzo'];
        if(isset($r['prezzo'])) $r['prezzo'] = $r['prezzo']*$r['persone'];

        if($r['prezzo_scontato']) {
            if(!empty($r['scadenza_sconto']) && (strtotime($r['scadenza_sconto']) > 0 && strtotime($r['scadenza_sconto']) < time())) {
                $r['prezzo_scontato'] = '';
            } else {
                $r['prezzo_scontato_persona'] = $r['prezzo_scontato'];
                $r['prezzo_scontato'] = $r['prezzo_scontato']*$r['persone'];
            }
        }

        if(json_decode($r['hotel'])) $r['hotel'] = json_decode($r['hotel'], true);

        if(json_decode($r['partenza'])) $r['partenza'] = json_decode($r['partenza'], true);

        if(json_decode($r['ritorno'])) $r['ritorno'] = json_decode($r['ritorno'], true);

        if(json_decode($r['extra_fields'])) $r['extra_fields'] = (new \MSFramework\utils())->formatExtraFieldsValues(json_decode($r['extra_fields'], true));
        else $r['extra_fields'] = array();

        $r['services'] = ($r['services'] != "" ? explode(',', $r['services']) : array());

        $r['url'] = $this->getURL($r);

        return $r;
    }

}
<?php
/**
 * MSFramework
 * Date: 15/02/18
 */

namespace MSFramework\TravelAgency;

class search {

    private $search_query = '';
    private $search_date_range = array();
    private $search_category = false;
    private $search_price_range = array();
    private $search_services = array();

    private $search_limit = 12;
    private $search_page = 1;
    private $search_order = array('id', 'DESC');

    public function __construct($params = false) {
        global $firephp;

        if($params) {
            $this->setParams($params);
        }

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkTravelAgencyPackages= new \MSFramework\TravelAgency\packages();
    }

    /**
     * Ottiene i pacchetti presenti nella ricerca
     *
     * @return mixed
     */
    public function getResults() {
        $ary_to_return = array();

        foreach($this->MSFrameworkDatabase->getAssoc($this->getSQL()['full_sql']) as $r) {
            $ary_to_return[$r['id']] = $this->MSFrameworkTravelAgencyPackages->formatPackageDetails($r);
        }

        return $ary_to_return;
    }

    /**
     * Conta il numero di pacchetti presenti nella ricerca seguente
     *
     * @return mixed
     */
    public function countResults() {
        return $this->MSFrameworkDatabase->getCount($this->getSQL()['sql']);
    }


    /**
     * Conta il numero di pacchetti presenti nella ricerca seguente
     *
     * @return mixed
     */
    public function getAvaialableCategories() {

        $avaialable_cats = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT categoria FROM (" . $this->getSQL()['sql'] . ") tab GROUP BY categoria") as $cat) {
            $avaialable_cats[] = $cat['categoria'];
        }
        return $avaialable_cats;

    }

    /**
     * Restituisce il range di prezzo all'interno di una determinata ricerca
     *
     * @param array $range Il range da controllare
     *
     * @return mixed
     */
    public function getAvaialablePriceRanges($range = array(1000, 100))
    {

        if(!$range) {
            $range = array(1000, 100);
        }

        $sql_range = '';
        for ($i = 0; $i <= $range[0]; $i += $range[1]) {
            $sql_range .= "WHEN prezzo < " . ($i + $range[1]) . " THEN '" . $i . "-" . ($i + $range[1]) . "' ";
        }

        $price_ranges = array();

        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT COUNT(*) as count, (CASE " . $sql_range . "ELSE '" . ($range[0] + $range[1]) . "' END) as raggio FROM (" . $this->getSQL()['sql'] . ") asd GROUP BY raggio") as $r) {
            $price_ranges[] = $r;
        }

        return $price_ranges;
    }

    /**
     * Restituisce un array con tutti gli ID dei servizi disponibili
     *
     * @return array
     */
    public function getAvaialableServices()
    {
        $services_ids = array();

        $services = $this->MSFrameworkDatabase->getAssoc("SELECT GROUP_CONCAT(services) as services FROM (" . $this->getSQL()['sql'] . ") alias", array(), true)['services'];
        if($services) {
            $services_ids = explode(',', $services);
        }

        return array_unique($services_ids);
    }

    /**
     * Restituisce un array con tutti i mesi disponibili
     *
     * @return array
     */
    public function getAvaialableMonths()
    {
        $months_ary = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT DATE_FORMAT(data_partenza, '%Y-%m') as data FROM (" . $this->getSQL()['sql'] . ") alias GROUP BY  DATE_FORMAT(data_partenza, '%Y-%m') ORDER BY data_partenza ASC") as $r) {
            $months_ary[] = $r['data'];
        }

        return $months_ary;
    }

    /**
     * Imposta i parametri di ricerca
     *
     * @param array $params I parametri da settare
     *
     */
    public function setParams($params)
    {
        foreach($params as $key=>$val){
            if(isset($this->{'search_' . $key})){
                $this->{'search_' . $key} = $val;
            }
        }
    }

    /**
     * Restituisce i parametri di ricerca attualmente utilizzati
     *
     * @return array I parametri utilizzati
     */
    public function getParams()
    {
        $params = array(
            'category' => $this->search_category,
            'query' => $this->search_query,
            'price_range' => $this->search_price_range,
            'date_range' => $this->search_date_range,
            'services' => $this->search_services,
            'limit' => $this->search_limit,
            'page' => $this->search_page,
            'order' => $this->search_order
        );
        return $params;
    }

    /**
     * Restituisce la struttura della paginazione in HTML
     *
     * @param $current_params array L'array con tutte le query attualmente attive
     * @param $param_name string Il nome del parametro utilizzato
     *
     * @return string
     */
    public function composeHTMLPagination($current_params = array(), $param_name = 'page')
    {
        
        $params = $this->getParams();

        unset($current_params[$param_name]);


        
        $links = 10;

        $last = ceil($this->countResults() / $params['limit']);

        $start = (($params['page'] - $links) > 0) ? $params['page'] - $links : 1;
        $end = (($params['page'] + $links) < $last) ? $params['page'] + $links : $last;

        $html = '<div class="pagination">';

        $class = ($params['page'] == 1) ? "disabled" : "";

        $current_params[$param_name] = $params['page'] - 1;
        $html .= '<a class="' . $class . '" href="?' . http_build_query($current_params) . '">&laquo;</a>';

        if ($start > 1) {
            $current_params[$param_name] = $params['page'] - 1;
            $html .= '<a href="?' . http_build_query($current_params) . '">1</a>';
            $html .= '<a class="disabled"><span>...</span></a>';
        }

        for ($i = $start; $i <= $end; $i++) {
            $class = ($params['page'] == $i) ? "active" : "";
            $current_params[$param_name] = $i;
            $html .= '<a class="' . $class . '" href="?' .  http_build_query($current_params) . '">' . $i . '</a>';
        }

        if ($end < $last) {
            $html .= '<a class="disabled"><span>...</span></a>';
            $current_params[$param_name] = $last;
            $html .= '<a href="?' .  http_build_query($current_params) . '">' . $last . '</a>';
        }

        $class = ($params['page'] == $last) ? "disabled" : "";

        $current_params[$param_name] = ($params['page'] + 1);
        $html .= '<a class="' . $class . '" ' . ($class != 'disabled' ? 'href="?' . http_build_query($current_params) . '"' : '') . '>&raquo;</a>';

        $html .= '</div>';

        return $html;

    }
    
    // FUNZIONI PRIVATE //

    private function getSQL() {

        $params = $this->getParams();

        /* CREA LA QUERY CATEGORY */
        if($params['category'] == 0) $category_where = "id != ''";
        else $category_where = "find_in_set(" . $params['category'] . ", categoria)";

        /* CREA LA QUERY PRICE RANGE */
        if(!$params['price_range']) $price_where = "prezzo >= 0";
        else
        {
            if (is_array($params['price_range'][0])) {
                $price_where = array();
                foreach($params['price_range'] as $p_range) {
                    $price_where[] = 'prezzo >= ' . intval($p_range[0]) . ($p_range[1] ? ' AND prezzo <= ' . intval($p_range[1]) : '');
                }
                $price_where = '(' . implode(' AND ', $price_where) . ')';
            } else {
                $price_where = 'prezzo >= ' . intval($params['price_range'][0]) . ($params['price_range'][1] ? ' AND prezzo <= ' . intval($params['price_range'][1]) : '');
            }
        }

        /* CREA LA QUERY PRICE RANGE */
        if($params['date_range'] && count($params['date_range']) == 2) {
            $date_where = "(data_partenza > '" . $params['date_range'][0] . "' AND data_partenza <= '" . $params['date_range'][1] . "')";

        }
        else $date_where = "1";

        /* CREA LA QUERY SERVIZI */
        if(!$params['services']) $services_where = "1";
        else
        {
            $services_where = array();
            foreach($params['services'] as $service) {
                $services_where[] = "find_in_set(" . $service . ", services)";
            }
            $services_where =  '(' . implode(' AND ', $services_where) . ')';
        }

        $same_db_string_ary = array("id != '' ", array());
        if(!empty($params['query'])) {
            $search_db_string_ary =  $this->MSFrameworkDatabase->composeSameDBFieldString(array($params['query']), "AND", "nome", "LIKE", "both");

            $same_db_string_ary[0] .= ' AND' . $search_db_string_ary[0];

            $key = array_keys($search_db_string_ary[1])[0];
            $value = array_values($search_db_string_ary[1])[0];

            $same_db_string_ary[1][$key] = $value;
        }

        /* IMPOSTA L'OFFSET IN BASE ALLA PAGINA DA MOSTRARE */
        $start = ($params['page'] * $params['limit']) - $params['limit'];
        if($start < 0) $start = 0;

        /* CREA LA QUERY PER ORDINARE CORRETTAMNETE L'ARRAY */
        switch($params['order'][0]) {
            case 'date':
                $order_key = 'data_partenza';
                break;
            case 'price':
                $order_key = 'prezzo * 1';
                break;
                break;
            default:
                $order_key = 'id';
        }
        if($params['order'][1] == 'ASC') $order_dir = 'ASC';
        else $order_dir = 'DESC';

        $return_ary = array();

        $return_ary['sql'] = $this->MSFrameworkDatabase->getReplacedQuery("SELECT * FROM travelagency__packages WHERE $category_where AND $price_where AND $services_where AND $date_where AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);
        $return_ary['full_sql'] =  $return_ary['sql']  . " ORDER BY $order_key $order_dir LIMIT $start, " . $params['limit'];

        return $return_ary;

    }

}
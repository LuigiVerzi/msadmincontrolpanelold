<?php
/**
 * MSFramework
 * Date: 07/03/18
 */

namespace MSFramework\Attivita;


class reparti {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Reparto => Dati Reparto) con i dati relativi al servizio.
     *
     * @param $id L'ID del reparto (stringa) o dei reparti (array) richiesti (se vuoto, vengono recuperati i dati di tutti i servizi)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getRepartoDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM reparti WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }
}
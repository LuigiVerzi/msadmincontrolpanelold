<?php
/**
 * MSFramework
 * Date: 07/03/18
 */

namespace MSFramework\Attivita;


class team {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Ottiene la lista dei membri dello staff
     *
     * @param $id L'ID della categoria (stringa) o delle categorie (array) richiesti (se vuoto, vengono recuperati i dati di tutte le cateogire)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getTeamByCategoryID($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "category");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" category != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM negozio_team WHERE category != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {

            $r['propic_friendly'] = array();

            $propic_gallery = json_decode($r['propic'], true);
            if(is_array($propic_gallery)) {
                foreach($propic_gallery as $file) {
                    $r['propic_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_NEGOZIO_TEAM_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_NEGOZIO_TEAM_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_NEGOZIO_TEAM_FOR_DOMAIN . $file,
                            "thumb" => UPLOADNEGOZIO_TEAM_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene i dettagli del membro dello staff
     *
     * @param $id L'ID del membro dello staff da ottenere
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getTeamMemberDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM negozio_team WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {

            $r['propic_friendly'] = array();

            $propic_gallery = json_decode($r['propic'], true);
            if(is_array($propic_gallery)) {
                foreach($propic_gallery as $file) {
                    $r['propic_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_NEGOZIO_TEAM_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_NEGOZIO_TEAM_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_NEGOZIO_TEAM_FOR_DOMAIN . $file,
                            "thumb" => UPLOADNEGOZIO_TEAM_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene le informazioni di una determinata categoria
     *
     * @param $id L'ID della categoria (stringa) o delle categorie (array) richiesti (se vuoto, vengono recuperati i dati di tutte le cateogire)
     * @param boolean $include_team Se TRUE includerà la lista dei membri dello staff nella chiave 'team'
     *
     * @return array
     */
    public function getCategoryInfo($id = "", $include_team = false) {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM negozio_team_category WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
            if($include_team) {
                $ary_to_return[$r['id']]['team'] = $this->getTeamByCategoryID($r['id']);
            }
        }

        return $ary_to_return;
    }
}
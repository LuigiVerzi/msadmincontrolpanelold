<?php
/**
 * MSFramework
 * Date: 20/03/19
 */

namespace MSFramework\Attivita;


class sedi {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Sede => Dati Sede) con i dati relativi alla sede.
     *
     * @param $id L'ID della sede (stringa) o delle sedi (array) richieste (se vuoto, vengono recuperati i dati di tutte le sedi)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getSedeDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM negozio_sedi WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }


    /**
     * Restituisce l'inirizzo completo della sede
     *
     * @param $id_sede L'ID della sede per la quale ottenere l'indirizzo
     *
     * @return string
     */
    public function getFullAddress($id_sede) {
        $MSFrameworkGeonames = (new \MSFramework\geonames());

        $r = json_decode($this->getSedeDetails($id_sede, 'geo_data')[$id_sede]['geo_data'], true);

        if($r['indirizzo_visibile'] && !empty($r['indirizzo_visibile'])) return $r['indirizzo_visibile'];

        return $r['indirizzo'] . ", " . $r['cap'] . " " . $MSFrameworkGeonames->getDettagliComune($r['comune'], 'name')['name'] . " (" . $MSFrameworkGeonames->getDettagliProvincia($r['provincia'], 'name')['name'] . ")";
    }

    public function getURL($id) {
        global $MSFrameworkUrl, $MSFrameworki18n;

        if(is_array($id) && isset($id['slug'])) $page_det = $id;
        else $page_det = $this->getSedeDetails($id, "slug")[$id];

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";
    }

    /**
     * Restituisce la sede impostata per l'utente
     *
     * @param $user_id L'ID dell'utente
     *
     * @return mixed
     */
    public function getUserAgency($user_id) {
        return $this->MSFrameworkDatabase->getAssoc("SELECT agency FROM users WHERE id = :id", array(":id" => $user_id), true)['agency'];
    }
}
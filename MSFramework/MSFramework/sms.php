<?php
/**
 * MSFramework
 * Date: 10/03/18
 */

namespace MSFramework;

class sms {

    private $debugRecipients = array(
        '+393279335367' // Luigi
    );

    private $skebbyCredential = array(
        "username" => 'marketingstudio',
        "password" => 'Marketingh24'
    );

    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        Global $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkCustomers = new \MSFramework\customers();
    }

    /**
     * Invia un SMS
     */
    public function sendSMS($recipient, $message, $sender = '') {

        $payload = array(
            "message_type" => 'TI',
            "message" => $message,
            "recipient" => array(
                trim(str_replace(' ', '', $recipient))
            )
        );

        // Se ci troviamo in sviluppo invio un SMS
        if(FRAMEWORK_DEBUG_MODE) {
            $payload["recipient"] = $this->debugRecipients;
        }

        if($sender)  $payload['sender'] = $sender;

        $skebbySession = $this->getSkebbySession();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.skebby.it/API/v1.0/REST/sms');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'user_key: ' . $skebbySession[0],
            'Access_token: ' . $skebbySession[1],
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ($info['http_code'] == 201) {
            $return = json_decode($response, true);
            return $return;
        }

        return false;
    }

    private function getSkebbySession() {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.skebby.it/API/v1.0/REST/token?username=' . $this->skebbyCredential['username'] . '&password=' . $this->skebbyCredential['password']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ($info['http_code'] == 200) {
            $values = explode(";", $response);
            return $values;
        }

        return array();
    }

}
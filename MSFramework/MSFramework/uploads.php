<?php
/**
 * MSFramework
 * Date: 11/07/18
 */

namespace MSFramework;


class uploads {
    /**
     * Gli elementi presenti in questo array useranno il path common per la gestione degli uploads
     *
     * @var array
     */
    public $force_common = array("ASSISTENZA_TICKET");

    /**
     * uploads constructor.
     *
     * @param $uploader_type mixed Il tipo di uploader da gestire. I tipi di uploader gestibili sono disponibili nelle chiavi dell'$array_for_domain all'interno della funzione initConstants. Per utilizzare alcune funzioni, inizializzare la classe con questo parametro è fondamentale in quanto verranno determinati i percorsi (assoluti ed HTML) da utilizzare all'interno delle funzioni
     * @param $use_common bool Se impostato su true, l'upload verrà gestito nella cartella COMMON. L'array $force_common sovrascrive questa impostazione.
     *
     */
    function __construct($uploader_type = null, $use_common = false) {
        global $MSFrameworkSaaSBase;

        if(in_array($uploader_type, $this->force_common)) {
            $use_common = true;
        }

        $this->MSFrameworkSaaS = $MSFrameworkSaaSBase;
        $this->MSFrameworkSaaSEnvironments = new \MSFramework\SaaS\environments();

        $this->uploader_type = $uploader_type;
        $this->path = constant("UPLOAD_" . strtoupper($uploader_type) . "_FOR_DOMAIN");
        $this->path_html = constant("UPLOAD_" . strtoupper($uploader_type) . "_FOR_DOMAIN_HTML");

        if($MSFrameworkSaaSBase->isSaaSDomain()) {
            $this->saas_basepath = "SaaS/" . $this->MSFrameworkSaaSEnvironments->getDBName() . '/';
        }

        if($MSFrameworkSaaSBase->isSaaSDomain() && !$use_common) {
            if(FRAMEWORK_DEBUG_MODE) {
                $this->path_html = str_replace(MS_DOCUMENT_ROOT . "/" . FRAMEWORK_SERVER_NAME, PATH_TO_COMMON_STORAGE_HTML . $this->saas_basepath,  $this->path);
            }
            $this->path = str_replace(MS_DOCUMENT_ROOT . "/" . FRAMEWORK_SERVER_NAME, PATH_TO_COMMON_STORAGE . $this->saas_basepath,  $this->path);
        } else if($use_common) {
            $current_website_paths = CUSTOMER_DOMAIN_INFO;

            $this->path = str_replace($current_website_paths['path'], PATH_TO_COMMON_STORAGE, $this->path);
            $this->path_html = str_replace('//' . $current_website_paths['url'], PATH_TO_COMMON_STORAGE_HTML , $this->path_html);

            if($MSFrameworkSaaSBase->isSaaSDomain()) {
                $this->path = str_replace($this->saas_basepath . "/", "", $this->path);
                $this->path_html = str_replace($this->saas_basepath . "/", "", $this->path_html);
            }
        }
    }

    /**
     * Inizializza tutte le costanti (e la relativa versione _HTML) riservate agli uploads
     */
    public function initConstants() {
        //Il path assoluto della cartella che contiene tutti gli uploads
        if($this->MSFrameworkSaaS->isSaaSDomain()) {
            define("MAIN_UPLOAD_FOLDERPATH", PATH_TO_COMMON_STORAGE . $this->saas_basepath . "/" . UPLOAD_FOLDERPATH_NAME . "/");
        } else {
            define("MAIN_UPLOAD_FOLDERPATH", MAIN_SITE_FOLDERPATH . UPLOAD_FOLDERPATH_NAME . "/");
        }

        $array_for_domain = $this->getCostants();

        foreach($array_for_domain as $cur_const => $cur_path_data) {
            define("UPLOAD_" . $cur_const . "_FOR_DOMAIN", MAIN_UPLOAD_FOLDERPATH . $cur_path_data[0]);

            if($this->MSFrameworkSaaS->isSaaSDomain()) {
                define("UPLOAD_" . $cur_const . "_FOR_DOMAIN_HTML", PATH_TO_COMMON_STORAGE_HTML . $this->saas_basepath . "/" . UPLOAD_FOLDERPATH_NAME . "/" . $cur_path_data[0]);
            } else {
                $final_upload_html = UPLOADS_CDN . str_replace(MAIN_UPLOAD_FOLDERPATH, "/", constant("UPLOAD_" . $cur_const . "_FOR_DOMAIN"));

                //Se il dominio corrente contiene "www." e la cartella sul server non lo contiene all'interno del proprio nome (lo script fa riferimento al nome della cartella per determinare il path di queste costanti), lo aggiungo io al volo
                if(preg_match('/^www./', $_SERVER['HTTP_HOST']) && !preg_match('/^\/www./', ABSOLUTE_SW_PATH_HTML)) {
                    $final_upload_html = str_replace("//", "//www.", $final_upload_html);
                }

                define("UPLOAD_" . $cur_const . "_FOR_DOMAIN_HTML", $final_upload_html);
            }
        }
    }

    /**
     * Verifica se la classe è stata inizializzata in modo tale da gestire i path degli uploaders
     *
     * @return bool
     */
    private function hasPaths() {
        return ($this->path != null && $this->path_html != null);
    }

    /**
     * Inizializza la struttura HTML standard per un uploader
     *
     * @param integer $id L'ID dell'uploader
     * @param array $preload Un array di immagini da precaricare nell'uploader (in formato JSON)
     * @param array $aryClasses Un array di classi (dove la chiave è indica a quale elemento aggiungere i valori) da aggiungere all'elemento
     * @param array $aryDataAttr Un array di attributi data-* (dove la chiave è indica a quale elemento aggiungere i valori) da aggiungere all'elemento
     * @param boolean $showSettings Mostra o nascondi il pulsante con le impostazioni relative all'uploader
     *
     * @return boolean
     *
     */
    public function initUploaderHTML($id, $preload, $aryClasses = array(), $aryDataAttr = array(), $showSettings = true) {
        if(!$this->hasPaths()) {
            return false;
        }

        if(is_array($preload)) {
            $preload = json_encode($preload);
        } else if(!json_decode($preload)) {
            $preload = json_encode(array());
        }

        $uploader_type = $this->uploader_type;
        $path = $this->path_html;
        $abs_path = $this->path;

        include(ABSOLUTE_SW_PATH . "includes/template/uploader/htmlStruct.php");

        return true;
    }

    /**
     * Prepara l'elenco dei files per il salvataggio nel DB, effettuando la verifica dell'estensione ed altri controlli preliminari
     * 
     * @param $array L'array dei files da salvare
     * @param array $allowed_ext Le estensioni ammesse
     *
     * @return array|bool
     */
    public function prepareForSave($array, $allowed_ext = array("png", "jpeg", "jpg", "gif")) {
        Global $uploadsCopyMode;

        if(!$this->hasPaths()) {
            \ChromePhp::log('NoPath');
            return false;
        }

        mkdir($this->path, 0777, true);
        mkdir($this->path . "tn/", 0777, true);

        $ary_files = array();
        foreach($array as $file) {
            if(!in_array(pathinfo($file, PATHINFO_EXTENSION), $allowed_ext) || (!file_exists(UPLOAD_TMP_FOR_DOMAIN . $file) && !file_exists($this->path . $file))) {
                continue;
            }


            if(isset($uploadsCopyMode) && $uploadsCopyMode && file_exists($this->path . $file)) {
                if (copy($this->path . $file, $this->path . 'c_' . $file)) {
                    copy($this->path . "tn/" . $file, $this->path . "tn/" . 'c_' .  $file);
                    $file = 'c_' . $file;
                }
            } else {

                if (!file_exists($this->path . $file)) {
                    if (rename(UPLOAD_TMP_FOR_DOMAIN . $file, $this->path . $file)) {
                        rename(UPLOAD_TMP_FOR_DOMAIN . "tn/" . $file, $this->path . "tn/" . $file);
                    } else {
                        return false;
                    }
                }
            }

            $ary_files[] = $file;
        }

        return $ary_files;
    }

    /**
     * Elimina i files
     *
     * @param $files Il file (o i files, sotto forma di array) da eliminare
     * @param string $files_diff Se valorizzato, elimina solo i files di $files_diff che non sono presenti in $files
     *
     * @return bool
     */
    public function unlink($files, $files_diff = "") {
        if($this->path == null || $files == "") {
            return false;
        }

        if(!is_array($files)) {
            $files = array($files);
        }

       if(is_null($files_diff) || $files_diff == "null") {
           $files_diff = array();
       }
       
        if($files_diff != "" && !is_array($files_diff)) {
            $files_diff = array($files_diff);
        }

        if(!is_array($files_diff)) {
            foreach ($files as $file) {
                unlink($this->path . $file);
                unlink($this->path . "tn/" . $file);
            }
        } else {
            foreach($files_diff as $old_file) {
                if(!in_array($old_file, $files)) {
                    unlink($this->path . $old_file);
                    unlink($this->path . "tn/" . $old_file);
                }
            }
        }
        
        return true;
    }

    /**
     * Ottiene la lista delle costanti
     *
     * @return array
     */
    public function getCostants() {
        $array_for_domain = array(
            "TMP" => array("tmp/"),
            "PROPIC" => array("img/propics/"),
            "CUSTOMER_AVATAR" => array("img/customer/avatar/"),
            "LOGOS" => array("img/logos/"),
            "ROOMGALLERY" => array("img/rooms/gallery/"),
            "CAMPING_PIAZZOLE" => array("img/camping/piazzole/gallery/"),
            "OFFERSGALLERY" => array("img/offers/gallery/"),
            "PAGEGALLERY" => array("img/pages/gallery/"),
            "GALLERY" => array("img/gallery/"),
            "SLIDERS" => array("img/sliders/"),
            "BLOG_CATEGORIES" => array("img/blog/categories/"),
            "BLOG_POSTS" => array("img/blog/posts/"),
            "ECOMMERCE_CATEGORIES" => array("img/ecommerce/categories/"),
            "ECOMMERCE_PRODUCTS" => array("img/ecommerce/products/"),
            "ECOMMERCE_ATTACHMENTS" => array("img/ecommerce/attachments/"),
            "ECOMMERCE_ATTRIBUTES" => array("img/ecommerce/attributes/"),
            "ECOMMERCE_BRAND" => array("img/ecommerce/brand/"),
            "ECOMMERCE_PAYMENTS_LOGOS" => array("img/ecommerce/payments/"),
            "WIDGET" => array("img/widget/"),
            "FAQ" => array("img/faq/"),
            "REVIEWS" => array("img/reviews/"),
            "PAGEWIDGET" => array("img/pages/widget/"),
            "POPUP" => array("img/popup/"),
            "MODULES" => array("img/modules/"),
            "RESTAURANT_MENU" => array("img/restaurant/menu/"),
            "NEGOZIO_TEAM" => array("img/negozio/team/"),
            "NEGOZIO_SEDI" => array("img/negozio/sedi/"),
            "NEWSLETTER" => array("img/newsletter/"),
            "NEWSLETTER_TEMPLATES" => array("img/newsletter/templates/"),
            "MAILBOX" => array("img/mailbox/"),
            "MAILBOX_TEMPLATES" => array("img/mailbox/templates/"),
            "MARKETING_AUTOMATIONS" => array("img/marketing/automations/"),
            "PACKAGES" => array("img/packages/"),
            "SERVICES" => array("img/services/"),
            "SERVICES_CATEGORY" => array("img/services/category/"),
            "SERVICES_SECTOR" => array("img/services/sector/"),
            "SERVICES_PORTFOLIO" => array("img/services/portfolio/"),
            "FORMS" => array("img/forms/"),
            "NOTIFICATIONS" => array("img/notifications/"),
            "BEAUTYOFFERSGALLERY" => array("img/beautycenter/offers/"),
            "FATTURAZIONE_CASSE" => array("img/fatturazione/casse/"),
            "FATTURAZIONE_FATTURE" => array("img/fatturazione/fatture/"),
            "TRAVELAGENCY_PACKAGE" => array("img/travelagency/packages/"),
            "TRAVELAGENCY_PACKAGE_HOTEL" => array("img/travelagency/packages_hotels/"),
            "TRAVELAGENCY_PACKAGE_CATEGORY" => array("img/travelagency/packages_category/"),
            "SUBSCRIPTION_PLANS" => array("img/subscriptions/plans/"),
            "REALESTATE_IMMOBILI" => array("img/realestate/immobili/"),
            "REALESTATE_PLANIMETRIE" => array("img/realestate/planimetrie/"),
            "REALESTATE_CATEGORIES" => array("img/realestate/categories/"),
            "ASSISTENZA_TICKET" => array("img/assistenza/ticket/"),
            "APPEARANCE_THEME" => array("img/appearance/themes/"),
        );

        return $array_for_domain;
    }

    /**
     * Restituisce un array già pronto da utilizzare (completo di path) per l'array di immagini passato
     *
     * @param $array L'array delle immagini per le quali restituire le informazioni
     * @param $const Il nome della costante (nel formato indicato dalla funzione getCostants()) per la costruzione del path
     *
     * @return array
     */
    public function composeGalleryFriendlyArray($array) {
        $return = array();

        foreach ($array as $file) {
            $return[] = array(
                "html" => array(
                    "main" => $this->path_html . $file,
                    "thumb" => $this->path_html . "tn/" . $file
                ),
                "absolute" => array(
                    "main" => $this->path . $file,
                    "thumb" => $this->path . "tn/" . $file
                ),
            );
        }

        return $return;
    }

    /* BLOB */

    /**
     * Estrapola le immagini blob dal codice HTML e li carica sul server
     *
     * @param $html L'HTML contenente i file blog
     * @param array $allowed_ext Le estensioni ammesse
     *
     * @return string Ritorna il codice HTML con le path delle immagini
     */
    public function saveFromBlob($html, $allowed_ext = array("png", "jpeg", "jpg")) {
        if(!$this->hasPaths()) {
            \ChromePhp::log('NoPath');
            return false;
        }

        mkdir($this->path, 0777, true);
        mkdir($this->path . "tn/", 0777, true);

        preg_match_all(
            '/src=\"(data:image\/([a-zA-Z]*);base64,([^\"]*))\"/m',
            str_replace('\"', '"', $html),
            $currentBlogImages,
            PREG_SET_ORDER,
            0
        );

        preg_match_all(
            '/"([0-9]{10}[^.]+.[^"]+)"/m',
            str_replace('\"', '"', $html),
            $currentInlineImages,
            PREG_SET_ORDER,
            0
        );

        // Carica le immagini BLOB sul server
        foreach($currentBlogImages as $currentBlogImage) {
            $preview_image = str_replace(' ', '+', $currentBlogImage[3]);
            $preview_image = base64_decode($preview_image);
            $new_image_name = uniqid('blob_', true) . '_editor_image.' . $currentBlogImage[2];
            file_put_contents($this->path . $new_image_name, $preview_image);
            $html = str_replace($currentBlogImage[1], $this->path_html . $new_image_name, $html);
        }

        // Carica le immagini Inline sul server
        foreach($currentInlineImages as $currentInlineImage) {
            $new_image_name = $currentInlineImage[1];
            if (rename(UPLOAD_TMP_FOR_DOMAIN .  $currentInlineImage[1], $this->path . $new_image_name)) {
                rename(UPLOAD_TMP_FOR_DOMAIN . "tn/" .  $currentInlineImage[1], $this->path . "tn/" . $new_image_name);
                $html = str_replace($currentInlineImage[1], $new_image_name, $html);
            }

        }

        return $html;
    }

    /**
     * Elimina i files
     *
     * @param $current_html string Il codice HTML attuale
     * @param string $old_html Se valorizzato, elimina solo i files di $old_html che non sono presenti in $current_html
     *
     * @return bool
     */
    public function unlinkFromHTML($current_html, $old_html = "") {
        if($this->path == null || $current_html == "") {
            return false;
        }

        $current_html = str_replace('\"', '"', $current_html);
        $old_html = str_replace('\"', '"', $old_html);

        $image_pattern = '/[0-9]{10}[^.]+.[^"\'\\\]+|blob_[0-9|a-z|\.]+_editor_image\.[^"\'\\\]+/m';

        preg_match_all(
            $image_pattern,
            $current_html,
            $currentUploadedImages,
            PREG_PATTERN_ORDER,
            0
        );

        $files_to_delete = array();

        if($old_html) {

            preg_match_all(
                $image_pattern,
                $old_html,
                $oldUploadedImage,
                PREG_PATTERN_ORDER,
                0
            );

            foreach ($oldUploadedImage[0] as $old_image) {
                if (!in_array($old_image, $currentUploadedImages[0])) {
                    $files_to_delete[] = $old_image;
                }
            }
        } else {
            foreach ($currentUploadedImages[0] as $current_image) {
                $files_to_delete[] = $current_image;
            }
        }

        foreach ($files_to_delete as $file_to_delete) {
            unlink($this->path . $file_to_delete);
            unlink($this->path . 'tn/' . $file_to_delete);
        }

        return true;
    }

}
<?php
/**
 * MSFramework
 * Date: 30/04/18
 */

namespace MSFramework;


class geonames {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    public function getCountriesList($fields = "*") {
        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_countryinfo` ORDER BY name");
    }

    public function getRegionsList($country_id, $fields = "*") {
        $r_country_data = $this->MSFrameworkDatabase->getAssoc("SELECT iso_alpha2 FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_countryinfo` WHERE geonameid = :id", array(":id" => $country_id), true);

        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin1Codes` WHERE MID(CODE, 1, 2) = :code ORDER BY name", array(":code" => $r_country_data['iso_alpha2']));
    }

    public function getProvinceList($region_id, $fields = "*") {
        $r_region_data = $this->MSFrameworkDatabase->getAssoc("SELECT code FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin1Codes` WHERE geonameid = :id", array(":id" => $region_id), true);

        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin2Codes` WHERE MID(CODE, 1, 5) = :code ORDER BY name", array(":code" => $r_region_data['code']));
    }

    public function getComuniList($provincia_id, $fields = "*") {
        $r_region_data = $this->MSFrameworkDatabase->getAssoc("SELECT code FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin2Codes` WHERE geonameid = :id", array(":id" => $provincia_id), true);
        $code = explode(".", $r_region_data['code']);

        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`geoname` WHERE country = :country AND admin1 = :admin1 AND admin2 = :admin2 GROUP BY name ORDER BY name", array(":country" => $code[0], ":admin1" => $code[1], ":admin2" => $code[2]));
    }

    public function getDettagliComune($id, $fields = "*") {
        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`geoname` WHERE geonameid = :id", array(":id" => $id), true);
    }

    public function getDettagliProvincia($id, $fields = "*") {
        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin2Codes` WHERE geonameid = :id", array(":id" => $id), true);
    }

    public function getDettagliRegione($id, $fields = "*") {
        return $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin1Codes` WHERE geonameid = :id", array(":id" => $id), true);
    }

    /**
     * Ricerca l'ID del comune. E' possibile effettuare la ricerca per nome o per codice
     *
     * @param $code Il nome o il codice del comune
     * @param string $type name/code
     * @param bool $approx Se impostato su "true", in caso di mancanza di match con una where esatta, prova ad effettuare una ricerca con una where like
     *
     * @return mixed
     */
    public function searchForComuneID($code, $type = "name", $approx = true) {
        if($type == "name") {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname` WHERE name = :name", array(":name" => $code), true);
            if($r['geonameid'] == "" && $approx) {
                $r = $this->MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname` WHERE name LIKE :name", array(":name" => '%' . $code . '%'), true);
            }
        } else if($type == "code") {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname` WHERE admin3 = :code", array(":code" => $code), true);
        }

        return $r['geonameid'];
    }

    /**
     * Ricerca l'ID della provincia. E' possibile effettuare la ricerca per nome o per codice
     *
     * @param $code Il nome o il codice della provincia
     * @param string $type name/code
     * @param bool $approx Se impostato su "true", in caso di mancanza di match con una where esatta, prova ad effettuare una ricerca con una where like
     *
     * @return mixed
     */
    public function searchForProvinciaID($code, $type = "name", $approx = true) {
        if($type == "name") {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin2Codes` WHERE name = :name", array(":name" => $code), true);
            if($r['geonameid'] == "" && $approx) {
                $r = $this->MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin2Codes` WHERE name LIKE :name", array(":name" => '%' . $code . '%'), true);
            }
        } else if($type == "code") {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin2Codes` WHERE code = :code", array(":code" => $code), true);
        }

        return $r['geonameid'];
    }

    /**
     * Ricerca l'ID della regione. E' possibile effettuare la ricerca per nome o per codice
     *
     * @param $code Il nome o il codice della regione
     * @param string $type name/code
     * @param bool $approx Se impostato su "true", in caso di mancanza di match con una where esatta, prova ad effettuare una ricerca con una where like
     *
     * @return mixed
     */
    public function searchForRegioneID($code, $type = "name", $approx = true) {
        if($type == "name") {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin1Codes` WHERE name = :name", array(":name" => $code), true);
            if($r['geonameid'] == "" && $approx) {
                $r = $this->MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin1Codes` WHERE name LIKE :name", array(":name" => '%' . $code . '%'), true);
            }
        } else if($type == "code") {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT geonameid FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_admin1Codes` WHERE code = :code", array(":code" => $code), true);
        }

        return $r['geonameid'];
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }
    
    /**
     * Restituisce un array associativo (ID Nazione => Dati Nazione) con i dati relativi alla Nazione
     *
     * @param string $id L'ID della Nazione (stringa) o delle Nazioni (array) richieste (se vuoto, vengono recuperati i dati di tutte le nazioni)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCountryDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "geonameId");
        } else {
            $same_db_string_ary = array(" geonameId != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "geonameId")) {
            $fields .= ", geonameId";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_countryinfo` WHERE geonameId != '' AND (" . $same_db_string_ary[0] . ") ORDER BY name ASC", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['geonameId']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Nazione => Dati Nazione) con i dati relativi alla Nazione
     *
     * @param string $col La conolla da controllare
     * @param string $val Il valore della colonna
     *
     * @return integer
     */
    public function getCountryIDBy($col, $val) {

        $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($val, "OR", $col);

        $country_id = 0;
        $country_info = $this->MSFrameworkDatabase->getAssoc("SELECT geonameId FROM `" . FRAMEWORK_DB_NAME . "`.`geoname_countryinfo` WHERE " . $same_db_string_ary[0], $same_db_string_ary[1], true);
        if($country_info) {
            $country_id = $country_info['geonameId'];
        }
        return $country_id;
    }

    /**
     * Ottiene l'indirizzo di una determinata coordinata
     *
     * @param $lat La latitudine
     * @param $lng La longitudine
     */
    public function getAddressFromLatLng($lat, $lng) {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=false&key=".GMAPS_API_KEY;

        $response = file_get_contents($url);
        $json = json_decode($response,true);

        if($json && is_array($json['results'][0]['address_components'])) {
            return $json['results'][0]['address_components'];
        }

        return false;
    }

    /**
     * Questa funzione analizza l'array generato dalla funzione JS getGeoDataAryToSave() (che viene passata ai file saveData.php che salvano anche i dati geonames) e blocca l'esecuzione se qualche parametro non è corretto
     *
     * @param bool $check_mandatory Se impostato su "true", verifica anche l'obbligatorietà dei campi geonames (non in tutti i moduli sono obbligatori)
     * @param array $data Se impostato, i controlli vengono effettuati su quest'array e non su $_POST['pGeoData']
     *
     * @return bool
     */
    public function checkDataBeforeSave($check_mandatory = false, $data = "") {
        $data_ary = $_POST['pGeoData'];
        if(is_array($data)) {
            $data_ary = $data;
        }

        if($check_mandatory) {
            if($data_ary['cap'] == "" || $data_ary['indirizzo'] == "" || $data_ary['regione'] == "" || $data_ary['provincia'] == "" || $data_ary['comune'] == "") {
                echo json_encode(array("status" => "mandatory_data_missing"));
                die();
            }
        }

        if($data_ary['indirizzo'] != "" && ($data_ary['regione'] == "" || $data_ary['provincia'] == "" || $data_ary['comune'] == "")) {
            echo json_encode(array("status" => "address_without_city"));
            die();
        }

        if($data_ary['lat'] != "" && !preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $data_ary['lat'])) {
            echo json_encode(array("status" => "lat_not_valid"));
            die();
        }

        if($data_ary['lng'] != "" && !preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $data_ary['lng'])) {
            echo json_encode(array("status" => "lng_not_valid"));
            die();
        }

        return true;
    }
}
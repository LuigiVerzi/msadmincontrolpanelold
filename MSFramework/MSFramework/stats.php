<?php
/**
 * MSFramework
 * Date: 17/02/18
 */

namespace MSFramework;


class stats {

    public $actionTables = array(
        'getBlogPost' => array(
            'id_col' => 'article_id',
            'stats' => 'blog_stats',
            'specific' => 'blog_posts'
        ),
        'getEcommerceProduct' => array(
            'id_col' => 'product_id',
            'stats' => 'ecommerce_product_stats',
            'specific' => 'ecommerce_products'
        )
    );

    public function __construct() {
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Salva la visita del visitatore nel DB
     *
     * @return mixed
     */
    public function saveVisitorVisit($ref = array()) {

        if($this->isAnBot()) return false;

        $ipaddress = $_SERVER['REMOTE_ADDR'];
        $day = date("Y-m-d");
        $hour = date("H");
        $minute = date("m");

        if(!isset($_SESSION['visit_saved'])) {
            $_SESSION['visit_saved'] = true;

            $already_stored = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM stats_visitatori WHERE ip LIKE '$ipaddress' AND day LIKE '$day' AND hour LIKE '$hour'", array(), true);
            if ($already_stored) {
                $db_id = $already_stored['id'];
                $this->MSFrameworkDatabase->query("UPDATE stats_visitatori SET n_visite = n_visite+1 WHERE id = $db_id");
            } else {
                $this->MSFrameworkDatabase->query("INSERT INTO stats_visitatori (ip, day, hour, minute, n_visite) VALUES ('$ipaddress', '$day', '$hour', '$minute', 1)");
            }
        }

        if($ref && isset($this->actionTables[$ref['type']])) {

            $table = $this->actionTables[$ref['type']]['stats'];
            $ref_id = $ref['id'];

            if(!isset($_SESSION[$table . '_' . $ref_id . '_visit_saved'])) {
                $_SESSION['article_' . $ref_id . '_visit_saved'] = true;

                $already_stored = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM $table WHERE " . $this->actionTables[$ref['type']]['id_col'] . " = '$ref_id' AND  ip LIKE '$ipaddress' AND day LIKE '$day' AND hour LIKE '$hour'", array(), true);

                if ($already_stored) {
                    $db_id = $already_stored['id'];
                    $this->MSFrameworkDatabase->query("UPDATE $table SET n_visite = n_visite+1 WHERE id = $db_id");
                } else {
                    $this->MSFrameworkDatabase->query("INSERT INTO $table (ip, " . $this->actionTables[$ref['type']]['id_col'] . ", day, hour, minute, n_visite) VALUES ('$ipaddress', $ref_id, '$day', '$hour', '$minute', 1)");
                }

                if(isset($this->actionTables[$ref['type']]['specific'])) {
                    $this->MSFrameworkDatabase->query("UPDATE " . $this->actionTables[$ref['type']]['specific'] . " SET visite = visite+1 WHERE id = $ref_id");
                }
            }
        }

        return true;
    }

    /**
     * Salva la visita del visitatore nel DB
     *
     * @return mixed
     */
    public function saveVisitorNavigationTime($time, $ref = array()) {

        if($this->isAnBot()) return false;

        $ipaddress = $_SERVER['REMOTE_ADDR'];
        $day = date("Y-m-d");
        $hour = date("H");

        $already_stored = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM stats_visitatori WHERE ip LIKE '$ipaddress' AND day LIKE '$day' AND hour LIKE '$hour'", array(),true);
        if($already_stored)
        {
            $db_id = $already_stored['id'];
            $this->MSFrameworkDatabase->query("UPDATE stats_visitatori SET tempo_medio = (tempo_medio+$time) WHERE id = $db_id");
        }

        // Se la visita arriva da un articolo di blog allora salvo le statistiche riguardo l'articolo
        if($ref && isset($this->actionTables[$ref['type']])) {
            $table = $this->actionTables[$ref['type']]['stats'];
            $ref_id = $ref['id'];

            $already_stored = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM $table WHERE " . $this->actionTables[$ref['type']]['id_col'] . " = '$ref_id' AND ip LIKE '$ipaddress' AND day LIKE '$day' AND hour LIKE '$hour'", array(),true);
            if($already_stored) {
                $this->MSFrameworkDatabase->query("UPDATE $table SET tempo_medio = (tempo_medio+$time) WHERE id = $ref_id");
            }
        }

        return true;
    }

    /**
     * Controlla se l'utente è un bot
     *
     * @return boolean
     */
    public function isAnBot() {

        // User lowercase string for comparison.
        $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);

        // A list of some common words used only for bots and crawlers.
        $bot_identifiers = array(
            'bot',
            'slurp',
            'patrol',
            'crawler',
            'crawl',
            'spider',
            'curl',
            'facebook',
            'fetch',
            'whatsapp',
            'mediapartners'
        );

        // See if one of the identifiers is in the UA string.
        foreach ($bot_identifiers as $identifier) {
            if (strpos($user_agent, $identifier) !== FALSE) {
                return TRUE;
            }
        }

        return FALSE;
    }


}
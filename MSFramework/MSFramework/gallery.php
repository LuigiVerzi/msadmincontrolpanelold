<?php
/**
 * MSFramework
 * Date: 02/03/18
 */

namespace MSFramework;


class gallery {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Gallery => Dati Gallery) con i dati relativi alla gallery.
     * L'array contiene (in una chiave addizionale 'gallery_friendly' aggiunta al volo) sia le informazioni da utilizzare nel DOM (HTML) che il path assoluto delle immagini
     *
     * @param $id L'ID della gallery (stringa) o delle gallery (array) richieste (se vuoto, vengono recuperati i dati di tutte le gallery)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getGalleryDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM gallery WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $page_gallery = json_decode($r['gallery'], true);
            if(is_array($page_gallery)) {

                $r['gallery_friendly'] = array();
                foreach($page_gallery as $k => $file) {

                    $titolo = '';
                    $testo = '';
                    if(is_array($file)) {
                        $titolo = $file[1];
                        $testo = $file[2];
                        $file = $file[0];
                        $page_gallery[$k] = $file;
                    }

                    $r['gallery_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_GALLERY_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_GALLERY_FOR_DOMAIN_HTML . "tn/" . $file,
                            "titolo" => $titolo,
                            "testo" => $testo
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_GALLERY_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_GALLERY_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }

                // Mostro il vecchio JSON con sole immagini per evitare conflitti con i vecchi sistemi
                $r['gallery'] = json_encode($page_gallery);
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }
}
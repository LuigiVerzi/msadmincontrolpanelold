<?php
/**
 * MSFramework
 * Date: 05/03/18
 */

namespace MSFramework;


class popup {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Popup => Dati Popup) con i dati relativi al blocco.
     *
     * @param mixed $id L'ID del blocco (stringa) o dei blocchi (array) richieste (se vuoto, vengono recuperati i dati di tutti i blocchi)
     * @return array
     */
    public function getPopupDetails($id = "") {
        global $firephp, $MSFrameworki18n;

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }
            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM popup WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $r['options'] = (json_decode($r['options']) ? json_decode($r['options'], true) : array());
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    public function drawPopupHtml($id, $content, $popupClasses, $popupStyles, $appearance) {
        Global $MSFrameworkVisualBuilder, $MSFrameworki18n;

        $popupHtml = array();

        $popupHtml[] = '<div class="ms-popup ' . implode(' ', $popupClasses) . '" id="ms-popup-' . $id . '" style="display: none;">';
        $popupHtml[] = '<div class="ms-popup-container animated" style="' . implode(';', $popupStyles) . '" data-in="' . $appearance['in-animation'] . '" data-out="' . $appearance['out-animation'] . '">';
        $popupHtml[] = '<div class="ms-popup-header">';
        $popupHtml[] = '<a class="ms-popup-close"></a>';
        $popupHtml[] = '</div>';
        $popupHtml[] = '<div class="ms-popup-content">';
        $popupHtml[] = $MSFrameworkVisualBuilder->draw($MSFrameworki18n->getFieldValue($content));
        $popupHtml[] = '</div>';
        $popupHtml[] = '</div>';
        $popupHtml[] = '</div>';

        return implode( '', $popupHtml);
    }
}
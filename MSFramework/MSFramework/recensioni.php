<?php
/**
 * MSFramework
 * Date: 09/03/18
 */

namespace MSFramework;


class recensioni {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }
    /**
     * Restituisce tutte le possibili provenienze delle recensioni
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return array|mixed
     */
    public function getProvenienze($key = "") {
        $types = array(
            "tripadvisor" => array("name" => "TripAdvisor"),
            "booking" => array("name" => "Booking"),
            "facebook" => array("name" => "Facebook"),
            "google" => array("name" => "Google"),
        );

        if($key != "") {
            return $types[$key];
        } else {
            return $types;
        }
    }

    /**
     * Restituisce un array associativo (ID Recensione => Dati Recensione) con i dati relativi alla recensione.
     *
     * @param $id L'ID della recensione (stringa) o delle recensioni (array) richiesti (se vuoto, vengono recuperati i dati di tutte le recensioni)
     * @param string $fields I campi da prelevare dal DB
     * @param string $lang Se impostato, limita la ricerca ai record appartenenti alla lingua richiesta (ID)
     *
     * @return array
     */
    public function getRecensioniDetails($id = "", $fields = "*", $lang = "") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        $str_lang_filter_str = "";
        $str_lang_filter_ary = array();
        if($lang != "") {
            $str_lang_filter_str = " AND lang = :lang ";
            $str_lang_filter_ary[':lang'] = $lang;
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM recensioni WHERE id != '' AND (" . $same_db_string_ary[0] . ") $str_lang_filter_str", array_merge($str_lang_filter_ary, $same_db_string_ary[1])) as $r) {

            $r['gallery_friendly'] = array();

            $testimonial_gallery = json_decode($r['gallery'], true);
            if(is_array($testimonial_gallery)) {
                foreach ($testimonial_gallery as $file) {
                    $r['gallery_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_REVIEWS_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_REVIEWS_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_REVIEWS_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_REVIEWS_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                    );
                }
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene il path (HTML) del logo relativo alla provenienza della recensione
     *
     * @param $provenienza La provenienza della recensione (colonna "provenienza" del db)
     * @param string $logo_type Il formato del logo da caricare
     * @param string $background_type light/dark in base al tipo di sfondo su cui sarà applicato il logo
     *
     * @return string
     */
    public function getPathLogoProvenienza($provenienza, $logo_type = "square", $background_type = "light") {
        return FRAMEWORK_COMMON_CDN . "img/reviews_logos/" . $logo_type . "/" . $background_type . "_bg/" . $provenienza . ".png";
    }
}
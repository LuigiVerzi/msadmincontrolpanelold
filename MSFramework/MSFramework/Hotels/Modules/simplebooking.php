<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Hotels\Modules;

class simplebooking {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $simplebooking_id = $this->modules->getSettingValue('simplebooking', 'simplebooking_id');

        if($simplebooking_id) {
            $files_to_include = array(
                'js' => array(
                    'https://www.simplebooking.it/search-box-script.axd?IDA=' . $simplebooking_id,
                    FRAMEWORK_COMMON_CDN . 'modules/hotel/simplebooking/js/simplebooking.js',
                )
            );
        } else return array();

        return $files_to_include;
    }

    /**
     * Ottiene le impostazioni del modulo
     *
     * @return array
     */
    public function getModuleSettings()
    {

        $module_settings = array(
            'simplebooking_id' => $this->modules->getSettingValue('simplebooking', 'simplebooking_id')
        );

        return $module_settings;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {

        return true;

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {

        $array_settings = array(
            "simplebooking_id" => array(
                "name" => "ID SimpleBooking",
                "input_type" => "number",
                "input_attributes" => array(),
                "helper" => "L'ID dell'account su SimpleBooking",
                "mandatory" => false,
                "translations" => false,
                "default" => '0',
            )
        );

        return $array_settings;
    }
}
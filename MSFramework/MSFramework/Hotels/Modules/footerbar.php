<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Hotels\Modules;

class footerbar {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;

        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array(
            'js' => array(
                //FRAMEWORK_COMMON_CDN . 'modules/hotel/footerbar/js/footerbar.js',
            ),
            'css' => array(
                FRAMEWORK_COMMON_CDN . 'modules/hotel/footerbar/css/footerbar.css',
            ),
            'html' => array(
                FRAMEWORK_ABSOLUTE_PATH . 'common/modules/hotel/footerbar/php/footerbar.php',
            )
        );

        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {

        return true;

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $array_settings = array(
            "color" => array(
                "name" => "Colore Principale",
                "input_type" => "text",
                "input_attributes" => array(),
                "helper" => "Il colore principale della barra",
                "mandatory" => false,
                "translations" => false,
                "default" => '#000000',
            ),
            "link_prenotazione" => array(
                "name" => "Link Prenotazione",
                "input_type" => "text",
                "input_attributes" => array(),
                "helper" => "Il link da aprire al click sull'icona della prenotazione",
                "mandatory" => false,
                "translations" => false,
                "default" => '#booking_request_detail',
            ),
            "testo_prenotazione" => array(
                "name" => "Testo Pulsante Prenota",
                "input_type" => "text",
                "input_attributes" => array(),
                "helper" => "Il testo del pulsante Prenota",
                "mandatory" => false,
                "translations" => true,
                "default" => 'Prenota',
            ),
            "icona_prenotazione" => array(
                "name" => "Icona Pulsante Prenota",
                "input_type" => "text",
                "input_attributes" => array(),
                "helper" => "L'icona del pulsante Prenota",
                "mandatory" => false,
                "translations" => false,
                "default" => 'fa fa-calendar',
            ),
        );

        if($this->modules->checkIfIsActive('simplebooking')) {
            $array_settings["use_simplebooking"] = array(
                "name" => "Utilizza SimpleBooking",
                "input_type" => "checkbox",
                "input_attributes" => array(),
                "helper" => "Selezionando questo campo il pulsante Prenota porterà a SimpleBooking invece che al modulo richiesta preventivo",
                "mandatory" => false,
                "translations" => false,
                "default" => false,
            );
        }

        return $array_settings;
    }
}
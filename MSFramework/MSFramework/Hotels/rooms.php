<?php
/**
 * MSFramework
 * Date: 15/02/18
 */

namespace MSFramework\Hotels;


class rooms {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Stanza => Dati Stanza) con i dati relativi alla stanza.
     * L'array contiene (in una chiave addizionale 'room_friendly' aggiunta al volo) sia le informazioni da utilizzare nel DOM (HTML) che il path assoluto delle immagini
     *
     * @param $id L'ID della stanza (stringa) o delle stanze (array) richieste (se vuoto, vengono recuperati i dati di tutte le stanze)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getRoomDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM hotel_rooms WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $page_gallery = json_decode($r['gallery'], true);
            if(is_array($page_gallery)) {

                $r['room_friendly'] = array();
                foreach($page_gallery as $file) {
                    $r['room_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_ROOMGALLERY_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_ROOMGALLERY_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_ROOMGALLERY_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_ROOMGALLERY_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }

            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria.
     *
     * @param $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM hotel_rooms_cats WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene le camere della stessa categoria di quella specificata nel parametro $room_id. Restituisce gli ID delle camere trovate.
     *
     * @param $room_id L'ID della camera di riferimento
     * @param int $limit Limita i risultati al numero specificato. -1 = nessun limite
     * @param bool $set_rand Ordina i risultati in maniera random
     *
     * @return array
     */
    public function getRoomsOfSameCategory($room_id, $limit = -1, $set_rand = true) {
        $r = $this->MSFrameworkDatabase->getAssoc("SELECT category FROM hotel_rooms WHERE id = :id", array(":id" => $room_id), true);
        $cat_to_search = $r['category'];

        $rooms_list = $this->getRoomsByCategory($cat_to_search, $limit, $set_rand);

        $key = array_search($room_id, $rooms_list);
        if (false !== $key) {
            unset($rooms_list[$key]);
        }

        return $rooms_list;

    }

    /**
     * Ottiene le camere della stessa categoria di quella specificata nel parametro $room_id. Restituisce gli ID delle camere trovate.
     *
     * @param $cat_to_search L'ID della categoria di riferimento
     * @param int $limit Limita i risultati al numero specificato. -1 = nessun limite
     * @param bool $set_rand Ordina i risultati in maniera random
     *
     * @return array
     */
    public function getRoomsByCategory($cat_to_search, $limit = -1, $set_rand = true) {

        if($cat_to_search != "") {
            $str_limit = "";
            if($limit != -1) {
                $str_limit = " LIMIT " . $str_limit;
            }

            $str_order = "";
            if($set_rand) {
                $str_order = " ORDER BY rand() ";
            }

            $ary_to_return = array();
            foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM hotel_rooms WHERE category = :cat  $str_order $str_limit", array(":cat" => $cat_to_search)) as $r) {
                $ary_to_return[] = $r['id'];
            }

            return $ary_to_return;
        }

        return array();
    }

    /**
     * Ottiene l'URL della stanza
     *
     * @param $id L'ID della stanza per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n;
        global $MSFrameworkUrl;

        if(is_array($id)) {
            $page_det = $id;
        }
        else {
            $page_det = $this->getRoomDetails($id, "slug")[$id];
        }

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) .  "/";
    }
}
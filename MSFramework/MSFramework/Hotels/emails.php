<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

namespace MSFramework\Hotels;

class emails extends \MSFramework\emails {
    /**
     * Ottiene la lista di template per l'ecommerce
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {
        $default_address = $this->MSFrameworkCMS->getCMSData("settings")['destination_address'];

        $templates = array(
            'hotel' => array(
                'hotel_booking_request' => array(
                    'nome' => 'Nuova Richiesta di Prenotazione',
                    'reply_to' => array("{[detail][email]}", "{[detail][name]}"),
                    'address' => $default_address,
                    'subject' => "Nuova Prenotazione Ricevuta - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{name}" => "Il nome del cliente",
                            "{surname}" => "Il cognome del cliente",
                            "{email}" => "L'email del cliente",
                            "{phone}" => "Il telefono del cliente",
                            "{arrival}" => "La data di arrivo",
                            "{departure}" => "La data di partenza",
                            "{adults}" => "Il numero di adulti",
                            "{children}" => "Il numero di bambini",
                            "{request}" => "Le informazioni addizionali",
                            "{request_url}" => "L'url della richiesta"
                        ),
                        array(
                            "{[detail][name]}",
                            "{[detail][surname]}",
                            "{[detail][email]}",
                            "{[detail][phone]}",
                            function($params, $template_settings) {
                                return (new \DateTime)::createFromFormat('!d/m/Y', $params['date']['arrival'])->format('Y-m-d');
                            },
                            function($params, $template_settings) {
                                return (new \DateTime)::createFromFormat('!d/m/Y', $params['date']['departure'])->format('Y-m-d');
                            },
                            "{[roommate][adults]}",
                            "{[roommate][children]}",
                            function($params, $template_settings) {

                                $additionals_html = '';
                                if(is_array($params['detail']['additional'])) {
                                    foreach($params['detail']['additional'] as $ad_name => $ad_val) {
                                        $additionals_html .= '<b>'. $ad_name . '</b><br>' . $ad_val . '<hr>';
                                    }
                                }
                                else {
                                    $additionals_html = $params['detail']['additional'];
                                }

                                return $additionals_html;

                            },
                            '{admin-url}modules/richieste/prenotazioni/edit.php?id={[request_id]}'
                        )
                    )
                )

            )
        );

        return $templates;
    }
}
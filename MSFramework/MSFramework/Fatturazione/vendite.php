<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Fatturazione;


class vendite {
    public function __construct() {
        global $firephp;
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Ottiene gli articoli più utilizzati
     *
     * @param string $id L'ID della vendita (stringa) o delle vendite (array) richieste (se vuoto, vengono recuperati i dati di tutte le vendite)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function ottieniDettagliVendita($id = "", $fields = "*") {

        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }
        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        $category = $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM fatturazione_vendite WHERE id != '' AND (" . $same_db_string_ary[0] . ") ORDER BY id DESC", $same_db_string_ary[1]);
        foreach ((array)$category as $r) {

            if($r['cliente'] && $r['dati_cliente'] && json_decode($r['dati_cliente'])) {
                $r['dati_cliente'] = json_decode($r['dati_cliente'], true);
            }
            if($r['carrello'] && json_decode($r['carrello'])) {
                $r['carrello'] = json_decode($r['carrello'], true);
            }
            if($r['sources'] && json_decode($r['sources'])) {
                $r['sources'] = json_decode($r['sources'], true);
            }
            if($r['info_documento'] && json_decode($r['info_documento'])) {
                $r['info_documento'] = json_decode($r['info_documento'], true);
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }


    /**
     * Ottiene gli articoli più utilizzati
     *
     * @return array
     */
    public function ottieniArticolPiuVenduti() {

        $articoli_frequenti = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM fatturazione_vendite LIMIT 10") as $vendite)
        {
            foreach(json_decode($vendite['carrello'], true) as $articolo) {

                $gia_presente = false;

                foreach($articoli_frequenti as $k => $a_f) {
                    if($a_f['type'] == $articolo['type'] && $a_f['id'] == $articolo['id']) {
                        $gia_presente = $k;
                        $articoli_frequenti[$k]['count']++;
                    }
                }

                if(!$gia_presente) {
                    $articolo['count'] = 1;
                    $articoli_frequenti[] = $articolo;
                }

            }
        }

        usort($articoli_frequenti, function ($a, $b) {
            return $b['count'] - $a['count'];
        });

        return $articoli_frequenti;

    }

    /**
     * Ottiene le tipologie di documento disponibili per la fatturazione
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     * @param bool $exclude_not_avail Impostando questo valore su true, vengono restituiti solo i valori effettivamente disponibili per la configurazione corrente del sito
     *
     * @return mixed
     */
    public function ottieniTipiDocumento($key = "", $exclude_not_avail = true) {
        if((count((new \MSFramework\Fatturazione\casse())->ottieniCasseAttive(1)) && $exclude_not_avail) || !$exclude_not_avail) {
            $types['Fiscale']['scontrino'] = "Scontrino - Fiscale";
        }

        if(((new \MSFramework\cms())->getCMSData('fatturazione_fatture')['enable'] && $exclude_not_avail) || !$exclude_not_avail) {
            $types['Fiscale']['fattura'] = "Fattura";
        }

        if((count((new \MSFramework\Fatturazione\casse())->ottieniCasseAttive(2)) && $exclude_not_avail) || !$exclude_not_avail) {
            $types['Non fiscale']['scontrino_non_fiscale'] = "Scontrino - Non Fiscale";
        }

        $types['Non fiscale']['ricevuta'] = "Ricevuta";
        $types['Non fiscale']['preventivo'] = "Preventivo";

        if($key != "") {
            foreach($types as $type_group => $v) {
                if($v[$key] != "") {
                    return $v[$key];
                }
            }
        } else {
            return $types;
        }
    }

    /**
     * Ottiene tutti i metodi di pagamento disponibili
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return mixed
     */
    public function ottieniMetodiPagamento($key = "") {
        global $MSFrameworki18n;

        $invoice_data = (new \MSFramework\cms())->getCMSData('fatturazione_fatture');
        $r_metodi_pagamento = json_decode($invoice_data['payMethods'], true);

        foreach($r_metodi_pagamento as $metodo) {
            $metodi_pagamento[] = array("nome" => $MSFrameworki18n->getFieldValue($metodo['name']), "resto" => ($metodo['manage_change'] == "1" ? true : false));
        }

        if($key != "") {
            return $metodi_pagamento[$key];
        } else {
            return $metodi_pagamento;
        }
    }

    /**
     * Ottiene tutti i metodi di pagamento disponibili per il pagamento via web
     *
     * @param string $key Passando questo parametro viene restituito SOLO il valore richiesto e non l'intero array.
     *
     * @return mixed
     */
    public function ottieniMetodiPagamentoWeb($key = "") {

        $MSFrameworkCMS = new \MSFramework\cms();
        $old_cassa_settings = $MSFrameworkCMS->getCMSData('ecommerce_cassa');

        /* RETROCOMPATIBILITA (TODO: DA RENDERE PREDEFINITO) */
        $metodi_pagamento = $MSFrameworkCMS->getCMSData('fatturazione_fatture');
        if(json_decode($metodi_pagamento['webPayments'])) {
            $metodi_pagamento = json_decode($metodi_pagamento['webPayments'], true);
            if(!$metodi_pagamento) {
                $metodi_pagamento = $old_cassa_settings;
            }
        }

        if($key != "") {
            return $metodi_pagamento[$key];
        } else {
            return $metodi_pagamento;
        }
    }

    /**
     * Importa una vendita da un'altro modulo
     *
     * @param string $from Il modulo dal quale verrà importato l'ordine
     * @param mixed $reference La referenza dell'ordine
     *
     * @return mixed
     */
    public function importFrom($from, $reference) {
        Global $MSFrameworki18n;

        $user_id = false;
        $array_to_save = array();

        // Se il documento esiste già evito di importarlo
        if($this->getOperationIDByReference($from, $reference)) {
            return false;
        }

        $carrello = array();

        if($from === 'ecommerce') {

            $order_details = (new \MSFramework\Ecommerce\orders())->getOrderDetails($reference);
            if($order_details) {
                $order_details = $order_details[$reference];
                $user_id = $order_details['user_id'];
            }

        } else if($from === 'subscriptions') {

            $order_details = (new \MSFramework\Subscriptions\orders())->getOrderDetails($reference);
            if($order_details) {
                $order_details = $order_details[$reference];
                $user_id = $order_details['user_id'];
            }

        } else if($from === 'travelagency') {

            $order_details = (new \MSFramework\TravelAgency\orders())->getOrderDetails($reference);
            if($order_details) {
                $order_details = $order_details[$reference];
                $user_id = $order_details['user_id'];
            }

        } else if($from === 'tracking') {

            $order_details = (json_decode($reference['track_extra']) ? json_decode($reference['track_extra'], true) : array());
            $user_id = $reference['user_ref'];

            if($order_details && $order_details['price'] > 0) {

            }
        }

        $carrello = $this->convertCartFrom($from, $order_details);

        if($user_id && (new \MSFramework\customers())->getCustomerDataFromDB($user_id) && $carrello) {

            $dati_cliente = (new \MSFramework\customers())->getCustomerDataFromDB($user_id, 'id, nome, cognome, sesso, data_nascita, indirizzo, citta, provincia, cap, email, dati_fatturazione as fatturazione');
            $dati_cliente['fatturazione'] = json_decode('fatturazione');

            $array_to_save['cliente'] = $user_id;
            $array_to_save['dati_cliente'] = json_encode($dati_cliente);
            $array_to_save['tipo_documento'] = '';
            $array_to_save['carrello'] = json_encode($carrello);
            $array_to_save['info_documento'] = json_encode(array());
            $array_to_save['stato'] = 1;
            $array_to_save['reference'] = $from . (is_numeric($reference) ? '_' . $reference : '');


            /* CREO LE INFO DEL DOCUMENTO */

            $info_documento = array(
                'data' => date('d/m/Y'),
                'oggetto' => 'Documento non fiscale',
                'note_interne' => 'Importato automaticamente (' . $from . ')',
                'source' => ''
            );


            Global $MSFrameworkCMS;
            ob_start();
            $from_import = true;
            $cliente = $_POST['cliente'];
            $info_cliente = $dati_cliente;
            $info_carrello = $carrello;
            $sedute_utilizzate = 0;

            include(ABSOLUTE_ADMIN_MODULES_PATH . 'fatturazione/operations/ajax/stampa/default/index.php');
            $info_documento['source'] = json_decode(ob_get_clean(), true)['source'];
            $array_to_save['info_documento'] = json_encode($info_documento);


            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
            $status = $this->MSFrameworkDatabase->pushToDB("INSERT INTO fatturazione_vendite ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

            if($status) {
                (new \MSFramework\tracking())->trackPurchase($this->MSFrameworkDatabase->lastInsertId());
            }

        }

    }

    /**
     * Converte il carrello da un altro modulo
     */
    public function convertCartFrom($from, $order_details) {
        Global $MSFrameworki18n;
        $carrello = array();

        if($from === 'ecommerce') {
            $totale = $order_details['cart']['total_price']['tax'];

            if((float)$order_details['tipo_spedizione']['prezzo'] > 0) {
                $order_details['cart']['products'][] = array(
                    'nome' => $MSFrameworki18n->getFieldValue($order_details['tipo_spedizione']['nome']),
                    'prezzo' =>  (new \MSFramework\Ecommerce\imposte())->createTaxPricesArray($order_details['tipo_spedizione']['prezzo'], 0),
                    'quantity' => 1
                );
            }

            foreach( $order_details['cart']['coupon'] as $coupon_id => $coupon_values) {
                if($coupon_values['error'] != true) {
                    $order_details['cart']['products'][] = array(
                        'nome' => $coupon_id,
                        'prezzo' => (new \MSFramework\Ecommerce\imposte())->createTaxPricesArray(-1 * abs($coupon_values['sale_value']), 0),
                        'quantity' => 1
                    );
                }
            }

            foreach($order_details['cart']['products'] as $e_cart) {

                $prezzo_tax = floatval($e_cart['prezzo']['tax'][0] . '.' . $e_cart['prezzo']['tax'][1]);
                $prezzo_no_tax = floatval($e_cart['prezzo']['no_tax'][0] . '.' . $e_cart['prezzo']['no_tax'][1]);

                $prezzo_totale = $prezzo_tax*$e_cart['quantity'];

                $carrello[] = array(
                    'nome' => $e_cart['nome'],
                    'qty' => $e_cart['quantity'],
                    'prezzo' => floatval($e_cart['prezzo']['no_tax'][0] . '.' . $e_cart['prezzo']['no_tax'][1]),
                    'sconto' => '',
                    'tipo_sconto' => '€',
                    'imposta' => ($e_cart['imposta'] == 'default' ? (new \MSFramework\Fatturazione\imposte())->getDefaultVat() : (float)$e_cart['imposta']),
                    'prezzo_subtotal' => $prezzo_totale,
                    'prezzo_pagato' => $prezzo_totale,
                    'no_tax_subtotal' => $prezzo_no_tax,
                    'subtotal' => $prezzo_totale,
                    'type' => 'prodotto',
                    'id' => $e_cart['product_id'],
                    'prezzo_iva_inclusa' => $prezzo_totale,
                    'valore_imposta' => $prezzo_totale-$prezzo_no_tax,
                    'gift_to' => ''
                );
            }
        } else if($from === 'subscriptions') {
            $sconto = 0;
            if(json_decode($order_details['coupons_used'])) {
                $coupon_usato = json_decode($order_details['coupons_used'], true);
                if($coupon_usato['discount'] > 0) {
                    $sconto = $coupon_usato['discount'];
                }
            }

            $totale = $order_details['plan_info']['prezzo_annuale'];

            if($order_details['is_trial']) {
                $sconto = $totale['no_tax'];
            }

            if($sconto > 0) {
                $totale = $totale-$sconto;
            }

            $totale = (new \MSFramework\Fatturazione\imposte())->getPriceToShow($totale, $order_details['plan_info']['imposta']);

            $carrello[] = array(
                'nome' =>  $MSFrameworki18n->getFieldValue($order_details['plan_info']['titolo']),
                'qty' =>  1,
                'prezzo' => $totale['no_tax'],
                'sconto' => $sconto,
                'tipo_sconto' => '€',
                'imposta' => $order_details['plan_info']['imposta'],
                'prezzo_subtotal' => $totale['tax'],
                'prezzo_pagato' => $totale['tax'],
                'no_tax_subtotal' => $totale['no_tax'],
                'subtotal' => $totale['tax'],
                'type' => 'prodotto',
                'id' => 'abbonamento_' . $order_details['plan_info']['id'],
                'prezzo_iva_inclusa' => $totale['tax'],
                'valore_imposta' => $totale['tax']-$totale['no_tax'],
                'gift_to' => ''
            );

        } else if($from === 'travelagency') {

            $sconto = 0;
            if(json_decode($order_details['coupons_used'])) {
                $coupon_usato = json_decode($order_details['coupons_used'], true);
                if($coupon_usato['discount'] > 0) {
                    $sconto = $coupon_usato['discount'];
                }
            }

            $totale = (new \MSFramework\Fatturazione\imposte())->getPriceToShow($order_details['package_info']['prezzo'], (isset($order_details['package_info']['extra_fields']['imposta']) ? $order_details['package_info']['extra_fields']['imposta'] : ''));

            if($sconto > 0) {
                $totale = $totale-$sconto;
            }

            $carrello[] = array(
                'nome' =>  $MSFrameworki18n->getFieldValue($order_details['package_info']['nome']),
                'qty' =>  1,
                'prezzo' => $totale['no_tax'],
                'sconto' => $sconto,
                'tipo_sconto' => '€',
                'imposta' => (isset($order_details['package_info']['extra_fields']['imposta']) ? $order_details['package_info']['extra_fields']['imposta'] : ''),
                'prezzo_subtotal' => $totale['tax'],
                'prezzo_pagato' => $totale['tax'],
                'no_tax_subtotal' => $totale['no_tax'],
                'subtotal' => $totale['tax'],
                'type' => 'prodotto',
                'id' => 'pacchetto_' . $order_details['package_info']['id'],
                'prezzo_iva_inclusa' => $totale['tax'],
                'valore_imposta' => $totale['tax']-$totale['no_tax'],
                'gift_to' => ''
            );
        } else if($from === 'tracking') {

            $carrello[] = array(
                'nome' => $order_details['title'],
                'qty' => 1,
                'prezzo' => $order_details['price'],
                'sconto' => 0,
                'tipo_sconto' => '€',
                'imposta' => 0,
                'prezzo_subtotal' => $order_details['price'],
                'prezzo_pagato' => $order_details['price'],
                'no_tax_subtotal' => $order_details['price'],
                'subtotal' => $order_details['price'],
                'type' => 'custom',
                'id' => '',
                'prezzo_iva_inclusa' => $order_details['price'],
                'valore_imposta' => 0,
                'gift_to' => ''
            );
        }

        return $carrello;
    }

    /**
     * Ottiene un'operazione tramite referenza
     *
     * @param string $from Il modulo dal quale verrà importato l'ordine
     * @param mixed $reference La referenza dell'ordine
     *
     * @return mixed
     */
    public function getOperationIDByReference($from, $reference) {

        if(is_numeric($reference)) {
            $ref_string = $from . (is_numeric($reference) ? '_' . $reference : '');
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT id FROM fatturazione_vendite WHERE reference = :reference", array(':reference' => $ref_string), true);
            if($r) {
                return $r['id'];
            }
        }

        return false;

    }


    /* =============== FUNZIONI ACP ============= */

    /**
     * Questa funzione permette di ottenere il grafico dell'ultimo mese
     *
     * @param $start La data YYYY-MM-DD di partenza
     * @param $end La data YYYY-MM-DD di finale
     * @param $document_type Il tipo di documento
     *
     * @return array
     */
    public function getDateRangeChart($start, $end, $document_type = 'ricevuta') {

        $grafico_ordini = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT carrello as cart, date(data) as day FROM fatturazione_vendite WHERE data >= '$start' AND tipo_documento LIKE '$document_type' AND data <= '$end 23:59'") as $r) {

            if(!isset($grafico_ordini[$r['day']])) $grafico_ordini[$r['day']] = array('earn' => 0, 'orders' => 0);

            $cart = json_decode($r['cart'], true);
            foreach($cart as $product) {
                $grafico_ordini[$r['day']]['earn'] += $product['subtotal'];
            }
            $grafico_ordini[$r['day']]['orders'] += 1;
        }

        $start =  new \DateTime($start);
        $end =  new \DateTime($end);

        $array_data = array();

        for($i = $start; $i <= $end; $i->modify('+1 day')) {
            if(isset($grafico_ordini[$i->format("Y-m-d")])) $array_data[] = $grafico_ordini[$i->format("Y-m-d")];
            else $array_data[] = array('earn' => 0, 'orders' => 0);
        }

        return $array_data;
    }

}
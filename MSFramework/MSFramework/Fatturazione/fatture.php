<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Fatturazione;


class fatture {
    public function __construct() {
        global $firephp;
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;

    }
    
    /**
     * Ottiene il numero di fatturazione attuale
     * @param $document_type string Il tipo di documento (fattura, ricevuta, preventivo)
     *
     * @return integer
     */
    public function getNextDocumentNumber($document_type = 'fattura') {
        Global $MSFrameworkCMS;

        $invoice_settings = $MSFrameworkCMS->getCMSData('fatturazione_fatture');

        $last_order_date = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `fatturazione_vendite` WHERE tipo_documento = :tipo ORDER BY id DESC", array(':tipo' => $document_type), true);

        if($last_order_date && date('Y', strtotime($last_order_date['data'])) < date('Y')) {
            $invoice_id = 1;
        }
        else
        {
            $invoice_id = json_decode($last_order_date['info_documento'], true)['numero'] + 1;
        }

        return $invoice_id;

    }


    /**
     * Controlla la disponibilità dell'ID e nel caso in cui non la trova ritorna il primo ID disponibile
     *
     * @param int $id_vendita L'id dell'ordine
     * @param int $invoice_id L'id della fattura
     * @param $document_type string Il tipo di documento (fattura, ricevuta, preventivo)
     *
     * @return bool
     */
    public function checkIDAvailability($id_vendita, $invoice_id, $document_type = 'fattura') {
        $exclude_id = "";
        if($id_vendita != "") {
            $exclude_id = "id != $id_vendita AND";
        }

        $not_available = $this->MSFrameworkDatabase->getCount("SELECT id FROM fatturazione_vendite WHERE $exclude_id tipo_documento = :tipo AND info_documento LIKE '%numero\":\"$invoice_id\"%'", array(':tipo' => $document_type));
        return (!$not_available);
    }    

    /**
     * Incrementa di 1 la numerazione delle fatture
     * @return int
     */
    public function incrementaNumerazione() {
        Global $MSFrameworkCMS;
        $impostazioni_fatture = $MSFrameworkCMS->getCMSData('fatturazione_fatture');
        $impostazioni_fatture["invoice_current_number"] = (int)$impostazioni_fatture["invoice_current_number"]+1;
        $array_to_save = array(
            "value" => json_encode($impostazioni_fatture),
            "type" => "fatturazione_fatture"
        );
        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'update');
        return $this->MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'fatturazione_fatture'", $stringForDB[0]);
    }


    /**
     * Restituisce un array associativo (ID Documento => Dati Documento) con i dati relativi al documento
     *
     * @param string $id L'ID del documento (stringa) o dei documenti (array) richiesti (se vuoto, vengono recuperati i dati di tutti i documenti)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getDocumentDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM fatturazione_vendite WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }
}
<?php
/**
 * MSFramework
 * Date: 31/03/18
 *
 * Docs utili: https://github.com/oscarotero/Gettext
 */

namespace MSFramework\Fatturazione;

class currencies {

    public $primary_currency_code = 'EUR';
    public $selected_currency_code = 'EUR';
    private $currencies_details = array();

    /**
     * currency constructor
     *
     * @param string $user_selected_currency_code La valuta impostata dall'utente (questo influenzerà il comportamento delle funzioni)
     */
    public function __construct($user_selected_currency_code = "") {
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;

        $this->currencies_details = $this->getCurrenciesDetails();
        $this->primary_currency_code = $this->getPrimaryCurrencyCode();
        $this->selected_currency_code = ($user_selected_currency_code && in_array($user_selected_currency_code, array_keys($this->getActiveCurrenciesDetails())) ? $user_selected_currency_code : $this->primary_currency_code);
    }

    public function convertValue($value) {
        $originCurrencyRate = $this->getCurrenciesDetails($this->primary_currency_code)[$this->primary_currency_code];
        $destCurrencyRate = $this->getCurrenciesDetails($this->selected_currency_code)[$this->selected_currency_code];

        return round(($value/$originCurrencyRate['eur_rate']) * $destCurrencyRate['eur_rate'], 2);
    }

    public function refreshConversionRates() {
        $response_json = file_get_contents('https://api.exchangeratesapi.io/latest?base=EUR');

        foreach(json_decode($response_json, true)['rates'] as $code => $rate) {
            $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`currencies` SET eur_rate = :rate WHERE code = :code", array(':rate' => $rate, ':code' => $code));
        }
        return false;
    }

    /**
     * Restituisce un array associativo (ID Valuta => Dati Valuta) con i dati relativi alla lingua
     *
     * @param mixed $code Il codice della valuta o un array contenente più codici
     *
     * @return array
     */
    public function getCurrenciesDetails($code = "") {

        $currencies_list = array();
        if($this->currencies_details) {
            $currencies_list = $this->currencies_details;
        } else {
            foreach ($this->MSFrameworkDatabase->getAssoc("SELECT * FROM `" . FRAMEWORK_DB_NAME . "`.`currencies` WHERE eur_rate > 0 ORDER BY is_main DESC, name ASC") as $r) {
                $currencies_list[$r['code']] = $r;
            }
        }

        if($code === "") {
            return $currencies_list;
        }

        if(!is_array($code)) $code = array($code);

        $ary_to_return = array();
        foreach($code as $i) {
            $ary_to_return[$i] = $currencies_list[$i];
        }

        return $ary_to_return;
    }

    /**
     * Ottiene i dettagli delle sole valute utilizzate
     *
     * @return array
     */
    public function getActiveCurrenciesDetails() {
        global $MSFrameworkCMS;
        $active_currencies = $MSFrameworkCMS->getCMSData('currencies');

        $ary_to_return = array();
        if($active_currencies) {
            foreach($active_currencies as $currency) {
                $ary_to_return[$currency['code']] = $this->getCurrenciesDetails($currency['code'])[$currency['code']];
            }
        }

        return $ary_to_return;
    }

    /**
     * Ottiene i dettagli relativi alla valuta attualmente utilizzata
     *
     * @return array
     */
    public function getCurrentCurrencyDetails() {
        return $this->getCurrenciesDetails($this->selected_currency_code)[$this->selected_currency_code];
    }

    /**
     * Ottiene il codice della valuta impostata come principale
     *
     * @return string
     */
    public function getPrimaryCurrencyCode() {
        global $MSFrameworkCMS;
        $active_currencies = $MSFrameworkCMS->getCMSData('currencies');

        foreach($active_currencies as $currency) {
            if($currency['is_primary'] == "1") {
                return $currency['code'];
            }
        }

        return "EUR";
    }

}
?>
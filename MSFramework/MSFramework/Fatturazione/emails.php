<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

namespace MSFramework\Fatturazione;

use MSFramework\cms;

class emails extends \MSFramework\emails {
    /**
     * Ottiene la lista di template per l'ecommerce
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {

        $document_shortcodes = array(
            array(
                "{nome}" => "Il nome del cliente",
                "{email}" => "L'email del cliente",
                "{data_documento}" => "La data del documento",
                "{numero_documento}" => "Il numero del documento",
                "{oggetto}" => "L'oggetto del documento",
                "{messaggio}" => "Eventuale messaggio allegato"
            ),
            array(
                "{[nome]}",
                "{[email]}",
                "{[data_documento]}",
                "{[numero_documento]}",
                "{[oggetto]}",
                "{[messaggio]}",
            )
        );

        $templates = array(
            'fatturazione' => array(
                'fattura-cliente' => array(
                    'nome' => 'Fattura Cliente',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "La tua fattura - {site-name}",
                    'shortcodes' => $document_shortcodes
                ),
                'ricevuta-cliente' => array(
                    'nome' => 'Ricevuta Cliente',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "La tua ricevuta - {site-name}",
                    'shortcodes' => $document_shortcodes
                ),
                'preventivo-cliente' => array(
                    'nome' => 'Preventivo Cliente',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Il tuo preventivo - {site-name}",
                    'shortcodes' => $document_shortcodes
                ),
                'standard-cliente' => array(
                    'nome' => 'Documento Cliente',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Il tuo resoconto - {site-name}",
                    'shortcodes' => $document_shortcodes
                )
            )
        );

        return $templates;
    }

}
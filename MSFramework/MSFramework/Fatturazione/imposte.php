<?php
/**
 * MSFramework
 * Date: 01/05/18
 */

namespace MSFramework\Fatturazione;


class imposte {
    public function __construct() {
        global $firephp;
       
        $this->fatturazione_imposte = (new \MSFramework\cms())->getCMSData('fatturazione_imposte', true);
        
    }
    
    public function getPriceType() {
        return $this->fatturazione_imposte['price_type'];
    }

    public function getImposte() {
        return json_decode($this->fatturazione_imposte['aliquote'], true);
    }

    public function getDefaultVat() {
        $imposta = 0;
        foreach($this->getImposte() as $value => $i_v) {
            if($i_v[1] == "1") {
                $imposta = (float)$value;
            }
        }
        return $imposta;
    }

    public function getSuffix() {
        $this->fatturazione_imposte = (new \MSFramework\cms())->getCMSData('ecommerce_imposte', true);

        return $this->fatturazione_imposte['price_suffix'];
    }

    public function getSettings() {
        Global $MSFrameworki18n;
        $return = array(
            'inclusa_prodotti' => ($this->fatturazione_imposte['price_type_shop'] == 1 ? 0 : 1),
            'inclusa_carrello' => ($this->fatturazione_imposte['price_type_cart'] == 1 ? 0 : 1),
            'mostra_per_singolo' => ($this->fatturazione_imposte['show_tot_type'] == 1 ? 1 : 0),
            'suffix' => $MSFrameworki18n->getFieldValue($this->fatturazione_imposte['price_suffix'])
        );

        return $return;
    }

    public function getPriceToShow($originale, $imposta = 0) {

        $originale = floatval($originale);

        $tax_included =  ($this->getPriceType() == '0' ? true : false);
        $tax_settings =  $this->getSettings();

        if($imposta === 'default') {
            $imposta = $this->getDefaultVat();
        }

        if((float)$imposta < 1) $imposta = 0;

        $imposta = floatval($imposta);

        $prezzo = array(
            'tax' => $originale,
            'no_tax' => $originale
        );

        if($tax_included) {
            $prezzo['no_tax'] = $this->removeTax($originale, $imposta);
        }
        else
        {
            $prezzo['tax'] = $this->addTax($originale, $imposta);
        }

        if($tax_settings['inclusa_prodotti'] == 1) $prezzo['shop_price'] = $prezzo['tax'];
        else $prezzo['shop_price'] = $prezzo['no_tax'];

        if($tax_settings['inclusa_carrello'] == 1) $prezzo['cart_price'] = $prezzo['tax'];
        else $prezzo['cart_price'] = $prezzo['no_tax'];

        return $prezzo;

    }

    public function addTax($prezzo, $imposta) {
        return $prezzo * ($imposta/100) + $prezzo;
    }

    public function removeTax($prezzo, $imposta) {
        $vatDivisor = 1 + ($imposta / 100);
        return (float)$prezzo / (float)$vatDivisor;
    }
}
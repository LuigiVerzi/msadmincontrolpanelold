<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Fatturazione\Services\FattureInCloud;

use Fazland\FattureInCloud\Model\Document\Payment;

class invoices extends \MSFramework\Fatturazione\Services\FattureInCloud\fattureInCloud {
    public function __construct() {
        parent::__construct();

        $this->fatture = $this->api()->document('fatture');
        $this->documentInvoice = new \Fazland\FattureInCloud\Model\Document\Invoice();

        $this->customers = new \MSFramework\Fatturazione\Services\FattureInCloud\customers();
    }

    /**
     * Genera una nuova fattura
     *
     * @param $customer_data I dati di fatturazione del cliente nel formato restituito da createCustomerDataArray. Se il cliente non esiste verrà creato, se esiste verrà aggiornato.
     * @param $goods_data Un array di merci/servizi venduti da inserire in fattura
     * @param $payment_data Un array con i dati di pagamento
     * @param $send_mail Invia/non invia la fattura via mail
     *
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function newInvoice($customer_data, $goods_data, $payment_data, $send_mail = true) {
        try {
            if($customer_data['nome'] == "") {
                throw new \Exception("Il nome è obbligatorio");
            }

            if($customer_data['piva'] == "" && $customer_data['cf'] == "") {
                throw new \Exception("E' necessario specificare almeno uno tra il nome e la partita IVA");
            }

            $this->customers->createCustomer($customer_data);

            try {
                $got_customer = $this->customers->getCustomerID($customer_data);
                if($got_customer == "") {
                    throw new \Exception("Impossibile trovare il cliente"); //non dovrebbe mai accadere
                }

                //dati cliente
                $this->documentInvoice->subject->id = $got_customer;
                $this->documentInvoice->subject->name = $customer_data['nome'];

                //indirizzo cliente (abilitando enableAutocompleteSubject(), questi dati non vengono considerati)
                $address = new \Fazland\FattureInCloud\Model\Subject\Address();
                $address->street = $customer_data['indirizzo_via'];
                $address->zip = $customer_data['indirizzo_cap'];
                $address->city = $customer_data['indirizzo_citta'];
                $address->province = $customer_data['indirizzo_provincia'];
                $this->documentInvoice->subject->address = $address;

                //merci/servizi
                foreach($goods_data as $good) {
                    if($good['name'] == "") {
                        throw new \Exception("Impostare il nome della merce.");
                    }

                    if($good['price'] == "") {
                        throw new \Exception("Impostare il prezzo della merce.");
                    }

                    $merce = new \Fazland\FattureInCloud\Model\Document\Good();
                    $merce->name = $good['name'];
                    $merce->grossPrice = (new \Fazland\FattureInCloud\Util\Money\PreciseMoney($good['price']*100, (new \Money\Currency("EUR"))));
                    $merce->taxable = true;
                    $merce->qty = $good['qty'] ?? 1;
                    $merce->description = $good['descr'] ?? "";
                    $merce->vatCode = $good['vat'] ?? 0; // 0 = 22%
                    $this->documentInvoice->addProduct($merce);
                }

                //pagamenti
                $payment = new \Fazland\FattureInCloud\Model\Document\Payment();
                $payment->dueDate = new \DateTime();
                $payment->method = $payment_data['method'];
                $payment->settlementDate = new \DateTime();
                $this->documentInvoice->addPayment($payment);

                //altri dati
                $this->documentInvoice->vatIncluded = true;
                $this->documentInvoice->showPaymentInfo = false; //la fattura è già stata pagata con paypal/stripe, non mostro le informazioni per effettuare il pagamento.
                $this->documentInvoice->enableAutocompleteSubject(); //se attivo, non tiene conto dei dati dell'indirizzo impostati sopra

                $this->fatture->create($this->documentInvoice);

                if($send_mail && $customer_data['mail'] != "") {
                    $this->fic_client->request('POST', 'fatture/inviamail', [
                        "id" => $this->documentInvoice->id,
                        "token" => $this->documentInvoice->token,
                        "mail_mittente" => "no-reply@fattureincloud.it",
                        "mail_destinatario" => $customer_data['mail'],
                        "oggetto" => "La tua fattura da Marketing Studio",
                        "messaggio" => "Grazie per il tuo acquisto! <br /> Fai click sul pulsante per scaricare la tua fattura. {{allegati}}",
                        "includi_documento" => true
                    ]);
                }
            } catch(\Exception $e) {
                die('newInvoice: ' . $e->getMessage());
            }
        } catch(\Exception $e) {
            die('newInvoice: ' . $e->getMessage());
        }
    }
}
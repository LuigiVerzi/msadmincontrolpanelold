<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

/*
 * Le API di fatture in cloud sono in beta! Questa classe è compatibile con la versione [ base url: /v1 , api version: 0.9.14 ]
 */
namespace MSFramework\Fatturazione\Services\FattureInCloud;

class fattureInCloud extends \Fazland\FattureInCloud\Client\Client {
    public function __construct() {
        global $MSFrameworkFW, $MSFrameworkCMS;

        $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.fattureincloud.it/v1/']);
        $adapter = new \Http\Adapter\Guzzle6\Client($client);

        $fatture_in_cloud = $MSFrameworkFW->getCMSData('settings')['invoices']['fatture_in_cloud'][PAYMETHODS_USE_KEY];

        parent::__construct($fatture_in_cloud['uid'], $fatture_in_cloud['api_key'], $adapter);

        $this->fic_client = new \Fazland\FattureInCloud\Client\Client($fatture_in_cloud['uid'], $fatture_in_cloud['api_key'], $adapter);
    }
}
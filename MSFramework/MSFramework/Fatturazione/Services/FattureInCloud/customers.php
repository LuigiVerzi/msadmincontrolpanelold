<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Fatturazione\Services\FattureInCloud;

class customers extends \MSFramework\Fatturazione\Services\FattureInCloud\fattureInCloud {
    public function __construct() {
        parent::__construct();

        $this->customer = $this->api()->customer();
    }

    /**
     * Genera un array con i dati di fatturazione compatibili con Fatture In Cloud
     *
     * @param $customer_data I dati di fatturazione del cliente nel formato prelevato dai settings del FW.
     *
     * @return array
     */
    public function createCustomerDataArray($customer_data) {
        global $MSFrameworkSaaSBase, $MSFrameworkSaaSEnvironments, $MSFrameworkCMS;
        $isSaaS = $MSFrameworkSaaSBase->isSaaSDomain();

        $MSFrameworkGeonames = new \MSFramework\geonames();

        if(!$isSaaS) {
            $r_site = $MSFrameworkCMS->getCMSData('site');
        }

        return array(
            "nome" => ($customer_data['rag_soc'] != "" ? $customer_data['rag_soc'] : $customer_data['cognome'] . " " . $customer_data['nome']),
            "indirizzo_via" => $customer_data['geo_data']['indirizzo'] . ($customer_data['geo_data']['street_number'] != "" ? ", " . $customer_data['geo_data']['street_number'] : ""),
            "indirizzo_cap" => $customer_data['geo_data']['cap'],
            "indirizzo_citta" => $MSFrameworkGeonames->getDettagliComune($customer_data['geo_data']['comune'], "name")['name'],
            "indirizzo_provincia" => $MSFrameworkGeonames->getDettagliProvincia($customer_data['geo_data']['provincia'], "name")['name'],
            "paese" => "Italia",
            "paese_iso" => "IT",
            "mail" => $customer_data['email'] ?? ($isSaaS ? $MSFrameworkSaaSEnvironments->getOwnerEmail() : $r_site['email']),
            "piva" => $customer_data['piva'],
            "cf" => $customer_data['cf']
        );
    }

    /**
     * Crea un nuovo cliente
     *
     * @param $customer_data I dati del cliente
     * @param bool $update_if_exists Se impostato su true, aggiorna i dati del cliente nel caso in cui questo sia già esistente (verifica tramite cf/piva)
     *
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function createCustomer($customer_data, $update_if_exists = true) {
        try {
            if($customer_data['nome'] == "") {
                throw new \Exception("Il nome è obbligatorio");
            }

            if($customer_data['piva'] == "" && $customer_data['cf'] == "") {
                throw new \Exception("E' necessario specificare almeno uno tra il nome e la partita IVA");
            }

            $got_customer = $this->getCustomerID($customer_data);
            if(empty($got_customer)) {
                return $this->customer->create((new \Fazland\FattureInCloud\Model\Subject\Customer())->fromArray($customer_data));
            } else if(!empty($got_customer) && $update_if_exists) {
                return $this->customer->update($got_customer, (new \Fazland\FattureInCloud\Model\Subject\Customer())->fromArray($customer_data));
            }
        } catch(\Exception $e) {
            die('createCustomer: ' . $e->getMessage());
        }
    }

    /**
     * Ottiene l'ID fatture in cloud del cliente cercando per partita iva e codice fiscale
     *
     * @param $customer_data I dati del cliente
     *
     * @return int|string|null
     */
    public function getCustomerID($customer_data) {
        $got_customer = array();
        if($customer_data['piva'] != "") {
            $got_customer = $this->getCustomersList(["piva" => $customer_data['piva']]);
        }

        if(empty($got_customer)) {
            if($customer_data['cf'] != "") {
                $got_customer = $this->getCustomersList(["cf" => $customer_data['cf']]);
            }
        }

        if(!empty($got_customer)) {
            reset($got_customer);
            return key($got_customer);
        } else {
            return "";
        }
    }

    /**
     * Ottiene la lista dei clienti
     *
     * @param $filter I parametri con i quali effettuare il filtro
     *
     * @return array
     */
    public function getCustomersList($filter) {
        $ary = array();
        foreach($this->customer->list($filter)->getIterator() as $k => $v) {
            $ary[$v->id] = $v;
        }

        return $ary;
    }
}
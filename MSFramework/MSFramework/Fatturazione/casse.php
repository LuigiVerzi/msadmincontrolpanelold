<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Fatturazione;


class casse {
    public function __construct() {
        global $firephp;
        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Ritorna un array con tutte le casse attive
     *
     * @param int (0 => Entrambi i tipi), (1 => Solo Casse Fiscali), (2=> Solo Casse Non Fiscali)
     * @param int Ottiene solo le casse attive (1 di default)
     *
     * @return array
     */
    public function ottieniCasseAttive($fiscal = 0, $only_active = 1) {

        $fiscal_sql = '';
        if($fiscal == 1) {
            $fiscal_sql = "AND fiscale = 1";
        } else if($fiscal == 2) {
            $fiscal_sql = "AND fiscale = 0";
        }

        $casse = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM fatturazione_casse WHERE is_active = $only_active $fiscal_sql") as $c) {
            $casse[$c['id']] = $c;
        }
        return $casse;
    }

    /**
     * Ritorna un array con tutti i dispositivi di cassa supportati
     *
     * @param mixed L'array con i dispositivi supportati
     *
     * @return array
     */
    public function ottieniDispositiviSupportati($id = false) {

        $dispositivi = array(
            'rch_printf' => array(
                'nome' => 'RCH Print! F',
                'connection_type' => 'webserver',
                'standard' => false
            )
        );

        if($id) {
            return (isset($dispositivi[$id]) ? $dispositivi[$id] : array());
        }

        return $dispositivi;
    }

    /**
     * Genera il file di Stampa adatto per stampante corrente
     *
     * @param array Un array con gli articoli
     * @param array Impostazioni del documento
     *
     * @return array
     */
    public function generaOutputCassa($articoli, $info_documento) {

        $id_cassa = $info_documento['cassa'];
        if($id_cassa == "") {
            $return = array('status' => 'ok', 'connection_info' => array("method" => "manual"));
        } else {
            $impostazioni_cassa = $this->ottieniCasseAttive()[$id_cassa];
            $dispositivo = $this->ottieniDispositiviSupportati($impostazioni_cassa['dispositivo']);

            if($dispositivo && $impostazioni_cassa) {

                if($dispositivo['standard']) {
                    $class_path = 'MSFramework\\Fatturazione\\Output\\Casse\\' . $dispositivo['standard'];
                }
                else {
                    $class_path = 'MSFramework\\Fatturazione\\Output\\Casse\\' . $impostazioni_cassa['dispositivo'];
                }

                $printClass = (new $class_path());
                $output = $printClass->generateOutput($articoli, $info_documento, $impostazioni_cassa);
                $configuration_output = $printClass->generateConfigOutput($articoli, $info_documento, $impostazioni_cassa);

                if($output) {
                    $dati_connessione = json_decode($impostazioni_cassa['dati_connessione'], true);

                    if($dati_connessione['method'] == 'webserver') {
                        $dati_connessione['ip'] = $this->formattaIndirizzoIP($dati_connessione['ip']);
                        $return = array('status' => 'ok', 'output' => $output, 'config_outputs' => $configuration_output, 'connection_info' => $dati_connessione);
                    }
                    else {
                        $return = array('status' => 'ok');
                    }
                }
                else {
                    $return = array('status' => 'invalid_output', 'error' => 'Si è verificato un errore inaspettato, nel caso in cui persista contattare il supporto tecnico.');
                }

            }
            else {
                $return = array('status' => 'invalid_cashdesk', 'error' => 'Non è stata selezionata nessuna cassa valida.');
            }
        }


        return $return;
    }

    /**
     * Formatta l'indirizzo IP
     *
     * @param string L'indirizzo IP
     *
     * @return string
     */
    public function formattaIndirizzoIP($ip) {

        if(is_numeric($ip[0])) {
            $ip = 'http://' . $ip;
        }

        return $ip;
    }

}
<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Fatturazione\Output\Casse;


class rch_printf {
    public function __construct() {
        global $firephp;
    }

    /**
     * Genera un file XML contenente i dati relativi alla stampa
     *
     * @param array Un array con gli articoli
     * @param array I settaggi del documento
     * @param array Impostazioni della cassa
     *
     * @return string
     */
    public function generateOutput($articoli, $info_documento, $impostazioni_cassa) {

        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<Service>';

        // Se la cassa è NON FISCALE apro lo scontrino non fiscale
        if($impostazioni_cassa['fiscale'] == '0') {
            $output .= '<cmd>=o</cmd>';
        }

        $totale = 0;
        foreach($articoli as $articolo) {
            $output .= '<cmd>=R' . (int)$info_documento['cassa'] . '/' . CURRENCY_SYMBOL . ($articolo['prezzo_iva_inclusa']*100) . '/*' . $articolo['qty'] .  '/(' . str_replace(array('/', '(', ')'), array('', '', ''), $articolo['nome']) . ')</cmd>';
            if(!empty($articolo['sconto'])) {
                if($articolo['tipo_sconto'] == '%') {
                    $output .= '<cmd>=%' . ($articolo['sconto'] > 0 ? '-' : '+') . '/' . str_replace(',', '.', abs($articolo['sconto'])) . '</cmd>';
                }
                else {
                    $output .= '<cmd>=V' . ($articolo['sconto'] > 0 ? '-' : '+') . '/' . (abs($articolo['sconto'])*100) . '</cmd>';
                }
            }

            $totale += $articolo['subtotal'];
        }

        $output .= '<cmd>=S</cmd>';

        if($info_documento['usa_crediti'] == 1 && $info_documento['valore_credito'] != 0) {
            $output .= '<cmd>=V' . ($info_documento['valore_credito'] > 0 ? '-' : '+') . '/' . (abs(floatval($info_documento['valore_credito']))*100) . '</cmd>';
        }

        // Chiude lo scontrino indicando il metodo di pagamento
        $id_pagamento = (isset($info_documento['pagamento']) && intval($info_documento['pagamento']) > 0 ? intval($info_documento['pagamento']) : 1);
        $output .= '<cmd>=T' . $id_pagamento . '</cmd>';

        // Stampa la numerazione interna (Se maggiore di 0)
        if($info_documento['numerazione'] > 0) {
            $output .= '<cmd>="/(' . intval($info_documento['numerazione']) . ')</cmd>';
        }

        // Stampa il totale sullo schermo di un eventuale display
        $output .= '<cmd>=D1/(' . count($articoli) . ' Articoli)</cmd>';
        $output .= '<cmd>=D2/(Totale: E. ' . $totale . ')</cmd>';

        // Apre il cassetto
        $output .= '<cmd>=C86</cmd>';

        $output .= '</Service>';

        return $output;

    }

    /**
     * Genera un file XML contenente i dati relativi alla configurazione della cassa (Da inviare prima della stampa)
     *
     * @param array Un array con gli articoli
     * @param array I settaggi del documento
     * @param array Impostazioni della cassa
     *
     * @return array (I comandi vengono divisi perchè devono essere inviati singolarmente)
     */
    public function generateConfigOutput($articoli, $info_documento, $impostazioni_cassa) {

        // Configurazione pagamenti
        $metodi_pagamento = (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamento();
        foreach($metodi_pagamento as $id=>$metodo) {
            $outputs[] = '<Payment id="' . $id . '"><txt>' . $metodo['nome'] . '</txt><change>' . ($metodo['resto'] ? 1 : 0) . '</change><cash>' . ($metodo['resto'] ? 1 : 0) . '</cash><credit>' . (!$metodo['resto'] ? 1 : 0) . '</credit><drawer>' . ($metodo['resto'] ? 1 : 0) . '</drawer></Payment>';
        }

        // Imposta il messaggio promozionale
        $outputs[] = '<courtesyLine id="1"><txt>' . str_replace('/', '-', substr($impostazioni_cassa['messaggio_promozionale'], 0, 24)) . '</txt></courtesyLine>';

        // Imposta il reparto
        $outputs[] = '<Department id="' . $info_documento['cassa'] . '"><txt>' . str_replace('/', '-', substr($impostazioni_cassa['descrizione'], 0, 24)) . '</txt></Department>';

        // Mostra il totale nel display
        $totale = 0;
        foreach($articoli as $articolo) {
            $totale += $articolo['subtotal'];
        }
        $outputs[] = '<Service><cmd>=D1/(' . count($articoli) . ' Articoli)</cmd><cmd>=D2/(Totale: E. ' . $totale . ')</cmd></Service>';

        foreach($outputs as $k => $output) {
            $outputs[$k] = '<?xml version="1.0" encoding="UTF-8"?>' . $output;
        }

        return $outputs;

    }
}
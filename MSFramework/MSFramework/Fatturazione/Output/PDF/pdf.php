<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

namespace MSFramework\Fatturazione\Output\PDF;

class pdf extends \Interpid\PdfLib\Pdf {
    public function __construct($invoiceName, $orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false) {

        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);

        $this->invoiceName = $invoiceName;
        $this->headerFont = 'helvetica';
        $this->defaultFont = 'helvetica';
        $this->defaultFontSize = 8;

        $this->SetCreator('MarketingStudio');
        $this->SetAuthor(SW_NAME);
        $this->SetTitle($this->invoiceName);
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->SetHeaderMargin(15);
        $this->SetFooterMargin(5);
        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $this->setColorArray('fill', array(255,255,200));
    }

    /**
     * Formatta i prezzi
     *
     * @param string $prezzo Il prezzo da formattare
     * @param string $valuta La valuta
     *
     * @return mixed
     *
     */
    public function formatPrice($prezzo, $valuta = CURRENCY_SYMBOL) {
        if(floatval($prezzo) == 0) return '-';
        return number_format($prezzo, 2) . ' ' . CURRENCY_NAME;
    }

    /**
     * Inizializza una serie di impostazioni/comportamenti finali da lanciare appena . Si occupa anche di eseguire l'output del file.
     *
     * @param string $dest Il tipo di output da eseguire
     *
     * @return mixed
     *
     */
    public function getOutput($dest = "D") {
        $this->lastPage();
        return $this->Output($this->invoiceName . ".pdf", $dest);
    }

    /**
     * Genera il footer del documento
     */
    public function Footer() {
        //$this->SetFont($this->defaultFont, '', 5);
        //$this->Cell(0, 15, 'Documento generato tramite MSFramework per ' . SW_NAME . '. Generato il ' . date("d/m/Y") . ' ore ' . date("H:i"), 0, false, 'R', 0, '', 0, false, 'M', 'M');
    }


    /**
     * Calcola restituisce il valore numerico del width calcolandolo da un valore percentuale (es: il 25% della pagina, margini esclusi)
     *
     * @param $percent La percentuale da calcolare
     *
     * @return float|int
     */
    public function widthFromPercentage($percent) {
        $margins = $this->getMargins();
        return ($percent*($this->getPageWidth()-$margins['left']-$margins['right']))/100;
    }

}
<?php
/**
 * MSFramework
 * Date: 31/03/18
 *
 * Docs utili: https://github.com/oscarotero/Gettext
 */

namespace MSFramework;

class i18n {

    public $backend_mode = false;

    /**
     * i18n constructor.
     *
     * @param $user_selected_lang string La lingua impostata dall'utente. Questa influenzerà il contenuto visualizzato nell'ACP e/o nel sito
     * @param $init_gettext bool  Se impostato su true, avvia e registra la classe "Gettext" per le traduzioni del contenuto statico del sito
     */
    public function __construct($user_selected_lang = "", $init_gettext = false) {
        global $firephp;
        Global $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->languages_details = $this->getLanguagesDetails();
        $this->primary_lang_id = $this->getPrimaryLangId();

        ($user_selected_lang != "") ? $this->user_selected_lang = $user_selected_lang : $this->user_selected_lang = $this->primary_lang_id;

        if($init_gettext) {
            $this->initGettext();
        }
    }

    /**
     * Restituisce un array associativo (ID Lingua => Dati Lingua) con i dati relativi alla lingua
     *
     * @param $id L'ID della lingua (stringa) o delle lingue (array) richieste (se vuoto, vengono recuperati i dati di tutte le lingue)
     * @param string $fields I campi da prelevare dal DB
     * @param string $filter Se impostato su "main" recupera solo le lingue principali (quelle più comunemente utilizzate, parametro impostato nel DB). Se è "not_main" recupera solo quelle non comunemente utilizzate. Se è vuoto, recupera tutto.
     *
     * @return array
     */
    public function getLanguagesDetails($id = "", $fields = "*", $filter = "") {

        // Riprendo l'array già dichiarato invece che effettuare una nuova chiamata al DB
        if(!is_null($this->languages_details) && $filter == "") {
            $ary_to_return = $this->languages_details;

            if($id != "") {

                if(!is_array($id)) {
                    $id = array($id);
                }

                $ary_tmp = array();
                foreach($id as $key) {
                    $ary_tmp[$key] = $ary_to_return[$key];
                }

                $ary_to_return = $ary_tmp;
            }

        } else {
            if ($id != "") {
                if (!is_array($id)) {
                    $id = array($id);
                }

                $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
            }

            if (!is_array($same_db_string_ary[1])) {
                $same_db_string_ary = array(" id != '' ", array());
            }

            if ($fields != "*" && !strstr($fields, "id")) {
                $fields .= ", id";
            }

            $append_where = "";
            if ($filter == "main") {
                $append_where = " AND is_main = 1 ";
            } else if ($filter == "not_main") {
                $append_where = " AND is_main = 0 ";
            }

            foreach ($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM `" . FRAMEWORK_DB_NAME . "`.`locales` WHERE id != '' AND (" . $same_db_string_ary[0] . ") $append_where", $same_db_string_ary[1]) as $r) {
                $ary_to_return[$r['id']] = $r;
            }
        }

        return $ary_to_return;
    }

    /**
     * Ottiene i dettagli delle sole lingue attivate e disponibili per modifica e visualizzazione in frontoffice
     *
     * @return array
     */
    public function getActiveLanguagesDetails() {
        Global $MSFrameworkCMS;
        $active_langs = json_decode($MSFrameworkCMS->getCMSData('producer_config')['languages'], true);

        $active_langs_id = array();
        foreach($active_langs as $lang) {
            $active_langs_id[] = $lang['id'];
        }

        if(count($active_langs_id) == 0) {
            return $active_langs_id;
        }

        $lang_det = $this->getLanguagesDetails($active_langs_id, "*");

        $main_lang_id = $this->primary_lang_id;
        if($main_lang_id != "") {
            $lang_det[$main_lang_id]['is_primary'] = "1";
        }

        return $lang_det;
    }

    /**
     * Ottiene i dettagli relativi alla lingua correntemente selezionata dall'utente
     *
     * @return array
     */
    public function getCurrentLanguageDetails() {
        return $this->getLanguagesDetails($this->user_selected_lang)[$this->user_selected_lang];
    }

    /**
     * Ottiene l'ID della lingua impostata come principale
     *
     * @return string
     */
    public function getPrimaryLangId() {
        global $MSFrameworkCMS;
        $active_langs = json_decode($MSFrameworkCMS->getCMSData('producer_config')['languages'], true);

        if(!$active_langs) {
            $active_langs = array(
                array(
                    "id" => "1",
                    "is_primary" => "1"
                )
            );
        }

        foreach($active_langs as $lang) {
            if($lang['is_primary'] == "1") {
                return $lang['id'];
            }
        }

        return "";
    }

    /**
     * Ottiene la versione tradotta a partire dal campo JSON che contiene i dati.
     *
     * @param $value string Il campo per il quale recuperare la traduzione. Se il testo inviato non è un array con le lingue, restituisce nuovamente il testo inviato senza modifiche
     * @param $return_primary string Se non esiste la traduzione per la lingua corrente, viene restituita la traduzione per la lingua principale. Il valore di default è false se la richiesta proviene dall'ACP, mentre diventa true se la richiesta proviene dal sito
     * @param $lang mixed Se viene passato il codice di una lingua ritorna la traduzione per la lingua selezionata altrimenti usa la lingua impostata
     *
     * @return mixed
     */
    public function getFieldValue($value, $return_primary = "", $lang = false) {
        if($return_primary == "") {
            if(strstr(REAL_SW_PATH, "/backend")) {
                $return_primary = false;
            } else {
                $return_primary = true;
            }
        }

        $field = json_decode($value, true);

        if(is_array($field)) {

            if(!$lang) $lang_det = $this->getLanguagesDetails($this->user_selected_lang, "long_code")[$this->user_selected_lang]['long_code'];
            else $lang_det = $this->getLanguagesDetails($this->primary_lang_id, "long_code")[$this->primary_lang_id]["long_code"];

            if($field[$lang_det] == "" && $return_primary) {
                $lang_det = $this->getLanguagesDetails($this->primary_lang_id, "long_code")[$this->primary_lang_id]["long_code"];
                return $field[$lang_det];
            } else {
                return $field[$lang_det];
            }
        } else {
            return $value;
        }
    }

    /**
     * Verifica se un determinato campo ha un valore impostato per la lingua principale
     *
     * @param array $translation_fields Un array associativo di campi da controllare
     *
     * @return bool
     */
    public function checkIfPrimaryHasValueForField($translation_fields) {
        return true;
    }

    /**
     * Imposta un nuovo valore all'interno del campo per la lingua corrente
     *
     * @param $old_field Il valore del campo (JSON) attuale (prima dell'impostazione del nuovo valore)
     * @param $value Il valore da impostare
     *
     * @return string
     */
    public function setFieldValue($old_field, $value) {
        $lang_det = $this->getLanguagesDetails($this->user_selected_lang, "long_code")[$this->user_selected_lang]['long_code'];

        if(json_decode($old_field)) {
            $old_field = json_decode($old_field, true);
        }
        else $old_field = array('it_IT' => '');

        $old_field[$lang_det] = $value;

        return json_encode($old_field);
    }

    /**
     * Verifica se una determinata lingua è attualmente attiva sul portale. Attraverso il parametro "$filter" è possibile indicare il tipo di dato da cercare all'interno dell'array delle lingue attive (es. id, url_code, short_code ecc)
     *
     * @param $code La lingua da verificare (dato coerente con quanto impostato in $filter)
     * @param string $filter Il filtro da applicare (corrisponde alla colonna del DB delle lingue)
     *
     * @return bool
     */
    public function checkLangIsActiveByFilter($code, $filter = "id") {
        foreach($this->getActiveLanguagesDetails() as $k => $v) {
            if($v[$filter] == $code) {
                return true;
            }
        }

        return false;
    }

    /**
     * Verifica se una determinata lingua è attualmente impostata come principale sul portale. Attraverso il parametro "$filter" è possibile indicare il tipo di dato da cercare all'interno dell'array delle lingue attive (es. id, url_code, short_code ecc)
     *
     * @param $code La lingua da verificare (dato coerente con quanto impostato in $filter)
     * @param string $filter Il filtro da applicare (corrisponde alla colonna del DB delle lingue)
     *
     * @return bool
     */
    public function checkLangIsPrimaryByFilter($code, $filter = "id") {
        foreach($this->getActiveLanguagesDetails() as $k => $v) {
            if($v[$filter] == $code && $v['is_primary'] == "1") {
                return true;
            }
        }

        return false;
    }

    /**
     * Ottiene l'ID della lingua impostata in $code. Attraverso il parametro "$filter" è possibile indicare il tipo di dato da cercare all'interno dell'array delle lingue attive (es. id, url_code, short_code ecc)
     *
     * @param $code La lingua da verificare (dato coerente con quanto impostato in $filter)
     * @param string $filter Il filtro da applicare (corrisponde alla colonna del DB delle lingue)
     *
     * @return bool
     */
    public function getLangIDByFilter($code, $filter = "id") {
        foreach($this->getActiveLanguagesDetails() as $k => $v) {
            if($v[$filter] == $code) {
                return $v['id'];
            }
        }

        return false;
    }

    /**
     * Inizializza la classe Gettext con la lingua impostata dall'utente
     */
    private function initGettext() {
        $this->checkPoFileExists(true);

        $this->t = new \Gettext\Translator();
        $this->t->loadTranslations($this->getCurTranslationsAry());
        $this->t->loadTranslations($this->getCurBackendTranslationsAry());
        $this->t->register();
    }

    /**
     * Ottiene l'array delle traduzioni (lingua corrente) dal file .po
     *
     * @return $this
     */
    public function getCurTranslationsAry() {
        return \Gettext\Translations::fromPoFile($this->getCurLangPoPath());
    }

    /**
     * Ottiene l'array delle traduzioni del backend/classi (lingua corrente) dal file .po
     *
     * @return $this
     */
    public function getCurBackendTranslationsAry() {
        return \Gettext\Translations::fromPoFile($this->getCurLangPoPath(false, true));
    }

    /**
     * Ottiene il path del file .po delle traduzioni (lingua corrente)
     *
     * @param bool $html Se impostato su true, restituisce la versione HTML del path
     * @param bool $backend Se impostato ottiene la path delle traduzioni backend
     *
     * @return string
     */
    public function getCurLangPoPath($html = false, $backend = false) {
        $dettagliLingua = $this->getLanguagesDetails($this->user_selected_lang)[$this->user_selected_lang];

        if($backend) {
            if (!$html) return PATH_TO_COMMON_STORAGE . "i18n/" . $dettagliLingua['long_code'] . '/LC_MESSAGES/messages.po';
            else return PATH_TO_COMMON_STORAGE_HTML . "i18n/" . $dettagliLingua['long_code'] . '/LC_MESSAGES/messages.po';
        } else {
            if (!$html) return MAIN_SITE_FOLDERPATH . "i18n/" . $dettagliLingua['long_code'] . '/LC_MESSAGES/messages.po';
            else return ABSOLUTE_SW_PATH_HTML . "i18n/" . $dettagliLingua['long_code'] . '/LC_MESSAGES/messages.po';
        }
    }

    /**
     * Si occupa di tradurre il testo passato utilizzando la lingua impostata dall'utente
     *
     * @param $txt Il testo da tradurre
     * @param array $ary_ph Un array di placeholder presenti all'interno del testo
     *
     * @return string
     */
    public function gettext($txt, $ary_ph = array()) {
        return __($txt, $ary_ph);
    }

    /**
     * Si occupa di tradurre il testo passato utilizzando la lingua impostata dall'utente
     *
     * @param string $html Il codice dove cercare gli shortcodes di traduzione
     *
     * @return string
     */
    public function gettextFromShortcodes($html) {

        if(preg_match_all('/{__([^_]+)__}/m', $html, $shortcodeMatches, PREG_PATTERN_ORDER)) {
            foreach($shortcodeMatches[0] as $k => $shortcodeMatch) {
                $html = str_replace($shortcodeMatch, __($shortcodeMatches[1][$k]), $html);
            }
        }

        return $html;
    }

    /**
     * Genera un file .po vuoto per la lingua impostata dall'utente (se non esiste già)
     *
     * @param bool $populate Se impostato su true, popola il file appena creato con le stringhe trovate nel SW
     * @param $backend bool Se impostato genera il file di traduzione del backend
     *
     * @return bool
     */
    public function generatePoFile($populate = false, $backend = false) {

        $po_path = ($backend ? $this->getCurLangPoPath(false, true) : $this->getCurLangPoPath());

        if(!is_file($po_path)) {
            mkdir(str_replace("messages.po", "", $po_path), 0755, true);
            $f = fopen($po_path, "w");
            fclose($f);
        }

        if($populate) {
            $this->populatePoFile($backend);
        }

        return true;
    }

    /**
     * Verifica se il file .po per la lingua selezionata è già presente sul server.
     *
     * @param bool $force_creation Se impostato su true denera un file .po vuoto per la lingua impostata dall'utente (se non esiste già)
     * @param bool $populate Se impostato su true, popola il file appena creato con le stringhe trovate nel SW
     *
     * @return bool
     */
    public function checkPoFileExists($force_creation = false, $populate = false) {

        $files = array(
            $this->getCurLangPoPath(),
            $this->getCurLangPoPath(false, true)
        );

        $to_return = true;
        foreach($files as $is_backend => $file) {
            $to_return = (is_file($file) && filesize($file) > 0) ? true : false;

            if (!$to_return && $force_creation) {
                $this->generatePoFile($populate, $is_backend);
            }
        }

        return $to_return;
    }

    /**
     * Popola il file .po (effettua un merge) della lingua selezionata dall'utente con le stringhe prelevate dal SW dal files .php e .js
     * @param $backend bool Se impostato genera il file di traduzione del backend
     */
    private function populatePoFile($backend = false) {
        $translations = new \Gettext\Translations();

        $MSFrameworkUtils = new \MSFramework\utils();

        $filesToSearch = array();
        foreach($MSFrameworkUtils->getFilesByExtension('php', $backend) as $file) {
            $translations->addFromPhpCodeFile($file);
            $filesToSearch[] = $file;
        }

        foreach($MSFrameworkUtils->getFilesByExtension('js', $backend) as $file) {
            $translations->addFromJsCodeFile($file);
        }

        // Cerco le stringe create tramite shortcode '{__Testo__}'
        $shortcodesStrings = array();
        foreach(array_merge($filesToSearch, $MSFrameworkUtils->getFilesByExtension('html', $backend)) as $file) {
            $file_content = file_get_contents($file);
            if(preg_match_all('/{__([^_]+)__}/m', $file_content, $shortcodeMatches, PREG_PATTERN_ORDER)) {
                foreach($shortcodeMatches[1] as $shortcodeMatch) {
                    $shortcodesStrings[] = "__('" . htmlentities($shortcodeMatch) . "')";
                }
            }
        }

        $shortcodesStrings = array_unique($shortcodesStrings);
        if($shortcodesStrings) {
            $translations->addFromPhpCodeString('<?php ' . implode(';' . PHP_EOL, $shortcodesStrings) . '; ?>');
        }

        $dest_po_path = $this->getCurLangPoPath(false, $backend);

        $translations->mergeWith(\Gettext\Translations::fromPoFile($dest_po_path), \Gettext\Merge::REFERENCES_OURS);
        $translations->toPoFile($dest_po_path);
    }

    /**
     * Restituisce il parametro da passare (in GET o in POST) alle varie pagine/funzioni per continuare a mantenere le preferenze della lingua
     *
     * @return string
     */
    public function getURLCodeParam() {
        if(USING_LANGUAGE_CODE == $this->primary_lang_id) {
            $value = "use_default";
        } else {
            $value = $this->getLanguagesDetails(USING_LANGUAGE_CODE, 'url_code')[USING_LANGUAGE_CODE]['url_code'];
        }

        return "&lang=" . $value;
    }

    /**
     * Verifica se è possibile switchare la lingua principale da $from_id a $to_id (è possibile solo se $to_id è già stato tradotto al 100%)
     *
     * @param $from_id string La lingua di origine (la primaria corrente)
     * @param $to_id  string La lingua di destinazione (la primaria desiderata)
     *
     * @return bool
     */
    public function canChangePrimary($from_id, $to_id) {
        if($from_id == $to_id || ($from_id === "" && $to_id !== "")) {
            return true;
        }

        if($from_id == "" || $to_id == "") {
            return false;
        }

        $from_lang_det = $this->getLanguagesDetails($from_id, "long_code")[$from_id]['long_code'];
        $to_lang_det = $this->getLanguagesDetails($to_id, "long_code")[$to_id]['long_code'];

        $from_counter = 0;
        $to_counter = 0;
        foreach($this->MSFrameworkDatabase->getAssoc("SHOW TABLES") as $table_obj) {
            $table_name = reset($table_obj);
            foreach($this->MSFrameworkDatabase->getAssoc("SHOW COLUMNS FROM `$table_name`") as $column_obj) {
                $from_counter += $this->MSFrameworkDatabase->getCount("SELECT `" . $column_obj['Field'] . "` FROM `$table_name` WHERE `" . $column_obj['Field'] . "` LIKE :like", array(":like" => '%"' . $from_lang_det . '":"%'));
                $from_counter -= $this->MSFrameworkDatabase->getCount("SELECT `" . $column_obj['Field'] . "` FROM `$table_name` WHERE `" . $column_obj['Field'] . "` LIKE :like", array(":like" => '%"' . $from_lang_det . '":""%'));

                $to_counter += $this->MSFrameworkDatabase->getCount("SELECT `" . $column_obj['Field'] . "` FROM `$table_name` WHERE `" . $column_obj['Field'] . "` LIKE :like", array(":like" => '%"' . $to_lang_det . '":"%')); //recupero qualunque occorrenza contenente la lingua di destinazione (e sommo)
                $to_counter -= $this->MSFrameworkDatabase->getCount("SELECT `" . $column_obj['Field'] . "` FROM `$table_name` WHERE `" . $column_obj['Field'] . "` LIKE :like", array(":like" => '%"' . $to_lang_det . '":""%')); //recupero qualunque occorrenza contenente la lingua di destinazione -ma questa vuolta controllo solo quelle vuote- (e sottraggo)
            }
        }

        if($from_counter != $to_counter) {
            return false;
        } else {
            return true;
        }
    }
}
?>
<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Ticket;
use MSFramework\cms;

class ticket {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Salva il biglietto acquistato nel database
     *
     * @param $event_id L'ID dell'evento
     * @param $ticket_id L'ID del biglietto
     * @param $quantity La quantità
     * @param $nome Il nome del beneficiario principale
     * @param $email L'email del beneficiario principale
     * @param $messaggio Un eventuale messaggio allegato all'acquisto
     *
     * @return integer Request ID
     */
    public function saveTicket($event_id, $ticket, $ticket_id, $quantity, $nome, $email, $messaggio) {

        $form_data = array(
            'Evento' => $event_id,
            'Biglietto' => $ticket,
            'Stato' => 1,
            'Ticket ID' => $ticket_id,
            'Quantità' => $quantity
        );

        $status = $this->MSFrameworkDatabase->pushToDB("INSERT INTO richieste (type, nome, contatto, messaggio, form_data, creation_time, lang) VALUES (:type, :nome, :contatto, :messaggio, :form_data, :creation_time, :lang)", array(":type" => "ticket", ":nome" => $nome, ":contatto" => $email, ":messaggio" => $messaggio, ":form_data" => json_encode($form_data), ":creation_time" => time(), ":lang" => USING_LANGUAGE_CODE));

        if($status) return $this->MSFrameworkDatabase->lastInsertId();
        else return false;

    }

    /**
     * Invia il biglietto acquistato via email
     *
     * @param $event_id L'ID dell'evento
     * @param $ticket_info JSON con le info del biglietto
     * @param $ticket_id L'ID del biglietto
     * @param $quantity La quantità
     * @param $name Il nome del beneficiario principale
     * @param $email L'email del beneficiario principale
     * @param $messaggio Un eventuale messaggio allegato all'acquisto
     * @param $oggetto L'oggetto dell'email
     * @param $messaggio_allegato Eventuale messaggio da allegare al ticket
     *
     * @return boolean
     */
    public function sendTicketViaEmail($event_id, $ticket_info, $ticket_id, $quantity, $name, $email, $messaggio, $oggetto = '', $messaggio_allegato = '') {

        $infoEvento = (new \MSFramework\Ticket\events())->getEventDetails($event_id)[$event_id];

        if(!$oggetto) {
            if($infoEvento) $oggetto = 'Biglietti ' . $infoEvento['titolo'] . ' - ' . SW_NAME;
            else $oggetto = 'Biglietti ' . SW_NAME;
        }

        $email_params = array(
            'oggetto' => $oggetto,
            'name' => $name,
            'email' => $email,
            'quantita' => $quantity,
            'ticket_id' => $ticket_id,
            'messaggio' => $messaggio,
            'messaggio_allegato' => $messaggio_allegato,
            'qr_image' => 'https://chart.googleapis.com/chart?chs=163x163&cht=qr&chl=' . urlencode((new cms())->getURLToSite() . $this->getCheckPagePrefix() . '/' . $ticket_id),
            'evento' => $infoEvento['titolo'],
            'ticket' => $ticket_info,
            'luogo' => $infoEvento['luogo'],
            'data' => date("d/m/Y H:i", $infoEvento['data'])
        );

        return (new \MSFramework\Ticket\emails())->sendMail('biglietto-acquistato', $email_params);
    }

    /**
     * Restituisce il prefisso che viene settato prima dello short code per il controllo dei biglietti
     *
     * @return string
     */
    public function getCheckPagePrefix() {
        return 'check';
    }

}
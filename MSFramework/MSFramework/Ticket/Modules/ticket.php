<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Ticket\Modules;

class ticket {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->ticket = new \MSFramework\Ticket\ticket();
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array();

        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {
        if (strpos($_SERVER["HTTP_ORIGIN"], 'admin.' . (new \MSFramework\cms())->getVirtualServerName()) !== false) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
        }

        if($params['action'] == 'savePreOrderInfo') {

            $form_data = array(
                'Evento' => $params['evento'],
                'Stato' => 0,
                'Biglietto' => $params['ticket'],
                'Ticket ID' => 'N/D',
                'Quantità' => $params['quantity']
            );

            $this->MSFrameworkDatabase->pushToDB("INSERT INTO richieste (type, nome, contatto, messaggio, form_data, creation_time, lang) VALUES (:type, :nome, :contatto, :messaggio, :form_data, :creation_time, :lang)", array(":type" => "ticket", ":nome" => $params['name'], ":contatto" => $params['email'], ":messaggio" => $params['provenienza'], ":form_data" => json_encode($form_data), ":creation_time" => time(), ":lang" => USING_LANGUAGE_CODE));

            die(base64_encode($this->MSFrameworkDatabase->lastInsertId()));
        }
        else if($params['action'] == 'sendTicketViaMail') {

            $ticket_id = $params['pID'];
            $ticket_code = $params['ticketID'];

            $result = false;

            $r = (new \MSFramework\database())->getAssoc("SELECT * FROM richieste WHERE id = :id", array(":id" => $ticket_id), true);
            $form_data = json_decode($r['form_data'], true);

            if($form_data['Stato'] == 0) die(json_encode(array("status" => "not_active")));


            if($ticket_code == $form_data['Ticket ID']) {
                $result = $this->ticket->sendTicketViaEmail($form_data['Evento'], $form_data['Biglietto'], $form_data['Ticket ID'], $form_data['Quantità'], $r['nome'], $r['contatto'], $r['messaggio'], '', (isset($form_data['Allegato']) ? $form_data['Allegato'] : ''));
            }

            if($result) echo json_encode(array("status" => "ok", "email" => $r['contatto']));
            else echo json_encode(array("status" => "query_error"));
        }
        die();
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        $array_settings = array();
        return $array_settings;
    }

}
<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Ticket;
use MSFramework\cms;

class events {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Servizio => Dati Servizio) con i dati relativi al servizio.
     *
     *
     * @return array
     */
    public function getNextEvent() {
        return $this->MSFrameworkDatabase->getAssoc("SELECT * FROM ticket_date WHERE attivo = 1 AND data > UNIX_TIMESTAMP() ORDER BY data ASC LIMIT 1", array(), true);
    }

    /**
     * Restituisce un array associativo (ID Servizio => Dati Servizio) con i dati relativi al servizio.
     *
     * @param $id L'ID dell'evento
     * @param string $fields I campi da prelevare dal DB
     * @param $just_active Se impostato su true, recupera solo gli eventi attivi
     * @param $just_next Se impostato su true, recupera solo gli eventi futuri
     *
     * @return array
     */
    public function getEventDetails($id = "", $fields = "*", $just_active = true, $just_next = true) {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = "";
        $append_where_ary = array();
        if($just_active) {
            $append_where .= " AND attivo = '1' ";
        }

        if($just_next) {
            $append_where .= " AND data > UNIX_TIMESTAMP() ";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ticket_date WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ") ORDER by data ASC", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {
            $r['biglietti'] = json_decode($r['biglietti'], true);
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }
}
<?php
/**
 * Sarah Cosmi Restart
 * Date: 1/08/18
 */

namespace MSFramework\Ticket;

class emails extends \MSFramework\emails {
    /**
     * Ottiene la lista di template per l'ecommerce
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {
        //$default_address = (new \MSFramework\cms())->getCMSData("settings")['destination_address'];

        $templates = array(
            'ticket' => array(
                'biglietto-acquistato' => array(
                    'nome' => 'Biglietto Acquistato',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Il tuo Ticket - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{quantita}" => "La quantità di biglietti acquistata",
                            "{ticket_id}" => "L'ID del biglietto",
                            "{qr_image}" => "Il link dell'immagine QR",
                            "{evento}" => "Il nome dell'evento",
                            "{messaggio_allegato}" => "Il messaggio allegato all'acquisto",
                            "{luogo}" => "Il luogo dell'evento",
                            "{data}" => "La data dell'evento",
                            "{tipo_biglietto}" => "Il tipo di biglietto acquistato",
                            "{prezzo_biglietto}" => "Il prezzo del biglietto acquistato",
                            "{descrizione_biglietto}" => "La descrizione del biglietto acquistato"
                        ),
                        array(
                            "{[name]}",
                            "{[email]}",
                            "{[quantita]}",
                            "{[ticket_id]}",
                            '<img src="{[qr_image]}">',
                            "{[evento]}",
                            "{[messaggio_allegato]}",
                            "{[luogo]}",
                            "{[data]}",
                            "{[ticket][nome]}",
                            "{[ticket][prezzo]}",
                            "{[ticket][note_aggiuntive]}",
                        )
                    )
                )

            )
        );

        return $templates;
    }
}
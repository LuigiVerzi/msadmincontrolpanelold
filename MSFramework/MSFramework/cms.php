<?php
/**
 * MSFramework
 * Date: 18/02/18
 */

namespace MSFramework;


class cms {

    private $memcached;
    private $cache_enabled = false;

    private $saasCommonTables = array(
        'producer_config',
        'site'
    );

    public function __construct() {
        global $MSFrameworkDatabase, $MSFrameworkSaaSBase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkSaaSBase = $MSFrameworkSaaSBase;

        $this->virtual_server_name = $this->getVirtualServerName();

        $this->website_url = array();
        $this->cms_data = array();
    }
    
    /**
     * Restituisce i dati impostati all'interno dei moduli "CMS"
     *
     * @param string $what Impostare il tipo di dato desiderato (la riga della tabella "cms")
     * @param bool $replace_with_default Impostare questo parametro su "true" per ricevere i dati "di default" del sito nel caso in cui per quest'ultimo nonsiano stati compilati i campi nel modulo
     * @param bool $cache Se impostato su true salva il risultato della query in memcached (o lo recupera dalla cache, se già presente)
     * @param string $get_from Se impostato, preleva i dati richiesti da un determinato database (nel caso dei siti) o ID SaaS (nel caso dei SaaS). Se non specificato, preleva i dati dal DB corrente.
     * @param string $simulate_type (saas|site) Se impostato su saas, la funzione si comporta come se si trovasse su un SaaS, altrimenti si comporta come se si trovasse su un sito normale. Se non impostato, determina il comportamento in base al dominio corrente.
     *
     * @return mixed
     */
    public function getCMSData($what, $replace_with_default = false, $cache = null, $get_from = "", $simulate_type = null) {
        global $MSFrameworkSaaSBase;

        if($simulate_type == null) {
            $isSaaS = $MSFrameworkSaaSBase->isSaaSDomain() && in_array($what, $this->saasCommonTables); //se vengono richiesti i "settings" i dati vengono recuperati dal database del cliente, anche se il dominio è un SaaS
        } else {
            $isSaaS = ($simulate_type == "saas");
        }

        if($get_from != "") {
            if($isSaaS) {
                $saas_env = $get_from;
            } else {
                $site_env = "`" . $get_from . "`.";
            }
        } else {
            if($isSaaS) {
                $saas_env = $MSFrameworkSaaSBase->getID();
            } else {
                $site_env = "";
            }
        }

        // Cerco se i dati sono stati ottenuti in precedenza per evitare richieste superflue
        $cache_key = base64_encode((string)$what . '_' . $replace_with_default . ($get_from != "" ? "_" . $get_from : ""));
        if(isset($this->cms_data[$cache_key])) {
            return $this->cms_data[$cache_key];
        }

        if($isSaaS) {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT cms FROM `" . FRAMEWORK_DB_NAME . "`.`saas__config` WHERE id = :id", array(":id" => $saas_env), true, $cache);
            $ary_data_cms = json_decode($r['cms'], true);
            $ary_data = $ary_data_cms[$what];
        } else {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT value FROM " . $site_env . "`cms` WHERE type = :type", array(":type" => $what), true, $cache);
            $ary_data = json_decode($r['value'], true);
        }

        if($replace_with_default) {
            if(is_array($ary_data)) {
                foreach($ary_data as $k => $v) {
                    if($v == "") {
                        $ary_data[$k] = $this->getCMSDefaultData($what, $k);
                    }
                }
            } else {
                $ary_data = $this->getCMSDefaultData($what);
            }
        }

        $this->cms_data[$cache_key] = $ary_data;

        return $ary_data;
    }

    /**
     * Permette di settare dei valori all'interno della tabella 'cms'
     *
     * @param string $what Impostare il tipo di dato desiderato (la riga della tabella "cms")
     * @param array $value Il valore da settare
     * @param boolean $merge_array Se impostato combina il valore dell'array con l'eventuale valore precedente
     * @param string $get_from Se impostato, preleva i dati richiesti da un determinato database
     *
     * @return mixed
     */

    public function setCMSData($what, $value, $merge_array = true, $get_from = "") {
        Global $MSFrameworkSaaSBase;

        if(!is_array($value)) return false;

        $old_row = $this->getCMSData($what, false, null, $get_from);

        $db_action = 'insert';
        if($old_row) {
            $db_action = 'update';
        }

        if($merge_array && $old_row) {
            $value = array_merge($old_row, $value);
        }

        $array_to_save = array(
            "value" => json_encode($value),
            "type" => $what
        );

        $table_to_prepend = "";
        if($get_from) {
            $table_to_prepend =  "`" . $get_from . "`.";
        }

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

        if(!$get_from && $MSFrameworkSaaSBase->isSaaSDomain() && in_array($what, $this->saasCommonTables)) {
            $r_old_cms = $this->MSFrameworkDatabase->getAssoc("SELECT cms FROM `" . FRAMEWORK_DB_NAME . "`.`saas__config` WHERE id = :id", array(":id" => $MSFrameworkSaaSBase->getID()), true);
            $old_cms_data = json_decode($r_old_cms['cms'], true);
            $old_cms_data[$what] = json_decode($array_to_save['value'], true);
            $stringForDB = $this->MSFrameworkDatabase->createStringForDB(array("cms" => json_encode($old_cms_data)), $db_action);
            $result = $this->MSFrameworkDatabase->pushToDB("UPDATE `" . FRAMEWORK_DB_NAME . "`.`saas__config` SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $MSFrameworkSaaSBase->getID()), $stringForDB[0]));
        } else if ($db_action == "insert") {
            $result = $this->MSFrameworkDatabase->pushToDB("INSERT INTO " . $table_to_prepend . "`cms` ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
        } else {
            $result = $this->MSFrameworkDatabase->pushToDB("UPDATE " . $table_to_prepend . "`cms` SET " . $stringForDB[1] . " WHERE type = '$what'", $stringForDB[0]);
        }

        return $result;
    }

    /**
     * Restituisce i dati "di default" relativi al tipo di dato selezionato, nel caso in cui i campi non sono stati compilati dall'utente
     *
     * @param string $what Impostare il tipo di dato desiderato (la riga della tabella "cms")
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    private function getCMSDefaultData($what, $key = "") {
        switch($what) {
            case "site":
                $defaults = array(
                    "nome" => "Un altro sito targato " . SW_NAME,
                    "indirizzo" => "",
                    "telefono" => "",
                    "email" => "",
                );
            break;

            case "ecommerce_imposte":
                $defaults = array(
                    "price_type" => "0",
                    "price_type_shop" => "0",
                    "price_type_cart" => "0",
                    "show_tot_type" => "0",
                );
            break;
        }

        if(!is_array($defaults)) {
            return false;
        }

        if($key != "") {
            return $defaults[$key];
        } else {
            return $defaults;
        }
    }

    /**
     * Restituisce l'inirizzo completo dell'azienda
     *
     * @param $short_address bool Mostra una versione più corta dell'indirizzo
     *
     * @return string
     */
    public function getFullAddress($short_address = false) {
        $MSFrameworkGeonames = (new \MSFramework\geonames());
        $siteCMSData = json_decode($this->getCMSData('site')['geo_data'], true);

        if($siteCMSData['indirizzo_visibile'] && !empty($siteCMSData['indirizzo_visibile'])) return $siteCMSData['indirizzo_visibile'];

        $info_indirizzo_cliente = array(
            $siteCMSData['indirizzo'],
            $siteCMSData['cap'],
            $MSFrameworkGeonames->getDettagliComune($siteCMSData['comune'], 'name')['name'] . " (" . $MSFrameworkGeonames->getDettagliProvincia($siteCMSData['provincia'], 'name')['name'] . ")"
        );

        $info_indirizzo_cliente = array_filter($info_indirizzo_cliente);

        $dettagli_provincia = $MSFrameworkGeonames->getDettagliProvincia($siteCMSData['provincia']);

        if($short_address) {
            $provincia = explode('.', $dettagli_provincia['code'])[2];
        } else {
            $provincia = $dettagli_provincia['name'];
        }

        return $siteCMSData['indirizzo'] . ", " . $siteCMSData['cap'] . " " . $MSFrameworkGeonames->getDettagliComune($siteCMSData['comune'], 'name')['name'] . " (" . $provincia . ")";
    }

    /**
     * Viene utilizzata nei casi in cui non è possibile determinare in automatico la cartella in cui si trova il sito del cliente (es: quando si lavora in fase di sviluppo all'interno dell'MSAdminControlPanel)
     * Restituisce il valore della colonna virtual_server_name presente nella tabella "customers" del DB generale di MSFramework
     *
     * @return mixed
     */
    public function getVirtualServerName() {
        $clean_host = $this->getCleanHost();

        if(!$this->MSFrameworkSaaSBase->isSaaSDomain()) {
            // Prendo i dati dalla definizione CUSTOMER_DOMAIN_INFO se già definita per evitare richieste superflue
            if (defined('CUSTOMER_DOMAIN_INFO')) {
                if (strlen(CUSTOMER_DOMAIN_INFO['virtual_server']) > 0) {
                    return CUSTOMER_DOMAIN_INFO['virtual_server'];
                }
            } else {
                if (strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/")) {
                    return $this->MSFrameworkDatabase->getAssoc("SELECT virtual_server_name FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE customer_domain = :domain", array(":domain" => $clean_host), true)['virtual_server_name'];
                }
            }
        }

        return $clean_host;
    }

    /**
     * Restituisce l'ID del sito attuale all'interno della tabella mark833_framework.customers
     *
     * @return mixed
     */
    public function getWebsiteID() {
        $clean_host = $this->getCleanHost();
        return $this->MSFrameworkDatabase->getAssoc("SELECT id FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE customer_domain = :domain", array(":domain" => $clean_host), true)['id'];
    }

    /**
     * Ottiene l'URL completo che punta alla cartella del sito.
     * In produzione sarà http(s)://dominio.ext/
     * In sviluppo sarà http(s)://<devname>.marketingstudio.it/dominio.ext/
     *
     * @param bool $admin Se impostato su "true" ritorna l'URL del backend
     * @param bool $append_virtual Se impostato su "true" aggiunge anche il "Virtual Server Name" alla stringa restituita
     *
     * @return string
     */
    public function getURLToSite($admin = false, $append_virtual = true) {
        // Cerco se l'URL è già stato ottenuto in precedenza per evitare richieste superflue
        $cache_key = base64_encode((string)$admin . '_' . (string)$append_virtual);
        if(isset($this->website_url[$cache_key])) {
            return $this->website_url[$cache_key];
        }

        $http_type = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://');

        $dev_admin_panel = $_SERVER['HTTP_HOST'] . "/MSAdminControlPanel/v" . FRAMEWORK_VERSION . '/';
        $dev_front_domain = $_SERVER['HTTP_HOST'] . ($append_virtual ? "/" . $this->virtual_server_name : "") . "/";
        $prod_domain = ($admin ? ADMIN_URL : ( count( explode('.', str_replace('www.', '', $_SERVER['HTTP_HOST']))) < 3 ? 'www.' : '') ) . ($append_virtual ? $this->virtual_server_name : "") . "/";

        if(($admin && !strstr($_SERVER['HTTP_HOST'], ADMIN_URL)) || (!$admin && strstr($_SERVER['HTTP_HOST'], ADMIN_URL))) { //se la richiesta è per l'URL admin, ma arriva dal front-office (o se la richiesta è per l'URL del front-office, ma arriva dall'ACP), verifico la disponibilità o meno di HTTPS
            $http_type = $this->getHTTPType($admin);
        }

        if(strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/")) {
            if ($admin) {
                $website_url = $http_type . $dev_admin_panel;
            } else {
                $website_url = $http_type . $dev_front_domain;
            }
        } else {
            $website_url = $http_type . $prod_domain;
        }

        // Salvo il link nella variabile cache per evitare future richieste
        $this->website_url[$cache_key] = $website_url;

        return $website_url;
    }

    /**
     * Ottiene la tipologia di HTTP(s) utilizzata
     *
     * @param bool $admin Se impostato su "true" effettua la verifica sul backend
     *
     * @return string
     */
    public function getHTTPType($admin = false) {
        $protocol = $this->getCMSData('server_protocols')[($admin ? 'back' : 'front')];
        return ($protocol ? $protocol : 'http://');
    }

    /**
     * Verifica se il sito è disponibile al pubblico
     *
     * @param bool $allow_to_superadmins Se impostato su "true" il sito risulterà sempre disponibile per gli utenti "SuperAdmin"
     *
     * @return bool
     */
    public function isOnline($allow_to_superadmins = true) {
        if($allow_to_superadmins && $_SESSION['userData']['userlevel'] == "0") {
            return true;
        }

        if((new \MSFramework\database())->getAssoc("SELECT set_offline FROM `" . FRAMEWORK_DB_NAME . "`.`customers` WHERE customer_domain = :domain", array(":domain" => $this->getCleanHost()), true)['set_offline'] == "1") {
            return false;
        }

        return true;
    }

    /**
     * Ottiene il valore dell'host "pulito"
     *
     * @param $url string L'URL da pulire (Lasciando vuoto ottiene quello del server)
     *
     * @return mixed
     */
    public function getCleanHost($url = '') {

        if($url === '') $url = $_SERVER['HTTP_HOST'];

        return str_replace(
            array("http://", "https://", "www.", ADMIN_URL),
            array("", "", "", ""),
            $url
        );
    }

    /**
     * Restituisce i dati delle funzionalità extra abilitabili per i siti
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array
     *
     */
    public function getExtraFunctionsDetails($key = "") {
        $array = array(
            "ecommerce" => array(
                "name" => "Ecommerce",
                "db_prefix" => "ecommerce_"
            ),
            "hotel" => array(
                "name" => "Hotel",
                "db_prefix" => "hotel_",
            ),
            "camping" => array(
                "name" => "Campeggio",
                "db_prefix" => "camping_",
            ),
            "restaurant" => array(
                "name" => "Ristorante",
                "db_prefix" => "restaurant_",
            ),
            "blog" => array(
                "name" => "Blog",
                "db_prefix" => "blog_",
            ),
            "ticket" => array(
                "name" => "Ticket",
                "db_prefix" => "ticket_",
            ),
            "beautycenter" => array(
                "name" => "Beauty Center",
                "db_prefix" => "beautycenter_",
            ),
            "travelagency" => array(
                "name" => "Agenzia di Viaggi",
                "db_prefix" => "travelagency_"
            ),
            "subscriptions" => array(
                "name" => "Sistema Abbonamenti",
                "db_prefix" => "subscriptions_"
            ),
            "affiliations" => array(
                "name" => "Sistema Affiliazioni",
                "db_prefix" => "affiliations__"
            ),
            "appointments" => array(
                "name" => "Calendario Appuntamenti",
                "db_prefix" => "appointments_",
                "dependencies" => array("work_hours")
            ),
            "realestate" => array(
                "name" => "Agenzie Immobiliari",
                "db_prefix" => "realestate_"
            ),
            "working_hours" => array(
                "name" => "Orari di Lavoro Utenti"
            ),
            "points" => array(
                "name" => "Sistema Punti",
                "db_prefix" => "points_"
            ),
        );

        if($key != "") {
            return $array[$key];
        } else {
            return $array;
        }
    }

    /**
     * Restituisce i dati delle funzionalità extra abilitabili per i siti
     * @param string $id L'id della funzionalità da disabilitare
     */
    public function enableExtraFunction($id) {
        global $MSFrameworkSaaSBase, $MSFrameworkSaaSEnvironments;

        $isSaaS = $MSFrameworkSaaSBase->isSaaSDomain();

        $module_db_prefix = $this->getExtraFunctionsDetails($id)['db_prefix'];
        if($module_db_prefix != "") {
            foreach($this->MSFrameworkDatabase->getFromDummyByPrefix($module_db_prefix) as $table) {
                if($isSaaS) {
                    foreach($MSFrameworkSaaSEnvironments->getEnvironmentsBySaaSID() as $env_r) {
                        $this->MSFrameworkDatabase->query("CREATE TABLE `" . (new \MSFramework\SaaS\environments($env_r['id']))->getDBName() . "`.`" . $table['TABLE_NAME'] . "` LIKE `" . DUMMY_DB_NAME . "`.`" . $table['TABLE_NAME'] . "`");
                    }
                } else {
                    $this->MSFrameworkDatabase->query("CREATE TABLE `" . $table['TABLE_NAME'] . "` LIKE `" . DUMMY_DB_NAME . "`.`" . $table['TABLE_NAME'] . "`");
                }
            }
        }
    }

    /**
     * Restituisce i dati delle funzionalità extra abilitabili per i siti
     * @param string $id L'id della funzionalità da disabilitare
     */
    public function disableExtraFunction($id) {
        global $MSFrameworkSaaSBase, $MSFrameworkSaaSEnvironments;

        $isSaaS = $MSFrameworkSaaSBase->isSaaSDomain();

        $module_db_prefix = $this->getExtraFunctionsDetails($id)['db_prefix'];
        if($module_db_prefix != "") {
            foreach($this->MSFrameworkDatabase->getFromDummyByPrefix($module_db_prefix) as $table) {
                if($isSaaS) {
                    foreach($MSFrameworkSaaSEnvironments->getEnvironmentsBySaaSID() as $env_r) {
                        $this->MSFrameworkDatabase->query("DROP TABLE `" . (new \MSFramework\SaaS\environments($env_r['id']))->getDBName() . "`.`" . $table['TABLE_NAME'] . "`");
                    }
                } else {
                    $this->MSFrameworkDatabase->query("DROP TABLE `" . $table['TABLE_NAME'] . "`");
                }
            }
        }
    }

    /**
     * Restituisce un array con tutte le funzionalità extra attive
     *
     * @return array
     */
    public function getActiveExtraFunctions() {
        $extra_functions = json_decode($this->getCMSData('producer_config', array(), false)['extra_functions'], true);
        return $extra_functions;
    }

    /**
     * Restituisce bool se la funzione passata è attiva altrimenti false
     *
     * @param string $id L'id della funzionalità da controllare
     *
     * @return boolean
     */
    public function checkExtraFunctionsStatus($id = "") {
        return in_array($id, $this->getActiveExtraFunctions());
    }

    /**
     * Restituisce "back" se la richiesta ha avuto origine dal backoffice (ACP), e "front" se la richiesta ha avuto origine dal frontoffice (sito)
     *
     * @return string
     */
    public function checkRequestOrigin() {
        if(strstr($_SERVER['HTTP_HOST'], ADMIN_URL) || strstr(REAL_SW_PATH, "MSAdminControlPanel")) {
            return "back";
        } else {
            return "front";
        }
    }

    /**
     * Restituisce il percorso del logo del SW in base alle impostazioni selezionate nell'ACP
     *
     * @return string
     */
    public function getLoginLogoPath() {
        global $MSFrameworkCMS;
        $r_settings = $MSFrameworkCMS->getCMSData('site');
        $logos = json_decode($r_settings["logos"], true);

        $path = "assets/img/logo.svg";
        if($r_settings['use_main_logo_in_login'] == "1") { //legacy per impostazione vecchia
            $path = UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logos['logo'];
        } else {
            if($r_settings['use_logo_in_login'] == "1") {
                $path = UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logos['logo'];
            } elseif($r_settings['use_logo_in_login'] == "2") {
                $path = UPLOAD_LOGOS_FOR_DOMAIN_HTML . $logos['login_logo'];
            }
        }

        return $path;
    }

    /**
     * Verifica se il FW sta girando nell'ambiente di Staging
     *
     * @return bool
     */
    public function isStaging() {
        return in_array($this->getCleanHost(), array(STAGING_DOMAIN, SAAS_STAGING_DOMAIN));
    }

    /**
     * Verifica se il FW sta girando nell'ambiente di Sviluppo
     *
     * @return bool
     */
    public function isDevelopment() {
        return (strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/") !== false ? true : false);
    }
}
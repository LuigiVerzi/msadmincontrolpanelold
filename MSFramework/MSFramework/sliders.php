<?php
/**
 * MSFramework
 * Date: 05/03/18
 */

namespace MSFramework;


class sliders {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Slider => Dati Slider) con i dati relativi allo slider.
     * L'array contiene (in una chiave addizionale 'slider_friendly' aggiunta al volo) sia le informazioni da utilizzare nel DOM (HTML) che il path assoluto delle immagini
     *
     * @param $id L'ID dello slider (stringa) o degli slider (array) richieste (se vuoto, vengono recuperati i dati di tutti gli slider)
     * @param $just_home Se impostato su true, recupera solo i dati degli slider con il check "Mostra in home page" attivo
     * @param string $fields I campi da prelevare dal DB
     * @param string $check_device Se impostato su true, restituisce solo gli slider disponibili in base al dispositivo (mobile/desktop) che effettua la richiesta senza modificare il formato del return
     *
     * @return array
     */
    public function getSliderDetails($id = "", $just_home = false, $fields = "*", $check_device = true) {
        global $firephp, $MSFrameworki18n;

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        $must_have_fields = array("id", "images", "images_mobile", "desktop_rollback");
        foreach($must_have_fields as $mfield) {
            if($fields != "*" && !strstr($fields, $mfield)) {
                $fields .= ", " . $mfield;
            }
        }

        $append_where = "";
        $append_where_ary = array();
        if($just_home) {
            $append_where .= " AND use_in_home = '1' ";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM sliders WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ")", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {

            $slider_images_overlay = json_decode($r['images_overlay'], true);
            if(!is_array($slider_images_overlay) || count($slider_images_overlay) == 0) {
                $slider_images_overlay = null;
            }

            if(((new \MobileDetect\MobileDetect())->isMobile() || (new \MobileDetect\MobileDetect())->isTablet()) && $check_device) {
                $slider_images = json_decode($r['images_mobile'], true);
                if(!is_array($slider_images) || count($slider_images) == 0) {
                    $slider_images = null;

                    if($r['desktop_rollback'] == "1") {
                        $slider_images = json_decode($r['images'], true);
                    }
                }
            } else {
                $slider_images = json_decode($r['images'], true);
                $slider_images_mobile = json_decode($r['images_mobile'], true);
            }

            if(!empty($r['background_video'])) {
                $r['background_video'] = (new \MSFramework\utils())->getEmbedVideoUrl($MSFrameworki18n->getFieldValue($r['background_video']), true);
            }

            if(is_array($slider_images)) {
                $r['slider_friendly'] = array();
                $r['slider_friendly_overlay'] = array();

                foreach($slider_images_overlay as $kfile => $file) {
                    $r['slider_friendly_overlay'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SLIDERS_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SLIDERS_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SLIDERS_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_SLIDERS_FOR_DOMAIN . "tn/" . $file
                        ),
                    );
                }

                foreach($slider_images as $kfile => $file) {
                    $r['slider_friendly'][] = array(
                        "html" => array(
                            "main" => UPLOAD_SLIDERS_FOR_DOMAIN_HTML . $file,
                            "thumb" => UPLOAD_SLIDERS_FOR_DOMAIN_HTML . "tn/" . $file
                        ),
                        "absolute" => array(
                            "main" => UPLOAD_SLIDERS_FOR_DOMAIN . $file,
                            "thumb" => UPLOAD_SLIDERS_FOR_DOMAIN . "tn/" . $file
                        ),
                    );

                    if(!$check_device) {
                        $r['slider_friendly_mobile'] = array();
                        if($slider_images_mobile[$kfile] != "") {
                            $r['slider_friendly_mobile'][] = array(
                                "html" => array(
                                    "main" => UPLOAD_SLIDERS_FOR_DOMAIN_HTML . $slider_images_mobile[$kfile],
                                    "thumb" => UPLOAD_SLIDERS_FOR_DOMAIN_HTML . "tn/" . $slider_images_mobile[$kfile]
                                ),
                                "absolute" => array(
                                    "main" => UPLOAD_SLIDERS_FOR_DOMAIN . $slider_images_mobile[$kfile],
                                    "thumb" => UPLOAD_SLIDERS_FOR_DOMAIN . "tn/" . $slider_images_mobile[$kfile]
                                ),
                            );
                        }
                    }
                }

                $ary_to_return[$r['id']] = $r;
            }
        }

        return $ary_to_return;
    }
}
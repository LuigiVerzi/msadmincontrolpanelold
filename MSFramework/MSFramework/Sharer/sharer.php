<?php
/**
 * MSAdminControlPanel
 * Date: 30/09/2018
 */

namespace MSFramework\Sharer;

class sharer {
    /**
    * Se impostata su "true" disabilita la funzionalità di condivisione di Facebook in tutto il SW
    * @var bool
    */
    private $disableSocialSharing = false;

    public function __construct() {
        global $firephp, $MSFrameworkDatabase, $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;

        if(FRAMEWORK_DEBUG_MODE) {
            $this->disableSocialSharing = false;
        }
    }

    /**
     * Determina se ci sono tutte le condizioni necessarie affinchè l'utente possa condividere un elemento sui social
     *
     * @param $social Il nome del social per il quale verificare la disponibilità a condividere. Se vuoto, verifica se è possibile condividere su almeno uno dei social
     *
     * @return bool
     */
    public function canShareOnSocial($social = "") {
        global $firephp;

        $array_social = array(
            "facebook" => (trim($this->MSFrameworkCMS->getCMSData('site')['fb_page_id']) != "" && $_SERVER['HTTPS'] === 'on' && !$this->disableSocialSharing),
            "instagram" => (trim($this->MSFrameworkCMS->getCMSData('site')['ig_page_id']) != "" && $_SERVER['HTTPS'] === 'on' && !$this->disableSocialSharing)
        );

        //$array_social['instagram'] = true; //tmp

        if($social == "") {
            return in_array(1, array_map('intval', $array_social));
        } else {
            return $array_social[$social];
        }
    }

    //TODO. questa funzione deve controllare se nel modulo è presente lo slug, in modo da abilitare il pulsante condividi ALMENO per prelevare l'URL dell'elemento da condividere. per il momento fa il solito controllo dei 3 moduli abilitati
    public function canShare($module_id = "") {
        return in_array($module_id, array("blog_articoli", "ecommerce_prodottiprodotti", "realestate_immobili"));
    }
}
?>

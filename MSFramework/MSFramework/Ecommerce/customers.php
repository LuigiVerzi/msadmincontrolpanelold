<?php
/**
 * MSFramework
 * Date: 01/05/18
 */

namespace MSFramework\Ecommerce;

class customers extends \MSFramework\customers {

    public $customer_fatturazione_match = array(
        "ragione_sociale" => "ragione_sociale",
        "piva" => "piva",
        "cf" => "cf",
        "indirizzo" => "indirizzo",
        "stato" => "stato",
        "citta" => "comune",
        "provincia" => "citta",
        "regione" => "regione",
        "cap" => "cap"
    );

    public $customer_data_match = array(
        "nome" => "nome",
        "cognome" => "cognome",
        "indirizzo" => "indirizzo",
        "stato" => "stato",
        "regione" => "regione",
        "citta" => "comune",
        "provincia" => "citta",
        "cap" => "cap",
        "telefono_casa" => "telefono",
        "telefono_cellulare" => "cellulare",
    );

    public function __construct() {
        parent::__construct();
    }

    public function checkCredentials($user, $pass) {

        $can_login = parent::checkCredentials($user, $pass);

        if($can_login) {

            $session_cart = (new \MSFramework\Ecommerce\cart())->getCart();

            if($session_cart['count'] > 0) {
               $this->saveInfo('carrello', $session_cart['products']);
            }
            else {
                $db_cart = $this->getInfo('carrello', array());
                if($db_cart && $db_cart['count'] > 0 && count($db_cart['products']) > 0) (new \MSFramework\Ecommerce\cart())->syncCart($db_cart['products']);
            }

            return true;
        } else {
            return false;
        }

    }

    /**
     * Crea un nuovo account nel DB
     *
     * @param $nome string Il nome dell'utente
     * @param $cognome string Il cognome dell'utente
     * @param $email string La mail dell'utente
     * @param $password string La password dell'utente
     * @param $cellulare string Il cellulare dell'utente
     * @param $livello string Il livello dell'utente (DEPRECATO; RIMANE FINO A QUANDO NON VERRà TOLTO DALLE FUNZIONI)
     * @param $secondary array Colonne secondarie da aggiungere
     *
     * @return bool
     */
    public function registerUser($nome, $cognome, $email, $password, $cellulare, $livello, $secondary = array()) {

        $inserted_id = parent::registerUser($nome, $cognome, $email, $password, $cellulare, 3, $secondary);

        if($inserted_id) {

            $array_to_save = array(
                "id" => $inserted_id,
                "carrello" => json_encode((new \MSFramework\Ecommerce\cart())->getCart()['products']),
            );

            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "insert");
            $this->MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_users_extra ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);

            // Invia l'email di convalida account
            $user = $this->getUserDataFromDB($inserted_id);

            $activation_code = $user['mail_auth'];
            $email_address = $user['email'];

            $emails = new \MSFramework\Ecommerce\emails();

            $return_array['status'] = $emails->sendMail('account-activation', array(
                'nome' => (!empty($user['nome']) ? $user['nome'] . ' ' . $user['cognome'] : $user['email']),
                'email' => $email_address,
                'subject' => (new \MSFramework\i18n())->gettext('Convalida account'),
                'code' => $activation_code
            ));

        }

        return $inserted_id;
    }

    /**
     * Recupera i dati dell'utente dal database + le info extra
     *
     * @param string $id L'ID dell'utente.
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getUserDataFromDB($id, $fields = "*") {
        $userdata =  parent::getCustomerDataFromDB($id, $fields);
        if($userdata) {
            $userdata['extra'] = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_users_extra WHERE id = :id", array(":id" => $id), true);
        }
        return $userdata;
    }

    /**
     * Ottiene le info relative all'ecommerce
     *
     * @param string $tabella La colonna da aggiornare
     * @param array $default L'array che verrà ritornato nel caso in cui non vengono trovati dati
     * @param integer $user_id L'ID del cliente
     *
     * @return array
     */
    public function getInfo($tabella, $default = array(), $user_id = 0) {

        if(!$user_id) {
            if(isset($_SESSION['customerData'])) {
                $user_id = $_SESSION['customerData']['user_id'];
            }
        }

        if($user_id) {
            $r = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_users_extra WHERE id = :id", array(":id" => $user_id), true);
            $json_table = json_decode($r[$tabella], true);

            if($tabella === 'dati_fatturazione') {

                $customer_data = parent::getCustomerDataFromDB($user_id);
                $customer_dati_fatturazione = (json_decode($customer_data['dati_fatturazione']) ? json_decode($customer_data['dati_fatturazione'], true) : array());

                // Se alcuni dati di fatturazione ecommerce sono vuoti allora li prendo dai dati fatturazione del cliente
                foreach($this->customer_fatturazione_match as $fatturazione_standard => $fatturazione_ecommerce) {
                    if(!$json_table[$fatturazione_ecommerce] && !empty($customer_dati_fatturazione[$fatturazione_standard])) {
                        $json_table[$fatturazione_ecommerce] = $customer_dati_fatturazione[$fatturazione_standard];
                    }
                }

                // Se non trova ancora i dati li prendo dalla tabella del cliente (customers)
                foreach($this->customer_data_match as $dati_standard => $fatturazione_ecommerce) {
                    if(!$json_table[$fatturazione_ecommerce] && !empty($customer_data[$dati_standard])) {
                        $json_table[$fatturazione_ecommerce] = $customer_data[$dati_standard];
                    }
                }

            }

            if($json_table) return $json_table;
        }

        return $default;
    }

    /**
     * Salva le info relative all'ecommerce sulla tabella
     *
     * @param string $tabella La colonna da aggiornare
     * @param array $info L'array contenente i dati da inserire
     * @param integer $user_id L'ID del cliente
     *
     * @return mixed
     */
    public function saveInfo($tabella, $info, $user_id = 0) {

        if(!$user_id) {
            if(isset($_SESSION['customerData'])) {
                $user_id = $_SESSION['customerData']['user_id'];
            }
        }

        if($user_id) {
            $info_json = json_encode($info);

            if($tabella == 'dati_fatturazione') {

                $customer_data = parent::getCustomerDataFromDB($user_id);
                $customer_dati_fatturazione = (json_decode($customer_data['dati_fatturazione']) ? json_decode($customer_data['dati_fatturazione'], true) : array());

                $customer_data_to_update = array();

                // Se non presenti i dati di fatturazione del cliente anche nella tabella customers['dati_fatturazione']
                foreach($this->customer_fatturazione_match as $fatturazione_standard => $fatturazione_ecommerce) {
                    if(empty($customer_dati_fatturazione[$fatturazione_standard]) && !empty($info[$fatturazione_ecommerce])) {
                        $customer_dati_fatturazione[$fatturazione_standard] = $info[$fatturazione_ecommerce];

                        if($fatturazione_standard === 'stato') {
                            $customer_dati_fatturazione[$fatturazione_standard] = (new \MSFramework\geonames())->getCountryDetails($customer_dati_fatturazione[$fatturazione_standard])[$customer_dati_fatturazione[$fatturazione_standard]]['name'];
                        }
                    }
                }

                if($customer_dati_fatturazione) {
                    $customer_data_to_update['dati_fatturazione'] = json_encode($customer_dati_fatturazione);
                }

                // Se non presenti salvo i dati di fatturazione del cliente anche nella tabella customers
                foreach($this->customer_data_match as $customer_standard_info => $fatturazione_ecommerce) {
                    if(empty($customer_data[$customer_standard_info]) && !empty($info[$fatturazione_ecommerce])) {
                        $customer_data_to_update[$customer_standard_info] = $info[$fatturazione_ecommerce];

                        if($customer_standard_info === 'stato') {
                            $customer_data_to_update[$customer_standard_info] = (new \MSFramework\geonames())->getCountryDetails($customer_data_to_update[$customer_standard_info])[$customer_data_to_update[$customer_standard_info]]['name'];
                        }

                    }
                }

                $stringForDB = $this->MSFrameworkDatabase->createStringForDB($customer_data_to_update, 'update');
                $this->MSFrameworkDatabase->pushToDB("UPDATE customers SET $stringForDB[1] WHERE id = :user_id", array_merge(array(":user_id" => $user_id), $stringForDB[0]));
            }

            if (!$this->MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_users_extra WHERE id = :user_id", array(':user_id' => $user_id)) ) {
                return $this->MSFrameworkDatabase->query("INSERT INTO ecommerce_users_extra (id, $tabella, edit_date) VALUES (:user_id, :info, NOW())", array(":info" => $info_json, ':user_id' => $user_id));
            } else {
                return $this->MSFrameworkDatabase->query("UPDATE ecommerce_users_extra SET $tabella = :info, edit_date = NOW(), cart_notified = 0 WHERE id = :user_id", array(":info" => $info_json, ':user_id' => $user_id));
            }
        }

        return false;
    }

}
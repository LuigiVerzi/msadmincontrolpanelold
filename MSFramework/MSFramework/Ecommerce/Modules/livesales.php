<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Ecommerce\Modules;

use MobileDetect\MobileDetect;
use MSFramework\Ecommerce\orders;

class livesales {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {

        if($this->modules->getSettingValue('livesales', 'hide_mobile') == "1" && (new MobileDetect())->isMobile()) {
            return array();
        }

        $files_to_include = array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'modules/ecommerce/live-sales/js/livesales.js'),
            'css' => array(FRAMEWORK_COMMON_CDN . 'modules/ecommerce/live-sales/css/livesales.css')
        );

        return $files_to_include;
    }

    /**
     * Ottiene le impostazioni del modulo
     *
     * @return array
     */
    public function getModuleSettings()
    {

        $module_settings = array(
            'cdn_url' => FRAMEWORK_COMMON_CDN,
            'interval' => $this->modules->getSettingValue('livesales', 'interval'),
            'duration' => $this->modules->getSettingValue('livesales', 'duration'),
            'total_per_page' => $this->modules->getSettingValue('livesales', 'total_per_page'),
            'max_old_date' => $this->modules->getSettingValue('livesales', 'max_old_date'),
            'play_sound' => $this->modules->getSettingValue('livesales', 'play_sound')
        );

        return $module_settings;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {
        return true;
    }

    /**
     * Funzione che viene richiamata nel footer dei siti common
     *
     * @return string
     */
    public function executeFunctions_footer() {

        echo '<script>window.live_sales_settings = ' . json_encode($this->getModuleSettings()) . ';</script>';
        echo '<script>window.live_sales_orders = ' . json_encode($this->getLiveSalesOrders()) . ';</script>';

        return true;
    }

    /**
     * Ottiene degli ordini in modo random
     *
     * @return array
     */
    private function getLiveSalesOrders() {
        $module_settings = $this->getModuleSettings();
        $purchase_info = array();

        $random_orders = $this->MSFrameworkDatabase->getAssoc("SELECT id FROM ecommerce_orders WHERE EXISTS(SELECT id FROM ecommerce_users_extra WHERE ecommerce_users_extra.id = ecommerce_orders.user_id) AND DATE(order_date) >= CURDATE() - INTERVAL " . $module_settings['max_old_date'] . " DAY  ORDER BY RAND() LIMIT " . $module_settings['total_per_page'], array());

        if($random_orders) {
            foreach ($random_orders as $random_order) {
                $random_order = (new \MSFramework\Ecommerce\orders())->getOrderDetails($random_order['id'])[$random_order['id']];

                // Rimuovo i prodotti non reali (Esempio: Contrassegno)
                foreach ($random_order['cart']['products'] as $pk => $pv) {
                    if ($pv['product_id'] == 0) unset($random_order['cart']['products'][$pk]);
                }

                $random_product = $random_order['cart']['products'][array_rand($random_order['cart']['products'])];

                $product_image = UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML . $random_product['image'];
                $product_name = $random_product['nome'];
                $product_url = ABSOLUTE_SW_PATH_HTML . $random_product['slug'];

                $order_date = (new \MSFramework\utils())->smartdate(strtotime($random_order['order_date']));

                $purchase_info[] = array(
                    'rand' => $random_order,
                    'title' => str_replace(array('{luogo}', '{nome}'), array($random_order['info_spedizione']['citta'], $random_order['info_spedizione']['nome']), (new \MSFramework\i18n())->gettext('{nome} da {luogo} ha acquistato:')),
                    'content' => '<a href="' . $product_url . '"><img src="' . $product_image . '">' . $product_name . '</a><small>' . $order_date . '</small>'
                );
            }
        }

        return $purchase_info;
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        return array(
            "interval" => array(
                "name" => "Intervallo",
                "input_type" => "number",
                "input_attributes" => array("min" => "0"),
                "helper" => "",
                "mandatory" => false,
                "translations" => false,
                "default" => 30000,
            ),
            "duration" => array(
                "name" => "Durata",
                "input_type" => "number",
                "input_attributes" => array("min" => "0"),
                "input_classes" => "",
                "helper" => "",
                "mandatory" => false,
                "translations" => false,
                "default" => 7000,
            ),
            "total_per_page" => array(
                "name" => "Totale per pagina",
                "input_type" => "number",
                "input_attributes" => array("min" => "0"),
                "helper" => "",
                "mandatory" => false,
                "translations" => false,
                "default" => 3,
            ),
            "max_old_date" => array(
                "name" => "Numero giorni precedenti",
                "input_type" => "number",
                "input_attributes" => array("min" => "0"),
                "helper" => "Impostando questo valore, verranno visualizzati solo gli ordini effettuati negli 'n' giorni precedenti",
                "mandatory" => false,
                "translations" => false,
                "default" => 7,
            ),
            "play_sound" => array(
                "name" => "Abilita suoni",
                "input_type" => "checkbox",
                "helper" => "",
                "mandatory" => false,
                "translations" => false,
                "default" => "0",
            ),
            "hide_mobile" => array(
                "name" => "Nascondi su smartphone",
                "input_type" => "checkbox",
                "helper" => "",
                "mandatory" => false,
                "translations" => false,
                "default" => "0",
            )
        );
    }

}
<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Ecommerce\Modules;

class feeds {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {

        $files_to_include = array();

        return $files_to_include;
    }

    /**
     * Ottiene le impostazioni del modulo
     *
     * @return array
     */
    public function getModuleSettings()
    {
        return array();
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params, $slug = '') {
        Global $MSFrameworki18n;

        $ecommerceProducts = (new \MSFramework\Ecommerce\products())->getProductDetails();
        $ecommerceCategories = (new \MSFramework\Ecommerce\categories())->getCategoryDetails();

        if($slug[0] === 'facebook.csv') {


            $products = array();
            foreach($ecommerceProducts as $product) {

                if(($product['prezzo']['tax'][0] . '.' . round($product['prezzo']['tax'][1])) <= 0) continue;
                if(!count($product['gallery_friendly'])) continue;

                $prezzo = $product['prezzo']['tax'][0] . '.' . round($product['prezzo']['tax'][1], 2);
                $prezzo_offerta = $product['prezzo']['tax'][0] . '.' . round($product['prezzo']['tax'][1], 2);

                if(floatval(implode('.', $product['prezzo_promo']['tax'])) > 0) {
                    $prezzo_offerta = $product['prezzo_promo']['tax'][0] . '.' . round($product['prezzo_promo']['tax'][1], 2);
                }

                $additional_images = array();
                foreach($product['gallery_friendly'] as $k => $v) {
                    if($k > 0) {
                        $additional_images = 'http:' . $v['html']['main'];
                    }
                }

                $products[] = array(
                    'id' => $product['id'], // ID Prodotto
                    'availability' => (!$product["manage_warehouse"] || $product['warehouse_qty'] > 0 ? 'in stock' : 'out of stock'), // Quantità
                    'condition' => 'new', // Condizione
                    'product_type' => $MSFrameworki18n->getFieldValue($ecommerceCategories[$product['category']]['nome']), // Condizione
                    'description' => (!empty(strip_tags($MSFrameworki18n->getFieldValue($product['short_descr']))) && strlen($MSFrameworki18n->getFieldValue($product['short_descr'])) > 30 ? strip_tags($MSFrameworki18n->getFieldValue($product['short_descr'])) : strip_tags($MSFrameworki18n->getFieldValue($product['long_descr']))), // Descrizione
                    'image_link' => 'http:' . $product['gallery_friendly'][0]['html']['main'], // Immagine
                    'additional_image_link' => implode(',', $additional_images), // Immagini addizionali
                    'link' =>  $product['url'], // URL Prodotto
                    'title' => $MSFrameworki18n->getFieldValue($product['nome']), // Nome prodotto
                    'price' => $prezzo . ' EUR', // Prezzo prodotto
                    'sale_price' => $prezzo_offerta . ' EUR', // Prezzo prodotto
                    'gtin' => $product['ean'], // EAN
                    'mpn' => ($product['product_code'] ? $product['product_code'] : $product['id']), // Codice univoco
                );
            }

            return $this->array2csv($products);
        }
        else if($slug[0] === 'googleads.csv')
        {
            $products = array();
            foreach($ecommerceProducts as $product) {

                $additional_images = array();
                foreach($product['gallery_friendly'] as $k => $v) {
                    if($k > 0) {
                        $additional_images = 'http:' . $v['html']['main'];
                    }
                }

                if(($product['prezzo']['tax'][0] . '.' . round($product['prezzo']['tax'][1])) <= 0) continue;
                if(!count($product['gallery_friendly'])) continue;

                $prezzo = $product['prezzo']['tax'][0] . '.' . round($product['prezzo']['tax'][1], 2);
                $prezzo_offerta = $product['prezzo']['tax'][0] . '.' . round($product['prezzo']['tax'][1], 2);
                if((float)implode('.', $product['prezzo_promo']) > 0) {
                    $prezzo_offerta = $product['prezzo_promo']['tax'][0] . '.' . round($product['prezzo_promo']['tax'][1], 2);
                }

                $products[] = array(
                    'id' => $product['id'], // ID Prodotto
                    'availability' => (!$product["manage_warehouse"] || $product['warehouse_qty'] > 0 ? 'in stock' : 'out of stock'), // Quantità
                    'condition' => 'new', // Condizione
                    'product_type' => $MSFrameworki18n->getFieldValue($ecommerceCategories[$product['category']]['nome']), // Condizione
                    'description' => (!empty(strip_tags($MSFrameworki18n->getFieldValue($product['short_descr']))) && strlen($MSFrameworki18n->getFieldValue($product['short_descr'])) > 30 ? strip_tags($MSFrameworki18n->getFieldValue($product['short_descr'])) : strip_tags($MSFrameworki18n->getFieldValue($product['long_descr']))), // Descrizione
                    'image_link' => 'http:' . $product['gallery_friendly'][0]['html']['main'], // Immagine
                    'additional_image_link' => implode(',', $additional_images), // Immagini addizionali
                    'link' =>  $product['url'], // URL Prodotto
                    'title' => $MSFrameworki18n->getFieldValue($product['nome']), // Nome prodotto
                    'price' => $prezzo . ' EUR', // Prezzo prodotto
                    'sale_price' => $prezzo_offerta . ' EUR', // Prezzo prodotto
                    'gtin' => $product['ean'], // EAN
                    'mpn' => ($product['product_code'] ? $product['product_code'] : $product['id']), // Codice univoco
                );
            }

            return $this->array2csv($products);
        }

        return true;
    }

    /**
     * Funzione che viene richiamata nel footer dei siti common
     *
     * @return string
     */
    public function executeFunctions_footer() {
        return true;
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        return array();
    }

    private function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

}
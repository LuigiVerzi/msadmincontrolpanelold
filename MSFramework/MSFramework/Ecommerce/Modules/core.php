<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Ecommerce\Modules;

class core {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {

        $files_to_include = array(
            'js' => array(
                FRAMEWORK_COMMON_CDN . 'modules/ecommerce/core/cart.js',
                FRAMEWORK_COMMON_CDN . 'modules/ecommerce/core/checkout.js',
                FRAMEWORK_COMMON_CDN . 'modules/ecommerce/core/wishlist.js',
            )
        );

        return $files_to_include;
    }

    /**
     * Ottiene le impostazioni del modulo
     *
     * @return array
     */
    public function getModuleSettings()
    {
        return array();
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params, $slug = '') {
        Global $MSFrameworki18n;

        $action = $params['action'];

        if($slug[0] === 'cart') {

            if($action == 'getProductVariations') {

                $products = new \MSFramework\Ecommerce\products();
                $array_to_return = $products->getProductVariationInfo($params['id'], $params['variations']);

                if($array_to_return['gallery'] !== $array_to_return['original_gallery']) {
                    $new_gallery = $array_to_return['original_gallery'];
                    $new_gallery[0] = $array_to_return['gallery'][0];
                    $array_to_return['gallery'] = $new_gallery;
                }

                return json_encode($array_to_return);

            } else if($action == 'addToCart') {
                $cart = new \MSFramework\Ecommerce\cart();

                $product_id = $params['id'];
                $variations = $params['variations'];
                $quantity = $params['quantity'];
                $custom = $params['custom'];

                $return_array = $cart->addToCart($product_id, $quantity, $variations, $custom);

                if($return_array['status'] == 'insert') $return_array['status_message'] = $MSFrameworki18n->gettext('Prodotto aggiunto correttamente al carrello');
                else if($return_array['status'] == 'updated') $return_array['status_message'] = $MSFrameworki18n->gettext('La quantità di questo prodotto nel tuo carrello è stata aggiornata');
                else if($return_array['status'] == 'no_avaialable') $return_array['status_message'] = $MSFrameworki18n->gettext('La quantità scelta non è disponibile o hai già inserito questo prodotto nel tuo carrello');
                else if($return_array['status'] == 'custom_required') $return_array['status_message'] = $MSFrameworki18n->gettext('Questo prodotto richiede ulteriori info prima di essere acquistato, per favore compila i campi richiesti.');

                if($return_array['status'] == 'insert' || $return_array['status'] == 'updated') {
                    $return_array['cart_message'] = $MSFrameworki18n->gettext('Hai aggiunto %s nel carrello.');
                    $return_array['cart_button_message'] = $MSFrameworki18n->gettext('Clicca qui per andare alla cassa.');
                }

                return json_encode($return_array);

            } else if($action == 'removeFromCart') {
                $cart = new \MSFramework\Ecommerce\cart();

                $cart_products = $cart->getCart()['products'];

                $cart_id = $params['id'];

                foreach($cart_products as $k=>$product) {
                    if($product['cart_unique_id'] == $cart_id) unset($cart_products[$k]);
                }

                $clean_cart = $cart->cleanCartProducts($cart_products);
                $cart->syncCart($clean_cart['products']);

                return json_encode(array('data' => $clean_cart, 'message' => $MSFrameworki18n->gettext("Il prodotto è stato rimosso correttamente dal carrelo")));

            }

        }

        return true;
    }

    /**
     * Funzione che viene richiamata nel footer dei siti common
     *
     * @return string
     */
    public function executeFunctions_footer() {
        return true;
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        return array();
    }

}
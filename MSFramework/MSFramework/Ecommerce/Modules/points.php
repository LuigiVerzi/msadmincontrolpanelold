<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Ecommerce\Modules;

class points {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista di Hook da caricare
     */
    public function setHooks() {
        Global $MSHooks, $MSFrameworkCMS;

        $pointSettings = $MSFrameworkCMS->getCMSData('ecommerce_points');

        if($pointSettings['enabled'] == "1") {

            $MSFrameworkPoints = new \MSFramework\Points\points();

            /* ASSEGNAZIONE DEI PUNTI */
            if((float)$pointSettings['assignation']['registration_points'] > 0) {
                $MSHooks->action('user', 'registration')->add(function ($data) use(&$MSFrameworkPoints, &$pointSettings) {
                    $MSFrameworkPoints->addNewOperation($data['user_id'], $pointSettings['assignation']['registration_points'], 'Bonus Benvenuto #welcome');
                });
            }

            if((float)$pointSettings['assignation']['newsletter_points'] > 0) {
                $MSHooks->action('newsletter', 'subscribe')->add(function ($data) use(&$MSFrameworkPoints, &$pointSettings) {
                    $MSFrameworkPoints->addNewOperation($data['user_id'], $pointSettings['assignation']['newsletter_points'], 'Bonus Iscrizione Newsletter #newsletter');
                });
            }

            $MSHooks->action('ecommerce', 'checkout', 'after_Payment')->add(function ($data) use(&$MSFrameworkPoints, &$pointSettings) {

                $orderDetails = (new \MSFramework\Ecommerce\orders())->getOrderDetails($data['order_id'])[$data['order_id']];

                if(isset($orderDetails['coupons_used']['POINTS'])) {
                    $pointsToRemove = $orderDetails['coupons_used']['POINTS']['points_used'];
                    $MSFrameworkPoints->addNewOperation($data['user_id'], (float)('-' . $pointsToRemove), 'Sconto sull\'acquisto #pay' . $data['order_id']);
                }

                // Rimuovi i rincari (Es: Contrassegno) dal totale.
                $valore_totale = $data['cart_total'];
                foreach($orderDetails['cart']['products'] as $ck => $cp) {
                    if($cp['product_id'] === 'rincaro_pagamento') {
                        $valore_totale -= (float)implode('.', $cp['prezzo']['tax']);
                    }
                }

                $pointsToAssign = $MSFrameworkPoints->getPointsToAssign($valore_totale);
                if($pointsToAssign > 0) {
                    $MSFrameworkPoints->addNewOperation($data['user_id'], $pointsToAssign, 'Bonus Acquisto #earn' .  $data['order_id']);
                }
            });

            $MSHooks->action('ecommerce', 'checkout', 'before_CartSummary')->add(function ($data) use(&$MSFrameworkPoints, &$pointSettings) {
                echo '<div class="checkoutPointsEarningBox">';
                echo strtr($pointSettings['messages']['checkout_earning'],
                    array(
                        '{points}' => $MSFrameworkPoints->getPointsToAssign($data['cart_total'])
                    )
                );
                echo '</div>';

                if(!$_SESSION["cart_coupon"]['POINTS']) {
                    $applicableSale = $MSFrameworkPoints->getSaleValue((new \MSFramework\customers())->getUserDataFromSession('user_id'), $data['cart_total']);

                    if ($applicableSale[0]) {

                        echo '<div class="checkoutApplyPointsBox">';
                        echo strtr($pointSettings['messages']['checkout_sale'],
                            array(
                                '{points}' => $applicableSale[1],
                                '{points-value}' => $applicableSale[0]
                            )
                        );
                        echo '<a href="#" class="applyPointsSaleBtn ' . $pointSettings['messages']['checkout_apply_sale_class'] . '">' . $pointSettings['messages']['checkout_apply_sale_txt'] . '</a>';
                        echo '<div style="clear: both;"></div>';
                        echo '</div>';

                        Global $MSFrameworkParser;
                        $MSFrameworkParser->addScript("$('.applyPointsSaleBtn').on('click', function(e) {
                            e.preventDefault();
                            
                             $.ajax({
                                url: window.base_url + 'm/points',
                                type: 'POST',
                                data: {
                                    action: 'applyPointsSale',
                                },
                                dataType: 'json',
                                success: function (data) {
                                    location.reload();
                                }
                             });
                        });");

                    }
                }

            });

            $MSHooks->action('ecommerce', 'buttons', 'before_AddToCart')->add(function ($data) use(&$MSFrameworkPoints, &$pointSettings) {
                $productInfo = (new \MSFramework\Ecommerce\products())->getProductDetails($data['product_id'])[$data['product_id']];

                echo strtr(($productInfo['range_prezzo'] ? $pointSettings['messages']['product_variable'] : $pointSettings['messages']['product']),
                    array(
                        '{points}' => $MSFrameworkPoints->getPointsToAssign(implode('.', ($productInfo['range_prezzo'] ? $productInfo['range_prezzo'][1] : $productInfo['prezzo']['tax'])))
                    )
                );
            });

            /* APPLICO LO SCONTO AL CARRELLO SE IMPOSTATO */
            if($_SESSION["cart_coupon"]['POINTS']) {
                $MSHooks->filter('ecommerce', 'getCart')->add(function ($currentCart) use (&$MSFrameworkPoints, &$pointSettings) {

                    $applicableSale = $MSFrameworkPoints->getSaleValue((new \MSFramework\customers())->getUserDataFromSession('user_id'), $currentCart['total_price']['tax']);

                    if($applicableSale[0] && $applicableSale[1]) {
                        $couponClass =  new \MSFramework\Ecommerce\coupon();

                        $coupon = $couponClass->getCustomCoupon('POINTS', $applicableSale[0]);
                        $applicationStatus = (new \MSFramework\Ecommerce\coupon())->applyToCart($coupon, $currentCart);

                        if(!$applicationStatus['status']['error']) {
                            $applicationStatus['points_used'] = $applicableSale[1];
                            $currentCart['coupon']['POINTS'] = $applicationStatus;
                            $currentCart['coupon_price'] -= $applicationStatus['sale_value'];
                        }

                    } else {
                        unset($_SESSION["cart_coupon"]['POINTS']);
                    }

                    return $currentCart;

                });
            }

        }

    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude() {
        return array();
    }

    /**
     * Ottiene le impostazioni del modulo
     *
     * @return array
     */
    public function getModuleSettings()
    {

        $module_settings = array();

        return $module_settings;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     * @param array $params I parametri passati
     * @return string
     */
    public function ajaxCall($params = array())
    {

        $status = array('status' => 0);

        if($params['action'] === 'applyPointsSale') {

            if($_SESSION["cart_coupon"]['POINTS']) return false;

            $MSFrameworkPoints = new \MSFramework\Points\points();
            $currentCart = (new \MSFramework\Ecommerce\cart())->getCart();

            $applicableSale = $MSFrameworkPoints->getSaleValue((new \MSFramework\customers())->getUserDataFromSession('user_id'), $currentCart['total_price']['tax']);

            if($applicableSale[0] && $applicableSale[1]) {
                $couponClass =  new \MSFramework\Ecommerce\coupon();

                $coupon = $couponClass->getCustomCoupon('POINTS', $applicableSale[0]);
                $status['status'] = (new \MSFramework\Ecommerce\coupon())->applyToCart($coupon, $currentCart);

                if(!$status['status']['error']) {
                    $_SESSION["cart_coupon"]['POINTS'] = $status;
                }

            }

        }

        die(json_encode($status));

        return true;
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        return array();
    }

}
<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Ecommerce\Modules;

class conditionalDiscount {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function setHooks()
    {
        Global $MSHooks;

        $_SESSION['conditionalDiscount_QueryResult'] = null;
        $_SESSION['conditionalDiscount_QueryNeedLogin'] = null;

        if($this->modules->getSettingValue('conditionalDiscount', 'active')) {
            $applyDiscount = function ($product_data) {

                if($this->modules->getSettingValue('conditionalDiscount', 'sale_type') == '0') {
                    if(!$product_data['prezzo_promo']) return $product_data;
                } else {
                    if(!$this->modules->getSettingValue('conditionalDiscount', 'percentage')) return $product_data;
                    $prezzoPromo = implode('.', $product_data['prezzo']['tax']);
                    $prezzoPromo = ($prezzoPromo * ($this->modules->getSettingValue('conditionalDiscount', 'percentage') / 100));
                    $product_data['prezzo_promo'] = (new \MSFramework\Ecommerce\imposte())->createTaxPricesArray($prezzoPromo, $product_data['imposta']);

                    if($product_data['range_prezzo'])  {
                        foreach($product_data['range_prezzo'] as $k => $range_price) {
                            $rangePromo = implode('.', $range_price);
                            $rangePromo = $rangePromo = ($rangePromo * ($this->modules->getSettingValue('conditionalDiscount', 'percentage') / 100));
                            $rangePromo = number_format(round($rangePromo, 2), '2', '.', '');
                            $product_data['range_prezzo'][$k] = explode('.', $rangePromo);
                        }
                    }
                }

                try {

                    if($_SESSION['conditionalDiscount_QueryResult'] !== null) {
                        $conditionResult = $_SESSION['conditionalDiscount_QueryResult'];
                    } else {
                        $conditionClass = new \MSFramework\Newsletter\conditions();
                        $conditionClass->ignoreMandatoryCustomers = true;
                        $conditionResult = $conditionClass->getPassingConditionUsers(json_decode($this->modules->getSettingValue('conditionalDiscount', 'conditions'), true));
                        $_SESSION['conditionalDiscount_QueryResult'] = $conditionResult;
                        $_SESSION['conditionalDiscount_QueryNeedLogin'] = $conditionClass->conditionNeedUserLogin;
                    }

                    if (!$conditionResult) {
                        unset($product_data['prezzo_promo']);
                    }

                    if ($_SESSION['conditionalDiscount_QueryNeedLogin']) {
                        $user_data = (new \MSFramework\customers())->getUserDataFromSession();
                        $user_id = 0;

                        if ($user_data) {
                            $user_id = $user_data['user_id'];
                        } else if ($_SESSION['trackinSuite_UserID']) {
                            $user_id = $_SESSION['trackinSuite_UserID'];
                        } else if ($_COOKIE['trackinSuite_UserID']) {
                            $user_id = $_COOKIE['trackinSuite_UserID'];
                        }

                        if (!in_array($user_id, $conditionResult) && !in_array($_SERVER['REMOTE_ADDR'], $conditionResult)) {
                            unset($product_data['prezzo_promo']);
                        }
                    }
                } catch(\Exception $exception) {
                    unset($product_data['prezzo_promo']);
                }

                return $product_data;

            };
            $MSHooks->filter('ecommerce', 'product', 'variation_info')->add($applyDiscount);
            $MSHooks->filter('ecommerce', 'product', 'data')->add($applyDiscount);
        }

    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {


    }

    /**
     * Ottiene le impostazioni del modulo
     *
     * @return array
     */
    public function getModuleSettings()
    {

    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {
        return true;
    }

    /**
     * Funzione che viene richiamata nel footer dei siti common
     *
     * @return string
     */
    public function executeFunctions_footer() {

        return true;
    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        return array(
            "active" => array(
                "name" => "Attiva sconto condizionato",
                "input_type" => "checkbox",
                "helper" => "",
                "mandatory" => false,
                "translations" => false,
                "default" => "0",
                "input_column" => 4
            ),
            "sale_type" => array(
                "name" => "Tipologia di sconto",
                "input_type" => "select",
                "input_values" => array(
                    "0" => "Sconto del prodotto",
                    "1" => "Percentuale fissa",
                ),
                "input_classes" => "",
                "helper" => "Sconto del prodotto: Viene utilizzato il valore dello sconto presente nelle impostazioni del prodotto, se disponibile.<br>Percentuale: Viene applicata una percentuale di sconto a tutti i prodotti presenti sul sito ",
                "mandatory" => false,
                "translations" => false,
                "default" => "sale",
                "input_column" => 4
            ),
            "percentage" => array(
                "name" => "Percentuale di sconto",
                "input_type" => "number",
                "input_attributes" => array("min" => "0", "max" => 100),
                "helper" => "",
                "mandatory" => false,
                "translations" => false,
                "default" => '5',
                "input_column" => 4,
                "check_to_show" => array('sale_type')
            ),
            "conditions" => array(
                "name" => "Condizioni",
                "input_type" => "conditions",
                "input_attributes" => array(),
                "helper" => "Impostando questo valore, verranno visualizzati solo gli ordini effettuati negli 'n' giorni precedenti",
                "mandatory" => false,
                "translations" => false,
                "default" => '[]',
                "input_column" => 12
            )
        );
    }

}
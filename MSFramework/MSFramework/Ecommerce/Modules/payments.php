<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Ecommerce\Modules;

use MSFramework\Ecommerce\orders;

class payments {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkModules;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->modules = $MSFrameworkModules;
    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {

        $files_to_include = array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'modules/ecommerce/payments/js/payments.js')
        );

        return $files_to_include;
    }

    /**
     * Ottiene le impostazioni del modulo
     *
     * @return array
     */
    public function getModuleSettings()
    {

        $module_settings = array(

        );

        return $module_settings;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     *
     * @return string
     */
    public function ajaxCall($params)
    {

        /* LE FUNZIONI IPN */
        if(isset($params["ipn_track_id"])) {
            $paypal = new \MSFramework\Ecommerce\Payments\paypal();
            $paypal->checkIPN($params);
        }

        if(isset($_REQUEST['mac'])) {
            $nexi = new \MSFramework\Ecommerce\Payments\nexi();
            $nexi->checkIPN($_REQUEST);
        }

        /* FUNZIONI PER L'ELABORAZIONE DEL PAGAMENTO */
        if(isset($params['pay_with_stripe'])) {
            $nexi = new \MSFramework\Ecommerce\Payments\stripe();
            $nexi->payOrder($params);
        }
        return '';

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings() {
        return array();
    }

}
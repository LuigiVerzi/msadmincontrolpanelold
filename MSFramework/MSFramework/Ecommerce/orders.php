<?php
/**
 * MSFramework
 * Date: 07/05/18
 */

namespace MSFramework\Ecommerce;


use MSFramework\cms;

class orders {
    public function __construct() {

        Global $MSFrameworkDatabase, $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
    }

    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente allo stato degli ordini
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getStatus($key = "") {
        $ary = array(
            "0" => "In attesa di pagamento",
            "1" => "In Elaborazione",
            "2" => "Spedito",
            "3" => "Consegnato",
            "4" => "Annullato",
            "5" => "Rimborsato",
        );

        if($key !== "") {
            return $ary[$key];
        } else {
            return $ary;
        }
    }

    /**
     * Invia l'ordine al database
     *
     * @param mixed $user_ref L'id o l'email (nel caso dell'acquisto ospite) del cliente che ha piazzato l'ordine
     * @param array $cart l'array del carrello
     * @param array $fatturazione l'array con i dati di fatturazione
     * @param array $spedizione l'array con i dati di spedizione
     * @param int $tipo_spedizione Il tipo di spedizione uisata
     * @param array $coupons_used Tutti i coupons utilizzati per quest'ordine
     * @param int $order_status Lo stato dell'ordine (da 0 a 4)
     * @param string $payment_type Il pagamento utilizzato per quest'ordine
     * @param int $payment_id L'id del pagamento nel caso in cui l'ordine sia già stato pagato con sistemi automatici
     *
     * @return array|mixed
     */
    public function createNewOrder($user_ref, $cart, $fatturazione, $spedizione, $tipo_spedizione, $coupons_used, $order_status, $payment_type = '', $payment_id = 0) {
        Global $MSFrameworkCurrencies;

        if($payment_type == 'contrassegno') {
            $metodi_pagamento = (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamentoWeb();

            if(isset($metodi_pagamento['contrassegno_data']['rincaro']) && $metodi_pagamento['contrassegno_data']['rincaro'] > 0) {
                $valore_rincaro = $metodi_pagamento['contrassegno_data']['rincaro'];

                if($metodi_pagamento['contrassegno_data']['tipo_rincaro'] == '%') {
                    $valore_rincaro = $cart['coupon_price'] * $valore_rincaro / 100;
                }

                $aliquota = $metodi_pagamento['contrassegno_data']['aliquota_rincaro'];

                (new \MSFramework\Ecommerce\cart())->customAddToCart('rincaro_pagamento', 'Contrassegno', $valore_rincaro, 1, $aliquota);
                $cart = (new \MSFramework\Ecommerce\cart())->getCart();
            }
        }

        // Se la valuta attuale non è quella primaria allora riconverto i vari prezzi dell'ordine
        if($MSFrameworkCurrencies->primary_currency_code !== CURRENCY_CODE) {
            $MSFrameworkCurrencies->selected_currency_code = $MSFrameworkCurrencies->primary_currency_code;
            $MSFrameworkCurrencies->primary_currency_code = CURRENCY_CODE;

            $cart['coupon_price'] = $MSFrameworkCurrencies->convertValue($cart['coupon_price']);

            foreach ( $cart['total_price'] as $type => $v) {
                $cart['total_price'][$type] = $MSFrameworkCurrencies->convertValue($v);
            }
            foreach ($cart['products'] as $pk => $pv) {
                foreach ($pv['prezzo'] as $type => $v) {
                    $cart['products'][$pk]['prezzo'][$type] = (new \MSFramework\Ecommerce\products())->formatPrice(implode('.', $v));
                }
            }
        }

        $array_to_save = array(
            "cart" => json_encode($cart),
            "info_fatturazione" => json_encode($fatturazione),
            "info_spedizione" => json_encode($spedizione),
            "tipo_spedizione" => $tipo_spedizione,
            "coupons_used" => (is_array($coupons_used) ? json_encode($coupons_used) : '[]'),
            "order_date" => date("Y-m-d H:i:s"),
            "order_status" => $order_status,
            "payment_type" => $payment_type,
            "payment_id" => $payment_id
        );

        if(is_numeric($user_ref)) {
            $array_to_save['user_id'] = $user_ref;
        }
        else
        {
            $array_to_save['guest_email'] = $user_ref;
        }

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "insert");

        if($this->MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_orders ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0])) {
            return $this->MSFrameworkDatabase->lastInsertId();
        }
        else {
            return false;
        }
    }

    /**
     * Restituisce un array associativo (ID Ordine => Dati ordine) con i dati relativi all'ordine
     *
     * @param string $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     * @param string $fields I campi da prelevare dal DB
     * @param string $only_user Trova solo gli ordini di un determinato utente
     *
     * @return array
     */
    public function getOrderDetails($id = "", $fields = "*", $only_user = false) {
        Global $MSFrameworkCurrencies;

        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }
        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        if($only_user != false) {
            $user_where = "user_id = " . $only_user;
        } else {
            $user_where = "id != ''";
        }

        $ary_to_return = array();
        $category = $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_orders WHERE id != '' AND (" . $same_db_string_ary[0] . ") AND $user_where ORDER BY id DESC", $same_db_string_ary[1]);
        foreach ((array)$category as $r) {
            $ary_to_return[$r['id']] = $r;

            if(isset($r['cart'])) {
                $r['cart'] = json_decode($r['cart'], true);
                $r['cart']['coupon_price'] = $MSFrameworkCurrencies->convertValue($r['cart']['coupon_price']);

                foreach ( $r['cart']['total_price'] as $type => $v) {
                    $r['cart']['total_price'][$type] = $MSFrameworkCurrencies->convertValue($v);
                }

                foreach ($r['cart']['products'] as $pk => $pv) {
                    foreach ($pv['prezzo'] as $type => $v) {
                        $r['cart']['products'][$pk]['prezzo'][$type] = (new \MSFramework\Ecommerce\products())->formatPrice(implode('.', $v));
                    }
                }

                $ary_to_return[$r['id']]['cart'] = $r['cart'];
            }

            if(isset($r['info_fatturazione'])) $ary_to_return[$r['id']]['info_fatturazione'] = json_decode($r['info_fatturazione'], true);
            if(isset($r['info_spedizione'])) {
                $ary_to_return[$r['id']]['info_spedizione'] = json_decode($r['info_spedizione'], true);
                if($ary_to_return[$r['id']]['info_spedizione']['usa_fatturazione'])  $ary_to_return[$r['id']]['info_spedizione'] =  $ary_to_return[$r['id']]['info_fatturazione'];
            }
            if(isset($r['coupons_used'])) $ary_to_return[$r['id']]['coupons_used'] = json_decode($r['coupons_used'], true);
            if(isset($r['tipo_spedizione'])) $ary_to_return[$r['id']]['tipo_spedizione'] = (new \MSFramework\Ecommerce\shipping())->getShippingDetails($r['tipo_spedizione'])[$r['tipo_spedizione']];

            if(!empty($r['courier']) || !empty($r['courier_custom_name'])) {
                $ary_to_return[$r['id']]['courier_info']['name'] = ($r['courier'] != 0 ? (new \MSFramework\Ecommerce\shipping())->getCouriers($r['courier']) : $r['courier_custom_name']);
                $ary_to_return[$r['id']]['courier_info']['tracking'] = $r['tracking_code'];
            } else {
                $ary_to_return[$r['id']]['courier_info'] = array();
            }

        }

        return $ary_to_return;
    }

    /**
     * Aggiorna la quantità dei prodotti disponibili in magazzino a seguito della modifica dello stato di un ordine
     *
     * @param $order_id L'ID dell'ordine variato
     * @param $action L'azione da intraprendere (remove/add)
     *
     * @return bool
     */
    public function updateProductsQtyOnStatusVariation($order_id, $action = "remove") {
        global $firephp;

        $order_details = $this->getOrderDetails($order_id, 'id, cart')[$order_id];
        $order_products = $order_details['cart']['products'];

        foreach($order_products as $product) {
            $product_details = (new \MSFramework\Ecommerce\products())->getProductDetails($product['product_id'], "id, warehouse_qty, variants")[$product['product_id']];

            if(!is_array($product['variations'])) {

                //aggiorno la quantità generale del prodotto
                if($action == "remove") {
                    $upd_qty = $product_details['warehouse_qty']-$product['quantity'];
                    if($upd_qty < 0) {
                        $upd_qty = 0;
                    }
                } else if($action == "add") {
                    $upd_qty = $product_details['warehouse_qty']+$product['quantity'];
                }

                $this->MSFrameworkDatabase->query("UPDATE ecommerce_products SET warehouse_qty = :qty WHERE id = :id", array(":qty" => $upd_qty, ':id' => $product['product_id']));

            } else {

                //aggiorno la quantità delle variazioni
                $compare_ary = array();
                foreach($product['variations'] as $variation) {
                    $compare_ary[$variation['id']] = $variation['value'];
                }

                $variants = json_decode($product_details['variants'], true);
                foreach($variants as $cur_prod_variantK => $cur_prod_variant) {
                    if($cur_prod_variant['type'] == $compare_ary) {
                        if($action == "remove") {
                            $variants[$cur_prod_variantK]['data']['warehouse_qty'] = $cur_prod_variant['data']['warehouse_qty']-$product['quantity'];
                            if($variants[$cur_prod_variantK]['data']['warehouse_qty'] < 0) {
                                $variants[$cur_prod_variantK]['data']['warehouse_qty'] = 0;
                            }
                        } else if($action == "add") {
                            $variants[$cur_prod_variantK]['data']['warehouse_qty'] = $cur_prod_variant['data']['warehouse_qty']+$product['quantity'];
                        }

                        break;
                    }
                }

                $this->MSFrameworkDatabase->query("UPDATE ecommerce_products SET variants = :variants WHERE id = :id", array(":variants" => json_encode($variants), ':id' => $product['product_id']));

            }
        }

        return true;
    }

    /**
     * Aggiorna lo stato dell'ordine
     *
     * @param $order_id L'ID dell'ordine completato
     * @param $status_id L'ID dello stato attuale
     * @param $send_email Se impostato su true allora invia anche l'email di conferma all'utente
     *
     * @return bool
     */
    public function updateOrderStatus($order_id, $status_id, $send_email = true) {

        $this->MSFrameworkDatabase->query("UPDATE ecommerce_orders SET order_status = " . $status_id . " WHERE id = " . $order_id);

        if($send_email) {
            $this->sendOrderStatusEmail($order_id);
        } else if($status_id == 1) {
            Global $MSHooks;
            (new \MSFramework\Fatturazione\vendite())->importFrom('ecommerce', $order_id);
            $orderDetails = $this->getOrderDetails($order_id)[$order_id];
            $MSHooks->action('ecommerce', 'checkout', 'after_Payment')->run(array('order_id' => $order_id, 'user_id' => $orderDetails['user_id'], 'cart_total' => $orderDetails['cart']['coupon_price']));
        }

        return true;
    }

    /**
     * Invia l'email relativa allo stato dell'ordine attuale
     *
     * @param $order_id L'ID dell'ordine completato
     * @param $origin L'origine dell'invio dell'email (0 = FRONT, 1 = BACK)
     *
     * @return bool
     */
    public function sendOrderStatusEmail($order_id, $origin = 0) {

        if(strstr(REAL_SW_PATH, "MSAdminControlPanel")) {
            $ajax_url = $this->MSFrameworkCMS->getURLToSite() . (new \MSFramework\emails())->getACPBridgeShort() . '/' . (new \MSFramework\emails())->getACPBridgeToken() . '/send_order_status_email/' . $order_id;
            return file_get_contents($ajax_url);
        } else {

            $emailClass = new \MSFramework\emails();

            $order_details = $this->getOrderDetails($order_id)[$order_id];
            $status_id = intval($order_details['order_status']);

            if($status_id === 1) {
                (new \MSFramework\Fatturazione\vendite())->importFrom('ecommerce', $order_id);

                Global $MSHooks;
                $MSHooks->action('ecommerce', 'checkout', 'after_Payment')->run(array('order_id' => $order_id, 'user_id' => $order_details['user_id'], 'cart_total' => $order_details['cart']['coupon_price']));
            }

            if($order_details['user_id']) {
                $customer = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($order_details['user_id']);
                $email_address = $customer['email'];
            }
            else {
                $customer = array('nome' => $order_details['info_fatturazione']['nome'], 'cognome' => $order_details['info_fatturazione']['cognome'], 'username' => $order_details['guest_email']);
                $email_address = $order_details['guest_email'];
            }

            $display_name = trim($customer['nome'] . ' ' . $customer['cognome']);
            if(empty($display_name)) $display_name = $customer['email'];
            $customer['display_name'] = $display_name;

            $invoice_settings = $this->MSFrameworkCMS->getCMSData('fatturazione_fatture');
            if(!empty($invoice_settings['when_to_send']) && $invoice_settings['when_to_send'] == $status_id) {
                (new \MSFramework\Ecommerce\invoices())->sendInvoiceToCustomer($order_id);
            }

            /* INVIO L'EMAIL ALL'ADMIN SE IL TEMPLATE PER LO STATO IN QUESTIONE ESISTE E SE LA RICHIESTA PARTE DAL FRONTEND */
            // if($origin == 0 && $emailClass->getTemplates('order-status/admin/' . $status_id, false)) {
            $emailClass->sendMail('order-status/admin/' . $status_id, array(
                'to' => 'admin',
                'order' => $order_details,
                'user' => $customer
            ));
            //    }


            //     return true;


            /* SE LE RECENSIONI SONO ATTIVE USO IL TEMPLATE CON L'INVITO A LASCIARLA */
            if($status_id == 3) {
                $prodotti_settings = $this->MSFrameworkCMS->getCMSData('ecommerce_prodotti');
                if ($prodotti_settings && (int)$prodotti_settings['manage_reviews'] == 1 && !$order_details['guest_email']) {
                    $status_id = '3/review';
                }
            }

            return $emailClass->sendMail('order-status/' . $status_id, array(
                'to' => 'customer',
                'order' => $order_details,
                'email' => $email_address,
                'user' => $customer
            ));
        }
    }

    /**
     * Conta il numero di acquisti avvenuti per un determinato prodotto
     *
     * @param $product_id L'ID del prodotto
     *
     * @return bool
     */
    public function countOrdersForProduct($product_id) {

        $orders = $this->MSFrameworkDatabase->getAssoc("SELECT cart FROM ecommerce_orders WHERE cart LIKE '%{\"product_id\":\"" . $product_id . "\"%'");

        $order_count = 0;

        foreach($orders as $order) {
            $cart = json_decode($order['cart'], true);

            foreach($cart['products'] as $product) {
                if($product['product_id'] == $product_id) {
                    $order_count += $product['quantity'];
                }
            }

        }

        return $order_count;

    }

    /**
     * Conta il numero di ordini totali
     *
     * @return mixed
     */
    public function countTotalOrders($product_id) {
        return $this->MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_orders");
    }

    /**
     * Questa funzione ritorna un URL che permette di vedere l'ordine anche agli ospiti
     *
     * @param $order_id L'ID dell'ordine
     *
     * @return string L'url per vedere l'ordine da ospite
     */
    public function getGuestOrderUrl($order_id) {
        $order_info = $this->MSFrameworkDatabase->getAssoc('SELECT id, order_date, guest_email FROM `ecommerce_orders` WHERE id = ' . $order_id, array(), true);

        if($order_info) {
            return $this->MSFrameworkCMS->getURLToSite() . '?action=ordine-ospite&id=' . $order_info['id'] . '&email=' . $order_info['guest_email'] . '&date=' . $order_info['order_date'];
        }

        return $this->MSFrameworkCMS->getURLToSite();

    }

    /**
     * Questa funzione ritorna un URL che permette di vedere l'ordine anche agli ospiti
     *
     * @param $details array L'array con i dettagli di fatturazione/spedizione
     * @param $type string Il tipo di dati da ritornare [shipping|billing]
     * @param $use_label int Indica se mostrare i label prima dei valori
     *
     * @return string La stringa con i dati formattati
     */
    public function formatCustomerDetails($details, $type = 'shipping', $use_label = 0) {

        $info_to_return = array();

        if($type === 'shipping') {

            if($use_label) $info_to_return[] = '<b>Nome:</b>';
            $info_to_return[] = $details['nome'] . ' ' . $details['cognome'];

            if(!empty($details['ragione_sociale'])) {
                if($use_label) $info_to_return[] = '<b>Ragione Sociale:</b>';
                $info_to_return[] = $details['ragione_sociale'];
            }

            $info_to_return[] = '<br>';

            if($use_label) $info_to_return[] = '<b>Indirizzo:</b>';
            $info_to_return[] = $details['indirizzo'] . ' ' . $details['indirizzo_2'];
            $info_to_return[] = $details['cap'] . ' ' . $details['comune'];
            $info_to_return[] = $details['citta'];
            $info_to_return[] = (is_numeric($details['stato']) ? (new \MSFramework\geonames())->getCountryDetails($details['stato'])[$details['stato']]['name'] : $details['stato']);

            if(!empty($details['telefono_cellulare'])) {
                if($use_label) $info_to_return[] = '<b>Cellulare:</b>';
                $info_to_return[] = $details['telefono_cellulare'];
            }

            if(!empty($details['cellulare'])) {
                if($use_label) $info_to_return[] = '<b>Cellulare:</b>';
                $info_to_return[] = $details['cellulare'];
            }

        } else {

            if($use_label) $info_to_return[] = '<b>Nome:</b>';
            $info_to_return[] = $details['nome'] . ' ' . $details['cognome'];

            if(!empty($details['cf'])) {
                if($use_label) $info_to_return[] = '<b>Codice Fiscale:</b>';
                $info_to_return[] = $details['cf'];
            }

            if(!empty($details['cellulare'])) {
                if($use_label) $info_to_return[] = '<b>Cellulare:</b>';
                $info_to_return[] = $details['cellulare'];
            }

            if(!empty($details['telefono_cellulare'])) {
                if($use_label) $info_to_return[] = '<b>Cellulare:</b>';
                $info_to_return[] = $details['telefono_cellulare'];
            }

            if(!empty($details['telefono_casa'])) {
                if($use_label) $info_to_return[] = '<b>Telefono:</b>';
                $info_to_return[] = $details['telefono_casa'];
            }

            $info_to_return[] = '<br>';

            if(!empty($details['ragione_sociale'])) {
                if($use_label) $info_to_return[] = '<b>Ragione Sociale:</b>';
                $info_to_return[] = $details['ragione_sociale'];
                if($use_label) $info_to_return[] = '<b>P.IVA:</b>';
                $info_to_return[] = $details['piva'];
                if(!empty($details['pec'])) {
                    if($use_label) $info_to_return[] = '<b>Pec:</b>';
                    $info_to_return[] = $details['pec'];
                    if($use_label) $info_to_return[] = '<b>Codice Univoco:</b>';
                    $info_to_return[] = $details['codice_univoco'];
                }

                $info_to_return[] = '<br>';
            }

            if($use_label) $info_to_return[] = '<b>Indirizzo:</b>';
            $info_to_return[] = $details['indirizzo'] . ' ' . $details['indirizzo_2'];
            $info_to_return[] = $details['cap'] . ' ' . $details['comune'];
            $info_to_return[] = $details['citta'];
            $info_to_return[] = (is_numeric($details['stato']) ? (new \MSFramework\geonames())->getCountryDetails($details['stato'])[$details['stato']]['name'] : $details['stato']);

            if(isset($details['richiedi_fattura']) && (int)$details['richiedi_fattura'] === 1) {
                $info_to_return[] = '<br><b>Il cliente ha richiesto fattura.</b>';
            }

        }

        return '<div>' . implode('</div><div>', $info_to_return) . '</div>';

    }

    /* =============== FUNZIONI ACP ============= */

    /**
     * Questa funzione permette di ottenere il grafico dell'ultimo mese
     *
     * @param $start La data YYYY-MM-DD di partenza
     * @param $end La data YYYY-MM-DD di finale
     * @param $order_status Lo stato dell'ordine
     *
     * @return array
     */
    public function getDateRangeChart($start, $end, $order_status = 1) {

        $grafico_ordini = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT cart, date(order_date) as day, (SELECT prezzo FROM ecommerce_shipping_methods WHERE ecommerce_shipping_methods.id = ecommerce_orders.tipo_spedizione) as prezzo_spedizione FROM ecommerce_orders WHERE order_status = $order_status AND order_date >= '$start' AND order_date <= '$end 23:59'") as $r) {

            if(!isset($grafico_ordini[$r['day']])) $grafico_ordini[$r['day']] = array('earn' => 0, 'orders' => 0);

            $cart = json_decode($r['cart'], true);

            $grafico_ordini[$r['day']]['earn'] += $cart['coupon_price'];
            $grafico_ordini[$r['day']]['earn'] += $r['prezzo_spedizione'];
            $grafico_ordini[$r['day']]['orders'] += 1;
        }

        $start =  new \DateTime($start);
        $end =  new \DateTime($end);

        $array_data = array();

        for($i = $start; $i <= $end; $i->modify('+1 day')) {
            if(isset($grafico_ordini[$i->format("Y-m-d")])) $array_data[] = $grafico_ordini[$i->format("Y-m-d")];
            else $array_data[] = array('earn' => 0, 'orders' => 0);
        }

        return $array_data;
    }

}
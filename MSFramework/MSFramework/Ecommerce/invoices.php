<?php
/**
 * MSFramework
 * Date: 17/04/18
 */

namespace MSFramework\Ecommerce;


class invoices {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Invia un email con la fattura allegata al cliente
     *
     * @param int $order_id L'id dell'ordine
     *
     * @return boolean
     */
    public function sendInvoiceToCustomer($order_id, $comment = '') {

        $order_info = (new \MSFramework\Ecommerce\orders())->getOrderDetails($order_id);
        $invoice_settings = (new \MSFramework\cms())->getCMSData('fatturazione_fatture');

        if($order_info) {

            $order_info = $order_info[$order_id];
            $user = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($order_info['user_id']);

            $is_fiscal = $this->checkUserFiscalInfo($user, $order_info['info_fatturazione']);

            $invoice_source = $this->getInvoiceSource($invoice_settings['attachment_format'], $order_id, $comment);

            $status = (new \MSFramework\Ecommerce\emails())->sendMail('order-' . ($is_fiscal ? 'invoice' : 'receipt'), array(
                'nome' => (!empty($user['nome']) ? $user['nome'] . ' ' . $user['cognome'] : $user['email']),
                'email' => $user['email'],
                'order_id' => $order_id,
                'subject' => (new \MSFramework\i18n())->gettext(($is_fiscal ? 'Fattura' : 'Ricevuta') . ' ordine effettuato su ' . SW_NAME),
                'file_name' => (new \MSFramework\i18n())->gettext(($is_fiscal ? 'fattura_ordine_' : 'ricevuta_ordine_') . $order_id),
                'invoice_source' => $invoice_source
            ));

            if($is_fiscal) {
                $invoice_id = $this->getInvoiceNumber($order_id);
                if ($status) {
                    $this->setInvoiceNumber($order_id, $invoice_id);
                }
            }

            return $status;
        }

        return false;

    }

    /**
     * Invia un email con la fattura allegata al cliente
     *
     * @param int $order_id L'id dell'ordine
     *
     * @return integer
     */
    public function getInvoiceNumber($order_id) {

        $order_info = (new \MSFramework\Ecommerce\orders())->getOrderDetails($order_id)[$order_id];
        $invoice_settings = (new \MSFramework\cms())->getCMSData('fatturazione_fatture');

        if(empty($order_info['invoice_id'])) {
            $last_order_date = $this->MSFrameworkDatabase->getAssoc("SELECT order_edit_date FROM `ecommerce_orders` WHERE invoice_id IS NOT NULL ORDER BY invoice_id DESC", array(), true);
            if(date('Y', strtotime($last_order_date['order_edit_date'])) < date('Y')) {
                $invoice_id = 0;
            }
            else
            {
                $invoice_id = (int)$invoice_settings['invoice_current_number'] + 1;
            }
        }
        else {
            return $order_info['invoice_id'];
        }

        return $invoice_id;

    }

    /**
     * Invia un email con la fattura allegata al cliente
     *
     * @param int $order_id L'id dell'ordine
     * @param int $invoice_id L'id della fattura
     *
     * @return integer L'ID corretto della fattura
     */
    public function setInvoiceNumber($order_id, $invoice_id) {

        $invoice_id = $this->checkIDAvailability($order_id, $invoice_id);

        $imposte_settings = (new \MSFramework\cms())->getCMSData('ecommerce_imposte');
        $this->MSFrameworkDatabase->query("UPDATE ecommerce_orders SET invoice_id = $invoice_id WHERE id = $order_id");

        if($invoice_id > $imposte_settings['invoice']['invoice_current_number']) {
            $imposte_settings['invoice']['invoice_current_number'] = $invoice_id;
            $array_to_save = array(
                "value" => json_encode($imposte_settings),
                "type" => "ecommerce_imposte"
            );
            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'update');
            $this->MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'ecommerce_imposte'", $stringForDB[0]);
        }

        return $invoice_id;

    }

    /**
     * Controlla la disponibilità dell'ID e nel caso in cui non la trova ritorna il primo ID disponibile
     *
     * @param int $order_id L'id dell'ordine
     * @param int $invoice_id L'id della fattura
     *
     * @return integer
     */
    public function checkIDAvailability($order_id, $invoice_id) {

        $not_available = $this->MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_orders WHERE id != $order_id AND invoice_id = $invoice_id");

        if($not_available)
        {
            $invoice_id = $this->MSFrameworkDatabase->getAssoc("SELECT MAX(invoice_id + 1) as invoice_id FROM ecommerce_orders WHERE id != $order_id", array(), true)['invoice_id'];
        }

        return $invoice_id;

    }

    /**
     * Controlla se l'utente ha fornito i dati legali per compilare una fattura
     *
     * @param int $user_info Le info dell'utente
     * @param int $info_fatturazione I dati di fatturazione dell'ordine
     *
     * @return integer
     */
    public function checkUserFiscalInfo($user_info, $info_fatturazione) {

        $fatturabile = true;
        if(!isset($info_fatturazione['ragione_sociale']) || empty($info_fatturazione['ragione_sociale'])) {
            $dati_extra = json_decode($user_info['extra']['dati_fatturazione'], true);
            if( isset($dati_extra['ragione_sociale']) && !empty($dati_extra['ragione_sociale']) && (!empty($dati_extra['piva']) || !empty($dati_extra['cf'])) )
            {
                $fatturabile = true;
            }
            else
            {
                $fatturabile = false;
            }
        }

        return $fatturabile;

    }

    /**
     * Invia un email con la fattura allegata al cliente
     *
     * @param string $type Specifica se deve ottenere la fattura in PDF o HTML
     * @param int $order_id L'id dell'ordine
     * @param string $comment Eventuali commenti da allegare
     *
     * @return string
     */
    private function getInvoiceSource($type, $order_id, $comment = '') {

        $invoice_url = (new \MSFramework\cms())->getURLToSite(1) . 'modules/ecommerce/ordini/ajax/getInvoice.php?type=' . $type . '&id=' . $order_id .'&commento=' . urlencode($comment);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $invoice_url);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;

    }


}
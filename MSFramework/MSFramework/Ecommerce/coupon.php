<?php
/**
 * MSFramework
 * Date: 05/05/18
 */

namespace MSFramework\Ecommerce;


class coupon {

    public $customerID = false;

    public function __construct() {
        Global $MSFrameworkDatabase;

        if(!$this->customerID) {
            $this->customerID = (new \MSFramework\Ecommerce\customers())->getUserDataFromSession()['user_id'];
        }

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente alla tipologia di coupon
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getTypes($key = "") {
        $ary = array(
            "0" => "Percentuale sul prodotto",
            "1" => "Percentuale sul carrello",
            "2" => "Sconto fisso sul prodotto",
            "3" => "Sconto fisso sul carrello",
        );

        if($key != "") {
            return $ary[$key];
        } else {
            return $ary;
        }
    }

    /**
     * Restituisce i dati sottoforma di array del coupon indicato
     *
     * @param string $code Il codice coupon
     * @param string $fields I campi da estrarre dal db
     *
     * @return array
     */
    public function getCoupon($code, $fields = '*') {
        $return_array = array();

        $return_array = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_coupon WHERE code = :code LIMIT 1",array(":code" => $code), true);

        // Formattiamo i campi trasformando in Array tutti i JSON
        $json_fields = array('product_limit', 'category_limit', 'product_exclude', 'category_exclude', 'mail_limit');
        if($return_array) {
            foreach($return_array as $k=>$v) {
                if(in_array($k, $json_fields)) {

                    $is_json = (FALSE != json_decode($v));

                    if($is_json) $return_array[$k] = json_decode($v, true);
                    else $return_array[$k] = array();

                }
            }
        }


        return $return_array;
    }

    /**
     * Permette di creare al volo un coupon dinamico
     *
     * @param string $code Il codice coupon
     * @param float $sale_value Il valore dello sconto
     *
     * @return array
     */
    public function getCustomCoupon($code, $sale_value) {
        return array (
            'id' => $code,
            'code' => $code,
            'type' => '3',
            'value' => $sale_value,
            'expiration_date' => '',
            'free_shipping' => '0',
            'product_limit' => array ( ),
            'category_limit' => array ( ),
            'product_exclude' => array ( ),
            'category_exclude' => array ( ),
            'min_expense' => '',
            'individual_use' => '0',
            'exclude_promo_items' => '0',
            'mail_limit' => array ( 0 => ''),
            'coupon_limit_usage' => '',
            'user_limit_usage' => ''
        );
    }

    /**
     * Restituisce i dati sottoforma di array del coupon indicato
     *
     * @param array $coupon L'array con il coupon selezionato
     * @param array $cart L'array del carrello in cui applicare il coupon
     *
     * @return array
     */
    public function applyToCart($coupon, $cart) {
        Global $MSFrameworki18n;

        if(!$coupon || !$cart) return array();
        else
        {


            if(in_array($coupon['code'], array_keys($cart['coupon']))) {
                $coupon_result['error'] = $MSFrameworki18n->gettext('Non puoi applicare più volte lo stesso coupon.');
                return $coupon_result;
            }

            $user = (new \MSFramework\Ecommerce\customers())->getCustomerDataFromDB($this->customerID);

            $used_times = $this->MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_orders WHERE coupons_used LIKE '%" . $coupon['code'] . "%'");
            if($user) {
                $user_used_times = $this->MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_orders WHERE coupons_used LIKE '%" . $coupon['code'] . "%' AND user_id = " . $user['id']);
            }
            else
            {
                $user_used_times = 0;
            }

            $coupon_result = array('coupon_id' => (string)$coupon['id'], 'error' => false, 'applied_to' => array(), 'not_applied_to' => array(), 'free_shipping' => (int)$coupon['free_shipping'], 'individual_use' => (int)$coupon['individual_use'], 'sale_value' => 0.0, 'total_cart' => 0.0);

            // Controlla se è scaduto
            if(!empty($coupon['expiration_date']) && $coupon['expiration_date'] <= time()) {
                $coupon_result['error'] = (new \MSFramework\i18n())->gettext('Il coupon inserito è scaduto');
                return $coupon_result;
            }

            // Controlla se ha superato il limite di utilizzo
            if( (!empty( $coupon['coupon_limit_usage']) && $used_times >= $coupon['coupon_limit_usage']) || (!empty($coupon['user_limit_usage']) && $user_used_times >= $coupon['user_limit_usage']) ) {
                $coupon_result['error'] = (new \MSFramework\i18n())->gettext('Questo coupon ha superato il limite di utilizzi e non è più attivo');
                return $coupon_result;
            }

            // Controlla se l'utilizzo di questo coupon o di un eventuale altro attivo è individuale
            if( count($cart['coupon']) && ($coupon['individual_use'] == 1 || reset($cart['coupon'])['individual_use'] == 1) ) {
                $coupon_result['error'] = (new \MSFramework\i18n())->gettext('Questo coupon può essere usato solo singolarmente, elimina l\'altro coupon attivo prima');
                return $coupon_result;
            }

            // Divide i prodotti inclusi ed esclusi
            foreach($cart['products'] as $product)
            {
                if($this->isApplicableToProduct($coupon, $product)) {
                    $coupon_result['applied_to'][] = $product;
                }
                else
                {
                    $coupon_result['not_applied_to'][] = $product;
                }
            }

            foreach ($coupon_result['applied_to'] as $product) {
                $tax_price = (new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['currentVariationInfo']['prezzo']['tax']);
                $tax_price = $tax_price * (int)$product['quantity'];

                $coupon_result['total_cart'] += $tax_price;
            }

            // Se il coupon è applicato ai singoli prodotti
            if($coupon['type'] == 0 || $coupon['type'] == 2) {
                foreach ($coupon_result['applied_to'] as $product)
                {
                    $tax_price = (new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['currentVariationInfo']['prezzo']['tax']);
                    $tax_price = $tax_price * (int)$product['quantity'];

                    // CALCOLO GLI SCONTI SUI PRODOTTI FISSI
                    if ($coupon['type'] == 0) $coupon_result['sale_value'] += ($tax_price * floatval($coupon['value'])) / 100;
                    else if ($coupon['type'] == 2) $coupon_result['sale_value'] += floatval($coupon['value']) * intval($product['quantity']);
                }
                $total_to_check = floatval($coupon_result['total_cart']);
            }
            // Se il coupon è applicato al carrello intero
            else {
                if ($coupon['type'] == 1) $coupon_result['sale_value'] += ($coupon_result['total_cart'] * floatval($coupon['value'])) / 100;
                else if ($coupon['type'] == 3) $coupon_result['sale_value'] = floatval($coupon['value']);

                $total_to_check = floatval($cart['total_price']['tax'][0] . '.' . $cart['total_price']['tax'][1]);

            }

            if($total_to_check < floatval($coupon['min_expense'])) {
                $coupon_result['error'] = (new \MSFramework\i18n())->gettext('Il coupon non può essere applicato perchè i prodotti abilitati non superano la spesa minima di') . ' ' . $coupon['min_expense'] . CURRENCY_SYMBOL;
            }

            if(count(array_filter(($coupon['mail_limit']))) != 0) {
                if(!$user) {
                    $coupon_result['error'] = (new \MSFramework\i18n())->gettext('Devi essere loggato per .') . '<br>' . 'Spesa minima: <b>' . $coupon['min_expense'] . CURRENCY_SYMBOL . '</b>';
                }
                else {
                    if(!in_array($user['email'], $coupon['mail_limit']))  $coupon_result['error'] = (new \MSFramework\i18n())->gettext('Il coupon inserito è riservato ad un gruppo di utenti');
                }
            }

           // \ChromePhp::log($coupon_result);

            return $coupon_result;
        }

    }

    /**
     * Controlla se il coupon è applicabile o no al prodotto
     *
     * @param array $coupon L'array con il coupon selezionato
     * @param array $cart L'array del carrello in cui applicare il coupon
     *
     * @return boolean
     */
    public function isApplicableToProduct($coupon, $product) {

        $possible_application = true;

        if(!$coupon || !$product) return false;
        else {

            if ($coupon['product_limit']) {
                if (!in_array($product['product_id'], $coupon['product_limit'])) $possible_application = false;
            }
            if ($coupon['product_exclude']) {
                if (in_array($product['product_id'], $coupon['product_exclude'])) $possible_application = false;
            }
            if ($coupon['exclude_promo_items'] == 1) {
                if ($product['currentVariationInfo']['prezzo_promo']['no_tax']) $possible_application = false;
            }
            if ($coupon['category_limit']) {
                if (!in_array($product['category'], $coupon['category_limit'])) $possible_application = false;
            }
            if ($coupon['category_exclude']) {
                if (in_array($product['category'], $coupon['category_exclude'])) $possible_application = false;
            }

            return $possible_application;
        }
    }

    /**
     * Restituisce un l'elenco di tutti gli ordini in cui è stato utilizzato un determinato coupon
     *
     * @param string $id L'ID del coupon (stringa) o dei coupon (array) richiesti
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getOrdersWithCoupon($id, $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "coupons_used", "LIKE", "both", "", '"coupon_id":"', '"');
        } else {
            return array();
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_orders WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

}
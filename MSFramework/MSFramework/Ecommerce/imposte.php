<?php
/**
 * MSFramework
 * Date: 01/05/18
 */

namespace MSFramework\Ecommerce;

class imposte {

    public function __construct() {
        global $firephp;
        $this->fatturazioneImposte = new \MSFramework\Fatturazione\imposte();
    }

    public function getPriceType() {
        return $this->fatturazioneImposte->getPriceType();
    }

    public function getImposte() {
        return $this->fatturazioneImposte->getImposte();
    }

    public function getSuffix() {
        return $this->fatturazioneImposte->getSuffix();
    }

    public function getSettings() {
        return $this->fatturazioneImposte->getSettings();
    }

    public function createTaxPricesArray($originale, $imposta = 1) {

        $prices = $this->fatturazioneImposte->getPriceToShow($originale, $imposta);

        foreach($prices as $k => $price) {
            $prices[$k] = (new \MSFramework\Ecommerce\products())->formatPrice($price);
        }

        return $prices;
    }
}
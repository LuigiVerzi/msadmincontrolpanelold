<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Ecommerce;


class attributes {
    public function __construct() {
        global $firephp;

        $this->MSFrameworkEcommerceProducts = new \MSFramework\Ecommerce\products();
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Attributo => Dati Attributo) di tutti gli attributi usati in una determinata lista di prodotti.
     *
     * @param $list array L'array della lista di prodotti generata
     *
     * @return array
     */
    public function getAttributesIntoList($list) {

        // categoria, search, filters, price_range, custom_db

        /* INIZIO RETROCOMPATIBILITA' */
        $args = func_get_args();
        if(count($args) > 1) {
            $range = $args[0];
            $params = $this->MSFrameworkEcommerceProducts->prepareProductListParams(
                array(
                    'category' => $args[0],
                    'filters' => $args[2],
                    'query' => $args[1],
                    'price_range' => $args[3],
                    'custom_db_query' => $args[4]
                )
            );
        }
        /* FINE RETROCOMPATIBILITA' */

        if(!isset($list['params']['sql'])) {
            return array();
        }

        $sql = $list['params']['sql'];
        $filters = $list['params']['filters'];

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT ecommerce_attributes.* FROM `ecommerce_attributes` WHERE ecommerce_attributes.id IS NOT NULL AND ecommerce_attributes.is_filter = 1  AND (SELECT GROUP_CONCAT(attributes) FROM ($sql) asd) LIKE CONCAT('%attr_', ecommerce_attributes.id, '%') GROUP BY ecommerce_attributes.id ") as $r) {

            /* Se l'attributo è attivo non necessita di essere mostrato, quindi evito di appesantire la funzione */
            if(!$filters || !isset($filters[$r['id']]))
            {

                $ary_to_return[$r['id']] = $r;
                $ary_to_return[$r['id']]['values'] = $this->getValuesOfAttributeDetails($r['id']);

                /* Vado ad ottenere il numero di eventuali prodotti per ogni attributo */
                $products_into_attribute = 0;
                foreach ($ary_to_return[$r['id']]['values'] as $k => $attribute_value) {
                    $current_attr = ($filters ? $filters : array());
                    $current_attr[$r['id']] = $attribute_value['id'];

                    $tmp_params = $list['params'];
                    $tmp_params['filters'] = $current_attr;
                    $products_list = $this->MSFrameworkEcommerceProducts->prepareProductListParams($tmp_params);

                    $ary_to_return[$r['id']]['values'][$k]['count'] = $this->MSFrameworkEcommerceProducts->countProducts($products_list);

                    $products_into_attribute += $ary_to_return[$r['id']]['values'][$k]['count'];
                    // Se il valore dell'attributo non contiene prodotti non lo mostro
                    if($ary_to_return[$r['id']]['values'][$k]['count'] == 0) unset($ary_to_return[$r['id']]['values'][$k]);
                }
                // Se l'attributo non contiene neanche un prodotto allora non lo mostro
                if($products_into_attribute == 0) unset($ary_to_return[$r['id']]);
            }

        }

        return $ary_to_return;
    }

    /**
     * Formatta l'array degli attributi ritornando gli attributi divisi dalle variazioni
     *
     * @param mixed $attributes L'array o la stringa json degli attributi
     * @param bool $extended Se positivo ritorna anche le eventuali info extra (codici hex, immagini)
     *
     * @return array Ritorna un (array) utilizzabile facilmente nella scheda prodotto
     */
    public function formatAttributesForProduct($attributes, $extended = false) {

        if(!is_array($attributes)) $attributes = json_decode($attributes, true);
        if(!$attributes) return array();

        $attributes = $this->cleanAttributesAry($attributes);

        $clean_attributes = array('attribute' => array(), 'variations' => array());
        foreach($attributes as $values) {

            $id = (int)$values[0];

            $attribute_detail = $this->getAttributeDetails($id)[$id];
            $attribute_type = ($attribute_detail['is_variant'] ? 'variations' : 'attribute');

            if($attribute_detail['show_in_product'] == 1 || $attribute_type == 'variations') {

                $clean_attributes[$attribute_type][$id] = array(
                    'nome' => $attribute_detail['nome'],
                    'show_mode' => $attribute_detail["show_mode"],
                    'is_multiple' => $attribute_detail["is_multiple"],
                    'value' => array(),
                );

                if ($this->isCustomAttr($id, $values[1])) {
                    $value_detail = $this->getValuesOfAttributeDetails($id, $values[1])[$values[1]];
                    if($extended)
                    {
                        $clean_attributes[$attribute_type][$id]['value'] = array('nome' => $value_detail['nome']);
                    }
                    else
                    {
                        $clean_attributes[$attribute_type][$id]['value'] = $value_detail['nome'];
                    }

                } else {
                    foreach ($values[1] as $value_id) {
                        $value_detail = $this->getValuesOfAttributeDetails($id, $value_id)[$value_id];
                        if($extended) {
                            if ($attribute_type == 'attribute' && !(int)$attribute_detail['is_multiple']) $clean_attributes[$attribute_type][$id]['value'] = $value_detail;
                            else $clean_attributes[$attribute_type][$id]['value'][$value_id] = $value_detail;
                        }
                        else
                        {
                            if ($attribute_type == 'attribute' && !(int)$attribute_detail['is_multiple']) $clean_attributes[$attribute_type][$id]['value'] = $value_detail['nome'];
                            else $clean_attributes[$attribute_type][$id]['value'][$value_id] = $value_detail['nome'];
                        }
                    }
                }

            }
        }

        return $clean_attributes;
    }

    /**
     * Restituisce un array associativo (ID Attributo => Dati Attributo) con i dati relativi all'attributo
     *
     * @param string $id L'ID dell'attributo (stringa) o degli attributi (array) richiesti (se vuoto, vengono recuperati i dati di tutti gli attributi)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getAttributeDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($this->jsonPrefix('attribute', 'remove', $id));
            } else {
                $id = $this->jsonPrefix('attribute', 'remove', $id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_attributes WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Attributo => Dati Attributo) con i dati relativi all'attributo
     *
     * @param string $type La tipologia di attributo (foto, brand, colori)
     *
     * @return array
     */
    public function getAttributeIDsByType($type = "") {
        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT id FROM ecommerce_attributes WHERE show_mode = :show_mode", array(':show_mode' => $type)) as $r) {
            $ary_to_return[] = $r['id'];
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Valore Attributo => Dati Valore Attributo) con i dati relativi all'valore dell'attributo
     *
     * @param string $parent L'ID dell'attributo di appartenenza del valore
     * @param string $id L'ID del valore dell'attributo (stringa) o dei valori degli attributi (array) richiesti (se vuoto, vengono recuperati i dati di tutti i valori degli attributi)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getValuesOfAttributeDetails($parent = "", $id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($this->jsonPrefix('attribute_value', 'remove', $id));
            } else {
                $id = $this->jsonPrefix('attribute_value', 'remove', $id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        $append_where = "";
        $append_where_ary = array();
        if($parent != "") {
            $append_where .= " AND child_of = :child_of ";
            $append_where_ary[':child_of'] = $this->jsonPrefix('attribute', 'remove', $parent);
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_attributes_values WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ")", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Verifica se un attributo è stato definito anche come "variazione"
     *
     * @param $id L'ID dell'attributo
     *
     * @return bool
     */
    public function checkIfIsVariant($id) {
        $r = $this->MSFrameworkDatabase->getAssoc("SELECT is_variant FROM ecommerce_attributes WHERE id = :id", array(":id" => $id), true);
        if($r['is_variant'] == "1") {
            return true;
        }

        return false;
    }

    /**
     * Verifica se un attributo accetta valori multipli
     *
     * @param $id L'ID dell'attributo
     *
     * @return bool
     */
    public function checkIfIsMultiple($id) {
        $r = $this->MSFrameworkDatabase->getAssoc("SELECT is_multiple, is_variant FROM ecommerce_attributes WHERE id = :id", array(":id" => $id), true);
        if($r['is_multiple'] == "1" || $r['is_variant'] == "1") {
            return true;
        }

        return false;
    }

    /**
     * Restituisce un array di "n" chiavi in cui ogni chiave è una possibile combinazione di varianti. Ogni chiave contiene al suo interno un ulteriore array nel formato [id attributo => valore attributo]
     *
     * @param $attributes array L'array degli attributi selezionati per il prodotto (colonna 'attributes' del DB)
     * @param $prefix string Parametro ad uso interno: NON MODIFICARE!
     *
     * @return array
     */
    public function getVariationsCombinationsFromAttributes($attributes, $prefix = "") {
        $attributes = $this->cleanAttributesAry($attributes);

        $result = array(array());

        $attributes = $this->getVariationsFromAttributes($attributes);

        foreach ($attributes as $attribute_id => $attribute_values) {
            $tmp = array();

            foreach ($result as $result_item) {
                foreach ($attribute_values as $attribute_value) {
                    $tmp[] = $result_item + array((string)$attribute_id => $attribute_value);
                }
            }

            $result = $tmp;
        }

        return $result;
    }

    /**
     * Restituisce un array di "n" chiavi in cui ogni chiave è l'ID dell'attributo disponibile per le variazioni.
     *
     * @param $attributes L'array degli attributi selezionati per il prodotto (colonna 'attributes' del DB)
     *
     * @return array
     */
    public function getVariationsFromAttributes($attributes) {
        $attributes = $this->cleanAttributesAry($attributes);

        foreach($attributes as $gk => $gv) {
            if($this->checkIfIsVariant($gv[0])) {
                $groups[$gv[0]] = $gv[1];
            }
        }

        return $groups;
    }

    /**
     * Ottiene, aggiunge o rimuove il prefisso di un attributo o valore di attributo
     *
     * @param $what: Il tipo di prefisso che si desidera ottenere. Accetta solo i valori "attribute" e "attribute_value"
     * @param string $operation Il tipo di operazione da compiere. Accetta "get" (ottiene il valore del prefisso), "add" (aggiunge il prefisso a $value), "remove" (rimuove il prefisso da $value)
     * @param string $value Il valore (o l'array di valori) ai quali aggiungere/rimuovere il prefisso
     *
     * @return array|bool|mixed|string
     */
    public function jsonPrefix($what, $operation = "get", $value = "") {
        if($what == "") {
            return false;
        }

        if($operation == "add") {
            if(!is_array($value)) {
                return $this->jsonPrefix($what) . $value;
            } else {
                array_walk($value, function(&$v, $k, &$what) { $v = $this->jsonPrefix($what) . $v; }, $what);
                return $value;
            }
        } else if($operation == "remove") {

            if(!is_array($value)) {
                return str_replace($this->jsonPrefix($what), "", $value);
            } else {
                array_walk($value, function(&$v, $k, &$what) { $v = str_replace($this->jsonPrefix($what), "", $v); }, $what);
                return $value;
            }
        } else if($operation == "get") {
            if($what == "attribute") {
                return "attr_";
            } else if($what == "attribute_value") {
                return "attrval_";
            }
        }
    }

    /**
     * Rimuove tutti i prefissi dall'array degli attributi del prodotto
     *
     * @param $attributes L'array degli attributi selezionati per il prodotto (colonna 'attributes' del DB)
     */
    public function cleanAttributesAry($attributes) {
        $new_atributes = array();

        foreach($attributes as $k => $v) {
            $new_atributes[] = array(
                $this->jsonPrefix('attribute', 'remove', $v[0]),
                $this->jsonPrefix('attribute_value', 'remove', $v[1])
            );
        }

        return $new_atributes;
    }

    /**
     * Verifica se il valore di un attributo è stato impostato manualmente dall'utente (e quindi non fa parte di quelli preimpostati nel modulo "Attributi")
     *
     * @param $attrID L'ID dell'attributo
     * @param $attrValue Il valore da analizzare
     *
     * @return bool
     */
    public function isCustomAttr($attrID, $attrValue) {
        if ($attrValue != "" && !is_array($attrValue) && !in_array($attrValue, $this->getValuesOfAttributeDetails($attrID)) && !strstr($attrValue, $this->jsonPrefix('attribute_value', 'get'))) {
            return true;
        }

        return false;
    }
}
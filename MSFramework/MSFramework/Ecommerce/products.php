<?php
/**
 * MSFramework
 * Date: 17/04/18
 */

namespace MSFramework\Ecommerce;

class products {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array con tutti i prodotti in Vetrina
     *
     * @param string $fields I campi da prelevare dal DB
     * @param int $limit Il numero di righe da mostrare
     *
     * @return array
     */
    public function getFeaturedProducts($fields = "*", $limit = 8) {

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_products WHERE category != '' AND is_highlighted = 1 LIMIT $limit") as $r) {
            $ary_to_return[$r['id']] = $this->formatProductArray($r);
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo contenente tutti i prodotti filtrati
     *
     * @param array $params Il parametro della lista da generare
     *
     *
     * @return array
     */
    public function getProductsList($params) {
        /* INIZIO RETROCOMPATIBILITA' */
        $args = func_get_args();
        if(count($args) > 1) {
            $params = $this->prepareProductListParams(
                array(
                    'category' => $args[0],
                    'page' => $args[1],
                    'filters' => $args[2],
                    'query' => $args[3],
                    'price_range' => $args[4],
                    'order' => $args[5],
                    'limit' => $args[6],
                    'custom_db_query' => $args[7]
                )
            );
        }

        /* FINE RETROCOMPATIBILITA' */

        if(is_array($params) && !isset($params['params'])) {
            $params = $this->prepareProductListParams($params);
        }

        $ary_to_return = $params;

        $ary_to_return['total_products'] = $this->countProducts($ary_to_return);

        $ary_to_return['products'] = array();

        foreach($this->MSFrameworkDatabase->getAssoc($params['params']['full_sql'])  as $r) {
            $ary_to_return['products'][$r['id']] = $this->formatProductArray($r);
        }

        return $ary_to_return;
    }

    public function prepareProductListParams($params) {

        $default_params = array(
            'category' => 0,
            'page' => 1,
            'filters' => array(),
            'query' => '',
            'price_range' => array(),
            'order' => array('date', 'DESC'),
            'limit' => 12,
            'custom_db_query' => array()
        );

        foreach($default_params as $key=>$def_val){
            if(!isset($params[$key])){
                $params[$key] = $def_val;
            }
        }
        /* CREA LA QUERY CATEGORY */
        if($params['category'] == 0) $category_where = "id != ''";
        else
        {
            $category_where = "find_in_set(" . $params['category'] . ", category)";

            /* OTTIENE LE CATEGORIE FIGLIE */
            $childrens_cats = (new \MSFramework\Ecommerce\categories())->getCategoryChildrens(array(
                    'params' => $params
                )
            );

            /* TROVA ANCHE I PRODOTTI NELLE SOTTOCATEGORIE */
            if($childrens_cats) {
                foreach($childrens_cats as $child_cat) {
                    $category_where .= ' OR find_in_set(' . $child_cat['id'] . ', category)';
                }
                $category_where = '(' . $category_where . ')';
            }

            $_SESSION['last_category_visited'] = $params['category'];
        }

        /* CREA LA QUERY PRICE RANGE */
        if(!$params['price_range']) $price_where = "prezzo >= 0";
        else
        {
            $price_where = 'prezzo >= ' . intval($params['price_range'][0]);
            if($params['price_range'][1]) $price_where .= ' AND prezzo <= ' . intval($params['price_range'][1]);
        }

        /* CREA LA QUERY PERSONALIZZATA */
        $custom_where = '';
        if($params['custom_db_query'])
        {
            foreach($params['custom_db_query'] as $db_q) {
                if (is_array($db_q) && is_array($db_q[0])) {
                    $or_custom_where = array();
                    foreach($db_q as $or_db_q) {
                        $or_custom_where[] = $or_db_q[0] . ' ' . $or_db_q[1] . ' ' . (is_numeric($or_db_q[2]) ? $or_db_q[2] : "'" . $or_db_q[2] . "'");
                    }
                    $custom_where .= ' AND (' . implode(' OR ', $or_custom_where) . ')';
                }
                else
                {
                    $custom_where .= ' AND ' . $db_q[0] . ' ' . $db_q[1] . ' ' . (is_numeric($db_q[2]) ? $db_q[2] : "'" . $db_q[2] . "'");
                }
            }
        }

        /* CREA LA QUERY PER I FILTRI */
        $filters_to_search = array();
        if(count($params['filters']) > 0) {
            foreach($params['filters'] as $id => $value) {
                $filters_to_search[] = '"' . (new \MSFramework\Ecommerce\attributes())->jsonPrefix('attribute', 'add', $id) . '[^]]+' .  (new \MSFramework\Ecommerce\attributes())->jsonPrefix('attribute_value', 'add', $value) . '"';
            }
            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($filters_to_search, "AND", "attributes", "REGEXP", "both", "");
        }
        else
        {
            $same_db_string_ary = array("id != '' ", array());
        }

        if(!empty($params['query'])) {
            $search_db_string_ary =  $this->MSFrameworkDatabase->composeSameDBFieldString(array($params['query']), "AND", "nome", "LIKE", "both");

            $same_db_string_ary[0] .= ' AND' . $search_db_string_ary[0];

            $key = array_keys($search_db_string_ary[1])[0];
            $value = array_values($search_db_string_ary[1])[0];

            $same_db_string_ary[1][$key] = $value;
        }

        /* IMPOSTA L'OFFSET IN BASE ALLA PAGINA DA MOSTRARE */
        $start = ($params['page'] * $params['limit']) - $params['limit'];
        if($start < 0) $start = 0;

        /* CREA LA QUERY PER ORDINARE CORRETTAMNETE L'ARRAY */
        switch($params['order'][0]) {
            case 'date':
                $order_key = 'id';
                break;
            case 'price':
                $order_key = 'prezzo * 1';
                break;
            case 'stock':
                $order_key = 'warehouse_qty';
                break;
            case 'popularity':
                $order_key = "(SELECT COUNT(*) FROM ecommerce_orders WHERE cart LIKE CONCAT('%product_id\":\"', id, '\"%'))";
                break;
            case 'manual':
                $order_key = "sort";
                break;
            case 'custom':
                $order_key = $params['order'][2];
                break;
            default:
                $order_key = ($params['order'][0] == '' || $params['order'][0] == 'default' ? 'id' : $params['order'][0]);
        }
        if($params['order'][1] == 'ASC') $order_dir = 'ASC';
        else $order_dir = 'DESC';

        $order_sql =  $order_key . ' ' . $order_dir;
        if($order_key !== 'id' && !stristr($order_sql, ',')) {
            $order_sql .= ', id ASC';
        }

        $sql = $this->MSFrameworkDatabase->getReplacedQuery("SELECT * FROM ecommerce_products WHERE is_active = 1 AND $category_where AND $price_where AND (" . $same_db_string_ary[0] . ") $custom_where", $same_db_string_ary[1]);

        $params['sql'] = $sql;
        $params['full_sql'] =  "$sql ORDER BY $order_sql LIMIT $start, " . $params['limit'];

        $ary_to_return = array(
            /* USATI PER LA RETROCOMPATIBILITA */
            'page' => $params['page'],
            'order' => $params['order'],
            'showing_per_page' => $params['limit'],
            /* FINE RETROCOMPATIBILITA */
            'products' => array(),
            'params' => $params
        );

        return $ary_to_return;

    }

    /**
     * Restituisce il numero di prodotti trovati
     *
     * @param mixed $list_or_category L'ID della categoria o l'array generato tramite la funzione getProductsList
     *
     * @return mixed
     */
    public function countProducts($list_or_category) {


        if(is_numeric($list_or_category)) {

            $params = $this->prepareProductListParams(
                array(
                    'category' => $list_or_category
                )
            );

            $sql = $params['params']['sql'];

        }
        else {
            $sql = $list_or_category['params']['sql'];
        }

        return $this->MSFrameworkDatabase->getCount($sql);

    }

    /**
     * Restituisce il range di prezzo all'interno di una determinata ricerca
     *
     * @param array $params Il parametro generato dalla lista dei prodotti
     * @param array $range Il range da controllare
     *
     * @return mixed
     */
    public function getProductsPriceRange($params, $range) {

        /* INIZIO RETROCOMPATIBILITA' */
        $args = func_get_args();
        if(count($args) > 2) {
            $range = $args[0];
            $params = $this->prepareProductListParams(
                array(
                    'category' => $args[2],
                    'filters' => $args[4],
                    'query' => $args[3],
                    'price_range' => $args[1]
                )
            );
        }
        /* FINE RETROCOMPATIBILITA' */

        $sql_range = '';
        for ($i = 0; $i <= $range[0]; $i+=$range[1]) {
            $sql_range .= "WHEN prezzo < " . ($i+$range[1]) . " THEN '" . $i. "-" . ($i+$range[1]) . "' ";
        }

        $price_ranges = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT COUNT(*) as count, (CASE " . $sql_range . "ELSE '" . ($range[0] + $range[1]) . "-' END) as raggio FROM (" . $params['params']['sql'] . ") asd GROUP BY raggio") as $r) {

            if($params['params']['price_range']) {
                if(implode('-', $params['params']['price_range']) == $r['raggio']) continue;
            }

            $price_ranges[] = $r;
        }

        return $price_ranges;

    }

    /**
     * Restituisce un array associativo (ID Prodotto => Dati Prodotto) con i dati relativi al prodotto
     *
     * @param string $id L'ID del prodotto (stringa) o dei prodotti (array) richiesti (se vuoto, vengono recuperati i dati di tutti i prodotti)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getProductDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        } else {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_products WHERE is_active = 1 AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $this->formatProductArray($r);
        }

        return $ary_to_return;
    }

    /**
     * Ottiene il range di prezzo delle varie variazioni
     *
     * @param array $variations L'array delle variazioni del prodotto
     * @param array $imposta Inserisce l'ìmposta del prodotto attuale
     *
     * @return array
     */
    private function getVariationPriceRange($variations, $imposta) {
        $variations = json_decode($variations, true);

        $tax_included =  ((new \MSFramework\Ecommerce\imposte())->getPriceType() == '0' ? true : false);
        $tax_settings =  (new \MSFramework\Ecommerce\imposte())->getSettings();

        $price_range = array(0, 0);

        if($variations) {

            foreach($variations as $v) {

                $current_price = 0;


                if(!empty($v['data']['prezzo'])) $current_price = $v['data']['prezzo'];
                if(!empty($v['data']['prezzo_promo'])) $current_price = $v['data']['prezzo_promo'];

                if(!$tax_included && $tax_settings['inclusa_prodotti'] == 1) {
                    $current_price = $current_price + ($current_price * ($imposta/100));
                }

                $current_price = round($current_price, 2);

                if($current_price > 0) {
                    if ($current_price > $price_range[1] || $price_range[0] == 0) $price_range[1] = $current_price;
                    if ($current_price < $price_range[0] || $price_range[0] == 0) $price_range[0] = $current_price;
                }

            }

        }

        // SE NON TROVO VARIAZIONI DI PREZZO RITORNO UN ARRAY VUOTO
        if($price_range[0] == 0) {
            $price_range = array();
        }
        else
        {
            // SE IL PREZZO NON VARIA ALLORA RITORNO UN ARRAY VUOTO
            if($price_range[0] != $price_range[1]) $price_range = array($this->formatPrice($price_range[0]), $this->formatPrice($price_range[1]));
            else $price_range = array();
        }

        return $price_range;
    }

    /**
     * Ottiene i dettagli del magazzino per il prodotto selezionato
     *
     * @param $product_id L'ID del prodotto (se si intende controllare la quantità PRINCIPALE del prodotto). Se si intende controllare la quantità di una particolare variazione è necessario passare un array. Es: array("product_id" => <id del prodotto>, "variation" => array("id" => <id variazione>, "value" => <valore variazione>))
     *
     * @return string
     */
    public function getWarehouseStatus($product_id) {
        if(is_array($product_id) && $product_id['product_id'] != "") {
            $variations_ary = $product_id['variation'];
            $product_id = $product_id['product_id'];

            $product_detail = $this->getProductVariationInfo($product_id, $variations_ary);
        } else {
            $product_detail = $this->getProductDetails($product_id, "manage_warehouse, warehouse_qty")[$product_id];
        }

        if($product_detail['manage_warehouse'] != 1) {
            return "unlimited";
        } else {
            return $product_detail['warehouse_qty'];
        }
    }

    /**
     * Aggiorna la quantità in magazzino (se abilitato per il prodotto)
     *
     * @param $product_id L'ID del prodotto (se si intende modificare la quantità PRINCIPALE del prodotto).  Se si intende modificare la quantità di una particolare variazione è necessario passare un array. Es: array("product_id" => <id del prodotto>, "variation" => array("id" => <id variazione>, "value" => <valore variazione>))
     * @param $qty Il valore della quantità (in base a $action)
     * @param string $action Definisce l'azione da compiere sulla quantità in magazzino. "subtract" sottrae $qty al valore corrente del magazzino, "sum" somma $qty al valore corrente del magazzino. Qualunque altra impostazione setterà il valore del magazzino a $qty. In nessun caso il valore del magazzino sarà < 0
     */
    public function updateWarehouseQty($product_id, $qty, $action = "") {
        if(is_array($product_id) && $product_id['product_id'] != "") {
            $variations_ary = $product_id['variation'];
            $product_id = $product_id['product_id'];

            $variations_data = $this->getProductDetails($product_id, "variants")[$product_id];
            $variants = json_decode($variations_data['variants'], true);
            $variations_to_search = array();

            foreach ($variations_ary as $single_input) {
                $variations_to_search[] = $single_input['value'];
            }

            foreach ($variants as $product_variationK => $product_variation) {
                $match = count(array_intersect($variations_to_search, $product_variation['type'])) == count($variations_to_search);
                if ($match) {
                    if($action == "subtract") {
                        $qty = $product_variation['data']['warehouse_qty'] - $qty;
                    } else if($action == "sum") {
                        $qty = $product_variation['data']['warehouse_qty'] + $qty;
                    }

                    if($qty < 0) $qty = 0;

                    $variants[$product_variationK]['data']['warehouse_qty'] = $qty;
                    break;
                }
            }

            $this->MSFrameworkDatabase->pushToDB("UPDATE ecommerce_products SET variants = :variants WHERE id = :id", array(":variants" => json_encode($variants), ":id" => $product_id));
        } else {
            $product_detail = $this->getProductDetails($product_id, "manage_warehouse, warehouse_qty")[$product_id];

            if($product_detail['manage_warehouse'] == 1) {
                if($action == "subtract") {
                    $qty = $product_detail['warehouse_qty'] - $qty;
                } else if($action == "sum") {
                    $qty = $product_detail['warehouse_qty'] + $qty;
                }

                if($qty < 0) $qty = 0;

                $this->MSFrameworkDatabase->pushToDB("UPDATE ecommerce_products SET warehouse_qty = :qty WHERE id = :id", array(":qty" => $qty, ":id" => $product_id));
            }
        }
    }

    /**
     * Ottiene un array con le informazioni (Prezzo, Quantità) della precisa combinazione scelta
     *
     * @param array $input_v L'array contenente le variazioni scelte dall'utente
     * @param array $products_v L'array contenente tutte le variazioni del prodotto
     *
     * @return array
     */
    public function getProductVariationInfo($product_id, $input_v) {
        Global $MSFrameworki18n;

        $product_detail = $this->getProductDetails($product_id)[$product_id];
        $product_variants = json_decode($product_detail['variants'], true);

        $array_to_return = $product_detail;
        $array_to_return['warehouse_qty'] = ($product_detail['manage_warehouse'] == 1 ? $product_detail['warehouse_qty'] : 100);
        $array_to_return['gallery'] = json_decode($product_detail['gallery'], true);
        $array_to_return['original_gallery'] = $array_to_return['gallery'];

        if($input_v) {
            $variations_to_search = array();

            foreach ($input_v as $single_input) {
                $variations_to_search[$single_input['id']] = $single_input['value'];
            }

            $specific_match = false;
            foreach ($product_variants as $product_variation) {

                $match = true;
                foreach($variations_to_search as $v_k => $v_v) {
                    if($product_variation['type'][$v_k] !== "" && $product_variation['type'][$v_k] !== $v_v) {
                        $match = false;
                    }
                }

                if ($match && !$specific_match) {

                    $empty_variations = false;
                    foreach($product_variation['type'] as $v_v) {
                        if(empty($v_v)) $empty_variations = true;
                    }

                    if(!$empty_variations) {
                        $specific_match = true;
                    }

                    if ($product_variation['data']['gallery']) {
                        $array_to_return['gallery'] = $product_variation['data']['gallery'];
                    }

                    /* SOVRASCRIVE I DATI DEL PRODOTTO CON QUELLI DELLA VARIAZIONE SOLO SE TROVATI */
                    if (!empty($product_variation['data']['prezzo']) && $array_to_return['prezzo'] !== $product_variation['data']['prezzo']) {

                        if($array_to_return['imposta'] === 'default') {
                            $array_to_return['imposta'] = (new \MSFramework\Fatturazione\imposte())->getDefaultVat();
                        }

                        $array_to_return['prezzo'] = (new \MSFramework\Ecommerce\imposte())->createTaxPricesArray($product_variation['data']['prezzo'], $product_detail['imposta']);
                        if (!empty($product_variation['data']['prezzo_promo']) || $array_to_return['prezzo'] !== $product_variation['data']['prezzo']) {
                            $array_to_return['prezzo_promo'] = (new \MSFramework\Ecommerce\imposte())->createTaxPricesArray($product_variation['data']['prezzo_promo'], $product_detail['imposta']);
                        }
                    }

                    if (!empty($product_variation['data']['sku'])) {
                        $array_to_return['product_code'] = $product_variation['data']['sku'];
                    }

                    if (!empty($MSFrameworki18n->getFieldValue($product_variation['data']['short_descr']))) {
                        $array_to_return['short_descr'] = $product_variation['data']['short_descr'];
                    }

                    if ($product_variation['data']['manage_warehouse'] == 1) $array_to_return['warehouse_qty'] = $product_variation['data']['warehouse_qty'];
                }
            }
        }

        Global $MSHooks;
        return $MSHooks->filter('ecommerce', 'product', 'variation_info')->run($array_to_return);
    }

    /**
     * Restituisce percentuale di sconto arrotondata per eccesso
     *
     * @param int $price Il campo del prezzo
     * @param int $price_promo Il campo del prezzo scontato
     *
     * @return int
     */
    public function pricePercentage($price, $price_promo){
        $percentage = 100 - (($price_promo * 100) / $price);

        return ceil($percentage);
    }

    /**
     * Restituisce un array associativo formato dal valore intero, e del decimale del prezzo.
     *
     * @param string $price Il campo del prezzo
     *
     * @return array
     */
    public function formatPrice($price) {

        Global $MSFrameworkCurrencies;
        $price = $MSFrameworkCurrencies->convertValue($price);

        $explode = explode('.', $price);

        if(!isset($explode[1])) {
            if(intval($price) > 0) $value = array(intval($price), '00');
            else $value = array();
        }
        else $value = array(intval($explode[0]), $explode[1] . (strlen($explode[1]) == 1 ? '0' : ''));

        return $value;
    }

    /**
     * Restituisce il valore numerico di un Array del prezzo.
     *
     * @param string $price_array Il campo del prezzo
     *
     * @return array
     */
    public function priceArrayToNumeric($price_array) {

        $price_numeric = floatval(implode('.', $price_array));

        return $price_numeric;
    }

    /**
     * Restituisce un array del prodotto formattato
     *
     * @param array $r L'array del prodotto da formattare
     *
     * @return array
     */
    public function formatProductArray($r, $format_variants = true) {

        Global $MSFrameworkUrl, $MSFrameworkCMS, $MSFrameworki18n;

        if($r['imposta'] == '') {
            $r['imposta'] = 0;
        } else if($r['imposta'] == 'default') {
            $r['imposta'] = (new \MSFramework\Fatturazione\imposte())->getDefaultVat();
        }

        if(isset($r['prezzo'])) $r['prezzo'] = (new \MSFramework\Ecommerce\imposte())->createTaxPricesArray($r['prezzo'], $r['imposta']);

        if(isset($r['prezzo_promo'])) {

            $r['prezzo_promo'] = (new \MSFramework\Ecommerce\imposte())->createTaxPricesArray($r['prezzo_promo'], $r['imposta']);

            $offerTime = new \DateTime();

            $offerTime->setTimestamp($r['prezzo_promo_expire']);
            $offerTime->setTime(0, 0);

            if(!empty($r['prezzo_promo_expire']) && time() >= $offerTime->format('U')) {

                if($r['auto_renew_promo'] > 0) {

                    // Continua ad aggiungere giorni alla data di scadenza fino a quando la scadenza sarà posteriore.
                    for($new_offer_date = $r['prezzo_promo_expire']; $new_offer_date < (time()+86400); ) {
                        $new_offer_date += (86400 * $r['auto_renew_promo']);
                    }

                    $r['prezzo_promo_expire'] = $new_offer_date;

                }
                else $r['prezzo_promo'] = (new \MSFramework\Ecommerce\imposte())->createTaxPricesArray(0, $r['imposta']);

            }
        }

        if(isset($r['category'])) {

            $category_array = explode(',', $r['category']);

            $r['all_categories'] = $category_array;

            if($_SERVER['HTTP_REFERER'] != $MSFrameworkCMS->getURLToSite() && isset($_SESSION['last_category_visited']) && in_array($_SESSION['last_category_visited'], $category_array)) {
                $r['category'] = $_SESSION['last_category_visited'];
            }
            else
            {
                $r['category'] = $category_array[0];
            }
        }

        if(isset($r['enable_custom_message'])) {
            if($r['enable_custom_message'] == 1) {
                $r['custom_message'] = json_decode($r['custom_message'], true);
            }
        }

        if(isset($r['attachments'])) {
            $r['attachments'] = json_decode($r['attachments'], true);
            if($r['attachments']) {
                foreach ($r['attachments'] as $att_k => $att_v) {
                    $r['attachments'][$att_k]['file'] = array(
                        'name' => $att_v['file'][0],
                        'url' => UPLOAD_ECOMMERCE_ATTACHMENTS_FOR_DOMAIN_HTML . $att_v['file'][0]
                    );
                    $r['attachments'][$att_k]['only_for_users'] = (int)$att_v['only_for_users'];
                }
            }
            else
            {
                $r['attachments'] = array();
            }
        }
        else
        {
            $r['attachments'] = array();
        }

        if(isset($r['gallery'])) $r['gallery_friendly'] = (new \MSFramework\uploads('ECOMMERCE_PRODUCTS'))->composeGalleryFriendlyArray(json_decode($r['gallery'], true));

        if(isset($r['variants'])) $r['range_prezzo'] = $this->getVariationPriceRange($r['variants'], $r['imposta']);

        if(isset($r['attributes'])) $r['formattedAttributes'] = (new \MSFramework\Ecommerce\attributes())->formatAttributesForProduct($r['attributes']);

        if(isset($r['slug'])) $r['url'] = $MSFrameworkCMS->getURLToSite() . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($r['slug'])) . "/";

        if(isset($r['seo'])) $r['seo'] = (json_decode($r['seo']) ? json_decode($r['seo'], true) : array());

        Global $MSHooks;
        return $MSHooks->filter('ecommerce', 'product', 'data')->run($r);
    }

    /**
     * Ottiene tutti i prodotti che utilizzano un determinato attributo
     *
     * @param $attributes L'ID dell'attributo (stringa) o degli attributi (array)
     * @param string $fields I campi da prelevare dalla tabella dei prodotti
     *
     * @return bool|void
     */
    public function getProductsByAttributes($attributes, $fields = "*") {
        if($attributes == "") {
            return false;
        }

        if(!is_array($attributes)) {
            $attributes = array($attributes);
        }

        $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($attributes, "OR", "attributes", "LIKE", "both", "", (new \MSFramework\Ecommerce\attributes())->jsonPrefix('attribute', 'get'));

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        foreach ($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_products WHERE is_active = 1 AND  (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $this->formatProductArray($r);
        }

        return $ary_to_return;
    }

    /**
     * Ottiene l'URL del prodotto
     *
     * @param $id L'ID del prodotto per il quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n, $MSFrameworkCMS, $MSFrameworkUrl;

        if(is_array($id)) {
            $page_det = $id;
        } else {
            $page_det = $this->getProductDetails($id, "slug")[$id];
        }

        return $MSFrameworkCMS->getURLToSite() . $MSFrameworkUrl->getLangPrefix() . $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";
    }

    /**
     * Converte l'ID (stringa separata da underscore) di un prodotto con variazione in un array strutturato ed utilizzabile in altre funzioni
     *
     * @param $string L'ID da convertire
     *
     * @return mixed
     */
    public function convertProductVariationStringToArray($string) {
        $ary_product_id = explode("_", $string);
        $ary_to_check['product_id'] = $ary_product_id[0];
        unset($ary_product_id[0]);

        $couple = false;
        foreach($ary_product_id as $k => $v) {
            if(!$couple) {
                $var_ary = array();
                $var_ary['id'] = $v;
                $couple = true;
            } else {
                $var_ary['value'] = $v;
                $ary_to_check['variation'][] = $var_ary;
                $couple = false;
            }
        }

        return $ary_to_check;
    }

    public function getWeightUM() {
        global $MSFrameworkCMS;

        $r = $MSFrameworkCMS->getCMSData('ecommerce_prodotti');
        if($r['weight_um'] == "") {
            $r['weight_um'] = "kg";
        }

        return $r['weight_um'];
    }
}
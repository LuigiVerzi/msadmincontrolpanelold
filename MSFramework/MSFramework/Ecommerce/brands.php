<?php
/**
 * MSFramework
 * Date: 17/04/18
 */

namespace MSFramework\Ecommerce;


class brands {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Ottiene tutti i brand disponibili
     *
     * @param string $fields I campi da estrapolare
     *
     * @return array
     */
    public function getBrands($id = "", $fields = '*') {

        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }
        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_brand WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
            $ary_to_return[$r['id']]['logo'] = json_decode($r['logo'], true);
        }
        return $ary_to_return;
    }

}
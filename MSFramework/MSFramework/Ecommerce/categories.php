<?php
/**
 * MSFramework
 * Date: 17/04/18
 */

namespace MSFramework\Ecommerce;


class categories
{
    public function __construct()
    {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria
     *
     * @param string $id L'ID della categoria (stringa) o delle categorie (array) richieste (se vuoto, vengono recuperati i dati di tutte le categorie)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryDetails($id = "", $fields = "*")
    {
        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }
        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $uploads = new \MSFramework\uploads('ECOMMERCE_CATEGORIES');

        $ary_to_return = array();
        $category = $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_categories WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);
        foreach ((array)$category as $r) {

            $r['gallery_friendly'] = array(
                'images' => array(),
                'banner' => array(),
                'images_extra' => array(),
            );

            foreach($r['gallery_friendly'] as $type => $a) {
                if(json_decode($r[$type])) {
                    $r['gallery_friendly'][$type] = $uploads->composeGalleryFriendlyArray(json_decode($r[$type], true));
                }
            }

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria
     *
     * @param array $list La lista della ricerca attuale o l'ID della categoria
     * @param boolean $check_filters Controlla solamente le categorie che hanno prodotti all'interno, utilizziando i filtri attivi.
     *
     * @return array
     */
    public function getCategoryChildrens($list, $check_filters = false)
    {
        /* INIZIO RETROCOMPATIBILITA' */
        if(is_numeric($list)) {
            $parent_id = $list;
            $args = func_get_args();
            if($check_filters) {
                $list = (new \MSFramework\Ecommerce\products())->prepareProductListParams(
                    array(
                        'category' => $parent_id,
                        'filters' => $args[2],
                        'query' => $args[3]
                    )
                );
            }
        }
        /* FINE RETROCOMPATIBILITA' */
        else {
            $parent_id = $list['params']['category'];
        }

        $children_ids = $this->getCategoryChildrenIds($parent_id, array());

        if($children_ids) {

            $children_categories = $this->getCategoryDetails($children_ids);

            /* Controlla la quantità di prodotti all'interno della categoria utilizzando i filtri attuali */
            if ($check_filters) {
                /* Vado ad ottenere il numero di eventuali prodotti per ogni categoria */
                foreach ($children_categories as $category_id => $children) {

                    // Cambio la categoria salvata nel params
                    $list['params']['category'] = $category_id;
                    $list_full_params = (new \MSFramework\Ecommerce\products())->prepareProductListParams($list['params']);

                    $children_categories[$category_id]['count'] = (new \MSFramework\Ecommerce\products())->countProducts($list_full_params);

                    // Se la categoria non contiene nessun prodotto non la mostro
                    if ($children_categories[$category_id]['count'] == 0) unset($children_categories[$category_id]);
                }
            }

        }
        else
        {
            $children_categories = array();
        }
        return $children_categories;
    }

    private function getCategoryChildrenIds($current, $category_order = array()) {

        if(!$category_order) {
            $category_order = $this->MSFrameworkCMS->getCMSData('ecommerce_cats_order')['list'];
        }

        $ids = array();

        foreach($category_order as $cat) {
            if($cat['id'] == $current) {
                foreach($cat['children'] as $child) {
                    $ids[] = $child['id'];
                    $ids = array_merge($ids, $this->getCategoryChildrenIds($child['id'], $cat));
                }
            } else {
                $ids = array_merge($ids, $this->getCategoryChildrenIds($current, $cat));
            }
        }

        return $ids;

    }

    /**
     * Restituisce un array associativo (ID Categoria => Dati Categoria) con i dati relativi alla categoria genitore
     *
     * @param string $id L'ID della categoria figlia
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getCategoryParent($children_id, $fields = "*")
    {

        $category_order = $this->MSFrameworkCMS->getCMSData('ecommerce_cats_order')['list'];

        $parent_id = array();
        foreach ($category_order as $parent) {
            foreach($parent['children'] as $children) {
                if ($children['id'] == $children_id) {
                   $parent_id = $parent['id'];
                }
                else if(isset($children['children']))
                {
                    foreach($children['children'] as $subchildren) {
                        if ($subchildren['id'] == $children_id) {
                            $parent_id = array($parent['id'], $children['id']);
                        } else if(isset($children['children'])) {
                            foreach($subchildren['children'] as $subsubchildren) {
                                if ($subsubchildren['id'] == $children_id) {
                                    $parent_id = array($parent['id'], $children['id'], $subchildren['id']);
                                }
                            }
                        }

                    }
                }
            }
        }

        $category = ($parent_id ? $this->getCategoryDetails($parent_id) : array());

        return $category;
    }

    /**
     * Restituisce (boolean) se la categoria è genitore o no
     *
     * @param string $id L'ID della categoria da controllare
     *
     * @return boolean
     */
    public function isCategoryParent($id)
    {

        $category_order = $this->MSFrameworkCMS->getCMSData('ecommerce_cats_order')['list'];

        foreach ($category_order as $parent) {
            if ($parent['id'] == $id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Restituisce la struttura padre/figlio del menu in formato HTML.
     *
     * @param string $menu_type Il tipo di HTML desiderato (accetta: select)
     * @param string $active_id L'ID della categoria attualmente in uso (o da impostare come attiva/selezionata)
     * @param string $ary_menu L'array contenente le voci del menu per il quale effettuare il parsing. Se vuoto, viene recuperato tutto il menu
     * @param string $html_struct L'array contenente i valori per la struttura HTML
     * @param boolean $hide_empty Se true nasconde le categorie che non contengono prodotti
     *
     * @return string
     */
    public function composeHTMLMenu($menu_type = "select", $active_id = "", $ary_menu = "", $html_struct = "", $hide_empty = false, $child_level = 0)
    {
        global $MSFrameworki18n, $ind_level, $firephp;

        if (!is_array($ary_menu)) {
            $ary_menu = $this->MSFrameworkCMS->getCMSData('ecommerce_cats_order')['list'];
        }

        $html = "";

        if ($menu_type == "select") {
            if ($ind_level == "") {
                $ind_level = 1;
            }

            if (!is_array($active_id)) {
                $active_id = array($active_id);
            }

            foreach ($ary_menu as $cur_menu) {
                if($hide_empty) {
                    if(!(new \MSFramework\Ecommerce\products())->countProducts($cur_menu['id'])) continue;
                }
                $nbsp = "";
                for ($x = 0; $x < $child_level; $x++) {
                    $nbsp .= "-";
                }

                $set_selected = (in_array($cur_menu['id'], $active_id) ? "selected" : "");

                $html .= '<option value="' . $cur_menu['id'] . '"' . $set_selected . '>' . $nbsp . "- " . $MSFrameworki18n->getFieldValue($this->getCategoryDetails($cur_menu['id'], "nome")[$cur_menu['id']]['nome'], true) . '</option>';

                if (is_array($cur_menu['children'])) {
                    $html .= $this->composeHTMLMenu($menu_type, $active_id, $cur_menu['children'], "", $hide_empty, $child_level+1);
                }
            }
        } else if($menu_type = "navigation") {

            if(!is_array($html_struct)) { //imposto un default nel caso in cui $html_struct non è un array
                $html_struct = array(
                    "main_element" => "<li>",
                    "main_element_append_class" => "",
                    "main_element_active_class" => "current-menu-item",
                    "sub_element" => "<ul>",
                    "sub_element_append_class" => "dropdown-menu",
                    "arrow_down" => '<span class="fa fa-caret-down"></span>',
                    "arrow_right" => '<span class="fa fa-caret-right"></span>',
                );
            }
            foreach ($ary_menu as $cur_menu) {

                $main_element_append_class = $html_struct['main_element_append_class'];

                if($hide_empty) {
                    if(!(new \MSFramework\Ecommerce\products())->countProducts($cur_menu['id'])) continue;
                }

                $caret = "";
                if (is_array($cur_menu['children'])) {
                    $caret = $html_struct['arrow_down'];
                }

                $category_info = $this->getCategoryDetails($cur_menu['id'])[$cur_menu['id']];
                $menu_colors = json_decode($category_info['menu_colors'], true);

                $menu_bgcolor = ($menu_colors['bgColor'] != '') ? " background-color: " . $menu_colors['bgColor'] . "; " : "";
                $menu_color = ($menu_colors['color'] != '') ? " color: " . $menu_colors['color'] . "; " : "";
                $menu_mouseover = ($menu_colors['mouseover'] != '') ? ' onmouseover="this.style.backgroundColor=\'' . $menu_colors['mouseover'] . '\';" onmouseout="this.style.backgroundColor=\'' . $menu_colors['bgColor'] . '\';"' : "";

                $html_childrens = "";
                $childrens_data = array();
                if(is_array($cur_menu['children'])) {
                    $childrens_data = $this->_getChildren($cur_menu['children'], $html_struct, $active_id, false, $category_info, $hide_empty);
                    $html_childrens = $childrens_data[0];
                }

                $set_page_active = "";
                if($active_id == $cur_menu['id'] || $childrens_data[1] === true) {
                    $set_page_active = " " . $html_struct['main_element_active_class'];
                }

                if($html_childrens != "") {
                    $main_element_append_class  .= ' dropdown';
                }

                $main_element = str_replace(">", " class='" . $main_element_append_class . $set_page_active . "'>", $html_struct['main_element']);

                $html .= $main_element . '<a href="' . $this->getURL($category_info['id']) . '" style="' . $menu_bgcolor . $menu_color . '" ' . $menu_mouseover . '>' . $MSFrameworki18n->getFieldValue($category_info['nome']) . ' ' . $caret . '</a>';

                if($html_childrens != "") {
                    $html .= $html_childrens;
                }

                $html .= str_replace("<", "</", $html_struct['main_element']);

            }

        }

        return $html;
    }

    private function _getChildren($children, $html_struct, $current_active_page, $set_parent_active = false, $parent_info = array(), $hide_empty = false) {
        global $firephp, $MSFrameworki18n;

        $sub_element = $html_struct['sub_element'];
        if($html_struct['sub_element_append_class'] != "") {
            $sub_element = str_replace(">", " class='" . $html_struct['sub_element_append_class'] . "'>", $html_struct['sub_element']);
        }
        $html = $sub_element;

        foreach($children as $cur_children) {

            if($hide_empty) {
                if(!((new \MSFramework\Ecommerce\products())->countProducts($cur_children['id']))) continue;
            }

            $caret = "";
            if(is_array($cur_children['children'])) {
                $caret = $html_struct['arrow_right'];
            }

            if($set_parent_active !== true) {
                if($current_active_page == $cur_children['id']) {
                    $set_parent_active = true;
                }
            }

            $category_info = $this->getCategoryDetails($cur_children['id'])[$cur_children['id']];
            $menu_colors = json_decode($category_info['menu_colors'], true);
            $menu_bgcolor = ($menu_colors['bgColor'] != '') ? " background-color: " . $menu_colors['bgColor'] . "; " : "";
            $menu_color = ($menu_colors['color'] != '') ? " color: " . $menu_colors['color'] . "; " : "";
            $menu_mouseover = ($menu_colors['mouseover'] != '') ? ' onmouseover="this.style.backgroundColor=\'' . $menu_colors['mouseover'] . '\';" onmouseout="this.style.backgroundColor=\'' . $menu_colors['bgColor'] . '\';"' : "";

            $html .= $html_struct['main_element'] . '<a href="' . $this->getURL($category_info['id']) . '" style="' . $menu_bgcolor . $menu_color . '" ' . $menu_mouseover . '>' . $MSFrameworki18n->getFieldValue($category_info['nome']) . ' ' . $caret . '</a>';

            if(is_array($cur_children['children'])) {
                $html .= $this->_getChildren($cur_children['children'], $html_struct, $current_active_page, $set_parent_active, $parent_info, $hide_empty)[0];
            }

            $html .= str_replace("<", "</", $html_struct['main_element']);
        }

        $html .= str_replace("<", "</", $html_struct['sub_element']);

        return array($html, $set_parent_active);
    }

    /**
     * Restituisce la struttura della paginazione in HTML
     *
     * @param int $current_query Il numero di prodotti totali ottenuti
     * @param int $total_products Il numero di prodotti totali ottenuti
     * @param int $current_page La pagina che si sta mostrando attualmente
     * @param int $ary_menu Il numero di prodotti che vengono mostrati per pagina
     *
     * @return string
     */
    public function composeHTMLPagination($current_query, $total_products, $current_page = 1, $show_per_page = 12)
    {
        $links = 10;

        $last = ceil($total_products / $show_per_page);

        $start = (($current_page - $links) > 0) ? $current_page - $links : 1;
        $end = (($current_page + $links) < $last) ? $current_page + $links : $last;

        $html = '<div class="pagination">';

        $class = ($current_page == 1) ? "disabled" : "";
        $html .= '<a class="' . $class . '" href="?' . $this->composeURLFilters($current_query, 'page', $current_page - 1) . '">&laquo;</a>';

        if ($start > 1) {
            $html .= '<a href="?page=1">1</a>';
            $html .= '<a class="disabled"><span>...</span></a>';
        }

        for ($i = $start; $i <= $end; $i++) {
            $class = ($current_page == $i) ? "active" : "";
            $html .= '<a class="' . $class . '" href="?' . $this->composeURLFilters($current_query, 'page', $i) . '">' . $i . '</a>';
        }

        if ($end < $last) {
            $html .= '<a class="disabled"><span>...</span></a>';
            $html .= '<a class="last" href="?' . $this->composeURLFilters($current_query, 'page', $last) . '">' . $last . '</a>';
        }

        $class = ($current_page == $last) ? "disabled" : "";
        $html .= '<a class="' . $class . '" ' . ($class != 'disabled' ? 'href="?' . $this->composeURLFilters($current_query, 'page', $current_page + 1) . '"' : '') . '>&raquo;</a>';

        $html .= '</div>';

        return $html;

    }

    /**
     * Restituisce la struttura della paginazione in HTML
     *
     * @param string $key Il parametro da modificare/aggiungere
     * @param string $value Il valore del parametro da modificare/aggiungere
     *
     * @return string
     */
    public function composeURLFilters($query, $key, $value)
    {
        $query[$key] = $value;
        $query_result = http_build_query($query);

        return urldecode($query_result);

    }


    /**
     * Restituisce un Array con tutti i filtri attivi
     *
     * @return array
     */
    public function getActiveFilters($query)
    {

        $active_filters = array();

        foreach ($query as $param => $val) {
            if ($param == 'price') {

                $explode = explode('-', $val);

                if (count($explode) == 2 && ($explode[0] < $explode[1] || empty($explode[1]))) {
                    $custom_query = $query;
                    unset($custom_query['price']);

                    if(!empty($explode[1])) {
                        $name = number_format($explode[0],2,',','.') . CURRENCY_SYMBOL . ' - ' . number_format($explode[1],2,',','.') . CURRENCY_SYMBOL;
                    }
                    else
                    {
                        $name = number_format($explode[0],2,',','.') . '+' . CURRENCY_SYMBOL;
                    }
                    $active_filters['price'] = array(
                        'id' => $val,
                        'nome' => $name,
                        'href' => urldecode(http_build_query($custom_query))
                    );
                }
            }
            else if ($param == 'cat') {
                $category = $this->getCategoryDetails($val);
                if (isset($category[$val])) {
                    $custom_query = $query;
                    unset($custom_query['cat']);

                    $active_filters['cat'] = array(
                        'id' => $val,
                        'nome' => $category[$val]['nome'],
                        'href' => urldecode(http_build_query($custom_query))
                    );
                }
            } else if ($param == 'attr') {
                foreach ($val as $attr_id => $attr_value) {
                    $attribute = (new \MSFramework\Ecommerce\attributes())->getValuesOfAttributeDetails($attr_id, $attr_value);
                    if ($attribute) {
                        $custom_query = $query;
                        unset($custom_query['attr'][$attr_id]);
                        $active_filters['attribute_' . $attr_id] = array(
                            'id' => $attr_value,
                            'nome' => $attribute[$attr_value]['nome'],
                            'href' => urldecode(http_build_query($custom_query)),
                            'data' => $attribute[$attr_value]
                        );
                    }
                }
            }
        }
        return $active_filters;
    }

    /**
     * Ottiene l'URL della categoria
     *
     * @param $id L'ID (o Array) della categoria per la quale si desidera l'URL
     *
     * @return string
     */
    public function getURL($id) {
        global $MSFrameworki18n, $MSFrameworkUrl;

        $ecommerce_settings = $this->MSFrameworkCMS->getCMSData('ecommerce_prodotti');

        if(is_array($id)) $page_det = $id;
        else $page_det = $this->getCategoryDetails($id, "slug")[$id];

        $slug = '';

        if(!isset($ecommerce_settings['category_multilevel_slug']) || $ecommerce_settings['category_multilevel_slug'] === "1") {
            $have_parents = $this->getCategoryParent($page_det['id']);
            if ($have_parents) {
                foreach ($have_parents as $parent) {
                    $slug .= $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($parent['slug'])) . "/";
                }
            }
        }

        $slug .= $MSFrameworkUrl->cleanString($MSFrameworki18n->getFieldValue($page_det['slug'])) . "/";

        return ABSOLUTE_SW_PATH_HTML . $MSFrameworkUrl->getLangPrefix() . $slug;
    }
}
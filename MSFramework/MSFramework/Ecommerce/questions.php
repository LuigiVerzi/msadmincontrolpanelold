<?php
/**
 * MSFramework
 * Date: 17/04/18
 */

namespace MSFramework\Ecommerce;


class questions {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Aggiunge una domanda al prodotto
     *
     * @param int $product_id L'id del prodotto
     * @param int $user_id L'id del cliente
     * @param string $question Il commento testuale (opzionale)
     * @param mixed $status Lo stato della recensione (Se da approvare o no)
     *
     * @return integer
     */
    public function addQuestionToProduct($product_id, $user_id, $question, $status = 1) {

        $array_to_save = array(
            "product_id" => $product_id,
            "user_id" => $user_id,
            "question" => $question,
            "status" => (int)$status,
            "question_date" => date("Y-m-d H:i:s")
        );

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "insert");

        if ($this->MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_product_questions ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0])) {
            return $this->MSFrameworkDatabase->lastInsertId();
        } else {
            return false;
        }


    }

    /**
     * Ottiene tutte le domande per un determinato prodotto
     *
     * @param int $product_id L'id del prodotto
     * @param int $page La pagina delle recensioni da mostrare
     *
     * @return array
     */
    public function getQuestionsByProductID($product_id, $page = 1, $show_user = false) {

        /* IMPOSTA L'OFFSET IN BASE ALLA PAGINA DA MOSTRARE */
        $limit = 8;
        $start = ($page * $limit) - $limit;
        if($start < 0) $start = 0;

        if(!$product_id) {
            $product_id = 0;
        }

        $total_questions = $this->countQuestion($product_id, true);

        $ary_to_return = array('page' => $page, 'showing_per_page' => $limit, 'total_questions' => $total_questions, 'questions' => array());

        $user = (new \MSFramework\Ecommerce\customers())->getUserDataFromSession();
        if($show_user && $user) {

            $show_where = "((answer != '' AND status = 1) OR user_id = " . (int)$user['user_id'] . ")";
        }
        else
        {
            $show_where = "answer != '' AND status = 1";
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_questions WHERE product_id = $product_id AND question != '' AND $show_where LIMIT $start, $limit") as $r) {
            $ary_to_return['questions'][$r['id']] = $this->formatQuestionArray($r);
        }
        return $ary_to_return;
    }

    /**
     * Conta le recensioni testuali per un determinato prodotto
     *
     * @param int $product_id L'id del prodotto da esaminare
     * @param int $only_complete Stabilisce se contare solo le recensioni che contengono la risposta
     *
     * @return integer
     */
    public function countQuestion($product_id, $show_user = false) {

        $user = (new \MSFramework\Ecommerce\customers())->getUserDataFromSession();
        if($show_user && $user) {

            $show_where = "((answer != '' AND status = 1) OR user_id = " . (int)$user['user_id'] . ")";
        }
        else
        {
            $show_where = "answer != '' AND status = 1";
        }

        return $this->MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_product_questions WHERE $show_where AND product_id = " . $product_id);
    }

    /**
     * Formatta l'array delle recensioni
     *
     * @param array $recensione I campi da formattare
     *
     * @return array
     */
    public function formatQuestionArray($domanda) {

        if($domanda['user_id'] == 0)
        {
            $domanda['user_info'] = json_decode($domanda['fake_customer'], true);
        }
        else {
            $domanda['user_info'] = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($domanda['user_id'], 'id, nome, cognome, email');
        }
        $domanda['question_date'] = (new \MSFramework\utils())->smartdate(strtotime($domanda['question_date']));
        $domanda['answer_date'] = (new \MSFramework\utils())->smartdate(strtotime($domanda['answer_date']));

        return $domanda;

    }

    /**
     * Invia l'email relativa alla risposta della domanda in questione
     *
     * @param $question_id L'ID della domanda
     *
     * @return bool
     */
    public function sendNewAnswerEmail($question_id) {

        if(strstr(REAL_SW_PATH, "MSAdminControlPanel")) {
            $key = 'asdgfdsagfsad';
            $ajax_url = (new \MSFramework\cms())->getURLToSite() . 'ajax/product.php?action=send_question_reply_email&key=' . $key . '&id=' . $question_id;

            return file_get_contents($ajax_url);
        }
        else
        {

            $question = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_questions WHERE id = $question_id", array(), true);

            $product_id = $question['product_id'];
            $user_id = $question['user_id'];

            $prodotto = (new \MSFramework\Ecommerce\products())->getProductDetails($product_id)[$product_id];
            $user = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($user_id);

            (new \MSFramework\Ecommerce\emails())->sendMail('products-questions/customer_new-answer', array(
                'nome' => $user['nome'] . ' ' . $user['cognome'],
                'email' => $user['email'],
                'subject' => (new \MSFramework\i18n())->gettext('Hai ricevuto una nuova risposta.'),
                'slug_prodotto' => (new \MSFramework\i18n())->getFieldValue($prodotto['slug']),
                'nome_prodotto' => (new \MSFramework\i18n())->getFieldValue($prodotto['nome'])
            ));
        }
    }

    /**
     * Restituisce la struttura della paginazione in HTML
     *
     * @param int $total_questions Il numero di prodotti totali ottenuti
     * @param int $current_page La pagina che si sta mostrando attualmente
     * @param int $show_per_page Il numero di prodotti che vengono mostrati per pagina
     *
     * @return string
     */
    public function composeHTMLPagination($total_questions, $current_page = 1, $show_per_page = 8)
    {

        $links = 10;

        $last = ceil($total_questions / $show_per_page);

        $start = (($current_page - $links) > 0) ? $current_page - $links : 1;
        $end = (($current_page + $links) < $last) ? $current_page + $links : $last;

        $html = '<div class="pagination">';

        $class = ($current_page == 1) ? "disabled" : "";
        $html .= '<a class="' . $class . '" href="?qp=' . ($current_page - 1) . '">&laquo;</a>';

        if ($start > 1) {
            $html .= '<a href="?qp=1">1</a>';
            $html .= '<a class="disabled"><span>...</span></a>';
        }

        for ($i = $start; $i <= $end; $i++) {
            $class = ($current_page == $i) ? "active" : "";
            $html .= '<a class="' . $class . '" href="?qp=' . $i . '">' . $i . '</a>';
        }

        if ($end < $last) {
            $html .= '<a class="disabled"><span>...</span></a>';
            $html .= '<a href="?qp=' . $last . '">' . $last . '</a>';
        }

        $class = ($current_page == $last) ? "disabled" : "";
        $html .= '<a class="' . $class . '" href="?qp=' . ($current_page + 1) . '">&raquo;</a>';

        $html .= '</div>';

        return $html;

    }

}
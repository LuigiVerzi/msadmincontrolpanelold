<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

namespace MSFramework\Ecommerce\Payments;


class nexi {
    public function __construct() {
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param array $orderDetails L'Array con le info relative all'ordine
     * @param array $preferences L'Array con le preferenze
     *
     * @return string La stringa con l'URL che porta al pagamento
     *
     */
    public function getPaymentUrl($orderDetails, $preferences = array()) {
        Global $MSFrameworkCMS;

        $merchantServerUrl = (new \MSFramework\cms())->getURLToSite();

        $default_preferences = array(
            'ipn_url' => $merchantServerUrl . "ipn/nexi_ipn.php",
            'order_url' =>  $merchantServerUrl . 'profilo/ordine/' . $orderDetails['id']
        );

        foreach($default_preferences as $def_k => $def_v) {
            if(!isset($preferences[$def_k])) {
                $preferences[$def_k] = $def_v;
            }
        }

        $loggedUser = (new \MSFramework\Ecommerce\customers())->getUserDataFromSession();
        $metodi_pagamento = (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamentoWeb();

        // Alias e chiave segreta
        $ALIAS = $metodi_pagamento['nexi_data']['alias'];
        $CHIAVESEGRETA = $metodi_pagamento['nexi_data']['secret_key'];

        $requestUrl = "https://" . ($MSFrameworkCMS->isStaging() || $MSFrameworkCMS->isDevelopment() ? 'int-' : '') . "ecommerce.nexi.it/ecomm/ecomm/DispatcherServlet";

        $codTrans = "ORDER" . $orderDetails['id'];
        $divisa = "EUR";
        $importo = ($orderDetails['cart']['coupon_price'] + ($orderDetails['tipo_spedizione']['prezzo'] > 0 ? $orderDetails['tipo_spedizione']['prezzo'] : 0))*100;

        // Calcolo MAC
        $mac = sha1('codTrans=' . $codTrans . 'divisa=' . $divisa . 'importo=' . $importo . $CHIAVESEGRETA);

        // Parametri obbligatori
        $obbligatori = array(
            'alias' => $ALIAS,
            'importo' => $importo,
            'divisa' => $divisa,
            'codTrans' => $codTrans,
            'url' => urlencode(($loggedUser ? $preferences['order_url'] : (new \MSFramework\Ecommerce\orders())->getGuestOrderUrl($orderDetails['id']))),
            'url_back' => urlencode(($loggedUser ? $preferences['order_url'] . (strpos($preferences['order_url'], '?') !== false ? '&' : '?') . 'annullato' : (new \MSFramework\Ecommerce\orders())->getGuestOrderUrl($orderDetails['id']) . '&annullato')),
            'mac' => $mac,
        );

        // Parametri facoltativi
        $facoltativi = array(
            'urlpost' => $preferences['ipn_url'],
            'mail' => $loggedUser['username']
        );

        $requestParams = array_merge($obbligatori, $facoltativi);

        $aRequestParams = array();
        foreach ($requestParams as $param => $value) {
            $aRequestParams[] = $param . "=" . $value;
        }

        $stringRequestParams = implode("&", $aRequestParams);

        $redirectUrl = $requestUrl . "?" . $stringRequestParams;

        return $redirectUrl;
    }
    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param array $request
     * @return string La stringa con l'URL che porta al pagamento
     *
     */
    public function checkIPN($request) {

        $metodi_pagamento = (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamentoWeb();

        // Chiave segreta
        $CHIAVESEGRETA = $metodi_pagamento['nexi_data']['secret_key'];

        // Controllo che ci siano tutti i parametri di ritorno obbligatori per calcolare il MAC
        $requiredParams = array('codTrans', 'esito', 'importo', 'divisa', 'data', 'orario', 'codAut', 'mac');
        foreach ($requiredParams as $param) {
            if (!isset($request[$param])) {
                echo 'Paramentro mancante ' . $param;
                header('500 Internal Server Error', true, 500);
                exit;
            }
        }

        // Calcolo MAC con i parametri di ritorno
        $macCalculated = sha1('codTrans=' . $request['codTrans'] .
            'esito=' . $request['esito'] .
            'importo=' . $request['importo'] .
            'divisa=' . $request['divisa'] .
            'data=' . $request['data'] .
            'orario=' . $request['orario'] .
            'codAut=' . $request['codAut'] .
            $CHIAVESEGRETA
        );

        // Verifico corrispondenza tra MAC calcolato e MAC di ritorno
        if ($macCalculated != $request['mac']) {
            echo 'Errore MAC: ' . $macCalculated . ' non corrisponde a ' . $request['mac'];
            header('500 Internal Server Error', true, 500);
            exit;
        }

        // Nel caso in cui non ci siano errori gestisco il parametro esito
        if ($request['esito'] == 'OK') {

            $orders = new \MSFramework\Ecommerce\orders();
            $order_id = (int)filter_var($request['codTrans'], FILTER_SANITIZE_NUMBER_INT);

            $array_to_save = array(
                "payment_log" => json_encode($_POST),
                "provider" => 'nexi'
            );

            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "insert");
            $this->MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_payments_logs ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            $payment_id = $this->MSFrameworkDatabase->lastInsertId();

            $this->MSFrameworkDatabase->query("UPDATE ecommerce_orders SET payment_id = " . $payment_id . ", payment_type = 'nexi' WHERE id = " . $order_id);

            $orders->updateOrderStatus($order_id, 1);
            $orders->updateProductsQtyOnStatusVariation($order_id);

            header('OK, pagamento avvenuto, preso riscontro', true, 200);

        } else {
            header('KO, pagamento non avvenuto, preso riscontro', true, 200);
        }

        return false;
    }
}
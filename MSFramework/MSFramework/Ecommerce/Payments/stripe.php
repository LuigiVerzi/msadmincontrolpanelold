<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

namespace MSFramework\Ecommerce\Payments;


class stripe {
    public function __construct() {
        Global $MSFrameworkDatabase;
        Global $MSFrameworki18n;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworki18n = $MSFrameworki18n;
    }

    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param array $orderDetails L'Array con le info relative all'ordine
     * @param array $preferences L'Array con le preferenze
     * @param boolean $autoclick Se positivo aprirà automaticamente il form di pagamento
     *
     * @return string La stringa con l'URL che porta al pagamento
     *
     */
    public function getPaymentButton($orderDetails, $preferences, $autoclick = false) {

        $default_preferences = array(
            'ipn_url' => (new \MSFramework\cms())->getURLToSite() . 'm/payments',
            'order_url' => (new \MSFramework\cms())->getURLToSite(). 'profilo/ordine/' . $orderDetails['id']
        );

        foreach($default_preferences as $def_k => $def_v) {
            if(!isset($preferences[$def_k])) {
                $preferences[$def_k] = $def_v;
            }
        }

        $metodi_pagamento = (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamentoWeb();

        if($metodi_pagamento['stripe_data']['logo'] && json_decode($metodi_pagamento['stripe_data']['logo'])) {
            $stripe_logo = (isset($_SERVER['HTTPS']) ? "https" : "http") . ":" . UPLOAD_ECOMMERCE_PAYMENTS_LOGOS_FOR_DOMAIN_HTML . json_decode($metodi_pagamento['stripe_data']['logo'], true)[0];
        }
        else {
            $stripe_logo = '';
        }

        $CHIAVEPUBBLICAZIONE = $metodi_pagamento['stripe_data']['publishable_key'];

        $importo = ($orderDetails['cart']['coupon_price'] + ($orderDetails['tipo_spedizione']['prezzo'] > 0 ? $orderDetails['tipo_spedizione']['prezzo'] : 0))*100;

        $html = '<script src="https://checkout.stripe.com/checkout.js"></script>';

        $html .= '<a id="stripe_payments" href="#" class="btn btn-success' . ($autoclick ? ' autoclick' : '') . '">' . $this->MSFrameworki18n->gettext("Procedi con il Pagamento") . '</a>';

        $html .= '<script>';
        $html .= 'document.onreadystatechange = function () {';
        $html .= 'if (document.readyState == "complete") {';
        $html .= 'attachStripePaymentsToButton("stripe_payments", ' . $orderDetails['id'] . ', ' . $importo . ', "' . SW_NAME . '" ,"' . $CHIAVEPUBBLICAZIONE . '", "' . $stripe_logo . '", "' . $preferences['ipn_url'] . '");';
        $html .= '}}';
        $html .= '</script>';


        return $html;

    }

    /**
     * Effettua il pagamento dell'ordine
     *
     * @param array $request
     *
     */
    public function payOrder($request) {

        $orders = new \MSFramework\Ecommerce\orders();
        $metodi_pagamento = (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamentoWeb();

        $order_id = $request['order_id'];
        $orderDetails = $orders->getOrderDetails($order_id)[$order_id];

        if(!$orderDetails) die(json_encode(array('status' => 'order_not_found', 'message' => 'Si è verificato un errore, prova a ricaricare la pagina ed a effettuare l\'ordine nuovamente.')));

        $CHIAVESEGRETA = $metodi_pagamento['stripe_data']['secret_key'];

        //if(SSL_ENABLED != 'https://') {
            \Stripe\Stripe::setVerifySslCerts(false);
        //}

        \Stripe\Stripe::setApiKey($CHIAVESEGRETA);

        $importo = ($orderDetails['cart']['coupon_price'] + ($orderDetails['tipo_spedizione']['prezzo'] > 0 ? $orderDetails['tipo_spedizione']['prezzo'] : 0))*100;

        $charge = \Stripe\Charge::create([
            'amount' => $importo,
            'currency' => 'EUR',
            'description' => 'Pagamento per Ordine #' . $order_id,
            'source' => $request['id'],
            'receipt_email' => $request['email'],
        ]);

        //retrieve charge details
        $chargeJson = $charge->jsonSerialize();

        //check whether the charge is successful
        if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){

            $array_to_save = array(
                "payment_log" => json_encode($request),
                "provider" => 'stripe'
            );

            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "insert");
            $this->MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_payments_logs ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            $payment_id = $this->MSFrameworkDatabase->lastInsertId();

            $this->MSFrameworkDatabase->query("UPDATE ecommerce_orders SET payment_id = " . $payment_id . ", payment_type = 'stripe' WHERE id = " . $order_id);

            $orders->updateOrderStatus($order_id, 1);
            $orders->updateProductsQtyOnStatusVariation($order_id);

            die(json_encode(array('status' => 'ok')));

        } else {
            die(json_encode(array('status' => 'transaction_error', 'message' => 'Si è verificato un errore durante il pagamento, se il problema persiste per favore contatta l\'assistenza')));
        }
    }

}
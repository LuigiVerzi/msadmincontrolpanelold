<?php
/**
 * MSAdminControlPanel
 * Date: 19/12/2018
 */

namespace MSFramework\Ecommerce\Payments;


class paypal {
    public function __construct() {
        Global $MSFrameworkDatabase;
        Global $MSFrameworki18n;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworki18n = $MSFrameworki18n;
    }

    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param array $orderDetails L'Array con le info relative all'ordine
     * @param array $preferences L'Array con le preferenze
     *
     * @return string La stringa con l'URL che porta al pagamento
     *
     */
    public function getPaymentButton($orderDetails, $preferences = array()) {

        Global $MSFrameworkCMS, $MSFrameworki18n, $MSFrameworkUrl;

        $default_preferences = array(
            'ipn_url' => $MSFrameworkCMS->getURLToSite() . 'ipn/paypal_ipn.php',
            'order_url' => $MSFrameworkCMS->getURLToSite() . $MSFrameworkUrl->getLangPrefix() . 'profilo/ordine/' . $orderDetails['id']
        );

        foreach($default_preferences as $def_k => $def_v) {
            if(!isset($preferences[$def_k])) {
                $preferences[$def_k] = $def_v;
            }
        }

        $siteCMSData = $MSFrameworkCMS->getCMSData('site');
        $site_logos = json_decode($siteCMSData['logos'], true);
        $loggedUser = (new \MSFramework\Ecommerce\customers())->getUserDataFromSession();

        $metodi_pagamento = (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamentoWeb();


        $html = '<form method="post" name="paypal_form" id="paypal_form" action="https://www' . (($MSFrameworkCMS->isStaging() || $MSFrameworkCMS->isDevelopment()) ? '.sandbox' : '') . '.paypal.com/cgi-bin/webscr">';
        $html .= '<input type="hidden" name="business" value="' . $metodi_pagamento['paypal_data']['email'] . '" />';
        $html .= '<input type="hidden" name="cmd" value="_cart" />';
        $html .= '<input type="hidden" name="upload" value="1">';
        
        $html .= '<input type="hidden" name="image_url" value="' . (isset($_SERVER['HTTPS']) ? "https" : "http") . ":" . UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo'] .'">';
        
        if($loggedUser) {
            $html .= '<input type="hidden" name="return" value="' . $preferences['order_url'] . '" />';
            $html .= '<input type="hidden" name="cancel_return" value="' . $preferences['order_url'] . (strpos($preferences['order_url'], '?') !== false ? '&' : '?') . 'annullato" />';
        } else {
            $html .= '<input type="hidden" name="return" value="' . (new \MSFramework\Ecommerce\orders())->getGuestOrderUrl($orderDetails['id']). '" />';
            $html .= '<input type="hidden" name="cancel_return" value="' . (new \MSFramework\Ecommerce\orders())->getGuestOrderUrl($orderDetails['id']). '&annullato" />';
        }
        $html .= '<input type="hidden" name="notify_url" value="' . $preferences['ipn_url'] . '" />';
        $html .= '<input type="hidden" name="rm" value="2" />';
        $html .= '<input type="hidden" name="currency_code" value="EUR" />';
        $html .= '<input type="hidden" name="lc" value="IT" />';
        $html .= '<input type="hidden" name="cbt" value="Vedi il tuo ordine su ' . SW_NAME. '" />';

        $html .= '<input type="hidden" name="shipping_1" value="' . ($orderDetails['tipo_spedizione']['prezzo'] > 0 ? $orderDetails['tipo_spedizione']['prezzo'] : '0.0'). '" />';
        $html .= '<input type="hidden" name="no_shipping" value="2" />';
        
        $i = 0;
        foreach($orderDetails['cart']['products'] as $product) {
            $i++;
            $html .= '<input type="hidden" name="item_name_' . $i . '" value="' . htmlspecialchars($product['nome']). '">';
            $html .= '<input type="hidden" name="amount_' . $i . '" value="' . (new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['prezzo']['tax']). '">';
            $html .= '<input type="hidden" name="quantity_' . $i . '" value="' . $product['quantity'] . '">';

            $v_i = 0;
        }
        foreach($product["formattedVariation"] as $v_k => $v_v) {
            $v_i++;
            $html .= '<input type="hidden" name="on' . $v_i. '_' . $i. '" value="' . htmlspecialchars(substr($this->MSFrameworki18n->getFieldValue($v_k), 0, 60)) . '">';
            $html .= '<input type="hidden" name="os' . $v_i. '_' . $i. '" value="' . htmlspecialchars(substr($this->MSFrameworki18n->getFieldValue($v_v), 0, 200)) . '">';
        } 
        
        if($product["custom"]) {
            $v_i++;
            $html .= '<input type="hidden" name="on' . $v_i. '_' . $i. '" value="' . htmlspecialchars(substr($this->MSFrameworki18n->gettext('Info Aggiuntive'), 0, 60)). '">';
            $html .= '<input type="hidden" name="os' . $v_i. '_' . $i. '" value="' . htmlspecialchars(substr($product['custom'], 0, 60)). '">';
        }

        $html .= '<input type="hidden" name="custom" value="' . $orderDetails['id'] . '" />';
        $html .= '<input type="hidden" name="discount_amount_cart" value="' . ($orderDetails['cart']['total_price']['tax'] - $orderDetails['cart']['coupon_price']) . '" />';

        $html .= '<input type="hidden" name="first_name" value="' . $orderDetails['info_spedizione']['nome'] . '"/>';
        $html .= '<input type="hidden" name="last_name" value="' . $orderDetails['info_spedizione']['cognome'] . '"/>';
        $html .= '<input type="hidden" name="address1" value="' . $orderDetails['info_spedizione']['indirizzo'] . '"/>';
        $html .= '<input type="hidden" name="address2" value="' . (!empty($orderDetails['info_spedizione']['indirizzo_2']) ? '<br>' . $orderDetails['info_spedizione']['indirizzo_2'] : ''). '"/>';
        $html .= '<input type="hidden" name="city" value="' . $orderDetails['info_spedizione']['comune'] . '"/>';
        $html .= '<input type="hidden" name="state" value="' . (new \MSFramework\geonames())->getCountryDetails($orderDetails['info_spedizione']['stato'])[$orderDetails['info_spedizione']['stato']]['name'] . '"/>';
        $html .= '<input type="hidden" name="zip" value="' . $orderDetails['info_spedizione']['cap'] . '"/>';
        $html .= '<input type="hidden" name="email" value="' . $loggedUser['username'] . '"/>';

        $html .= '<input type="image" src="' . ABSOLUTE_SW_PATH_HTML. 'assets/images/paypal_logo.gif" border="0" name="submit" alt="' . $this->MSFrameworki18n->gettext('Paga subito tramite PayPal'). '" />';

        $html .= '</form>';

        return $html;

    }
    /**
     * Restituisce l'URL per l'invio del pagamento tramite Nexi
     *
     * @param array $request
     *
     */
    public function checkIPN($request) {
        Global $MSFrameworkCMS;

        $orders = new \MSFramework\Ecommerce\orders();
        $ipn = new \PayPal\PaypalIPN();

        // Use the sandbox endpoint during testing.
        if($MSFrameworkCMS->isStaging() || $MSFrameworkCMS->isDevelopment()) {
            $ipn->useSandbox();
        }
        $verified = $ipn->verifyIPN();

        if ($verified) {

            $receiver_email = filter_var($request['receiver_email'], FILTER_SANITIZE_EMAIL);
            $order_id = filter_var($request['custom'], FILTER_SANITIZE_STRING);

            $metodi_pagamento = (new \MSFramework\Fatturazione\vendite())->ottieniMetodiPagamentoWeb();

            if($receiver_email == $metodi_pagamento['paypal_data']['email']) {

                $array_to_save = array(
                    "payment_log" => json_encode($request),
                    "provider" => 'paypal'
                );
                $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "insert");
                $this->MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_payments_logs ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
                $payment_id = $this->MSFrameworkDatabase->lastInsertId();

                $this->MSFrameworkDatabase->query("UPDATE ecommerce_orders SET payment_id = " . $payment_id . ", payment_type = 'paypal' WHERE id = " . $order_id);

                $orders->updateOrderStatus($order_id, 1);
                $orders->updateProductsQtyOnStatusVariation($order_id);

            }
        }

        header("HTTP/1.1 200 OK");
    }
}
<?php
/**
 * MSFramework
 * Date: 30/04/18
 */

namespace MSFramework\Ecommerce;


class shipping {
    public function __construct() {
        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce un array associativo (ID Spedizione => Dati Spedizione) con i dati relativi alla tipologia di spedizione
     *
     * @param string $id L'ID della tipologia di spedizione (stringa) o delle tipologie di spedizioni (array) richieste (se vuoto, vengono recuperati i dati di tutte le tipologie di spedizione)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getShippingDetails($id = "", $fields = "*") {
        Global $MSFrameworkCurrencies;

        if ($id != "") {
            if (!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }
        if(!isset($same_db_string_ary) || !is_array($same_db_string_ary))
        {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if ($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $ary_to_return = array();
        $shipping = $this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM ecommerce_shipping_methods WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]);

        foreach ($shipping as $r) {
            if($r['prezzo'] > 0) $r['prezzo'] = $MSFrameworkCurrencies->convertValue($r['prezzo']);
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente alla disponibilità della spedizione (per i paesi)
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getCountryAvailType($key = "") {
        $ary = array(
            "0" => "Tutto il mondo",
            "1" => "Solo paesi selezionati",
            "2" => "Tutto il mondo tranne i paesi selezionati",
        );

        if($key != "") {
            return $ary[$key];
        } else {
            return $ary;
        }
    }

    /**
     * Restituisce un array con gli ID di tutti gli stati disponibili per la spedizione
     *
     * @param array $carrello Il carrello attuale, in modo da poter calcoare anche le spedizioni che richiedono particolari condizioni
     * @param string $fields I campi da ottenere
     *
     * @return array|mixed
     */
    public function getAllAvailState($carrello = array(), $fields = 'geonameId, name') {
        $all_state = (new \MSFramework\geonames)->getCountryDetails('', $fields);
        $shipping_methods = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_shipping_methods");

        $avaialable_state = array();

        foreach($shipping_methods as $s_m) {

            if($s_m['min_expense'] == '' || !$carrello || $s_m['min_expense'] < $carrello['total_price']['tax']) {
                if ($s_m['ships_to'] == 0) {
                    foreach($all_state as $state) {
                        $avaialable_state[$state['geonameId']] = $state['geonameId'];
                    }
                }
                elseif ($s_m['ships_to'] == 1) {
                    $sel_countries = json_decode($s_m['sel_countries'], true);
                    if($sel_countries) {
                        foreach($sel_countries as $id) {
                            $avaialable_state[$id] = $id;
                        }
                    }
                }
                elseif ($s_m['ships_to'] == 2) {
                    $no_sel_countries = json_decode($s_m['sel_countries'], true);
                    if($no_sel_countries) {
                        foreach($no_sel_countries as $exclude_id) {
                            foreach($all_state as $state) {
                                if($exclude_id != $state['geonameId']) $avaialable_state[$state['geonameId']] = $state['geonameId'];
                            }
                        }
                    }
                }
            }
        }

        return $avaialable_state;

    }

    /**
     * Restituisce un array con la lista dei metodi di spedizione disponibili
     *
     * @param array $stato Lo stato nel quale si desidera spedire l'ordine
     * @param string $spesa La spesa che si sta effettuando
     *
     * @return array|mixed
     */
    public function getAllAvailMethods($stato, $spesa = 0.0) {
        $shipping_methods = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_shipping_methods");

        $avaialable_methods = array();

        foreach($shipping_methods as $s_m) {

            if( ($s_m['min_expense'] <= $spesa || $s_m['min_expense'] == '') && ($s_m['max_expense'] >= $spesa || $s_m['max_expense'] == '') )
            {
                Global $MSFrameworkCurrencies;

                if($s_m['prezzo'] > 0) $s_m['prezzo'] = $MSFrameworkCurrencies->convertValue($s_m['prezzo']);

                if($s_m['ships_to'] == 0) {
                    $avaialable_methods[] = $s_m;
                }
                elseif($s_m['ships_to'] == 1)
                {
                    $sel_countries = json_decode($s_m['sel_countries'], true);
                    if(in_array($stato, $sel_countries)) {
                        $avaialable_methods[] = $s_m;
                    }
                }
                elseif($s_m['ships_to'] == 2)
                {
                    $sel_countries = json_decode($s_m['sel_countries'], true);
                    if(!in_array($stato, $sel_countries)) {
                        $avaialable_methods[] = $s_m;
                    }
                }
            }
        }

        return $avaialable_methods;

    }

    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente ai corrieri utilizzabili per le spedizioni
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getCouriers($key = "") {
        $ary = array(
            //0 è riservato ad "Altro - Specificare"
            "1" => "DHL",
            "2" => "GLS",
            "3" => "BRT",
            "4" => "Poste Italiane (Pacco Celere)",
            "5" => "Poste Italiane (Raccomandata)",
            "6" => "TNT",
            "7" => "UPS",
        );

        if($key != "") {
            return $ary[$key];
        } else {
            asort($ary);
            return $ary;
        }
    }
}
<?php
/**
 * MSFramework
 * Date: 17/04/18
 */

namespace MSFramework\Ecommerce;


class reviews {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Ottiene il resoconto delle recensioni di un prodotto
     *
     * @param int $product_id L'id del prodotto
     * @param int $user_id L'id del cliente
     * @param int $rating Il voto da 1 a 5
     * @param string $comment_body Il commento testuale (opzionale)
     * @param array $product_variations Le variazioni del prodotto votato
     * @param mixed $status Lo stato della recensione (Se da approvare o no)
     *
     * @return integer
     */
    public function addReviewToProduct($product_id, $user_id, $rating, $comment_body = '', $product_variations = array(), $status = 'default') {

        if($status == 'default') {
            $prodotti_settings = (new \MSFramework\cms())->getCMSData('ecommerce_prodotti');
            $product_info = (new \MSFramework\Ecommerce\products())->getProductDetails($product_id)[$product_id];

            if((int)$prodotti_settings['moderate_reviews'] == 1) $status = 0;
            else $status = 1;

            if((int)$product_info['moderate_reviews'] == 1) $status = 0;
            else if((int)$product_info['moderate_reviews'] == 2) $status = 1;

        }

        $array_to_save = array(
            "product_id" => $product_id,
            "user_id" => $user_id,
            "rating" => (int)$rating,
            "comment_body" => $comment_body,
            "product_variations" => json_encode($product_variations),
            "status" => (int)$status
        );

        $check_if_exists = $this->getUserReviewForProductID($user_id, $product_id, $product_variations);

        if(!$check_if_exists) {

            $array_to_save['comment_date'] = date("Y-m-d H:i:s");

            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "insert");

            if ($this->MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_product_reviews ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0])) {
                return $this->MSFrameworkDatabase->lastInsertId();
            } else {
                return false;
            }
        }
        else
        {
            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, "update");
            return $this->MSFrameworkDatabase->pushToDB("UPDATE ecommerce_product_reviews SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $check_if_exists['id']), $stringForDB[0]));
        }

    }

    /**
     * Ottiene il resoconto delle recensioni di un prodotto
     *
     * @param int $product_id I campi da prelevare dal DB
     *
     * @return array
     */
    public function getReviewsSummaryByProductID($product_id, $page = 1) {

        if(!$product_id) {
            $product_id = 0;
        }

        $ary_to_return = array('rating_count' => array());
        for ($i = 5; $i >= 1; $i--) $ary_to_return['rating_count'][$i] = 0;

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT COUNT(*) as n_of_review, rating FROM `ecommerce_product_reviews` WHERE product_id = $product_id AND status = 1 GROUP BY rating") as $r) {
            $ary_to_return['rating_count'][$r['rating']] = (int)$r['n_of_review'];
        }

        // Ottengo la media dei voti
        $ary_to_return['average'] = $this->MSFrameworkDatabase->getAssoc("SELECT AVG(rating) as avg FROM `ecommerce_product_reviews` WHERE  product_id = $product_id AND status = 1", array(), true)['avg'];
        $ary_to_return['total_review'] = $this->MSFrameworkDatabase->getAssoc("SELECT COUNT(*) as n_review FROM `ecommerce_product_reviews` WHERE  product_id = $product_id AND status = 1", array(), true)['n_review'];

        return $ary_to_return;
    }

    /**
     * Ottiene le recensioni scritte da un utente per un determinato prodotto
     *
     * @param int $user_id L'utente che ha scritto la recensione
     * @param int $product_id I campi da prelevare dal DB
     * @param int $formatted_variations Le variazioni del prodotto
     *
     * @return array
     */
    public function getUserReviewForProductID($user_id, $product_id, $formatted_variations = array()) {

        if(!$product_id) {
            $product_id = 0;
        }

        $formatted_variations = json_encode($formatted_variations);

        $user_reviews = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_reviews WHERE product_id = $product_id AND comment_body != '' AND product_variations = :variations AND user_id = $user_id", array(':variations' => $formatted_variations)) as $r) {
            $user_reviews = $this->formatReviewArray($r);
        }
        return $user_reviews;

    }

    /**
     * Ottiene tutte le recensioni testuali per un determinato prodotto
     *
     * @param int $product_id L'id del prodotto
     * @param int $page La pagina delle recensioni da mostrare
     *
     * @return array
     */
    public function getReviewsByProductID($product_id, $page = 1) {

        if(!$product_id) {
            $product_id = 0;
        }

        /* IMPOSTA L'OFFSET IN BASE ALLA PAGINA DA MOSTRARE */
        $limit = 12;
        $start = ($page * $limit) - $limit;
        if($start < 0) $start = 0;

        $total_reviews = $this->countReviews($product_id, true);

        $ary_to_return = array('page' => $page, 'showing_per_page' => $limit, 'total_reviews' => $total_reviews, 'reviews' => array());

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM ecommerce_product_reviews WHERE product_id = $product_id AND comment_body != '' AND status = 1  LIMIT $start, $limit") as $r) {
            $ary_to_return['reviews'][$r['id']] = $this->formatReviewArray($r);
        }
        return $ary_to_return;
    }

    /**
     * Conta le recensioni testuali per un determinato prodotto
     *
     * @param int $product_id L'id del prodotto da esaminare
     * @param int $only_complete Stabilisce se contare solo le recensioni che contengono testo
     *
     * @return integer
     */
    public function countReviews($product_id, $only_complete = false) {
        return $this->MSFrameworkDatabase->getCount("SELECT * FROM ecommerce_product_reviews WHERE status = 1 AND product_id = " . $product_id . ($only_complete ? " AND comment_body != ''" : ''));
    }

    /**
     * Formatta l'array delle recensioni
     *
     * @param array $recensione I campi da formattare
     *
     * @return array
     */
    public function formatReviewArray($recensione) {

        if($recensione['user_id'] == 0)
        {
            $recensione['user_info'] = json_decode($recensione['fake_customer'], true);
        }
        else {
            $recensione['user_info'] = (new \MSFramework\Ecommerce\customers())->getUserDataFromDB($recensione['user_id'], 'id, nome, cognome, email');
        }
        $recensione['rating'] = intval($recensione['rating']);
        $recensione['comment_date'] = (new \MSFramework\utils())->smartdate(strtotime($recensione['comment_date']));
        $recensione['product_variations'] = json_decode($recensione['product_variations'], true);

        return $recensione;

    }

    /**
     * Restituisce la struttura della paginazione in HTML
     *
     * @param int $total_review Il numero di prodotti totali ottenuti
     * @param int $current_page La pagina che si sta mostrando attualmente
     * @param int $show_per_page Il numero di prodotti che vengono mostrati per pagina
     *
     * @return string
     */
    public function composeHTMLPagination($total_review, $current_page = 1, $show_per_page = 12)
    {
        $links = 10;

        $last = ceil($total_review / $show_per_page);

        $start = (($current_page - $links) > 0) ? $current_page - $links : 1;
        $end = (($current_page + $links) < $last) ? $current_page + $links : $last;

        $html = '<div class="pagination">';

        $class = ($current_page == 1) ? "disabled" : "";
        $html .= '<a class="' . $class . '" href="?rp=' . ($current_page - 1) . '">&laquo;</a>';

        if ($start > 1) {
            $html .= '<a href="?rp=1">1</a>';
            $html .= '<a class="disabled"><span>...</span></a>';
        }

        for ($i = $start; $i <= $end; $i++) {
            $class = ($current_page == $i) ? "active" : "";
            $html .= '<a class="' . $class . '" href="?rp=' . $i . '">' . $i . '</a>';
        }

        if ($end < $last) {
            $html .= '<a class="disabled"><span>...</span></a>';
            $html .= '<a href="?rp=' . $last . '">' . $last . '</a>';
        }

        $class = ($current_page == $last) ? "disabled" : "";
        $html .= '<a class="' . $class . '" href="?rp=' . ($current_page + 1) . '">&raquo;</a>';

        $html .= '</div>';

        return $html;

    }

}
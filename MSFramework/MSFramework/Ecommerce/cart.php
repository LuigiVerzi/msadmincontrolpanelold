<?php
/**
 * MSFramework
 * Date: 21/04/18
 */

namespace MSFramework\Ecommerce;


class cart {

    public $customerID = false;
    public $isAdminManaged = false;

    public function __construct() {
        global $firephp;

        if(!$this->customerID) {
            $this->customerID = (new \MSFramework\Ecommerce\customers())->getUserDataFromSession()['user_id'];
        }

        $this->couponClass = new \MSFramework\Ecommerce\coupon();
        $this->couponClass->customerID = $this->customerID;

        Global $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Ritorna i prodotti attualmente dentro il carrello
     *
     * @param $default array Il carrello predefinito
     * @param mixed $coupons la lista di coupon da applicare (lasciare vuoto per prenderli dalla sessione utente)
     *
     * @return array
     */
    public function getCart($default = array(), $coupons = false) {
        Global $MSHooks;

        $return_array = array(
            'products' => array(),
            'count' => 0
        );

        if(!is_array($coupons)) {
            $coupons = (isset($_SESSION["cart_coupon"]) && is_array($_SESSION['cart_coupon']) ? $_SESSION["cart_coupon"] : array());
        }

        if(isset($_SESSION["cart"]) && is_array($_SESSION["cart"])) $return_array['products'] = $_SESSION["cart"];
        else $return_array['products'] = $default;

        /* Pulisce e formatta correttamente la scritta */
        $products_clean = $this->cleanCartProducts($return_array['products']);

        /* Allega i coupon attivi */
        $return_array['coupon'] = array();
        foreach ($coupons as $key => $code) {
            // Se il coupon viene passato sotto forma di array completo (ad esempio  lo applico direttamente
            if(isset($code['coupon_id'])) {

                $return_array['coupon'][$key] = $code;
            } else {
                $coupon = $this->couponClass->getCoupon($code);
                $coupon_check = $this->couponClass->applyToCart($coupon, $products_clean);
                if ($coupon_check) $return_array['coupon'][$code] = $coupon_check;
                else unset($_SESSION["cart_coupon"][$code]);
            }
        }

        $return_array['products'] = $products_clean['products'];
        $return_array['messages'] = $products_clean['messages'];

        $return_array['count'] = count($return_array['products']);

        // Crea il prezzo totale dei prodotti
        $return_array['total_price'] = array(
            'no_tax' => 0,
            'tax' => 0
        );

        foreach($return_array['products'] as $k=>$product) {
            $return_array['total_price']['no_tax'] += (floatval($product['prezzo']['no_tax'][0] . '.' . $product['prezzo']['no_tax'][1]) * intval($product['quantity']));
            $return_array['total_price']['tax'] += (floatval($product['prezzo']['tax'][0] . '.' . $product['prezzo']['tax'][1]) * intval($product['quantity']));
        }

        // Crea il prezzo totale rimuovendo lo sconto di eventuali coupon
        $return_array['coupon_price'] = $return_array['total_price']['tax'];

        foreach($return_array['coupon'] as $k=>$coupon) {
            $return_array['coupon_price'] -= $coupon['sale_value'];
        }

        $return_array = $MSHooks->filter('ecommerce', 'getCart')->run($return_array);

        return $return_array;
    }

    /**
     * Sincronizza il carrello dell'utente con la sessione attuale e con in DB (Se loggato)
     *
     * @param array L'array con i prodotti attualmente nel carrello
     *
     * @return boolean
     */
    public function syncCart($my_cart) {
        if(is_array($my_cart) && !isset($my_cart['products'])) {
            $_SESSION["cart"] = $my_cart;
            if ((new \MSFramework\Ecommerce\customers())->getUserDataFromSession()) {
                (new \MSFramework\Ecommerce\customers())->saveInfo('carrello', $my_cart);
            }
        } else {
            $_SESSION["cart"] = array();
        }
        return true;
    }

    /**
     *  Aggiunge un prodotto al carrello
     *
     * @param array $product_id L'id del prodotto da aggiungere al carrello
     * @param integer $quantity La quantità del prodotto
     * @param array $variations L'array con le variazioni scelte (vuoto se non presenti)
     *
     * @return array
     */
    public function addToCart($product_id, $quantity, $variations = array(), $custom = '') {
        Global $MSFrameworki18n;

        $unique_chose_id = $product_id;
        $product_info = (new \MSFramework\Ecommerce\products())->getProductDetails($product_id)[$product_id];
        $variation_info = (new \MSFramework\Ecommerce\products())->getProductVariationInfo($product_id, $variations);

        $product_code = $product_info['product_code'];

        if((int)$product_info['enable_custom_message'] && (int)$product_info['custom_message']['required'] && empty($custom)) {
            return array('status' => 'custom_required');
        }

        $formatted_variations = array();
        if($variations) {
            foreach($variations as $v) {
                $attribute_key =  (new \MSFramework\Ecommerce\attributes())->getAttributeDetails($v['id'])[$v['id']];
                $attribute_value_detail =  (new \MSFramework\Ecommerce\attributes())->getValuesOfAttributeDetails($v['id'], $v['value'])[$v['value']];
                $formatted_variations[$attribute_key['nome']] =  $attribute_value_detail['nome'];
                $unique_chose_id .= '_' . $v['id'] . '_' . $v['value'];
            }
        }

        $category_info = (new \MSFramework\Ecommerce\categories())->getCategoryDetails($product_info['category'])[$product_info['category']];

        $product_for_cart = array(
            'product_id' => $product_info['id'],
            'product_code' => $product_code,
            'ean' => $product_info['ean'],
            'unique_chose_id' => $unique_chose_id,
            'cart_unique_id' => $unique_chose_id . time(),
            'nome' => $MSFrameworki18n->getFieldValue($product_info['nome']),
            'slug' => $MSFrameworki18n->getFieldValue($product_info['slug']),
            'category' => $product_info['category'],
            'category_name' => $MSFrameworki18n->getFieldValue($category_info['nome']),
            'image' => $variation_info["gallery"][0],
            'quantity' => intval($quantity),
            'variations' => $variations,
            'custom' => $custom,
            'imposta' => $product_info['imposta'],
            'prezzo' => array(
                'no_tax' => ($variation_info['prezzo_promo']['no_tax'] ? $variation_info['prezzo_promo']['no_tax'] : $variation_info['prezzo']['no_tax']),
                'tax' => ($variation_info['prezzo_promo']['tax'] ? $variation_info['prezzo_promo']['tax'] : $variation_info['prezzo']['tax']),
                'cart_price' => ($variation_info['prezzo_promo']['cart_price'] ? $variation_info['prezzo_promo']['cart_price'] : $variation_info['prezzo']['cart_price'])
            ),
            'cross_sell' => $product_info['cross_sell'],
            'formattedVariation' => $formatted_variations
        );

        $my_cart = $this->getCart()['products'];
        $new_cart = $my_cart;

        $already_exist = null;

        foreach($my_cart as $k=>$single_cart_product) {
            if($product_for_cart['unique_chose_id'] == $single_cart_product['unique_chose_id']) {
                if($product_for_cart['custom'] == $single_cart_product['custom']) $already_exist = $k;
            }
        }

        if(is_null($already_exist)) {

            if(intval($variation_info['warehouse_qty']) >= intval($product_for_cart['quantity'])) {
                $new_cart[] = $product_for_cart;
                $status = 'insert';
            }
            else
            {
                $status = 'no_avaialable';
            }
        }
        else {
            if(intval($variation_info['warehouse_qty']) >= (intval($new_cart[$already_exist]['quantity']) + intval($product_for_cart['quantity'])))
            {
                $status = 'updated';
                $new_cart[$already_exist]['quantity'] = intval($new_cart[$already_exist]['quantity']) + intval($product_for_cart['quantity']);
            }
            else
            {
                $status = 'no_avaialable';
            }
        }

        $this->syncCart($new_cart);

        $return_array = array(
            'status' => $status,
            'products' => $new_cart
        );

        return $return_array;

    }

    /**
     *  Aggiunge un prodotto al carrello
     *
     * @param array $product_id L'id del prodotto da aggiungere al carrello
     * @param string $product_name Il nome del prodotto
     * @param float Il prezzo del prodotto
     * @param integer $quantity La quantità del prodotto
     * @param mixed $vat Le imposte sul prodotto
     * @return array
     */
    public function customAddToCart($product_id, $product_name, $price, $quantity = 1, $vat = 'default') {
        Global $MSFrameworki18n;

        if($vat === 'default') {
            $vat = (new \MSFramework\Fatturazione\imposte())->getDefaultVat();
        }

        $product_for_cart = array(
            'product_id' => $product_id,
            'unique_chose_id' => $product_id,
            'cart_unique_id' => $product_id,
            'is_custom' => true,
            'nome' => $product_name,
            'slug' => '',
            'category' => '',
            'image' => '',
            'quantity' => $quantity,
            'variations' => array(),
            'custom' => '',
            'imposta' => (float)$vat,
            'prezzo' => (is_array($price) ? $price : (new \MSFramework\Ecommerce\imposte())->createTaxPricesArray($price, (float)$vat))
        );

        $my_cart = $this->getCart()['products'];
        $new_cart = $my_cart;

        $already_exist = null;

        foreach($my_cart as $k=>$single_cart_product) {
            if($product_for_cart['unique_chose_id'] == $single_cart_product['unique_chose_id']) {
                $already_exist = $k;
            }
        }

        if(is_null($already_exist)) {
            $new_cart[] = $product_for_cart;
            $status = 'insert';
        }

        $this->syncCart($new_cart);

        $return_array = array(
            'status' => $status,
            'products' => $new_cart
        );

        return $return_array;

    }

    /**
     * Pulisce l'array dei prodotti nel arrello sincronizzando le quantità con il magazzino attuale
     *
     * @param array L'array con i prodotti attualmente nel carrello
     *
     * @return array
     */
    public function cleanCartProducts($my_cart) {

        $return_messages = array();

        foreach($my_cart as $k=>$single) {

            if(!isset($single['product_id']) || empty($single['product_id'])) {
                unset($my_cart[$k]);
                continue;
            }

            /* CONTROLLA SE LA QUANTITà NON è PIù DISPONIBILE */
            if(!isset($single['is_custom']) && !$this->isAdminManaged) {
                $my_cart[$k]['currentVariationInfo'] = (new \MSFramework\Ecommerce\products())->getProductVariationInfo($single['product_id'], $single['variations']);

                if (!$my_cart[$k]['currentVariationInfo'] || $my_cart[$k]['currentVariationInfo']['warehouse_qty'] < $single['quantity']) {
                    if (!$my_cart[$k]['currentVariationInfo'] || $my_cart[$k]['currentVariationInfo']['warehouse_qty'] == 0) {
                        unset($my_cart[$k]);
                        $return_messages['messages'][] = array(
                            'type' => 'warning',
                            'title' => $single['nome'],
                            'message' => (new \MSFramework\i18n())->gettext('Il prodotto non è più disponibile ed è stato quindi rimosso dal carrello')
                        );
                    } else {
                        $my_cart[$k]['quantity'] = $my_cart[$k]['currentVariationInfo']['warehouse_qty'];
                        $return_messages[] = array(
                            'type' => 'warning',
                            'title' => $single['nome'],
                            'message' => (new \MSFramework\i18n())->gettext('La quantità del prodotto è stata diminuita perchè non più presente in magazzino.')
                        );
                    }
                }

                /* CONTROLLA SE IL PREZZO è CAMBIATO */
                if (isset($my_cart[$k])) {

                    $current_cart_price = ($my_cart[$k]['currentVariationInfo']['prezzo_promo']['cart_price'] ? $my_cart[$k]['currentVariationInfo']['prezzo_promo']['cart_price'] : $my_cart[$k]['currentVariationInfo']['prezzo']['cart_price']);
                    $current_tax_price = ($my_cart[$k]['currentVariationInfo']['prezzo_promo']['tax'] ? $my_cart[$k]['currentVariationInfo']['prezzo_promo']['tax'] : $my_cart[$k]['currentVariationInfo']['prezzo']['tax']);
                    $current_no_tax_price = ($my_cart[$k]['currentVariationInfo']['prezzo_promo']['no_tax'] ? $my_cart[$k]['currentVariationInfo']['prezzo_promo']['no_tax'] : $my_cart[$k]['currentVariationInfo']['prezzo']['no_tax']);

                    if (!empty($my_cart[$k]['currentVariationInfo']['product_code'])) {
                        $my_cart[$k]['product_code'] = $my_cart[$k]['currentVariationInfo']['product_code'];
                    }

                    if ($current_cart_price != $my_cart[$k]['prezzo']['cart_price'] || $my_cart[$k]['imposta'] != $my_cart[$k]['currentVariationInfo']['imposta']) {
                        $my_cart[$k]['imposta'] = $my_cart[$k]['currentVariationInfo']['imposta'];

                        $my_cart[$k]['prezzo']['cart_price'] = $current_cart_price;
                        $my_cart[$k]['prezzo']['tax'] = $current_tax_price;
                        $my_cart[$k]['prezzo']['no_tax'] = $current_no_tax_price;
                        $return_messages[] = array(
                            'type' => 'warning',
                            'title' => $single['nome'],
                            'message' => (new \MSFramework\i18n())->gettext('Il prezzo del prodotto è cambiato dall\'ultima volta ed è stato aggiornato.')
                        );
                    }
                }
            } else if($this->isAdminManaged) {
                $my_cart[$k]['currentVariationInfo'] = $single;
            }
        }

        return array(
            'products' => $my_cart,
            'messages' => $return_messages
        );
    }

    /**
     * Ritorna un array con tutte le imposte applicate
     *
     * @param array L'array con i prodotti attualmente nel carrello
     *
     * @return array
     */
    public function ottieniImposteApplicati($prodotti) {
        $data = (new \MSFramework\Ecommerce\imposte())->getImposte();

        $return_array = array();

        foreach($prodotti as $prodotto) {
            $imposta = $prodotto['imposta'];

            if($imposta === 'default') {
                $imposta = (new \MSFramework\Fatturazione\imposte())->getDefaultVat();
            }

            if($imposta > 0) {
                if (!isset($return_array[$imposta])) {
                    $return_array[$imposta] = array('nome' => $data[$imposta][0], 'value' => 0);
                }

                $return_array[$imposta]['value'] += ((new \MSFramework\Ecommerce\products())->priceArrayToNumeric($prodotto["currentVariationInfo"]['prezzo']['tax']) - (new \MSFramework\Ecommerce\products())->priceArrayToNumeric($prodotto["currentVariationInfo"]['prezzo']['no_tax'])) * $prodotto['quantity'];
            }

        }

        return $return_array;
    }
}
<?php
/**
 * MSAdminControlPanel
 * Date: 10/03/18
 */

namespace MSFramework\Ecommerce;

use MSFramework\cms;

class emails extends \MSFramework\emails {
    /**
     * Ottiene la lista di template per l'ecommerce
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @return array
     */

    public function getExtraTemplates()
    {

        $order_status_params = array(
            'prepareParams' => function ($params, $template_settings) {
                if ($params['order']['info_spedizione']['usa_fatturazione'] == 1) {
                    $params['order']['info_spedizione'] = $params['order']['info_fatturazione'];
                }
                $params['order']['info_fatturazione']['stato'] = (new \MSFramework\geonames())->getCountryDetails($params['order']['info_fatturazione']['stato'])[$params['order']['info_fatturazione']['stato']]['name'];
                $params['order']['info_spedizione']['stato'] = (new \MSFramework\geonames())->getCountryDetails($params['order']['info_spedizione']['stato'])[$params['order']['info_spedizione']['stato']]['name'];
                return $params;
            },
            'shortcodes' => array(
                array(
                    "{nome}" => 'Il nome del cliente',
                    "{email}" => 'L\'email del cliente',
                    "{prezzo_spedizione}" => "Il prezzo della spedizione",
                    "{prezzo_totale}" => "Il prezzo totale",
                    "{tabella_prodotti}" => "La tabella con la lista dei prodotti",
                    "{dati_fatturazione}" => "I dati di fatturazione del cliente",
                    "{dati_spedizione}" => "I dati di spedizione del cliente",
                    "{order_url}" => 'L\'URL del pannello relativo all\'ordine',
                    "{order_id}" => 'L\'ID dell\'ordine',
                ),
                array(
                    function ($params, $template_settings) {
                        return $params['user']['display_name'];
                    },
                    function ($params, $template_settings) {
                        return $params['user']['email'];
                    },
                    function ($params, $template_settings) {
                        return number_format(($params['order']['tipo_spedizione']['prezzo'] > 0 ? $params['order']['tipo_spedizione']['prezzo'] : 0), 2, ',', '.') . CURRENCY_SYMBOL;
                    },
                    function ($params, $template_settings) {
                        return number_format($params['order']['cart']['coupon_price'] + ($params['order']['tipo_spedizione']['prezzo'] > 0 ? $params['order']['tipo_spedizione']['prezzo'] : 0), 2, ',', '.') . CURRENCY_SYMBOL;
                    },
                    function ($params, $template_settings) {
                        $products_rows_template = '<tr><td style="text-align: left;"><small><b>{quantity}*</b></small> {product_name}{product_code}</td><td style="text-align: right;">{product_price}</td></tr>';
                        $products_row_html = '';
                        foreach ($params['order']['cart']['products'] as $product) {
                            $products_row_html .= str_replace(
                                array(
                                    "{quantity}",
                                    "{product_name}",
                                    "{product_price}",
                                    "{product_code}",
                                ),
                                array(
                                    $product['quantity'],
                                    $product['nome'],
                                    number_format((new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['prezzo']['tax']) * $product['quantity'], 2, ',', '.') . CURRENCY_SYMBOL,
                                    (!empty($product['product_code']) ? ' <small>(' . $product['product_code'] . ')</small>' : '')
                                ),
                                $products_rows_template
                            );
                        }

                        return '<table style="width: 100%;">' . $products_row_html . '</table>';

                    },
                    function ($params, $template_settings) {
                        $info_fatturazione = $params['order']['info_fatturazione'];
                        $use_label = (isset($template_settings['visualizzazione_label_info_utente']) ? (int)$template_settings['visualizzazione_label_info_utente'] : 0);
                        return (new \MSFramework\Ecommerce\orders())->formatCustomerDetails($info_fatturazione, 'billing', $use_label);
                        
                    },
                    function ($params, $template_settings) {
                        $info_spedizione = $params['order']['info_spedizione'];
                        $use_label = (isset($template_settings['visualizzazione_label_info_utente']) ? (int)$template_settings['visualizzazione_label_info_utente'] : 0);
                        return (new \MSFramework\Ecommerce\orders())->formatCustomerDetails($info_spedizione, 'shipping', $use_label);
                        
                    },
                    function ($params, $template_settings) {

                        if($params['to'] == 'admin')
                        {
                            return $this->MSFrameworkCMS->getURLToSite(1) . 'modules/ecommerce/ordini/edit.php?id=' . $params['order']['id'];
                        }

                        if(!empty($params['order']['guest_email'])) {
                            return (new \MSFramework\Ecommerce\orders())->getGuestOrderUrl($params['order']['id']);
                        } else {
                            return $this->MSFrameworkCMS->getURLToSite() . 'redirect_to_order/' . $params['order']['id'];
                        }
                    },
                    function ($params, $template_settings) {
                        return $params['order']['id'];
                    }
                )
            )
        );
        $order_info_settings = array(
            'visualizzazione_label_info_utente' => array(
                'label' => 'Visualizzazione info utente (Spedizione & Fatturazione)',
                'type' => 'select',
                'values' => array(
                    '0' => 'Visualizza direttamente i dati',
                    '1' => 'Visualizza i label prima dei dati'
                ),
                'default' => 0
            )
        );

        $default_address = $this->MSFrameworkCMS->getCMSData("settings")['destination_address'];

        $templates = array(
            'ecommerce' => array(
                /* TEMPLATE DOMANDE PRODOTTI */
                'products-questions/admin_new-question' => array(
                    'nome' => 'Nuova Domanda Ricevuta [ADMIN]',
                    'reply_to' => array(),
                    'address' => $default_address,
                    'subject' => "Hai ricevuto una nuova domanda - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{subject}" => "L'oggetto della mail",
                            "{product_name}" => "Il nome del prodotto in questione",
                            "{domanda}" => "Il testo della domanda"
                        ),
                        array(
                            "{[nome]}",
                            "{[subject]}",
                            "{[nome_prodotto]}",
                            "{[domanda]}"
                        )
                    )
                ),
                'products-questions/customer_new-answer' => array(
                    'nome' => 'Risposta Ricevuta [CLIENTE]',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Abbiamo risposto alla tua domanda - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{subject}" => "L'oggetto della mail",
                            "{product_name}" => "Il nome del prodotto in questione",
                            "{product_url}" => "Il link del prodotto"
                        ),
                        array(
                            "{[nome]}",
                            "{[subject]}",
                            "{[nome_prodotto]}",
                            "{site-url}{[slug_prodotto]}"
                        )
                    )
                ),
                /* TEMPLATE STATO ORDINI */
                'order-status/0' => array(
                    'nome' => '[STATO ORDINE] In Attesa di Pagamento',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "In Attesa di Pagamento - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes'],
                    'settings' => $order_info_settings
                ),
                'order-status/1' => array(
                    'nome' => '[STATO ORDINE] In Elaborazione',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Ordine in Elaborazione - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes'],
                    'settings' => $order_info_settings
                ),
                'order-status/2' => array(
                    'nome' => '[STATO ORDINE] Spedito',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Ordine Spedito - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes'],
                    'settings' => $order_info_settings
                ),
                'order-status/3' => array(
                    'nome' => '[STATO ORDINE] Consegnato',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Ordine Consegnato - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes'],
                    'settings' => $order_info_settings
                ),
                'order-status/3/review' => array(
                    'nome' => '[STATO ORDINE] Consegnato + Invito Recensione',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Ordine Consegnato - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes'],
                    'settings' => $order_info_settings
                ),
                'order-status/4' => array(
                    'nome' => '[STATO ORDINE] Annullato',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Ordine Annullato - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes'],
                    'settings' => $order_info_settings
                ),
                /* TEMPLATE STATO ORDINE ADMIN */
                'order-status/admin/0' => array(
                    'nome' => '[STATO ORDINE][ADMIN] Nuovo Ordine (In Attesa di Pagamento)',
                    'reply_to' => array(),
                    'address' => $default_address,
                    'subject' => "Nuovo Ordine Ricevuto - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes'],
                    'settings' => $order_info_settings
                ),
                'order-status/admin/1' => array(
                    'nome' => '[STATO ORDINE][ADMIN] Nuovo Ordine (Pagato)',
                    'reply_to' => array(),
                    'address' => $default_address,
                    'subject' => "Nuovo Pagamento Ricevuto - {site-name}",
                    'prepareParams' => $order_status_params['prepareParams'],
                    'shortcodes' => $order_status_params['shortcodes'],
                    'settings' => $order_info_settings
                ),
                /* FATTURE E RICEVUTE */
                'order-invoice' => array(
                    'nome' => 'Fattura Cliente',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Fattura ordine #{order_id} - {site-name}",
                    'beforeSend' => function($params, $template_settings) {
                        $invoice_settings = $this->MSFrameworkCMS->getCMSData('fatturazione_fatture');
                        if ($invoice_settings['attachment_format'] == 1) {
                            $this->mail->AddStringAttachment($params['invoice_source'], $params['file_name'] . ".htm", "base64", "text/html");
                        }
                        else
                        {
                            $pdf = new \TCPDF\TCPDF();

                            $pdf->SetCreator('MarketingStudio');
                            $pdf->SetAuthor(SW_NAME);
                            $pdf->SetTitle('Fattura Ordine');
                            $pdf->SetSubject($params['subject']);
                            $pdf->setPrintHeader(false);
                            $pdf->setPrintFooter(false);
                            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                            $pdf->setImageScale(1.5);
                            $pdf->SetFont('dejavusans', '', 10);
                            $pdf->AddPage();
                            $pdf->writeHTML($params['invoice_source'], true, false, true, false, '');
                            $pdf->lastPage();

                            $email_source = $pdf->Output("fattura_ordine_" . $params['order_id'] . ".pdf", 'S');
                            $this->mail->AddStringAttachment($email_source, $params['file_name'] . ".pdf");
                        }

                    },
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{order_id}" => "L'ID dell'ordine"
                        ),
                        array(
                            "{[nome]}",
                            "{[email]}",
                            "{[order_id]}"
                        )
                    )
                ),
                'order-receipt' => array(
                    'nome' => 'Ricevuta Cliente',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Ricevuta ordine #{order_id} - {site-name}",
                    'beforeSend' => function($params, $template_settings) {

                        $invoice_settings = $this->MSFrameworkCMS->getCMSData('fatturazione_fatture');

                        if ($invoice_settings['attachment_format'] == 1) {
                            $this->mail->AddStringAttachment($params['invoice_source'], $params['file_name'] . ".htm", "base64", "text/html");
                        }
                        else
                        {
                            $pdf = new \TCPDF\TCPDF();

                            $pdf->SetCreator('MarketingStudio');
                            $pdf->SetAuthor(SW_NAME);
                            $pdf->SetTitle('Ricevuta Ordine');
                            $pdf->SetSubject($params['subject']);
                            $pdf->setPrintHeader(false);
                            $pdf->setPrintFooter(false);
                            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                            $pdf->setImageScale(1.5);
                            $pdf->SetFont('dejavusans', '', 10);
                            $pdf->AddPage();
                            $pdf->writeHTML($params['invoice_source'], true, false, true, false, '');
                            $pdf->lastPage();

                            $email_source = $pdf->Output("ricevuta_ordine_" . $params['order_id'] . ".pdf", 'S');
                            $this->mail->AddStringAttachment($email_source, $params['file_name'] . ".pdf");
                        }

                        if(!empty($invoice_settings['invoice_mail_cc'])) {
                            $this->mail->addBCC($invoice_settings['invoice_mail_cc']);
                        }

                    },
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{order_id}" => "L'ID dell'ordine"
                        ),
                        array(
                            "{[nome]}",
                            "{[email]}",
                            "{[order_id]}"
                        )
                    )
                ),
                /* ALTRO */
                'abandoned-cart' => array(
                    'nome' => 'Carrello Abbandonato',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Hai abbandonato il tuo carrello? - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => 'Il nome del cliente',
                            "{email}" => 'L\'email del cliente',
                            "{prezzo_totale}" => "Il prezzo totale",
                            "{tabella_prodotti}" => "La tabella con la lista dei prodotti"
                        ),
                        array(
                            "{[user][nome]} {[user][cognome]}",
                            "{[email]}",
                            function ($params) {
                                return number_format($params['cart']['coupon_price'], 2, ',', '.') . CURRENCY_SYMBOL;
                            },
                            function ($params) {
                                $products_rows_template = '<tr><td style="text-align: left;"><small><b>{quantity}*</b></small> {product_name}</td><td style="text-align: right;">{product_price}</td></tr>';
                                $products_row_html = '';
                                foreach ($params['cart']['products'] as $product) {
                                    $products_row_html .= str_replace(
                                        array(
                                            "{quantity}",
                                            "{product_name}",
                                            "{product_price}",
                                        ),
                                        array(
                                            $product['quantity'],
                                            $product['nome'],
                                            number_format((new \MSFramework\Ecommerce\products())->priceArrayToNumeric($product['prezzo']['tax']) * $product['quantity'], 2, ',', '.') . CURRENCY_SYMBOL
                                        ),
                                        $products_rows_template
                                    );
                                }

                                return '<table style="width: 100%;">' . $products_row_html . '</table>';

                            }
                        )
                    )
                ),
            )
        );

        return $templates;
    }

}
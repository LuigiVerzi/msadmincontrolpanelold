<?php
/**
 * MSFramework
 * Date: 18/05/18
 */

namespace MSFramework;


class widget {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
$this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente alle tipologie di campo del widget
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getFieldTypes($key = "") {
        $ary = array(
            "0" => "Testo",
            "1" => "Immagine",
            "2" => "Textarea",
            "3" => "Menu",
        );

        if($key != "") {
            return $ary[$key];
        } else {
            return $ary;
        }
    }

    /**
     * Restituisce un array associativo (ID Widget => Dati Widget) con i dati relativi al Widget.
     *
     * @param $id L'ID del Widget (stringa) o dei widgets (array) richiesti (se vuoto, vengono recuperati i dati di tutti i widget)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getFieldsByWidgetID($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }


        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM widget WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene tutti i campi del widget specificato nel parametro "nome". Viene restituito un array associativo le cui chiavi sono valorizzate con i nomi dei singoli campi.
     *
     * @param $nome string Il nome del widget
     *
     * @return array|bool
     */
    public function getFieldsByWidgetName($nome) {
        if($nome == "") {
            return false;
        }

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT is_active, fields FROM widget WHERE nome = :nome", array(":nome" => $nome), true);
        $fields = json_decode($r['fields'], true);

        $to_return = array('is_active' => (int)$r['is_active']);
        foreach($fields as $field) {
            $to_return[$field['name']] = $field;
        }

        return $to_return;
    }

    /**
     * Ottiene tutti i campi degli widget che contengono il tag [$tag] prima del titolo. Viene restituito un array associativo le cui chiavi sono valorizzate con i nomi dei singoli campi.
     *
     * @param $tag string Il nome del widget
     *
     * @return array|bool
     */
    public function getFieldsByTag($tag) {
        if($tag == "") {
            return false;
        }
        $ary_to_return = array();

       foreach($this->MSFrameworkDatabase->getAssoc("SELECT id, is_active, fields FROM widget WHERE nome LIKE :nome", array(":nome" => "[" . $tag . "]%")) as $r) {
           $fields = json_decode($r['fields'], true);

           $to_return = array('is_active' => (int)$r['is_active']);
           foreach ($fields as $field) {
               $to_return[$field['name']] = $field;
           }
           $ary_to_return[$r['id']] = $to_return;
       }

        return $ary_to_return;
    }
}
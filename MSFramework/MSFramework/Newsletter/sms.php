<?php
/**
 * MSFramework
 * Date: 10/03/18
 */

namespace MSFramework\Newsletter;

class sms {

    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        Global $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkCustomers = new \MSFramework\customers();

        $this->MSFrameworkSMS = new \MSFramework\sms();
    }

    /**
     * Restituisce un array associativo (ID SMS => Dati SMS) con i dati relativi agli SMS.
     *
     * @param mixed $id L'ID dell'SMS o un array contenente gli IDS dei vari SMS
     *
     * @return array
     */
    public function getSMSDetails($id) {

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__sms WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;

    }

    /**
     * Invia un SMS
     */
    public function sendSMS($id_recipient, $id_sms) {

        $smsDetails = $this->getSMSDetails($id_sms)[$id_sms];
        $userDetails = $this->MSFrameworkCustomers->getCustomerDataFromDB($id_recipient);

        if(!$smsDetails || !$userDetails || empty($userDetails['telefono_cellulare'])) return false;

        $status = false;
        if((new \MSFramework\Framework\credits())->removeCreditsForActions('sms')) {
            $status = $this->MSFrameworkSMS->sendSMS($userDetails['telefono_cellulare'], $smsDetails['message'], $smsDetails["sender"]);
            $this->MSFrameworkDatabase->query("INSERT IGNORE INTO newsletter__sms_destinatari_status (id_sms, id_recipient, date_received, sms_id) VALUES (:id_sms, :id_recipient, :status, :msg_id)", array(':id_sms' => $smsDetails['id'], ':id_recipient' => $id_recipient, ':status' => ($status ? date('Y-m-d H:i:s') : ''), ':msg_id' => $status["internal_order_id"]));
        } else {
            // Imposto la campagna in pausa
            $this->MSFrameworkDatabase->query("UPDATE newsletter__campagne SET status = 3 WHERE id = :campaign_id", array(':campaign_id' => $smsDetails['campaign_id']));
        }

        return $status;
    }

}
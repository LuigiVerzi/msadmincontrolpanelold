<?php
/**
 * MSFramework
 * Date: 10/03/18
 */

namespace MSFramework\Newsletter;

class lists {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        Global $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkCustomers = new \MSFramework\customers();

        $this->Subscribers = new subscribers();
        $this->Conditions = new conditions();
    }

    /**
     * Restituisce un array associativo (ID Tag => Dati Tag) con i dati relativi ai tag.
     *
     * @param $id L'ID del tag (stringa) o dei tag (array) richiesti (se vuoto, vengono recuperati i dati di tutti i tag)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getTagDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = "";
        $append_where_ary = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM newsletter__tag WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ")", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce il numero di utenti iscritti in un determinato tag (gruppo newsletter)
     *
     * @param integer $tag_id L'ID del tag
     * @param bool $exclude_unsubscribed Se impostato su true, esclude dal conteggio gli utenti disiscritti dalla newsletter
     *
     * @return null|void
     */
    public function getSubscribersCountForTag($tag_id, $exclude_unsubscribed = true) {
        if($tag_id == "") {
            return null;
        }

        $unsubscribed_lj = "";
        $unsubscribed_where = "";
        if($exclude_unsubscribed) {
            $unsubscribed_lj = " LEFT JOIN newsletter__destinatari ON newsletter__destinatari.id = newsletter__tag_destinatari.destinatario ";
            $unsubscribed_where = " AND newsletter__destinatari.active = 1 ";
        }

        return $this->MSFrameworkDatabase->getCount("SELECT newsletter__tag_destinatari.id FROM newsletter__tag_destinatari $unsubscribed_lj WHERE tag = :tag $unsubscribed_where ", array(":tag" => $tag_id));
    }

    /**
     * Sincronizza le liste con i contatti che necessitano di essere aggiunti
     * @param array $list_ids L'id delle liste da aggiornare
     * @param bool $force_import Se indicato forza l'importazione anche se impostato il behavior only_presents
     * @return bool Se trova destinatari da aggiornare allora ritorna TRUE altrimenti FALSE
     */
    public function syncContactToLists($list_ids = array(), $force_import = false) {

        $ids_sql = '';
        if($list_ids) {
            if(!is_array($list_ids)) $list_ids = array($list_ids);
            $ids_sql = "AND id IN(" . implode(',', $list_ids) . ")";
        }

        $user_updated = false;
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM `newsletter__tag` WHERE sync_options NOT LIKE '{}' $ids_sql ORDER by id DESC") as $tag) {
            $sync_options = json_decode($tag['sync_options'], true);

            $accepted_behaviors = array('both', 'only_future');
            if($force_import) {
                $accepted_behaviors[] = 'only_presents';
            }

            if(in_array($sync_options['behavior'], $accepted_behaviors)) {

                if(!$sync_options['conditions']) {
                    $this->MSFrameworkDatabase->getAssoc("SET SESSION group_concat_max_len = 100000000;");
                    $user_to_sync = explode(',', $this->MSFrameworkDatabase->getAssoc("SELECT GROUP_CONCAT(id ORDER BY id DESC SEPARATOR ',') as ids FROM `customers`", array(), true)['ids']);
                } else {
                    $user_to_sync = $this->Conditions->getPassingConditionUsers($sync_options['conditions']);
                }

                if($user_to_sync) {
                    $insert_sql_ids = implode("'), ('", $user_to_sync);
                    $this->MSFrameworkDatabase->query("INSERT IGNORE INTO newsletter__destinatari (id) VALUES ('$insert_sql_ids')");

                    $insert_tag_sql_ids = implode("'), (" . $tag['id'] . ", '", $user_to_sync);
                    $this->MSFrameworkDatabase->query("INSERT IGNORE INTO newsletter__tag_destinatari (tag, destinatario) VALUES (" . $tag['id'] . ", '$insert_tag_sql_ids')");
                    $user_updated = true;
                }
            }
        }

        return $user_updated;
    }



}
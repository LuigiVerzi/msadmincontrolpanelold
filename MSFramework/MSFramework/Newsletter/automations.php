<?php
/**
 * MSFramework
 * Date: 10/03/18
 */

namespace MSFramework\Newsletter;

class automations {
    public function __construct() {
        Global $MSFrameworkDatabase;
        Global $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;

        $this->Conditions = new \MSFramework\Newsletter\conditions();
        $this->Emails = new \MSFramework\Newsletter\emails();
        $this->Sms = new \MSFramework\Newsletter\sms();
    }

    public function prepareDiagram($diagramma) {

        if(!is_array($diagramma)) {
            $diagramma = array();
        }

        foreach($diagramma as $k => $item) {
            if ($item['type'] == 'connect') {
                if (
                    $item['data']['destination'] == $item['id']
                    ||
                    $item['data']['destination'] == $item['parent']
                    ||
                    (
                        $item['data']['destination'] !== "0"
                        &&
                        !isset($diagramma[str_replace(array('_result_1', '_result_0'), array('', ''), $item['data']['destination'])])
                    )
                ) {
                    $diagramma[$k]['data']['destination'] = 'err';
                }
            }
        }

        return $diagramma;

    }

    /**
     * Stampa l'HTML del diagramma
     * @param array $diagram L'array contenente il diagramma
     * @param string $parent L'id del genitore
     * @param boolean $only_view Se impostato su true non permette la modifica
     * @param array $origin L'origine dell'automazione
     * @return string L'HTML del diagramma
     */
    public function drawAutomationChart($diagram, $parent = "", $only_view = false, $origin = array()) {

        if(!is_array($diagram)) {
            return '';
        }

        if($parent == "") {

            $diagram = array_merge($diagram,
                array(
                    array('id' => '0', 'parent' => '', 'type' => 'start_automation', 'data' => $origin)
                )
            );

            foreach($diagram as $k => $item) {
                if($item['type'] == 'condition') {
                    $diagram = array_merge($diagram,
                        array(
                            array('id' => $item['id'] . '_result_1', 'css' => 'condition_result_1', 'parent' => $item['id'], 'type' => 'condition_result', 'condition_result' => 1),
                            array('id' => $item['id'] . '_result_0', 'css' => 'condition_result_0', 'parent' => $item['id'], 'type' => 'condition_result', 'condition_result' => 0)
                        )
                    );
                }
            }
        }

        $output = "";
        foreach($diagram as $k => $item){

            if((string)$parent === (string)$item['parent']) {
                $output .= '<li class="item ' . $item['type'] . '">';

                $output .= '<div class="automationBox type_' . $item['type'] . (isset($item['css']) ? ' ' . $item['css'] : '') . '" data-id="' . $item['id'] . '" data-parent="' . $item['parent'] . '" data-type="' . $item['type'] . '">';

                $output .= $this->formatAutomationBlock($item);

                if(!in_array($item['type'], array('condition_result')) && !$only_view) {
                    $output .= '<div class="automationBtns">';
                    if(!in_array($item['type'], array('start_automation'))) {
                        $output .= '<a href="#" class="deleteAutomation"><i class="fa fa-trash"></i></a>';
                    }
                    if(!in_array($item['type'], array('end'))) {
                        $output .= '<a href="#" class="editAutomation"><i class="fa fa-pencil"></i></a>';
                    }
                    if(!in_array($item['type'], array('start_automation'))) {
                        $output .= '<a href="#" class="moveAutomation"><i class="fa fa-arrows"></i></a>';
                    }
                    $output .= '</div>';
                }

                $output .= '</div>';

                if($item['type'] !== 'condition') {

                    if($only_view) {
                        $output .= '<div class="attachedHere"></div>';

                        if($item['id'] !== "0") {
                            $output .= $this->getPassedRecipientsStats($item['id'], $item['type']);
                        }
                    } else if(in_array($item['type'], array('email', 'sms', 'wait', 'condition_result', 'start_automation'))) {
                        $output .= $this->getInsertBtn($item['id'], true);
                    }

                }

                unset($diagram[$k]);
                $output .= $this->drawAutomationChart($diagram, $item['id'], $only_view);

                $output .= '</li>';
            }
        }

        if($output !== "") {
            $output = '<ul class="group">' . $output . '</ul>';
        }

        return $output;

    }

    public function getInsertBtn($parent, $moving_enabled = false) {

        $btns = '<div class="attachedHere"></div>';

        $btns .= '<div class="btn-group afterAutomationBtn actionsGroupBtn" data-parent="' . $parent . '">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><i class="fa fa-plus"></i></button>
                    <ul class="dropdown-menu afterEvents">
                        <li><a class="dropdown-item" data-type="wait" href="#"><i class="fa fa-clock-o"></i> Attesa</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#" data-type="email"><i class="fa fa-envelope"></i> Invia email</a></li>
                        <li><a class="dropdown-item" href="#" data-type="sms"><i class="fa fa-comment-o"></i> Invia SMS</a></li>
                        <!--
                        <li><a class="dropdown-item" href="#" data-type="whatsapp"><i class="fa fa-whatsapp"></i> Invia Whatsapp</a></li>
                         -->
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#" data-type="condition"><i class="fa fa-list-alt"></i> Condizione</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#" data-type="connect"><i class="fa fa-code-fork"></i> Passa ad un\'altro evento</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#" data-type="end"><i class="fa fa-ban"></i> Termina automazione</a></li>
                    </ul>
                </div>';

        if($moving_enabled) {
            $btns .= '<button class="btn btn-success afterAutomationBtn moveAutomationHere" data-parent="' . $parent . '">...</button>';
            $btns .= '<button class="btn btn-danger afterAutomationBtn connectAutomationHere" data-parent="' . $parent . '"><i class="fa fa-plug"></i> Connetti</button>';
        }

        return $btns;
    }

    public function getPassedRecipientsStats($parent, $type) {

        $passed_stats = '<div class="attachedHere"></div>';

        if($type === 'condition_result') return $passed_stats;

        if($type === 'wait') {
            $count_waiting = $this->MSFrameworkDatabase->getCount("SELECT * FROM newsletter__campagne_destinatari_status where automations_data LIKE :parent_search", array(':parent_search' => '%"awaiting_actions":["' . $parent . '"]%'));
            if ($count_waiting > 0) {
                $passed_stats .= '<div class="action_recipient_info waiting" title="Attualmente ci sono ' . $count_waiting . ' contatti in attesa qui""><b>' . $count_waiting . '</b> in attesa</div>';
            }
        }

        $count_passed = $this->MSFrameworkDatabase->getCount("SELECT * FROM newsletter__campagne_destinatari_status where automations_data LIKE :parent_search", array(':parent_search' => '%"' . $parent . '":{"passed_date"%'));
        $passed_stats .= '<div class="action_recipient_info passed" title="Questo punto è stato raggiunto ' . $count_passed . ' volte"><b>' . $count_passed . '</b> passati</div>';

        return $passed_stats;
    }

    /**
     * Effettua la lista delle automazioni disponibili per un certo utente
     *
     * @param array $recipient_data Un array con le informazioni del destinatari (tabella newsletter__campagne_destinatari_status)
     * @param array $diagramma Un array con il diagramma della campagna
     * @param string $parent L'id del genitore
     * @param array $return_info Le info ritornate dalla chiamata
     * @param int $jump Il numero di volte che si viene rimandati programmaticamente dentro l'automazione (Azione passa ad un'altra azione)
     *
     * @return array Status 0 => Completo; 1 => In Corso; 3 => Errore troppi redirect; 'smtp_error' => 'Errore SMTP
     */
    public function runAutomationForRecipient($recipient_data, $diagramma, $parent = "0", $return_info = array(), $jump = 0) {

        if(!$return_info) {
            $return_info = array('status' => 1, 'data' => $recipient_data['automations_data']);
            if(!isset($return_info['data']['items'])) $return_info['data']['items'] = array();

            if(!isset($return_info['data']['awaiting_actions'])) {
                $return_info['data']['awaiting_actions'] = array();
            }

            if($return_info['data']['awaiting_actions']) {

                // Faccio ripartire l'automazione da dove era rimasta
                foreach($return_info['data']['awaiting_actions'] as $parent_to_resume => $awaiting_date) {
                    if($awaiting_date <= time()) {
                        unset($return_info['data']['awaiting_actions'][$parent_to_resume]);

                        $return_info = array_merge(
                            $return_info,
                            $this->runAutomationForRecipient($recipient_data, $diagramma, $parent_to_resume, $return_info, $jump)
                        );
                    }
                }

                return $return_info;

            } else if(count($return_info['data']['items']) > 0) {

                // Se non ci sono azioni in attesa ma sono presenti eventi passati nella cronologia significa che l'automazione è terminata
                $return_info['status'] = 0;
                return $return_info;
            }

        }

        // Se i redirect sono troppi fermo l'esecuzione e imposto lo stato di errore (3)
        if($jump > 1) {
            $return_info['status'] = 3;
        }

        // Se l'automazione viene fermata ritorno l'array e chiudo il ciclo
        if($return_info['status'] !== 1) {
            return $return_info;
        }

        foreach($diagramma as $k => $item) {

            if((string)$parent === (string)$item['parent']) {

                // Se non esiste l'array dell'azione corrente lo creo
                if(!isset($return_info['data']['items'][$item['id']])) {
                    $return_info['data']['items'][$item['id']] = array();
                }

                // Salvo la data del primo incontro
                if(!isset($return_info['data']['items'][$item['id']]['passed_date'])) {
                    $return_info['data']['items'][$item['id']]['passed_date'] = time();
                }

                if($item['type'] == 'wait') {

                    // Aggiungo l'azione in attesa (verrà processata nelle esecuzioni future)
                    if(!isset($return_info['data']['awaiting_actions'][$item['id']])) {
                        if($item['data']['type'] && $item['data']['type'] === 'date') {
                            $return_info['data']['awaiting_actions'][$item['id']] = $item['data']['date'];
                        } else if((int)$item['data']['value'] > 0) {
                            $return_info['data']['awaiting_actions'][$item['id']] = strtotime(' + ' . (int)$item['data']['value'] . ' ' . $item['data']['what']);
                        } else {
                            $return_info = array_merge(
                                $return_info,
                                $this->runAutomationForRecipient($recipient_data, $diagramma, $item['id'], $return_info, $jump)
                            );
                        }
                    }

                } else if($item['type'] == 'wait_jump') {

                    // Aggiungo l'azione in attesa (verrà processata nelle esecuzioni future)
                    if(!isset($return_info['data']['awaiting_actions'][$item['id']]) && (int)$item['data']['value'] > 0) {
                        $return_info['data']['awaiting_actions'][$item['data']['target']] = strtotime(' + ' . (int)$item['data']['value'] . ' ' . $item['data']['what']);
                    }

                } else if($item['type'] == 'condition') {

                    // Ottengo lo stato della condizione (1 => Vero; 0 => Falso)
                    $condition_status = (in_array($recipient_data['id_recipient'], $this->Conditions->getPassingConditionUsers($item['data']['conditions'])) ? 1 : 0);

                    $time_expiried = false;
                    if(!$condition_status) {

                        // Se non esiste imposto la data di scadenza della condizione (oltre la quale il controllo proseguirà anche se con risultato negativo)
                        if(!isset($return_info['data']['items'][$item['id']]['condition_expiration']) || !(int)$return_info['data']['items'][$item['id']]['condition_expiration']) {
                            $return_info['data']['items'][$item['id']]['condition_expiration'] =  strtotime(' + ' .  (int)$item['data']['wait_value'] . ' ' . $item['data']['wait_what'], $return_info['data']['items'][$item['id']]['passed_date']);
                        }

                        // Se il tempo è scaduto allora faccio proseguire l'azione (indipendentemente dal risultato) altrimenti la rimetto in attesa
                        if($return_info['data']['items'][$item['id']]['condition_expiration'] <= time()) {
                            $time_expiried = true;
                        } else {
                            $return_info['data']['awaiting_actions'][$parent] = time();
                        }
                    }

                    // Se la condizione è stata superata oppure il tempo è scaduto proseguo con il risultato
                    if($condition_status || $time_expiried) {

                        // Elimino la data di scadenza della condizione
                        unset($return_info['data']['items'][$item['id']]['condition_expiration']);

                        $return_info['data']['items'][$item['id']]['condition_result'] = $condition_status;
                        $return_info['data']['items'][$item['id'] . '_result_' . $condition_status] = array('passed_date' => time());

                        $return_info = array_merge(
                            $return_info,
                            $this->runAutomationForRecipient($recipient_data, $diagramma, $item['id'] . '_result_' . $condition_status, $return_info, $jump)
                        );
                    }

                    // Se l'automazione è ancora in attesa esco
                    return $return_info;

                } else if($item['type'] == 'connect') {

                    // Ritorno al parent di destinazione
                    $return_info = array_merge(
                        $return_info,
                        $this->runAutomationForRecipient($recipient_data, $diagramma, $item['data']['destination'], $return_info, ($jump+1))
                    );

                } else if($item['type'] == 'end') {

                    // Imposto lo stato a 0 (L'automazione non sarà più eseguita per questo utente)
                    $return_info['status'] = 0;
                    return $return_info;

                } else if($item['type'] == 'email') {
                    $this->Emails->sendEmail($recipient_data['id_recipient'], $item['data']['id']);
                } else if($item['type'] == 'sms') {
                    $this->Sms->sendSMS($recipient_data['id_recipient'], $item['data']['id']);
                }

                // Se l'elemento consente di avere child allora controllo
                if(!in_array($item['type'], array('wait', 'condition', 'end', 'connect'))) {
                    $return_info = array_merge(
                        $return_info,
                        $this->runAutomationForRecipient($recipient_data, $diagramma, $item['id'], $return_info, $jump)
                    );
                }

            }
        }

        // Se dopo l'automazione non ci sono azioni da attendere imposto lo stato a 0 (Terminata)
        if(!$return_info['data']['awaiting_actions']) {
            $return_info['status'] = 0;
        }

        return $return_info;
    }

    /**
     * Formatta i blocchi dell'automazione in base alla tipologia
     * @param array $item L'oggetto dell'automazione
     * @return string Ritorna l'HTML del blocco
     */
    public function formatAutomationBlock($item) {

        $html = '';

        $time_what_array = array(
            'minutes' => array('minut', 'i', 'o'),
            'hours' => array('or', 'e', 'a'),
            'days' => array('giorn', 'i', 'o'),
            'weeks' => array('settiman', 'e', 'a'),
            'months' => array('mes', 'i', 'e'),
            'years' => array('ann', 'i', 'o'),
        );

        if($item['type'] == 'start_automation') {
            if($item['data']['tags']) {

                $behavior_label = 'Solo i contatti già iscritti';
                if($item['data']['behavior'] === 'only_future') {
                    $behavior_label = 'Solo i futuri iscritti';
                } else if($item['data']['behavior'] === 'both') {
                    $behavior_label = 'Sia gli attuali che i futuri contatti';
                }

                $html .= '<h2><u>' . $behavior_label . '</u> nelle seguenti liste:</h2>';

                $html .= '<ul class="tag-list" style="padding: 0">';
                foreach((new \MSFramework\Newsletter\lists())->getTagDetails($item['data']['tags']) as $list) {
                    $html .= '<li><a><i class="fa fa-tag"></i> Lista ' . $list['nome'] . '</a></li>';
                }
                $html .= '</ul>';

            } else {
                $html .= '<h2>Nessuna lista selezionata</h2>';
                $html .= '<p>Seleziona una o più liste dalla quale far entrare i contatti.</p>';
            }
            //$html .= '<i class="fa fa-angle-down automationIcon bigIcon"></i>';
        } else if($item['type'] == 'email') {
            $html .= '<i class="fa fa-envelope automationIcon bigIcon"></i>';
            $html .= '<h2>Invio email <b>' . htmlentities($item['data']['nome']) . '</b></h2>';
        } else if($item['type'] == 'sms') {
            $html .= '<i class="fa fa-sms automationIcon bigIcon"></i>';
            $html .= '<h2>Invio sms <b>' . htmlentities($item['data']['name']) . '</b></h2>';
        } else if($item['type'] == 'wait') {
            $html .= '<i class="fa fa-clock-o automationIcon"></i>';

            if($item['data']['type'] && $item['data']['type'] === 'date') {
                $html .= '<p>Attendi le ore <b>' . date('H:i', $item['data']['date']) . '</b> di giorno <b>' . date('d/m/Y', $item['data']['date']) . '</b></p>';
            } else {
                $html .= '<p>Attendi <b>' . (int)$item['data']['value'] . ' ' . $time_what_array[$item['data']['what']][0] . $time_what_array[$item['data']['what']][($item['data']['value'] == 1 ? 2 : 1)] . '</b></p>';
            }
        } else if($item['type'] == 'connect') {
            if($item['data']['destination'] == 'err') {
                $html .= '<i class="fa fa-warning automationIcon"></i>';
                $html .= '<p>Errore! Collegamento non valido!</p>';
            } else {
                $html .= '<i class="fa fa-code-fork automationIcon"></i>';
                $html .= '<p>Passa ad un\'altra azione</p>';
            }
            $html .= '<input type="hidden" name="destination" value="' . htmlentities($item['data']['destination']) . '">';
        } else if($item['type'] == 'end') {
            $html .= '<i class="fa fa-ban automationIcon"></i>';
            $html .= '<p>Termina automazione</p>';
        } else if($item['type'] == 'condition') {
            $html .= '<i class="fa fa-question automationIcon bigIcon"></i>';
            $html .= '<p>' . htmlentities($item['data']['description']) . '</p>';
            $html .= '<small class="badge badge-white">Entro <b>' . (int)$item['data']['wait_value'] . ' ' . $time_what_array[$item['data']['wait_what']][0] . $time_what_array[$item['data']['wait_what']][($item['data']['wait_value'] == 1 ? 2 : 1)]  . '</b></small>';
        } else if($item['type'] == 'condition_result') {
            if($item['condition_result'] == 1) {
                $html .= '<h2 title="Se la condizione viene passata">Si</h2>';
            } else if($item['condition_result'] == 0) {
                $html .= '<h2 title="Se la condizione non viene passata">No</h2>';
            }
        }

        return $html;

    }
}
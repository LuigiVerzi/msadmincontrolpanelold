<?php
/**
 * MSFramework
 * Date: 10/03/18
 */

namespace MSFramework\Newsletter;

class conditions {

    public $conditionNeedUserLogin = false;
    public $ignoreMandatoryCustomers = false;

    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        Global $MSFrameworkCMS;

        $this->conditions_cache = array();

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
    }

    /**
     * Ottiene i filtri utilizzabili dalla query condizioni
     */
    public function getConditionsQueryFilters() {

        $this->MSFrameworkDatabase->query("SET SESSION group_concat_max_len = 100000000;");

        $filters = array();

        /* FILTRI DATA & ORA */
        $filters[] = array(
            'id' => 'sistema_data',
            'label' => 'Data attuale',
            'icon' => 'fa fa-calendar-o',
            'type' => 'datetime',
            'data' => array(
                'type' => 'sistema',
                'id' => 'data'
            ),
            'optgroup' => 'Data & Ora',
            'plugin' => 'datepicker',
            'plugin_config' => array(
                'dateFormat' => 'dd/mm/yy',
                'autoclose' => true,
                'todayBtn' => 'linked',
                'todayHighlight' => true
            )
        );

        $filters[] = array(
            'id' => 'sistema_ora',
            'label' => 'Ora attuale',
            'icon' => 'fa fa-clock-o',
            'type' => 'time',
            'data' => array(
                'type' => 'sistema',
                'id' => 'ora'
            ),
            'optgroup' => 'Data & Ora',
            'plugin' => 'clockpicker',
            'plugin_config' => array(
                'autoclose' => true
            ),
            'operators' => array('greater', 'less', 'between')
        );

        /* FILTRI ANAGRAFICA */
        $filters[] = array(
            'id' => 'anagrafica_registrazione',
            'label' => 'Data registrazione',
            'icon' => 'fa fa-calendar-o',
            'type' => 'date',
            'data' => array(
                'type' => 'anagrafica',
                'id' => 'registration_date'
            ),
            'optgroup' => 'Anagrafica',
            'plugin' => 'datepicker',
            'plugin_config' => array(
                'dateFormat' => 'dd/mm/yy',
                'autoclose' => true,
                'todayBtn' => 'linked',
                'todayHighlight' => true
            )
        );

        $filters[] = array(
            'id' => 'anagrafica_nascita',
            'label' => 'Data di nascita',
            'icon' => 'fa fa-birthday-cake',
            'type' => 'date',
            'data' => array(
                'type' => 'anagrafica',
                'id' => 'FROM_UNIXTIME(data_nascita)'
            ),
            'optgroup' => 'Anagrafica',
            'plugin' => 'datepicker',
            'plugin_config' => array(
                'dateFormat' => 'dd/mm/yy',
                'autoclose' => true,
                'todayBtn' => 'linked',
                'todayHighlight' => true
            )
        );

        $filters[] = array(
            'id' => 'anagrafica_sesso',
            'label' => 'Sesso',
            'icon' => 'fa fa-mars',
            'type' => 'string',
            'input' => 'select',
            'data' => array(
                'type' => 'anagrafica',
                'id' => 'sesso'
            ),
            'optgroup' => 'Anagrafica',
            'operators' => array('equal'),
            'values' => array(
                'm' => 'Maschile',
                'f' => 'Femminile',
                'N/A' => 'Non Specificato',
            )
        );

        $filters[] = array(
            'id' => 'anagrafica_luogo',
            'label' => 'L\'indirizzo',
            'icon' => 'fa fa-map-marker',
            'type' => 'string',
            'input' => 'text',
            'data' => array(
                'type' => 'anagrafica',
                'id' => 'CONCAT(customers.indirizzo,customers.citta,customers.provincia)',
                'extra' => 'partial',
            ),
            'optgroup' => 'Anagrafica',
            'operators' => array('contains')
        );


        $filters[] = array(
            'id' => 'anagrafica_telefono',
            'label' => 'Il telefono',
            'icon' => 'fa fa-phone',
            'type' => 'string',
            'data' => array(
                'type' => 'anagrafica',
                'id' => 'CONCAT(customers.telefono_cellulare,customers.telefono_casa)'
            ),
            'optgroup' => 'Anagrafica',
            'operators' => array('is_empty', 'not_empty')
        );

        /* FILTRI ANAGRAFICA */
        $filters[] = array(
            'id' => 'anagrafica_ultimo_accesso',
            'label' => 'Data ultimo accesso',
            'icon' => 'fa fa-calendar-o',
            'type' => 'date',
            'data' => array(
                'type' => 'anagrafica',
                'id' => 'last_login'
            ),
            'optgroup' => 'Anagrafica',
            'plugin' => 'datepicker',
            'plugin_config' => array(
                'dateFormat' => 'dd/mm/yy',
                'autoclose' => true,
                'todayBtn' => 'linked',
                'todayHighlight' => true
            )
        );

        $filters[] = array(
            'id' => 'anagrafica_ora_ultimo_accesso',
            'label' => 'Ora ultimo accesso',
            'icon' => 'fa fa-clock-o',
            'type' => 'time',
            'data' => array(
                'type' => 'anagrafica',
                'id' => 'TIME(customers.last_login)'
            ),
            'optgroup' => 'Anagrafica',
            'plugin' => 'clockpicker',
            'plugin_config' => array(
                'autoclose' => true
            ),
            'operators' => array('greater', 'less', 'between')
        );

        if($this->MSFrameworkCMS->checkExtraFunctionsStatus('ecommerce')) {

            $filters[] = array(
                'id' => 'ecommerce_carrello_abbandonato',
                'label' => 'Il carrello attuale',
                'icon' => 'fa fa-cart-arrow-down',
                'type' => 'integer',
                'input' => 'select',
                'data' => array(
                    'type' => 'ecommerce',
                    'id' => 'carrello_abbandonato'
                ),
                'optgroup' => 'Ecommerce',
                'operators' => array('equal'),
                'values' => array(
                    1 => 'Contiene prodotti',
                    0 => 'È vuoto'
                )
            );

            $filters[] = array(
                'id' => 'ecommerce_ordini_effettuati',
                'label' => 'Ha effettuato ordini',
                'icon' => 'fa fa-cart-arrow-down',
                'type' => 'date',
                'input' => 'radio',
                'data' => array(
                    'type' => 'ecommerce',
                    'id' => 'ordini_effettuati'
                ),
                'optgroup' => 'Ecommerce',
                'operators' => array('equal'),
                'values' => array(
                    1 => 'Si',
                    0 => 'No',
                )
            );
        }

        /* ABBONAMENTI */
        if($this->MSFrameworkCMS->checkExtraFunctionsStatus('subscriptions')) {

            $filters[] = array(
                'id' => 'subscriptions_status',
                'label' => 'Stato abbonamento',
                'icon' => 'fa fa-diamond',
                'type' => 'integer',
                'input' => 'select',
                'data' => array(
                    'type' => 'subscriptions',
                    'id' => 'subscriptions_status'
                ),
                'optgroup' => 'Abbonamento',
                'operators' => array('equal'),
                'values' => array(
                    1 => 'Attivo',
                    0 => 'Disattivato'
                )
            );

            $filters[] = array(
                'id' => 'subscriptions_start_date',
                'label' => 'Data inizio abbonamento',
                'icon' => 'fa fa-calendar-o',
                'type' => 'date',
                'data' => array(
                    'type' => 'subscriptions',
                    'id' => 'subscriptions_start_date'
                ),
                'optgroup' => 'Abbonamento',
                'plugin' => 'datepicker',
                'plugin_config' => array(
                    'dateFormat' => 'dd/mm/yy',
                    'autoclose' => true,
                    'todayBtn' => 'linked',
                    'todayHighlight' => true
                )
            );

            $filters[] = array(
                'id' => 'subscriptions_end_date',
                'label' => 'Data fine abbonamento',
                'icon' => 'fa fa-calendar',
                'type' => 'date',
                'data' => array(
                    'type' => 'subscriptions',
                    'id' => 'subscriptions_end_date'
                ),
                'optgroup' => 'Abbonamento',
                'plugin' => 'datepicker',
                'plugin_config' => array(
                    'dateFormat' => 'dd/mm/yy',
                    'autoclose' => true,
                    'todayBtn' => 'linked',
                    'todayHighlight' => true
                )
            );

            $filters[] = array(
                'id' => 'subscriptions_remaining_days',
                'label' => 'Giorni abbonamento rimanenti',
                'icon' => 'fa fa-clock-o',
                'type' => 'string',
                'input' => 'number',
                'data' => array(
                    'type' => 'subscriptions',
                    'id' => 'subscriptions_remaining_days'
                ),
                'optgroup' => 'Abbonamento',
                'operators' => array('equal', 'not_equal', 'greater', 'less', 'between')
            );
        }

        /* FILTRI NEWSLETTER */
        $campaign_emails_values = array(array('value' => 0, 'label' => 'Qualsiasi campagna o email', 'optgroup' => ''));
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT *, (SELECT nome FROM newsletter__campagne WHERE id = newsletter__emails.campaign_id) as campaign_name FROM newsletter__emails") as $email) {
            $campaign_emails_values[] = array('value' => $email['id'], 'label' => $email['nome'], 'optgroup' => $email['campaign_name']);
        }

        $filters[] = array(
            'id' => 'newsletter_open',
            'label' => 'Ha aperto',
            'icon' => 'fa fa-envelope-open-o',
            'type' => 'integer',
            'input' => 'select',
            'data' => array(
                'type' => 'newsletter_open',
                'id' => '1'
            ),
            'optgroup' => 'Azioni',
            'operators' => array('email'),
            'values' => $campaign_emails_values
        );

        $filters[] = array(
            'id' => 'newsletter_not_open',
            'label' => 'Non ha aperto',
            'icon' => 'fa fa-envelope',
            'type' => 'integer',
            'input' => 'select',
            'data' => array(
                'type' => 'newsletter_open',
                'id' => '0'
            ),
            'optgroup' => 'Azioni',
            'operators' => array('email'),
            'values' => $campaign_emails_values
        );

        $filters[] = array(
            'id' => 'newsletter_click',
            'label' => 'Ha cliccato',
            'icon' => 'fa fa-mouse-pointer',
            'type' => 'integer',
            'input' => 'select',
            'data' => array(
                'type' => 'newsletter_click',
                'id' => '1'
            ),
            'optgroup' => 'Azioni',
            'operators' => array('email'),
            'values' => $campaign_emails_values
        );

        $filters[] = array(
            'id' => 'newsletter_not_click',
            'label' => 'Non ha cliccato',
            'icon' => 'fa fa-mouse-pointer',
            'type' => 'integer',
            'input' => 'select',
            'data' => array(
                'type' => 'newsletter_click',
                'id' => '0'
            ),
            'optgroup' => 'Azioni',
            'operators' => array('email'),
            'values' => $campaign_emails_values
        );

        /* FILTRI TRACKING */
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM tracking__projects") as $tracking) {
            $azioni_tracking = (json_decode($tracking['actions_value']) ? json_decode($tracking['actions_value'], true) : array());

            if($azioni_tracking) {
                $tracking_values = array();
                $actions_details = (new \MSFramework\tracking())->getTrackingActionsInfo();

                foreach($azioni_tracking as $action_id => $action_values) {

                    $recursiveArr = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($action_values), \RecursiveIteratorIterator::SELF_FIRST);
                    foreach ($recursiveArr as $key => $value) {
                        if ($key === 'event_name' && !empty($value)) {
                            $tracking_values[$value] = array(
                                'value' => $value,
                                'icon' => $actions_details[$action_id]['fa-icon']
                            );
                        }
                    }
                }
                if($tracking_values) {
                    foreach($tracking_values as $track_value) {
                        $filters[] = array(
                            'id' => 'tracking_' . $tracking['id'] . '_' . str_replace(array(' '), array(''), $track_value['value']),
                            'label' => $track_value['value'],
                            'icon' => 'fa ' . $track_value['icon'],
                            'data' => array(
                                'type' => 'tracking',
                                'id' => $tracking['id'],
                                'event' => $track_value['value']
                            ),
                            'type' => 'string',
                            'input' => 'text',
                            'optgroup' => 'Tracking ' . $tracking['name'],
                            'operators' => array('received', 'not_received', 'with_value', 'with_different_value', 'not_with_value')
                        );
                    }
                }
            }
        }

        return $filters;

    }

    /**
     * Ottiene gli operatori utilizzate dal query builder
     */
    public function getConditionsQueryOperators() {

        $operators = array (
            array (
                'type' => 'email',
                'nb_inputs' => 1,
                'multiple' => false,
                'apply_to' => array (
                    'string'
                )
            ),
            array (
                'type' => 'received',
                'nb_inputs' => 0,
                'multiple' => false,
                'apply_to' => array (
                    'string'
                )
            ),
            array (
                'type' => 'not_received',
                'nb_inputs' => 0,
                'multiple' => false,
                'apply_to' => array (
                    'string'
                )
            ),
            array (
                'type' => 'with_value',
                'nb_inputs' => 1,
                'multiple' => false,
                'apply_to' => array (
                    'string'
                )
            ),
            array (
                'type' => 'with_different_value',
                'nb_inputs' => 1,
                'multiple' => false,
                'apply_to' => array (
                    'string'
                )
            ),
            array (
                'type' => 'not_with_value',
                'nb_inputs' => 1,
                'multiple' => false,
                'apply_to' => array (
                    'string'
                )
            ),
            array ('type' => 'contains'),
            array ('type' => 'equal'),
            array ('type' => 'not_equal'),
            array ('type' => 'less'),
            array ('type' => 'greater'),
            array ('type' => 'between'),
            array ('type' => 'is_empty'),
            array ('type' => 'is_not_empty'),
            array (
                'type' => 'da almeno',
                'nb_inputs' => 1,
                'multiple' => false,
                'apply_to' => array (
                    'string'
                )
            )
        );

        return $operators;

    }

    public function getPassingConditionUsers($options) {
        $users_ids = array();

        // Cerco se i dati sono stati ottenuti in precedenza per evitare richieste superflue
        $cache_key = base64_encode(json_encode($options));
        if(isset($this->conditions_cache[$cache_key])) {
            return $this->conditions_cache[$cache_key];
        }

        $condition = $options['condition'];
        foreach($options['rules'] as $option) {

            if(isset($option['condition'])) {
                $tmp_ids = $this->getPassingConditionUsers($option);
            } else {
                $tmp_ids = $this->getConditionQuery($option['data']['type'], $option['value'], ($option['data']['id'] == "all" ? "" : $option['data']['id']), $option['operator'], $option['data']);
            }

            if(empty($users_ids)) {
                $users_ids = $tmp_ids;
            } else if($condition == 'AND') {
                $users_ids = array_intersect($users_ids, $tmp_ids);
            } else {
                $users_ids = array_merge($users_ids, $tmp_ids);
            }

        }

        $this->conditions_cache[$cache_key] = $users_ids;

        return array_filter($users_ids);
    }

    private function getConditionQuery($type, $value, $ref_id = "", $operator = "", $data = array()) {

        if($value == "true") {
            $value = 1;
        } else if($value == "false") {
            $value = 0;
        }

        if($type !== 'sistema') {
            $this->conditionNeedUserLogin = true;
        }

        $sql_match = array(
            'sistema' => function($value, $col = "", $operator = "", $data = array()) {

                $operators_match = array(
                    'greater' => '>',
                    'less' => '<',
                    'equal' => '==',
                    'not_equal' => '!=',
                    'between' => '!='
                );

                if(!in_array($operator, array_keys($operators_match))) {
                    return "SELECT ''";
                }

                $date_match = false;

                if($col === 'data') {
                    if(is_array($value)) {
                        foreach($value as $k => $v) {
                            if(\DateTime::createFromFormat('d/m/Y', $v)) {
                                $value[$k] = \DateTime::createFromFormat('d/m/Y', $v)->format('Y-m-d');
                            }
                        }

                        if(
                            (strtotime($value[0]) <= strtotime(date('Y-m-d')) && strtotime($value[1]) >= strtotime(date('Y-m-d')))
                            ||
                            (strtotime($value[0]) >= strtotime(date('Y-m-d')) && strtotime($value[1]) <= strtotime(date('Y-m-d')))
                        ) {
                            $date_match = true;
                        }

                    } else if(\DateTime::createFromFormat('d/m/Y', $value)) {
                        $value = \DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
                        eval('$date_match = strtotime(date("Y-m-d"))' . $operators_match[$operator] . ' strtotime($value);');
                    }
                } else {

                    if(is_array($value)) {
                        foreach($value as $k => $v) {
                            $value[$k] = str_replace(':', '.', $value[$k]);
                        }

                        $timearray = explode(":", date('H:i'));
                        $timeint = (int)$timearray[0] + ((int)$timearray[1] / 60);
                        if ($timeint < 1) $timeint += 24;

                        $fromarray = explode(":", $value[0]);
                        $fromint = (int)$fromarray[0];

                        $toarray = explode(":", $value[1]);
                        $toint = (int)$toarray[0];

                        if (($fromint > 12 && $fromint > $toint) || $fromint == 0) {
                            $toint += 24;
                            if ($timeint <= 12) $timeint += 24;
                        }

                        if ($timeint >= $fromint && $timeint <= $toint) {
                            $date_match = true;
                        }

                    } else {
                        $value = (float)str_replace(':', '.', $value);
                        eval('$date_match = (float)date("H.i") ' . $operators_match[$operator] . ' ' . $value . ';');

                    }

                }

                if($date_match) {
                    if($this->ignoreMandatoryCustomers) {
                        return "SELECT CONCAT('" . $_SERVER['REMOTE_ADDR'] . "', ', ', (SELECT GROUP_CONCAT(DISTINCT customers.id SEPARATOR ', ') as ids FROM customers))";
                    } else {
                        return "SELECT GROUP_CONCAT(DISTINCT customers.id SEPARATOR ', ') FROM customers";
                    }
                } else {
                    return "SELECT ''";
                }
            },
            'anagrafica' => function($value, $col = "", $operator = "", $data = array()) {

                $where = array();

                $accepted_cols = array(
                    'registration_date', 'data_nascita', 'sesso', 'citta', 'provincia', 'telefono_cellulare', 'telefono_casa', 'last_login', 'TIME(customers.last_login)', 'CONCAT(customers.indirizzo,customers.citta,customers.provincia)'
                );

                $operators_match = array(
                    'greater' => '>',
                    'less' => '<',
                    'equal' => '=',
                    'not_equal' => '!=',
                    'between' => '!=',
                    'is_empty' => '=',
                    'is_not_empty' => '!=',
                    'contains' => 'LIKE'
                );

                if(!in_array($operator, array_keys($operators_match)) || !in_array($col, $accepted_cols)) {
                    return "SELECT ''";
                }

                if(in_array($col, array('data_nascita', 'registration_date', 'last_login'))) {

                    $date_format = array(
                        'data_nascita' => "FROM_UNIXTIME(customers.data_nascita, '%Y-%m-%d')",
                        'registration_date' => "DATE_FORMAT(customers.registration_date, '%Y-%m-%d')",
                        'last_login' => "DATE_FORMAT(customers.last_login, '%Y-%m-%d')",
                    );

                    $where[] = "customers." . $col . " != ''";

                    $col = $date_format[$col];

                    if(is_array($value)) {
                        foreach($value as $k => $v) {
                            if(\DateTime::createFromFormat('d/m/Y', $v)) $value[$k] = \DateTime::createFromFormat('d/m/Y', $v)->format('Y-m-d');
                            else return "SELECT ''";
                        }

                    } else {
                        if(\DateTime::createFromFormat('d/m/Y', $value)) $value = \DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
                        else return "SELECT ''";
                    }
                }

                if(in_array($operator, array('is_empty', 'is_not_empty')) || $value === 'N/A') {
                    $value = '';
                }

                if(strpos($col, 'TIME') === 0) {
                    if(!is_array($value)) $value = array($value);

                    foreach($value as $k => $value_to_check) {
                        $value[$k] = str_replace(array('.', ','), ':', $value_to_check);
                        if(!preg_match('/[0-9]{1,2}+:[0-9]{1,2}/m', $value[$k])) {
                            if(is_numeric($value[$k]) && (int)$value[$k] < 24) {
                                $value[$k] = (int)$value[$k] . ':00';
                            } else {
                                return "SELECT ''";
                            }
                        }
                        $exploded_time = explode(':', $value[$k]);
                        $value[$k] = str_pad($exploded_time[0], 2, '0', STR_PAD_LEFT) . ':' . str_pad($exploded_time[1], 2, '0', STR_PAD_LEFT);
                    }

                    if(count($value) == 1) {
                        $value = $value[0];
                    }
                }

                if(strpos($col, 'customers.') === false) {
                    $col = 'customers.' . $col;
                }

                if($operator == 'between') {
                    if(strpos($col, 'TIME') !== false) {
                        $exploded_time = array(
                            explode(':', $value[0]),
                            explode(':', $value[1])
                        );

                        if($exploded_time[0][0] > $exploded_time[1][0]) {
                            $where[] = '(' . $col . " BETWEEN " . $this->MSFrameworkDatabase->quote($value[0]) . " AND '00:00' OR " . $col . " BETWEEN '00:00' AND " . $this->MSFrameworkDatabase->quote($value[1]) . ")";
                        } else {
                            $where[] = $col . " BETWEEN " . $this->MSFrameworkDatabase->quote($value[0]) . " AND " . $this->MSFrameworkDatabase->quote($value[1]);
                        }

                    } else {
                        $where[] = $col . " BETWEEN " . $this->MSFrameworkDatabase->quote($value[0]) . " AND " . $this->MSFrameworkDatabase->quote($value[1]);
                    }
                } else if($operator == 'contains') {
                    $where[] = $col . " LIKE " . $this->MSFrameworkDatabase->quote('%' . $value . '%');
                } else {
                    $where[] = $col . " " . $operators_match[$operator] . " " . $this->MSFrameworkDatabase->quote($value);
                }

                return "SELECT GROUP_CONCAT(DISTINCT customers.id SEPARATOR ', ') FROM customers WHERE " . implode(' AND ', $where);
            },
            'ecommerce' => function($value, $type = "", $operator = "", $data = array()) {

                $where = array();

                if($type == 'carrello_abbandonato') {
                    if($value) {
                        $where[] = "customers.id IN (SELECT ecommerce_users_extra.id FROM ecommerce_users_extra WHERE ecommerce_users_extra.id = customers.id AND LENGTH(ecommerce_users_extra.carrello) > 5)";
                    } else {
                        $where[] = "customers.id NOT IN (SELECT ecommerce_users_extra.id FROM ecommerce_users_extra WHERE ecommerce_users_extra.id = customers.id AND LENGTH(ecommerce_users_extra.carrello) > 5)";
                    }
                    return "SELECT GROUP_CONCAT(DISTINCT customers.id SEPARATOR ', ') FROM customers WHERE " . implode(' AND ', $where);
                } else if($type == 'ordini_effettuati') {
                    $where[] = "customers.id " . ($value == "1" ? "IN" : "NOT IN") . " (SELECT ecommerce_orders.user_id FROM ecommerce_orders)";
                    return "SELECT GROUP_CONCAT(DISTINCT customers.id SEPARATOR ', ') FROM customers WHERE " . implode(' AND ', $where);
                }

                return "SELECT ''";

            },
            'subscriptions' => function($value, $type = "", $operator = "", $data = array()) {

                $where = array();

                $operators_match = array(
                    'greater' => '>',
                    'less' => '<',
                    'equal' => '=',
                    'not_equal' => '!=',
                    'between' => '!=',
                    'is_empty' => '=',
                    'is_not_empty' => '!=',
                    'contains' => 'LIKE'
                );

                if(!in_array($operator, array_keys($operators_match))) {
                    return "SELECT ''";
                }

                if($type == 'subscriptions_status') {

                    if($value) {
                        $where[] = "customers.id IN (SELECT subscriptions__subscribers.cliente FROM subscriptions__subscribers WHERE subscriptions__subscribers.cliente = customers.id AND subscriptions__subscribers.data_fine > NOW())";
                    } else {
                        $where[] = "customers.id NOT IN (SELECT subscriptions__subscribers.cliente FROM subscriptions__subscribers WHERE subscriptions__subscribers.cliente = customers.id AND subscriptions__subscribers.data_fine > NOW())";
                    }

                    return "SELECT GROUP_CONCAT(DISTINCT customers.id SEPARATOR ', ') FROM customers WHERE " . implode(' AND ', $where);

                } else if($type == 'subscriptions_start_date' || $type == 'subscriptions_end_date') {

                    if(is_array($value)) {
                        foreach($value as $k => $v) {
                            if(\DateTime::createFromFormat('d/m/Y', $v)) $value[$k] = \DateTime::createFromFormat('d/m/Y', $v)->format('Y-m-d');
                            else return "SELECT ''";
                        }

                    } else {
                        if(\DateTime::createFromFormat('d/m/Y', $value)) $value = \DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
                        else return "SELECT ''";
                    }

                    if($type === "subscriptions_start_date") {
                        $col = 'DATE_FORMAT(subscriptions__subscribers.data_inizio, "%Y-%m-%d")';
                    } else {
                        $col = 'DATE_FORMAT(subscriptions__subscribers.data_fine, "%Y-%m-%d")';
                    }

                    if($operator == 'between') {
                        $where[] = $col . " > " . $this->MSFrameworkDatabase->quote($value[0]) . " AND " . $col . " < " . $this->MSFrameworkDatabase->quote($value[1]);
                    } else {
                        $where[] = $col . " " . $operators_match[$operator] . " " . $this->MSFrameworkDatabase->quote($value);
                    }

                    return "SELECT GROUP_CONCAT(DISTINCT customers.id SEPARATOR ', ') FROM customers WHERE customers.id IN (SELECT subscriptions__subscribers.cliente FROM subscriptions__subscribers WHERE subscriptions__subscribers.cliente = customers.id AND " . implode(' AND ', $where) . ")";
                } else if($type === 'subscriptions_remaining_days') {

                    $col = 'DATEDIFF(data_fine, NOW())';

                    if($operator == 'between') {
                        $where[] = $col . " > " . $this->MSFrameworkDatabase->quote($value[0]) . " AND " . $col . " < " . $this->MSFrameworkDatabase->quote($value[1]);
                    } else {
                        $where[] = $col . " " . $operators_match[$operator] . " " . $this->MSFrameworkDatabase->quote($value);
                    }

                    return "SELECT GROUP_CONCAT(DISTINCT customers.id SEPARATOR ', ') FROM customers WHERE customers.id IN (SELECT subscriptions__subscribers.cliente FROM subscriptions__subscribers WHERE subscriptions__subscribers.cliente = customers.id AND " . implode(' AND ', $where) . ")";

                }

                return "SELECT ''";

            },
            'newsletter_open' => function($email_id, $view = "", $operator = "", $data = array()) {


                $where = array();
                $where[] = "newsletter__emails_destinatari_status.id_recipient IN (SELECT id FROM customers)";

                if($view) {
                    $where[] = "newsletter__emails_destinatari_status.status > 1";
                } else {
                    $where[] = "newsletter__emails_destinatari_status.status = 0";
                }

                if($email_id) {
                    $email_col = 'automation_id';
                    if(is_numeric($email_id)) {
                        $email_col = 'id';
                    }
                    $where[] = "newsletter__emails_destinatari_status.id_email IN (SELECT id FROM newsletter__emails WHERE newsletter__emails.$email_col = " . $this->MSFrameworkDatabase->quote($email_id) . ")";
                }

                return "SELECT GROUP_CONCAT(DISTINCT newsletter__emails_destinatari_status.id_recipient SEPARATOR ', ') FROM newsletter__emails_destinatari_status WHERE " . implode(' AND ', $where);
            },
            'newsletter_click' => function($email_id, $view = "", $operator = "", $data = array()) {

                $where = array();
                $where[] = "newsletter__emails_destinatari_status.id_recipient IN (SELECT id FROM customers)";

                if($view) {
                    $where[] = "newsletter__emails_destinatari_status.clicked > 0";
                } else {
                    $where[] = "newsletter__emails_destinatari_status.clicked = 0";
                }

                if($email_id) {
                    $email_col = 'automation_id';
                    if(is_numeric($email_id)) {
                        $email_col = 'id';
                    }
                    $where[] = "newsletter__emails_destinatari_status.id_email IN (SELECT id FROM newsletter__emails WHERE newsletter__emails.$email_col = " . $this->MSFrameworkDatabase->quote($email_id) . ")";
                }

                return "SELECT GROUP_CONCAT(DISTINCT newsletter__emails_destinatari_status.id_recipient SEPARATOR ', ') FROM newsletter__emails_destinatari_status WHERE " . implode(' AND ', $where);
            },
            'tracking' => function($value, $project_id, $operator = "", $data = array()) {
                Global $MSFrameworkDatabase;

                $where = array();

                if(!$this->ignoreMandatoryCustomers) $where[] = "tracking__stats.user_ref IN (SELECT id FROM customers)";

                $where[] = "tracking__stats.track_event LIKE " . $MSFrameworkDatabase->quote($data['event']) . "";

                $where[] = "tracking__stats.project_id = " . (int)$project_id . "";

                if($operator == 'with_value' || $operator == 'not_with_value') {
                    $where[] = "tracking__stats.track_ref = " . $MSFrameworkDatabase->quote($value) . "";
                } else if($operator == 'with_different_value') {
                    $where[] = "tracking__stats.track_ref NOT LIKE " . $MSFrameworkDatabase->quote($value) . "";
                }

                $where_sql = implode(' AND ',  $where);

                if($operator == 'not_received' || $operator == 'not_with_value') {
                    $where_sql = 'tracking__stats.user_ref NOT IN (SELECT tracking__stats.user_ref FROM tracking__stats WHERE ' . $where_sql . ')';
                }

                return "SELECT GROUP_CONCAT(DISTINCT IF(user_ref > 0, user_ref, user_ip) SEPARATOR ', ') FROM tracking__stats WHERE " . $where_sql;
            }
        );

        $this->MSFrameworkDatabase->query("SET SESSION group_concat_max_len = 100000000;");

        $results = array_values($this->MSFrameworkDatabase->getAssoc($sql_match[$type]($value, $ref_id, $operator, $data), array(), true))[0];

        return ($results ? explode(', ', $results) : array());
    }
}
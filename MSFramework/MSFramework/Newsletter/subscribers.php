<?php
/**
 * MSFramework
 * Date: 10/03/18
 */

namespace MSFramework\Newsletter;

class subscribers {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        Global $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkCustomers = new \MSFramework\customers();

        $this->Campaigns = new \MSFramework\Newsletter\campaigns();
    }

    /**
     * Iscrive l'utente alla newsletter (se non è già iscritto)
     *
     * @param $mail string L'email da iscrivere
     * @param $nome string Il nome (e cognome) dell'utente
     * @param $tag mixed Uno o più tag sotto forma di ID (se presenti nel DB) o stringhe
     * @throws \phpmailerException
     *
     * @return bool
     */
    public function subscribe($mail, $nome, $tag = array('iscrizione_form')) {

        if(!is_array($tag)) $tag = array($tag);

        if(!$this->checkIfAlreadySubscribed($mail)) {
            if(filter_var($mail, FILTER_VALIDATE_EMAIL) && $nome != "") {
                $auth = ($this->MSFrameworkCMS->getCMSData('newsletter_settings')['disable_newsletter__doi'] == "1" ? "" : sha1(time()));
                $this->subscribeNewEmail($nome, $mail, $tag, $auth);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Invia la conferma di iscrizione all'utente, attraverso la quale lo stesso potrà anche confermare il suo indirizzo email
     *
     * @param $mail L'indirizzo email dell'utente
     * @param $auth L'auth code relativo all'utente
     * @throws \phpmailerException
     */
    private function sendConfirmationMail($mail, $auth) {
        (new \MSFramework\emails())->sendMail('newsletter-confirm', array(
            "dest" => $mail,
            "auth" => $auth,
        ));
    }

    /**
     * Prova ad attivare l'iscrizione alla newsletter corrispondente all'auth specificato
     *
     * @param $auth L'auth da attivare
     *
     * @return bool
     */
    public function confirmAuth($auth) {
        global $firephp;

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT id FROM newsletter__destinatari WHERE auth = :auth", array(":auth" => $auth), true);

        if($r['id'] != "") {
            $this->MSFrameworkDatabase->pushToDB("UPDATE newsletter__destinatari SET auth = '', active = 1 WHERE id = :id", array(":id" => $r['id']));
            $this->Campaigns->syncCampaignRecipients();
            return true;
        }

        return false;
    }

    /**
     * Determina se un dato indirizzo email ha già effettuato l'iscrizione alla newsletter
     *
     * @param $mail L'email da controllare
     *
     * @return bool
     */
    public function checkIfAlreadySubscribed($mail) {
        if($this->MSFrameworkDatabase->getCount("SELECT id FROM newsletter__destinatari WHERE id IN (SELECT id FROM customers WHERE email = :email) AND data_inserimento < NOW() - INTERVAL 1 SECOND", array(":email" => $mail)) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Questa funzione permette di ottenere i dettagli di un iscritto alla newsletter tramite la sua email
     *
     * @param string $mail Email dell'iscritto
     * @param string $fields Passando questo parametro è possibile selezionare solo le colonne desiderate.
     *
     * @return mixed
     */
    public function getSubscriberByMail($mail, $fields = "*") {
        return $this->MSFrameworkDatabase->getAssoc("SELECT " . $fields . " FROM newsletter__destinatari WHERE id = (SELECT id FROM customers WHERE email = :email)", array(":email" => $mail), true);
    }

    /**
     * Questa funzione permette di inserire un nuovo iscritto all'interno della newsletter
     *
     * @param string $name Il nome dell'utente
     * @param string $mail L'email dell'utente
     * @param mixed $tags Passando questo campo si può cambiare il tag predefinito da associare all'utente (Si può passare un array contenente gli ID, o il nome dello slug)
     * @param mixed $auth L'eventuale codice di autenticazione utente
     * @throws \phpmailerException
     *
     * @return mixed
     */
    public function subscribeNewEmail($name, $mail, $tags = 'iscrizione_form', $auth = '') {

        $check_customer = $this->MSFrameworkCustomers->getCustomerDataByEmail($mail);

        if($check_customer) {
            $customer_data = $check_customer;
        } else {

            $cognome = '';

            // Provo a dividere il nome se è stato passato un solo campo
            $name_exploded = explode(' ', $name);

            if(count($name_exploded) > 1) {
                $cognome = end($name_exploded);
                $name = str_replace(' ' . $cognome, '', $name);
            }

            $user_id = $this->MSFrameworkCustomers->registerUser($name, $cognome, $mail, '', '', 7);
            $customer_data = $this->MSFrameworkCustomers->getCustomerDataFromDB($user_id);
        }

        return $this->subscribeCustomer($customer_data, $tags, $auth);
    }

    /**
     * Questa funzione permette di inserire un nuovo iscritto all'interno della newsletter
     *
     * @param integer|array $customer_info L'array con le info del cliente (o l'id)
     * @param mixed $tags Passando questo campo si può cambiare il tag predefinito da associare all'utente (Si può passare un array contenente gli ID, o il nome dello slug)
     * @param mixed $auth L'eventuale codice di autenticazione utente
     * @throws \phpmailerException
     *
     * @return mixed
     */
    public function subscribeCustomer($customer_info, $tags = 'iscrizione_form', $auth = '') {
        Global $MSHooks;

        if(is_numeric($customer_info)) {
            $customer_info = $this->MSFrameworkCustomers->getCustomerDataFromDB($customer_info);
        }

        $this->MSFrameworkDatabase->query("INSERT IGNORE INTO newsletter__destinatari (id, auth, active) VALUES (:id, :auth, :is_active)", array(':id' => $customer_info['id'], ':auth' => $auth, ':is_active' => (empty($auth) ? 1 : 0 )));

        if(!empty($auth)) {
            $this->sendConfirmationMail($customer_info['email'], $auth);
        }

        if($customer_info) {

            $MSHooks->action('newsletter', 'subscribe')->run(array('user_id' => $customer_info['id'], 'user_email' => $customer_info['email']));

            if(!is_array($tags)) $tags = array($tags);

            foreach($tags as $tag)
            {
                if(is_numeric($tag))
                {
                    $tag_id = $tag;
                }
                else
                {
                    $tag_id = $this->MSFrameworkDatabase->getAssoc("SELECT id FROM newsletter__tag WHERE nome LIKE :tag", array(":tag" => $tag), true);
                    if (!$tag_id) {
                        $this->MSFrameworkDatabase->pushToDB("INSERT INTO newsletter__tag (nome) VALUES (:nome)", array(":nome" => $tag));
                        $tag_id = $this->MSFrameworkDatabase->lastInsertId();
                    } else {
                        $tag_id = $tag_id['id'];
                    }
                }

                $this->subscribeToTag($tag_id, $customer_info['id']);
            }

        }

        return true;
    }

    /**
     * Iscrive un utente ad una lista, utilizzando l'ID del destinatario
     *
     * @param $tags mixed La lista (o le liste, sotto forma di array) alla quale iscrivere l'utente
     * @param $destinatario L'ID dell'utente
     *
     * @return bool
     */
    public function subscribeToTag($tags, $destinatario) {
        if($tags == "" || $destinatario == "") {
            return false;
        }

        if(!is_array($tags)) $tags = array($tags);

        $result = false;

        foreach($tags as $tag) {
            if (!$this->checkIfAlreadySubscribedToTag($tag, $destinatario)) {
                $this->MSFrameworkDatabase->pushToDB("INSERT INTO newsletter__tag_destinatari (tag, destinatario, creation_time) VALUES (:tag, :destinatario, :creation_time)", array(":tag" => $tag, ":destinatario" => $destinatario, ":creation_time" => time()));
            }
        }

        $this->Campaigns->syncCampaignRecipients();

        return $result;
    }

    /**
     * Verifica se un determinato utente (ID) è iscritto ad una lista
     *
     * @param $tag La lista alla quale iscrivere l'utente
     * @param $destinatario L'ID dell'utente
     *
     * @return bool|null
     */
    public function checkIfAlreadySubscribedToTag($tag, $destinatario) {
        if($tag == "" || $destinatario == "") {
            return null;
        }

        if($this->MSFrameworkDatabase->getCount("SELECT id FROM newsletter__tag_destinatari WHERE destinatario = :dest AND tag = :tag", array(":dest" => $destinatario, ":tag" => $tag)) == 0) {
            return false;
        } else {
            return true;
        }
    }
}
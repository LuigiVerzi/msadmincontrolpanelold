<?php
/**
 * MSFramework
 * Date: 10/03/18
 */

namespace MSFramework\Newsletter;

class emails {

    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase;
        Global $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkCustomers = new \MSFramework\customers();

        // Salvo i template qui per evitare troppe chiamate
        $this->emailsData = array();
        $this->emailClass = new \MSFramework\emails();
    }

    /**
     * Restituisce un array associativo (ID Email => Dati Email) con i dati relativi alle emails.
     *
     * @param mixed $id L'ID dell'email o un array contenente gli IDS delle varie email
     *
     * @return array
     */
    public function getEmailDetails($id) {

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        $ary_to_return = array();
        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM newsletter__emails WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $lista_mittenti = $this->getMittenteDetails();

            if((int)$r['mittente'] > 0 && isset($lista_mittenti[$r['mittente']])) {
                $info_mittente = $lista_mittenti[$r['mittente']];
            } else {
                $info_mittente = array('nome' => SW_NAME, 'email' => PRODUCER_EMAIL, 'reply_to' => FRAMEWORK_NOREPLY_MAIL['mail']);
            }

            if(empty($info_mittente['reply_to']) || !filter_var($info_mittente['reply_to'], FILTER_VALIDATE_EMAIL)) {
                $info_mittente['reply_to'] = FRAMEWORK_NOREPLY_MAIL['mail'];
            }

            $r['send_details'] = array(
                'titolo' => $r['nome'],
                'html' => $r['custom_layout_html'],
                'txt' => $r['custom_layout_txt'],
                'mittente' => $info_mittente,
                'attachments' => (json_decode($r['attachments']) ? json_decode($r['attachments']) : array()),
            );

            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;

    }

    /**
     * Invia l'email all'utente
     * @param integer $user_id L'id del destinatario
     * @param integer $email_id L'id dell'email da inviare
     */
    public function sendEmail($user_id, $email_id) {

        $emailDetails = $this->getEmailDetails($email_id)[$email_id];
        $userDetails = $this->MSFrameworkCustomers->getCustomerDataFromDB($user_id);

        if(!$emailDetails || !$userDetails) return false;

        $preparedValues = $this->prepareEmailForUser($emailDetails, $userDetails);

        $emailDetails = $preparedValues[0];

        /* IMPOSTO I DATI SMTP DEL SERVER */
        $smtp_server = $this->emailClass->getSMTPDetails($emailDetails['smtp'], true)['data'];

        if (!empty($smtp_server['smtpsecure'])) {
            $this->emailClass->mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
        }

        $this->emailClass->mail->Host = $smtp_server['host'];
        $this->emailClass->mail->Port = $smtp_server['port'];
        $this->emailClass->mail->SMTPAuth = true;
        $this->emailClass->mail->AuthType = 'LOGIN';
        $this->emailClass->mail->Username = $smtp_server['login'];
        $this->emailClass->mail->Password = $smtp_server['password'];

        $this->emailClass->mail->SMTPSecure = $smtp_server['smtpsecure'];
        $this->emailClass->mail->Sender = $smtp_server['username'];  // bounce to

        $this->emailClass->mail->SMTPKeepAlive = true;

        $this->emailClass->mail->setFrom($emailDetails['send_details']['mittente']['email'], $emailDetails['send_details']['mittente']['nome']);
        $reply_to = array('email' => $emailDetails['send_details']['mittente']['reply_to'], 'name' => $emailDetails['send_details']['mittente']['nome']);

        $attachments_array = array();
        foreach ($emailDetails['send_details']['attachments'] as $file) {
            $attachments_array[] = array(
                'path' => UPLOAD_MARKETING_AUTOMATIONS_FOR_DOMAIN . (int)$emailDetails['campaign_id'] . '/' . $file,
                'name' => $file
            );
        }

        $this->emailClass->mail->addCustomHeader("List-Unsubscribe",'<' . $emailDetails['send_details']['mittente']['email'] . '>, <' . $this->MSFrameworkCMS->getURLToSite(1) . 'ajax/mail/track/track.php?c=' . $emailDetails['id'] . '&r=' . $user_id . '&type=unsubscribe>');

        $return = $this->emailClass->sendCustomMail(
            $emailDetails['send_details']['titolo'],
            $emailDetails['send_details']['html'],
            array($userDetails['email']),
            $reply_to,
            $emailDetails['send_details']['txt'],
            $attachments_array,
            $preparedValues[1]
        );

        $email_unique_id = $this->emailClass->mail->getLastMessageID();

        if(!$return) {
            if ((stristr($this->emailClass->mail->ErrorInfo, 'recipients failed') || stristr($this->emailClass->mail->ErrorInfo, 'policy violation SMTP code: 550'))) {
                $this->MSFrameworkDatabase->pushToDB("UPDATE newsletter__destinatari SET active = 0, commenti = CONCAT('Indirizzo email non raggiungibile', ' ', commenti) WHERE id = :id", array(':id' => $user_id));
            } else if(stristr($this->emailClass->mail->ErrorInfo, 'SMTP connect()') || stristr($this->emailClass->mail->ErrorInfo, 'SMTP server error')) {
                return $return;
            }
        }

        $this->MSFrameworkDatabase->pushToDB("INSERT IGNORE INTO newsletter__emails_destinatari_status (id_email, id_recipient, status, msg_id) VALUES (:id_email, :id_recipient, :status, :msg_id)", array(':id_email' => $emailDetails['id'], ':id_recipient' => $user_id, ':status' => ($return ? 1 : 0), ':msg_id' => $email_unique_id));

        return $return;

    }

    /**
     * Prepara il contenuto dell'email priam di inviarla all'utente
     * @param array $emailDetails I dettagli dell'email
     * @param array $userDetails I dettagli dell'utente
     *
     * @return array I valori dell'email sostituiti e l'array di sostituzione
     */
    public function prepareEmailForUser($emailDetails, $userDetails) {


        $newsletter_settings = $this->MSFrameworkCMS->getCMSData('newsletter_settings');
        $backend_url = $this->MSFrameworkCMS->getURLToSite(true);

        /* SOSTITUISCO I LINK */
        $emailDetails['send_details']['html'] = str_replace(
            'href="',
            'href="' . $backend_url . 'ajax/mail/track/track.php?c=' . $emailDetails['id'] . '&r=' . $userDetails['id'] . '&type=click&link=',
            $emailDetails['send_details']['html']
        );

        /* AGGIUNGO IL LINK UNSUBSCRIBE DI DEFAULT */
        $emailDetails['send_details']['html'] .= '<p style="display: block;text-align: center;"><span style="color: #999999;"><a style="color: #999999; font-size: 10px;" href="{unsubscribe}">Non voglio pi&ugrave; ricevere email.</a></span></p>';

        /* AGGIUNGO IL PIXEL DI TRACKING */
        if ($newsletter_settings['activate_monitoring'] != "0") {
            $emailDetails['send_details']['html'] .= '<img src="' . $backend_url . 'ajax/mail/track/track.php?c=' . $emailDetails['id'] . '&r=' . $userDetails['id'] . '&type=view' . '" width="0" height="0">';
        }

        $replaces_values = array(
            $userDetails['nome'] . ' ' . $userDetails['cognome'],
            $userDetails['email'],
            $this->MSFrameworkDatabase->getAssoc("SELECT commenti FROM newsletter__destinatari WHERE id = :user_id", array(':user_id' => $userDetails['id']), true)['commenti'],
            $backend_url . 'ajax/mail/track/track.php?c=' . $emailDetails['id'] . '&r=' . $userDetails['id'] . '&type=unsubscribe',
            $browserview_link = $backend_url . 'ajax/mail/track/browserview.php?c=' . $emailDetails['id'] . '&r=' . $userDetails['id'],
            $emailDetails['send_details']['titolo'],
            $emailDetails['send_details']['mittente']['nome'],
            $emailDetails['send_details']['mittente']['email'],
            date("Y"),
            date("m"),
            date("d"),
        );

        $replace_array = array();
        foreach($this->getShortcodeList() as $sKey => $shortcode) {
            $replace_array[$shortcode] = $replaces_values[$sKey];
        }

        return array($emailDetails, $replace_array);

    }

    /**
     * Restituisce un array associativo (ID Mittente => Dati Mittente) con i dati relativi ai mittenti.
     *
     * @param $id L'ID del mittente (stringa) o dei mittenti (array) richiesti (se vuoto, vengono recuperati i dati di tutti i mittenti)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getMittenteDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = "";
        $append_where_ary = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM newsletter__mittenti WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ")", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Restituisce un array associativo (ID Template => Dati Template) con i dati relativi ai template.
     *
     * @param $id L'ID del template (stringa) o dei template (array) richiesti (se vuoto, vengono recuperati i dati di tutti i template)
     * @param string $fields I campi da prelevare dal DB
     *
     * @return array
     */
    public function getTemplateDetails($id = "", $fields = "*") {
        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        if($fields != "*" && !strstr($fields, "id")) {
            $fields .= ", id";
        }

        $append_where = "";
        $append_where_ary = array();

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT $fields FROM newsletter__template WHERE id != '' $append_where AND (" . $same_db_string_ary[0] . ")", array_merge($append_where_ary, $same_db_string_ary[1])) as $r) {
            $ary_to_return[$r['id']] = $r;
        }

        return $ary_to_return;
    }

    /**
     * Ottiene la lista degli shortcodes utilizzabili
     *
     * @param $get_description bool Se true ottiene anche la descrizione degli shortcodes
     *
     * @return array
     */
    public function getShortcodeList($get_description = false) {

        $tag_from = array(
            '{recipient_name}' => "Il nome del destinatario",
            '{recipient_email}' => "L'email del destinatario",
            '{recipient_description}' => "Eventuale descrizione allegata al cliente",
            '{unsubscribe}' => "Il link per disiscriversi dalla email (Viene già incluso in tutte le email in automatico)",
            '{browser_view}' => "Il link per visualizzare l'email nel browser",
            '{campaign_name}' => "Il nome della campagna attuale",
            '{sender_name}' => "Il nome del mittente",
            '{sender_email}' => "L'indirizzo email del mittente",
            '{current_year}' => "L'anno nel quale l'email è stata inviata",
            '{current_month}' => "Il mese nel quale l'email è stata inviata",
            '{current_day}' => "Il giorno nel quale l'email è stata inviata"
        );

        if(!$get_description) {
            return array_keys($tag_from);
        }

        return $tag_from;
    }

    /**
     * Verifica se un particolare file è condiviso da più moduli (ad esempio, il modulo template, il modulo lista campagne ed il modulo campagne inviate) escudendo dalla ricerca il modulo/record corrente
     *
     * @param $filename string Il nome del file da controllare
     * @param $exclude_table string Il nome della tabella da escludere dalla ricerca
     * @param $exclude_id integer L'ID del record da escludere dalla ricerca
     *
     * @return bool
     */
    public function isSharedAttachment($filename, $exclude_table, $exclude_id) {
        if($filename == "" || $exclude_table == "" || $exclude_id == "") {
            return null;
        }

        $tables_to_check = array("newsletter__template", "newsletter__emails");
        foreach($tables_to_check as $table) {
            $exclude_id_str = "";
            $exclude_id_ary = array();

            if($exclude_table == $table) {
                $exclude_id_str = " AND id != :id ";
                $exclude_id_ary[':id'] = $exclude_id;
            }

            if($this->MSFrameworkDatabase->getCount("SELECT id FROM $table WHERE attachments LIKE :filename " . $exclude_id_str, array_merge($exclude_id_ary, array(":filename" => "%" . $filename . "%"))) != 0) {
                return true;
            }
        }

        return false;
    }

    /* ================ FUNZIONI DI TRACCIAMENTO ================ */

    /**
     * Tiene traccia dell'apertura della mail da parte dell'utente, aggiornando lo stato all'interno del DB e registrando l'IP dal quale è arrivata la richiesta
     *
     * @param $id_email L'id della campagna
     * @param $id_recipient L'id dell'utente
     */
    public function trackMailOpened($id_email, $id_recipient) {
        $this->MSFrameworkDatabase->pushToDB("UPDATE newsletter__emails_destinatari_status SET status = '2', date_open = :date_open WHERE id_email = :id_email AND id_recipient = :id_recipient", array(":id_email" => $id_email, ":id_recipient" => $id_recipient, ":date_open" => date("Y-m-d H:i:s", time())));
        $this->MSFrameworkDatabase->pushToDB("UPDATE newsletter__campagne_destinatari_status SET ip_receiver = :ip WHERE id_campaign = (SELECT campaign_id FROM newsletter__emails WHERE id = :id_email)", array(":ip" => $_SERVER["REMOTE_ADDR"], ":id_email" => $id_email));
    }

    /**
     * Tiene traccia del click sul link da parte dell'utente, aggiornando lo stato all'interno del DB e registrando l'IP dal quale è arrivata la richiesta. Poi redirecta l'utente al link richiesto
     *
     * @param $id_email L'id della campagna
     * @param $id_recipient L'id dell'utente
     * @param $redirect L'URL verso il quale effettuare il redirect
     */
    public function trackMailLinkClicked($id_email, $id_recipient, $redirect) {
        $r = $this->MSFrameworkDatabase->getAssoc("SELECT clicked, clicked_info FROM newsletter__emails_destinatari_status WHERE id_email = :id_email AND id_recipient = :id_recipient", array(":id_email" => $id_email, ":id_recipient" => $id_recipient), true);
        $clicked = (int)($r['clicked']) + 1;

        $clicked_info = (!empty($r['clicked_info']) ? explode(',', $r['clicked_info']) : array());
        $clicked_info[] = $redirect;
        $clicked_info = implode(',', $clicked_info);

        $this->MSFrameworkDatabase->pushToDB("UPDATE newsletter__emails_destinatari_status SET status = '2', clicked = :clicked, clicked_info = :clicked_info WHERE id_email = :id_email AND id_recipient = :id_recipient", array(":clicked" => $clicked, ":clicked_info" => $clicked_info, ":id_email" => $id_email, ":id_recipient" => $id_recipient));
        header("Location: " . $redirect);
    }

    /**
     * Tiene traccia del click sul link unsubscribe da parte dell'utente. A seconda delle impostazioni, cancella anche la mail dell'utente dal DB.
     *
     * @param $id_email L'id della campagna
     * @param $id_recipient L'id dell'utente
     */
    public function trackMailUnsubscribe($id_email, $id_recipient) {
        $this->MSFrameworkDatabase->pushToDB("UPDATE newsletter__emails_destinatari_status SET status = '3' WHERE id_email = :id_email AND id_recipient = :id_recipient", array(":id_email" => $id_email, ":id_recipient" => $id_recipient));
        $this->MSFrameworkDatabase->pushToDB("UPDATE newsletter__campagne_destinatari_status SET status = '0' WHERE id_recipient = :id_recipient", array(":id_recipient" => $id_recipient));

        if ($this->MSFrameworkCMS->getCMSData('newsletter_settings')['automatic_delete'] == '1') {
            $this->MSFrameworkDatabase->deleteRow('newsletter__destinatari', 'id', $id_recipient);
            $this->MSFrameworkDatabase->deleteRow('newsletter__tag_destinatari', 'destinatario', $id_recipient);
        } else {
            $this->MSFrameworkDatabase->pushToDB("UPDATE newsletter__destinatari SET active = '0' WHERE id = :id", array(":id" => $id_recipient));

        }
    }

    /**
     * Ottiene i dati della geolocalizzazione per un determninato REMOTE_ADDR e li memorizza nell'apposita tabella
     */
    public function trackGeoLocationMailOpened($id_email, $id_recipient) {
        $use_service = "geoplugin";

        if($use_service == "freegeoip") {
            $url = "http://freegeoip.net/json/".$_SERVER["REMOTE_ADDR"]; //questo servizio è stato sostituito da https://ipstack.com/ che, nella versione free, include solo 10000 richieste al mese.
        } else if($use_service == "geoplugin") {
            $url = "http://www.geoplugin.net/php.gp?ip=".$_SERVER["REMOTE_ADDR"];
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $result = curl_exec($ch);
        curl_close($ch);

        if($use_service == "freegeoip") {
            $json = json_decode($result, true);

            $ip_receiver = $json['ip'];
            $country_code = $json['country_code'];
            $country_name = $json['country_name'];
            $region = '';
            $region_code = $json['region_code'];
            $region_name = $json['region_name'];
            $continent_code = '';
            $city = $json['city'];
            $latitude = $json['latitude'];
            $longitude = $json['longitude'];
            $currency_code = '';
        } else if($use_service == "geoplugin") {
            $json = unserialize($result);

            $ip_receiver = $json['geoplugin_request'];
            $country_code = $json['geoplugin_countryCode'];
            $country_name = $json['geoplugin_countryName'];
            $region = $json['geoplugin_region'];
            $region_code = $json['geoplugin_regionCode'];
            $region_name = $json['geoplugin_regionName'];
            $continent_code = $json['geoplugin_continentCode'];
            $city = $json['geoplugin_city'];
            $latitude = $json['geoplugin_latitude'];
            $longitude = $json['geoplugin_longitude'];
            $currency_code = $json['geoplugin_currencyCode'];
        }

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB(array(
            'user_agent' => $_SERVER["HTTP_USER_AGENT"],
            'city' => $city,
            'region' => $region,
            'country_code' => $country_code,
            'country_name' => $country_name,
            'continent_code' => $continent_code,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'region_code' => $region_code,
            'region_name' => $region_name,
            'currency_code' => $currency_code,
        ), 'update');

        $this->MSFrameworkDatabase->pushToDB("UPDATE newsletter__campagne_destinatari_status SET $stringForDB[1] WHERE id_campaign = (SELECT campaign_id FROM newsletter__emails WHERE id = :id_email) AND id_recipient = :id_recipient", array_merge($stringForDB[0], array(":id_email" => $id_email, ":id_recipient" => $id_recipient)));
    }

    /**
     * Restituisce un array associativo (ID server SMTP => Dati server SMTP) con i dati relativi ai server SMTP.
     */
    public function checkBounced() {
        $MSFrameworkMailBox = new \MSFramework\mailBox();

        foreach ((new \MSFramework\emails())->getSMTPDetails('', true) as $server) {
            $serverDetails = $server['data'];

            if ($serverDetails['username'] == "" || $serverDetails['bmh_password'] == "" || $serverDetails['bmh_server'] == "") {
                continue;
            }

            $con = $MSFrameworkMailBox->getImapConnection($serverDetails);
            if (!$con) continue;

            $emails = imap_search($con,'ALL');
            if(!$emails) continue;

            $emails = array_slice(array_reverse($emails), 0, 500);

            /* for every email... */
            foreach($emails as $email_number) {
                $raw = imap_fetchbody($con, $email_number, "");
                if (!$raw) continue;

                $re = '/Message-ID: <([^>\n\s]+)>/m';
                if(preg_match_all($re, $raw, $matches, PREG_SET_ORDER, 0)) {
                    if($matches[0]) {
                        $this->MSFrameworkDatabase->pushToDB(" UPDATE newsletter__emails_destinatari_status SET bounced = '1' WHERE msg_id = :msg_id", array(":msg_id" => "<" . $matches[0][1] . ">"));
                    }
                }
            }
            imap_close($con);
            die();

        }
    }
}
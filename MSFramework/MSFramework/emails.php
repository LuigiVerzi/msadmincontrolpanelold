<?php
/**
 * MSFramework
 * Date: 17/02/18
 */

namespace MSFramework;

require_once(FRAMEWORK_ABSOLUTE_PATH . 'PHPMailer/phpmailer.php');
require_once(FRAMEWORK_ABSOLUTE_PATH . 'PHPMailer/phpmailer.smtp.php');

class emails {

    public $forceRealRecipients = false;
    private $ignoreErrors = false;

    public $noReplyInfo = array();
    private $noReplyReserveInfo = array();

    /**
     * emails constructor.
     *
     * @param bool $ignoreErrors Se impostato su true evita di tracciare eventuali errori relativi all'invio delle email
     * @param bool $use_framework_config Se impostato su true, utilizza la configurazione del framework (e non del singolo sito) per gestire l'invio
     *
     * @throws \phpmailerException
     */

    public function __construct($ignoreErrors = false, $use_framework_config = false) {
        global $MSFrameworkDatabase, $MSFrameworkCMS, $MSFrameworkSaaSBase, $MSFrameworkUsers, $MSFrameworki18n;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkSaaSBase = $MSFrameworkSaaSBase;
        $this->MSFrameworkSaaSEnvironments = new \MSFramework\SaaS\environments();
        $this->MSFrameworkUtils = new \MSFramework\utils();
        $this->MSFrameworkUsers = $MSFrameworkUsers;
        $this->MSFrameworki18n = $MSFrameworki18n;

        $this->producer_config = $this->MSFrameworkCMS->getCMSData('producer_config');
        $this->email_colors = json_decode($this->producer_config['templatemail_colors'], true);

        $this->useFrameworkConfig = $use_framework_config;
        $this->ignoreErrors = $ignoreErrors;

        $this->noReplyInfo = json_decode(MARKETINGSTUDIO_NOREPLY_MAIL, true);

        $this->noReplyReserveInfo = json_decode(MARKETINGSTUDIO_RESERVE_NOREPLY_MAIL, true);
        $this->mail = new \PHPMailer;

        $this->mail->isSMTP();
        $this->mail->isHTML(true);
        $this->mail->CharSet = 'UTF-8';

        $this->mail->Host = $this->noReplyInfo['host'];
        $this->mail->SMTPAuth = true;
        $this->mail->Username = $this->noReplyInfo['mail'];
        $this->mail->Password = $this->noReplyInfo['pass'];
        $this->mail->SMTPSecure = 'ssl';
        $this->mail->Port = 465;
        $this->mail->setFrom($this->noReplyInfo['mail'], SW_NAME);

        $this->forceRealRecipients = false;
    }

    /**
     * Ottiene la lista di template utilizzabili
     * I valori racchiusi tra le parentesi quadre [] verranno sostituiti con le medesime chiavi contenuti nell'array $params che solitamente viene passato nella funzione setTemplate
     *
     * @param string $key Se impostato ritorna esclusivamente il template selezionato
     * @param boolean $load_info Se impostato ritorna tutti i dati relativi all'email (versioni, testo, html etc)
     * @param boolean $not_overwrite Se impostato su TRUE ottiene solamente i template common
     *
     * @return array
     * @throws \phpmailerException
     */
    public function getTemplates($key = "", $load_info = false, $not_overwrite = false) {
        $email_classes = array(
            (new \MSFramework\MSAgency\emails()),
            (new \MSFramework\Ecommerce\emails()),
            (new \MSFramework\Camping\emails()),
            (new \MSFramework\Hotels\emails()),
            (new \MSFramework\Ticket\emails()),
            (new \MSFramework\Fatturazione\emails()),
            (new \MSFramework\TravelAgency\emails()),
            (new \MSFramework\Subscriptions\emails()),
            (new \MSFramework\Affiliations\emails()),
            (new \MSFramework\RealEstate\emails()),
            (new \MSFramework\Points\emails()),
        );

        $default_address = $this->MSFrameworkCMS->getCMSData("settings")['destination_address'];

        $ticket_beforeSend = function($params, $template_settings) {

            if(isset($params['attachments']) && count($params['attachments'])) {
                $ticket_details = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($params['id'])[$params['id']];
                $ticket_domain_uploads = new \MSFramework\uploads('ASSISTENZA_TICKET');
                foreach($params['attachments'] as $attachment) {
                    $this->mail->addAttachment($ticket_domain_uploads->path . $attachment, $attachment);
                }
            }

            $this->mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $ticket_formatted_id = str_pad($params['id'], 5, '0', STR_PAD_LEFT);
            $this->mail->MessageID = sprintf('<%s@%s>', base64_encode($ticket_formatted_id) . '//' . (isset($params['notification_email']) ? 0 : 1), 'framework360.it');
            $this->mail->Body = '<p style="text-align: right; padding: 0; margin: 0;"><small>Ticket #' . $ticket_formatted_id . '</small></p>' . $this->mail->Body ;
        };

        $templates = array(
            'framework' => array(
                /* SISTEMA NOTIFICHE ERRORI */
                'error-notification-email' => array(
                    'nome' => 'Notifica Errore Email',
                    'descrizione' => 'Viene inviata nel momento in cui si verificano degli errori con l\'invio delle email',
                    'reply_to' => array(),
                    'address' => "notifiche@marketingstudio.it",
                    'subject' => "Notifica errori (Email) - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{priority}" => "La priorità dell'errore",
                            "{website}" => "Il sito web in cui ha avuto origine l'errore",
                            "{error_email_subject}" => "L'oggetto della mail incriminata",
                            "{error_email_sender}" => "Il mittente della mail incriminata",
                            "{error_email_recipients}" => "I destinatari della mail incriminata",
                            "{error_url}" => "L'URL in cui si è verificato il problema",
                            "{error_log}" => "I log dettagliati dell'errore",
                            "{detail-url}" => 'Il link del dettaglio dell\'errore',
                        ),
                        array(
                            "{[priority]}",
                            "{[website]}",
                            "{[error_email_subject]}",
                            "{[error_email_sender]}",
                            "{[error_email_recipients]}",
                            "{[error_url]}",
                            "{[error_log]}",
                            '{admin-url}modules/framework360/errors_log/email/edit.php?id={[id]}'
                        )
                    )
                ),
                'error-notification-javascript' => array(
                    'nome' => 'Notifica Errore Javascript',
                    'descrizione' => 'Viene inviata nel momento in cui si verificano degli errori javascript',
                    'reply_to' => array(),
                    'address' => "notifiche@marketingstudio.it",
                    'subject' => "Notifica errori (JavaScript) - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{priority}" => "La priorità dell'errore",
                            "{website}" => "Il sito web in cui ha avuto origine l'errore",
                            "{user_agent}" => "L'oggetto della mail incriminata",
                            "{error_url}" => "L'URL in cui si è verificato il problema",
                            "{error_referer}" => "L'URL in cui si è verificato il problema",
                            "{error_log}" => "I log dettagliati dell'errore",
                            "{detail-url}" => 'Il link del dettaglio dell\'errore',
                        ),
                        array(
                            "{[priority]}",
                            "{[website]}",
                            "{[user_agent]}",
                            "{[error_url]}",
                            "{[error_referer]}",
                            "{[error_log]}",
                            '{admin-url}modules/framework360/errors_log/javascript/edit.php?id={[id]}'
                        )
                    )
                ),
                'error-notification-php' => array(
                    'nome' => 'Notifica Errore PHP',
                    'descrizione' => 'Viene inviata nel momento in cui si verificano degli errori PHP',
                    'reply_to' => array(),
                    'address' => "notifiche@marketingstudio.it",
                    'subject' => "Notifica errori (PHP) - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{priority}" => "La priorità dell'errore",
                            "{website}" => "Il sito web in cui ha avuto origine l'errore",
                            "{error_url}" => "L'URL in cui si è verificato il problema",
                            "{error_referer}" => "L'URL in cui si è verificato il problema",
                            "{error_log}" => "I log dettagliati dell'errore",
                            "{error_type}" => "Il tipo di errore (Warning | Errore Fatale | Sconosciuto)",
                            "{detail-url}" => 'Il link del dettaglio dell\'errore',
                        ),
                        array(
                            "{[priority]}",
                            "{[website]}",
                            "{[error_url]}",
                            "{[error_referer]}",
                            "{[error_log]}",
                            "{[error_type]}",
                            '{admin-url}modules/framework360/errors_log/php/edit.php?id={[id]}'
                        )
                    )
                ),
                'error-notification-sql' => array(
                    'nome' => 'Notifica Errore SQL',
                    'descrizione' => 'Viene inviata nel momento in cui si verificano degli errori SQL',
                    'reply_to' => array(),
                    'address' => "notifiche@marketingstudio.it",
                    'subject' => "Notifica errori (SQL) - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{priority}" => "La priorità dell'errore",
                            "{website}" => "Il sito web in cui ha avuto origine l'errore",
                            "{error_url}" => "L'URL in cui si è verificato il problema",
                            "{error_referer}" => "L'URL in cui si è verificato il problema",
                            "{error_log}" => "I log dettagliati dell'errore",
                            "{detail-url}" => 'Il link del dettaglio dell\'errore',
                        ),
                        array(
                            "{[priority]}",
                            "{[website]}",
                            "{[error_url]}",
                            "{[error_referer]}",
                            "{[error_log]}",
                            '{admin-url}modules/framework360/errors_log/sql/edit.php?id={[id]}'
                        )
                    )
                ),
                /* SISTEMA NOTIFICHE TICKET */
                'ticket-new-question' => array(
                    'nome' => 'Nuovo Ticket aperto',
                    'descrizione' => 'Viene inviata agli admin nel momento in cui un cliente apre un Ticket',
                    'reply_to' => array(FRAMEWORK_SUPPORT_EMAIL, '{[user_name]}'),
                    'from' => array(FRAMEWORK_SUPPORT_EMAIL, '{[user_name]}'),
                    'address' => $this->getSuperAdminEmails(),
                    'subject' => "Richiesta di Assistenza - {origin-website}",
                    'shortcodes' => array(
                        array(
                            "{user_name}" => "Il nome dell'utente che ha aperto il ticket",
                            "{titolo}" => "Il titolo del ticket",
                            "{messaggio}" => "Il messaggio del ticket",
                            "{categoria}" => "La tipologia di richiesta di assistenza",
                            "{detail-url}" => 'Il link del pannello per la risposta',
                            "{origin-website}" => 'Il nome del sito web relativo al ticket'
                        ),
                        array(
                            "{[user_name]}",
                            "{[titolo]}",
                            "{[messaggio]}",
                            "{[categoria]}",
                            function($params, $template_settings) {
                                return 'https://login.marketingstudio.it/modules/framework360/assistenza/ticket/edit.php?id=' . $params['id'];
                            },
                            function($params, $template_settings) {
                                $ticket_info = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($params['id'])[$params['id']];
                                return $this->MSFrameworki18n->getFieldValue($this->MSFrameworki18n->getFieldValue(json_decode($this->MSFrameworkDatabase->getAssoc("SELECT value FROM `" . $ticket_info['domain_info']['database'] ."`.`cms` WHERE type = 'site'", array(), true)['value'], true)['nome']));
                            }
                        )
                    ),
                    'beforeSend' => $ticket_beforeSend
                ),
                'ticket-user-summary' => array(
                    'nome' => 'Resoconto Ticket - Utente',
                    'descrizione' => 'Viene inviata al cliente nel momento in cui apre un Ticket',
                    'reply_to' => array(FRAMEWORK_SUPPORT_EMAIL, 'Assistenza Marketing Studio'),
                    'from' => array(FRAMEWORK_SUPPORT_EMAIL, "Assistenza Marketing Studio"),
                    'address' => "{[notification_email]}",
                    'subject' => "Ticket aperto - {origin-website}",
                    'shortcodes' => array(
                        array(
                            "{user_name}" => "Il nome dell'utente che ha aperto il ticket",
                            "{titolo}" => "Il titolo del ticket",
                            "{messaggio}" => "Il messaggio del ticket",
                            "{categoria}" => "La tipologia di richiesta di assistenza",
                            "{detail-url}" => 'Il link del pannello per la risposta',
                            "{origin-website}" => 'Il nome del sito web relativo al ticket'
                        ),
                        array(
                            "{[user_name]}",
                            "{[titolo]}",
                            "{[messaggio]}",
                            "{[categoria]}",
                            function($params, $template_settings) {
                                $ticket_info = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($params['id'])[$params['id']];
                                return $ticket_info['domain_info']['backend_url'] . 'modules/assistenza/ticket/edit.php?id=' . $params['id'];
                            },
                            function($params, $template_settings) {
                                $ticket_info = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($params['id'])[$params['id']];
                                return $this->MSFrameworki18n->getFieldValue(json_decode($this->MSFrameworkDatabase->getAssoc("SELECT value FROM `" . $ticket_info['domain_info']['database'] ."`.`cms` WHERE type = 'site'", array(), true)['value'], true)['nome']);
                            }
                        )
                    ),
                    'beforeSend' => $ticket_beforeSend
                ),
                'ticket-new-reply[admin]' => array(
                    'nome' => 'Nuova risposta al Ticket - Admin',
                    'descrizione' => 'Viene inviata allo staff nel momento in cui il cliente risponde',
                    'reply_to' => array(FRAMEWORK_SUPPORT_EMAIL, '{[user_name]}'),
                    'from' => array(FRAMEWORK_SUPPORT_EMAIL, '{[user_name]}'),
                    'address' => $this->getSuperAdminEmails(),
                    'subject' => "Ticket aggiornato - {origin-website}",
                    'shortcodes' => array(
                        array(
                            "{user_name}" => "Il nome dell'utente che ha inviato il messaggio",
                            "{titolo}" => "Il titolo del ticket",
                            "{messaggio}" => "Il messaggio del ticket",
                            "{detail-url}" => 'Il link del pannello per la risposta',
                            "{origin-website}" => 'Il nome del sito web relativo al ticket'
                        ),
                        array(
                            "{[user_name]}",
                            "{[titolo]}",
                            "{[messaggio]}",
                            function($params, $template_settings) {
                                return 'https://login.marketingstudio.it/modules/framework360/assistenza/ticket/edit.php?id=' . $params['id'];
                            },
                            function($params, $template_settings) {
                                $ticket_info = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($params['id'])[$params['id']];
                                return $this->MSFrameworki18n->getFieldValue(json_decode($this->MSFrameworkDatabase->getAssoc("SELECT value FROM `" . $ticket_info['domain_info']['database'] ."`.`cms` WHERE type = 'site'", array(), true)['value'], true)['nome']);
                            }
                        )
                    ),
                    'beforeSend' => $ticket_beforeSend
                ),
                'ticket-new-reply[user]' => array(
                    'nome' => 'Nuova risposta al Ticket - Utente',
                    'descrizione' => 'Viene inviata al cliente nel momento in cui il lo staff risponde',
                    'reply_to' => array(FRAMEWORK_SUPPORT_EMAIL, 'Assistenza Marketing Studio'),
                    'from' => array(FRAMEWORK_SUPPORT_EMAIL, "Assistenza Marketing Studio"),
                    'address' => "{[notification_email]}",
                    'subject' => "Ticket aggiornato - {origin-website}",
                    'shortcodes' => array(
                        array(
                            "{user_name}" => "Il nome dell'utente che ha aperto il ticket",
                            "{titolo}" => "Il titolo del ticket",
                            "{messaggio}" => "Il messaggio del ticket",
                            "{detail-url}" => 'Il link del pannello per la risposta',
                            "{origin-website}" => 'Il nome del sito web relativo al ticket'
                        ),
                        array(
                            "{[user_name]}",
                            "{[titolo]}",
                            function($params, $template_settings) {
                                return "{[messaggio]}" . ($params['quotation']['enabled'] === "1" ? " <div>E' disponibile un preventivo per questo ticket. Accedi alla tua area riservata per visualizzare tutti i dettagli.</div>" : "");
                            },
                            function($params, $template_settings) {
                                $ticket_info = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($params['id'])[$params['id']];
                                return $ticket_info['domain_info']['backend_url'] . 'modules/assistenza/ticket/edit.php?id=' . $params['id'];
                            },
                            function($params, $template_settings) {
                                $ticket_info = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($params['id'])[$params['id']];
                                return $this->MSFrameworki18n->getFieldValue(json_decode($this->MSFrameworkDatabase->getAssoc("SELECT value FROM `" . $ticket_info['domain_info']['database'] ."`.`cms` WHERE type = 'site'", array(), true)['value'], true)['nome']);
                            }
                        )
                    ),
                    'beforeSend' => $ticket_beforeSend
                ),
                'ticket-status-update' => array(
                    'nome' => 'Stato del ticket aggiornato',
                    'descrizione' => 'Viene inviata al cliente nel momento in cui il lo staff risponde',
                    'reply_to' => array(FRAMEWORK_SUPPORT_EMAIL, 'Assistenza Marketing Studio'),
                    'from' => array(FRAMEWORK_SUPPORT_EMAIL, "Assistenza Marketing Studio"),
                    'address' => "{[notification_email]}",
                    'subject' => "Stato del Ticket aggiornato - {origin-website}",
                    'shortcodes' => array(
                        array(
                            "{user_name}" => "Il nome dell'utente che ha aperto il ticket",
                            "{stato}" => "Lo stato del ticket in versione testuale (Es: Completato al 50%)",
                            "{titolo}" => "Il titolo del ticket",
                            "{detail-url}" => 'Il link del pannello per la risposta',
                            "{origin-website}" => 'Il nome del sito web relativo al ticket'
                        ),
                        array(
                            "{[user_name]}",
                            "{[stato]}",
                            "{[titolo]}",
                            function($params, $template_settings) {
                                $ticket_info = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($params['id'])[$params['id']];
                                return $ticket_info['domain_info']['backend_url'] . 'modules/assistenza/ticket/edit.php?id=' . $params['id'];
                            },
                            function($params, $template_settings) {
                                $ticket_info = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($params['id'])[$params['id']];
                                return $this->MSFrameworki18n->getFieldValue(json_decode($this->MSFrameworkDatabase->getAssoc("SELECT value FROM `" . $ticket_info['domain_info']['database'] ."`.`cms` WHERE type = 'site'", array(), true)['value'], true)['nome']);
                            }
                        )
                    ),
                    'beforeSend' => $ticket_beforeSend
                ),
            ),
            'main' => array(
                /* TEMPLATE RELATIVI AL SISTEMA UTENTE */
                'account-activation' => array(
                    'nome' => '[CLIENTE] Attivazione Account',
                    'descrizione' => 'Viene inviata nel momento in cui un utente si iscrive al sito web',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Attivazione Account - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{subject}" => "L'oggetto della mail",
                            "{activation-url}" => 'Il link di attivazione',
                        ),
                        array(
                            "{[nome]}",
                            "{[email]}",
                            "{[subject]}",
                            '{admin-url}ajax/mail/accountConfirmation.php?auth={[code]}' . ($this->MSFrameworkSaaSBase->isSaaSDomain() ? '&env_id={[env_id]}' : '')
                        )
                    )
                ),
                'customer-awaiting-activation' => array(
                    'nome' => '[CLIENTE] In attesa di approvazione',
                    'descrizione' => 'Viene inviata al cliente nel momento in cui il suo account risulta in attesa di convalida da parte degli admin',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Il tuo account è in attesa di approvazione - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente"
                        ),
                        array(
                            "{[nome]}",
                            "{[email]}"
                        )
                    )
                ),
                'customer-account-confirmed' => array(
                    'nome' => '[CLIENTE] Account Confermato dagli Admin',
                    'descrizione' => 'Viene inviata al cliente nel momento in cui il suo account viene confermato dagli admin',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Il tuo account è stato convalidato - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente"
                        ),
                        array(
                            "{[nome]}",
                            "{[email]}"
                        )
                    )
                ),
                'admin-customer-awaiting-activation' => array(
                    'nome' => '[CLIENTE] [Notifica] Richiesta di convalida account',
                    'descrizione' => 'Viene inviata agli admin nel momento in cui un account risulta in attesa di convalida',
                    'reply_to' => array(),
                    'address' => $default_address,
                    'subject' => "Nuovo account in attesa di approvazione - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{customer-url}" => "L'indirizzo che porta al profilo del cliente"
                        ),
                        array(
                            "{[nome]}",
                            "{[email]}",
                            "{admin-url}modules/customers/list/edit.php?id={[id]}"
                        )
                    )
                ),
                'admin-new-customer-registration' => array(
                    'nome' => '[CLIENTE] [Notifica] Si è registrato un nuovo Cliente',
                    'descrizione' => 'Viene inviata agli admin nel momento in cui un Cliente si registrea',
                    'reply_to' => array(),
                    'address' => $default_address,
                    'subject' => "Nuovo cliente registrato - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{customer-url}" => "L'indirizzo che porta al profilo del cliente"
                        ),
                        array(
                            "{[nome]}",
                            "{[email]}",
                            "{admin-url}modules/customers/list/edit.php?id={[id]}"
                        )
                    )
                ),
                'mail-confirmation' => array(
                    'nome' => '[CLIENTE] Conferma Email',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => 'Conferma Email - {site-name}',
                    'prepareParams' => function ($params, $template_settings) {
                        $r = $this->MSFrameworkDatabase->getAssoc("SELECT nome, mail_auth FROM customers WHERE email = :mail", array(":mail" => $params['email']), true);
                        $params['nome'] = $r['nome'] . ' ' . $r['cognome'];
                        $params['activation_link'] = "{admin-url}login.php?a=activate-customer&mail_auth=" . $r['mail_auth'];
                        return $params;
                    },
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{activation_link}" => "Il link per l'attivazione dell'account",
                        ), array(
                            "{[nome]}",
                            "{[email]}",
                            "{[activation_link]}"
                        )
                    )
                ),
                'account-activation-no-confirm' => array(
                    'nome' => '[STAFF] Attivazione Account (senza conferma email)',
                    'descrizione' => 'Viene inviata nel momento in cui un utente si iscrive al sito web (non richiede l\'attivazione dell\'account tramite conferma email)',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => "Attivazione Account - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{email}" => "L'email del cliente",
                            "{subject}" => "L'oggetto della mail",
                        ),
                        array(
                            "{[nome]}",
                            "{[email]}",
                            "{[subject]}",
                        )
                    )
                ),
                'notify-admin-activate-new-user' => array(
                    'nome' => '[STAFF] [Notifica] Richiesta di convalida account',
                    'descrizione' => 'Viene inviata agli nel momento in cui un utente si iscrive al sito web ed è in attesa di attivazione',
                    'reply_to' => array(),
                    'address' => "{[admin_email]}",
                    'subject' => "Richiesta Attivazione Account - {site-name}",
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome dell'admin",
                            "{email}" => "L'email del cliente",
                            "{admin_email}" => "L'email dell'admin",
                            "{subject}" => "L'oggetto della mail",
                        ),
                        array(
                            "{[nome]}",
                            "{[email]}",
                            "{[admin_email]}",
                            "{[subject]}",
                        )
                    )
                ),
                'pass-reset' => array(
                    'nome' => '[CLIENTE & STAFF] Reimpostazione Password',
                    'reply_to' => array(),
                    'address' => "{[email]}",
                    'subject' => 'Reset Password - {site-name}',
                    'shortcodes' => array(
                        array(
                            "{nome}" => "Il nome del cliente",
                            "{reset_link}" => "Il link per il reset della password"
                        ), array(
                            "{[nome]}",
                            function($params, $template_settings) {
                                if($params['origin'] == 'customer') {
                                    $r = $this->MSFrameworkDatabase->getAssoc("SELECT nome, passreset_token FROM customers WHERE email = :mail", array(":mail" => $params['email']), true);
                                    return "{admin-url}login/resetPassword.php?t=" . $r['passreset_token'] . '&o=customer' . ($this->MSFrameworkSaaSBase->isSaaSDomain() ? '&env_id={[env_id]}' : '');
                                } else {
                                    $table = ((count($this->MSFrameworkUsers->getSuperAdmins($params['email'])) > 0) ?  "`" . FRAMEWORK_DB_NAME . "`.`users`" : "users");

                                    if($this->MSFrameworkSaaSBase->isSaaSDomain()) {
                                        global $MSFrameworkSaaSEnvironments;
                                        $db_name = (new \MSFramework\SaaS\environments($MSFrameworkSaaSEnvironments->getEnvIDByMail($params['email'])))->getDBName();
                                        $set_table = "`$db_name`.`$table`";
                                    } else {
                                        $set_table = $table;
                                    }

                                    $r = $this->MSFrameworkDatabase->getAssoc("SELECT nome, passreset_token FROM $set_table WHERE email = :mail", array(":mail" => $params['email']), true);

                                    return "{admin-url}login/resetPassword.php?t=" . $r['passreset_token'] . ($this->MSFrameworkSaaSBase->isSaaSDomain() ? '&env_id={[env_id]}' : '');
                                }
                            }
                        )
                    )
                ),
                /* TEMPLATE GENERALI */
                'contact-form' => array(
                    'nome' => 'Form Contatti',
                    'reply_to' => array("{[email]}", "{[name]}"),
                    'address' => "{[dest]}",
                    'from' => array(json_decode(MARKETINGSTUDIO_NOREPLY_MAIL, true)['mail'], '{[name]} tramite {site-name}'),
                    'subject' => "Nuova richiesta ricevuta - {subject}",
                    'shortcodes' => array(
                        array(
                            "{name}" => "Il nome del cliente",
                            "{email}" => "L'email utilizzata dal cliente",
                            "{message}" => "Il messaggio inviato nel form",
                            "{subject}" => "L'oggetto della richiesta del form",
                            "{site-link}" => "Il link della pagina tramite la quale hanno inviato il form"
                        ),
                        array(
                            "{[name]}",
                            "{[email]}",
                            "{[message]}",
                            "{[subject]}",
                            "{[site-link]}"
                        )
                    )
                ),
                'newsletter-confirm' => array(
                    'nome' => 'Conferma Iscrizione Newsletter',
                    'reply_to' => array("{[email]}", "{[name]}"),
                    'address' => "{[dest]}",
                    'subject' => 'Conferma iscrizione newsletter {site-name}',
                    'shortcodes' => array(
                        array(
                            "{auth_confirm}" => "Il codice di convalida per l'iscrizione",
                            "{auth_confirm_url}" => "L'url per la convalida dell'iscrizione",
                        ),
                        array(
                            '<a href="{admin-url}ajax/mail/newsletterConfirmation.php?auth={[auth]}">questo link</a>',
                            '{admin-url}ajax/mail/newsletterConfirmation.php?auth={[auth]}',
                        )
                    )
                )
            ),
        );

        foreach($email_classes as $specific_class) {
            $templates = array_merge($templates, $specific_class->getExtraTemplates());
        }

        if($load_info) {
            $templates_db = array();
            foreach ($this->MSFrameworkDatabase->getAssoc("SELECT * FROM " . FRAMEWORK_DB_NAME . ".email_templates") as $db_mail) {
                $db_mail['origin'] = 'common';
                $templates_db[$db_mail['id']] = $db_mail;
            }

            /* SOVRASCRIVE IL TEMPLATE CON QUELLO PERSONALIZATO SE DISPONIBILE */
            if(!$not_overwrite) {
                foreach ($this->MSFrameworkDatabase->getAssoc("SELECT * FROM email_templates") as $custom_mail) {
                    $custom_mail['origin'] = 'custom';
                    $templates_db[$custom_mail['id']] = $custom_mail;
                }
            }

            foreach($templates as $group => $templates_list) {
                foreach ($templates_list as $id => $template) {
                    if (isset($templates_db[$id])) {
                        $templates[$group][$id]['data'] = $templates_db[$id];
                    } else {
                        $templates[$group][$id]['data'] = array();
                    }
                }
            }
        }

        if($key != "") {
            foreach($templates as $group => $templates_list) {
                if(isset($templates_list[$key]))
                {
                    if($load_info) {
                        $templates_list[$key]['data']['html'] = $this->formatTemplateForEditor($templates_list[$key]['data']['html']);
                    }
                    return $templates_list[$key];
                }
            }
        }
        else {
            return $templates;
        }

        return array();

    }

    /**
     * Effettua l'invio della mail
     *
     * @param string $template Il nome del template da utilizzare
     * @param array $params Eventuali parametri aggiuntivi per arricchire le informazioni della mail
     *
     * @return mixed
     * @throws \phpmailerException
     */
    public function sendMail($template, $params = array()) {

        if($this->setTemplate($template, $params)) {

            // Se ci troviamo in staging/sviluppo e stiamo richiamando la tabella producer_config sostituisco l'email
            if(($this->MSFrameworkCMS->isStaging() || $this->MSFrameworkCMS->isDevelopment()) && !$this->forceRealRecipients) {

                if($_SESSION['userData'] && filter_var($_SESSION['userData']['email'], FILTER_VALIDATE_EMAIL)) {
                    $testing_address = $_SESSION['userData']['email'];
                } else {
                    $testing_address = STAGING_EMAIL;
                }

                $this->mail->clearAllRecipients();
                $this->mail->addAddress($testing_address);
            }


            // Se il mittente non è valido imposto i dati default del FW (Evito l'iscrizione su BlackList)
            if(!filter_var($this->mail->From, FILTER_VALIDATE_EMAIL) || stristr($this->mail->From,'POSTMASTER') !== false) {
                $this->mail->setFrom($this->noReplyInfo['mail'], SW_NAME);
            }

            $status = false;
            foreach(array_keys($this->mail->getAllRecipientAddresses()) as $address) {

                $this->mail->clearAllRecipients();
                $this->mail->addAddress(str_replace(' ', '', $address));

                if ($this->mail->send()) {
                    $status = true;
                } else {
                    $single_status = false;
                    if ($this->mail->Host === $this->noReplyReserveInfo['host'] && strstr($this->mail->ErrorInfo, 'SMTP service for this hosting account will be restored') !== false) {
                        $this->mail->Host = $this->noReplyReserveInfo['host'];
                        $this->mail->Username = $this->noReplyReserveInfo['mail'];
                        $this->mail->Password = $this->noReplyReserveInfo['pass'];
                        $this->mail->setFrom($this->noReplyReserveInfo['mail'], $this->noReplyReserveInfo['mail_name']);

                        if ($this->mail->send()) {
                            $status = true;
                        } else {
                            $single_status = false;
                            $noreply_reserve_mail = json_decode(FRAMEWORK_RESERVE_NOREPLY_MAIL, true);
                            if ($this->mail->Host === $noreply_reserve_mail['host'] && strstr($this->mail->ErrorInfo, 'SMTP service for this hosting account will be restored') !== false) {

                                $this->mail->Host = $noreply_reserve_mail['host'];
                                $this->mail->Username = $noreply_reserve_mail['mail'];
                                $this->mail->Password = $noreply_reserve_mail['pass'];
                                $this->mail->setFrom($noreply_reserve_mail['mail'], $noreply_reserve_mail['mail_name']);

                                if ($this->mail->send()) {
                                    $single_status = true;
                                    $status = true;
                                }
                            }

                            if (!$single_status && strpos('error-notification', $template) === false && !$this->ignoreErrors) {
                                (new \MSFramework\Modules\errorTracker())->saveEmailErrorLog($template, $this->mail->Subject, $this->mail->From, array_keys($this->mail->getAllRecipientAddresses()), $this->mail->ErrorInfo);
                            }
                        }
                    }
                }
            }
            return $status;
        } else {
            return false;
        }
    }

    /**
     * Invia una mail generica, senza basarsi su un template specifico
     *
     * @param string $subject L'oggetto della mail
     * @param string $content_html Il contenuto della mail (html)
     * @param mixed $addresses I destinatari della mail (se uno solo, in forma di stringa. Se più di uno, in forma di array)
     * @param array $reply_to L'indirizzo reply to. L'array deve contenere due chiavi: email e nome
     * @param string $content_txt Il contenuto della mail (txt)
     * @param array $attachments Un array di allegati. L'array deve contenere due chiavi: path e name
     * @param array $replace Un array sostituzioni da effettuare. La chiave deve contenere la stringa da cercare, il valore la sostituzione da applicare
     *
     * @return bool
     * @throws \phpmailerException
     */
    public function sendCustomMail($subject, $content_html, $addresses, $reply_to = array(), $content_txt = "", $attachments = array(), $replace = array()) {

        if(is_array($replace)) {
            $content_html = str_replace(array_keys($replace), array_values($replace), $content_html);
            $content_txt = str_replace(array_keys($replace), array_values($replace), $content_txt);
            $subject = str_replace(array_keys($replace), array_values($replace), $subject);
        }

        $this->mail->Body = $content_html;
        $this->mail->AltBody = "";

        if($content_txt != "") {
            $this->mail->AltBody = html_entity_decode($content_txt);
        }

        if(isset($reply_to['email']) && $reply_to['email'] != "") {
            $this->mail->AddReplyTo($reply_to['email'], $reply_to['name']);
        } else {
            $this->mail->AddReplyTo($this->replaceGlobalShortcode('{site-email}'),  $this->replaceGlobalShortcode('{business-name}'));
        }

        if(!is_array($addresses)) {
            $addresses = array($addresses);
        }

        $tmp_addresses = array();
        foreach($addresses as $k => $address) {
            foreach(explode(',', $address) as $exploded_address) {
                $addresses[$k] = html_entity_decode($exploded_address);
                if (filter_var($exploded_address, FILTER_VALIDATE_EMAIL)) {
                    $tmp_addresses[] = $exploded_address;
                }
            }
        }
        $addresses = $tmp_addresses;

        $this->mail->clearAttachments();

        foreach ($attachments as $file) {
            if(is_array($file)) {
                if($file['path'] != "" && file_exists($file['path'])) {
                    $this->mail->addAttachment($file['path'], $file['name']);
                }
            }
            else {
                if ($file != "" && file_exists($file)) {
                    $this->mail->addAttachment($file);
                }
            }
        }

        /* Sostituisco gli shortcodes globali e formatto l'HTML */
        $this->mail->Body = $this->replaceGlobalShortcode($this->mail->Body);
        $this->mail->AltBody = $this->replaceGlobalShortcode($this->mail->AltBody);

        $this->mail->Body = $this->formatTemplateHTML($this->mail->Body);

        $this->mail->Subject = html_entity_decode($this->replaceGlobalShortcode($subject));

        // Se ci troviamo in staging/sviluppo e stiamo richiamando la tabella producer_config sostituisco l'email
        if(($this->MSFrameworkCMS->isStaging() || $this->MSFrameworkCMS->isDevelopment()) && !$this->forceRealRecipients) {

            if($_SESSION['userData'] && filter_var($_SESSION['userData']['email'], FILTER_VALIDATE_EMAIL)) {
                $testing_address = $_SESSION['userData']['email'];
            } else {
                $testing_address = STAGING_EMAIL;
            }

            $addresses = array($testing_address);
        }

        // Se il mittente non è valido imposto i dati default del FW (Evito l'iscrizione su BlackList)
        if(!filter_var($this->mail->From, FILTER_VALIDATE_EMAIL) || stristr($this->mail->From,'POSTMASTER') !== false) {
            $this->mail->setFrom($this->noReplyInfo['mail'], SW_NAME);
        }

        $status = false;
        foreach($addresses as $address) {
            $this->mail->clearAllRecipients();
            $this->mail->addAddress(str_replace(' ', '', $address));

            if ($this->mail->send()) {
                $status = true;
            } else {
                $single_status = false;
                if ($this->mail->Host === $this->noReplyReserveInfo['host'] && strstr($this->mail->ErrorInfo, 'SMTP service for this hosting account will be restored') !== false) {
                    $this->mail->Host = $this->noReplyReserveInfo['host'];
                    $this->mail->Username = $this->noReplyReserveInfo['mail'];
                    $this->mail->Password = $this->noReplyReserveInfo['pass'];
                    $this->mail->setFrom($this->noReplyReserveInfo['mail'], $this->noReplyReserveInfo['mail_name']);

                    if ($this->mail->send()) {
                        $single_status = true;
                        $status = true;
                    }
                }

                if (!$this->ignoreErrors && !$single_status) {
                    (new \MSFramework\Modules\errorTracker())->saveEmailErrorLog('custom', $this->mail->Subject, $this->mail->From, array_keys($this->mail->getAllRecipientAddresses()), $this->mail->ErrorInfo);
                }
            }
        }

        return $status;
    }

    /**
     * Recupera il file del template e imposta le informazioni prima dell'invio della mail
     *
     * @param $template string Il nome del template da utilizzare
     * @param $params array Eventuali parametri aggiuntivi per arricchire le informazioni della mail
     *
     * @return mixed
     * @throws \phpmailerException
     */
    public function setTemplate($template, $params) {

        $params['template'] = $template;

        $template_info = $this->getTemplates($template, true);

        if($template_info && $template_info['data']) {

            /* Ottiene la versione da usare */
            $template_data = $template_info['data'];

            $template_settings = (json_decode($template_data['settings']) ? json_decode($template_data['settings'], true) : array());

            /* Prepara l'array $params */
            if(isset($template_info['prepareParams'])) $params = $template_info['prepareParams']($params, $template_settings);

            /* Imposto i contenuti principali */
            $this->mail->Body = $template_data['html'];
            $this->mail->AltBody  = $template_data['txt'];
            $this->mail->Subject = html_entity_decode($template_data['subject']);

            $s_key = 0;
            foreach($template_info['shortcodes'][0] as $param_key => $param_value) {

                $shortcode = $param_key;
                if(is_numeric($param_key)) {
                    $shortcode = $param_value;
                }

                /* Ottengo il valore dello shortcode */
                $param_value = $this->getParamContent($template_info['shortcodes'][1][$s_key], $params, $template_settings);

                /* Sostituisco gli shortcode con il valore */
                $this->mail->Body = str_replace($shortcode, $param_value, $this->mail->Body);
                $this->mail->AltBody = str_replace($shortcode, $param_value, $this->mail->AltBody);
                $this->mail->Subject = str_replace($shortcode, $param_value, $this->mail->Subject);

                $s_key++;
            }

            /* Sostituisco gli shortcode globali */
            $this->mail->Body = $this->replaceGlobalShortcode($this->mail->Body);
            $this->mail->AltBody = $this->replaceGlobalShortcode($this->mail->AltBody);
            $this->mail->Subject = $this->replaceGlobalShortcode($this->mail->Subject);

            // Sostituisce le variabili dinamiche
            $this->mail->Body = $this->replaceMultilevelShortcodes($this->mail->Body, $params);
            $this->mail->AltBody  = $this->replaceMultilevelShortcodes($this->mail->AltBody, $params);

            /* Aggiungo l'eventuale reply-to */
            if(isset($template_info['from']) && $template_info['from']) {
                $this->mail->setFrom(html_entity_decode($this->getParamContent($template_info['from'][0], $params, $template_settings)), html_entity_decode($template_info['from'][1] ? $this->getParamContent($template_info['from'][1], $params, $template_settings) : ''));
            }

            /* Aggiungo l'eventuale reply-to */
            if($template_info['reply_to']) {
                $this->mail->AddReplyTo($this->getParamContent($template_info['reply_to'][0], $params, $template_settings), ($template_info['reply_to'][1] ? $this->getParamContent($template_info['reply_to'][1], $params, $template_settings) : ''));
            } else {
                $this->mail->AddReplyTo($this->replaceGlobalShortcode('{site-email}'),  $this->replaceGlobalShortcode('{business-name}'));
            }

            if(!is_array($template_info['address'])) {
                $dest = html_entity_decode($this->getParamContent($template_info['address'], $params, $template_settings));
            } else {
                $dest = array();
                foreach($template_info['address'] as $k => $add) {
                    $dest[] = html_entity_decode($this->getParamContent($add, $params, $template_settings));
                }
            }

            if(!is_array($dest)) {
                $dest = array($dest);
            }

            foreach($dest as $cur_dest) {
                foreach(explode(',', $cur_dest) as $exploded_dest) {
                    if (filter_var($exploded_dest, FILTER_VALIDATE_EMAIL)) {
                        $this->mail->addAddress(str_replace(' ', '', $exploded_dest));
                    }
                }
            }

            /* Formatta il template HTML, ad esempio sostituisce i colori del template con quelli impostati sul singolo sito */
            $this->mail->Body = $this->formatTemplateHTML($this->mail->Body);

            /* Eseguo la funzione beforeSend se presente (solitamente viene usata per generare allegati o altro) */
            if(isset($template_info['beforeSend'])) {
                if($template_info['beforeSend']($params, $template_settings) === false) return false;
            }

            return true;

        }

        return false;
    }

    /**
     *  Ottiene il contenuto della chiave all'interno dell'array $params o il valore iniziale se $param_key è una semplice stringa
     *
     * @param string $param_key La chiave da ottenere (all'interno delle parentesi quadre) o una semplice stringa
     * @param array $params L'array con i parametri
     *
     * @return string
     */
    private function getParamContent($param_key, $params, $template_settings = array()) {
        $return = '';

        if(is_callable( $param_key ))
        {
            $return = $param_key($params, $template_settings);
        } else {
            $return = $this->replaceMultilevelShortcodes($param_key, $params);
        }

        return $this->replaceGlobalShortcode($return);
    }

    /**
     *  Sostituisce gli shortcode multilivello (esempio {[cart][total]} con il loro valore ($params['cart']['total'])
     *
     * @param string $content Il contenuto da sostituire
     * @param array $params L'array con i parametri
     *
     * @return string
     */
    private function replaceMultilevelShortcodes($content, $params) {

        $content = preg_replace_callback(array(
            "/{\[([a-z|A-Z|_|0-9|-]+)]}/i",
            "/{\[([a-z|A-Z|_|0-9|-]+)]\[([a-z|A-Z|_|0-9|-]+)]}/",
            "/{\[([a-z|A-Z|_|0-9|-]+)]\[([a-z|A-Z|_|0-9|-]+)]\[([a-z|A-Z|_|0-9|-]+)]}/i"
        ), function (array $matches) use ($params) {
            if(count($matches) == 2) return $params[$matches[1]];
            if(count($matches) == 3) return $params[$matches[1]][$matches[2]];
            if(count($matches) == 4) return $params[$matches[1]][$matches[2]][$matches[3]];
        }, $content);

        return $content;

    }

    /**
     * Sostituisce i vari tag e adatta gli stili e i contenuti dell'email in base al sito web.
     *
     * @param string $content Il codice HTML del template
     *
     * @return string
     */
    public function formatTemplateHTML($content) {
        $dom = new \DOMDocument;
        @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
        $xpath = new \DOMXpath($dom);

        // Trovo i CSS dinamici da sostituire
        $css_replace = $xpath->query('//*[@data-css_replace]');
        foreach ($css_replace as $link) {

            $replace_tags = $link->getAttribute('data-css_replace');
            $original_style_params = explode(";", $link->getAttribute('style'));
            array_pop($original_style_params);

            preg_match_all('/\[(.*?)\]/m', $replace_tags, $matches);
            foreach ($matches[1] as $match) {
                $single_css = explode("|", $match);

                // Rimuovo il vecchio stile per sostituirlo col nuovo
                foreach ($original_style_params as $k => $original_param) {
                    if (strpos($original_param, $single_css[0]) !== false) {
                        unset($original_style_params[$k]);
                    }
                }
                // Aggiungo il nuovo stile
                $original_style_params[] = $single_css[0] . ': ' . $single_css[1];

                /* AGGIUNGO ANCHE I PARAMETRI DIRETTAMENTE ALL'ELEMENTO SE DISPONIBILI */
                if($single_css[0] == 'width' || $single_css[0] == 'height') {
                    if(intval($single_css[1]) > 0) {
                        $link->setAttribute($single_css[0], intval($single_css[1]));
                    }
                }

            }

            $new_style = implode(';', $original_style_params) . ';';
            $link->setAttribute('style', $new_style);
        }

        /* GESTISCO I PULSANTI */
        $link_check = $xpath->query('//a');
        foreach ($link_check as $link) {

            /* RIMUOVO I PULSANTI CON UN LINK NON VALIDO */
            $link_attr = $link->getAttribute('href');

            if (!filter_var(filter_var($link_attr, FILTER_SANITIZE_URL), FILTER_VALIDATE_URL)) {
                $link->parentNode->removeChild($link);
            }

            /* Aggiungo un bordo invece del padding per migliorare la compatibilità con i vecchi lettori */
            $original_style_params = explode(";", $link->getAttribute('style'));
            array_pop($original_style_params);

            // Rimuovo il vecchio stile per sostituirlo col nuovo
            $have_padding = false;
            $have_background = false;
            foreach ($original_style_params as $k => $original_param) {
                if (strpos($original_param, 'padding') !== false) {
                    unset($original_style_params[$k]);
                    $have_padding = true;
                }
                else if (strpos($original_param, 'background') !== false) {

                    preg_match_all('/#(?:[0-9a-fA-F]{6}|[0-9a-fA-F]{3})[\s;]*/', $original_param,$matches);
                    if(isset($matches[0][0]))
                    {
                        $have_background = $matches[0][0];
                    }
                }
            }

            if($have_background && $have_padding) {
                $link->setAttribute('style', implode(';', $original_style_params) . ';' . 'border: 12px solid ' . $have_background . ';');
            }
        }

        /* GESTISCO I PARAGRAFI */
        $paragraphs = $xpath->query('//p');
        foreach ($paragraphs as $p) {
            $p->setAttribute('style', $p->getAttribute('style') . 'margin: 0; min-height: 20px; margin-bottom: 10px;');
        }

        /* AGGIUNGO IL TAG SE NON DISPONIBILE (Punteggio email migliorato) */
        $image_check = $xpath->query('//img');
        foreach ($image_check as $image) {

            /* RIMUOVO I PULSANTI CON UN LINK NON VALIDO */
            $image_alt = $image->getAttribute('alt');
            $image_src = $image->getAttribute('src');

            $image_style = $image->getAttribute('style');
            if($image_style) {
                $style_exploded = explode(';', $image_style);
                foreach($style_exploded as $k => $single_style) {
                    if(stristr($single_style, 'width') && (int)filter_var($single_style, FILTER_SANITIZE_NUMBER_INT) > 300) {
                        $style_exploded[$k] = 'width: 100%;';
                    }
                }
                $image_style = implode(';', $style_exploded);
            } else {
                $image_style = 'width: 100%';
            }

            $image->setAttribute('style', $image_style);

            if (!$image_alt) {
                $image->setAttribute('alt', end(array_values(explode('/', $image_src))));
            }
        }

        // Trovo gli attributi da sostituire
        $attr_replace = $xpath->query('//*[@data-attr_replace]');
        foreach ($attr_replace as $link) {

            $replace_tags = $link->getAttribute('data-attr_replace');

            preg_match_all('/\[((?:[^\[\]]+|(?R))*+)\]/m', $replace_tags, $matches);
            foreach ($matches[1] as $match) {
                $single_param = explode("|", $match);

                // Sostituisco il vecchio attributo con il nuovo
                $link->setAttribute($single_param[0], $single_param[1]);
            }

            $link->removeAttribute('data-attr_replace');

            // Elimino gli oggetti che hanno un determinato valore vuoto
            $attr_replace = $xpath->query('//*[@data-remove_if_empty]');
            foreach ($attr_replace as $link) {

                $attr_to_check = $link->getAttribute('data-remove_if_empty');
                $attr_value = $link->getAttribute($attr_to_check);

                if (!$attr_value || empty($attr_value)) {
                    $link->parentNode->removeChild($link);
                }

                $link->removeAttribute('data-remove_if_empty');
            }

        }

        $dom_content = $dom->saveHTML();

        if($dom_content) {
            $content = $dom_content;
        }

        // AGGIUNGO IL PROTOCOLLO NELLE IMMAGINI (SOLITAMENTE L'EDITOR NON LO METTE E CREA PROBLEMI)
        if(strpos($content, 'src="//') !== false) {
            $content = str_replace(
                'src="//',
                'src="' . (SSL_ENABLED ? 'https' : 'http') . '://',
                $content
            );
        }

        // TRASFORMO I LINK RELATIVI IN ASSOLUTI
        if(strpos($content, 'src="/') !== false) {
            $content = str_replace(
                'src="/',
                'src="' . $this->MSFrameworkCMS->getURLToSite(true),
                $content
            );
        }

        // RIMUOVO GLI STILI IMPORTANT (POSSONO CAUSARE PROBLEMI)
        $content = str_replace(
            '!important',
            '',
            $content
        );

        $head_content = '<meta name="viewport" content="width=device-width, initial-scale=1">';
        $head_content .= '<meta name="x-apple-disable-message-reformatting">';
        $head_content .=
            '<style>
                @media screen and (max-width: 650px) {
                    .main, table > tbody > tr > td > div {width: 100% !important;}
                }
                .content-image {max-width: 100%;}
                body {padding: 0px;}
                .emailContainer, #emailContainer {max-width: 100%}
                .emailContainer, #emailContainer {max-width: 100%}
                h1,h2{margin: 0 0 15px;}
                h3,h4,h5{margin: 0 0 10px;}
            </style>';

        $head = '<head>' . $head_content . '</head>';

        if(strpos($content, '<head>') >= 0) {
            $content = str_replace('<body>', $head . '<body>', $content);
        } else {
            $content = str_replace('</head>', $head_content . '</head>', $content);
        }

        $content = str_replace(
            array('<body>', '</body>'),
            array('<body><!--[if mso]><center><table width="600" style="margin: auto;"><tr><td><![endif]-->', '<!--[if mso]></td></tr></table></center><![endif]--></body>'),
            $content
        );

        return $content;

    }

    /**
     * Effettua quelle modifiche che servono ad adattare il template al sito web in questione
     * A differenza della funzione precedente (formatTemplateHTML) questa continuerà a mantenere tutti gli attributi HTML
     * necessari a mantenere dinamica l'email.
     *
     * @param string $content Il codice HTML del template
     *
     * @return string
     */
    public function formatTemplateForEditor($content) {


        $dom = new \DOMDocument;
        @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
        $xpath = new \DOMXpath($dom);


        // Trovo i CSS dinamici da sostituire
        $css_replace = $xpath->query('//*[@data-css_replace]');
        foreach ($css_replace as $link) {

            $replace_tags = $link->getAttribute('data-css_replace');
            $original_style_params = explode(";", $link->getAttribute('style'));
            array_pop($original_style_params);

            preg_match_all('/\[(.*?)\]/m', $replace_tags, $matches);
            foreach ($matches[1] as $match) {
                $single_css = explode("|", $match);

                if(in_array($single_css[0], array('background', 'width', 'height'))) {

                    // Rimuovo il vecchio stile per sostituirlo col nuovo
                    foreach ($original_style_params as $k => $original_param) {
                        if (strpos($original_param, $single_css[0]) !== false) {
                            unset($original_style_params[$k]);
                        }
                    }

                    $single_css[1] = $this->replaceGlobalShortcode($single_css[1]);

                    // Cambio il colore in base alla luminosità dello sfondo
                    if($single_css[0] == 'background') {
                        foreach ($original_style_params as $k => $original_param) {
                            if (strpos($original_param, 'color:') !== false) {
                                unset($original_style_params[$k]);
                            }
                        }
                        $original_style_params[] = 'color:' . ($this->MSFrameworkUtils->getHexLightness($single_css[1]) > 50 ? 'black' : 'white');
                    }

                    // Aggiungo il nuovo stile
                    $original_style_params[] = $single_css[0] . ': ' . $single_css[1];
                }
            }

            $new_style = implode(';', $original_style_params) . ';';
            $link->setAttribute('style', $new_style);
        }

        // Sostituisco i loghi con il logo del sito web relativo
        $attr_replace = $xpath->query('//*[@data-attr_replace]');
        foreach ($attr_replace as $link) {

            $replace_tags = $link->getAttribute('data-attr_replace');


            preg_match_all('/\[((?:[^\[\]]+|(?R))*+)\]/m', $replace_tags, $matches);

            foreach ($matches[1] as $match) {
                $single_param = explode("|", $match);

                if(in_array($single_param[1], array('{logo}'))) {
                    $link->setAttribute($single_param[0], $this->replaceGlobalShortcode($single_param[1]));
                }
            }

        }

        $dom_content = $dom->saveHTML();

        // Sistemo i link con caratteri speciali (LE GRAFFE NON VENGONO SALVATE)
        $re = '/href="%7B(([^"]+)+)%7D"/m';
        $dom_content = preg_replace($re, 'href="{$1}"', $dom_content);

        if($dom_content) {
            $content = $dom_content;
        }

        return $content;

    }

    /**
     * Sostituisce gli shortcode generali del template con i valori dinamici ottenuti dal sito
     *
     * @param string $content Il codice HTML del template
     *
     * @return string
     */
    public function replaceGlobalShortcode($content) {
        $shortcodes = $this->getCommonShortcodes();

        return str_replace(
            $shortcodes[0],
            $shortcodes[1],
            $content
        );

    }

    /**
     * Ottiene la lista di shortcodes
     *
     *
     * @return array
     */
    public function getCommonShortcodes() {

        $siteCMSData = $this->MSFrameworkCMS->getCMSData('site');
        $site_logos = json_decode($siteCMSData['logos'], true);

        return array(
            array(
                /* LOGHI */
                "{logo}",
                "{logo_footer}",
                "{logo_size}",
                /* INFO SITO */
                "{site-name}",
                "{site-url}",
                "{admin-url}",
                "{site-protocol}",
                "{geo-address}",
                "{common-cdn}",
                "{site-phone}",
                "{site-email}",
                /* AZIENDA */
                "{business-name}",
                "{business-vat}",
                /* COLORI */
                "{color_1}",
                "{color_2}",
                "{color_3}",
                "{color_4}",
                /* SOCIAL */
                "{social-instagram}",
                "{social-twitter}",
                "{social-facebook}",
                "{social-linkedin}",
                "{social-youtube}",
                "{social-skype}",
            ),
            array(
                /* LOGHI */
                (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https:' : 'http:') . UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo'],
                (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https:' : 'http:') . UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo_footer'],
                $this->producer_config['templatemail_logosize'] . 'px',
                /* INFO SITO */
                SW_NAME,
                $this->MSFrameworkCMS->getURLToSite(),
                $this->MSFrameworkCMS->getURLToSite(1),
                (SSL_ENABLED ? 'https:' : 'http:'),
                $this->MSFrameworkCMS->getFullAddress(),
                FRAMEWORK_COMMON_CDN,
                $siteCMSData['telefono'],
                $siteCMSData['email'],
                /* AZIENDA */
                $siteCMSData['rag_soc'],
                $siteCMSData['piva'],
                /* COLORI */
                $this->email_colors[0],
                $this->email_colors[1],
                $this->email_colors[2],
                $this->email_colors[3],
                /* SOCIAL */
                $siteCMSData['instagram_profile'],
                $siteCMSData['twitter_profile'],
                $siteCMSData['fb_profile'],
                $siteCMSData['linkedin_profile'],
                $siteCMSData['youtube_profile'],
                $siteCMSData['skype_profile'],
            )
        );

    }

    /**
     * Restituisce il prefisso che viene settato prima dello short code
     *
     * @return string
     */
    public function getACPBridgeShort() {
        return 'send_email';
    }

    /**
     * Restituisce il codice necessario per l'invio delle email (Sfruttato per convalidare richieste provenienti dall'ACP al front)
     *
     * @return string
     */
    public function getACPBridgeToken() {
        return 'gfdsgdfsgfds';
    }

    /**
     *  Restituisce un array con le info riguardo i gruppi dei template
     *
     * @return array
     */
    public function getTemplateGroupsStylesInfo() {
        return array(
            'msagency' => array(
                'icon' => 'fa-money',
                'background' => '#27bb69',
                'color' => 'white',
                'label' => 'Marketing Studio Agency'
            ),
            'framework' => array(
                'icon' => 'fa-puzzle-piece',
                'background' => 'red',
                'color' => 'white',
                'label' => 'Framework'
            ),
            'main' => array(
                'icon' => 'fa-envelope',
                'background' => 'grey',
                'color' => 'white',
                'label' => 'Principali'
            ),
            'ecommerce' => array(
                'icon' => 'fa-shopping-cart',
                'background' => '#00BCD4',
                'color' => 'white',
                'label' => 'Ecommerce'
            ),
            'fatturazione' => array(
                'icon' => 'fa-file-o',
                'background' => '#3F51B5',
                'color' => 'white',
                'label' => 'Fatturazione'
            ),
            'camping' => array(
                'icon' => 'fa-bed',
                'background' => '#a64e1b',
                'color' => 'white',
                'label' => 'Campeggio'
            ),
            'hotel' => array(
                'icon' => 'fa-bed',
                'background' => '#3f51b5',
                'color' => 'white',
                'label' => 'Hotel'
            ),
            'ticket' => array(
                'icon' => 'fa-ticket',
                'background' => '#ff9800',
                'color' => 'white',
                'label' => 'Ticket'
            ),
            'travelagency' => array(
                'icon' => 'fa-plane',
                'background' => '#9c27b0',
                'color' => 'white',
                'label' => 'Agenzia Viaggi'
            ),
            'subscriptions' => array(
                'icon' => 'fa-diamond',
                'background' => '#bf436d',
                'color' => 'white',
                'label' => 'Abbonamenti'
            ),
            'affiliations' => array(
                'icon' => 'fa-users',
                'background' => '#578cb7',
                'color' => 'white',
                'label' => 'Affiliazioni'
            ),
            'realestate' => array(
                'icon' => 'fa-building-o',
                'background' => '#ff0024',
                'color' => 'white',
                'label' => 'Agenzia Immobiliare'
            ),
            'points' => array(
                'icon' => 'fa-ticket',
                'background' => '#e87c1f',
                'color' => 'white',
                'label' => 'Punti clienti'
            ),
        );
    }

    /**
     * Ottiene le credenziali SMTP predefinite
     * @return array
     */
    public function getDefaultSMTPDetails() {
        $default_smtp = array(
            'name' => 'Marketing Studio',
            'host' => 'marketingstudio.it',
            'smtpauth' => 1,
            'username' => 'bounce@marketingstudio.it',
            'login' => 'noreply@marketingstudio.it',
            'password' => 'jhT6yGf4s',

            'smtpsecure' => 'ssl',
            'port' => '465',
            'maxlimit' => 10,
            'verifypeer' => 1,
            'forcesmtp' => 1,

            'bmh_server' => 'marketingstudio.it',
            'bmh_port' => '465',
            'bmh_service' => 'imap',
            'bmh_soption' => 'tls',
            'bmh_folder' => '',
            'bmh_password' => 'DXeJmOo4fOlV',
        );

        return $default_smtp;
    }

    /**
     * Se non indicato un ID restituisce un array associativo (ID server SMTP => Dati server SMTP) altrimenti riporterà solamente i dati del server indicato.
     *
     * @param $id L'ID della configurazione (stringa) o delle configurazioni (array) richieste (se vuoto, vengono recuperati i dati di tutte le configurazione)
     * @param boolean $return_default Se impostato ritorna le impostazioni SMTP di default nel caso in cui non siano state trovate impostazioni
     *
     * @return array
     */
    public function getSMTPDetails($id = '', $return_default = true) {
        $ary_to_return = array();

        if($id != "") {
            if(!is_array($id)) {
                $id = array($id);
            }

            $same_db_string_ary = $this->MSFrameworkDatabase->composeSameDBFieldString($id, "OR", "id");
        }

        if(!is_array($same_db_string_ary[1])) {
            $same_db_string_ary = array(" id != '' ", array());
        }

        foreach($this->MSFrameworkDatabase->getAssoc("SELECT * FROM email_config WHERE id != '' AND (" . $same_db_string_ary[0] . ")", $same_db_string_ary[1]) as $r) {
            $r['data'] = json_decode($r['data'], true);
            $ary_to_return[$r['id']] = $r;
        }

        if($return_default) {
            $default = array(
                'id' => 'default',
                'data' => (new \MSFramework\emails())->getDefaultSMTPDetails(),
                'enable_mailbox' => 0
            );
            $ary_to_return['default'] = $default;
        }

        if($id !== '' && count($id) === 1) {
            if(isset($ary_to_return[end($id)])) return $ary_to_return[end($id)];
            else if($return_default) return $ary_to_return['default'];
            else return array();
        }

        return $ary_to_return;
    }

    /**
     *  Ottiene gli indirizzi emai dei superadmin
     *
     * @return array
     */
    public function getSuperAdminEmails() {
        $emails_to_return = array();

        foreach($this->MSFrameworkUsers->getSuperAdmins() as $superadmin) {
            $emails_to_return[] = $superadmin['email'];
        }

        return $emails_to_return;
    }


}
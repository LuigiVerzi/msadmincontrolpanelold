<?php
/**
 * MSFramework
 * Date: 15/02/18
 */

namespace MSFramework;

class database extends \PDO {
    /**
     * Impostando questa variabile su "true", in caso di errore, la funzione restituirà degli errori più dettagliati invece di un semplice false
     * @var bool
     */
    public $detailed_error_info = false;

    private $memcached;
    private $cache_enabled = false;
    private $cachedQuery = array();

    public $lastAffectedRows = 0;

    public function __construct($host = FRAMEWORK_DB_HOST, $DBName = FRAMEWORK_DB_NAME, $DBUser = FRAMEWORK_DB_USER, $DBPassword = FRAMEWORK_DB_PASS, $type = "mysql", $charset = "utf8", $options = array()) {
        global $MSFrameworkSaaSBase, $MSFrameworkSaaSEnvironments;

        $got_customer_db = false;

        $this->cleanHost = str_replace(array("http://", "https://", "www.", ADMIN_URL), array("", "", "", ""), $_SERVER['HTTP_HOST']);

        if(!is_object($MSFrameworkSaaSBase)) { //in qualche caso la classe DB viene usata "decontestualizzata" dal FW, quindi non sempre ho l'oggetto $MSFrameworkSaaSBase pronto
            require_once(dirname( __FILE__ ) . "/SaaS/base.php");
            $MSFrameworkSaaSBase = new \MSFramework\SaaS\base();
        }

        if(!is_object($MSFrameworkSaaSEnvironments)) { //in qualche caso la classe DB viene usata "decontestualizzata" dal FW, quindi non sempre ho l'oggetto $MSFrameworkSaaSBase pronto
            require_once(dirname( __FILE__ ) . "/SaaS/environments.php");
            $MSFrameworkSaaSEnvironments = new \MSFramework\SaaS\environments();
        }

        $isSaaS = $MSFrameworkSaaSBase->isSaaSDomain() || $MSFrameworkSaaSBase->isSaaSDatabase($DBName);

        if(in_array($DBName, array("marke833_dev_vincenzo", "marke833_dev_luigi", "marke833_dev_luca"))) {
            $this->isDevelopDB = true;
        } else {
            $this->isDevelopDB = false;
        }

        if(!$isSaaS) {
            try {
                $tmp_obj = new \PDO($type . ':host=' . $host . ';dbname=' . FRAMEWORK_DB_NAME . ';charset=' . $charset, $DBUser, $DBPassword); //mi connetto al DB principale del Framework
                $ex = $tmp_obj->prepare("SELECT id, customer_database, virtual_server_name FROM customers WHERE customer_domain = :domain");

                $ex->execute(array(":domain" => $this->cleanHost));

                $result = $ex->fetchAll(\PDO::FETCH_ASSOC);

                if ($result[0]['customer_database'] != "" && count($result) == 1) {
                    $customer_dbname = $result[0]['customer_database'];

                    if (strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/")) {
                        $virtual_server_name = $result[0]['virtual_server_name'];
                    }

                    $_SESSION['domain_id'] = $result[0]['id'];
                    $_SESSION['db'] = $customer_dbname;
                    $_SESSION['virtual_server_name'] = $virtual_server_name;

                    $got_customer_db = true;

                    unset($tmp_obj);
                }
            } catch (\PDOException $e) {
                die('Impossibile connettersi al DB principale del Framework.');
            }
        } else {
            if(!strstr($_SERVER['HTTP_HOST'], ADMIN_URL) && !strstr(REAL_SW_PATH, "MSAdminControlPanel")) {
                $customer_dbname = "marke833__SaaS__id_" . $MSFrameworkSaaSBase->getID() . "__env_front";
            } else {
                $customer_dbname = ($_SESSION['SaaS']['saas__environments']['database'] == "" ? FRAMEWORK_DB_NAME : $_SESSION['SaaS']['saas__environments']['database']);
            }

            $_SESSION['db'] = $customer_dbname;
        }

        $connectingToFramework = true;
        if($DBName != FRAMEWORK_DB_NAME) { //verifico la presenza del db dei clienti SOLO se la connessione principale viene fatta sul database del framework. in caso contrario, la connessione viene fatta ad un DB non-framework e quindi non mi preoccupo di gestire i clienti.
            $connectingToFramework = false;
        }

        if(!$isSaaS && (!$this->checkDBExists($customer_dbname, $type) || !$got_customer_db)) {
            die('Impossibile avviare il DB del cliente MarketingStudio (non è stato possibile determinare il DB / non ci sono privilegi sufficienti per leggere il DB / il DB non esiste)');
        } else {
            try {
                if($connectingToFramework) {
                    $tmp_obj = new \PDO($type . ':host=' . FRAMEWORK_DB_HOST . ';dbname=' . $customer_dbname . ';charset=' . $charset, FRAMEWORK_DB_USER, FRAMEWORK_DB_PASS);
                    parent::__construct($type . ':host=' . FRAMEWORK_DB_HOST . ';dbname=' . $customer_dbname . ';charset=' . $charset, FRAMEWORK_DB_USER, FRAMEWORK_DB_PASS);
                } else {
                    $tmp_obj = new \PDO($type . ':host=' . $host . ';dbname=' . $DBName . ';charset=' . $charset, $DBUser, $DBPassword);
                    parent::__construct($type . ':host=' . $host . ';dbname=' . $DBName . ';charset=' . $charset, $DBUser, $DBPassword);
                }

                parent::setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                parent::setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

                if(!$isSaaS || ($isSaaS && $_SESSION['SaaS']['saas__environments']['database'] != "")) {
                    if(!$isSaaS) {
                        $ex = $tmp_obj->prepare("SELECT value FROM cms WHERE type = :type");
                        $ex->execute(array(":type" => 'producer_config'));
                        $result = $ex->fetchAll(\PDO::FETCH_ASSOC);
                        $producer_config = json_decode($result[0]['value'], true);
                    } else {
                        $ex = $tmp_obj->prepare("SELECT cms FROM `" . FRAMEWORK_DB_NAME . "`.`saas__config` WHERE id = :id");
                        $ex->execute(array(":id" => $MSFrameworkSaaSBase->getID()));
                        $result = $ex->fetchAll(\PDO::FETCH_ASSOC);
                        $producer_config = json_decode($result[0]['cms'], true)['producer_config'];
                    }
                }

                $this->pdoInstance = $tmp_obj;

            } catch (\PDOException $e){
                session_destroy(); //distruggo completamente la sessione per evitare che il SW resti bloccato nel cercare sempre lo stesso db inesistente

                if($DBName == FRAMEWORK_DB_NAME) {
                    die('Impossibile connettersi al DB del cliente.');
                } else {
                    die('Impossibile connettersi al DB ' . $DBName);
                }
            }
        }
    }

    /**
     * Restituisce l'oggetto PDO che si connette al DB del cliente
     *
     * @return \PDO
     */
    public function getPDOInstance() {
        return $this->pdoInstance;
    }

    /**
     * Controlla l'esistenza di un determinato database sul server
     *
     * @access public
     *
     * @return bool
     */
    public function checkDBExists($customer_dbname, $type = "mysql", $charset = "utf8") {
        $tmp_obj = new \PDO($type . ':host=' . FRAMEWORK_DB_HOST . ';dbname=' . FRAMEWORK_DB_NAME . ';charset=' . $charset, FRAMEWORK_DB_USER, FRAMEWORK_DB_PASS); //mi connetto ad un database sicuramente esistente

        $ex = $tmp_obj->prepare("SELECT COUNT(*) as count FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = :dbname");
        $ex->execute(array(":dbname" => $customer_dbname));
        $result = $ex->fetchAll(\PDO::FETCH_ASSOC);
        unset($tmp_obj);

        if($result[0]['count'] == "1") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Esegue le query sul DB preparandole e restituendo un array associativo
     *
     * @access public
     * @param $q - La query
     * @param array $ary_execute (default: array()) - L'array contenente i valori da preparare
     * @param bool $return_first (default: false) - Se impostato su true restituisce solo la chiave [0] dell'array (utile quando si filtra una query per ID, quindi si è certi che ci sarà un solo risultato)
     * @param bool $cache (default: null) - Se impostato su true salva il risultato della query in memcached (o lo recupera dalla cache, se già presente). Se impostato su null i valori di default sono true se la richiesta proviene dal front, false se la richiesta proviene da ACP
     * @param int $cache_expire (default: 600) - Consente di impostare la scadenza della query (in secondi a partire dalla data di creazione). Max 30 giorni: se il valore è > di 30 giorni, il server lo considererà come un TimeStamp UNIX
     *
     * @return mixed
     */
    public function getAssoc($q, $ary_execute=array(), $return_first = false, $cache = null, $cache_expire = 86400) {
        global $firephp;

        $cacheKey = json_encode(func_get_args());
        if(isset($this->cachedQuery[$cacheKey])) return $this->cachedQuery[$cacheKey];

        try{
            $ex = $this->prepare($q);
            $ex->execute($ary_execute);
            $result = $ex->fetchAll(\PDO::FETCH_ASSOC);

            if(!$return_first) {
                $this->cachedQuery[$cacheKey] = $result;
                return $result;
            } else {
                $this->cachedQuery[$cacheKey] = $result[0];
                return $result[0];
            }

        } catch(\PDOException $e) {
            $exception_details = $this->getExceptionDetails($e, $q, $ary_execute);
            (new \MSFramework\Modules\errorTracker())->saveSQLErrorLog($exception_details);

            if($this->detailed_error_info) {
                return $exception_details;
            } else {
                return false;
            }
        }
    }

    /**
     * Prova ad ottenere il nome della tabella sulla quale viene effettuata una query. La query deve iniziare con "SELECT", "UPDATE" o "INSERT INTO".
     *
     * @param $query La query da analizzare
     *
     * @return bool|string
     */
    private function getTableNameFromQuery($query) {
        $needle = "";
        foreach(array("select" => " from ", "insert" => " into " , "update" => "update ") as $exprK => $exprV) {
            if(strtolower(substr($query, 0, strlen($exprK))) == $exprK) {
                $needle = $exprV;
                break;
            }
        }

        if($needle == "") {
            return false;
        }

        $from_pos = stripos($query, $needle);
        $to_pos = strpos($query, " ", $from_pos+strlen($needle)+1);
        if(!$to_pos) {
            $to_pos = strlen($query);
        }

        $result = substr($query, $from_pos+strlen($needle), $to_pos-$from_pos-strlen($needle));
        if($result != "") {
            return trim(str_replace("`", "", $result));
        }

        return false;
    }

    /**
     * Restituisce la stringa della query "pulita", con i valori passati come placeholder sostituiti con i valori reali dell'array.
     *
     * @access public
     * @param $q - La query
     * @param array $ary_execute (default: array()) - L'array contenente i valori da preparare
     * @return string
     */
    public function getReplacedQuery($q, $ary_execute=array()) {
        global $firephp;

        $keys = array();

        foreach ($ary_execute as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }

            $ary_execute[$key] = "'" . $value . "'";
        }

        $q = preg_replace($keys, $ary_execute, $q, 1, $count);

        return $q;
    }

    /**
     * Esegue una query generica
     *
     * @access public
     * @param $q - La query
     * @param array $ary_execute (default: array()) - L'array contenente i valori da preparare
     * @return void
     */
    public function query($q, $ary_execute=array()) {
        global $firephp;

        try{
            $ex = $this->prepare($q);
            $ex->execute($ary_execute);

            $this->lastAffectedRows = $ex->rowCount();

        } catch(\PDOException $e) {
            $exception_details = $this->getExceptionDetails($e, $q, $ary_execute);
            (new \MSFramework\Modules\errorTracker())->saveSQLErrorLog($exception_details);

            $this->lastAffectedRows = 0;

            if($this->detailed_error_info) {
                return $exception_details;
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Esegue gli insert e gli update nel database, restituendo un array con le informazioni dell'errore in caso di errore, oppure true.
     *
     * @access public
     * @param mixed $q - La query
     * @param array $ary_execute (default: array()) - L'array contenente i valori da preparare (i valori null sono convertiti in "vuoto")
     * @return mixed
     */
    public function pushToDB($q, $ary_execute=array()) {
        global $firephp;
        try {
            $ex = $this->prepare($q);
            $ex->execute(
                array_map(function($v){
                    return (is_null($v)) ? "" : $v;
                },$ary_execute)
            );

            $this->lastAffectedRows = $ex->rowCount();

        } catch (\PDOException $e) {
            $exception_details = $this->getExceptionDetails($e, $q, $ary_execute);
            (new \MSFramework\Modules\errorTracker())->saveSQLErrorLog($exception_details);

            $this->lastAffectedRows = 0;

            if($this->detailed_error_info) {
                return $exception_details;
            } else {
                return false;
            }

        }

        return true;
    }


    /**
     * Elimina un record dal DB
     *
     * @param $table string La tabella interessata
     * @param $id_col Il nome della colonna che contiene l'ID del record
     * @param $id string|array L'ID del record
     *
     * @return bool
     */
    public function deleteRow($table, $id_col, $id) {
        global $firephp;

        try{

            if(is_array($id)) {

                if(!count($id)) {
                    return false;
                }

                $same_db = $this->composeSameDBFieldString($id, 'OR', $id_col);
                $q = "DELETE IGNORE FROM $table WHERE " . $same_db[0];
                $ary_execute = $same_db[1];
            } else {

                if($id === null) {
                    $id = '';
                } else if(empty($id)) {
                    return false;
                }

                $q = "DELETE IGNORE FROM $table WHERE `$id_col` = :id";
                $ary_execute = array(":id" => $id);
            }

            $ex = $this->prepare($q);
            $ex->execute($ary_execute);
        } catch(\PDOException $e) {

            return false; // Non ritorno errori per l'eliminazione

            $exception_details = $this->getExceptionDetails($e, $q, $ary_execute);
            (new \MSFramework\Modules\errorTracker())->saveSQLErrorLog($exception_details);

            if($this->detailed_error_info) {
                return $exception_details;
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Ottiene (a fronte dell'array passato) le stringhe formattate pronte per essere inserite nella query ed un array con i valori associati alla nuova stringa formattata.
     * L'array deve avere il formato "COLONNA_DB" => "valore"
     * La funzione restituirà una stringa "COLONNA_DB" = :colonnadb ed un array ":colonnadb" => "valore"
     *
     * @access public
     * @param mixed $array - L'array
     * @param mixed $type - Che tipo di stringa utilizzare (se insert o update)
     * @return mixed
     */
    function createStringForDB($array, $type) {
        $string_update = "";
        $string_insert_keys = "";
        $string_insert_values = "";
        unset($array_pairs);

        foreach($array as $k => $v) {
            if($type == "update") {
                $string_update .= "`" . $k . "` = :" . $this->formatNamedParameter($k) . ", ";
            } else if($type == "insert") {
                $string_insert_keys .= "`" . $k . "`, ";
                $string_insert_values .= ":" . $this->formatNamedParameter($k) . ", ";
            }

            $array_pairs[":" . $this->formatNamedParameter($k)] = $v;
        }

        if($type == "update") {
            $string_update = substr($string_update, 0, -2);

            return array($array_pairs, $string_update);
        } else if($type == "insert") {
            $string_insert_keys = substr($string_insert_keys, 0, -2);
            $string_insert_values = substr($string_insert_values, 0, -2);

            return array($array_pairs, $string_insert_keys, $string_insert_values);
        }
    }

    /**
     * Restituisce stringa ed array per execute() in caso di controlli sullo stesso campo del db
     * Quando vengono eseguiti più controlli sullo stesso campo del db (es ID = 1 OR ID = 2) bisogna posizionare placeholder con valori diversi al posto di 1 e 2 (es ID = :id1 OR ID = :id2).
     *
     * @access public
     * @param mixed $array - L'array unidimensionale con i valori da confrontare per lo stesso campo
     * @param mixed $andor - L'operatore della query (AND/OR)
     * @param mixed $field - Il campo in cui cercare. Se all'interno di $field viene passata una funzione, evito di forzare l'aggiunta dei backtick (dovrà farlo l'utente dove necessario)
     * @param string $operator - L'operatore per il confronto
     * @param string $jolly - Se $operator è "LIKE" si utilizza la variabile $jolly per determinare dove inserire il carattere %. I possibili valori sono before, after e both
     * @param string $prepend_to_col - Se valorizzato, inserisce il valore della variabile prima del nome della colonna ($field) (utile per le JOIN)
     * @param string $prepend_to_search - Se valorizzato, inserisce il valore della variabile prima del valore da cercare
     * @param string $append_to_search - Se valorizzato, inserisce il valore della variabile dopo del valore da cercare
     * @return void
     */
    function composeSameDBFieldString($array, $andor, $field, $operator = "=", $jolly = "both", $prepend_to_col = "", $prepend_to_search = "", $append_to_search = "") {
        if(!is_array($array)) {
            $array = array($array);
        }

        $count_for_field = 0;
        $string = "";
        $jolly_before = "";
        $jolly_after = "";

        if($operator == "LIKE") {
            if($jolly == "both" || $jolly == "before") {
                $jolly_before = "%";
            }

            if($jolly == "both" || $jolly == "after") {
                $jolly_after = "%";
            }
        }

        if($prepend_to_col != "") {
            $prepend_to_col = "`" . $prepend_to_col . "`.";
        }

        $addfield_backtick = "`";
        if(strstr($field, "(") && strstr($field, ")")) {
            $addfield_backtick = "";
        }

        foreach($array as $single) {
            $append_random_str = '_' . str_replace(array(".", " "), array(), microtime()) . rand(1, 100);
            $string .= " $andor $prepend_to_col$addfield_backtick$field$addfield_backtick $operator :" . $this->formatNamedParameter($prepend_to_col.$field) . $count_for_field . $append_random_str;
            $array_to_return[":" . $this->formatNamedParameter($prepend_to_col.$field) . $count_for_field . $append_random_str] = $jolly_before . $prepend_to_search . $single . $append_to_search . $jolly_after;
            $count_for_field++;
        }

        return array(substr($string, strlen($andor)+1), $array_to_return);
    }

    /**
     * Restituisce stringa ed array per execute() per effettuare ricerche di testo in una serie di colonne specificate (ad esempio una parte del nome nome ed una parte del cognome).
     * La ricerca viene effettuata per ogni termine in ogni colonna, e restituirà un risultato se parte della stringa è presente all'interno della colonna in questione.
     *
     * @param $string La stringa da cercare. Le parole separate da spazi verranno ricercate singolarmente in tutte le colonne
     * @param $db_fields La (o le) colonne in cui effettuare la ricerca
     *
     * @return array
     */
    public function composeSearch($string, $db_fields) {
        $where_str = '';
        $where_ary = array();

        $expl_term = explode(" ", $string);

        foreach($expl_term as $str) {
            $where_str .= "(";

            foreach($db_fields as $field) {
                $same_db = $this->composeSameDBFieldString($str, 'OR', $field, 'LIKE');

                $where_str .= $same_db[0] . " OR ";
                $where_ary = array_merge($where_ary, $same_db[1]);
            }

            $where_str = substr($where_str, 0, -4) . ") AND ";
        }

        if($where_str != "") {
            $where_str = ' AND ' . substr($where_str, 0, -5);
        }

        return array($where_str, $where_ary);
    }

    /**
     * Restituisce il numero di righe corrispondenti alla query
     *
     * @access public
     * @param mixed $q - La query
     * @param array $ary_execute (default: array()) - L'array contenente i valori da preparare
     * @return mixed
     */
    public function getCount($q, $ary_execute=array()) {
        global $firephp;

        $replacedQuery = $this->getReplacedQuery($q, $ary_execute);
        $replaced = preg_replace('/^SELECT ([^FROM]+) FROM/m', 'SELECT COUNT(*) as count_total FROM', $replacedQuery);
        $assoc = $this->getAssoc($replaced, array(), true)['count_total'];

        if(!array_key_exists("errorInfo", $assoc) && $assoc !== false) {
            return $assoc;
        } else {
            if($this->detailed_error_info) {
                return $assoc;
            } else {
                return false;
            }
        }
    }

    /**
     * Restituisce una stringa pulita per poter essere utilizzata come "named parameter" nelle query (es. :TEST-COL(1) diventa :TESTCOL1)
     *
     * @access public
     * @param mixed $str
     * @return void
     */
    public function formatNamedParameter($str) {
        return preg_replace("/[^a-zA-Z0-9]/", "", $str);
    }

    /**
     * Verifica se si sta creando un nuovo record o se si sta aggiornando un record esistente. Utile in combinazione con la funzione createStringForDB
     *
     * @param $id
     *
     * @return string
     */
    public function checkInsertOrUpdate($id) {
        if($id === "" || $id === 0 || !$id) {
            return "insert";
        } else {
            return "update";
        }
    }

    /**
     * Restituisce un array di tabelle prelevate dal database dummy (DUMMY_DB_NAME) effettuando la ricerca per prefisso (es: ecommerce_)
     *
     * @param $prefix Il prefisso da cercare
     *
     * @return bool|void
     */
    public function getFromDummyByPrefix($prefix) {
        if($prefix == "") {
            return false;
        }

        return $this->getAssoc("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" . DUMMY_DB_NAME . "' AND TABLE_NAME LIKE '" . $prefix . "%'");
    }

    /**
     * Verifica se memcached è abilitato o meno
     *
     * @return bool
     */
    public function cacheEnabled() {
        return false;

        $r = (new \MSFramework\cms())->getCMSData('producer_config');
        if($r['memcached_on'] == "1" || $r['memcached_on'] == "") {
            return true;
        }

        return false;
    }

    /**
     * Svuota completamente la cache (di tutti i domini)
     *
     * @return bool
     */
    public function flushCache() {
        return true;
    }

    /**
     * Svuota completamente la cache del dominio corrente.
     *
     * @param string $ns Passando questo parametro, verranno eliminate solo le chiavi che hanno come NS nomedominiocorrente_$ns. $ns, ad esempio, può essere il nome di una tabella del DB. In questo caso, verrebbe svuotata solo la cache per una determinata tabella del DB (e solo per il dominio corrente)
     * @param string $host Passando questo parametro, verranno eliminate solo le chiavi per un determinato host
     */
    public function flushCacheByNameSpace($ns = "", $host = "") {
       return true;
    }

    /**
     * Ottiene tutti i dati memorizzati nella cache per tutti i domini
     *
     * @return array
     */
    public function getCacheStatus() {
       return array();
    }

    /**
     * Ottiene tutti i dati memorizzati nella cache per il dominio corrente
     *
     * @param string $ns Passando questo parametro, verranno restituite solo le chiavi che hanno come NS nomedominiocorrente_$ns. $ns, ad esempio, può essere il nome di una tabella del DB. In questo caso, verrebbe restituita solo la cache per una determinata tabella del DB (e solo per il dominio corrente)
     *
     * @return array
     */
    public function getCacheStatusByNameSpace($ns = "") {
       return array();
    }

    /**
     * Effettua l'hash della chiave specificata tenendo conto delle opzioni globali di memcached
     *
     * @param $key La chiave su cui applicare l'hash
     *
     * @return string
     */
    private function hashCacheKey($key) {
        $hash_method = $this->memcached->getOption(\Memcached::OPT_HASH);
        if($hash_method == \Memcached::HASH_MD5) {
            return md5($key);
        } else {
            return $key;
        }
    }

    /**
     * Restituisce un array con i dettagli dell'eccesione restituita
     *
     * @param $e L'oggetto dell'eccesione
     * @param $q La query effettuata
     * @param $ary_execute L'array di parametri per la query
     *
     * @return array
     */
    private function getExceptionDetails($e, $q, $ary_execute) {
        return array(
            'error_info' => parent::errorInfo(),
            'exception_message' => $e->getMessage(),
            'query' => $this->getReplacedQuery($q, $ary_execute)
        );
    }

    /**
     * Effettua la copia di un database
     *
     * @param $from Il database da cui copiare
     * @param $to Il database in cui copiare
     * @param bool $overwrite Se true, sovrascrive il contenuto del DB di destinazione
     * @param bool $create Se true, crea il DB di destinazione se non esiste
     *
     * @return bool
     */
    public function copyDB($from, $to, $overwrite = false, $create = false) {
        $cPanel = new \cPanel\Database\database();

        $dest_exists = $this->checkDBExists($to);
        $newly_created = false;

        if(!$dest_exists && $create) {
            $cPanel->create($to);
            $cPanel->assignUser($to, FRAMEWORK_DB_USER);
            $cPanel->close();

            $newly_created = true;
        }

        if($overwrite || (!$overwrite && $newly_created)) {
            shell_exec('mysqldump ' . $from . ' --password=' . FRAMEWORK_DB_PASS . ' --user=' . FRAMEWORK_DB_USER . ' | mysql -u ' . FRAMEWORK_DB_USER . ' -p' . FRAMEWORK_DB_PASS . ' -h ' . FRAMEWORK_DB_HOST . ' ' . $to);
        } else {
            return false;
        }

        return true;
    }
}
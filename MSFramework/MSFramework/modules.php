<?php
/**
 * MSFramework
 * Date: 23/05/18
 */

namespace MSFramework;

class modules {

    private $activeModule = null;

    public function __construct() {
        global $firephp;

        Global $MSFrameworkCMS, $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Restituisce i dati selezionabili/utilizzabili relativamente ai moduli extra attivabili sul sito
     *
     * @param string $key Impostando questo parametro si riceve solo la chiave desiderata e non l'intero array
     *
     * @return array|mixed
     */
    public function getModules($key = "") {

        if(!is_null($this->activeModule)) {
            $ary = $this->activeModule;
        } else {
            $ary = array(
                "bridge" => array(
                    "name" => "FW360 - Bridge",
                    "path" => "MSFramework\\Modules\\bridge",
                    "required" => true
                ),
                "livesales" => array(
                    "required_functions" => array('ecommerce'),
                    "name" => "Live Sales",
                    "path" => "MSFramework\\Ecommerce\\Modules\\livesales",
                    "required" => false // Se impostato su true il modulo è sempre attivo
                ),
                "ecommerceCore" => array(
                    "required_functions" => array('ecommerce'),
                    "name" => "Core Ecommerce",
                    "path" => "MSFramework\\Ecommerce\\Modules\\core",
                    "required" => true // Se impostato su true il modulo è sempre attivo
                ),
                "ecommerceFeeds" => array(
                    "required_functions" => array('ecommerce'),
                    "name" => "Feed Ecommerce",
                    "path" => "MSFramework\\Ecommerce\\Modules\\feeds",
                    "required" => true // Se impostato su true il modulo è sempre attivo
                ),
                "conditionalDiscount" => array(
                    "required_functions" => array('ecommerce'),
                    "name" => "Sconto condizionato",
                    "path" => "MSFramework\\Ecommerce\\Modules\\conditionalDiscount",
                    "required" => false // Se impostato su true il modulo è sempre attivo
                ),
                "payments" => array(
                    "required_functions" => array('ecommerce'),
                    "name" => "Pagamenti",
                    "path" => "MSFramework\\Ecommerce\\Modules\\payments",
                    "required" => true // Se impostato su true il modulo è sempre attivo
                ),
                "points" => array(
                    "required_functions" => array('points'),
                    "name" => "Sistema Punti Clienti",
                    "path" => "MSFramework\\Ecommerce\\Modules\\points",
                    "required" => true, // Se impostato su true il modulo è sempre attivo
                    "check" => function () {
                        Global $MSFrameworkCMS;
                        $pointSettings = $MSFrameworkCMS->getCMSData('ecommerce_points');
                        return ($pointSettings['enabled'] == "1" ? true : false);
                    },
                ),
                "subscriptions_payments" => array(
                    "required_functions" => array('subscriptions'),
                    "name" => "Pagamenti",
                    "path" => "MSFramework\\Subscriptions\\Modules\\payments",
                    "required" => true // Se impostato su true il modulo è sempre attivo
                ),
                "timeonsitetracker" => array(
                    "required_functions" => array(),
                    "name" => "Time On Site Tracker",
                    "path" => "MSFramework\\Modules\\timeonsitetracker",
                    "required" => true
                ),
                "emails" => array(
                    "required_functions" => array(),
                    "name" => "Email (Contatti & Newsletter)",
                    "path" => "MSFramework\\Modules\\emails",
                    "required" => true
                ),
                "simplebooking" => array(
                    "required_functions" => array('hotel'),
                    "name" => "SimpleBooking per Hotel",
                    "path" => "MSFramework\\Hotels\\Modules\\simplebooking",
                    "required" => false
                ),
                "footerbar" => array(
                    "required_functions" => array(),
                    "name" => "Footer Bar Mobile",
                    "path" => "MSFramework\\Hotels\\Modules\\footerbar",
                    "required" => false
                ),
                "gdpr" => array(
                    "required_functions" => array(),
                    "name" => "GDPR",
                    "path" => "MSFramework\\Modules\\gdpr",
                    "required" => false,
                    "extra_settings" => true,
                ),
                "ticket" => array(
                    "required_functions" => array('ticket'),
                    "name" => "Ticket System",
                    "path" => "MSFramework\\Ticket\\Modules\\ticket",
                    "required" => true
                ),
                "richsnippets" => array(
                    "required_functions" => array(),
                    "name" => "Rich Snippets",
                    "path" => "MSFramework\\Modules\\richsnippets",
                    "required" => false
                ),
                "forms" => array(
                    "required_functions" => array(),
                    "name" => "Forms",
                    "path" => "MSFramework\\Modules\\forms",
                    "required" => true
                ),
                "multiportal" => array(
                    "required_functions" => array('realestate'),
                    "name" => "Integrazione Multiportale",
                    "extra_settings" => true,
                    "path" => "MSFramework\\RealEstate\\Modules\\multiportal"
                ),
                "scraper" => array(
                    "required_functions" => array('realestate'),
                    "name" => "Scraper",
                    "extra_settings" => true,
                    "path" => "MSFramework\\RealEstate\\Modules\\scraper"
                ),
                "tripadvisor_autofill" => array(
                    "required_functions" => array(),
                    "name" => "TripAdvisor (Autofill Recensione)",
                    "extra_settings" => true,
                    "path" => "MSFramework\\Modules\\tripAdvisorReview"
                ),
                "error_tracker" => array(
                    "required_functions" => array(),
                    "name" => "Error Tracker",
                    "extra_settings" => true,
                    "path" => "MSFramework\\Modules\\errorTracker",
                    "required" => true
                ),
                "trackingsuite" => array(
                    "required_functions" => array(),
                    "name" => "TrackingSuite",
                    "extra_settings" => true,
                    "path" => "MSFramework\\Modules\\trackingSuite",
                    "required" => true
                ),
                "frontend_parser" => array(
                    "required_functions" => array(),
                    "name" => "Frontend Parser",
                    "extra_settings" => true,
                    "path" => "MSFramework\\Frontend\\Modules\\frontendParser",
                    "required" => true
                ),
                "popup" => array(
                    "required_functions" => array(),
                    "check" => function () {
                        return ($this->MSFrameworkDatabase->getCount("SELECT id FROM popup") ? true : false);
                    },
                    "name" => "Popup",
                    "extra_settings" => false,
                    "path" => "MSFramework\\Modules\\popup",
                    "required" => true
                )
            );

            // Rimuovo i moduli che non hanno le dipendenze richieste
            $extra_functions_db = json_decode($this->MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true);
            foreach ($ary as $id => $module) {
                foreach ($module['required_functions'] as $required_function) {
                    if (is_array($required_function)) {
                        $unset_module = true;
                        foreach ($required_function as $required_or) {
                            if (in_array($required_or, $extra_functions_db)) {
                                $unset_module = false;
                            }
                        }
                        if ($unset_module) {
                            unset($ary[$id]);
                        }
                    } else {
                        if (!in_array($required_function, $extra_functions_db)) {
                            unset($ary[$id]);
                        }
                    }
                }

                if (isset($ary[$id]) && isset($ary[$id]['check'])) {
                    if (!$ary[$id]['check']()) unset($ary[$id]);
                }
            }

            $this->activeModule = $ary;
        }

        if($key != "") {
            return $ary[$key];
        } else {
            return $ary;
        }
    }

    /**
     * Imposta tutti gli Hooks dei moduli
     */
    public function setModulesHooks()
    {
        $all_modules = $this->getModules();
        foreach($all_modules as $module_id => $module) {
            if(class_exists($module['path'])) {
                $moduleClass = new $module['path']();
                if(method_exists($moduleClass, 'setHooks')) {
                    $moduleClass->setHooks();
                }
            }
        }
    }

    /**
     * Ottiene i file CSS & JS da includere nel front del progetto
     *
     * @param $from_where string La posizione dalla quale si desidera ottenere i file da includere [header|footer]
     *
     * @return array
     */
    public function getModulesFiles($from_where = 'footer')
    {
        $botList = array(
            'MJ12bot',
            'AhrefsBot',
            'http://www.bing.com/bingbot.htm',
            'SemrushBot'
        );

        foreach($botList as $bot) {
            if (stristr($_SERVER['HTTP_USER_AGENT'], $bot)) {
                return array();
            }
        }

        $all_modules = $this->getModules();

        $default_files_structure = array(
            'header' => array(
                'js' => array(),
                'css' => array(),
                'html' => array(),
            ),
            'footer' => array(
                'js' => array(),
                'css' => array(),
                'html' => array(),
            ),
            'admin_header' => array(
                'js' => array(),
                'css' => array(),
                'html' => array(),
            ),
            'admin_footer' => array(
                'js' => array(),
                'css' => array(),
                'html' => array(),
            )
        );

        $files_to_include = $default_files_structure;

        foreach($all_modules as $module_id => $module) {
            if($module['required'] || ($this->checkIfIsActive($module_id)) && class_exists($module['path'])) {
                $module_files = (new $module['path']())->getFilesToInclude();

                // Se non è stata indicata la posizione allora mostro il JS e l'HTML nel footer e il CSS nell'header (FRONT)
                if(!isset($module_files['header']) && !isset($module_files['footer'])) {
                    $compatible_module_files = $default_files_structure;
                    if(isset($module_files['css']) && $module_files['css']) $compatible_module_files['header']['css'] = array_merge($module_files['css'], (is_array($compatible_module_files['header']['css']) ? $compatible_module_files['header']['css'] : array()));
                    if(isset($module_files['js']) && $module_files['js']) $compatible_module_files['footer']['js'] = array_merge($module_files['js'], (is_array($compatible_module_files['footer']['js']) ? $compatible_module_files['footer']['js'] : array()));
                    if(isset($module_files['html']) && $module_files['html']) $compatible_module_files['footer']['html'] = array_merge($module_files['html'], (is_array($compatible_module_files['footer']['html']) ? $compatible_module_files['footer']['html'] : array()));
                    $module_files = $compatible_module_files;
                }

                $files_to_include = array_merge_recursive($files_to_include, $module_files);
            }
        }

        return $files_to_include[$from_where];
    }

    /**
     * Esegue le funzioni dei vari moduli
     *
     * @param $where string Il luogo dalla quale viene eseguita la funzione [header|footer]
     *
     * @return array
     */
    public function executeFunctions($where = 'footer')
    {
        $all_modules = $this->getModules();

        foreach($all_modules as $module_id => $module) {
            if(($module['required'] || $this->checkIfIsActive($module_id)) && class_exists($module['path'])) {
                $class = new $module['path']();
                if(method_exists($class, 'executeFunctions_' . $where)) {
                    $class->{'executeFunctions_' . $where}();
                }
            }
        }
    }

    /**
     * Verifica se un determinato modulo è attivo o meno
     *
     * @param $module_id L'ID del modulo
     *
     * @return bool
     */
    public function checkIfIsActive($module_id) {
        $r = $this->MSFrameworkCMS->getCMSData('producer_config');
        $r_extra_modules = json_decode($r['extra_modules'], true);

        return in_array($module_id, $r_extra_modules);
    }

    /**
     * Ottiene il valore di un settaggio considerando eventuali traduzioni e/o valori di default impostati
     *
     * @param $moduleK L'id del modulo al quale appartiene il settaggio
     * @param $settingK L'id del settaggio per il quale si desidera ottenere il valore
     *
     * @return mixed
     */
    public function getSettingValue($moduleK, $settingK) {
        global $MSFrameworki18n;

        $r = $this->MSFrameworkCMS->getCMSData('modules_settings');
        $r_settings = json_decode($r['settings'], true);
        $field_value = $r_settings[$moduleK][$settingK];

        $moduleSettings = $this->getModuleSettings($moduleK);
        $settingV = $moduleSettings[$settingK];

        if($settingV['input_type'] == "checkbox") {
            $field_value = ($field_value == "true") ? true : false;
        }

        if($settingV['translations']) {
            if(!strstr(REAL_SW_PATH, "MSAdminControlPanel")) {
                $field_value = $MSFrameworki18n->getFieldValue($field_value);
            }
        }

        if($settingV['default'] && $field_value == "") {
            $field_value = $settingV['default'];
        }

        return $field_value;
    }

    /**
     * Ottiene tutti i settaggi disponibili per il modulo in questione. L'array ottenuto è nel seguente formato e si compone dei seguenti parametri:
     *
     * "duration" => array( [la chiave dell'array rappresenta l'id - obbligatorio]
     *      "name" => "Durata", [il nome - obbligatorio]
     *      "input_type" => "number", [il tipo di input da utilizzare all'interno dell'ACP - sono supportati text|number|checkbox|image - obbligatorio]
     *      "uploader_settings" => array("max_uploads" => "1", "min_width" => "250", "min_height" => "250", "tn_width" => "100") [le impostazioni da applicare all'input_type=image - obbligatorio se si sceglie "image" come input type. se non impostato, vengono applicati i valori di default]
     *      "input_attributes" => array("min" => "0"), [gli attributi da aggiungere all'input - NON obbligatorio]
     *      "input_classes" => "", [le classi da aggiungere all'input - NON obbligatorio]
     *      "helper" => "", [l'helper da visualizzare - NON obbligatorio]
     *      "mandatory" => false, [indica se il campo è obbligatorio - NON obbligatorio]
     *      "translations" => false, [indica se il campo gestisce le traduzioni - NON obbligatorio]
     *      "default" => "", [il valore di default per il campo - obbligatorio se il campo è obbligatorio, NON obbligatorio se il campo non lo è]
     *  )
     *
     * @param $moduleK L'ID del modulo
     *
     * @return bool
     */
    public function getModuleSettings($moduleK) {
        $class = $this->getModules($moduleK)['path'];

        if(class_exists($class)) {
            $moduleClass = new $class();
            return $moduleClass->getSettings();
        }

        return false;
    }


    /**
     * Alcuni moduli hanno bisogno di informazioni supplementari in fase di attivazione. Attraverso questa funzione è possibile recuperare tali informazioni supplementari.
     *
     * @param $moduleK L'ID del modulo
     *
     * @return array
     */
    public function getExtraModuleSettings($moduleK) {
        $r = $this->MSFrameworkCMS->getCMSData('producer_config');
        $r_extra_modules_settings = json_decode($r['extra_modules_settings'], true)[$moduleK];

        return $r_extra_modules_settings;
    }


    /**
     * Restituisce il prefisso che viene settato prima dello short code per le chiamate Ajax
     *
     * @return string
     */
    public function getShortPrefix() {
        return 'm';
    }
}
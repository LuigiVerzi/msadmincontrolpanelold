<?php
/**
 * MSFramework
 * Date: 05/03/18
 */

namespace MSFramework\Appearance;


class themes {
    public function __construct() {
        global $firephp, $MSFrameworkDatabase;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
    }

    /**
     * Ottiene la lista dei temi disponibili
     * @return array
     */
    public function getThemesList() {
        $themes_list = array();
        foreach(glob(PROD_SITES_HOME_FOLDER . 'MSCommonStorage/MSThemes/*', GLOB_ONLYDIR) as $theme_path) {
            $theme_id = end(explode('/', $theme_path));
            $themes_list[$theme_id] = $this->getThemeInfo($theme_id);
        }

        return $themes_list;
    }

    /**
     * Ottiene le informazioni di un tema
     * @param string $theme_id L'ID del tema
     * @return array
     */
    public function getThemeInfo($theme_id) {

        $theme_path = PROD_SITES_HOME_FOLDER . 'MSCommonStorage/MSThemes/' . $theme_id;
        $theme_info = json_decode(file_get_contents(PROD_SITES_HOME_FOLDER . 'MSCommonStorage/MSThemes/' . $theme_id . '/info.json'), true);

        $theme_preview = '';
        $theme_gallery = array();
        foreach(glob($theme_path . '/preview/*') as $theme_image) {
            $image_name = end(explode('/', $theme_image));

            if(!stristr($image_name, 'main')) {
                $theme_gallery[] = PATH_TO_COMMON_STORAGE_HTML . 'MSThemes/' . $theme_id . '/preview/' . $image_name;
            } else {
                $theme_preview = PATH_TO_COMMON_STORAGE_HTML . 'MSThemes/' . $theme_id . '/preview/' . $image_name;
            }
        }

        return array(
            'id' => $theme_id,
            'name' => $theme_info['name'],
            'description' => $theme_info['description'],
            'details' => $theme_info['details'],
            'functionalities' => $theme_info['functionalities'],
            'dummy_data' => $theme_info['dummy_data'],
            'default_settings' => $theme_info['default_settings'],
            'preview' => $theme_preview,
            'images' => $theme_gallery,
            'path' => $theme_path,
            'url' => PATH_TO_COMMON_STORAGE_HTML . 'MSThemes/' . $theme_id . '/'
        );
    }

    /**
     * Ottiene gli shortcode delle impostazioni del tema
     * @param $theme_settings
     * @param $parent
     * @return array
     */
    public function getSettingsShortcodes($theme_settings = false, $parent = null) {
        Global $MSFrameworkCMS;

        if(!$theme_settings) $theme_settings = $MSFrameworkCMS->getCMSData('theme_info')['settings'];

        $paths = array();

        if($parent !== null) $parent = $parent . '->';

        foreach($theme_settings as $k => $v) {
            if(is_array($v)) {
                $currentPath = $parent . $k;
                $paths = array_merge($paths, $this->getSettingsShortcodes($v, $currentPath));
            } else {
                $paths['{theme->' . $parent . $k . '}'] = $v;
            }
        }

        return $paths;
    }

    public function getTableToExport($limit_to_existent = false) {
        $exportable_tables = array(
            'Standard' => array(
                'pagine' => array(
                    'also' => array('pagine_private'),
                    'title' => 'Pagine',
                    'uploads' => array('PAGEGALLERY', 'PAGEWIDGET')
                ),
                'blocks' => array(
                    'title' => 'Blocchi',
                    'uploads' => array('PAGEGALLERY')
                ),
                'forms' => array(
                    'title' => 'Form',
                    'uploads' => array('FORMS')
                ),
                'faq' =>  array(
                    'title' => 'FAQ',
                    'uploads' => array('FAQ')
                ),
                'gallery' => array(
                    'title' => 'Gallery',
                    'uploads' => array('GALLERY')
                ),
                'popup' => array(
                    'title' => 'Popup',
                    'uploads' => array('POPUP')
                ),
                'recensioni' => array(
                    'title' => 'Recensioni',
                    'uploads' => array('REVIEWS')
                ),
            ),
            /* SERVIZI */
            'Servizi' => array(
                'servizi' => array(
                    'title' => 'Servizi',
                    'uploads' => array('SERVICES')
                ),
                'servizi_categorie' => array(
                    'title' => 'Categorie Servizi',
                    'uploads' => array('SERVICES_CATEGORY')
                ),
                'servizi_portfolio' => array(
                    'title' => 'Portfolio Servizi',
                    'uploads' => array('SERVICES_PORTFOLIO')
                ),
            ),
            /* Agenzia immobiliare */
            'Agenzia Viaggio' => array(
                'travelagency__packages' => array(
                    'title' => 'Pacchetti Agenzia Viaggio',
                    'uploads' => array('TRAVELAGENCY_PACKAGE')
                ),
                'travelagency__packages_categories' => array(
                    'title' => 'Categorie Agenzia Viaggio',
                    'uploads' => array('TRAVELAGENCY_PACKAGE_CATEGORY')
                ),
            ),
            /* BLOG */
            'Blog' => array(
                'blog_posts' => array(
                    'title' => 'Articoli Blog',
                    'uploads' => array('BLOG_POSTS')
                ),
                'blog_categories' => array(
                    'title' => 'Categorie Blog',
                    'uploads' => array('BLOG_CATEGORIES')
                ),
            ),
            /* ECOMMERCE */
            'Ecommerce' => array(
                'ecommerce_products' => array(
                    'title' => 'Prodotti',
                    'uploads' => array('ECOMMERCE_PRODUCTS', 'ECOMMERCE_ATTACHMENTS')
                ),
                'ecommerce_categories' => array(
                    'title' => 'Categorie Prodotti',
                    'uploads' => array('ECOMMERCE_CATEGORIES')
                ),
                'ecommerce_brand' => array(
                    'title' => 'Brand',
                    'uploads' => array('ECOMMERCE_BRAND')
                ),
                'ecommerce_attributes' => array(
                    'title' => 'Attrbuti',
                    'uploads' => array('ECOMMERCE_ATTRIBUTES')
                ),
                'ecommerce_attributes_values' => array(
                    'title' => 'Valore attributi',
                    'uploads' => array()
                ),
            ),
            /* REAL ESTATE */
            'Agenzia Immobiliare' => array(
                'realestate_categories' => array(
                    'title' => 'Categorie Immobili',
                    'uploads' => array('REALESTATE_CATEGORIES')
                ),
                'realestate_immobili' => array(
                    'title' => 'Immobili',
                    'uploads' => array('REALESTATE_IMMOBILI')
                ),
            ),
            /* HOTEL */
            'Hotel' => array(
                'hotel_rooms' => array(
                    'title' => 'Camere Hotel',
                    'uploads' => array('ROOMGALLERY')
                ),
                'hotel_rooms_cats' => array(
                    'title' => 'Categorie Camere',
                    'uploads' => array('ROOMGALLERY')
                ),
                'hotel_offerte'=> array(
                    'title' => 'Offerte Hotel',
                    'uploads' => array('OFFERSGALLERY')
                ),
            ),
            /* CAMPEGGIO */
            'Campeggio' => array(
                'camping_piazzole'=> array(
                    'title' => 'Piazzole',
                    'uploads' => array('CAMPING_PIAZZOLE')
                ),
                'camping_offerte' => array(
                    'title' => 'Offerte Campeggio',
                    'uploads' => array('OFFERSGALLERY')
                ),
            )
        );

        if($limit_to_existent) {
            $tableNames = array();
            if(is_array($limit_to_existent)) {
                $tableNames = $limit_to_existent;
            } else {
                foreach ($this->MSFrameworkDatabase->getAssoc("SELECT table_name FROM information_schema.tables WHERE table_schema = '" . CUSTOMER_DOMAIN_INFO['database'] . "'") as $v) {
                    $tableNames[] = $v['table_name'];
                };
            }

            foreach($exportable_tables as $tableCategory => $tables) {
                foreach($tables as $tn => $tv) {
                    if (!in_array($tn, $tableNames)) unset($exportable_tables[$tableCategory][$tn]);
                }
                if(!count($exportable_tables[$tableCategory])) unset($exportable_tables[$tableCategory]);
            }

        }

        return $exportable_tables;
    }

    public function installTheme($theme_id, $install_content = false, $website_id = false) {
        Global $MSFrameworkCMS, $MSFrameworkDatabase, $MSFrameworkFW;

        $themeDetails = (new \MSFramework\Appearance\themes())->getThemeInfo($theme_id);

        $status = true;
        if($themeDetails) {

            if($website_id) {
                $customer_domain_info =  $MSFrameworkFW->getWebsitePathsBy('id', $website_id);
                $customer_website_path = $customer_domain_info['path'];
            } else {
                $customer_domain_info = CUSTOMER_DOMAIN_INFO;
                $customer_website_path = CUSTOMER_DOMAIN_INFO['path'];
            }

            if(!$customer_domain_info) {
                return false;
            }

            $template_files_path = $themeDetails['path'] . '/files/';

            mkdir($customer_website_path, 0777, true);

            // Elimino tutto il contenuto attuale della cartella /www del cliente
            shell_exec('rm -f -r ' . $customer_website_path . 'www/*');

            // Copio il contenuto del tema nella cartella /www del cliente
            shell_exec("cp -r " . $template_files_path . ". " . $customer_website_path . "www/");

            // Copio il contenuto della cartella settings nella cartella /www del cliente
            shell_exec('rm -f -r ' . $customer_website_path . "settings/*");
            shell_exec("cp -r " . $themeDetails['path'] . "/settings/. " . $customer_website_path . "settings/");

            if($install_content && count(glob($themeDetails['path'] . '/install/*.sql'))) {

                $extra_functions = $MSFrameworkCMS->getActiveExtraFunctions();

                // Installo tutte le funzionalità non attualmente attive
                $current_enabled_functions = json_decode($MSFrameworkCMS->getCMSData('producer_config', array(), false, $customer_domain_info['database'])['extra_functions'], true);
                foreach ($themeDetails['functionalities'] as $functionality) {
                    if (!in_array($functionality, $current_enabled_functions)) {

                        $module_db_prefix = $MSFrameworkCMS->getExtraFunctionsDetails($functionality)['db_prefix'];
                        if($module_db_prefix != "") {
                            foreach($this->MSFrameworkDatabase->getFromDummyByPrefix($module_db_prefix) as $table) {
                                $this->MSFrameworkDatabase->query("CREATE TABLE `" . $customer_domain_info['database']   . "`.`" . $table['TABLE_NAME'] . "` LIKE `" . DUMMY_DB_NAME . "`.`" . $table['TABLE_NAME'] . "`");
                            }
                        }

                        $extra_functions[] = $functionality;
                    }
                }

                // Svuoto tutte le tabelle che sto per riempire con i dati DUMMY
                foreach ($themeDetails['dummy_data'] as $table_to_truncate) {
                    $MSFrameworkDatabase->pushToDB("TRUNCATE `" . $customer_domain_info['database']   . "`.`" . $table_to_truncate . "`");
                }

                // Salvo la lista delle nuove funzionalità extra attive dopo l'importazione
                $MSFrameworkCMS->setCMSData("producer_config", array(
                    "extra_functions" => json_encode($extra_functions)
                ), true, $customer_domain_info['database']);

                // Copio tutti i dati DUMMY nel database
                foreach(glob($themeDetails['path'] . '/install/*.sql') as $sqlFile) {
                    shell_exec('mysql --user=' . FRAMEWORK_DB_USER . ' --password=' . FRAMEWORK_DB_PASS . ' --host=' . FRAMEWORK_DB_HOST . ' ' . $customer_domain_info['database'] . ' < ' . $sqlFile);
                }
                // Copio tutti gli uploads DUMMY nel sito web
                shell_exec("cp -r " . $themeDetails['path'] . "/uploads/. " . $customer_website_path . "uploads/");
            }

            // Salvo la referenza dell'ID del tema attivo
            $MSFrameworkCMS->setCMSData("theme_info", array(
                'id' => $theme_id,
                'settings' => $themeDetails['default_settings']
            ), false, $customer_domain_info['database']);

            if(!file_exists($customer_website_path . 'index.php')) {

                // Creo il file index.php se mancante
                file_put_contents($customer_website_path . 'index.php',
                    '<?php
                    define("SITE_FRAMEWORK_VERSION", "1");
                    require(dirname( __FILE__ , 2) . "/MSAdminControlPanel/v" . SITE_FRAMEWORK_VERSION . "/MSFramework/common/site-config.php");
                    $MSFrameworkParser->setSlug($_GET[\'slug\'])->parse();'
                );


                // Creo il file .htaccess se mancante
                file_put_contents($customer_website_path . '.htaccess',
                    'Options +FollowSymLinks
                    RewriteEngine On
                    
                    # ====== ASSETS ======
                    # Aggiungo WWW agli assets se non vengono trovati
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteCond %{REQUEST_URI} !www/
                    RewriteCond %{REQUEST_URI} \.(?!php|py|phtml|pl|cgi|html)\w+$
                    RewriteRule (.*)$ www/$1 [QSA,L]
                    
                    # Se ancora non esistono allora tolgo il WWW e li faccio puntare allo slug del FW
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteCond %{REQUEST_URI} www/
                    RewriteRule www/(.*)$ index.php?check_dynamic_url=true&slug=$1 [QSA]
                    
                    # Faccio passare tutti gli URL dal FW
                    RewriteCond %{SCRIPT_FILENAME} !-d
                    RewriteCond %{SCRIPT_FILENAME} !-f
                    RewriteRule ^(.*)$ index.php?check_dynamic_url=true&slug=$1 [QSA]'
                );

            }

        } else {
            $status = false;
        }

        return $status;
    }

}
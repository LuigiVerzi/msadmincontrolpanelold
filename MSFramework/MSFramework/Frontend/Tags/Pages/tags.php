<?php
/**
 * MSAdminControlPanel
 * Date: 04/08/2019
 */

namespace MSFramework\Frontend\Tags\Pages;

class tags
{

    private $currentPageDetails;

    public function __construct($currentPageDetails = array()) {
        $this->currentPageDetails = $currentPageDetails;
    }

    /**
     * @return array Ritorna la lista dei tag MS
     */
    public function getMSTags()
    {
        $ms_tags = array(
            'ms-page' => array(
                'name' => 'Pagina',
                'description' => 'Ottiene i dati di una pagina e permette di mostrarli passando degli shortcodes.',
                'module_relation' => 'contenuti_pagine',
                'has_returns' => array(
                    'returned_params' => array(
                        'page-title' => "Il titolo della pagina",
                        'page-content' => "Il contenuto della pagina"
                    ),
                    'example' => '<h1>{ms-page-title}</h1>' . PHP_EOL . '<div>{ms-page-content}</div>'
                ),
                'parameters' => array(
                    'required' => array(),
                    'optionals' => array(
                        'id' => array(
                            'description' => "L'ID della pagina da includere, se non indicato otterrà i dati della pagina corrente",
                            'accepted_params' => array(),
                            'type' => 'int'
                        )
                    )
                ),
                'childrens' => array(),
                'exec' => function($parameters, $settings, $content = '') {
                    Global $MSFrameworkParser, $MSFrameworki18n, $MSFrameworkPages;

                    if($parameters['id']) {
                        $pageDetails = $MSFrameworkPages->getPageDetails($parameters['id'])[$parameters['id']];
                        $MSFrameworkParser->currentPageDetails['title'] = $MSFrameworki18n->getFieldValue($pageDetails['titolo']);
                        $MSFrameworkParser->currentPageDetails['subtitle'] = $MSFrameworki18n->getFieldValue($pageDetails['subtitle']);
                        $MSFrameworkParser->currentPageDetails['content'] = $MSFrameworki18n->getFieldValue($pageDetails['content']);
                    }

                    if($MSFrameworkParser->currentPageDetails['content']) {
                        return strtr(
                            $content,
                            array(
                                '{ms-page-title}' => $MSFrameworkParser->currentPageDetails['title'],
                                '{ms-page-slogan}' => $MSFrameworkParser->currentPageDetails['subtitle'],
                                '{ms-page-content}' => $MSFrameworkParser->currentPageDetails['content']
                            )
                        );
                    }

                }
            ),
            'ms-page-content' => array(
                'name' => 'Pagina',
                'description' => 'Mostra il contenuto della pagina',
                'module_relation' => 'contenuti_pagine',
                'parameters' => array(
                    'required' => array(),
                    'optionals' => array(
                        'id' => array(
                            'description' => "L'ID della pagina da includere, se non indicato otterrà i dati della pagina corrente.",
                            'accepted_params' => array(),
                            'type' => 'int'
                        )
                    )
                ),
                'childrens' => array(),
                'exec' => function($parameters, $settings, $content = '') {
                    Global $MSFrameworkParser, $MSFrameworkCMS, $MSFrameworki18n, $MSFrameworkPages;

                    $pageContent = '';
                    if($parameters['id']) {
                        $pageDetails = $MSFrameworkPages->getPageDetails($parameters['id'])[$parameters['id']];

                        if($pageDetails) {
                            $pageContent = $MSFrameworki18n->getFieldValue($pageDetails['content']);
                        }
                    } else if($parameters['url']) {

                        if(!filter_var($parameters['url'], FILTER_VALIDATE_URL)) {
                            $parameters['url'] = $MSFrameworkCMS->getURLToSite() . str_replace(ABSOLUTE_SW_PATH_HTML, '', $parameters['url']);;
                        }

                        $pageContent = file_get_contents($parameters['url'] . '?ms-page-content');
                    } else if($MSFrameworkParser->currentPageDetails) {
                        $pageContent = $MSFrameworkParser->currentPageDetails['content'];
                    }

                    if($pageContent) {
                        return $MSFrameworkPages->parsePageContent($MSFrameworki18n->getFieldValue($pageContent));
                    }

                }
            )
        );

        return $ms_tags;

    }
}
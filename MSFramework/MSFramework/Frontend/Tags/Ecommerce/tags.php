<?php
/**
 * MSAdminControlPanel
 * Date: 04/08/2019
 */

namespace MSFramework\Frontend\Tags\Ecommerce;

class tags
{

    private $currentPageDetails;

    public function __construct($currentPageDetails = array()) {
        $this->currentPageDetails = $currentPageDetails;
    }

    /**
     * @return array Ritorna la lista dei tag MS
     */
    public function getMSTags()
    {
        $ms_tags = array(
            'ms-cart-widget' => array(
                'name' => 'Widget Carrello',
                'description' => 'Ottiene e mostra i prodotti contenuti nel carrello.',
                'has_returns' => array(
                    'returned_params' => array(
                        'cart-subtotal' => "Il sub-totale (iva non inclusa)",
                        'cart-total' => "Il totale (iva inclusa)",
                        'cart-url' => "L'URL della pagina carrello",
                        'cart-count' => "Il numero di prodotti nel carrello",
                        'checkout-url' => "L'URL della pagina checkout",
                    ),
                    'example' => ''
                ),
                'parameters' => array(
                    'required' => array(),
                    'optionals' => array()
                ),
                'cycle' => array(
                    'enabled' => true,
                    'returned_params' => array(
                        'product-url' => "L'URL del prodotto",
                        'product-quantity' => "La quantità del prodotto",
                        'product-image' => "L'immagine del prodotto",
                        'product-title' => "Il titolo del prodotto",
                        'product-category' => "La categoria del prodotto"
                    ),
                    'example' => array(
                        '<a href="{ms-product-url}" title="{ms-product-title}"><img src="{ms-product-image}"></a>'
                    )
                ),
                'childrens' => array(),
                'exec' => function($parameters, $settings, $content = '') {
                    Global $MSFrameworkParser, $MSFrameworki18n, $MSFrameworkPages;

                    $currentCartDetails = (new \MSFramework\Ecommerce\cart())->getCart();

                    $return_html = $content;

                    if($currentCartDetails['products']) {
                        $return_html = preg_replace('/<ms-empty-cart>(.*?)<\/ms-empty-cart>/ims', '', $return_html);

                        if(!empty($settings['ms-cycle'])) {

                            $msCycleContent = '';
                            foreach($currentCartDetails['products'] as $product) {
                                $msCycleContent .= str_replace(
                                    array(
                                        '{ms-product-id}',
                                        '{ms-product-url}',
                                        '{ms-product-image}',
                                        '{ms-product-title}',
                                        '{ms-product-variations}',
                                        '{ms-product-price}',
                                        '{ms-product-category}',
                                        '{ms-product-quantity}',
                                    ),
                                    array(
                                        $product['cart_unique_id'],
                                        (new \MSFramework\Ecommerce\products())->getURL($product['id']),
                                        UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML . $product['image'],
                                        $MSFrameworki18n->getFieldValue($product['nome']),
                                        '<span class="ms-cart-product-attribute">' . implode(', ', array_map(array($MSFrameworki18n, 'getFieldValue'), $product['formattedVariation'])) . '</span>',
                                        number_format(implode('.', $product['prezzo']['cart_price']), 2, ',', '.'),
                                        $MSFrameworki18n->getFieldValue($product['category_name']),
                                        $product['quantity'],
                                    ),
                                    $settings['ms-cycle']
                                );
                            }

                            $return_html = preg_replace('/<ms-cycle[^>]*>(.*?)<\/ms-cycle>/ims', $msCycleContent, $return_html);

                        }

                    } else {
                        $return_html = preg_replace('/<ms-not-empty-cart>(.*?)<\/ms-not-empty-cart>/ims', '', $return_html);
                    }

                    $return_html = preg_replace('/<ms-([^>]+)>|<\/ms-([^>]+)>/m', '', $return_html);

                    $return_html = strtr($return_html, array(
                        '{ms-cart-subtotal}' =>  number_format($currentCartDetails['total_price']['no_tax'], 2, ',', '.'),
                        '{ms-cart-total}' => number_format($currentCartDetails['total_price']['tax'], 2, ',', '.'),
                        '{ms-cart-url}' => '/cart',
                        '{ms-cart-count}' => count($currentCartDetails['products']),
                        '{ms-checkout-url}' => "/checkout",
                    ));

                    $return_html .= '<textarea class="ms-ecommerce-cart-tpl" style="display: none;">' . $content . '</textarea>';

                    return $return_html;

                }
            ),
            'ms-product-variations' => array(
                'name' => 'Select variazioni prodotto',
                'description' => 'Ottiene i select relativi alle variazioni del prodotto.',
                'has_returns' => array(),
                'parameters' => array(
                    'required' => array(),
                    'optionals' => array(
                        'id' => array(
                            'description' => "L'ID del prodotto di riferimento, se non indicato otterrà i dati del prodotto attuale (se disponibile)",
                            'accepted_params' => array(),
                            'type' => 'int'
                        )
                    )
                ),
                'cycle' => array(
                    'enabled' => true,
                    'returned_params' => array(
                        'variation-id' => "L'ID della variazione attuale",
                        'variation-title' => "Il nome della variazione",
                        'variation-options' => "La lista di \<option\> disponibili"
                    ),
                    'example' => array(
                        '<label>{ms-variation-id}</label><select>{ms-variation-options}</select>'
                    )
                ),
                'childrens' => array(),
                'exec' => function($parameters, $settings, $content = '') {
                    Global $MSFrameworkParser, $MSFrameworki18n, $MSFrameworkPages;

                    $currentProduct = array();
                    if($parameters['id']) {
                        $currentProduct = (new \MSFramework\Ecommerce\products())->getProductDetails($parameters['id'])[$parameters['id']];
                    } else if(in_array('product_detail', explode(' ', $MSFrameworkParser->currentPageDetails['type']))) {
                        $currentProduct = $MSFrameworkParser->currentPageDetails['details'];
                    }

                    $productAttributes = (new \MSFramework\Ecommerce\attributes())->formatAttributesForProduct($currentProduct['attributes'], true);

                    $return_html = $content;

                    $msCycleContent = '';
                    if(!empty($settings['ms-cycle'])) {

                        foreach($productAttributes['variations'] as $id => $variation) {
                            $options = '<option value="">Seleziona ' . $MSFrameworki18n->getFieldValue($variation['nome']) . '</option>';
                            foreach($variation['value'] as $value=>$info) {
                                $options .= '<option name="var_' . $id . '" value="' . $value . '">' . $MSFrameworki18n->getFieldValue($info['nome']) . '</option>';
                            }

                            $msCycleContent .= str_replace(
                                array(
                                    '{ms-variation-id}',
                                    '{ms-variation-title}',
                                    '{ms-variation-options}',
                                ),
                                array(
                                    $id,
                                    $MSFrameworki18n->getFieldValue($variation['nome']),
                                    $options
                                ),
                                $settings['ms-cycle']
                            );
                        }

                        $msCycleContent .= str_replace(
                            array(
                                '{ms-variation-id}',
                                '{ms-variation-title}',
                                '{ms-variation-options}',
                            ),
                            array(
                                'qty',
                                'Quantità',
                                '<option value="">Sto calcolando le scorte...</option>'
                            ),
                            $settings['ms-cycle']
                        );

                        $return_html = preg_replace('/<ms-cycle[^>]*>(.*?)<\/ms-cycle>/ims', $msCycleContent, $content);
                    }

                    if($msCycleContent) {
                        $return_html = '<div class="ms-product-variations" data-id="' . $currentProduct['id'] . '">' . $return_html . '</div>';
                    } else $return_html = '';

                    return $return_html;

                }
            ),
        );

        return $ms_tags;

    }
}
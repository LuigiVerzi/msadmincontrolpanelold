<?php
/**
 * MSAdminControlPanel
 * Date: 04/08/2019
 */

namespace MSFramework\Frontend\Tags\Profile;

class tags
{

    private $currentPageDetails;

    public function __construct($currentPageDetails = array()) {
        $this->currentPageDetails = $currentPageDetails;
    }

    /**
     * @return array Ritorna la lista dei tag MS
     */
    public function getMSTags()
    {
        $ms_tags = array(
            'ms-logged-user' => array(
                'name' => 'Utente loggato',
                'description' => 'Mostra il contenuto all\'interno solo se l\'utente è loggato.',
                'has_returns' => array(),
                'parameters' => array(),
                'childrens' => array(),
                'exec' => function($parameters, $settings, $content = '') {
                    Global $MSFrameworkParser, $MSFrameworki18n, $MSFrameworkPages;
                    if((new \MSFramework\customers())->getUserDataFromSession()) return $content;
                    return '';

                }
            ),
            'ms-not-logged-user' => array(
                'name' => 'Utente non loggato',
                'description' => 'Mostra il contenuto all\'interno solo se l\'utente <b>non</b> è loggato.',
                'has_returns' => array(),
                'parameters' => array(),
                'childrens' => array(),
                'exec' => function($parameters, $settings, $content = '') {
                    Global $MSFrameworkParser, $MSFrameworkCustomers, $MSFrameworki18n, $MSFrameworkPages;
                    if(!(new \MSFramework\customers())->getUserDataFromSession()) return $content;
                    return '';

                }
            )
        );

        return $ms_tags;

    }
}
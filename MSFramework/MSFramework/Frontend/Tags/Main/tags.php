<?php
/**
 * MSAdminControlPanel
 * Date: 04/08/2019
 */

namespace MSFramework\Frontend\Tags\Main;

class tags
{

    private $currentPageDetails;

    public function __construct($currentPageDetails = array()) {
        $this->currentPageDetails = $currentPageDetails;
    }

    /**
     * @return array Ritorna la lista dei tag MS
     */
    public function getMSTags()
    {
        $ms_tags = array(
            'ms-include' => array(
                'name' => 'Inclusioni blocchi',
                'description' => 'Include un file HTML (dalla cartella /includes)',
                'container' => false,
                'parameters' => array(
                    'required' => array(
                        'path' => array(
                            'description' => "Il percorso del file da includere (partendo dalla cartella /includes/)",
                            'accepted_params' => array(),
                            'type' => 'string'
                        )
                    ),
                    'optionals' => array()
                ),
                'childrens' => array(),
                'exec' => function($parameters, $settings = array()) {
                    Global $MSFrameworkCMS, $MSFrameworki18n, $MSFrameworkModules, $MSFrameworkUrl;
                    ob_start();
                    include(ABSOLUTE_SW_PATH . 'www/includes/' . $parameters['path']);
                    return ob_get_clean();

                }
            ),
            'ms-cms-data' => array(
                'name' => 'Dati CMS',
                'description' => 'Mostra i dati del CMS',
                'container' => false,
                'parameters' => array(
                    'required' => array(
                        'id' => array(
                            'description' => "Il tipo di informazione da ottenere",
                            'accepted_params' => array(),
                            'type' => 'int'
                        )
                    ),
                    'optionals' => array()
                ),
                'childrens' => array(),
                'exec' => function($parameters, $settings = array()) {
                    Global $MSFrameworkCMS, $MSFrameworki18n;

                    $siteCMSData = $MSFrameworkCMS->getCMSData('site');
                    $site_logos = json_decode($siteCMSData['logos'], true);

                    $params_match = array(
                        'name' => $MSFrameworki18n->getFieldValue($siteCMSData['nome']),
                        'description' => $MSFrameworki18n->getFieldValue($siteCMSData['motto']),

                        'logo' => UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo'],
                        'favicon' => UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['favicon'],

                        'email' => $siteCMSData['email'],
                        'phone' => $siteCMSData['cellulare'],

                        'business-name' => $siteCMSData['rag_soc'],
                        'pec' => $siteCMSData['pec'],
                        'vat' => $siteCMSData['piva'],
                        'address' => $MSFrameworkCMS->getFullAddress()
                    );

                    if(in_array($parameters['id'], array_keys($params_match))) {
                        return $params_match[$parameters['id']];
                    }

                    return '';
                }
            ),
            'ms-title' => array(
                'name' => 'Titolo della pagina',
                'description' => 'Da usare in sostituzione di ' . htmlentities('<title>') . ', mostra il titolo della pagina.',
                'container' => false,
                'parameters' => array(
                    'required' => array(),
                    'optionals' => array()
                ),
                'childrens' => array(),
                'exec' => function($parameters, $settings = array()) {
                    Global $MSFrameworkParser, $MSFrameworki18n;
                    if($MSFrameworki18n->getFieldValue($MSFrameworkParser->currentPageDetails['seo']['title'])) {
                        return '<title>' . $MSFrameworki18n->getFieldValue($MSFrameworkParser->currentPageDetails['seo']['title']) . '</title>';
                    } else {
                        return '<title>' . $MSFrameworkParser->currentPageDetails['title'] . ' | {ms-site-name}</title>';
                    }
                }
            ),
            'ms-global-block' => array(
                'name' => 'Blocco globale',
                'description' => 'Mostra il contenuto di un blocco dinamico',
                'module_relation' => 'contenuti_blocchi',
                'parameters' => array(
                    'required' => array(
                        'id' => array(
                            'description' => "L'ID del blocco generato",
                            'accepted_params' => array(),
                            'type' => 'int'
                        )
                    ),
                    'optionals' => array()
                ),
                'childrens' => array(),
                'exec' => function($parameters, $settings) {
                    Global $MSFrameworki18n, $MSFrameworkVisualBuilder;

                    $blockDetails =  (new \MSFramework\blocks())->getBlockDetails($parameters['id']);

                    if($blockDetails) {
                        $blockDetails = $blockDetails[$parameters['id']];

                        $blockJSON = str_replace(
                            array('<!--VISUAL-BUILDER-START-->', '<!--VISUAL-BUILDER-END-->'),
                            array(),
                            $MSFrameworki18n->getFieldValue($blockDetails['content'])
                        );

                        if(json_decode($blockJSON)) {
                            return $MSFrameworkVisualBuilder->drawVisualContent(json_decode($blockJSON, true), true);
                        }
                    }

                    return '';

                }
            ),
            'ms-menu' => array(
                'name' => 'Menù',
                'description' => 'Genera un menù',
                'container' => false,
                'module_relation' => 'contenuti_menu',
                'parameters' => array(
                    'required' => array(
                        'id' => array(
                            'description' => "L'ID del menù",
                            'accepted_params' => array(),
                            'type' => 'int'
                        )
                    ),
                    'optionals' => array()
                ),
                'childrens' => array(
                    'ms-menu-class' => array(
                        'name' => 'Imposta la classe delle voci del menù',
                        'description' => 'Imposta la classe da mostrare negli elementi del menù',
                        'required' => false,
                        'parameters' => array(
                            'required' => array(
                                'type' => array(
                                    'description' => "La tipologia del campo",
                                    'accepted_params' => array('main_element_append_class', 'main_element_active_class', 'dropdown_class', 'sub_element_append_class'),
                                    'type' => 'string'
                                ),
                                'class' => array(
                                    'description' => "La classe da applicare",
                                    'accepted_params' => array(),
                                    'type' => 'string'
                                )
                            ),
                            'optionals' => array()
                        )
                    ),
                    'ms-menu-element' => array(
                        'name' => 'Tag elementi menù',
                        'description' => 'Permette di selezionare il tipo di tag da usare negli elementi del menù',
                        'required' => false,
                        'parameters' => array(
                            'required' => array(
                                'type' => array(
                                    'description' => "La tipologia del campo",
                                    'accepted_params' => array('main_element', 'sub_element'),
                                    'type' => 'string'
                                )
                            ),
                            'optionals' => array(
                                'value' => array(
                                    'description' => "Il tipo di elemento da usare",
                                    'accepted_params' => array('ul', 'li', 'div', 'p'),
                                    'type' => 'string'
                                )
                            )
                        )
                    ),
                ),
                'exec' => function($parameters, $settings) {

                    $menu_classes = array();

                    if($settings['ms-menu-class']) {
                        foreach ($settings['ms-menu-class'] as $form_setting) {
                            $menu_classes[$form_setting['type']] = $form_setting['class'];
                        }
                    }

                    if($settings['ms-menu-element']) {
                        foreach ($settings['ms-menu-element'] as $k => $form_setting) {
                            if(!stristr($form_setting['type'], 'arrow_')) $menu_classes[$form_setting['type']] = '<' . $form_setting['value'] . '>';
                            else $menu_classes[$form_setting['type']] =  $form_setting['value'];
                        }
                    }

                    return (new \MSFramework\menu($parameters['id']))->composeHTMLStructure('', $menu_classes);
                }
            ),
            'ms-form' => array(
                'name' => 'Form',
                'description' => 'Genera un form dinamico',
                'module_relation' => 'contenuti_form',
                'parameters' => array(
                    'required' => array(
                        'id' => array(
                            'description' => "L'ID del form generato",
                            'accepted_params' => array(),
                            'type' => 'int'
                        )
                    ),
                    'optionals' => array(
                        'hide-label' => array(
                            'description' => "Nasconde i label degli input",
                            'accepted_params' => array('0', '1'),
                            'type' => 'int'
                        ))
                ),
                'childrens' => array(
                    'ms-field-class' => array(
                        'name' => 'Imposta classe',
                        'description' => 'Imposta la classe da mostrare in base alla tipologia del campo',
                        'required' => false,
                        'parameters' => array(
                            'required' => array(
                                'type' => array(
                                    'description' => "La tipologia del campo",
                                    'accepted_params' => array('input_error', 'input', 'radio', 'checkbox', 'textarea', 'select', 'button'),
                                    'type' => 'string'
                                ),
                                'class' => array(
                                    'description' => "La classe da applicare all'elemento",
                                    'accepted_params' => array(),
                                    'type' => 'string'
                                )
                            )
                        )
                    ),
                    'ms-field-value' => array(
                        'name' => 'Imposta un valore predefinito',
                        'description' => 'Imposta un valore predefinito ad un input e lo nasconde dal form.',
                        'required' => false,
                        'parameters' => array(
                            'required' => array(
                                'id' => array(
                                    'description' => "L'ID del campo",
                                    'type' => 'string'
                                )
                            ),
                            'optionals' => array(
                                'value' => array(
                                    'description' => "Il valore da assegnare al campo",
                                    'accepted_params' => array(),
                                    'type' => 'string'
                                )
                            )
                        )
                    )
                ),
                'exec' => function($parameters, $settings) {

                    $form_input_classes = array();
                    $form_input_values = array();

                    if($settings['ms-field-class']) {
                        foreach ($settings['ms-field-class'] as $form_setting) {
                            $form_input_classes[$form_setting['type']] = $form_setting['class'];
                        }
                    }

                    if($settings['ms-field-value']) {
                        foreach ($settings['ms-field-value'] as $form_values) {
                            $form_input_values[$form_values['id']] = $form_values['value'];
                        }
                    }

                    $show_label = (!isset($parameters['hide-label']) || $parameters['hide-label'] != "1");

                    foreach(array(
                                'input' => 'ms-input',
                                'checkbox' => 'ms-checkbox',
                                'radio' => 'ms-radio',
                                'textarea' => 'ms-textarea',
                                'select' => 'ms-select',
                                'button' => 'ms-form-button',
                            ) as $def_class_k => $def_class_v) {
                        if(!isset($form_input_classes[$def_class_k])) $form_input_classes[$def_class_k] = '';
                        $form_input_classes[$def_class_k] .= ' ' . $def_class_v;
                        $form_input_classes[$def_class_k] = trim($form_input_classes[$def_class_k]);
                    }

                    return (new \MSFramework\forms())->composeHTMLForm($parameters['id'],  array(
                        'templates' => array(
                            'form' => '<div class="ms-row">{form}</div>',
                            'input' => '<div class="ms-form-col {col-size} {classes}">' . ($show_label ? '<label>{label}</label>' : '') . '{input}</div>',
                            'select' => '<div class="ms-form-col {col-size} {classes}">' . ($show_label ? '<label>{label}</label>' : '') . '{input}</div>',
                            'checkbox' => '<div class="ms-form-col {col-size} {classes}"><label>{input} {label}</label></div>',
                            'textarea' => '<div class="ms-form-col {col-size} {classes}">' . ($show_label ? '<label>{label}</label>' : '') . '{input}</div>',
                            'button' => '<div class="ms-form-col ms-col-12">{button}</div>'
                        ),
                        'classes' => $form_input_classes
                    ),
                        $form_input_values
                    );
                }
            ),
            'ms-gallery' => array(
                'name' => 'Galleria',
                'description' => 'Mostra una galleria',
                'module_relation' => 'gallery_gallery',
                'dependencies' => array(),
                'cycle' => array(
                    'enabled' => array(
                        'show-mode' => ''
                    ),
                    'returned_params' => array(
                        'image-url' => "L'URL dell'immagine",
                        'image-thumb' => "L'URL della miniatura dell'immagine",
                        'image-name' => "Eventuale nome dato all'immagine",
                        'image-descr' => "Eventuale descrizione data all'immagine"
                    ),
                    'example' => array(
                        '<a href="{ms-image-url}" title="{ms-image-name}"><img src="{ms-image-thumb}"></a>'
                    )
                ),
                'parameters' => array(
                    'required' => array(),
                    'optionals' => array(
                        'id' => array(
                            'description' => "L'ID della galleria da mostrare",
                            'accepted_params' => array(),
                            'type' => 'int'
                        ),
                        'images' => array(
                            'description' => "Lista delle immagini (separate da una virgola) da mostrare (Opzionale se passato un ID)",
                            'accepted_params' => array(),
                            'type' => 'string'
                        ),
                        'show-mode' => array(
                            'description' => "Il metodo di visualizzazione",
                            'accepted_params' => array('carousel', 'grid', 'thumbnails'),
                            'type' => 'string'
                        ),
                        'limit' => array(
                            'description' => "Il limite di immagini da mostrare",
                            'accepted_params' => array(),
                            'type' => 'integer'
                        ),
                        'columns' => array(
                            'description' => "Le immagini da mostrare per riga",
                            'accepted_params' => array("1", "2", "3", "4", "5"),
                            'type' => 'integer'
                        ),
                        'md-columns' => array(
                            'description' => "Le immagini da mostrare per riga (Tablet)",
                            'accepted_params' => array("1", "2", "3", "4", "5"),
                            'type' => 'integer'
                        ),
                        'sm-columns' => array(
                            'description' => "Le immagini da mostrare per riga (Mobile)",
                            'accepted_params' => array("1", "2", "3", "4", "5"),
                            'type' => 'integer'
                        ),
                        'autoplay' => array(
                            'description' => "[Carosello] Autoplay",
                            'accepted_params' => array("0", "1"),
                            'type' => 'integer'
                        ),
                        'dots' => array(
                            'description' => "[Carosello] Mostra o nascondi i dots",
                            'accepted_params' => array("0", "1"),
                            'type' => 'integer'
                        )
                    )
                ),
                'exec' => function($parameters, $settings) {
                    Global $MSFrameworki18n, $MSFrameworkParser;

                    $html_to_return = array();

                    $images_list = array();
                    if($parameters['id']) {
                        foreach((new \MSFramework\gallery())->getGalleryDetails($parameters['id'])[$parameters['id']]['gallery_friendly'] as $img) {
                            $images_list[] = array(
                                'main' => $img['html']['main'],
                                'thumb' => $img['html']['thumb'],
                                'titolo' => $img['html']['titolo'],
                                'testo' =>  $img['html']['testo']
                            );
                        }
                    } else if($parameters['images']) {
                        foreach(explode(',', $parameters['images']) as $img) {
                            $images_list[] = array(
                                'main' => $img,
                                'thumb' => $img,
                                'titolo' => '',
                                'testo' =>  ''
                            );
                        }
                    }

                    if(isset($parameters['limit']) && (int)$parameters['limit'] > 0) {
                        $images_list = array_slice($images_list, 0, (int)$parameters['limit']);
                    }

                    if($parameters['thumbnails']) {
                        $main_images_html = array();
                        $main_images_html[] = '<div class="ms-gallery-main-images">';
                        $main_images_html[] = '<a class="ms-gallery-image" href="' . $images_list[0]['main'] . '" title="' . htmlentities((!empty($MSFrameworki18n->getFieldValue($images_list[0]['titolo'])) ? $MSFrameworki18n->getFieldValue($images_list[0]['titolo']) : '')) . '">';
                        $main_images_html[] = '<img src="' . $images_list[0]['main'] . '">';
                        $main_images_html[] = '</a>';
                        $main_images_html[] = '</div>';
                    }

                    if($parameters['thumbnails'] === 'bottom') {
                        $html_to_return[] = implode('', $main_images_html);
                    }

                    if($parameters['thumbnails']) $html_to_return[] = '<div class="ms-gallery-thumbnails">';

                    $cycle_html = (!empty($settings['ms-cycle']) ? $settings['ms-cycle'] : '');
                    if(!isset($parameters['show-mode']) || $parameters['show-mode'] === "") {
                        foreach($images_list as $img) {
                            $html_to_return[] = str_replace(
                                array(
                                    '{ms-image-url}',
                                    '{ms-image-thumb}',
                                    '{ms-image-name}',
                                    '{ms-image-descr}'
                                ),
                                array(
                                    $img['main'],
                                    $img['thumb'],
                                    $MSFrameworki18n->getFieldValue($img['titolo']),
                                    $MSFrameworki18n->getFieldValue($img['testo']),
                                ),
                                $cycle_html
                            );
                        }
                    } else if($parameters['show-mode'] === 'carousel') {
                        $MSFrameworkParser->loadLibrary('swipebox');
                        $MSFrameworkParser->loadLibrary('owl-slider');

                        $carousel_params = array(
                            'sm-cols' => ($parameters['sm-columns'] ? (int)$parameters['sm-columns'] : 3),
                            'md-cols' => ($parameters['md-columns'] ? (int)$parameters['md-columns'] : 3),
                            'cols' => ($parameters['columns'] ? (int)$parameters['columns'] : 3),
                            'autoplay' => ($parameters['autoplay'] == "1" ? 1 : 0),
                            'dots' => ($parameters['dots'] == "1" ? 1 : 0)
                        );

                        $html_to_return[] = '<div class="ms-gallery" data-type="carousel">';
                        $html_to_return[] = '<div class="ms-gallery-carousel ms-carousel owl-carousel" data-carousel-settings=\'' . htmlentities(json_encode($carousel_params)) . '\'>';
                        foreach($images_list as $k => $img) {
                            $html_to_return[] = '<a class="ms-gallery-image" href="' . $img['main'] . '" title="' . htmlentities((!empty($MSFrameworki18n->getFieldValue($img['titolo'])) ? $MSFrameworki18n->getFieldValue($img['titolo']) : '')) . '">';
                            $html_to_return[] = '<img src="' . $img['thumb'] . '">';
                            $html_to_return[] = '</a>';
                        }
                        $html_to_return[] = '</div>';
                        $html_to_return[] = '</div>';

                    } else if(in_array($parameters['show-mode'], array('grid'))) {
                        $MSFrameworkParser->loadLibrary('swipebox');

                        $columns_classes = array_filter(array($parameters['columns']) + array($parameters['sm-columns']) + array($parameters['md-columns']));

                        $html_to_return[] = '<div class="ms-gallery" data-type="' . $parameters['show-mode'] . '">';
                        $html_to_return[] = '<div class="row">';
                        foreach($images_list as $k => $img) {
                            $html_to_return[] = '<div class="' . implode(' ' , $columns_classes) . '">';
                            $html_to_return[] = '<a class="ms-gallery-image" href="' . $img['main'] . '" title="' . htmlentities((!empty($MSFrameworki18n->getFieldValue($img['titolo'])) ? $MSFrameworki18n->getFieldValue($img['titolo']) : '')) . '">';
                            $html_to_return[] = '<img src="' . $img['thumb'] . '">';
                            $html_to_return[] = '</a>';
                            $html_to_return[] = '</div>';
                        }
                        $html_to_return[] = '</div>';
                        $html_to_return[] = '</div>';
                    }

                    if($parameters['thumbnails']) $html_to_return[] = '</div>';

                    if($parameters['thumbnails'] === 'top') {
                        $html_to_return[] = implode('', $main_images_html);
                    }

                    if($parameters['thumbnails']) {
                        $MSFrameworkParser->addScript("
                        $('.ms-gallery-main-images').on('click', '.ms-gallery-image', function(e) {
                            e.preventDefault();
                            if($(this).closest('.ms-gallery').find('.ms-gallery-thumbnails .ms-gallery-image.active').length) {
                                $(this).closest('.ms-gallery').find('.ms-gallery-thumbnails .ms-gallery-image.active').click();
                            } else {
                                $(this).closest('.ms-gallery').find('.ms-gallery-thumbnails .ms-gallery-image').first().click();
                            }
                        });
                        
                        var MSGallerySlideChange = function(index) {
                            $('.ms-gallery-thumbnails .active').removeClass('active');
                            var elem = $('.ms-gallery-thumbnails .ms-gallery-image').eq((index-1));
                            var href = elem.attr('href');
                            elem.addClass('active');
                            $('.ms-shortcode-element.ms-gallery .ms-gallery-main-images .ms-gallery-image').attr('href', href).find('img').attr('src', href);
                        };
                        
                        $('.ms-gallery .ms-gallery-thumbnails .ms-gallery-image').swipebox({
                            nextSlide: MSGallerySlideChange,
                            prevSlide: MSGallerySlideChange,
                            afterOpen: MSGallerySlideChange,
                            hideBarsDelay: 0
                        });");
                    } else {
                        $MSFrameworkParser->addScript("$('.ms-gallery .ms-gallery-image').swipebox({
                            hideBarsDelay: 0
                        });");
                    }

                    return implode('', $html_to_return);
                }
            ),
            'ms-slider' => array(
                'name' => 'Slider',
                'description' => 'Mostra uno slider',
                'module_relation' => 'gallery_sliders',
                'dependencies' => array(),
                'cycle' => array(
                    'enabled' => true,
                    'returned_params' => array(
                        'slide-title' => "Il titolo della slide",
                        'slide-subtitle' => "Il sottotitolo della slide",
                        'slide-heading' => "L'intestazione della slide",
                        'slide-overlay-img' => "L'URL dell'immagine",
                        'slide-btn-text' => "Il testo del pulsante",
                        'slide-btn-link' => "Il link del pulsante",
                    ),
                    'example' => array(
                        '<div class="slide-content"><h2>{ms-slide-heading}</h2><h1>{ms-slide-title}</h1><p>{ms-slide-subtitle}</p><img src="{ms-slide-overlay-img}"><a href="{ms-slide-btn-link}" title="{ms-slide-btn-text}">{ms-slide-btn-text}></a></div>'
                    )
                ),
                'parameters' => array(
                    'optionals' => array(
                        'id' => array(
                            'description' => "L'ID dello slider da mostrare (Se non passato ritornerà lo slider della home)",
                            'accepted_params' => array(),
                            'type' => 'int'
                        )
                    )
                ),
                'exec' => function($parameters, $settings) {
                    Global $MSFrameworki18n, $MSFrameworkParser;

                    $html_to_return = array();

                    $ids = array();
                    if($parameters['id']) {
                        $ids = explode(',', $parameters['id']);
                    }

                    $sliderDetails = (new \MSFramework\sliders())->getSliderDetails($ids, ($ids ? false : true), '*', true);

                    $cycle_html = (!empty($settings['ms-cycle']) ? $settings['ms-cycle'] : '');
                    if($sliderDetails) {

                        $random_slider_id = uniqid('ms-slider_');

                        $html_to_return[] = '<div id="' . $random_slider_id . '" class="owl-carousel carousel slide">';

                        $MSFrameworkParser->loadLibrary('owl-slider');

                        foreach($sliderDetails as $slider) {

                            $slide_css = '';
                            $slide_class = array();
                            if(!empty($slider['background_color'])) $slide_css .= 'background-color: ' . $slider['background_color'] . ';';
                            if(!empty($slider['slider_friendly'][0])) $slide_css .= 'background: url(\'' . $slider['slider_friendly'][0]['html']['main'] . '\');';
                            if(!empty($slider['bg_type'])) $slide_class[] = $slider['bg_type'];

                            if(!empty($slider['bg_type'])) {
                                echo '<div class="slide-overlay ' .  $slider['bg_type'] . '"></div>';
                            }

                            $html_to_return[] = '<div class="ms-slider-slide ' . implode(' ', $slide_class) . '" style="' . $slide_css . '">';

                            if(!empty($slider['bg_type'])) echo '<div class="ms-slide-overlay ' .  $slider['bg_type'] . '"></div>';

                            $html_to_return[] = str_replace(
                                array(
                                    '{ms-slide-title}',
                                    '{ms-slide-subtitle}',
                                    '{ms-slide-heading}',
                                    '{ms-slide-overlay-img}',
                                    '{ms-slide-btn-text}',
                                    '{ms-slide-btn-link}',
                                ),
                                array(
                                    $MSFrameworki18n->getFieldValue($slider['titolo']),
                                    $MSFrameworki18n->getFieldValue($slider['sottotitolo']),
                                    $MSFrameworki18n->getFieldValue($slider['intestazione']),
                                    '',
                                    $MSFrameworki18n->getFieldValue($slider['btn_text']),
                                    $MSFrameworki18n->getFieldValue($slider['btn_href']),
                                ),
                                $cycle_html
                            );

                            $html_to_return[] = '</div>';
                        }

                        $MSFrameworkParser->addScript("$('#" . $random_slider_id . "').owlCarousel({
                                animateOut: 'fadeOut',
                                loop: true,
                                autoplay: ($('#" . $random_slider_id . "').length > 1 ? 1000 : false),
                                autoplayTimeout: 6000,
                                autoplaySpeed: 1000,
                                items: 1,
                                autoplayHoverPause: false,
                                dots: true,
                                nav: false,
                                singleItem: true,
                                mouseDrag: true,
                                touchDrag: true,
                                onInitialized: function (event) {
                                   $('#" . $random_slider_id . "').removeClass('loading');
                                }
                            });");

                        $html_to_return[] = '</div>';
                    }

                    return implode('', $html_to_return);
                }
            ),
            'ms-faq' => array(
                'name' => 'FAQ',
                'description' => 'Mostra una lista di FAQ',
                'module_relation' => 'contenuti_faq',
                'dependencies' => array(),
                'cycle' => array(
                    'enabled' => array(
                        'show-mode' => ''
                    ),
                    'returned_params' => array(
                        'faq-question' => "La domanda della FAQ",
                        'faq-answer' => "La risposta alla domanda",
                        'faq-icon' => "Eventuale icona collegata alla questione"
                    ),
                    'example' => array(
                        '<div class="faq-content">
                            <h2>{ms-faq-question}</h2>
                            <p>{ms-faq-answer}</p>
                            <img src="{ms-faq-icon}">
                        </div>'
                    )
                ),
                'parameters' => array(
                    'required' => array(
                        'id' => array(
                            'description' => "L'ID della galleria da mostrare",
                            'accepted_params' => array(),
                            'type' => 'int'
                        )
                    ),
                    'optionals' => array(
                        'show-mode' => array(
                            'description' => "Il metodo di visualizzazione",
                            'accepted_params' => array('accordion', 'standard'),
                            'type' => 'string'
                        )
                    )
                ),
                'exec' => function($parameters, $settings) {
                    Global $MSFrameworki18n, $MSFrameworkParser;

                    $html_to_return = array();

                    $faqDetails = (new \MSFramework\faq())->getFAQDetails($parameters['id'])[$parameters['id']];

                    if(isset($parameters['limit']) && (int)$parameters['limit'] > 0) {
                        $faqDetails['gallery_friendly'] = array_slice($faqDetails['gallery_friendly'], 0, (int)$parameters['limit']);
                    }

                    $cycle_html = (!empty($settings['ms-cycle']) ? $settings['ms-cycle'] : '');
                    if(!isset($parameters['show-mode']) || $parameters['show-mode'] === "") {
                        foreach($faqDetails['questions'] as $q) {
                            $html_to_return[] = str_replace(
                                array(
                                    '{ms-faq-question}',
                                    '{ms-faq-answer}',
                                    '{ms-faq-icon}'
                                ),
                                array(
                                    $MSFrameworki18n->getFieldValue($q[0]),
                                    $MSFrameworki18n->getFieldValue($q[1]),
                                    UPLOAD_FAQ_FOR_DOMAIN_HTML . $q[2][0]
                                ),
                                $cycle_html
                            );
                        }

                    } else if($parameters['show-mode'] === 'standard') {

                        $html_to_return[] = '<div class="ms-standard-faq-content">';
                        foreach($faqDetails['questions'] as $q) {
                            $html_to_return[] = '<div class="ms-standard-faq-inside">';
                            $html_to_return[] = '<h2 class="ms-faq-question">' . $MSFrameworki18n->getFieldValue($q[0]) . '</h2>';
                            $html_to_return[] = '<p class="ms-faq-answer">' . $MSFrameworki18n->getFieldValue($q[1]) . '</p>';
                            $html_to_return[] = '</div>';
                        }
                        $html_to_return[] = '</div>';

                    } else if($parameters['show-mode'] === 'accordion') {

                        $random_accordion_id = uniqid('accordion_');
                        $html_to_return[] = '<div class="ms-accordion-faq-content" id="' . $random_accordion_id . '">';
                        foreach($faqDetails['questions'] as $q) {
                            $html_to_return[] = '<span class="ms-faq-question">' . $MSFrameworki18n->getFieldValue($q[0]) . '</span>';
                            $html_to_return[] = '<div class="ms-faq-answer">' . $MSFrameworki18n->getFieldValue($q[1]) . '</div>';
                        }
                        $html_to_return[] = '</div>';

                        $MSFrameworkParser->loadLibrary('jquery-ui');
                        $MSFrameworkParser->addScript('$("#' . $random_accordion_id . '" ).accordion({collapsible: true, active: false}).on( "accordionbeforeactivate", function( event, ui ) {$(this).accordion( "refresh" );} );');

                    }

                    return implode('', $html_to_return);
                }
            )
        );

        return $ms_tags;
    }
}
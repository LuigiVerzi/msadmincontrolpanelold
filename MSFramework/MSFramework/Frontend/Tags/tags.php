<?php
/**
 * MSAdminControlPanel
 * Date: 04/08/2019
 */

namespace MSFramework\Frontend\Tags;

class tags
{

    public $tags_categories = array(
        'Main' => array(
            'name' => 'Generali',
            'description' => ''
        ),
        'Pages' => array(
            'name' => 'Pagine',
            'description' => ''
        ),
        'Profile' => array(
            'name' => 'Area Riservata',
            'description' => ''
        ),
        'Ecommerce' => array(
            'name' => 'Ecommerce',
            'description' => ''
        ),
        'Hotels' => array(
            'name' => 'Hotel',
            'description' => ''
        )
    );

    public function __construct($currentPageDetails = array())
    {
        global $firephp, $MSFrameworkDatabase, $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;

        $this->currentPageDetails = $currentPageDetails;

    }

    /**
     * @param $show_hierarchy bool Indica se ritornare i tag divisi per categoria
     * @return array Ritorna la lista dei tag MS
     */
    public function getMSTags($show_hierarchy = false)
    {
        $ms_tags = array();
        foreach($this->tags_categories as $category_name => $category_info) {
            $className = "MSFramework\\Frontend\\Tags\\$category_name\\tags";

            $tagList = (new $className())->getMSTags($this->currentPageDetails);

            foreach($tagList as $tag_name => $tag_values) {
                $tagList[$tag_name]['id'] = $tag_name;
                $tagList[$tag_name]['is_parent'] = true;
            }

            if($show_hierarchy) {
                $ms_tags[$category_name] = $tagList;
            } else {
                $ms_tags = array_merge($ms_tags, $tagList);
            }
        }

        return $ms_tags;
    }

    public function generateSingleTagDocumentation($tags) {
        $html_to_return = array();

        if(isset($tags['name'])) {
            $tags = array($tags['id'] => $tags);
        }

        $table_classes = "table table-bordered";

        $element_types = array(
            'int' => 'Intero',
            'string' => 'Stringa'
        );

        foreach($tags as $tag_id => $tag) {

            $html_to_return[] = '<tr>';
            $html_to_return[] = '<th>Elemento:</th>';
            $html_to_return[] = '<td><span class="label label-primary">' . $tag_id . '</span></td>';
            $html_to_return[] = '</tr>';

            $html_to_return[] = '<tr>';
            $html_to_return[] = '<th>Descrizione:</th>';
            $html_to_return[] = '<td>' . $tag['description'] . '</td>';
            $html_to_return[] = '</tr>';


            if($tag['parameters']['required'] || $tag['parameters']['optionals']) {

                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Parametri:</th>';
                $html_to_return[] = '<td>';

                $html_to_return[] = '<table class="' . $table_classes . '">';

                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Nome</th>';
                $html_to_return[] = '<th>Descrizione</th>';
                $html_to_return[] = '<th>Valori accettati</th>';
                $html_to_return[] = '</tr>';

                foreach($tag['parameters'] as $type => $parameters) {
                    foreach ($parameters as $param_id => $param_values) {
                        $html_to_return[] = '<tr>';

                        $html_to_return[] = '<td>';
                        $html_to_return[] = $param_id . ' <small class="label label-' . ($type === 'required' ? 'danger' : 'grey') . ' pull-right">' . ($type === 'required' ? 'Richiesto' : 'Opzionale') . '</small>';
                        $html_to_return[] = '</td>';

                        $html_to_return[] = '<td>';
                        $html_to_return[] = '<small>' . $param_values['description'] . '</small>';
                        $html_to_return[] = '</td>';

                        $html_to_return[] = '<td>';
                        if($param_values['accepted_params']) {
                            $html_to_return[] = '<small class="label label-inverse">' . implode('</small> <small class="label label-inverse">', $param_values['accepted_params']) . '</small>';
                        } else {
                            $html_to_return[] = '<small><i class="text-info">' . $element_types[$param_values['type']] . '</i></small>';
                        }
                        $html_to_return[] = '</td>';

                        $html_to_return[] = '</tr>';
                    }
                }

                $html_to_return[] = '</table>';

                $html_to_return[] = '</td>';
                $html_to_return[] = '</tr>';
            }

            if($tag['childrens']) {

                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Elementi figli:</th>';
                $html_to_return[] = '<td>';

                $html_to_return[] = '<table class="' . $table_classes . '">';
                $html_to_return[] = $this->generateSingleTagDocumentation($tag['childrens']);
                $html_to_return[] = '</table>';

                $html_to_return[] = '</td>';
                $html_to_return[] = '</tr>';

            }

            if(isset($tag['cycle'])) {

                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Ciclo dei valori:</th>';
                $html_to_return[] = '<td>';

                $html_to_return[] = '<table class="table table-bordered">';

                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Elemento:</th>';
                $html_to_return[] = '<td><span class="label label-info">' . htmlentities('<ms-cycle>') . '</span></td>';
                $html_to_return[] = '</tr>';

                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Descrizione:</th>';
                $html_to_return[] = '<td>';

                $html_to_return[] = '<p>';
                if(is_array($tag['cycle']['enabled'])) {

                    if(array_values($tag['cycle']['enabled'])[0] === '') {
                        $html_to_return[] = 'Ignorando il parametro <small class="label label-grey">' . array_keys($tag['cycle']['enabled'])[0] . '</small>';
                    } else {
                        $html_to_return[] = 'Se impostato il valore <u>' . array_values($tag['cycle']['enabled'])[0] . '</u> al parametro <small class="label label-grey">' . array_keys($tag['cycle']['enabled'])[0] . '</small>';
                    }

                    $html_to_return[] = ' sarà possibile';

                } else {
                    $html_to_return[] = ' è possibile';
                }
                $html_to_return[] = ' effettuare un ciclo dei valori ottenuti tramite la funzione <span class="label label-inverse">ms-cycle</span>.';

                $html_to_return[] = '</p>';

                $html_to_return[] = 'Tutto il contenuto di <b>' . htmlentities('<ms-cycle>') . '</b> <u>verrà ripetuto</u> per ogni valore.';

                $html_to_return[] = '</td>';
                $html_to_return[] = '</tr>';

                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Esempio:</th>';
                $html_to_return[] = '<td>';

                $html_to_return[] = '<textarea class="frontendShortcodeMirror" disabled>' . $this->generateCycleExample($tag['cycle']['example'], $tag) . '</textarea>';

                $html_to_return[] = '</td>';
                $html_to_return[] = '</tr>';

                $html_to_return[] = '</td>';
                $html_to_return[] = '</tr>';

                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Parametri ritornati:</th>';
                $html_to_return[] = '<td>';

                /* INIZIO TABELLA DOCUMENTAZIONE CICLO */
                $html_to_return[] = '<table class="table table-bordered">';
                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Tag</th>';
                $html_to_return[] = '<th>Descrizione</th>';
                $html_to_return[] = '</tr>';

                foreach($tag['cycle']['returned_params'] as $returned_tag => $description) {
                    $html_to_return[] = '<tr>';

                    $html_to_return[] = '<td>';
                    $html_to_return[] = '<small class="text-info">{ms-' . $returned_tag . '}</small>';
                    $html_to_return[] = '</td>';

                    $html_to_return[] = '<td>';
                    $html_to_return[] = $description;
                    $html_to_return[] = '</td>';

                    $html_to_return[] = '</tr>';
                }

                $html_to_return[] = '</table>';
                /* FINE TABELLA DOCUMENTAZIONE CICLO */

                $html_to_return[] = '</td>';
                $html_to_return[] = '</tr>';

                $html_to_return[] = '</table>';

                $html_to_return[] = '</td>';
                $html_to_return[] = '</tr>';

            }

            if($tag['has_returns']) {

                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Parametri ritornati:</th>';
                $html_to_return[] = '<td>';

                /* INIZIO TABELLA DOCUMENTAZIONE CICLO */
                $html_to_return[] = '<table class="table table-bordered">';
                $html_to_return[] = '<tr>';
                $html_to_return[] = '<th>Tag</th>';
                $html_to_return[] = '<th>Descrizione</th>';
                $html_to_return[] = '</tr>';

                foreach($tag['has_returns']['returned_params'] as $returned_tag => $description) {
                    $html_to_return[] = '<tr>';

                    $html_to_return[] = '<td>';
                    $html_to_return[] = '<small class="text-info">{ms-' . $returned_tag . '}</small>';
                    $html_to_return[] = '</td>';

                    $html_to_return[] = '<td>';
                    $html_to_return[] = $description;
                    $html_to_return[] = '</td>';

                    $html_to_return[] = '</tr>';
                }

                $html_to_return[] = '</table>';
                /* FINE TABELLA DOCUMENTAZIONE CICLO */

                $html_to_return[] = '</td>';
                $html_to_return[] = '</tr>';

            }

        }

        return implode('', $html_to_return);

    }

    public function generateSingleTagExample($tags) {
        $html_to_return = array();
        if(isset($tags['name'])) {
            $tags = array($tags['id'] => $tags);
        }

        foreach($tags as $tag_id => $tag) {

            $parameters_to_append = array();
            if(array_keys($tag['parameters']['required'])) {
                foreach($tag['parameters']['required'] as $param_id => $param_values) {
                    $parameters_to_append[] = $param_id . '="' . ($param_values['accepted_params'] ? implode('|', $param_values['accepted_params']) : '') . '"';
                }
            }

            $optionals_to_append = array();
            if(array_keys($tag['parameters']['optionals'])) {
                foreach($tag['parameters']['optionals'] as $param_id => $param_values) {
                    $optionals_to_append[] = $param_id . '="' . ($param_values['accepted_params'] ? implode('|', $param_values['accepted_params']) : '') . '"';
                }
            }

            if(!isset($tag['exec'])) {
                $html_to_return[] = '<!-- [' . (isset($tag['required']) && $tag['required'] ? 'Obbligatorio' : 'Opzionale') . '] ' . $tag['description'] . ' -->';
            }

            if($tag['childrens'] || $tag['has_returns']) {
                $html_to_return[] = '<' . $tag_id . ($parameters_to_append ? ' ' . implode(' ', $parameters_to_append) : '') . '>';
                $html_to_return[] = $this->generateSingleTagExample($tag['childrens']);

                if($tag['has_returns']) {
                    $html_to_return[] = $tag['has_returns']['example'];
                }

                $html_to_return[] = '</' . $tag_id . '>';
            } else {
                $html_to_return[] = '<' . $tag_id . ($parameters_to_append ? ' ' . implode(' ', $parameters_to_append) : '') . (isset($tag['is_parent']) ? '></' . $tag_id . '>' : '/>');
            }

        }

        return implode(PHP_EOL, $html_to_return);

    }

    public function generateCycleExample($tags, $tagDetail) {
        $html_to_return = array();

        $parameters_to_append = array();
        if(array_keys($tagDetail['parameters']['required'])) {
            foreach($tagDetail['parameters']['required'] as $param_id => $param_values) {
                $parameters_to_append[] = $param_id . '="' . ($param_values['accepted_params'] ? implode('|', $param_values['accepted_params']) : '') . '"';
            }
        }

        $html_to_return[] = '<' . $tagDetail['id'] . ($parameters_to_append ? ' ' . implode(' ', $parameters_to_append) : '') . '>';

        $html_to_return[] = '<ms-cycle>';

        foreach($tags as $line) {
            $html_to_return[] = $line;
        }

        $html_to_return[] = '</ms-cycle>';

        $html_to_return[] = '</' . $tagDetail['id'] . '>';

        return implode(PHP_EOL, $html_to_return);

    }
}
<?php
/**
 * MSFramework
 * Date: 26/05/18
 */

namespace MSFramework\Frontend\Modules;

class frontendParser
{
    public function __construct()
    {
        global $MSFrameworkDatabase, $MSFrameworki18n, $MSFrameworkCMS;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworki18n = $MSFrameworki18n;
        $this->MSFrameworkCMS = $MSFrameworkCMS;

    }

    /**
     * Ottiene la lista dei file da includere nel Front
     *
     * @return array
     */
    public function getFilesToInclude()
    {
        $files_to_include = array(
            'js' =>  array(FRAMEWORK_COMMON_CDN . 'modules/frontendParser/js/frontendHelper.js'),
            'css' =>  array(FRAMEWORK_COMMON_CDN . 'modules/frontendParser/css/frontendHelper.css'),
        );

        return $files_to_include;
    }

    /**
     * Gestisce le chiamate Ajax della funzione
     */
    public function ajaxCall($params, $slug = array())
    {
        global $firephp, $MSFrameworki18n, $MSFrameworkParser, $MSFrameworkSaaSBase;

        if ($params['action'] == 'getElementsHTML') {
            $return_elements = array(
                'dependencies' => '',
                'elements' => array()
            );

            $parser = $MSFrameworkParser;
            foreach($params['elements'] as $elemClass => $html) {
                $parser->source = $html;
                $parser->parseHTML(false);
                $return_elements['elements'][$elemClass] = $parser->source;
            }

            /* Aggiungo gli script JS */
            $inline_js = array();
            if($parser->filesToAttach['inline_js']['instant']) $inline_js[] = implode(PHP_EOL, $parser->filesToAttach['inline_js']['instant']);
            if($parser->filesToAttach['inline_js']['onload']) $inline_js[] = '$(document).ready(function() {' . PHP_EOL . implode(PHP_EOL, $parser->filesToAttach['inline_js']['onload']) . PHP_EOL . '});';
            if($inline_js) $return_elements['dependencies'] .= '<script type="application/javascript">' . implode(PHP_EOL, $inline_js) . '</script>';
            foreach($parser->filesToAttach['js'] as $js) $return_elements['dependencies'] .= '<script type="application/javascript" src="' . $js . '"></script>';

            /* Aggiungo gli stili */
            $inline_css = array();
            if($parser->filesToAttach['inline_css']) $inline_css[] = implode(PHP_EOL, $parser->filesToAttach['inline_css']);
            foreach($parser->filesToAttach['css'] as $css) $return_elements['dependencies'] .= ' <link href="' . $css . '" rel="stylesheet">';
            if($inline_css) $return_elements['dependencies'] .= '<style type="text/css">' . implode(PHP_EOL, $inline_css) . '</style>';


            return json_encode($return_elements);
        }

    }

    /**
     * Vedi DOC della funzione \MSFramework\modules()->getModuleSettings
     *
     * @return array
     */
    public function getSettings()
    {
        $array_settings = array();
        return $array_settings;
    }
}
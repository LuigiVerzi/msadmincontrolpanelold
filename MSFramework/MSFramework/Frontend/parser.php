<?php
/**
 * MSAdminControlPanel
 * Date: 04/08/2019
 */

namespace MSFramework\Frontend;

class parser
{

    public $source = '';
    public $mode = 'full';

    private $template_path = '';
    public $currentPageDetails = array();

    /**
     * @var array La lista di librerie.
     */
    private $assetsLibraries = array(
        'swipebox' => array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'vendor/swipebox/js/jquery.swipebox.min.js'),
            'css' => array(FRAMEWORK_COMMON_CDN . 'vendor/swipebox/css/swipebox.min.css')
        ),
        'jquery' => array(
            'js' => array(
                FRAMEWORK_COMMON_CDN . 'vendor/jquery/jquery.min.js'
            )
        ),
        'jquery-ui' => array(
            'js' => array(
                FRAMEWORK_COMMON_CDN . 'vendor/jquery/jquery-ui.min.js'
            )
        ),
        'owl-slider' => array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'vendor/owl-slider/owl.carousel.js'),
            'css' => array(FRAMEWORK_COMMON_CDN . 'vendor/owl-slider/assets/owl.carousel.min.css')
        ),
        'lightGallery' => array(
            'js' => array(
                FRAMEWORK_COMMON_CDN . 'vendor/lightBox/js/lightgallery.js',
                FRAMEWORK_COMMON_CDN . 'vendor/lightBox/js/lg-zoom.min.js',
                FRAMEWORK_COMMON_CDN . 'vendor/lightBox/js/lg-fullscreen.min.js'
            ),
            'css' => array(FRAMEWORK_COMMON_CDN . 'vendor/lightBox/css/lightgallery.css')
        ),
        'animate' => array(
            'js' => array(),
            'css' => array(FRAMEWORK_COMMON_CDN . 'vendor/animate/animate.css')
        ),
        'modalVideo' => array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'vendor/modalVideo/js/jquery-modal-video.min.js'),
            'css' => array(FRAMEWORK_COMMON_CDN . 'vendor/modalVideo/css/modal-video.min.css')
        ),
        'fontAwesome' => array(
            'css' => array(FRAMEWORK_COMMON_CDN . 'vendor/font-awesome/css/all.css')
        ),
        'countdown' => array(
            'js' => array(FRAMEWORK_COMMON_CDN . 'vendor/countdown/jquery.countdown.min.js')
        )
    );

    private $emptyFilesToAttach = array('css' => array(), 'js' => array(), 'inline_js' => array('instant' => array(), 'onload' => array()), 'html' => array());

    public $filesToAttach = array();

    private $replace_array = array();
    private $global_shortcodes = array();
    private $theme_shortcodes = array();

    public function __construct()
    {
        global $MSFrameworkDatabase, $MSFrameworkCMS, $MSFrameworkUrl, $MSFrameworkPages;

        $this->filesToAttach = $this->emptyFilesToAttach;

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
        $this->MSFrameworkUrl = $MSFrameworkUrl;
        $this->MSFrameworkPages = $MSFrameworkPages;
        $this->MSFrameworkTags = new Tags\tags();
    }

    /**
     * Imposto lo slug della pagina per far generare il sorgente da elaborare
     * @param array $slug Lo slug attuale
     * @return mixed La classe del parser
     */
    public function setSlug($slug = array()) {
        $this->source = $this->getCurrentPageSource($this->MSFrameworkUrl->getDetailsFromSlug($slug));
        return $this;
    }

    /**
     * Carica una libreria (vedi $assetsLibraries sopra)
     * @param string $library La libreria da caricare
     */
    public function loadLibrary($library) {

        if(isset($this->assetsLibraries[$library])) {
            if($this->assetsLibraries[$library]['js']) {
                $this->filesToAttach['js'] = array_merge($this->filesToAttach['js'], $this->assetsLibraries[$library]['js']);
            }
            if($this->assetsLibraries[$library]['css']) {
                $this->filesToAttach['css'] = array_merge($this->filesToAttach['css'], $this->assetsLibraries[$library]['css']);
            }
        }
    }

    /**
     * Aggiunge uno script JavaScript alla fine del file.
     * @param string $js Il codice JS da eseguire o l'URL del file
     * @param bool $onload Se impostato carica i file dentro $(document).ready
     */
    public function addScript($js, $onload = true) {
        if(filter_var($js, FILTER_VALIDATE_URL)) {
            if(!in_array($js, $this->filesToAttach['js'])) {
                $this->filesToAttach['js'][] = $js;
            }
        } else {
            if(!in_array($js, $this->filesToAttach['inline_js'][($onload ? 'onload' : 'instant')])) {
                $this->filesToAttach['inline_js'][($onload ? 'onload' : 'instant')][] = $js;
            }
        }
    }

    /**
     * Aggiunge un elemento HTML alla fine del file.
     * @param string $html Il codice HTML da includere alla fine del file
     */
    public function addHtml($html) {
        $this->filesToAttach['html'][] = $html;
    }

    /**
     * Aggiunge uno stile alla fine del file.
     * @param string $css L'URL del file o direttamente lo stile da aggiungere
     */
    public function addStyle($css) {
        $type = 'inline_css';
        if(filter_var($css, FILTER_VALIDATE_URL)) {
            $type = 'css';
        }
        if(!in_array($css, $this->filesToAttach[$type])) {
            $this->filesToAttach[$type][] = $css;
        }
    }

    /**
     * Stampa il contenuto della pagina dopo averlo formattato
     */
    public function parse() {
        Global $MSFrameworkVisualBuilder, $MSFrameworki18n;

        // Ottengo gli shortcodes globali
        $this->global_shortcodes = $this->MSFrameworkPages->getCommonShortcodes(true);

        // Ottengo gli shortcode delle impostazioni del tema
        $this->theme_shortcodes = (new \MSFramework\Appearance\themes())->getSettingsShortcodes();

        // Duplico gli shortcodes sostituendo le graffe con l'HTML Entity in modo da permettere la sostituzione all'interno degli attributi
        foreach($this->global_shortcodes as $shortcode => $s_v) $this->global_shortcodes[str_replace(array('{', '}'), array('%7B', '%7D'), $shortcode)] = $s_v;
        foreach($this->theme_shortcodes as $shortcode => $s_v) $this->theme_shortcodes[str_replace(array('{', '}'), array('%7B', '%7D'), $shortcode)] = $s_v;
        foreach($this->theme_shortcodes as $shortcode => $s_v) $this->theme_shortcodes[str_replace(array('>'), array('&gt;'), $shortcode)] = $s_v;

        if($this->mode === 'full') {
            $this->execPHP();
            $this->attachCommonParts();
        }

        $this->parseHTML();
        $this->source = $MSFrameworkVisualBuilder->draw($this->source);
        $this->source = $MSFrameworki18n->gettextFromShortcodes($this->source);

        echo $this->source;
    }

    /**
     * Ottiene il sorgente della pagina attuale in base allo slug
     * @param $slug_params array I dati passati da $_GET['slug']
     * @return string Il sorgente del template
     */
    private function getCurrentPageSource($slug_params) {
        Global $MSFrameworkVisualBuilder, $MSFrameworkParser, $MSFrameworki18n, $MSFrameworkPages;

        $this->template_path = $MSFrameworkPages->getModelPageURL('', '', 'templates/standard.php');

        if($slug_params['action'] === 'getPage') {

            $pageDetails = $MSFrameworkPages->getPageDetails($slug_params['id'])[$slug_params['id']];

            if ($pageDetails) {

                if(!empty($pageDetails['additional_css'])) $MSFrameworkParser->addStyle($pageDetails['additional_css']);

                $this->currentPageDetails = array(
                    'id' => $slug_params['id'],
                    'title' => $MSFrameworki18n->getFieldValue($pageDetails['titolo']),
                    'subtitle' => $MSFrameworki18n->getFieldValue($pageDetails['sottotitolo']),
                    'seo' => $pageDetails['seo'],
                    'type' => 'page',
                    'details' => $pageDetails
                );

                $this->currentPageDetails['content'] = $MSFrameworkVisualBuilder->draw($MSFrameworki18n->getFieldValue($pageDetails['content']));

                $this->template_path = $MSFrameworkPages->getModelPageURL($pageDetails['id'], $pageDetails['type']);
            }

        } else {

            /* Se ci troviamo sulla index imposto la pagina di sistema */
            if(!$slug_params && !isset($_GET['check_dynamic_url'])) {
                $slug_params['action'] = 'getHomePage';
                $this->template_path = $MSFrameworkPages->getModelPageURL('', '', 'index.php');
            }

            /* Cerco tra le pagine di sistema */
            foreach ($MSFrameworkPages->privatePages as $cat) {
                foreach ($cat['pages'] as $page_id => $page) {
                    if ( ($slug_params['action'] === "getPrivatePage" && $page_id === $slug_params['id']) || ($page['action'] && $page['action'] == $slug_params['action']) ) {

                        $privatePageData = $MSFrameworkPages->getPrivatePageData($page_id);

                        $this->currentPageDetails['title'] = $MSFrameworki18n->getFieldValue($privatePageData['titolo']);
                        $this->currentPageDetails['subtitle'] = $MSFrameworki18n->getFieldValue($privatePageData['sottotitolo']);
                        $this->currentPageDetails['type'] = $page_id . ' private-page';
                        $this->currentPageDetails['shortcodes'] = $page['shortcodes'];
                        if(!$page['have_seo']) {
                            $this->currentPageDetails['seo'] = $privatePageData['seo'];
                        }

                        if (is_callable($page['exec'])) {
                            $this->currentPageDetails['details'] = $page['exec']($slug_params['id']);
                            if(isset($this->currentPageDetails['details']['seo'])) {
                                $this->currentPageDetails['seo'] = $this->currentPageDetails['details']['seo'];
                            }
                        }

                        $this->currentPageDetails['content'] = $MSFrameworkVisualBuilder->draw($MSFrameworki18n->getFieldValue($privatePageData['content']));

                        if (!empty($privatePageData['additional_css'])) $MSFrameworkParser->addStyle($privatePageData['additional_css']);

                        if ($page['shortcodes']) {
                            foreach ($page['shortcodes'] as $short => $short_values) {
                                $currentShortCodeValue = $short_values['value']($this->currentPageDetails['details']);

                                $this->currentPageDetails['title'] = str_replace($short, $currentShortCodeValue, $this->currentPageDetails['title']);
                                $this->currentPageDetails['subtitle'] = str_replace($short, $currentShortCodeValue, $this->currentPageDetails['subtitle']);
                                $this->currentPageDetails['content'] = str_replace($short, $currentShortCodeValue, $this->currentPageDetails['content']);
                            }
                        }

                        if ($slug_params['action'] != 'getHomePage') {
                            $this->template_path = $MSFrameworkPages->getModelPageURL($privatePageData['id'], $privatePageData['type']);
                        }
                    }
                }
            }
        }

        /* Se non è stata trovata nessuna pagina mostro la 404 */
        if(!$this->currentPageDetails) {
            $this->template_path = $MSFrameworkPages->getModelPageURL('', '', '404.php');
            $this->currentPageDetails = array(
                'type' => 'page 404-page',
                'title' => '404',
                'subtitle' => '',
                'description' => '',
            );
        }

        if(isset($_GET['ms-page-content'])) {
            die($this->currentPageDetails['content'])
            ;        }

        $this->template_path = str_replace(ABSOLUTE_SW_PATH, ABSOLUTE_SW_PATH . 'www/', $this->template_path);

        if(!file_exists($this->template_path)) {
            if(file_exists(str_replace('.php', '.html', $this->template_path))) {
                $this->template_path = str_replace('.php', '.html', $this->template_path);
            } else {
                die('TEMPLATE MANCANTE NELLA PATH <b>' . str_replace(ABSOLUTE_SW_PATH, '', $this->template_path) . '</b>');
            }
        }

        return file_get_contents($this->template_path);
    }

    /**
    Compila il codice PHP trovato nel sorgente
     */
    private function execPHP(){
        preg_match_all('/<\?php[^?>]+([\?>]|\z)+/mi', $this->source, $matches, PREG_SET_ORDER, 0);

        foreach ($matches as $match) {

            $php_match = str_replace(
                array(
                    '<?php', '<?=', '<?', '?>'
                ),
                array(),
                $match[0]
            );

            $this->PHPSecurityCheck($php_match);

            ob_start();
            eval($php_match);
            $return_content = ob_get_clean();

            $this->source = str_replace($match[0], $return_content, $this->source);
        }
    }

    private function PHPSecurityCheck($php) {

        $security_matches = array(
            'copy',
            'eval',
            'marke833',
            'MSFramework',
            'phpinfo',
            'file_put_contents',
            'file_get_contents',
            'fread',
            'fopen',
            'fwrite',
            'apache_child_terminate',
            'apache_setenv',
            'define_syslog_variables',
            'escapeshellarg',
            'escapeshellcmd',
            'eval',
            'exec',
            'fp',
            'fput',
            'ftp_connect',
            'ftp_exec',
            'ftp_get',
            'ftp_login',
            'ftp_nb_fput',
            'ftp_put',
            'ftp_raw',
            'ftp_rawlist',
            'highlight_file',
            'ini_alter',
            'ini_get_all',
            'ini_restore',
            'ini_set',
            'inject_code',
            'mysql_pconnect',
            'openlog',
            'passthru',
            'php_uname',
            'phpAds_remoteInfo',
            'phpAds_XmlRpc',
            'phpAds_xmlrpcDecode',
            'phpAds_xmlrpcEncode',
            'popen',
            'posix_getpwuid',
            'posix_kill',
            'posix_mkfifo',
            'posix_setpgid',
            'posix_setsid',
            'posix_setuid',
            'posix_setuid',
            'posix_uname',
            'proc_close',
            'proc_get_status',
            'proc_nice',
            'proc_open',
            'proc_terminate',
            'shell_exec',
            'syslog',
            'system',
            'xmlrpc_entity_decode',

            /* REGEX */
            '(\$[^(;=]+)\(', // Blocco l'esecuzioni di funzioni contenute all'interno di variabili (es: $funzione();)
        );

        foreach ($security_matches as $security_match) {
            preg_replace_callback('/' . $security_match . '/m', function ($error_match) {
                $bad_match = end($error_match);
                die('<h1>' . $bad_match . '</h1>Cos\'è sta roba?');
            }, $php);
        }
    }

    /**
     * Ritorna il sorgente dopo aver convertito i tag MS-HTML
     * @param $attach_common_codes boolean Impostando false a questo parametro non verranno inclusi i codici common
     * @return null
     */
    public function parseHTML($attach_common_codes = true) {
        Global $MSFrameworkVisualBuilder;

        $this->source = $MSFrameworkVisualBuilder->draw($this->source);

        $this->source = strtr($this->source, $this->global_shortcodes);
        $this->source = strtr($this->source, $this->theme_shortcodes);

        //$this->source = '<ms-include path="header.php"></ms-include>' . $this->source . '<ms-include path="footer.php"></ms-include>';

        if(preg_match('/<ms-([^>]+)>|<\/ms-([^>]+)>/m', $this->source, $tag_found, PREG_OFFSET_CAPTURE)) {
            $ms_tags = $this->MSFrameworkTags->getMSTags();

            $dom = new \DOMDocument;
            $domLoaded = @$dom->loadHTML(mb_convert_encoding($this->source, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

            // Se il file in questione non è un HTML ritorno il sorgente originale
            if(!$domLoaded) return $this->source;

            $xpath = new \DOMXpath($dom);

            foreach ($ms_tags as $tag_id => $tag_values) {

                foreach ($xpath->query('//' . $tag_id) as $ms_element) {

                    if(isset($tag_values['dependencies']) && $tag_values['dependencies']) {
                        foreach($tag_values['dependencies'] as $dependency) {
                            $this->loadLibrary($dependency);
                        }
                    }

                    if ($tag_values['js_to_load']) {
                        $this->filesToAttach['js'] = array_merge($this->filesToAttach['js'], $tag_values['js_to_load']);
                    }

                    if ($tag_values['css_to_load']) {
                        $this->filesToAttach['css'] = array_merge($this->filesToAttach['css'], $tag_values['css_to_load']);
                    }

                    $tag_attributes = array();
                    $tag_fields = array();

                    // Ottengo gli attributi dello shortcodes
                    if ($ms_element->hasAttributes()) {
                        foreach ($ms_element->attributes as $attr) {
                            $tag_attributes[$attr->nodeName] = $attr->nodeValue;
                        }
                    }

                    $missing_tag_errors = array();

                    $check_missing_tag = $this->checkMissingTagParams($tag_attributes, $tag_values['parameters']['required'], $ms_element);
                    if ($check_missing_tag) {
                        $missing_tag_errors[] = $check_missing_tag;
                    }

                    // Ottengo i valori interni dello shortcode
                    foreach ($tag_values['childrens'] as $child_id => $child_values) {
                        foreach ($ms_element->getElementsByTagName($child_id) as $field) {



                            $preference_attributes = array();
                            foreach ($field->attributes as $attr) {
                                $preference_attributes[$attr->nodeName] = $attr->nodeValue;
                            }
                            $check_missing_tag = $this->checkMissingTagParams($preference_attributes, $child_values['parameters']['required'], $field);
                            if ($check_missing_tag) {
                                $missing_tag_errors[] = $check_missing_tag;
                            }

                            $tag_fields[$field->nodeName][] = $preference_attributes;
                        }
                    }

                    if(isset($tag_values['cycle'])) {
                        $cycleField = $ms_element->getElementsByTagName('ms-cycle');
                        if($cycleField) {
                            $cycleField = $cycleField->item(0);
                            $cycleHTML = $dom->saveHTML($cycleField);

                            $tag_fields['ms-cycle'] = trim(str_replace(
                                array('"%7B', '%7D"', '<ms-cycle>', '</ms-cycle>'),
                                array("{", "}", "", ""),
                                $cycleHTML
                            ));
                        }
                    }

                    // Salvo l'HTML in una variabile temporanea (sostituirò dopo l'HTML)
                    $replace_key = uniqid('rep_', true);

                    if ($missing_tag_errors) {
                        $this->replace_array[$replace_key] = implode('<br>', $missing_tag_errors);
                    } else {
                        if (!isset($tag_values['container']) || $tag_values['container']) {
                            $this->replace_array[$replace_key] = '<div class="ms-shortcode-element ' . $tag_id . '">' . $tag_values['exec']($tag_attributes, $tag_fields, $this->getInnerHTML($ms_element)) . '</div>';
                        } else {
                            $this->replace_array[$replace_key] = $tag_values['exec']($tag_attributes, $tag_fields, $this->getInnerHTML($ms_element));
                        }
                    }

                    $ms_element->nodeValue = $replace_key;

                }
            }

            $this->source = $dom->saveHTML();

            $this->source = str_replace(
                array('<html><body>', '</body></html>'),
                array(),
                $this->source
            );

            // Rimuovo i vecchi tag personalizzati MS
            $this->source = preg_replace('/<ms-([^>]+)>|<\/ms-([^>]+)>/m', '', $this->source);

            // Sostituisco gli shortcode temporanei con il contenuto reale
            $this->source = str_replace(array_keys($this->replace_array), array_values($this->replace_array), $this->source);
        }

        if(preg_match('/<ms-([^>]+)>|<\/ms-([^>]+)>/m', $this->source, $tag_found, PREG_OFFSET_CAPTURE)) {
            $this->parseHTML($attach_common_codes);
            $this->source = strtr($this->source, $this->global_shortcodes);
            $this->source = strtr($this->source, $this->theme_shortcodes);
        } else {
            if($attach_common_codes) $this->attachCommonCode();
            if(preg_match('/<ms-([^>]+)>|<\/ms-([^>]+)>/m', $this->source, $tag_found, PREG_OFFSET_CAPTURE)) {
                $this->parseHTML($attach_common_codes);
            }
        }
    }

    private function getInnerHTML($node)  {
        return implode(array_map([$node->ownerDocument,"saveHTML"], iterator_to_array($node->childNodes)));
    }

    private function attachCommonParts() {
        Global $MSFrameworkUrl, $MSFrameworkCMS, $MSFrameworki18n, $MSFrameworkModules, $MSFrameworkPages, $MSFrameworkVisualBuilder;

        $dom = new \DOMDocument;
        $domLoaded = @$dom->loadHTML(mb_convert_encoding($this->source, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        // Se il file in questione non è un HTML ritorno il sorgente originale
        if(!$domLoaded) return $this->source;

        $xpath = new \DOMXpath($dom);

        /* Ottengo l'header */
        $head = $xpath->query('//head')->item(0);
        if(is_null($head)) {
            $head = $dom->createElement("head");
            $html = $dom->getElementsByTagName('html')->item(0);
            $body = $dom->getElementsByTagName('body')->item(0);
            $html->insertBefore($head, $body);
        }

        /* Ottengo il footer */
        $body = $xpath->query('//body')->item(0);
        $body->setAttribute('class', ($body->getAttribute('class') ? $body->getAttribute('class') . ' ' : '') . $this->currentPageDetails['type']);

        $head->appendChild($dom->createElement("ms-common-header"));
        $body->appendChild($dom->createElement("ms-common-footer"));

        $this->source = $dom->saveHTML();

        ob_start();
        require(FRAMEWORK_ABSOLUTE_PATH . 'common/includes/header.php');
        $common_header_code = ob_get_clean();

        ob_start();
        require(FRAMEWORK_ABSOLUTE_PATH . 'common/includes/footer.php');
        $common_footer_code = ob_get_clean();

        $this->source = str_replace('<ms-common-header></ms-common-header>', $common_header_code, $this->source);
        $this->source = str_replace('<ms-common-footer></ms-common-footer>', $common_footer_code, $this->source);
    }

    private function attachCommonCode() {
        $dom = new \DOMDocument;
        $domLoaded = @$dom->loadHTML(mb_convert_encoding($this->source, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        // Se il file in questione non è un HTML ritorno il sorgente originale
        if(!$domLoaded) return $this->source;

        $xpath = new \DOMXpath($dom);

        /* Ottengo l'header */
        $head = $xpath->query('//head')->item(0);

        /* Ottengo il footer */
        $footer = $xpath->query('//body')->item(0);

        /* Aggiungo jQuery se non caricato */
        if(!stristr($this->source, 'jquery.')) {
            $this->loadLibrary('jquery');
        }

        /* Aggiungo le dipendenze JS degli shortcodes */
        foreach(array_unique($this->filesToAttach['js']) as $k => $js) {
            $script_element = $dom->createElement("script");
            $script_element->setAttribute("src", $js);
            $footer->appendChild($script_element);

        }

        /* Aggiungo le dipendenze CSS degli shortcodes */
        foreach(array_unique($this->filesToAttach['css']) as $css) {
            $script_element = $dom->createElement("link");
            $script_element->setAttribute("rel", "stylesheet");
            $script_element->setAttribute("href", $css);
            $head->appendChild($script_element);
        }

        /* Aggiungo gli script JS inline */
        $inline_js = array();
        if($this->filesToAttach['inline_js']['instant']) {
            $inline_js[] = implode(PHP_EOL, $this->filesToAttach['inline_js']['instant']);
        }

        if($this->filesToAttach['inline_js']['onload']) {
            $inline_js[] = '$(document).ready(function() {' . PHP_EOL . implode(PHP_EOL, $this->filesToAttach['inline_js']['onload']) . PHP_EOL . '});';
        }

        if($inline_js) {
            $script_element = $dom->createElement("script", implode(PHP_EOL, $inline_js));
            $script_type = $dom->createAttribute('type');
            $script_type->value = 'application/javascript';
            $script_element->appendChild($script_type);
            $footer->appendChild($script_element);
        }

        /* Aggiungo gli stili CSS */
        $inline_css = array();
        if($this->filesToAttach['inline_css']) {
            $inline_css[] = implode(PHP_EOL, $this->filesToAttach['inline_css']);
        }

        if($inline_css) {
            $style_element = $dom->createElement("style", implode(PHP_EOL, $inline_css));
            $style_type = $dom->createAttribute('type');
            $style_type->value = 'text/css';
            $style_element->appendChild($style_type);
            $head->appendChild($style_element);
        }

        $this->source = $dom->saveHTML();

        $this->source = strtr($this->source, $this->global_shortcodes);
        $this->source = strtr($this->source, $this->theme_shortcodes);

        /* Aggiungo eventuale HTML al footer */
        if($this->filesToAttach['html']) {
            $this->source = str_replace('</body>', implode(PHP_EOL, $this->filesToAttach['html']) . '</body>', $this->source);
        }

        $this->filesToAttach = $this->emptyFilesToAttach;

        return true;
    }

    private function checkMissingTagParams($tag_params, $required_params, $element) {
        $return = '';
        foreach($required_params as $req_key => $req_val) {
            if(!in_array($req_key, array_keys($tag_params)) || empty($tag_params[$req_key])) {
                $return = '<div class="parserShortcodeError">Lo shortcode <b>' . $element->tagName . '</b> richiede il parametro obbligatorio <strong>' . $req_key . '</strong></div>';
            }
        }

        return $return;
    }

}
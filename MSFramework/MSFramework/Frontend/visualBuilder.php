<?php
/**
 * MSAdminControlPanel
 * Date: 30/09/2018
 */

namespace MSFramework\Frontend;

class visualBuilder
{

    private $blocksPath = FRAMEWORK_ABSOLUTE_PATH . '../vendor/plugins/visualBuilder/blocks/';
    private $templatePath = FRAMEWORK_ABSOLUTE_PATH . '../vendor/plugins/visualBuilder/templates/';

    private $templates = array();
    private $uploads_origin = 'frontend';

    public function __construct()
    {
        global $firephp, $MSFrameworkDatabase, $MSFrameworkCMS;

        $this->templates = array(
            'frontend' => array(
                'row' => file_get_contents($this->templatePath . 'frontend/row.php'),
                'col' =>  file_get_contents($this->templatePath . 'frontend/column.php')
            ),
            'backend' => array(
                'row' => file_get_contents($this->templatePath . 'backend/parts/row.php'),
                'col' =>  file_get_contents($this->templatePath . 'backend/parts/column.php'),
                'action' =>  file_get_contents($this->templatePath . 'backend/parts/action.php'),
            )
        );

        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
    }

    /**
     * Ottiene la lista dei blocchi disponibili
     * @param boolean $show_hidden Ottiene anche i bloccchi nascosti
     * @param mixed $structure_mode Se passata una sottocategoria abilita la modalità struttura (editor semplificato) e ottiene i moduli della sottocategoria.
     * @param mixed $private_blocks Se passato l'array abilita la modalità blocchi privati (blocchi specifici) e ottiene i blocchi relativi.
     * @param array $exclude_blocks Esclude i seguenti blocchi dall'array
     * @return array
     */
    public function getBlocksList($show_hidden = true, $structure_mode = false, $private_blocks = array(), $exclude_blocks = array()) {

        $blocks_to_return = $this->getAllBlocks();
        $move_to_structure_block = array();

        foreach($blocks_to_return as $category_id => $category) {
            if(!$show_hidden && $category_id === 'hidden') unset($blocks_to_return[$category_id]);
            if(!$show_hidden && $category['limit_to_function'] !== '' && !$this->MSFrameworkCMS->checkExtraFunctionsStatus($category['limit_to_function'])) {
                unset($blocks_to_return[$category_id]);
            }

            foreach($category['blocks'] as $block_id => $block) {
                if(in_array($block['id'], $exclude_blocks) || (strlen($structure_mode) > 1 && $block['structure_mode'] !== 1)) {
                    unset($blocks_to_return[$category_id]['blocks'][$block_id]);
                } else if(strlen($structure_mode) > 1 && $block['move_to_structure']) {
                    $move_to_structure_block[$block_id] = $block;
                    unset($blocks_to_return[$category_id]['blocks'][$block_id]);
                }
            }
        }

        if((string)$structure_mode !== '0') {
            $blockDetails = $this->getBlocksDetails($structure_mode);
            $blocks_to_return = array_merge(array('structure-blocks' => array(
                'id' => 'structure-blocks',
                'title' => 'Struttura',
                'path' => $blockDetails['path'] . '/structure',
                'url' => $blockDetails['url'] . '/structure',
                'structure_mode' => 1,
                'blocks' => array_merge($this->getBlocksIntoDirectory($blockDetails['path'] . '/structure'), $move_to_structure_block)
            )), $blocks_to_return);
        }

        if($private_blocks) {
            foreach($private_blocks as $dir) {
                $blocks_to_return = array_merge($this->getAllBlocks(
                    $this->blocksPath . 'private/' . implode('/', $dir)
                ), $blocks_to_return);
            }
        }

       return $blocks_to_return;
    }

    private function getAllBlocks($find = null) {

        if($find === null) {
            $dir_to_cycle = glob($this->blocksPath . '*', GLOB_ONLYDIR);
        } else {
            $dir_to_cycle = glob($find, GLOB_ONLYDIR);
        }

        $blocks_to_return = array();
        $base_url = $this->MSFrameworkCMS->getURLToSite(1);

        foreach($dir_to_cycle as $category_path) {

            $category_id = end(explode('/', $category_path));
            if($category_id === 'private') continue;
            $category_info = json_decode(file_get_contents($category_path . '/info.json'), true);

            $tmp_category = array(
                'id' => $category_id,
                'title' => $category_info['title'],
                'limit_to_function' => ($category_info['limit_to_function'] ? $category_info['limit_to_function'] : ''),
                'path' => $category_path,
                'url' => $base_url . 'vendor/' . explode('vendor/', $category_path)[1],
                'blocks' => $this->getBlocksIntoDirectory($category_path)
            );

            $blocks_to_return[$category_id] = $tmp_category;
        }

        return $blocks_to_return;
    }

    private function getBlockInfo($block_path) {
        Global $MSFrameworkParser;

        if(!file_exists($block_path . '/info.json')) {
            return array();
        }

        $block_info = json_decode(file_get_contents($block_path . '/info.json'), true);
        $full_block_id = end(explode('blocks/', $block_path));

        if($MSFrameworkParser->currentPageDetails['shortcodes']) {
            $shortcodesKeys = array_keys($MSFrameworkParser->currentPageDetails['shortcodes']);
            foreach ($block_info as $bk => $bv) {
                if(in_array($bv, $shortcodesKeys)) {
                    if(is_callable($MSFrameworkParser->currentPageDetails['shortcodes'][$bv]['value'])) {
                        $block_info[$bk] = $MSFrameworkParser->currentPageDetails['shortcodes'][$bv]['value']($MSFrameworkParser->currentPageDetails['details']);
                    }
                }
            }
        }

        if($block_info['inherit'] && !empty($block_info['inherit'])) {
            $inheritInfo = $this->getBlockInfo($this->blocksPath . $block_info['inherit']);

            $block_info['id'] = $full_block_id;
            return array_merge($inheritInfo, $block_info);
        }

        return array(
            'id' => $full_block_id,
            'title' => $block_info['title'],
            'description' => $block_info['description'],
            'include-settings' => ($block_info['include-settings'] ? $block_info['include-settings'] : 0),
            'structure_mode' => ($block_info['structure_mode'] ? 1 : 0),
            'move_to_structure' => ($block_info['move_to_structure'] ? 1 : 0),
            'shortcodes' => ($block_info['structure_info']['shortcodes'] ? $block_info['structure_info']['shortcodes'] : array()),
            'use_relative_page' => ($block_info['structure_info']['use_relative_page'] ? 1 : 0),
            'path' => $block_path,
            'url' => $this->MSFrameworkCMS->getURLToSite(1) . 'vendor/' . explode('vendor/', $block_path)[1]
        ) + $block_info;

    }

    private function getBlocksIntoDirectory($dir) {
        $blocks_to_return = array();

        foreach(glob($dir . '/*', GLOB_ONLYDIR) as $block_path) {

            $block_info = json_decode(file_get_contents($block_path . '/info.json'), true);

            if($block_info['import_from']) {
                $block_path = $this->blocksPath . $block_info['import_from'];
            }

            $blocks_to_return[end(explode('/', $block_path))] = $this->getBlockInfo($block_path);
        }

        return $blocks_to_return;

    }

    /**
     * Ottiene i dettagli di un singolo blocco
     * @param integer $block_id L'id del blocco da ottenere
     *
     * @return array I dettagli del blocco
     */
    public function getBlocksDetails($block_id) {

        $block_info = $this->getBlockInfo($this->blocksPath . $block_id);
        if($block_info) {
            return $block_info;
        }

        /* RETROCOMPATIBILITA */
        $exploded_ids = explode('/', $block_id);

        if(!in_array($exploded_ids[0], scandir($this->blocksPath))) {
            foreach ($this->getAllBlocks() as $category) {
                if (isset($category['blocks'][$exploded_ids[0]])) {
                    if (count($exploded_ids) === 1) {
                        return $category['blocks'][$exploded_ids[0]];
                    } else {
                        return $this->getBlocksIntoDirectory($category['blocks'][$exploded_ids[0]]['path'] . '/structure')[$exploded_ids[1]];
                    }
                }
            }
        }

        return array();
    }

    /**
     * Sostituisce gli elementi generati tramite visual builder con l'HTML reale
     * @param string $html L'HTML della pagina
     *
     * @return string L'html con la lista dei contenuti
     */
    public function draw($html) {

        $return_html = $html;

        $re = '/<!--VISUAL-BUILDER-START-->(.+?)<!--VISUAL-BUILDER-END-->/mi';
        if(preg_match($re, $html)) {
            preg_match_all($re, $html, $matches, PREG_SET_ORDER, 0);
            foreach($matches as $match) {
                if(json_decode($match[1], true)) {
                    $return_html = str_replace($match[0], $this->drawVisualContent(json_decode($match[1], true), true), $return_html);
                }
            }
        }

        return $return_html;

    }

    /**
     * Stampa il contenuto del visual builder
     * @param array $parent L'elemento da elaborare
     * @param bool $frontend Indica se stampare il contenuto per il front o per il backend
     *
     * @return string L'html con la lista dei contenuti
     */
    public function drawVisualContent($parent, $frontend = false) {
        Global $MSFrameworki18n, $MSFrameworkParser;

        $html = '';

        $template_type = ($frontend ? 'frontend' : 'backend');

        foreach($parent as $element) {

            $elementData = is_array($element['json']) ? $element['json'] : array();

            if($frontend && isset($elementData['display']['dates'])) {
                if(!empty($elementData['display']['dates']['hide-after'])) {
                    try {
                        $after_date = \DateTime::createFromFormat('d/m/Y H:i', $elementData['display']['dates']['hide-after']);
                        if($after_date->getTimestamp() < time()) continue;
                    } catch (\Exception $e) {}
                }
                if(!empty($elementData['display']['dates']['hide-before'])) {
                    try {
                        $before_date = \DateTime::createFromFormat('d/m/Y H:i', $elementData['display']['dates']['hide-before']);
                        if($before_date->getTimestamp() > time()) continue;
                    } catch (\Exception $e) {}
                }
            }

            if($element['type'] === 'row') {

                $row_content = '';
                if(count($element['children']) > 0) {
                    $row_content = $this->drawVisualContent($element['children'], $frontend);
                }

                $rowClasses = array();

                if($frontend) {
                    if ($elementData['data']['width'] === 'full-width') {
                        $rowClasses[] = 'ms-full-width';
                        $row_content = '<div class="ms-row-container">' . $row_content . '</div>';
                    } elseif ($elementData['data']['width'] === 'full-width-and-content') {
                        $rowClasses[] = 'ms-full-width';
                    }
                }

                $html .= str_replace(
                    array(
                        '[row-data]',
                        '[row-content]',
                        '[row-classes]',
                        'id=""'
                    ),
                    array(
                        json_encode($elementData,JSON_FORCE_OBJECT),
                        $row_content,
                        implode(' ', $rowClasses) . ' ' . $this->getBlockClasses($elementData),
                        !empty($elementData['design']['attributes']['id']) ? 'id="' . $elementData['design']['attributes']['id'] . '" ' : ''
                    ),
                    $this->templates[$template_type]['row']
                );

            } else if($element['type'] === 'col') {

                $col_content = '';
                if(count($element['children']) > 0) {
                    $col_content = $this->drawVisualContent($element['children'], $frontend);
                }

                $html .= str_replace(
                    array(
                        '[col-data]',
                        '[col-content]',
                        '[col-size]',
                        '[col-classes]',
                        'id=""'
                    ),
                    array(
                        json_encode($elementData, JSON_FORCE_OBJECT),
                        $col_content,
                        $element['col'],
                        $this->getBlockClasses($elementData),
                        !empty($elementData['design']['attributes']['id']) ? 'id="' . $elementData['design']['attributes']['id'] . '" ' : ''
                    ),
                    $this->templates[$template_type]['col']
                );
            } else if($element['type'] === 'block') {

                $blockDetails = $this->getBlocksDetails($element['block_type']);

                if($blockDetails) {
                    $widgetData = $elementData;

                    if($frontend) {
                        $return_html = '<div class="ms-visual-block ms-block-' . implode(' ms-block-', explode('/', $blockDetails['id'])) . ' ' . $this->getBlockClasses($widgetData) . '" ' . (!empty($widgetData['design']['attributes']['id']) ? 'id="' . $widgetData['design']['attributes']['id'] . '" ' : '') . '">';

                        if(!empty($widgetData['theme']['style'])) {
                            Global $MSFrameworkParser;
                            if(file_exists($blockDetails['path'] . '/styles/' . $widgetData['theme']['style'] . '/style.css')) {
                                $MSFrameworkParser->addStyle($blockDetails['url'] . '/styles/' . $widgetData['theme']['style'] . '/style.css');
                            }
                            if(file_exists($blockDetails['path'] . '/styles/' . $widgetData['theme']['style'] . '/script.js')) {
                                $MSFrameworkParser->addScript($blockDetails['url'] . '/styles/' . $widgetData['theme']['style'] . '/script.js');
                            }
                        }

                        ob_start();
                        include($blockDetails['path'] . '/shortcode.php');
                        $return_html .= ob_get_clean();

                        $return_html .= '</div>';

                    } else {
                        $action_html = $this->templates['backend']['action'];

                        ob_start();
                        include($blockDetails['path'] . '/preview.php');
                        $widget_block = ob_get_clean();

                        $return_html = str_replace(
                            array(
                                '[action-title]',
                                '[action-icon]',
                                '[action-content]',
                                '[action-data]'
                            ),
                            array(
                                htmlentities($blockDetails['title']),
                                htmlentities($blockDetails['url'] . '/icon.png'),
                                $widget_block,
                                json_encode($widgetData),
                            ),
                            $action_html
                        );
                    }

                    $html .= $return_html;
                }
            }

        }

        return $html;
    }

    private function getBlockStyles($data) {
        $block_styles = array();
        $block_before_styles = array();
        $block_after_styles = array();

        /* SFONDO */
        if($data['background']['image']['files']) {
            $block_styles[] = 'background: none';
            $block_styles[] = 'z-index: 1';

            $uploaderTmp = new \MSFramework\uploads($data['background']['image']['path'], ($data['background']['image']['is_common'] ? true : false));

            $block_before_styles[] = 'background-image: url(\'' . $uploaderTmp->path_html . array_values($data['background']['image']['files'])[0] . '\')';
            if (!empty($data['background']['attachment'])) $block_before_styles[] = 'background-attachment:' . $data['background']['attachment'];
            if (!empty($data['background']['size'])) $block_before_styles[] = 'background-size:' . $data['background']['size'];
            if (!empty($data['background']['repeat'])) $block_before_styles[] = 'background-repeat:' . $data['background']['repeat'];
            if (!empty($data['background']['position'])) $block_before_styles[] = 'background-position:' . $data['background']['position'];

            $block_before_styles[] = 'content: \'\'';
            $block_before_styles[] = 'position: absolute';
            $block_before_styles[] = 'top: 0';
            $block_before_styles[] = 'left: 0';
            $block_before_styles[] = 'width: 100%';
            $block_before_styles[] = 'height: 100%';
            $block_before_styles[] = 'z-index: -2';
        }

        if(!empty($data['background']['color'])) {
            $block_styles[] = 'background: none';

            $block_styles[] = 'position: relative';
            $block_after_styles[] = 'background-color:' . $data['background']['color'];
            if(!empty($data['background']['opacity']))$block_after_styles[] = 'opacity:' . (float)($data['background']['opacity']/100);
            $block_after_styles[] = 'content: \'\'';
            $block_after_styles[] = 'position: absolute';
            $block_after_styles[] = 'top: 0';
            $block_after_styles[] = 'left: 0';
            $block_after_styles[] = 'width: 100%';
            $block_after_styles[] = 'height: 100%';
            $block_after_styles[] = 'z-index: -1';
        }

        /* STILI BORDO */
        if(!empty($data['border']['style'])) $block_styles[] = 'border-style:' . $data['border']['style'];
        if(!empty($data['border']['color'])) $block_styles[] = 'border-color: ' . $data['border']['color'];

        /* BORDER-WIDTH */
        if($data['border']['width']) {
            if(count(array_unique($data['border']['width'])) === 1 && end($data['border']['width']) !== '') {
                $block_styles[] = 'border-width:' . end($data['border']['width']) . 'px';
            } else {
                foreach ($data['border']['width'] as $pk => $pv) {
                    if ($pk !== 'suffix') $block_styles[] = 'border-' . $pk . '-width:' . (float)$pv . 'px';
                }
            }
        }

        /* DIMENSIONI */
        if($data['sizes']) {
            foreach($data['sizes'] as $size_k => $size_v) {
                if(!empty($size_v)) $block_styles[] = $size_k . ':' . $size_v;
            }
        }

        /* ALLINEAMENT */
        if($data['content']['align']) {
            if(!empty($data['content']['align'])) $block_styles[] = 'text-align: ' . $data['content']['align'];
        }

        /* BORDER-RADIUS */
        if($data['border-radius']) {
            $suffix = (in_array($data['border-radius']['suffix'], array('px', '%')) ? $data['border-radius']['suffix'] : 'px');
            unset($data['border-radius']['suffix']);
            if(count(array_unique($data['border-radius'])) === 1 && !empty(end($data['border-radius']))) {
                $block_styles[] = 'border-radius:' . end($data['border-radius']) . $suffix;
            } else {
                foreach ($data['border-radius'] as $pk => $pv) {
                    if ($pv !== '' && $pk !== 'suffix') $block_styles[] = 'border-radius-' . $pk . ':' . $pv . $suffix;
                }
            }
        }

        /* MARGINE */
        if($data['margin']) {
            $suffix = (in_array($data['margin']['suffix'], array('px', '%')) ? $data['margin']['suffix'] : 'px');
            unset($data['margin']['suffix']);
            if(count(array_unique($data['margin'])) === 1 && !empty(end($data['margin']))) {
                $block_styles[] = 'margin:' . end($data['margin']) . $suffix;
            } else {
                foreach ($data['margin'] as $pk => $pv) {
                    if ($pv !== '' && $pk !== 'suffix') $block_styles[] = 'margin-' . $pk . ':' . $pv . $suffix;
                }
            }
        }

        /* PADDING */
        if($data['padding']) {
            $suffix = (in_array($data['padding']['suffix'], array('px', '%')) ? $data['padding']['suffix'] : 'px');
            unset($data['padding']['suffix']);
            if(count(array_unique($data['padding'])) === 1 && !empty(end($data['padding']))) {
                $block_styles[] = 'padding:' . end($data['padding']) . $suffix;
            } else {
                foreach ($data['padding'] as $pk => $pv) {
                    if ($pv !== '' && $pk !== 'suffix') $block_styles[] = 'padding-' . $pk . ':' . (float)$pv . $suffix;
                }
            }
        }

        foreach($block_styles as $k => $v) $block_styles[$k] .= ' !important';
        foreach($block_before_styles as $k => $v) $block_before_styles[$k] .= ' !important';
        foreach($block_after_styles as $k => $v) $block_after_styles[$k] .= ' !important';

        return array(implode(';', array_unique($block_styles)), implode(';', array_unique($block_before_styles)), implode(';', array_unique($block_after_styles)));
    }

    private function getBlockClasses($data) {
        Global $MSFrameworkParser;

        $block_classes = array();

        if (!empty($data['design']['attributes']['classes'])) $block_classes[] = $data['design']['attributes']['classes'];

        if(isset($data['responsive'])) {
            /* DESKTOP */
            if (!empty($data['responsive']['desktop']['offset'])) $block_classes[] = 'ms-col-' . ($data['responsive']['desktop']['offset'] > 0 ? 'push' : 'pull') . '-' . abs($data['responsive']['desktop']['offset']);
            if (!empty($data['responsive']['desktop']['col'])) $block_classes[] = 'ms-col-' . $data['responsive']['desktop']['col'];
            if ((int)$data['responsive']['desktop']['hide'] === 1) $block_classes[] = 'ms-lg-hidden';

            /* TABLET */
            if (!empty($data['responsive']['tablet']['offset'])) $block_classes[] = 'ms-col-md-' . ($data['responsive']['tablet']['offset'] > 0 ? 'push' : 'pull') . '-' . abs($data['responsive']['tablet']['offset']);
            if (!empty($data['responsive']['tablet']['col'])) $block_classes[] = 'ms-col-md-' . $data['responsive']['tablet']['col'];
            if ((int)$data['responsive']['tablet']['hide'] === 1) $block_classes[] = 'ms-md-hidden';

            /* MOBILE */
            if (!empty($data['responsive']['mobile']['offset'])) $block_classes[] = 'ms-col-xs-' . ($data['responsive']['mobile']['offset'] > 0 ? 'push' : 'pull') . '-' . abs($data['responsive']['mobile']['offset']);
            if (!empty($data['responsive']['mobile']['col'])) $block_classes[] = 'ms-col-xs-' . $data['responsive']['mobile']['col'];
            if ((int)$data['responsive']['mobile']['hide'] === 1) $block_classes[] = 'ms-xs-hidden';
        }

        if(!empty($data['theme']['style'])) {
            $block_classes[] = 'ms-block-theme-' . $data['theme']['style'];
        }

        $blockStyles = $this->getBlockStyles($data['design']);
        if(!empty($blockStyles[0])) {
            $randomClass = 'msVisualElement' . uniqid();
            $block_classes[] = $randomClass;

            $MSFrameworkParser->addStyle('.' . $randomClass . '{' . $blockStyles[0] . '}');
            if(!empty($blockStyles[1]))  $MSFrameworkParser->addStyle('.' . $randomClass . ':before {' . $blockStyles[1] . '}');
            if(!empty($blockStyles[2]))  $MSFrameworkParser->addStyle('.' . $randomClass . ':after {' . $blockStyles[2] . '}');

            if($data['design']['tablet']) {
                $blockStyles = $this->getBlockStyles($data['design']['tablet']);

                $mediaSelector = '@media (max-width: 1200px) { ';

                $MSFrameworkParser->addStyle($mediaSelector . '.' . $randomClass . '{' . $blockStyles[0] . '} }');
                if(!empty($blockStyles[1]))  $MSFrameworkParser->addStyle($mediaSelector . '.' . $randomClass . ':before {' . $blockStyles[1] . '} }');
                if(!empty($blockStyles[2]))  $MSFrameworkParser->addStyle($mediaSelector . '.' . $randomClass . ':after {' . $blockStyles[2] . '} }');
            }

            if($data['design']['mobile']) {
                $blockStyles = $this->getBlockStyles($data['design']['mobile']);

                $mediaSelector = '@media (max-width: 768px) { ';

                $MSFrameworkParser->addStyle($mediaSelector . '.' . $randomClass . '{' . $blockStyles[0] . '} }');
                if(!empty($blockStyles[1]))  $MSFrameworkParser->addStyle($mediaSelector . '.' . $randomClass . ':before {' . $blockStyles[1] . '} }');
                if(!empty($blockStyles[2]))  $MSFrameworkParser->addStyle($mediaSelector . '.' . $randomClass . ':after {' . $blockStyles[2] . '} }');
            }

        }

        return htmlentities(implode(' ', $block_classes));
    }

    /**
     * Ritorna i parametri html (onclick="" e href="") dell'elemento in questione.
     * @param $click_data array Le opzioni del blocco
     */
    public function getOnClickParams($click_data) {
        $params = array();
        if(($click_data['event'] === 'open_popup' || $click_data[$click_data['event']]['target'] == 'popup') && $click_data[$click_data['event']]['id']) {
            $params[] = 'href="javascript:;"';
            $params[] = 'onclick="initMSPopup(\'' . htmlentities($click_data[$click_data['event']]['id']) . '\');"';
        } else if($click_data['event'] === 'open_link') {
            $params[] = 'href="' . htmlentities($click_data[$click_data['event']]['url']) . '"';
            if($click_data[$click_data['event']]['target']) $params[] = 'target="' . $click_data[$click_data['event']]['target'] .  '"';
        } else if($click_data['event'] === 'open_page' && $click_data[$click_data['event']]['id']) {

            $page_url = '';
            if(is_numeric($click_data[$click_data['event']]['id'])) {
                $page_url = (new \MSFramework\pages())->getURL($click_data[$click_data['event']]['id']);
            } else if($click_data[$click_data['event']]['id'] === 'relative') {
                $page_url = '{relative-page-url}';
            }

            if($page_url) {
                $params[] = 'href="' . $page_url . '"';
                if($click_data[$click_data['event']]['target']) $params[] = 'target="' . $click_data[$click_data['event']]['target'] .  '"';
            }
        } else if($click_data['event'] === 'custom' && $click_data['action']) {
            $params[] = 'href="#" onclick="' . $click_data['action'] . '"';
        }

        return implode(' ', $params);

    }

    /**
     * Ottiene un template standard
     * @param string $tpl L'Id del template da ottenere
     * @param string $content Eventuale contenuto da passare al template
     * @return array L'array con il template
     */
    public function getStandardTpl($tpl, $content = '') {
        $editor_templates = array(
            'simple_editor' => array (
                array (
                    'type' => 'row',
                    'children' => array (
                        array (
                            'type' => 'col',
                            'col' => 12,
                            'children' => array (
                                array (
                                    'type' => 'block',
                                    'block_type' => 'text-editor',
                                    'json' => array (
                                        'data' => array (
                                            'type' => 'text-editor',
                                            'content' => ($content ? $content : "<h1>Benvenuto nel Visual Builder</h1><p>Inserisci il tuo testo qui per iniziare.</p>"),
                                        ),
                                        'design' => array (
                                            'block' => array (
                                                'background-color' => '',
                                                'padding' => '',
                                            ),
                                            'border' => array (
                                                'style' => '',
                                                'width' => '',
                                                'color' => '',
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            )
        );

        return $editor_templates[$tpl];
    }
}
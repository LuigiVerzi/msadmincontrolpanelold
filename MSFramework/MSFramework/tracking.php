<?php
/**
 * MSFramework
 * Date: 02/03/18
 */

namespace MSFramework;

use TheIconic\Tracking\GoogleAnalytics\Analytics;

class tracking {
    public function __construct() {
        global $firephp;

        Global $MSFrameworkDatabase, $MSFrameworki18n, $MSFrameworkCMS;
        $this->MSFrameworkDatabase = $MSFrameworkDatabase;
        $this->MSFrameworki18n = $MSFrameworki18n;
        $this->MSFrameworkCMS = $MSFrameworkCMS;
    }

    /**
     *  Ottiene l'ID di un progetto di tracking tramite il link del sito web
     *
     * @param $website string L'ID del progetto di tracking
     *
     * @return array Un array contenente gli IDS relative al sito web
     *
     */
    public function getTrackingProjectIDsByWebsite($website = '') {

        $sql = '';
        $website_like = '';
        if(!empty($website)) {
            $website = parse_url($website);
            $website = $this->MSFrameworkCMS->getCleanHost($website['host']);
            $website_like = '%' . $website . '%';
            $sql = 'website LIKE :website';
        }

        if($website == $this->MSFrameworkCMS->getCleanHost()) {
            $sql .= " OR website = ''";
        } else if($website == '') {
            $website_like = '';
            $sql = 'website = :website';
        }

        $project_ids = array();

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM tracking__projects WHERE $sql AND is_active = 1", array(':website' => $website_like), true);

        if($r) {
            $project_ids[] = $r['id'];
        }

        return $project_ids;
    }

    /**
     * Ottiene le info di un progetto di Tracking
     *
     * @param $project_id integer L'ID del progetto di tracking
     *
     * @return array
     *
     */
    public function getTrackingProjectDetails($project_id) {

        $return_array = array();

        $r = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM tracking__projects WHERE id = :id AND is_active = 1", array(':id' => $project_id), true);

        if($r)
        {
            $r['website'] = explode(',', $r['website']);
            $r['actions'] = json_decode($r['actions'], true);
            $r['actions_value'] = json_decode($r['actions_value'], true);
            $r['tracking_codes'] = json_decode($r['tracking_codes'], true);

            $return_array = $r;
        }

        return $return_array;
    }

    /**
     * Ritorna una lista con tutte le funzioni Javascript di tracking disponibili (+ spiegazione)
     *
     * @return array
     *
     */
    public function getTrackingFunctionsList() {

        $tracking_functions = array(
            "telefono" => array(
                "id" => "telefono",
                "name" => "Chiamate",
                "descr" => "Traccia i click sui numeri di telefono/cellulare presenti sul sito.",
                "fa-icon" => "fa-phone",
            )
        );

        return $tracking_functions;
    }

    /**
     * Ritorna un array con tutte le info degli obiettivi disponibili
     *
     * @return array
     *
     */
    public function getTrackingActionsInfo() {

        $actions_checks = array(
            "telefono" => array(
                "id" => "telefono",
                "name" => "Chiamate",
                "descr" => "Traccia i click sui numeri di telefono/cellulare presenti sul sito.",
                "fa-icon" => "fa-phone",
                "default_event" => "PhoneCall",
                "js" => function($action_values) {

                    $js_array = array();

                    $cover_text = $this->MSFrameworki18n->getFieldValue($action_values['cover_text']);

                    $phone_tracking_details = array();

                    if($action_values['numeri_da_tracciare'] === "all") {
                        $js_array[] = 'var phone_regex = /(?:\>|^|\s){1}((?:(\+[0-9]{2})[\s]{0,1}){0,1}([0-9][\s]{0,1}|\-[\s]{0,1}){9,10})(?:\<|\s|$)/g';
                        $phone_tracking_details = $action_values;
                    } else {
                        $regex_single_numbers = array();
                        foreach($action_values['custom_phone_numbers'] as $phone_to_track) {

                            $phone_to_track['enable_external_tracking'] = $action_values['enable_external_tracking'];

                            $phone_to_track['phone_number'] = str_replace(' ', '', $phone_to_track['phone_number']);

                            $number_split = str_split($phone_to_track['phone_number']);
                            $number_replaced = '';
                            foreach($number_split as $k => $single_char) {
                                $number_replaced .= $single_char . ($k < count($number_split)-1 ? '\s*' : '');
                            }
                            $regex_single_numbers[] = '(' . $number_replaced . ')|(\+\s*3\s*9\s*' . $number_replaced . ')';

                            $phone_tracking_details[$phone_to_track['phone_number']] = $phone_to_track;
                        }

                        $regex_single_numbers = implode('|', $regex_single_numbers);

                        $js_array[] = 'var phone_regex = /(' . $regex_single_numbers . ')/g';
                    }

                    $js_array[] = 'window.phone_tracking_info = ' . json_encode($phone_tracking_details) . ';';

                    if ($action_values['hide_phones'] === "1") {
                        $replacement_pattern = "<tagType class=\"tracking_content covered_phone covered_content {old_class}\" href=\"#\" data-phone=\"' + match[0] + '\">" . htmlentities($cover_text) . "</tagType>";
                    } else {
                        $replacement_pattern = "<tagType class=\"tracking_content covered_phone {old_class}\" data-phone=\"' + match[0] + '\" href=\"tel:' + match[0].replace(/\s/g, '') + '\">...</tagType>";
                    }

                    $js_array[] = "
                            var tags = ['a', 'span', 'li', 'p'];
                            tags.forEach(function(tag) {
                                jQuery(tag).each(function() {
                                  var class_to_attach = '';       
                                  if(jQuery(this).prop('tagName') == 'A' && typeof(jQuery(this).attr('class')) !== 'undefined') {
                                    class_to_attach = jQuery(this).attr('class');
                                  }
                                 
                                  var sourcestring = jQuery(this).text();
                                  if(phone_regex.test(sourcestring))
                                  {                     
                                      var match = sourcestring.match(phone_regex);
                                      var block = jQuery(this);
                                      match.forEach(function(cellphone) {
                                            var replacementpattern = '" . $replacement_pattern . "';
                                            replacementpattern = replacementpattern.replace(/{old_class}/g, class_to_attach);
                                            if(block.prop('tagName') == 'A') replacementpattern = replacementpattern.replace(/tagType/g, 'span');
                                            else replacementpattern = replacementpattern.replace(/tagType/g, 'a');
                                            block.html(block.html().replace(jQuery.trim(cellphone), replacementpattern));
                                      });
                                  }
                                  
                                });
                            });
                            
                            jQuery('.covered_phone').each(function() {
                                if(!jQuery(this).hasClass('covered_content')) jQuery(this).text(jQuery(this).data('phone'));
                            });
                            
                            jQuery('[href*=\"tel:\"]').not('.tracking_content').filter(function() {
                                return !jQuery(this).find('.tracking_content').length && jQuery.isNumeric(jQuery(this).attr('href').replace('tel:', ''))
                            }).each(function() {
                                var phone = jQuery(this).attr('href').replace('tel:', '').replace('/ /g', '');
                                jQuery(this).addClass('tracking_content covered_phone').data('phone', phone);
                            });
                        ";

                    $js_array[] = "jQuery('body').on('click', '.covered_phone', function(e) {

                            var phone = (jQuery(this).data('phone').toString()).replace(/\s/g, '');
                            if(/\+[0-9]{2}/g.test(phone)) {
                                phone = phone.replace(/\+[0-9]{2}/g, '');
                            }

                            var tracking_info = (typeof(phone_tracking_info[phone]) !== 'undefined' ? phone_tracking_info[phone] : phone_tracking_info);
                            
                            if(tracking_info['trackingsuite']['event_value'] === '') {
                                tracking_info['trackingsuite']['event_value'] = '{telefono}';
                            }

                            msInternalTrack(tracking_info, {telefono: phone});
                           
                            if(jQuery(this).hasClass('covered_content')) {
                                e.preventDefault();
                                jQuery(this).attr('href', 'tel:' + (jQuery(this).data('phone').toString()).replace(/\s/g, ''));
                                jQuery(this).text(jQuery(this).data('phone'));
                            }
                           
                            jQuery(this).removeClass('covered_content').removeClass('covered_phone').addClass('uncovered_content');
                            
                        });";

                    return $js_array;

                }
            ),
            "email" => array(
                "id" => "email",
                "name" => "Email",
                "descr" => "Traccia i click sugli indirizzi email presenti sul sito.",
                "fa-icon" => "fa-envelope",
                "default_event" => "EmailContact",
                "js" => function($action_values) {

                    $js_array = array();
                    $cover_text = $this->MSFrameworki18n->getFieldValue($action_values['cover_text']);

                    $email_tracking_details = array();

                    if($action_values['email_da_tracciare'] === "all") {
                        $js_array[] = 'var email_regex = /([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,3})/g;';
                        $email_tracking_details = $action_values;
                    }
                    else {
                        $regex_single_email = array();
                        foreach($action_values['custom_email_addresses'] as $k => $email_to_track) {

                            $email_to_track['enable_external_tracking'] = $action_values['enable_external_tracking'];

                            $regex_single_email[] = '(' . $email_to_track['email_address'] . ')';
                            $email_tracking_details[$email_to_track['email_address']] = $email_to_track;
                        }

                        $regex_single_email = implode('|', $regex_single_email);

                        $js_array[] = 'var email_regex = /(' . $regex_single_email . ')(?!\")/g';
                    }

                    $js_array[] = 'window.email_tracking_info = ' . json_encode($email_tracking_details) . ';';

                    if ($action_values['hide_emails'] === "1") {
                        $js_array[] = 'var hide_emails = true;';
                        $replacement_pattern = "<tagType class=\"tracking_content covered_email covered_content {old_class}\" href=\"#\" data-email=\"' + match[0].replace(/\s/g, '')  + '\">" . htmlentities($cover_text) . "</tagType>";
                    } else {
                        $js_array[] = 'var hide_emails = false;';
                        $replacement_pattern = "<tagType class=\"tracking_content covered_email {old_class}\" data-email=\"' + match[0].replace(/\s/g, '')  + '\" href=\"mailto:' + match[0].replace(/\s/g, '')  + '\">...</tagType>";
                    }

                    $js_array[] = "
                            var tags = ['a', 'span', 'li', 'p'];
                            tags.forEach(function(tag) {
                                jQuery(tag).each(function() {
    
                                  var class_to_attach = '';  
                                  if(jQuery(this).prop('tagName') == 'A' && typeof(jQuery(this).attr('class')) !== 'undefined') {
                                    class_to_attach = jQuery(this).attr('class');
                                  }
           
                                  var sourcestring = jQuery(this).text();
                                  if(email_regex.test(sourcestring))
                                  {
                                    var match = sourcestring.match(email_regex);
                                    var block = jQuery(this);
                                    match.forEach(function(email) {
                                        var replacementpattern = '" . $replacement_pattern . "';
                                        replacementpattern = replacementpattern.replace(/{old_class}/g, class_to_attach);
                                        if(block.prop('tagName') == 'A') replacementpattern = replacementpattern.replace(/tagType/g, 'span');
                                        else replacementpattern = replacementpattern.replace(/tagType/g, 'a');
                                        block.html(block.html().replace(jQuery.trim(match), replacementpattern));
                                    });
                                  }
                                });
                            });
                                                       
                            jQuery('.covered_email').each(function() {
                                if(!jQuery(this).hasClass('covered_content')) jQuery(this).text(jQuery(this).data('email'));
                            });
                            
                            jQuery('[href]').not('.tracking_content').filter(function() {
                                return !jQuery(this).find('.tracking_content').length && email_regex.test(jQuery(this).attr('href'));
                            }).each(function() {
                                var email = jQuery(this).attr('href').match(email_regex);
                                jQuery(this).addClass('tracking_content covered_email').data('email', email[0]);
                            });
                        ";

                    $js_array[] = "jQuery('body').on('click', '.covered_email', function(e) {
                            e.preventDefault();

                            var email = jQuery(this).data('email');
                            
                            var tracking_info = (typeof(email_tracking_info[email]) !== 'undefined' ? email_tracking_info[email] : email_tracking_info);
                            
                            if(tracking_info['trackingsuite']['event_value'] === '') {
                                tracking_info['trackingsuite']['event_value'] = '{email}';
                            }
                            
                            msInternalTrack(tracking_info, {email: email});
                            
                            if(jQuery(this).hasClass('covered_content'))
                            {
                                jQuery(this).attr('href', 'mailto:' + jQuery(this).data('email'));
                                jQuery(this).text(jQuery(this).data('email'));
                            }
                            
                            jQuery(this).removeClass('covered_content').removeClass('covered_email').addClass('uncovered_content');
                 
                        });";

                    return $js_array;

                }
            ),
            "click" => array(
                "id" => "click",
                "name" => "Click",
                "descr" => "Traccia un click su un pulsante, link o qualsiasi altro elemento della pagina.",
                "fa-icon" => "fa-mouse-pointer",
                "default_event" => "",
                "js" => function($action_values) {
                    Global $MSFrameworkCMS;

                    $js_array = array();

                    foreach($action_values['clicks'] as $click) {

                        $selettore = '';

                        if($click['metodo_selezione'] == 'selettore') {
                            $selettore = $click['selettore'];
                        } else if($click['metodo_selezione'] == 'classe') {
                            $selettore = ($click['selettore'][0] !== '.' ? '.' : '') . $click['selettore'];
                        } else if($click['metodo_selezione'] == 'id') {
                            $selettore = ($click['selettore'][0] !== '#' ? '#' : '') . $click['selettore'];
                        } else if($click['metodo_selezione'] == 'testo') {
                            $selettore = $click['selettore'];
                        } else if($click['metodo_selezione'] == 'link') {
                            $selettore = '[href*="' . trim(htmlentities(str_replace($MSFrameworkCMS->getURLToSite(), '', $click['selettore'])), '/') . '"]';
                        }

                        $selettore = str_replace("'", "\\'", $selettore);

                        $js_array[] = 'try {';

                        if($click['metodo_selezione'] == 'testo') {
                            $js_array[] = 'var $element_to_click = jQuery(":contains(\'' . str_replace("\'", "\\\'", htmlentities($selettore)) . '\')").filter(function() {
                                return jQuery(this).text() === \'' . $selettore . '\';
                            });';
                        } else {
                            $js_array[] = 'var $element_to_click = jQuery(\'' . $selettore . '\');';
                        }

                        $js_array[] = '$element_to_click.one("click", function() {
                            msInternalTrack(' .json_encode($click) . ');
                        });';

                        $js_array[] = '} catch(e) {}';

                    }

                    return $js_array;

                }
            ),
            "referer" => array(
                "id" => "referer",
                "name" => "Referer",
                "descr" => "Traccia i siti di provenienza degli utenti",
                "fa-icon" => "fa-retweet",
                "default_event" => "Referer",
                "js" => function($action_values) {

                    // Se il referente è già stato tracciato evito di tracciarlo nuovamente
                    if(isset($_SESSION['trackingSuite_trackedReferrers'])) {
                        //return array();
                    }

                    $_SESSION['trackingSuite_trackedReferrers'] = true;

                    $js_array = array();

                    $js_array[] = "var current_clean_domain = (document.location.origin.replace('www.', '').replace('://', '').replace('https', '').replace('http', '')).split('/')[0]";
                    $js_array[] = "var current_clean_referer = (document.referrer.replace('www.', '').replace('://', '').replace('https', '').replace('http', '')).split('/')[0]";

                    $referer_tracking_details = array();
                    if($action_values['referer_da_tracciare'] === "all") {
                        $js_array[] = 'var track_referer = (current_clean_referer.length && current_clean_referer !== current_clean_domain);';
                        $referer_tracking_details = $action_values;
                    } else {
                        $js_referer_to_track = array();
                        foreach($action_values['custom_referer_url'] as $k => $referer_url) {
                            $js_referer_to_track[] = $referer_url['referer'];
                            $referer_tracking_details[$referer_url['referer']] = $referer_url;
                        }

                        $js_array[] = 'var referer_to_track = ' . json_encode($js_referer_to_track) . ';';
                        $js_array[] = 'var track_referer = (referer_to_track.indexOf(current_clean_referer) >= 0);';
                    }

                    $js_array[] = 'window.referer_tracking_info = ' . json_encode($referer_tracking_details) . ';';

                    $js_array[] = "if(track_referer) {";
                    $js_array[] = "if(typeof(referer_tracking_info[current_clean_referer]) !== 'undefined') {";
                    $js_array[] = "referer_tracking_info = referer_tracking_info[current_clean_referer];";
                    $js_array[] = "}";
                    $js_array[] = "msInternalTrack(referer_tracking_info, {domain: current_clean_referer, url: document.referrer});";
                    $js_array[] = "}";

                    return $js_array;

                }
            ),
            "durata_sessione" => array(
                "id" => "durata_sessione",
                "name" => "Durata Sessione",
                "descr" => "Traccia gli utenti che navigano per più di un certo numero di tempo",
                "fa-icon" => "fa-clock-o",
                "default_event" => "",
                "js" => function($action_values) {

                    if (!isset($_SESSION['session_start_time'])) {
                        $_SESSION['session_start_time'] = $_SERVER['REQUEST_TIME'];
                    }

                    $js_array = array();

                    $js_array[] = 'window.session_start_time = ' . $_SESSION['session_start_time'] . ';';

                    foreach($action_values['sessioni'] as $session_k => $sessione) {

                        if($sessione['unita_durata'] == 'm') {
                            $sessione['durata_minima'] = $sessione['durata_minima']*60;
                        }

                        $js_array[] = 'if(!sessionStorage.getItem("session_' . $session_k . '_passed")) {';

                        $js_array[] = 'window.session_' . $session_k . '_interval = setInterval(function() {';

                        $js_array[] = 'if(!sessionStorage.getItem("session_' . $session_k . '_passed")) {';

                        $js_array[] = 'if(Math.floor(Date.now() / 1000)-window.session_start_time > ' . (float)$sessione['durata_minima'] . ') {';
                        $js_array[] = 'sessionStorage.setItem("session_' . $session_k . '_passed", 1);';
                        $js_array[] = 'clearInterval(window.session_' . $session_k . '_interval);';
                        $js_array[] = 'msInternalTrack(' .json_encode($sessione) . ');';
                        $js_array[] = '}';

                        $js_array[] = '}';

                        $js_array[] = '}, 5000);';

                        $js_array[] = '}';
                    }

                    return $js_array;

                }
            ),
            "visita_pagina" => array(
                "id" => "visita_pagina",
                "name" => "Visualizzazione Contenuto",
                "descr" => "Traccia gli utenti che visitano una determinata pagina o un contenuto del sito",
                "fa-icon" => "fa-eye",
                "default_event" => "PageVisit",
                "manual_tracking" => array(
                    "description" => "Per tracciare la visualizzazione di un contenuto richiama la seguente funzione.
                    <br>
                    Insieme alla funzione è possibile passare dei parametri aggiuntivi."
                ),
                "js" => function($action_values) {
                    Global $MSFrameworkCMS;

                    $js_array = array();

                    $current_clean_url = rtrim($MSFrameworkCMS->getCleanHost($MSFrameworkCMS->getCleanHost($_SERVER['HTTP_REFERER'])), '/');

                    if(!isset($_SESSION['trackingSuite_visited_urls'])) {
                        $_SESSION['trackingSuite_visited_urls'] = array();
                    }

                    // Se questo link è già stato visitato non stampo il codice di tracciamento
                    if(in_array($current_clean_url,  $_SESSION['trackingSuite_visited_urls'])) {
                        //return array();
                    }

                    $_SESSION['trackingSuite_visited_urls'][] = $current_clean_url;

                    $shortcodes = array('link' => htmlentities($current_clean_url));
                    $js_array[] = "var shortcodes = " . json_encode($shortcodes) . ";";

                    foreach($action_values['url'] as $k => $url_details) {

                        $js_array[] = "var url_details = " . json_encode($url_details) . ";";

                        // Ottengo il nome del prodotto
                        $js_array[] = "shortcodes['title'] = '';";
                        if(!empty($url_details['match_title'])) {
                            $js_array[] = "var nearest_name = trackingSuite_getNearestElement(jQuery('" . $this->prepareStringForJSParam($url_details['match_title']) . "')[0], '" . $this->prepareStringForJSParam($url_details['match_title']) . "');";
                            $js_array[] = "shortcodes['title'] = (nearest_name !== false ? nearest_name.text() : '');";
                        }

                        $js_array[] = "shortcodes['product_id'] = '';";
                        if(!empty($url_details['match_product_id'])) {
                            $js_array[] = "var nearest_id = trackingSuite_getNearestElement(jQuery('" . $this->prepareStringForJSParam($url_details['match_product_id']) . "')[0], '" . $this->prepareStringForJSParam($url_details['match_product_id']) . "');";
                            $js_array[] = "shortcodes['product_id'] = (nearest_id !== false ? (nearest_id.text().match(/\d+/) ? nearest_id.text().match(/\d+/)[0] : '') : '');";
                        }

                        // Ottengo il prezzo del prodotto
                        $js_array[] = "shortcodes['value'] = '';";
                        $js_array[] = "shortcodes['currency'] = '';";
                        if(!empty($url_details['match_value'])) {
                            $js_array[] = "var nearest_price = trackingSuite_getNearestElement(jQuery('" . $this->prepareStringForJSParam($url_details['match_value']) . "')[0], '" . $this->prepareStringForJSParam($url_details['match_value']) . "');";

                            $js_array[] = "shortcodes['value'] = (nearest_price !== false ? nearest_price.text() : '');";

                            $js_array[] = "if(shortcodes['value'] !== '') {";
                            $js_array[] = "shortcodes['currency'] = trackingSuite_extractCurrencyFromString(shortcodes['value']);";
                            $js_array[] = "shortcodes['value'] = trackingSuite_extractPriceFromString(shortcodes['value']);";
                            $js_array[] = "}";
                        }

                        if($url_details['metodo_selezione'] == 'link' || !$url_details['metodo_selezione']) {
                            foreach(explode(',', $url_details['link']) as $url) {
                                if (stristr($current_clean_url, rtrim($MSFrameworkCMS->getCleanHost($url), '/'))) {
                                    $js_array[] = 'msInternalTrack(url_details, shortcodes);';
                                }
                            }
                        } else {
                            $js_array[] = 'if((document.documentElement.textContent || document.documentElement.innerText).indexOf("' . str_replace('"', '\"', $url_details['text']) . '") > -1) {';
                            $js_array[] = 'msInternalTrack(url_details, shortcodes);';
                            $js_array[] = '}';
                        }
                    }

                    return $js_array;
                }
            ),
            "invio_form" => array(
                "id" => "invio_form",
                "name" => "Invio Form",
                "descr" => "Traccia gli utenti che effettuano l'invio di un Form",
                "fa-icon" => "fa-book",
                "default_event" => "FormSubmit",
                "js" => function($action_values) {
                    $js_array = array();

                    foreach($action_values['forms'] as $form) {
                        if($form['form_selezione'] == 'custom') $selettore = $form['selettore'];
                        else $selettore = '.MSForm[data-form_id="' . $form['form_selezione'] . '"]';

                        $js_array[] = 'try {';
                        $js_array[] = 'trackingSuite_prependEvent("onsubmit", "' . addslashes($selettore) . '", function(element) {
                            var form_params = {\'site-link\': document.location.href};
                            jQuery(this).find("[name]").each(function() {
                                var input_value = "";
                                if(typeof(jQuery(this).attr("type") !== "undefined") && jQuery(this).attr("type") === "checkbox") {
                                    input_value = (jQuery(this).prop("checked") ? 1 : 0);
                                } else {
                                    input_value = jQuery(this).val();
                                }
                                form_params[jQuery(this).attr("name")] = input_value;
                            });
                            msInternalTrack(' .json_encode($form) . ', "trackingSuite_getFormParams(this)");
                        });';

                        $js_array[] = '} catch(e) {}';
                    }

                    return $js_array;
                }
            ),
            "ricerca" => array(
                "id" => "ricerca",
                "name" => "Ricerca",
                "descr" => "Traccia gli utenti che effettuano una ricerca",
                "fa-icon" => "fa-search",
                "default_event" => "Search",
                "js" => function($action_values) {
                    $js_array = array();

                    foreach($action_values['forms'] as $form) {
                        if($form['form_selezione'] == 'custom') $selettore = $form['selettore'];
                        else $selettore = '.MSForm[data-form_id="' . $form['form_selezione'] . '"]';

                        $js_array[] = 'try {';
                        $js_array[] = 'trackingSuite_prependEvent("onsubmit", "' . addslashes($selettore) . '", function(element) {
                            var form_params = {\'site-link\': document.location.href};
                            jQuery(this).find("[name]").each(function() {
                                var input_value = "";
                                if(typeof(jQuery(this).attr("type") !== "undefined") && jQuery(this).attr("type") === "checkbox") {
                                    input_value = (jQuery(this).prop("checked") ? 1 : 0);
                                } else {
                                    input_value = jQuery(this).val();
                                }
                                form_params[jQuery(this).attr("name")] = input_value;
                            });
                            msInternalTrack(' .json_encode($form) . ', "trackingSuite_getFormParams(this)");
                        });';

                        $js_array[] = '} catch(e) {}';
                    }

                    return $js_array;
                }
            ),
            "carrello_ecommerce" => array(
                "id" => "carrello_ecommerce",
                "name" => "Aggiunto al Carrello",
                "descr" => "Traccia gli utenti che aggiungono dei prodotti al carrello",
                "fa-icon" => "fa-shopping-cart",
                "default_event" => "AddToCart",
                "manual_tracking" => array(
                    "description" => "Per tracciare l'aggiunta al carrello di un prodotto richiama la seguente funzione nel momento in cui il cliente aggiunge il prodotto al carrello.
                    <br>
                    Insieme alla funzione è possibile passare dei parametri aggiuntivi."
                ),
                "js" => function($action_values) {

                    $js_array = array();
                    $js_array[] = "var carrello_tracking_info = {};";

                    foreach($action_values["carrelli"] as $k => $carrello) {

                        $js_array[] = "carrello_tracking_info[$k] = " . json_encode($carrello) . ";";

                        if($carrello["carrello_selezione"] == 'custom') {
                            $js_array[] = "jQuery('body').on('click', '" . $carrello["add_to_cart_button"] . "', function(e) {";

                            // Ottengo il nome del prodotto
                            if(empty($carrello['add_to_cart_title'])) {
                                $js_array[] = "var product_name = '';";
                            } else {
                                $js_array[] = "var nearest_name = trackingSuite_getNearestElement(jQuery(this)[0], '" . $this->prepareStringForJSParam($carrello['add_to_cart_title']) . "');";
                                $js_array[] = "var product_name = (nearest_name !== false ? nearest_name.text() : '');";
                            }

                            // Ottengo la categoria del prodotto
                            if(empty($carrello['add_to_cart_category'])) {
                                $js_array[] = "var product_category = '';";
                            } else {
                                $js_array[] = "var nearest_category = trackingSuite_getNearestElement(jQuery(this)[0], '" . $this->prepareStringForJSParam($carrello['add_to_cart_category']) . "');";
                                $js_array[] = "var product_category = (nearest_category !== false ? jQuery.trim(nearest_category.text()) : '');";
                            }

                            // Ottengo l'id del prodotto
                            if(empty($carrello['add_to_cart_product_id'])) {
                                $js_array[] = "var product_id = '';";
                            } else {
                                $js_array[] = "var nearest_id = trackingSuite_getNearestElement(jQuery(this)[0], '" . $this->prepareStringForJSParam($carrello['add_to_cart_product_id']) . "');";
                                $js_array[] = "var product_id = (nearest_id !== false ? (nearest_id.text().match(/\d+/) ? nearest_id.text().match(/\d+/)[0] : '') : '');";
                            }

                            // Ottengo il prezzo del prodotto
                            if(empty($carrello['add_to_cart_price'])) {
                                $js_array[] = "var product_price = '';";
                                $js_array[] = "var product_currency = '';";
                            } else {
                                $js_array[] = "var nearest_price = trackingSuite_getNearestElement(jQuery(this)[0], '" . $this->prepareStringForJSParam($carrello['add_to_cart_price']) . "');";

                                $js_array[] = "var product_price = (nearest_price !== false ? nearest_price.text() : '');";
                                $js_array[] = "var product_currency = '';";

                                $js_array[] = "if(product_price !== '') {";
                                $js_array[] = "product_currency = trackingSuite_extractCurrencyFromString(product_price);";
                                $js_array[] = "product_price = trackingSuite_extractPriceFromString(product_price);";
                                $js_array[] = "}";
                            }

                            $js_array[] = "msInternalTrack(carrello_tracking_info[$k], {price: product_price, currency: product_currency, title: product_name, category: product_category, product_id: product_id});";

                            $js_array[] = "});";
                        } else if($carrello["carrello_selezione"] == 'auto') {
                            // Cerco di intercettare le funzioni ajax del FW dell'aggiunta al carrello
                            $js_array[] = 'jQuery( document ).ajaxSuccess(function( event, xhr, settings ) {
                                
                                var trackCart = false;
                                try {
                                    trackCart = settings.url.indexOf("addToCart") > 0 || settings.data.indexOf("addToCart") > 0;
                                } catch(e) {}
                            
                                if(trackCart) {           
                                    var request_data = JSON.parse(\'{"\' + settings.data.replace(/&/g, \'","\').replace(/=/g,\'":"\') + \'"}\', function(key, value) { return key===""?value:decodeURIComponent(value) });
                                    var product_id = request_data.id;
                                    var quantity = request_data.quantity;
                                    var response = xhr.responseJSON;
                                    if(typeof(response.status) === "undefined" || response.status !== "no_avaialable")
                                    {
                                        var cart = response.products;
                                        var product_detail = {};
                                        Object.keys(cart).forEach(function(k) {
                                            if(cart[k].product_id == product_id) {
                                                product_detail = cart[k];
                                            }
                                        });
                                        
                                        if(carrello_tracking_info[' . $k . '][\'trackingsuite\'][\'event_value\'] === \'\') {
                                            carrello_tracking_info[' . $k . '][\'trackingsuite\'][\'event_value\'] = product_detail.nome;
                                        }
                                        msInternalTrack(carrello_tracking_info[' . $k . '], {price: product_detail.prezzo.tax[0] + "." + product_detail.prezzo.tax[1], currency: "EUR", title: product_detail.nome, category: product_detail.category_name, product_id: product_detail.product_id});
                                    }
                                }
                            });';
                        } else if($carrello["carrello_selezione"] == 'track_manually') {
                            // Aggiungo l'azione nella lista di azioni traccibili manualmente
                            $js_array[] = 'window.trackingsuite_manual_actions[carrello_tracking_info[' . $k . '][\'trackingsuite\'][\'event_name\']] = carrello_tracking_info[' . $k . ']';
                        }

                    }
                    return $js_array;

                }
            ),
            "acquisto" => array(
                "id" => "acquisto",
                "name" => "Acquisto",
                "descr" => "Traccia gli utenti che acquistano un bene o un servizio",
                "fa-icon" => "fa-credit-card-alt",
                "default_event" => "Purchase",
                "manual_tracking" => array(
                    "description" => "Per tracciare un acquisto richiama la seguente funzione nel momento in cui il cliente paga l'ordine (Ad esempio nella Thank You page).
                    <br>
                    Insieme alla funzione è possibile passare dei parametri aggiuntivi."
                ),
                "js" => function($carrello) {

                    $js_array = array();
                    $js_array[] = "var acquisto_tracking_info = " . json_encode($carrello) . ";";

                    if($carrello["carrello_selezione"] == 'auto') {
                        // Nulla, verrà tracciato automaticamente una volta creato l'ordine.

                    } else if($carrello["carrello_selezione"] == 'track_manually') {
                        // Aggiungo l'azione nella lista di azioni traccibili manualmente
                        $js_array[] = 'acquisto_tracking_info["execAction"] = \'createOperation\'';
                        $js_array[] = 'window.trackingsuite_manual_actions[acquisto_tracking_info[\'trackingsuite\'][\'event_name\']] = acquisto_tracking_info';
                    }

                    return $js_array;

                }
            ),
        );

        foreach($actions_checks as $k => $a) {
            if($a['default_event']) {
                $actions_checks[$k]['params'] = $this->getStandardTrackingEvents($a['default_event']);
            } else {
                $actions_checks[$k]['params'] = $this->getStandardTrackingEvents("Standard");
            }
        }

        return $actions_checks;
    }

    /**
     * Ritorna un array con tutte le info dei goal disponibili
     *
     * @return array
     *
     */
    public function getTrackingParams() {

        $tracking_params = array(
            'trackingsuite' => array(
                'color' => '#3C5A99',
                'params' => array(
                    'event_name' => array('name' => 'Evento', 'required' => true, 'translate' => false, 'primary' => true),
                    'event_value' => array('name' => 'Valore Evento', 'required' => false, 'translate' => false, 'use_extra_params' => true)
                )
            ),
            'analytics' => array(
                'name' => "Google Analytics",
                'icon' => 'fa-google',
                'color' => '#ED750A',
                'params' => array(
                    'event_category' => array('name' => 'Categoria', 'required' => false, 'translate' => true),
                    'event_action' => array('name' => 'Azione', 'required' => false, 'translate' => true),
                    'event_label' => array('name' => 'Etichetta', 'required' => false, 'translate' => true, 'use_extra_params' => true),
                    'value' => array('name' => 'Valore', 'required' => false, 'type' => 'number', 'translate' => false)
                )
            ),
            'gads' => array(
                'name' => "Google Ads",
                'icon' => 'fa-google',
                'color' => '#13a365',
                'params' => array(
                    'conversion_label' => array('name' => 'Etichetta Conversione', 'required' => false, 'translate' => false),
                    'value' => array('name' => 'Valore', 'required' => false, 'type' => 'number', 'translate' => false)
                )
            ),
            'linkedin_insight' => array(
                'name' => "LinkedIn Insight",
                'icon' => 'fa-linkedin',
                'color' => '#0073b1',
                'params' => array(
                    'conversion_id' => array('name' => 'ID Conversione', 'required' => false, 'translate' => false)
                )
            ),
            'facebook' => array(
                'name' => "Facebook Pixel",
                'icon' => 'fa-facebook',
                'color' => '#3C5A99',
                'type' => 'standard',
                'params' => array(
                    'label' => array('name' => 'Evento', 'required' => false, 'translate' => true, 'primary' => true)
                )
            )
        );

        return $tracking_params;

    }

    /**
     * Ottiene tutti gli eventi standard tracciabili e le relative informazioni
     * Es: i parametri extra necessari, i parametri da passare sulle piattaforme esterne etc...
     *
     * @param $id string L'id dell'azione da ottenere
     *
     * @return array
     *
     */
    public function getStandardTrackingEvents($id = "") {

        $standard_params = array(
            "Standard" => array(
                'name' => "Contattato via Email",
                'extra_params' => array(),
                'platforms' => array(
                    "trackingsuite" => array(
                        "inputs" => array(),
                        'extra_params' => array()
                    ),
                    "facebook" => array(
                        "inputs" => array(
                            'label' =>  array(
                                'value' => '',
                                'disabled' => false
                            )
                        ),
                        'extra_params' => array(
                            'currency' => array(
                                'type' => 'string',
                                'description' => 'La valuta utilizzata dal cliente',
                                'required' => false,
                                'default' => '',
                                'match' => false
                            ),
                            'value' => array(
                                'type' => 'float',
                                'description' => 'Il valore del prodotto',
                                'required' => false,
                                'default' => '',
                                'match' => false
                            )
                        )
                    ),
                    "analytics" => array(
                        "inputs" => array(),
                        'extra_params' => array()
                    )
                )
            ),
            /* EVENTI DI CONTATTO */
            "EmailContact" => array(
                'name' => "Contattato via Email",
                'extra_params' => array(
                    'email' => array(
                        'type' => 'string',
                        'description' => 'L\'indirizzo email cliccato',
                        'required' => true,
                        'default' => ''
                    )
                ),
                'platforms' => array(
                    "trackingsuite" => array(
                        "inputs" => array(
                            'event_name' => array(
                                'value' => 'Email Ricevuta',
                                'disabled' => false
                            ),
                            'event_value' => array(
                                'value' => '{email}',
                                'placeholder' => 'Se vuoto indicherà l\'indirizzo email'
                            )
                        ),
                    ),
                    "facebook" => array(
                        "inputs" => array(
                            'label' =>  array(
                                'value' => 'Contact',
                                'disabled' => false
                            )
                        ),
                        'extra_params' => array()
                    ),
                    "analytics" => array(
                        "inputs" => array(
                            'label' => array(
                                'value' => 'Contact',
                                'disabled' => false
                            )
                        ),
                    )
                )
            ),
            "PhoneCall" => array(
                'name' => "Contattato via Telefono",
                'extra_params' => array(
                    'telefono' => array(
                        'type' => 'string',
                        'description' => 'Il numero di cellulare chiamato',
                        'required' => true,
                        'default' => ''
                    )
                ),
                'platforms' => array(
                    "trackingsuite" => array(
                        "inputs" => array(
                            'event_name' => array(
                                'value' => 'Chiamata Ricevuta',
                                'disabled' => false
                            ),
                            'event_value' => array(
                                'value' => '{telefono}',
                                'placeholder' => 'Se vuoto indicherà il numero di telefono'
                            )
                        ),
                    ),
                    "facebook" => array(
                        "inputs" => array(
                            'label' =>  array(
                                'value' => 'Contact',
                                'disabled' => false
                            )
                        ),
                        'extra_params' => array()
                    ),
                    "analytics" => array(
                        "inputs" => array(
                            'label' => array(
                                'value' => 'Contact',
                                'disabled' => false
                            )
                        ),
                    )
                )
            ),
            "PageVisit" => array(
                'name' => "Pagina Visitata",
                'extra_params' => array(
                    'link' => array(
                        'type' => 'string',
                        'description' => 'Il link visitato dal cliente',
                        'required' => true,
                        'default' => '{link}'
                    ),
                    'value' => array(
                        'type' => 'float',
                        'description' => 'Il valore dell\'oggetto visualizzato',
                        'required' => false,
                        'default' => '0.0'
                    ),
                    'currency' => array(
                        'type' => 'string',
                        'description' => 'Il codice della valuta in uso (USD, EUR ...)',
                        'required' => false,
                        'default' => ''
                    ),
                    'title' => array(
                        'type' => 'string',
                        'description' => 'Il titolo dell\'oggetto visualizzato',
                        'required' => false
                    ),
                    'product_id' => array(
                        'type' => 'string',
                        'description' => 'L\'ID del prodotto, o un array contenente più id (Es: [\'1\',\'2\'])',
                        'required' => false
                    )
                ),
                'platforms' => array(
                    "trackingsuite" => array(
                        "inputs" => array(
                            'event_name' => array(
                                'value' => 'Link Visitato',
                                'disabled' => false
                            ),
                            'event_value' => array(
                                'value' => '{link}',
                                'placeholder' => ''
                            )
                        ),
                    ),
                    "facebook" => array(
                        "inputs" => array(
                            'label' =>  array(
                                'value' => 'ViewContent',
                                'disabled' => true
                            )
                        ),
                        'extra_params' =>  array(
                            'content_name' => array(
                                'type' => 'string',
                                'description' => 'Il nome del prodotto visualizzato',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            ),
                            'content_ids' => array(
                                'type' => 'string',
                                'description' => 'L\'ID del prodotto, o un array contenente più id (Es: [\'1\',\'2\'])',
                                'required' => false,
                                'default' => '',
                                'match' => 'product_id'
                            ),
                            'content_type' => array(
                                'type' => 'string',
                                'description' => 'La tipologia del prodotto passato (Lascia \'product\' per il corretto funzionamento della ads dinamiche)',
                                'required' => false,
                                'default' => 'product',
                                'match' => ''
                            ),
                            'currency' => array(
                                'type' => 'string',
                                'description' => 'La valuta utilizzata',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            ),
                            'value' => array(
                                'type' => 'float',
                                'description' => 'Il valore dell\'azione effettuata',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            )
                        )
                    ),
                    "analytics" => array(
                        "inputs" => array(
                            'label' => array(
                                'value' => 'Contact',
                                'disabled' => false
                            )
                        ),
                    )
                )
            ),
            "Referer" => array(
                "name" => "Referente",
                'extra_params' => array(
                    'url' => array(
                        'type' => 'string',
                        'description' => 'L\'URL referente',
                        'required' => false,
                        'default' => ''
                    ),
                    'domain' => array(
                        'type' => 'string',
                        'description' => 'Il dominio referente',
                        'required' => false,
                        'default' => ''
                    ),
                ),
                'platforms' => array(
                    "trackingsuite" => array(
                        "inputs" => array(
                            'event_name' => array(
                                'value' => 'Referer',
                                'disabled' => false
                            ),
                            'event_value' => array(
                                'value' => '{domain}',
                                'placeholder' => ''
                            )
                        )
                    ),
                    "facebook" => array(),
                    "analytics" => array()
                )
            ),
            "Search" => array(
                "name" => "Ricerca",
                'extra_params' => array(
                    'page-link' => array(
                        'type' => 'string',
                        'description' => 'L\'URL da cui il cliente ha inviato la ricerca',
                        'required' => false,
                        'default' => ''
                    )
                ),
                'platforms' => array(
                    "trackingsuite" => array(
                        "inputs" => array(
                            'event_name' =>  array(
                                'value' => 'Ricerca',
                                'disabled' => false
                            )
                        )
                    ),
                    "facebook" => array(
                        "inputs" => array(
                            'label' =>  array(
                                'value' => 'Search',
                                'disabled' => true
                            )
                        ),
                        'extra_params' => array(
                            'search_string' => array(
                                'type' => 'string',
                                'description' => 'La stringa ricercata dall\'utente',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            ),
                            'value' => array(
                                'type' => 'float',
                                'description' => 'Il valore dell\'azione effettuata',
                                'required' => false,
                                'default' => '0',
                                'match' => ''
                            )
                        )
                    ),
                    "analytics" => array()
                )
            ),
            "FormSubmit" => array(
                "name" => "Invio Form",
                'extra_params' => array(
                    'site-link' => array(
                        'type' => 'string',
                        'description' => 'L\'URL da cui il cliente ha inviato il form',
                        'required' => false,
                        'default' => ''
                    )
                ),
                'platforms' => array(
                    "trackingsuite" => array(),
                    "facebook" => array(),
                    "analytics" => array()
                )
            ),
            /* EVENTI ECOMMERCE */
            "AddToCart" => array(
                "name" => "Aggiungi al carrello",
                'extra_params' => array(
                    'price' => array(
                        'type' => 'float',
                        'description' => 'Il prezzo del prodotto',
                        'required' => true,
                        'default' => '0.0'
                    ),
                    'currency' => array(
                        'type' => 'string',
                        'description' => 'Il codice della valuta in uso (USD, EUR ...)',
                        'required' => true,
                        'default' => ''
                    ),
                    'title' => array(
                        'type' => 'string',
                        'description' => 'Il titolo del prodotto',
                        'required' => false
                    ),
                    'category' => array(
                        'type' => 'string',
                        'description' => 'La categoria del prodotto',
                        'required' => false
                    ),
                    'product_id' => array(
                        'type' => 'string',
                        'description' => 'L\'ID del prodotto, o un array contenente più id (Es: [\'1\',\'2\'])',
                        'required' => false
                    )
                ),
                'platforms' => array(
                    "trackingsuite" => array(
                        "inputs" => array(
                            'event_name' => array(
                                'value' => 'Aggiungi al carrello',
                                'disabled' => false
                            ),
                            'event_value' => array(
                                'value' => '',
                                'placeholder' => ''
                            )
                        )
                    ),
                    "facebook" => array(
                        "inputs" => array(
                            'label' =>  array(
                                'value' => 'AddToCart',
                                'disabled' => true
                            )
                        ),
                        'extra_params' => array(
                            'content_name' => array(
                                'type' => 'string',
                                'description' => 'Il nome del prodotto aggiunto al carrello',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            ),
                            'content_ids' => array(
                                'type' => 'string',
                                'description' => 'L\'ID del prodotto, o un array contenente più id (Es: [\'1\',\'2\'])',
                                'required' => false,
                                'default' => '',
                                'match' => 'product_id'
                            ),
                            'content_type' => array(
                                'type' => 'string',
                                'description' => 'La tipologia del prodotto passato (Lascia \'product\' per il corretto funzionamento della ads dinamiche)',
                                'required' => false,
                                'default' => 'product',
                                'match' => ''
                            ),
                            'currency' => array(
                                'type' => 'string',
                                'description' => 'La valuta utilizzata dal cliente',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            ),
                            'value' => array(
                                'type' => 'float',
                                'description' => 'Il valore del prodotto',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            )
                        )
                    ),
                    "analytics" => array()
                )
            ),
            "Purchase" => array(
                "name" => "Acquisto",
                'extra_params' => array(
                    'price' => array(
                        'type' => 'float',
                        'description' => 'Il prezzo del prodotto',
                        'required' => true,
                        'default' => '0.0'
                    ),
                    'currency' => array(
                        'type' => 'string',
                        'description' => 'Il codice della valuta in uso (USD, EUR ...)',
                        'required' => true,
                        'default' => ''
                    ),
                    'title' => array(
                        'type' => 'string',
                        'description' => 'Il titolo del prodotto',
                        'required' => false
                    ),
                    'category' => array(
                        'type' => 'string',
                        'description' => 'La categoria del prodotto',
                        'required' => false
                    ),
                    'product_id' => array(
                        'type' => 'string',
                        'description' => 'L\'ID del prodotto, o un array contenente più id (Es: [\'1\',\'2\'])',
                        'required' => false
                    )
                ),
                'platforms' => array(
                    "trackingsuite" => array(
                        "inputs" => array(
                            'event_name' => array(
                                'value' => 'Acquisto',
                                'disabled' => false
                            ),
                            'event_value' => array(
                                'value' => '',
                                'placeholder' => ''
                            )
                        )
                    ),
                    "facebook" => array(
                        "inputs" => array(
                            'label' =>  array(
                                'value' => 'Purchase',
                                'disabled' => true
                            )
                        ),
                        'extra_params' => array(
                            'content_name' => array(
                                'type' => 'string',
                                'description' => 'Il nome del prodotto acquistato',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            ),
                            'content_category' => array(
                                'type' => 'string',
                                'description' => 'Categoria del prodotto',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            ),
                            'content_ids' => array(
                                'type' => 'string',
                                'description' => 'L\'ID del prodotto, o un array contenente più id (Es: [\'1\',\'2\'])',
                                'required' => false,
                                'default' => '',
                                'match' => 'product_id'
                            ),
                            'content_type' => array(
                                'type' => 'string',
                                'description' => 'La tipologia del prodotto passato (Lascia \'product\' per il corretto funzionamento della ads dinamiche)',
                                'required' => false,
                                'default' => 'product',
                                'match' => ''
                            ),
                            'currency' => array(
                                'type' => 'string',
                                'description' => 'La valuta utilizzata dal cliente',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            ),
                            'value' => array(
                                'type' => 'float',
                                'description' => 'Il valore del prodotto',
                                'required' => false,
                                'default' => '',
                                'match' => ''
                            )
                        )
                    ),
                    "analytics" => array()
                )
            )
        );

        foreach($standard_params as $k => $p) {

            if($k !== 'Standard') {
                foreach($standard_params[$k]['platforms'] as $platform => $platform_values) {
                    if(!isset($platform_values['extra_params'])) $standard_params[$k]['platforms'][$platform]['extra_params'] = array();
                    $standard_params[$k]['platforms'][$platform]['extra_params'] = array_merge($standard_params['Standard']['platforms'][$platform]['extra_params'], $standard_params[$k]['platforms'][$platform]['extra_params']);
                }
            }

            if(isset($p['alternative_names'])) {
                foreach($p['alternative_names'] as $aname)  {
                    $standard_params[$aname] = $standard_params[$k];
                }
            }
        }

        if($id !== "") {
            if(isset($standard_params[$id])) return $standard_params[$id];
            else return array();
        }

        return $standard_params;

    }

    /**
     * Ottiene la lsita delle funzioni di tracking avanzato con relativa documentazione
     *
     * @return array
     *
     */
    public function getAdvancedTrackingFunctions() {

        $advanced_functions = array(
            /* EVENTI DI CONTATTO */
            "msTrackUser" => array(
                'name' => "Tracking informazioni utente",
                'description' => "Permette di indicare i dati dell'utente loggato.
                                  Utilizzando la funzione <i>msTrackUser</i> ogni evento verrà automaticamente collegato all'utente indicato.",
                'advantages' => "Oltre ad avere un resoconto più dettagliato, diventerà possibile inviare campagne pubblicitarie (Newsletter o SMS)
                                 agli utenti in base a delle azioni effettuate precedentemente.<br><br><b style='color: black;'>NB: I clienti verranno memorizzati nel modulo Clienti.</b>",
                'usage_example' => array(
                    'text' => "Includi la funzione nell'header o nel footer di tutte le pagine del sito web. Puoi far generare i parametri da inserire tramite PHP.",
                    'code' => "msTrackUser('mariorossi@live.it', {name: 'Mario', surname: 'Rossi', phone: '001122334'});"
                ),
                'params' => array(
                    'required' => array(
                        'email' => 'L\'email del cliente'
                    ),
                    'optional' => array(
                        'user_extra' => array(
                            'name' => 'Il nome utente',
                            'surname' => 'Il cognome utente',
                            'phone' => 'Il numero di telefono',
                            'sex' => 'Sesso (M|F)',
                            'address' => 'L\'indirizzo del cliente',
                            'city' => 'La città del cliente',
                            'province' => 'La provincia del cliente',
                            'postal_code' => 'Codice postale',
                        )
                    )
                ),
                'js' => function() {
                    $trackingSuite_userData = $this->getUserData();
                    $js_function = "function msTrackUser(email, user_data) {
                            if(typeof(user_data) !== 'object') {
                                user_data = {};
                            }
                            
                            var previous_email = '" . ($trackingSuite_userData ? htmlentities($trackingSuite_userData['email']) : '') . "';
                            
                            if(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email)) {
                                window.trackingSuite_userEmail = email;
                                window.trackingSuite_userExtra = user_data;
                                window.trackingSuite_needLogin = (previous_email !== email || " . ($trackingSuite_userData ? '0' : '1') . ");
                            }
                        }";
                    return $js_function;
                },
            )
        );

        return $advanced_functions;

    }

    /**
     * Ottiene il codice JS da iniettare sul sito
     *
     * @param $project_info mixed L'ID del progetto o l'array contenente le info del progetto
     * @param $type string Il tipo di codice da ottenere (utilizza l'ID di getTrackingParams())
     *
     * @return bool|string
     */
    public function getJSCodeForTracking($project_info, $type) {

        if(is_array($project_info)) $project_details = $project_info;
        else $project_details = $this->getTrackingProjectDetails($project_info);

        $code = false;
        if($type == "analytics") {
            $code = "var script = document.createElement(\"script\");
            script.type = \"text/javascript\";
            script.src = 'https://www.googletagmanager.com/gtag/js?id=" . $project_details['tracking_codes']['google_analytics_ua'] . "';
            document.body.appendChild(script);window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '" . $project_details['tracking_codes']['google_analytics_ua'] . "', {
                    'anonymize_ip': true,
                    'send_page_view': false
                });
                ";

            if(!empty($project_details['tracking_codes']['gads_id'])) {
                $code .= "gtag('config', '" . $project_details['tracking_codes']['gads_id'] . "');" . PHP_EOL;
            }

            $code .= "
                gtag('event', 'page_view', {
                    'event_callback': function() {
                        try {
                            var trackers = ga.getAll();
                            var analyticsID = '';
                            var i, len;
                            for (i = 0, len = trackers.length; i < len; i += 1) {
                              analyticsID = trackers[i].get('clientId');
                            }
                            if(analyticsID.length) {
                                msSetTrackReferences('ga_clientId', analyticsID);
                            }
                        } catch(e) {}
                    }
                });";
        } else if($type == "gads") {
            if(empty($project_details['tracking_codes']['google_analytics_ua'])) {
                $code = "var script = document.createElement(\"script\");
                script.type = \"text/javascript\";
                script.src = 'https://www.googletagmanager.com/gtag/js?id=" . $project_details['tracking_codes']['gads_id'] . "';
                document.body.appendChild(script);window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '" . $project_details['tracking_codes']['gads_id'] . "');";
            }
        } else if($type == "tagmanager") {
            $code = "(function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '" . $project_details['tracking_codes']['google_tagmanager_gtm'] . "');";
        } else if($type == "albacross") {
            $code = '(function(a,l,b,c,r,s){
            _nQc=c,r=a.createElement(l),s=a.getElementsByTagName(l)[0];
            r.async=1;
            r.src=l.src=("https:"==a.location.protocol?"https://":"http://")+b;
            s.parentNode.insertBefore(r,s);})
            (document,"script","serve.albacross.com/track.js","' . $project_details['tracking_codes']['albacross'] . '");';
        } else if($type == "facebook") {
            $code = "!function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');";

            if($this->getUserData()) {
                $code .= "fbq('init', '" . $project_details['tracking_codes']['facebook_pixel_id'] . "', {
                    em: '" . $this->getUserData()['email'] . "'
                });";

            } else {
                $code .= "fbq('init', '" . $project_details['tracking_codes']['facebook_pixel_id'] . "');";
            }

            $code .= "fbq('track', 'PageView');";
        } else if($type == "linkedin_insight") {
            $code = "_linkedin_partner_id = \"" . $project_details['tracking_codes']['linkedin_insight_tag'] . "\";
            window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
            window._linkedin_data_partner_ids.push(_linkedin_partner_id);";
            $code .= "(function(){var s = document.getElementsByTagName(\"script\")[0];
            var b = document.createElement(\"script\");
            b.type = \"text/javascript\";b.async = true;
            b.src = \"https://snap.licdn.com/li.lms-analytics/insight.min.js\";
            s.parentNode.insertBefore(b, s);})();";
        } else if($type == "yandex_metrica") {
            $code = " (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, \"script\", \"https://mc.yandex.ru/metrika/tag.js\", \"ym\");
        
            ym(" . (int)$project_details['tracking_codes']['yandex_metrica'] . ", \"init\", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true,
                ecommerce:\"dataLayer\"
            });";
        }

        return $code;
    }

    /**
     * Ritorna il codice Javascript contenente tutte le funzionalità di tracking
     *
     * @param $project_info mixed L'ID del progetto o l'array contenente le info del progetto
     *
     * @return string
     *
     */
    public function generateTrackingJavascript($project_info)
    {
        global $MSFrameworkCMS, $MSFrameworki18n, $MSFrameworkModules;

        if (is_array($project_info)) $project_details = $project_info;
        else $project_details = $this->getTrackingProjectDetails($project_info);

        $tracking_actions = $this->getTrackingActionsInfo();

        $js_array = array();

        if (!$project_details) return "
            alert('[TrackingSuite] Il dominio ' + window.location.hostname + ' non è abilitato o non esiste nessun progetto attivo');
            throw new Error('[TrackingSuite] Il dominio ' + window.location.hostname + ' non è abilitato o non esiste nessun progetto attivo');
        ";

        $js_array[] = "if(typeof(window.trackingsuite_id) !== 'undefined') {alert('TrackingSuite è stato caricato più di una volta, per favore controllare.'); throw new Error('TrackingSuite è stato caricato più di una volta, per favore controllare.');}";

        $js_array[] = "window.trackingsuite_id = '" . $project_details['id'] . "';";
        $js_array[] = "ts_setCookie('trackingsuite_project_id', window.trackingsuite_id, 365);";
        $js_array[] = "window.trackingsuite_url = '" . $MSFrameworkCMS->getURLToSite() . $MSFrameworkModules->getShortPrefix() . "/trackingsuite';";

        $js_array[] = "window.trackingsuite_manual_actions = {};";

        // Aggiungo il parametro della lingua (flag)
        $js_array[] = "window.trackingsuite_current_lang = '" . $MSFrameworki18n->getCurrentLanguageDetails()['long_code'] . "';";
        $js_array[] = "window.trackingsuite_default_lang = '" . $MSFrameworki18n->getLanguagesDetails($MSFrameworki18n->getPrimaryLangId())[$MSFrameworki18n->getPrimaryLangId()]['long_code'] . "';";

        if (!empty($project_details['tracking_codes']['google_analytics_ua'])) {
            $js_array[] = $this->getJSCodeForTracking($project_details, 'analytics');
        }

        if (!empty($project_details['tracking_codes']['gads_id'])) {
            $js_array[] = $this->getJSCodeForTracking($project_details, 'gads');
        }

        if (!empty($project_details['tracking_codes']['google_tagmanager_gtm'])) {
            $js_array[] = $this->getJSCodeForTracking($project_details, 'tagmanager');
        }

        if (!empty($project_details['tracking_codes']['facebook_pixel_id'])) {
            $js_array[] = $this->getJSCodeForTracking($project_details, 'facebook');
        }

        if (!empty($project_details['tracking_codes']['albacross'])) {
            $js_array[] = $this->getJSCodeForTracking($project_details, 'albacross');
        }

        if (!empty($project_details['tracking_codes']['linkedin_insight_tag'])) {
            $js_array[] = $this->getJSCodeForTracking($project_details, 'linkedin_insight');
        }

        if (!empty($project_details['tracking_codes']['yandex_metrica'])) {
            $js_array[] = $this->getJSCodeForTracking($project_details, 'yandex_metrica');
        }

        /* CREO LA FUNZIONE CON TUTTI GLI EVENTI DI TRACKINGSUITE */
        $js_array[] = "function initTrackingSuiteActions() {";

        /* Salvo i pixel di tracking usati da AdWords & Analytics */
        $js_array[] = "if(window.location.search.match(/(\?|&)gclid\=([^&]*)/)) {";
        $js_array[] = "msSetTrackReferences('gclid', decodeURIComponent(window.location.search.match(/(\?|&)gclid\=([^&]*)/)[2]));";
        $js_array[] = "}";

        $js_array[] = "if(window.location.search.match(/(\?|&)fbclid\=([^&]*)/)) {";
        $js_array[] = "msSetTrackReferences('fbclid', decodeURIComponent(window.location.search.match(/(\?|&)fbclid\=([^&]*)/)[2]));";
        $js_array[] = "}";

        foreach ($project_details['actions'] as $action_id) {
            $js_array[] = implode(PHP_EOL . PHP_EOL, $tracking_actions[$action_id]['js']($project_details['actions_value'][$action_id]));
        }
        $js_array[] = "}";

        /* TRAMITE QUESTA FUNZIONE RIUSCIAMO A CAPIRE SE UN EVENTO SI RIFERISCE AD UN EVENTO STANDARD (Quindi se ha parametri extra da passare) */
        $js_array[] = "function getTrackingDefaultEventByName(id) {";
        $js_array[] = "var extra_params = {};";
        foreach ($this->getStandardTrackingEvents() as $standardID => $standardParam) {
            $js_array[] = 'extra_params[\'' . $standardID . '\'] = ' . json_encode($standardParam) . ';';
        }

        $js_array[] = "var standard_objectives_events = {};";
        foreach ($project_details['actions'] as $action_id) {
            $js_array[] = "standard_objectives_events['" . str_replace("'", "\'", $project_details['actions_value'][$action_id]['trackingsuite']['event_name']) . "'] = '" . $tracking_actions[$action_id]['default_event'] . "';";
        }

        $js_array[] = "if(typeof(standard_objectives_events[id]) !== 'undefined' && typeof(extra_params[standard_objectives_events[id]]) !== 'undefined') {
            return extra_params[standard_objectives_events[id]];
        }";

        $js_array[] = "return false;";
        $js_array[] = "}";

        $js_array[] = "function trackingSuite_getNearestElement(element, selector) {
            for (; element && element.tagName !== undefined; element = element.parentNode) {
                if(jQuery(element).find(selector).length) return jQuery(element).find(selector);
            }
            return false;
        }";

        $js_array[] = "function trackingSuite_extractPriceFromString(price) {
            price = price.replace(/[^0-9.,-]+/g,\"\");
            if(parseFloat(price) && parseFloat(price) > 0) {
                price = price.trim();
                var result = price.replace(/[^0-9]/g, '');
                if (/[,\.]\d{1,2}$/.test(price)) {
                    price = result.replace(/(\d{2})$/, '.$1');
                }
            }
            return price;
        }";

        $js_array[] = "function trackingSuite_extractCurrencyFromString(price) {
            var currency_list = {\"USD\":\"$\",\"CAD\":\"CA$\",\"EUR\":\"€\",\"AED\":\"AED\",\"AFN\":\"Af\",\"ALL\":\"ALL\",\"AMD\":\"AMD\",\"ARS\":\"AR$\",\"AUD\":\"AU$\",\"AZN\":\"man.\",\"BAM\":\"KM\",\"BDT\":\"Tk\",\"BGN\":\"BGN\",\"BHD\":\"BD\",\"BIF\":\"FBu\",\"BND\":\"BN$\",\"BOB\":\"Bs\",\"BRL\":\"R$\",\"BWP\":\"BWP\",\"BYR\":\"BYR\",\"BZD\":\"BZ$\",\"CDF\":\"CDF\",\"CHF\":\"CHF\",\"CLP\":\"CL$\",\"CNY\":\"CN¥\",\"COP\":\"CO$\",\"CRC\":\"₡\",\"CVE\":\"CV$\",\"CZK\":\"Kč\",\"DJF\":\"Fdj\",\"DKK\":\"Dkr\",\"DOP\":\"RD$\",\"DZD\":\"DA\",\"EEK\":\"Ekr\",\"EGP\":\"EGP\",\"ERN\":\"Nfk\",\"ETB\":\"Br\",\"GBP\":\"£\",\"GEL\":\"GEL\",\"GHS\":\"GH₵\",\"GNF\":\"FG\",\"GTQ\":\"GTQ\",\"HKD\":\"HK$\",\"HNL\":\"HNL\",\"HRK\":\"kn\",\"HUF\":\"Ft\",\"IDR\":\"Rp\",\"ILS\":\"₪\",\"INR\":\"Rs\",\"IQD\":\"IQD\",\"IRR\":\"IRR\",\"ISK\":\"Ikr\",\"JMD\":\"J$\",\"JOD\":\"JD\",\"JPY\":\"¥\",\"KES\":\"Ksh\",\"KHR\":\"KHR\",\"KMF\":\"CF\",\"KRW\":\"₩\",\"KWD\":\"KD\",\"KZT\":\"KZT\",\"LBP\":\"LB£\",\"LKR\":\"SLRs\",\"LTL\":\"Lt\",\"LVL\":\"Ls\",\"LYD\":\"LD\",\"MAD\":\"MAD\",\"MDL\":\"MDL\",\"MGA\":\"MGA\",\"MKD\":\"MKD\",\"MMK\":\"MMK\",\"MOP\":\"MOP$\",\"MUR\":\"MURs\",\"MXN\":\"MX$\",\"MYR\":\"RM\",\"MZN\":\"MTn\",\"NAD\":\"N$\",\"NGN\":\"₦\",\"NIO\":\"C$\",\"NOK\":\"Nkr\",\"NPR\":\"NPRs\",\"NZD\":\"NZ$\",\"OMR\":\"OMR\",\"PAB\":\"B\/.\",\"PEN\":\"S\/.\",\"PHP\":\"₱\",\"PKR\":\"PKRs\",\"PLN\":\"zł\",\"PYG\":\"₲\",\"QAR\":\"QR\",\"RON\":\"RON\",\"RSD\":\"din.\",\"RUB\":\"RUB\",\"RWF\":\"RWF\",\"SAR\":\"SR\",\"SDG\":\"SDG\",\"SEK\":\"Skr\",\"SGD\":\"S$\",\"SOS\":\"Ssh\",\"SYP\":\"SY£\",\"THB\":\"฿\",\"TND\":\"DT\",\"TOP\":\"T$\",\"TRY\":\"TL\",\"TTD\":\"TT$\",\"TWD\":\"NT$\",\"TZS\":\"TSh\",\"UAH\":\"₴\",\"UGX\":\"USh\",\"UYU\":\"\$U\",\"UZS\":\"UZS\",\"VEF\":\"Bs.F.\",\"VND\":\"₫\",\"XAF\":\"FCFA\",\"XOF\":\"CFA\",\"YER\":\"YR\",\"ZAR\":\"R\",\"ZMK\":\"ZK\"};
            var current_currency_name = 'EUR';
            Object.keys(currency_list).forEach(function(name) {
                if(price.indexOf(currency_list[name]) >= 0) {
                    current_currency_name = name;
                }
            });
            return current_currency_name;
        }";

        $js_array[] = "function trackingSuite_prependEvent(type, selettore, fn) {
               
            var attachFn = function(element) {
                var oldEvent = jQuery(element).attr(type);
                jQuery(element).removeAttr(type).unbind(type);
                var function_name = 'ms_' + type + '_' + Math.random().toString(36).substr(2, 9);        
                window[function_name] = fn;
                jQuery(element).attr(type, function_name + '(this)' + (oldEvent ? '; ' + oldEvent : ''));     
            }
            
            if(jQuery(selettore).length) {
                attachFn(jQuery(selettore));
            } else {
                jQuery('body').on('mouseenter touchenter ', selettore, function() {attachFn(jQuery(selettore))});
            }
            
        }";

        /* SE JQUERY ESISTE INIZIALIZZO LA FUNZIONE, ALTRIMENTI LO CARICO DINAMICAMENTE PRIMA */
        $js_array[] = "jQuery(document).ready(function() {
            initTrackingSuiteActions();
        });";

        /* HELPER FUNZIONI TRACK */
        $js_array[] = "function _msFormatParamsFor(platform, id, parameters, extra_parameters) {            
            if(typeof(extra_parameters) !== 'object') {
                extra_parameters = {};
            }
            
            var ary_to_return = {};

            if(platform === 'analytics') {
                 ary_to_return = Object.assign({}, parameters);
                 delete ary_to_return['event_action'];
            }
            else if(platform === 'gads') {
                 ary_to_return = Object.assign({}, parameters);
                 ary_to_return['send_to'] = '" . trim($project_details['tracking_codes']['gads_id']) . "/' + parameters['conversion_label'];
                 delete ary_to_return['conversion_label'];
            }
            else if(platform === 'facebook') {
                var current_params = getTrackingDefaultEventByName(id);
                
                 ary_to_return = Object.assign({}, parameters);
                 delete ary_to_return['label'];
                 
                if(current_params) {
                
                    try {
                        var current_extra_params = current_params['platforms'][platform]['extra_params'];
       
                        Object.keys(current_extra_params).forEach(function(param_id) {
                            ary_to_return[param_id] = current_extra_params[param_id]['default'];
                        });
                        
                        Object.keys(current_extra_params).forEach(function(param_id) {
                            var value_to_sent = '';
                            var current_param_match = current_extra_params[param_id]['match'];

                            if(typeof(parameters[param_id]) !== 'undefined' && parameters[param_id].length) {
                                value_to_sent = parameters[param_id];
                            } else if(current_param_match && current_param_match.length > 1) {
                                if(typeof(extra_parameters[current_param_match]) !== 'undefined' && extra_parameters[current_param_match].length) {
                                    value_to_sent = extra_parameters[current_param_match];
                                }
                            }
               
                            if(current_extra_params[param_id]['type'] === 'int') {
                                value_to_sent = (!isNaN(parseInt(value_to_sent)) ? parseInt(value_to_sent) : 0);
                            } else if(current_extra_params[param_id]['type'] === 'float') {
                                value_to_sent = (!isNaN(parseFloat(value_to_sent)) ? parseFloat(value_to_sent) : 0);
                            }
                            
                            ary_to_return[param_id] = value_to_sent;
                        });
                        
                    } catch(e) {}
                
                }
                
            }
            
             Object.keys(ary_to_return).forEach(function(param_key) {
                if(ary_to_return[param_key] === '') {
                    delete ary_to_return[param_key];
                }
             });
            
            return ary_to_return;
        }";

        /* CREO LA FUNZIONE CHE GENERA I TRACKING (FUNZIONE INTERNA) */
        $js_array[] = "function msInternalTrack(tracking_parameters, extra_parameters) {
            if(typeof(extra_parameters) !== 'object') {
                extra_parameters = {};
            }

            /* SOSTITUISCO EVENTUALI SHORTCODES CON VALORI DEI PARAMETRI */
            ['trackingsuite', 'facebook', 'analytics', 'gads'].forEach(function(platform) {
                /* Sostituisco eventuali shortcodes parametri */
                if(typeof(tracking_parameters[platform]) === 'object') {
                    Object.keys(tracking_parameters[platform]).forEach(function(param_key) {
                        Object.keys(extra_parameters).forEach(function(extra_key) {
                            tracking_parameters[platform][param_key] = tracking_parameters[platform][param_key].replace('{' + extra_key + '}', extra_parameters[extra_key]);
                        });
                    });
                }
            });";

        $js_array[] = "if(tracking_parameters.enable_external_tracking == '1' && (typeof(tracking_parameters['execAction']) === 'undefined' || !tracking_parameters['execAction'])) {";

        if (!empty($project_details['tracking_codes']['google_analytics_ua'])) {
            $js_array[] = "
            if(typeof(tracking_parameters['analytics']) !== 'undefined') {";
            if(!empty($project_details['tracking_codes']['gads_id'])) {
                $js_array[] = "if(typeof(tracking_parameters['gads']) !== 'undefined' && tracking_parameters['gads']['conversion_label'].length) {
                    tracking_parameters['analytics']['send_to'] = '" . trim($project_details['tracking_codes']['gads_id']) . "/' + tracking_parameters['gads']['conversion_label'];
                }";
            }

            $js_array[] = "Object.keys(tracking_parameters['analytics']).forEach(function(key) {
                    if(isNaN(tracking_parameters['analytics'][key])) {
                        try {
                            var lang_json = JSON.parse(tracking_parameters['analytics'][key]);
                            tracking_parameters['analytics'][key] = (typeof(lang_json[window.trackingsuite_current_lang]) !== 'undefined' && lang_json[window.trackingsuite_current_lang] !== '' ? lang_json[window.trackingsuite_current_lang] : lang_json[window.trackingsuite_default_lang]);
                        } catch(e) {}
                    }
                                        
                    if(tracking_parameters['analytics'][key] === NaN) {
                        tracking_parameters['analytics'][key] = '';
                    }
                });
                
                var event_action = tracking_parameters['analytics']['event_action'];
                var analytics_extra_params = _msFormatParamsFor('analytics', tracking_parameters['trackingsuite']['event_name'], tracking_parameters['analytics']);   
                
                try {                 
                    if(event_action.length || analytics_extra_params.length) {";
            if(FRAMEWORK_DEBUG_MODE) {
                $js_array[] = "console.log('gtag', [event_action, analytics_extra_params]);";
            } else {
                $js_array[] = "gtag('event', event_action, analytics_extra_params);";
            }
            $js_array[] = "}";
            $js_array[] = "} catch(e) {console.log(e);}
            }";
        }

        if (!empty($project_details['tracking_codes']['gads_id']) && empty($project_details['tracking_codes']['google_analytics_ua'])) {
            $js_array[] = "     
            if(typeof(tracking_parameters['gads']) !== 'undefined') {
                Object.keys(tracking_parameters['gads']).forEach(function(key) {
                    if(isNaN(tracking_parameters['gads'][key])) {
                        try {
                            var lang_json = JSON.parse(tracking_parameters['gads'][key]);
                            tracking_parameters['gads'][key] = (typeof(lang_json[window.trackingsuite_current_lang]) !== 'undefined' && lang_json[window.trackingsuite_current_lang] !== '' ? lang_json[window.trackingsuite_current_lang] : lang_json[window.trackingsuite_default_lang]);
                        } catch(e) {}
                    }
                    
                    if(tracking_parameters['gads'][key] === NaN) {
                        tracking_parameters['gads'][key] = '';
                    }
                });
                
                var event_action = 'conversion';
                var gads_extra_params = _msFormatParamsFor('gads', event_action, tracking_parameters['gads']);   
                try { 
                    if(event_action.length || gads_extra_params.length) {                        
                        " . (FRAMEWORK_DEBUG_MODE ? 'console.log("' : '') . "gtag('event', event_action, gads_extra_params);" . (FRAMEWORK_DEBUG_MODE ? '")' : '') . "
                    }
                } catch(e) {}    
            }";
        }

        if (!empty($project_details['tracking_codes']['facebook_pixel_id'])) {
            $js_array[] = "
            if(typeof(tracking_parameters['facebook']) !== 'undefined') {
                Object.keys(tracking_parameters['facebook']).forEach(function(key) {
                    if(isNaN(tracking_parameters['facebook'][key])) {
                        try {
                            var lang_json = JSON.parse(tracking_parameters['facebook'][key]);
                            if(typeof(lang_json) === 'object') {
                                tracking_parameters['facebook'][key] = (typeof(lang_json[window.trackingsuite_current_lang]) !== 'undefined' && lang_json[window.trackingsuite_current_lang] !== '' ? lang_json[window.trackingsuite_current_lang] : lang_json[window.trackingsuite_default_lang]);
                            }
                        } catch(e) {}
                    }
                    
                    if(tracking_parameters['facebook'][key] === NaN) {
                        tracking_parameters['facebook'][key] = '';
                    }
                });
                
                try { 
                    if(tracking_parameters['facebook']['label'].length) {
                        var facebook_extra_params = _msFormatParamsFor('facebook', tracking_parameters['trackingsuite']['event_name'], tracking_parameters['facebook'], extra_parameters);
                        if(Object.keys(facebook_extra_params).length > 0) {
                            " . (FRAMEWORK_DEBUG_MODE ? "console.log('[TrackingSuite] FBQ:', tracking_parameters['facebook']['label'], facebook_extra_params);" : "fbq('track', tracking_parameters['facebook']['label'], facebook_extra_params);") . "
                        } else {
                            " . (FRAMEWORK_DEBUG_MODE ? "console.log('[TrackingSuite]  FBQ:', tracking_parameters['facebook']['label']);" : "fbq('track', tracking_parameters['facebook']['label']);") . "
                        }
                    }
                } catch(e) {}
            }";
        }

        if (!empty($project_details['tracking_codes']['linkedin_insight_tag'])) {
            $js_array[] = "
            if(typeof(tracking_parameters['linkedin_insight']) !== 'undefined') {       
                try { 
                    if(tracking_parameters['linkedin_insight']['conversion_id'].length) {
                        " . (FRAMEWORK_DEBUG_MODE ? "console.log('[TrackingSuite] LinkedIn:', tracking_parameters['linkedin_insight']['conversion_id']);" : "$('body').append('<img height=\"1\" width=\"1\" style=\"display:none;\" alt=\"\" src=\"https://px.ads.linkedin.com/collect/?pid=" . $project_details['tracking_codes']['linkedin_insight_tag'] . "&conversionId=' + tracking_parameters['linkedin_insight']['conversion_id'] + '&fmt=gif\" />')") . "
                    }
                } catch(e) {}
            }";
        }

        $js_array[] = "}";

        $js_array[] = "tracking_parameters['trackingsuite_id'] = window.trackingsuite_id;
            
            var trackingUserData = {};
            if(window.trackingSuite_needLogin) {
                trackingUserData = {
                    email: window.trackingSuite_userEmail,
                    extra: window.trackingSuite_userExtra
                }
            }
            
            " . (!FRAMEWORK_DEBUG_MODE ? "
            jQuery.ajax({
                type: \"POST\",
                url: window.trackingsuite_url,
                xhrFields: { withCredentials: true },
                data: {
                    project_id: window.trackingsuite_id,
                    track_info: tracking_parameters['trackingsuite'],
                    track_extra: extra_parameters,
                    register_user: trackingUserData,
                    tracking_references: ts_getCookie('trackingsuite_track_codes'),
                    exec_action: (typeof(tracking_parameters['execAction']) !== 'undefined' ?  tracking_parameters['execAction'] : '')
                },
                dataType: 'json'
             });
            " : "
            console.log('[TrackingSuite]', {
                project_id: window.trackingsuite_id,
                track_info: tracking_parameters['trackingsuite'],
                track_extra: extra_parameters,
                register_user: trackingUserData,
                tracking_references: ts_getCookie('trackingsuite_track_codes'),
                exec_action: (typeof(tracking_parameters['execAction']) !== 'undefined' ?  tracking_parameters['execAction'] : '')
            });
            ") . "
        }";


        /* CREO LA FUNZIONE CHE GENERA I TRACKING */
        $js_array[] = "function msTrack(event_name, extra_params) {
            if(typeof(extra_params) !== 'undefined') {
                if(typeof(extra_params) !== 'object') {
                    extra_params = {value: extra_params};
                }
            } else {
                extra_params = {};
            }
            
            var manual_action = getTrackingSuiteManualAction(event_name, extra_params);
            msInternalTrack(manual_action, extra_params);
        }";

        $js_array[] = "function getTrackingSuiteManualAction(event_name, extra_params) {
            
            var manual_action = {};
            
            if(typeof(window.trackingsuite_manual_actions[event_name]) == 'object') {   
                manual_action = window.trackingsuite_manual_actions[event_name];
            } else {
                console.warn('[TrackingSuite] È stato inviato un Evento (' + event_name + ') non esistente negli Obiettivi, pertanto l\'azione verrà tracciata solo su TrackingSuite e non saranno inviati parametri extra.');
                manual_action = {
                    enable_external_tracking: 0,
                    trackingsuite: {
                        event_name: event_name,
                        event_value: (typeof(extra_params.value) !== 'undefined' ? extra_params.value : '')
                    }
                };
            }
        
            return manual_action;
        }";

        /* CREO LA FUNZIONE CHE ASSEGNA I CODICI DELLE REFERENZE DI TRACKING (gclid, fbclid etc) */

        $js_array[] = "function msSetTrackReferences(ref_name, ref_value) {
            var current_track_references = ts_getCookie('trackingsuite_track_codes');
            if(typeof(current_track_references) !== 'object') {
                current_track_references = {};
            }
            
            current_track_references[ref_name] = ref_value;
            ts_setCookie('trackingsuite_track_codes', JSON.stringify(current_track_references), 365);
            return true;
        }";

        /* STAMPO LE FUNZIONI AVANZATE */
        foreach($this->getAdvancedTrackingFunctions() as $function_name => $function) {
            $js_array[] = $function['js']();
        }

        $js_array[] = "if(window !== window.top) {";
        $js_array[] = file_get_contents(FRAMEWORK_ABSOLUTE_PATH . 'frontend/modules/trackingsuite/js/visual_selector.js');
        $js_array[] = "}";

        /* FUNZIONI RELATIVE ALLA GESTIONE DEI COOKIE */
        $js_array[] = 'function ts_setCookie(cname, cvalue, exdays) {
          var d = new Date();
          d.setTime(d.getTime() + (exdays*24*60*60*1000));
          var expires = "expires="+ d.toUTCString();
           
          document.cookie = cname + "=" + cvalue + ";expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/";
          document.cookie = cname + "=" + cvalue + ";" + expires + ";domain=" + document.domain + ";path=/";
        }';

        $js_array[] = 'function ts_getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(\';\');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == \' \') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    var return_string = c.substring(name.length, c.length);
                    try {
                        return_string = $.parseJSON(return_string);
                    } catch(err) {}
                    
                    return return_string;
                }
            }
            return "";
        }';

        /* CONVERTO L'ARRAY IN STRINGA */
        $js_code = preg_replace(array("/\s+\n/","/\n\s+/","/ +/"),array("\n","\n "," "), implode(' ', $js_array));

        return $js_code;

    }

    /* FUNZIONI BACKEND */
    /**
     * Salva i dati del tracking sul database del Framework
     *
     * @return string
     */
    public function saveTrackDetails($track_detail) {

        $project_ids = $this->getTrackingProjectIDsByWebsite($_SERVER['HTTP_REFERER']);

        if(!in_array($track_detail['project_id'], $project_ids)) {
            return '';
        }

        $project_id = $track_detail['project_id'];

        $result = 0;
        if($project_id) {

            setcookie(
                "trackingsuite_project_id",
                $project_id,
                time() + (10 * 365 * 24 * 60 * 60),
                '/',
                $this->MSFrameworkCMS->getCleanHost()
            );

            $user_info = $this->getUserData();
            $user_id = ($user_info ? $user_info['id'] : 0);

            // Se l'utente non è loggato oppure l'account attualmente associato risulta diverso da quello indicato dalla funzione msTrackUser lo registro
            if(isset($track_detail['register_user']) && count($track_detail['register_user']) > 0 && (!$user_info || $user_info['email'] !== $track_detail['register_user']['email']) ) {
                $user_id = $this->storeUserData($track_detail['register_user']['email'], $track_detail['extra']);
            }

            $array_to_save = array(
                "user_ref" => $user_id,
                "user_ip" => $_SERVER['REMOTE_ADDR'],
                "project_id" => $project_id,
                "track_event" => $track_detail['track_info']['event_name'],
                "track_ref" => $track_detail['track_info']['event_value'],
                "track_extra" => json_encode($track_detail['track_extra']),
            );

            $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
            $result = $this->MSFrameworkDatabase->pushToDB("INSERT INTO tracking__stats ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
            $cookie_id = $this->MSFrameworkDatabase->lastInsertId();

            if($track_detail['exec_action'] === 'createOperation') {
                (new \MSFramework\Fatturazione\vendite())->importFrom('tracking', $array_to_save);
            }

            if(!isset($_SESSION['trackingSuite_trackedEvents'])) {
                $_SESSION['trackingSuite_trackedEvents'] = array();
            }

            $_SESSION['trackingSuite_trackedEvents'][] = array($track_detail['track_info']['event_name'], $track_detail['track_info']['event_value']);

            if($_POST['tracking_references']) {
                setcookie(
                    "trackingsuite_track_codes",
                    json_encode($_POST['tracking_references']),
                    time() + (10 * 365 * 24 * 60 * 60),
                    '/',
                    $this->MSFrameworkCMS->getCleanHost()
                );
            }

            if($user_id > 0) {

                /* Aggiorno eventuali referenze relativi al gclid, fbclid etc */
                if($_POST['tracking_references']) {
                    $this->updateUserReferences($user_id, $project_id, $_POST['tracking_references']);
                }

                //(new \MSFramework\Newsletter\lists())->syncContactToLists();
            } else if(!$user_info && isset($cookie_id)) {

                $trackingsuite_cookie = (isset($_COOKIE["trackingsuite_event_match"]) ? json_decode($_COOKIE["trackingsuite_event_match"], true) : array());

                /* Utilizzo la sessione perchè la variabile $_COOKIE viene aggiornata solamente se il cliente aggiorna la pagina, quindi non funzionerebbe per più cookie nella stessa */
                $trackingsuite_session = (isset($_SESSION["trackingsuite_event_match"]) ? json_decode($_SESSION["trackingsuite_event_match"], true) : array());
                $trackingsuite_session[] = $cookie_id;

                if($trackingsuite_cookie) {
                    $trackingsuite_cookie = $trackingsuite_session + $trackingsuite_cookie;
                } else {
                    $trackingsuite_cookie = $trackingsuite_session;
                }

                $_SESSION["trackingsuite_event_match"] = json_encode($trackingsuite_cookie);

                setcookie(
                    "trackingsuite_event_match",
                    json_encode($trackingsuite_cookie),
                    time() + (10 * 365 * 24 * 60 * 60),
                    '/',
                    $this->MSFrameworkCMS->getCleanHost()
                );
            }
        }

        return $result;
    }

    /**
     * Aggiorna le referenze del cliente (ID dei servizi di tracking esterni)
     *
     * @param $user_id integer L'id del cliente che ha effettuato il login
     * @param $project_id integer L'id del progetto
     * @param $references_to_save array L'array contenente i vari codici di tracking da salvare
     *
     * @return boolean
     */
    private function updateUserReferences($user_id, $project_id, $references_to_save) {

        if(!(int)$project_id) {
            return false;
        }

        $current_user_track_datas = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM tracking__references WHERE project_id = :project_id AND user_ref = :user_id", array(':project_id' => $project_id, ':user_id' => $user_id), true);
        $current_user_reference = array();
        $current_user_useragents = array();

        if($current_user_track_datas) {

            $current_user_reference = json_decode($current_user_track_datas['track_references'], true);
            $current_user_useragents = json_decode($current_user_track_datas['user_agent'], true);

            foreach($current_user_reference as $old_ref_k => $old_ref_v) {
                if(!isset($references_to_save[$old_ref_k]) || empty($references_to_save[$old_ref_k])) {
                    $references_to_save[$old_ref_k] = $old_ref_v;
                }
            }
        }

        foreach($current_user_useragents as $ua_date => $ua_value) {
            if($ua_value == $_SERVER['HTTP_USER_AGENT']) unset($current_user_useragents[$ua_date]);
        }

        $current_user_useragents[time()] = $_SERVER['HTTP_USER_AGENT'];

        $array_to_save = array(
            "user_ref" => $user_id,
            "project_id" => $project_id,
            "track_references" => json_encode($references_to_save),
            "user_agent" => json_encode($current_user_useragents)
        );

        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, ($current_user_reference ? 'update' : 'insert'));

        if(!$current_user_track_datas) {
            $result = $this->MSFrameworkDatabase->pushToDB("INSERT INTO tracking__references ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
        } else {
            $result = $this->MSFrameworkDatabase->pushToDB("UPDATE tracking__references SET $stringForDB[1] WHERE id = :id", array_merge(array(":id" => $current_user_reference['id']), $stringForDB[0]));
        }

        if($result) {
            foreach(array('fbclid', 'gclid') as $refToSave) {
                if((isset($references_to_save[$refToSave]) && (!isset($current_user_reference[$refToSave])) || ($current_user_reference[$refToSave] != $references_to_save[$refToSave]))) {
                    $array_to_save = array(
                        "user_ref" => $user_id,
                        "user_ip" => $_SERVER['REMOTE_ADDR'],
                        "project_id" => $project_id,
                        "track_event" => $refToSave,
                        "track_ref" => $references_to_save[$refToSave],
                        "track_extra" => json_encode(array()),
                    );

                    $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
                    $this->MSFrameworkDatabase->pushToDB("INSERT INTO tracking__stats ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
                }
            }
        }

        return $result;

    }

    /**
     * Controlla se l'utente (ora loggato) ha accumulato dei cookie in precedenza o se l'IP è uguale a quello di eventuali eventi già tracciati
     * e nel caso collega la referenza del cliente ai vecchi eventi tracciati.
     *
     * @param $user_id integer L'id del cliente che ha effettuato il login
     *
     * @return string
     */
    public function checkForUserCookie($user_id = 0) {

        // Se il cliente
        if($user_id > 0 && $this->getUserData('id') != $user_id) {
            $_SESSION['trackinSuite_UserID'] = $user_id;
        } else if($user_id == 0) {
            $user_id = (int)$this->getUserData('id');
        }

        if($user_id > 0) {

            $trackingsuite_cookie = (isset($_COOKIE["trackingsuite_event_match"]) ? json_decode($_COOKIE["trackingsuite_event_match"], true) : array());

            $cookie_where = '';
            if($trackingsuite_cookie) {
                $cookie_where = "id IN (" . implode(',', $trackingsuite_cookie) . ") OR";
            }

            $this->MSFrameworkDatabase->pushToDB("UPDATE tracking__stats SET user_ref = $user_id WHERE ($cookie_where user_ip LIKE '" . $_SERVER['REMOTE_ADDR'] . "') AND user_ref = 0");

            $_SESSION["trackingsuite_event_match"] = json_encode(array());
            setcookie(
                "trackingsuite_event_match",
                json_encode(array()),
                time() + (10 * 365 * 24 * 60 * 60),
                '/',
                $this->MSFrameworkCMS->getCleanHost()
            );

            $this->MSFrameworkDatabase->pushToDB("UPDATE customers SET last_login = NOW() WHERE id = $user_id");

            if(isset($_COOKIE['trackingsuite_track_codes']) && json_decode($_COOKIE['trackingsuite_track_codes'])) {
                $this->updateUserReferences($user_id, $_COOKIE['trackingsuite_project_id'], json_decode($_COOKIE['trackingsuite_track_codes'], true));
            }

            //(new \MSFramework\Newsletter\lists())->syncContactToLists();

        }

        return true;

    }

    /**
     * Ottiene l'id e l'email dell'eventuale cliente tracciato
     *
     * @param $field mixed il campo da ottenere (Se vuoto ottiene tutto)
     *
     * @return mixed
     */
    public function getUserData($field = '') {

        $return_info = array();

        if($_SESSION['trackinSuite_UserID'] && (int)$_SESSION['trackinSuite_UserID'] > 0) {
            $return_info = array(
                'id' => $_SESSION['trackinSuite_UserID'],
                'email' => $_SESSION['trackinSuite_UserEmail']
            );
        } else if($_COOKIE['trackinSuite_UserID'] && (int)$_COOKIE['trackinSuite_UserID'] > 0) {
            $return_info = array(
                'id' => $_COOKIE['trackinSuite_UserID'],
                'email' => $_COOKIE['trackinSuite_UserEmail']
            );
        } else {
            $MSFrameworkCustomers = new \MSFramework\customers();

            $user_data = $MSFrameworkCustomers->getUserDataFromSession();
            if ($user_data) {
                $return_info = array(
                    'id' => $user_data['user_id'],
                    'email' => $user_data['email']
                );
            }
        }

        // Salvo l'ID e l'email nella sessione di trackingSuite in modo da rendere più veloce le future letture
        if ($return_info) {
            $_SESSION['trackinSuite_UserID'] = $return_info['id'];
            $_SESSION['trackinSuite_UserEmail'] = $return_info['email'];

            setcookie(
                "trackinSuite_UserID",
                $return_info['id'],
                time() + (10 * 365 * 24 * 60 * 60),
                '/',
                $this->MSFrameworkCMS->getCleanHost()
            );

            setcookie(
                "trackinSuite_UserEmail",
                $return_info['email'],
                time() + (10 * 365 * 24 * 60 * 60),
                '/',
                $this->MSFrameworkCMS->getCleanHost()
            );
        }

        if($field !== '')
        {
            if(isset($return_info[$field])) return $return_info[$field];
            else return false;
        }

        return $return_info;

    }

    /**
     * Salva i dati del cliente tracciato nel database
     *
     * @param $email string L'email del cliente
     * @param $extras array I dati aggiuntivi da salvare
     *
     * @return string
     */
    public function storeUserData($email, $extras = array()) {

        $extra_match = array(
            'name' => 'nome',
            'surname' => 'cognome',
            'phone' => 'telefono_cellulare',
            'sex' => 'sesso',
            'address' => 'indirizzo',
            'city' => 'citta',
            'province' => 'provincia',
            'postal_code' => 'cap',

        );

        $extra_params = array();

        foreach($extra_match as $k => $name) {
            if(isset($extras[$k])) {
                $extra_params[$name] = $extras[$k];
            } else {
                $extra_params[$name] = '';
            }
        }

        $return_id = 0;

        $check_user = $this->getUserData();
        if($check_user && $check_user['email'] == $email)
        {
            // Se il cliente da registrare è già loggato ritorno direttamente la referenza
            $return_id = $check_user['id'];
        }

        // Se non è loggato lo registro
        if(!$return_id)
        {
            if(filter_var($email, FILTER_SANITIZE_EMAIL)) {
                $MSFrameworkCustomers = new \MSFramework\customers();
                $user_data = $MSFrameworkCustomers->getCustomerDataByEmail($email);

                // Se il cliente esiste ritorno la referenza del cliente già registrato altrimenti lo creo
                if ($user_data) {
                    $return_id = $user_data['id'];
                } else {
                    $return_id = $MSFrameworkCustomers->registerUser($extra_params['nome'], $extra_params['cognome'], $email, '', $extra_params['telefono_cellulare'], '', $extra_params);
                }
            }
        }

        // Salvo l'ID e l'email nella sessione di trackingSuite in modo da rendere più veloce le future letture
        if ($return_id) {
            $_SESSION['trackinSuite_UserID'] = $return_id;
            $_SESSION['trackinSuite_UserEmail'] = $email;
        }

        return (int)$return_id;

    }

    /**
     * Invia un acquisto ad eventuali sistemi di Tracking se le referenze sono disponibili
     *
     * @param $operation_id integer L'ID dell'operazione da controllare
     *
     * @return bool nb
     *
     */
    public function trackPurchase($operation_id) {

        $operation_details = (new \MSFramework\Fatturazione\vendite())->ottieniDettagliVendita($operation_id);

        if($operation_details) {

            $operation_details = $operation_details[$operation_id];

            // Aggiungo l'origine dell'ordine
            $customer_data = (new \MSFramework\customers())->getCustomerDataFromDB($operation_details['cliente']);

            if($customer_data) {
                $operation_sources = array();

                /* Ottengo il referer del cliente */
                $customer_referer = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `tracking__stats` WHERE user_ref = :user_id AND track_extra LIKE '%\"domain\":\"%' ORDER BY track_date DESC", array(':user_id' => $customer_data['id']), true);
                if($customer_referer) $operation_sources['referer'] =  array('ref' => json_decode($customer_referer['track_extra'], true)['domain'], 'date' => strtotime($customer_referer['track_date']));

                /* Ottengo l'origine pubblicitaria del cliente */
                $customer_gclid = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `tracking__stats` WHERE user_ref = :user_id AND track_event LIKE 'gclid' ORDER BY track_date DESC", array(':user_id' => $customer_data['id']), true);
                if($customer_gclid) $operation_sources['gclid'] = array('ref' => $customer_gclid['track_ref'], 'date' => strtotime($customer_gclid['track_date']));

                $customer_fbclid = $this->MSFrameworkDatabase->getAssoc("SELECT * FROM `tracking__stats` WHERE user_ref = :user_id AND track_event LIKE 'fbclid' ORDER BY track_date DESC", array(':user_id' => $customer_data['id']), true);
                if($customer_fbclid) $operation_sources['fbclid'] = array('ref' => $customer_fbclid['track_ref'], 'date' => strtotime($customer_fbclid['track_date']));

                $this->MSFrameworkDatabase->pushToDB("UPDATE fatturazione_vendite SET sources = :sources WHERE id = :id", array(':sources' => json_encode($operation_sources), ':id' => $operation_details['id']));
            }

            // Se l'ordine proviene dal tracking allora è già stato tracciato
            if(explode('_', $operation_details['reference'])[0] === 'tracking') {
                return false;
            }

            $user_track_codes = $this->MSFrameworkDatabase->getAssoc("SELECT project_id, track_references FROM tracking__references WHERE user_ref = :user_id", array(':user_id' => $operation_details['cliente']), true);

            /* Ottengo il totale dell'ordine */
            $no_tax_total = 0;
            $totale = 0;
            $tasse_totali = 0;

            $product_ids = array();
            $product_names = array();

            foreach ($operation_details['carrello'] as $k => $prodotto) {
                $no_tax_total += $prodotto['no_tax_subtotal'];
                $totale += $prodotto['prezzo_subtotal'];
                $tasse_totali += ($prodotto['prezzo_subtotal'] - $prodotto['no_tax_subtotal']);

                $product_ids[] = $prodotto['id'];
                $product_names[] = $prodotto['nome'];
            }

            $product_ids = array_filter($product_ids);
            $product_names = array_filter($product_names);

            $extra_params_values = array(
                '{price}' => $totale,
                '{currency}' => 'EUR',
                '{title}' => implode(', ', $product_names),
                '{category}' => '',
                '{product_id}' => json_encode($product_ids)
            );

            $urls_to_post = array();

            if($user_track_codes) {
                $tracking_project_info = $this->getTrackingProjectDetails($user_track_codes['project_id']);

                if($tracking_project_info) {
                    $tracking_codes = json_decode($user_track_codes['track_references'], true);

                    $customer_user_agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)';
                    if(json_decode($user_track_codes['user_agent'])) {
                        $used_user_agents = json_decode($user_track_codes['user_agent'], true);
                        asort($used_user_agents);
                        $customer_user_agent = array_values($used_user_agents)[0];
                    }

                    if(in_array('acquisto', $tracking_project_info['actions'])) {

                        $azione_acquisto = $tracking_project_info['actions_value']['acquisto'];

                        // Salvo la vendita su Google Analytics
                        if (isset($tracking_codes['ga_clientId']) && !empty($tracking_project_info['tracking_codes']['google_analytics_ua'])) {
                            $analytics = new Analytics(true);

                            $analytics
                                ->setProtocolVersion('1')
                                ->setTrackingId($tracking_project_info['tracking_codes']['google_analytics_ua'])
                                ->setUserAgentOverride($customer_user_agent)
                                ->setClientId($tracking_codes['ga_clientId']);

                            // TODO GCLID Adwords ID
                            if (!empty($tracking_codes['gclid'])) {
                                $analytics->setGoogleAdwordsId($tracking_codes['gclid']);
                            }

                            $analytics
                                ->setTransactionId('purchase_' . $operation_id)
                                ->setRevenue($no_tax_total)
                                ->setTax($tasse_totali)
                                ->setCurrencyCode('EUR');

                            foreach ($operation_details['carrello'] as $k => $prodotto) {
                                $analytics->addProduct([
                                    'sku' => (empty($prodotto['id']) ? $k . uniqid() : 'id_' . $prodotto['id']),
                                    'name' => $prodotto['nome'],
                                    'price' => (float)number_format($prodotto['no_tax_subtotal']/$prodotto['qty'], 2, '.', '.'),
                                    'quantity' => (int)$prodotto['qty'],
                                ]);
                            }

                            $analytics->setProductActionToPurchase();
                            $analytics->sendEvent();

                            // Invia l'evento impostato nel modulo tracking (Se disponibile)
                            $analytics->setEventCategory(strtr($this->MSFrameworki18n->getFieldValue($azione_acquisto['analytics']['event_category']), $extra_params_values))
                                ->setEventAction(strtr($this->MSFrameworki18n->getFieldValue($azione_acquisto['analytics']['event_action']), $extra_params_values))
                                ->setEventLabel(strtr($this->MSFrameworki18n->getFieldValue($azione_acquisto['analytics']['event_label']), $extra_params_values));

                            $event_value = (int)strtr($this->MSFrameworki18n->getFieldValue($azione_acquisto['analytics']['value']), $extra_params_values);
                            if($event_value > 0) $analytics->setEventValue($event_value);

                            $analytics->sendEvent();
                        }

                        // Salvo la vendita su Facebook
                        if (!empty($tracking_project_info['tracking_codes']['facebook_pixel_id'])) {

                            $facebook_url = 'https://www.facebook.com/tr/?';

                            $facebook_params = array(
                                'id' => $tracking_project_info['tracking_codes']['facebook_pixel_id'],
                                'ev' => 'Purchase',
                                'ud[em]' => hash('sha256', $operation_details['dati_cliente']['email']),
                                'ud[fn]' => hash('sha256', $operation_details['dati_cliente']['nome']),
                                'ud[ln]' => hash('sha256', $operation_details['dati_cliente']['cognome']),
                                'cd[currency]' => strtr($azione_acquisto['facebook']['currency'], $extra_params_values),
                                'cd[content_type]' => 'product',
                                'cd[content_name]' => strtr($azione_acquisto['facebook']['content_name'], $extra_params_values),
                                'cd[content_ids]' => strtr($azione_acquisto['facebook']['content_ids'], $extra_params_values),
                                'cd[value]' => strtr($azione_acquisto['facebook']['value'], $extra_params_values)
                            );

                            if(!empty($operation_details['dati_cliente']['email'])) {
                                $facebook_params['ud[em]'] = hash('sha256', $operation_details['dati_cliente']['email']);
                            }

                            if(!empty($operation_details['dati_cliente']['cap'])) {
                                $facebook_params['ud[zp]'] = hash('sha256', $operation_details['dati_cliente']['cap']);
                            }

                            $urls_to_post[] = $facebook_url . '&' . http_build_query($facebook_params);
                        }

                        // Salvo la vendita su TrackingSuite
                        $array_to_save = array(
                            "user_ref" => $operation_details['cliente'],
                            "user_ip" => $_SERVER['REMOTE_ADDR'],
                            "project_id" => $user_track_codes['project_id'],
                            "track_event" => $azione_acquisto['trackingsuite']['event_name'],
                            "track_ref" => strtr($azione_acquisto['trackingsuite']['event_value'], $extra_params_values),
                            "track_extra" => json_encode(array(
                                'price' => $extra_params_values['{price}'],
                                'currency' => $extra_params_values['{currency}'],
                                'title' => $extra_params_values['{title}'],
                                'category' => $extra_params_values['{category}'],
                                'product_id' => $extra_params_values['{product_id}']
                            )),
                        );

                        $stringForDB = $this->MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
                        $this->MSFrameworkDatabase->pushToDB("INSERT INTO tracking__stats ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
                    }

                    foreach($urls_to_post as $k => $url) {
                        $curl = curl_init($url);
                        curl_setopt($curl, CURLOPT_USERAGENT, $customer_user_agent);
                        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_exec($curl);
                        curl_close($curl);
                    }
                }
            }
        }
    }

    private function prepareStringForJSParam($string) {
        return str_replace("'", "\'", $string);
    }

}
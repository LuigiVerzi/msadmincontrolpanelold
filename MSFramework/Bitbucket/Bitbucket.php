<?php

namespace Bitbucket;

class Bitbucket
{

    private $url = "https://api.bitbucket.org/2.0/";

    private $user = "info@marketingstudio.it";
    private $pwd = "Marketingh24";

    protected $ch = false;

    /**
     * Effettua il login su Bitbucket e prepara alcune configurazioni di default
     */
    public function __construct() {
        ob_start();
        ob_get_contents();

        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_USERPWD, $this->user . ':' . $this->pwd);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
    }

    /**
     * Esegue una chiamata alle API
     *
     * @param $url L'URL al quale effettuare la chiamata (senza "https://api.bitbucket.org/2.0/")
     * @param null $postfields I parametri da passare in POST. Se questo parametro non viene specificato, la chiamata sarà effettuata in GET
     * @param bool $build_query Specifica se preparare i parametri con la funzione http_build_query (in caso contrario prepara un json)
     *
     * @return bool|string
     */
    protected function exec($url, $postfields = null, $build_query = false) {
        curl_setopt($this->ch,CURLOPT_URL,$this->url . $url);

        if($postfields != null) {
            curl_setopt($this->ch, CURLOPT_POST, 1);
            curl_setopt($this->ch,CURLOPT_POSTFIELDS, (!$build_query ? json_encode($postfields) : http_build_query($postfields)));
        } else {
            curl_setopt($this->ch, CURLOPT_POST, 0);
        }

        return curl_exec($this->ch);
    }

    /**
     * Chiude la connessione cUrl
     */
    public function close() {
        curl_close($this->ch);
        ob_end_clean();
    }
}
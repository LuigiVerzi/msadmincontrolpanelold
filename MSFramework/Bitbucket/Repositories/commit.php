<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-08
 */

namespace Bitbucket\Repositories;
use Bitbucket\Bitbucket;

class commit extends Bitbucket {
    private $url;

    public function __construct($repo_slug) {
        parent::__construct();

        $this->url = 'repositories/marketingstudiomainteam/' . $repo_slug . "/src";
    }

    /**
     * Crea un commit
     *
     * @param $content Il contenuto del commit (https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/src#post)
     *
     * @return $this|bool
     */
    public function create($content) {
        if(!is_array($content)) {
            return false;
        }

        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'application/x-www-form-urlencoded'
        ));

        $this->exec($this->url, $content, true);

        return $this;
    }

    /**
     * Effettua un commit creando/aggiornando il file .gitignore con le impostazioni standard
     *
     * @return $this
     */
    public function gitIgnore() {
        $this->create(array(
            ".gitignore" => ".idea/\n.ftpsync_settings/",
            "message" => "Creato .gitignore",
            "author" => "Marketing Studio <info@marketingstudio.it>",
        ));

        return $this;
    }
}
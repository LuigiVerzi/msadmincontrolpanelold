<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-08
 */

namespace Bitbucket\Repositories;
use Bitbucket\Bitbucket;

class repositories extends Bitbucket {
    private $url = 'repositories/marketingstudiomainteam/';

    /**
     * Crea un nuovo repository
     *
     * @param $name Il nome
     * @param $slug Lo slug
     * @param string $project L'ID del progetto
     *
     * @return $this
     */
    public function create($name, $slug, $project = "CCF") {
        $this->exec($this->url . $slug, array(
            'scm'           => 'git',
            'is_private'    => 'true',
            'fork_policy'   => 'no_public_forks',
            'language'   => 'php',
            'name'   => $name,
            'project' =>  array(
                "key" => $project
            )
        ));

        return $this;
    }

    /**
     * Ottiene i dettagli di un repository
     *
     * @param $slug Lo slug del repo
     *
     * @return mixed
     */
    public function get($slug) {
        $r = $this->exec($this->url . $slug);

        return json_decode($r, true);
    }

    /**
     * Elimina un repository
     *
     * @param $slug Lo slug del repo
     *
     * @return mixed
     */
    public function delete($slug) {
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $r = $this->exec($this->url . $slug);

        return json_decode($r, true);
    }

    public function checkSlugAvailability($slug) {
        $check_repo = $this->get($slug);
        if(is_array($check_repo['error']) && strstr($check_repo['error']['message'], "not found")) {
            return true;
        } else {
            return false;
        }
    }

    public function findNextAvailableSlug($slug, $current_counter = 1) {
        $count = $current_counter;

        $new_slug = $slug . '-' . $count;

        if($this->checkSlugAvailability($new_slug)) {
            return $new_slug;
        } else {
            $count++;
            return $this->findNextAvailableSlug($slug, $count);
        }
    }
}
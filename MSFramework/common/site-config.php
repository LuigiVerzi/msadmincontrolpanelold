<?php
$getCleanHost = function($url = '') {
    if($url === '') $url = $_SERVER['HTTP_HOST'];

    return str_replace(
        array("http://", "https://", "www.", ADMIN_URL),
        array("", "", "", ""),
        $url
    );
};

$is_staging = ($getCleanHost() == 'stagingmarketingstudio.it' ? true : false);
$is_saas_staging = ($getCleanHost() == 'saasmarketingstudio.it' ? true : false);

if($is_staging && end(explode("/", dirname( __FILE__ , 3))) != "vStaging") { //sono in staging, ma il file proviene dalla versione in produzione. reincludo il corrispondente file dello staging.
    unset($getCleanHost);
    require(dirname( __FILE__ , 4) . "/vStaging/MSFramework/common/site-config.php");
} else if($is_saas_staging && end(explode("/", dirname( __FILE__ , 3))) != "vSaaSStaging") { //sono in staging, ma il file proviene dalla versione in produzione. reincludo il corrispondente file dello staging.
    unset($getCleanHost);
    require(dirname( __FILE__ , 4) . "/vSaaSStaging/MSFramework/common/site-config.php");
} else {


    if(!defined("MS_DOCUMENT_ROOT")) {
        define("MS_DOCUMENT_ROOT", str_replace(str_replace("www.", "", $_SERVER['HTTP_HOST']), "", $_SERVER['DOCUMENT_ROOT']));
    }

    define("FRAMEWORK_VERSION", ($is_staging ? "Staging" : '1'));
    define("FRAMEWORK_FOLDER", "MSAdminControlPanel/v" . FRAMEWORK_VERSION . "/MSFramework");

    define("REAL_SW_PATH", $_SERVER['DOCUMENT_ROOT'] . "/"); //questa costante viene sfruttata in framework-config!

    define("PATH_TO_FRAMEWORK", MS_DOCUMENT_ROOT . "/" . FRAMEWORK_FOLDER . "/");
    require(PATH_TO_FRAMEWORK . "framework-config.php");

    unset($getCleanHost);
}
?>
<?php
/**
 * MSFramework
 * Date: 23/06/18
 */

// IL CONTENUTO DI QUESTO FILE VIENE INCLUSO PRIMA DELLA CHIUSURA DEL TAG <BODY> DI TUTTI I SITI CHE SI APPOGGIANO AL FRAMEWORK!
$modules_files = $MSFrameworkModules->executeFunctions('footer');
foreach($MSFrameworkModules->getModulesFiles('footer') as $type => $files) {
    if($type == 'css') {
        foreach($files as $url) echo '<link href="' . $url . '" rel="stylesheet">';
    }
    if($type == 'js') {
        foreach($files as $url) echo '<script src="' . $url . '" ' . (strpos($url, FRAMEWORK_COMMON_CDN) !== false ? 'crossorigin="anonymous"' : '') . '></script>';
    }
    if($type == 'html') {
        foreach($files as $url) {
            if(file_exists($url)) include($url);
            else echo $url;
        }
    }
}

if($MSFrameworki18n->checkPoFileExists()) { //se il file delle traduzioni esiste ed è popolato, carico la classe gettext ed il relativo file di traduzioni
?>
    <link rel="gettext" type="application/x-po" href='<?php echo $MSFrameworki18n->getCurLangPoPath(true) ?>' />
    <script type="text/javascript" src="<?php echo FRAMEWORK_COMMON_CDN ?>js/lib/gettext.js" crossorigin="anonymous"></script>
    <script type="text/javascript">
        try {
            var gt = new Gettext(
                {'domain': 'messages'}
            );
        } catch(e) {
            var gt = {
                Gettext: function(str) {
                    return str;
                }
            }
        }
    </script>
<?php
}  else { //altrimenti istanzio un oggetto fittizio di gettext per gestire eventuali richiami nel codice JS del sito alla classe
?>
    <script>
        var gt = {
            Gettext: function(str) {
                return str;
            }
        }
    </script>
<?php
}

?>

<?php
$gmaps_code = $MSFrameworkCMS->getCMSData('producer_config')['gmaps_apikey'];
if(!empty($gmaps_code)) { ?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo trim(($gmaps_code)); ?>&libraries=places<?= ($_GET['mapsCallback'] != "" ? '&callback=' . $_GET['mapsCallback'] : '') ?>"></script>
<?php } ?>

<script>
    <?= $MSFrameworkCMS->getCMSData('site')['additional_js'] ?>
</script>

<?php
// Se il sorgente non è stato ancora elaborato (Quindi ci troviamo su un sito standard) eseguo una formattazione parziale.
if(!is_null($MSFrameworkParser)) {
    register_shutdown_function(function () {
        Global $MSFrameworkParser;

        $content = ob_get_clean();

        /* Aggiungo jQuery se non è stato trovato */
        $jquery_check = preg_match('/src="(.*?)jquery(.min){0,1}.js"/mi', $content);
        if (!$jquery_check) {
            $pos = strpos($content, '<script');
            $jquery_include_script = '<script type="text/javascript" src="' . FRAMEWORK_COMMON_CDN . 'js/lib/jquery.min.js" crossorigin="anonymous"></script>' . PHP_EOL;
            if ($pos !== false) {
                $content = substr_replace($content, $jquery_include_script, $pos, 0);
            }
        }

        $MSFrameworkParser->mode = 'partial';
        $MSFrameworkParser->source = $content;
        $MSFrameworkParser->parse();
    });
}
?>
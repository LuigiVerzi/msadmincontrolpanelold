<?php
/**
 * MSFramework
 * Date: 23/06/18
 */

//IL CONTENUTO DI QUESTO FILE VIENE INCLUSO NEL TAG <HEAD> DI TUTTI I SITI CHE SI APPOGGIANO AL FRAMEWORK!

$currentLangDetails = $MSFrameworki18n->getLanguagesDetails($MSFrameworki18n->user_selected_lang)[$MSFrameworki18n->user_selected_lang];

$siteCMSData = $MSFrameworkCMS->getCMSData('site');
$site_logos = json_decode($siteCMSData['logos'], true);

?>

<link rel="canonical" href="<?php echo $GLOBALS['CANONICAL_URL']; ?>"/>

<?php
if(isset($GLOBALS['CANONICAL_PREV'])) echo '<link rel="prev" href="' . $GLOBALS['CANONICAL_PREV'] . '">' . PHP_EOL;
if(isset($GLOBALS['CANONICAL_PREV'])) echo '<link rel="next" href="' . $GLOBALS['CANONICAL_NEXT'] . '">' . PHP_EOL;
?>


<?php
foreach($MSFrameworki18n->getActiveLanguagesDetails() as $menuLangK => $altmenuLangV) {
    $cur_url_dets = $MSFrameworki18n->getLanguagesDetails($altmenuLangV['id'], "long_code")[$altmenuLangV['id']];
    $slug_to_use = "";
    if($page_details_from_slug['i18n'][$cur_url_dets['long_code']] != "") {
        $slug_to_use = $page_details_from_slug['i18n'][$cur_url_dets['long_code']] . "/";
    }
?>
    <link rel="alternate" hreflang="<?= $altmenuLangV['url_code'] ?>" href="<?= (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . '/' . (new \MSFramework\url())->getLangPrefix($altmenuLangV['id']) . $slug_to_use ?>" />
<?php
}
?>

<?php if(!empty($site_logos['favicon'])) { ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['favicon'] ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?= UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['favicon'] ?>">
<?php } ?>

<style media="screen" type="text/css">
    <?php echo $MSFrameworkCMS->getCMSData('site')['additional_css'] ?>
</style>

<?php

// Include le funzionalità dei moduli
$MSFrameworkModules->executeFunctions('header');
foreach($MSFrameworkModules->getModulesFiles('header') as $type => $files) {
    if($type == 'css') {
        foreach($files as $url) echo '<link href="' . $url . '" rel="stylesheet">';
    }
    if($type == 'js') {
        foreach($files as $url) echo '<script src="' . $url . '" ' . (strpos($url, FRAMEWORK_COMMON_CDN) !== false ? 'crossorigin="anonymous"' : '') . '></script>';
    }
    if($type == 'html') {
        foreach($files as $url) include($url);
    }
}
?>
<script>
    window.base_url = '<?= $MSFrameworkCMS->getURLToSite(); ?>';
    window.cdn_url = '<?= FRAMEWORK_COMMON_CDN; ?>';
    window.current_lang_flag_code = '<?= strtoupper($currentLangDetails["short_code"]); ?>';
    window.current_lang_code = '<?= $currentLangDetails["long_code"]; ?>';
    window.current_lang_id = '<?= $MSFrameworki18n->user_selected_lang; ?>';
    window.current_currency = <?= json_encode($MSFrameworkCurrencies->getCurrentCurrencyDetails()); ?>;

    window.current_page_action = '<?= $MSFrameworkUrl->currentSlugDetails['action']; ?>';
    window.current_page_id = '<?= $MSFrameworkUrl->currentSlugDetails['id']; ?>';
</script>
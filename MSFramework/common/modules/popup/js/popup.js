function initMSPopup(id) {

    $('#ms-popup-' + id + ' .ms-popup-container')
        .removeClass($('#ms-popup-' + id + ' .ms-popup-container').data('in'))
        .removeClass($('#ms-popup-' + id + ' .ms-popup-container').data('out'))
        .addClass('animate')
        .addClass($('#ms-popup-' + id + ' .ms-popup-container').data('in'));

    if($('#ms-popup-' + id + ' .ms-popup-container .owl-carousel').length) {
        setTimeout(function () {
            var owl = $('#ms-popup-' + id + ' .ms-popup-container .owl-carousel').data('owlCarousel');
            owl.onResize();
        }, 100);
    }

    $('#ms-popup-' + id).show().one('click', '.ms-popup-close', function (e) {
        e.preventDefault();
        document.cookie = "ms_onExitPopup_" + id + "_closed=1; path=/";
        $('#ms-popup-' + id + ' .ms-popup-container')
            .removeClass($('#ms-popup-' + id + ' .ms-popup-container').data('in'))
            .addClass($('#ms-popup-' + id + ' .ms-popup-container').data('out'));
        $('#ms-popup-' + id).fadeOut(function () {
            if($('#ms-popup-' + id).find('iframe').length) {
                var $cloned = $('#ms-popup-' + id).find('iframe').clone();
                $('#ms-popup-' + id).find('iframe').replaceWith($cloned);
            }
        });
    });
}

function initMSPopupOnExit(id) {
    var printOnExitPopup = function(e){
        if( e.clientY < 0 && e.target.nodeName.toLowerCase() !== "select") {
            initMSPopup(id);
            document.removeEventListener("mouseout", printOnExitPopup);
        }
    };

    document.addEventListener("mouseout", printOnExitPopup, false);
}
<?php
$siteCMSData = (new \MSFramework\cms())->getCMSData('site');
$site_logos = json_decode($siteCMSData['logos'], true);

$loggedUser = $MSFrameworkUsers->getUserDataFromSession();

$ticket = $MSFrameworkDatabase->getAssoc("SELECT * FROM richieste WHERE form_data LIKE :id", array(':id' => '%' . $ticket_id . '%'), true);

if($ticket) {
    $form_data = json_decode($ticket['form_data'], true);
    $ticket_info = array(
        'nome' => $ticket['nome'],
        'email' => $ticket['contatto'],
        'provenienza' => $ticket['messaggio'],
        'quantita' => $form_data['Quantità'],
        'stato' => $form_data['Stato'],
        'biglietto' => $form_data['Biglietto'],
        'evento' => $form_data['Evento'],
        'data' => $ticket['creation_time']
    );
}

if(isset($_GET['mark']) && $_GET['mark'] == 1 && $loggedUser) {
    $form_data['Stato'] = 0;
    $return = (new MSFramework\database())->pushToDB("UPDATE richieste SET form_data = :form_data WHERE id = :id", array(":form_data" => json_encode($form_data), ":id" => $ticket['id']));
    die($return);
}

?>

<!DOCTYPE html>
<html lang="it">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="MarketingStudio.it">

    <title>Biglietto #<?= $ticket_id; ?></title>

    <!-- CSS -->
    <link href="<?= FRAMEWORK_COMMON_CDN; ?>modules/ticket/css/check.css" rel="stylesheet">

    <?php
    require(FRAMEWORK_ABSOLUTE_PATH . 'common/includes/header.php');
    ?>
</head>

<body>
<div id="ticket_container">
    <div class="logo">
        <img src="<?= UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo']; ?>" alt="logo">
    </div>
    <?php if($ticket) { ?>
        <div class="ticket_box">
            <h1><?= $ticket_info['nome']; ?></h1>
            <h2>Per <?= $ticket_info['quantita']; ?> person<?= ($ticket_info['quantita'] == 1 ? 'a' : 'e'); ?></h2>
            <span class="status <?= ($ticket_info['stato'] == 1 ? 'Attivo' : 'Inattivo'); ?>"><?= ($ticket_info['stato'] == 1 ? 'Biglietto valido' : 'Biglietto già utilizzato'); ?></span>
            <hr>
            <span class="ticket"><b>Ticket:</b> <?= $ticket_info['biglietto']['nome']; ?> - <?= $ticket_info['biglietto']['prezzo']; ?><?= CURRENCY_SYMBOL; ?></span>
            <br>
            <span class="date"><b>Acquistato Il:</b> <?= date('d/m/Y H:i', $ticket_info['data']); ?></span>
            <br>
            <span><b>Provenienza:</b> <?= $ticket_info['provenienza']; ?></span>
            <br>
            <span><b>Email:</b> <?= $ticket_info['email']; ?></span>
        </div>
        <?php if($loggedUser && $ticket_info['stato'] == 1) { ?>
            <a href="" id="convalida">Contrassegna come <i>Utilizzato</i></a>
        <?php } ?>
    <?php } else { ?>
        <div class="ticket_box">
            <h1 style="text-align: center; margin: 10px auto;">Biglietto Non Trovato</h1>
            <span class="status">
           Ticket non esistente
        </span>
        </div>
    <?php } ?>
</div>
</body>

<script src="<?= ABSOLUTE_SW_PATH_HTML; ?>vendor/jquery/jquery.min.js"></script>
<?php require(FRAMEWORK_ABSOLUTE_PATH . 'common/includes/footer.php'); ?>
</html>

<script>
    $(document).ready(function () {
        $('#convalida').on('click', function (e) {
            e.preventDefault();
            swal({
                title: 'Vuoi impostare il seguente biglietto come \'Utilizzato\'?',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(willDelete) {
                if (willDelete) {
                    $.ajax({
                        url: window.base_url + 'check/<?= $ticket_id; ?>?mark=1',
                        type: 'POST',
                        dataType: 'html',
                        success: function (cart) {
                            swal('Il ticket (<?= $ticket_id; ?>) è stato impostato \'Inattivo\'', {
                                icon: "success",
                            }).then(() => {
                                location.reload();
                            });
                        }
                    });
                }
            });

        });
    });
</script>
var start = new Date();

$(document).ready(function () {

    $(window).on("unload", function(e) {
        var end = new Date();

        $.ajax({
            url: window.base_url + 'm/timeonsitetracker',
            type: 'POST',
            async: false,
            data: {
                action: 'saveTimeSpent',
                time: (end.getTime() - start.getTime())/1000,
                reference: {type: window.current_page_action, id: window.current_page_id}
            }
        });
    });

});
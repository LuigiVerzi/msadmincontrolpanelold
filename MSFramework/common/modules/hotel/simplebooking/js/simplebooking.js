var SBParameters = {
    CodLang:  window.current_lang_flag_code
};


if($('.simplebooking_offer_request').length) {
    $('.simplebooking_offer_request').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);

        $form.find('.awe-calendar').each(function () {
            if($(this).val().length <= 0) {
                $(this).addClass('error');
                $(this).one('focus focusin', function () {
                    $(this).removeClass('error');
                });
            }
        });

        if($form.find('.error').length) {
            return false;
        }

        var offer = $form.find('.simplebooking_offer_id').val();
        var ospiti = formatGuestForSimpleBooking($form.find('.simplebooking_ospiti').val());
        var from = italianDateToInternationFormat($form.find('.simplebooking_from').val());
        var to = italianDateToInternationFormat($form.find('.simplebooking_to').val());

        location.href = "https://www.simplebooking.it/ibe/hotelbooking/search?hid=" + window.sbSearch.Parameters.HotelId + "&lang=IT#/packageNew&pkgId=" + offer + "&type=&q&guests=" + ospiti +  "&in=" + from + "&out=" + to + "&coupon="

        return true;
    });
}

function italianDateToInternationFormat(date){
    var parts = date.split("/");
    return parts[2] + "-" + parts[1] + "-" + parts[0];
}

function formatGuestForSimpleBooking(guests){
    var guest_array = [];
    for (i = 0; i < guests; i++) {
        guest_array.push('A');
    }
     return guest_array.join(',');
}
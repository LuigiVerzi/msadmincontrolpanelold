<?php $footerbar_color = $MSFrameworkModules->getSettingValue('footerbar', 'color'); ?>
<?php $footerbar_link_prenotazione = $MSFrameworkModules->getSettingValue('footerbar', 'link_prenotazione'); ?>
<?php $footerbar_usa_simplebooking = $MSFrameworkModules->getSettingValue('footerbar', 'use_simplebooking'); ?>
<?php $footerbar_pulsante_prenotazione = $MSFrameworkModules->getSettingValue('footerbar', 'testo_prenotazione'); ?>
<?php $footerbar_icona_prenotazione = $MSFrameworkModules->getSettingValue('footerbar', 'icona_prenotazione'); ?>
<style>
    #footer-bar {
        background-color: <?= $footerbar_color; ?>
    }
    #footer-bar .mb_mail {
        color: <?= $footerbar_color; ?>;
    }
</style>

<?php
if($footerbar_usa_simplebooking) {
    $simplebooking_id = $MSFrameworkModules->getSettingValue('simplebooking', 'simplebooking_id');
    if($simplebooking_id) {
        $menuLangV = $MSFrameworki18n->getLanguagesDetails($MSFrameworki18n->user_selected_lang)[$MSFrameworki18n->user_selected_lang];
        $footerbar_link_prenotazione = 'https://www.simplebooking.it/ibe/hotelbooking/search?hid=' . $simplebooking_id . '&lang=' . $menuLangV['short_code'];
    }
}

$siteCMSData = $MSFrameworkCMS->getCMSData('site');

?>

<!-- BEGIN / MOBILE BAR -->
<div id="footer-bar">
    <a class="actioncall" href="tel:<?php echo !empty($siteCMSData['telefono']) ? $siteCMSData['telefono'] : $siteCMSData['cellulare']; ?>">
        <span><i class="fa fa-phone" aria-hidden="true"></i></span>
        <span class="mobilebar-align">Chiama</span>
    </a>
    <a class="actionbook" href="<?= $footerbar_link_prenotazione; ?>">
        <span class="mobilebar-align"><?= $footerbar_pulsante_prenotazione; ?></span>
        <span><i class="<?= $footerbar_icona_prenotazione; ?>" aria-hidden="true"></i></span>
    </a>
    <div id="footer-bar_center">
        <a class="mb_mail" href="mailto:<?php echo $siteCMSData['email']; ?>">
            <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
        </a>
    </div>
    <div id="footer-bar_round"></div>
</div>
<!-- END / MOBILE BAR -->
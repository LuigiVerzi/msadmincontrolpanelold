var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

eventer(messageEvent,function(event) {
    if(event.data.cmd === 'initVisualSelector') {
        initVisualSelector(event.data.preferences);
    }
}, false);

/* DISABILITA TUTTI I LINK ESTERNI */
jQuery('a').filter(function() {
    return (this.hostname && this.hostname !== location.hostname) || jQuery(this).attr('target') === "_blank";
}).attr("href", "#");

function initVisualSelector(preferences) {

    if(typeof(preferences.include_css) !== 'undefined') {
        jQuery("head").append('<style>' + preferences.include_css + '</style>');
    }

    var mousedownTimer = false;

    var onSelectionFunction = function (target) {

        var selector = jQuery(target).getSelector(true, 3, (typeof (preferences.limit_tags) !== 'undefined' ? preferences.limit_tags : []));

        var is_form = 0;
        var $form_element = (jQuery(target)[0].attrName === 'FORM' ? jQuery(target) : jQuery(target).closest('form'));
        var extra = {};

        if($form_element.hasClass('.MSForm')) {
            is_form = $form_element.data('form_id');
        }

        if($form_element.length) {
            var form_fields = [];
            $form_element.find('[name]').each(function() {
                form_fields.push(jQuery(this).attr('name'));
            });
            extra.form_fields = form_fields;
        }

        parent.postMessage(JSON.parse(JSON.stringify({
            cmd: "onSelectionFunction",
            selector: selector,
            is_form: is_form,
            extra: extra
        })),"*");

    };

    jQuery('body').on('contextmenu', function (event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        onSelectionFunction(event.target);

    }).on('mousedown', function (event) {

        event.preventDefault();
        event.stopImmediatePropagation();
        event.stopPropagation();

        var $button = jQuery(event.target);
        mousedownTimer = setTimeout(function () {
            onSelectionFunction(event.target);
            $button.addClass('disable_temp');
        }, 1000);
    }).on('mouseup', function (event) {
        if (mousedownTimer !== false) {
            clearTimeout(mousedownTimer);
            mousedownTimer = false;
        }
    }).on('click', function (event) {

        var $button = jQuery(event.target);

        if ($button.hasClass('disable_temp')) {
            event.preventDefault();
            event.stopImmediatePropagation();
            event.stopPropagation();
            $button.removeClass('disable_temp');
            return false;
        }
    });

    window.addEventListener('beforeunload', function (e) {
        parent.postMessage(JSON.parse(JSON.stringify({
            cmd: "onIframeUnload"
        })),"*");
    });

    parent.postMessage(JSON.parse(JSON.stringify({
        cmd: "connected",
        url: location.href
    })),"*");
}

/* FUNZIONE PER OTTENERE IL SELETTORE */
!(function ($, undefined) {

    var get_selector = function (element, max_parents, tag_limit) {
        var pieces = [];
        var current_parent = 0;
        var is_ok = false;
        var tag_found = true;

        if(tag_limit.length > 0) {
            tag_found = false;
        }

        for (; (element && element.tagName !== undefined && current_parent < max_parents && !is_ok); element = element.parentNode) {

            var currently_pieces = [];

            if(tag_limit.length > 0) {
                if(tag_limit.indexOf(element.tagName) >= 0) {
                    tag_found = true;
                }
            }

            if(tag_found) {
                if (element.id && !/\s/.test(element.id)) {
                    if (current_parent < 2 && element.tagName === 'A') {
                        return '#' + element.id;
                    }
                    currently_pieces.unshift('#' + element.id);
                    is_ok = true;
                } else {
                    if (element.className) {
                        var classes = element.className.split(' ');
                        for (var i in classes) {
                            if (classes.hasOwnProperty(i) && classes[i] && classes[i].indexOf('col-') < 0 && classes[i].indexOf('msVisualElement') < 0 && classes[i].indexOf('mb-') < 0 && classes[i] !== 'row') {
                                currently_pieces.unshift('.' + classes[i]);
                            }
                        }
                    } else if (element.name) {
                        currently_pieces.unshift('[name="' + element.name + '"]');
                    }
                }

                if (currently_pieces.length > 0 && tag_found) {
                    currently_pieces.unshift(' ');
                    current_parent++;
                    pieces = currently_pieces.concat(pieces);
                }
            }

        }

        return pieces.slice(1).join('');
    };

    jQuery.fn.getSelector = function (only_one, max_parent, tag_limit) {
        if(typeof(max_parent) == 'undefined') max_parent = 10;
        if(typeof(tag_limit) == 'undefined') tag_limit = [];
        if (true === only_one) {
            return get_selector(this[0], max_parent, tag_limit);
        } else {
            return jQuery.map(this, function (el) {
                return get_selector(el, max_parent, tag_limit);
            });
        }
    };

})(window.jQuery);
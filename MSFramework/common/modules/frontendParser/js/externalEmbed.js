jQuery(document).ready(function () {

    var $FW360_Elements = jQuery("*").filter(function(){
        return /^.*ms-.*$/i.test(this.nodeName) && !/^.*ms-.*$/i.test(jQuery(this).parent()[0].tagName);
    });

    if($FW360_Elements.length) {
        MS_Parser_ParseElements($FW360_Elements);
    }

});

function MS_Parser_ParseElements($FW360_Elements) {
    var elementsToReplace = {};
    $FW360_Elements.each(function () {
        var uniqueID = 'MSReplace_' + (new Date().getUTCMilliseconds());
        jQuery(this).addClass(uniqueID);
        elementsToReplace[uniqueID] = jQuery('<div>').append($(this).clone()).html();
    });

    jQuery.ajax({
        url: window.FW360_SaaS_Url + 'm/frontend_parser/' + window.FW360_SaaS_Environment,
        type: 'POST',
        data: {
            action: 'getElementsHTML',
            elements: elementsToReplace
        },
        dataType: 'json',
        success: function (data) {

            Object.keys(data['elements']).forEach(function (replaceClass) {
                jQuery('.' + replaceClass).replaceWith(data['elements'][replaceClass]);
            });

            jQuery('body').append(data['dependencies']);

            if(typeof(MS_reDrawFrontEnd) === 'function') {
                MS_reDrawFrontEnd();
            }
        }
    });

}
$(document).ready(function () {
    MS_reDrawFrontEnd();

    /* Attacco il carosello */
    $('.ms-carousel').each(function () {
        MS_AttachCarousel($(this), $(this).data('carousel-settings'));
    });
});

function MS_reDrawFrontEnd() {
    /* Sistema la larghezza delle righe */
    $(window).off('resize', MS_reDrawFullWidthRows).on('resize', MS_reDrawFullWidthRows);
    MS_reDrawFullWidthRows();
    MS_initTabs();
}

function MS_reDrawFullWidthRows() {
    $('.ms-row.ms-full-width').each(function () {
        $(this).css('margin-left', '0px').css('margin-right', '0px').css('width', '').css('float', 'none');

        var content_width = $(this).width();
        content_width += (parseFloat($(this).css('border-left-width')) + parseFloat($(this).css('border-right-width'))); // Aggiungo la larghezza di eventuali bordi

        $(this).css('margin-left', '-' + ($(this).offset().left - parseInt($('body').css('padding-left'))) + 'px').css('width', $('body').width());

        if($(this).find('.ms-row-container').length) {
            $(this).find('.ms-row-container').width(content_width).css('margin', 'auto');
        }
    });
}

function MS_initTabs() {
    $('.ms-tabs').each(function () {
        var $currentNav = $(this);
        var $tabsNav = $('<ul class="ms-tab-nav"></ul>').prependTo($(this));
        $(this).children('.ms-tab-element').each(function () {
            var $title = $(this).children('.ms-tab-title');
            var $content = $(this).children('.ms-tab-content');

            var $navLink = $('<li><a href="#" class="ms-tab-link">' + $title.html() + '</a></li>').prependTo($tabsNav);
            $navLink.on('click', function (e) {
                e.preventDefault();
                $currentNav.find('.ms-tab-active').removeClass('ms-tab-active');
                $(this).addClass('ms-tab-active');
                $content.parent().addClass('ms-tab-active');
            });
            $title.remove();
        });

        $(this).find('.ms-tab-nav > li:first-child').click();
    });
}

/* INPUT */
function MS_Input_setError($elem) {
    $elem.addClass('error').one('change', function () {
       $(this).removeClass('error');
    });
}

/* UTILS */
function MS_PriceFormat(number, decimals, dec_point, thousands_sep, no_price_string) {
    if (typeof (no_price_string) === 'undefined') no_price_string = '';

    number = number * 1;

    if (number > 0) {
        var str = number.toFixed(decimals ? decimals : 0).toString().split('.');
        var parts = [];
        for (var i = str[0].length; i > 0; i -= 3) {
            parts.unshift(str[0].substring(Math.max(0, i - 3), i));
        }
        str[0] = parts.join(thousands_sep ? thousands_sep : ',');
        return str.join(dec_point ? dec_point : '.');
    } else {
        return no_price_string;
    }
}

function MS_AttachCarousel($elem, carousel_settings) {

    $elem.addClass('owl-carousel');

    var options = {
        items: carousel_settings.cols,
        dots: carousel_settings.dots == 1,
        loop: carousel_settings.loop == 1,
        singleItem: true,
        mouseDrag: true,
        touchDrag: true,
        responsive: {
            0: {
                items: carousel_settings['sm-cols'],
            },
            768: {
                items: carousel_settings['md-cols'],
            },
            992: {
                items: carousel_settings['cols'],
            }
        }
    };

    if(carousel_settings.autoplay == 1) {
        options.autoplay = true;
        options.autoplayTimeout = 6000;
        options.autoplaySpeed = 1000;
        options.autoplayHoverPause = false;
    }

    $elem.owlCarousel(options);
}
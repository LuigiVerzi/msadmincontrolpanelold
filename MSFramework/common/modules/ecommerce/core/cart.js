window.MS_Ecommerce_Endpoint = window.base_url + 'm/ecommerceCore/';

$(document).ready(function () {
    $('.ms-product-variations').on('change', 'select:not([data-id="qty"])', function () {
        MS_Ecommerce_Product_CheckQuantity($(this).closest('.ms-product-variations').data('id'));
    });

    $('.ms-product-variations').each(function () {
        if(!$(this).find('select:not([data-id="qty"])').length) {
            MS_Ecommerce_Product_CheckQuantity($(this).data('id'));
        }
    });
});

function MS_Ecommerce_Cart_Refresh() {
    $('.ms-shortcode-element.ms-cart-widget').each(function () {
        var $cartWidget = $(this);
        var cartTPL = $(this).find('.ms-ecommerce-cart-tpl').val();

        // TODO: Per adesso riaggiorno la pagina ma dovrebbe riaggiornare solo il carrello
        location.reload();
    });

    location.reload();
}

function MS_Ecommerce_Cart_AddToCart(button, product_id, product_type) {

    var $button = $(button);

    var original_content = $button.html();
    var variations = [];
    var quantity = 1;

    $button.addClass('ms-btn-loading');

    var addToCart_errors = false;
    if($('.ms-product-variations[data-id="' + product_id + '"]').length) {
        variations = MS_Ecommerce_Product_GetSelectedVariations(product_id);

        variations.forEach(function (variation) {
            if(!variation.value.length) {
                MS_Input_setError($('.ms-product-variations[data-id="' + product_id + '"] .ms-product-variation-select[data-id="' + variation.id + '"]'));
                addToCart_errors = true;
            }
        });

        quantity = $('.ms-product-variations[data-id="' + product_id + '"] select[data-id="qty"]').val();
    }

    if(addToCart_errors) {
        swal('Per aggiungere l\'articolo al carrello compila prima le variazioni richieste', {
            icon: 'error',
            animation: false
        });
    } else  {

        if(quantity < 1) {
            swal('Per aggiungere l\'articolo al carrello seleziona prima la quantità richiesta', {
                icon: 'error',
                animation: false
            });

            return false;
        }

        $.ajax({
            url: window.MS_Ecommerce_Endpoint + 'cart/',
            type: 'POST',
            data: {
                action: 'addToCart',
                id: product_id,
                variations: variations,
                my_cart: (MS_Utility_Store('cart') ? MS_Utility_Store('cart') : []),
                quantity: quantity
            },
            dataType: 'json',
            success: function (data) {
                MS_Utility_Store('cart', JSON.stringify(data.products));

                $button.removeClass('ms-btn-loading');

                if(data.status == 'insert' || data.status == 'updated') {
                    swal(data.status_message, {icon: 'success', animation: false}).then(function(value) {
                        MS_Ecommerce_Cart_Refresh();
                    });
                }
                else if(data.status == 'no_avaialable') {
                    swal(data.status_message, {icon: 'error', animation: false}).then(function(value) {
                        MS_Ecommerce_Cart_Refresh();
                    });
                }
            }
        });
    }
}

function MS_Ecommerce_Cart_Remove(product_id) {
    $.ajax({
        url: window.MS_Ecommerce_Endpoint + 'cart/',
        type: 'POST',
        data: {
            action: 'removeFromCart',
            id: product_id
        },
        dataType: 'json',
        success: function (data) {

            MS_Utility_Store('cart', JSON.stringify(data.data.products));
            swal(data.message, {
                icon: "success", animation: false
            }).then(function(value) {
                MS_Ecommerce_Cart_Refresh();
            });

        }
    });
}

function MS_Ecommerce_Product_GetSelectedVariations(product_id) {
    var variations = [];

    $('.ms-product-variations[data-id="' + product_id + '"] select:not([data-id="qty"])').each(function () {
        variations.push({
            id: $(this).data('id'),
            value: $(this).val()
        });
    });

    return variations;
}

function MS_Ecommerce_Product_CheckQuantity(product_id) {
    if($('.ms-product-variations[data-id="' + product_id + '"] select[data-id="qty"]').length) {
        var currentVariations = MS_Ecommerce_Product_GetSelectedVariations(product_id);

        $.ajax({
            url: window.MS_Ecommerce_Endpoint + 'cart/',
            type: 'POST',
            data: {
                action: 'getProductVariations',
                id: product_id,
                variations: currentVariations
            },
            dataType: 'json',
            success: function (data) {

                var quantity = data.warehouse_qty;
                var select_content = '';

                for (i = 1; i <= quantity; i++) {
                    select_content += '<option value="' + i + '"' + (i === 1 ? ' selected' : '') + '>' + i + '</option>';
                }

                $('.ms-product-variations[data-id="' + product_id + '"] select[data-id="qty"]').html(select_content);

            },
            error: function (err) {

            }
        });

    }

}

function MS_Utility_Store(key, value) {

    var lsSupport = false;

    // Check for native support
    if (localStorage) {
        lsSupport = true;
    }

    // If value is detected, set new or modify store
    if (typeof value !== "undefined" && value !== null) {
        // Convert object values to JSON
        if ( typeof value === 'object' ) {
            value = JSON.stringify(value);
        }
        // Set the store
        if (lsSupport) { // Native support
            localStorage.setItem(key, value);
        } else { // Use Cookie
            createCookie(key, value, 30);
        }
    }

    // No value supplied, return value
    if (typeof value === "undefined") {
        // Get value
        if (lsSupport) { // Native support
            data = localStorage.getItem(key);
        } else { // Use cookie
            data = readCookie(key);
        }

        // Try to parse JSON...
        try {
            data = JSON.parse(data);
        }
        catch(e) {
            data = data;
        }

        return data;

    }

    // Null specified, remove store
    if (value === null) {
        if (lsSupport) { // Native support
            localStorage.removeItem(key);
        } else { // Use cookie
            createCookie(key, '', -1);
        }
    }

    /**
     * Creates new cookie or removes cookie with negative expiration
     * @param  key       The key or identifier for the store
     * @param  value     Contents of the store
     * @param  exp       Expiration - creation defaults to 30 days
     */

    function createCookie(key, value, exp) {
        var date = new Date();
        date.setTime(date.getTime() + (exp * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
        document.cookie = key + "=" + value + expires + "; path=/";
    }

    /**
     * Returns contents of cookie
     * @param  key       The key or identifier for the store
     */

    function readCookie(key) {
        var nameEQ = key + "=";
        var ca = document.cookie.split(';');
        for (var i = 0, max = ca.length; i < max; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

}
function attachStripePaymentsToButton(button_id, order_id, totale, site_name, public_key, logo, ipn_url) {

    if(typeof(ipn_url) == 'undefined' || ipn_url === "") {
        ipn_url = window.base_url + 'm/payments';
    }

    var handler = StripeCheckout.configure({
        key: public_key,
        image: logo,
        locale: 'auto',
        token: function(data) {

            data.pay_with_stripe = 1;
            data.total = totale;
            data.order_id = order_id;
            
            $.ajax({
                url: ipn_url,
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if(data.status == 'ok') {
                        window.location.reload();
                    }
                    else if(data.status == 'transaction_error') {
                        swal({
                            title: data.message,
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        });
                    }
                    else if(data.status == 'order_not_found') {
                        swal({
                            title: data.message,
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        });
                    }
                }
            });

        }
    });

    document.getElementById(button_id).addEventListener('click', function(e) {
        // Open Checkout with further options:
        handler.open({
            name: site_name,
            description: 'Payment for Order #' + order_id,
            currency: 'eur',
            amount: totale
        });
        e.preventDefault();
    });

    window.addEventListener('popstate', function() {
        handler.close();
    });

    if($('#' + button_id).hasClass('autoclick')) {
        handler.open({
            name: site_name,
            description: 'Ordine N#' + order_id,
            currency: 'eur',
            amount: totale
        });
    }
}
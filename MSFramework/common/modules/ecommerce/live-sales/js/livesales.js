'use strict';
jQuery(document).ready(function () {
    initLiveSales(window.live_sales_settings);
});

function initLiveSales(info) {
    info.call_n = 0;
    info.liveSalesInterval = setInterval(function () {
        if(typeof(window.live_sales_orders[info.call_n]) !== 'undefined') {

            var purchase = window.live_sales_orders[info.call_n];
            if (Object.keys(purchase).length) {
                toastr["info"](purchase.content, purchase.title, {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-left liveSalesPopup",
                    "preventDuplicates": true,
                    "onclick": null,
                    "showDuration": "3000",
                    "hideDuration": "1000",
                    "timeOut": info.duration,
                });
                if (info.play_sound) new Audio(info.cdn_url + 'modules/ecommerce/live-sales/audio/notification.mp3').play();
            }
            if (info.call_n >= info.total_per_page) {
                clearInterval(info.liveSalesInterval);
            } else {
                info.call_n++;
            }
            
        }
    }, info.interval);
}



window.addEventListener('error', function(event) {
    if(event.type === 'error') {
        $.ajax({
            url: window.base_url + 'm/error_tracker?type=js',
            type: 'POST',
            data: {
                type: 'js',
                msg: event.message,
                url: event.filename,
                line: event.lineno,
                col: event.colno,
                error: event.message
            }
        });
    }
});

/*
window.onerror = function(msg, url, line, col, error) {
    if(msg.length) {
        $.ajax({
            url: window.base_url + 'm/error_tracker?type=js',
            type: 'POST',
            data: {
                type: 'js',
                msg: msg,
                url: url,
                line: line,
                col: col,
                error: error
            }
        });
    }
    return false;
};
*/
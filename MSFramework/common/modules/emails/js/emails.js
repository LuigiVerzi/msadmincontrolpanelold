if($('.section-check-availability').length) {
    $('.mb_book').on('click', function () {
        if($('.section-check-availability').hasClass('open')) {
            $('.section-check-availability').removeClass('open');
            $('.mb_book i').attr('class', 'fa fa-bed');
        }
        else {
            $('.section-check-availability').addClass('open');
            $('.mb_book i').attr('class', 'fa fa-close');
        }
    });
}

$("#form_contatti").on("submit", function(e) {
    e.preventDefault();

    var data = {};
    $.each($(this).serializeArray(), function() {
        data[this.name] = this.value;
    });
    data['action'] = 'contact-form';
    $('#form_contatti input, #form_contatti textarea, #form_contatti button').attr('disabled', 'disabled');

    window.form_data = data;

    // Se è presente l'invisible reCaptcha allora ottengo il token prima di inviare altrimenti invio direttamente
    if($('.g-recaptcha').length && $('.g-recaptcha').data('size') == 'invisible') {
        if(grecaptcha.getResponse() == "") grecaptcha.execute();
        else formContattiSubmit(data);
    }
    else
    {
        formContattiSubmit(data);
    }

});

function formContattiSubmit(data) {

    var recaptcha_obj = new Object();
    if($('#g-recaptcha-response').length != 0) {
        recaptcha_obj = grecaptcha.getResponse();
        data['g-recaptcha-response'] = recaptcha_obj;
    }

    $.ajax({
        url: window.base_url + 'm/emails',
        type: 'POST',
        data: data,
        dataType: "json",
        success: function(data){
            if(data.result == "mandatory_data_missing") {
                swal(data.return_message, {
                    icon: "warning",
                });
            } else if(data.result == "mail_not_ok") {
                swal(data.return_message, {
                    icon: "error",
                });
            } else if(data.result == "newsletter__mail_empty") {
                swal(data.return_message, {
                    icon: "error",
                });
            } else if(data.result == "captcha_failed") {
                swal(data.return_message, {
                    icon: "error",
                });
            } else if(data.result == "ok") {
                swal(data.return_message, {
                    icon: "success",
                });
                $('#form_contatti')[0].reset();

                if (typeof onContactFormSubmit === 'function') {
                    onContactFormSubmit(window.form_data);
                }

            } else if(data.result == "err") {
                swal(data.return_message, {
                    icon: "error",
                });
            }

            $('#form_contatti input, #form_contatti textarea, #form_contatti button').attr('disabled', false);
        }
    });
}
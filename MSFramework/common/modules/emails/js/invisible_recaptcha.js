if($('.g-recaptcha').length) {
    $('.g-recaptcha')[0].setAttribute('data-size', 'invisible');
    $('.g-recaptcha')[0].setAttribute('data-callback', 'invisibleCaptchaOnSubmit');
}

function invisibleCaptchaOnSubmit(token) {
    formContattiSubmit(window.form_data);
}

function onloadInvisibleRecaptcha() {

}
<?php
if(!(new \MSFramework\modules())->checkIfIsActive('multiportal') || !(new \MSFramework\cms())->checkExtraFunctionsStatus('realestate')) {
    header('HTTP/1.0 404 Not Found', true, 404);
    die();
}

$MSFrameworkImmobili = new \MSFramework\RealEstate\immobili();
$MSFrameworkImmobiliCat = new \MSFramework\RealEstate\categories();
$MSFrameworkGeonames = new \MSFramework\geonames();
$MSFrameworkDatabase = new \MSFramework\database();

function getBoxAutoType($tipo) {
    $boxAutoTypes = array(
        "0" => "Singolo",
        "1" => "Doppio",
        "2" => "Singolo",
        "3" => "Assente"
    );

    return $boxAutoTypes[$tipo];
}

function getArredamentoType($tipo) {
    $arredamentoTypes = array(
        "0" => "Arredato",
        "1" => "Parzialmente Arredato",
        "2" => "Non Arredato"
    );

    return $arredamentoTypes[$tipo];
}

function getCucinaType($tipo) {
    $cucinaTypes = array(
        "0" => "Abitabile",
        "1" => "Angolo cottura",
        "2" => "Cucinotto",
        "3" => "Semi abitabile",
        "4" => "A vista",
    );

    if($cucinaTypes[$tipo] != "") {
        return $cucinaTypes[$tipo];
    } else {
        return "Non presente";
    }
}

function getCategoryType($tipo) {
    $destinationTypes = array(
        "0" => "Residenziale",
        "1" => "Residenziale",
        "2" => "Residenziale",
        "3" => "Commerciale",
        "4" => "Residenziale",
        "5" => "Commerciale",
    );

    return $destinationTypes[$tipo];
}

header('Content-Type: text/xml');

$id_agenzia = (new \MSFramework\modules())->getExtraModuleSettings('multiportal')['gestionalere']['agency_id'];

$writer = new \XMLWriter();
$writer->openURI('php://output');
$writer->startDocument('1.0','UTF-8');
$writer->setIndent(true);

$writer->startElement("feed");
foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM realestate_immobili WHERE stato_vendita = '0'") as $immobile) {
    $multiportal_id = $MSFrameworkImmobiliCat->getMultiportalLink($immobile['category'], 'gestionalere');
    $immobileType = ($multiportal_id != "" ? $multiportal_id : 0);

    $geodata = json_decode($immobile['geo_data'], true);
    //alcuni campi che RealBox non considera obbligatori, lo sono invece per la struttura XML. Li verifico preventivamente e, se non ho altro modo per popolarli, salto l'immobile nel caso in cui non siano stati compilati su RealBox
    if($geodata['indirizzo'] == "" || $geodata['cap'] == "" || $immobileType == 0) {
        continue;
    }

    $caratteristiche = json_decode($immobile['caratteristiche'], true);
    $servizi = json_decode($immobile['servizi'], true);

    $macrocategory = $MSFrameworkImmobiliCat->getMacroForCategory($immobile['category']);

    $writer->startElement("item");
        $writer->writeAttribute('datamodifica', date("d/m/Y", $immobile['last_update_time']));

            $writer->writeElement('idagenzia', $id_agenzia);
            $writer->writeElement('id', $immobile['id']);
            $writer->writeElement('type', $immobileType);

            $writer->startElement("tipoannuncio");
                $writer->writeCData(($immobile['tipo_vendita'] == "0") ? "Vendita" : "Affitto");
            $writer->endElement();

            $writer->startElement("codimm");
                $writer->writeCData(($immobile['riferimento'] != "" ? $immobile['riferimento'] : $id_agenzia . "-" . $immobile['id']));
            $writer->endElement();

            $writer->startElement("title");
                $immobile['titolo'] = json_decode($immobile['titolo'], true)['it_IT'];
                $writer->writeCData(substr($immobile['titolo'], 0, 150));
            $writer->endElement();

            $writer->startElement("dated");
                $writer->writeCData(date("d/m/Y", $immobile['creation_time']));
            $writer->endElement();

            $writer->startElement("datamodifica");
                $writer->writeCData(date("d/m/Y", $immobile['last_update_time']));
            $writer->endElement();

            $writer->startElement("statonome");
                $writer->writeCData("Italia");
            $writer->endElement();

            $writer->startElement("provincianome");
                $writer->writeCData(str_replace(array("Provincia di ", "Città metropolitana di "), array("", ""), $MSFrameworkGeonames->getDettagliProvincia($geodata['provincia'], "name")['name']));
            $writer->endElement();

            $writer->startElement("comune");
                $writer->writeCData($MSFrameworkGeonames->getDettagliComune($geodata['comune'], "name")['name']);
            $writer->endElement();

            $writer->startElement("comunecap");
                $writer->writeCData($immobile['cap']);
            $writer->endElement();

            $writer->startElement("comuneistat");
                $writer->writeCData($MSFrameworkGeonames->getDettagliComune($geodata['comune'], "admin3")['admin3']);
            $writer->endElement();

            $writer->startElement("localita");
                $writer->writeCData($MSFrameworkGeonames->getDettagliComune($geodata['comune'], "name")['name']);
            $writer->endElement();

            $writer->startElement("indirizzo");
                $writer->writeCData(($geodata['street_number'] != "") ? str_replace(", " . $geodata['street_number'], "", $geodata['indirizzo']) : $geodata['indirizzo']);
            $writer->endElement();

            $writer->startElement("numerocivico");
                $writer->writeCData(($geodata['street_number'] != "") ? $geodata['street_number'] : "1");
            $writer->endElement();

            $writer->writeElement('mostraindirizzo', "1");

            $writer->startElement("descrizione");
                $immobile['descrizione'] = json_decode($immobile['descrizione'], true);
                $writer->writeCData($immobile['descrizione']['it_IT']);
            $writer->endElement();

            if($immobile['descrizione_eng'] != "") {
                $writer->startElement("endescrizione");
                    $writer->writeCData($immobile['descrizione']['en_GB']);
                $writer->endElement();
            }

            $writer->startElement("vani");
                $writer->writeCData(($caratteristiche['totale_locali'] != "") ? $caratteristiche['totale_locali'] : "0");
            $writer->endElement();

            $writer->startElement("camere");
                $writer->writeCData(($caratteristiche['camere_da_letto'] != "") ? $caratteristiche['camere_da_letto'] : "0");
            $writer->endElement();

            $writer->startElement("bagni");
                $writer->writeCData(($caratteristiche['bagni'] != "") ? $caratteristiche['bagni'] : "0");
            $writer->endElement();

            $writer->startElement("metri");
                $writer->writeCData($immobile['superficie']);
            $writer->endElement();

            $writer->writeElement('prezzovisibile', ($immobile['trattativa_riservata'] == "1") ? "0" : "1");

            $writer->startElement("prezzo");
                $writer->writeCData($immobile['prezzo']);
            $writer->endElement();

            $writer->startElement("prezzostimato");
                $writer->writeCData(($immobile['stima_prezzo'] != "") ? $immobile['stima_prezzo'] : $immobile['prezzo']-1);
            $writer->endElement();

            if($macrocategory == "5") {
                $classe_energetica = "N.C.";
            } else {
                if($caratteristiche['classe_energetica'] == "8" || $caratteristiche['classe_energetica'] == "") {
                    $classe_energetica = "N.D.";
                } else {
                    $classe_energetica = $MSFrameworkImmobili->getClassiEnergetiche($caratteristiche['classe_energetica']);
                }
            }

            $writer->startElement("classenergetica");
                $writer->writeCData($classe_energetica);
            $writer->endElement();

            $writer->startElement("clenrgid");
                $writer->writeCData("2");
            $writer->endElement();

            $writer->startElement("riscaldamento");
                $writer->writeCData(($caratteristiche['riscaldamento'] != "") ? substr($MSFrameworkImmobili->getTipiRiscaldamento($caratteristiche['riscaldamento']), 0, 80) : "-----");
            $writer->endElement();

            $writer->startElement("serv");
                $writer->writeCData(($caratteristiche['stato_immobile'] != "") ? substr($MSFrameworkImmobili->getStatiImmobili($caratteristiche['stato_immobile']), 0, 80) : "-----");
            $writer->endElement();

            $writer->startElement("acces");
                $writer->writeCData(($caratteristiche['box_garage'] != "") ? substr($MSFrameworkImmobili->getTipiGarage($caratteristiche['box_garage']), 0, 80) : "-----");
            $writer->endElement();

            $writer->startElement("categ");
                $writer->writeCData(getCategoryType($macrocategory));
            $writer->endElement();

            $writer->startElement("terrazzo1");
                $writer->writeCData(($caratteristiche['check_terrazzo'] == "1") ? "si" : "no");
            $writer->endElement();

            $writer->startElement("terrazzo");
                $writer->writeCData(($caratteristiche['terrazzo_mq'] != "") ? substr($caratteristiche['terrazzo_mq'], 0, 80) : "");
            $writer->endElement();

            $writer->startElement("giardino1");
                $writer->writeCData(($caratteristiche['giardino'] != "2" && $caratteristiche['giardino'] != "") ? "si" : "no");
            $writer->endElement();

            $writer->startElement("giardino");
                $writer->writeCData(($caratteristiche['giardino_mq'] != "") ? substr($caratteristiche['giardino_mq'], 0, 80) : "");
            $writer->endElement();

            $writer->startElement("balcone");
                $writer->writeCData(($caratteristiche['check_balcone'] == "1") ? "si" : "no");
            $writer->endElement();

            $writer->startElement("cucina");
                $writer->writeCData(getCucinaType($caratteristiche['cucina']));
            $writer->endElement();

            $writer->startElement("ascensore");
                $writer->writeCData(($caratteristiche['check_ascensore'] == "1") ? "si" : "no");
            $writer->endElement();

            $writer->startElement("condizionatore");
                $writer->writeCData(($caratteristiche['check_condizionatore'] == "1") ? "si" : "no");
            $writer->endElement();

            if($caratteristiche['stato_arredamento'] != "") {
                $writer->startElement("arredato");
                    $writer->writeCData(getArredamentoType($caratteristiche['stato_arredamento']));
                $writer->endElement();
            }

            $writer->startElement("allarme");
                $writer->writeCData(($caratteristiche['check_allarme'] == "1") ? "si" : "no");
            $writer->endElement();

            if($caratteristiche['box_garage'] != "") {
                $writer->startElement("boxauto");
                    $writer->writeCData(getBoxAutoType($caratteristiche['box_garage']));
                $writer->endElement();
            }

            $writer->startElement("cantina");
                $writer->writeCData(($caratteristiche['check_cantina'] == "1") ? "Presente" : "Assente");
            $writer->endElement();

            $writer->startElement("portineria");
                $writer->writeCData((array_key_exists('0_6', $servizi)) ? "si" : "no");
            $writer->endElement();

            $writer->startElement("piscina");
                $writer->writeCData((array_key_exists('0_11', $servizi)) ? "Si" : "No");
            $writer->endElement();

            $writer->startElement("caminetto");
                $writer->writeCData((array_key_exists('0_12', $servizi)) ? "Si" : "No");
            $writer->endElement();

            $writer->startElement("spesecondominiali");
                $writer->writeCData(($caratteristiche['check_spese_condominiali'] == "1") ? "1" : "0");
            $writer->endElement();

            $writer->startElement("spesecondominialiimporto");
                $writer->writeCData(($caratteristiche['spese_condominiali_importo'] != "") ? $caratteristiche['spese_condominiali_importo'] : "0");
            $writer->endElement();

            $writer->writeElement('mappa', ($immobile['lat'] != "" && $immobile['lng'] != "") ? "1" : "0");

            $writer->startElement("latlon");
                $writer->writeCData(($immobile['lat'] != "" && $immobile['lng'] != "") ? "(" . $immobile['lat'] . "," . $immobile['lng'] . ")" : "(0,0)");
            $writer->endElement();

            $writer->writeElement('mostrazoom', "1");
            $writer->writeElement('zoommappa', "8");

            $writer->startElement("piano");
                $writer->writeCData(($caratteristiche['piano'] != "") ? substr($MSFrameworkImmobili->getElencoPiani($caratteristiche['piano']), 0, 100) : "");
            $writer->endElement();

            $writer->startElement("numeropiano");
                $writer->writeCData(($caratteristiche['piani_totali'] != "") ? substr($caratteristiche['piani_totali'], 0, 100) : "");
            $writer->endElement();

            $writer->startElement("annocostruzione");
                $writer->writeCData(($immobile['anno_costruzione'] != "") ? substr($immobile['anno_costruzione'], 0, 5) : "");
            $writer->endElement();

            $writer->startElement("video1");
                $writer->writeAttribute('id', $immobile['id'] . "1");
                $writer->writeCData((strlen($immobile['youtube_url'] <= 255)) ? $immobile['youtube_url'] : "");
            $writer->endElement();

            $done_images = 0;
            foreach(json_decode($immobile['images'], true) as $image) {
                if(strlen($image) > 255) {
                    continue;
                }

                if($done_images == 20) {
                    break;
                }
                
                $done_images++;

                $writer->startElement("foto" . $done_images);
                    $writer->writeAttribute('id', $immobile['id'] . $done_images);
                    $writer->text((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? "https:" : "http:") . UPLOAD_REALESTATE_IMMOBILI_FOR_DOMAIN_HTML . $image);
                $writer->endElement();
            }

            $done_planimetria = 0;
            foreach(json_decode($immobile['images_plan'], true) as $image) {
                if(strlen($image) > 255) {
                    continue;
                }

                if($done_planimetria == 20) {
                    break;
                }
                
                $done_planimetria++;

                $writer->startElement("planimetria" . $done_planimetria);
                    $writer->writeAttribute('id', $immobile['id'] . $done_planimetria);
                    $writer->text((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? "https:" : "http:") . UPLOAD_REALESTATE_PLANIMETRIE_FOR_DOMAIN_HTML . $image);
                $writer->endElement();
            }

    $writer->endElement();
}

$writer->endElement();

$writer->endDocument();
$writer->flush();
?>
<?php
$microdata_images = array();
foreach(json_decode($currentPageDetails['gallery'], true) as $i=>$thumb) {
    $microdata_images[] = (SSL_ENABLED ? 'https:' : 'http:') . UPLOAD_ECOMMERCE_PRODUCTS_FOR_DOMAIN_HTML . $thumb;
}

$prodotti_settings = (new \MSFramework\cms())->getCMSData('ecommerce_prodotti');
if( ((int)$prodotti_settings['manage_reviews'] == 1 && (int)$currentPageDetails['manage_reviews'] != 2) || (int)$currentPageDetails['manage_reviews'] == 1 ) {
    $reviewSummary = (new \MSFramework\Ecommerce\reviews())->getReviewsSummaryByProductID($_GET['id']);
}
?>

<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "name": "<?= $MSFrameworki18n->getFieldValue($currentPageDetails['nome']); ?>",
  "image": <?= json_encode($microdata_images); ?>,
  "description": "<?= strip_tags($MSFrameworki18n->getFieldValue($currentPageDetails['short_descr'])); ?>",
  <?php if(isset($reviewSummary) && $reviewSummary['total_review'] > 0) { ?>
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "<?= floatval($reviewSummary['average']); ?>",
    "reviewCount": "<?= $reviewSummary['total_review']; ?>"
  },
  <?php } ?>
  "offers": {
    "@type": "Offer",
    "priceCurrency": "EUR",
    "price": "<?= $currentPageDetails['prezzo']['shop_price'][0] . '.' . $currentPageDetails['prezzo']['shop_price'][1]; ?>",
    "itemCondition": "http://schema.org/NewCondition",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "<?= SW_NAME; ?>"
    }
  }
}
</script>

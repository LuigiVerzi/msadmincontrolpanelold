<?php
$articleImage = false;
$decodeImages = json_decode($currentPageDetails['images'], true);
if($decodeImages) {
    if($decodeImages['icon'] && $decodeImages['icon'][0]) $articleImage = (SSL_ENABLED ? 'https:' : 'http:') . UPLOAD_BLOG_POSTS_FOR_DOMAIN_HTML . $decodeImages['icon'][0];
}

if(!$articleImage) return false;

?>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "NewsArticle",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "https://google.com/article"
  },
  "headline": "<?= $MSFrameworki18n->getFieldValue($currentPageDetails['titolo']); ?>",
  <?php if($articleImage) { ?>
  "image": [
    "<?= $articleImage; ?>"
   ],
   <?php } ?>
  "datePublished": "<?= $currentPageDetails['data_creazione']; ?>",
  "dateModified": "<?= $currentPageDetails['last_update']; ?>",
  "author": {
    "@type": "Organization",
    "name": "<?= SW_NAME; ?>"
  },
   "publisher": {
    "@type": "Organization",
    "name": "<?= SW_NAME; ?>",
    "logo": {
      "@type": "ImageObject",
      "url": "<?= (SSL_ENABLED ? 'https:' : 'http:'); ?><?= UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo'] ?>"
    }
  },
  "description": "<?= $MSFrameworki18n->getFieldValue($currentPageDetails['descrizione_breve']); ?>"
}
</script>

<?php

$url_to_website = (new \MSFramework\cms())->getURLToSite();
if((new \MSFramework\modules())->getSettingValue('richsnippets', 'generale_logo')) {
    $site_logos = json_decode((new \MSFramework\cms())->getCMSData('site')['logos'], true);
?>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "<?= $url_to_website; ?>",
      "logo": "<?= (SSL_ENABLED ? 'https:' : 'http:') . UPLOAD_LOGOS_FOR_DOMAIN_HTML . $site_logos['logo']; ?>"
    }
    </script>
<?php } ?>

<?php if((new \MSFramework\modules())->getSettingValue('richsnippets', 'generale_ricerca_active')) { ?>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "WebSite",
      "url": "<?= $url_to_website; ?>",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "<?= (new \MSFramework\modules())->getSettingValue('richsnippets', 'generale_ricerca_url'); ?>",
        "query-input": "required name=search_term"
      }
    }
    </script>
<?php } ?>

<?php if((new \MSFramework\modules())->getSettingValue('richsnippets', 'generale_social')) {
    $snippet_socials = array();
    if(!empty($siteCMSData['fb_profile'])) $snippet_socials[] = $siteCMSData['fb_profile'];
    if(!empty($siteCMSData['instagram_profile'])) $snippet_socials[] = $siteCMSData['instagram_profile'];
    if(!empty($siteCMSData['linkedin_profile'])) $snippet_socials[] = $siteCMSData['linkedin_profile'];
    if(!empty($siteCMSData['googplus_profile'])) $snippet_socials[] = $siteCMSData['googplus_profile'];
    if(!empty($siteCMSData['twitter_profile'])) $snippet_socials[] = $siteCMSData['twitter_profile'];
    if($snippet_socials) { ?>
        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "Organization",
          "name": "<?= SW_NAME; ?>",
          "url": "<?= $url_to_website; ?>",
          "sameAs": <?= json_encode($snippet_socials); ?>
        }
        </script>
    <?php } ?>
<?php } ?>

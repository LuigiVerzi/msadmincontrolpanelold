$(document).ready(function () {
    initMSHooks();
    initMSForm();
});

function initMSForm() {

    $('.MSForm [required]').addClass('required');
    $('.MSForm :hidden > .required').attr('required', false);
    $('.MSForm :visible > .required').attr('required', true);

    $('body').on('click', '.MSForm label', function() {
        if(!$(this).find('input').length) {
            $(this).parent().find('input').click();
        }
    });

    $('body').on('change', '.MSForm input, .MSForm select, .MSForm textarea', function (e) {
        e.preventDefault();

        $('.MSForm :hidden > .required').attr('required', false);
        $('.MSForm :visible > .required').attr('required', true);

        var $form = $(this).closest('.MSForm');
        var $input = $(this);

        var form_id = $form.data('form_id');

        /* PREPARA GLI HOOK DA RICHIAMARE */
        var hook_on_change = $form.data('on_change');

        /* RICHIAMA L'HOOK PER SEGNALARE IL CAMBIAMENTO DEL VALORE */
        if(typeof(window[hook_on_change]) == "function") {
            window[hook_on_change]($form, $input, $input.val());
        }

        /* OTTIENE I VALORI DEL FORM E LI SALVA IN UN OBJECT */
        var form_data = {};
        $.map($form.serializeArray(), function(n, i) {
            form_data[n['name']] = n['value'];
        });

        /* CONTROLLA CHE NON CI SIANO CAMPI VUOTI */
        var fields_errors = false;
        $form.find(':visible > [name].required').each(function () {
            if(((typeof($(this).attr('type')) === 'undefined' || $(this).attr('type') != 'checkbox') && $(this).val() == "") || ($(this).attr('type') == 'checkbox' && !$(this).prop('checked') ) ) {
                fields_errors = true;
            }
        });

        if(!fields_errors && $form.data('setting_quote_force_manually') != 1) {
            MSForm_GenerateQuote($form, form_id, form_data);
        }
    });

    $('body').on('submit', '.MSForm', function (e) {
        e.preventDefault();

        var $form = $(this);

        var form_id = $form.data('form_id');
        var loading_class = $form.data('loading_class');
        var input_error_class = $form.data('input_error_class');

        if($form.hasClass(loading_class)) {
            return false;
        }

        /* IMPOSTA LA CLASSE LOADING AL FORM */
        $form.addClass(loading_class);

        $form.find('[name]').each(function () {
           if(!$.trim($(this).val()).length) $(this).val('');
        });

        /* OTTIENE I VALORI DEL FORM E LI SALVA IN UN OBJECT */
        var form_data = {};
        $.map($form.serializeArray(), function(n, i) {
            form_data[n['name']] = n['value'];
        });

        form_data['site-link'] = document.location.href;

        /* PREPARA GLI HOOK DA RICHIAMARE */
        var hook_before_submit = $form.data('before_form_submit');
        var hook_after_submit = $form.data('after_form_submit');
        var hook_on_error = $form.data('on_error');
        var hook_show_message = $form.data('show_message');

        /* PREPARA I MESSAGGI DEL FORM */
        var message_required_input = $form.data('message_required_field');

        /* PREPARA L'ARRAY CON I SETTINGS DEL FORM */
        var form_settings = {};
        $.each($form.data(), function(k, v) {
            if(k.indexOf("setting_") != -1) {
                form_settings[k.replace("setting_", "")] = v;
            }
        });

        /* CONTROLLA CHE NON CI SIANO CAMPI VUOTI */
        var fields_errors = {};
        $form.find(':visible > [name].required').each(function () {
            if(((typeof($(this).attr('type')) === 'undefined' || $(this).attr('type') != 'checkbox') && $(this).val() == "") || ($(this).attr('type') == 'checkbox' && !$(this).prop('checked') ) ) {
                fields_errors[$(this).attr('name')] = message_required_input;
                $(this).addClass(input_error_class);
                $(this).one('focus keypress', function () {
                    $(this).removeClass(input_error_class);
                });
            }
        });

        if(Object.keys(fields_errors).length) {
            /* RICHIAMA L'HOOK PER GLI INPUT VUOTI */
            if(typeof(window[hook_on_error]) == "function") {
                window[hook_on_error]($form, 'empty_fields', fields_errors);
            }
            $form.removeClass(loading_class);
            $form.find('[type="submit"]').attr("disabled", false);
        }
        else
        {

            $('.MSForm :hidden > .required').attr('required', false);
            $('.MSForm :visible > .required').attr('required', true);

            /* RICHIAMA L'HOOK PRECEDENTE AL SUBMIT */
            if(typeof(window[hook_before_submit]) == "function") {

                var before_submit_value = window[hook_before_submit]($form, form_data);

                if(typeof(before_submit_value) !== 'undefined') {
                    if (!before_submit_value) {
                        $form.removeClass(loading_class);
                        $form.find('[type="submit"]').attr("disabled", false);
                        return false; // Se l'hook ha un return negativo allora blocchiamo il submit
                    } else if (before_submit_value === Object(before_submit_value)) {
                        form_data = before_submit_value; // Nel caso in cui è stato ritornato un object (probabilmente formattato) lo sostituisco al vecchio
                    }
                }
            }

            $form.find('[type="submit"]').attr("disabled", "disabled");

            /* AVVIA LA CHIAMATA AJAX */
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: {
                    action: 'formSubmit',
                    id: form_id,
                    form_data: form_data,
                    form_settings: form_settings,
                    lang_code: window.current_lang_flag_code,
                    lang_id: window.current_lang_id
                },
                dataType: 'json',
                success: function (data) {

                    /* RIMUOVE LA CLASSE DEL CARICAMENTO */
                    $form.removeClass(loading_class);
                    $form.find('[type="submit"]').attr("disabled", false);

                    /* RICHIAMA L'HOOK AL SUBMIT COMPLETO */
                    if (typeof(window[hook_after_submit]) == "function") {

                        var after_submit_value = window[hook_after_submit]($form, form_data, data);

                        if(typeof(after_submit_value) !== 'undefined') {
                            if (after_submit_value === false) {
                                return false; // Se l'hook ha un return negativo allora blocchiamo la funzione
                            }
                        }
                    }

                    // Se tutti gli eventi sono andati a buon fine elimino il contenuto del form altrimenti no
                    var totalSuccess = true;
                    Object.keys(data).forEach(function(key) {
                        if(!data[key].status) {
                            totalSuccess = false;
                        }
                    });

                    if(totalSuccess) $form.html('');
                    else $form.find('.alert').remove();


                    var show_message = true;
                    var alert_message = '';

                    // Se la funzione non è stata bloccata dal return dell'HOOK continuo con le funzioni predefintie
                    if(typeof(data.anagrafica) !== "undefined") {
                        alert_message = data.anagrafica.html;

                        show_message = true;
                        if(typeof(window[hook_show_message]) == "function") {
                            show_message = window[hook_show_message]($form, 'anagrafica', data.anagrafica);
                        }

                        if(show_message) $form.prepend(alert_message);

                    }

                    if(typeof(data.reservation) !== "undefined") {
                        alert_message = data.reservation.html;
                        show_message = true;
                        if(typeof(window[hook_show_message]) == "function") {
                            show_message = window[hook_show_message]($form, 'reservation', data.reservation);
                        }

                        if(show_message) $form.prepend(alert_message);

                    }

                    if(typeof(data.generic_request) !== "undefined") {
                        alert_message = data.generic_request.html;
                        show_message = true;
                        if(typeof(window[hook_show_message]) == "function") {
                            show_message = window[hook_show_message]($form, 'generic_request', data.generic_request);
                        }

                        if(show_message) $form.prepend(alert_message);
                    }

                    if(typeof(data.property_info) !== "undefined") {
                        alert_message = data.property_info.html;
                        show_message = true;
                        if(typeof(window[hook_show_message]) == "function") {
                            show_message = window[hook_show_message]($form, 'property_info', data.property_info);
                        }

                        if(show_message) $form.prepend(alert_message);
                    }

                    if(typeof(data.newsletter_subscribe) !== "undefined") {
                        alert_message = data.newsletter_subscribe.html;
                        show_message = true;
                        if(typeof(window[hook_show_message]) == "function") {
                            show_message = window[hook_show_message]($form, 'newsletter_subscribe', data.newsletter_subscribe);
                        }

                        if(show_message) $form.prepend(alert_message);
                    }

                    if(typeof(data.login) !== "undefined") {
                        alert_message = data.login.html;
                        show_message = true;
                        if(typeof(window[hook_show_message]) == "function") {
                            show_message = window[hook_show_message]($form, 'login', data.login);
                        }

                        if(data.login.status) {
                            setTimeout(function () {
                                location.reload();
                            }, 1000);
                        }

                        if(show_message) $form.prepend(alert_message);
                    }

                    if(typeof(data.password_recovery) !== "undefined") {
                        alert_message = data.password_recovery.html;
                        show_message = true;
                        if(typeof(window[hook_show_message]) == "function") {
                            show_message = window[hook_show_message]($form, 'password_recovery', data.password_recovery);
                        }

                        if(show_message) $form.prepend(alert_message);
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $form.removeClass(loading_class);
                    $form.find('[type="submit"]').attr("disabled", false);
                }
            });
        }

    });
}

function MSForm_GenerateQuote($form, form_id, form_data) {
    /* PREPARA IL CALCOLO DEL PREVENTIVO */
    var before_quote_calculation = (typeof ($form.data("before_quote_calculation")) != "undefined" ? $form.data("before_quote_calculation") : false);
    var after_quote_calculation = (typeof ($form.data("after_quote_calculation")) != "undefined" ? $form.data("after_quote_calculation") : false);

    if (after_quote_calculation != false) {

        /* RICHIAMA L'HOOK PRECEDENTE AL SUBMIT SE PRESENTE */
        if (typeof (window[before_quote_calculation]) == "function") {
            var before_submit_value = window[before_quote_calculation]($form, form_data);
            if (!before_submit_value) {
                return false; // Se l'hook ha un return negativo allora blocchiamo il submit
            } else if (before_submit_value === Object(before_submit_value)) {
                form_data = before_submit_value; // Nel caso in cui è stato ritornato un object (probabilmente formattato) lo sostituisco al vecchio
            }
        }

        /* AVVIA LA CHIAMATA AJAX PER GENERARE IL PREVENTIVO */
        $.ajax({
            url: $form.attr('action'),
            type: 'POST',
            data: {
                action: 'generateQuote',
                id: form_id,
                form_data: form_data,
                lang: window.current_lang_id
            },
            dataType: 'json',
            success: function (data) {
                if (typeof (window[after_quote_calculation]) === "function") {
                    window[after_quote_calculation]($form, data);
                }
            },
            error: function (error) {
                if (typeof (window[after_quote_calculation]) === "function") {
                    window[after_quote_calculation]($form, false);
                }
            }
        });
    }

}

function initMSHooks() {
    if(typeof(window.MSFormsHooks) == 'undefined') window.MSFormsHooks = [];

    $('.MSForm').each(function () {

        var form_id = $(this).data('form_id');

        var form_hooks = {
            before_form_submit: $(this).data('before_form_submit'),
            after_form_submit: $(this).data('after_form_submit'),
            on_error: $(this).data('on_error'),
            on_change: $(this).data('on_change'),
            show_message: $(this).data('show_message')
        };

        if(typeof(window.MSFormsHooks[form_id]) == 'undefined') window.MSFormsHooks[form_id] = {};

        if (typeof (window['MSForm' + form_id + '_GetHooks']) == "function") {

            var custom_form_hooks = window['MSForm' + form_id + '_GetHooks']();

            Object.keys(form_hooks).forEach(function (hook_id) {
                if(typeof(custom_form_hooks[hook_id]) === "function") {

                    var custom_form_function = custom_form_hooks[hook_id];

                    if (typeof (window[form_hooks[hook_id]]) === "function") {
                        var oldHookFunction = window[form_hooks[hook_id]];
                        window[form_hooks[hook_id]] = function () {
                            oldHookFunction.apply(oldHookFunction, arguments);
                            var return_code = custom_form_function.apply(custom_form_function, arguments);
                            if(typeof(return_code) == 'undefined') return true;
                            return return_code;
                        }
                    } else {
                        window[form_hooks[hook_id]] = function () {
                            var return_info = custom_form_function.apply(custom_form_function, arguments);
                            if(typeof(return_info) == 'undefined') return true;
                            return return_info;
                        }
                    }
                }

            });

        }

    });

}
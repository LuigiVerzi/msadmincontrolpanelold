<?php
/**
 * MSFramework
 * Date: 20/07/18
 */

header('Access-Control-Allow-Origin: *');

function array_insert_before($array, $key, $new) {
    $keys = array_keys($array);
    $pos = (int) array_search($key, $keys);
    return array_merge(
        array_slice($array, 0, $pos),
        $new,
        array_slice($array, $pos)
    );
}

$gdpr_modal = array(
    'close_icon' => '<img src="' . FRAMEWORK_COMMON_CDN . 'modules/gdpr/images/icons/times-solid.svg" class="gdpr_svg_ico">', // Pulsante chiusura
    'update_text' => 'Aggiorna', // Pulsante aggiorna
    'enable_text' => 'Abilita', // Pulsante Abilita
    'enable_all_text' => (new \MSFramework\modules())->getSettingValue('gdpr', 'accept_all_label'), // Pulsante Abilita tutto
    'enabled_text' => 'Abilitato', // Label Abilitato
    'disabled_text' => 'Non Attivo', // Label Non Attivo
    'main_color' => '#0C4DA2', // Colore principale
    'tabs' => array(
        'necessari' => array(
            'open' => true, // La tab si trova aperta come predefinita
            'titolo' => 'Necessari', // Il titolo della tab
            'icona' => '<img src="' . FRAMEWORK_COMMON_CDN . 'modules/gdpr/images/icons/ex-circle.svg" class="gdpr_svg_ico">', // L'icona della tab
            'checkbox' => false // Se è modificabile o no
        ),
        'politica' => array(
            'titolo' => 'Politica Dei Cookie',
            'icona' => '<img src="' . FRAMEWORK_COMMON_CDN . 'modules/gdpr/images/icons/file-alt-solid.svg" class="gdpr_svg_ico">',
            'checkbox' => false
        )
    )
);

$gdpr_modal['tabs'] = array_insert_before($gdpr_modal['tabs'], 'politica', (new \MSFramework\Modules\gdpr())->getCategorieGDPR());
?>


<!-- IMPORTIAMO LA MODAL -->
<div id="gdpr_modal">
    <a class="gdpr_modal_close"><?= $gdpr_modal['close_icon']; ?></a>
    <div class="gdpr_modal_header">
        <img class="gdpr_modal_header_logo" src="<?= FRAMEWORK_COMMON_CDN . 'modules/gdpr/images/logo.png' ?>">
        <div class="gdpr_modal_navigation">
            <ul>
                <?php foreach($gdpr_modal['tabs'] as $id=>$tab) { ?>
                    <li class="gdpr_modal_nav_tab<?= (isset($tab['open']) ? ' active' : ''); ?>" data-tab="<?= $id; ?>"><?= $tab['icona']; ?><span><?= $tab['titolo']; ?></span></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="gdpr_modal_content">
        <?php foreach($gdpr_modal['tabs'] as $id=>$tab) { ?>
            <div class="gdpr_modal_tab_content <?= $id; ?><?= (isset($tab['open']) ? ' show' : ''); ?>" style="display: none; height: 100%; overflow: auto;">
                <h1 class="gdpr_modal_tab_title"><?= $tab['titolo']; ?></h1>
                <?php include(FRAMEWORK_ABSOLUTE_PATH . 'common/modules/gdpr/tabs/' . $id . '.html'); ?>
                <?php if(is_array($tab['checkbox'])) { ?>
                    <form class="gdpr_enable_section <?= ($_POST['GDPR_' . $id] == 1 ? 'enabled' : 'disabled'); ?> <?= (isset($tab['principale']) ? 'principale' : ''); ?>" style="display: <?= (!(new \MSFramework\modules())->getSettingValue('gdpr', 'hide_settings') ? 'block' : 'none'); ?>">
                        <input type="hidden" value="<?= $id; ?>" name="cookie_section">
                        <label class="switch">
                            <input type="checkbox" class="gdpr_cookie_checkbox" name="cookie_enable" <?= ($_POST['GDPR_' . $id] == 1 ? 'checked' : ''); ?>>
                            <span class="slider round"></span>
                            <b class="status_message"><span class="enabled"><?= $gdpr_modal['enabled_text']; ?></span><span class="disabled"><?= $gdpr_modal['disabled_text']; ?></span></b>
                        </label>
                        <?php if(isset($tab['checkbox']['required'])) { ?>
                            <input type="hidden" name="required_cookie" value="<?= $tab['checkbox']['required']; ?>">
                            <div class="gdpr_warning_message gdpr_required_message">
                                <?= $tab['checkbox']['required_message']; ?>
                            </div>
                        <?php } ?>
                        <?php if(isset($tab['checkbox']['unchecked_message'])) { ?>
                            <div class="gdpr_warning_message gdpr_unchecked_message">
                                <?= $tab['checkbox']['unchecked_message']; ?>
                            </div>
                        <?php } ?>
                    </form>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
    <div class="gdpr_modal_footer">
        <a href="#" class="gdpr_footer_button gdpr_enable_all">
            <?= $gdpr_modal['enable_all_text']; ?>
        </a>
        <a href="#" class="gdpr_footer_button gdpr_update">
            <?= $gdpr_modal['update_text']; ?>
        </a>
    </div>
</div>
<div id="gdpr_black_screen"></div>
<?php
header('Access-Control-Allow-Origin: *');

//questo file viene chiamato tramite il bridge per le chiamate ajax del moduli, quindi posso includere sw-config.php (del sito) che a sua volta mi restituirà tutto quello di cui ho bisogno
/*
 *  Per utilizzarlo bisogna includere il file PHP
 *
 */

$str_accept_on_scroll = "";
$accept_on_scroll = "0";
if((new \MSFramework\modules())->getExtraModuleSettings('gdpr')['accept_on_scroll']['enabled'] == "1") {
    if((new \MSFramework\modules())->getExtraModuleSettings('gdpr')['accept_on_scroll']['notifyuser'] == "1") {
        $str_accept_on_scroll = "Proseguendo con la navigazione, accetti automaticamente l'utilizzo dei cookie.";
    }

    $accept_on_scroll = "1";
}

/* IMPOSTAZIONI MODAL GDPR */
$gdpr_bar = array(
    'gdpr_bar_cookie' => 'GDPR_preferenze', // Il cookie principale (deve corrispondere ad uno delle tabs)
    'cookie_bar_text' => 'Utilizziamo i cookie per offrirti la migliore esperienza sul nostro sito web. ' . $str_accept_on_scroll . '<br>Puoi scoprire di più su quali cookie stiamo utilizzando o disattivarli nelle <a class="open_gdpr_cookie" href="#">impostazioni</a>.',
    'accept_button' => (new \MSFramework\modules())->getSettingValue('gdpr', 'accept_all_label'), // Scritta Pulsante 'Accetta Tutto'
    'main_color' => '#0C4DA2', // Colore principale barra
    'text_color' => '#FFFFFF', // Colore testo barra
    'background_color' => '#000000' // Colore sfondo barra
);

$gdpr_modal_button = array(
    'modal_icon' => '<img src="' . FRAMEWORK_COMMON_CDN . 'modules/gdpr/images/icons/cog-solid.svg" class="gdpr_svg_ico">',
    'modal_text' => 'Modifica le impostazioni dei Cookie'
);


/* NON MODIFICARE A SEGUIRE */
?>
<style>

    #gdpr_cookie_bar {
        background: <?= $gdpr_bar['background_color']; ?>;
    }

    #gdpr_cookie_bar a,
    #gdpr_cookie_bar p{
        color: <?= $gdpr_bar['text_color']; ?>;
    }

    #gdpr_cookie_bar .cookie_bar_accept {
        background: <?= $gdpr_bar['main_color']; ?>;
        color: <?= $gdpr_bar['text_color']; ?>;
    }

    #gdpr_modal .gdpr_modal_close {
        color: #0C4DA2;
    }

    #gdpr_modal .gdpr_modal_navigation ul li .gdpr_svg_ico path {
        fill: #0C4DA2;
    }
    #gdpr_modal .gdpr_modal_footer .gdpr_footer_button {
        background: #0C4DA2;
    }

</style>


<div id="gdpr_cookie_bar" style="display: none;">
    <div class="container">
        <p class="cookie_bar_message">
            <?= $gdpr_bar['cookie_bar_text']; ?>
        </p>
        <a class="cookie_bar_accept" href="javascript:void(0)">
            <?= $gdpr_bar['accept_button']; ?>
        </a>
    </div>

    <input type="hidden" id="gdpr_accept_on_scroll" value="<?= $accept_on_scroll ?>" />
</div>

<?php if(!(new \MSFramework\modules())->getSettingValue('gdpr', 'hide_settings')) { ?>
<div id="gdpr_open_modal" style="display: none;">
    <?= $gdpr_modal_button['modal_icon']; ?> <span><?= $gdpr_modal_button['modal_text']; ?></span>
</div>
<?php } ?>
$(document).ready(function () {

    /* APRE IL MODAL GDPR */
    $('#gdpr_cookie_bar').on('click', '.open_gdpr_cookie', function (e) {
        e.preventDefault();
        loadModalFromAjax(true);
    });

    $('#gdpr_open_modal').on('click', function (e) {
        e.preventDefault();
        loadModalFromAjax(true);
    });

    $('#gdpr_cookie_bar').on('click', '.cookie_bar_accept', function (e) {
        e.preventDefault();

        GDPR_acceptAll();
        silentInjection();
        //location.reload();
    });

    if(GDPR_getCookie('GDPR_preferenze') != "1") {
        $('#gdpr_cookie_bar').show();
    } else {
        $('#gdpr_open_modal').show();
    }

    enableScrollManager();
});

function enableScrollManager() {
    if(window.GDPRAcceptedAll != true && $('#gdpr_accept_on_scroll').val() == "1") {
        startingScrollPosition = $(window).scrollTop();

        $(document).on('scroll', function() {
            if($(window).scrollTop() != startingScrollPosition) {
                disableScrollManager();
                GDPR_acceptAll();
                silentInjection();
            }
        })
    }
}

function disableScrollManager() {
    $(document).unbind('scroll');
}

function loadModalFromAjax(show) {
    disableScrollManager();

    $.ajax({
        url: window.base_url + "m/gdpr",
        type: "POST",
        async: false,
        data: {
            action: "getModal",
            GDPR_preferenze: parseInt(GDPR_getCookie('GDPR_preferenze')),
            GDPR_statistiche: parseInt(GDPR_getCookie('GDPR_statistiche')),
            GDPR_marketing: parseInt(GDPR_getCookie('GDPR_marketing'))
        },
        success: function(data) {
            $('body').append(data);

            if(show) {
                $('#gdpr_modal').attr('class', 'show');
            }

            /* NAVIGA TRA LE VOCI DEL MODAL GDPR */
            $('#gdpr_modal').on('click', '.gdpr_modal_nav_tab', function (e) {
                e.preventDefault();

                var target = $(this).data('tab');

                $('#gdpr_modal .gdpr_modal_navigation ul li').removeClass('active');
                $('#gdpr_modal .gdpr_modal_content .gdpr_modal_tab_content').removeClass('show');

                $(this).addClass('active');
                $('#gdpr_modal .gdpr_modal_content .' + target).addClass('show');

            });

            /* QUANDO CAMBIAMO UN OPZIONE */
            $('#gdpr_modal').on('change', 'input', function (e) {
                e.preventDefault();

                var $checkbox = $(this);
                var $form = $(this).closest('form');

                $form.find('.gdpr_required_message').hide();

                var serializedForm = $form.serializeArray();
                var formArray = {};
                for (var i = 0; i < serializedForm.length; i++){
                    formArray[serializedForm[i]['name']] = serializedForm[i]['value'];
                }

                if(typeof(formArray.required_cookie) !== 'undefined')
                {
                    var required_enabled = $('.gdpr_enable_section.principale').hasClass('enabled');
                    if(!required_enabled) {
                        $form.find('.gdpr_required_message').show();
                        $checkbox.prop('checked', false);
                        return false;
                    }
                }
                else if($form.hasClass('principale') && !$checkbox.prop('checked')) {

                    $('#gdpr_modal .gdpr_enable_section').each(function () {
                        $(this).find('.gdpr_cookie_checkbox').prop('checked', false);
                    });

                }

                $form.removeClass('disabled enabled').addClass( ($checkbox.prop('checked') ? 'enabled' : 'disabled') );
                $('#gdpr_modal').addClass('with_changes');

            });

            /* QUANDO SALVIAMO LE IMPOSTAZIONI */
            $('#gdpr_modal').on('click', '.gdpr_update', function (e) {
                e.preventDefault();

                GDPR_saveCurrent();
                silentInjection();
                //location.reload();
            });

            /* QUANDO ABILITIAMO TUTTO */
            $('#gdpr_modal').on('click', '.gdpr_enable_all', function (e) {
                e.preventDefault();

                GDPR_acceptAll();
                silentInjection();
                //location.reload();
            });

            /* CHIUDE IL MODAL GDPR */
            $('#gdpr_modal').on('click', '.gdpr_modal_close', function (e) {
                e.preventDefault();

                unloadModal();
            });
            $('#gdpr_black_screen').on('click', function (e) {
                e.preventDefault();

                unloadModal();
            });
        }
    })
}

function unloadModal() {
    $('#gdpr_modal').fadeOut(function () {
        $('#gdpr_modal').remove();
    });

    enableScrollManager();
}

function silentInjection() {
    $('.gdpr_waiting').each(function() {
        type = $(this).attr('data-type');
        in_tag = $(this).attr('data-in-tag');
        in_tag_type = $(this).attr('data-in-tag-type');
        async = $(this).attr('data-async');

        if(GDPR_getCookie('GDPR_' + type) == "1") {
            script = document.createElement(in_tag_type);
            if(in_tag_type == "script") {
                script.type = 'text/javascript';
                script.async = true;
            }

            /*script.onload = function(){};*/

            if(in_tag == "1") {
                script.textContent = $(this).html();
            } else {
                script.src = $(this).html();
                script.async = async;
            }

            document.getElementsByTagName('head')[0].appendChild(script);

            $(this).remove();
        }
    })
}

/* FUNZIONI COOKIE SENZA LIBRERIA */
function GDPR_setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + 31536000000); //1 year
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + ';path=/';
}

function GDPR_getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function GDPR_deleteCookie(key) {
    document.cookie = key + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT';
    GDPR_setCookie(key, 0);
}

function GDPR_acceptAll() {
    if($('#gdpr_modal').length == 0) {
        loadModalFromAjax(false);
    }

    $('#gdpr_modal .gdpr_enable_section').each(function () {

        var $form = $(this);

        var serializedForm = $form.serializeArray();
        var formArray = {};
        for (var i = 0; i < serializedForm.length; i++){
            formArray[serializedForm[i]['name']] = serializedForm[i]['value'];
        }

        GDPR_setCookie('GDPR_' + formArray.cookie_section, 1);
    });

    $('#gdpr_cookie_bar').fadeOut(function() {
        $('#gdpr_open_modal').fadeIn();
    });

    window.GDPRAcceptedAll = true;
    $(document).unbind('scroll');

    unloadModal();
}

function GDPR_saveCurrent() {

    $('#gdpr_modal').fadeOut(function () {
        $('#gdpr_modal').removeClass('show');
    });

    $('#gdpr_modal .gdpr_enable_section').each(function () {

        var $form = $(this);

        var serializedForm = $form.serializeArray();
        var formArray = {};
        for (var i = 0; i < serializedForm.length; i++){
            formArray[serializedForm[i]['name']] = serializedForm[i]['value'];
        }

        if(typeof(formArray.cookie_enable) != 'undefined') {
            GDPR_setCookie('GDPR_' + formArray.cookie_section, 1);
        }
        else
        {
            GDPR_deleteCookie('GDPR_' + formArray.cookie_section);
        }

    });

    if(GDPR_getCookie('GDPR_preferenze') != "1") {
        $('#gdpr_open_modal').fadeOut(function() {
            $('#gdpr_cookie_bar').fadeIn();
        });
    }
}
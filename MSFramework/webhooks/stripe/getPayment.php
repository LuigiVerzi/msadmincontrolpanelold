<?php
/**
 * MSAdminControlPanel
 * Date: 2019-09-11
 */

/*
 * Questo file viene chiamato da un webhook di Stripe attraverso i seguenti URL:
 *
 * login.marketingstudio.it/MSFramework/webhooks/stripe/getPayment.php - per la produzione
 * login.stagingmarketingstudio.it/MSFramework/webhooks/stripe/getPayment.php - per lo staging
 */

define("DO_NOT_CHECK_OFFLINE", true);

require_once('../../../sw-config.php');
\Stripe\Stripe::setApiKey(STRIPE_SECRET);

$event = null;

try {
    $stripeData = $MSFrameworkFW->getCMSData('settings')['payMethods']['stripe'];
    if($stripeData['enable'] != "1") {
        $MSFrameworkUsers->endUserSession();
        header("location: " . ABSOLUTE_SW_PATH_HTML . "login.php?not-logged");
        die();
    }

    $settings_data = $stripeData['pay_data'][PAYMETHODS_USE_KEY];

    $event = \Stripe\Webhook::constructEvent(
        file_get_contents('php://input'), $_SERVER['HTTP_STRIPE_SIGNATURE'], $settings_data['webhook_getpayment']
    );
} catch(\UnexpectedValueException $e) {
    // Invalid payload
    http_response_code(400);
    exit();
} catch(\Stripe\Error\SignatureVerification $e) {
    // Invalid signature
    http_response_code(400);
    exit();
}

if ($event->type == 'checkout.session.completed') {
    $session = $event->data->object;
    $ms_data = json_decode($session->client_reference_id, true);

    $preorder = (new \MSFramework\Framework\payway())->getPreorderData($ms_data);
    $preorder_framework_data = json_decode($preorder['framework_data'], true);

    if($ms_data[0] == "saas__transactions") {
        $MSFrameworkSaaSEnvironments = new \MSFramework\SaaS\environments($preorder_framework_data['environment']['id']);
        $MSFrameworkSaaSEnvironments->renew($ms_data, (array)$session);
    } else if($ms_data[0] == "ticket__transactions") {
        $MSFrameworkTicketsQuotation = new \MSFramework\Framework\Ticket\quotations();
        $MSFrameworkTicketsQuotation->changeStatus($ms_data, 'paid', (array)$session);
    } else if($ms_data[0] == "ms_agency__transactions") {
        $MSAgencyOrders = new \MSFramework\MSAgency\orders();
        $MSAgencyOrders->changeStatus($ms_data, 1, (array)$session);
    }
}

http_response_code(200);
?>
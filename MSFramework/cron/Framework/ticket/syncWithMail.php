#!/usr/local/php71/bin/php -q
<?php
/**
 * PRIMA DI MODIFICARE IL FILE LEGGI!!
 * Per far funzionare il file assicurarsi di salvarlo in US-ASCII
 * Separatore di linee: LF (Unix)
 * Permessi: 755
 */
setlocale(LC_ALL, 'it_IT.UTF8');
date_default_timezone_set('Europe/Rome');
ini_set('max_execution_time', 0);
set_time_limit(0);

$ms_path = dirname(__FILE__, 5);
$_SERVER['HTTP_HOST'] = "www.marketingstudio.it";
$_SERVER['DOCUMENT_ROOT'] = dirname($ms_path, 2);

require_once($ms_path . '/sw-config.php');

$emailMime = file_get_contents("php://stdin");
$email = new EmailUtils\PlancakeEmailParser($emailMime);

$reply_reference = $email->getHeader('References');
if(empty($reply_reference)) {
    $reply_reference = $email->getHeader('in-reply-to');
}

$ticket_info = array();

$exploded_reference = array();
$exploded_reference_ids = array();

if(!empty($reply_reference)) {
    $exploded_reference = explode('@', trim($reply_reference, '><'));
    $exploded_reference_ids = explode('//', $exploded_reference[0]);
}

if ($exploded_reference_ids) {

    $ticket_id = (int)base64_decode($exploded_reference_ids[0]);
    $from_staff = (int)$exploded_reference_ids[1];

    if (is_numeric($ticket_id) && (int)$ticket_id > 0) {
        $ticket_info = (new \MSFramework\Framework\ticket())->getTicketQuestionDetails($ticket_id);
    }
}

$attachments_info = array();
mkdir(UPLOAD_TMP_FOR_DOMAIN, 0777, true);
mkdir(UPLOAD_TMP_FOR_DOMAIN . "tn/", 0777, true);

foreach($email->getAttachments() as $attachment) {
    if($attachment['size'] < 35) {

        $attachment_name = time() . '_' . $attachment['filename'];

        $file_ext = end(explode('.', $attachment['filename']));

        if($attachment['size'] > 5 || !in_array($file_ext, array("png", "jpeg", "jpg", "pdf", "rar", "zip", "doc", "docx", "ppt", "svg", "txt", "xls", "xlsx", "xml"))) {
            // Se il file ? troppo grande o il formato del file non ? consentito allora inserico l'allegato dentro un archivio

            $zip = new \ZipArchive();

            $attachment_name .= "_archive.zip";

            if ($zip->open(UPLOAD_TMP_FOR_DOMAIN . $attachment_name, ZIPARCHIVE::CREATE)) {
                $zip->addFromString($attachment['filename'], base64_decode($attachment['file']));
                $zip->close();
            }

        } else {

            file_put_contents(UPLOAD_TMP_FOR_DOMAIN . $attachment_name, base64_decode($attachment['file']));
            file_put_contents(UPLOAD_TMP_FOR_DOMAIN . 'tn/' . $attachment_name, base64_decode($attachment['file']));

        }

        $attachments_info[] = $attachment_name;
    }
}

if($ticket_info) {

    $ticket_info = $ticket_info[$ticket_id];
    $only_reply = EmailUtils\EmailReplyParser\EmailReplyParser::parseReply($email->getPlainBody());

    if ($from_staff) {
        // Risposta ad un ticket inviata da un assistente
        $admin_email = getSenderEmail($email->getHeader('from'));
        $admin_info = $MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.users WHERE email = :admin_email", array(':admin_email' => $admin_email), true);

        $message_id = (new \MSFramework\Framework\ticket())->addReplyToTicket($ticket_id, 'global-' . (int)$admin_info['id'], $only_reply, $attachments_info, true);

    } else {
        // Richiesta di assistenza inviata da un cliente
        $message_id = (new \MSFramework\Framework\ticket())->addReplyToTicket($ticket_id, array_values($ticket_info['messages'])[0]['author_info']['id'], $only_reply, $attachments_info, true);
    }

} else {
    // Semplice email di richiesta assistenza

    $sender_email = getSenderEmail($email->getHeader('from'));

    $user_domain = '';
    $user_domain_id = '';
    $user_db = '';
    $user_id = 0;
    $user_data = array();

    // Controllo se il dominio della mail ? presente tra i clienti del FW
    $exploded_email = explode('@', $sender_email);

    $is_superadmin = false;
    $is_superadmin_reply = false;

    // Controllo che il mittente non sia un SuperAdmin
    if($MSFrameworkDatabase->getCount("SELECT * FROM marke833_framework.users WHERE email = :admin_email", array(':admin_email' => $sender_email))) {
        $is_superadmin = true;

        // Se la referenza contiene un email allora significa che l'email ? una risposta
        if($exploded_reference_ids && count($exploded_reference_ids) == 2 && filter_var($exploded_reference_ids[0], FILTER_VALIDATE_EMAIL)) {
            $is_superadmin_reply = true;
        }

    }

    if(!$is_superadmin_reply) {
        // Se non ? un superadmin cerco di associarlo ad un cliente

        $check_user_domain = $MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.customers WHERE customer_domain = :customer_domain AND customer_domain NOT LIKE 'framework360.it' AND customer_domain NOT LIKE 'marketingstudio.it'", array(':customer_domain' => $exploded_email[1]), true);

        if ($check_user_domain) {
            // Se il dominio ? stato trovato allora associo il ticket al cliente in questione

            $user_domain = $check_user_domain['customer_domain'];
            $user_domain_id = $check_user_domain['id'];
            $user_db = $check_user_domain['customer_database'];

            $check_user_id = $MSFrameworkDatabase->getAssoc("SELECT * FROM `$user_db`.users WHERE email = :user_email", array(':user_email' => $sender_email), true);

            if ($check_user_id) {
                $user_id = $check_user_id['id'];
                $user_data = $check_user_id;
            }

        } else {
            // Se il dominio non ? stato trovato cerco tra tutti gli utenti di tutti i siti web dentro il FW

            $all_db_sql = array();
            foreach ($MSFrameworkDatabase->getAssoc("SELECT DISTINCT CONCAT('`', TABLE_SCHEMA, '`', '.', '`', 'users`') as db_table, TABLE_SCHEMA as db FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'users' AND TABLE_ROWS > 0 AND TABLE_SCHEMA NOT LIKE '%_framework'") as $db_table) {
                $all_db_sql[] = "SELECT *, '" . $db_table['db'] . "' as db FROM " . $db_table['db_table'];
            }

            $check_user_id = $MSFrameworkDatabase->getAssoc("SELECT * FROM (" . implode(' UNION ', $all_db_sql) . ") global_tables WHERE email = :email ORDER BY ruolo ASC LIMIT 1", array(':email' => $sender_email), true);

            if ($check_user_id) {
                $user_id = $check_user_id['id'];
                $user_data = $check_user_id;
                $user_db = $check_user_id['db'];

                $check_user_domain = $MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_framework.customers WHERE customer_database = :customer_database", array(':customer_database' => $user_db), true);

                if ($check_user_domain) {
                    $user_domain = $check_user_domain['customer_domain'];
                    $user_domain_id = $check_user_domain['id'];
                }
            }
        }

        // Se non ? stato trovato un utente ma sappiamo il dominio di appartenenza creo un utente fittizzio per far ricevere la notifica
        if(!$user_data) {
            $user_data = array(
                'id' => 0,
                'nome' => $sender_email,
                'cognome' => '',
                'email' => $sender_email,
                'username' => $sender_email,
            );
        }

        $new_ticket_id = (new \MSFramework\Framework\ticket())->createNewTicket((!empty($user_domain) ? $user_domain_id : 0), $user_data, $email->getHeader('subject'), $email->getPlainBody(), $sender_email, 'assistenza_tecnica', $attachments_info, true);
    }

    if(empty($user_domain) || !$new_ticket_id) {
        // Se non abbiamo potuto inviare il ticket allora inoltro semplicemente l'email.

        $emailClass = new \MSFramework\emails();
        $emailClass->forceRealRecipients = true;

        // Preparo l'array con gli allegati
        $custom_email_attachments = array();
        foreach($attachments_info as $attachment) {
            $custom_email_attachments[] = array(
                'path' => UPLOAD_TMP_FOR_DOMAIN . $attachment,
                'name' => $attachment
            );
        }

        if($is_superadmin_reply) {
            $emailClass->mail->setFrom(FRAMEWORK_SUPPORT_EMAIL, 'Assistenza Marketing Studio');
            $emailClass->sendCustomMail($email->getHeader('subject'), $email->getHTMLBody(), $exploded_reference_ids[0], array('email' => FRAMEWORK_SUPPORT_EMAIL, 'name' => 'Assistenza Marketing Studio'), $email->getPlainBody(), $custom_email_attachments);
        } else {
            // Imposto la referenza del cliente nell'header dell'email cos? che sar? poi possibile rispondere al cliente
            $emailClass->mail->MessageID = sprintf('<%s@%s>', base64_encode($sender_email) . '//' . time(), 'framework360.it');

            $emailClass->mail->setFrom(FRAMEWORK_SUPPORT_EMAIL, getSenderName($email->getHeader('from')));
            $emailClass->sendCustomMail($email->getHeader('subject'), $email->getHTMLBody(), $emailClass->getSuperAdminEmails(1), array('email' => FRAMEWORK_SUPPORT_EMAIL, 'name' => getSenderName($email->getHeader('from'))), $email->getPlainBody(), $custom_email_attachments);
        }

    }
}

// Elimino eventuali allegati dalla cartella temporanea del server
foreach($attachments_info as $attachment) {
    unlink(UPLOAD_TMP_FOR_DOMAIN . $attachment);
    unlink(UPLOAD_TMP_FOR_DOMAIN . 'tn/' . $attachment);
}

function getSenderEmail($from) {

    preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $from, $matches);

    if($matches && $matches[0]) {
        return $matches[0][0];
    }

    return '';
}

function getSenderName($from) {
    return preg_replace("/\s<[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+>/i", '', $from);
}

header("HTTP/1.1 200 OK");
exit;
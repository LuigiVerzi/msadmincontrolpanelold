<?php
/**
 * MSAdminControlPanel
 * Date: 12/12/18
 */

//Questo file gestisce le mail da inviare in base alle notifiche attivate nel modulo "Gestione Notifiche"

$notifications_settings = $MSFrameworkCMS->getCMSData('notifications_settings');
if($notifications_settings['is_active'] === "0") {
    //le notifiche sono state disabilitate per l'intero sito
    exit;
}

$availNotifications = (new \MSFramework\notifications())->getAvailNotifications();

foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM notifications WHERE is_active = '1'") as $scheduled_notification) {
    $email_settings = json_decode($scheduled_notification['email_settings'], true);
    $specific_settings = json_decode($scheduled_notification['specific_settings'], true);

    $availNotifications[$scheduled_notification['type']]['send']($email_settings, $specific_settings);
}
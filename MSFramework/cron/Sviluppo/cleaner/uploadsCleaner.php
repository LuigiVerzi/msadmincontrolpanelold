<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

$exclude_dir = array(
    '.',
    '..',
    'MSAdminControlPanel',
    'MSCommonStorage',
    CUSTOMER_DOMAIN_INFO['virtual_server']
);

foreach(scandir(SITES_HOME_FOLDER) as $wip_website) {
    $path = SITES_HOME_FOLDER . $wip_website;

    if(!in_array($wip_website, $exclude_dir)) {
        $upload_path = $path . '/uploads';

        if(file_exists($upload_path)) {
            foreach (glob($upload_path . '/*') as $upload_file) {
                recursiveRemove($upload_file);
            }
        }
    }

}

function recursiveRemove($dir) {
    $structure = glob(rtrim($dir, "/").'/*');
    if (is_array($structure)) {
        foreach($structure as $file) {
            if (is_dir($file)) recursiveRemove($file);
            elseif (is_file($file)) unlink($file);
        }
    }
}
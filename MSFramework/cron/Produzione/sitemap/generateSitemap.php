<?php
/**
 * MSAdminControlPanel
 * Date: 27/09/2018
 */

if(isset($_GET['from_acp'])) {
    include('../../../../sw-config.php');
}

$languagesInfo = array(
    'activeLanguages' => $MSFrameworki18n->getActiveLanguagesDetails(),
    'primaryId' => $MSFrameworki18n->getPrimaryLangId(),
    'langClasses' => array()
);

foreach($languagesInfo['activeLanguages'] as $lang) {
    $languagesInfo['langClasses'][$lang['id']] = new \MSFramework\i18n($lang['id']);
}

$base_url = $MSFrameworkCMS->getURLToSite();

$mainSiteFolderpath = realpath(dirname( __FILE__ , 7)) . "/" . $MSFrameworkCMS->getVirtualServerName() . "/";
$must_update = false;
$sitemap_settings = json_decode($MSFrameworkCMS->getCMSData('site')['sitemap'], true);

if(!isset($_GET['from_acp'])) { //se la chiamata non viene fatta da ACP (quindi viene dal CJ) verifico se l'utente ha richiesto l'aggiornamento automatico e se è il caso di aggiornare
    $last_updated_sitemap = (filemtime($mainSiteFolderpath . "sitemap.xml") ? filemtime($mainSiteFolderpath . "sitemap.xml") : time()-1);

    if($sitemap_settings['auto_generate'] == "1") {
        $time_diff = time()-$last_updated_sitemap;
        $ary_regen_freq = array("1" => 1, "2" => 7, "3" => 15, "4" => 30);
        if($time_diff > (86400*$ary_regen_freq[$sitemap_settings['regen_freq']])) {
            $must_update = true;
        }
    }
} else {
    $must_update = true;
}

if($must_update) {
    $MSFrameworkSitemap = new \MSFramework\sitemap($base_url, $mainSiteFolderpath);
    $done_urls = array();

    $primary_lang_details = $MSFrameworki18n->getLanguagesDetails($languagesInfo['primaryId'], 'url_code')[$languagesInfo['primaryId']];

    $primary_base_url = $base_url;
    $done_urls[$primary_base_url][] = array("code" => $primary_lang_details['url_code'], "url" => $primary_base_url);

    //URL BASE
    foreach($languagesInfo['activeLanguages'] as $lingua) {
        if($lingua['is_primary'] != "1") {
            $done_urls[$primary_base_url][] = array("code" => $lingua['url_code'], "url" => $base_url . $lingua['url_code']);
        }
    }

    //PAGINE
    foreach ((new \MSFramework\pages())->getPageDetails('') as $page_id => $details) {
        if(stristr($page_id, 'global-')) continue;
        $done_urls = array_merge($done_urls, composePagei18nArray($details));
    }

    //HOTEL
    if (in_array('hotel', json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true))) {
        foreach ((new \MSFramework\Hotels\offers())->getOfferDetails('', true, "slug") as $details) {
            $done_urls = array_merge($done_urls, composei18nArray($details['slug']));
        }

        foreach ((new \MSFramework\Hotels\rooms())->getRoomDetails('', "slug") as $details) {
            $done_urls = array_merge($done_urls, composei18nArray($details['slug']));
        }
    }

    //CAMPEGGI
    if (in_array('camping', json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true))) {
        foreach ((new \MSFramework\Camping\offers())->getOfferDetails('', true, "slug") as $details) {
            $done_urls = array_merge($done_urls, composei18nArray($details['slug']));
        }

        foreach ((new \MSFramework\Camping\piazzole())->getPiazzolaDetails('', "slug") as $details) {
            $done_urls = array_merge($done_urls, composei18nArray($details['slug']));
        }
    }

    //ECOMMERCE
    if (in_array('ecommerce', json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true))) {

        foreach ($MSFrameworkDatabase->getAssoc("SELECT id, slug FROM `ecommerce_categories` WHERE 1") as $details) {
            $done_urls = array_merge($done_urls, composeEcommerceCategoryi18nArray($details));
        }

        foreach ($MSFrameworkDatabase->getAssoc("SELECT slug FROM `ecommerce_products` WHERE 1") as $details) {
            $done_urls = array_merge($done_urls, composei18nArray($details['slug']));
        }
    }

    //BLOG
    if (in_array('blog', json_decode($MSFrameworkCMS->getCMSData('producer_config')['extra_functions'], true))) {
        foreach ($MSFrameworkDatabase->getAssoc("SELECT slug FROM `blog_categories` WHERE 1") as $details) {
            $done_urls = array_merge($done_urls, composei18nArray($details['slug']));
        }

        foreach ($MSFrameworkDatabase->getAssoc("SELECT slug FROM `blog_posts` WHERE 1") as $details) {
            $done_urls = array_merge($done_urls, composei18nArray($details['slug']));
        }
    }

    foreach($done_urls as $primary_url => $oth_urls) {
        $MSFrameworkSitemap->addUrl($primary_url, null, null, null, $oth_urls);
    }

    $MSFrameworkSitemap->createSitemap();
    $MSFrameworkSitemap->writeSitemap();
    $MSFrameworkSitemap->updateRobots();

    if(!strstr($_SERVER['DOCUMENT_ROOT'], "/SVILUPPO/") && $sitemap_settings['auto_send'] == "1") {
        $MSFrameworkSitemap->submitSitemap();
    }
}

if(isset($_GET['from_acp'])) {
    echo json_encode(array("upd_date" => date("d/m/Y H:i"), "urls" => $done_urls));
}

function composei18nArray($slug) {
    Global $languagesInfo;
    $got_data = array();

    foreach($languagesInfo['activeLanguages'] as $lingua) {
        if ($lingua['is_primary'] == "1") {
            $saved_primary = convertURL($slug, $lingua['id']);
        }

        $got_data[] = array("code" => $lingua['url_code'], "url" => convertURL($slug, $lingua['id']));
    }

    $done_urls = array();
    foreach($got_data as $data) {
        $done_urls[$saved_primary][] = $data;
    }

    return $done_urls;
}

function composeEcommerceCategoryi18nArray($page_det) {
    Global $languagesInfo, $base_url, $MSFrameworkUrl;

    $MSFrameworkEcommerceCategory = new \MSFramework\Ecommerce\categories();

    $got_data = array();

    foreach($languagesInfo['activeLanguages'] as $lingua) {

        $slug = '';

        $have_parents = $MSFrameworkEcommerceCategory->getCategoryParent($page_det['id']);

        if ($have_parents) {
            foreach ($have_parents as $parent) {
                $slug .= $languagesInfo['langClasses'][$lingua['id']]->getFieldValue($parent['slug']) . "/";
            }
        }

        $slug .=  $languagesInfo['langClasses'][$lingua['id']]->getFieldValue($page_det['slug']) . "/";

        $prefix = "";
        if($lingua['is_primary'] !== "1") {
            $prefix = $languagesInfo['activeLanguages'][$lingua['id']]['url_code'] . "/";
        }

        if ($lingua['is_primary'] == "1") {
            $saved_primary = $base_url . $prefix . $slug;
        }

        $got_data[] = array("code" => $lingua['url_code'], "url" => $base_url . $prefix . $slug);

    }


    $done_urls = array();
    foreach($got_data as $data) {
        $done_urls[$saved_primary][] = $data;
    }

    return $done_urls;
}

function composePagei18nArray($page_det) {
    Global $languagesInfo, $MSFrameworkPages;
    $got_data = array();

    foreach($languagesInfo['activeLanguages'] as $lingua) {
        if ($lingua['is_primary'] == "1") {
            $saved_primary = $MSFrameworkPages->getURL($page_det, $languagesInfo['langClasses'][$lingua['id']]);
        }

        $got_data[] = array("code" => $lingua['url_code'], "url" => $MSFrameworkPages->getURL($page_det, $languagesInfo['langClasses'][$lingua['id']]));
    }

    $done_urls = array();
    foreach($got_data as $data) {
        $done_urls[$saved_primary][] = $data;
    }

    return $done_urls;
}

function convertURL($slug, $lang_id) {
    global $languagesInfo, $base_url, $MSFrameworkUrl;
    //non posso utilizzare le funzioni getURL delle classi, perchè getFieldValue non mi restituirebbe il valore di default per eventuali slug non tradotti

    $prefix = "";
    if($languagesInfo['primaryId'] != $lang_id) {
        $prefix = $languagesInfo['activeLanguages'][$lang_id]['url_code'] . "/";
    }

    return $base_url . $prefix . $MSFrameworkUrl->cleanString($languagesInfo['langClasses'][$lang_id]->getFieldValue($slug, true)) . "/";
}
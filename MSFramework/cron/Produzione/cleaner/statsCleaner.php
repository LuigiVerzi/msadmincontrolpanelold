<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

$MSFrameworkDatabase->query("DELETE FROM `stats_visitatori` WHERE STR_TO_DATE(CONCAT(day, ' ', hour, ':', minute), '%Y-%m-%d %H:%i') < (NOW() - INTERVAL 30 DAY)");
$MSFrameworkDatabase->query("DELETE FROM `blog_stats` WHERE STR_TO_DATE(CONCAT(day, ' ', hour, ':', minute), '%Y-%m-%d %H:%i') < (NOW() - INTERVAL 30 DAY)");
$MSFrameworkDatabase->query("DELETE FROM `notifications__histories` WHERE sent_date < (NOW() - INTERVAL 365 DAY)");
$MSFrameworkDatabase->query("DELETE FROM `forms_histories` WHERE submit_date < (NOW() - INTERVAL 365 DAY)");

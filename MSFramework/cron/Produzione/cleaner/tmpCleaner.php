<?php
/**
 * MSAdminControlPanel
 * Date: 16/09/18
 */

recursiveRemoveOldFiles(ABSOLUTE_SW_PATH . 'uploads/tmp/');

function recursiveRemoveOldFiles($dir) {
    $structure = glob(rtrim($dir, "/").'/*');
    if (is_array($structure)) {
        foreach($structure as $file) {
            if (is_dir($file)) recursiveRemove($file);
            elseif (is_file($file)) {
                if(filemtime($file) <= strtotime('-6 hours')) {
                    unlink($file);
                }
            }
        }
    }
}

?>

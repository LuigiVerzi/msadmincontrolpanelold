<?php
/**
 * MSFramework
 * Date: 17/05/2019

 * File principale per la gestione dei Cron Job
 */

setlocale(LC_ALL, 'it_IT.UTF8');
date_default_timezone_set('Europe/Rome');
ini_set('max_execution_time', 0);
set_time_limit(0);

$ms_path = dirname(__FILE__, 3);

$_SERVER['HTTP_HOST'] = "www.marketingstudio.it"; //questo file viene chiamato da cronJob, devo impostare un nome fittizio per consentire al DB di connettersi ad un cliente specifico
$_SERVER['DOCUMENT_ROOT'] = dirname($ms_path, 2);
require_once($ms_path . '/sw-config.php');

$MSFrameworkCronJobs = new \MSFramework\cronjob();

$cron_urls = array();

/* AGGIUNGO I CRON DA LANCIARE NEL FRAMEWORK */
foreach ($MSFrameworkCronJobs->getFrameworkCronJobs() as $cronDirectory => $cronJobs) {
    foreach ($cronJobs as $cronFile => $cronJob) {
        /* SE IL CONTROLLO PRELIMINARE RISULTA POSITIVO EFFETTUO LA CHIAMATA */
        if (is_time_cron(time(), implode(' ', $cronJob['intervallo'])) && $cronJob['check']($MSFrameworkDatabase, $MSFrameworkCMS)) {
            $cron_urls[] = 'http://marketingstudio.it/' . $MSFrameworkCronJobs->getShortPrefix() . '/' . $cronDirectory . '/' . $cronFile;
        }
    }
}

/* OTTENGO I CRONJOB DA LANCIARE IN PRODUZIONE */
foreach($MSFrameworkDatabase->getAssoc("SELECT virtual_server_name, customer_domain FROM " . FRAMEWORK_DB_NAME . ".customers") as $site) {

    /* RIDEFINISCO LE FUNZIONI CON I PARAMETRI DEL SITO ATTUALE */
    $_SERVER['HTTP_HOST'] = $site['customer_domain'];

    $MSFrameworkDatabase = new \MSFramework\database();
    $MSFrameworkCMS = new \MSFramework\cms();

    if($site['virtual_server_name'] == '') {
        foreach ($MSFrameworkCronJobs->getStandardCronJobs() as $cronDirectory => $cronJobs) {
            foreach ($cronJobs as $cronFile => $cronJob) {
                /* SE IL CONTROLLO PRELIMINARE RISULTA POSITIVO EFFETTUO LA CHIAMATA */
                if (is_time_cron(time(), implode(' ', $cronJob['intervallo'])) && $cronJob['check']($MSFrameworkDatabase, $MSFrameworkCMS)) {
                    $cron_urls[] = 'http://' . $site['customer_domain'] . '/' . $MSFrameworkCronJobs->getShortPrefix() . '/' . $cronDirectory . '/' . $cronFile;
                }
            }
        }
    } else {
        foreach($MSFrameworkCronJobs->getDevelopmentCronJobs() as $cronDirectory => $cronJobs) {
            foreach($cronJobs as $cronFile => $cronJob) {
                /* SE IL CONTROLLO PRELIMINARE RISULTA POSITIVO EFFETTUO LA CHIAMATA */
                if(is_time_cron(time(), implode(' ', $cronJob['intervallo'])) && $cronJob['check']($MSFrameworkDatabase, $MSFrameworkCMS)) {
                    $cron_urls[] = 'http://' . $site['customer_domain'] . '/' . $site['virtual_server_name'] . '/' . $MSFrameworkCronJobs->getShortPrefix() . '/' . $cronDirectory . '/' . $cronFile;
                }
            }
        }
    }
}

if(!$cron_urls) {
    die();
}

$requests = array();
$mh = curl_multi_init();
foreach($cron_urls as $k => $url){
    $requests[$k] = array();
    $requests[$k]['url'] = $url;
    $requests[$k]['curl_handle'] = curl_init($url);
    curl_setopt($requests[$k]['curl_handle'], CURLOPT_RETURNTRANSFER, true);
    curl_setopt($requests[$k]['curl_handle'], CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($requests[$k]['curl_handle'], CURLOPT_TIMEOUT, 10);
    curl_setopt($requests[$k]['curl_handle'], CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($requests[$k]['curl_handle'], CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($requests[$k]['curl_handle'], CURLOPT_SSL_VERIFYPEER, false);
    curl_multi_add_handle($mh, $requests[$k]['curl_handle']);
}

$stillRunning = false;
do {
    curl_multi_exec($mh, $stillRunning);
} while ($stillRunning);

foreach($requests as $k => $request){
    curl_multi_remove_handle($mh, $request['curl_handle']);
    curl_close($requests[$k]['curl_handle']);
}
curl_multi_close($mh);


function is_time_cron($time , $cron)
{
    $cron_parts = explode(' ' , $cron);
    if(count($cron_parts) != 5)
    {
        return false;
    }

    list($min , $hour , $day , $mon , $week) = explode(' ' , $cron);

    $to_check = array('min' => 'i' , 'hour' => 'G' , 'day' => 'j' , 'mon' => 'n' , 'week' => 'w');

    $ranges = array(
        'min' => '0-59' ,
        'hour' => '0-23' ,
        'day' => '1-31' ,
        'mon' => '1-12' ,
        'week' => '0-6' ,
    );

    foreach($to_check as $part => $c)
    {
        $val = $$part;
        $values = array();

        if(strpos($val , '/') !== false)
        {
            list($range , $steps) = explode('/' , $val);

            if($range == '*')
            {
                $range = $ranges[$part];
            }
            list($start , $stop) = explode('-' , $range);

            for($i = $start ; $i <= $stop ; $i = $i + $steps)
            {
                $values[] = $i;
            }
        }
        else
        {
            $k = explode(',' , $val);

            foreach($k as $v)
            {
                if(strpos($v , '-') !== false)
                {
                    list($start , $stop) = explode('-' , $v);

                    for($i = $start ; $i <= $stop ; $i++)
                    {
                        $values[] = $i;
                    }
                }
                else
                {
                    $values[] = $v;
                }
            }
        }

        if ( !in_array( date($c , $time) , $values ) and (strval($val) != '*') )
        {
            return false;
        }
    }

    return true;
}
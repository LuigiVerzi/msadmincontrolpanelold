<?php

namespace Interpid\PdfLib;

/**
 * TCPDF extended class.
 * This class extends the TCPDF class. In all subclasses we refer to Pdf class and not TCPDF.
 * Also some methods and variables are set to Public in order to access them in the addons.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL WE OR OUR SUPPLIERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT
 * OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS
 * OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR ANY OTHER
 * PECUNIARY LAW) ARISING OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE, EVEN IF WE
 * HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * @author    : Interpid <office@interpid.eu>
 * @package   : Interpid\PdfLib
 * @copyright : Interpid, http://www.interpid.eu
 * @license   : http://www.interpid.eu/pdf-addons/eula
 * @SuppressWarnings(PHPMD)
 */
class Pdf extends \TCPDF\TCPDF
{
    public $images;
    public $w;
    public $tMargin;
    public $bMargin;
    public $lMargin;
    public $rMargin;
    public $k;
    public $h;
    public $x;
    public $y;
    public $ws;
    public $FontFamily;
    public $FontStyle;
    public $FontSize;
    public $FontSizePt;
    public $CurrentFont;
    public $TextColor;
    public $FillColor;
    public $ColorFlag;
    public $AutoPageBreak;
    public $CurOrientation;
    public $encoding;
    public $isunicode;
    public $doc_creation_timestamp;
    public $doc_modification_timestamp;
    public $file_id;
    public $tcpdf_version;
    public $svgunit;

    // phpcs:disable
    public function _out($s)
    {
        return parent::_out($s);
    }
    // phpcs:enable

    public function getCellCode(
        $w,
        $h = 0,
        $txt = '',
        $border = 0,
        $ln = 0,
        $align = '',
        $fill = false,
        $link = '',
        $stretch = 0,
        $ignore_min_height = false,
        $hAlign = 'T',
        $vAlign = 'M'
    ) {
        return parent::getCellCode(
            $w,
            $h,
            $txt,
            $border,
            $ln,
            $align,
            $fill,
            $link,
            $stretch,
            $ignore_min_height,
            $hAlign,
            $vAlign
        );
    }

    public function saveToFile($fileName)
    {
        $this->Output($fileName, "F");
    }
}

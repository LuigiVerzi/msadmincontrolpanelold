<?php
/**
 * MSAdminControlPanel
 * Date: 2019-05-18
 */

namespace Scraper;

class utils {
    /**
     * Questa funzione si occupa di controllare se il dato richiesto è presente e, nel caso, restituire il valore richiesto in base ad eventuali filtri applicati
     *
     * @param $selector Il selettore dell'elemento da cercare
     * @param string $what text|attr. Se è impostato su "text" restituisce il testo contenuto nel selettore. Se impostato su "attr" restituisce il valore dell'attributo impostato nel parametro $attrToGet
     * @param array $filterByAttr Un array di filtri da applicare sugli attributi. La chiave dell'array è l'attributo, mentre il valore è il testo che deve avere l'attributo affinchè venga incluso nei risultati.
     * @param string $attrToGet Il parametro del quale si vuole recuperare il valore (viene utilizzato solo se $what == "attr")
     *
     * @return array|string
     */
    public function getSelectorContent($selector, $what = "text", $filterByAttr = array(), $attrToGet = "") {
        global $doc;

        $r = $doc->find($selector);
        if($r != null) {
            if(count($filterByAttr) != 0 && is_array($filterByAttr)) {
                $ary_from_attrs = array();

                foreach($r as $element) {
                    foreach ($filterByAttr as $filterK => $filterV) {
                        if ($element->attr($filterK) != $filterV) {
                            continue;
                        }

                        $ary_from_attrs[$filterV] = $r->attr($attrToGet);
                    }
                }

                return $ary_from_attrs;
            }


            if($what == "text") {
                return trim($r->text());
            } else if($what == "clean_html") {
                $html = strip_tags($r->html(), '<br><br />');
                $html = str_replace(array("<br />", "<br/>", "<br>"), array("\n", "\n", "\n"), $html);
                return trim($html);
            } else if($what == "attr") {
                return trim($r->attr($attrToGet));
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    /**
     * Returns a random valid public IP address. For the definition of a
     * public IP address see http://www.faqs.org/rfcs/rfc1918.html
     * @return string The IP address
     */
    public function random_valid_public_ip() {
        // Generate a random IP
        $ip =mt_rand(0, 255) . '.' .mt_rand(0, 255) . '.' .mt_rand(0, 255) . '.' .mt_rand(0, 255);

        // Return the IP if it is a valid IP, generate another IP if not
        if (
            !$this->ip_in_range($ip, '10.0.0.0', '10.255.255.255') &&
            !$this->ip_in_range($ip, '172.16.0.0', '172.31.255.255') &&
            !$this->ip_in_range($ip, '192.168.0.0', '192.168.255.255')
        ) {
            return $ip;
        } else {
            return $this->random_valid_public_ip();
        }
    }

    /**
     * Returns true if the IP address supplied is within the range from
     * $start to $end inclusive
     * @param string $ip The IP address to be checked
     * @param string $start The start IP address
     * @param string $end The end IP address
     * @return boolean
     */
    private function ip_in_range($ip, $start, $end) {
        // Split the IP addresses into their component octets
        $i = explode('.', $ip);
        $s = explode('.', $start);
        $e = explode('.', $end);

        // Return false if the IP is in the restricted range
        return in_array($i[0], range($s[0], $e[0])) &&
            in_array($i[1], range($s[1], $e[1])) &&
            in_array($i[2], range($s[2], $e[2])) &&
            in_array($i[3], range($s[3], $e[3]));
    }
}
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-04
 */

namespace cPanel\Database;
use cPanel\cPanel;

class database extends cPanel {
    /**
     * Crea un nuovo database MYSQL
     * @param $db_name string Il nome del database (includere marke833_)
     * @return $this
     */
    public function create($db_name) {

        $db_name = $this->addPrefix($db_name);

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "MysqlFE",
                "cpanel_jsonapi_func" => "createdb",
                "db" => $db_name
            )
        );

        return $this;
    }

    /**
     * Crea un nuovo database MYSQL
     * @param $db_name string Il nome del database (includere marke833_)
     * @param $user_name string Il nome dell'utente MYSQL
     * @param $privileges string I privilegi da assegnare all'utente
     * @return $this
     */
    public function assignUser($db_name, $user_name, $privileges = "ALL PRIVILEGES") {

        $db_name = $this->addPrefix($db_name);
        $user_name = $this->addPrefix($user_name);

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "MysqlFE",
                "cpanel_jsonapi_func" => "setdbuserprivileges",
                "db" => $db_name,
                "dbuser" => $user_name,
                "privileges" => $privileges
            )
        );

        return $this;
    }

    /**
     * Aggiunge il prefisso marke833_ se non presente
     * @param $string string Il nome del database/nome utente da preparare
     * @return string Ritorna la stringa con il prefisso
     */
    public function addPrefix($string) {
        if(strpos($string, 'marke833_') === false) {
            $string = 'marke833_' . $string;
        }

        return $string;
    }

    /**
     * Cancella un database MYSQL
     * @param $db_name string Il nome del database (includere marke833_)
     * @return $this
     */
    public function delete($db_name) {
        $db_name = $this->addPrefix($db_name);

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "MysqlFE",
                "cpanel_jsonapi_func" => "deletedb",
                "db" => $db_name
            )
        );

        return $this;
    }
}
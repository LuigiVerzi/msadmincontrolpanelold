<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-04
 */

namespace cPanel\Ftp;
use cPanel\cPanel;

class ftp extends cPanel {
    /**
     * Crea un nuovo sottodominio
     * @param $domain string Il dominio del sito
     * @param $user string Il nome utente
     * @param $pass string La password
     * @param $quota int La quota massima che l'utente può occupare
     * @return $this
     */
    public function create($domain, $user, $pass, $quota = 250) {
        $domain = str_replace("www.", "", $domain);

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "Ftp",
                "cpanel_jsonapi_func" => "addftp",
                "domain" => $domain,
                "homedir" => MS_PRODUCTION_DOCUMENT_ROOT . $domain,
                "user" => $user,
                "pass" => $pass
            )
        );

        \ChromePhp::log(array(
            "cpanel_jsonapi_module" => "Ftp",
            "cpanel_jsonapi_func" => "addftp",
            "domain" => $domain,
            "homedir" => MS_PRODUCTION_DOCUMENT_ROOT . $domain,
            "user" => $user,
            "pass" => $pass
        ));

        return $this;
    }

    /**
     * Elimina un account FTP
     *  @param $domain string Il dominio del sito
     * @param $user string Il nome utente dell'account FTP da eliminare
     * @return $this
     */
    public function delete($domain, $user) {

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "Ftp",
                "cpanel_jsonapi_func" => "delftp",
                "domain" => $domain,
                "user" => $user,
                "destroy" => 0
            )
        );

        return $this;
    }

    /**
     * Crea un nuovo sottodominio
     * @param $user string Il nome utente
     * @param $pass string La nuova password
     * @return $this
     */
    public function changePassword($user, $pass) {

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "Ftp",
                "cpanel_jsonapi_func" => "passwd",
                "user" => $user,
                "pass" => $pass
            )
        );

        return $this;
    }
}
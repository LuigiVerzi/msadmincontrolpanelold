<?php

namespace cPanel;

class cPanel
{

    private $cpanel_url = "https://securees59.sgcpanel.com:2083/";

    private $cp_user = "marke833";
    private $cp_pwd = "Marketingh24@";

    private $ch = false;
    private $session_id = '';

    /**
     * Effettua il login sul cPanel e memorizza l'id della sessione nella variabile $this->session_id
     */
    public function __construct() {

        ob_start();
        ob_get_contents();

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_URL, $this->cpanel_url . "login");
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, SITES_HOME_FOLDER . "cookies.txt");
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, "user=" . $this->cp_user . "&pass=" . $this->cp_pwd);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 100020);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

        $f = curl_exec($this->ch);
        $h = curl_getinfo($this->ch);

        if ($f == true and strpos($h['url'],"cpsess"))
        {
            $pattern="/.*?(\/cpsess.*?)\/.*?/is";
            preg_match($pattern,$h['url'],$cpsess);
        }

        $this->session_id = $cpsess[1];
    }


    /**
     * @param $params array L'array con i parametri della chiamata
     */
    protected function exec($params) {

        curl_setopt(
            $this->ch,
            CURLOPT_URL,
            $this->cpanel_url . $this->session_id . "/json-api/cpanel?cpanel_jsonapi_user=user&cpanel_jsonapi_apiversion=2&" . http_build_query($params)
        );

        \ChromePhp::log(curl_exec($this->ch));

    }

    /**
     * Chiude la connessione cUrl
     */
    public function close() {
        curl_close($this->ch);
        ob_end_clean();
    }

}
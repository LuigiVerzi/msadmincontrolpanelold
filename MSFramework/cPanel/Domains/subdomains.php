<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-04
 */

namespace cPanel\Domains;
use cPanel\cPanel;

class subdomains extends cPanel {
    /**
     * Crea un nuovo sottodominio
     * @param $subdomain string Il sottodominio (solo il nome pulito del sottodominio senza punti o altro)
     * @param $rootdomain string Il dominio principale (es: marketingstudio.it)
     * @param $directory string La directory dentro la quale far puntare il sottodominio (es: /home/marke833/FRAMEWORK/example.ext)
     * @return $this
     */
    public function create($subdomain, $rootdomain, $directory = "") {
        $subdomain = str_replace("www.", "", $subdomain);

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "SubDomain",
                "cpanel_jsonapi_func" => "addsubdomain",
                "domain" => $subdomain,
                "rootdomain" => $rootdomain,
                'dir' => ($directory != "" ? $directory : MS_PRODUCTION_DOCUMENT_ROOT . $subdomain),
            )
        );

        return $this;
    }

    /**
     * Elimina un sottodominio
     * @param $subdomain string Il sottodominio (solo il nome pulito del sottodominio senza punti o altro)
     * @return $this
     */
    public function delete($subdomain) {
        $subdomain = str_replace("www.", "", $subdomain);

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "SubDomain",
                "cpanel_jsonapi_func" => "delsubdomain",
                "domain" => $subdomain,
            )
        );

        return $this;
    }

    /**
     * Modifica la directory di un sottodominio esistente
     * @param $subdomain string Il sottodominio (solo il nome pulito del sottodominio senza punti o altro)
     * @param $rootdomain string Il dominio principale (es: marketingstudio.it)
     * @param $directory string La directory dentro la quale far puntare il sottodominio (es: /FRAMEWORK/sottodominio.marketingstudio.it)
     * @return $this
     */
    public function changeDir($subdomain, $rootdomain, $directory) {

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "SubDomain",
                "cpanel_jsonapi_func" => "changedocroot",
                "domain" => $subdomain,
                "rootdomain" => $rootdomain,
                "dir" => $directory,
            )
        );

        return $this;
    }
}
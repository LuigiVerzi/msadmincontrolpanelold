<?php
/**
 * MSAdminControlPanel
 * Date: 2019-07-04
 */

namespace cPanel\Domains;
use cPanel\cPanel;

class additionalDomains extends cPanel {
    /**
     * Crea un nuovo dominio aggiuntivo
     * @param $domain string Il dominio da creare (senza www.)
     * @param $directory string La directory dentro la quale far puntare il sottodominio (es: /home/marke833/FRAMEWORK/example.ext)
     * @return $this
     */
    public function create($domain, $directory = "") {
        $domain = str_replace("www.", "", $domain);

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "AddonDomain",
                "cpanel_jsonapi_func" => "addaddondomain",
                'dir' => ($directory != "" ? $directory : MS_PRODUCTION_DOCUMENT_ROOT . $domain),
                'newdomain' => $domain,
                'subdomain' => explode(".", $domain)[0],
            )
        );

        return $this;
    }

    /**
     * Elimina un dominio aggiuntivo
     * @param $domain string Il dominio da eliminare (senza www.)
     * @return $this
     */
    public function delete($domain) {
        $domain = str_replace("www.", "", $domain);

        $this->exec(
            array(
                "cpanel_jsonapi_module" => "AddonDomain",
                "cpanel_jsonapi_func" => "deladdondomain",
                'domain' => $domain,
                'subdomain' => explode(".", $domain)[0] . "_marketingstudio.agency",
            )
        );

        return $this;
    }
}
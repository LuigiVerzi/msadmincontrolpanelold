<?php
/**
 * MSFramework
 * Date: 16/02/18

 * File di base per la configurazione della gestione delle classi
 */

//error_reporting(E_ALL);

error_reporting(E_ERROR);

/*
 * Questa costante definisce il numero di versione corrente del pacchetto ACP+Framework.
 * In base a questo valore, verranno eseguite procedure di aggiornamento sugli archivi.
 * Il valore viene aggiornato automaticamente dal modulo "Aggiornamento SW" quando FW360 viene mandato in produzione.
 */

date_default_timezone_set('Europe/Rome');

$info_json = json_decode(file_get_contents(dirname( __FILE__ , 2) . "/info.json"), true);
define("SW_VERSION", $info_json['version']);
define("ADMIN_URL", "login."); //url default per accedere al pannello di amministrazione

//condivido la sessione anche con i sottodomini
preg_match('@^(?:http://)?([^/]+)@i', $_SERVER['HTTP_HOST'], $matches);
preg_match('/[^.]+\\.[^.]+$/', $matches[1], $matches);
ini_set('session.cookie_domain', '.' . $matches[0]);

// Aumento la durata della sessione fino a 10 ore
ini_set('session.gc_maxlifetime', (3600 * 10));
session_set_cookie_params((3600 * 10));

session_start();
ob_start();

if(!isset($_SESSION['start_time'])) $_SESSION['start_time'] = time();

define('SSL_ENABLED', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443));

/* url completa della pagina */
$GLOBALS['CANONICAL_URL'] = (SSL_ENABLED ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

/* costanti utilizzate per gli include/require di PHP */
define("FRAMEWORK_ABSOLUTE_PATH", dirname( __FILE__ ) . "/");
define("FRAMEWORK_ABSOLUTE_PATH_HTML", str_replace($_SERVER['DOCUMENT_ROOT'], "", FRAMEWORK_ABSOLUTE_PATH));

/* costanti utilizzate per i Database (NON quello dei clienti, ma quello "madre") */
require_once(FRAMEWORK_ABSOLUTE_PATH . 'common/database-config.php');

define("UPLOAD_FOLDERPATH_NAME", "uploads"); //Il nome della cartella (principale) che contiene tutti gli uploads. Tale cartella si troverà all'interno delle cartelle dei vari clienti (siti)
define("DEV_FOLDER_NAME", "SVILUPPO"); //Il nome della cartella all'interno della quale sono contenuti i siti in fase di sviluppo (riservata alla fase DEV)
define("PROD_FOLDER_NAME", "FRAMEWORK"); //Il nome della cartella all'interno della quale sono contenuti i siti rilasciati (riservata alla fase PROD)

define("FRAMEWORK_NOREPLY_MAIL", json_encode(array( //i dati per l'accesso alla casella di posta "noreply" di framework360
    "mail" => "noreply@framework360.it",
    "mail_name" => "Framework360 - NoReply",
    "pass" => "jhT6yGf4s",
    "host" => "securees59.sgcpanel.com",
)));

define("MARKETINGSTUDIO_NOREPLY_MAIL", json_encode(array( //i dati per l'accesso alla casella di posta "noreply" di marketing studio (da non confondere con quella del cliente che è definita in sw-config.php)
    "mail" => "noreply@marketingstudio.it",
    "mail_name" => "Marketing Studio - NoReply",
    "pass" => "jhT6yGf4s",
    "host" => "marketingstudio.it",
)));

// Se la prima email MARKETINGSTUDIO_NOREPLY ha esaurito il numero di invii disponibili riprovo con la seconda
define("MARKETINGSTUDIO_RESERVE_NOREPLY_MAIL", json_encode(array(
    "mail" => "noreply2@marketingstudio.it",
    "mail_name" => "Marketing Studio - NoReply",
    "pass" => "jhT6yGf4s",
    "host" => "marketingstudio.it",
)));

define("FRAMEWORK_SUPPORT_EMAIL", 'assistenza@marketingstudio.it');

define("STAGING_DOMAIN", "stagingmarketingstudio.it");
define("SAAS_STAGING_DOMAIN", "saasmarketingstudio.it");

if(strstr($_SERVER['DOCUMENT_ROOT'], "luigi.marketingstudio")) {
    define("STAGING_EMAIL", "info@luigiverzi.it");
} else if(strstr($_SERVER['DOCUMENT_ROOT'], "luca.marketingstudio")) {
    define("STAGING_EMAIL", "sollamiluca@gmail.com");
} else if(strstr($_SERVER['DOCUMENT_ROOT'], "vincenzo.marketingstudio")) {
    define("STAGING_EMAIL", "info@vincenzomennella.it");
} else {
    define("STAGING_EMAIL", "staging@marketingstudio.it");
}

/* ONESIGNAL API */
define("ONESIGNAL_APP_ID", "33388adf-a7d2-48b1-9329-caf81f5499a5");
define("ONESIGNAL_APP_KEY", "YmVlYmNhMTItZmU2Zi00YmE4LTk0MjktYjQzOGQwYmNmZTdj");

/*
 *
 *
 * NON MODIFICARE OLTRE QUESTO PUNTO!
 *
 *
 */

spl_autoload_register(function($class) {
    $path_framework = FRAMEWORK_ABSOLUTE_PATH . str_replace('\\', '/', $class) . '.php';
    $path_sw = REAL_SW_PATH . "classes/" . str_replace('\\', '/', $class) . '.php';

    if(file_exists($path_framework)) {
        require_once $path_framework;
    } else if(file_exists($path_sw)) {
        require_once $path_sw;
    }
});

require(FRAMEWORK_ABSOLUTE_PATH . "../classes/composer/autoload.php"); //autoload composer


if(isset($_GET['switchCurrency'])) {
    $_SESSION['currency'] = $_GET['switchCurrency'];
}

/* INCLUSIONE CLASSI PRINCIPALI */
$MSFrameworkSimulator = new \MSFramework\Framework\simulator();
$MSFrameworkSaaSBase = new \MSFramework\SaaS\base();
$MSFrameworkDatabase = new \MSFramework\database();
$MSFrameworkCMS = new \MSFramework\cms();
$MSFrameworki18n = new \MSFramework\i18n(); // Verrà dichiarata di nuovo successivamente (con i parametri della lingua) ma la chiamo comunque qui perchè servirà alla classe URL
$MSHooks = new \MSFramework\hooks();
$MSFrameworkModules = new \MSFramework\modules();
$MSFrameworkUrl = new \MSFramework\url();
$MSFrameworkUploads = new \MSFramework\uploads();
$MSFrameworkUsers = new \MSFramework\users();
$MSFrameworkSaaSEnvironments = new \MSFramework\SaaS\environments();
$MSFrameworkModules = new \MSFramework\modules();
$MSFrameworkFW = new \MSFramework\framework();
$MSFrameworkPages = new \MSFramework\pages();
$MSFrameworkCurrencies = new \MSFramework\Fatturazione\currencies(($_SESSION['currency'] && !strstr(REAL_SW_PATH, "MSAdminControlPanel") ? $_SESSION['currency'] : ""));

// Inizializza tutti gli Hooks
$MSFrameworkModules->setModulesHooks();

// Imposto l'hander degli errori facendolo puntare al sistema di log errori
set_error_handler(array(new \MSFramework\Modules\errorTracker(), 'savePHPErrorLog'));

define("FRAMEWORK_SERVER_NAME", $MSFrameworkCMS->getVirtualServerName());
if(FRAMEWORK_SERVER_NAME == "") {
    die('Impossibile determinare la posizione del sito di destinazione');
}

define("MAIN_SITE_FOLDERPATH", realpath(dirname( __FILE__ ) . '/../../..') . "/" . (FRAMEWORK_SERVER_NAME ? FRAMEWORK_SERVER_NAME . "/" : "")); //Il path assoluto della cartella che contiene il sito del cliente

if(strstr($_SERVER['DOCUMENT_ROOT'], "/" . DEV_FOLDER_NAME . "/")) {
    define("FRAMEWORK_DEBUG_MODE", true);
    define('UPLOADS_CDN', '//' . $_SERVER['HTTP_HOST'] . "/" . FRAMEWORK_SERVER_NAME . "/" . UPLOAD_FOLDERPATH_NAME); //Questa costante punta direttamente alla cartella uploads ed è utile per recuperare il percorso esatto della cartella upload per il sito corrente!

    define('FRAMEWORK_COMMON_CDN', '//' . $_SERVER['HTTP_HOST'] . "/" . FRAMEWORK_ABSOLUTE_PATH_HTML . "common/"); //Questa costante punta direttamente alla cartella "common" del framework
    define('SITES_HOME_FOLDER', realpath(dirname( FRAMEWORK_ABSOLUTE_PATH ) . '/../../../../') . "/" . DEV_FOLDER_NAME . "/" . $_SERVER['HTTP_HOST'] . "/"); //Questa costante punta direttamente alla cartella che contiene tutti i siti in sviluppo (cartella /SVILUPPO)
    define('SERVER_HOME_FOLDER', realpath(dirname( SITES_HOME_FOLDER ) . '/../') . "/"); // La cartella marke833
} else {
    define("FRAMEWORK_DEBUG_MODE", false);
    define('UPLOADS_CDN', '//' . FRAMEWORK_SERVER_NAME . "/" . UPLOAD_FOLDERPATH_NAME); //Questa costante punta direttamente alla cartella uploads ed è utile per recuperare il percorso esatto della cartella upload per il sito corrente!

    define('FRAMEWORK_COMMON_CDN', 'https://fw' . strtolower(FRAMEWORK_VERSION) . 'cdn.marketingstudio.it/'); //Questo CDN punta alla cartella /common del framework e consente di accedere alle risorse presenti nella cartella del framework (che è esterna al dominio corrente)
    define('SITES_HOME_FOLDER', realpath(dirname( FRAMEWORK_ABSOLUTE_PATH ) . '/../../../') . "/" . PROD_FOLDER_NAME . "/"); //Questa costante punta direttamente alla cartella che contiene tutti i siti in produzione (cartella /FRAMEWORK)
    define('SERVER_HOME_FOLDER', realpath(dirname( SITES_HOME_FOLDER )) . "/"); // La cartella marke833
}

define('PROD_SITES_HOME_FOLDER', SERVER_HOME_FOLDER . PROD_FOLDER_NAME . "/"); //Questa costante punta direttamente alla cartella che contiene tutti i siti in produzione (cartella /FRAMEWORK)

/* Definisco le variabili degli uploads common */
define("PATH_TO_COMMON_STORAGE", PROD_SITES_HOME_FOLDER . "/MSCommonStorage/");
define("PATH_TO_COMMON_STORAGE_HTML", 'https://commonstoragecdn.marketingstudio.it/'); //Questo CDN punta alla cartella /MSCommonStorage fuori dal framework e consente di accedere alle risorse presenti nella cartella (che è esterna al dominio corrente)

/* Definisco le variabili delle valute */
define("CURRENCY_NAME", $MSFrameworkCurrencies->getCurrentCurrencyDetails()['name']);
define("CURRENCY_SYMBOL", $MSFrameworkCurrencies->getCurrentCurrencyDetails()['symbol']);
define("CURRENCY_CODE", $MSFrameworkCurrencies->getCurrentCurrencyDetails()['code']);

// Ottiene e formatta le info contenute nella tabella customers del database FRAMEWORK
if($MSFrameworkSaaSBase->isSaaSDomain()) {
    define("CUSTOMER_DOMAIN_INFO", $MSFrameworkFW->getWebsitePathsBy('id', $MSFrameworkSaaSEnvironments->getCurrentEnvironmentID(), true));
} else {
    define("CUSTOMER_DOMAIN_INFO", $MSFrameworkFW->getWebsitePathsBy());
}

/* INIZIALIZZAZIONE CLASSI PARSER */
$MSFrameworkVisualBuilder = new \MSFramework\Frontend\visualBuilder();
$MSFrameworkParser = new \MSFramework\Frontend\parser();

/* AGGIUNGE IL CORS HEADER PER PERMETTERE CHIAMATE AJAX DAL BACK AL FRONT E VICEVERSA */
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Content-Type, x-fw360-environment");

if(FRAMEWORK_DEBUG_MODE){
    $firephp = ChromePhp::getInstance();
} else {
    class fakelogger{
        public function log(){

        }

        public function error(){

        }

        public function info(){

        }
    }

    $firephp = new fakelogger();
}


/*
 *
 *
 * queste costanti verranno utilizzate all'interno dei SW (pannello di amministrazione e siti)
 *
 *
 */

define("ABSOLUTE_SW_PATH", REAL_SW_PATH);
define("ABSOLUTE_SW_PATH_HTML", str_replace($_SERVER['DOCUMENT_ROOT'], "", ABSOLUTE_SW_PATH));

define("ABSOLUTE_ADMIN_MODULES_PATH", dirname(__FILE__, 2) . "/modules/");
define("ABSOLUTE_ADMIN_MODULES_PATH_HTML", str_replace($_SERVER['DOCUMENT_ROOT'], "", ABSOLUTE_ADMIN_MODULES_PATH));

define("PRODUCER_NAME", "Marketing Studio");
define("PRODUCER_EMAIL", "info@marketingstudio.it");

/*dati personalizzati in base al dominio, ma che comunque sono funzionalità comuni tra tutti i domini*/
define("NOREPLY_MAIL", json_encode(array( //i dati per l'accesso alla casella di posta "noreply" dalla quale saranno inviate le mail del sito (configurazione per utente)
    "mail" => $MSFrameworkCMS->getCMSData("producer_config")['noreplymail_address'],
    "mail_name" => $MSFrameworkCMS->getCMSData("producer_config")['noreplymail_name'],
    "pass" => $MSFrameworkCMS->getCMSData("producer_config")['noreplymail_pass'],
    "host" => $MSFrameworkCMS->getCMSData("producer_config")['noreplymail_host'],
)));

$MSFrameworkUploads->initConstants();

/* SISTEMA GLI URL (AGGIUNGE/RIMUOVE WWW | SUPPORTO HTTPS) */
$parsedUrl = parse_url($GLOBALS['CANONICAL_URL']);
$host = explode('.', $parsedUrl['host']);

if(count($host) == 2 && $host[0] != 'www') {
    header('Location: ' . (SSL_ENABLED ? "https" : "http") . "://www." .$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    exit;
}
else if (count($host) > 3 && $host[0] == 'www'){
    header('Location: ' . (SSL_ENABLED ? "https" : "http") . "://" . str_replace('www.', '', $_SERVER['HTTP_HOST']) . $_SERVER['REQUEST_URI']);
    exit;
}

if( (SSL_ENABLED) && $parsedUrl["scheme"] == 'http') {
    header('Location: ' . "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
}

// Nel caso in cui stiamo effettuando una chiamata ajax (quindi senza prefisso di lingua) imposto la lingua salvata nella sessione invece di dichiararla nuovamente
if(!isset($_SESSION['site_set_lang']) || empty($_SERVER['QUERY_STRING'])) {
    $_SESSION['site_set_lang'] = $MSFrameworki18n->getPrimaryLangId();
}

//I redirect impostati nel modulo hanno priorità su qualunque altro controllo. Verifico subito se è necessario reindirizzare l'utente su un'altra pagina
if($_GET['slug'] != "") {
    $parts = parse_url($_SERVER['REQUEST_URI']);
    parse_str($parts['query'], $query);

    $string = trim($_GET['slug'], '/');

    if($parts['query'] != "") {
        $string .= "?" . $parts['query'];
    }

    $r_redirect = $MSFrameworkDatabase->getAssoc("SELECT url_to, redirect_type FROM redirect WHERE TRIM(BOTH '/' FROM url_from) = :slug", array(":slug" => $string), true);
    if($r_redirect['url_to'] != "") {
        $url_to = $r_redirect['url_to'];

        if(!substr($url_to, 0, 4) != "http") {
            if(FRAMEWORK_DEBUG_MODE && substr($url_to, 0, 1) == "/") {
                $url_to = "/" . FRAMEWORK_SERVER_NAME . $url_to;
            }
        }

        header('Location: ' . $url_to, true, $r_redirect['redirect_type']);
        die();
    }
}

define("SW_NAME", $MSFrameworki18n->getFieldValue($MSFrameworkCMS->getCMSData("site")['nome']));

/* FUNZIONE PER IL REWRITE DEGLI SHORT URL */
if(strstr(REAL_SW_PATH, "MSAdminControlPanel")) {
    //in qualche caso, anche se siamo nell'ACP, la selezione della lingua non è stata ancora effettuata (es. nella pagina registrazione). La inizializzo con quella principale.
    if($_SESSION['admin_set_lang'] == "") {
        $_SESSION['admin_set_lang'] = $MSFrameworki18n->getPrimaryLangId();
    }

    define("USING_LANGUAGE_CODE", $_SESSION['admin_set_lang']);
    $MSFrameworki18n = new \MSFramework\i18n(USING_LANGUAGE_CODE, true);
} else {
    // AGGIUNTO PER LA RETROCOMPATIBILITA'
    if(isset($_GET['lang'])) {
        if($_GET['lang'] != "use_default" && $_GET['lang'] != "") {
            //se la lingua impostata non esiste, non è attiva o corrisponde a quella principale, redirecto la richiesta alla versione / (quella della lingua di default) come ultimo tentativo prima di restituire un 404
            if(!$MSFrameworki18n->checkLangIsActiveByFilter($_GET['lang'], "url_code") || (new \MSFramework\i18n())->checkLangIsPrimaryByFilter($_GET['lang'], "url_code")) {
                $protocol = SSL_ENABLED ? "https://" : "http://";
                header('HTTP/1.1 404 Not Found');
                header('location: ' . $protocol . $_SERVER['HTTP_HOST'] . ABSOLUTE_SW_PATH_HTML);
            } else {
                $_SESSION['site_set_lang'] = $MSFrameworki18n->getLangIDByFilter($_GET['lang'], "url_code");
            }
        }
    }
    else if(isset($_GET['slug']))
    {
        $exploded_url = explode('/', rtrim($_GET['slug'], '/'));

        // Se il primo parametro è la lingua allora la imposto
        if ($exploded_url && $MSFrameworki18n->checkLangIsActiveByFilter($exploded_url[0], "url_code")) {
            $_SESSION['site_set_lang'] = $MSFrameworki18n->getLangIDByFilter($exploded_url[0], "url_code");
            $lang_in_slug = true;
        }
    }

    define("USING_LANGUAGE_CODE", $_SESSION['site_set_lang']);
    $MSFrameworki18n = new \MSFramework\i18n(USING_LANGUAGE_CODE, true);

    if (isset($_GET['check_dynamic_url'])) {

        $slug = rtrim($_GET['slug'], '/');
        $exploded_url = explode('/', $slug);

        if ($exploded_url) {

            if(isset($lang_in_slug)) {
                unset($exploded_url[0]);
                $exploded_url = array_values($exploded_url);
            }

            if ($exploded_url[0] == 'robots.txt')
            {   // Mostro il robots.php
                $robots_txt = "";
                $original_robots = $MSFrameworkCMS->getCMSData('site')['robots_txt'];

                if($MSFrameworkCMS->isStaging()) { //solo per lo staging, aggiungo un Disallow / per tutti gli User-agents
                    $robots_txt .= "User-agent: * \r\n";
                    $robots_txt .= "Disallow: / \r\n";

                    $original_robots = str_replace(array("Allow: /", "User-agent: *"), array(), $original_robots);
                }

                $robots_txt .= $original_robots;

                header("Content-Type: text/plain");
                die($robots_txt);
            }
            else if ($exploded_url[0] == (new \MSFramework\cronjob())->getShortPrefix() && count($exploded_url) == 3)
            {
                $cron_path = FRAMEWORK_ABSOLUTE_PATH . str_replace('.', '', '/cron/' . (FRAMEWORK_DEBUG_MODE ? 'Sviluppo' : 'Produzione') . '/' . $exploded_url[1] . '/' . $exploded_url[2]) . '.php';
                if(file_exists($cron_path)) {
                    ini_set('max_execution_time', 1*60);
                    set_time_limit(1*60);
                    include($cron_path);
                }
                die();
            }
            else if ($exploded_url[0] == 'gestionaleRe.xml' && $MSFrameworkCMS->checkExtraFunctionsStatus('realestate'))
            {
                include(FRAMEWORK_ABSOLUTE_PATH . '/common/modules/multiportal/xmlGestionaleRe.php');
                die();
            }
            else if ($exploded_url[0] == $MSFrameworkUrl->getShortPrefix() && strlen($exploded_url[1]) == 6)
            {   // è uno short URL, effettuo il redirect
                $MSFrameworkUrl->redirectFromShort($exploded_url[1]);
                die();
            }
            else if (count($exploded_url) == 4 && $exploded_url[0] == (new \MSFramework\emails())->getACPBridgeShort() && $exploded_url[1] == (new \MSFramework\emails())->getACPBridgeToken())
            {
                // Invio le email ecommerce passate dall'ACP al front-end
                $order_id = $exploded_url[3];
                if ($exploded_url[2] == "send_order_status_email") {
                    (new \MSFramework\Ecommerce\orders())->sendOrderStatusEmail($order_id, 1);
                }
                else if ($exploded_url[2] == "send_subscription_status_email") {
                    (new \MSFramework\Subscriptions\orders())->sendOrderStatusEmail($order_id, 1);
                }
                else if ($exploded_url[2] == "send_travelagency_status_email") {
                    (new \MSFramework\TravelAgency\orders())->sendOrderStatusEmail($order_id, 1);
                }
                die('1');
            }
            else if ($exploded_url[0] == $MSFrameworkModules->getShortPrefix())
            {   //è uno short URL, richiamo le funzioni ajax dei moduli
                $module_info = $MSFrameworkModules->getModules($exploded_url[1]);
                $MSFrameworkUrl->current_url = rtrim($MSFrameworkCMS->getCleanHost($MSFrameworkCMS->getCleanHost($_SERVER['HTTP_REFERER'])), '/');

                if ($module_info) {
                    $class = $module_info['path'];
                    if (class_exists($class)) {
                        $module = new $class();

                        // Se la chiamata arriva da un cliente SaaS allora mi connetto al suo database prima di eseguire le azioni
                        if($MSFrameworkSaaSBase->isSaaSDomain()) {
                            if(apache_request_headers()['X-Fw360-Environment']) {
                                $MSFrameworkDatabase->exec('USE marke833__SaaS__id_' . $MSFrameworkSaaSBase->getID() . '__env_' . apache_request_headers()['X-Fw360-Environment']);
                            }
                        }

                        if(count($exploded_url) > 1) {
                            echo $module->ajaxCall($_POST, array_slice($exploded_url, 2));
                        } else {
                            echo $module->ajaxCall($_POST);
                        }
                        die();
                    }
                }
            }
            else if ($exploded_url[0] == (new \MSFramework\Ticket\ticket())->getCheckPagePrefix() && isset($exploded_url[1]))
            {
                $ticket_id = $exploded_url[1];
                include(FRAMEWORK_ABSOLUTE_PATH . 'common/modules/ticket/php/check.php');
                die();
            }
            else if(in_array($exploded_url[0], array_keys($MSFrameworkUrl->getParamsLocation(false)))) {
                $redirect_url = $MSFrameworkUrl->getParamsLocation(true)[$exploded_url[0]];

                foreach ($exploded_url as $k => $val) {
                    if($k > 0) {
                        $redirect_url = str_replace(
                            '$' . $k,
                            $val,
                            $redirect_url
                        );
                    }
                }

                header("Location: " . ABSOLUTE_SW_PATH_HTML . $redirect_url);
                die();

            } else {

                // Nel caso in cui la lingua sia diversa da quella precedente impostata
                // ricarico la pagina per far ricaricare le variabili di lingua corrette
                // (Necessario per poter mantenere la variabile sessione della lingua anche nei link short qui sopra)

                $currentSlugDetails = $MSFrameworkUrl->getDetailsFromSlug($exploded_url);
                $currentSlugArray = $exploded_url;
                if(!isset($lang_in_slug) && ( ($currentSlugDetails && !$exploded_url) || (!$currentSlugDetails && $exploded_url)) && $_SESSION['site_set_lang'] != $MSFrameworki18n->getPrimaryLangId()) {
                    $_SESSION['site_set_lang'] = $MSFrameworki18n->getPrimaryLangId();
                    header("Refresh:0");
                    die();
                }

                $_GET['slug'] = $exploded_url;
            }
        }
    }
}

if(!$MSFrameworkCMS->isOnline() && !defined("DO_NOT_CHECK_OFFLINE")) {
    die('Il sito è momentaneamente offline');
}

// Salvo il protocollo attuale nel database per evitare future richieste
$MSFrameworkCMS->setCMSData('server_protocols', array(
    (strstr(REAL_SW_PATH, "MSAdminControlPanel") ? 'back' : 'front') => (SSL_ENABLED ? 'https://' : 'http://')
), true);

<?php
    require_once('../../../../../sw-config.php');
    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

    foreach($dbs as $dbname) {
        unset($MSFrameworkDatabase);
        $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query("ALTER TABLE `ecommerce_products` ADD `ean` VARCHAR(13) NOT NULL"));
    }

    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
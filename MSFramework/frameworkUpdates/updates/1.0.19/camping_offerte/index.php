<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `camping_offerte` (
              `id` int(10) NOT NULL,
              `nome` longtext NOT NULL,
              `type` varchar(20) NOT NULL,
              `notti` varchar(3) NOT NULL,
              `prezzo` varchar(20) NOT NULL,
              `scadenza` varchar(10) NOT NULL,
              `short_content` longtext NOT NULL,
              `content` longtext NOT NULL,
              `gallery` longtext NOT NULL,
              `slug` longtext NOT NULL,
              `auto_renew_promo` int(5) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 30/12/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `blog_stats` (
              `id` int(11) NOT NULL,
              `ip` mediumtext NOT NULL,
              `article_id` int(11) NOT NULL,
              `day` date NOT NULL,
              `hour` int(11) NOT NULL,
              `minute` int(11) NOT NULL,
              `tempo_medio` int(11) NOT NULL,
              `n_visite` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `blog_stats` ADD PRIMARY KEY (`id`);"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `blog_stats` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `blog_posts` ADD `visite` INT NOT NULL AFTER `author`;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>



<?php
/**
 * MSAdminControlPanel
 * Date: 10/12/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `notifications` (
             `id` int(11) NOT NULL AUTO_INCREMENT,
             `type` varchar(50) NOT NULL,
             `email_settings` longtext NOT NULL,
             `sms_settings` longtext NOT NULL,
             `is_active` varchar(1) NOT NULL,
             PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>





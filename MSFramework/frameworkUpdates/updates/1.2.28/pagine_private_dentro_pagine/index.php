<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `pagine_private` ADD `last_update` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `settings`;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `pagine_private` CHANGE `data` `content` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `pagine_private` ADD `type` TEXT NOT NULL AFTER `slug`;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `pagine_private` CHANGE `settings` `additional_css` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;")
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
<?php
/**
 * MSAdminControlPanel
 * Date: 30/12/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `customers` ADD `registration_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `dati_fatturazione`;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
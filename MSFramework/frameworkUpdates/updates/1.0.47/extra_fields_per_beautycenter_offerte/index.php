<?php
/**
 * MSAdminControlPanel
 * Date: 14/11/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `beautycenter_offerte` ADD `extra_fields` LONGTEXT NOT NULL AFTER `auto_renew_promo`;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>



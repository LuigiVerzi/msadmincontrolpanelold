<?php
/**
 * MSAdminControlPanel
 * Date: 10/09/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    foreach($MSFrameworkDatabase->getAssoc("SELECT id, descrizione FROM `realestate_immobili`") as $r) {
        $got_ary = json_decode($r['descrizione'], true);

        if(!is_array($got_ary)) {
            $got_ary['it_IT'] = $r['descrizione'];
            $got_errors[] = $updater->checkQueryError(
                $MSFrameworkDatabase->query("UPDATE `realestate_immobili` SET descrizione = :descr WHERE id = :id", array(':descr' => json_encode($got_ary), ':id' => $r['id']))
            );
        }
    }

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

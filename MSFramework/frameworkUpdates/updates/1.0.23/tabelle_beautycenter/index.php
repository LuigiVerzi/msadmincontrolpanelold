<?php
/**
 * MSAdminControlPanel
 * Date: 14/11/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `beautycenter_reparti` (
            `id` int(11) NOT NULL,
            `nome` longtext NOT NULL,
            `descr` longtext NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `beautycenter_servizi` (
            `id` int(10) NOT NULL,
            `nome` longtext NOT NULL,
            `descr` longtext NOT NULL,
            `long_descr` longtext NOT NULL,
            `reparto` int(11) NOT NULL,
            `prezzo` varchar(50) NOT NULL,
            `images` longtext NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>



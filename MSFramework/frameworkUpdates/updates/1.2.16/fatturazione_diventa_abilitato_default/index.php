<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    if(!$MSFrameworkDatabase->getCount("SELECT * FROM information_schema.tables WHERE table_schema = '$dbname' AND table_name = 'fatturazione_vendite'")) {
        foreach ($MSFrameworkDatabase->getFromDummyByPrefix("fatturazione_") as $table) {
           $MSFrameworkDatabase->query("CREATE TABLE `" . $table['TABLE_NAME'] . "` LIKE `" . DUMMY_DB_NAME . "`.`" . $table['TABLE_NAME'] . "`");
        }
    }
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
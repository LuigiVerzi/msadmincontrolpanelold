<?php
/**
 * MSAdminControlPanel
 * Date: 14/11/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `fatturazione_casse` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `nome` text NOT NULL,
              `descrizione` text NOT NULL,
              `dispositivo` text NOT NULL,
              `logo_scontrino` text NOT NULL,
              `messaggio_promozionale` text NOT NULL,
              `dati_connessione` longtext NOT NULL,
              `is_active` int(11) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>



<?php
/**
 * MSAdminControlPanel
 * Date: 2019-03-13
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $attribute_brand_id = 0;

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `ecommerce_brand`") as $brand) {

        $attribute_brand_id = $brand['attribute'];

        // Sposta la referenza delle immagini nel DB
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query(
                "UPDATE ecommerce_attributes_values SET images = :images WHERE id = :id;",
                array(':images' => $brand['logo'], ':id' => $brand['attribute_value'])
            )
        );

        // Sposta fisicamente le foto nella cartella Attributi invece che Brand
        if(json_decode($brand['logo'])) {

            $foto_main = UPLOAD_ECOMMERCE_BRAND_FOR_DOMAIN . json_decode($brand['logo'], true)[0];
            $foto_tn = UPLOAD_ECOMMERCE_BRAND_FOR_DOMAIN . 'tn/' . json_decode($brand['logo'], true)[0];

            $dest_main = str_replace(UPLOAD_ECOMMERCE_BRAND_FOR_DOMAIN, UPLOAD_ECOMMERCE_ATTRIBUTES_FOR_DOMAIN, $foto_main);
            $dest_tn = str_replace(UPLOAD_ECOMMERCE_BRAND_FOR_DOMAIN, UPLOAD_ECOMMERCE_ATTRIBUTES_FOR_DOMAIN, $foto_tn);

            rename($foto_main, $dest_main);
            rename($foto_tn, $dest_tn);

        }

    };

    // Imposta la visualizzazione 'FOTO'
    if($attribute_brand_id > 0) {
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query(
                "UPDATE ecommerce_attributes SET show_mode = :show_mode WHERE id = :id;",
                array(':show_mode' => 'brand', ':id' => $attribute_brand_id)
            )
        );
    }
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 10/09/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    /* Tabella punteggio punti */
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("CREATE TABLE `points_histories` ( `id` INT NOT NULL AUTO_INCREMENT , `cliente` INT NOT NULL , `valore` FLOAT NOT NULL , `note` MEDIUMTEXT NOT NULL , `data` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;")
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

$envolved_tables = array("servizi_categorie", "servizi");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    foreach($envolved_tables as $table) {
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query(
                "ALTER TABLE `$table` ADD `slug` LONGTEXT NOT NULL"
            )
        );

        foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome FROM `$table` WHERE slug = ''") as $record) {
            $nomi = json_decode($record['nome'], true);
            $slugs = array();

            foreach($nomi as $lingua => $nome) {
                $slug = $MSFrameworkUrl->cleanString($nome);
                if(!$MSFrameworkUrl->checkSlugAvailability($slug, $table, $record['id'])) {
                    $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
                }

                $slugs[$lingua] = $slug;

                $got_errors[] = $updater->checkQueryError(
                    $MSFrameworkDatabase->pushToDB(
                        "UPDATE `$table` SET slug = :slug WHERE id = :id", array(":slug" => json_encode($slugs), ":id" => $record['id'])
                    )
                );
            }
        }
    }
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>


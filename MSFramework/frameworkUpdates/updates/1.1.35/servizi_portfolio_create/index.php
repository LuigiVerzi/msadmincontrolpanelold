<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("
            CREATE TABLE `servizi_portfolio` (
                `id` int(10) NOT NULL AUTO_INCREMENT,
                `nome` longtext NOT NULL,
                `descr` longtext NOT NULL,
                `long_descr` longtext NOT NULL,
                `images` longtext NOT NULL,
                `slug` longtext NOT NULL,
                `url` varchar(50) NOT NULL,
                `service` int(11) NOT NULL,
                `customer` int(11) NOT NULL,
                `is_active` int(1) NOT NULL DEFAULT '1',
                `is_highlighted` int(1) NOT NULL DEFAULT '0',
                `web_images` longtext NOT NULL,
                `mob_images` longtext NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ")
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("ALTER TABLE `servizi` ADD `descr_title` LONGTEXT NOT NULL")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("ALTER TABLE `servizi` ADD `banner` LONGTEXT NOT NULL")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("ALTER TABLE `servizi` ADD `sector` LONGTEXT NOT NULL")
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
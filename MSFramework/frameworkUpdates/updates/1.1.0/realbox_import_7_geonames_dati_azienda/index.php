<?php
/**
 * MSAdminControlPanel
 * Date: 20/03/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

$MSFrameworkGeonames = (new \MSFramework\geonames());

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $regione = "";

    $r_cms = $MSFrameworkDatabase->getAssoc("SELECT value FROM cms WHERE type = 'site'", array(), true);
    $r = json_decode($r_cms['value'], true);

    $idComune = $MSFrameworkGeonames->searchForComuneID($r['city']);
    if($idComune != "") {
        $dettagli_comune = $MSFrameworkGeonames->getDettagliComune($idComune, 'latitude,longitude,admin1');
        $regione = $MSFrameworkGeonames->searchForRegioneID('IT.' . $dettagli_comune['admin1'], 'code');
    }

    $r['geo_data'] = json_encode(array(
        "regione" => $regione,
        "provincia" => $MSFrameworkGeonames->searchForProvinciaID($r['provincia']),
        "comune" => $idComune,
        "indirizzo" => $r['address'],
        "cap" => $r['cap'],
        "lat" => $dettagli_comune['latitude'],
        "lng" => $dettagli_comune['longitude'],
    ));
    unset($r['address']);
    unset($r['cap']);
    unset($r['city']);
    unset($r['provincia']);


    $array_to_save = array(
        "value" => json_encode($r),
        "type" => "site"
    );

    $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'update');

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'site'", $stringForDB[0])
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

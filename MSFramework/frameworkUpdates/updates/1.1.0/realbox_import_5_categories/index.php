<?php
/**
 * MSAdminControlPanel
 * Date: 20/03/2019
 */
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

$array_main = array(
    "id_0" => "Residenziale",
    "id_1" => "Immobili di Lusso",
    "id_2" => "Immobili all'asta",
    "id_3" => "Commerciale",
    "id_4" => "Nuove Costruzioni",
    "id_5" => "Terreni"
);

$gestionalereTypes = array(
    "id_0" => array(
        "id_0" => 3,
        "id_1" => 7,
        "id_2" => 8,
        "id_3" => 9,
        "id_4" => 10,
        "id_5" => 11,
        "id_6" => 12,
        "id_7" => 15,
        "id_8" => 16,
        "id_9" => 9
    ),
    "id_1" => array(
        "id_0" => 3,
        "id_1" => 7,
        "id_2" => 8,
        "id_3" => 9,
        "id_4" => 10,
        "id_5" => 11,
        "id_6" => 12,
        "id_7" => 15,
        "id_8" => 16,
        "id_9" => 9
    ),
    "id_2" => array(
        "id_0" => 3,
        "id_1" => 7,
        "id_2" => 8,
        "id_3" => 9,
        "id_4" => 10,
        "id_5" => 11,
        "id_6" => 12,
        "id_7" => 15,
        "id_8" => 16,
        "id_9" => 9
    ),
    "id_3" => array(
        "id_0" => 6,
        "id_1" => 30,
        "id_2" => 6,
        "id_3" => 6,
        "id_4" => 27,
        "id_5" => 28,
        "id_6" => 31,
        "id_7" => 6,
        "id_8" => 6,
        "id_9" => 26,
        "id_10" => 6,
        "id_11" => 6,
        "id_12" => 6,
        "id_13" => 6,
        "id_14" => 6,
        "id_15" => 25,
        "id_16" => 6,
        "id_17" => 26,
        "id_18" => 6,
        "id_19" => 6,
        "id_20" => 6,
        "id_21" => 26,
        "id_22" => 28,
        "id_23" => 28,
        "id_24" => 28,
        "id_25" => 28,
        "id_26" => 28,
        "id_27" => 26,
        "id_28" => 6,
        "id_29" => 6,
        "id_30" => 6,
        "id_33" => 29
    ),
    "id_4" => array(
        "id_0" => 3,
        "id_1" => 7,
        "id_2" => 8,
        "id_3" => 23,
        "id_4" => 10,
        "id_5" => 17,
        "id_6" => 6,
        "id_7" => 29,
        "id_8" => 15
    ),
    "id_5" => array(
        "id_0" => 14,
        "id_1" => 13
    )
);

$sub = array(
    "id_0" => array(
        "id_0" => "Appartamento",
        "id_1" => "Attico/Mansarda",
        "id_2" => "Box Garage",
        "id_3" => "Casa Indipendente",
        "id_4" => "Loft/Open Space",
        "id_5" => "Palazzo/Stabile",
        "id_6" => "Rustico/Casale",
        "id_7" => "Villa",
        "id_8" => "Villetta a schiera",
        "id_9" => "Terracielo o Terratetto",
        "id_10" => "Pertinenza",
        "id_11" => "Altro"
    ),
    "id_1" => array(
        "id_0" => "Appartamento",
        "id_1" => "Attico/Mansarda",
        "id_2" => "Box Garage",
        "id_3" => "Casa Indipendente",
        "id_4" => "Loft/Open Space",
        "id_5" => "Palazzo/Stabile",
        "id_6" => "Rustico/Casale",
        "id_7" => "Villa",
        "id_8" => "Villetta a schiera",
        "id_9" => "Terracielo o Terratetto",
        "id_10" => "Pertinenza",
        "id_11" => "Altro"
    ),
    "id_2" => array(
        "id_0" => "Appartamento",
        "id_1" => "Attico/Mansarda",
        "id_2" => "Box Garage",
        "id_3" => "Casa Indipendente",
        "id_4" => "Loft/Open Space",
        "id_5" => "Palazzo/Stabile",
        "id_6" => "Rustico/Casale",
        "id_7" => "Villa",
        "id_8" => "Villetta a schiera",
        "id_9" => "Terracielo o Terratetto",
        "id_10" => "Pertinenza",
        "id_11" => "Altro"
    ),
    "id_3" => array(
        "id_0" => "Abbigliamento",
        "id_1" => "Agriturismo",
        "id_2" => "Alimentari",
        "id_3" => "Auto officina",
        "id_4" => "Azienda agricola",
        "id_5" => "Bar",
        "id_6" => "Bed & Breakfast",
        "id_7" => "Cartoleria/Libreria",
        "id_8" => "Centro commerciale",
        "id_9" => "Discoteca",
        "id_10" => "Edicola",
        "id_11" => "Estetica/Solarium",
        "id_12" => "Ferramenta/Casalinghi",
        "id_13" => "Gelateria",
        "id_14" => "Giochi/Scommesse",
        "id_15" => "Hotel",
        "id_16" => "Negozio",
        "id_17" => "Palestra",
        "id_18" => "Panetteria",
        "id_19" => "Parrucchiere uomo/donna",
        "id_20" => "Pasticceria",
        "id_21" => "Pensione",
        "id_22" => "Pizza al taglio",
        "id_23" => "Pizzeria",
        "id_24" => "Pub",
        "id_25" => "Ristorante",
        "id_26" => "Rosticceria",
        "id_27" => "Tabaccheria",
        "id_28" => "Telefonia/Informatica",
        "id_29" => "Tintoria/Lavanderia",
        "id_30" => "Videonoleggio",
        "id_31" => "Altro (Alimentare)",
        "id_32" => "Altro (Non alimentare)",
        "id_33" => "Ufficio",
    ),
    "id_4" => array(
        "id_0" => "Appartamento",
        "id_1" => "Attico/Mansarda",
        "id_2" => "Box Auto",
        "id_3" => "Capannone",
        "id_4" => "Loft",
        "id_5" => "Magazzino",
        "id_6" => "Negozio",
        "id_7" => "Ufficio",
        "id_8" => "Villa/Villetta"
    ),
    "id_5" => array(
        "id_0" => "Terreno agricolo",
        "id_1" => "Terreno Edificabile"
    )
);

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);
    $ary_order = array();
    $ary_cat_assoc = array();

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM realestate_categories"
        )
    );

    foreach($array_main as $mainCatK => $mainCat) {
        asort($sub[$mainCatK]);
        $workingOn = $sub[$mainCatK];

        $slug = $MSFrameworkUrl->cleanString($mainCat);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "realestate_categories", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO realestate_categories (nome, slug, macro) VALUES (:nome, :slug, :macro)", array(":nome" => json_encode(array("it_IT" => $mainCat)), ":slug" => json_encode(array("it_IT" => $slug)), ":macro" => str_replace("id_", "", $mainCatK))
            )
        );

        $ary_order[str_replace("id_", "", $mainCatK)] = array("id" => $MSFrameworkDatabase->lastInsertId());

        foreach($workingOn as $subCatK => $subCat) {
            $slug = $MSFrameworkUrl->cleanString($subCat);
            if(!$MSFrameworkUrl->checkSlugAvailability($slug, "realestate_categories", '0')) {
                $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
            }

            $idGestionaleRe = $gestionalereTypes[$mainCatK][$subCatK];

            $got_errors[] = $updater->checkQueryError(
                $MSFrameworkDatabase->pushToDB(
                    "INSERT INTO realestate_categories (nome, slug, macro, multiportal_links) VALUES (:nome, :slug, :macro, :multiportal_links)", array(":nome" => json_encode(array("it_IT" => $subCat)), ":slug" => json_encode(array("it_IT" => $slug)), ":macro" => str_replace("id_", "", $mainCatK), ":multiportal_links" => json_encode(array("gestionalere" => $idGestionaleRe)))
                )
            );

            $ary_order[str_replace("id_", "", $mainCatK)]['children'][] = array("id" => $MSFrameworkDatabase->lastInsertId());
            $ary_cat_assoc[str_replace("id_", "", $mainCatK)][str_replace("id_", "", $subCatK)] = $MSFrameworkDatabase->lastInsertId();
        }
    }

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM cms WHERE type = 'realestate_cats_order'"
        )
    );

    $array_to_save = array(
        "value" => json_encode(array(
            "list" => $ary_order
        )),
        "type" => "realestate_cats_order"
    );

    $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');

    $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);


    //creo una sede fittizia
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM negozio_sedi"
        )
    );

    $nome_sede = 'Agenzia di Default';
    $slug = $MSFrameworkUrl->cleanString($nome_sede);
    if(!$MSFrameworkUrl->checkSlugAvailability($slug, "negozio_sedi", '0')) {
        $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
    }
    $MSFrameworkDatabase->pushToDB("INSERT INTO negozio_sedi (nome, slug) VALUES (:nome, :slug)", array(":nome" => json_encode(array("it_IT" => $nome_sede)), ":slug" => json_encode(array("it_IT" => $slug))));
    $id_sede = $MSFrameworkDatabase->lastInsertId();

    //importo gli immobili
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM realestate_immobili"
        )
    );

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_remaxfamosa.immobili") as $immobile) {
        $slug = $MSFrameworkUrl->cleanString($immobile['titolo']);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "realestate_immobili", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $ary_geonames = json_encode(array(
            "regione" => $immobile['regione'],
            "provincia" => $immobile['provincia'],
            "comune" => $immobile['comune'],
            "indirizzo" => $immobile['indirizzo'],
            "street_number" => $immobile['street_number'],
            "lat" => $immobile['lat'],
            "lng" => $immobile['lng'],
            "cap" => $immobile['cap']
        ));

        $immobile['category'] = $ary_cat_assoc[$immobile['tipo_immobile']][$immobile['categoria']];
        $immobile['agency'] = $id_sede;
        $immobile['geo_data'] = $ary_geonames;
        $immobile['slug'] = json_encode(array("it_IT" => $slug));
        $immobile['titolo'] = json_encode(array("it_IT" => $immobile['titolo']));
        unset($immobile['tipo_immobile']);
        unset($immobile['categoria']);
        unset($immobile['regione']);
        unset($immobile['provincia']);
        unset($immobile['comune']);
        unset($immobile['indirizzo']);
        unset($immobile['street_number']);
        unset($immobile['lat']);
        unset($immobile['lng']);
        unset($immobile['cap']);
        $stringForDB = $MSFrameworkDatabase->createStringForDB($immobile, 'insert');
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB("INSERT INTO realestate_immobili ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0])
        );
    }

    //importo le ricerche
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM realestate_ricerche"
        )
    );

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_remaxfamosa.ricerche") as $ricerca) {
        $ricerca['category'] = $ary_cat_assoc[$ricerca['tipo_immobile']][$ricerca['categoria']];

        unset($ricerca['tipo_immobile']);
        unset($ricerca['categoria']);

        $stringForDB = $MSFrameworkDatabase->createStringForDB($ricerca, 'insert');
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB("INSERT INTO realestate_ricerche ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0])
        );
    }

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("UPDATE users SET agency = :agency", array(":agency" => $id_sede))
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

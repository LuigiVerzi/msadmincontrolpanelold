<?php
/**
 * MSAdminControlPanel
 * Date: 20/03/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM users"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM users_roles"
        )
    );

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_remaxfamosa.users") as $user) {
        $slug = $MSFrameworkUrl->cleanString($user['nome'] . " " . $user['cognome']);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "users", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $user['slug'] = $slug;
        $user['ruolo'] = 'realestate-' . $user['ruolo'];
        $user['extra_data'] = json_encode(array("cons_rif" => $user['cons_rif']));
        unset($user['cons_rif']);

        $stringForDB = $MSFrameworkDatabase->createStringForDB($user, 'insert');
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB("INSERT INTO users ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0])
        );
    }

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

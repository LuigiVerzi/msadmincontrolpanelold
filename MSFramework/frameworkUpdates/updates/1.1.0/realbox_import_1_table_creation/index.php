<?php
/**
 * MSAdminControlPanel
 * Date: 20/03/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `realestate_categories` (
                 `id` int(10) NOT NULL AUTO_INCREMENT,
                 `nome` longtext NOT NULL,
                 `slug` longtext NOT NULL,
                 `descr` longtext NOT NULL,
                 `images` longtext NOT NULL,
                 `banner` longtext NOT NULL,
                 `menu_colors` longtext NOT NULL,
                 `multiportal_links` longtext NOT NULL,
                 `macro` varchar(2) NOT NULL,
                 PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `realestate_ricerche` (
                 `id` int(10) NOT NULL AUTO_INCREMENT,
                 `keywords` longtext NOT NULL,
                 `riferimento` varchar(50) NOT NULL,
                 `tipo_vendita` varchar(5) NOT NULL,
                 `category` longtext NOT NULL,
                 `comune` varchar(50) NOT NULL,
                 `price_from` double NOT NULL,
                 `price_to` double NOT NULL,
                 `mq_from` double NOT NULL,
                 `mq_to` double NOT NULL,
                 `oth_filters` longtext NOT NULL,
                 `results` int(10) NOT NULL,
                 `user_ip` longtext NOT NULL,
                 `creation_time` longtext NOT NULL,
                 PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `realestate_immobili` (
                 `id` int(10) NOT NULL AUTO_INCREMENT,
                 `category` longtext NOT NULL,
                 `tipo_vendita` varchar(5) NOT NULL,
                 `titolo` longtext NOT NULL,
                 `riferimento` varchar(50) NOT NULL,
                 `prezzo` varchar(20) NOT NULL,
                 `stima_prezzo` varchar(20) NOT NULL,
                 `trattativa_riservata` int(5) NOT NULL,
                 `superficie` double NOT NULL,
                 `anno_costruzione` varchar(5) NOT NULL,
                 `caratteristiche` longtext NOT NULL,
                 `descrizione` longtext NOT NULL,
                 `descrizione_eng` longtext NOT NULL,
                 `servizi` longtext NOT NULL,
                 `dati_mutuo` longtext NOT NULL,
                 `images` longtext NOT NULL,
                 `images_plan` longtext NOT NULL,
                 `youtube_url` longtext NOT NULL,
                 `owner_id` int(5) NOT NULL,
                 `in_evidenza` int(2) NOT NULL,
                 `landing_page` int(1) NOT NULL,
                 `stato_vendita` int(2) NOT NULL,
                 `creation_time` longtext NOT NULL,
                 `last_update_time` longtext NOT NULL,
                 `visite` int(50) NOT NULL,
                 `contatti` int(50) NOT NULL,
                 `slug` longtext NOT NULL,
                 `agency` int(11) NOT NULL,
                 PRIMARY KEY (`id`),
                 FULLTEXT KEY `descr_ft` (`descrizione`,`titolo`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

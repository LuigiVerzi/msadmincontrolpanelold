<?php
/**
 * MSAdminControlPanel
 * Date: 20/03/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    foreach($MSFrameworkDatabase->getAssoc("SELECT id, nome FROM negozio_sedi WHERE slug = ''") as $sede) {
        $nomi = json_decode($sede['nome'], true);
        $slugs = array();

        foreach($nomi as $lingua => $nome) {
            $slug = $MSFrameworkUrl->cleanString($nome);
            if(!$MSFrameworkUrl->checkSlugAvailability($slug, "negozio_sedi", $sede['id'])) {
                $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
            }

            $slugs[$lingua] = $slug;

            $got_errors[] = $updater->checkQueryError(
                $MSFrameworkDatabase->pushToDB(
                    "UPDATE `negozio_sedi` SET slug = :slug WHERE id = :id", array(":slug" => json_encode($slugs), ":id" => $sede['id'])
                )
            );
        }
    }

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

<?php
/**
 * MSAdminControlPanel
 * Date: 06/02/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `customers_gift` (
                 `id` int(11) NOT NULL AUTO_INCREMENT,
                 `cliente` int(11) NOT NULL,
                 `cliente_from` int(11) NOT NULL,
                 `gift_id` varchar(30) NOT NULL,
                 `qty` int(11) NOT NULL,
                 `ref` longtext NOT NULL,
                 `note` longtext NOT NULL,
                 `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                 PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

<?php
    require_once('../../../../../sw-config.php');
    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

    foreach($dbs as $dbname) {
        unset($MSFrameworkDatabase);
        $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query("ALTER TABLE `ecommerce_products` CHANGE `category` `category` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;"));
    }

    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
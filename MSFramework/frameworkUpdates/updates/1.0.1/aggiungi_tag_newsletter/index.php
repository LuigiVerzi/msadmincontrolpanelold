<?php
    require_once('../../../../../sw-config.php');
    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

    foreach($dbs as $dbname) {
        unset($MSFrameworkDatabase);
        $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query("ALTER TABLE `newsletter` ADD `tag` TEXT NOT NULL AFTER `nome`;"));
    }

    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
<?php
    require_once('../../../../../sw-config.php');
    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

    foreach($dbs as $dbname) {
        unset($MSFrameworkDatabase);
        $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query(
            "CREATE TABLE IF NOT EXISTS `blog_categories` (
                  `id` int(10) NOT NULL AUTO_INCREMENT,
                  `nome` longtext NOT NULL,
                  `slug` longtext NOT NULL,
                  `descr` longtext NOT NULL,
                  `images` longtext NOT NULL,
                  `banner` longtext NOT NULL,
                  `menu_colors` longtext NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        );

        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query(
            "CREATE TABLE `blog_posts` (
                  `id` int(11) NOT NULL,
                  `titolo` longtext NOT NULL,
                  `slug` longtext NOT NULL,
                  `descrizione_breve` longtext NOT NULL,
                  `content` longtext NOT NULL,
                  `tag` mediumtext NOT NULL,
                  `seo_titolo` longtext NOT NULL,
                  `seo_descrizione` longtext NOT NULL,
                  `images` longtext NOT NULL,
                  `category` longtext NOT NULL,
                  `author` int(11) NOT NULL,
                  `data_creazione` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `last_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `active` varchar(1) NOT NULL DEFAULT '1'
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
        );

        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query(
            "ALTER TABLE `blog_posts`
                ADD PRIMARY KEY (`id`);")
        );

        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query(
            "ALTER TABLE `blog_posts`
                MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;")
        );
    }

    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
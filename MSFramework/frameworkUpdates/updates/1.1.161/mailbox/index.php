<?php
/**
 * MSAdminControlPanel
 * Date: 10/09/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("CREATE TABLE `email_config` ( `id` INT NOT NULL AUTO_INCREMENT , `data` LONGTEXT NOT NULL , `enable_mailbox` INT NOT NULL DEFAULT '0' , PRIMARY KEY (`id`)) ENGINE = InnoDB;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("CREATE TABLE `mailbox__messages` ( `message_id` BIGINT(20) NOT NULL , `body` LONGTEXT NOT NULL , `files` LONGBLOB NOT NULL , `sender_email` INT NOT NULL , `sender_info` VARCHAR(200) NOT NULL , `receiver_email` VARCHAR(200) NOT NULL , `status` INT NOT NULL , `subject` VARCHAR(200) NOT NULL , `read_status` INT NOT NULL , `folder` VARCHAR(200) NOT NULL ) ENGINE = InnoDB;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `mailbox__messages` ADD `mail_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `folder`;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `mailbox__messages` ADD `reply_to_info` VARCHAR(200) NOT NULL AFTER `sender_info`;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `mailbox__messages` CHANGE `message_id` `message_id` VARCHAR(100) NOT NULL;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `mailbox__messages` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `mailbox__messages` ADD `server_id` INT NOT NULL AFTER `message_id`;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `mailbox__messages` DROP `files`;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("CREATE TABLE `mailbox__attachments` ( `id` INT NOT NULL AUTO_INCREMENT , `file_name` VARCHAR(200) NOT NULL , `data` LONGBLOB NOT NULL , `email_id` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `mailbox__attachments` ADD `mime` VARCHAR(150) NOT NULL AFTER `data`;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("CREATE TABLE `mailbox__templates` ( `id` INT NOT NULL AUTO_INCREMENT , `autore` INT NOT NULL , `titolo` TEXT NOT NULL , `versione_html` LONGTEXT NOT NULL , `versione_testuale` LONGTEXT NOT NULL , `data_creazione` DATETIME NOT NULL , `attachments` LONGTEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `mailbox__templates` CHANGE `data_creazione` `data_creazione` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `mailbox__messages` CHANGE `sender_email` `sender_email` VARCHAR(200) NOT NULL;")
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
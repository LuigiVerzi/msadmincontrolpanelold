<?php
/**
 * MSAdminControlPanel
 * Date: 14/11/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DROP TABLE `fatturazione_casse`, `fatturazione_vendite`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `fatturazione_casse` (
              `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `nome` text NOT NULL,
              `descrizione` text NOT NULL,
              `dispositivo` text NOT NULL,
              `logo_scontrino` text NOT NULL,
              `messaggio_promozionale` text NOT NULL,
              `dati_connessione` longtext NOT NULL,
              `fiscale` int(11) NOT NULL,
              `is_active` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `fatturazione_vendite` (
              `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `cliente` int(11) NOT NULL,
              `dati_cliente` longtext NOT NULL,
              `operatore` int(11) NOT NULL,
              `carrello` longtext NOT NULL,
              `tipo_documento` text NOT NULL,
              `info_documento` longtext NOT NULL,
              `coupon` text NOT NULL,
              `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `stato` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>



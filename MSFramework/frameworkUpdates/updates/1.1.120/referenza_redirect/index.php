<?php
/**
 * MSAdminControlPanel
 * Date: 06/02/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `redirect` ADD `redirect_ref` VARCHAR(50) NOT NULL AFTER `redirect_type`;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `redirect` ADD `creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `redirect_ref`;")
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

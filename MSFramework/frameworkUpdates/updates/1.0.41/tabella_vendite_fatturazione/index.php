<?php
/**
 * MSAdminControlPanel
 * Date: 14/11/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `fatturazione_vendite` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `cliente` int(11) NOT NULL,
              `operatore` int(11) NOT NULL,
              `carrello` longtext NOT NULL,
              `cassa` int(11) NOT NULL,
              `coupon` text NOT NULL,
              `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `stato` int(11) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>



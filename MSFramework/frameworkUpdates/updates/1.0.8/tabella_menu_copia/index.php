<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $r = $MSFrameworkCMS->getCMSData('menu');

    $array_to_save = array(
        "nome" => $MSFrameworki18n->setFieldValue('', 'Menu Principale'),
        "list" => json_encode($r['list']),
    );

    $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
    $result = $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO menu ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]));

    $updater->checkQueryError($MSFrameworkDatabase->query("DELETE FROM cms WHERE type = 'menu'"));
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

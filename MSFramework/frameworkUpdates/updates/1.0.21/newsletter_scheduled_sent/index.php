<?php
/**
 * MSAdminControlPanel
 * Date: 14/11/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `newsletter__autosent_log` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `campaign_id` INT(11) NOT NULL , `dest_id` INT(11) NOT NULL, `last_sent` INT(10) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `newsletter__campagne_inviate` ADD `include_email_ids` LONGTEXT NOT NULL AFTER `include_email`, ADD `exclude_email_ids` LONGTEXT NOT NULL AFTER `include_email_ids`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `newsletter__campagne_inviate` ADD `original_campaign_id` INT(11) NOT NULL"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
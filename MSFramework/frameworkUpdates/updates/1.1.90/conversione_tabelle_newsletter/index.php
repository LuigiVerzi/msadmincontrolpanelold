<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_campagne TO newsletter__emails;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_campagne_destinatari_status TO newsletter__emails_destinatari_status;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_destinatari TO newsletter__destinatari;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_mittenti TO newsletter__mittenti;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_smtp TO newsletter__smtp;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_tag TO newsletter__tag;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_tag_destinatari TO newsletter__tag_destinatari;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_autosent_log TO newsletter__autosent_log;"
        )
    );


    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_template TO newsletter__template;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "RENAME TABLE newsletter_campagne_inviate TO newsletter__emails_inviate;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__emails` ADD `status` INT NOT NULL AFTER `send_background`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "CREATE TABLE `newsletter__campagne` (
              `id` int(11) NOT NULL,
              `nome` text NOT NULL,
              `data_creazione` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `status` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__campagne` ADD PRIMARY KEY (`id`);"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__campagne` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__emails` ADD `campaign_id` INT NOT NULL AFTER `id`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__campagne` ADD `tag` LONGTEXT NOT NULL AFTER `nome`, ADD `diagramma` LONGTEXT NOT NULL AFTER `tag`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__campagne` ADD `autore` INT NOT NULL AFTER `id`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__campagne` CHANGE `tag` `origin` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__emails_destinatari_status`
  DROP `ip_receiver`,
  DROP `user_agent`,
  DROP `city`,
  DROP `region`,
  DROP `country_code`,
  DROP `country_name`,
  DROP `continent_code`,
  DROP `latitude`,
  DROP `longitude`,
  DROP `region_code`,
  DROP `region_name`,
  DROP `currency_code`,
  DROP `comment`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__emails_destinatari_status` CHANGE `id_campaign` `id_email` INT(11) NOT NULL DEFAULT '0';"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__emails`
  DROP `tags`,
  DROP `exclude_tags`,
  DROP `exclude_email`,
  DROP `include_email`,
  DROP `send_schedule`,
  DROP `schedule_data`,
  DROP `smtp_server`,
  DROP `send_background`,
  DROP `status`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__emails` ADD `automation_id` VARCHAR(150) NOT NULL AFTER `id`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "CREATE TABLE `newsletter__campagne_destinatari_status` (
  `id_campaign` int(11) NOT NULL DEFAULT '0',
  `id_recipient` varchar(50) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_last_action` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1',
  `ip_receiver` varchar(48) NOT NULL DEFAULT '',
  `user_agent` varchar(128) NOT NULL DEFAULT '',
  `city` varchar(48) NOT NULL DEFAULT '',
  `region` varchar(48) NOT NULL DEFAULT '',
  `country_code` varchar(12) NOT NULL DEFAULT '',
  `country_name` varchar(48) NOT NULL DEFAULT '',
  `continent_code` varchar(12) NOT NULL DEFAULT '',
  `latitude` varchar(12) NOT NULL DEFAULT '',
  `longitude` varchar(12) NOT NULL DEFAULT '',
  `region_code` varchar(12) NOT NULL DEFAULT '',
  `region_name` varchar(48) NOT NULL DEFAULT '',
  `currency_code` varchar(12) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__campagne_destinatari_status` ADD PRIMARY KEY (`id_campaign`,`id_recipient`);"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__campagne_destinatari_status` ADD `automations_data` LONGTEXT NOT NULL AFTER `date_last_action`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "DROP TABLE `newsletter__emails_inviate`"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__emails_destinatari_status` CHANGE `date_sent` `date_sent` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__emails_destinatari_status` ADD `clicked_info` LONGTEXT NOT NULL AFTER `clicked`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "DROP TABLE `newsletter__smtp`"
        )
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
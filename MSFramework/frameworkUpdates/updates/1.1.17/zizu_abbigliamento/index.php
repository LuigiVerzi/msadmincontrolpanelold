<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");
require REAL_SW_PATH . '/classes/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$filename = 'abbigliamento.xls';
$images_path = "images/";
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
$reader->setReadDataOnly(true);
$excel = $reader->load($filename);
$worksheetData = $reader->listWorksheetInfo($filename);

//recupero i dati dal primo foglio del file
$excel->setActiveSheetIndex(0);
$worksheet = $excel->getActiveSheet();
$highestRow = $worksheet->getHighestRow();

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $site_folder_path = realpath(dirname( __FILE__ ) . '/../../../../../../..') . "/" . (new \MSFramework\cms())->getVirtualServerName() . "/";
    mkdir($site_folder_path . "uploads/img/ecommerce/products/", 0755, true);
    mkdir($site_folder_path . "uploads/img/ecommerce/products/tn/", 0755, true);

    $db_action_for_cats = "insert";
    $category_assoc = array();
    $categorie = array("Abiti", "Giacche", "Maglie", "Gonne", "Pantaloni");
    foreach($categorie as $categoria) {
        $cat1 = ucfirstSentence($categoria);

        $categoria2 = $MSFrameworkDatabase->getAssoc("SELECT id FROM ecommerce_categories WHERE nome = :nome", array(":nome" => '{"it_IT":"' . $cat1 . '"}'), true);
        if($categoria2['id'] != "") {
            $use_cat_id = $categoria2['id'];
        } else {
            $slug_cat = (new MSFramework\url())->cleanString($cat1);
            if(!(new MSFramework\url())->checkSlugAvailability($slug_cat)) {
                $slug_cat = (new MSFramework\url())->findNextAvailableSlug($slug_cat);
            }

            $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_categories (nome, slug) VALUES (:nome, :slug)", array(":nome" => $MSFrameworki18n->setFieldValue('', $cat1), ":slug" => $MSFrameworki18n->setFieldValue('', $slug_cat))));
            $use_cat_id = $MSFrameworkDatabase->lastInsertId();

            $ecommerce_cats_order['list'][]['id'] = $use_cat_id;
        }

        $category_assoc[$categoria] = $use_cat_id;
    }

    for($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
        $cod = trim($worksheet->getCell('A' . $row)->getValue());
        $prezzo = trim(str_replace(array("€", ","), array("", "."), $worksheet->getCell('D' . $row)->getCalculatedValue()));
        if(!strstr($prezzo, ".") && $prezzo != "") {
            $prezzo .= ".00";
        }

        if(!is_numeric($prezzo) && $prezzo != "") {
            $got_infos[] = "DB: " . $dbname . ". Prodotto: " . $cod . ". Il prezzo non è nel formato adatto. Saltato.";
            continue;
        }

        $nome = ucfirstSentence($worksheet->getCell('B' . $row)->getValue());

        $slug = (new MSFramework\url())->cleanString($nome);
        if(!(new MSFramework\url())->checkSlugAvailability($slug)) {
            $slug = (new MSFramework\url())->findNextAvailableSlug($slug);
        }

        //Struttura cartella /images
        /*
         * [ABITI] (verrà usata per creare/determinare la categoria di appartenenza)
         *      [Codice prodotto] (verrà usata per linkare la riga del file alle immagini del prodotto)
         *          file 1.jpg
         *          file 2.jpg
         *          ...
         */
        foreach ((new \DirectoryIterator(dirname(__FILE__) . "/" . $images_path)) as $fileinfo) {
            if (!$fileinfo->isDot()) {
                $dir_category = $fileinfo->getFilename();

                $new_filename = "";
                $found_product_path = array();

                $cur_cat_path = $images_path . $dir_category . "/";
                if(file_exists($cur_cat_path . $cod)) {
                    $found_product_path[0] = $cur_cat_path . $cod;
                } else if(file_exists($cur_cat_path . strtolower($cod))) {
                    $found_product_path[0] = $cur_cat_path . strtolower($cod);
                } else if(file_exists($cur_cat_path . strtoupper($cod))) {
                    $found_product_path[0] = $cur_cat_path . strtoupper($cod);
                }

                if(count($found_product_path) != 0) {
                    $found_product_path[1] = ucfirstSentence(strtolower($dir_category));
                    break;
                }
            }
        }

        $final_files = array();
        $files = glob($found_product_path[0] . '/*.{jpg}', GLOB_BRACE);
        foreach($files as $fileK => $fileTmp) {
            $file = preg_replace('/\s/i', '%20', $fileTmp);
            $new_filename = preg_replace('/\s/i', '_', time() . "_"  . $cod . "_" . $fileK . ".jpg");
            $final_files[] = $new_filename;

            copy(dirname(__FILE__) . "/" . $file, $site_folder_path . "uploads/img/ecommerce/products/" . $new_filename);
            createThumbnail($site_folder_path . "uploads/img/ecommerce/products/", $new_filename, $site_folder_path . "uploads/img/ecommerce/products/tn/", 750);
        }

        $taglia_r = $MSFrameworkDatabase->getAssoc("SELECT id FROM ecommerce_attributes WHERE nome = :nome", array(":nome" => '{"it_IT":"Taglie"}'), true);
        if($taglia_r['id'] != "") {
            $id_cat_taglie = $taglia_r['id'];
        } else {
            $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_attributes (nome, is_variant, is_filter, show_in_product, is_main, show_mode) VALUES (:nome, 1, 1, 1, 0, '')", array(":nome" => $MSFrameworki18n->setFieldValue('', 'Taglie'))));
            $id_cat_taglie = $MSFrameworkDatabase->lastInsertId();
        }

        $colori_r = $MSFrameworkDatabase->getAssoc("SELECT id FROM ecommerce_attributes WHERE nome = :nome", array(":nome" => '{"it_IT":"Colori"}'), true);
        if($colori_r['id'] != "") {
            $id_cat_colori = $colori_r['id'];
        } else {
            $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_attributes (nome, is_variant, is_filter, show_in_product, is_main, show_mode) VALUES (:nome, 1, 1, 1, 0, '')", array(":nome" => $MSFrameworki18n->setFieldValue('', 'Colori'))));
            $id_cat_colori = $MSFrameworkDatabase->lastInsertId();
        }

        $taglie = explode("\\", $worksheet->getCell('E' . $row)->getValue());
        $attributes = array();
        $taglie_ary = array();
        foreach($taglie as $tagliaOriginal) {
            $taglia = ucfirstSentence($tagliaOriginal);

            $taglia_r = $MSFrameworkDatabase->getAssoc("SELECT id FROM ecommerce_attributes_values WHERE nome = :nome AND child_of = :child", array(":nome" => '{"it_IT":"' . $taglia . '"}', ":child" => $id_cat_taglie), true);
            if($taglia_r['id'] != "") {
                $use_taglia_id = $taglia_r['id'];
            } else {
                $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_attributes_values (nome, child_of) VALUES (:nome, :child)", array(":nome" => $MSFrameworki18n->setFieldValue('', $taglia), ":child" => $id_cat_taglie)));
                $use_taglia_id = $MSFrameworkDatabase->lastInsertId();
            }

            $taglie_ary[] = 'attrval_' . $use_taglia_id;
        }

        $attributes[] = array('attr_' . $id_cat_taglie, $taglie_ary);

        $colori = explode("\\", $worksheet->getCell('F' . $row)->getValue());
        $colori_to_use = array();
        $colori_ary = array();
        foreach($colori as $coloreOriginal) {
            $colore = ucfirstSentence($coloreOriginal);

            $colore_r = $MSFrameworkDatabase->getAssoc("SELECT id FROM ecommerce_attributes_values WHERE nome = :nome AND child_of = :child", array(":nome" => '{"it_IT":"' . $colore . '"}', ":child" => $id_cat_colori), true);
            if($colore_r['id'] != "") {
                $use_colore_id = $colore_r['id'];
            } else {
                $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_attributes_values (nome, child_of) VALUES (:nome, :child)", array(":nome" => $MSFrameworki18n->setFieldValue('', $colore), ":child" => $id_cat_colori)));
                $use_colore_id = $MSFrameworkDatabase->lastInsertId();
            }

            $colori_ary[] = 'attrval_' . $use_colore_id;
        }

        $attributes[] = array('attr_' . $id_cat_colori, $colori_ary);

        $disponibilita = $worksheet->getCell('G' . $row)->getValue();
        $descrizione = $worksheet->getCell('C' . $row)->getValue();

        $array_to_save = array(
            "nome" => $MSFrameworki18n->setFieldValue('', $nome),
            "slug" => $MSFrameworki18n->setFieldValue('', $slug),
            "category" => $category_assoc[$found_product_path[1]],
            "is_active" => "1",
            "long_descr" => $MSFrameworki18n->setFieldValue('', $descrizione),
            "short_descr" => $MSFrameworki18n->setFieldValue('', $descrizione),
            "gallery" => (count($final_files) != 0 ? json_encode($final_files) : ""),
            "attributes" => (count($attributes) != 0 ? json_encode($attributes) : ""),
            "prezzo" => number_format($prezzo, 2),
            "imposta" => "22.00",
            "manage_warehouse" => '1',
            "warehouse_qty" => ($disponibilita != '') ? $disponibilita : '0',
        );

        $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_products ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]));
    }

    $array_to_save = array(
        "value" => json_encode(array(
            "list" => $ecommerce_cats_order['list']
        )),
        "type" => "ecommerce_cats_order"
    );

    $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action_for_cats);

    if($db_action_for_cats == "insert") {
        $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    } else {
        $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'ecommerce_cats_order'", $stringForDB[0]);
    }
}

function ucfirstSentence($input){
    $output = preg_replace_callback('/([.!?])\s*(\w)/', function ($matches) {
        return strtoupper($matches[1] . ' ' . $matches[2]);
    }, ucfirst(strtolower($input)));

    return $output;
}

function createThumbnail($imageDirectory, $imageName, $thumbDirectory, $thumbWidth, $quality = 100) {
    $image_extension = @end(explode(".", $imageName));

    switch($image_extension)
    {
        case "jpg":
            @$srcImg = imagecreatefromjpeg("$imageDirectory/$imageName");
            break;
        case "jpeg":
            @$srcImg = imagecreatefromjpeg("$imageDirectory/$imageName");
            break;
        case "png":
            $srcImg = imagecreatefrompng("$imageDirectory/$imageName");
            break;
    }

    if(!$srcImg) exit;
    $origWidth = imagesx($srcImg);
    $origHeight = imagesy($srcImg);
    $ratio = $origHeight/ $origWidth;
    $thumbHeight = $thumbWidth * $ratio;

    $thumbImg = imagecreatetruecolor($thumbWidth, $thumbHeight);

    if($image_extension == 'png')
    {
        $background = imagecolorallocate($thumbImg, 0, 0, 0);
        imagecolortransparent($thumbImg, $background);
        imagealphablending($thumbImg, false);
        imagesavealpha($thumbImg, true);
    }

    imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origWidth, $origHeight);

    switch($image_extension)
    {
        case "jpg":
            imagejpeg($thumbImg, "$thumbDirectory/$imageName", $quality);
            break;
        case "jpeg":
            imagejpeg($thumbImg, "$thumbDirectory/$imageName", $quality);
            break;
        case "png":
            imagepng($thumbImg, "$thumbDirectory/$imageName");
            break;
    }
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 06/02/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

$replace_array = array(
    "{logo}" => "{ms-site-logo}",
    "{logo_footer}" => "{ms-site-logo2}",
    "{site-name}" => "{ms-site-name}",
    "{site-url}" => "{ms-site-url}",
    "{geo-address}" => "{ms-geo-address}",
    "{site-phone}" => "{ms-site-phone}",
    "{site-email}" => "{ms-site-email}",
    "{site-pec}" => "{ms-site-pec}",
    "{social-instagram}" =>  "{ms-social-instagram}",
    "{social-twitter}" =>  "{ms-social-twitter}",
    "{social-facebook}" =>  "{ms-social-facebook}",
    "{social-linkedin}" =>  "{ms-social-linkedin}",
    "{social-youtube}" =>  "{ms-social-youtube}",
    "{social-skype}" =>  "{ms-social-skype}",
);

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $shortcodes_where = "content LIKE '%" . implode("%' OR content LIKE '%", array_keys($replace_array)) . "%'";

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `pagine` WHERE $shortcodes_where") as $page) {
        $replaced_content = strtr($page['content'], $replace_array);

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query("UPDATE `pagine` SET content = :content WHERE id = :page_id", array(':content' => $replaced_content, ':page_id' => $page['id']))
        );

    }

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

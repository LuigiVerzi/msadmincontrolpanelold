<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("CREATE TABLE `newsletter__sms` ( `id` INT NOT NULL AUTO_INCREMENT , `automation_id` VARCHAR(150) NOT NULL , `campaign_id` INT NOT NULL , `name` MEDIUMTEXT NOT NULL , `sender` TEXT NOT NULL , `message` MEDIUMTEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("CREATE TABLE `newsletter__sms_destinatari_status` ( `id_sms` INT NOT NULL , `id_recipient` INT NOT NULL , `date_sent` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `date_received` TIMESTAMP NOT NULL DEFAULT '2000-01-01 00:00:01' ) ENGINE = InnoDB;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `newsletter__sms_destinatari_status` ADD `sms_id` TEXT NOT NULL AFTER `date_received`;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `newsletter__sms_destinatari_status` ADD UNIQUE(`id_sms`);")
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
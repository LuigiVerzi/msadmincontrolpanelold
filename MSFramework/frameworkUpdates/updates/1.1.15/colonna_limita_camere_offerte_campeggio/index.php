<?php
/**
 * MSAdminControlPanel
 * Date: 2019-04-09
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `camping_offerte` ADD `avaialable_rooms` LONGTEXT NOT NULL AFTER `auto_renew_promo`;"
        )
    );

    // Ottengo un array ID_Offerta => Array(ID PIAZZOLE)
    $offerte_piazzola = array();
    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `camping_piazzole`") as $piazzola) {
        if(json_decode($piazzola['offerte'])) {
            foreach(json_decode($piazzola['offerte'], true) as $offerta) {
               if(!isset($offerte_piazzola[$offerta])) {
                   $offerte_piazzola[$offerta] = array();
               }
                $offerte_piazzola[$offerta][] = $piazzola['id'];
            }
        }
    }

    foreach($offerte_piazzola as $id_offerta => $id_piazzole) {
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query(
                "UPDATE camping_offerte SET avaialable_rooms = :piazzole WHERE id = :id_offerta;",  array(':piazzole' => json_encode($id_piazzole), ':id_offerta' => $id_offerta)
            )
        );
    }

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `camping_piazzole` DROP `offerte`;"
        )
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
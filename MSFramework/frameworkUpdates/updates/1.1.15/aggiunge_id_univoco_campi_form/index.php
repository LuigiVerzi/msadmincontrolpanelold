<?php
/**
 * MSAdminControlPanel
 * Date: 2019-04-09
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);


    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `forms`") as $form) {

        if(json_decode($form['fields'])) {
            $form_fields = json_decode($form['fields'], true);

            foreach($form_fields as $k => $field) {
                $form_fields[$k]['unique_id'] = uniqid('u');
            }

            $got_errors[] = $updater->checkQueryError(
                $MSFrameworkDatabase->query(
                    "UPDATE forms SET fields = :fields WHERE id = :form_id;",  array(':fields' => json_encode($form_fields), ':form_id' => $form['id'])
                )
            );

        }
    }

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
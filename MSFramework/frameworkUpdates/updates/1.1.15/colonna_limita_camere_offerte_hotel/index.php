<?php
/**
 * MSAdminControlPanel
 * Date: 2019-04-09
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `hotel_offerte` ADD `avaialable_rooms` LONGTEXT NOT NULL AFTER `auto_renew_promo`;"
        )
    );

    // Ottengo un array ID_Offerta => Array(ID rooms)
    $offerte_camera = array();
    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM `hotel_rooms`") as $camera) {
        if(json_decode($camera['offerte'])) {
            foreach(json_decode($camera['offerte'], true) as $offerta) {
                if(!isset($offerte_camera[$offerta])) {
                    $offerte_camera[$offerta] = array();
                }
                $offerte_camera[$offerta][] = $camera['id'];
            }
        }
    }

    foreach($offerte_camera as $id_offerta => $id_rooms) {
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query(
                "UPDATE hotel_offerte SET avaialable_rooms = :rooms WHERE id = :id_offerta;",  array(':rooms' => json_encode($id_rooms), ':id_offerta' => $id_offerta)
            )
        );
    }

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `hotel_rooms` DROP `offerte`;"
        )
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
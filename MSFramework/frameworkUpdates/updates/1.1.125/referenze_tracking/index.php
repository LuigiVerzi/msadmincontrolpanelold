<?php
/**
 * MSAdminControlPanel
 * Date: 10/09/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("CREATE TABLE `tracking__references` (`id` int(11) NOT NULL, `project_id` int(11) NOT NULL, `user_ref` int(11) NOT NULL, `track_references` longtext NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `tracking__references` ADD PRIMARY KEY (`id`);")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `tracking__references` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("ALTER TABLE `fatturazione_vendite` ADD `reference` VARCHAR(100) NOT NULL AFTER `sent_date`;")
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
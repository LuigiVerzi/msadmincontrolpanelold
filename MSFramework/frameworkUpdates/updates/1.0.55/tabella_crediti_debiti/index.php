<?php
/**
 * MSAdminControlPanel
 * Date: 30/12/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE `customers_balance` (
              `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `cliente` int(11) NOT NULL,
              `tipo` text NOT NULL,
              `valore` float NOT NULL,
              `note` longtext NOT NULL,
              `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $r = $MSFrameworkDatabase->getAssoc("SELECT value FROM `cms` WHERE type = 'producer_config'", array(), true);

    $r_producer_config = json_decode($r['value'], true);
    $extra_functions = json_decode($r_producer_config['extra_functions'], true);
    $extra_functions[] = "working_hours";
    $r_producer_config['extra_functions'] = json_encode($extra_functions);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "UPDATE `cms` SET `value` = :myvalue WHERE `type` = 'producer_config'", array(':myvalue' => json_encode($r_producer_config))
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
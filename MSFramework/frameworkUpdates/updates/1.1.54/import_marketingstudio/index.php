<?php
/**
 * MSAdminControlPanel
 * Date: 20/03/2019
 */


$_POST['version'] =	'1.1.54';
$_POST['procedure_folder'] = 'import_marketingstudio';

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");


foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    /*IMPORTO GLI UTENTI*/
    $user_assoc = array();
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM users"
        )
    );

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.users") as $user) {
        $slug = $MSFrameworkUrl->cleanString($user['nome'] . " " . $user['cognome']);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "users", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO users (slug, nome, cognome, ruolo, email, cellulare, website, password, active, propic) VALUES (:slug, :nome, :cognome, :ruolo, :email, :cellulare, :website, :password, :active, :propic)", array(
                    ":slug" => $slug,
                    ":nome" => $user['nome'],
                    ":cognome" => $user['cognome'],
                    ":ruolo" => ($user['ruolo'] == "0" ? "1" : $user['ruolo']),
                    ":email" => $user['email'],
                    ":cellulare" => $user['cellulare'],
                    ":website" => $user['website'],
                    ":password" => $user['password'],
                    ":active" => $user['active'],
                    ":propic" => $user['propic'],
                )
            )
        );

        $user_assoc[$user['id']] = $MSFrameworkDatabase->lastInsertId();
    }


    /*IMPORTO LE CATEGORIE DEL BLOG*/
    $cat_assoc = array();
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM blog_categories"
        )
    );

    $blog_cats_order = array();

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.blog_categorie") as $categoria_blog) {
        $slug = $MSFrameworkUrl->cleanString($categoria_blog['titolo']);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "blog_categories", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO blog_categories (nome, slug, descr) VALUES (:nome, :slug, :descr)", array(":nome" => json_encode(array("it_IT" => $categoria_blog['titolo'])), ":slug" => json_encode(array("it_IT" => $slug)), ":descr" => json_encode(array("it_IT" => $categoria_blog['descrizione_breve'])))
            )
        );

        $cat_id = $MSFrameworkDatabase->lastInsertId();
        $blog_cats_order['list'][]['id'] = $cat_id;

        $cat_assoc[$categoria_blog['id']] = $cat_id;
    }

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM cms WHERE type = 'blog_cats_order'"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "INSERT INTO cms (`type`, `value`) VALUES ('blog_cats_order', :value)",
            array(
                ":value" => json_encode(
                    array(
                        "list" => $blog_cats_order['list']
                    )
                )
            )
        )
    );

    /*IMPORTO I POST DEL BLOG*/
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM blog_posts"
        )
    );


    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.blog_post") as $post_blog) {
        $slug = $MSFrameworkUrl->cleanString($post_blog['titolo']);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "blog_posts", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $new_images = array();
        foreach(json_decode($post_blog['images']) as $imgK => $imgV) {
            $new_images[$imgK] = array($imgV);
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO blog_posts (titolo, slug, descrizione_breve, content, tag, seo_titolo, seo_descrizione, images, category, author, visite, active, data_creazione, last_update) VALUES (:nome, :slug, :descr, :content, :tag, :seo_titolo, :seo_descrizione, :images, :category, :author, :visite, :active, :data_creazione, :last_update)",
                array(
                    ":nome" => json_encode(array("it_IT" => $post_blog['titolo'])),
                    ":slug" => json_encode(array("it_IT" => $slug)),
                    ":descr" => json_encode(array("it_IT" => $post_blog['descrizione_breve'])),
                    ":content" => json_encode(array("it_IT" => $post_blog['content'])),
                    ":tag" => json_encode(array("it_IT" => $post_blog['tag'])),
                    ":seo_titolo" => json_encode(array("it_IT" => $post_blog['seo_titolo'])),
                    ":seo_descrizione" => json_encode(array("it_IT" => $post_blog['seo_descrizione'])),
                    ":images" => json_encode($new_images),
                    ":category" => $cat_assoc[$post_blog['categoria']],
                    ":author" => $user_assoc[$post_blog['author']],
                    ":visite" => $post_blog['visit_num'],
                    ":active" => $post_blog['active'],
                    ":data_creazione" => $post_blog['data_creazione'],
                    ":last_update" => $post_blog['last_update'],
                )
            )
        );
    }

    /*IMPORTO LE PREFERENZE DEL BLOG*/
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM cms WHERE type = 'blog'"
        )
    );

    $cms_blog_r = $MSFrameworkDatabase->getAssoc("SELECT value FROM marke833_marketingstudio.cms WHERE type = 'blog'", array(), true);
    $cms_blog = json_decode($cms_blog_r['value'], true);
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "INSERT INTO cms (`type`, `value`) VALUES ('blog', :value)",
            array(
                ":value" => json_encode(
                    array(
                        "titolo" => json_encode(array("it_IT" => $cms_blog['titolo'])),
                        "slug" => json_encode(array("it_IT" => "blog")),
                        "descrizione" => json_encode(array("it_IT" => $cms_blog['descrizione'])),
                        "seo_titolo" => json_encode(array("it_IT" => $cms_blog['seo_titolo'])),
                        "seo_descrizione" => json_encode(array("it_IT" => $cms_blog['seo_descrizione'])),
                        "blog_articoli_mostrati" => $cms_blog['blog_articoli_mostrati'],
                        "categoria_articoli_mostrati" => $cms_blog['categoria_articoli_mostrati']
                    )
                )
            )
        )
    );

    /*IMPORTO LO STAFF*/
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM negozio_team_category"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM negozio_team"
        )
    );

    $team_cat_assoc = array();
    foreach(array("Engineers", "Marketing Specialists") as $inserting_cat) {
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO negozio_team_category (nome) VALUES (:nome)",
                array(
                    ":nome" => json_encode(array("it_IT" => $inserting_cat)),
                )
            )
        );

        $team_cat_assoc[$inserting_cat] = $MSFrameworkDatabase->lastInsertId();
    }

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.staff") as $r_staff) {
        $nome = explode(" ", $r_staff['nome']);
        $category = ($r_staff['ruolo'] == "Programmatore" ? $team_cat_assoc['Engineers'] : $team_cat_assoc['Marketing Specialists']);
        $social = json_decode($r_staff['social'], true);

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO negozio_team (nome, cognome, posizione, descrizione, category, propic, website, fb_profile, instagram_profile, linkedin_profile) VALUES (:nome, :cognome, :posizione, :descrizione, :category, :propic, :website, :fb_profile, :instagram_profile, :linkedin_profile)",
                array(
                    ":nome" => $nome[0],
                    ":cognome" => $nome[1],
                    ":posizione" => json_encode(array("it_IT" => $r_staff['ruolo'])),
                    ":descrizione" => json_encode(array("it_IT" => $r_staff['descrizione'])),
                    ":category" => $category,
                    ":propic" => $r_staff['foto'],
                    ":website" => $social['website'],
                    ":fb_profile" => $social['facebook'],
                    ":instagram_profile" => $social['instagram'],
                    ":linkedin_profile" => $social['linkedin'],
                )
            )
        );
    }

    /*IMPORTO I CLIENTI*/
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM customers"
        )
    );

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.customers") as $r_customer) {
        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO customers (nome, cognome, email, telefono_casa, telefono_cellulare, active) VALUES (:nome, :cognome, :email, :telefono_casa, :telefono_cellulare, :active)",
                array(
                    ":nome" => $r_customer['nome'],
                    ":cognome" => $r_customer['cognome'],
                    ":email" => $r_customer['email'],
                    ":telefono_casa" => $r_customer['telefono'],
                    ":telefono_cellulare" => $r_customer['cellulare'],
                    ":active" => "0",
                )
            )
        );
    }

    /*IMPORTO I SETTORI*/
    $sector_assoc = array();
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM servizi_settori"
        )
    );

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.settori") as $r) {
        $images = json_decode($r['images'], true);

        $slug = $MSFrameworkUrl->cleanString($r['nome']);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "servizi_settori", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO servizi_settori (nome, images, slug) VALUES (:nome, :images, :slug)",
                array(
                    ":nome" => json_encode(array("it_IT" => $r['nome'])),
                    ":slug" => json_encode(array("it_IT" => $slug)),
                    ":images" => ($images['main'] != "" ? json_encode(array($images['main'])) : ""),
                )
            )
        );

        $sector_assoc[$r['id']] = $MSFrameworkDatabase->lastInsertId();
    }


    /*IMPORTO I SERVIZI/CONSULENZE*/
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM servizi_categorie"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM servizi"
        )
    );

    $services_cat_assoc = array();
    foreach(array("Software", "Consulenza") as $inserting_cat) {
        $slug = $MSFrameworkUrl->cleanString($inserting_cat);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "servizi_categorie", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO servizi_categorie (nome, slug) VALUES (:nome, :slug)",
                array(
                    ":nome" => json_encode(array("it_IT" => $inserting_cat)),
                    ":slug" => json_encode(array("it_IT" => $slug))
                )
            )
        );

        $services_cat_assoc[$inserting_cat] = $MSFrameworkDatabase->lastInsertId();
    }

    $services_assoc = array();
    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.servizi") as $r) {
        $images = json_decode($r['images'], true);
        $slug = $MSFrameworkUrl->cleanString($r['nome']);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "servizi", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $sectors_new = array();
        foreach(json_decode($r['sectors_info'], true) as $sk => $sv) {
            if($sv['active'] == "1") {
                $sectors_new[] = $sector_assoc[$sk];
            }
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO servizi (nome, descr, long_descr, descr_title, category, slug, sector, images, banner, prezzo) VALUES (:nome, :descr, :long_descr, :descr_title, :category, :slug, :sector, :images, :banner, :prezzo)",
                array(
                    ":nome" => json_encode(array("it_IT" => $r['nome'])),
                    ":descr" => json_encode(array("it_IT" => $r['short_descr'])),
                    ":long_descr" => json_encode(array("it_IT" => $r['long_descr'])),
                    ":descr_title" => json_encode(array("it_IT" => $r['long_descr_title'])),
                    ":category" => $services_cat_assoc['Software'],
                    ":slug" => json_encode(array("it_IT" => $slug)),
                    ":sector" => json_encode($sectors_new),
                    ":images" => ($images['icon'] != "" ? json_encode(array($images['icon'])) : ""),
                    ":banner" => ($images['main'] != "" ? json_encode(array($images['main'])) : ""),
                    ":prezzo" => "0.00"
                )
            )
        );

        $services_assoc[$r['id']] = $MSFrameworkDatabase->lastInsertId();
    }

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.consulenza") as $r) {
        $images = json_decode($r['images'], true);
        $slug = $MSFrameworkUrl->cleanString($r['nome']);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "servizi", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $sectors_new = array();
        foreach(json_decode($r['sectors_info'], true) as $sk => $sv) {
            if($sv['active'] == "1") {
                $sectors_new[] = $sector_assoc[$sk];
            }
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO servizi (nome, descr, long_descr, descr_title, category, slug, sector, images, banner, prezzo) VALUES (:nome, :descr, :long_descr, :descr_title, :category, :slug, :sector, :images, :banner, :prezzo)",
                array(
                    ":nome" => json_encode(array("it_IT" => $r['nome'])),
                    ":descr" => json_encode(array("it_IT" => $r['short_descr'])),
                    ":long_descr" => json_encode(array("it_IT" => $r['long_descr'])),
                    ":descr_title" => json_encode(array("it_IT" => $r['long_descr_title'])),
                    ":category" => $services_cat_assoc['Consulenza'],
                    ":slug" => json_encode(array("it_IT" => $slug)),
                    ":sector" => json_encode($sectors_new),
                    ":images" => ($images['icon'] != "" ? json_encode(array($images['icon'])) : ""),
                    ":banner" => ($images['main'] != "" ? json_encode(array($images['main'])) : ""),
                    ":prezzo" => "0.00"
                )
            )
        );
    }

    /*IMPORTO IL PORTFOLIO*/
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM servizi_portfolio"
        )
    );

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.portfolio") as $r) {
        $slug = $MSFrameworkUrl->cleanString($r['name']);
        if (!$MSFrameworkUrl->checkSlugAvailability($slug, "servizi_portfolio", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "INSERT INTO servizi_portfolio (nome, is_active, is_highlighted, slug, url, service, sector, descr, images, web_images, mob_images) VALUES (:nome, :active, :highlighted, :slug, :url, :service, :sector, :descr, :images, :web_images, :mob_images)",
                array(
                    ":nome" => json_encode(array("it_IT" => $r['name'])),
                    ":active" => $r['active'],
                    ":highlighted" => $r['important'],
                    ":slug" => json_encode(array("it_IT" => $slug)),
                    ":url" => $r['url'],
                    ":service" => $services_assoc[$r['service_id']],
                    ":sector" => $sector_assoc[$r['sector_id']],
                    ":descr" => json_encode(array("it_IT" => $r['description'])),
                    ":images" => $r['images'],
                    ":web_images" => $r['web_images'],
                    ":mob_images" => $r['mob_images'],
                )
            )
        );
    }

    /*IMPORTO LE PAGINE*/
    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "DELETE FROM pagine WHERE id != '11' AND id != '21' AND id != '22'"
        )
    );

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.servizi") as $r) {
        $sectors_info = json_decode($r['sectors_info'], true);
        foreach($sectors_info as $sectorK => $sector) {
            if($sector['active'] == "0") {
                continue;
            }

            $nome_servizio = $r['nome'];

            $r_settore = $MSFrameworkDatabase->getAssoc("SELECT nome, images FROM servizi_settori WHERE id = :id", array(":id" => $sector_assoc[$sectorK]), true);
            $nome_settore = $MSFrameworki18n->getFieldValue($r_settore['nome']);
            $immagini_settori = json_decode($r_settore['images'], true);

            $slug = $MSFrameworkUrl->cleanString($nome_servizio . " " . $nome_settore);
            if(!$MSFrameworkUrl->checkSlugAvailability($slug, "pagine", '0')) {
                $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
            }

            $got_errors[] = $updater->checkQueryError(
                $MSFrameworkDatabase->pushToDB(
                    "INSERT INTO pagine (slug, titolo, sottotitolo, short_content, content, gallery, `type`, last_update) VALUES (:slug, :titolo, :sottotitolo, :short_content, :content, :gallery, :pagetype, :last_upd)", array(
                        ":slug" => json_encode(array("it_IT" => $slug)),
                        ":titolo" => json_encode(array("it_IT" => ($sector['tag_title'] != "" ? $sector['tag_title'] : $nome_servizio . " per " . $nome_settore))),
                        ":sottotitolo" => json_encode(array("it_IT" => $sector['descr_title'])),
                        ":short_content" => json_encode(array("it_IT" => $sector['short_descr'])),
                        ":content" => json_encode(array("it_IT" => $sector['long_descr'])),
                        ":gallery" => json_encode(array("type" => "1", "gallery" => $immagini_settori)),
                        ":pagetype" => "service",
                        ":last_upd" => time(),
                    )
                )
            );

        }
    }

    foreach($MSFrameworkDatabase->getAssoc("SELECT * FROM marke833_marketingstudio.consulenza") as $r) {
        $sectors_info = json_decode($r['sectors_info'], true);
        foreach($sectors_info as $sectorK => $sector) {
            if($sector['active'] == "0") {
                continue;
            }

            $nome_servizio = $r['nome'];

            $r_settore = $MSFrameworkDatabase->getAssoc("SELECT nome, images FROM servizi_settori WHERE id = :id", array(":id" => $sector_assoc[$sectorK]), true);
            $nome_settore = $MSFrameworki18n->getFieldValue($r_settore['nome']);
            $immagini_settori = json_decode($r_settore['images'], true);

            $slug = $MSFrameworkUrl->cleanString($nome_servizio . " " . $nome_settore);
            if(!$MSFrameworkUrl->checkSlugAvailability($slug, "pagine", '0')) {
                $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
            }

            $got_errors[] = $updater->checkQueryError(
                $MSFrameworkDatabase->pushToDB(
                    "INSERT INTO pagine (slug, titolo, sottotitolo, short_content, content, gallery, `type`, last_update) VALUES (:slug, :titolo, :sottotitolo, :short_content, :content, :gallery, :pagetype, :last_upd)", array(
                        ":slug" => json_encode(array("it_IT" => $slug)),
                        ":titolo" => json_encode(array("it_IT" => ($sector['tag_title'] != "" ? $sector['tag_title'] : $nome_servizio . " per " . $nome_settore))),
                        ":sottotitolo" => json_encode(array("it_IT" => $sector['descr_title'])),
                        ":short_content" => json_encode(array("it_IT" => $sector['short_descr'])),
                        ":content" => json_encode(array("it_IT" => $sector['long_descr'])),
                        ":gallery" => json_encode(array("type" => "1", "gallery" => $immagini_settori)),
                        ":pagetype" => "service",
                        ":last_upd" => time(),
                    )
                )
            );
        }
    }
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

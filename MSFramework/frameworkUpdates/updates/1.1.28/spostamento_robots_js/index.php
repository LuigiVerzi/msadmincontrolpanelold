<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $r_producer = $MSFrameworkDatabase->getAssoc("SELECT value FROM cms WHERE type = 'producer_config'", array(), true);
    $r_site = $MSFrameworkDatabase->getAssoc("SELECT value FROM cms WHERE type = 'site'", array(), true);

    $ary_data_producer = json_decode($r_producer['value'], true);
    $ary_data_site = json_decode($r_site['value'], true);

    $ary_data_site['additional_css'] = $ary_data_producer['additional_css'];
    $ary_data_site['robots_txt'] = $ary_data_producer['robots_txt'];
    unset($ary_data_producer['additional_css']);
    unset($ary_data_producer['robots_txt']);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("UPDATE cms SET value = :value WHERE type = 'producer_config'", array(":value" => json_encode($ary_data_producer)))
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("UPDATE cms SET value = :value WHERE type = 'site'", array(":value" => json_encode($ary_data_site)))
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
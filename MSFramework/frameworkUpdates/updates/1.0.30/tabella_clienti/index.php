<?php
/**
 * MSAdminControlPanel
 * Date: 12/12/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `customers` ADD `dati_fatturazione` LONGTEXT NOT NULL;"
        )
    );

    foreach($MSFrameworkDatabase->getAssoc("SELECT id, data_nascita FROM customers WHERE data_nascita != ''") as $customer) {
        $ts_scadenza = "";
        if($customer['data_nascita'] != "") {
            $ary_scadenza = explode("/", $customer['data_nascita']);
            $ts_scadenza = mktime(23, 59, 59, $ary_scadenza[1], $ary_scadenza[0], $ary_scadenza[2]);
            if(!$ts_scadenza) {
                continue;
            }
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB("UPDATE `customers` SET data_nascita = :data WHERE id = :id", array(":data" => $ts_scadenza, ":id" => $customer['id']))
        );
    }
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>





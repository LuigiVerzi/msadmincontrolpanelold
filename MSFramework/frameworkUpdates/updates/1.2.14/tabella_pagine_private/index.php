<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "CREATE TABLE `pagine_private` ( `id` VARCHAR(200) NOT NULL , `data` LONGTEXT NOT NULL ) ENGINE = InnoDB;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `pagine_private` ADD UNIQUE(`id`);"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `pagine_private` ADD INDEX(`id`);"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
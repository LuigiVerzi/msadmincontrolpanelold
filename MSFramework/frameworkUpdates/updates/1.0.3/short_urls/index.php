<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query(
        "CREATE TABLE IF NOT EXISTS `short_urls` (
          `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
          `url` longtext NOT NULL,
          `created` int(11) UNSIGNED NOT NULL,
          `hits` int(10) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `tracking__references` ADD `user_agent` MEDIUMTEXT NOT NULL AFTER `track_references`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `fatturazione_vendite` ADD `sources` MEDIUMTEXT NOT NULL AFTER `reference`;"
        )
    );

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
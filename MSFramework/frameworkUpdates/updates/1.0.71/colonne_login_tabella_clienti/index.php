<?php
/**
 * MSAdminControlPanel
 * Date: 30/12/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `customers` ADD `password` LONGTEXT NOT NULL AFTER `registration_date`, ADD `mail_auth` LONGTEXT NOT NULL AFTER `password`, ADD `passreset_token` LONGTEXT NOT NULL AFTER `mail_auth`, ADD `active` INT NOT NULL AFTER `passreset_token`;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 14/11/2018
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE IF NOT EXISTS `customers` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `nome` varchar(50) NOT NULL,
                `cognome` varchar(50) NOT NULL,
                `indirizzo` longtext NOT NULL,
                `citta` varchar(50) NOT NULL,
                `provincia` varchar(50) NOT NULL,
                `cap` varchar(5) NOT NULL,
                `sesso` varchar(1) NOT NULL,
                `email` varchar(50) NOT NULL,
                `telefono_casa` varchar(50) NOT NULL,
                `telefono_cellulare` varchar(50) NOT NULL,
                `data_nascita` varchar(10) NOT NULL,
                `provenienza` int(5) NOT NULL,
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>



<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "CREATE TABLE IF NOT EXISTS `camping_piazzole` (
                  `id` int(10) NOT NULL AUTO_INCREMENT,
                  `nome` longtext NOT NULL,
                  `sottotitolo` longtext NOT NULL,
                  `prezzo` varchar(20) NOT NULL,
                  `prezzi_aggiuntivi` longtext NOT NULL,
                  `prezzi_stagionali` longtext,
                  `content` longtext NOT NULL,
                  `gallery` longtext NOT NULL,
                  `services` longtext NOT NULL,
                  `offerte` longtext NOT NULL,
                  `slug` longtext NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>

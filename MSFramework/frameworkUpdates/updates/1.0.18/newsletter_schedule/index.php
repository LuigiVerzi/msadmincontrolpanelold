<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `newsletter__campagne` ADD `send_schedule` VARCHAR(1) NOT NULL, ADD `schedule_data` LONGTEXT NOT NULL"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `newsletter__tag_destinatari` ADD `creation_time` INT(10) NOT NULL"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `newsletter__campagne` ADD `smtp_server` INT(11) NOT NULL, ADD `send_background` VARCHAR(1) NOT NULL"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query(
            "ALTER TABLE `newsletter__campagne_inviate` ADD `send_background` VARCHAR(1) NOT NULL"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
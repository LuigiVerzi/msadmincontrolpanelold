<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__destinatari` DROP `nome`, DROP `email`, DROP `telefono`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `newsletter__destinatari` CHANGE `id` `id` INT(11) NOT NULL;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
<?php
/**
 * MSAdminControlPanel
 * Date: 10/09/2019
 */

require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->query("CREATE TABLE `popup` ( `id` INT NOT NULL AUTO_INCREMENT ,  `titolo` LONGTEXT NOT NULL ,  `content` LONGTEXT NOT NULL ,  `options` LONGTEXT NOT NULL ,  `creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `active` INT NOT NULL,   PRIMARY KEY  (`id`)) ENGINE = InnoDB;")
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
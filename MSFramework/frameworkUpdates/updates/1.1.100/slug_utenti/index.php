<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    foreach($MSFrameworkDatabase->getAssoc("SELECT nome, cognome, id FROM users WHERE slug = ''") as $user) {
        $slug = $MSFrameworkUrl->cleanString($user['nome'] . " " . $user['cognome']);
        if(!$MSFrameworkUrl->checkSlugAvailability($slug, "users", '0')) {
            $slug = $MSFrameworkUrl->findNextAvailableSlug($slug);
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "UPDATE users SET slug = :slug WHERE id = :id", array(
                    ":slug" => $slug,
                    ":id" => $user['id']
                )
            )
        );
    }
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
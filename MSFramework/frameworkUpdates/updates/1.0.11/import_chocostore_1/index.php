<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");
require REAL_SW_PATH . '/classes/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$filename = 'cdd.xls';
$images_path = "images/";
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
$reader->setReadDataOnly(true);
$excel = $reader->load($filename);
$worksheetData = $reader->listWorksheetInfo($filename);

//recupero i dati dal secondo foglio del file

$excel->setActiveSheetIndex(1);
$worksheet = $excel->getActiveSheet();
$array_product_details = array();

for($row = 4; $row <= $worksheetData[1]['totalRows']; $row++) {
    $cod = $worksheet->getCell('A' . $row)->getValue();
    if($cod == "") {
        continue;
    }

    $array_product_details[$cod] = array(
        "peso" => $worksheet->getCell('G' . $row)->getValue(),
        "descrizione_breve" => ucfirstSentence(strip_tags($worksheet->getCell('H' . $row)->getValue())),
        "descrizione" => ucfirstSentence(strip_tags($worksheet->getCell('I' . $row)->getValue())),
    );
}

//recupero i dati dal primo foglio del file
$excel->setActiveSheetIndex(0);
$highestRow = $worksheet->getHighestRow();
$worksheet = $excel->getActiveSheet();

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $site_folder_path = realpath(dirname( __FILE__ ) . '/../../../../../../..') . "/" . (new \MSFramework\cms())->getVirtualServerName() . "/";
    $ecommerce_cats_order = (new \MSFramework\cms())->getCMSData('ecommerce_cats_order');
    mkdir($site_folder_path . "uploads/img/ecommerce/products/", 0755, true);
    mkdir($site_folder_path . "uploads/img/ecommerce/products/tn/", 0755, true);

    if($MSFrameworkDatabase->getCount("SELECT type FROM cms WHERE type = 'ecommerce_cats_order'") == 0) {
        $db_action = "insert";
    } else {
        $db_action = "update";
    }

    for($row = 3; $row <= $worksheetData[0]['totalRows']; $row++) {
        $cod = $worksheet->getCell('C' . $row)->getValue();
        $prod_det = $array_product_details[$cod];

        if(!is_array($prod_det)) {
            $got_infos[] = "DB: " . $dbname . ". Codice Prodotto: " . $cod . ". Non ho trovato corrispondenza di codici (mancano i dettagli). Inserito, ma senza dettagli.";
            //continue;
        }

        $ean = $worksheet->getCell('B' . $row)->getValue();
        if($MSFrameworkDatabase->getCount("SELECT id FROM ecommerce_products WHERE ean = :ean", array(":ean" => $ean)) != 0) {
            $got_infos[] = "DB: " . $dbname . ". L'EAN " . $ean . " esiste già all'interno del DB. Saltato.";
            continue;
        }

        $prezzo = $worksheet->getCell('L' . $row)->getCalculatedValue();
        $prezzo = str_replace(",", ".", $prezzo);
        if(!strstr($prezzo, ".") && $prezzo != "") {
            $prezzo .= ".00";
        }

        if(!is_numeric($prezzo) && $prezzo != "") {
            $got_infos[] = "DB: " . $dbname . ". Prodotto: " . $ean . ". Il prezzo non è nel formato adatto. Saltato.";
            continue;
        }


        $nome = ucfirstSentence($worksheet->getCell('H' . $row)->getValue());

        $slug = (new MSFramework\url())->cleanString($nome);
        if(!(new MSFramework\url())->checkSlugAvailability($slug)) {
            $slug = (new MSFramework\url())->findNextAvailableSlug($slug);
        }

        $new_filename = "";
        $is_active = 0;
        if(file_exists($images_path . $cod . ".jpg")) {
            $is_active = 1;
            $new_filename = time() . "_"  . $cod . ".jpg";
            copy($images_path . $cod . ".jpg", $site_folder_path . "uploads/img/ecommerce/products/" . $new_filename);
            createThumbnail($site_folder_path . "uploads/img/ecommerce/products/", $new_filename, $site_folder_path . "uploads/img/ecommerce/products/tn/", 200);
        }

        if($is_active == 0) {
            $got_infos[] = "DB: " . $dbname . ". Prodotto: " . $ean . ". Il prodotto non ha immagini ed è stato inserito come 'Non in vendita'";
        }

        $imposta = preg_replace("/[^0-9]/", "", $worksheet->getCell('J' . $row)->getValue());
        $imposta = number_format($imposta, 2);

        $cat1 = ucfirstSentence($worksheet->getCell('D' . $row)->getValue());
        $cat2 = ucfirstSentence($worksheet->getCell('F' . $row)->getValue());

        $categoria2 = $MSFrameworkDatabase->getAssoc("SELECT id FROM ecommerce_categories WHERE nome = :nome", array(":nome" => '{"it_IT":"' . $cat1 . '"}'), true);
        if($categoria2['id'] != "") {
            $use_cat_id = $categoria2['id'];
        } else {
            $slug_cat = (new MSFramework\url())->cleanString($cat1);
            if(!(new MSFramework\url())->checkSlugAvailability($slug_cat)) {
                $slug_cat = (new MSFramework\url())->findNextAvailableSlug($slug_cat);
            }

            $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_categories (nome, slug) VALUES (:nome, :slug)", array(":nome" => $MSFrameworki18n->setFieldValue('', $cat1), ":slug" => $MSFrameworki18n->setFieldValue('', $slug_cat))));
            $use_cat_id = $MSFrameworkDatabase->lastInsertId();

            $ecommerce_cats_order['list'][]['id'] = $use_cat_id;
        }

        $parent_cat_id = "";
        $categoria3 = $MSFrameworkDatabase->getAssoc("SELECT id FROM ecommerce_categories WHERE nome = :nome", array(":nome" => '{"it_IT":"' . $cat2 . '"}'), true);
        if ($categoria3['id'] != "") {
            $use_cat_id = $categoria3['id'];
        } else {
            $slug_cat = (new MSFramework\url())->cleanString($cat2);
            if (!(new MSFramework\url())->checkSlugAvailability($slug_cat)) {
                $slug_cat = (new MSFramework\url())->findNextAvailableSlug($slug_cat);
            }

            $parent_cat_id = $use_cat_id;
            $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_categories (nome, slug) VALUES (:nome, :slug)", array(":nome" => $MSFrameworki18n->setFieldValue('', $cat2), ":slug" => $MSFrameworki18n->setFieldValue('', $slug_cat))));
            $use_cat_id = $MSFrameworkDatabase->lastInsertId();

            if ($parent_cat_id != "") {
                foreach ($ecommerce_cats_order['list'] as $cur_listK => $cur_listV) {
                    if ($cur_listV['id'] == $parent_cat_id) {
                        $ecommerce_cats_order['list'][$cur_listK]['children'][]['id'] = $use_cat_id;
                        break;
                    }
                }
            } else {
                $ecommerce_cats_order['list'][]['id'] = $use_cat_id;
            }
        }

        $array_to_save = array(
            "nome" => $MSFrameworki18n->setFieldValue('', $nome),
            "slug" => $MSFrameworki18n->setFieldValue('', $slug),
            "category" => $use_cat_id,
            "is_active" => $is_active,
            "long_descr" => $MSFrameworki18n->setFieldValue('', $prod_det['descrizione']),
            "short_descr" => $MSFrameworki18n->setFieldValue('', $prod_det['descrizione_breve']),
            "gallery" => ($new_filename != "" ? json_encode(array($new_filename)) : ""),
            "prezzo" => number_format($prezzo, 2),
            "imposta" => $imposta,
            "manage_warehouse" => '1',
            "warehouse_qty" => ($worksheet->getCell('O' . $row)->getValue() == '0') ? '0' : '10',
            "ean" => $ean,
        );

        $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO ecommerce_products ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]));
    }

    $array_to_save = array(
        "value" => json_encode(array(
            "list" => $ecommerce_cats_order['list']
        )),
        "type" => "ecommerce_cats_order"
    );

    $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, $db_action);

    if($db_action == "insert") {
        $result = $MSFrameworkDatabase->pushToDB("INSERT INTO cms ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]);
    } else {
        $result = $MSFrameworkDatabase->pushToDB("UPDATE cms SET $stringForDB[1] WHERE type = 'ecommerce_cats_order'", $stringForDB[0]);
    }
}

function ucfirstSentence($input){
    $output = preg_replace_callback('/([.!?])\s*(\w)/', function ($matches) {
        return strtoupper($matches[1] . ' ' . $matches[2]);
    }, ucfirst(strtolower($input)));

    return $output;
}

function createThumbnail($imageDirectory, $imageName, $thumbDirectory, $thumbWidth, $quality = 100) {
    $image_extension = @end(explode(".", $imageName));

    switch($image_extension)
    {
        case "jpg":
            @$srcImg = imagecreatefromjpeg("$imageDirectory/$imageName");
            break;
        case "jpeg":
            @$srcImg = imagecreatefromjpeg("$imageDirectory/$imageName");
            break;
        case "png":
            $srcImg = imagecreatefrompng("$imageDirectory/$imageName");
            break;
    }

    if(!$srcImg) exit;
    $origWidth = imagesx($srcImg);
    $origHeight = imagesy($srcImg);
    $ratio = $origHeight/ $origWidth;
    $thumbHeight = $thumbWidth * $ratio;

    $thumbImg = imagecreatetruecolor($thumbWidth, $thumbHeight);

    if($image_extension == 'png')
    {
        $background = imagecolorallocate($thumbImg, 0, 0, 0);
        imagecolortransparent($thumbImg, $background);
        imagealphablending($thumbImg, false);
        imagesavealpha($thumbImg, true);
    }

    imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origWidth, $origHeight);

    switch($image_extension)
    {
        case "jpg":
            imagejpeg($thumbImg, "$thumbDirectory/$imageName", $quality);
            break;
        case "jpeg":
            imagejpeg($thumbImg, "$thumbDirectory/$imageName", $quality);
            break;
        case "png":
            imagepng($thumbImg, "$thumbDirectory/$imageName");
            break;
    }
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
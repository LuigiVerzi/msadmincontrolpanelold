<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query("DELETE FROM ecommerce_categories"));
    $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query("DELETE FROM ecommerce_products"));
    $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query("DELETE FROM ecommerce_product_questions"));
    $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query("DELETE FROM ecommerce_product_reviews"));
    $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->query("UPDATE cms SET value = '' WHERE type = 'ecommerce_cats_order'"));

    $img_cat_path = realpath(dirname( __FILE__ ) . '/../../../../../../..') . "/" . (new \MSFramework\cms())->getVirtualServerName() . "/uploads/img/ecommerce/categories/";
    $img_prod_path = realpath(dirname( __FILE__ ) . '/../../../../../../..') . "/" . (new \MSFramework\cms())->getVirtualServerName() . "/uploads/img/ecommerce/products/";

    shell_exec("rm -R " . $img_cat_path);
    shell_exec("rm -R " . $img_prod_path);
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("CREATE TABLE `tracking__projects` (
          `id` int(11) NOT NULL,
          `name` text NOT NULL,
          `website` mediumtext NOT NULL,
          `actions` longtext NOT NULL,
          `actions_value` longtext NOT NULL,
          `tracking_codes` longtext NOT NULL,
          `is_active` int(11) NOT NULL,
          `is_protected` int(11) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("CREATE TABLE `tracking__stats` (
          `id` int(11) NOT NULL,
          `project_id` int(11) NOT NULL,
          `user_ref` int(11) NOT NULL,
          `track_event` text NOT NULL,
          `track_ref` longtext NOT NULL,
          `track_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("ALTER TABLE `tracking__projects` ADD PRIMARY KEY (`id`);")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("ALTER TABLE `tracking__stats` ADD PRIMARY KEY (`id`);")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("ALTER TABLE `tracking__projects` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;")
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB("ALTER TABLE `tracking__stats` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;")
    );

    /* COPIA DEI DATI DI TRACKING DAL VECCHIO MODULO */
    $r_producer = $MSFrameworkDatabase->getAssoc("SELECT value FROM cms WHERE type = 'producer_config'", array(), true);
    $ary_data_producer = json_decode($r_producer['value'], true);

    $ganalytics_id = $ary_data_producer['ganalytics_ua'];
    $facebook_id = $ary_data_producer['fbpixel_id'];

    $tracking_codes = json_encode(array(
        'google_analytics_ua' => $ganalytics_id,
        'facebook_pixel_id' => $facebook_id
    ));

    $array_to_save = array(
        "name" => SW_NAME,
        "website" => "",
        "actions" => "[]",
        "actions_value" => "{}",
        "tracking_codes" => $tracking_codes,
        "is_active" => "1"
    );

    $stringForDB = $MSFrameworkDatabase->createStringForDB($array_to_save, 'insert');
    $result = $updater->checkQueryError($MSFrameworkDatabase->pushToDB("INSERT INTO tracking__projects ($stringForDB[1]) VALUES ($stringForDB[2])", $stringForDB[0]));

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
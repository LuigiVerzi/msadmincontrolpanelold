<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    if(!$MSFrameworkDatabase->getCount("SELECT * FROM information_schema.tables WHERE table_schema = '$dbname' AND table_name = 'tracking__references'")) {
        foreach ($MSFrameworkDatabase->getFromDummyByPrefix("tracking__references") as $table) {
           $MSFrameworkDatabase->query("CREATE TABLE `" . $table['TABLE_NAME'] . "` LIKE `" . DUMMY_DB_NAME . "`.`" . $table['TABLE_NAME'] . "`");
        }

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query("ALTER TABLE `tracking__references` ADD PRIMARY KEY (`id`);")
        );

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query("ALTER TABLE `tracking__references` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;")
        );

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query("ALTER TABLE `fatturazione_vendite` ADD `reference` VARCHAR(100) NOT NULL AFTER `sent_date`;")
        );

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "ALTER TABLE `tracking__references` ADD `user_agent` MEDIUMTEXT NOT NULL AFTER `track_references`;"
            )
        );

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB(
                "ALTER TABLE `fatturazione_vendite` ADD `sources` MEDIUMTEXT NOT NULL AFTER `reference`;"
            )
        );

    }

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `pagine_private` ADD `settings` LONGTEXT NOT NULL AFTER `data`;"
        )
    );

    $got_errors[] = $updater->checkQueryError(
        $MSFrameworkDatabase->pushToDB(
            "ALTER TABLE `pagine_private` ADD `titolo` MEDIUMTEXT NOT NULL AFTER `data`, ADD `sottotitolo` MEDIUMTEXT NOT NULL AFTER `titolo`, ADD `slug` MEDIUMTEXT NOT NULL AFTER `sottotitolo`;"
        )
    );
}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
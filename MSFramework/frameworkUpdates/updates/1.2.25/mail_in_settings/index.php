<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    $old = $MSFrameworkDatabase->getAssoc("SELECT * FROM cms WHERE type = 'producer_config'", array(), true);
    $old_value = json_decode($old['value'], true);
    $moving = $old_value['destination_address'];

    if($moving != "") {
        unset($old_value['destination_address']);

        $new = $MSFrameworkDatabase->getAssoc("SELECT * FROM cms WHERE type = 'settings'", array(), true);
        $new_value = json_decode($new['value'], true);
        $new_value['destination_address'] = $moving;

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->pushToDB("UPDATE cms SET `value` = :myval WHERE `type` = 'producer_config'", array(":myval" => json_encode($old_value)))
        );

        if($MSFrameworkDatabase->getCount("SELECT `value` FROM cms WHERE `type` = 'settings'") == 0) {
            $got_errors[] = $updater->checkQueryError(
                $MSFrameworkDatabase->pushToDB("INSERT INTO cms (`value`, `type`) VALUES (:myval, 'settings')", array(":myval" => json_encode($new_value)))
            );
        } else {
            $got_errors[] = $updater->checkQueryError(
                $MSFrameworkDatabase->pushToDB("UPDATE cms SET `value` = :myval WHERE `type` = 'settings'", array(":myval" => json_encode($new_value)))
            );
        }

    }

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
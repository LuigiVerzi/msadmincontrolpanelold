<?php
require_once('../../../../../sw-config.php');
require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

foreach($dbs as $dbname) {
    unset($MSFrameworkDatabase);
    $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

    foreach($MSFrameworkDatabase->getAssoc("SELECT id, original_campaign_id FROM newsletter__campagne_inviate") as $r) {
        $r_original = $MSFrameworkDatabase->getAssoc("SELECT send_schedule FROM newsletter__campagne WHERE id = :id", array(":id" => $r['original_campaign_id']), true);

        $got_errors[] = $updater->checkQueryError(
            $MSFrameworkDatabase->query(
                "UPDATE `newsletter__campagne_inviate` SET `send_schedule` = :send_schedule WHERE id = :id", array(":send_schedule" => $r_original['send_schedule'], ":id" => $r['id'])
            )
        );
    }

}

require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
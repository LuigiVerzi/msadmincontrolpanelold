<?php
/**
 * MSAdminControlPanel
 * Date: 11/09/18
 */

$MSSoftwareModules->accessGranted('db_updater', true);

ini_set('memory_limit', '-1');
set_time_limit(0);

$updater = new \MSFramework\Updater\dbUpdater();

//questa è la lista dei DB sui quali applicare le procedure.
foreach($updater->getEnvolvedProcedures()['ready'] as $env_proc => $env_proc_data) {
    if(in_array($_POST['procedure_folder'], $env_proc_data[$_POST['version']])) {
        $dbs[] = $env_proc;
    }
}

//questo array viene riempito di volta in volta con i risultati delle varie query eseguite sul DB
$got_errors = array();

//questo array può essere utilizzato per salvare nei log informazioni utili riguardanti la procedura
$got_infos = array();

//questo array può essere utilizzato per salvare nei log avvisi importanti da controllare (ma non errori gravi) al termine della procedura
$got_warnings = array();
?>

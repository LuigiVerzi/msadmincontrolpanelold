<?php
/**
 * MSAdminControlPanel
 * Date: 20/09/18
 */

$log_path = $updater->getLogPath();

if(count($got_infos) > 0) {
    $updater->writeLogs('info', $got_infos);
}

if(is_file($log_path . "sqlWarnings.txt")) {
    $got_warnings[] = file_get_contents($log_path . "sqlWarnings.txt");
}

echo json_encode(array("got_errors" => $got_errors, "got_infos" => $got_infos, "got_warnings" => $got_warnings));
?>

##Guida all'utilizzo di frameworkUpdater (Aggiornamento DB)
Tramite questo strumento, è possibile eseguire (contemporaneamente su tutti i database) una o più procedure di aggiornamento. Le procedure possono variare da semplici query ad operazioni più complesse.

Lo strumento determinerà automaticamente i DB coinvolti nella procedura utilizzando due informazioni: la versione impostata nella costante `SW_VERSION` presente nel file framework-config, e la versione presente nella tabella `_db_info`di ogni database. Quest'ultima sarà aggiornata automaticamente dal SW alla fine di ogni aggiornamento.

####Creazione della procedura
Una volta impostata la versione del SW all'interno di `SW_VERSION`, è possibile creare la procedura (o le procedure) di aggiornamento per tale versione. Per fare ciò, è necessario creare una nuova cartella all'interno di `/MSAdminControlPanel/MSFramework/frameworkUpdates/updates`. **La cartella deve avere lo stesso nome del valore impostato in `SW_VERSION`**.

All'interno di tale cartella è possibile creare "n" sottocartelle, una per ogni procedura. E' possibile definire a proprio piacimento i nomi di tali sottocartelle. Ogni sottocartella deve contenere un file index.php, che sarà richiamato dal SW per l'esecuzione della procedura. Generalmente, il file index.php, è composto come segue (vedere commenti codice per dettagli):
```php
<?php
    require_once('../../../../../sw-config.php');
    //esegue alcune operazioni preliminari necessarie per il corretto funzionamento della procedura
    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-config.php");

    foreach($dbs as $dbname) { //$dbs contiene un array di database coinvolti. Questo array è creato automaticamente dal SW in base alle informazioni sulla versione.
        //in questo punto viene creata una nuova connessione al DB sul quale sarà applicata la procedura. Viene richiamata una funzione speciale della classe "updater" che si occupa di impostare alcune opzioni extra per la gestione degli errori
        unset($MSFrameworkDatabase);
        $MSFrameworkDatabase = $updater->initNewDBConnection($dbname);

        //L'array $got_errors contiene solo valori booleani (true/false) che saranno utilizzati in seguito per determinare l'eventuale presenza di errori durante l'aggiornamento.
        //La funzione checkQueryError, oltre a restituire un valore booleano adatto per essere inserito in $got_errors, si occupa anche di scrivere in un file (scaricabile a fine procedura) i dettagli dell'errore.
        $got_errors[] = $updater->checkQueryError($MSFrameworkDatabase->pushToDB("UPDATE XYZ set xyz = 1"));
        
        //L'array $got_infos contiene stringhe. E' possibile utilizzare quest'array per salvare nei log informazioni utili al termine della procedura
        $got_infos[] = "Sono stati aggiornati 1254 record.";
        
        //L'array $got_warnings contiene stringhe. E' possibile utilizzare quest'array per salvare nei log avvisi importanti da controllare (ma non errori gravi) al termine della procedura
        $got_warnings[] = "C'è stato un errore di aggiornamento su un DB di sviluppo.";
    }

    //esegue alcune operazioni necessarie al termine dell'aggiornamento
    require(FRAMEWORK_ABSOLUTE_PATH . "/frameworkUpdates/common/updater-exit-proc.php");
?>
```

Una copia di esempio di questo file, è presente nella versione di aggiornamento (fake) "0.0.0"

####Limitare l'esecusione della procedura solo a determinati database
E' possibile limitare l'esecuzione della procedura solo a determinati database creando all'interno della cartella della procedura il file **config.json**. Di seguito è presente una struttura di esempio del file.

```json
{
  "limit_to_functions": ["ecommerce", "hotel"],
  "limit_to_dbs": ["marke833_xyz"],
  "exclude_dbs": ["marke833_abc"],
  "info": "Informazione importante da mostrare prima dell'avvio della procedura."
}
```

* **limit_to_functions**: la procedura sarà applicata solo ai siti per i quali sono state abilitate (dal modulo Framework -> Impostazioni) le funzioni specificate all'interno dell'array
* **limit_to_dbs**: la procedura sarà applicata **solo** ai database indicati nell'array
* **exclude_dbs**: la procedura **non** sarà applicata ai database indicati nell'array

Una copia di esempio di questo file, è presente nella versione di aggiornamento (fake) "0.0.0"

Non è necessario specificare tutte le chiavi dell'array di configurazione, ma è possibile limitarsi ad utilizzare quelle necessarie.

####Note generali

In ambiente di sviluppo e staging (o se la procedura viene avviata da un utente non SuperAdmin), le procedure saranno applicate solo sul database che sta eseguendo il software in quel momento, indipendentemente dallo stato degli altri database.

In ambiente di produzione (ed utilizzando un utente SuperAdmin), le procedure saranno applicate tutte ed indistintamente (senza tenere conto del file config.js) su tutti gli archivi sviluppo e staging (se hanno versione inferiore a quella corrente). Questo è necessario per evitare che gli archivi di sviluppo restino orfani di qualche aggiornamento a causa della disabilitazione temporanea di qualche funzionalità.

In ambiente di produzione (ed utilizzando un utente SuperAdmin), il database "dummy" verrà sempre aggiornato a meno che non sia esplicitamente escluso (utile, ad esempio, quando si creano delle procedure per importare dei dati)
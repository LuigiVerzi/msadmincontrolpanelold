function initProductsSelect2(elements) {
    elements.select2({
        ajax: {
            url: $('#baseElementPathAdmin').val() + "ajax/ecommerce/select2/getProducts.php",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        language: "it",
        placeholder: 'Cerca un prodotto',
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        templateResult: formattaProdotto,
        templateSelection: formattaProdottoSelezionato
    });
}

function formattaProdotto (repo) {
    if (repo.loading) {
        return repo.text;
    }

    var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__avatar'><img src='" + repo.avatar + "' /></div>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'>" + repo.nome + "</div>";

    if (repo.description) {
        markup += "<div class='select2-result-repository__description'>" + repo.descrizione + "</div>";
    }

    return markup;
}

function formattaProdottoSelezionato (repo) {
    if(repo.nome) {

        var variazioni_select = '';
        for (var id in repo.variazioni) {

            var val = repo.variazioni[id];
            var valori_variazioni = '';

            for (var v_id in val.value) {
                var v_nome = val.value[v_id];
                valori_variazioni += '<option value="' + v_id + '">' + v_nome + '</option>';
            }

            variazioni_select += '<div class="attribute"><select class="product_variation form-control" data-id="' + id + '"><option value="">Seleziona ' + val.nome + '</option>' + valori_variazioni + '</select></div>';
        }
        if(variazioni_select != '') {
            $('#contenitore_variazioni').html(variazioni_select);
        }
        else {
            $('#contenitore_variazioni').html('Nessuna variazione disponibile');
        }

        return repo.nome + ' [ID: ' + repo.id + ']';
    }
    else {
        return repo.text;
    }
}
function openGranularSettings(what, index, role_id) {
    if(typeof(index) == "undefined") index = 0;
    if(typeof(role_id) == "undefined") role_id = 0;

    $.ajax({
        url: $('#baseElementPathAdmin').val() + "ajax/menuSetter/html/settings_popup.php?module_id=" + what + "&abb_id=" + $('.abb_id:eq(' + index + ')').val() + "&role_id=" + role_id + "&loadedModuleID=" + $('#loadedModuleID').val(), //&abb_id viene usato solo nei SaaS, &role_id viene usato solo nei non-SaaS (modulo configurazioni produttore)
        dataType: "html",
        method: "POST",
        data: {
            "on_the_fly_data" : $('.abb_menu_granular:eq(' + index + ')').val()
        },
        success: function(data) {
            if($('.modal:visible').length != 0) {
                $('.modal.bootbox.in').toggleClass('dimmedModal');
            }

            var dialog = bootbox.dialog({
                title: 'Permessi Granulari',
                message: data,
                size: 'large',
                closeButton: false,
                buttons: {
                    cancel: {
                        label: "Annulla",
                        className: 'btn-danger',
                        callback: function(){
                            if($('.modal:visible').length != 0) {
                                $('.modal.bootbox.in').toggleClass('dimmedModal');
                            }
                        }
                    },
                    ok: {
                        label: "Salva",
                        className: 'btn-info',
                        callback: function(){
                            try {
                                granular = JSON.parse($('.abb_menu_granular:eq(' + index + ')').val()) || new Object();
                                if(typeof(granular) !== 'object' || granular.length == 0) {
                                    granular = new Object();
                                }
                            } catch(e) {
                                granular = {}
                            }

                            module_id = $('.modal #module_id').val();
                            if($('#loadedModuleID').val() == "producer_config") {
                                granular[role_id] = new Object();
                                granular[role_id][module_id] = new Object();
                            } else {
                                granular[module_id] = new Object();
                            }

                            $('.modal .granular_field').each(function() {
                                field_id = $(this).attr('id');
                                got_value = "";

                                if($(this).is('input')) {
                                    type = $(this).attr('type');
                                    if(type == "text" || type == "number") {
                                        got_value = $(this).val();
                                    } else if(type == "checkbox") {
                                        got_value = ($(this).is(":checked") ? "1" : "0");
                                    }
                                } else if($(this).is('select')) {
                                    got_value = $(this).val();
                                }

                                if(got_value != "") {
                                    if($('#loadedModuleID').val() == "producer_config") {
                                        granular[role_id][module_id][field_id] = got_value;
                                    } else {
                                        granular[module_id][field_id] = got_value;
                                    }
                                }
                            })

                            $('.abb_menu_granular:eq(' + index + ')').val(JSON.stringify(granular));

                            if($('.modal:visible').length != 0) {
                                $('.modal.bootbox.in').toggleClass('dimmedModal');
                            }
                        }
                    }
                }
            });

            dialog.init(function(){
                initIChecks();
                initMSTooltip();
            })
        }
    })
}
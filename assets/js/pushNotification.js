window.notificationURL = 'https://www.marketingstudio.it/includes/onesignal/subscriptionFrame.php';

if(localStorage.notificationNotSupported) {
    $('.navbar-top-links li.push_notification').remove();
}
else if(localStorage.notificationEnabled == 1) {
    // Aggiunge il pulsante per la cancellazione dal servizio di notifiche push
    $('#pushNotificationStatus').attr('class', 'label label-primary').html('Attivato');
    $('#enablePushNotification').off('click').on('click', function (e) {
        e.preventDefault();
        sendActionToPushIframe(function () {
            initPushNotificationListener();
            sendMessageToNotificationPopup('unsubscribe');
        });
    }).html('Disabilita Notifiche');
} else {

    $('#pushNotificationStatus').attr('class', 'label label-warning').html('Caricamento...');
    $('#enablePushNotification').off('click').on('click', function (e) {
        e.preventDefault();
    }).html('...');

    sendActionToPushIframe(function () {
        initPushNotificationListener();
    });
}

function sendActionToPushIframe(callback) {
    if($('#pushNotificationIframe').length) {
        callback();
    } else {
        $('body').append('<iframe id="pushNotificationIframe" style="display: none;" src="' + window.notificationURL + '"></iframe>');
        $('#pushNotificationIframe').on('load', function () {
           callback();
        });
    }
}

function initPushNotificationListener() {
    window.addEventListener('message', function (e) {

        if(typeof(e.data) === 'undefined') return false;

        if(e.data.indexOf('now_registered') >= 0 || e.data == 'already_registered') {
            localStorage.notificationEnabled = 1;

            // Aggiunge il pulsante per l'iscrizione alle notifiche Push
            $('#pushNotificationStatus').attr('class', 'label label-primary').html('Attivato');
            $('#enablePushNotification').off('click').on('click', function (e) {
                e.preventDefault();
                sendMessageToNotificationPopup('unsubscribe');
            }).html('Disabilita Notifiche');

            if(e.data.indexOf('now_registered') >= 0) {
                var userId = e.data.split("|")[1];

                $.ajax({
                    url: $('#baseElementPathAdmin').val() + "ajax/pushNotification/saveTag.php",
                    type: "POST",
                    data: {
                        "id": userId,
                    },
                    async: false,
                    dataType: "json",
                    success: function (data) {
                    }
                });
            }

        } else if(e.data == 'not_registered') {
            localStorage.notificationEnabled = 0;
            // Aggiunge il pulsante per l'iscrizione alle notifiche Push
            $('#pushNotificationStatus').attr('class', 'label label-danger').html('Non Attivo');
            $('#enablePushNotification').off('click').on('click', function (e) {
                e.preventDefault();
                sendMessageToNotificationPopup('subscribe');
            }).html('Abilita Notifiche');
        } else if(e.data == 'enabled_message') {

            toastr["success"]("Inizierai a ricevere le notifiche su questo dispositivo", "Notifiche Attive", {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "preventDuplicates": true,
                "showDuration": "3000",
                "hideDuration": "3000",
                "timeOut": 3000,
            });
            window.messagePushWindow.close();

        } else if(e.data == 'disabled_message') {

            toastr["success"]("Non riceverai più nessuna notifica su questo dispositivo", "Notifiche Disattivate", {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "preventDuplicates": true,
                "showDuration": "3000",
                "hideDuration": "3000",
                "timeOut": 3000,
            });
            window.messagePushWindow.close();

        } else if(e.data == 'not_supported') {
            localStorage.notificationNotSupported = 1;
            $('.navbar-top-links li.push_notification').remove();
        }

    }, false);
}

function sendMessageToNotificationPopup(type, callback) {
    var stile = "top=10, left=10, width=250, height=200, status=no, menubar=no, toolbar=no scrollbars=no";
    window.messagePushWindow =  window.open(window.notificationURL + '?' + type, "_blank", stile);
}
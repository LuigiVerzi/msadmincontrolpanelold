/**
 *
 * @param selectors string Il selettore dell'elemento sul quale attaccare tinymce
 * @param options array Un array con le preferenze (Vedi default_options)
 * @param custom_params array La lista di parametri da sostituire/aggiungere a tinymce
 */
function initTinyMCE(selectors, options, custom_params, uploads_id) {

    if(typeof(uploads_id) == 'undefined') {
        uploads_id = '';
    }

    var default_shortcodes = {global: Object.keys(JSON.parse($('#MSCommonShortcodes').val()))};

    if(typeof(options) !== 'undefined' && typeof(options.shortcodes) !== 'undefined') {
        if(typeof(options.shortcodes) === 'object') {
            default_shortcodes = options.shortcodes;
        } else {
            default_shortcodes.custom = Object.values(options.shortcodes);
        }
        delete options.shortcodes;
    }

    $(selectors).each(function () {
        var selector = this;

        var default_options = {
            page: false,
            uploader: true,
            small: false,
            autoresize: false,
            shortcodes: default_shortcodes
        };

        if (typeof (options) === 'undefined') {
            options = default_options;
        } else {
            Object.keys(options).forEach(function (key) {
                default_options[key] = options[key];
            });
            options = default_options;
        }

        if (typeof (custom_params) === 'undefined') {
            custom_params = {};
        }

        var tinymceSetup = function (editor) {};
        var filePickerCallback = function (callback, value, meta) {};
        var pages_toolbar = '';

        var previousSetup = tinymceSetup;
        if ($('#usingPrimaryLang').val() === "0") {
            var $translation_icon = $(selector).closest('[class*="col-"]').find('.ms-label-tooltip [data-translations]');
            if($translation_icon.length && typeof($translation_icon.data('translations')) === 'object') {
                tinymceSetup = function (editor) {
                    previousSetup.call(this, editor);
                    addShortcodesBtn(editor, options.shortcodes);
                    addTranslationBtn(editor, $translation_icon.data('translations'));
                };
            }
        } else {
            tinymceSetup = function (editor) {
                previousSetup.call(this, editor);
                addShortcodesBtn(editor, options.shortcodes);
            };
        }

        if (options.uploader) {

            var uploaderIDS = 'tinyUploader_' + Math.random().toString(36).substr(2, 9);
            $('body').append('<input name="image" type="file" id="' + uploaderIDS + '" class="hidden" onchange="">');

            filePickerCallback = function (callback, value, meta) {

                if (meta.filetype == 'image') {
                    $('#' + uploaderIDS).trigger('click');
                    $('#' + uploaderIDS).on('change', function () {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function (e) {

                            callback(e.target.result, {
                                alt: ''
                            });
                        };
                        reader.readAsDataURL(file);
                    });
                }
            };
        }

        if(options.small) {
            var toolbar = 'removeformat | styleselect | bold italic | fontsizeselect | alignleft aligncenter alignright | bullist numlist | shortcodes | forecolor backcolor';
        } else {
            var toolbar = 'undo redo | removeformat | styleselect | bold italic | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | link image |' + pages_toolbar + ' shortcodes | fullscreen | preview | translations';
        }

        var params = {
            target: selector,
            height: 500,
            plugins: "link paste print preview code advlist lists table image media fullscreen preview" + (options.autoresize ? ' autoresize' : ''),
            language: 'it_IT',
            paste_remove_styles: true,
            paste_remove_styles_if_webkit: true,
            paste_word_valid_elements: "div,b,strong,i,em,h1,h2,h3,h5,h6",
            invalid_elements : "form,select,input",
            toolbar: toolbar,
            mobile: {
                theme: 'mobile',
                plugins: [ 'autosave', 'lists', 'autolink' ],
                toolbar: [ 'undo', 'bold', 'italic', 'underline', 'forecolor', 'bullist', 'styleselect' ]
            },
            force_br_newlines: true,
            force_p_newlines: false,
            relative_urls: false,
            remove_script_host: false,
            paste_data_images: false,
            image_advtab: true,
            convert_urls: false,
            verify_html: false,
            fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
            autoresize_bottom_margin: 0,
            file_picker_callback: filePickerCallback,
            setup: tinymceSetup
        };

        if (Object.keys(custom_params).length) {
            Object.keys(custom_params).forEach(function (key) {

                if(typeof(params[key]) === 'function') {

                    var previousFunction = params[key];
                    params[key] = function (editor) {
                        previousFunction.call(this, editor);
                        custom_params[key](editor);
                    };

                } else {
                    params[key] = custom_params[key];
                }
            });
        }

        tinymce.init(params);
    });
}

function addShortcodesBtn(editor, shortcodes){
   var btn_to_return = new Array();

   if(typeof(shortcodes) !== 'object' || !Object.keys(shortcodes).length) return false;

   var type_convert = {custom: 'Specifici', global: 'Globali'};

    Object.keys(shortcodes).forEach(function (tipologia) {
        if(shortcodes[tipologia].length)
        {
            btn_to_return.push({
                type: 'separator',
                text: (typeof(type_convert[tipologia]) !== 'undefined' ? type_convert[tipologia] : tipologia)
            });

            var v = (typeof(shortcodes[tipologia][0]) === 'object' ? shortcodes[tipologia][0] : shortcodes[tipologia]);

            v.forEach(function (shortcode, key) {
                btn_to_return.push({
                    type: 'menuitem',
                    text:  shortcode,
                    onAction: function () {
                        editor.insertContent(shortcode);
                    }
                });
            });
        }
    });

    return editor.ui.registry.addMenuButton('shortcodes', {
        text: 'Shortcodes',
        fetch: function (callback) {
            callback(btn_to_return);
        }
    });
}

function addTranslationBtn(editor, translations){

    btn_to_return = new Array();
    Object.keys(translations).forEach(function (lang_code) {
        btn_to_return_tmp = new Object();

        btn_to_return_tmp['type'] = 'menuitem';
        btn_to_return_tmp['text'] = 'Importa lingua ' + lang_code;
        btn_to_return_tmp['onAction'] = function () {
            editor.insertContent(translations[lang_code]);
        };

        btn_to_return.push(btn_to_return_tmp);
    });

    return editor.ui.registry.addMenuButton('translations', {
        text: 'Traduzioni',
        fetch: function (callback) {
            callback(btn_to_return);
        }
    });

}
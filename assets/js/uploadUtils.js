$(document).ready(function () {
    $('.modal').on('shown.bs.modal', function() {
        $(this).find(".orakUploaderCompressionRange").ionRangeSlider('update');
    });
});

function initOrak(id, max_uploads, min_width, min_height, thumb_size, preattach_array, watermark, allowed_ext, crop_to_width, crop_to_height, crop_thumb_to_width, crop_thumb_to_height) {

    if(!$('#' + id).length) return false;
    initOrakQualityRange();

    if(typeof(crop_to_width) == "undefined") {
        crop_to_width = 0;
    }

    if(typeof(crop_to_height) == "undefined") {
        crop_to_height = 0;
    }

    if(typeof(crop_thumb_to_width) == "undefined") {
        crop_thumb_to_width = 0;
    }

    if(typeof(crop_thumb_to_height) == "undefined") {
        crop_thumb_to_height = 0;
    }
    $('#' + id).orakuploader({
        orakuploader_path : $('#baseElementPathAdmin').val() + 'vendor/plugins/orakuploader/',
        orakuploader_thumbnail_path : $('#baseElementPathTmpFolder').val() + 'tn',
        orakuploader_real_path : $('#' + id + '_realPath').val(),

        orakuploader_add_image : $('#baseElementPathAdmin').val() + 'vendor/plugins/orakuploader/images/add.png',
        orakuploader_add_label : ['Seleziona immagine', 'Seleziona file'],

        orakuploader_thumbnail_size  : thumb_size,
        orakuploader_maximum_uploads : max_uploads,
        orakuploader_use_dragndrop: true,
        orakuploader_hide_on_exceed: true,
        orakuploader_min_width: min_width,
        orakuploader_min_height: min_height,
        orakuploader_attach_images: preattach_array,
        orakuploader_use_sortable: true,
        orakuploader_watermark: watermark,
        orakuploader_allowed_ext: allowed_ext,
        orakuploader_crop_to_width: crop_to_width,
        orakuploader_crop_to_height: crop_to_height,
        orakuploader_crop_thumb_to_width: crop_thumb_to_width,
        orakuploader_crop_thumb_to_height: crop_thumb_to_height,
        orakuploader_picture_deleted: function (data) {
            if(typeof(email_editor_orakuploader_picture_deleted) === 'function' && id === 'emailEditorUploader') {
                email_editor_orakuploader_picture_deleted(data);
            }

            if(typeof(orakuploader_picture_deleted) === 'function') {
                orakuploader_picture_deleted(data);
            }
        },
        orakuploader_finished: function(data) {

            error_descr = "";
            if(data == "customerr_heavy_file") {
                error_descr = "Il file deve pesare massimo 2 MB";
            } else if(data == "customerr_min_size") {
                error_descr = "Il file deve essere di almeno " + min_width + "x" + min_height + " px";
            } else if(data == "customerr_min_size_crop") {
                error_descr = "Le impostazioni del cropping non possono essere inferiori a " + min_width + "x" + min_height + " px";
            } else if(data == "customerr_ext_not_allowed") {
                error_descr = "L'estensione del file non è ammessa";
            }

            if(error_descr != "") {
                bootbox.error('Si è verificato un errore in fase di upload! ' + error_descr);
            }
            else {
                if(typeof(email_editor_orakuploader_finished) === 'function' && id === 'emailEditorUploader') {
                    email_editor_orakuploader_finished(data);
                }

                if(typeof(orakuploader_finished) === 'function') {
                    orakuploader_finished(data);
                }
            }
        },
    });
}

function getOrakImagesToPreattach(id) {
    preattach_array_images = new Array();
    if($('#prev_upl_' + id).val() != "") {

        img_to_cycle = $.parseJSON($('#prev_upl_' + id).val()) || new Array();
        $(img_to_cycle).each(function(k, v) {
            preattach_array_images.push(v);
        })
    }

    return preattach_array_images;
}

function composeOrakImagesToSave(id) {
    images_ary = new Array();
    $('input[name^=' + id + ']').each(function(k, v) {
        images_ary.push($(this).val());
    })

    return images_ary;
}

function initOrakQualityRange() {
    $(".orakUploaderCompressionRange:not(.loaded)").each(function () {
        var $slider = $(this);
        $slider.addClass('loaded');

        $slider.ionRangeSlider({
            type: 'single',
            min: 10,
            max: 100,
            step: 5,
            from: $slider.data('current'),
            postfix: " %",
            onFinish: function(data){
                var value = data.fromNumber;
                $slider.data('current', value);
            }
        });
    });
}
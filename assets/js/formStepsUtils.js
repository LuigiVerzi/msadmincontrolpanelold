function onStepChangingBasic(form_obj, event, currentIndex, newIndex) {
    // Always allow going backward even if the current step contains invalid fields!
    if (currentIndex > newIndex)
    {
        return true;
    }

    var form = form_obj;

    // Clean up if user went backward before
    if (currentIndex < newIndex)
    {
        // To remove error styles
        $(".body:eq(" + newIndex + ") label.error", form).remove();
        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
    }
    // Disable validation on fields that are disabled or hidden.
    form.validate().settings.ignore = ":disabled,:hidden";

    // Start validation; Prevent going forward if false
    return form.valid();
}

function onFinishingBasic(form_obj, event, currentIndex) {
    var form = form_obj;

    // Disable validation on fields that are disabled.
    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
    form.validate().settings.ignore = ":disabled,:hidden";
    // Start validation; Prevent form submission if false
    return form.valid();
}

function onFinishedBasic(form_obj, event, currentIndex) {
    /*var form = form_obj;

    // Submit form input
    form.submit();*/
    bootbox.confirm('Desideri salvare i dati inseriti?', function(result) {
        if(result) {
            $('#editSaveBtn').trigger('click');
        }
    })

}

function onStepChangedModule(form_obj, event, currentIndex, priorIndex) {
    return true; //lasciare questa funzione VUOTA e sovrascriverla eventualmente nel modulo in cui si vuole intercettare il cambio di tab!
}

function onStepChangedBasic(form_obj, event, currentIndex, priorIndex) {
    setTimeout(function() {
        $(window).trigger('stepChanged');

        if($("#form").hasClass('sectionWizard')) {
            if ($("#form").hasClass('sectionWizard') && $("#form").data('state').currentIndex < $("#form").data('state').stepCount - 1 && $("#form").data('state').stepCount > 1) {
                $('#editSaveBtn').text('Continua');
            } else {
                $('#editSaveBtn').text($('#editSaveBtn').data('original_text'));
            }
        }

        onStepChangedModule(form_obj, event, currentIndex, priorIndex);
    }, 50);
}

function initClassicTabsEditForm() {

    if($("#form").hasClass('sectionWizard')) {
        $('#editSaveBtn').data('original_text', $('#editSaveBtn').text()).text('Continua');
    }

    $("#form").steps({
        bodyTag: "fieldset",
        enableFinishButton: false,
        enablePagination: false,
        enableAllSteps: ($("#form").hasClass('sectionWizard') ? false : true),
        titleTemplate: "#title#",
        cssClass: "tabcontrol" + ($("#form").hasClass('sectionWizard') ? ' sectionWizard' : ''),
        enableKeyNavigation: false,
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            return onStepChangedBasic($(this), event, currentIndex, priorIndex);
        },
        onInit: function (event, currentIndex) {
            if($("#form > .steps li").length > 1) {
                refreshMobileTabSwitchers();
                $(window).on('resize', refreshMobileTabSwitchers);
            }
        }
    });

    initTabsEditForm();
}

function initTabsEditForm() {
    $(document).ready(function () {

        var current_module_fields_status = JSON.parse($('#loadedModuleFieldsStatus').val());

        var moduleOptionalFields = getModuleOptionalFields();
        if(moduleOptionalFields) {
            moduleOptionalFields.forEach(function (elements) {
                if ((typeof (current_module_fields_status[elements[1].attr('id')]) !== 'undefined' ? current_module_fields_status[elements[1].attr('id')] : (typeof (elements[0].data('default-view')) !== 'undefined' ? elements[0].data('default-view') : '1')) == "0") {
                    elements[0].closest('[class*="col-"]').hide();
                }
            });
            refreshModuleFieldsState($('#form [data-optional]'));
        }

        if(typeof(initializeModuleFieldsToggler) === 'function') {
            initializeModuleFieldsToggler();
        }

        $('.required:not([name]), .required[name=""]').each(function() {
            $(this).attr('name', $(this).attr('id'))
        })
    });
}

function refreshMobileTabSwitchers() {
    if($(window).width() > 768) {
        $("#form > .steps").show();
        $('#mobileTabsNavigation').remove();
        $('#wrapper').css('margin-bottom', '');
    } else {
        $("#form > .steps").hide();
        $('#mobileTabsNavigation').remove();
        $('#wrapper').css('margin-bottom', '50px');
        var mobileTabSelect = '<div id="mobileTabsNavigation"><span>Scheda:</span> <b class="current"></b><i class="fa fa-exchange" aria-hidden="true"></i><select id="mobileTabselect">';
        $("#form > .steps li:not(.hidden) > a").each(function () {
            $(this).find('span').remove();
            mobileTabSelect += '<option value="' + $(this).attr('id') + '" ' + ($(this).parent().hasClass('current') ? 'selected' : '') + '>' + $(this).text() + '</option>';
        });
        mobileTabSelect += '</select></div>';
        $('body').append(mobileTabSelect);
        $('#mobileTabselect').on('change', function () {
            $('#mobileTabsNavigation .current').text($(this).find('option:selected').text());
            $("#form > .steps #" + $(this).val()).click();
            $('html, body').scrollTop(0);
        });
        $('#mobileTabsNavigation .current').text($('#mobileTabselect option:selected').text());
    }
}

function getModuleOptionalFields() {

    var fields = [];
    if($('#form [data-optional]').length) {
        $('#form fieldset [data-optional]').each(function () {
            if($(this).find('.images_container').length) {
                fields.push([$(this), $(this).find('.images_container input[id]').first()]);
            } else {
                fields.push([$(this), $(this).find('input[id], textarea[id], select[id]').first()]);
            }
        });
    }

    return fields;
}

jQuery.extend(jQuery.validator.messages, {
    required: "Campo obbligatorio",
    remote: "Controlla questo campo",
    email: "Inserisci un indirizzo email valido",
    url: "Inserisci un indirizzo web valido",
    date: "Inserisci una data valida",
    dateISO: "Inserisci una data valida (ISO)",
    number: "Inserisci un numero valido",
    digits: "Inserisci solo numeri",
    creditcard: "Inserisci un numero di carta di credito valido",
    equalTo: "Il valore non corrisponde",
    extension: "Inserisci un valore con un&apos;estensione valida",
    maxlength: $.validator.format("Non inserire pi&ugrave; di {0} caratteri"),
    minlength: $.validator.format("Inserisci almeno {0} caratteri"),
    rangelength: $.validator.format("Inserisci un valore compreso tra {0} e {1} caratteri"),
    range: $.validator.format("Inserisci un valore compreso tra {0} e {1}"),
    max: $.validator.format("Inserisci un valore minore o uguale a {0}"),
    min: $.validator.format("Inserisci un valore maggiore o uguale a {0}"),
    nifES: "Inserisci un NIF valido",
    nieES: "Inserisci un NIE valido",
    cifES: "Inserisci un CIF valido",
    currency: "Inserisci una valuta valida"
});

function refreshModuleFieldsState($closest_col) {

    /* NASCONDE I CAMPI */
    $closest_col.closest('fieldset').find('[class*="col-"]').each(function () {
        if ($(this).find('[data-optional]').length) {

            if ($(this).find('* > [class*="col-"]').filter(function () {
                return ($(this).css('display') !== 'none');
            }).length) {
                $(this).show();
            } else {
                $(this).hide();
            }

        }
    });

    /* ORGANIZZA LE LARGHEZZE DEI CAMPI */
    $closest_col.closest('fieldset').find('.row').each(function () {
        if ($(this).find('[data-optional]').length) {

            var visibleFields = ($(this).find('> [class*="col-"]').filter(function() {
                return ($(this).css('display') !== 'none');
            }).length);

            if (visibleFields === 1) {
                $(this).find('> [class*="col-"]').css('width', '100%');
            } else {
                $(this).find('> [class*="col-"]').css('width', '');
            }

            var $prev = $(this).prev();

            if (visibleFields) {
                $(this).show();
                if($prev.length && ($prev.hasClass('hr-line-dashed') || $prev.is('h2'))) {
                    $prev.show();
                }
            } else {
                $(this).hide();
                if($prev.length && ($prev.hasClass('hr-line-dashed') || $prev.is('h2'))) {
                    $prev.hide();
                }
            }
        }

    });
}
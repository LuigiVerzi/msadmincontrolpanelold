function initializeModuleFieldsToggler() {
    var col_ary = [];
    var current_module_fields_status = JSON.parse($('#loadedModuleFieldsStatus').val());

    var moduleOptionalFields = getModuleOptionalFields();
    if (moduleOptionalFields && moduleOptionalFields.length) {
        moduleOptionalFields.forEach(function (elements) {
            col_ary.push({
                id: elements[1].attr('id'),
                label: elements[0].find('label').first().text(),
                current_state: (typeof (current_module_fields_status[elements[1].attr('id')]) !== 'undefined' ? current_module_fields_status[elements[1].attr('id')] : (typeof (elements[0].data('default-view')) !== 'undefined' ? elements[0].data('default-view') : '1')),
                required: elements[1].hasClass('.required')
            });
        });
        composeGridBuilder(col_ary);
    }
}

function composeGridBuilder(blocks) {

    var html = '<h2>Preferenze ' + $('.page-heading h2').text() + '<i class="fa fa-close"></i></h2>';
    blocks.forEach(function (block) {
        html += '<div class="col-sm-6">\n' +
            '        <div class="styled-checkbox form-control">\n' +
            '            <label style="width: 100%;">\n' +
            '                <div class="checkbox i-checks pull-right" style="margin: 0; padding-top: 0px;">\n' +
            '                    <input type="checkbox" class="toggle_field" data-id="' + block.id + '" data-current-state="' + block.current_state + '" ' + (block.current_state == '1' ? 'checked' : '') + '>\n' +
            '                    <i></i>\n' +
            '                </div>\n' +
            '                <span style="font-weight: normal;">Mostra ' + block.label + '</span>\n' +
            '            </label>\n' +
            '        </div>\n' +
            '    </div>';
    });

    $('.title-action').prepend('<a class="btn btn-default" id="editModuleFields"><i class="fa fa-gear"></i> Preferenze</a>');
    $('body').append('<div id="moduleGridEditor"><div class="row">' + html + '</div><a href="#" class="btn btn-primary" id="submit_form_extra"><i class="fa fa-check"></i> Modifica visualizzazione campi</a></div>');
    initIChecks();


    $('#editModuleFields, #moduleGridEditor h2 i').on('click', function (e) {
        e.preventDefault();

        $('body').toggleClass('openModuleEditor');
        $('#moduleGridEditor').toggle();

        $('#moduleGridEditor .toggle_field').each(function () {
           if( ($(this).prop('checked') ? '1' : '0') !== $(this).data('current-state')) {
               $(this).iCheck(($(this).data('current-state') === 1 ? 'check' : 'uncheck'));
           }
        });

    });

    $('#submit_form_extra').on('click', function (e) {
        e.preventDefault();

        updateModuleFieldsState();

        $('body').toggleClass('openModuleEditor');
        $('#moduleGridEditor').toggle();

    });

    $('#moduleGridEditor').on('ifChanged', '.toggle_field', function () {

        var $element = $('#' + $(this).data('id'));
        var $closest_col = $element.closest('[data-optional]');

        if($(this).prop('checked')) {
            $closest_col.show();
        } else {
            $closest_col.hide();
        }

        refreshModuleFieldsState($closest_col);

    });

}

function updateModuleFieldsState() {
    var fields_status = {};

    $('#moduleGridEditor .toggle_field').each(function () {
        $(this).data('current-state', ($(this).prop('checked') ? '1' : '0'));
        fields_status[$(this).data('id')] = ($(this).prop('checked') ? '1' : '0');
    });

    $.ajax({
        url: $('#baseElementPathAdmin').val() + "ajax/moduleFieldsToggle.php",
        type: "POST",
        data: {
            "module_id": $('#loadedModuleID').val(),
            "fields_status": fields_status,
        },
        async: false,
        dataType: "json",
        success: function (data) {

            console.log(data);

        }
    });

}
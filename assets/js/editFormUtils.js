/*
 * Questa funzione consente di gestire la casistica standard di "Salva" ed "Annulla" per un form
 */
function manageEditButtons() {
    $('#editUndoBtn').on('click', function() {
        if($(this).hasClass('no_check')) {
            window.location.href = 'index.php';
        }
        else {
            bootbox.confirm('Sei sicuro di voler annullare? I dati non salvati andranno persi.', function (result) {
                if (result) {
                    window.location.href = 'index.php';
                }
            });
        }
    });

    $('body').on('click', '#editSaveBtn, #editSaveAsNewBtn', function() {

        var $form = $('#form');

        var form_is_valid = true;
        if($form.length !== 0) {
            form_is_valid = validateAndStyleForm();
        }

        if(form_is_valid) {

            if($form.hasClass('sectionWizard') && $form.data('state').currentIndex < $form.data('state').stepCount-1) {
                $form.steps('next');
                return true;
            }

            save_as_new = false;
            if($(this).attr('id') == "editSaveAsNewBtn") {
                save_as_new = true;
            }

            original_save_value = $('#editSaveBtn').html();

            if($('#editSaveAsNewBtn').length == 1) {
                original_save_as_new_value = $('#editSaveAsNewBtn').html();

                if(save_as_new) {
                    $('#editSaveAsNewBtn').html($('#editSaveAsNewBtn').attr('saving_txt'));
                }

                $('#editSaveAsNewBtn').addClass('disabled').attr('disabled', 'disabled');
            }

            if(!save_as_new) {
                $('#editSaveBtn').html($('#editSaveBtn').attr('saving_txt'));
            }

            $('#editSaveBtn').addClass('disabled').attr('disabled', 'disabled');
            $('#editUndoBtn').addClass('disabled').attr('disabled', 'disabled');

            moduleSaveFunction(save_as_new);

            if($('#editSaveAsNewBtn').length == 1) {
                $('#editSaveAsNewBtn').html(original_save_as_new_value).removeClass('disabled').attr('disabled', false);
            }

            $('#editSaveBtn').html(original_save_value).removeClass('disabled').attr('disabled', false);
            $('#editUndoBtn').removeClass('disabled').attr('disabled', false);
        } else {
            setTimeout(function() {
                bootbox.error("Per poter compiere questa operazione, devi prima compilare tutti i campi obbligatori (*)!");
            }, 50);

            if($form.length != 0) {
                $form.addClass('had_errors')
            }

            return false;
        }
    });

    $('#editDeleteBtn').on('click', function() {
        if($(this).hasClass('no_check')) {
            window.location.href = 'index.php';
        }
        else {
            bootbox.confirm('Sei sicuro di voler eliminare definitivamente il contenuto? I dati andranno persi definitivamente.', function (result) {
                if (result) {
                    proceedWithEditDeletion();
                }
            });
        }
    });

    $(window).bind('keydown', function(event) {
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    moduleSaveFunction();
                    break;
            }
        }
    });

    $('#editSaveBtn').attr('title', 'CTRL + S');
}

function validateAndStyleForm() {
    $('.steps li').removeClass('with_error');
    $('#form').validate({
        ignore: ":disabled,:hidden",
        onfocusout: function() {
            //styleValidationTabs();
            //return true;
        }
    });

    return styleValidationTabs();
}

function styleValidationTabs() {
    var $form = $('#form');

    if($form.hasClass('sectionWizard') && $form.data('state').currentIndex < $form.data('state').stepCount-1) {
        var $show_before_check = $('#form fieldset.current');
        $show_before_check = $show_before_check.add('#form fieldset.current .ibox-content');
        $show_before_check = $show_before_check.add('#form fieldset.current [data-tagsinput-init]');
    } else {
        var $show_before_check = $('#form fieldset');
        $show_before_check = $show_before_check.add('#form .ibox-content');
        $show_before_check = $show_before_check.add('[data-tagsinput-init]');
    }

    $show_before_check.each(function () {
        $(this).data('original-display', $(this).css('display'));
    });

    $show_before_check.not('.current').show().css('opacity', 0).css('position', 'fixed').css('top', 1000);

    $('.steps li').removeClass('with_error');
    $('#form').validate().settings.ignore = ":disabled,:hidden";

    form_is_valid = $('#form').valid();
    if($('#form').find('.required.error:visible').not(':disabled').length) {
        form_is_valid = false;

        if($('#form').find('fieldset').length) {
            $('#form').find('fieldset').each(function () {
                if($(this).find('.required.error:visible').not(':disabled').length) {
                    $('.steps [aria-controls="' + $(this).attr('id') + '"]').parent().addClass('with_error');
                }
            });
        }
    }

    // Richiudo le tab e gli accordion
    $show_before_check.css('opacity', 1).css('position', 'initial').css('top', 'initial');

    $show_before_check.each(function () {
        $(this).css('display', ($(this).data('original-display') !== 'none' ? '' : 'none'));
    });

    if( $('.steps .with_error:visible a').length) {
        $('.steps .with_error:visible a').first().click();
    }

    return form_is_valid;
}

function proceedWithEditDeletion() {
    $.ajax({
        url: "db/deleteData.php",
        type: "POST",
        data: {
            "pID": [$('#record_id').val()],
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "ok") {
                bootbox.alert("I dati sono stati eliminati correttamente.", function () {
                    window.location.href = 'index.php';
                });
            } else {
                bootbox.error("Si è verificato un errore durante l'eliminazione. Probabilmente la pagina non esiste o è già stata eliminata.", function () {
                    window.location.reload();
                });
            }
        }
    })
}

function initSlugChecker(field_title_id, field_slug_id, current_db_table, $current_record_id, parent_id) {

    window.recheckSlug = function () {
        checkSlugAvailability(field_slug_id, current_db_table, $current_record_id, window.current_parent_id);
    };

    if(typeof(parent_id) == 'undefined') {
        parent_id = 0;
    }

    window.current_parent_id = parent_id;

    if(!Array.isArray(field_title_id)) {
        field_title_id = [field_title_id];
    }

    var $slug_field = $('#' + field_slug_id);

    $slug_field.hide().after('<span class="slug_inline_viewer" style="cursor: default; float: none;"><a href="#" class="slug_click_to_edit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fare click per modificare lo slug"><i>' + $('#baseElementPathSW').val().replace($('#baseCurDomain').val(), '/') + ($('#currentLangUrlCode').val().length ? $('#currentLangUrlCode').val() + '/' : '') + '</i><span>' + $slug_field.val() + '</span></a></span>');

    $('.slug_inline_viewer').on('click', function () {
        $(this).hide();
        $(this).parent().find('> *').not('.slug_inline_viewer').show();
        $(field_title_id).each(function (k, field_id) {
            $('#' + field_id).off('change').addClass('manually_set');
        });
        $('#' + field_slug_id).focus();
    });

    $('.slug_inline_viewer').parent().find('> *').hide();
    if($slug_field.val().length > 0) {
        $('.slug_inline_viewer').show();
    }

    $(field_title_id).each(function (k, field_id) {

        if(!$('#' + field_slug_id).val().length) {
            $('#' + field_id).on('change', function () {

                if($('#' + field_slug_id).is(':disabled')) return true;

                var value = '';
                $(field_title_id).each(function (k, field_id) {
                    value += ' ' + $('#' + field_id).val();
                });
                value = $.trim(value);
                $('#' + field_slug_id).val(value).change();

            });
        }

        $('#' + field_slug_id).on('change focusout', function () {

            if($(this).val().length) {
                if ($('.slug_inline_viewer').length) {
                    composeSlug($(this).val(), field_slug_id);
                    $('.slug_inline_viewer').show().parent().find('> *').not('.slug_inline_viewer').hide();
                    $('.slug_inline_viewer').show().find('span').text($(this).val());
                    checkSlugAvailability(field_slug_id, current_db_table, $current_record_id, window.current_parent_id);
                }
            } else {
                $('.slug_inline_viewer').hide().parent().find('> *').not('.slug_inline_viewer').show();
            }

        });
    });

}

function updateSlugParent(slug_id, parent_id, slug, divider, check) {

    if(typeof(divider) == 'undefined') {
        divider = '/';
    }

    if(typeof(check) == 'undefined') {
        check = true;
    }

    window.current_parent_id = parent_id;

    var prepend_slug = $('#baseElementPathSW').val().replace($('#baseCurDomain').val(), '/') + ($('#currentLangUrlCode').val().length ? $('#currentLangUrlCode').val() + '/' : '');
    if(parent_id.length) {
        prepend_slug = prepend_slug + slug + divider;
    }

    var $slug_button = $('#' + slug_id).parent().find('.slug_click_to_edit');

    if($slug_button.length) {
        $slug_button.find('i').text(prepend_slug);
    }

    if($('#' + slug_id).val().length) {
        $('#' + slug_id).change();
    }

    if(check) {
        recheckSlug();
    }
}

function getFullSlug(slug_id) {
    var base = $('#baseElementPathSW').val().replace($('#baseCurDomain').val(), '/');
    var $slug_button = $('#' + slug_id).parent().find('.slug_click_to_edit');
    return $slug_button.text().replace(base, '');
}

function composeSlug(title_val, field_slug_id) {

    var conversion_array = {'á': 'a', 'Á': 'A', 'à': 'a', 'À': 'A', 'ă': 'a', 'Ă': 'A', 'â': 'a', 'Â': 'A', 'å': 'a', 'Å': 'A', 'ã': 'a', 'Ã': 'A', 'ą': 'a', 'Ą': 'A', 'ā': 'a', 'Ā': 'A', 'ä': 'ae', 'Ä': 'AE', 'æ': 'ae', 'Æ': 'AE', 'ḃ': 'b', 'Ḃ': 'B', 'ć': 'c', 'Ć': 'C', 'ĉ': 'c', 'Ĉ': 'C', 'č': 'c', 'Č': 'C', 'ċ': 'c', 'Ċ': 'C', 'ç': 'c', 'Ç': 'C', 'ď': 'd', 'Ď': 'D', 'ḋ': 'd', 'Ḋ': 'D', 'đ': 'd', 'Đ': 'D', 'ð': 'dh', 'Ð': 'Dh', 'é': 'e', 'É': 'E', 'è': 'e', 'È': 'E', 'ĕ': 'e', 'Ĕ': 'E', 'ê': 'e', 'Ê': 'E', 'ě': 'e', 'Ě': 'E', 'ë': 'e', 'Ë': 'E', 'ė': 'e', 'Ė': 'E', 'ę': 'e', 'Ę': 'E', 'ē': 'e', 'Ē': 'E', 'ḟ': 'f', 'Ḟ': 'F', 'ƒ': 'f', 'Ƒ': 'F', 'ğ': 'g', 'Ğ': 'G', 'ĝ': 'g', 'Ĝ': 'G', 'ġ': 'g', 'Ġ': 'G', 'ģ': 'g', 'Ģ': 'G', 'ĥ': 'h', 'Ĥ': 'H', 'ħ': 'h', 'Ħ': 'H', 'í': 'i', 'Í': 'I', 'ì': 'i', 'Ì': 'I', 'î': 'i', 'Î': 'I', 'ï': 'i', 'Ï': 'I', 'ĩ': 'i', 'Ĩ': 'I', 'į': 'i', 'Į': 'I', 'ī': 'i', 'Ī': 'I', 'ĵ': 'j', 'Ĵ': 'J', 'ķ': 'k', 'Ķ': 'K', 'ĺ': 'l', 'Ĺ': 'L', 'ľ': 'l', 'Ľ': 'L', 'ļ': 'l', 'Ļ': 'L', 'ł': 'l', 'Ł': 'L', 'ṁ': 'm', 'Ṁ': 'M', 'ń': 'n', 'Ń': 'N', 'ň': 'n', 'Ň': 'N', 'ñ': 'n', 'Ñ': 'N', 'ņ': 'n', 'Ņ': 'N', 'ó': 'o', 'Ó': 'O', 'ò': 'o', 'Ò': 'O', 'ô': 'o', 'Ô': 'O', 'ő': 'o', 'Ő': 'O', 'õ': 'o', 'Õ': 'O', 'ø': 'oe', 'Ø': 'OE', 'ō': 'o', 'Ō': 'O', 'ơ': 'o', 'Ơ': 'O', 'ö': 'oe', 'Ö': 'OE', 'ṗ': 'p', 'Ṗ': 'P', 'ŕ': 'r', 'Ŕ': 'R', 'ř': 'r', 'Ř': 'R', 'ŗ': 'r', 'Ŗ': 'R', 'ś': 's', 'Ś': 'S', 'ŝ': 's', 'Ŝ': 'S', 'š': 's', 'Š': 'S', 'ṡ': 's', 'Ṡ': 'S', 'ş': 's', 'Ş': 'S', 'ș': 's', 'Ș': 'S', 'ß': 'SS', 'ť': 't', 'Ť': 'T', 'ṫ': 't', 'Ṫ': 'T', 'ţ': 't', 'Ţ': 'T', 'ț': 't', 'Ț': 'T', 'ŧ': 't', 'Ŧ': 'T', 'ú': 'u', 'Ú': 'U', 'ù': 'u', 'Ù': 'U', 'ŭ': 'u', 'Ŭ': 'U', 'û': 'u', 'Û': 'U', 'ů': 'u', 'Ů': 'U', 'ű': 'u', 'Ű': 'U', 'ũ': 'u', 'Ũ': 'U', 'ų': 'u', 'Ų': 'U', 'ū': 'u', 'Ū': 'U', 'ư': 'u', 'Ư': 'U', 'ü': 'ue', 'Ü': 'UE', 'ẃ': 'w', 'Ẃ': 'W', 'ẁ': 'w', 'Ẁ': 'W', 'ŵ': 'w', 'Ŵ': 'W', 'ẅ': 'w', 'Ẅ': 'W', 'ý': 'y', 'Ý': 'Y', 'ỳ': 'y', 'Ỳ': 'Y', 'ŷ': 'y', 'Ŷ': 'Y', 'ÿ': 'y', 'Ÿ': 'Y', 'ź': 'z', 'Ź': 'Z', 'ž': 'z', 'Ž': 'Z', 'ż': 'z', 'Ż': 'Z', 'þ': 'th', 'Þ': 'Th', 'µ': 'u', 'а': 'a', 'А': 'a', 'б': 'b', 'Б': 'b', 'в': 'v', 'В': 'v', 'г': 'g', 'Г': 'g', 'д': 'd', 'Д': 'd', 'е': 'e', 'Е': 'E', 'ё': 'e', 'Ё': 'E', 'ж': 'zh', 'Ж': 'zh', 'з': 'z', 'З': 'z', 'и': 'i', 'И': 'i', 'й': 'j', 'Й': 'j', 'к': 'k', 'К': 'k', 'л': 'l', 'Л': 'l', 'м': 'm', 'М': 'm', 'н': 'n', 'Н': 'n', 'о': 'o', 'О': 'o', 'п': 'p', 'П': 'p', 'р': 'r', 'Р': 'r', 'с': 's', 'С': 's', 'т': 't', 'Т': 't', 'у': 'u', 'У': 'u', 'ф': 'f', 'Ф': 'f', 'х': 'h', 'Х': 'h', 'ц': 'c', 'Ц': 'c', 'ч': 'ch', 'Ч': 'ch', 'ш': 'sh', 'Ш': 'sh', 'щ': 'sch', 'Щ': 'sch', 'ъ': '', 'Ъ': '', 'ы': 'y', 'Ы': 'y', 'ь': '', 'Ь': '', 'э': 'e', 'Э': 'e', 'ю': 'ju', 'Ю': 'ju', 'я': 'ja', 'Я': 'ja'};

    for (var prop in conversion_array) {
        if (conversion_array.hasOwnProperty(prop)) {
            var re = new RegExp(prop, 'g');
            title_val = title_val.replace(re, conversion_array[prop]);
        }
    }

    $('#' + field_slug_id).val(title_val.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, ''));            // Trim - from end of text
}

function checkSlugAvailability(field_slug_id, current_db_table, $current_record_id) {

    if(!$('#' + field_slug_id).val().length) return false;

    $.ajax({
        url: $('#baseElementPathAdmin').val() + "ajax/slug/slugAvailCheck.php",
        async: true,
        type: "POST",
        data: {
            slug: getFullSlug('slug'),
            single_slug: $('#' + field_slug_id).val(),
            db_table: current_db_table,
            record_id: $current_record_id.val()
        },
        dataType: "json",
        success: function(data) {
            if(data.result == false) {
                bootbox.alert('Questo slug non è disponibile. Lo slug verrà modificato aggiungendo un numero per renderlo univoco!');
                $('#' + field_slug_id).val(data.next).change();
            }
        }
    })
}

function succesModuleSaveFunctionCallback(ajax_data, custom_message, custom_title, custom_redirect) {

    var form_data = {};
    $('#form').serializeArray().forEach(function (val) {
        form_data[val.name] = val.value;
    });

    if($('html').hasClass('fromFastEditor')) {
        parent.postMessage(JSON.parse(JSON.stringify({
            cmd: "onFastModuleSave",
            result: Object.assign({form_data: form_data}, ajax_data),
        })), "*");
    }

    var id = ajax_data.id;
    id = (typeof(id) == "undefined" ? "" : id);
    force_reload = (typeof(ajax_data.force_reload) == "undefined" ? false : ajax_data.force_reload);

    custom_message = (typeof(custom_message) == "undefined" || !custom_message.length ? 'Dati salvati con successo.' : custom_message) + ($('#record_id').length && $('#exitAfterSave').val() != "1" && !force_reload ? " <br /><br /> Fai click qui per tornare indietro." : "");
    custom_title = (typeof(custom_title) == "undefined" || !custom_title.length ? 'Dati Salvati' : custom_title);
    custom_redirect = (typeof(custom_redirect) == "undefined" ? 'index.php' : custom_redirect);

    if($('#exitAfterSave').val() != "1" && !force_reload) {
        toastr["success"](custom_message, custom_title, {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": function() {
                if(typeof(custom_redirect) == 'function') custom_redirect();
                else window.location.href = custom_redirect;
            },
            "showDuration": "3000",
            "hideDuration": "3000",
            "timeOut": 3000,
        });

        if(id != "") {
            $('#record_id').val(id);

            if(location.href.indexOf('id=') < 0) {
                var new_url = location.href + (location.href.indexOf('?') < 0 ? '?' : '&') + 'id=' + id;
            } else {
                var url = new URL(location.href);
                var new_url = location.href.replace('id=' + url.searchParams.get("id"), 'id=' + id);
            }

            window.history.pushState("","", new_url);

            if($('#addElementToPage').length) {
                $('#addElementToPage').attr('href', $('#addElementToPage').attr('href') + id).show();
            }
        }
    } else {
        if(force_reload && $('#exitAfterSave').val() != "1") {
            custom_message += "<br /> Per completare le modifiche, la pagina sarà aggiornata.";
        }

        bootbox.alert(custom_message, function() {
            if(typeof(custom_redirect) == 'function') custom_redirect();
            else window.location.href = custom_redirect;
        });
    }

    if($('#moduleParserTag').length && !$('#moduleParserTagBtn').length && $('#record_id').length && $('#record_id').val().length) {
        appendFrontendTagsBtn();
    }

}

function globalInitForm() {
    manageEditButtons();
    initForm();

    $('form').attr('autocomplete', 'off');
    $('form input').each(function () {
       $(this).attr('autocomplete', 'new-password').attr('autofill', 'off');
    });

    if(location.href.indexOf('copy=') != -1) {
        var url = new URL(location.href);
        var new_url = location.href.replace('id=' + url.searchParams.get("id"), 'id=').replace('&copy=1', '');

        if($('#slug').length) {
            $('#slug').val($('#slug').val() + '-1');
            $('.slug_click_to_edit span').text($('#slug').val());
        }

        window.history.pushState("","", new_url);
        $('#record_id').val('copy');
    }

    // Imposto il placeholder con il valore della lingua principale se ci troviamo in una lingua secondaria
    if($('#usingPrimaryLang').val() === "0") {

        var primaryLangCode = $('#primaryLangCode').val();
        var $translatable_inputs = [];
        $('.ms-label-tooltip [data-translations]').each(function () {
            var translations = $(this).data('translations');

            var original_text = '';
            if(typeof(translations) === 'object' && typeof(translations[primaryLangCode]) !== 'undefined') {
                original_text = translations[primaryLangCode];
            }

            var $destination_input = $(this).closest('[class*="col-"]').find('.form-control');
            if(original_text.length) {
                $destination_input
                    .attr('placeholder', 'Originale: ' + original_text).addClass('translation-placeholder')
                    .attr('data-toggle', 'tooltip').attr('title', '<b>Originale:</b>' + original_text + '<a class="apply_original_translation" data-original="' + original_text + '">Usa traduzione principale</a>').attr('rel', 'translationTooltip');
            }

            $destination_input.each(function () {
                $translatable_inputs.push($(this)[0]);
            });
        });

        // Inizializzo i tooltip della traduzione
        $('[rel="translationTooltip"]').tooltip({
            template: '<div class="tooltip translationTooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
            trigger: 'focus',
            html: true,
            placement: 'bottom'
        });

        $('body').on('click', '.translationTooltip .apply_original_translation', function () {
            var original = $(this).data('original');
            $(this).closest('[class*="col-"]').find('.form-control').val(original).change();
        });

        /* Disabilito tutti i campi e i select non traducibili (Modalità Traduzione) */
        $('#form').find('.form-control, .orakUploaderContainer, [type="checkbox"], .irs').not($translatable_inputs)
            .prop('disabled', true).trigger("chosen:updated")
            .parent()
            .addClass('disableContent');

        $('.easyRowDuplicationAddBtn').prop('disabled', true).addClass('disableContent');

        $('.disableContent')
            .attr('data-toggle', 'tooltip').attr('rel', 'translationDisabledTooltip')
            .on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();
            });

        // Inizializzo i tooltip dei campi non traducibili
        $('[rel="translationDisabledTooltip"]').tooltip({
            template: '<div class="tooltip translationTooltip translationDisabledTooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
            trigger: 'hover',
            title: '<b>Modalità Traduzione:</b>Non è possibile modificare questo campo nella modalità traduzione. Per modificarlo passa alla lingua principale.',
            html: true,
            placement: 'bottom'
        });
    }

    // Intercetto tutte le chiamate di salvataggio per eseguire funzioni common prima di quelle specifiche
    $( document ).ajaxSuccess(function( event, xhr, settings ) {
        if (settings.url.indexOf("saveData.php") > 0) {
            if(xhr.responseJSON.status == "input_error") {
                $('#' + xhr.responseJSON.input).parent().find('label.error').remove();
                $('#' + xhr.responseJSON.input).addClass('error').parent().append('<label id="' + xhr.responseJSON.input + '-error" class="error" for="' + xhr.responseJSON.input + '">' + xhr.responseJSON.message + '</label>');
                $('#' + xhr.responseJSON.input).on('change ifToggled', function () {
                    $(this).removeClass('error').parent().find('label.error').remove();
                });

                bootbox.error("Alcuni campi presentano degli errori, per favore controlla di aver inserito tutto correttamente.");
            }
        }
    });

    $(document).ready(function () {
        prepareFormLabels($('form'));

        /* CREO IL PULSANTE AGGIUNGI/MODIFICA IN UNA PAGINA */
        if($('#loadedModuleRelatedPage').length && $('#loadedModuleRelatedPage').val().length) {
            var text = 'Aggiungi in una pagina';
            var link = $('#baseElementPathAdmin').val() + 'modules/pages/list/edit.php?relation=' + $('#loadedModuleRelatedPage').val() + '&relation_id=';

            var is_visible = false;
            if ($('#loadedModuleRelatedPageID').val().length) {
                text = 'Modifica nella pagina';
                link = $('#baseElementPathAdmin').val() + 'modules/pages/list/edit.php?id=' + $('#loadedModuleRelatedPageID').val();
                is_visible = true;
            } else if($('#record_id').val().length) {
                is_visible = true;
                link = link + $('#record_id').val();
            }

            $('.title-action').prepend('<a class="btn btn-default" style="margin: 0 3px 0 0px; display: ' + (is_visible ? 'inline-block': 'none') + ';" target="_blank" href="' + link + '" id="addElementToPage"><i class="fa fa-file"></i> ' + text + '</a>');
        }

        if($('#moduleParserTag').length && $('#record_id').length && $('#record_id').val().length) {
            appendFrontendTagsBtn();
        }

        if($('html').hasClass('fromFastEditor')) {

            /* CREO LA COMUNICAZIONE CON LA PAGINA GENITORE PER LA CHIAMATA fastModuleEditor */
            var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
            var eventer = window[eventMethod];
            var messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";

            eventer(messageEvent, function (event) {
                if (event.data.cmd === 'save') {
                    $('#editSaveBtn').click();
                }
            }, true);

            $(document).on("shown.bs.modal", function (event) {
                if($(event.target).hasClass('bootbox-alert')) {
                    parent.postMessage(JSON.parse(JSON.stringify({
                        cmd: "fastModuleBootbox",
                        data: $(event.target).find('.bootbox-body').text()
                    })), "*");

                    $(event.target).modal('hide');
                }
            });

        }

    });

    initMSShortcodeBtns();
}

function initMSShortcodeBtns() {

    if(typeof(window.msInputShortcodeInitialized) === 'undefined') {
        $('body').on('click', '.msInputShortcodeBtn', function (e) {
            e.preventDefault();
            window.currentFocusedInput = $(this).parent().find('.msShort');
            $('#inputShortcodesModal').modal('toggle');
        });

        $('#inputShortcodesModal .msInsertBtn').on('click', function (e) {
            e.preventDefault();
            $('#inputShortcodesModal').modal('toggle');

            var shortcode = $(this).data('shortcode');
            window.currentFocusedInput.val(window.currentFocusedInput.val() + shortcode);
        });

        $('#inputShortcodesModal .shortCodeCopy').on('click', function (e) {
            e.preventDefault();
            $('#inputShortcodesModal').modal('toggle');

            var $copyInput = $(this).closest('[class^="col-"]').find('.msShort');
            $copyInput.show();
            $copyInput[0].select();
            document.execCommand("copy");
            $copyInput.hide();

            toastr['info']('Shortcode copiato negli appunti');
        });
    }

    window.msInputShortcodeInitialized = true;

    /* Aggiunge il pulsante per attaccare gli shortcodes */
    $('.msShort:not(.msShortInitialized)').each(function () {
        var $container = $(this).parent();
        var top = 1;
        if($container.find('> label').length) {
            top += $container.find('> label').height() + 10;
        }
        $container.css('position', 'relative').append('<a href="#" title="Inserisci valore dinamico" class="msInputShortcodeBtn" style="top: ' + top + 'px; display: none;"><i class="fa fa-code"></i></a>')
    }).on('focusin', function () {
        $(this).parent().find('.msInputShortcodeBtn').fadeIn();
    }).on('focusout', function () {
        $(this).parent().find('.msInputShortcodeBtn').fadeOut();
    }).addClass('msShortInitialized');
}

function prepareFormLabels($parent) {
    $parent.find('[class*="col-"] > label').each(function() {
        if(!$(this).hasClass('form-label')) {
            $(this).html('<span>' + $(this).html() + '</span>').addClass('form-label');
            $(this).parent().find(' > span.ms-label-tooltip').appendTo($(this));
        }
    });
}

function appendFrontendTagsBtn() {
    $('.title-action').prepend('<a class="btn btn-default" id="moduleParserTagBtn" style="margin-right: 3px;" onclick="$(\'#moduleParserTag\').modal(\'show\');"><i class="fa fa-code"></i> Tag</a>');

    $('#moduleParserTag .frontendShortcodeMirror').each(function () {
        var $frontEndTagTextarea = $(this);

        $frontEndTagTextarea.val( $frontEndTagTextarea.val().replace(/id="[0-9]{0,}"/g, 'id="' + $('#record_id').val() + '"'));

        if($frontEndTagTextarea.length) {
            $frontEndTagTextarea.val($.trim($frontEndTagTextarea.val()));
            CodeMirror.fromTextArea($frontEndTagTextarea[0], {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true,
                readOnly: true,
                mode: 'text/html',
            });
        }
    });

    $("#moduleParserTag").on('shown.bs.modal', function (e) {
        $('#moduleParserTag .CodeMirror').each(function(i, el) {
            el.CodeMirror.refresh();
            el.CodeMirror.autoFormatRange({line:0, ch:0}, {line: el.CodeMirror.lineCount()});
        });
    });
}
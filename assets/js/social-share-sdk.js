window.fbAsyncInit = function() {
    initFBApp();
    FB.AppEvents.logPageView();

    $('body').on('click', '.dataTableSocialShare', function() {
        sel_id = $('#' + window.grid_table_id + ' tbody').find('tr.selected').attr('id');
        publishOnSocialPopup(sel_id);
    })
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v3.0&appId=1897811476961567&autoLogAppEvents=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function initFBApp() {
    FB.init({
        appId      : '1897811476961567',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.0'
    });
}

function publishOnSocialPopup(sel_id) {
    if(typeof(sel_id) == "undefined" || sel_id == "") {
        bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
        return false;
    }

    $.ajax({
        url: $('#baseElementPathAdmin').val() + "ajax/socialSharer/html/publish_popup.php",
        method: "POST",
        data: {
            module_id: $('#loadedModuleID').val(),
            record_id: sel_id,
        },
        success: function(data) {
            if(data == "must_select_row") {
                bootbox.error("Non è stato possibile avviare il processo di condivisione. Impossibile continuare");
                return false;
            } else if(data == "unable_to_share") {
                bootbox.error("L'elemento selezionato non è condivisibile, assicurarsi che sia visibile sul sito (attivo) e disponibile al pubblico.");
                return false;
            }

            var dialog = bootbox.dialog({
                message: data,
                title: "Condividi elemento",
                buttons: {
                    publish: {
                        label: "Condividi",
                        className: 'btn-info',
                        callback: function(){
                            window.errorRecap = [];
                            window.successRecap = [];

                            window.facebookDone = false;
                            window.instagramDone = false;

                            window.shareOnFacebookCheck = $('#share_on_fb:checked').length;
                            window.shareOnInstagramCheck = $('#share_on_instagram:checked').length;

                            window.modal_pictures = $('.modal-dialog .picture_uploaded');
                            window.modal_message = $('.modal-dialog #fb_message').val();
                            window.modal_include_images = $('.modal-dialog #fb_add_images:checked').length;
                            window.modal_include_short_link = $('.modal-dialog #fb_add_link:checked').length;
                            
                            dialog.modal('hide');
                            startSharing();
                        }
                    },
                    close: {
                        label: "Annulla",
                        className: 'btn-danger'
                    }
                }
            });

            dialog.init(function(){
                $(dialog).find('.modal-header.modal-success').remove();
                $(dialog).find('.modal-header').css('padding-bottom', '0');
                $(dialog).find('.modal-body').css('padding-top', '0');
                $(dialog).find('.modal-body').removeClass('text-center');
                $(dialog).find('.modal-title').css('font-size', '17px');

                initIChecks();
                initMSTooltip();

                $('.show_el_url').on('click', function() {
                    $(this).hide();
                    $('.el_url').show();
                })

                $('#fb_add_images').on('ifToggled', function(event){
                    if($(this).is(':checked')){
                        $('#fb_images_preview').show();

                        if(!$('#share_on_instagram').closest('.form-control').hasClass('keep_locked')) {
                            $('#share_on_instagram').closest('.form-control').removeClass('ms-label-tooltip');
                            $('#share_on_instagram').iCheck('enable');
                        }
                    } else {
                        $('#fb_images_preview').hide();

                        $('#share_on_instagram').iCheck('uncheck').iCheck('disable');
                        $('#share_on_instagram').closest('.form-control').addClass('ms-label-tooltip');
                    }

                    $('#share_on_instagram').iCheck('update');
                })
            });
        }
    });
}

function showRecapModal() {
    window.recapModalDismissed = false;

    var modal_str = "";

    if(window.errorRecap.length != 0) {
        modal_str += "<b>Sono stati rilevati i seguenti errori:</b> <br /><br /> - " + window.errorRecap.join("<br /> - ");
        modal_str += "<br /><br />";
    }

    if(window.successRecap != "") {
        modal_str += "<b>La pubblicazione sui seguenti social è andata a buon fine:</b> <br /><br /> - " + window.successRecap.join("<br /> - ");
        modal_str += "<br /><br />";
    }

    modal_str = modal_str.substring(0, modal_str.length-12);
    var dialog = bootbox.alert(modal_str, function(){
        window.recapModalDismissed = true;
    });

    dialog.init(function() {
        $(dialog).find('.modal-header.modal-success').remove();
    })
}

function startSharing() {
    if(window.errorRecap != "") {
        if(window.shareOnFacebookCheck == 1 && !window.facebookDone) {
            window.errorRecap.push('La condivisione su Facebook è stata interrotta per consentire la risoluzione dei problemi indicati');
        }

        if(window.shareOnInstagramCheck == 1 && !window.instagramDone) {
            window.errorRecap.push('La condivisione su Instagram è stata interrotta per consentire la risoluzione dei problemi indicati');
        }

        showRecapModal();

        return false;
    }

    if(window.shareOnFacebookCheck == 1 && !window.facebookDone) {
        shareOnFacebook();
        return false;
    }

    if(window.shareOnInstagramCheck == 1 && !window.instagramDone) {
        shareInstagram();
        return false;
    }

    showRecapModal();
}

function shareInstagram() {
    window.instagramDone = true;

    bootbox.error('La condivisione su Instagram sarà disponibile a breve');

    startSharing();
}

function shareOnFacebook() {
    window.facebookDone = true;

    var fb_check_dialog = bootbox.dialog({
        message: '<p class="text-center">Mi sto connettendo a Facebook. Attendere...</p>',
        closeButton: false
    });

    FB.getLoginStatus(function (response) {
        fb_check_dialog.modal('hide');

        if (response.status != "connected") {
            FB.login(function (login_response) {
                if (login_response.status == "connected") {
                    shareOnFacebook();
                }
            }, {scope: 'manage_pages,publish_pages'});
        } else {
            setTimeout(function () {
                ary_pictures = new Array();
                var import_done_uploads = 0;

                var fb_publish_dialog = bootbox.dialog({
                    message: '<div id="publishing_on_fb_modal"><p class="text-center">Pubblicazione del post su Facebook. Attendere...</p></div>',
                    closeButton: false
                });

                if (window.modal_include_images == "1") {
                    $('#publishing_on_fb_modal').append('<p class="text-center">Caricamento delle immagini su Facebook in corso (completato <span id="booxbox_img_cur">0</span>/' + window.modal_pictures.length + '). Attendere...</p>');

                    $(window.modal_pictures).each(function () {
                        var pic_name = $(this).attr('src').replace("/tn", "");

                        $.ajax({
                            url: $('#baseElementPathAdmin').val() + "ajax/socialSharer/facebook/uploadPhotosToFB.php",
                            method: "post",
                            async: false,
                            data: {
                                token: response.authResponse.accessToken,
                                message: window.modal_message,
                                picture: pic_name,
                                module_id: $('#loadedModuleID').val(),
                                record_id: sel_id,
                            },
                            success: function (data) {
                                if (data == "can_not_get_page_access_token") {
                                    window.errorRecap.push('C\'è stato un errore in fase di pubblicazione sulla pagina Facebook');
                                } else if (data == "not_allowed") {
                                    window.errorRecap.push('Non sono riuscito ad ottenere l\'ID della pagina di destinazione o la connessione al pannello di controllo non è avvenuta in HTTPS');
                                } else {
                                    import_done_uploads++;
                                    $('.modal-dialog #booxbox_img_cur').html(import_done_uploads);

                                    ary_pictures.push(data);
                                }
                            }
                        })
                    })
                }


                $.ajax({
                    url: $('#baseElementPathAdmin').val() + "ajax/socialSharer/facebook/publishToFBPage.php",
                    method: "post",
                    data: {
                        token: response.authResponse.accessToken,
                        message: window.modal_message,
                        include_short_link: window.modal_include_short_link,
                        pictures: ary_pictures,
                        record_id: sel_id,
                        module_id: $('#loadedModuleID').val(),
                    },
                    success: function (data) {
                        fb_publish_dialog.modal('hide');

                        if (data == "can_not_get_page_access_token" || data == "graph_error" || data == "sdk_error") {
                            FB.api('/me', function (response) {
                                var fb_account_string = "";
                                var response_name = response.name;

                                FB.api('/me/permissions', function (response) {
                                    var declined = [];
                                    for (i = 0; i < response.data.length; i++) {
                                        if (response.data[i].status == 'declined') {
                                            declined.push(response.data[i].permission)
                                        }
                                    }

                                    if (typeof (response_name) == "undefined") {
                                        fb_account_string = "Utilizza il pulsante qui sotto per uscire ed effettuare il login con un account autorizzato a pubblicare sulla pagina!";
                                    } else {
                                        fb_account_string = "Al momento sei connesso su Facebook come <b>" + response_name + "</b>.<br />Se non hai il permesso di pubblicare sulla pagina con questo account";

                                        if (declined.length != 0) {
                                            fb_account_string += " (o non hai concesso le autorizzazioni richieste per poter pubblicare sulla pagina)";
                                        }

                                        fb_account_string += " , utilizza il pulsante qui sotto per uscire ed effettuare nuovamente il login!";
                                    }

                                    window.errorRecap.push('Non sono riuscito ad ottenere i permessi per pubblicare sulla pagina Facebook');
                                    startSharing();

                                    var intervalWaitingForModalDismission = setInterval(function() {
                                        console.log(window.recapModalDismissed);
                                        if(window.recapModalDismissed) {
                                            clearInterval(intervalWaitingForModalDismission);

                                            var fb_status_dialog = bootbox.dialog({
                                                message: "<div style=\"text-align: center; margin: 30px 0 0 0; color: #a94442;\"><i class=\"fa fa-exclamation-circle fa-3x\"></i></div><div class=\"alert alert-danger\" role=\"alert\" style=\"margin: 20px 0 0 0;\"> <span class=\"bootbox-body\">" + fb_account_string + "</span></div>" +
                                                    "<div style='width: 100%; text-align: center; margin-top: 20px;'><div class=\"fb-login-button\" data-max-rows=\"1\" data-size=\"large\" data-button-type=\"continue_with\" data-show-faces=\"false\" data-auto-logout-link=\"true\" data-use-continue-as=\"true\"></div></div>",
                                                buttons: {
                                                    ok: {
                                                        label: "Riprova",
                                                        className: 'btn-info',
                                                        callback: function () {
                                                            var keep_sel_id = sel_id;

                                                            FB.logout(function (response) {
                                                                publishOnSocialPopup(sel_id);
                                                            })

                                                        }
                                                    },
                                                    cancel: {
                                                        label: "Chiudi",
                                                        className: 'btn-danger',
                                                        callback: function () {

                                                        }
                                                    }
                                                }
                                            });

                                            fb_status_dialog.init(function () {
                                                $(fb_status_dialog).find('.modal-header.modal-success').remove();
                                            })

                                            initFBApp();
                                        }
                                    }, 500)
                                });
                            });
                        } else if (data == "not_allowed") {
                            window.errorRecap.push('Non sono riuscito ad ottenere l\'ID della pagina di destinazione o la connessione al pannello di controllo non è avvenuta in HTTPS');
                            startSharing();
                        } else {
                            window.errorRecap.push('Pubblicazione su Facebook effettuata con successo');
                            startSharing();
                        }
                    },
                    error: function () {
                        fb_publish_dialog.modal('hide');

                        window.errorRecap.push('C\'è stato un errore in fase di pubblicazione sulla pagina Facebook');

                        startSharing();
                    }
                })

            }, 500);
        }
    });
}
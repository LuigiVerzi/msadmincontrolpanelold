$(document).ready(function () {
    initIChecks();
    initMSTooltip();
    initSessionLogoutCheck();
    checkMSStudioPopup();

    setTimeout(function () {
        // Se qualche browser riscontra problemi con animate carichiamo con .show()
        try {
            $("#form, .wrapper-content > .row, .title-action").animate({opacity: 1}, 500);
        } catch(e) {
            $("#form, .wrapper-content > .row, .title-action").show();
        }
    }, 100);

    $('#langs-htmlselect').ddslick({
        width: 150,
        onSelected: function(data){
            $.ajax({
                url: $('#baseElementPathAdmin').val() + "ajax/updateLang.php?id=" + data.selectedData.value,
                success: function(data) {
                    if(data == "reload") {
                        location.reload();
                    }
                }
            });
        }
    });

    $('select[multiple]:not(.chosen-select, .dual_select, .select2-hidden-accessible, .no-plugin)').each(function () {
        var data = $(this).data();
        $(this).multiselect({
            columns: (typeof(data.cols) !== 'undefined' ? data.cols : 3),
            texts: {
                placeholder: (typeof(data.placeholder) !== 'undefined' ? data.placeholder : 'Seleziona elementi')
            }
        });
    });

    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });

    if($('#change_menu_view').length) {
        $('#change_menu_view').on('change', function () {
            var view_type = $(this).val();
            if(view_type != '') {
                document.cookie = "menu_view_type=" + view_type + ";domain=." + location.hostname.split('.').reverse()[1] + "." + location.hostname.split('.').reverse()[0] + ";path=/";
                location.reload();
            }
        });
    }

    addRequiredFieldLabel();

    /* MENU FRAMEWORK SWITCH */
    $('.fw360_menu_switch').on('click', function (e) {
        e.preventDefault();

        var $button = $(this);
        $('.fw360_menu_switch').removeClass('active');
        $button.addClass('active');

        $('.nav.side-menu').hide();

        document.cookie = "framework_menu_view=" + $button.data('id') + ";domain=." + location.hostname.split('.').reverse()[1] + "." + location.hostname.split('.').reverse()[0] + ";path=/";
        $('.nav.side-menu.' + $button.data('id') + '_menu').fadeIn();
    });

    if($('.nav.side-menu .active').length) {
        var $current_menu_parent = $('.nav.side-menu .active').closest('.side-menu');
        if($current_menu_parent.css('display') !== 'block') {
            $('.fw360_menu_switch.' + $current_menu_parent.data('id')).click();
        }
    }

    // Attacca i pulsanti delle datatable alla finestra
    if($('.title-action').length && $('.navbar-static-top').length && $(window).width() < 768) {
        $(window).on('scroll', function () {

            var navbarHeight = $('.navbar-static-top').height();
            var headingHeight = $('.page-heading').outerHeight();
            var titleHeight = $('.title-action').outerHeight();

            if($('.page-heading .btn:visible').length && $('html').scrollTop() > (navbarHeight + headingHeight - titleHeight)) {
                if(!$('#wrapper').hasClass('headingScroll')) {
                    $('.navbar-static-top').css('margin-bottom', headingHeight);
                    $('#wrapper').addClass('headingScroll');
                }
            } else {
                $('#wrapper').removeClass('headingScroll');
                $('.navbar-static-top').css('margin-bottom', '');
            }
        });

    }

    if($('html').hasClass('fromFastEditor')) {
        setTimeout(function () {
            $('body').fadeIn();
        }, 1000);

        window.onload = function() {
            $('body').fadeIn();
            parent.postMessage(JSON.parse(JSON.stringify({
                cmd: "fastEditorLoaded"
            })), "*");
        };

    }

    /* Aggiunge automaticamente il prelaoder nelle chiamate ajax che lo richiedono */
    $( document ).ajaxSend(function( event, xhr, settings ) {
        if(typeof(settings.preloader) !== 'undefined' && settings.preloader !== false) {
            if(typeof(settings.preloader) === 'string') {
                startLoadingMode(settings.preloader);
            } else {
                startLoadingMode();
            }
        } else if (settings.url.indexOf("saveData.php") > 0) {
            startLoadingMode('Salvataggio dei dati', 0);
        }
    });

    $( document ).ajaxComplete(function( event, xhr, settings ) {
        if(typeof(settings.preloader) !== 'undefined' && settings.preloader !== false || settings.url.indexOf("saveData.php") > 0) {
            endLoadingMode();
        }
    });

    /* RENDO I BOOTSTRAP SELECTBOX APRIBILI/CHIUDIBILI */
    $(document).on("mouseup", ".bootstrap-select .dropdown-header", function (e) {
        if (e.which !== 1) return false;

        var $optgroup = $(this), optgroup = $optgroup.data("optgroup");

        $optgroup.parent().find("." + optgroup).not($optgroup).toggle();
        $optgroup.toggleClass("closed");

        $optgroup.parent().find('.dropdown-header').not('.closed').not(this).each(function () {
            var $optgroup = $(this), optgroup = $optgroup.data("optgroup");
            $optgroup.parent().find("." + optgroup).not($optgroup).toggle();
            $optgroup.toggleClass("closed");
        });

    });

    $(document).on('show.bs.select', function (e) {

        var $menu = $(e.target).parent().find('.dropdown-menu.open');

        if(!$menu.length) {
            $menu = $('.bs-container.open .dropdown-menu');
        }

        if($menu.find('li[class*="optgroup-"]').length) {
            if(!$menu.find('li[class*="optgroup-"]').not('.divider, .dropdown-header').find('.active').length) {
                $menu.find('li[class*="optgroup-"]').not('.divider, .dropdown-header').hide();
                $menu.find('.dropdown-header').each(function () {
                    var optgroup = ($(this).attr('class').match(/optgroup-([0-9]+)/gm))[0];
                    $(this).addClass('closed').data('optgroup', optgroup);
                });
            }
        }
    });

    $(document).on('focusin', function(e) {
        if ($(e.target).closest(".tox-dialog-wrap").length) {
            e.stopImmediatePropagation();
        }
    });

    /* CONTROLLA I CREDITI */
    $( document ).ajaxComplete(function( event, xhr, settings ) {
        if(typeof(xhr) !== 'undefined' && typeof(xhr.responseJSON) !== 'undefined' && typeof(xhr.responseJSON.status) !== 'undefined') {
            event.stopImmediatePropagation();
            event.stopPropagation();

            if(xhr.responseJSON.status === 'credits_confirmation') {
                bootbox.confirm({
                    title: xhr.responseJSON.title,
                    message: xhr.responseJSON.message,
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> ' + xhr.responseJSON.cancel_label
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> ' + xhr.responseJSON.confirm_label
                        }
                    },
                    callback: function (confirm) {
                        if (confirm) {
                            if (typeof (settings.headers) === 'undefined') settings.headers = {};
                            settings.headers['X-Credits-Confirmed'] = 1;
                            $.ajax(settings);
                        }
                    }
                });
            } else if(xhr.responseJSON.status === 'credits_insufficient') {
                bootbox.confirm({
                    title: xhr.responseJSON.title,
                    message: xhr.responseJSON.message,
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> ' + xhr.responseJSON.cancel_label
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> ' + xhr.responseJSON.confirm_label
                        }
                    },
                    callback: function (confirm) {
                        if (confirm) {
                            location.href = xhr.responseJSON.redirect;
                        }
                    }
                });
            }
        }
    });

});

function initIChecks() {
    $('.i-checks').each(function () {
        if(!$(this).find('.iCheck-helper').length) {
            $(this).iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $(this).parent().find('span a').on("tap click", function( event, data ){
                event.stopPropagation();
                event.preventDefault();
            });

            $(this).on('ifToggled', function () {
                var $input = $(this).find('input');
                if($input.hasClass('error')) {
                    $input.removeClass('error').parent().find('.error').remove();
                }
            });

        }
    });
}

function initMSTooltip() {
    $('[title]:not(.ms-label-tooltip)').tooltip({
        container: "body",
        html: true
    });

    $('.ms-label-tooltip').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body",
        html: true
    });

    $(".ms-label-tooltip .fa-language").unbind('click');
    $(".ms-label-tooltip .fa-language").on('click', function() {

        var data_trans = new Array();

        try {
            data_trans = $.parseJSON($(this).attr('data-translations'));
        } catch (err) {

        }

        if(data_trans.length == 0) {
            bootbox.error("Non è stato possibile recuperare le traduzioni per questo campo (o non ci sono ancora traduzioni impostate!)");
            return false;
        }

        var html = "";

        for(var key in data_trans) {
            value = data_trans[key];
            html += "<div class='row'>";

            html += '<div class="col-sm-12">';
            html += '<label>Traduzione impostata per ' + key + '</label>';
            html += '<div style="max-height: 120px; overflow: auto;">' + value + '</div>';
            html += '</div>';

            html += "</div>";

            html += '<div class="hr-line-dashed"></div>';
        };

        bootbox.alert(html);
    })
}

function initSessionLogoutCheck() {

    window.sessionLogoutInterval = setInterval(function () {
        if( (new Date().getTime()/1000)-$('#sessionStartTime').val() > parseInt($('#sessionMaxLifetime').val())) {
            clearInterval(window.sessionLogoutInterval);
            showLoginPopup();
        }
    }, 10000);

    $( document ).ajaxComplete(function( event, request, settings ) {
        if(request.getResponseHeader("islogin") !== null) {
            showLoginPopup(function () {
                $.ajax(settings);
            });
        }
    });

}

function showLoginPopup(callback) {
    if(typeof(callback) === 'undefined') {
        callback = function () {};
    }
    $.ajax({
        url: $('#baseElementPathAdmin').val() + "ajax/sessionTimeout.php?action=getModal",
        type: 'post',
        dataType: 'text',
        success: function (modal_html) {
            $('body').append(modal_html);

            $('#sessionTimeoutModal').on('hidden.bs.modal', function () {
                $('#sessionTimeoutModal').modal({backdrop: 'static', keyboard: false})
            });

            $('#sessionTimeoutModal').modal({backdrop: 'static', keyboard: false});

            $('#checkSessionLogin').on('click', function (e) {
                e.preventDefault();

                var email = $('#sessionTimeoutEmail').val();
                var password = $('#sessionTimeoutPassword').val();

                $.ajax({
                    url: $('#baseElementPathAdmin').val() + "ajax/sessionTimeout.php?action=checkLogin",
                    type: 'post',
                    data: {
                        email: email,
                        password: password
                    },
                    dataType: 'text',
                    success: function (status) {

                        if(status === "1") {
                            $('#sessionTimeoutModal').off('hidden.bs.modal');
                            $('#sessionTimeoutModal').modal('hide');

                            toastr["success"]("Login effettuato correttamente, la tua sessione è stata ripristinata.", "Accesso Effettuato", {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "preventDuplicates": true,
                                "showDuration": "3000",
                                "hideDuration": "3000",
                                "timeOut": 3000
                            });

                            callback();

                        } else if(status === "saas_superadmin") {
                            window.location.href = $('#baseElementPathAdmin').val() + 'login/saasSelector.php';
                        } else {
                            $('#sessionTimeoutAlert').fadeIn();
                        }

                    }
                });

            });

        }
    });
}

function checkMSStudioPopup() {
    /* NASCOSTO PER ADESSO
    $.ajax({
        url: $('#baseElementPathAdmin').val() + "ajax/checkMessage.php?action=check_popup",
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if(data.length) {
                data.forEach(function (popup) {

                    var html =
                        '<div class="alert alert-' + popup.type + ' alert-dismissable" id="acp_alert_' + popup.id + '">' +
                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' +
                        '<b>' + popup.title + '</b><br>' +
                        '<p>' + popup.body + '</p>' +
                        '</div>';

                    $('body .wrapper-content').prepend(html);

                    $('#acp_alert_' + popup.id).one('click', 'button', function () {
                        $.ajax({
                            url: $('#baseElementPathAdmin').val() + "ajax/checkMessage.php?action=dismiss_popup&id=" + popup.id,
                        });
                    });

                });
            }
        }
    });
    */
}

function addRequiredFieldLabel() {

    $('label .req_input').remove();

    $('input.required, select.required, textarea.required').each(function () {
        var $label = $(this).closest("[class*='col-']").find('label').first();
        if($label.length) {
            if(!$label.find('.req_input').length) {
                $label.html($label.html().replace(/\*$/, ''));
                $label.append('<span class="req_input">*</span>');
            }
        }
    });
}

window.showFwLoaderTimeout = false;
function startLoadingMode(text, show_after) {
    if(typeof(text) === 'undefined') {
        text = 'Caricamento in corso...';
    }

    if(typeof(show_after) === 'undefined') {
        show_after = 0;
    }

    if(!$('#fwLoading').length) {

        if(!window.showFwLoaderTimeout) {
            window.showFwLoaderTimeout = setTimeout(function () {
                $('body').addClass('fwLoading').append('<div id="fwLoading"><div class="circlePreloader"><div></div></div><span class="fwLoadingText">' + text + '</span></div>');
            }, show_after);
        }
    } else {
        $('#fwLoading .loadingText').html(text);
    }
}

function endLoadingMode() {

    if( window.showFwLoaderTimeout !== false) {
        clearTimeout( window.showFwLoaderTimeout);
        window.showFwLoaderTimeout = false;
    }

    if($('body').hasClass('fwLoading')) {
        $('body').removeClass('fwLoading').find('#fwLoading').fadeOut(100, function () {
            $('#fwLoading').remove();
        });
    }
}

/* FAST EDITOR POPUP */
function showFastEditor(module_id, record_id, callback, source) {

    if(typeof(source) === 'undefined') {
        source = '';
    }

    if($("#fastModuleEditorIframe").length) {
        $("#fastModuleEditorIframe").closest('.modal').remove();
    }

    if(typeof(callback) === 'undefined') {
        callback = function(result) {
            $('#fastModuleEditor').modal('toggle');
            toastr['success']('Contenuto aggiornato correttamente');
        };
    }

    window.fastModuleSaveCallback = callback;

    $.ajax({
        url: $('#baseElementPathAdmin').val() + "ajax/fastModuleEditor.php?module=" + module_id + "&record=" + record_id + '&source=' + source,
        type: 'post',
        dataType: 'text',
        success: function (modal_html) {
            $('#fastModuleEditor').remove();
            $('body').append(modal_html);
            $('#fastModuleEditor').modal('show');

            $('#fastModuleEditor_Save').on('click', function (e) {
                $("#fastModuleEditorIframe")[0].contentWindow.postMessage(JSON.parse(JSON.stringify({
                    cmd: "save"
                })), "*");
            });
        }
    });

    if(typeof(window.fastModuleEditorEvents) === 'undefined') {
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";

        window.fastModuleEditorEvents = true;
        eventer(messageEvent, function (event) {
            if (event.data.cmd === 'onFastModuleSave') {
                window.fastModuleSaveCallback(event.data.result);
            } else if (event.data.cmd === 'fastModuleBootbox') {
                toastr['error'](event.data.data);
            } else if (event.data.cmd === 'fastEditorLoaded') {
                $('#fastModuleEditor').find('.fastEditorPreloader').fadeOut(
                    function () {
                        $('#fastModuleEditor').find('iframe').show();
                    }
                );
            }
        }, true);
    }
}

/* FAST PICKER POPUP */
function showFastPicker(module_id, callback, title, source) {

    if(typeof(source) === 'undefined') {
        source = '';
    }

    if(typeof(title) === 'undefined') {
        title = 'elementi';
    }

    if($("#fastModuleEditorIframe").length) {
        $("#fastModuleEditorIframe").closest('.modal').remove();
    }

    window.fastModulePickCallback = callback;

    $.ajax({
        url: $('#baseElementPathAdmin').val() + "ajax/fastModulePicker.php?module=" + module_id + '&title=' + title + '&source=' + source,
        type: 'post',
        dataType: 'text',
        success: function (modal_html) {
            $('#fastModulePicker').remove();
            $('body').append(modal_html);
            $('#fastModulePicker').modal('show');
        }
    });

    if(typeof(window.fastModulePickerEvents) === 'undefined') {
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";

        window.fastModulePickerEvents = true;
        eventer(messageEvent, function (event) {
            if (event.data.cmd === 'onFastModulePick') {
                window.fastModulePickCallback(event.data.row);
            } else if (event.data.cmd === 'fastEditorLoaded') {
                $('#fastModulePicker').find('.fastEditorPreloader').fadeOut(
                    function () {
                        $('#fastModulePicker').find('iframe').show();
                    }
                );
            }
        }, true);
    }
}

function onFastPickerSelect(row) {
    parent.postMessage(JSON.parse(JSON.stringify({
        cmd: "onFastModulePick",
        row: row
    })), "*");
}
function extraFunctionIfChanged() {
    $('.extra_function').on('ifChanged', function (event) {
        var dependencies = $(this).data('dependencies');
        var isChecked = $(this).prop('checked');

        if (typeof (dependencies) != 'undefined') {

            dependencies.forEach(function (val) {
                if (isChecked) {
                    $('#extra_function_' + val).iCheck('check');
                    $('#extra_function_' + val).attr('disabled', true);
                    $('#extra_function_' + val).closest('.styled-checkbox').addClass('dependencies');
                } else {
                    $('#extra_function_' + val).attr('disabled', false);

                    var has_dependencies = false;
                    $('.extra_function:checked').each(function () {
                        var input_dep = $(this).data('dependencies');
                        if (typeof (input_dep) != 'undefined') {
                            if (input_dep.indexOf(val) >= 0) {
                                has_dependencies = true;
                            }
                        }
                    });

                    if (!has_dependencies) {
                        $('#extra_function_' + val).closest('.styled-checkbox').removeClass('dependencies');
                    }
                }
            });
        }
    });
}
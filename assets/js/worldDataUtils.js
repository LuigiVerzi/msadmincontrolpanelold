window.GooglePlacesAddressMandatory = false;

function manageWorldDataSelects() {
    $('#indirizzo').on('blur', function () {
        if(window.GooglePlacesAddressMandatory) {
            if ($('#indirizzo_hidden').val() == "") {
                $('#indirizzo').val('');
            }
        } else {
            $('#indirizzo_hidden').val($('#indirizzo').val().replace($("#comune_sel option:selected").html() + ", ", ""));
            $('#indirizzo_lat, #indirizzo_lng, #street_number_hidden').val('');
        }
    })

    initSelectComune();

    $('#nazione').on('change', function () {
        $('#regione_sel').html('').removeClass('use_me_for_regione');
        $('#provincia_sel').html('').removeClass('use_me_for_provincia');
        $('#comune_sel').html('').removeClass('use_me_for_comune');

        $('#regione_txt').val('').removeClass('use_me_for_regione');
        $('#provincia_txt').val('').removeClass('use_me_for_provincia');
        $('#comune_txt').val('').removeClass('use_me_for_comune');

        if ($(this).val() == "-1") {
            $('#regione_sel').hide().removeClass('use_me_for_regione');
            $('#provincia_sel').hide().removeClass('use_me_for_provincia');
            $('#comune_sel_chosen').hide();
            $('#comune_sel').removeClass('use_me_for_comune');

            $('#regione_txt').show().addClass('use_me_for_regione');
            $('#provincia_txt').show().addClass('use_me_for_provincia');
            $('#comune_txt').show().addClass('use_me_for_comune');

            $("#indirizzo").unbind('keydown');
            $("#indirizzo").val('');
            $("#indirizzo_hidden").val('');
            $('#cap').val('');
            $("#indirizzo_lat").val('');
            $("#indirizzo_lng").val('');
            $("#indirizzo").attr('disabled', false);
        } else {
            $('#regione_sel').show().addClass('use_me_for_regione');
            $('#provincia_sel').show().addClass('use_me_for_provincia');
            $('#comune_sel_chosen').show();
            $('#comune_sel').addClass('use_me_for_comune');

            $('#regione_txt').hide().removeClass('use_me_for_regione');
            $('#provincia_txt').hide().removeClass('use_me_for_provincia');
            $('#comune_txt').hide().removeClass('use_me_for_comune');

            $.ajax({
                url: $('#baseElementPathAdmin').val() + "ajax/worldData/getWorldData.php?id=" + $(this).val() + "&search=regione",
                async: false,
                success: function (data) {
                    $('#regione_sel').html(data);
                    $('#provincia_sel').html('');
                    $('#comune_sel').html('');
                    $("#comune_sel").trigger("chosen:updated");
                    updateAddressStatus()
                }
            })
        }
    })

    $('#regione_sel').on('change', function () {
        $.ajax({
            url: $('#baseElementPathAdmin').val() + "ajax/worldData/getWorldData.php?id=" + $(this).val() + "&search=provincia",
            async: false,
            success: function (data) {
                $('#provincia_sel').html(data);
                $('#comune_sel').html('');
                $("#comune_sel").trigger("chosen:updated");
                updateAddressStatus()
            }
        })
    })

    $('#provincia_sel').on('change', function () {
        $.ajax({
            url: $('#baseElementPathAdmin').val() + "ajax/worldData/getWorldData.php?id=" + $(this).val() + "&search=comune",
            async: false,
            success: function (data) {
                $('#comune_sel').html(data);
                $("#comune_sel").trigger("chosen:updated");
                updateAddressStatus()
            }
        })
    })

    $("#comune_sel").chosen().change(function () {
        $("#indirizzo").val('');
        $("#indirizzo_hidden").val('');
        $('#cap').val('');
        $("#indirizzo_lat").val('');
        $("#indirizzo_lng").val('');
        updateAddressStatus();
    });

    $('#indirizzo_custom').on('ifToggled', function (e) {
       if($(this).is(':checked')) {
        $('#showCustomAddress').show();
           if(!$('#indirizzo_visibile').val().length) {
               $('#indirizzo_visibile').val($('#indirizzo_hidden').val());
           }
       } else {
           $('#showCustomAddress').hide();
           $('#indirizzo_visibile').val('');
       }
    }).trigger('ifToggled');
}

function initSelectComune() {
    var config = {
        '.chosen-select' : {},
    }

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $('#comune_sel_chosen').css('width', '100%');
}

function initGooglePlaces() {
    if($("#indirizzo").length == 0) {
        return false;
    }

    autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('indirizzo')),
        {
            types: ['geocode'],
        }
    );

    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();

        $('#indirizzo_hidden').val(place['name']);
        $("#indirizzo_lat").val(place.geometry.location.lat());
        $("#indirizzo_lng").val(place.geometry.location.lng());

        $(place.address_components).each(function(compK, compV) {
            if(compV['types']['0'] == "postal_code") {
                $('#cap').val(compV['short_name']);
            }
        })
    });

    setAddressKeyDown();
}

function updateAddressStatus() {
    if($("#comune_sel").val() == "" || $("#comune_sel").val() == null) {
        $("#indirizzo").val('');
        $("#indirizzo_hidden").val('');
        $('#cap').val('');
        $("#indirizzo_lat").val('');
        $("#indirizzo_lng").val('');
        $("#indirizzo").attr('disabled', 'disabled');
        return false;
    }

    $("#indirizzo").attr('disabled', false);
    $("#indirizzo").val($("#comune_sel option:selected").html() + ", ");
    setAddressKeyDown();
}

function setAddressKeyDown() {
    $("#indirizzo").keydown(function(event) {
        var localeKeyword = $("#comune_sel option:selected").html() + ", "
        var localeKeywordLen = localeKeyword.length;
        var keyword = $("#indirizzo").val();
        var keywordLen = keyword.length;

        if(keywordLen == localeKeywordLen) {
            var e = event || window.event;
            var key = e.keyCode || e.which;

            if(key == Number(46) || key == Number(8) || key == Number(37)){
                e.preventDefault();
            }

            if(keyword != localeKeyword) {
                $("#indirizzo").val(localeKeyword);
                $("#indirizzo_hidden").val('');
                $('#cap').val('');
            }
        }

        if(!(keyword.includes(localeKeyword))) {
            $("#indirizzo").val(localeKeyword);
            $("#indirizzo_hidden").val('');
            $('#cap').val('');
            $("#indirizzo_lat").val('');
            $("#indirizzo_lng").val('');
        }
    });
}

function getGeoDataAryToSave() {
    return {
        "regione": $('#regione_sel').val(),
        "provincia": $('#provincia_sel').val(),
        "comune": $('#comune_sel').val(),
        "indirizzo": $('#indirizzo_hidden').val(),
        "indirizzo_visibile": ($('#indirizzo_custom:checked').length ? $('#indirizzo_visibile').val() : ''),
        "street_number": $('#street_number_hidden').val(),
        "lat": $('#indirizzo_lat').val(),
        "lng": $('#indirizzo_lng').val(),
        "cap": $('#cap').val(),
    }
}

function geoDataErrorsOnSave(status) {
    if(status == "address_without_city") {
        bootbox.error("Se compili il campo indirizzo, i campi 'Regione', 'Provincia' e 'Comune' sono obbligatori!");
        return false;
    } else if(status == "lat_not_valid") {
        bootbox.error("Il valore del campo 'latitudine' non è valido");
        return false;
    } else if(status == "lng_not_valid") {
        bootbox.error("Il valore del campo 'longitudine' non è valido");
        return false;
    }
}
var linkToPageButton = function (context) {
    var ui = $.summernote.ui;

    var button = ui.buttonGroup([
        ui.button({
            className: 'dropdown-toggle',
            contents: '<span class="fa fa-database"></span> MSTags <span class="caret"></span>',
            tooltip: "Elenco MSTags",
            data: {
                toggle: 'dropdown'
            }
        }),
        ui.dropdown({
            className: 'drop-default summernote-list',
            contents: $('#dynTagList').val(),
            callback: function ($dropdown) {
                $dropdown.find('li').each(function () {
                    $(this).click(function () {
                        context.invoke('editor.insertText', $(this).attr('tag'));
                    });
                });
            }
        })
    ]);

    return button.render();
}
window.activeEmailEditors = {};

function initEmailEditor(id, uploads_id, shortcodes_list) {

    if(typeof(uploads_id) == 'undefined') uploads_id = 'NEWSLETTER';

    if(typeof(shortcodes_list) == 'undefined') {
        shortcodes_list = {global: [], custom: []};
    }

    var loading_url = $('#baseElementPathAdmin').val() + 'vendor/plugins/email-editor/template-load-page.php';
    if($(id).html().length > 10) {
        loading_url = 'data:text/html;base64,' +  btoa(unescape(encodeURIComponent(($(id).html()))));
    }

    var blank_page_url = $('#baseElementPathAdmin').val() + 'vendor/plugins/email-editor/template-blank-page.php';

    //showSettingsLoadTemplate

    var emailID = id.replace('#', '');

    var options = {
        lang: 'it',
        elementJsonUrl: $('#baseElementPathAdmin').val() + 'vendor/plugins/email-editor/elements-1.json',
        langJsonUrl: $('#baseElementPathAdmin').val() + 'vendor/plugins/email-editor/lang-1.json',
        showLoading: false,

        blankPageHtmlUrl: blank_page_url,
        loadPageHtmlUrl: loading_url,

        showContextMenu: true,
        showContextMenu_FontFamily: true,
        showContextMenu_FontSize: true,
        showContextMenu_Bold: true,
        showContextMenu_Italic: true,
        showContextMenu_Underline: true,
        showContextMenu_Strikethrough: true,
        showContextMenu_Hyperlink: true,

        showElementsTab: true,
        showPropertyTab: true,
        showCollapseMenu: true,
        showCollapseMenuinBottom: true,
        showBlankPageButton: true,

        showSettingsBar: true,
        showSettingsPreview: false,
        showSettingsExport: false,
        showSettingsSendMail: true,
        showSettingsSave: false,
        showSettingsLoadTemplate: true,
        showMobileView: true,

        showRowMoveButton: true,
        showRowRemoveButton: true,
        showRowDuplicateButton: true,
        showRowCodeEditorButton: false,

        shortcodesList: shortcodes_list,

        onBeforeChangeImageClick: function(e) {
            e.preventDefault();
            window.active_email_editor = e.delegateTarget;
            $('#emailEditorImagesModal').modal('show');
        },

        onSettingsSendMailButtonClick: function(e) {
            e.preventDefault();

            console.log('click');

            $.ajax({
                url: $('#baseElementPathAdmin').val() + 'ajax/email-editor/SendTemplateToMailSettings.php',
                dataType: "html",
                success: function (data) {
                    bootbox.dialog({
                        message: data,
                        closeButton: false,
                        buttons: {
                            cancel: {
                                label: "Annulla",
                                className: 'btn-danger'
                            },
                            ok: {
                                label: "Invia anteprima",
                                className: 'btn-info',
                                callback: function(){

                                    if($('#test_send_to').val() == "") {
                                        bootbox.error("Per poter compiere questa operazione, devi impostare un destinatario!");
                                        return false;
                                    }

                                    $.ajax({
                                        url: $('#baseElementPathAdmin').val() + 'ajax/email-editor/SendTemplateToMail.php',
                                        method: "POST",
                                        data: {
                                            email: $('#test_send_to').val(),
                                            template_content: window.emailBuilder[emailID].getContentHtml()
                                        },
                                        async: false,
                                        success: function (data) {
                                            if (data == "ok") {
                                                bootbox.alert("La mail è stata inviata con successo all'indirizzo impostato");
                                            } else {
                                                bootbox.error("Questo è davvero imbarazzante! Non posso inviare la tua mail! Contatta un amministratore di sistema per segnalare il problema.");
                                            }
                                        }
                                    });

                                }
                            }
                        }
                    });
                }
            });

        }
    };

    $(id).addClass('emailEditorContainer');

    var _emailBuilder =  $(id).emailBuilder(options);

    _emailBuilder.setAfterLoad(function() {
        setTimeout(function () {
            _emailBuilder.makeSortable();
            _emailBuilder.find('[data-mce-src]').attr('data-mce-src', '');
        }, 2000);
        loadEmailEditorImages(_emailBuilder, uploads_id);
        loadEmailColorSelect();
    });

    _emailBuilder.setBeforeSettingsLoadTemplateButtonClick(function(e) {
        loadEmailTemplates(_emailBuilder);
    });

    if(typeof(window.emailBuilder) === 'undefined') {
        window.emailBuilder = {};
    }

    window.emailBuilder[id.replace('#', '')] = _emailBuilder;

    return _emailBuilder;
}


function loadEmailColorSelect() {
    $.ajax({
        url: $("#baseElementPathAdmin").val() + "ajax/email-editor/colors.php",
        type: "POST",
        dataType: "text",
        success: function(data) {
            $('.defaultColorsSelect').html(data);
        }
    });
}

function loadEmailTemplates(emailBuilder, id) {
    if(typeof(id) == 'undefined') id = '';

    $.ajax({
        url: $("#baseElementPathAdmin").val() + "ajax/email-editor/loadTemplates.php",
        type: "POST",
        data: {id: id},
        dataType: "text",
        success: function(data) {
            if(id == '') {
                $('#popup_load_template .template-list').html(data);

                $('#popup_load_template .import_template_button').on('click', function () {
                    loadEmailTemplates(emailBuilder, $(this).data('id'));
                    $('#popup_load_template').modal('hide');
                });
            }
            else {
                emailBuilder.setHtmlContent(data);
            }
        }
    });
}

function loadEmailEditorImages(emailBuilder, uploads_id) {
    $.ajax({
        url: $("#baseElementPathAdmin").val() + "ajax/email-editor/imageUploader.php?action=uploader&dir=" + uploads_id,
        type: "POST",
        dataType: "text",
        success: function(data) {

            // Se il modal è già presente (perchè magari creato da un'altra istanza) allora non lo ricreo
            if($('#emailEditorImagesModal').length) {
                return true;
            }

            $('body').append(data);
            initOrak('emailEditorUploader', 100, 0, 0, 100, getOrakImagesToPreattach('emailEditorUploader'), false, ['image/jpeg', 'image/png']);

            $('#emailEditorUploader .multibox.file').append('<div class="btn btn-success select_btn">Seleziona</div>');

            $('#emailEditorUploader').on('click', '.multibox.file', function (e) {

                if($(e.target).hasClass('picture_delete')) {
                    return false;
                }

                e.preventDefault();
                var filename = $(this).attr('filename');
                var full_url = $('#emailEditorUploader_realPath').val() + filename;
                if($(window.active_email_editor).find('.element-content.active img').length) {
                    $(window.active_email_editor).find('.element-content.active img').attr('src', full_url);

                    if(tinymce["activeEditor"]) {
                        var $tmp = $('<div>' + tinymce["activeEditor"].getContent() + '</div>');
                        $tmp.find('img').attr('src', full_url);
                        tinymce["activeEditor"].setContent($tmp[0].outerHTML);
                    }

                }
                $('#emailEditorImagesModal').modal('hide');
            });

        }
    });
}

function email_editor_orakuploader_finished(data) {

    $.ajax({

        url: $("#baseElementPathAdmin").val() + "ajax/email-editor/imageUploader.php?action=upload&dir=" + $('#constant_editor_upload').val(),
        type: "POST",
        data: {images: composeOrakImagesToSave('emailEditorUploader')},
        dataType: "json",
        success: function(data) {

            data.forEach(function (image) {
                $('.multibox.file[filename="' + image + '"]').append('<div class="btn btn-success select_btn">Seleziona</div>').find('img').attr('src', $('#emailEditorUploader_realPath').val() + 'tn/' + image);
            });

        }
    });
}

function email_editor_orakuploader_picture_deleted(data) {
    $.ajax({
        url: $("#baseElementPathAdmin").val() + "ajax/email-editor/imageUploader.php?action=delete&dir=" + $('#constant_editor_upload').val(),
        type: "POST",
        data: {id: data},
        dataType: "json"
    });
}

function attachShortcodesToTextarea($textarea, shortcodes) {

    $textarea.css('width', '100%');
    $textarea.highlightTextarea('destroy');
    $textarea.highlightTextarea(
        {
            words: [{
                color: '#ff8178',
                class: 'not_avaialable',
                words: ["((?!(" + (shortcodes['custom'].concat(shortcodes['global']).join('|')).replace(/{/g, '{(').replace(/}/g, ')}') + ")){(?!\\[|\\])(.+?)(.+?)})+"]
            }, {
                color: '#b0ffb9',
                class: 'avaialable',
                words: ["(" + (shortcodes['custom'].join('|')).replace(/{/g, '{(').replace(/}/g, ')}') + ")"]
            }, {
                color: '#00c0ff',
                class: 'global',
                words: ["(" + (shortcodes['global'].join('|')).replace(/{/g, '{(').replace(/}/g, ')}') + ")"]
            }, {
                color: '#ff00b6',
                class: 'array',
                words: ["{[[|\\]](.+?)[[|\\]]}"]
            }]
        }
    );

}
window.dataTablesRef = {};
window.selectedRowsInfo = {};
window.dataTableRightMenu = {};
window.keyDownDeclared = false;

function loadStandardDataTable(table_id, serverSide, overrideOptions, appendButtons) {

    if(!$('#' + table_id).length) return false;

    if(typeof(serverSide) == "undefined") {
        serverSide = false;
    }
    if(typeof(appendButtons) == "undefined") {
        appendButtons = true;
    }
    if(typeof(overrideOptions) == "undefined") {
        overrideOptions = new Array;
    }

    var is_readonly = $('#' + table_id).data('readonly');
    if(is_readonly) {
        appendButtons = false;
    }

    var is_deleteonly = $('#' + table_id).data('deleteonly');

    var aoColumns = new Array;

    if(!$('html').hasClass('fromFastPicker')) {
        aoColumns.push({
            bSortable: false,
            bSearchable: false,
            data: function (row, type, val, meta) {
                if (!row.DT_RowId) {
                    return "Asd";
                }
                return row.DT_RowId;
            }
        });
    } else {
        window.datatablesRows = {};
    }

    var offset = 0;
    $('#' + table_id + ' th').each(function () {
        if($(this).hasClass('is_data')) {
            aoColumns.push({
                data:{
                    display: offset + ".display",
                    sort: offset + ".sort"
                }
            });
        }
        else if($(this).hasClass('have_filter')) {
            aoColumns.push({
                data: offset,
                bSortable: false,
                bSearchable: true
            });
        }
        else aoColumns.push({data: offset});
        offset++;
    });

    if(appendButtons) {
        aoColumns.push({
            "mData": "Name",
            "mRender": function (data, type, row) {
                if (!row.DT_RowId) {
                    return "";
                }

                if($('html').hasClass('fromFastPicker')) {
                    window.datatablesRows[row.DT_RowId] = row;
                    return '<a href="#" class="btn btn-block btn-info fastSelectElement" onclick="onFastPickerSelect(window.datatablesRows[' + row.DT_RowId + ']);" data-id="' + row.DT_RowId + '">Seleziona</a>';
                }

                btns_limit = ((typeof(row.DT_buttonsLimit) != "undefined" && row.DT_buttonsLimit != "") ? row.DT_buttonsLimit : "edit|delete").split("|");

                btns = '<div class="text-right dataTableInnerButtons">';

                if(typeof(row.DT_ExtraBtn) !== 'undefined') {
                    row.DT_ExtraBtn.forEach(function (extraBtn) {

                        if(typeof(extraBtn.childrens) !== 'undefined') {

                            if( extraBtn.childrens.length) {
                                var btnChildrens = '';
                                extraBtn.childrens.forEach(function (extraChildBtn) {
                                    btnChildrens += '<li><a href="javascript:;" title="' + extraChildBtn.label + '" data-label="' + extraChildBtn.label + '"  data-fn="' + extraChildBtn.function + '"  data-icon="' + extraChildBtn.icon + '" data-context="' + (typeof (extraChildBtn.context) !== 'undefined' && extraChildBtn.context ? '1' : '0') + '" class="btn btn-' + extraChildBtn.btn + (typeof (extraChildBtn.full) == 'undefined' ? ' btn-outline' : '') + ' btn-sm ' + extraChildBtn.class + '" onclick="' + extraChildBtn.function + '(\'' + row.DT_RowId + '\');"><i class="fa fa-' + extraChildBtn.icon + '"></i> ' + extraChildBtn.label + '</a></li>';
                                });

                                btns += '<div class="btn-group"><button data-toggle="dropdown" class="btn btn-default dropdown-toggle">' + extraBtn.label + ' <i class="fa fa-angle-down"></i></button><ul class="dropdown-menu">' + btnChildrens + '</ul></div>';
                            }

                        } else {
                            btns += '<a href="javascript:;" title="' + extraBtn.label + '" data-label="' + extraBtn.label + '"  data-fn="' + extraBtn.function + '"  data-icon="' + extraBtn.icon + '" data-context="' + (typeof(extraBtn.context) !== 'undefined' && extraBtn.context ? '1' : '0') + '" class="btn btn-' + extraBtn.btn + (typeof(extraBtn.full) == 'undefined' ? ' btn-outline' : '') + ' btn-sm ' + extraBtn.class + '" onclick="' + extraBtn.function + '(\'' + row.DT_RowId + '\');"><i class="fa fa-' + extraBtn.icon + '"></i></a>';
                        }
                    });
                }

                if(!is_deleteonly) {
                    btns += ($('#dataTableSendTestTemplate_' + table_id).length ? '<a href="javascript:;" onclick="$(this).closest(\'tr\').addClass(\'selected\'); $(\'#dataTableSendTestTemplate_' + table_id + '\').click();" class="btn btn-info btn-sm"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>' : '');
                    edit_url = "edit.php?id=" + row.DT_RowId;

                    if($('#' + table_id).data('saasselector')) {
                        edit_url = "saasSelector.php?id=" + row.DT_RowId;
                    }
                    btns += ($('#dataTableEdit_' + table_id).length ? '<a href="' + edit_url + '" class="btn btn-warning btn-sm"><i class="fa fa-' + (btns_limit.indexOf('read') != -1 ? "eye" : "pencil") + '"></i></a>' : '');
                }

                if(btns_limit.indexOf('delete') != -1 && $('#usingPrimaryLang').val() === "1") {
                    btns += '<a href="javascript:;" class="btn btn-danger btn-sm" onclick="deleteRecord(\'' + row.DT_RowId + '\');"><i class="fa fa-trash-o"></i></a>';
                }

                btns += ($('#dataTableBugSolved_' + table_id).length ? '<a href="javascript:;" class="btn btn-success btn-sm" onclick="markBugAsSolved(\'' + row.DT_RowId + '\', \'' + table_id + '\');"><i class="fa fa-check"></i></a>' : '');

                btns += '</div>';

                return btns;
            }
        });
    }

    if(!$('html').hasClass('fromFastPicker')) {
        $('#' + table_id + ' thead tr').prepend('<th width="15" style="width: 15px;" class="no-sort"></th>');
    }

    var order = [1, "desc"];
    if ($('#' + table_id).find('.default-sort').length) {
        order = [$('#' + table_id).find('.default-sort').index(), $('#' + table_id).find('.default-sort').data('sort')]
    }

    if(appendButtons) {
        $('#' + table_id + ' thead tr').append('<th class="no-sort actions"></th>');
    }

    if($(window).width() < 768) {
        $.fn.DataTable.ext.pager.numbers_length = 3;
    }

    var dataTableOptions = {
        "ajax":  {
            "url": 'ajax/dataTable.php'
        },
        "stateSave": true,
        "stateSaveCallback": function(settings,data) {
            localStorage.setItem( 'DataTables_' + ($('#loadedModuleID').val() + '_' + table_id + '_' + settings.sInstance), JSON.stringify(data) )
        },
        "stateLoadCallback": function(settings) {

            var loadState = false;
            try {
                var split_referrer = document.referrer.split('/');
                var split_location = document.location.href.split('/');
                split_referrer.pop();
                split_location.pop();

                if (split_referrer.toString() === split_location.toString()) {
                    //api.state.clear();
                    loadState = true;
                }
            } catch(e) {}

            if(loadState) {
                var oldState = JSON.parse( localStorage.getItem( 'DataTables_' + ($('#loadedModuleID').val() + '_' + table_id + '_' + settings.sInstance) ) )
                if(oldState && typeof(oldState) === 'object') {
                    delete oldState.checkboxes;
                    return oldState;
                }
            }

            var api = new $.fn.dataTable.Api( settings );
            api.state.clear();
            return {};

        },
        "serverSide": serverSide,
        "processing": true,
        "language": {
            "url": $('#baseElementPathAdmin').val() + "vendor/plugins/dataTables/locales/it.json"
        },
        "aoColumns": aoColumns,
        "order": [order],
        "columnDefs": [{
            'targets': 0,
            "visible": false,
            'checkboxes': {
                'selectRow': true,
                'selectCallback': onRowSelect,
                'selectAllCallback': function(nodes, selected, indeterminate){
                    $('input[type="checkbox"]', nodes).iCheck('update');
                }
            }
        },{
            "targets": 'no-sort',
            "orderable": false
        }],
        'select': {
            'style': 'multi'
        },
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            btns_limit = ((typeof(aData.DT_buttonsLimit) != "undefined" && aData.DT_buttonsLimit != "") ? aData.DT_buttonsLimit : "edit|delete").split("|");
            var cur_row = nRow;
            $(btns_limit).each(function(k, v) {
                $(cur_row).attr(v, '1');
            })
        },
        "fnDrawCallback": function(oSettings) {
            drawCallback(table_id, oSettings);

            window.dataTablesRef[table_id].columns.adjust();
            setTimeout(function () {
                var datatableButtonsWidth = (35*$('#' + table_id + ' tbody .dataTableInnerButtons').first().find('a').length)+5;
                $('#' + table_id + ' thead tr th:last-child').css('width', datatableButtonsWidth);
            }, 1);
        }
    };

    //dataTableOptions
    if($('html').hasClass('fromFastPicker')) {
        delete dataTableOptions['select'];
        delete dataTableOptions['fnDrawCallback'];
        delete dataTableOptions['columnDefs'][0]['checkboxes'];
    }

    for(var k in overrideOptions) {

        if(k == 'ajax') {

            if(overrideOptions[k] !== false) {
                overrideOptions[k] = {
                    "url": overrideOptions[k]
                }
            } else {
                if(!$('html').hasClass('fromFastPicker')) {
                    $('#' + table_id + ' tbody tr').each(function () {
                        $(this).prepend('<td></td>');
                    });
                }
            }

        };

        dataTableOptions[k] = overrideOptions[k];
    }

    window.dataTablesRef[table_id] = $('#' + table_id).DataTable(dataTableOptions);

    /* CREO I LABEL RESPONSIVE */
    var dt_styles = '@media only screen and (max-width: 768px) {';
    var mobileLabelOffset = ($('#' + table_id + ' .dt-checkboxes-cell').length ? 1 : 0);
    $('#' + table_id + ' thead th').each(function () {
        dt_styles += '#' + table_id + ' td:nth-of-type(' + (parseInt($(this).index())+mobileLabelOffset) + '):before { content: "' + ($.trim($(this).text()) ? $.trim($(this).text()) + ':' : '') + '"; }\n';
    });
    dt_styles += '}';

    $('body').append('<style>' + dt_styles + '</style>');

    /* IMPOSTO LA LARGHEZZA DELL'ULTIMA COLONNA */
    if($('#' + table_id + ' tbody .dataTableInnerButtons').length) {
        window.addEventListener("resize", function () {
            var datatableButtonsWidth = (35 * $('#' + table_id + ' tbody .dataTableInnerButtons').first().find('a').length) + 5;
            $('#' + table_id + ' thead tr th:last-child').css('width', datatableButtonsWidth);
        });
    }

    return window.dataTablesRef[table_id];
}

function drawCallback(table_id, oSettings) {
    var is_readonly = $('#' + table_id).data('readonly');

    refreshDatatableButtons(table_id);

    if ($('#' + table_id + ' tr').length < oSettings._iDisplayLength && oSettings._iDisplayStart === 0) {
        $('#' + table_id + '_wrapper .dataTables_paginate').hide();
    } else {
        $('#' + table_id + '_wrapper .dataTables_paginate').show();
    }

    $('#' + table_id +' input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_square-blue'
    });

    if(typeof(window.dataTableRightMenu[table_id]) !== 'undefined') {
        $.contextMenu( 'destroy' );
    }
    if(typeof($('#' + table_id + ' tbody tr').first().attr('id')) !== 'undefined') {

        var extraBtn = getContextMenuExtraBtn(table_id);

        if(!is_readonly) {
            if($('#' + table_id).data('saasselector') != true) {
                window.dataTableRightMenu[table_id] = $.contextMenu({
                    selector: '#' + table_id + ' tbody tr',
                    callback: function (key, options) {
                        switch (key) {
                            case 'delete' :
                                deleteRecord(id);
                                break;
                            case 'delete_selected' :
                                $('#dataTableDelete_' + table_id).click();
                                break;
                            case 'copy' :
                                copyFromRecord(table_id, id);
                                break;
                            case 'copy_new_tab' :
                                window.maiuscIsPressed = true;
                                copyFromRecord(table_id, id);
                                window.maiuscIsPressed = false;
                                break;
                            case 'edit' :
                                editTableRow(table_id);
                                break;
                            case 'edit_new_tab' :
                                window.maiuscIsPressed = true;
                                editTableRow(table_id);
                                window.maiuscIsPressed = false;
                                break;
                            case 'select_all' :
                                multiSelectRow(table_id, $('#' + table_id + ' tr'));
                                break;
                            default:
                                if (typeof (extraBtn[1][key]) !== 'undefined') {
                                    extraBtn[1][key](id, getSelectedRows(table_id));
                                }
                                break
                        }
                        $('.context-menu-list').hide();
                    },
                    events: {
                        preShow: function (el, e) {
                            id = $(el).attr('id');
                            if (!$('#' + table_id + ' tr#' + id).hasClass('selected')) {
                                $('#' + table_id + ' tbody tr.selected:not(.multiselected)').removeClass('selected');
                                $('#' + table_id + ' tr#' + id).addClass('selected');
                            }

                            refreshDatatableButtons(table_id);

                            var selectedRow = getSelectedRows(table_id).length;

                            $('.context-menu-list .rowCount').text($('#' + table_id + ' tbody tr').length);
                            if (selectedRow > 1) {
                                $('.context-menu-list .selectionCount').show().find('span').text(selectedRow);
                            } else {
                                $('.context-menu-list .selectionCount').hide();
                            }

                            e.preventDefault();
                        }
                    },
                    items: Object.assign({}, {
                        "copy": {
                            name: "Copia",
                            icon: "fa-copy",
                            visible: function (key, opt) {
                                return ($('#dataTableNew_' + table_id + ':visible').length || !getSelectedRows(table_id).length);
                            }
                        },
                        "copy_new_tab": {
                            name: "Copia in una nuova scheda",
                            icon: "fa-object-ungroup",
                            visible: function (key, opt) {
                                return ($('#dataTableNew_' + table_id + ':visible').length || !getSelectedRows(table_id).length);
                            }
                        },
                        "sep1": {
                            name: "<hr>",
                            type: 'cm_separator',
                            visible: function (key, opt) {
                                return ($('#dataTableNew_' + table_id + ':visible').length || !getSelectedRows(table_id).length);
                            }
                        },
                        "edit": {
                            name: "Modifica",
                            icon: "fa-edit",
                            visible: function (key, opt) {
                                return $('#dataTableEdit_' + table_id + ':visible').length;
                            }
                        },
                        "edit_new_tab": {
                            name: "Modifica in una nuova scheda",
                            icon: "fa-share-square-o",
                            visible: function (key, opt) {
                                return $('#dataTableEdit_' + table_id + ':visible').length;
                            }
                        },
                        "sep2": {
                            name: "<hr>",
                            type: 'cm_separator',
                            visible: function (key, opt) {
                                return $('#dataTableEdit_' + table_id + ':visible').length;
                            }
                        },
                        "delete": {
                            name: "Elimina",
                            icon: "fa-trash-o",
                            visible: function (key, opt) {
                                return $('#dataTableDelete_' + table_id + ':visible').length;
                            }
                        },
                        "delete_selected": {
                            name: "Elimina selezionati <span class='selectionCount'>(<span></span> righe)</span>",
                            icon: "fa-trash-o",
                            isHtmlName: true,
                            visible: function (key, opt) {
                                return $('#dataTableDelete_' + table_id + ':visible').length && getSelectedRows(table_id).length > 1;
                            }
                        },
                        "sep4": {
                            name: "<hr>",
                            type: 'cm_separator',
                            visible: function (key, opt) {
                                return $('#dataTableDelete_' + table_id + ':visible').length;
                            }
                        },
                        "select_all": {
                            name: "Seleziona tutto (<span class='rowCount'>" + $('#' + table_id + ' tbody tr').length + "</span> righe)",
                            icon: "fa-check-square-o",
                            isHtmlName: true,
                            visible: function (key, opt) {
                                return $('#' + table_id + ' .dt-checkboxes:visible').length;
                            }
                        }
                    }, extraBtn[0])
                });
            }
        }
    }

    // Aggiunge lo stile a tutte le righe selezionate al cambio di pagina (Con il sistema serverSide si perdeva)
    var pagesSelected = $('#' + table_id + ' tr .dt-checkboxes:checked');
    pagesSelected.each(function () {
        $(this).closest('tr').addClass('selected multiselected');
    });

    if(pagesSelected.length > 0) {
        if(pagesSelected.length == $('#' + table_id + ' tr .dt-checkboxes').length) {
            $('#' + table_id + ' .dt-checkboxes-select-all input').prop("checked", true);
        } else {
            $('#' + table_id + ' .dt-checkboxes-select-all input').prop("indeterminate", true);
        }
    }

    $('#' + table_id).trigger('drawCallback');
}

function getContextMenuExtraBtn(table_id) {

    var contextItem = {};
    var contextFn = {};

    if( $('#' + table_id + ' [data-context="1"]').length) {
        contextItem["sepCustom"] = {
            name: "<hr>",
            type: 'cm_separator'
        }
    }

    $('#' + table_id + ' [data-context="1"]').each(function () {
        var fn = $(this).data('fn');

        if(typeof(contextItem[fn]) === 'undefined') {
            var label = $(this).data('label');
            var icon = $(this).data('icon');

            contextItem[fn] = {
                name: label + " <span class='selectionCount'>(<span></span> righe)</span>",
                icon: 'fa-' + icon,
                isHtmlName: true,
                visible: function (key, opt) {
                    return ($('#' + table_id + ' .selected [data-fn="' + fn + '"]').length ? true : false)
                }
            }

            contextFn[fn] = function (single_row, sel_row) {
                window[fn]();
            };
        }
    });

    return [contextItem, contextFn];
}

function copyFromRecord(table_id, from_id) {
    window.triggerCopy = true;
    editTableRow(table_id);
    window.triggerCopy = false;
}

/*
* Questa funzione consente di rendere una tabella selezionabile (al click evidenzia la riga) passando l'ID della tabella come primo parametro.
*/
function allowRowHighlights(table_id) {

    if(!$('#' + table_id).length) return false;

    var is_readonly = $('#' + table_id).data('readonly');
    var is_deleteonly = $('#' + table_id).data('deleteonly');
    var is_multiselect = ($('#' + table_id).data('multiselect') != false ? true : false);

    if(is_readonly) {
        return false;
    }

    dataTablesRef[table_id].column( 0 ).visible( is_multiselect );
    $('#' + table_id + ' thead tr .dt-checkboxes-select-all').css('width', '15px');

    $('#' + table_id + ' tbody').on('click', 'a.btn', function (e) {
        e.stopImmediatePropagation();
    });

    window.cntrlIsPressed = false;
    $(document).keydown(function (event) {
        if (event.which == "17") {
            window.cntrlIsPressed = true;
        }
    });

    $(document).keyup(function (event) {
        if (event.which == "17") {
            window.cntrlIsPressed = false;
        }
    });
    window.keyDownDeclared = true;


    $(window).blur(function() {
        window.cntrlIsPressed = false;
    });

    if(!is_deleteonly) {
        window.maiuscIsPressed = false;

        if(!window.keyDownDeclared) {
            $(document).keydown(function (event) {
                if (event.which == "16") {
                    window.maiuscIsPressed = true;
                    if ($('.dataTableEdit').html().indexOf('nuova') < 0) {
                        $('.dataTableEdit').html($('.dataTableEdit').html() + ' (apri nuova scheda)');
                    }
                    $('.dataTableNew').html('Aggiungi (apri nuova scheda)');
                }
            });

            $(document).keyup(function (event) {
                if (event.which == "16") {
                    window.maiuscIsPressed = false;
                    $('.dataTableEdit').html($('.dataTableEdit').html().replace(' (apri nuova scheda)', ''));
                    $('.dataTableNew').html('Aggiungi');
                }
            });
            window.keyDownDeclared = true;


            $(window).blur(function() {
                window.maiuscIsPressed = false;

                $('.dataTableEdit').html($('.dataTableEdit').html().replace(' (apri nuova scheda)', ''));
                $('.dataTableNew').html('Aggiungi');
            });
        }

    }

    $('#' + table_id + ' tbody').on('dblclick', 'tr', function (e) {

        // Evito l'apertura perchè durante la seleziona capita spesso di cliccare due volte
        if(window.cntrlIsPressed) return false;

        if($(e.target).closest('.dt-checkboxes-cell').length) {
            return true;
        }

        $('#' + table_id + ' tbody').find('tr.selected').removeClass('selected');
        $(this).addClass('selected');

        if($('html').hasClass('fromFastPicker')) {
            onFastPickerSelect(window.datatablesRows[getRowInfo($(this)).id]);
            return true;
        }

        if(typeof(onDataTableEdit) === 'function') {
            var hookResult = onDataTableEdit(getSelectedRows(table_id));
            if(hookResult === false) return false;
        }

        $('#dataTableEdit_' + table_id).trigger('click');
        $('#' + table_id).trigger('dbclick');
    });

    $('#' + table_id + ' tbody').on('click', 'tr', function (e) {

        // Se il click è avvenuto sulla colonna del checkbox allora lascio eseguire le funzioni default
        if($(e.target).closest('.dt-checkboxes-cell').length) {
            $(e.target).closest('.dt-checkboxes-cell').find('input').iCheck('toggle');
            return true;
        }

        // Se il click è avvenuto su un pulsante lascio eseguire le funzioni di default
        if($(e.target).hasClass('.btn') || $(e.target).closest('.btn').length) {
            $(e.target).closest('.dt-checkboxes-cell').find('input').iCheck('toggle');
            return true;
        }

        if($(this).hasClass('selected')) {
            if(window.cntrlIsPressed) {
                multiUnselectRow(table_id, $(this));
            } else {
                unselectAllRow(table_id);
            }
        } else {
            if(window.cntrlIsPressed) {
                var $single_selected_row = $('#' + table_id + ' tbody tr.selected:not(.multiselected)');
                if($single_selected_row.length) {
                    multiSelectRow(table_id, $single_selected_row);
                }
                multiSelectRow(table_id, $(this));
            } else {
                unselectAllRow(table_id);
                $(this).addClass('selected');
            }
        }

        refreshDatatableButtons(table_id);
    });

    // Handle iCheck change event for "select all" control
    $('#' + table_id).on('ifChanged', '.dt-checkboxes-select-all input[type="checkbox"]', function(event){
        var col = dataTablesRef[table_id].column($(this).closest('th'));
        col.checkboxes.select(this.checked);
    });

    // Handle iCheck change event for checkboxes in table body
    $('#' + table_id).on('ifChanged', '.dt-checkboxes', function(event){
        var cell = dataTablesRef[table_id].cell($(this).closest('td'));
        cell.checkboxes.select(this.checked);
    });

    if(!$('#selectedRowCounts').length) {
        $('body').append('<div id="selectedRowCounts" style="display: none;"><h4>Stai selezionando:</h4><p><b>...</b> <span class="rowsTXT">righe</span></p></div>');
    }

}

function onRowSelect(nodes, selected) {
    $('input[type="checkbox"]', nodes).iCheck('update');

    $(nodes).each(function () {
        var $tr = $(this).closest('tr');
        if(selected) {
            selectedRowsInfo[$tr.attr('id')] = getRowInfo($tr);
            $tr.addClass('selected multiselected');
        } else {
            delete selectedRowsInfo[$tr.attr('id')];
            $tr.removeClass('selected multiselected');
        }
    });

    updateRowSelectionPopup(Object.keys(selectedRowsInfo).length);

    refreshDatatableButtons($(nodes).closest('table').attr('id'));
}

/*
 * Questa funzione consente di gestire la casistica standard di "Aggiungi", "Modifica" ed "Elimina" per una dataTable
 */

function manageGridButtons(table_id, $grid_container) {

    if(typeof($grid_container) == 'undefined') {
        $grid_container = $('.page-heading .title-action');
    }

    if(!$('#' + table_id).length) return false;

    var is_readonly = $('#' + table_id).data('readonly');
    var is_deleteonly = $('#' + table_id).data('deleteonly');

    $grid_container.not('.specific').hide();

    if(is_readonly) {
        return false;
    }

    var $tableButtons = $grid_container.not('.specific');

    // Se sono più di uno (pulsanti custom ad esempio) allora li unisco
    if($tableButtons.length > 1) {
        var $specificTableButtons = $tableButtons.first().clone();
        $tableButtons.not(':first').each(function () {
            $specificTableButtons.append($(this).html());
        });
    } else {
        var $specificTableButtons = $grid_container.not('.specific').clone();
    }

    $specificTableButtons.addClass(table_id).addClass('specific').data('table', table_id);
    $specificTableButtons.find('.btn').each(function () {
        var btnId = $(this).attr('id');
        $(this).attr('id', btnId + '_' + table_id).addClass(btnId).data('table', table_id);
    });

    if(is_deleteonly) {
        $specificTableButtons.find('.btn').not('.dataTableDelete').remove();
    }

    window.grid_table_id = table_id;

    $specificTableButtons.on('click', '.dataTableDelete', function() {

        // Ottengo la datatable visibile (perchè alcuni moduli ne utilizzano diverse su tab separati)
        var current_table = $(this).data('table');

        var sel_row = getSelectedRows(current_table);

        var trigger = $('#' + table_id).triggerHandler('dataTableEdit', [sel_row]);
        if(trigger === false) return false;

        if(sel_row.length === 1) {
            deleteRecord(sel_row[0].id);
        } else if(sel_row.length > 0) {
            bootbox.confirm('Sei sicuro di voler eliminare ' + sel_row.length  + ' righe? I dati non saranno recuperabili.', function(result) {
                if(result) {
                    var ids = [];
                    sel_row.forEach(function(row, key) {
                        ids.push(row.id);
                    });
                    proceedWithDeletion(ids, false);
                    unselectAllRow(table_id);
                }
            })

        } else {
            bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
            return false;
        }
    });

    $specificTableButtons.on('click', '.dataTableBugSolved', function() {

        // Ottengo la datatable visibile (perchè alcuni moduli ne utilizzano diverse su tab separati)
        var current_table = $(this).data('table');

        var sel_row = getSelectedRows(current_table);

        var trigger = $('#' + table_id).triggerHandler('dataTableEdit', [sel_row]);
        if(trigger === false) return false;

        var ids = [];
        sel_row.forEach(function(row, key) {
            ids.push(row.id);
        });

        markBugAsSolved(ids, table_id);
    });

    if(!is_deleteonly) {

        $specificTableButtons.on('click', '.dataTableNew', function () {

            var trigger = $('#' + table_id).triggerHandler('dataTableNew', [window.maiuscIsPressed]);
            if(trigger === false) return false;

            openRecordForm('', window.maiuscIsPressed, table_id);
        });

        $specificTableButtons.on('click', '.dataTableEdit', function () {
            var current_table = $(this).data('table');
            editTableRow(current_table);
        });

        $specificTableButtons.on('click', '.dataTableExport a', function () {
            var type = $(this).data('type');
            $('.dataTables_wrapper .dt-buttons .buttons-' + type).click();
        });
    }

    if($('#' + table_id).is(':visible')) {
        $specificTableButtons.show();
    }

    // Mostro e nascondo i pulsanti quando la tabella non è più visibile (essenziale per i moduli multi-tabella)
    $(window).on('stepChanged', function() {
        if($('#' + table_id).is(':visible')) {
            $grid_container.parent().find('.title-action.' + table_id).show();
        } else {
            $grid_container.parent().find('.title-action.' + table_id).hide();
        }
    });

    $specificTableButtons.insertAfter($grid_container.not('.specific').first());


    /* BLOCCO L'ELIMINAZIONE O L'AGGIUNTA NELLE LINGUE SECONDARIE */
    if($('#usingPrimaryLang').val() === "0") {
        $('.dataTableNew, .dataTableDelete').addClass('disableContent')
            .attr('data-toggle', 'tooltip').attr('rel', 'translationDisabledButtonsTooltip')
            .on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

                return false;
            }).tooltip({
            template: '<div class="tooltip translationTooltip translationDisabledTooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
            trigger: 'hover',
            title: '<b>Modalità Traduzione:</b>Non è possibile aggiunge o eliminare nuovi elementi durante la modalità traduzione.',
            html: true,
            placement: 'bottom'
        });
    }

}

function markBugAsSolved(ids, table_id) {
    if(typeof(table_id) == "undefined") {
        table_id = "";
    }

    if(!Array.isArray(ids)) {
        ids = [ids];
    }

    if(ids.length === 0) {
        bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
        return false;
    } else {
        el_length = ids.length;
        bootbox.confirm('Sei sicuro di voler contrassegnare ' + el_length  + ' element' + (el_length == "1" ? "o" : "i") + ' come risolt' + (el_length == "1" ? "o" : "i") + '?', function(result) {
            if(result) {
                $.ajax({
                    url: "db/markAsSolved.php",
                    type: "POST",
                    data: {
                        "pID": ids,
                    },
                    async: false,
                    dataType: "json",
                    success: function(data) {
                        if(data.status == "ok") {
                            if(table_id != "") {
                                $('.dataTable').DataTable().ajax.reload(null, false);
                                unselectAllRow(table_id);
                            } else {
                                window.location.href = 'index.php';
                            }
                        } else if(data.status == "query_error") {
                            bootbox.error("Questo è davvero imbarazzante! Non posso eliminare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
                        } else if(data.status == "protected") {
                            bootbox.error("Questo contenuto è protetto. Non è possibile eliminarlo.");
                        } else if(data.status == "not_allowed") {
                            bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
                        }
                    }
                })
            }
        })
    }
}

function editTableRow(table_id) {

    var selectedRows = getSelectedRows(table_id);

    var trigger = $('#' + table_id).triggerHandler('dataTableEdit', [selectedRows]);
    if(trigger === false) return false;

    if(selectedRows.length === 1 && selectedRows[0].id) {
        openRecordForm(selectedRows[0].id, window.maiuscIsPressed, table_id);
    } else if(selectedRows.length > 1) {
        bootbox.error("Non puoi effettuare questa operazione se stai selezionando più di 1 riga!");
        return false;
    } else {
        bootbox.error("Per poter compiere questa operazione, devi prima selezionare una riga!");
        return false;
    }

}

/*
 * Questa funzione porta l'utente alla pagina del form per il modulo corrente. Il parametro "sel_id" verrà sempre passato alla pagina PHP, che si occuperà di determinare se il caso in questione è un'aggiunta di un nuovo record o una modifica
 */
function openRecordForm(sel_id, new_tab, table_id) {
    if(typeof(new_tab) === 'undefined') new_tab = false;
    if(typeof(window.triggerCopy) === 'undefined') window.triggerCopy = false;

    edit_url = "edit.php?id=" + sel_id;
    if($('#' + table_id).data('saasselector')) {
        edit_url = "saasSelector.php?id=" + sel_id;
    }

    if(new_tab) {
        var win = window.open(edit_url + (window.triggerCopy ? "&copy=1" : ""), '_blank');
        win.focus();
    } else {
        window.location.href = edit_url + (window.triggerCopy ? "&copy=1" : "");
    }
}

/*
 * Questa funzione chiama una pagina (via ajax) che elimina il record dal DB (dopo aver chiesto conferma all'utente)
 */
function deleteRecord(sel_id, reload_index) {
    if(typeof(reload_index) == "undefined") {
        reload_index = false;
    }

    append_string_delete = "";
    if($('.table_ecommerce_attributi').length != 0) {
        append_string_delete = " <br /> <br /> Proseguendo, eliminerai questo attributo ed eventuali variazioni associate anche dai prodotti che lo utilizzano.";
    } else if($('.table_framework360_customers').length != 0) {
        append_string_delete = " <br /> <br /> Proseguendo, eliminerai anche il relativo dominio, i files e il database.";
    } else if($('.table_framework360_saas_env').length != 0) {
        append_string_delete = " <br /> <br /> Proseguendo, eliminerai anche i files ed il database di questo ambiente.";
    } else if($('.table_framework360_saas_config').length != 0) {
        append_string_delete = " <br /> <br /> Proseguendo, eliminerai anche tutti gli ambienti collegati ed i relativi files/database.";
    }

    bootbox.confirm('Sei sicuro di voler eliminare? I dati non saranno recuperabili.' + append_string_delete, function(result) {
        if(result) {
            proceedWithDeletion(sel_id, reload_index);
        }
    })
}

function proceedWithDeletion(sel_id, reload_index, reload_datatable) {
    activateDeletingStatus();

    if(typeof(reload_datatable) == 'undefined') {
        reload_datatable = true;
    }

    if(typeof(sel_id) !== 'array' && typeof(sel_id) !== 'object') {
        sel_id = [sel_id];
    }

    $.ajax({
        url: "db/deleteData.php",
        type: "POST",
        data: {
            "pID": sel_id
        },
        async: false,
        dataType: "json",
        success: function(data) {
            if(data.status == "ok") {
                if(reload_datatable) {
                    $('.dataTable').DataTable().ajax.reload(null, false);
                }
            } else if(data.status == "query_error") {
                bootbox.error("Questo è davvero imbarazzante! Non posso eliminare i tuoi dati! Contatta un amministratore di sistema per segnalare il problema.");
            } else if(data.status == "protected") {
                bootbox.error("Questo contenuto è protetto. Non è possibile eliminarlo.");
            } else if(data.status == "not_allowed") {
                bootbox.error("Non hai abbastanza privilegi per eseguire questa operazione!");
            } else if(data.status == "deleting_saas_owner") {
                bootbox.error("Non è possibile eliminare il proprietario dell'abbonamento!");
            } else if(data.status == "user_with_role") {
                bootbox.error("Impossibile eliminare questo ruolo in quanto è già stato assegnato ad uno o più utenti.");
            } else if(data.status == "agent_has_immobili" || data.status == "agent_has_assistenti" || data.status == "agent_has_customers" || data.status == "agent_has_requests") { //utilizzato solo in modulo utenti per le funzionalità extra immobiliari
                showModalDelAgente(data.status, data.id, "delete");
            }

            deactivateDeletingStatus();

            if(reload_index) {
                window.location.href = 'index.php';
            }
        }
    })
}

function activateDeletingStatus() {
    original_del_value = $('.dataTableDelete').html();
    $('.dataTableDelete').html($('.dataTableDelete').attr('deleting_txt')).addClass('disabled').attr('disabled', 'disabled');
    $('.dataTableEdit').addClass('disabled').attr('disabled', true);
    $('.dataTableNew').addClass('disabled').attr('disabled', true);
}

function deactivateDeletingStatus() {
    $('.dataTableDelete').html(original_del_value).removeClass('disabled').attr('disabled', false);
    $('.dataTableEdit').removeClass('disabled').attr('disabled', false);
    $('.dataTableNew').removeClass('disabled').attr('disabled', false);
}

function refreshDatatableButtons(table_id) {

    var rowInfo = getSelectedRows(table_id);
    var buttonAttributes = {read: "1", edit: "1", delete: "1"};

    rowInfo.forEach(function (attr) {
        if(typeof(attr.read) == 'undefined' || attr.read == 0) {
            buttonAttributes.read = "0";
        }
        if(typeof(attr.edit) == 'undefined' || attr.edit == 0) {
            buttonAttributes.edit = "0";
        }
        if(typeof(attr.delete) == 'undefined' || attr.delete == 0) {
            buttonAttributes.delete = "0";
        }
    });

    if(rowInfo.length && typeof(rowInfo[0].id) !== 'undefined') {
        if(rowInfo.length > 1) {
            if($('#dataTableEdit_' + table_id).css('display') !== 'none') $('#dataTableEdit_' + table_id).hide();
            if($('#dataTableSocialShare_' + table_id).css('display') !== 'none') $('#dataTableSocialShare_' + table_id).hide();
            $('#dataTableDelete_' + table_id + ', #dataTableSendTestTemplate_' + table_id + ', #dataTableBugSolved_'  + table_id ).show();
        }
        else $('#dataTableEdit_' + table_id + ', #dataTableDelete_' + table_id + ', #dataTableSendTestTemplate_' + table_id + ', #dataTableSocialShare_' + table_id + ', #dataTableBugSolved_' + table_id).show();
    } else {
        $('#dataTableEdit_' + table_id + ', #dataTableDelete_' + table_id + ', #dataTableSendTestTemplate_' + table_id + ', #dataTableSocialShare_' + table_id + ', #dataTableBugSolved_' + table_id).hide();
    }

    edit_btn_text = "Modifica";
    if(rowInfo.length === 1 && buttonAttributes.read === "1") {
        edit_btn_text = "Visualizza";
    }

    if(buttonAttributes.delete != "1" || $('#usingPrimaryLang').val() === "0") {
        $('#dataTableDelete_' + table_id).hide();
    }

    $('#dataTableEdit_' + table_id).html(edit_btn_text);
}

/*
 * Questa funzione ottiene la lista delle righe selezionate (anche quelle in altre pagine)
 */
function getSelectedRows(table_id) {
    var selected_rows = [];
    var current_rows_info = {};
    if(dataTablesRef[table_id].column().checkboxes.selected().length) {
        dataTablesRef[table_id].column().checkboxes.selected().each(function(id) {
            if(typeof(selectedRowsInfo[id]) !== 'undefined') {
                selected_rows.push(selectedRowsInfo[id]);
                current_rows_info[id] = selectedRowsInfo[id];
            }
        });
        window.selectedRowsInfo = current_rows_info;
    } else if($('#' + table_id + ' tbody tr.selected').length) {
        selected_rows.push(getRowInfo($('#' + table_id + ' tbody tr.selected')));
    }
    return selected_rows;
}

function updateRowSelectionPopup(row_selected) {
    if(row_selected > 0) {
        $('#selectedRowCounts').show().find('b').text(row_selected);
        $('#selectedRowCounts').find('.rowsTXT').html((row_selected == "1" ? "riga" : "righe"));
    } else {
        $('#selectedRowCounts').hide();
    }
}

/*
 * Seleziona la riga (modalità multiselezione)
 */
function multiSelectRow(table_id, $rows) {
    $rows.each(function () {
        var cell = dataTablesRef[table_id].cell(dataTablesRef[table_id].row(this), dataTablesRef[table_id].column(0));
        dataTablesRef[table_id].cell(cell[0]).checkboxes.select(true);
    });
}

/*
 * Deleziona la riga (modalità multiselezione)
 */
function multiUnselectRow(table_id, $rows) {
    $rows.each(function () {
        var cell = dataTablesRef[table_id].cell(dataTablesRef[table_id].row(this), dataTablesRef[table_id].column(0));
        dataTablesRef[table_id].cell(cell[0]).checkboxes.select(false);
    });
}

/*
 * Deseleziona tutte le righe
 */
function unselectAllRow(table_id) {
    $('#' + table_id + ' tbody tr').removeClass('selected multiselected');
    dataTablesRef[table_id].column().checkboxes.deselectAll();
    selectedRowsInfo = {};
    updateRowSelectionPopup(0);
}
/*
 * Questa funzione ottiene gli attributi di una determinata riga
 */
function getRowInfo($row) {
    var attributes = {};
    if( $row.length ) {
        $.each( $row[0].attributes, function( index, attr ) {
            attributes[ attr.name ] = attr.value;
        } );
    }
    return attributes;
}
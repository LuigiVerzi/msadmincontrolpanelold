$.fn.serpPreview = function(inputs, init) {

    if(!$(this).length) return false;

    var $element = this;

    if(typeof(init) === 'undefined') init = true;

    var DESC_LENGHT = 156;
    var TITLE_LENGTH = 78;

    var $updateElements = $();

    var title = '';
    var description = '';
    var url = '';

    inputs.forEach(function (input) {

        if(input.preview_title) {
            if(typeof(input.preview_title) == 'object') {
                if(!title.length) {
                    title = '';
                    input.preview_title.forEach(function (single) {
                        title += single.val();
                    });
                }
                input.preview_title.forEach(function (single) {
                    $updateElements.push(single[0]);
                });
            } else if(!title.length) title = input.preview_title;
        }

        if(input.preview_description) {
            if(typeof(input.preview_description) == 'object') {
                if(!description.length) {
                    description = '';
                    input.preview_description.forEach(function (single) {
                        description += single.val();
                    });
                }
                input.preview_description.forEach(function (single) {
                    $updateElements.push(single[0]);
                });

            } else if(!description.length) description = input.preview_description;
        }

        if(input.preview_url) {
            if(typeof(input.preview_url) == 'object') {
                if(!url.length) {
                    url = '';
                    input.preview_url.forEach(function (single) {
                        url += single.val();
                    });
                }
                input.preview_url.forEach(function (single) {
                    $updateElements.push(single[0]);
                });
            } else if(!url.length) url = input.url;
        }

    });

    console.log($updateElements);

    if(init) {
        $updateElements.on('keyup', function (event) {
            $element.serpPreview(inputs, false);
        });
    }

    return this.each( function() {
        // Create the html to insert on target element
        var html = "";
        html += "<div class='serpPreview'>";
        html += "<div class='PreviewContainer'>";

        html += "<div class='title'>" + "<a href='#'>" + truncate(title, TITLE_LENGTH) + "</a>" + "</div>";
        html += "<div class='url'>" + url + "</div>";
        html += "<div class='description'>" + truncate(description, DESC_LENGHT) + "</div>";

        html += "</div>";
        html += "</div>";

        // Insert the html created on target element
        $(this).html(html);
    });

}

function truncate(text, maxLength) {
    if (text.length > maxLength) {
        text = text.substr(0, maxLength-3) + "...";
    }
    return text;
}